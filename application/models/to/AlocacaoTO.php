<?php

/**
 * Classe para encapsular os dados da tabela de alocacao
 * @author Eduardo Rom�o - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */

class AlocacaoTO extends Ead1_TO_Dinamico

{

    const SITUACAO_ATIVA = 55;

    const EVOLUCAO_CURSANDO = 14;

    const SITUACAOTCC_NAOAPTO = 121;
    const SITUACAOTCC_APTO = 122;
    const SITUACAOTCC_PENDENTE = 203;
    const SITUACAOTCC_EM_ANALISE = 204;
    const SITUACAOTCC_DEVOLVIDO_ALUNO = 205;
    const SITUACAOTCC_AVALIADO_PELO_ORIENTADOR = 206;
    const SITUACAOTCC_DEVOLVIDO_TUTOR = 207;

    public $id_alocacao;

    public $id_saladeaula;

    public $id_evolucao;

    public $id_situacao;

    public $id_usuariocadastro;

    public $id_matriculadisciplina;

    public $dt_cadastro;

    public $bl_ativo;

    public $id_situacaotcc;

    public $id_categoriasala;

    public $bl_desalocar;

    public $dt_inicio;

    public $nu_diasextensao;


    /**
     * @return the $id_alocacao
     */

    public function getId_alocacao()
    {

        return $this->id_alocacao;

    }


    /**
     * @return int $id_saladeaula
     */

    public function getId_saladeaula()
    {

        return $this->id_saladeaula;

    }


    /**
     * @return the $id_evolucao
     */

    public function getId_evolucao()
    {

        return $this->id_evolucao;

    }


    /**
     * @return the $id_situacao
     */

    public function getId_situacao()
    {

        return $this->id_situacao;

    }


    /**
     * @return the $id_usuariocadastro
     */

    public function getId_usuariocadastro()
    {

        return $this->id_usuariocadastro;

    }


    /**
     * @return int $id_matriculadisciplina
     */

    public function getId_matriculadisciplina()
    {

        return $this->id_matriculadisciplina;

    }


    /**
     * @return the $dt_cadastro
     */

    public function getDt_cadastro()
    {

        return $this->dt_cadastro;

    }


    /**
     * @return the $dt_inicio
     */

    public function getDt_inicio()
    {

        return $this->dt_inicio;

    }


    /**
     * @return the $bl_ativo
     */

    public function getBl_ativo()
    {

        return $this->bl_ativo;

    }


    /**
     * @param field_type $id_alocacao
     */

    public function setId_alocacao($id_alocacao)
    {

        $this->id_alocacao = $id_alocacao;

    }


    /**
     * @param int $id_saladeaula
     */

    public function setId_saladeaula($id_saladeaula)
    {

        $this->id_saladeaula = $id_saladeaula;

    }


    /**
     * @param int $id_evolucao
     */

    public function setId_evolucao($id_evolucao)
    {

        $this->id_evolucao = $id_evolucao;

    }


    /**
     * @param int $id_situacao
     */

    public function setId_situacao($id_situacao)
    {

        $this->id_situacao = $id_situacao;

    }


    /**
     * @param field_type $id_usuariocadastro
     */

    public function setId_usuariocadastro($id_usuariocadastro)
    {

        $this->id_usuariocadastro = $id_usuariocadastro;

    }


    /**
     * @param int $id_matriculadisciplina
     */

    public function setId_matriculadisciplina($id_matriculadisciplina)
    {

        $this->id_matriculadisciplina = $id_matriculadisciplina;

    }


    /**
     * @param field_type $dt_cadastro
     */

    public function setDt_cadastro($dt_cadastro)
    {

        $this->dt_cadastro = $dt_cadastro;

    }
  /**
     * @param field_type $dt_inicio
     */

    public function setDt_inicio($dt_inicio)
    {

        $this->dt_inicio = $dt_inicio;

    }


    /**
     * @param field_type $bl_ativo
     */

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }


    public function setId_situacaotcc($id_situacaotcc)
    {

        $this->id_situacaotcc = $id_situacaotcc;
    }


    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }


    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return
     */

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function setBl_desalocar($bl_desalocar)
    {
        $this->bl_desalocar = $bl_desalocar;
    }

    public function getBl_desalocar()
    {
        return $this->bl_desalocar;
    }



    public function setNu_diasextensao($nu_diasextensao)
    {
        $this->nu_diasextensao = $nu_diasextensao;
    }

    public function getNu_diasextensao()
    {
        return $this->nu_diasextensao;
    }

}


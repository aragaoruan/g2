<?php

/**
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @since 23/05/2012
 * @package models
 * @subpackage to
 */
class VwNucleoCoTO extends Ead1_TO_Dinamico
{

    public $id_nucleoco;
    public $id_tipoocorrencia;
    public $id_entidade;
    public $id_usuariocadastro;
    public $id_situacao;
    public $st_nucleoco;
    public $dt_cadastro;
    public $bl_ativo;
    public $id_textonotificacao;
    public $bl_notificaatendente;
    public $bl_notificaresponsavel;
    public $st_tipoocorrencia;
    public $st_situacao;
    public $id_assuntocancelamento;

    /**
     * @return mixed
     */
    public function getid_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param mixed $id_nucleoco
     * @return VwNucleoCoTO
     */
    public function setid_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param mixed $id_tipoocorrencia
     * @return VwNucleoCoTO
     */
    public function setid_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     * @return VwNucleoCoTO
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $id_usuariocadastro
     * @return VwNucleoCoTO
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param mixed $id_situacao
     * @return VwNucleoCoTO
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_nucleoco()
    {
        return $this->st_nucleoco;
    }

    /**
     * @param mixed $st_nucleoco
     * @return VwNucleoCoTO
     */
    public function setst_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return VwNucleoCoTO
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     * @return VwNucleoCoTO
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    /**
     * @param mixed $id_textonotificacao
     * @return VwNucleoCoTO
     */
    public function setid_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param mixed $bl_notificaatendente
     * @return VwNucleoCoTO
     */
    public function setbl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_notificaresponsavel()
    {
        return $this->bl_notificaresponsavel;
    }

    /**
     * @param mixed $bl_notificaresponsavel
     * @return VwNucleoCoTO
     */
    public function setbl_notificaresponsavel($bl_notificaresponsavel)
    {
        $this->bl_notificaresponsavel = $bl_notificaresponsavel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_tipoocorrencia()
    {
        return $this->st_tipoocorrencia;
    }

    /**
     * @param mixed $st_tipoocorrencia
     * @return VwNucleoCoTO
     */
    public function setst_tipoocorrencia($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param mixed $st_situacao
     * @return VwNucleoCoTO
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_assuntocancelamento()
    {
        return $this->id_assuntocancelamento;
    }

    /**
     * @param mixed $id_assuntocancelamento
     * @return VwNucleoCoTO
     */
    public function setid_assuntocancelamento($id_assuntocancelamento)
    {
        $this->id_assuntocancelamento = $id_assuntocancelamento;
        return $this;
    }


}

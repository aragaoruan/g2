<?php
/**
 * Classe para encapsular os dados de vw integração da disciplina no moodle.
 * @author Denise Xavier
 * @package models
 * @subpackage to
 */
class VwDisciplinaIntegracaoMoodleTO extends DisciplinaIntegracaoTO{
	
	public $st_nomeentidade;
    public $st_sistema;
	
	
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
    }

    public function getSt_sistema()
    {
        return $this->st_sistema;
    }



	

}

?>
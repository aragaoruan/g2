<?php
/**
 * Classe para encapsular os dados da view de grade e sala.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class VwMatriculaGradeSalaTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	public $id_matricula;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $id_modulo;
	public $st_modulo;
	public $st_moduloexibicao;
	public $id_disciplina;
	public $st_disciplina;
	public $st_disciplinaexibicao;
	public $id_saladeaula;
	public $st_saladeaula;
	public $id_matriculadisciplina;
	public $id_situacao;
	public $st_situacao;
	public $id_evolucao;
	public $st_evolucao;
	public $nu_notafinal;
	public $nu_notatemp;
	public $st_situacaoentrega;
	public $st_nomecompleto;
	
	
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacaoentrega() {
		return $this->st_situacaoentrega;
	}
	
	/**
	 * @param unknown_type $nu_notatemp
	 */
	public function setSt_situacaoentrega($st_situacaoentrega) {
		$this->st_situacaoentrega = $st_situacaoentrega;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_notatemp() {
		return $this->nu_notatemp;
	}
	
	/**
	 * @param unknown_type $nu_notatemp
	 */
	public function setNu_notatemp($nu_notatemp) {
		$this->nu_notatemp = $nu_notatemp;
	}

	
	/**
	 * @return unknown
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}
	
	/**
	 * @param unknown_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matriculadisciplina() {
		return $this->id_matriculadisciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_notafinal() {
		return $this->nu_notafinal;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplinaexibicao() {
		return $this->st_disciplinaexibicao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_moduloexibicao() {
		return $this->st_moduloexibicao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_matriculadisciplina
	 */
	public function setId_matriculadisciplina($id_matriculadisciplina) {
		$this->id_matriculadisciplina = $id_matriculadisciplina;
	}
	
	/**
	 * @param unknown_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param unknown_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $nu_notafinal
	 */
	public function setNu_notafinal($nu_notafinal) {
		$this->nu_notafinal = $nu_notafinal;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_disciplinaexibicao
	 */
	public function setSt_disciplinaexibicao($st_disciplinaexibicao) {
		$this->st_disciplinaexibicao = $st_disciplinaexibicao;
	}
	
	/**
	 * @param unknown_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param unknown_type $st_moduloexibicao
	 */
	public function setSt_moduloexibicao($st_moduloexibicao) {
		$this->st_moduloexibicao = $st_moduloexibicao;
	}
	
	/**
	 * @param unknown_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

}

?>
<?php
/**
 * Classe de valor representante da view vw_produtoprojetotipovalor
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class VwProdutoProjetoTipoValorTO extends Ead1_TO_Dinamico {
	
	public $id_produto;
	public $id_situacao;
	public $id_entidade;
	public $id_entidadematriz;
	public $id_projetopedagogico;
	public $id_produtovalor;
	public $id_tipoprodutovalor;
	public $id_nivelensino;
	public $id_areaconhecimento;
	public $id_contratoregra;
	
	public $st_produto;
	public $st_situacao;
	public $st_descricaosituacao;
	public $st_nomeentidade;
	public $st_tituloexibicao;
	public $st_tipoprodutovalor;
	public $st_nivelensino;
	public $st_projetopedagogico;
	
	public $bl_ativo;
	public $bl_todasformas;
	public $bl_todascampanhas;

	public $dt_inicio;
	public $dt_termino;

	public $nu_valor;
	public $nu_valormensal;
	public $nu_basepropor;
	public $bl_turma;
	

	/**
	 * @return the $bl_turma
	 */
	public function getBl_turma() {
		return $this->bl_turma;
	}
	
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_entidadematriz
	 */
	public function getId_entidadematriz() {
		return $this->id_entidadematriz;
	}


	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_produtovalor
	 */
	public function getId_produtovalor() {
		return $this->id_produtovalor;
	}

	/**
	 * @return the $id_tipoprodutovalor
	 */
	public function getId_tipoprodutovalor() {
		return $this->id_tipoprodutovalor;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_descricaosituacao
	 */
	public function getSt_descricaosituacao() {
		return $this->st_descricaosituacao;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $st_tipoprodutovalor
	 */
	public function getSt_tipoprodutovalor() {
		return $this->st_tipoprodutovalor;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $bl_todasformas
	 */
	public function getBl_todasformas() {
		return $this->bl_todasformas;
	}

	/**
	 * @return the $bl_todascampanhas
	 */
	public function getBl_todascampanhas() {
		return $this->bl_todascampanhas;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $nu_valormensal
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}

	/**
	 * @return the $nu_basepropor
	 */
	public function getNu_basepropor() {
		return $this->nu_basepropor;
	}

	/**
	 * @param $id_produto the $id_produto to set
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_entidadematriz the $id_entidadematriz to set
	 */
	public function setId_entidadematriz($id_entidadematriz) {
		$this->id_entidadematriz = $id_entidadematriz;
	}


	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $id_produtovalor the $id_produtovalor to set
	 */
	public function setId_produtovalor($id_produtovalor) {
		$this->id_produtovalor = $id_produtovalor;
	}

	/**
	 * @param $id_tipoprodutovalor the $id_tipoprodutovalor to set
	 */
	public function setId_tipoprodutovalor($id_tipoprodutovalor) {
		$this->id_tipoprodutovalor = $id_tipoprodutovalor;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $st_produto the $st_produto to set
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param $st_descricaosituacao the $st_descricaosituacao to set
	 */
	public function setSt_descricaosituacao($st_descricaosituacao) {
		$this->st_descricaosituacao = $st_descricaosituacao;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $st_tituloexibicao the $st_tituloexibicao to set
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param $st_tipoprodutovalor the $st_tipoprodutovalor to set
	 */
	public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
		$this->st_tipoprodutovalor = $st_tipoprodutovalor;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param $st_projetopedagogico the $st_projetopedagogico to set
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $bl_todasformas the $bl_todasformas to set
	 */
	public function setBl_todasformas($bl_todasformas) {
		$this->bl_todasformas = $bl_todasformas;
	}

	/**
	 * @param $bl_todascampanhas the $bl_todascampanhas to set
	 */
	public function setBl_todascampanhas($bl_todascampanhas) {
		$this->bl_todascampanhas = $bl_todascampanhas;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_termino the $dt_termino to set
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @param $nu_valor the $nu_valor to set
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param $nu_valormensal the $nu_valormensal to set
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
	}

	/**
	 * @param $nu_basepropor the $nu_basepropor to set
	 */
	public function setNu_basepropor($nu_basepropor) {
		$this->nu_basepropor = $nu_basepropor;
	}
	/**
	 * @return the $id_contratoregra
	 */
	public function getId_contratoregra() {
		return $this->id_contratoregra;
	}

	/**
	 * @param $id_contratoregra the $id_contratoregra to set
	 */
	public function setId_contratoregra($id_contratoregra) {
		$this->id_contratoregra = $id_contratoregra;
	}

	/**
	 * @param $bl_turma the $bl_turma to set
	 */
	public function setBl_turma($bl_turma) {
		$this->bl_turma = $bl_turma;
	}



}
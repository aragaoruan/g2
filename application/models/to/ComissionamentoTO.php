<?php
/**
 * Classe para encapsular os dados da tb_comissionamento.
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class ComissionamentoTO extends Ead1_TO_Dinamico {
	
	public $id_comissionamento;
	public $id_usuariocadastro;
	public $nu_maxmeta;
	public $nu_minmeta;
	public $nu_comissao;
	public $dt_cadastro;
	public $bl_ativo;
	
	/**
	 * @return the $id_comissionamento
	 */
	public function getId_comissionamento() {
		return $this->id_comissionamento;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $nu_maxmeta
	 */
	public function getNu_maxmeta() {
		return $this->nu_maxmeta;
	}

	/**
	 * @return the $nu_minmeta
	 */
	public function getNu_minmeta() {
		return $this->nu_minmeta;
	}

	/**
	 * @return the $nu_comissao
	 */
	public function getNu_comissao() {
		return $this->nu_comissao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_comissionamento
	 */
	public function setId_comissionamento($id_comissionamento) {
		$this->id_comissionamento = $id_comissionamento;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $nu_maxmeta
	 */
	public function setNu_maxmeta($nu_maxmeta) {
		$this->nu_maxmeta = $nu_maxmeta;
	}

	/**
	 * @param field_type $nu_minmeta
	 */
	public function setNu_minmeta($nu_minmeta) {
		$this->nu_minmeta = $nu_minmeta;
	}

	/**
	 * @param field_type $nu_comissao
	 */
	public function setNu_comissao($nu_comissao) {
		$this->nu_comissao = $nu_comissao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

}

?>
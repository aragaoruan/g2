<?php
/**
 * Classe para encapsular os dados de convênio.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ConvenioTO extends Ead1_TO_Dinamico {

	/**
	 * Id do convênio.
	 * @var int
	 */
	public $id_convenio;
	
	/**
	 * Nome do convênio.
	 * @var String
	 */
	public $st_convenio;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return int
	 */
	public function getId_convenio() {
		return $this->id_convenio;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return String
	 */
	public function getSt_convenio() {
		return $this->st_convenio;
	}
	
	/**
	 * @param int $id_convenio
	 */
	public function setId_convenio($id_convenio) {
		$this->id_convenio = $id_convenio;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param String $st_convenio
	 */
	public function setSt_convenio($st_convenio) {
		$this->st_convenio = $st_convenio;
	}

}

?>
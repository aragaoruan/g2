<?php

/**
 * Classe TO para NuclePessoaCo
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 * 
 */
class NucleoPessoaCoTO extends Ead1_TO_Dinamico {

		public $id_nucleopessoaco;
		public $id_nucleoco;
		public $id_assuntoco;
		public $id_usuario;
		public $id_funcao;
		public $id_usuariocadastro;
		public $dt_cadastro;
		public $bl_prioritario;
        	public $bl_todos;
		
		
		/**
	 * @return the $id_nucleopessoaco
	 */
	public function getId_nucleopessoaco() {
		return $this->id_nucleopessoaco;
	}

		/**
	 * @param field_type $id_nucleopessoaco
	 */
	public function setId_nucleopessoaco($id_nucleopessoaco) {
		$this->id_nucleopessoaco = $id_nucleopessoaco;
	}

		/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

		/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

		/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

		/**
	 * @param field_type $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

		/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

		/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

		/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

		/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

		/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

		/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

		/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

		/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

		/**
	 * @return the $bl_prioritario
	 */
	public function getBl_prioritario() {
		return $this->bl_prioritario;
	}

		/**
	 * @param field_type $bl_prioritario
	 */
	public function setBl_prioritario($bl_prioritario) {
		$this->bl_prioritario = $bl_prioritario;
	}

		
}

?>

<?php
/**
 * Classe para encapsular os dados de contato de telefone para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosTelefoneEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Atributo que contém o id do telefone.
	 * @var int
	 */
	public $id_telefone;
	
	/**
	 * Atributo que diz se o telefone é o padrão da entidade.
	 * @var boolean
	 */
	public $bl_padrao;
	
	/**
	 * @return boolean
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_telefone() {
		return $this->id_telefone;
	}
	
	/**
	 * @param boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_telefone
	 */
	public function setId_telefone($id_telefone) {
		$this->id_telefone = $id_telefone;
	}

}

?>
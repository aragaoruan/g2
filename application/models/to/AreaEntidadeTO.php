<?php
/**
 * Classe para encapsular os dados de relacionamento entre entidade e área.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class AreaEntidadeTO extends Ead1_TO_Dinamico {

	public $id_areaconhecimento;
	
	/**
	 * @var unknown_type
	 */
	public $id_entidade;
	
	/**
	 * @return unknown
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param unknown_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}

?>
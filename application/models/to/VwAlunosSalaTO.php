<?php

class VwAlunosSalaTO extends Ead1_TO_Dinamico {

    public $id_alocacao;
    public $id_saladeaula;
    public $id_usuario;
    public $st_cpf;
    public $st_email;
    public $st_nomecompleto;
    public $id_matricula;
    public $id_disciplina;
    public $id_matriculadisciplina;
    public $st_nota;
    public $id_logacesso;
    public $id_sistema;
    public $id_entidade;
    public $id_projetopedagogico;
    public $st_projetopedagogico;
    public $st_evolucao;
    public $st_urlacesso;
    public $id_situacaotcc;
    public $id_upload;
    public $id_tipodisciplina;
    public $id_professor;
    public $st_professor;
    public $id_emailprofessor;
    public $st_emailprofessor;
    public $id_entidadematricula;
    public $id_categoriasala;

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param field_type $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return the $id_alocacao
     */
    public function getId_alocacao() {
        return $this->id_alocacao;
    }

    /**
     * @return the $id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @return the $st_email
     */
    public function getSt_email() {
        return $this->st_email;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @return the $id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @return the $st_nota
     */
    public function getSt_nota() {
        return $this->st_nota;
    }

    /**
     * @return the $id_logacesso
     */
    public function getId_logacesso() {
        return $this->id_logacesso;
    }

    /**
     * @return the $id_sistema
     */
    public function getId_sistema() {
        return $this->id_sistema;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @return the $st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @return the $st_urlacesso
     */
    public function getSt_urlacesso() {
        return $this->st_urlacesso;
    }

    /**
     * @param field_type $id_alocacao
     */
    public function setId_alocacao($id_alocacao) {
        $this->id_alocacao = $id_alocacao;
    }

    /**
     * @param field_type $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param field_type $st_email
     */
    public function setSt_email($st_email) {
        $this->st_email = $st_email;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param field_type $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }

    /**
     * @param field_type $st_nota
     */
    public function setSt_nota($st_nota) {
        $this->st_nota = $st_nota;
    }

    /**
     * @param field_type $id_logacesso
     */
    public function setId_logacesso($id_logacesso) {
        $this->id_logacesso = $id_logacesso;
    }

    /**
     * @param field_type $id_sistema
     */
    public function setId_sistema($id_sistema) {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @param field_type $st_evolucao
     */
    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @param field_type $st_urlacesso
     */
    public function setSt_urlacesso($st_urlacesso) {
        $this->st_urlacesso = $st_urlacesso;
    }

    public function setId_situacaotcc($id_situacaotcc) {
        $this->id_situacaotcc = $id_situacaotcc;
    }

    public function getId_situacaotcc() {
        return $this->id_situacaotcc;
    }

    public function getId_upload() {
        return $this->id_upload;
    }

    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }
    
    public function getId_professor() {
        return $this->id_professor;
    }

    public function getSt_professor() {
        return $this->st_professor;
    }

    public function getId_emailprofessor() {
        return $this->id_emailprofessor;
    }

    public function getSt_emailprofessor() {
        return $this->st_emailprofessor;
    }

    public function setId_professor($id_professor) {
        $this->id_professor = $id_professor;
    }

    public function setSt_professor($st_professor) {
        $this->st_professor = $st_professor;
    }

    public function setId_emailprofessor($id_emailprofessor) {
        $this->id_emailprofessor = $id_emailprofessor;
    }

    public function setSt_emailprofessor($st_emailprofessor) {
        $this->st_emailprofessor = $st_emailprofessor;
    }

    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @return int
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param int $id_categoriasala
     * @return $this
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

}

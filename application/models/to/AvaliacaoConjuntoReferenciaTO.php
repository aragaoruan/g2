<?php
/**
 * Classe para encapsular os dados de referencia do conjunto de avaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoConjuntoReferenciaTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjuntoreferencia;
	public $id_avaliacaoconjunto;
	public $id_projetopedagogico;
	public $id_saladeaula;
	public $id_modulo;
	public $dt_inicio;
	public $dt_fim;
	
	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
	public function getId_avaliacaoconjuntoreferencia() {
		return $this->id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoreferencia
	 */
	public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

}
<?php
/**
 * Classe para encapsular os dados de país.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PaisTO extends Ead1_TO_Dinamico {

	const BRASIL = 22;
	
	/**
	 * Contém o id do país.
	 * @var int
	 */
	public $id_pais;
	
	/**
	 * Contém o nome do País.
	 * @var string
	 */
	public $st_nomepais;
	
	/**
	 * @return int
	 */
	public function getId_pais() {
		return $this->id_pais;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomepais() {
		return $this->st_nomepais;
	}
	
	/**
	 * @param int $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}
	
	/**
	 * @param string $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais) {
		$this->st_nomepais = $st_nomepais;
	}

}

?>
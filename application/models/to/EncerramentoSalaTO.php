<?php
/**
 * Classe para encapsular os dados da de encerramento de sala de aula
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class EncerramentoSalaTO extends Ead1_TO_Dinamico {

    public $id_encerramentosala;
    public $id_saladeaula;
    public $id_usuariocoordenador;
    public $id_usuariopedagogico;
    public $id_usuarioprofessor;
    public $id_usuariofinanceiro;
    public $dt_encerramentoprofessor;
    public $dt_encerramentocoordenador;
    public $dt_encerramentopedagogico;
    public $dt_encerramentofinanceiro;
    public $dt_recusa;
    public $id_usuariorecusa;
    public $st_motivorecusa;
    public $nu_valorpago;

    /**
     * @return int
     */
    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_coordenador() {
        return $this->id_coordenador;
    }

    /**
     * @param int $id_coordenador
     */
    public function setId_coordenador($id_coordenador) {
        $this->id_coordenador = $id_coordenador;
    }

    /**
     * @return int
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_usuarioprofessor() {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param int $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor) {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    /**
     * @return int
     */
    public function getId_usuariocoordenador() {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param int $id_usuariocoordenador
     */
    public function setId_usuariocoordenador($id_usuariocoordenador) {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    /**
     * @return int
     */
    public function getId_usuariopedagogico() {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param int $id_usuariopedagogico
     */
    public function setId_usuariopedagogico($id_usuariopedagogico) {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    /**
     * @return int
     */
    public function getId_usuariofinanceiro() {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param int $id_usuariofinanceiro
     */
    public function setId_usuariofinanceiro($id_usuariofinanceiro) {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor() {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador() {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getDt_encerramentopedagogico() {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getDt_encerramentofinanceiro() {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * @return string
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function setDt_recusa($dt_recusa) {
        $this->dt_recusa = $dt_recusa;
    }

    public function getDt_recusa() {
        return $this->dt_recusa;
    }

    public function setId_usuariorecusa($id_usuariorecusa) {
        $this->id_usuariorecusa = $id_usuariorecusa;
    }

    public function getId_usuariorecusa() {
        return $this->id_usuariorecusa;
    }

    public function setSt_motivorecusa($st_motivorecusa) {
        $this->st_motivorecusa = $st_motivorecusa;
    }

    public function getSt_motivorecusa() {
        return $this->st_motivorecusa;
    }

    public function getNu_valorpago() {
        return $this->nu_valorpago;
    }

    public function setNu_valorpago($nu_valorpago) {
        $this->nu_valorpago = $nu_valorpago;
    }

}

?>
<?php
class DocumentoEntregueImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_documentoentregueimportacao;
	public $id_entregadocumentos;
	public $id_sistemaimportacao;
	public $id_pessoaimportacao;
	public $nu_codmatriculaorigem;
	public $nu_coddocumentoorigem;
	
	/**
	 * @return the $id_documentoentregueimportacao
	 */
	public function getId_documentoentregueimportacao() {
		return $this->id_documentoentregueimportacao;
	}

	/**
	 * @return the $id_entregadocumentos
	 */
	public function getId_entregadocumentos() {
		return $this->id_entregadocumentos;
	}

	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @return the $id_pessoaimportacao
	 */
	public function getId_pessoaimportacao() {
		return $this->id_pessoaimportacao;
	}

	/**
	 * @return the $nu_codmatriculaorigem
	 */
	public function getNu_codmatriculaorigem() {
		return $this->nu_codmatriculaorigem;
	}

	/**
	 * @return the $nu_coddocumentoorigem
	 */
	public function getNu_coddocumentoorigem() {
		return $this->nu_coddocumentoorigem;
	}

	/**
	 * @param field_type $id_documentoentregueimportacao
	 */
	public function setId_documentoentregueimportacao($id_documentoentregueimportacao) {
		$this->id_documentoentregueimportacao = $id_documentoentregueimportacao;
	}

	/**
	 * @param field_type $id_entregadocumentos
	 */
	public function setId_entregadocumentos($id_entregadocumentos) {
		$this->id_entregadocumentos = $id_entregadocumentos;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}

	/**
	 * @param field_type $id_pessoaimportacao
	 */
	public function setId_pessoaimportacao($id_pessoaimportacao) {
		$this->id_pessoaimportacao = $id_pessoaimportacao;
	}

	/**
	 * @param field_type $nu_codmatriculaorigem
	 */
	public function setNu_codmatriculaorigem($nu_codmatriculaorigem) {
		$this->nu_codmatriculaorigem = $nu_codmatriculaorigem;
	}

	/**
	 * @param field_type $nu_coddocumentoorigem
	 */
	public function setNu_coddocumentoorigem($nu_coddocumentoorigem) {
		$this->nu_coddocumentoorigem = $nu_coddocumentoorigem;
	}

}
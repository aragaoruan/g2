<?php
/**
 * Classe para encapsular os dados da view de geração de certificado
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwGerarCertificadoTO extends Ead1_TO_Dinamico {

	public $id_matricula;
	
	public $id_entidade;
	
	public $st_entidade;
	
	public $st_rg;
	
	public $dt_dataexpedicao;
	
	public $st_orgaoexpeditor;
	
	public $sg_uf;
	
	public $id_municipio;
	
	public $st_municipio;
	
	public $dt_nascimento;
	
	public $id_usuario;
	
	public $st_nomecompleto;
	
	public $id_nivelensino;
	
	public $st_nivelensino;
	
	/**
	 * @return unknown
	 */
	public function getDt_dataexpedicao() {
		return $this->dt_dataexpedicao;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_rg() {
		return $this->st_rg;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_entidade() {
		return $this->st_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_municipio() {
		return $this->st_municipio;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}
	
	/**
	 * @param unknown_type $dt_dataexpedicao
	 */
	public function setDt_dataexpedicao($dt_dataexpedicao) {
		$this->dt_dataexpedicao = $dt_dataexpedicao;
	}
	
	/**
	 * @param unknown_type $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param unknown_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type st_rgg
	 */
	public function setSt_rg($st_rg) {
		$this->st_rg = $st_rg;
	}
	
	/**
	 * @param unknown_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param unknown_type $st_entidade
	 */
	public function setSt_entidade($st_entidade) {
		$this->st_entidade = $st_entidade;
	}
	
	/**
	 * @param unknown_type $st_municipio
	 */
	public function setSt_municipio($st_municipio) {
		$this->st_municipio = $st_municipio;
	}
	
	/**
	 * @param unknown_type $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}

	
}

?>
<?php
/**
 * To da view vw_areaprojetonivelensino
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwAreaProjetoNivelEnsinoTO extends Ead1_TO_Dinamico {

	/**
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	
	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	
}

?>
<?php
/**
 * Classe para encapsular os dados da view de disciplinas e pre requisitos
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwDisciplinaRequisitoTO extends Ead1_TO_Dinamico{
	
	public $id_trilha;
	public $id_disciplina;
	public $st_disciplina;
	public $id_disciplinaprerequisito;
	public $st_disciplinaprerequisito;
	
	/**
	 * @return the $id_trilha
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $id_disciplinaprerequisito
	 */
	public function getId_disciplinaprerequisito() {
		return $this->id_disciplinaprerequisito;
	}

	/**
	 * @return the $st_disciplinaprerequisito
	 */
	public function getSt_disciplinaprerequisito() {
		return $this->st_disciplinaprerequisito;
	}

	/**
	 * @param field_type $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $id_disciplinaprerequisito
	 */
	public function setId_disciplinaprerequisito($id_disciplinaprerequisito) {
		$this->id_disciplinaprerequisito = $id_disciplinaprerequisito;
	}

	/**
	 * @param field_type $st_disciplinaprerequisito
	 */
	public function setSt_disciplinaprerequisito($st_disciplinaprerequisito) {
		$this->st_disciplinaprerequisito = $st_disciplinaprerequisito;
	}

	
	
}
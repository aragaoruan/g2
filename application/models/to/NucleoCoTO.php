<?php


/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class NucleoCoTO extends Ead1_TO_Dinamico
{

    public $id_nucleoco;
    public $id_tipoocorrencia;
    public $id_entidade;
    public $id_usuariocadastro;
    public $id_situacao;
    public $st_nucleoco;
    public $dt_cadastro;
    public $bl_ativo;
    public $id_textonotificacao;
    public $bl_notificaatendente;
    public $bl_notificaresponsavel;
    public $st_corinteracao;
    public $nu_horasmeta;
    public $st_cordevolvida;
    public $st_coratrasada;
    public $st_cordefault;
    public $id_nucleofinalidade;
    public $id_assuntocoresgate;
    public $id_assuntorecuperacao;
    public $id_assuntorenovacao;
    public $id_holdingcompartilhamento;
    public $id_assuntocancelamento;

    /**
     * @return int $id_nucleoco
     */
    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    /**
     * @return int $id_tipoocorrencia
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     */
    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return string $st_nucleoco
     */
    public function getSt_nucleoco()
    {
        return $this->st_nucleoco;
    }

    /**
     * @param string $st_nucleoco
     */
    public function setSt_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
    }

    /**
     * @return int $nu_horasmeta
     */
    public function getNu_horasmeta()
    {
        return $this->nu_horasmeta;
    }

    /**
     * @param int $nu_horasmeta
     */
    public function setNu_horasmeta($nu_horasmeta)
    {
        $this->nu_horasmeta = $nu_horasmeta;
    }

    /**
     * @return string $st_corinteracao
     */
    public function getSt_corinteracao()
    {
        return $this->st_corinteracao;
    }

    /**
     * @param string $st_corinteracao
     */
    public function setSt_corinteracao($st_corinteracao)
    {
        $this->st_corinteracao = $st_corinteracao;
    }

    /**
     * @return string $st_cordevolvida
     */
    public function getSt_cordevolvida()
    {
        return $this->st_cordevolvida;
    }

    /**
     * @param string $st_cordevolvida
     */
    public function setSt_cordevolvida($st_cordevolvida)
    {
        $this->st_cordevolvida = $st_cordevolvida;
    }

    /**
     * @return string $st_coratrasada
     */
    public function getSt_coratrasada()
    {
        return $this->st_coratrasada;
    }

    /**
     * @param string $st_coratrasada
     */
    public function setSt_coratrasada($st_coratrasada)
    {
        $this->st_coratrasada = $st_coratrasada;
    }

    /**
     * @return string $st_cordefault
     */
    public function getSt_cordefault()
    {
        return $this->st_cordefault;
    }

    /**
     * @param string $st_cordefault
     */
    public function setSt_cordefault($st_cordefault)
    {
        $this->st_cordefault = $st_cordefault;
    }

    /**
     * @return string $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param string $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return bool $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return int $id_textonotificacao
     */
    public function getId_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    /**
     * @param int $id_textonotificacao
     */
    public function setId_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
    }

    /**
     * @return bool $bl_notificaatendente
     */
    public function getBl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param bool $bl_notificaatendente
     */
    public function setBl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
    }

    /**
     * @return bool $bl_notificaresponsavel
     */
    public function getBl_notificaresponsavel()
    {
        return $this->bl_notificaresponsavel;
    }

    /**
     * @param bool $bl_notificaresponsavel
     */
    public function setBl_notificaresponsavel($bl_notificaresponsavel)
    {
        $this->bl_notificaresponsavel = $bl_notificaresponsavel;
    }

    /**
     * @return int $id_nucleofinalidade
     */
    public function getId_nucleofinalidade()
    {
        return $this->id_nucleofinalidade;
    }

    /**
     * @param int $id_nucleofinalidade
     */
    public function setId_nucleofinalidade($id_nucleofinalidade)
    {
        $this->id_nucleofinalidade = $id_nucleofinalidade;
    }

    /**
     * @param mixed $id_assuntocoresgate
     * @return NucleoCoTO
     */
    public function setId_assuntocoresgate($id_assuntocoresgate)
    {
        $this->id_assuntocoresgate = $id_assuntocoresgate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_assuntocoresgate()
    {
        return $this->id_assuntocoresgate;
    }

    /**
     * @param mixed $id_assuntorecuperacao
     * @return NucleoCoTO
     */
    public function setId_assuntorecuperacao($id_assuntorecuperacao)
    {
        $this->id_assuntorecuperacao = $id_assuntorecuperacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_assuntorecuperacao()
    {
        return $this->id_assuntorecuperacao;
    }

    /**
     * @param mixed $id_assuntorenovacao
     * @return NucleoCoTO
     */
    public function setId_assuntorenovacao($id_assuntorenovacao)
    {
        $this->id_assuntorenovacao = $id_assuntorenovacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_assuntorenovacao()
    {
        return $this->id_assuntorenovacao;
    }

    /**
     * @return int
     */
    public function getId_holdingcompartilhamento()
    {
        return $this->id_holdingcompartilhamento;
    }

    /**
     * @param int $id_holdingcompartilhamento
     * @return $this
     */
    public function setId_holdingcompartilhamento($id_holdingcompartilhamento)
    {
        $this->id_holdingcompartilhamento = $id_holdingcompartilhamento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_assuntocancelamento()
    {
        return $this->id_assuntocancelamento;
    }

    /**
     * @param mixed $id_assuntocancelamento
     */
    public function setid_assuntocancelamento($id_assuntocancelamento)
    {
        $this->id_assuntocancelamento = $id_assuntocancelamento;
    }

}

<?php

class VwRetornoLancamentoTO extends Ead1_TO_Dinamico {

	public $id_arquivoretorno;
	public $st_arquivoretorno;
	public $id_entidade;
	public $bl_ativoretorno;
	public $st_banco;
	
	public $id_arquivoretornolancamento;
	public $st_nossonumero;
	public $nu_valorretorno;
    public $nu_descontoretorno;
    public $nu_jurosretorno;
	public $dt_ocorrenciaretorno;
	public $bl_quitadoretorno;
	public $nu_valor;
	
	public $id_meiopagamento;
	public $bl_quitado;
	public $dt_vencimento;
	public $dt_quitado;
	public $dt_emissao;
	public $dt_prevquitado;
	public $id_entidadelancamento;
	public $bl_ativo;
	public $id_lancamento;
	public $st_coddocumento;
    public $st_carteira;
    public $nu_valornominal;
    public $dt_quitadoretorno;
    public $nu_tarifa;
    /** @var campo para efetuar o calculo no php */
    public $nu_valortotal;
	public $id_venda;

    /**
     * @param mixed $nu_descontoretorno
     */
    public function setNu_descontoretorno($nu_descontoretorno)
    {
        $this->nu_descontoretorno = $nu_descontoretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_descontoretorno()
    {
        return $this->nu_descontoretorno;
    }

    /**
     * @param mixed $nu_jurosretorno
     */
    public function setNu_jurosretorno($nu_jurosretorno)
    {
        $this->nu_jurosretorno = $nu_jurosretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_jurosretorno()
    {
        return $this->nu_jurosretorno;
    }


	/**
	 * @return the $st_coddocumento
	 */
	public function getSt_coddocumento() {
		return $this->st_coddocumento;
	}

	/**
	 * @param field_type $st_coddocumento
	 */
	public function setSt_coddocumento($st_coddocumento) {
		$this->st_coddocumento = $st_coddocumento;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @return the $id_arquivoretorno
	 */
	public function getId_arquivoretorno() {
		return $this->id_arquivoretorno;
	}

	/**
	 * @return the $st_arquivoretorno
	 */
	public function getSt_arquivoretorno() {
		return $this->st_arquivoretorno;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativoretorno
	 */
	public function getBl_ativoretorno() {
		return $this->bl_ativoretorno;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @return the $id_arquivoretornolancamento
	 */
	public function getId_arquivoretornolancamento() {
		return $this->id_arquivoretornolancamento;
	}

	/**
	 * @return the $st_nossonumero
	 */
	public function getSt_nossonumero() {
		return $this->st_nossonumero;
	}

	/**
	 * @return the $nu_valorretorno
	 */
	public function getNu_valorretorno() {
		return $this->nu_valorretorno;
	}

	/**
	 * @return the $dt_ocorrenciaretorno
	 */
	public function getDt_ocorrenciaretorno() {
		return $this->dt_ocorrenciaretorno;
	}

	/**
	 * @return the $bl_quitadoretorno
	 */
	public function getBl_quitadoretorno() {
		return $this->bl_quitadoretorno;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $bl_quitado
	 */
	public function getBl_quitado() {
		return $this->bl_quitado;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $dt_quitado
	 */
	public function getDt_quitado() {
		return $this->dt_quitado;
	}

	/**
	 * @return the $dt_emissao
	 */
	public function getDt_emissao() {
		return $this->dt_emissao;
	}

	/**
	 * @return the $dt_prevquitado
	 */
	public function getDt_prevquitado() {
		return $this->dt_prevquitado;
	}

	/**
	 * @return the $id_entidadelancamento
	 */
	public function getId_entidadelancamento() {
		return $this->id_entidadelancamento;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_arquivoretorno
	 */
	public function setId_arquivoretorno($id_arquivoretorno) {
		$this->id_arquivoretorno = $id_arquivoretorno;
	}

	/**
	 * @param field_type $st_arquivoretorno
	 */
	public function setSt_arquivoretorno($st_arquivoretorno) {
		$this->st_arquivoretorno = $st_arquivoretorno;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_ativoretorno
	 */
	public function setBl_ativoretorno($bl_ativoretorno) {
		$this->bl_ativoretorno = $bl_ativoretorno;
	}

	/**
	 * @param field_type $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	/**
	 * @param field_type $id_arquivoretornolancamento
	 */
	public function setId_arquivoretornolancamento($id_arquivoretornolancamento) {
		$this->id_arquivoretornolancamento = $id_arquivoretornolancamento;
	}

	/**
	 * @param field_type $st_nossonumero
	 */
	public function setSt_nossonumero($st_nossonumero) {
		$this->st_nossonumero = $st_nossonumero;
	}

	/**
	 * @param field_type $nu_valorretorno
	 */
	public function setNu_valorretorno($nu_valorretorno) {
		$this->nu_valorretorno = $nu_valorretorno;
	}

	/**
	 * @param field_type $dt_ocorrenciaretorno
	 */
	public function setDt_ocorrenciaretorno($dt_ocorrenciaretorno) {
		$this->dt_ocorrenciaretorno = $dt_ocorrenciaretorno;
	}

	/**
	 * @param field_type $bl_quitadoretorno
	 */
	public function setBl_quitadoretorno($bl_quitadoretorno) {
		$this->bl_quitadoretorno = $bl_quitadoretorno;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param field_type $bl_quitado
	 */
	public function setBl_quitado($bl_quitado) {
		$this->bl_quitado = $bl_quitado;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $dt_quitado
	 */
	public function setDt_quitado($dt_quitado) {
		$this->dt_quitado = $dt_quitado;
	}

	/**
	 * @param field_type $dt_emissao
	 */
	public function setDt_emissao($dt_emissao) {
		$this->dt_emissao = $dt_emissao;
	}

	/**
	 * @param field_type $dt_prevquitado
	 */
	public function setDt_prevquitado($dt_prevquitado) {
		$this->dt_prevquitado = $dt_prevquitado;
	}

	/**
	 * @param field_type $id_entidadelancamento
	 */
	public function setId_entidadelancamento($id_entidadelancamento) {
		$this->id_entidadelancamento = $id_entidadelancamento;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

    /**
     * @param mixed $st_carteira
     */
    public function setSt_carteira($st_carteira)
    {
        $this->st_carteira = $st_carteira;
    }

    /**
     * @return mixed
     */
    public function getSt_carteira()
    {
        return $this->st_carteira;
    }

    /**
     * @param mixed $dt_quitadoretorno
     */
    public function setDt_quitadoretorno($dt_quitadoretorno)
    {
        $this->dt_quitadoretorno = $dt_quitadoretorno;
    }

    /**
     * @return mixed
     */
    public function getDt_quitadoretorno()
    {
        return $this->dt_quitadoretorno;
    }

    /**
     * @param mixed $nu_valornominal
     */
    public function setNu_valornominal($nu_valornominal)
    {
        $this->nu_valornominal = $nu_valornominal;
    }

    /**
     * @return mixed
     */
    public function getNu_valornominal()
    {
        return $this->nu_valornominal;
    }

    /**
     * @param mixed $nu_tarifa
     */
    public function setNu_tarifa($nu_tarifa)
    {
        $this->nu_tarifa = $nu_tarifa;
    }

    /**
     * @return mixed
     */
    public function getNu_tarifa()
    {
        return $this->nu_tarifa;
    }

    /**
     * @param mixed $nu_valortotal
     */
    public function setNu_valortotal($nu_valortotal)
    {
        $this->nu_valortotal = $nu_valortotal;
    }

    /**
     * @return mixed
     */
    public function getNu_valortotal()
    {
        return $this->nu_valortotal;
    }

	/**
	 * @return mixed
	 */
	public function getId_venda()
	{
		return $this->id_venda;
	}

	/**
	 * @param mixed $id_venda
	 */
	public function setId_venda($id_venda)
	{
		$this->id_venda = $id_venda;
	}

	
}

<?php
/**
 * Classe para encapsular da vw
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwSincronizarMoodleTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	public $id_perfilreferencia;
	public $id_saladeaulaintegracao;
	public $st_codsistemacurso;
	public $id_sistema;
	public $nu_codigoperfil;
	public $id_entidade;
	public $id_saladeaula;
	public $st_nomecompleto;
	public $st_saladeaula;
	public $id_entidadeintegracao;

	/**
	 * @return integer $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param integer $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return integer $id_perfilreferencia
	 */
	public function getId_perfilreferencia() {
		return $this->id_perfilreferencia;
	}

	/**
	 * @param integer $id_perfilreferencia
	 */
	public function setId_perfilreferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}

	/**
	 * @return integer $id_saladeaulaintegracao
	 */
	public function getId_saladeaulaintegracao() {
		return $this->id_saladeaulaintegracao;
	}

	/**
	 * @param integer $id_saladeaulaintegracao
	 */
	public function setId_saladeaulaintegracao($id_saladeaulaintegracao) {
		$this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
	}

	/**
	 * @return string $st_codsistemacurso
	 */
	public function getSt_codsistemacurso() {
		return $this->st_codsistemacurso;
	}

	/**
	 * @param string $st_codsistemacurso
	 */
	public function setSt_codsistemacurso($st_codsistemacurso) {
		$this->st_codsistemacurso = $st_codsistemacurso;
	}

	/**
	 * @return string $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return string $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @param string $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @return integer $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param integer $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return integer $nu_codigoperfil
	 */
	public function getNu_codigoperfil() {
		return $this->nu_codigoperfil;
	}

	/**
	 * @param integer $nu_codigoperfil
	 */
	public function setNu_codigoperfil($nu_codigoperfil) {
		$this->nu_codigoperfil = $nu_codigoperfil;
	}
	/**
	 * @return integer $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param integer $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return integer $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param integer $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return integer
	 */
	public function getId_entidadeintegracao()
	{
		return $this->id_entidadeintegracao;
	}

	/**
	 * @param integer $id_entidadeintegracao
	 */
	public function setId_entidadeintegracao($id_entidadeintegracao)
	{
		$this->id_entidadeintegracao = $id_entidadeintegracao;
	}

}

?>
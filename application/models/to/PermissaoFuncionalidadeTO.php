<?php
/**
 * Classe para encapsular dados que definem quais permissões a funcionalidade possui.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PermissaoFuncionalidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da funcionalidade.
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Contém o id da permissão.
	 * @var int
	 */
	public $id_permissao;
	
	/**
	 * @return int
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_permissao() {
		return $this->id_permissao;
	}
	
	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}
	
	/**
	 * @param int $id_permissao
	 */
	public function setId_permissao($id_permissao) {
		$this->id_permissao = $id_permissao;
	}

}

?>
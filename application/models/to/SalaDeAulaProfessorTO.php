<?php
/**
 * Classe para encapsular os dados de professor com sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class SalaDeAulaProfessorTO extends Ead1_TO_Dinamico {

	
	/**
	 * Id de professor da sala de aula.
	 * @var int
	 */
	public $id_saladeaulaprofessor;
	
	/**
	 * Número máximo de alunos na sala de aula para aquele professor.
	 * @var int
	 */
	public $nu_maximoalunos;
	
	/**
	 * Id do tipo de avaliação da sala de aula.
	 * @var int
	 */
	public $id_tipoavaliacaosaladeaula;
	
	/**
	 * Diz se aquele professor é o titular.
	 * @var Boolean
	 */
	public $bl_titular;
	
	/**
	 * Id do perfil referencia.
	 * @var int
	 */
	public $id_perfilreferencia;
	
	/**
	 * @return int
	 */
	public function getId_tipoavaliacaosaladeaula() {
		return $this->id_tipoavaliacaosaladeaula;
	}
	
	/**
	 * @return int
	 */
	public function getNu_maximoalunos() {
		return $this->nu_maximoalunos;
	}
	
	/**
	 * @param int $id_tipoavaliacaosaladeaula
	 */
	public function setId_tipoavaliacaosaladeaula($id_tipoavaliacaosaladeaula) {
		$this->id_tipoavaliacaosaladeaula = $id_tipoavaliacaosaladeaula;
	}
	
	/**
	 * @param int $nu_maximoalunos
	 */
	public function setNu_maximoalunos($nu_maximoalunos) {
		$this->nu_maximoalunos = $nu_maximoalunos;
	}
	
	/**
	 * @return Boolean
	 */
	public function getBl_titular() {
		return $this->bl_titular;
	}
	
	/**
	 * @return int
	 */
	public function getId_saladeaulaprofessor() {
		return $this->id_saladeaulaprofessor;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfilReferencia() {
		return $this->id_perfilreferencia;
	}
	
	/**
	 * @param Boolean $bl_titular
	 */
	public function setBl_titular($bl_titular) {
		$this->bl_titular = $bl_titular;
	}
	
	/**
	 * @param int $id_saladeaulaprofessor
	 */
	public function setId_saladeaulaprofessor($id_saladeaulaprofessor) {
		$this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
	}
	
	/**
	 * @param int $id_perfilreferencia
	 */
	public function setId_perfilReferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}

}

?>
<?php
/**
 * Classe de valor da tabela tb_tipoareaconhecimento
 * @author Arthur Cláudio de ALmeida Pereira < arthur.almeida@ead1.com.br >
 */

class TipoAreaConhecimentoTO extends Ead1_TO_Dinamico {
	
	public $id_tipoareaconhecimento;
	public $st_tipoareaconhecimento;
	public $st_descricao;
	/**
	 * @return the $id_tipoareaconhecimento
	 */
	public function getId_tipoareaconhecimento(){

		return $this->id_tipoareaconhecimento;
	}

	/**
	 * @return the $st_tipoareaconhecimento
	 */
	public function getSt_tipoareaconhecimento(){

		return $this->st_tipoareaconhecimento;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao(){

		return $this->st_descricao;
	}

	/**
	 * @param $id_tipoareaconhecimento the $id_tipoareaconhecimento to set
	 */
	public function setId_tipoareaconhecimento( $id_tipoareaconhecimento ){

		$this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
	}

	/**
	 * @param $st_tipoareaconhecimento the $st_tipoareaconhecimento to set
	 */
	public function setSt_tipoareaconhecimento( $st_tipoareaconhecimento ){

		$this->st_tipoareaconhecimento = $st_tipoareaconhecimento;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao( $st_descricao ){

		$this->st_descricao = $st_descricao;
	}

	
	
}
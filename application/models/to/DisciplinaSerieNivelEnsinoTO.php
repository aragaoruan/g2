<?php
/**
 * Classe para encapsular os dados de relação de série e nível de ensino com disciplina.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DisciplinaSerieNivelEnsinoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

}

?>
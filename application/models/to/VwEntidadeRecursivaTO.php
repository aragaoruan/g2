<?php
/**
 * Classe para encapsular os dados de view de entidades com suas filhas.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeRecursivaTO extends Ead1_TO_Dinamico {

	public $id_entidade;
	
	public $id_entidadepai;
	
	public $st_nomeentidade;
	
	public $nu_nivelhierarquia;
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_nivelhierarquia() {
		return $this->nu_nivelhierarquia;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_entidadepai
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}
	
	/**
	 * @param unknown_type $nu_nivelhierarquia
	 */
	public function setNu_nivelhierarquia($nu_nivelhierarquia) {
		$this->nu_nivelhierarquia = $nu_nivelhierarquia;
	}
	
	/**
	 * @param unknown_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

}

?>
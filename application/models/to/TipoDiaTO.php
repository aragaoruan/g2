<?php
/**
 * Classe para encapsular os dados de tipo de dia.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDiaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de dia.
	 * @var int
	 */
	public $id_tipodia;
	
	/**
	 * Nome do tipo de dia.
	 * @var string
	 */
	public $st_tipodia;
	
	/**
	 * @return int
	 */
	public function getId_tipodia() {
		return $this->id_tipodia;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipodia() {
		return $this->st_tipodia;
	}
	
	/**
	 * @param int $id_tipodia
	 */
	public function setId_tipodia($id_tipodia) {
		$this->id_tipodia = $id_tipodia;
	}
	
	/**
	 * @param string $st_tipodia
	 */
	public function setSt_tipodia($st_tipodia) {
		$this->st_tipodia = $st_tipodia;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de rede social com a pessoa.
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class ContatosRedeSocialPessoaTO extends Ead1_TO_Dinamico {

	/**
	* Contém o id da usuario.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id da rede social.
	 * @var string
	 */
	public $id_redesocial;
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_redesocial() {
		return $this->id_redesocial;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param string $id_redesocial
	 */
	public function setId_redesocial($id_redesocial) {
		$this->id_redesocial = $id_redesocial;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de tipo sanguíneo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoSanguineoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do tipo sanguíneo.
	 * @var int
	 */
	public $id_tiposanguineo;
	

	/**
	 * Contém a sigla do tipo sanguíneo.
	 * @var string
	 */
	public $sg_tiposanguineo;
	
	/**
	 * @return int
	 */
	public function getId_tiposanguineo() {
		return $this->id_tiposanguineo;
	}
	
	/**
	 * @return string
	 */
	public function getSg_tiposanguineo() {
		return $this->sg_tiposanguineo;
	}
	/**
	 * @param int $id_tiposanguineo
	 */
	public function setId_tiposanguineo($id_tiposanguineo) {
		$this->id_tiposanguineo = $id_tiposanguineo;
	}
	
	/**
	 * @param string $sg_tiposanguineo
	 */
	public function setSg_tiposanguineo($sg_tiposanguineo) {
		$this->sg_tiposanguineo = $sg_tiposanguineo;
	}

}

?>
<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package application
 * @subpackage to
 */
class VwLogMatriculaTO extends Ead1_TO_Dinamico {
	
	public $id_matricula;
	public $st_nomecompleto;
	public $id_usuario;
	public $id_funcionalidade;
	public $st_saladeaula;
	public $st_funcionalidade;
	public $st_nomeperfil;
	public $dt_cadastro;
	public $bl_institucional;
	
	
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $st_funcionalidade
	 */
	public function getSt_funcionalidade() {
		return $this->st_funcionalidade;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_institucional
	 */
	public function getBl_institucional() {
		return $this->bl_institucional;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param field_type $st_funcionalidade
	 */
	public function setSt_funcionalidade($st_funcionalidade) {
		$this->st_funcionalidade = $st_funcionalidade;
	}

	/**
	 * @param field_type $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_institucional
	 */
	public function setBl_institucional($bl_institucional) {
		$this->bl_institucional = $bl_institucional;
	}

	
	
	
	

}
<?php
/**
 * Classe para encapsular os dados da view de responsável legal.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 21/02/2011
 * 
 * @package models
 * @subpackage to
 */
class VwReponsavelLegalTO extends Ead1_TO_Dinamico {

	public $id_entidaderesponsavellegal;
	
	public $id_entidade;
	
	public $bl_padrao;
	
	public $id_situacao;
	
	public $st_situacao;
	
	public $id_tipoentidaderesponsavellegal;
	
	public $st_tipoentidaderesponsavellegal;
	
	public $id_usuario;
	
	public $nu_registro;
	
	public $sg_uf;
	
	public $st_orgaoexpeditor;
	
	/**
	 * @return unknown
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidaderesponsavellegal() {
		return $this->id_entidaderesponsavellegal;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoentidaderesponsavellegal() {
		return $this->id_tipoentidaderesponsavellegal;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_registro() {
		return $this->nu_registro;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipoentidaderesponsavellegal() {
		return $this->st_tipoentidaderesponsavellegal;
	}
	
	/**
	 * @param unknown_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_entidaderesponsavellegal
	 */
	public function setId_entidaderesponsavellegal($id_entidaderesponsavellegal) {
		$this->id_entidaderesponsavellegal = $id_entidaderesponsavellegal;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $id_tipoentidaderesponsavellegal
	 */
	public function setId_tipoentidaderesponsavellegal($id_tipoentidaderesponsavellegal) {
		$this->id_tipoentidaderesponsavellegal = $id_tipoentidaderesponsavellegal;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $nu_registro
	 */
	public function setNu_registro($nu_registro) {
		$this->nu_registro = $nu_registro;
	}
	
	/**
	 * @param unknown_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param unknown_type $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
	
	/**
	 * @param unknown_type $st_tipoentidaderesponsavellegal
	 */
	public function setSt_tipoentidaderesponsavellegal($st_tipoentidaderesponsavellegal) {
		$this->st_tipoentidaderesponsavellegal = $st_tipoentidaderesponsavellegal;
	}

}

?>
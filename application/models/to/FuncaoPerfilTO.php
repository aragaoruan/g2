<?php
/**
 * Classe para encapsular os dados de Função com Perfil.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class FuncaoPerfilTO extends Ead1_TO_Dinamico {

	public $id_funcao;
	public $id_perfil;
	public $id_usuariocadastro;
	public $dt_cadastro;
	public $bl_ativo;
	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

}

?>
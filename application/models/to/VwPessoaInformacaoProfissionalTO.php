<?php
/**
 * Classe para encapsular os dados de view de informação profissional da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaInformacaoProfissionalTO extends Ead1_TO_Dinamico {
	
	/**
	 * Variavel que contem o id da informaçao profissional da pessoa
	 * @var int
	 */
	public $id_informacaoprofissionalpessoa;

	/**
	 * Id da entidade.
	 * @var int 
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id da área de atuação da pessoa.
	 * @var int
	 */
	public $id_areaatuacao;
	
	/**
	 * Cargo da pessoa.
	 * @var string
	 */
	public $st_cargo;
	
	/**
	 * Organização da pessoa.
	 * @var string
	 */
	public $st_organizacao;
	
	/**
	 * Área de atuação da pessoa
	 * @var string
	 */
	public $st_areaatuacao;
	
	/**
	 * Data de inicio
	 * @var date
	 */
	public $dt_inicio;
	
	/**
	 * Data de inicio
	 * @var date
	 */
	public $dt_fim;
	
	/**
	 * @return int
	 */
	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_fim the $dt_fim to set
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	public function getId_areaatuacao() {
		return $this->id_areaatuacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaatuacao() {
		return $this->st_areaatuacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cargo() {
		return $this->st_cargo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_organizacao() {
		return $this->st_organizacao;
	}
	
	/**
	 * @param int $id_areaatuacao
	 */
	public function setId_areaatuacao($id_areaatuacao) {
		$this->id_areaatuacao = $id_areaatuacao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_areaatuacao
	 */
	public function setSt_areaatuacao($st_areaatuacao) {
		$this->st_areaatuacao = $st_areaatuacao;
	}
	
	/**
	 * @param string $st_cargo
	 */
	public function setSt_cargo($st_cargo) {
		$this->st_cargo = $st_cargo;
	}
	
	/**
	 * @param string $st_organizacao
	 */
	public function setSt_organizacao($st_organizacao) {
		$this->st_organizacao = $st_organizacao;
	}
	/**
	 * @return the $id_informacaoprofissionalpessoa
	 */
	public function getId_informacaoprofissionalpessoa() {
		return $this->id_informacaoprofissionalpessoa;
	}

	/**
	 * @param $id_informacaoprofissionalpessoa the $id_informacaoprofissionalpessoa to set
	 */
	public function setId_informacaoprofissionalpessoa($id_informacaoprofissionalpessoa) {
		$this->id_informacaoprofissionalpessoa = $id_informacaoprofissionalpessoa;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados da view de tipo de endereço para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 19/10/2010
 * 
 * @package models
 * @subpackage to
 */
class VwTipoEnderecoEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de endereço.
	 * @var int
	 */
	public $id_tipoendereco;
	
	/**
	 * Tipo de endereço.
	 * @var string
	 */
	public $st_tipoendereco;
	
	/**
	 * Id da categoria de endereço.
	 * @var int
	 */
	public $id_categoriaendereco;
	
	/**
	 * Categoria de endereço.
	 * @var string
	 */
	public $st_categoriaendereco;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return int
	 */
	public function getId_categoriaendereco() {
		return $this->id_categoriaendereco;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_categoriaendereco() {
		return $this->st_categoriaendereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoendereco() {
		return $this->st_tipoendereco;
	}
	
	/**
	 * @param int $id_categoriaendereco
	 */
	public function setId_categoriaendereco($id_categoriaendereco) {
		$this->id_categoriaendereco = $id_categoriaendereco;
	}
	
	/**
	 * @param int $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}
	
	/**
	 * @param string $st_categoriaendereco
	 */
	public function setSt_categoriaendereco($st_categoriaendereco) {
		$this->st_categoriaendereco = $st_categoriaendereco;
	}
	
	/**
	 * @param string $st_tipoendereco
	 */
	public function setSt_tipoendereco($st_tipoendereco) {
		$this->st_tipoendereco = $st_tipoendereco;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados do motivo da ocorrencia
 * @author Rafael Costa Rocha
 * 
 * @package models
 * @subpackage to
 */
class MotivoOcorrenciaTO extends Ead1_TO_Dinamico {

	const MOTIVO_RESGATE = 1;
	const MOTIVO_CANCELAMENTO_TRANCAMENTO = 2;
	
	/**
	 * Contém o id do motivo da ocorrencia.
	 * @var int
	 */
	public $id_motivoocorrencia;
	
	/**
	 * Contém o texto explicativo do motivo da ocorrencia.
	 * @var int
	 */
	public $st_motivoocorrencia;
	
	/**
	 * Contém a tabela referenciada pelo motivo da ocorrencia.
	 * @var int
	 */
	public $tb_motivoocorrencia;
	
	/**
	 * @return the $id_motivoocorrencia
	 */
	public function getId_motivoocorrencia() {
		return $this->id_motivoocorrencia;
	}

	/**
	 * @return the $st_motivoocorrencia
	 */
	public function getSt_motivoocorrencia() {
		return $this->st_motivoocorrencia;
	}

	/**
	 * @return the $tb_motivoocorrencia
	 */
	public function getTb_motivoocorrencia() {
		return $this->tb_motivoocorrencia;
	}

	/**
	 * @param number $id_motivoocorrencia
	 */
	public function setId_motivoocorrencia($id_motivoocorrencia) {
		$this->id_motivoocorrencia = $id_motivoocorrencia;
	}

	/**
	 * @param field_type $st_motivoocorrencia
	 */
	public function setSt_motivoocorrencia($st_motivoocorrencia) {
		$this->st_motivoocorrencia = $st_motivoocorrencia;
	}

	/**
	 * @param field_type $tb_motivoocorrencia
	 */
	public function setTb_motivoocorrencia($tb_motivoocorrencia) {
		$this->tb_motivoocorrencia = $tb_motivoocorrencia;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados da view de grade e sala por tipo.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class VwGradeTipoTccTO extends Ead1_TO_Dinamico {

	public $dt_abertura;
	public $id_avaliacao;
	public $st_nota;
	public $id_tipoavaliacao;
	public $st_labeltipo;
	public $id_matricula;
	public $id_modulo;
	public $st_modulo;
	public $st_tituloexibicaomodulo;
	public $id_disciplina;
	public $st_disciplina;
	public $st_tituloexibicaodisciplina;
	public $st_situacao;
	public $st_evolucao;
	public $nu_notafinal;
	public $nu_notamax;
	public $nu_cargahoraria;
	public $dt_encerramento;
	public $st_status;
	public $st_saladeaula;
	public $id_projetopedagogico;
    public $bl_status;
	public $nu_ordem;
	public $st_statusdisciplina;
	public $id_situacao;

    /**
     * @param mixed $bl_status
     */
    public function setBl_status($bl_status)
    {
        $this->bl_status = $bl_status;
    }

    /**
     * @return mixed
     */
    public function getBl_status()
    {
        return $this->bl_status;
    }
	
	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return unknown
	 */
	public function getSt_nota() {
		return $this->st_nota;
	}
	
	/**
	 * @param unknown_type $st_nota
	 */
	public function setSt_nota($st_nota) {
		$this->st_nota = $st_nota;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_notamax() {
		return $this->nu_notamax;
	}
	
	/**
	 * @param unknown_type $nu_notamax
	 */
	public function setNu_notamax($nu_notamax) {
		$this->nu_notamax = $nu_notamax;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}
	
	/**
	 * @param unknown_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_labeltipo() {
		return $this->st_labeltipo;
	}
	
	/**
	 * @param unknown_type $st_labeltipo
	 */
	public function setSt_labeltipo($st_labeltipo) {
		$this->st_labeltipo = $st_labeltipo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_status() {
		return $this->st_status;
	}
	
	/**
	 * @param unknown_type $st_status
	 */
	public function setSt_status($st_status) {
		$this->st_status = $st_status;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_notafinal() {
		return $this->nu_notafinal;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tituloexibicaodisciplina() {
		return $this->st_tituloexibicaodisciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tituloexibicaomodulo() {
		return $this->st_tituloexibicaomodulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}
	
	/**
	 * @param unknown_type $id_tipoavaliacao
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $nu_notafinal
	 */
	public function setNu_notafinal($nu_notafinal) {
		$this->nu_notafinal = $nu_notafinal;
	}
	
	/**
	 * @param unknown_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}
	
	/**
	 * @param unknown_type $dt_encerramento
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}
	
	/**
	 * @param unknown_type $dt_abertura
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_tituloexibicaodisciplina
	 */
	public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
		$this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
	}
	
	/**
	 * @param unknown_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param unknown_type $st_tituloexibicaomodulo
	 */
	public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
		$this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
	
	

	public function getNu_ordem()
	{
		return $this->nu_ordem;
	}

	public function setNu_ordem($nu_ordem)
	{
		$this->nu_ordem = $nu_ordem;
	}

	public function getSt_statusdisciplina()
	{
		return $this->st_statusdisciplina;
	}

	public function setSt_statusdisciplina($st_statusdisciplina)
	{
		$this->st_statusdisciplina = $st_statusdisciplina;
	}




}


<?php
/**
 * Classe para encapsular os dados de relacionamento entre entidade e perfil com mais adicões.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PerfilEntidadeCompletoTO extends Ead1_TO_Dinamico {

	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Contém o nome do perfil que a entidade quer.
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * Contém o nome da entidade.
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param string $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidadel) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

}

?>
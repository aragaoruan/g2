<?php

/**
 * Class VwAssuntosTO
 * @author Denise Xavier
 * @package models
 * @subpackage to
 */
class VwAssuntosTO extends Ead1_TO_Dinamico
{

    public $bl_abertura;
    public $bl_autodistribuicao;
    public $bl_notificaatendente;
    public $bl_notificaresponsavel;
    public $id_assuntoco;
    public $id_assuntocopai;
    public $id_entidade;
    public $id_situacao;
    public $id_textosistema;
    public $id_tipoocorrencia;
    public $st_assuntoco;
    public $bl_ativo;
    public $bl_interno;

    /**
     * @return the $bl_abertura
     */
    public function getBl_abertura()
    {
        return $this->bl_abertura;
    }

    /**
     * @return the $bl_autodistribuicao
     */
    public function getBl_autodistribuicao()
    {
        return $this->bl_autodistribuicao;
    }

    /**
     * @return the $bl_notificaatendente
     */
    public function getBl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @return the $bl_notificaresponsavel
     */
    public function getBl_notificaresponsavel()
    {
        return $this->bl_notificaresponsavel;
    }

    /**
     * @return the $id_assuntoco
     */
    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @return the $id_assuntocopai
     */
    public function getId_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_textosistema
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @return the $id_tipoocorrencia
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @return the $st_assuntoco
     */
    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }


    /**
     * @param field_type $bl_abertura
     */
    public function setBl_abertura($value)
    {
        $this->bl_abertura = $value;
    }

    /**
     * @param field_type $bl_autodistribuicao
     */
    public function setBl_autodistribuicao($value)
    {
        $this->bl_autodistribuicao = $value;
    }

    /**
     * @param field_type $bl_notificaatendente
     */
    public function setBl_notificaatendente($value)
    {
        $this->bl_notificaatendente = $value;
    }

    /**
     * @param field_type $bl_notificaresponsavel
     */
    public function setBl_notificaresponsavel($value)
    {
        $this->bl_notificaresponsavel = $value;
    }

    /**
     * @param field_type $id_assuntoco
     */
    public function setId_assuntoco($value)
    {
        $this->id_assuntoco = $value;
    }

    /**
     * @param field_type $id_assuntocopai
     */
    public function setId_assuntocopai($value)
    {
        $this->id_assuntocopai = $value;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($value)
    {
        $this->id_entidade = $value;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($value)
    {
        $this->id_situacao = $value;
    }

    /**
     * @param field_type $id_textosistema
     */
    public function setId_textosistema($value)
    {
        $this->id_textosistema = $value;
    }

    /**
     * @param field_type $id_tipoocorrencia
     */
    public function setId_tipoocorrencia($value)
    {
        $this->id_tipoocorrencia = $value;
    }

    /**
     * @param field_type $st_assuntoco
     */
    public function setSt_assuntoco($value)
    {
        $this->st_assuntoco = $value;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($value)
    {
        $this->bl_ativo = $value;
    }

    /**
     * @return mixed
     */
    public function getBl_interno()
    {
        return $this->bl_interno;
    }

    /**
     * @param mixed $bl_interno
     */
    public function setBl_interno($bl_interno)
    {
        $this->bl_interno = $bl_interno;
    }


}

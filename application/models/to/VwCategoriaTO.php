<?php

/**
 * @author Rafael Costa Rocha
 * @package models
 * @subpackage to
 *
 */
class VwCategoriaTO extends Ead1_TO_Dinamico {
	
	public $id_categoria;
	public $id_categoriapai;
	public $id_entidade;
	public $id_entidadecadastro;
	public $id_situacao;
	public $st_categoria;
	public $st_categoriapai;
	public $st_situacao;
	public $dt_cadastro;
	public $bl_ativo;
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $id_categoria
	 */
	public function getId_categoria() {
		return $this->id_categoria;
	}

	/**
	 * @param field_type $id_categoria
	 */
	public function setId_categoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	/**
	 * @return the $id_categoriapai
	 */
	public function getId_categoriapai() {
		return $this->id_categoriapai;
	}

	/**
	 * @param field_type $id_categoriapai
	 */
	public function setId_categoriapai($id_categoriapai) {
		$this->id_categoriapai = $id_categoriapai;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_categoria
	 */
	public function getSt_categoria() {
		return $this->st_categoria;
	}

	/**
	 * @param field_type $st_categoria
	 */
	public function setSt_categoria($st_categoria) {
		$this->st_categoria = $st_categoria;
	}

	/**
	 * @return the $st_categoriapai
	 */
	public function getSt_categoriapai() {
		return $this->st_categoriapai;
	}

	/**
	 * @param field_type $st_categoriapai
	 */
	public function setSt_categoriapai($st_categoriapai) {
		$this->st_categoriapai = $st_categoriapai;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
}

?>
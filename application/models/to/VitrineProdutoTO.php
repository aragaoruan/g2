<?php
/**
 * Classe para encapsular os dados da tb_vitrineproduto
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class VitrineProdutoTO extends Ead1_TO_Dinamico {

	public $id_vitrineproduto;
	public $id_vitrine;
	public $id_produto;
	public $id_categoria;
	public $bl_ativo;
	
	
	/**
	 * @return the $id_vitrineproduto
	 */
	public function getId_vitrineproduto() {
		return $this->id_vitrineproduto;
	}

	/**
	 * @param field_type $id_vitrineproduto
	 */
	public function setId_vitrineproduto($id_vitrineproduto) {
		$this->id_vitrineproduto = $id_vitrineproduto;
	}

	/**
	 * @return the $id_vitrine
	 */
	public function getId_vitrine() {
		return $this->id_vitrine;
	}

	/**
	 * @param field_type $id_vitrine
	 */
	public function setId_vitrine($id_vitrine) {
		$this->id_vitrine = $id_vitrine;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $id_categoria
	 */
	public function getId_categoria() {
		return $this->id_categoria;
	}

	/**
	 * @param field_type $id_categoria
	 */
	public function setId_categoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
}

?>
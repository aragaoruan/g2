<?php
class VwTurmasDisponiveisTO extends Ead1_TO_Dinamico{
	
	public $id_turmaprojeto;
	public $id_turma;
	public $id_projetopedagogico;
	public $nu_alunos;
	public $id_usuariocadastro;
	public $id_situacao;
	public $id_entidadecadastro;
	public $st_turma;
	public $st_tituloexibicao;
	public $nu_maxalunos;
	public $dt_inicioinscricao;
	public $dt_fimincricao;
	public $dt_inicio;
	public $dt_fim;
	public $dt_cadastro;
	public $bl_ativo;
	
	public function getId_turmaprojeto(){
		return $this->id_turmaprojeto;
	}
	
	public function setId_turmaprojeto($id_turmaprojeto){
		$this->id_turmaprojeto = $id_turmaprojeto;
	}
	
	public function getId_turma(){
		return $this->id_turma;
	}
	
	public function setId_turma($id_turma){
		$this->id_turma = $id_turma;
	}
	
	public function getNu_maxalunos(){
		return $this->nu_maxalunos;
	}
	
	public function setNu_maxalunos($nu_maxalunos){
		$this->nu_maxalunos = $nu_maxalunos;
	}
	
	public function getNu_alunos(){
		return $this->nu_alunos;
	}
	
	public function setNu_alunos($nu_alunos){
		$this->nu_alunos = $nu_alunos;
	}
	
	public function getId_usuariocadastro(){
		return $this->id_usuariocadastro;
	}
	
	public function setId_usuariocadastro($id_usuariocadastro){
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	public function getId_entidadecadastro(){
		return $this->id_entidadecadastro;
	}
	
	public function setId_entidadecadastro($id_entidadecadastro){
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	public function getId_situacao(){
		return $this->id_situacao;
	}
	
	public function setId_situacao($id_situacao){
		$this->id_situacao = $id_situacao;
	}
	
	public function getSt_turma(){
		return $this->st_turma;
	}
	
	public function setSt_turma($st_turma){
		$this->st_turma = $st_turma;
	}
	
	public function getSt_tituloexibicao(){
		return $this->st_tituloexibicao;
	}
	
	public function setSt_tituloexibicao($st_tituloexibicao){
		$this->st_tituloexibicao = $st_tituloexibicao;
	}
	
	public function getDt_inicioinscricao(){
		return $this->dt_inicioinscricao;
	}
	
	public function setDt_inicioinscricao($dt_inicioinscricao){
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}
	
	public function getDt_inicio(){
		return $this->dt_inicio;
	}
	
	public function setDt_inicio($dt_inicio){
		$this->dt_inicio = $dt_inicio;
	}
	
	public function getDt_fim(){
		return $this->dt_fim;
	}
	
	public function setDt_fim($dt_fim){
		$this->dt_fim = $dt_fim;
	}
	
	public function getDt_fiminscricao(){
		return $this->dt_fiminscricao;
	}
	
	public function setDt_fiminscricao($dt_fiminscricao){
		$this->dt_fiminscricao = $dt_fiminscricao;
	}
	
	public function getDt_cadastro(){
		return $this->dt_cadastro;
	}
	
	public function setDt_cadastro($dt_cadastro){
		$this->dt_cadastro = $dt_cadastro;
	}
	
	public function getBl_ativo(){
		return $this->bl_ativo;
	}
	
	public function setBl_ativo($bl_ativo){
		$this->bl_ativo = $bl_ativo;
	}

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }


	
}
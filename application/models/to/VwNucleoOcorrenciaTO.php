<?php
/**
 * Classe para encapsular os dados de vw_nucleoocorrencia.
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class VwNucleoOcorrenciaTO extends Ead1_TO_Dinamico {
	
    public $id_ocorrencia;
	public $id_nucleoco;
	public $st_nucleoco;
	public $id_nucleofinalidade;
	public $st_nucleofinalidade;
	public $id_venda;
	
	
	
	
	
	/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

	/**
	 * @param field_type $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @return the $st_nucleoco
	 */
	public function getSt_nucleoco() {
		return $this->st_nucleoco;
	}

	/**
	 * @return the $id_nucleofinalidade
	 */
	public function getId_nucleofinalidade() {
		return $this->id_nucleofinalidade;
	}

	/**
	 * @return the $st_nucleofinalidade
	 */
	public function getSt_nucleofinalidade() {
		return $this->st_nucleofinalidade;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	/**
	 * @param field_type $st_nucleoco
	 */
	public function setSt_nucleoco($st_nucleoco) {
		$this->st_nucleoco = $st_nucleoco;
	}

	/**
	 * @param field_type $id_nucleofinalidade
	 */
	public function setId_nucleofinalidade($id_nucleofinalidade) {
		$this->id_nucleofinalidade = $id_nucleofinalidade;
	}

	/**
	 * @param field_type $st_nucleofinalidade
	 */
	public function setSt_nucleofinalidade($st_nucleofinalidade) {
		$this->st_nucleofinalidade = $st_nucleofinalidade;
	}
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}


}

?>
<?php

/**
 * Classe de RemessaTO
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
 class RemessaTO extends Ead1_TO_Dinamico {

	public $id_remessa;
	public $id_projetopedagogico;
	public $id_usuariocadastro;
	public $id_situacao;
	public $nu_remessa;
	public $dt_cadastro;
	public $dt_inicio;
	public $dt_fim;
	public $id_entidade;
	
	
	const ATIVO 	= 81; 
	const INATIVO 	= 82; 
	
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_remessa
	 */
	public function getId_remessa() {
		return $this->id_remessa;
	}

	/**
	 * @param field_type $id_remessa
	 */
	public function setId_remessa($id_remessa) {
		$this->id_remessa = $id_remessa;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $nu_remessa
	 */
	public function getNu_remessa() {
		return $this->nu_remessa;
	}

	/**
	 * @param field_type $nu_remessa
	 */
	public function setNu_remessa($nu_remessa) {
		$this->nu_remessa = $nu_remessa;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}


}

?>
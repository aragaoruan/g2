<?php
/**
 * Classe para encapsular os dados de relacionamento entre Lancamento e campanha comercial.
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class LancamentoCampanhaTO extends Ead1_TO_Dinamico {

	
	public $id_lancamentocampanha;
	public $id_lancamento;
	public $id_campanhacomercial;
	public $dt_cadastro;
	/**
	 * @return the $id_lancamentocampanha
	 */
	public function getId_lancamentocampanha() {
		return $this->id_lancamentocampanha;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_lancamentocampanha
	 */
	public function setId_lancamentocampanha($id_lancamentocampanha) {
		$this->id_lancamentocampanha = $id_lancamentocampanha;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}


}

?>
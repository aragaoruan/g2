<?php
/**
 * Classe para encapsular os dados de view de VwModuloDisciplinaProjetoTrilhaTO.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 26/05/2010
 * 
 * @package models
 * @subpackage to
 */
class VwModuloDisciplinaProjetoTrilhaTO extends Ead1_TO_Dinamico{
	
	public $id_modulo;
	public $st_modulo;
	public $bl_ativo;
	public $id_situacao;
	public $id_projetopedagogico;
	public $st_disciplina;
	public $id_disciplina;
	public $nu_cargahoraria;
	public $nu_creditos;
	public $nu_codigoparceiro;
	public $id_tipodisciplina;
	public $id_entidade;
	public $id_trilha;
	public $id_serie;
	public $id_nivelensino;
	public $id_areaconhecimento;
	/**
	 * @todo Esta variavel não consta na view mas é retratada dinamicamente de acordo com a disponibilidade
	 * da disciplina
	 * @var string
	 */
	public $st_status;
	
	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_modulo
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @return the $nu_creditos
	 */
	public function getNu_creditos() {
		return $this->nu_creditos;
	}

	/**
	 * @return the $nu_codigoparceiro
	 */
	public function getNu_codigoparceiro() {
		return $this->nu_codigoparceiro;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_trilha
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}

	/**
	 * @param field_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param field_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}

	/**
	 * @param field_type $nu_creditos
	 */
	public function setNu_creditos($nu_creditos) {
		$this->nu_creditos = $nu_creditos;
	}

	/**
	 * @param field_type $nu_codigoparceiro
	 */
	public function setNu_codigoparceiro($nu_codigoparceiro) {
		$this->nu_codigoparceiro = $nu_codigoparceiro;
	}

	/**
	 * @param field_type $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}
	/**
	 * @return the $st_status
	 */
	public function getSt_status() {
		return $this->st_status;
	}

	/**
	 * @param string $st_status
	 */
	public function setSt_status($st_status) {
		$this->st_status = $st_status;
	}
	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}



	
}
<?php
/**
 * Classe para encapsular os dados de Entrega de Documentos
 * @author Eder Lamar edermariano@gmail.com
 * @package models
 * @subpackage to 
 */
class EntregaDocumentosTO extends Ead1_TO_Dinamico{
	
	const SITUACAO_PENDENTE = 57;
	const SITUACAO_ENTREGUE_CONFIRMADO = 59;
	
	public $id_entregadocumentos;
	public $id_documentos;
	public $id_usuario;
	public $id_entidade;
	public $id_matricula;
	public $id_situacao;
	public $dt_cadastro;
	public $id_usuariocadastro;
	public $id_contratoresponsavel;
	
	/**
	 * @return the $id_entregadocumentos
	 */
	public function getId_entregadocumentos() {
		return $this->id_entregadocumentos;
	}

	/**
	 * @return the $id_documentos
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_contratoresponsavel
	 */
	public function getId_contratoresponsavel() {
		return $this->id_contratoresponsavel;
	}

	/**
	 * @param $id_entregadocumentos the $id_entregadocumentos to set
	 */
	public function setId_entregadocumentos($id_entregadocumentos) {
		$this->id_entregadocumentos = $id_entregadocumentos;
	}

	/**
	 * @param $id_documentos the $id_documentos to set
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_matricula the $id_matricula to set
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $id_contratoresponsavel the $id_contratoresponsavel to set
	 */
	public function setId_contratoresponsavel($id_contratoresponsavel) {
		$this->id_contratoresponsavel = $id_contratoresponsavel;
	}

	
	
}
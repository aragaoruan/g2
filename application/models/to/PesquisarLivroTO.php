<?php

class PesquisarLivroTO extends VwPesquisarLivroTO {

	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.telemarketing.cadastrar.CadastrarLivro';
	
	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @param string $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

}


<?php
/**
 * Classe para encapsular os dados de funcionalidades de entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeFuncionalidadeTO extends Ead1_TO_Dinamico {
	
	const SITUACAO_ATIVA = 5;

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id da funcionalidade.
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Diz se está ativa ou não a funcionalidade.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém o id da situação da funcionalidade.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Label que vai ser usado da funcionalidade.
	 * @var String
	 */
	public $st_label;
	
	/**
	 * Diz se a funcionalidade é obrigatório.
	 * @var boolean
	 */
	public $bl_obrigatorio;
	
	/**
	 * Diz se a funcionalidade é visível para entidade.
	 * @var Boolean
	 */
	public $bl_visivel;
	
	/**
	 * @return boolean
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}
	
	/**
	 * @return Boolean
	 */
	public function getBl_visivel() {
		return $this->bl_visivel;
	}
	
	/**
	 * @return String
	 */
	public function getSt_label() {
		return $this->st_label;
	}
	
	/**
	 * @param boolean $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}
	
	/**
	 * @param Boolean $bl_visivel
	 */
	public function setBl_visivel($bl_visivel) {
		$this->bl_visivel = $bl_visivel;
	}
	
	/**
	 * @param String $st_label
	 */
	public function setSt_label($st_label) {
		$this->st_label = $st_label;
	}

	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

    public function tipaTOConsulta($exceptions = array())
    {
        return parent::tipaTOConsulta(array('bl_visivel'));
    }

}

?>
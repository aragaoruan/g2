<?php
/**
 * Classe para encapsular os dados de tipo de aplicação de avaliação.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoAplicacaoAvaliacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de aplicação de avaliação.
	 * @var int
	 */
	public $id_tipoaplicacaoavaliacao;
	
	/**
	 * Nome do tipo de aplicação de avaliação.
	 * @var string
	 */
	public $st_tipoaplicacaoavaliacao;
	
	/**
	 * Descrição do tipo de aplicação de avaliação.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tipoaplicacaoavaliacao() {
		return $this->id_tipoaplicacaoavaliacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoaplicacaoavaliacao() {
		return $this->st_tipoaplicacaoavaliacao;
	}
	
	/**
	 * @param int $id_tipoaplicacaoavaliacao
	 */
	public function setId_tipoaplicacaoavaliacao($id_tipoaplicacaoavaliacao) {
		$this->id_tipoaplicacaoavaliacao = $id_tipoaplicacaoavaliacao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipoaplicacaoavaliacao
	 */
	public function setSt_tipoaplicacaoavaliacao($st_tipoaplicacaoavaliacao) {
		$this->st_tipoaplicacaoavaliacao = $st_tipoaplicacaoavaliacao;
	}

}

?>
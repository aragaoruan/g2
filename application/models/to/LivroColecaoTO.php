<?php
/**
 * Classe para encapsular os dados de LivroColecao.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class LivroColecaoTO extends Ead1_TO_Dinamico {
	
	public $id_livrocolecao;
	public $id_entidadecadastro;
	public $id_usuariocadastro;
	public $st_livrocolecao;
	public $bl_ativo;
	public $dt_cadastro;
	
	
	
	/**
	 * @return the $id_livrocolecao
	 */
	public function getId_livrocolecao() {
		return $this->id_livrocolecao;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_livrocolecao
	 */
	public function getSt_livrocolecao() {
		return $this->st_livrocolecao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_livrocolecao
	 */
	public function setId_livrocolecao($id_livrocolecao) {
		$this->id_livrocolecao = $id_livrocolecao;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $st_livrocolecao
	 */
	public function setSt_livrocolecao($st_livrocolecao) {
		$this->st_livrocolecao = $st_livrocolecao;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}



}




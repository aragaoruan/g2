<?php
/**
 * Classe para encapsular os dados completos de funcionário de núcleo de telemarketing.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class NucleoFuncionariotmCompletoTO extends Ead1_TO_Dinamico {

	public $id_nucleofuncionariotm;
	
	public $id_funcao;
	
	public $st_funcao;
	
	public $id_usuario;
	
	public $st_nomecompleto;
	
	public $id_nucleotm;
	
	public $dt_cadastro;
	
	public $bl_ativo;
	
	public $id_usuariocadastro;
	
	public $id_situacao;
	
	public $st_situacao;
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nucleofuncionariotm() {
		return $this->id_nucleofuncionariotm;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nucleotm() {
		return $this->id_nucleotm;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $id_nucleofuncionariotm
	 */
	public function setId_nucleofuncionariotm($id_nucleofuncionariotm) {
		$this->id_nucleofuncionariotm = $id_nucleofuncionariotm;
	}
	
	/**
	 * @param unknown_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm) {
		$this->id_nucleotm = $id_nucleotm;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

}

?>
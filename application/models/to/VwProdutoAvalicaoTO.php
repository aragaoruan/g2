<?php
/**
 * Classe que encapsula dados da view de produto
 * @autor Yannick NR
 * @package models
 * @subpackage to
 */
class VwProdutoAvaliacaoTO extends Ead1_TO_Dinamico {

	public $id_avaliacao;
	public $st_avaliacao;
	public $id_tipoproduto;
	public $id_tipoavaliacao;
	public $st_linkreferencia;
	public $nu_quantquestoes;
	public $st_descricao;
	public $id_avaliacaoaplicacao;
	public $nu_maxaplicacao;
	public $dt_aplicacao;
	public $id_produto;
	public $id_entidade;
        
        
        /**
	 * @return int $id_avaliacao
	 */
	public function getId_avaliacao() {
            return $this->id_avaliacao;
        }
        /**
	 * @param int $id_avaliacao
	 */
        public function setId_avaliacao($id_avaliacao) {
            $this->id_avaliacao = $id_avaliacao;
            return $this;
        }
        
        /**
	 * @return String $st_avaliacao
	 */
        public function getSt_avaliacao() {
            return $this->st_avaliacao;
        }
        /**
	 * @param String $st_avaliacao
	 */
        public function setSt_avaliacao($st_avaliacao) {
            $this->st_avaliacao = $st_avaliacao;
            return $this;
        }
        
        /**
	 * @return int $id_tipoproduto
	 */
        public function getId_tipoproduto() {
            return $this->id_tipoproduto;
        }
        /**
	 * @return int $id_tipoproduto
	 */
        public function setId_tipoproduto($id_tipoproduto) {
            $this->id_tipoproduto = $id_tipoproduto;
            return $this;
        }
        
        /**
	 * @return int $id_tipoavaliacao
	 */
        public function getId_tipoavaliacao() {
            return $this->id_tipoavaliacao;
        }
        /**
	 * @param int $id_tipoavaliacao
	 */
        public function setId_tipoavaliacao($id_tipoavaliacao) {
            $this->id_tipoavaliacao = $id_tipoavaliacao;
            return $this;
        }
        
        /**
	 * @return String $st_linkreferencia
	 */
        public function getSt_linkreferencia() {
            return $this->st_linkreferencia;
        }
        /**
	 * @param String $st_linkreferencia
	 */
        public function setSt_linkreferencia($st_linkreferencia) {
            $this->st_linkreferencia = $st_linkreferencia;
            return $this;
        }
        
        /**
	 * @return int $nu_quantquestoes
	 */
        public function getNu_quantquestoes() {
            return $this->nu_quantquestoes;
        }
        /**
	 * @param int $nu_quantquestoes
	 */
        public function setNu_quantquestoes($nu_quantquestoes) {
            $this->nu_quantquestoes = $nu_quantquestoes;
            return $this;
        }
        
        /**
	 * @return String $st_descricao
	 */
        public function getSt_descricao() {
            return $this->st_descricao;
        }
        /**
	 * @param String $st_descricao
	 */
        public function setSt_descricao($st_descricao) {
            $this->st_descricao = $st_descricao;
            return $this;
        }
        
        /**
	 * @return int $id_avaliacaoaplicacao
	 */
        public function getId_avaliacaoaplicacao() {
            return $this->id_avaliacaoaplicacao;
        }
        /**
	 * @param int $id_avaliacaoaplicacao
	 */
        public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
            $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
            return $this;
        }
        
        /**
	 * @return int $nu_maxaplicacao
	 */
        public function getNu_maxaplicacao() {
            return $this->nu_maxaplicacao;
        }
        /**
	 * @param int $id_avaliacaoaplicacao
	 */
        public function setNu_maxaplicacao($nu_maxaplicacao) {
            $this->nu_maxaplicacao = $nu_maxaplicacao;
            return $this;
        }
        
        /**
	 * @return String $dt_aplicacao
	 */
        public function getDt_aplicacao() {
            return $this->dt_aplicacao;
        }
        /**
	 * @param String $dt_aplicacao
	 */
        public function setDt_aplicacao($dt_aplicacao) {
            $this->dt_aplicacao = $dt_aplicacao;
            return $this;
        }
        
        /**
	 * @return int $id_produto
	 */
        public function getId_produto() {
            return $this->id_produto;
        }
        /**
	 * @param int $id_produto
	 */
        public function setId_produto($id_produto) {
            $this->id_produto = $id_produto;
            return $this;
        }
        
        /**
	 * @return int $id_entidade
	 */
        public function getId_entidade() {
            return $this->id_entidade;
        }
        /**
	 * @param int $id_entidade
	 */
        public function setId_entidade($id_entidade) {
            $this->id_entidade = $id_entidade;
            return $this;
        }

}

?>
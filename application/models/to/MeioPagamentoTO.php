<?php
/**
 * Classe para encapsular os dados de meios de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class MeioPagamentoTO extends Ead1_TO_Dinamico {
	
	const CARTAO 		= 1;
	const BOLETO 		= 2;
	const DINHEIRO 		= 3;
	const CHEQUE 		= 4;
	const EMPENHO 		= 5;
	const DEPOSITO 		= 6;
	const DEBITO 		= 7;
	const TRANSFERENCIA = 8;
	const PERMUTA 		= 9;
	const BOLSA 		= 10;
	const RECORRENTE 	= 11;

	/**
	 * Id do meio de pagamento.
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * Nome do meio de pagamento.
	 * @var string
	 */
	public $st_meiopagamento;
	
	/**
	 * Descrição do meio de pagamento.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}
	
	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}

}

?>
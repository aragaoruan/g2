<?php
/**
 * @package models
 * @subpackage to
 */
class VwProjetoPlanoMatrizTO extends Ead1_TO_Dinamico {	


	public $bl_padrao;
	public $id_produto;
	public $id_projetopedagogico;
	public $nu_parcelas;
	public $nu_valorentrada;
	public $nu_valorparcela;
	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @param field_type $nu_parcelas
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @return the $nu_valorentrada
	 */
	public function getNu_valorentrada() {
		return $this->nu_valorentrada;
	}

	/**
	 * @param field_type $nu_valorentrada
	 */
	public function setNu_valorentrada($nu_valorentrada) {
		$this->nu_valorentrada = $nu_valorentrada;
	}

	/**
	 * @return the $nu_valorparcela
	 */
	public function getNu_valorparcela() {
		return $this->nu_valorparcela;
	}

	/**
	 * @param field_type $nu_valorparcela
	 */
	public function setNu_valorparcela($nu_valorparcela) {
		$this->nu_valorparcela = $nu_valorparcela;
	}



}
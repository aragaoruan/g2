<?php
/**
 * Classe para encapsular os dados da view de moduloDisciplina
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class VwModuloDisciplinaTO extends Ead1_TO_Dinamico{
	
	public $id_modulodisciplina;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $id_modulo;
	public $st_modulo;
	public $id_disciplina;
	public $st_disciplina;
	public $id_serie;
	public $st_serie;
	public $id_nivelensino;
	public $st_nivelensino;
	public $bl_ativo;
	public $bl_obrigatoria;
	public $id_areaconhecimento;
	public $nu_ordem;
	public $st_areaconhecimento;
	public $nu_ponderacaocalculada;
	public $nu_ponderacaoaplicada;
	public $id_usuarioponderacao;
	public $dt_cadastroponderacao;
	public $nu_importancia;
    public $nu_obrigatorioalocacao;
    public $nu_disponivelapartirdo;
	
	/**
	 * @return the $nu_importancia
	 */
	public function getNu_importancia() {
		return $this->nu_importancia;
	}

	/**
	 * @param field_type $nu_importancia
	 */
	public function setNu_importancia($nu_importancia) {
		$this->nu_importancia = $nu_importancia;
		return $this;
	}

	/**
	 * @return the $id_modulodisciplina
	 */
	public function getId_modulodisciplina() {
		return $this->id_modulodisciplina;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $nu_ponderacaocalculada
	 */
	public function getNu_ponderacaocalculada() {
		return $this->nu_ponderacaocalculada;
	}

	/**
	 * @return the $nu_ponderacaoaplicada
	 */
	public function getNu_ponderacaoaplicada() {
		return $this->nu_ponderacaoaplicada;
	}

	/**
	 * @return the $id_usuarioponderacao
	 */
	public function getId_usuarioponderacao() {
		return $this->id_usuarioponderacao;
	}

	/**
	 * @return the $dt_cadastroponderacao
	 */
	public function getDt_cadastroponderacao() {
		return $this->dt_cadastroponderacao;
	}

	/**
	 * @param field_type $id_modulodisciplina
	 */
	public function setId_modulodisciplina($id_modulodisciplina) {
		$this->id_modulodisciplina = $id_modulodisciplina;
		return $this;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
		return $this;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
		return $this;
	}

	/**
	 * @param field_type $nu_ponderacaocalculada
	 */
	public function setNu_ponderacaocalculada($nu_ponderacaocalculada) {
		$this->nu_ponderacaocalculada = $nu_ponderacaocalculada;
		return $this;
	}

	/**
	 * @param field_type $nu_ponderacaoaplicada
	 */
	public function setNu_ponderacaoaplicada($nu_ponderacaoaplicada) {
		$this->nu_ponderacaoaplicada = $nu_ponderacaoaplicada;
		return $this;
	}

	/**
	 * @param field_type $id_usuarioponderacao
	 */
	public function setId_usuarioponderacao($id_usuarioponderacao) {
		$this->id_usuarioponderacao = $id_usuarioponderacao;
		return $this;
	}

	/**
	 * @param field_type $dt_cadastroponderacao
	 */
	public function setDt_cadastroponderacao($dt_cadastroponderacao) {
		$this->dt_cadastroponderacao = $dt_cadastroponderacao;
		return $this;
	}

	/**
	 * @return the $bl_obrigatoria
	 */
	public function getBl_obrigatoria() {
		return $this->bl_obrigatoria;
	}

	/**
	 * @param field_type $bl_obrigatoria
	 */
	public function setBl_obrigatoria($bl_obrigatoria) {
		$this->bl_obrigatoria = $bl_obrigatoria;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_modulo
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param field_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @param field_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param field_type $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @param field_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param field_type $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param field_type $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

    public function getNu_obrigatorioalocacao()
    {
        return $this->nu_obrigatorioalocacao;
    }

    public function setNu_obrigatorioalocacao($nu_obrigatorioalocacao)
    {
        $this->nu_obrigatorioalocacao = $nu_obrigatorioalocacao;
    }

    public function getNu_disponivelapartirdo()
    {
        return $this->nu_disponivelapartirdo;
    }

    public function setNu_disponivelapartirdo($nu_disponivelapartirdo)
    {
        $this->nu_disponivelapartirdo = $nu_disponivelapartirdo;
    }


	
}
<?php
/**
 * TO que encapsula dados do Header do XLS
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class XLSHeaderTO extends Ead1_TO_Dinamico{
	
	/**
	 * Variavel que Contem o Nome que sera exibido no Header 
	 * @var string
	 */
	public $st_header;
	
	/**
	 * Variavel que contem o Parametro para ser Exibido na coluna <td> do Header
	 * @var string
	 */
	public $st_par;
	
	/**
	 * @return the $st_header
	 */
	public function getSt_header() {
		return $this->st_header;
	}

	/**
	 * @return the $st_par
	 */
	public function getSt_par() {
		return $this->st_par;
	}

	/**
	 * @param string $st_header
	 */
	public function setSt_header($st_header) {
		$this->st_header = $st_header;
	}

	/**
	 * @param string $st_par
	 */
	public function setSt_par($st_par) {
		$this->st_par = $st_par;
	}

	
	
	
}
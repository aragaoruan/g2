<?php
/**
 * Classe para encapsular os dados de tipo de disciplina.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDisciplinaTO extends Ead1_TO_Dinamico {

	const PADRAO 		= 1;
	const NORMAL 		= 1; //	Normal/Padrão	Tipo padrão de Disciplina
	const TCC	 		= 2; //	TCC	Disciplina de trabalho de conclusão de curso
	const AMBIENTACAO	= 3; //	TCC	Disciplina de trabalho de conclusão de curso
	
	/**
	 * Contém o id do tipo de disciplina.
	 * @var int
	 */
	public $id_tipodisciplina;
	
	/**
	 * Contém o nome do tipo de disciplina.
	 * @var string
	 */
	public $st_tipodisciplina;
	
	/**
	 * Contém a descrição do tipo de disciplina.
	 * @var string
	 */
	public $st_descricao;

    /**
     * @param int $id_tipodisciplina
     */
    public function setIdTipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getIdTipodisciplina()
    {
        return $this->id_tipodisciplina;
    }


    /**
     * @param string $st_descricao
     */
    public function setStDescricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return string
     */
    public function getStDescricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_tipodisciplina
     */
    public function setStTipodisciplina($st_tipodisciplina)
    {
        $this->st_tipodisciplina = $st_tipodisciplina;
    }

    /**
     * @return string
     */
    public function getStTipodisciplina()
    {
        return $this->st_tipodisciplina;
    }
	


}

?>

<?php
/**
 * Classe para encapsular os dados de relação entre projeto pedagógico e documentos.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentosProjetoPedagogicoTO extends Ead1_TO_Dinamico {

	/**
	 * Id de documentos.
	 * @var int
	 */
	public $id_documentos;
	
	/**
	 * Id de projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * @return int
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @param int $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

}

?>
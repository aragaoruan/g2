<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class SalaDeAulaEntidadeTO extends Ead1_TO_Dinamico{

	public $id_saladeaula;
	public $id_entidade;


	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula(){ 
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}


	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($value){ 
		$this->id_saladeaula = $value;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($value){ 
		$this->id_entidade = $value;
	}

}
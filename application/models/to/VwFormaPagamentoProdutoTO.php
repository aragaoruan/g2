<?php
class VwFormaPagamentoProdutoTO extends Ead1_TO_Dinamico{
	
	public $id_formapagamento;
	public $st_formapagamento;
	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $st_tipoproduto;
	
	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param field_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}


	
}
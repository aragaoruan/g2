<?php

/**
 * Classe para encapsular os dados da view de encerramento de sala de aula
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 *
 * @package models
 * @subpackage to
 */
class VwEncerramentoSalasTO extends Ead1_TO_Dinamico
{

    public $id_saladeaula;
    public $id_encerramentosala;
    public $id_disciplina;
    public $id_usuariocoordenador;
    public $id_usuariopedagogico;
    public $id_usuarioprofessor;
    public $id_usuariofinanceiro;
    public $dt_encerramentoprofessor;
    public $dt_encerramentocoordenador;
    public $dt_encerramentopedagogico;
    public $dt_encerramentofinanceiro;
    public $st_disciplina;
    public $st_saladeaula;
    public $id_areaconhecimento;
    public $st_areaconhecimento;
    public $id_entidade;
    public $st_aluno;
    public $st_tituloavaliacao;
    public $id_tipodisciplina;
    public $nu_cargahoraria;
    public $st_upload;
    public $id_aluno;
    public $id_categoriasala;
    public $st_categoriasala;
    public $st_urlinteracoes;
    public $dt_recusa;
    public $id_usuariorecusa;
    public $st_motivorecusa;
    public $id_matricula;
    public $nu_alunos;
    public $nu_valor;
    public $nu_valorpago;
    public $st_nota;
    public $st_professor;
    public $st_projetopedagogico;
    public $st_tipodisciplina;
    public $st_encerramentoprofessor;
    public $st_encerramentocoordenador;
    public $st_encerramentopedagogico;
    public $st_encerramentofinanceiro;


    /**
     * @param mixed $id_aluno
     */
    public function setId_aluno($id_aluno)
    {
        $this->id_aluno = $id_aluno;
    }

    /**
     * @return mixed
     */
    public function getId_aluno()
    {
        return $this->id_aluno;
    }

    /**
     * @return int
     */
    public function getId_encerramentosala()
    {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_aluno()
    {
        return $this->st_aluno;
    }

    /**
     * @param string $st_aluno
     */
    public function setSt_aluno($st_aluno)
    {
        $this->st_aluno = $st_aluno;
    }

    /**
     * @return string
     */
    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param string $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_usuarioprofessor()
    {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param int $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor)
    {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    /**
     * @return int
     */
    public function getId_usuariocoordenador()
    {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param int $id_usuariocoordenador
     */
    public function setId_usuariocoordenador($id_usuariocoordenador)
    {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    /**
     * @return int
     */
    public function getId_usuariopedagogico()
    {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param int $id_usuariopedagogico
     */
    public function setId_usuariopedagogico($id_usuariopedagogico)
    {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    /**
     * @return int
     */
    public function getId_usuariofinanceiro()
    {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param int $id_usuariofinanceiro
     */
    public function setId_usuariofinanceiro($id_usuariofinanceiro)
    {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor()
    {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador()
    {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador)
    {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getDt_encerramentopedagogico()
    {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico)
    {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getDt_encerramentofinanceiro()
    {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro)
    {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return the $nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param field_type $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return the $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function getSt_upload()
    {
        return $this->st_upload;
    }

    /**
     * @param field $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function setSt_upload($st_upload)
    {
        $this->st_upload = $st_upload;
    }

    /**
     * @param mixed $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $st_categoriasala
     */
    public function setSt_categoriasala($st_categoriasala)
    {
        $this->st_categoriasala = $st_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_categoriasala()
    {
        return $this->st_categoriasala;
    }

    /**
     * @param mixed $st_urlinteracoes
     */
    public function setSt_urlinteracoes($st_urlinteracoes)
    {
        $this->st_urlinteracoes = $st_urlinteracoes;
    }

    /**
     * @return mixed
     */
    public function getSt_urlinteracoes()
    {
        return $this->st_urlinteracoes;
    }

    public function setDt_recusa($dt_recusa)
    {
        $this->dt_recusa = $dt_recusa;
    }

    public function getDt_recusa()
    {
        return $this->dt_recusa;
    }

    public function setId_usuariorecusa($id_usuariorecusa)
    {
        $this->id_usuariorecusa = $id_usuariorecusa;
    }

    public function getId_usuariorecusa()
    {
        return $this->id_usuariorecusa;
    }

    public function setSt_motivorecusa($st_motivorecusa)
    {
        $this->st_motivorecusa = $st_motivorecusa;
    }

    public function getSt_motivorecusa()
    {
        return $this->st_motivorecusa;
    }

    function getId_matricula()
    {
        return $this->id_matricula;
    }

    function getNu_alunos()
    {
        return $this->nu_alunos;
    }

    function getNu_valor()
    {
        return $this->nu_valor;
    }

    function getNu_valorpago()
    {
        return $this->nu_valorpago;
    }

    function getSt_nota()
    {
        return $this->st_nota;
    }

    function getSt_professor()
    {
        return $this->st_professor;
    }

    function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    function getSt_tipodisciplina()
    {
        return $this->st_tipodisciplina;
    }

    function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    function setNu_alunos($nu_alunos)
    {
        $this->nu_alunos = $nu_alunos;
    }

    function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    function setNu_valorpago($nu_valorpago)
    {
        $this->nu_valorpago = $nu_valorpago;
    }

    function setSt_nota($st_nota)
    {
        $this->st_nota = $st_nota;
    }

    function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    function setSt_tipodisciplina($st_tipodisciplina)
    {
        $this->st_tipodisciplina = $st_tipodisciplina;
    }

    public function getSt_encerramentoprofessor()
    {
        return $this->st_encerramentoprofessor;
    }

    public function setSt_encerramentoprofessor($st_encerramentoprofessor)
    {
        $this->st_encerramentoprofessor = $st_encerramentoprofessor;
    }

    public function getSt_encerramentocoordenador()
    {
        return $this->st_encerramentocoordenador;
    }

    public function setSt_encerramentocoordenador($st_encerramentocoordenador)
    {
        $this->st_encerramentocoordenador = $st_encerramentocoordenador;
    }

    public function getSt_encerramentopedagogico()
    {
        return $this->st_encerramentopedagogico;
    }

    public function setSt_encerramentopedagogico($st_encerramentopedagogico)
    {
        $this->st_encerramentopedagogico = $st_encerramentopedagogico;
    }

    public function getSt_encerramentofinanceiro()
    {
        return $this->st_encerramentofinanceiro;
    }

    public function setSt_encerramentofinanceiro($st_encerramentofinanceiro)
    {
        $this->st_encerramentofinanceiro = $st_encerramentofinanceiro;
    }



}

<?php
/**
 * To de Login
 * @author edermariano
 * @package models
 * @subpackage to
 *
 */
class LoginTO extends Ead1_TO{
	
	public $usuario;
	public $senha;
	/**
	 * @return the $usuario
	 */
	public function getUsuario() {
		return $this->usuario;
	}

	/**
	 * @param bool $toMd5
	 * @return string
	 */
	public function getSenha($toMd5 = true) {
		return $toMd5? md5($this->senha) : $this->senha;
	}

	/**
	 * @param $usuario the $usuario to set
	 */
	public function setUsuario($usuario) {
		$this->usuario = $usuario;
	}

	/**
	 * @param $senha the $senha to set
	 */
	public function setSenha($senha) {
		$this->senha = $senha;
	}

}
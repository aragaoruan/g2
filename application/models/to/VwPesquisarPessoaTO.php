<?php
/**
 * Classe para encapsular os dados de view para pesquisa de pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPesquisarPessoaTO extends Ead1_TO_Dinamico {

	/*
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o nome completo.
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Contém o número do CPF.
	 * @var string
	 */
	public $st_cpf;
	
	/**
	 * Contém o id do município.
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Contém a UF.
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * Contém o nome do município.
	 * @var string
	 */
	public $st_nomemunicipio;
	
	/**
	 * Contém o email da pessoa.
	 * @var string
	 */
	public $st_email;
	
	/**
	 * Diz se é ativo.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Nome da instituição.
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Número do CNPJ da instituição.
	 * @var string
	 */
	public $st_cnpj;
	
	/**
	 * Razão social da instituição.
	 * @var string
	 */
	public $st_razaosocial;
	
	/**
	 * Sexo da pessoa.
	 * @var string
	 */
	public $st_sexo;
	
	/**
	 * Data de nascimento.
	 * @var string
	 */
	public $dt_nascimento;
	
	/**
	 * Nome do pai.
	 * @var string
	 */
	public $st_nomepai;
	
	/**
	 * Nome da mãe.
	 * @var string
	 */
	public $st_nomemae;
	
	/**
	 * @return string
	 */
	public function getSt_nomemae() {
		return $this->st_nomemae;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomepai() {
		return $this->st_nomepai;
	}
	
	/**
	 * @param string $st_nomemae
	 */
	public function setSt_nomemae($st_nomemae) {
		$this->st_nomemae = $st_nomemae;
	}
	
	/**
	 * @param string $st_nomepai
	 */
	public function setSt_nomepai($st_nomepai) {
		$this->st_nomepai = $st_nomepai;
	}

	
	/**
	 * @return string
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_sexo() {
		return $this->st_sexo;
	}
	
	/**
	 * @param string $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}
	
	/**
	 * @param string $st_sexo
	 */
	public function setSt_sexo($st_sexo) {
		$this->st_sexo = $st_sexo;
	}
	/**
	 * @return string
	 */
	public function getSt_cnpj() {
		return $this->st_cnpj;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}
	
	/**
	 * @param string $st_cnpj
	 */
	public function setSt_cnpj($st_cnpj) {
		$this->st_cnpj = $st_cnpj;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}

	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_email() {
		return $this->st_email;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param string $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}
	
	/**
	 * @param string $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}
	
	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param string $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

}

?>
<?php

/**
 * Classe para vincular o usuario e seu perfil a uma entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class UsuarioPerfilEntidadeTO extends Ead1_TO_Dinamico
{

    /**
     * Contém o id do usuário.
     * @var int
     */
    public $id_usuario;

    /**
     * Contém o id da entidade.
     * @var int
     */
    public $id_entidade;

    /**
     * Contém o id do perfil.
     * @var int
     */
    public $id_perfil;

    /**
     * Diz se o perfil está ou não ativo.
     * @var Boolean
     */
    public $bl_ativo;

    /**
     * Data de cadastro do registro.
     * @var String
     */
    public $dt_cadastro;

    /**
     * Data de início de vingência do usuário com o perfil nessa entidade.
     * @var String
     */
    public $dt_inicio;

    /**
     * Data de término de vingência do usuário com o perfil nessa entidade.
     * @var String
     */
    public $dt_termino;

    /**
     * Contém o id da situação do perfil nesse usuário nessa entidade.
     * @var int
     */
    public $id_situacao;

    /**
     * Diz o id da entidade em que o usuário está passando.
     * @var int
     */
    public $id_entidadeidentificacao;

    public $bl_tutorial;


    public $st_token;

    /**
     * @return mixed
     */
    public function getStToken()
    {
        return $this->st_token;
    }

    /**
     * @param mixed $st_token
     */
    public function setStToken($st_token)
    {
        $this->st_token = $st_token;
    }



    /**
     * @return int
     */
    public function getId_entidadeidentificacao()
    {
        return $this->id_entidadeidentificacao;
    }

    /**
     * @param int $id_entidadeidentificacao
     */
    public function setId_entidadeidentificacao($id_entidadeidentificacao)
    {
        $this->id_entidadeidentificacao = $id_entidadeidentificacao;
    }

    /**
     * @return String
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return String
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @return String
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param String $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param String $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @param String $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param mixed $bl_tutorial
     */
    public function setBl_tutorial($bl_tutorial)
    {
        $this->bl_tutorial = $bl_tutorial;
    }

    /**
     * @return mixed
     */
    public function getBl_tutorial()
    {
        return $this->bl_tutorial;
    }


}

<?php

/**
 * Classe de VwUnidadesTO
 
 * @package models
 * @subpackage to
 */
 class VwUnidadesTO extends Ead1_TO_Dinamico {

	public $id_entidadematriz;
	public $id_entidade;
	public $st_nomeentidade;
	public $st_razaosocial;
	public $st_urlsite;
	public $st_cnpj;
	public $st_endereco;
	public $st_complemento;
	public $nu_numero;
	public $st_nomemunicipio;
	public $sg_uf;
	public $st_cep;
	public $st_nomepais;
	public $st_bairro;
	public $nu_ddi;
	public $nu_ddd;
	public $nu_telefone;
	/**
	 * @return the $id_entidadematriz
	 */
	public function getId_entidadematriz() {
		return $this->id_entidadematriz;
	}

	/**
	 * @param field_type $id_entidadematriz
	 */
	public function setId_entidadematriz($id_entidadematriz) {
		$this->id_entidadematriz = $id_entidadematriz;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @return the $st_razaosocial
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}

	/**
	 * @param field_type $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}

	/**
	 * @return the $st_urlsite
	 */
	public function getSt_urlsite() {
		return $this->st_urlsite;
	}

	/**
	 * @param field_type $st_urlsite
	 */
	public function setSt_urlsite($st_urlsite) {
		$this->st_urlsite = $st_urlsite;
	}

	/**
	 * @return the $st_cnpj
	 */
	public function getSt_cnpj() {
		return $this->st_cnpj;
	}

	/**
	 * @param field_type $st_cnpj
	 */
	public function setSt_cnpj($st_cnpj) {
		$this->st_cnpj = $st_cnpj;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}

	/**
	 * @param field_type $st_complemento
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @return the $nu_numero
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}

	/**
	 * @param field_type $nu_numero
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @return the $st_nomemunicipio
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}

	/**
	 * @param field_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}

	/**
	 * @param field_type $st_cep
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}

	/**
	 * @return the $st_nomepais
	 */
	public function getSt_nomepais() {
		return $this->st_nomepais;
	}

	/**
	 * @param field_type $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais) {
		$this->st_nomepais = $st_nomepais;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro() {
		return $this->st_bairro;
	}

	/**
	 * @param field_type $st_bairro
	 */
	public function setSt_bairro($st_bairro) {
		$this->st_bairro = $st_bairro;
	}

	/**
	 * @return the $nu_ddi
	 */
	public function getNu_ddi() {
		return $this->nu_ddi;
	}

	/**
	 * @param field_type $nu_ddi
	 */
	public function setNu_ddi($nu_ddi) {
		$this->nu_ddi = $nu_ddi;
	}

	/**
	 * @return the $nu_ddd
	 */
	public function getNu_ddd() {
		return $this->nu_ddd;
	}

	/**
	 * @param field_type $nu_ddd
	 */
	public function setNu_ddd($nu_ddd) {
		$this->nu_ddd = $nu_ddd;
	}

	/**
	 * @return the $nu_telefone
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}

	/**
	 * @param field_type $nu_telefone
	 */
	public function setNu_telefone($nu_telefone) {
		$this->nu_telefone = $nu_telefone;
	}


}

?>
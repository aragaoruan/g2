<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwPerfilEntidadeFuncionalidadeTO extends Ead1_TO_Dinamico{

    public $id_entidade;
    public $id_perfil;
    public $id_perfilpai;
    public $id_funcionalidade;
    public $st_urlicone;
    public $st_nomeperfil;
    public $st_funcionalidade;
    public $bl_pesquisa;
    public $st_label;
    public $id_funcionalidadepai;
    public $bl_ativo;
    public $st_classeflex;
    public $st_tipofuncionalidade;
    public $id_situacao;
    public $id_sistema;
    public $nu_ordem;
    public $st_classeflexbotao;
    public $bl_obrigatorio;
    public $bl_visivel;
    public $bl_lista;
    public $st_ajuda;
    public $id_tipofuncionalidade;
    public $st_target;
    public $st_urlcaminho;
    public $st_urlcaminhoedicao;
    public $bl_funcionalidadevisivel;
    public $bl_relatorio;
	public $bl_delete;

    /**
	 * @return the $id_tipofuncionalidade
	 */
	public function getId_tipofuncionalidade() {
		return $this->id_tipofuncionalidade;
	}

	/**
	 * @param field_type $id_tipofuncionalidade
	 */
	public function setId_tipofuncionalidade($id_tipofuncionalidade) {
		$this->id_tipofuncionalidade = $id_tipofuncionalidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil(){ 
		return $this->id_perfil;
	}

	/**
	 * @return the $id_perfilpai
	 */
	public function getId_perfilpai(){ 
		return $this->id_perfilpai;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade(){ 
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $st_urlicone
	 */
	public function getSt_urlicone(){ 
		return $this->st_urlicone;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil(){ 
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $st_funcionalidade
	 */
	public function getSt_funcionalidade(){ 
		return $this->st_funcionalidade;
	}

	/**
	 * @return the $bl_pesquisa
	 */
	public function getBl_pesquisa(){ 
		return $this->bl_pesquisa;
	}

	/**
	 * @return the $st_label
	 */
	public function getSt_label(){ 
		return $this->st_label;
	}

	/**
	 * @return the $id_funcionalidadepai
	 */
	public function getId_funcionalidadepai(){ 
		return $this->id_funcionalidadepai;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex(){ 
		return $this->st_classeflex;
	}

	/**
	 * @return the $st_tipofuncionalidade
	 */
	public function getSt_tipofuncionalidade(){ 
		return $this->st_tipofuncionalidade;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema(){ 
		return $this->id_sistema;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem(){ 
		return $this->nu_ordem;
	}

	/**
	 * @return the $st_classeflexbotao
	 */
	public function getSt_classeflexbotao(){ 
		return $this->st_classeflexbotao;
	}

	/**
	 * @return the $bl_obrigatorio
	 */
	public function getBl_obrigatorio(){ 
		return $this->bl_obrigatorio;
	}

	/**
	 * @return the $bl_visivel
	 */
	public function getBl_visivel(){ 
		return $this->bl_visivel;
	}

	/**
	 * @return the $bl_lista
	 */
	public function getBl_lista(){ 
		return $this->bl_lista;
	}

	/**
	 * @return the $st_ajuda
	 */
	public function getSt_ajuda(){ 
		return $this->st_ajuda;
	}


	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_perfil
	 */
	public function setId_perfil($id_perfil){ 
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param field_type $id_perfilpai
	 */
	public function setId_perfilpai($id_perfilpai){ 
		$this->id_perfilpai = $id_perfilpai;
	}

	/**
	 * @param field_type $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade){ 
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param field_type $st_urlicone
	 */
	public function setSt_urlicone($st_urlicone){ 
		$this->st_urlicone = $st_urlicone;
	}

	/**
	 * @param field_type $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil){ 
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param field_type $st_funcionalidade
	 */
	public function setSt_funcionalidade($st_funcionalidade){ 
		$this->st_funcionalidade = $st_funcionalidade;
	}

	/**
	 * @param field_type $bl_pesquisa
	 */
	public function setBl_pesquisa($bl_pesquisa){ 
		$this->bl_pesquisa = $bl_pesquisa;
	}

	/**
	 * @param field_type $st_label
	 */
	public function setSt_label($st_label){ 
		$this->st_label = $st_label;
	}

	/**
	 * @param field_type $id_funcionalidadepai
	 */
	public function setId_funcionalidadepai($id_funcionalidadepai){ 
		$this->id_funcionalidadepai = $id_funcionalidadepai;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex){ 
		$this->st_classeflex = $st_classeflex;
	}

	/**
	 * @param field_type $st_tipofuncionalidade
	 */
	public function setSt_tipofuncionalidade($st_tipofuncionalidade){ 
		$this->st_tipofuncionalidade = $st_tipofuncionalidade;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema){ 
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param field_type $nu_ordem
	 */
	public function setNu_ordem($nu_ordem){ 
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param field_type $st_classeflexbotao
	 */
	public function setSt_classeflexbotao($st_classeflexbotao){ 
		$this->st_classeflexbotao = $st_classeflexbotao;
	}

	/**
	 * @param field_type $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio){ 
		$this->bl_obrigatorio = $bl_obrigatorio;
	}

	/**
	 * @param field_type $bl_visivel
	 */
	public function setBl_visivel($bl_visivel){ 
		$this->bl_visivel = $bl_visivel;
	}

	/**
	 * @param field_type $bl_lista
	 */
	public function setBl_lista($bl_lista){ 
		$this->bl_lista = $bl_lista;
	}

	/**
	 * @param field_type $st_ajuda
	 */
	public function setSt_ajuda($st_ajuda){ 
		$this->st_ajuda = $st_ajuda;
	}
	/**
	 * @return the $st_target
	 */
	public function getSt_target() {
		return $this->st_target;
	}

	/**
	 * @param field_type $st_target
	 */
	public function setSt_target($st_target) {
		$this->st_target = $st_target;
	}
        public function getId_situacao() {
            return $this->id_situacao;
        }

        public function getSt_urlcaminho() {
            return $this->st_urlcaminho;
        }

        public function getSt_urlcaminhoedicao() {
            return $this->st_urlcaminhoedicao;
        }

        public function getBl_funcionalidadevisivel() {
            return $this->bl_funcionalidadevisivel;
        }

        public function getBl_relatorio() {
            return $this->bl_relatorio;
        }

        public function setId_situacao($id_situacao) {
            $this->id_situacao = $id_situacao;
        }

        public function setSt_urlcaminho($st_urlcaminho) {
            $this->st_urlcaminho = $st_urlcaminho;
        }

        public function setSt_urlcaminhoedicao($st_urlcaminhoedicao) {
            $this->st_urlcaminhoedicao = $st_urlcaminhoedicao;
        }

        public function setBl_funcionalidadevisivel($bl_funcionalidadevisivel) {
            $this->bl_funcionalidadevisivel = $bl_funcionalidadevisivel;
        }

        public function setBl_relatorio($bl_relatorio) {
            $this->bl_relatorio = $bl_relatorio;
        }

		public function getBl_delete()
		{
			return $this->bl_delete;
		}

		public function setBl_delete($bl_delete)
		{
			$this->bl_delete = $bl_delete;
		}



}
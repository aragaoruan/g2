<?php
/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */

class DadosBiomedicosTO extends Ead1_TO_Dinamico{

	public $id_dadosbiomedicos;
	public $id_etnia;
	public $id_tiposanguineo;
    public $id_fatorrh;
	public $id_usuario;
	public $id_entidade;
	public $nu_glicemia;
	public $st_glicemia;
	public $st_alergias;
	public $st_deficiencias;
	public $st_necessidadesespeciais;

	/**
	 * @return the $id_dadosbiomedicos
	 */
	public function getId_dadosbiomedicos(){
		return $this->id_dadosbiomedicos;
	}

    /**
     * @return the $id_tiposanguineo
     */
    public function getId_tiposanguineo(){
        return $this->id_tiposanguineo;
    }

	/**
	 * @return the $id_etnia
	 */
	public function getId_etnia(){
		return $this->id_etnia;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_glicemia
	 */
	public function getNu_glicemia(){
		return $this->nu_glicemia;
	}

	/**
	 * @return the $st_glicemia
	 */
	public function getSt_glicemia(){
		return $this->st_glicemia;
	}

	/**
	 * @return the $st_alergias
	 */
	public function getSt_alergias(){
		return $this->st_alergias;
	}

	/**
	 * @return the $st_deficiencias
	 */
	public function getSt_deficiencias(){
		return $this->st_deficiencias;
	}

    /**
     * @return the $id_fatorrh
     */
    public function getId_fatorrh(){
        return $this->id_fatorrh;
    }

	/**
	 * @return the $st_necessidadesespeciais
	 */
	public function getSt_necessidadesespeciais(){
		return $this->st_necessidadesespeciais;
	}

	/**
	 * @param field_type $id_dadosbiomedicos
	 */
	public function setId_dadosbiomedicos($id_dadosbiomedicos){
		$this->id_dadosbiomedicos = $id_dadosbiomedicos;
	}

	/**
	 * @param field_type $id_etnia
	 */
	public function setId_etnia($id_etnia){
		$this->id_etnia = $id_etnia;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){
		$this->id_usuario = $id_usuario;
	}

    /**
     * @param field_type $id_tiposanguineo
     */
    public function setId_tiposanguineo($id_tiposanguineo){
        $this->id_tiposanguineo = $id_tiposanguineo;
    }

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $nu_glicemia
	 */
	public function setNu_glicemia($nu_glicemia){
		$this->nu_glicemia = $nu_glicemia;
	}

	/**
	 * @param field_type $st_glicemia
	 */
	public function setSt_glicemia($st_glicemia){
		$this->st_glicemia = $st_glicemia;
	}

	/**
	 * @param field_type $st_alergias
	 */
	public function setSt_alergias($st_alergias){
		$this->st_alergias = $st_alergias;
	}

	/**
	 * @param field_type $st_deficiencias
	 */
	public function setSt_deficiencias($st_deficiencias){
		$this->st_deficiencias = $st_deficiencias;
	}

    /**
     * @param field_type $id_fatorrh
     */
    public function setId_fatorrh($id_fatorrh){
        $this->id_fatorrh = $id_fatorrh;
    }

	/**
	 * @param field_type $st_necessidadesespeciais
	 */
	public function setSt_necessidadesespeciais($st_necessidadesespeciais){
		$this->st_necessidadesespeciais = $st_necessidadesespeciais;
	}

}
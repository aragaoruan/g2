<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 *
 */
class ArquivoRetornoLancamentoTO extends Ead1_TO_Dinamico {

	public $id_arquivoretornolancamento;
	public $id_arquivoretorno;
	public $id_lancamento;
	public $st_nossonumero;
	public $nu_valor;
	public $dt_ocorrencia;
	public $id_usuariocadastro;
	public $dt_cadastro;
	public $bl_quitado;
	public $nu_desconto;
	public $nu_juros;
    public $st_carteira;
    public $nu_tarifa;
    public $st_documento;
    public $nu_valornominal;
    public $dt_quitado;

    /**
     * @param mixed $st_documento
     */
    public function setSt_documento($st_documento)
    {
        $this->st_documento = $st_documento;
    }

    /**
     * @return mixed
     */
    public function getSt_documento()
    {
        return $this->st_documento;
    }

    /**
     * @param mixed $nu_tarifa
     */
    public function setNu_tarifa($nu_tarifa)
    {
        $this->nu_tarifa = $nu_tarifa;
    }

    /**
     * @return mixed
     */
    public function getNu_tarifa()
    {
        return $this->nu_tarifa;
    }

    /**
     * @param mixed $st_carteira
     */
    public function setSt_carteira($st_carteira)
    {
        $this->st_carteira = $st_carteira;
    }

    /**
     * @return mixed
     */
    public function getSt_carteira()
    {
        return $this->st_carteira;
    }



    /**
	 * @return the $id_arquivoretornolancamento
	 */
	public function getId_arquivoretornolancamento() {
		return $this->id_arquivoretornolancamento;
	}

	/**
	 * @return the $id_arquivoretorno
	 */
	public function getId_arquivoretorno() {
		return $this->id_arquivoretorno;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $st_nossonumero
	 */
	public function getSt_nossonumero() {
		return $this->st_nossonumero;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $dt_ocorrencia
	 */
	public function getDt_ocorrencia() {
		return $this->dt_ocorrencia;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_quitado
	 */
	public function getBl_quitado() {
		return $this->bl_quitado;
	}

	/**
	 * @param field_type $id_arquivoretornolancamento
	 */
	public function setId_arquivoretornolancamento($id_arquivoretornolancamento) {
		$this->id_arquivoretornolancamento = $id_arquivoretornolancamento;
	}

	/**
	 * @param field_type $id_arquivoretorno
	 */
	public function setId_arquivoretorno($id_arquivoretorno) {
		$this->id_arquivoretorno = $id_arquivoretorno;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $st_nossonumero
	 */
	public function setSt_nossonumero($st_nossonumero) {
		$this->st_nossonumero = $st_nossonumero;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $dt_ocorrencia
	 */
	public function setDt_ocorrencia($dt_ocorrencia) {
		$this->dt_ocorrencia = $dt_ocorrencia;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_quitado
	 */
	public function setBl_quitado($bl_quitado) {
		$this->bl_quitado = $bl_quitado;
	}

    /**
     * @param mixed $nu_desconto
     */
    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return mixed
     */
    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param mixed $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return mixed
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @param mixed $nu_valornominal
     */
    public function setNu_valornominal($nu_valornominal)
    {
        $this->nu_valornominal = $nu_valornominal;
    }

    /**
     * @return mixed
     */
    public function getNu_Valornominal()
    {
        return $this->nu_valornominal;
    }

    /**
     * @param mixed $dt_quitado
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @return mixed
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }


}

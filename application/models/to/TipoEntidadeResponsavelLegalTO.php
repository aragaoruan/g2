<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoEntidadeResponsavelLegalTO extends Ead1_TO_Dinamico{

	public $id_tipoentidaderesponsavel;
	public $st_tipoentidaderesponsavel;


	/**
	 * @return the $id_tipoentidaderesponsavel
	 */
	public function getId_tipoentidaderesponsavel(){ 
		return $this->id_tipoentidaderesponsavel;
	}

	/**
	 * @return the $st_tipoentidaderesponsavel
	 */
	public function getSt_tipoentidaderesponsavel(){ 
		return $this->st_tipoentidaderesponsavel;
	}


	/**
	 * @param field_type $id_tipoentidaderesponsavel
	 */
	public function setId_tipoentidaderesponsavel($id_tipoentidaderesponsavel){ 
		$this->id_tipoentidaderesponsavel = $id_tipoentidaderesponsavel;
	}

	/**
	 * @param field_type $st_tipoentidaderesponsavel
	 */
	public function setSt_tipoentidaderesponsavel($st_tipoentidaderesponsavel){ 
		$this->st_tipoentidaderesponsavel = $st_tipoentidaderesponsavel;
	}

}
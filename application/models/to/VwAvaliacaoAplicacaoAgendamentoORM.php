<?php
/**
 * Mapeamento da view vw_disciplinaareaconhecimentonivelserie
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoAplicacaoAgendamentoORM extends Ead1_ORM{
	
	public $_name = 'vw_avaliacaoaplicacaoagendamento';
	public $_primary = array('id_matricula', 'id_tipoprova');
}
<?php

/**
 * Classe que encapsula os dados da tb_pa_marcacaoetapa
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PAMarcacaoEtapaTO extends Ead1_TO_Dinamico {

    public $bl_aceitacaocontrato;
    public $bl_boasvindas;
    public $bl_dadoscadastrais;
    public $bl_dadoscontato;
    public $bl_mensageminformativa;
    public $dt_aceita;
    public $dt_checkin;
    public $id_pa_marcacaoetapa;
    public $id_venda;

    public function getBl_aceitacaocontrato() {
        return $this->bl_aceitacaocontrato;
    }

    public function setBl_aceitacaocontrato($bl_aceitacaocontrato) {
        $this->bl_aceitacaocontrato = $bl_aceitacaocontrato;
    }

    public function getBl_boasvindas() {
        return $this->bl_boasvindas;
    }

    public function setBl_boasvindas($bl_boasvindas) {
        $this->bl_boasvindas = $bl_boasvindas;
    }

    public function getBl_dadoscadastrais() {
        return $this->bl_dadoscadastrais;
    }

    public function setBl_dadoscadastrais($bl_dadoscadastrais) {
        $this->bl_dadoscadastrais = $bl_dadoscadastrais;
    }

    public function getBl_dadoscontato() {
        return $this->bl_dadoscontato;
    }

    public function setBl_dadoscontato($bl_dadoscontato) {
        $this->bl_dadoscontato = $bl_dadoscontato;
    }

    public function getBl_mensageminformativa() {
        return $this->bl_mensageminformativa;
    }

    public function setBl_mensageminformativa($bl_mensageminformativa) {
        $this->bl_mensageminformativa = $bl_mensageminformativa;
    }

    public function getDt_aceita() {
        return $this->dt_aceita;
    }

    public function setDt_aceita($dt_aceita) {
        $this->dt_aceita = $dt_aceita;
    }

    public function getDt_checkin() {
        return $this->dt_checkin;
    }

    public function setDt_checkin($dt_checkin) {
        $this->dt_checkin = $dt_checkin;
    }

    public function getId_pa_marcacaoetapa() {
        return $this->id_pa_marcacaoetapa;
    }

    public function setId_pa_marcacaoetapa($id_pa_marcacaoetapa) {
        $this->id_pa_marcacaoetapa = $id_pa_marcacaoetapa;
    }

    public function getId_venda() {
        return $this->id_venda;
    }

    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

}

?>
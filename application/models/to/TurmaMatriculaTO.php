<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TurmaMatriculaTO extends Ead1_TO_Dinamico{

	public $id_turmamatricula;
	public $id_usuariocadastro;
	public $id_turma;
	public $id_matricula;
	public $bl_ativo;
	public $dt_cadastro;
	/**
	 * @return the $id_turmamatricula
	 */
	public function getId_turmamatricula() {
		return $this->id_turmamatricula;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_turma
	 */
	public function getId_turma() {
		return $this->id_turma;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_turmamatricula
	 */
	public function setId_turmamatricula($id_turmamatricula) {
		$this->id_turmamatricula = $id_turmamatricula;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma) {
		$this->id_turma = $id_turma;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}


}
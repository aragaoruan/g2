<?php
/**
 * Classe para encapsular os dados da view de avaliacaoconjunto
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwAvaliacaoConjuntoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjunto;
	public $id_situacao;
	public $st_situacao;
	public $id_tipoprova;
	public $st_tipoprova;
	public $id_tipocalculoavaliacao;
	public $st_tipocalculoavaliacao;
	public $id_entidade;
	public $st_avaliacaoconjunto;
	
	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $st_tipoprova
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}

	/**
	 * @return the $id_tipocalculoavaliacao
	 */
	public function getId_tipocalculoavaliacao() {
		return $this->id_tipocalculoavaliacao;
	}

	/**
	 * @return the $st_tipocalculoavaliacao
	 */
	public function getSt_tipocalculoavaliacao() {
		return $this->st_tipocalculoavaliacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_avaliacaoconjunto
	 */
	public function getSt_avaliacaoconjunto() {
		return $this->st_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $st_tipoprova
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

	/**
	 * @param field_type $id_tipocalculoavaliacao
	 */
	public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $st_tipocalculoavaliacao
	 */
	public function setSt_tipocalculoavaliacao($st_tipocalculoavaliacao) {
		$this->st_tipocalculoavaliacao = $st_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_avaliacaoconjunto
	 */
	public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
		$this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
	}

}
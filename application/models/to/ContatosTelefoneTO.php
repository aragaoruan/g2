<?php
/**
 * Classe para encapsular os dados de contato para telefone.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosTelefoneTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id do telefone.
	 * @var int
	 */
	public $id_telefone;
	
	/**
	 * Atributo que contém o número do telefone.
	 * @var string
	 */
	public $nu_telefone;
	
	/**
	 * Atributo que contém o ddd do telefone.
	 * @var int
	 */
	public $nu_ddd;
	
	/**
	 * Atributo que contém o ddi do telefone.
	 * @var int
	 */
	public $nu_ddi;
	
	/**
	 * Atributo que contém o id do tipo de telefone.
	 * @var int
	 */
	public $id_tipotelefone;
	
	/**
	 * Contém a descrição opcional para o telefone.
	 * @var String
	 */
	public $st_descricao;
	/**
	 * @return String
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param String $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
	/**
	 * @return int
	 */
	public function getId_telefone() {
		return $this->id_telefone;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipotelefone() {
		return $this->id_tipotelefone;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ddd() {
		return $this->nu_ddd;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ddi() {
		return $this->nu_ddi;
	}
	
	/**
	 * @return string
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}
	
	/**
	 * @param int $id_telefone
	 */
	public function setId_telefone($id_telefone) {
		$this->id_telefone = $id_telefone;
	}
	
	/**
	 * @param int $id_tipotelefone
	 */
	public function setId_tipotelefone($id_tipotelefone) {
		$this->id_tipotelefone = $id_tipotelefone;
	}
	
	/**
	 * @param int $nu_ddd
	 */
	public function setNu_ddd($nu_ddd) {
		$this->nu_ddd = $nu_ddd;
	}
	
	/**
	 * @param int $nu_ddi
	 */
	public function setNu_ddi($nu_ddi) {
		$this->nu_ddi = $nu_ddi;
	}
	
	/**
	 * @param string $st_telefone
	 */
	public function setNu_telefone($nu_telefone) {
		$this->nu_telefone = $nu_telefone;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de local de aula
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class LocalAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id de local de aula.
	 * @var int
	 */
	public $id_localaula;
	
	/**
	 * Nome do local de aula.
	 * @var string
	 */
	public $st_localaula;
	
	/**
	 * Data de cadastro da sala de aula.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Id do usuário que cadastrou a sala de aula.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id da situação da sala de aula.
	 * @var int
	 */
	public $id_situacao;
	
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_localaula() {
		return $this->id_localaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return string
	 */
	public function getSt_localaula() {
		return $this->st_localaula;
	}

	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param int $id_localaula
	 */
	public function setId_localaula($id_localaula) {
		$this->id_localaula = $id_localaula;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param string $st_localaula
	 */
	public function setSt_localaula($st_localaula) {
		$this->st_localaula = $st_localaula;
	}



}

?>
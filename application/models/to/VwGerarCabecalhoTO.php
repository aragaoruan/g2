<?php
class VwGerarCabecalhoTO extends Ead1_TO_Dinamico{
	
	public $st_nomeentidade;
	public $st_razaosocial;
	public $st_urlimglogo;
	public $st_urlsite;
	public $nu_ddd;
	public $nu_telefone;
	public $id_endereco;
	public $id_pais;
	public $sg_uf;
	public $id_municipio;
	public $id_tipoendereco;
	public $st_cep;
	public $st_endereco;
	public $st_bairro;
	public $st_complemento;
	public $nu_numero;
	public $st_estadoprovincia;
	public $st_cidade;
	public $bl_ativo;
	public $id_entidade;
	
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_razaosocial
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}

	/**
	 * @return the $st_urlimglogo
	 */
	public function getSt_urlimglogo() {
		return $this->st_urlimglogo;
	}

	/**
	 * @return the $st_urlsite
	 */
	public function getSt_urlsite() {
		return $this->st_urlsite;
	}

	/**
	 * @return the $nu_ddd
	 */
	public function getNu_ddd() {
		return $this->nu_ddd;
	}

	/**
	 * @return the $st_telefone
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}

	/**
	 * @return the $id_endereco
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}

	/**
	 * @return the $id_pais
	 */
	public function getId_pais() {
		return $this->id_pais;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}

	/**
	 * @return the $id_tipoendereco
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro() {
		return $this->st_bairro;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}

	/**
	 * @return the $st_numero
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}

	/**
	 * @return the $st_estadoprovincia
	 */
	public function getSt_estadoprovincia() {
		return $this->st_estadoprovincia;
	}

	/**
	 * @return the $st_cidade
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}

	/**
	 * @param field_type $st_urlimglogo
	 */
	public function setSt_urlimglogo($st_urlimglogo) {
		$this->st_urlimglogo = $st_urlimglogo;
	}

	/**
	 * @param field_type $st_urlsite
	 */
	public function setSt_urlsite($st_urlsite) {
		$this->st_urlsite = $st_urlsite;
	}

	/**
	 * @param field_type $nu_ddd
	 */
	public function setNu_ddd($nu_ddd) {
		$this->nu_ddd = $nu_ddd;
	}

	/**
	 * @param field_type $st_telefone
	 */
	public function setNu_telefone($nu_telefone) {
		$this->nu_telefone = $nu_telefone;
	}

	/**
	 * @param field_type $id_endereco
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}

	/**
	 * @param field_type $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param field_type $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}

	/**
	 * @param field_type $st_cep
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_bairro
	 */
	public function setSt_bairro($st_bairro) {
		$this->st_bairro = $st_bairro;
	}

	/**
	 * @param field_type $st_complemento
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @param field_type $st_numero
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param field_type $st_estadoprovincia
	 */
	public function setSt_estadoprovincia($st_estadoprovincia) {
		$this->st_estadoprovincia = $st_estadoprovincia;
	}

	/**
	 * @param field_type $st_cidade
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

}
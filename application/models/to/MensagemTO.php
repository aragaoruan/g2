<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class MensagemTO extends Ead1_TO_Dinamico
{

    public $id_mensagem;

    /**
     * @var int
     */
    public $id_usuariocadastro;
    public $id_entidade;
    public $st_mensagem;
    public $st_texto;
    public $dt_cadastro;
    public $st_caminhoanexo;
    public $bl_importante;

    /**
     * @return the $id_mensagem
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $st_mensagem
     */
    public function getSt_mensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @return the $st_texto
     */
    public function getSt_texto()
    {
        return $this->st_texto;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param field_type $id_mensagem
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $st_mensagem
     */
    public function setSt_mensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @param field_type $st_texto
     */
    public function setSt_texto($st_texto)
    {
        $this->st_texto = $st_texto;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param mixed $st_caminhoanexo
     */
    public function setStCaminhoanexo($st_caminhoanexo)
    {
        $this->st_caminhoanexo = $st_caminhoanexo;
    }

    /**
     * @return mixed
     */
    public function getStCaminhoanexo()
    {
        return $this->st_caminhoanexo;
    }


    /**
     * @param bool $bl_importante
     */
    public function setBl_importante($bl_importante)
    {
        $this->bl_importante = $bl_importante;
    }

    /**
     * @return boll
     */
    public function getBl_importante()
    {
        return $this->bl_importante;
    }

}
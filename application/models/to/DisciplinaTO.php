<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2013-31-12
 * @package models
 * @subpackage to
 */
class DisciplinaTO extends Ead1_TO_Dinamico
{

    public $id_disciplina;
    public $st_disciplina;
    public $st_descricao;
    public $nu_identificador;
    public $nu_cargahoraria;
    public $nu_creditos;
    public $nu_codigoparceiro;
    public $bl_ativa;
    public $bl_compartilhargrupo;
    public $id_situacao;
    public $st_tituloexibicao;
    public $id_tipodisciplina;
    public $id_entidade;
    public $dt_cadastro;
    public $id_usuariocadastro;
    public $st_identificador;
    public $st_ementacertificado;
    public $bl_provamontada;
    public $bl_disciplinasemestre;
    public $nu_repeticao;
    public $sg_uf;
    public $id_grupodisciplina;
    public $bl_coeficienterendimento;
    public $bl_cargahorariaintegralizada;

    /**
     * @return integer $nu_repeticao
     */
    public function getNu_repeticao ()
    {
        return $this->nu_repeticao;
    }

    /**
     * @param integer $nu_repeticao
     */
    public function setNu_repeticao ($nu_repeticao)
    {
        $this->nu_repeticao = $nu_repeticao;
    }

    /**
     * @return string $sg_uf
     */
    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * @return the $st_disciplina
     */
    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    /**
     * @return the $st_descricao
     */
    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    /**
     * @return the $nu_identificador
     */
    public function getNu_identificador ()
    {
        return $this->nu_identificador;
    }

    /**
     * @return the $nu_cargahoraria
     */
    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @return the $nu_creditos
     */
    public function getNu_creditos ()
    {
        return $this->nu_creditos;
    }

    /**
     * @return the $nu_codigoparceiro
     */
    public function getNu_codigoparceiro ()
    {
        return $this->nu_codigoparceiro;
    }

    /**
     * @return the $bl_ativa
     */
    public function getBl_ativa ()
    {
        return $this->bl_ativa;
    }

    /**
     * @return the $bl_compartilhargrupo
     */
    public function getBl_compartilhargrupo ()
    {
        return $this->bl_compartilhargrupo;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $st_tituloexibicao
     */
    public function getSt_tituloexibicao ()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @return the $id_tipodisciplina
     */
    public function getId_tipodisciplina ()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $st_identificador
     */
    public function getSt_identificador ()
    {
        return $this->st_identificador;
    }

    /**
     * @return the $st_ementacertificado
     */
    public function getSt_ementacertificado ()
    {
        return $this->st_ementacertificado;
    }

    /**
     * @param field_type $id_disciplina
     */
    public function setId_disciplina ($value)
    {
        $this->id_disciplina = $value;
    }

    /**
     * @param field_type $st_disciplina
     */
    public function setSt_disciplina ($value)
    {
        $this->st_disciplina = $value;
    }

    /**
     * @param field_type $st_descricao
     */
    public function setSt_descricao ($value)
    {
        $this->st_descricao = $value;
    }

    /**
     * @param field_type $nu_identificador
     */
    public function setNu_identificador ($value)
    {
        $this->nu_identificador = $value;
    }

    /**
     * @param field_type $nu_cargahoraria
     */
    public function setNu_cargahoraria ($value)
    {
        $this->nu_cargahoraria = $value;
    }

    /**
     * @param field_type $nu_creditos
     */
    public function setNu_creditos ($value)
    {
        $this->nu_creditos = $value;
    }

    /**
     * @param field_type $nu_codigoparceiro
     */
    public function setNu_codigoparceiro ($value)
    {
        $this->nu_codigoparceiro = $value;
    }

    /**
     * @param field_type $bl_ativa
     */
    public function setBl_ativa ($value)
    {
        $this->bl_ativa = $value;
    }

    /**
     * @param field_type $bl_compartilhargrupo
     */
    public function setBl_compartilhargrupo ($value)
    {
        $this->bl_compartilhargrupo = $value;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao ($value)
    {
        $this->id_situacao = $value;
    }

    /**
     * @param field_type $st_tituloexibicao
     */
    public function setSt_tituloexibicao ($value)
    {
        $this->st_tituloexibicao = $value;
    }

    /**
     * @param field_type $id_tipodisciplina
     */
    public function setId_tipodisciplina ($value)
    {
        $this->id_tipodisciplina = $value;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade ($value)
    {
        $this->id_entidade = $value;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro ($value)
    {
        $this->dt_cadastro = $value;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro ($value)
    {
        $this->id_usuariocadastro = $value;
    }

    /**
     * @param field_type $st_identificador
     */
    public function setSt_identificador ($value)
    {
        $this->st_identificador = $value;
    }

    /**
     * @param field_type $st_ementacertificado
     */
    public function setSt_ementacertificado ($value)
    {
        $this->st_ementacertificado = $value;
    }

    /**
     * @param mixed $bl_provamontada
     */
    public function setBl_provamontada ($bl_provamontada)
    {
        $this->bl_provamontada = $bl_provamontada;
    }

    /**
     * @return mixed
     */
    public function getBl_provamontada ()
    {
        return $this->bl_provamontada;
    }

    /**
     * @param mixed $bl_disciplinasemestre
     */
    public function setBl_disciplinasemestre ($bl_disciplinasemestre)
    {
        $this->bl_disciplinasemestre = $bl_disciplinasemestre;
    }

    /**
     * @return mixed
     */
    public function getBl_disciplinasemestre ()
    {
        return $this->bl_disciplinasemestre;
    }

    /**
     * @return mixed
     */
    public function getid_grupodisciplina()
    {
        return $this->id_grupodisciplina;
    }

    /**
     * @param int $id_grupodisciplina
     * @return $this
     */
    public function setid_grupodisciplina($id_grupodisciplina)
    {
        $this->id_grupodisciplina = $id_grupodisciplina;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getId_coeficienterendimento()
    {
        return $this->id_coeficienterendimento;
    }

    /**
     * @param boolean $id_coeficienterendimento
     * @return $this;
     */
    public function setBl_coeficienterendimento($id_coeficienterendimento)
    {
        $this->id_coeficienterendimento = $id_coeficienterendimento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_cargahorariaintegralizada()
    {
        return $this->id_cargahorariaintegralizada;
    }

    /**
     * @param boolean $id_cargahorariaintegralizada
     * @return $this;
     */
    public function setBl_cargahorariaintegralizada($id_cargahorariaintegralizada)
    {
        $this->id_cargahorariaintegralizada = $id_cargahorariaintegralizada;
        return $this;
    }

}

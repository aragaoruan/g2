<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class AproveitamentoDisciplinaTO extends Ead1_TO_Dinamico{

	public $id_aproveitamentodisciplina;
	public $id_municipio;
	public $sg_uf;
	public $id_usuariocadastro;
	public $st_instituicao;
	public $dt_conclusao;
	public $st_curso;
	public $st_disciplina;
	public $dt_cadastro;
	public $st_cargahoraria;
	public $bl_ativo;


	/**
	 * @return the $id_aproveitamentodisciplina
	 */
	public function getId_aproveitamentodisciplina(){ 
		return $this->id_aproveitamentodisciplina;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio(){ 
		return $this->id_municipio;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf(){ 
		return $this->sg_uf;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_instituicao
	 */
	public function getSt_instituicao(){ 
		return $this->st_instituicao;
	}

	/**
	 * @return the $dt_conclusao
	 */
	public function getDt_conclusao(){ 
		return $this->dt_conclusao;
	}

	/**
	 * @return the $st_curso
	 */
	public function getSt_curso(){ 
		return $this->st_curso;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina(){ 
		return $this->st_disciplina;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_cargahoraria
	 */
	public function getSt_cargahoraria(){ 
		return $this->st_cargahoraria;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}


	/**
	 * @param field_type $id_aproveitamentodisciplina
	 */
	public function setId_aproveitamentodisciplina($id_aproveitamentodisciplina){ 
		$this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio){ 
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf){ 
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $st_instituicao
	 */
	public function setSt_instituicao($st_instituicao){ 
		$this->st_instituicao = $st_instituicao;
	}

	/**
	 * @param field_type $dt_conclusao
	 */
	public function setDt_conclusao($dt_conclusao){ 
		$this->dt_conclusao = $dt_conclusao;
	}

	/**
	 * @param field_type $st_curso
	 */
	public function setSt_curso($st_curso){ 
		$this->st_curso = $st_curso;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina){ 
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $st_cargahoraria
	 */
	public function setSt_cargahoraria($st_cargahoraria){ 
		$this->st_cargahoraria = $st_cargahoraria;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

}
<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class MensagemCobrancaTO extends Ead1_TO_Dinamico {

    public $id_mensagemcobranca;
    public $id_textosistema;
    public $id_entidade;
    public $nu_diasatraso;
    public $dt_cadastro;
    public $bl_ativo;
    public $bl_antecedencia;

    /**
     * @return the $id_mensagemcobranca
     */
    public function getId_mensagemcobranca() {
        return $this->id_mensagemcobranca;
    }

    /**
     * @return the $id_textosistema
     */
    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $nu_diasatraso
     */
    public function getNu_diasatraso() {
        return $this->nu_diasatraso;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $id_mensagemcobranca
     */
    public function setId_mensagemcobranca($id_mensagemcobranca) {
        $this->id_mensagemcobranca = $id_mensagemcobranca;
    }

    /**
     * @param field_type $id_textosistema
     */
    public function setId_textosistema($id_textosistema) {
        $this->id_textosistema = $id_textosistema;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $nu_diasatraso
     */
    public function setNu_diasatraso($nu_diasatraso) {
        $this->nu_diasatraso = $nu_diasatraso;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_antecedencia() {
        return $this->bl_antecedencia;
    }

    public function setBl_antecedencia($bl_antecedencia) {
        $this->bl_antecedencia = $bl_antecedencia;
    }

}

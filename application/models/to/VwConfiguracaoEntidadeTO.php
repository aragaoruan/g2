<?php
/**
 * Classe para encapsular os dados de Configuracao Entidade e Tipo Configuração Entidade
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwConfiguracaoEntidadeTO extends Ead1_TO_Dinamico {
	
	/**
	 * Id da Entidade.
	 * @var int
	 */
	public $id_entidade;
		
	/**
	 * Id da Configuracao.
	 * @var int
	 */
	public $id_tipoconfiguracao;
		
	/**
	 * Contem o Nome Padrão da Configuração.
	 * @var string
	 */
	public $st_padrao;
		
	/**
	 * Contem o Alias da Configuração.
	 * @var string
	 */
	public $st_valorconfiguracao;
		
	/**
	 * Contem a Categoria que Pertence a Configuração.
	 * @var string
	 */
	public $st_categoria;
		
	/**
	 * Contem a Descrição da Configuração.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_tipoconfiguracao
	 */
	public function getId_tipoconfiguracao() {
		return $this->id_tipoconfiguracao;
	}

	/**
	 * @return the $st_padrao
	 */
	public function getSt_padrao() {
		return $this->st_padrao;
	}

	/**
	 * @return the $st_valorconfiguracao
	 */
	public function getSt_valorconfiguracao() {
		return $this->st_valorconfiguracao;
	}

	/**
	 * @return the $st_categoria
	 */
	public function getSt_categoria() {
		return $this->st_categoria;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_tipoconfiguracao the $id_tipoconfiguracao to set
	 */
	public function setId_tipoconfiguracao($id_tipoconfiguracao) {
		$this->id_tipoconfiguracao = $id_tipoconfiguracao;
	}

	/**
	 * @param $st_padrao the $st_padrao to set
	 */
	public function setSt_padrao($st_padrao) {
		$this->st_padrao = $st_padrao;
	}

	/**
	 * @param $st_valorconfiguracao the $st_valorconfiguracao to set
	 */
	public function setSt_valorconfiguracao($st_valorconfiguracao) {
		$this->st_valorconfiguracao = $st_valorconfiguracao;
	}

	/**
	 * @param $st_categoria the $st_categoria to set
	 */
	public function setSt_categoria($st_categoria) {
		$this->st_categoria = $st_categoria;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
	
	
}
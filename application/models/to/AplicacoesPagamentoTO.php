<?php
/**
 * Classe para encapsular os dados de aplicações de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AplicacoesPagamentoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da aplicação de pagamento.
	 * @var int
	 */
	public $id_aplicacoespagamento;
	
	/**
	 * Nome da aplicação de pagamento.
	 * @var string
	 */
	public $st_aplicacoespagamento;
	
	/**
	 * Descrição da aplicação de pagamento.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_aplicacoespagamento() {
		return $this->id_aplicacoespagamento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_aplicacoespagamento() {
		return $this->st_aplicacoespagamento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param int $id_aplicacoespagamento
	 */
	public function setId_aplicacoespagamento($id_aplicacoespagamento) {
		$this->id_aplicacoespagamento = $id_aplicacoespagamento;
	}
	
	/**
	 * @param string $st_aplicacoespagamento
	 */
	public function setSt_aplicacoespagamento($st_aplicacoespagamento) {
		$this->st_aplicacoespagamento = $st_aplicacoespagamento;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

}

?>
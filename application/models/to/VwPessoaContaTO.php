<?php
/**
 * Classe para encapsular os dados de view de conta para pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaContaTO extends Ead1_TO_Dinamico {

	/**
	 * Chave primaria da tabela
	 * @var int
	 */
	public $id_contapessoa;

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do banco.
	 * @var int
	 */
	public $st_banco;
	
	/**
	 * Contém o nome do banco.
	 * @var string
	 */
	public $st_nomebanco;
	
	/**
	 * Contém o número da agência.
	 * @var string
	 */
	public $st_agencia;
	
	/**
	 * Contém o número da conta.
	 * @var string
	 */
	public $st_conta;
	
	/**
	 * Id do tipo de conta.
	 * @var int
	 */
	public $id_tipodeconta;
	
	/**
	 * Contém o Nome do Banco e o Tipo de Conta.
	 * @var string
	 */
	public $st_bancoconta;
	
	/**
	 * Contém o tipo de conta.
	 * @var string
	 */
	public $st_tipodeconta;
	
	/**
	 * Diz se a conta é padrão ou não.
	 * @var boolean
	 */
	public $bl_padrao;
	
	/**
	 * @return the $id_contapessoa
	 */
	public function getId_contapessoa() {
		return $this->id_contapessoa;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @return the $st_nomebanco
	 */
	public function getSt_nomebanco() {
		return $this->st_nomebanco;
	}

	/**
	 * @return the $st_agencia
	 */
	public function getSt_agencia() {
		return $this->st_agencia;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $id_tipodeconta
	 */
	public function getId_tipodeconta() {
		return $this->id_tipodeconta;
	}

	/**
	 * @return the $st_bancoconta
	 */
	public function getSt_bancoconta() {
		return $this->st_bancoconta;
	}

	/**
	 * @return the $st_tipodeconta
	 */
	public function getSt_tipodeconta() {
		return $this->st_tipodeconta;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @param int $id_contapessoa
	 */
	public function setId_contapessoa($id_contapessoa) {
		$this->id_contapessoa = $id_contapessoa;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	/**
	 * @param string $st_nomebanco
	 */
	public function setSt_nomebanco($st_nomebanco) {
		$this->st_nomebanco = $st_nomebanco;
	}

	/**
	 * @param string $st_agencia
	 */
	public function setSt_agencia($st_agencia) {
		$this->st_agencia = $st_agencia;
	}

	/**
	 * @param string $st_conta
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param int $id_tipodeconta
	 */
	public function setId_tipodeconta($id_tipodeconta) {
		$this->id_tipodeconta = $id_tipodeconta;
	}

	/**
	 * @param string $st_bancoconta
	 */
	public function setSt_bancoconta($st_bancoconta) {
		$this->st_bancoconta = $st_bancoconta;
	}

	/**
	 * @param string $st_tipodeconta
	 */
	public function setSt_tipodeconta($st_tipodeconta) {
		$this->st_tipodeconta = $st_tipodeconta;
	}

	/**
	 * @param boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	
}

?>
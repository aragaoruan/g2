<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class PesquisaAvaliacaoAplicacaoTO extends Ead1_TO_Dinamico{

	public $id_avaliacaoaplicacao;
        public $id_aplicadorprova;
	public $nu_maxaplicacao;
	public $id_horarioaula;
	public $id_endereco;
        public $dt_alteracaolimite;
	public $id_usuariocadastro;
	public $dt_aplicacao;
	public $bl_unica;
	public $dt_cadastro;
	public $bl_ativo;
	public $dt_antecedenciaminima;
	//Para Exibição da data no formato string

	//-----------------------------
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.CadastrarAplicacaoAvaliacao';
        
        public function getId_avaliacaoaplicacao() {
            return $this->id_avaliacaoaplicacao;
        }

        public function getId_aplicadorprova() {
            return $this->id_aplicadorprova;
        }

        public function getNu_maxaplicacao() {
            return $this->nu_maxaplicacao;
        }

        public function getId_horarioaula() {
            return $this->id_horarioaula;
        }

        public function getId_endereco() {
            return $this->id_endereco;
        }

        public function getDt_alteracaolimite() {
            return $this->dt_alteracaolimite;
        }

        public function getId_usuariocadastro() {
            return $this->id_usuariocadastro;
        }

        public function getDt_aplicacao() {
            return $this->dt_aplicacao;
        }

        public function getBl_unica() {
            return $this->bl_unica;
        }

        public function getDt_cadastro() {
            return $this->dt_cadastro;
        }

        public function getBl_ativo() {
            return $this->bl_ativo;
        }

        public function getDt_antecedenciaminima() {
            return $this->dt_antecedenciaminima;
        }

        public function getSt_classeflex() {
            return $this->st_classeflex;
        }

        public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
            $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        }

        public function setId_aplicadorprova($id_aplicadorprova) {
            $this->id_aplicadorprova = $id_aplicadorprova;
        }

        public function setNu_maxaplicacao($nu_maxaplicacao) {
            $this->nu_maxaplicacao = $nu_maxaplicacao;
        }

        public function setId_horarioaula($id_horarioaula) {
            $this->id_horarioaula = $id_horarioaula;
        }

        public function setId_endereco($id_endereco) {
            $this->id_endereco = $id_endereco;
        }

        public function setDt_alteracaolimite($dt_alteracaolimite) {
            $this->dt_alteracaolimite = $dt_alteracaolimite;
        }

        public function setId_usuariocadastro($id_usuariocadastro) {
            $this->id_usuariocadastro = $id_usuariocadastro;
        }

        public function setDt_aplicacao($dt_aplicacao) {
            $this->dt_aplicacao = $dt_aplicacao;
        }

        public function setBl_unica($bl_unica) {
            $this->bl_unica = $bl_unica;
        }

        public function setDt_cadastro($dt_cadastro) {
            $this->dt_cadastro = $dt_cadastro;
        }

        public function setBl_ativo($bl_ativo) {
            $this->bl_ativo = $bl_ativo;
        }

        public function setDt_antecedenciaminima($dt_antecedenciaminima) {
            $this->dt_antecedenciaminima = $dt_antecedenciaminima;
        }

        public function setSt_classeflex(String $st_classeflex) {
            $this->st_classeflex = $st_classeflex;
        }


}
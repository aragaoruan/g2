<?php

/**
 * Classe para encapsular os dados de Tramite.
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage to
 */
class TramiteTO extends Ead1_TO_Dinamico
{

    /**
     * @var integer
     */
    public $id_tramite;

    /**
     * @var integer
     */
    public $id_tipotramite;

    /**
     * @var integer
     */
    public $id_usuario;

    /**
     * @var integer
     */
    public $id_entidade;

    /**
     * @var string
     */
    public $st_tramite;

    /**
     * @var string
     */
    public $st_url;

    /**
     * @var mixed
     */
    public $dt_cadastro;

    /**
     * @var integer
     */
    public $id_upload;

    /**
     * @var boolean
     */
    public $bl_visivel;

    /**
     * @return string
     */
    public function getSt_url()
    {
        return $this->st_url;
    }

    /**
     * @param string $st_url
     */
    public function setSt_url($st_url)
    {
        $this->st_url = $st_url;
        return $this;

    }

    /**
     * @return int $id_tramite
     */
    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    /**
     * @return int $id_tipotramite
     */
    public function getId_tipotramite()
    {
        return $this->id_tipotramite;
    }

    /**
     * @return int $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string $st_tramite
     */
    public function getSt_tramite()
    {
        return $this->st_tramite;
    }

    /**
     * @return string $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_tramite
     */
    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
    }

    /**
     * @param int $id_tipotramite
     */
    public function setId_tipotramite($id_tipotramite)
    {
        $this->id_tipotramite = $id_tipotramite;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param string $st_tramite
     */
    public function setSt_tramite($st_tramite)
    {
        $this->st_tramite = $st_tramite;
    }

    /**
     * @param string $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }


    /**
     * @return bool $bl_visivel
     */
    public function getBl_visivel()
    {
        return $this->bl_visivel;
    }

    /**
     * @param bool $bl_visivel
     */
    public function setBl_visivel($bl_visivel)
    {
        $this->bl_visivel = $bl_visivel;
    }

    /**
     * @return int $id_upload
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param int $id_upload
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
    }


}

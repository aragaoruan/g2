<?php

/**
 * Classe para encapsular dados da view
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwVendaLancamentoTO extends Ead1_TO_Dinamico
{

    public $id_venda;
    public $id_evolucao;
    public $id_formapagamento;
    public $nu_parcelas;
    public $nu_valorliquido;
    public $bl_entrada;
    public $nu_ordem;
    public $id_lancamento;
    public $nu_valor;
    public $nu_vencimento;
    public $id_tipolancamento;
    public $dt_cadastro;
    public $id_meiopagamento;
    public $id_usuariolancamento;
    public $id_entidadelancamento;
    public $id_entidade;
    public $id_usuariocadastro;
    public $st_banco;
    public $id_cartaoconfig;
    public $id_boletoconfig;
    public $bl_quitado;
    public $dt_vencimento;
    public $dt_quitado;
    public $dt_emissao;
    public $dt_prevquitado;
    public $st_emissor;
    public $st_coddocumento;
    public $st_meiopagamento;
    public $st_formapagamento;
    public $nu_multa;
    public $nu_juros;
    public $nu_desconto;
    public $nu_quitado;
    public $nu_valoratualizado;
    public $nu_cartao;

    public $nu_jurosatraso;
    public $nu_multaatraso;
    public $nu_diavencimento;

    public $st_nomecompleto;
    public $bl_ativo;
    public $id_campanhacomercial;
    public $bl_original;

    public $id_vendaaditivo;
    public $nu_cobranca;
    public $nu_acordos;

    /**
     * @return mixed
     */
    public function getNu_acordos()
    {
        return $this->nu_acordos;
    }

    /**
     * @param mixed $nu_acordos
     */
    public function setNu_acordos($nu_acordos)
    {
        $this->nu_acordos = $nu_acordos;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getNu_cobranca()
    {
        return $this->nu_cobranca;
    }

    /**
     * @param mixed $nu_cobranca
     */
    public function setNu_cobranca($nu_cobranca)
    {
        $this->nu_cobranca = $nu_cobranca;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param mixed $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }


    /**
     * @var String
     */
    public $recorrente_orderid;

    /**
     * @return mixed
     */
    public function getnu_cartao()
    {
        return $this->nu_cartao;
    }

    /**
     * @param mixed $nu_cartao
     */
    public function setnu_cartao($nu_cartao)
    {
        $this->nu_cartao = $nu_cartao;
        return $this;

    }

    /**
     * @return the $id_campanhacomercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param field_type $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @return the $nu_jurosatraso
     */
    public function getNu_jurosatraso()
    {
        return $this->nu_jurosatraso;
    }

    /**
     * @return the $nu_multaatraso
     */
    public function getNu_multaatraso()
    {
        return $this->nu_multaatraso;
    }

    /**
     * @param field_type $nu_jurosatraso
     */
    public function setNu_jurosatraso($nu_jurosatraso)
    {
        $this->nu_jurosatraso = $nu_jurosatraso;
    }

    /**
     * @param field_type $nu_multaatraso
     */
    public function setNu_multaatraso($nu_multaatraso)
    {
        $this->nu_multaatraso = $nu_multaatraso;
    }

    /**
     * @return the $id_venda
     */
    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @return the $id_evolucao
     */
    public function getNu_quitado()
    {
        return $this->nu_quitado;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return the $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return the $id_formapagamento
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @return the $nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @return the $nu_valorliquido
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @return the $bl_entrada
     */
    public function getBl_entrada()
    {
        return $this->bl_entrada;
    }

    /**
     * @return the $nu_ordem
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @return the $nu_vencimento
     */
    public function getNu_vencimento()
    {
        return $this->nu_vencimento;
    }

    /**
     * @return the $id_tipolancamento
     */
    public function getId_tipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @return the $id_usuariolancamento
     */
    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @return the $id_entidadelancamento
     */
    public function getId_entidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $st_banco
     */
    public function getSt_banco()
    {
        return $this->st_banco;
    }

    /**
     * @return the $id_cartaoconfig
     */
    public function getId_cartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @return the $id_boletoconfig
     */
    public function getId_boletoconfig()
    {
        return $this->id_boletoconfig;
    }

    /**
     * @return the $bl_quitado
     */
    public function getBl_quitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @return the $dt_vencimento
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @return the $dt_quitado
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @return the $dt_emissao
     */
    public function getDt_emissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @return the $dt_prevquitado
     */
    public function getDt_prevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @return the $st_emissor
     */
    public function getSt_emissor()
    {
        return $this->st_emissor;
    }

    /**
     * @return the $st_coddocumento
     */
    public function getSt_coddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @return the $st_meiopagamento
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @return the $st_formapagamento
     */
    public function getSt_formapagamento()
    {
        return $this->st_formapagamento;
    }

    /**
     * @return the $nu_multa
     */
    public function getNu_multa()
    {
        return $this->nu_multa;
    }

    /**
     * @return the $nu_juros
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @return the $nu_diavencimento
     */
    public function getNu_diavencimento()
    {
        return $this->nu_diavencimento;
    }


    /**
     * @param field_type $nu_desconto
     */
    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @param field_type $nu_quitado
     */
    public function setNu_quitado($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
    }

    /**
     * @param field_type $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @param field_type $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param field_type $id_formapagamento
     */
    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    /**
     * @param field_type $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @param field_type $nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @param field_type $bl_entrada
     */
    public function setBl_entrada($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
    }

    /**
     * @param field_type $nu_ordem
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    /**
     * @param field_type $id_lancamento
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param field_type $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param field_type $nu_vencimento
     */
    public function setNu_vencimento($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @param field_type $id_tipolancamento
     */
    public function setId_tipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param field_type $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param field_type $id_usuariolancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param field_type $st_banco
     */
    public function setSt_banco($st_banco)
    {
        $this->st_banco = $st_banco;
    }

    /**
     * @param field_type $id_cartaoconfig
     */
    public function setId_cartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
    }

    /**
     * @param field_type $id_boletoconfig
     */
    public function setId_boletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
    }

    /**
     * @param field_type $bl_quitado
     */
    public function setBl_quitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param field_type $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param field_type $dt_quitado
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param field_type $dt_emissao
     */
    public function setDt_emissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @param field_type $dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param field_type $st_emissor
     */
    public function setSt_emissor($st_emissor)
    {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @param field_type $st_coddocumento
     */
    public function setSt_coddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @param field_type $st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @param field_type $st_formapagamento
     */
    public function setSt_formapagamento($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
    }

    /**
     * @param field_type $nu_multa
     */
    public function setNu_multa($nu_multa)
    {
        $this->nu_multa = $nu_multa;
    }

    /**
     * @param field_type $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @param field_type $nu_diavencimento
     */
    public function setNu_diavencimento($nu_diavencimento)
    {
        $this->nu_diavencimento = $nu_diavencimento;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param field_type $id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return the $nu_valoratualizado
     */
    public function getNu_valoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    /*_c
     * @param field_type $nu_valoratualizado
     */
    public function setNu_valoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param mixed $recorrente_orderid
     */
    public function setRecorrenteOrderid($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
    }

    /**
     * @return mixed
     */
    public function getRecorrenteOrderid()
    {
        return $this->recorrente_orderid;
    }

    /**
     * @return mixed
     */
    public function getBl_original()
    {
        return $this->bl_original;
    }

    /**
     * @param mixed $bl_original
     */
    public function setBl_original($bl_original)
    {
        $this->bl_original = $bl_original;
    }


}

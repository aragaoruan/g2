<?php
/**
 * Classe para encapsular os dados de endereço.
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @package models
 * @subpackage to
 */
class PesquisaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que diz qual o tipo de to que vai enviar
	 * @var String
	 */
	public $to_envia;
	
	/**
	 * Atributo que diz qual o tipo de to que vai retornar
	 * @var String
	 */
	public $to_retorno;

	/**
	 * Atributo que diz qual o valor do filtro da pesquisa
	 * @var Array
	 */
	public $filtros = array();
	
	/**
	 * Atributo que diz qual o RO
	 * @var String
	 */
	public $ro;
	
	/**
	 * Atributo que diz qual o Método da RO
	 * @var String
	 */
	public $metodo;
	
	/**
	 * Atributo que qual o tipo da pesquisa
	 * @var String
	 */
	public $tipo;
	
	/**
	 * Atributo que contém os arrays com os campos da Grid
	 * @var Array
	 */
	public $camposGrid;
	
	/**
	 * Contém o id_funcionalidade da pesquisa atual
	 * @var int
	 */
	public $id_funcionalidade;

	
	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @param $id_funcionalidade the $id_funcionalidade to set
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @return the $tipo
	 */
	public function getTipo() {
		return $this->tipo;
	}

	/**
	 * @param $tipo the $tipo to set
	 */
	public function setTipo($tipo) {
		$this->tipo = $tipo;
	}

	
	/**
	 * @return the $ro
	 */
	public function getRo() {
		return $this->ro;
	}

	/**
	 * @return the $metodo
	 */
	public function getMetodo() {
		return $this->metodo;
	}

	/**
	 * @param $ro the $ro to set
	 */
	public function setRo($ro) {
		$this->ro = $ro;
	}

	/**
	 * @param $metodo the $metodo to set
	 */
	public function setMetodo($metodo) {
		$this->metodo = $metodo;
	}

	public function getTo_envia() {
		return $this->to_envia;
	}

	/**
	 * @return the $to_retorno
	 */
	public function getTo_retorno() {
		return $this->to_retorno;
	}

	/**
	 * @return the $filtros
	 */
	public function getFiltros() {
		return $this->filtros;
	}

	/**
	 * @param $to_envia the $to_envia to set
	 */
	public function setTo_envia($to_envia) {
		$this->to_envia = $to_envia;
	}

	/**
	 * @param $to_retorno the $to_retorno to set
	 */
	public function setTo_retorno($to_retorno) {
		$this->to_retorno = $to_retorno;
	}

	/**
	 * @param $filtros the $filtros to set
	 */
	public function setFiltros($filtros) {
		$this->filtros = $filtros;
	}
	/**
	 * @return the $camposGrid
	 */
	public function getCamposGrid() {
		return $this->camposGrid;
	}

	/**
	 * @param $camposGrid the $camposGrid to set
	 */
	public function setCamposGrid($camposGrid) {
		$this->camposGrid = $camposGrid;
	}
}


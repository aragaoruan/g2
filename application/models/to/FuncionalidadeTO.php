<?php
/**
 * Classe para encapsular os dados de funcionalidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FuncionalidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da funcionalidade.
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Contém a funcionalidade.
	 * @var string
	 */
	public $st_funcionalidade;
	
	/**
	 * Contém o id da funcionalidade pai.
	 * @var int
	 */
	public $id_funcionalidadepai;
	
	/**
	 * Diz se a funcionalidade é ativa ou não.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém o id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Atributo que define a classe do factory no flex
	 * @var String
	 */
	public $st_classeflex;
	
	/**
	 * Atributo que contém a url para o ícone a ser apresentado nos menus.
	 * @var String
	 */
	public $st_urlicone;
	
	/**
	 * Id do tipo de funcionalidade.
	 * @var int
	 */
	public $id_tipofuncionalidade;
	
	/**
	 * Número da ordem da funcionalidade.
	 * @var int
	 */
	public $nu_ordem;
	
	/**
	 * @return int
	 */
	public function getId_tipofuncionalidade() {
		return $this->id_tipofuncionalidade;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}
	
	/**
	 * @param int $id_tipofuncionalidade
	 */
	public function setId_tipofuncionalidade($id_tipofuncionalidade) {
		$this->id_tipofuncionalidade = $id_tipofuncionalidade;
	}
	
	/**
	 * @param int $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	
	/**
	 * @return String
	 */
	public function getSt_urlicone() {
		return $this->st_urlicone;
	}
	
	/**
	 * @param String $st_urlicone
	 */
	public function setSt_urlicone($st_urlicone) {
		$this->st_urlicone = $st_urlicone;
	}
	/**
	 * @return String
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}
	
	/**
	 * @param String $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_funcionalidadepai() {
		return $this->id_funcionalidadepai;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_funcionalidade() {
		return $this->st_funcionalidade;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}
	
	/**
	 * @param int $id_funcionalidadepai
	 */
	public function setId_funcionalidadepai($id_funcionalidadepai) {
		$this->id_funcionalidadepai = $id_funcionalidadepai;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param string $st_funcionalidade
	 */
	public function setSt_funcionalidade($st_funcionalidade) {
		$this->st_funcionalidade = $st_funcionalidade;
	}

}

?>
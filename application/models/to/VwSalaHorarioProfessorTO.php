<?php
/**
 * Classe para encapsular os dados da view de horario aula, local e professor
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @package models
 * @subpackage to
 */
class VwSalaHorarioProfessorTO extends Ead1_TO_Dinamico {
	
	public $id_saladeaula;
	public $id_horarioaula;
	public $id_localaula;
	public $id_usuario;
	public $hr_inicio;
	public $hr_fim;
	public $st_horarioaula;
	
	
	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $id_localaula
	 */
	public function getId_localaula() {
		return $this->id_localaula;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $hr_inicio
	 */
	public function getHr_inicio() {
		return $this->hr_inicio;
	}

	/**
	 * @return the $hr_fim
	 */
	public function getHr_fim() {
		return $this->hr_fim;
	}

	/**
	 * @return the $st_horarioaula
	 */
	public function getSt_horarioaula() {
		return $this->st_horarioaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param field_type $id_localaula
	 */
	public function setId_localaula($id_localaula) {
		$this->id_localaula = $id_localaula;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $hr_inicio
	 */
	public function setHr_inicio($hr_inicio) {
		$this->hr_inicio = $hr_inicio;
	}

	/**
	 * @param field_type $hr_fim
	 */
	public function setHr_fim($hr_fim) {
		$this->hr_fim = $hr_fim;
	}

	/**
	 * @param field_type $st_horarioaula
	 */
	public function setSt_horarioaula($st_horarioaula) {
		$this->st_horarioaula = $st_horarioaula;
	}


	
	

}
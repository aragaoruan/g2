<?php
/**
 * 
 * Encapsula os dados da view de forma pagamento
 * @author Dimas Sulz
 *
 */
class VwDivisaoFormaMeioPagamentoTO extends Ead1_TO_Dinamico
{
	public $id_formapagamento;
    public $id_tipodivisaofinanceira;
    public $id_meiopagamento;
    public $st_descricao;
    public $st_meiopagamento;
    
	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @return the $id_tipodivisaofinanceira
	 */
	public function getId_tipodivisaofinanceira() {
		return $this->id_tipodivisaofinanceira;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_meiopagamento
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $id_tipodivisaofinanceira
	 */
	public function setId_tipodivisaofinanceira($id_tipodivisaofinanceira) {
		$this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}

}
?>
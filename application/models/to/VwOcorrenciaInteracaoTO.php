<?php
/**
 * Classe para encapsular a view vw_ocorrenciainteracao
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage to
 */
class VwOcorrenciaInteracaoTO extends Ead1_TO_Dinamico {
		
		public $id_tramite;
		public $id_ocorrencia;
		public $bl_visivel;
		public $id_usuario;
		public $dt_cadastro;
		public $st_tramite;
		public $id_entidade;
		public $st_upload;
		public $id_upload;
		public $st_nomecompleto; 
		
		/**
	 * @return the $id_tramite
	 */
	public function getId_tramite() {
		return $this->id_tramite;
	}

		/**
	 * @param field_type $id_tramite
	 */
	public function setId_tramite($id_tramite) {
		$this->id_tramite = $id_tramite;
	}

		/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

		/**
	 * @param field_type $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}

		/**
	 * @return the $bl_visivel
	 */
	public function getBl_visivel() {
		return $this->bl_visivel;
	}

		/**
	 * @param field_type $bl_visivel
	 */
	public function setBl_visivel($bl_visivel) {
		$this->bl_visivel = $bl_visivel;
	}

		/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

		/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

		/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

		/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

		/**
	 * @return the $st_tramite
	 */
	public function getSt_tramite() {
		return $this->st_tramite;
	}

		/**
	 * @param field_type $st_tramite
	 */
	public function setSt_tramite($st_tramite) {
		$this->st_tramite = $st_tramite;
	}

		/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

		/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

		/**
	 * @return the $st_upload
	 */
	public function getSt_upload() {
		return $this->st_upload;
	}

		/**
	 * @param field_type $st_upload
	 */
	public function setSt_upload($st_upload) {
		$this->st_upload = $st_upload;
	}

		/**
	 * @return the $id_upload
	 */
	public function getId_upload() {
		return $this->id_upload;
	}

		/**
	 * @param field_type $id_upload
	 */
	public function setId_upload($id_upload) {
		$this->id_upload = $id_upload;
	}
	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}


		
		
		
}

?>
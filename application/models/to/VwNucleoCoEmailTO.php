<?php

/**
 * @author DeniseXavier denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwNucleoCoEmailTO extends Ead1_TO_Dinamico {
	
	public $adicionar;
	public $id_nucleoco;
	public $id_emailconfig;
	public $st_titulo;
	public $st_usuario;
	public $id_entidade;
	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @return the $st_titulo
	 */
	public function getSt_titulo() {
		return $this->st_titulo;
	}

	/**
	 * @param field_type $st_titulo
	 */
	public function setSt_titulo($st_titulo) {
		$this->st_titulo = $st_titulo;
	}

	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario() {
		return $this->st_usuario;
	}

	/**
	 * @param field_type $st_usuario
	 */
	public function setSt_usuario($st_usuario) {
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return the $adicionar
	 */
	public function getAdicionar() {
		return $this->adicionar;
	}

	/**
	 * @param field_type $adicionar
	 */
	public function setAdicionar($adicionar) {
		$this->adicionar = $adicionar;
	}


	
	
}

?>
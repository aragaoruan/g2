<?php

/**
 * Classe para encapsular os dados de OcorrenciaResponsavel.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class OcorrenciaResponsavelTO extends Ead1_TO_Dinamico
{

    /**
     * @var integer
     */
    public $id_ocorrenciaresponsavel;
    /**
     * @var integer
     */
    public $id_ocorrencia;

    /**
     * @var integer
     */
    public $id_usuario;
    /**
     * @var integer
     */
    public $dt_cadastro;
    /**
     * @var integer
     */
    public $bl_ativo;

    /**
     * @return int
     */
    public function getid_ocorrenciaresponsavel()
    {
        return $this->id_ocorrenciaresponsavel;
    }

    /**
     * @param int $id_ocorrenciaresponsavel
     * @return OcorrenciaResponsavelTO
     */
    public function setid_ocorrenciaresponsavel($id_ocorrenciaresponsavel)
    {
        $this->id_ocorrenciaresponsavel = $id_ocorrenciaresponsavel;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param int $id_ocorrencia
     * @return OcorrenciaResponsavelTO
     */
    public function setid_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return OcorrenciaResponsavelTO
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $dt_cadastro
     * @return OcorrenciaResponsavelTO
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $bl_ativo
     * @return OcorrenciaResponsavelTO
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


}

?>
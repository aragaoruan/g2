<?php

/**
 * Classe para encapsular os dados da vw_alunosencerradosmoodle
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwAlunosEncerradosMoodleTO extends Ead1_TO_Dinamico
{

    public $id_entidade;
    public $id_alocacao;
    public $id_usuario;
    public $id_matricula;
    public $st_codsistemacurso;
    public $nu_perfilalunoencerrado;
    public $id_saladeaula;
    public $id_entidadeintegracao;

    /**
     * @return mixed
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param mixed $id_entidadeintegracao
     * @return VwAlunosEncerradosMoodleTO
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return the $id_alocacao
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param field_type $id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return the $st_codsistemacurso
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param field_type $st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }

    /**
     * @return the $nu_perfilalunoencerrado
     */
    public function getNu_perfilalunoencerrado()
    {
        return $this->nu_perfilalunoencerrado;
    }

    /**
     * @param field_type $nu_perfilalunoencerrado
     */
    public function setNu_perfilalunoencerrado($nu_perfilalunoencerrado)
    {
        $this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
    }

    /**
     * @return the $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param field_type $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }


}

?>

<?php

/**
 * Classe para encapsular os dados da tabela de integração das entidade
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class EntidadeIntegracaoTO extends Ead1_TO_Dinamico
{

    public $id_entidadeintegracao;
    public $id_entidade;
    public $id_usuariocadastro;
    public $id_sistema;
    public $st_codsistema;
    public $dt_cadastro;
    public $st_codchave;
    public $st_codarea;
    public $st_caminho;
    public $nu_perfilsuporte;
    public $nu_perfilprojeto;
    public $nu_perfildisciplina;
    public $nu_perfilobservador;
    public $nu_perfilalunoobs;
    public $nu_perfilalunoencerrado;
    public $nu_contexto;
    public $st_linkedserver;
    public $bl_criarsala;
    public $st_codchavewsuny;
    public $bl_ativo;
    public $id_situacao;
    public $st_titulo;

    /**
     * @return mixed
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     * @return EntidadeIntegracaoTO
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param mixed $id_situacao
     * @return EntidadeIntegracaoTO
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_titulo()
    {
        return $this->st_titulo;
    }

    /**
     * @param mixed $st_titulo
     * @return EntidadeIntegracaoTO
     */
    public function setst_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
        return $this;
    }

    /**
     * @return the $st_caminho
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param field_type $st_caminho
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
    }

    /**
     * @return the $id_entidadeintegracao
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param $id_entidadeintegracao the $id_entidadeintegracao to set
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_entidade the $id_entidade to set
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return the $id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return the $st_codsistema
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return the $st_codchave
     */
    public function getSt_codchave()
    {
        return $this->st_codchave;
    }

    /**
     * @param $st_codchave the $st_codchave to set
     */
    public function setSt_codchave($st_codchave)
    {
        $this->st_codchave = $st_codchave;
    }

    /**
     * @return the $nu_perfilsuporte
     */
    public function getNu_perfilsuporte()
    {
        return $this->nu_perfilsuporte;
    }

    /**
     * @param field_type $nu_perfilsuporte
     */
    public function setNu_perfilsuporte($nu_perfilsuporte)
    {
        $this->nu_perfilsuporte = $nu_perfilsuporte;
    }

    /**
     * @return the $nu_perfilprojeto
     */
    public function getNu_perfilprojeto()
    {
        return $this->nu_perfilprojeto;
    }

    /**
     * @param field_type $nu_perfilprojeto
     */
    public function setNu_perfilprojeto($nu_perfilprojeto)
    {
        $this->nu_perfilprojeto = $nu_perfilprojeto;
    }

    /**
     * @return the $nu_perfildisciplina
     */
    public function getNu_perfildisciplina()
    {
        return $this->nu_perfildisciplina;
    }

    /**
     * @param field_type $nu_perfildisciplina
     */
    public function setNu_perfildisciplina($nu_perfildisciplina)
    {
        $this->nu_perfildisciplina = $nu_perfildisciplina;
    }

    /**
     * @return the $nu_perfilobservador
     */
    public function getNu_perfilobservador()
    {
        return $this->nu_perfilobservador;
    }

    /**
     * @param field_type $nu_perfilobservador
     */
    public function setNu_perfilobservador($nu_perfilobservador)
    {
        $this->nu_perfilobservador = $nu_perfilobservador;
    }

    /**
     * @return the $nu_perfilalunoobs
     */
    public function getNu_perfilalunoobs()
    {
        return $this->nu_perfilalunoobs;
    }

    /**
     * @param field_type $nu_perfilalunoobs
     */
    public function setNu_perfilalunoobs($nu_perfilalunoobs)
    {
        $this->nu_perfilalunoobs = $nu_perfilalunoobs;
    }

    /**
     * @return the $nu_contexto
     */
    public function getNu_contexto()
    {
        return $this->nu_contexto;
    }

    /**
     * @param field_type $nu_contexto
     */
    public function setNu_contexto($nu_contexto)
    {
        $this->nu_contexto = $nu_contexto;
    }

    /**
     * @return the $st_codarea
     */
    public function getSt_codarea()
    {
        return $this->st_codarea;
    }

    /**
     * @param $st_codarea the $st_codarea to set
     */
    public function setSt_codarea($st_codarea)
    {
        $this->st_codarea = $st_codarea;
    }

    /**
     * @return the $nu_perfilalunoencerrado
     */
    public function getNu_perfilalunoencerrado()
    {
        return $this->nu_perfilalunoencerrado;
    }

    /**
     * @param field_type $nu_perfilalunoencerrado
     */
    public function setNu_perfilalunoencerrado($nu_perfilalunoencerrado)
    {
        $this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
    }

    public function getSt_linkedserver()
    {
        return $this->st_linkedserver;
    }

    public function setSt_linkedserver($st_linkedserver)
    {
        $this->st_linkedserver = $st_linkedserver;
    }

    public function getSt_codchavewsuny()
    {
        return $this->st_codchavewsuny;
    }

    public function setSt_codchavewsuny($st_codchavewsuny)
    {
        $this->st_codchavewsuny = $st_codchavewsuny;
    }

    public function getBl_criarsala()
    {
        return $this->bl_criarsala;
    }

    public function setBl_criarsala($bl_criarsala)
    {
        $this->bl_criarsala = $bl_criarsala;
    }
}

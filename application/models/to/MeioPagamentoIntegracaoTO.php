<?php
/**
 * Classe para encapsular os dados de meios de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class MeioPagamentoIntegracaoTO extends Ead1_TO_Dinamico {

	public $id_meiopagamentointegracao;
	public $id_meiopagamento;
	public $id_cartaobandeira;
	public $id_sistema;
	public $id_entidade;
	public $id_usuariocadastro;
	public $st_codsistema;
	public $dt_cadastro;
	public $st_codcontacaixa;
	
	/**
	 * @return int
	 */
	public function getId_meiopagamentointegracao() {
		return $this->id_meiopagamentointegracao;
	}
	
	/**
	 * @return int
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_cartaobandeira() {
		return $this->id_cartaobandeira;
	}
	
	/**
	 * @return int
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return string
	 */
	public function getSt_codsistema() {
		return $this->st_codsistema;
	}
	
	/**
	 * @return string
	 */
	public function getSt_codcontacaixa() {
		return $this->st_codcontacaixa;
	}
	
	/**
	 * @return date
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @param int $id_meiopagamentointegracao
	 */
	public function setId_meiopagamentointegracao($id_meiopagamentointegracao) {
		$this->id_meiopagamentointegracao = $id_meiopagamentointegracao;
	}
	
	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param int $id_cartaobandeira
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}
	
	/**
	 * @param int $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param string $st_codsistema
	 */
	public function setSt_codsistema($st_codsistema) {
		$this->st_codsistema = $st_codsistema;
	}
	
	/**
	 * @param string $st_codcontacaixa
	 */
	public function setSt_codcontacaixa($st_codcontacaixa) {
		$this->st_codcontacaixa = $st_codcontacaixa;
	}
	
	/**
	 * @param date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados da view de observacao da entrega de material
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwObservacaoEntregaMaterialTO extends Ead1_TO_Dinamico{
	
	public $id_usuario;
	public $id_observacao;
	public $dt_cadastro;
	public $id_itemdematerial;
	public $id_matricula;
	public $id_entregamaterial;
	public $st_observacao;
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_observacao
	 */
	public function getId_observacao() {
		return $this->id_observacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_entregamaterial
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}

	/**
	 * @return the $st_observacao
	 */
	public function getSt_observacao() {
		return $this->st_observacao;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_observacao
	 */
	public function setId_observacao($id_observacao) {
		$this->id_observacao = $id_observacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}

	/**
	 * @param field_type $st_observacao
	 */
	public function setSt_observacao($st_observacao) {
		$this->st_observacao = $st_observacao;
	}

	
}
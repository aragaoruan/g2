<?php
/**
 * Classe para encapsular os dados de Tipo de Tramite.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class TipoTramiteTO extends Ead1_TO_Dinamico{
	
	public $id_tipotramite;
	public $id_categoriatramite;
	public $st_tipotramite;
	
	/**
	 * @return the $id_tipotramite
	 */
	public function getId_tipotramite() {
		return $this->id_tipotramite;
	}

	/**
	 * @return the $id_categoriatramite
	 */
	public function getId_categoriatramite() {
		return $this->id_categoriatramite;
	}

	/**
	 * @return the $st_tipotramite
	 */
	public function getSt_tipotramite() {
		return $this->st_tipotramite;
	}

	/**
	 * @param field_type $id_tipotramite
	 */
	public function setId_tipotramite($id_tipotramite) {
		$this->id_tipotramite = $id_tipotramite;
	}

	/**
	 * @param field_type $id_categoriatramite
	 */
	public function setId_categoriatramite($id_categoriatramite) {
		$this->id_categoriatramite = $id_categoriatramite;
	}

	/**
	 * @param field_type $st_tipotramite
	 */
	public function setSt_tipotramite($st_tipotramite) {
		$this->st_tipotramite = $st_tipotramite;
	}

}
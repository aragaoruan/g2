<?php
/** 
 * Classe para encapsular os dados da tabela tb_vendaprodutonivelserie
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class VendaProdutoNivelSerieTO extends Ead1_TO_Dinamico{
	
	public $id_vendaproduto;     
	public $id_projetopedagogico;     
	public $id_serie;
	public $id_nivelensino;
	
	/**
	 * @return the $id_vendaproduto
	 */
	public function getId_vendaproduto() {
		return $this->id_vendaproduto;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @param $id_vendaproduto the $id_vendaproduto to set
	 */
	public function setId_vendaproduto($id_vendaproduto) {
		$this->id_vendaproduto = $id_vendaproduto;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	
	
}
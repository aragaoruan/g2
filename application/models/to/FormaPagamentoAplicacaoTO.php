<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class FormaPagamentoAplicacaoTO extends Ead1_TO_Dinamico{
	
	const VENDA_INTERNA = 1;
	const VENDA_SITE = 2;
	const PRE_VENDA_INTERNA = 3;
	const PRE_VENDA_SITE = 4;

	public $id_formapagamentoaplicacao;
	public $st_formapagamentoaplicacao;


	/**
	 * @return the $id_formapagamentoaplicacao
	 */
	public function getId_formapagamentoaplicacao(){ 
		return $this->id_formapagamentoaplicacao;
	}

	/**
	 * @return the $st_formapagamentoaplicacao
	 */
	public function getSt_formapagamentoaplicacao(){ 
		return $this->st_formapagamentoaplicacao;
	}


	/**
	 * @param field_type $id_formapagamentoaplicacao
	 */
	public function setId_formapagamentoaplicacao($value){ 
		$this->id_formapagamentoaplicacao = $value;
	}

	/**
	 * @param field_type $st_formapagamentoaplicacao
	 */
	public function setSt_formapagamentoaplicacao($value){ 
		$this->st_formapagamentoaplicacao = $value;
	}

}
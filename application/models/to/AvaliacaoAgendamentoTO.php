<?php

/**
 * Classe para encapsular os dados de agendamento de avaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class AvaliacaoAgendamentoTO extends Ead1_TO_Dinamico
{

    const SITUACAO_AGENDADO = 68;

    public $id_avaliacaoagendamento;
    public $id_avaliacaoaplicacao;
    public $id_matricula;
    public $id_situacao;
    public $id_usuario;
    public $id_usuariocadastro;
    public $id_avaliacaoconjuntoreferencia;
    public $id_avaliacao;
    public $dt_agendamento;
    public $dt_cadastro;
    public $bl_ativo;
    public $id_entidade;
    public $dt_envioaviso;


    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }


    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return the $id_avaliacaoagendamento
     */
    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    /**
     * @return the $id_avaliacaoaplicacao
     */
    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_avaliacaoconjuntoreferencia
     */
    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @return the $id_avaliacao
     */
    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @return the $dt_agendamento
     */
    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $id_avaliacaoagendamento
     */
    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    /**
     * @param field_type $id_avaliacaoaplicacao
     */
    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param field_type $id_avaliacaoconjuntoreferencia
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param field_type $id_avaliacao
     */
    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
    }

    /**
     * @param field_type $dt_agendamento
     */
    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return mixed
     */
    public function getDt_envioaviso()
    {
        return $this->dt_envioaviso;
    }

    /**
     * @param mixed $dt_envioaviso
     */
    public function setDt_envioaviso($dt_envioaviso)
    {
        $this->dt_envioaviso = $dt_envioaviso;
    }



}
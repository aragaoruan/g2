<?php
/**
 * Classe para encapsular os dados de selecionar perfil
 * @author edermariano
 * @package models
 * @subpackage to
 */
class SelecionarPerfilTO extends Ead1_TO_Dinamico {
	
	/**
	 * Atributo que contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var String
	 */
	public $id_entidade;


	public $id_linhadenegocio;
	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	public function getId_linhadenegocio()
	{
		return $this->id_linhadenegocio;
	}

	public function setId_linhadenegocio($id_linhadenegocio)
	{
		$this->id_linhadenegocio = $id_linhadenegocio;
	}



}
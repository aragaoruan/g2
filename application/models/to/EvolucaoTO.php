<?php
/**
 * Classe para encapsular os dados de Evolucao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class EvolucaoTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id da evolucao
	 * @var int
	 */
	public $id_evolucao;
	
	/**
	 * Contem o nome da evolucao
	 * @var string
	 */
	public $st_evolucao;
	
	/**
	 * Contem o nome da tabela da evolucao
	 * @var string
	 */
	public $st_tabela;
	
	/**
	 * Contem o nome do campo da tabela da evolucao
	 * @var string
	 */
	public $st_campo;
	
	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @return the $st_tabela
	 */
	public function getSt_tabela() {
		return $this->st_tabela;
	}

	/**
	 * @return the $st_campo
	 */
	public function getSt_campo() {
		return $this->st_campo;
	}

	/**
	 * @param int $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param string $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param string $st_tabela
	 */
	public function setSt_tabela($st_tabela) {
		$this->st_tabela = $st_tabela;
	}

	/**
	 * @param string $st_campo
	 */
	public function setSt_campo($st_campo) {
		$this->st_campo = $st_campo;
	}

	
}

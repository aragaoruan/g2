<?php
/**
 * Classe para encapsular os dados de endereço.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PesquisaOrganizacaoTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_instituicao;
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_polo;

	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_grupo;
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_nucleo;
	
	/**
	 * Atributo que contem o id da situação
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * @return the $bl_instituicao
	 */
	public function getBl_instituicao() {
		return $this->bl_instituicao;
	}

	/**
	 * @return the $bl_polo
	 */
	public function getBl_polo() {
		return $this->bl_polo;
	}

	/**
	 * @return the $bl_grupo
	 */
	public function getBl_grupo() {
		return $this->bl_grupo;
	}

	/**
	 * @return the $bl_nucleo
	 */
	public function getBl_nucleo() {
		return $this->bl_nucleo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $bl_instituicao the $bl_instituicao to set
	 */
	public function setBl_instituicao($bl_instituicao) {
		$this->bl_instituicao = $bl_instituicao;
	}

	/**
	 * @param $bl_polo the $bl_polo to set
	 */
	public function setBl_polo($bl_polo) {
		$this->bl_polo = $bl_polo;
	}

	/**
	 * @param $bl_grupo the $bl_grupo to set
	 */
	public function setBl_grupo($bl_grupo) {
		$this->bl_grupo = $bl_grupo;
	}

	/**
	 * @param $bl_nucleo the $bl_nucleo to set
	 */
	public function setBl_nucleo($bl_nucleo) {
		$this->bl_nucleo = $bl_nucleo;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	
}


<?php
/**
 * Classe para encapsular os dados de Transação Lançamento.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class TransacaoLancamentoTO extends Ead1_TO_Dinamico {

	public $id_lancamento;
	public $id_transacaofinanceira;
	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_transacaofinanceira
	 */
	public function getId_transacaofinanceira() {
		return $this->id_transacaofinanceira;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $id_transacaofinanceira
	 */
	public function setId_transacaofinanceira($id_transacaofinanceira) {
		$this->id_transacaofinanceira = $id_transacaofinanceira;
	}

	
}
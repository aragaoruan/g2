<?php

class VwPagamentoCartaoTO extends Ead1_TO_Dinamico {

	public $nu_valorcartao;
	public $nu_vezes;
	public $id_venda;
	/**
	 * @return the $nu_valorcartao
	 */
	public function getNu_valorcartao() {
		return $this->nu_valorcartao;
	}

	/**
	 * @return the $nu_vezes
	 */
	public function getNu_vezes() {
		return $this->nu_vezes;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $nu_valorcartao
	 */
	public function setNu_valorcartao($nu_valorcartao) {
		$this->nu_valorcartao = $nu_valorcartao;
	}

	/**
	 * @param field_type $nu_vezes
	 */
	public function setNu_vezes($nu_vezes) {
		$this->nu_vezes = $nu_vezes;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	
	
}

?>
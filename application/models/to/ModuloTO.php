<?php
/**
 * Classe para encapsular os dados de módulo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ModuloTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do módulo.
	 * @var int
	 */
	public $id_modulo;
	
	/**
	 * Contém o nome do módulo.
	 * @var string
	 */
	public $st_modulo;
	
	/**
	 * Contém o título de exibição do módulo.
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * Exclusão Lógica.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém a descrição do módulo.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Contém o id da situação do módulo.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém o id do projeto pedagógico do módulo.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Contém o id do módulo anterior a este.
	 * @var int
	 */
	public $id_moduloanterior;
	
	/**
	 * Variavel que contem o array de disciplinas
	 * @var array
	 */
	public $arrDisciplinas;
	
	/**
	 * @return the $arrDisciplinas
	 */
	public function getArrDisciplinas() {
		return $this->arrDisciplinas;
	}

	/**
	 * @param $arrDisciplinas the $arrDisciplinas to set
	 */
	public function setArrDisciplinas($arrDisciplinas) {
		$this->arrDisciplinas = null;
		$this->arrDisciplinas[] = $arrDisciplinas;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return int
	 */
	public function getId_moduloanterior() {
		return $this->id_moduloanterior;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param int $id_moduloanterior
	 */
	public function setId_moduloanterior($id_moduloanterior) {
		$this->id_moduloanterior = $id_moduloanterior;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de observacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class ObservacaoTO extends Ead1_TO_Dinamico{
	
	public $id_observacao;
	public $id_usuario;
	public $id_entidade;
	public $st_observacao;
	public $dt_cadastro;
	
	/**
	 * @return the $id_observacao
	 */
	public function getId_observacao() {
		return $this->id_observacao;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_observacao
	 */
	public function getSt_observacao() {
		return $this->st_observacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_observacao
	 */
	public function setId_observacao($id_observacao) {
		$this->id_observacao = $id_observacao;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_observacao
	 */
	public function setSt_observacao($st_observacao) {
		$this->st_observacao = $st_observacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

}
<?php
/**
 * Classe para encapsular os dados da view de Avaliacao e disciplinas.
 * @author Eder Lamar
 * 
 * @package models
 * @subpackage to
 */
class VwAvaliacaoDisciplinaTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaodisciplina;
	public $id_disciplina;
	public $id_avaliacao;
	public $st_avaliacao;
	public $id_tipoavaliacao;
	public $nu_valor;
	public $id_situacao;
	public $st_disciplina;
	public $st_descricao;
	public $st_tituloexibicao;
	
	/**
	 * @return the $id_avaliacaodisciplina
	 */
	public function getId_avaliacaodisciplina() {
		return $this->id_avaliacaodisciplina;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $id_tipoavaliacao
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @param $id_avaliacaodisciplina the $id_avaliacaodisciplina to set
	 */
	public function setId_avaliacaodisciplina($id_avaliacaodisciplina) {
		$this->id_avaliacaodisciplina = $id_avaliacaodisciplina;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_avaliacao the $id_avaliacao to set
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param $st_avaliacao the $st_avaliacao to set
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param $id_tipoavaliacao the $id_tipoavaliacao to set
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param $nu_valor the $nu_valor to set
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $st_tituloexibicao the $st_tituloexibicao to set
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}
            
}
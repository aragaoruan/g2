<?php
/**
 * Classe para encapsular os dados de relacionamento entre aplicações e forma de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FormaPagamentoAplicacoesTO extends Ead1_TO_Dinamico {

	/**
	 * Id da forma de pagamento.
	 * @var int
	 */
	public $id_formapagamento;
	
	/**
	 * Id da aplicação de pagamento.
	 * @var int
	 */
	public $id_aplicacoespagamento;
	
	/**
	 * @return int
	 */
	public function getId_aplicacoespagamento() {
		return $this->id_aplicacoespagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @param int $id_aplicacoespagamento
	 */
	public function setId_aplicacoespagamento($id_aplicacoespagamento) {
		$this->id_aplicacoespagamento = $id_aplicacoespagamento;
	}
	
	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

}

?>
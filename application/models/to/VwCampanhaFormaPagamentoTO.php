<?php

class VwCampanhaFormaPagamentoTO extends Ead1_TO_Dinamico {

	public $id_campanhacomercial;
	
	public $st_campanhacomercial;
	
	public $id_formapagamento;
	
	public $st_formapagamento;
	
	public $id_entidade;
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}
	
	/**
	 * @param unknown_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param unknown_type $st_campanhacomercial
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

}

?>
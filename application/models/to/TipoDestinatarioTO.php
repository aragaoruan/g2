<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoDestinatarioTO extends Ead1_TO_Dinamico{

	public $id_tipodestinatario;
	public $st_tipodestinatario;
	
	const DESTINATARIO = 1;
	const CC = 2;
	const BCC = 3;


	/**
	 * @return the $id_tipodestinatario
	 */
	public function getId_tipodestinatario(){ 
		return $this->id_tipodestinatario;
	}

	/**
	 * @return the $st_tipodestinatario
	 */
	public function getSt_tipodestinatario(){ 
		return $this->st_tipodestinatario;
	}


	/**
	 * @param field_type $id_tipodestinatario
	 */
	public function setId_tipodestinatario($id_tipodestinatario){ 
		$this->id_tipodestinatario = $id_tipodestinatario;
	}

	/**
	 * @param field_type $st_tipodestinatario
	 */
	public function setSt_tipodestinatario($st_tipodestinatario){ 
		$this->st_tipodestinatario = $st_tipodestinatario;
	}

}
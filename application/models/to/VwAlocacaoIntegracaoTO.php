<?php
/** 
 * Classe para encapsular os dados da view de alocacao na integração
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class VwAlocacaoIntegracaoTO extends Ead1_TO_Dinamico{
	
	public $id_alocacao;
	public $id_saladeaula;
	public $id_situacao;
	public $st_codsistemasala;
	public $st_codsistemacurso;
	public $st_codsistemareferencia;
	public $id_usuario;
	public $st_codusuario;
	public $st_codalocacao;
	public $id_sistema;
	public $id_usuariocadastro;
	
	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_codsistemasala
	 */
	public function getSt_codsistemasala() {
		return $this->st_codsistemasala;
	}

	/**
	 * @return the $st_codsistemacurso
	 */
	public function getSt_codsistemacurso() {
		return $this->st_codsistemacurso;
	}

	/**
	 * @return the $st_codsistemareferencia
	 */
	public function getSt_codsistemareferencia() {
		return $this->st_codsistemareferencia;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_codusuario
	 */
	public function getSt_codusuario() {
		return $this->st_codusuario;
	}

	/**
	 * @return the $st_codalocacao
	 */
	public function getSt_codalocacao() {
		return $this->st_codalocacao;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param $id_alocacao the $id_alocacao to set
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @param $id_saladeaula the $id_saladeaula to set
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_codsistemasala the $st_codsistemasala to set
	 */
	public function setSt_codsistemasala($st_codsistemasala) {
		$this->st_codsistemasala = $st_codsistemasala;
	}

	/**
	 * @param $st_codsistemacurso the $st_codsistemacurso to set
	 */
	public function setSt_codsistemacurso($st_codsistemacurso) {
		$this->st_codsistemacurso = $st_codsistemacurso;
	}

	/**
	 * @param $st_codsistemareferencia the $st_codsistemareferencia to set
	 */
	public function setSt_codsistemareferencia($st_codsistemareferencia) {
		$this->st_codsistemareferencia = $st_codsistemareferencia;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $st_codusuario the $st_codusuario to set
	 */
	public function setSt_codusuario($st_codusuario) {
		$this->st_codusuario = $st_codusuario;
	}

	/**
	 * @param $st_codalocacao the $st_codalocacao to set
	 */
	public function setSt_codalocacao($st_codalocacao) {
		$this->st_codalocacao = $st_codalocacao;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}


	
}
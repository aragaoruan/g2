<?php
/**
 * Classe para encapsular os dados de Transação Financeira.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class TransacaoFinanceiraTO extends Ead1_TO_Dinamico {

	const AUTORIZADA 	= 1;
	const RECUSADA 		= 2;
	const CANCELADA		= 3;

	public $dt_cadastro;
	public $id_usuariocadastro;
	public $id_transacaofinanceira;
	public $id_venda;
	public $id_cartaobandeira;
	public $id_cartaooperadora;
	public $nu_verificacao;
	public $un_orderid;


	/**
	 * Status da transacao no gateway
	 * @var int
	 */
	public $nu_status;

	/**
	 * Código da transação no gateway
	 * BRASPAG = AuthorizationCode
	 * LOCAWEB - ?
	 * @var string
	 */
	public $st_codtransacaogateway;

	/**
	 * Codigo da transação na operadora
	 * BRASPAG = AcquirerTransactionId
	 * LOCAWEB - ?
	 * @var string
	 */
	public $st_codtransacaooperadora;

	/**
	 * Identificador único da transacao
	 * BRASPAG = BraspagTransactionId
	 * LOCAWEB - ?
	 * @var uniq
	 */
	public $un_transacaofinanceira;


    /** @var $st_titularcartao string(255) */
    public $st_titularcartao;

    /** @var $st_ultimosdigitos string(4) */
    public $st_ultimosdigitos;

    /** @var $nu_parcelas integer */
    public $nu_parcelas;

    /** @var $nu_valorcielo decimal(10,2) */
    public $nu_valorcielo;

    public $id_respostaautorizacao;

    /**
     * @param string $st_titularcartao
     */
    public function setSt_titularcartao($st_titularcartao)
    {
        $this->st_titularcartao = $st_titularcartao;
    }

    /**
     * @return string
     */
    public function getSt_titularcartao()
    {
        return $this->st_titularcartao;
    }

    /**
     * @param string $st_ultimosdigitos
     */
    public function setSt_ultimosdigitos($st_ultimosdigitos)
    {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @return string
     */
    public function getSt_ultimosdigitos()
    {
        return $this->st_ultimosdigitos;
    }

    /**
     * @param \decimal $nu_valorcielo
     */
    public function setNu_valorcielo($nu_valorcielo)
    {
        $this->nu_valorcielo = $nu_valorcielo;
    }

    /**
     * @return \decimal
     */
    public function getNu_valorcielo()
    {
        return $this->nu_valorcielo;
    }

    /**
     * @param int $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @return int
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

	/**
	 * @return the $un_orderid
	 */
	public function getUn_orderid() {
		return $this->un_orderid;
	}

	/**
	 * @param field_type $un_orderid
	 */
	public function setUn_orderid($un_orderid) {
		$this->un_orderid = $un_orderid;
	}

	/**
	 * @return the $nu_verificacao
	 */
	public function getNu_verificacao() {
		return $this->nu_verificacao;
	}

	/**
	 * @param field_type $nu_verificacao
	 */
	public function setNu_verificacao($nu_verificacao) {
		$this->nu_verificacao = $nu_verificacao;
	}

	/**
	 * @return the $id_cartaobandeira
	 */
	public function getId_cartaobandeira() {
		return $this->id_cartaobandeira;
	}

	/**
	 * @return the $id_cartaooperadora
	 */
	public function getId_cartaooperadora() {
		return $this->id_cartaooperadora;
	}

	/**
	 * @param field_type $id_cartaobandeira
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}

	/**
	 * @param field_type $id_cartaooperadora
	 */
	public function setId_cartaooperadora($id_cartaooperadora) {
		$this->id_cartaooperadora = $id_cartaooperadora;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_transacaofinanceira
	 */
	public function getId_transacaofinanceira() {
		return $this->id_transacaofinanceira;
	}

	/**
	 * @param field_type $id_transacaofinanceira
	 */
	public function setId_transacaofinanceira($id_transacaofinanceira) {
		$this->id_transacaofinanceira = $id_transacaofinanceira;
	}

	/**
	 * @return the $nu_status
	 */
	public function getNu_status() {
		return $this->nu_status;
	}

	/**
	 * @param number $nu_status
	 */
	public function setNu_status($nu_status) {
		$this->nu_status = $nu_status;
	}

	/**
	 * @return the $st_codtransacaogateway
	 */
	public function getSt_codtransacaogateway() {
		return $this->st_codtransacaogateway;
	}

	/**
	 * @param string $st_codtransacaogateway
	 */
	public function setSt_codtransacaogateway($st_codtransacaogateway) {
		$this->st_codtransacaogateway = $st_codtransacaogateway;
	}

	/**
	 * @return the $st_codtransacaooperadora
	 */
	public function getSt_codtransacaooperadora() {
		return $this->st_codtransacaooperadora;
	}

	/**
	 * @param string $st_codtransacaooperadora
	 */
	public function setSt_codtransacaooperadora($st_codtransacaooperadora) {
		$this->st_codtransacaooperadora = $st_codtransacaooperadora;
	}

	/**
	 * @return the $un_transacaofinanceira
	 */
	public function getUn_transacaofinanceira() {
		return $this->un_transacaofinanceira;
	}

	/**
	 * @param uniq $un_transacaofinanceira
	 */
	public function setUn_transacaofinanceira($un_transacaofinanceira) {
		$this->un_transacaofinanceira = $un_transacaofinanceira;
	}

    /**
      * @param mixed $id_respostaautorizacao
      */
    public function setId_respostaautorizacao($id_respostaautorizacao)
    {
        $this->id_respostaautorizacao = $id_respostaautorizacao;
    }

    /**
     * @return mixed
     */
    public function getId_respostaautorizacao()
    {
        return $this->id_respostaautorizacao;
    }

}
<?php

class PesquisarItemMaterialTO extends Ead1_TO_Dinamico {
	
	public $id_itemdematerial;
	public $st_itemdematerial;
	public $id_situacao;
	public $id_tipodematerial;
	public $dt_cadastro;
	public $id_usuariocadastro;
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.material.CadastrarItemMaterial';
	
	
	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $st_itemdematerial
	 */
	public function getSt_itemdematerial() {
		return $this->st_itemdematerial;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_tipodematerial
	 */
	public function getId_tipodematerial() {
		return $this->id_tipodematerial;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @param $id_itemdematerial the $id_itemdematerial to set
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param $st_itemdematerial the $st_itemdematerial to set
	 */
	public function setSt_itemdematerial($st_itemdematerial) {
		$this->st_itemdematerial = $st_itemdematerial;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_tipodematerial the $id_tipodematerial to set
	 */
	public function setId_tipodematerial($id_tipodematerial) {
		$this->id_tipodematerial = $id_tipodematerial;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $st_classeflex the $st_classeflex to set
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

	
	

}

?>
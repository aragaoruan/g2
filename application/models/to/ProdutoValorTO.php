<?php
/**
 * Classe para encapsular os dados do valor do produto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class ProdutoValorTO extends Ead1_TO_Dinamico {

	public $id_produtovalor;
	public $id_produto;
	public $dt_cadastro;
	public $dt_termino;
	public $dt_inicio;
	public $id_usuariocadastro;
	public $id_tipoprodutovalor;
	public $nu_valor;
	public $nu_valormensal;
	public $nu_basepropor;
	
	/**
	 * @return the $id_produtovalor
	 */
	public function getId_produtovalor() {
		return $this->id_produtovalor;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_tipoprodutovalor
	 */
	public function getId_tipoprodutovalor() {
		return $this->id_tipoprodutovalor;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $nu_valormensal
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}

	/**
	 * @return the $nu_basepropor
	 */
	public function getNu_basepropor() {
		return $this->nu_basepropor;
	}

	/**
	 * @param field_type $id_produtovalor
	 */
	public function setId_produtovalor($id_produtovalor) {
		$this->id_produtovalor = $id_produtovalor;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_tipoprodutovalor
	 */
	public function setId_tipoprodutovalor($id_tipoprodutovalor) {
		$this->id_tipoprodutovalor = $id_tipoprodutovalor;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
	}

	/**
	 * @param field_type $nu_basepropor
	 */
	public function setNu_basepropor($nu_basepropor) {
		$this->nu_basepropor = $nu_basepropor;
	}

}
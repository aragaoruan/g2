<?php

class VwFuncaoUsuarioTO extends Ead1_TO_Dinamico {

	public $id_funcao;
	public $st_funcao;
	public $id_entidade;
	public $id_usuario;
	public $st_nomecompleto;
	public $id_evolucao;
	public $nu_vendas;
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_vendas() {
		return $this->nu_vendas;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $nu_vendas
	 */
	public function setNu_vendas($nu_vendas) {
		$this->nu_vendas = $nu_vendas;
	}
	
	/**
	 * @param unknown_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwGridEstabelecimentoTO extends Ead1_TO_Dinamico{

	public $id_matricula;
	public $id_serie;
	public $st_serie;
	public $dt_avaliacao;
	public $st_instituicao;
	public $sg_uf;
	public $st_nomemunicipio;
	public $nu_anoconclusao;
	public $nu_ordem;
	public $dt_cadastro;
	public $id_evolucao;


	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie(){ 
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie(){ 
		return $this->st_serie;
	}

	/**
	 * @return the $dt_avaliacao
	 */
	public function getDt_avaliacao(){ 
		return $this->dt_avaliacao;
	}

	/**
	 * @return the $st_instituicao
	 */
	public function getSt_instituicao(){ 
		return $this->st_instituicao;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf(){ 
		return $this->sg_uf;
	}

	/**
	 * @return the $st_nomemunicipio
	 */
	public function getSt_nomemunicipio(){ 
		return $this->st_nomemunicipio;
	}

	/**
	 * @return the $nu_anoconclusao
	 */
	public function getNu_anoconclusao(){ 
		return $this->nu_anoconclusao;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem(){ 
		return $this->nu_ordem;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao(){ 
		return $this->id_evolucao;
	}


	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie){ 
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $st_serie
	 */
	public function setSt_serie($st_serie){ 
		$this->st_serie = $st_serie;
	}

	/**
	 * @param field_type $dt_avaliacao
	 */
	public function setDt_avaliacao($dt_avaliacao){ 
		$this->dt_avaliacao = $dt_avaliacao;
	}

	/**
	 * @param field_type $st_instituicao
	 */
	public function setSt_instituicao($st_instituicao){ 
		$this->st_instituicao = $st_instituicao;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf){ 
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio){ 
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

	/**
	 * @param field_type $nu_anoconclusao
	 */
	public function setNu_anoconclusao($nu_anoconclusao){ 
		$this->nu_anoconclusao = $nu_anoconclusao;
	}

	/**
	 * @param field_type $nu_ordem
	 */
	public function setNu_ordem($nu_ordem){ 
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao){ 
		$this->id_evolucao = $id_evolucao;
	}

}
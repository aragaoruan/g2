<?php

/**
 * Classe para encapsular os dados de relacionamento entre disciplina e item de material.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ItemDeMaterialDisciplinaTO extends Ead1_TO_Dinamico
{

    /**
     * Id do item de material.
     * @var int
     */
    public $id_itemdematerial;

    /**
     * Id da disciplina
     * @var int
     */
    public $id_disciplina;

    /**
     * Chave primaria da tabela
     * @var int
     */
    public $id_itemdematerialdisciplina;

    /**
     * @return the $id_itemdematerial
     */
    public function getId_itemdematerial ()
    {
        return $this->id_itemdematerial;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * @return the $id_itemdematerialdisciplina
     */
    public function getId_itemdematerialdisciplina ()
    {
        return $this->id_itemdematerialdisciplina;
    }

    /**
     * @param int $id_itemdematerial
     */
    public function setId_itemdematerial ($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param field_type $id_itemdematerialdisciplina
     */
    public function setId_itemdematerialdisciplina ($id_itemdematerialdisciplina)
    {
        $this->id_itemdematerialdisciplina = $id_itemdematerialdisciplina;
    }

}

?>
<?php
/**
 * Classe para encapsular os dados da view série e nível de ensino com módulo e projeto.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 22/10/2010
 * 
 * @package models
 * @subpackage to
 */
class VwSerieModuloTO extends Ead1_TO_Dinamico {

	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id da entidade cadastradoral.
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * Id do módulo.
	 * @var int
	 */
	public $id_modulo;
	
	/**
	 * Id do módulo anterior.
	 * @var int
	 */
	public $id_moduloanterior;
	
	/**
	 * Nome do módulo.
	 * @var string
	 */
	public $st_modulo;
	
	/**
	 * Título de exibição do módulo.
	 * @var string
	 */
	public $st_tituloexibicaomodulo;
	
	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Nome da disciplina.
	 * @var string
	 */
	public $st_disciplina;
	
	/**
	 * Descrição da disciplina.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return int
	 */
	public function getId_moduloanterior() {
		return $this->id_moduloanterior;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return string
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicaomodulo() {
		return $this->st_tituloexibicaomodulo;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 * @param int $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param int $id_moduloanterior
	 */
	public function setId_moduloanterior($id_moduloanterior) {
		$this->id_moduloanterior = $id_moduloanterior;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param string $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param string $st_tituloexibicaomodulo
	 */
	public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
		$this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados da vw_setorcargofuncaousuario.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class VwSetorCargoFuncaoUsuarioTO extends Ead1_TO_Dinamico {
		
	public $id_usuario;
	public $st_login;
	public $st_nomecompleto;
	public $id_cargo;
	public $st_cargo;
	public $st_ramal;
	public $id_funcao;
	public $st_funcao;
	public $id_setor;
	public $st_setor;
	
	/**
	 * @return the $st_login
	 */
	public function getSt_login() {
		return $this->st_login;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_cargo
	 */
	public function getId_cargo() {
		return $this->id_cargo;
	}

	/**
	 * @return the $st_cargo
	 */
	public function getSt_cargo() {
		return $this->st_cargo;
	}

	/**
	 * @return the $st_ramal
	 */
	public function getSt_ramal() {
		return $this->st_ramal;
	}

	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @return the $st_funcao
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}

	/**
	 * @return the $id_setor
	 */
	public function getId_setor() {
		return $this->id_setor;
	}

	/**
	 * @return the $st_setor
	 */
	public function getSt_setor() {
		return $this->st_setor;
	}

	/**
	 * @param field_type $st_login
	 */
	public function setSt_login($st_login) {
		$this->st_login = $st_login;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $id_cargo
	 */
	public function setId_cargo($id_cargo) {
		$this->id_cargo = $id_cargo;
	}

	/**
	 * @param field_type $st_cargo
	 */
	public function setSt_cargo($st_cargo) {
		$this->st_cargo = $st_cargo;
	}

	/**
	 * @param field_type $st_ramal
	 */
	public function setSt_ramal($st_ramal) {
		$this->st_ramal = $st_ramal;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

	/**
	 * @param field_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}

	/**
	 * @param field_type $id_setor
	 */
	public function setId_setor($id_setor) {
		$this->id_setor = $id_setor;
	}

	/**
	 * @param field_type $st_setor
	 */
	public function setSt_setor($st_setor) {
		$this->st_setor = $st_setor;
	}
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
}
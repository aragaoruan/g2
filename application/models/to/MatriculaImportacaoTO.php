<?php
class MatriculaImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_matriculaimportacao;
	public $id_pessoaimportacao;
	public $id_sistemaimportacao;
	public $id_entidade;
	public $nu_codpessoaorigem;
	public $nu_codmatriculaorigem;
	public $nu_codprojetoorigem;
	public $dt_inicio;
	public $dt_fim;
	public $nu_codsituacao;
	public $nu_livro;
	public $nu_registro;
	public $nu_bolsa;
	
	public function getNu_bolsa() {
		return $this->nu_bolsa;
	}
	
	public function getNu_livro() {
		return $this->nu_livro;
	}
	
	public function getNu_registro() {
		return $this->nu_registro;
	}
	
	
	/**
	 * @return the $id_matriculaimportacao
	 */
	public function getId_matriculaimportacao() {
		return $this->id_matriculaimportacao;
	}

	/**
	 * @return the $id_pessoaimportacao
	 */
	public function getId_pessoaimportacao() {
		return $this->id_pessoaimportacao;
	}

	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_codpessoaorigem
	 */
	public function getNu_codpessoaorigem() {
		return $this->nu_codpessoaorigem;
	}

	/**
	 * @return the $nu_codmatriculaorigem
	 */
	public function getNu_codmatriculaorigem() {
		return $this->nu_codmatriculaorigem;
	}

	/**
	 * @return the $nu_codprojetoorigem
	 */
	public function getNu_codprojetoorigem() {
		return $this->nu_codprojetoorigem;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @return the $nu_codsituacao
	 */
	public function getNu_codsituacao() {
		return $this->nu_codsituacao;
	}

	/**
	 * @param field_type $id_matriculaimportacao
	 */
	public function setId_matriculaimportacao($id_matriculaimportacao) {
		$this->id_matriculaimportacao = $id_matriculaimportacao;
	}

	/**
	 * @param field_type $id_pessoaimportacao
	 */
	public function setId_pessoaimportacao($id_pessoaimportacao) {
		$this->id_pessoaimportacao = $id_pessoaimportacao;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $nu_codpessoaorigem
	 */
	public function setNu_codpessoaorigem($nu_codpessoaorigem) {
		$this->nu_codpessoaorigem = $nu_codpessoaorigem;
	}

	/**
	 * @param field_type $nu_codmatriculaorigem
	 */
	public function setNu_codmatriculaorigem($nu_codmatriculaorigem) {
		$this->nu_codmatriculaorigem = $nu_codmatriculaorigem;
	}

	/**
	 * @param field_type $nu_codprojetoorigem
	 */
	public function setNu_codprojetoorigem($nu_codprojetoorigem) {
		$this->nu_codprojetoorigem = $nu_codprojetoorigem;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @param field_type $nu_codsituacao
	 */
	public function setNu_codsituacao($nu_codsituacao) {
		$this->nu_codsituacao = $nu_codsituacao;
	}
	
	public function setNu_livro($nu_livro) {
		$this->nu_livro = $nu_livro;
	}

	public function setNu_bolsa($nu_bolsa) {
		$this->nu_bolsa = $nu_bolsa;
	}
	
	public function setNu_registro($nu_registro) {
		$this->nu_registro = $nu_registro;
	}

}
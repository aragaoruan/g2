<?php


/**
 * @author Elcio Mauro Guimarães
 * @package models
 * @subpackage  to
 *
 */
class VwProdutoTaxaContratoTO extends Ead1_TO_Dinamico {

	public $id_matricula;
	public $id_taxa;
	public $st_taxa;
	public $nu_valor;
	public $id_produto;
	public $id_entidade;
	public $id_situacao;
	public $id_tipoproduto;
	public $nu_gratuito;
	public $st_produto;
	public $id_contrato;
	public $id_projetopedagogico;
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_taxa
	 */
	public function getId_taxa() {
		return $this->id_taxa;
	}

	/**
	 * @param field_type $id_taxa
	 */
	public function setId_taxa($id_taxa) {
		$this->id_taxa = $id_taxa;
	}

	/**
	 * @return the $st_taxa
	 */
	public function getSt_taxa() {
		return $this->st_taxa;
	}

	/**
	 * @param field_type $st_taxa
	 */
	public function setSt_taxa($st_taxa) {
		$this->st_taxa = $st_taxa;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @return the $nu_gratuito
	 */
	public function getNu_gratuito() {
		return $this->nu_gratuito;
	}

	/**
	 * @param field_type $nu_gratuito
	 */
	public function setNu_gratuito($nu_gratuito) {
		$this->nu_gratuito = $nu_gratuito;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @param field_type $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	
	
}

?>
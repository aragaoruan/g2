<?php

/**
 * Classe para encapsular os dados de pessoa
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class UsuarioTO extends Ead1_TO_Dinamico
{

    /**
     * Atributo que contém o login do Usuario
     * @var string
     */
    public $st_login;
    /**
     * Atributo que contém a senha do Usuario
     * @var string
     */
    public $st_senha;
    /**
     * Atributo que contém o nome completo do Usuario
     * @var string
     */
    public $st_nomecompleto;
    /**
     * Atributo que contém o CPF do Usuario
     * @var string
     */
    public $st_cpf;
    /**
     * Atributo que contém o situacao Usuario
     * @var bool
     */
    public $bl_ativo;
    /**
     * Atributo que contém o id do Usuario
     * @var int
     */
    public $id_usuario;
    /**
     * Atributo que contém o tipo Usuario
     * @var int
     */
    public $id_registropessoa;
    /**
     * Atributo que contém o id do Usuario que registrou este Usuario
     * @var int
     */
    public $id_usuariopai;

    /**
     * @var bool
     */
    public $bl_redefinicaosenha;

    /**
     * @var int
     */
    public $id_titulacao;

    /**
     * @return string
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @param string $st_login
     * @return $this
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param $st_senha
     * @param null $naoconverter
     * @return $this
     */
    public function setSt_senha($st_senha, $naoconverter = null)
    {
        if (!empty($st_senha) && strlen($st_senha) < 32 && $naoconverter != true) {

            $st_senha = md5($st_senha);
        }


        $this->st_senha = $st_senha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = (empty($st_cpf) ? null : $st_cpf);
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_registropessoa()
    {
        return $this->id_registropessoa;
    }

    /**
     * @param int $id_registropessoa
     * @return $this
     */
    public function setId_registropessoa($id_registropessoa)
    {
        $this->id_registropessoa = $id_registropessoa;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariopai()
    {
        return $this->id_usuariopai;
    }

    /**
     * @param int $id_usuariopai
     * @return $this
     */
    public function setId_usuariopai($id_usuariopai)
    {
        $this->id_usuariopai = $id_usuariopai;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_redefinicaosenha()
    {
        return $this->bl_redefinicaosenha;
    }

    /**
     * @param bool $bl_redefinicaosenha
     * @return $this
     */
    public function setBl_redefinicaosenha($bl_redefinicaosenha)
    {
        $this->bl_redefinicaosenha = $bl_redefinicaosenha;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_titulacao()
    {
        return $this->id_titulacao;
    }

    /**
     * @param int $id_titulacao
     * @return $this
     */
    public function setId_titulacao($id_titulacao)
    {
        $this->id_titulacao = $id_titulacao;
        return $this;
    }

}

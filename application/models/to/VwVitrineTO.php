<?php
/**
 * Classe para encapsular os dados da vw_vitrine
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class VwVitrineTO extends Ead1_TO_Dinamico {

	public $id_contratoafiliado;
	public $st_contratoafiliado;
	public $id_classeafiliado;
	public $id_entidadeafiliada;
	public $id_entidadecadastro;
	public $id_usuarioresponsavel;
	public $dt_cadastro;
	public $st_url;
	public $st_descricao;
	public $id_tipopessoa;
	public $id_situacao;
	public $id_vitrine;
	public $st_vitrine;
	public $id_modelovitrine;
	public $st_nomeentidade;
	public $st_nomecompleto;
	
	
	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @return the $st_contratoafiliado
	 */
	public function getSt_contratoafiliado() {
		return $this->st_contratoafiliado;
	}

	/**
	 * @param field_type $st_contratoafiliado
	 */
	public function setSt_contratoafiliado($st_contratoafiliado) {
		$this->st_contratoafiliado = $st_contratoafiliado;
	}

	/**
	 * @return the $id_classeafiliado
	 */
	public function getId_classeafiliado() {
		return $this->id_classeafiliado;
	}

	/**
	 * @param field_type $id_classeafiliado
	 */
	public function setId_classeafiliado($id_classeafiliado) {
		$this->id_classeafiliado = $id_classeafiliado;
	}

	/**
	 * @return the $id_entidadeafiliada
	 */
	public function getId_entidadeafiliada() {
		return $this->id_entidadeafiliada;
	}

	/**
	 * @param field_type $id_entidadeafiliada
	 */
	public function setId_entidadeafiliada($id_entidadeafiliada) {
		$this->id_entidadeafiliada = $id_entidadeafiliada;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @return the $id_usuarioresponsavel
	 */
	public function getId_usuarioresponsavel() {
		return $this->id_usuarioresponsavel;
	}

	/**
	 * @param field_type $id_usuarioresponsavel
	 */
	public function setId_usuarioresponsavel($id_usuarioresponsavel) {
		$this->id_usuarioresponsavel = $id_usuarioresponsavel;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $st_url
	 */
	public function getSt_url() {
		return $this->st_url;
	}

	/**
	 * @param field_type $st_url
	 */
	public function setSt_url($st_url) {
		$this->st_url = $st_url;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @return the $id_tipopessoa
	 */
	public function getId_tipopessoa() {
		return $this->id_tipopessoa;
	}

	/**
	 * @param field_type $id_tipopessoa
	 */
	public function setId_tipopessoa($id_tipopessoa) {
		$this->id_tipopessoa = $id_tipopessoa;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $id_vitrine
	 */
	public function getId_vitrine() {
		return $this->id_vitrine;
	}

	/**
	 * @param field_type $id_vitrine
	 */
	public function setId_vitrine($id_vitrine) {
		$this->id_vitrine = $id_vitrine;
	}

	/**
	 * @return the $st_vitrine
	 */
	public function getSt_vitrine() {
		return $this->st_vitrine;
	}

	/**
	 * @param field_type $st_vitrine
	 */
	public function setSt_vitrine($st_vitrine) {
		$this->st_vitrine = $st_vitrine;
	}

	/**
	 * @return the $id_modelovitrine
	 */
	public function getId_modelovitrine() {
		return $this->id_modelovitrine;
	}

	/**
	 * @param field_type $id_modelovitrine
	 */
	public function setId_modelovitrine($id_modelovitrine) {
		$this->id_modelovitrine = $id_modelovitrine;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
}

?>
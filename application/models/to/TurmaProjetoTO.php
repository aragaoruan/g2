<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TurmaProjetoTO extends Ead1_TO_Dinamico{

	public $id_turmaprojeto;
	public $id_usuariocadastro;
	public $id_turma;
	public $id_projetopedagogico;
	public $bl_ativo;
	public $dt_cadastro;


	/**
	 * @return the $id_turmaprojeto
	 */
	public function getId_turmaprojeto(){ 
		return $this->id_turmaprojeto;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_turma
	 */
	public function getId_turma(){ 
		return $this->id_turma;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_turmaprojeto
	 */
	public function setId_turmaprojeto($id_turmaprojeto){ 
		$this->id_turmaprojeto = $id_turmaprojeto;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma){ 
		$this->id_turma = $id_turma;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

}
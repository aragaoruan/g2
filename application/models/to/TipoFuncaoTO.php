<?php
/**
 * Classe para encapsular os dados de tipo de função
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TipoFuncaoTO extends Ead1_TO_Dinamico {
	
	public $id_tipofuncao;
	public $st_tipofuncao;
	
	/**
	 * @return unknown
	 */
	public function getId_tipofuncao() {
		return $this->id_tipofuncao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipofuncao() {
		return $this->st_tipofuncao;
	}
	
	/**
	 * @param unknown_type $id_tipofuncao
	 */
	public function setId_tipofuncao($id_tipofuncao) {
		$this->id_tipofuncao= $id_tipofuncao;
	}
	
	/**
	 * @param unknown_type $st_tipofuncao
	 */
	public function setSt_funcao($st_tipofuncao) {
		$this->st_tipofuncao = $st_tipofuncao;
	}
}
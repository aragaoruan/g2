<?php
/**
 * Classe para encapsular os dados de município.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class MunicipioTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do município.
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Contém o nome do município.
	 * @var string
	 */
	public $st_nomemunicipio;
	
	/**
	 * Contém o número do ibge do município.
	 * @var int
	 */
	public $nu_codigoibge;
	
	/**
	 * Contém a UF do município.
	 * @var String
	 */
	public $sg_uf;
	
	/**
	 * @return int
	 */
	public function getNu_codigoibge() {
		return $this->nu_codigoibge;
	}
	
	/**
	 * @return String
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @param int $nu_codigoibge
	 */
	public function setNu_codigoibge($nu_codigoibge) {
		$this->nu_codigoibge = ($nu_codigoibge? $nu_codigoibge : null );
	}
	
	/**
	 * @param String $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	/**
	 * @return int
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @param int $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param string $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

}

?>
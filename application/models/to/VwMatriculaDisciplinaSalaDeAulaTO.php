<?php
/**
 * 
 * @author edermariano
 *
 */
class VwMatriculaDisciplinaSalaDeAulaTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;     
	public $bl_ativo;     
	public $id_entidadematricula;     
	public $id_evolucao;     
	public $st_evolucao;     
	public $id_situacao;     
	public $st_situacao;     
	public $id_matriculadisciplina;     
	public $id_disciplina;     
	public $id_saladeaula;
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_entidadematricula
	 */
	public function getId_entidadematricula() {
		return $this->id_entidadematricula;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_matriculadisciplina
	 */
	public function getId_matriculadisciplina() {
		return $this->id_matriculadisciplina;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param $id_matricula the $id_matricula to set
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $id_entidadematricula the $id_entidadematricula to set
	 */
	public function setId_entidadematricula($id_entidadematricula) {
		$this->id_entidadematricula = $id_entidadematricula;
	}

	/**
	 * @param $id_evolucao the $id_evolucao to set
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param $st_evolucao the $st_evolucao to set
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param $id_matriculadisciplina the $id_matriculadisciplina to set
	 */
	public function setId_matriculadisciplina($id_matriculadisciplina) {
		$this->id_matriculadisciplina = $id_matriculadisciplina;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_saladeaula the $id_saladeaula to set
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
}
<?php
/**
 * Classe para encapsular dados da tabela tb_interface  
 * @author Eder Lamar 
 * @package models
 * @subpackage to
 */
class InterfaceTO extends Ead1_TO_Dinamico{
	
	public $id_interface;
	public $st_interface;
	/**
	 * @return the $id_interface
	 */
	public function getId_interface() {
		return $this->id_interface;
	}

	/**
	 * @return the $st_interface
	 */
	public function getSt_interface() {
		return $this->st_interface;
	}

	/**
	 * @param $id_interface the $id_interface to set
	 */
	public function setId_interface($id_interface) {
		$this->id_interface = $id_interface;
	}

	/**
	 * @param $st_interface the $st_interface to set
	 */
	public function setSt_interface($st_interface) {
		$this->st_interface = $st_interface;
	}
}
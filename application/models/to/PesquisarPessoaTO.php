<?php

/**
 * Classe para encapsular os dados de pesquisa de pessoa
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PesquisarPessoaTO extends Ead1_TO_Pesquisar
{

    /**
     * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
     * @var String
     */
    public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pessoa.cadastrar.CadastrarPessoa';

    /**
     * Atributo que contém a data de nascimento
     * @var mixed
     */
    public $dt_nascimento;

    /**
     * Atributo que contém o st sexo
     * @var mixed
     */
    public $st_sexo;

    /**
     * Atributo que contém o nome completo do Usuario
     * @var string
     */
    public $st_nomecompleto;

    /**
     * Atributo que contém o email.
     * @var string
     */
    public $st_email;

    /**
     * Atributo que contém o valor da situação da pessoa
     * @var int
     */
    public $bl_ativo;

    /**
     * Atribuco que contém o id do usuario
     * @var string
     */
    public $id_usuario;

    /**
     * Atribuco que contém o id da entidade
     * @var string
     */
    public $id_entidade;

    /**
     * Atributo que contém o CPF da Pessoa
     */
    public $st_cpf;

    /**
     * Atributo que contém o id do municipio da Pessoa
     */
    public $id_municipio;

    /**
     * Atributo que contém o nome do municipio da Pessoa
     */
    public $st_nomemunicipio;

    /**
     * Atributo que contém a sigla do uf da Pessoa
     */
    public $sg_uf;

    /**
     * Situação
     * @var String
     */
    public $st_situacao;

    /**
     * Situação
     * @var String
     */
    public $id_situacao;

    /**
     * Variavel que indica se a pessoa é maior ou menor de idade
     * Este atributo não se encontra na tabela
     * @var Boolean
     */
    public $bl_menoridade;
    public $st_nomepai;
    public $st_nomemae;

    /**
     * @return unknown
     */
    public function getSt_nomemae ()
    {
        return $this->st_nomemae;
    }

    /**
     * @return unknown
     */
    public function getSt_nomepai ()
    {
        return $this->st_nomepai;
    }

    /**
     * @param unknown_type $st_nomemae
     */
    public function setSt_nomemae ($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
    }

    /**
     * @param unknown_type $st_nomepai
     */
    public function setSt_nomepai ($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
    }

    /**
     * @return the $dt_nascimento
     */
    public function getDt_nascimento ()
    {
        return $this->dt_nascimento;
    }

    /**
     * @return the $st_sexo
     */
    public function getSt_sexo ()
    {
        return $this->st_sexo;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $st_email
     */
    public function getSt_email ()
    {
        return $this->st_email;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $nu_cpf
     */
    public function getSt_cpf ()
    {
        return $this->st_cpf;
    }

    /**
     * @return the $id_municipio
     */
    public function getId_municipio ()
    {
        return $this->id_municipio;
    }

    /**
     * @return the $st_nomemunicipio
     */
    public function getSt_nomemunicipio ()
    {
        return $this->st_nomemunicipio;
    }

    /**
     * @return the $sg_uf
     */
    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @param $dt_nascimento the $dt_nascimento to set
     */
    public function setDt_nascimento ($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @param $st_sexo the $st_sexo to set
     */
    public function setSt_sexo ($st_sexo)
    {
        $this->st_sexo = $st_sexo;
    }

    /**
     * @param $st_nomecompleto the $st_nomecompleto to set
     */
    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param $st_email the $st_email to set
     */
    public function setSt_email ($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @param $bl_ativo the $bl_ativo to set
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param $id_usuario the $id_usuario to set
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param $id_entidade the $id_entidade to set
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param $nu_cpf the $nu_cpf to set
     */
    public function setSt_cpf ($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param $id_municipio the $id_municipio to set
     */
    public function setId_municipio ($id_municipio)
    {
        $this->id_municipio = $id_municipio;
    }

    /**
     * @param $st_nomemunicipio the $st_nomemunicipio to set
     */
    public function setSt_nomemunicipio ($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    /**
     * @param $sg_uf the $sg_uf to set
     */
    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @param $st_situacao the $st_situacao to set
     */
    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @param $id_situacao the $id_situacao to set
     */
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return the $bl_menoridade
     */
    public function getBl_menoridade ()
    {
        return $this->bl_menoridade;
    }

    /**
     * @param Boolean $bl_menoridade
     */
    public function setBl_menoridade ($bl_menoridade)
    {
        $this->bl_menoridade = $bl_menoridade;
    }

}
<?php
/**
 * Classe para encapsular os dados de Produto.
 * 
 * [Quem utiliza]
 * 	@see ProdutoRO
 * 
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 21/02/2011
 * @package models
 * @subpackage to
 */
class PlanoPagamentoTO extends Ead1_TO_Dinamico {

	public $id_planopagamento;
	public $id_produto;
	public $nu_parcelas;
	public $nu_valorentrada;
	public $nu_valorparcela;
	
	
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @return the $id_planopagamento
	 */
	public function getId_planopagamento() {
		return $this->id_planopagamento;
	}

	/**
	 * @return the $nu_valorentrada
	 */
	public function getNu_valorentrada() {
		return $this->nu_valorentrada;
	}

	/**
	 * @return the $nu_valorparcela
	 */
	public function getNu_valorparcela() {
		return $this->nu_valorparcela;
	}


	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $id_planopagamento
	 */
	public function setId_planopagamento($id_planopagamento) {
		$this->id_planopagamento = $id_planopagamento;
	}

	/**
	 * @param field_type $nu_parcelas
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @param field_type $nu_valorentrada
	 */
	public function setNu_valorentrada($nu_valorentrada) {
		$this->nu_valorentrada = $nu_valorentrada;
	}

	/**
	 * @param field_type $nu_valorparcela
	 */
	public function setNu_valorparcela($nu_valorparcela) {
		$this->nu_valorparcela = $nu_valorparcela;
	}


	
}
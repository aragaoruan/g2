<?php

/**
 * @author Elcio Mauro Guimarães
 */
class VwVendaLancamentoQuitadosTO extends Ead1_TO_Dinamico {

	public $id_venda;
	public $id_usuario;
	public $st_nomecompleto;
	public $id_evolucao;
	public $nu_parcelas;
	public $nu_valorliquido;
	public $id_situacao;
	public $nu_quitado;
	public $dt_primeiroquitado;
	public $dt_ultimoquitado;
	public $st_mesanoprimeiroquitado;
	public $st_mesanoultimoquitado;
	
	
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $nu_quitado
	 */
	public function getNu_quitado() {
		return $this->nu_quitado;
	}

	/**
	 * @return the $dt_primeiroquitado
	 */
	public function getDt_primeiroquitado() {
		return $this->dt_primeiroquitado;
	}

	/**
	 * @return the $dt_ultimoquitado
	 */
	public function getDt_ultimoquitado() {
		return $this->dt_ultimoquitado;
	}

	/**
	 * @return the $st_mesanoprimeiroquitado
	 */
	public function getSt_mesanoprimeiroquitado() {
		return $this->st_mesanoprimeiroquitado;
	}

	/**
	 * @return the $st_mesanoultimoquitado
	 */
	public function getSt_mesanoultimoquitado() {
		return $this->st_mesanoultimoquitado;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $nu_parcelas
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @param field_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $nu_quitado
	 */
	public function setNu_quitado($nu_quitado) {
		$this->nu_quitado = $nu_quitado;
	}

	/**
	 * @param field_type $dt_primeiroquitado
	 */
	public function setDt_primeiroquitado($dt_primeiroquitado) {
		$this->dt_primeiroquitado = $dt_primeiroquitado;
	}

	/**
	 * @param field_type $dt_ultimoquitado
	 */
	public function setDt_ultimoquitado($dt_ultimoquitado) {
		$this->dt_ultimoquitado = $dt_ultimoquitado;
	}

	/**
	 * @param field_type $st_mesanoprimeiroquitado
	 */
	public function setSt_mesanoprimeiroquitado($st_mesanoprimeiroquitado) {
		$this->st_mesanoprimeiroquitado = $st_mesanoprimeiroquitado;
	}

	/**
	 * @param field_type $st_mesanoultimoquitado
	 */
	public function setSt_mesanoultimoquitado($st_mesanoultimoquitado) {
		$this->st_mesanoultimoquitado = $st_mesanoultimoquitado;
	}

	
	
}


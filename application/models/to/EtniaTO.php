<?php
/**
 * Classe para encapsular os dados de cor e raça.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EtniaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id de cor e raça.
	 * @var int
	 */
	public $id_etnia;
	
	/**
	 * Contém a descrição da ETNIA.
	 * @var string
	 */
	public $st_etnia;
	
	/**
	 * @return int
	 */
	public function getId_etnia() {
		return $this->id_etnia;
	}
	
	/**
	 * @return string
	 */
	public function getSt_etnia() {
		return $this->st_etnia;
	}
	
	/**
	 * @param int $id_etnia
	 */
	public function setId_etnia($id_etnia) {
		$this->id_etnia = $id_etnia;
	}
	
	/**
	 * @param string $st_etnia
	 */
	public function setSt_etnia($st_etnia) {
		$this->st_etnia = $st_etnia;
	}

}

?>
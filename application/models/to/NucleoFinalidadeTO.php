<?php

/**
 * Classe TO para NucleoFinalidade
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class NucleoFinalidadeTO extends Ead1_TO_Dinamico {
	
	const ACADEMICO = 1;
	const FINANCEIRO = 2;
	const TECNOLOGIA = 3;
	const COMERCIAL = 4;
	const CONTABIL = 5;
	
	public $id_nucleofinalidade;
	public $st_nucleofinalidade;
	
	/**
	 * @return the $id_nucleofinalidade
	 */
	public function getId_nucleofinalidade() {
		return $this->id_nucleofinalidade;
	}

	/**
	 * @return the $st_nucleofinalidade
	 */
	public function getSt_nucleofinalidade() {
		return $this->st_nucleofinalidade;
	}

	/**
	 * @param field_type $id_nucleofinalidade
	 */
	public function setId_nucleofinalidade($id_nucleofinalidade) {
		$this->id_nucleofinalidade = $id_nucleofinalidade;
	}

	/**
	 * @param field_type $st_nucleofinalidade
	 */
	public function setSt_nucleofinalidade($st_nucleofinalidade) {
		$this->st_nucleofinalidade = $st_nucleofinalidade;
	}
}

?>
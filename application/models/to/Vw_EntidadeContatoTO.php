<?php
/**
 * Classe para encapsular os dados da view de contato para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class Vw_EntidadeContatoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id de contato entidade.
	 * @var int
	 */
	public $id_contatoentidade;
	
	/**
	 * Contém o email da entidade.
	 * @var String
	 */
	public $st_email;

	/**
	 * Contém a função do contato da entidade.
	 * @var string
	 */
	public $st_funcao;
	
	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o nome do contato.
	 * @var string
	 */
	public $st_nome;
	
	/**
	 * @return int
	 */
	public function getId_contatoentidade() {
		return $this->id_contatoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return String
	 */
	public function getSt_email() {
		return $this->st_email;
	}
	
	/**
	 * @return string
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nome() {
		return $this->st_nome;
	}
	
	/**
	 * @param int $id_contatoentidade
	 */
	public function setId_contatoentidade($id_contatoentidade) {
		$this->id_contatoentidade = $id_contatoentidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param String $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}
	
	/**
	 * @param string $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @param string $st_nome
	 */
	public function setSt_nome($st_nome) {
		$this->st_nome = $st_nome;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de tipo de fundamento legal.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoFundamentoLegalTO extends Ead1_TO_Dinamico {
	
	const LEI = 1;
	const PORTARIA = 2;
	const RESOLUCAO = 3;
	const PARECER = 4;

	/**
	 * id do tipo de fundamento legal.
	 * @var int
	 */
	public $id_tipofundamentolegal;
	
	/**
	 * Tipo de fundamento legal.
	 * @var string
	 */
	public $st_tipofundamentolegal;
	
	/**
	 * @return int
	 */
	public function getId_tipofundamentolegal() {
		return $this->id_tipofundamentolegal;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipofundamentolegal() {
		return $this->st_tipofundamentolegal;
	}
	
	/**
	 * @param int $id_tipofundamentolegal
	 */
	public function setId_tipofundamentolegal($id_tipofundamentolegal) {
		$this->id_tipofundamentolegal = $id_tipofundamentolegal;
	}
	
	/**
	 * @param string $st_tipofundamentolegal
	 */
	public function setSt_tipofundamentolegal($st_tipofundamentolegal) {
		$this->st_tipofundamentolegal = $st_tipofundamentolegal;
	}

}

?>
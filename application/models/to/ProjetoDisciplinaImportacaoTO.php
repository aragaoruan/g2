<?php

/**
 * Classe de valor da tabela tb_projetodisciplinaimportacao
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */

class ProjetoDisciplinaImportacaoTO extends Ead1_TO_Dinamico {
	
	public $id_projetodisciplinaimportacao;
	public $id_disciplina;
	public $id_sistemaimportacao;

	public $id_projetopedagogico;
	public $st_disciplinaorigem;
	public $st_projetoorigem;
	public $st_areaoriginal;
	
	public $nu_codprojetoorigem;
	public $nu_coddisciplinaorigem;
	public $nu_codareaorigem;
	
	
	
}
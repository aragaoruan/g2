<?php
class PesquisaAvaliacaoAgendamentoTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_usuario;
	public $st_nomecompleto;
	public $st_avaliacao;
	public $id_tipoprova;
	public $st_tipoprova;
	public $dt_agendamento;
	public $id_situacao;
	public $st_situacao;
	public $id_avaliacaoagendamento;
	
	//Filtro de pesquisa entre datas
	public $dt_agendamentoinicio;
	public $dt_agendamentofim;
	//-----------------------------
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.CadastrarAgendamentoAvaliacao';
	
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){

		return $this->id_matricula;
	}

	/**
	 * @param $id_matricula the $id_matricula to set
	 */
	public function setId_matricula( $id_matricula ){

		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $st_tipoprova
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}

	/**
	 * @return the $dt_agendamento
	 */
	public function getDt_agendamento() {
		return $this->dt_agendamento;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $dt_agendamentoinicio
	 */
	public function getDt_agendamentoinicio() {
		return $this->dt_agendamentoinicio;
	}

	/**
	 * @return the $dt_agendamentofim
	 */
	public function getDt_agendamentofim() {
		return $this->dt_agendamentofim;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $st_tipoprova
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

	/**
	 * @param field_type $dt_agendamento
	 */
	public function setDt_agendamento($dt_agendamento) {
		$this->dt_agendamento = $dt_agendamento;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $dt_agendamentoinicio
	 */
	public function setDt_agendamentoinicio($dt_agendamentoinicio) {
		$this->dt_agendamentoinicio = $dt_agendamentoinicio;
	}

	/**
	 * @param field_type $dt_agendamentofim
	 */
	public function setDt_agendamentofim($dt_agendamentofim) {
		$this->dt_agendamentofim = $dt_agendamentofim;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_avaliacaoagendamento() {
		return $this->id_avaliacaoagendamento;
	}
	
	/**
	 * @return String
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}
	
	/**
	 * @param unknown_type $id_avaliacaoagendamento
	 */
	public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}
	
	/**
	 * @param String $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}


	
}
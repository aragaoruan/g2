<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class ProdutoAvaliacaoTO extends Ead1_TO_Dinamico{

	public $id_entidade;
	public $id_avaliacao;
	public $id_produto;


	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao(){ 
		return $this->id_avaliacao;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto(){ 
		return $this->id_produto;
	}


	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao){ 
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto){ 
		$this->id_produto = $id_produto;
	}

}
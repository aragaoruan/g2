<?php
/**
 * Classe para encapsular os dados de divisão pedagógico a que o perfil se refere.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PerfilPedagogicoTO extends Ead1_TO_Dinamico {
	
	const PROFESSOR = 1;
	const COORDENADOR_DE_PROJETO_PEDAGOGICO = 2;
	const COORDENADOR_DE_AREA = 3;
	const COORDENADOR_DE_DISCIPLINA = 4;
	const ALUNO = 5;
	const ALUNO_INSTITUCIONAL = 6;
	const SUPORTE = 7;
    const TITULAR_CERTIFICACAO = 9;
	

	/**
	 * Id da divisão pedagógico a que o perfil se refere.
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * Nome da divisão pedagógico a que o perfil se refere.
	 * @var String
	 */
	public $st_perfilpedagogico;
	
	/**
	 * @return int
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}
	
	/**
	 * @return String
	 */
	public function getSt_perfilpedagogico() {
		return $this->st_perfilpedagogico;
	}
	
	/**
	 * @param int $id_perfilpedagogico
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}
	
	/**
	 * @param String $st_perfilpedagogico
	 */
	public function setSt_perfilpedagogico($st_perfilpedagogico) {
		$this->st_perfilpedagogico = $st_perfilpedagogico;
	}

}
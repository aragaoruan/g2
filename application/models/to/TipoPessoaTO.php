<?php
/**
 * Classe para encapsular os dados do tipo da pessoa
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class TipoPessoaTO extends Ead1_TO_Dinamico {

	/**
	 * Constantes
	 * 1	PF
	 * 2	PJ
	 */
	const PF = 1;
	const PJ = 2;
	
	public $id_tipopessoa;
	public $st_tipopessoa;
	
	/**
	 * @return the $id_tipopessoa
	 */
	public function getId_tipopessoa() {
		return $this->id_tipopessoa;
	}

	/**
	 * @param field_type $id_tipopessoa
	 */
	public function setId_tipopessoa($id_tipopessoa) {
		$this->id_tipopessoa = $id_tipopessoa;
	}

	/**
	 * @return the $st_tipopessoa
	 */
	public function getSt_tipopessoa() {
		return $this->st_tipopessoa;
	}

	/**
	 * @param field_type $st_tipopessoa
	 */
	public function setSt_tipopessoa($st_tipopessoa) {
		$this->st_tipopessoa = $st_tipopessoa;
	}
}

?>
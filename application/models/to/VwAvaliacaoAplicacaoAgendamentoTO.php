<?php
/**
 * Classe que encapsula dados do de lancamento de uma venda
 * @autor Eder Lamar
 * @package models
 * @subpackage to
 */
class VwAvaliacaoAplicacaoAgendamentoTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_tipoprova;
	public $id_avaliacao;
	public $st_avaliacao;
	public $st_tituloexibicao;
	public $st_evolucao;
	public $st_disciplina;
	public $hr_inicio;
	public $hr_fim;
	public $bl_unica;
	public $dt_aplicacao;
	public $id_horarioaula;
	public $id_avaliacaoconjuntoreferencia;
	public $id_avaliacaoaplicacao;
	public $id_avaliacaoagendamento;
	public $st_nomeentidade;
	public $id_situacao;
	public $st_situacao;
	public $dt_agendamento;
	
	/**
	 * @return unknown
	 */
	public function getDt_agendamento() {
		return $this->dt_agendamento;
	}
	
	/**
	 * @param unknown_type $dt_agendamento
	 */
	public function setDt_agendamento($dt_agendamento) {
		$this->dt_agendamento = $dt_agendamento;
	}

	
	/**
	 * @return unknown
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param unknown_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	
	/**
	 * @return unknown
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @param unknown_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_avaliacaoagendamento() {
		return $this->id_avaliacaoagendamento;
	}
	
	/**
	 * @param unknown_type $id_avaliacaoagendamento
	 */
	public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}

	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $hr_inicio
	 */
	public function getHr_inicio() {
		return $this->hr_inicio;
	}

	/**
	 * @return the $hr_fim
	 */
	public function getHr_fim() {
		return $this->hr_fim;
	}

	/**
	 * @return the $bl_unica
	 */
	public function getBl_unica() {
		return $this->bl_unica;
	}

	/**
	 * @return the $dt_aplicacao
	 */
	public function getDt_aplicacao() {
		return $this->dt_aplicacao;
	}

	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
	public function getId_avaliacaoconjuntoreferencia() {
		return $this->id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $hr_inicio
	 */
	public function setHr_inicio($hr_inicio) {
		$this->hr_inicio = $hr_inicio;
	}

	/**
	 * @param field_type $hr_fim
	 */
	public function setHr_fim($hr_fim) {
		$this->hr_fim = $hr_fim;
	}

	/**
	 * @param field_type $bl_unica
	 */
	public function setBl_unica($bl_unica) {
		$this->bl_unica = $bl_unica;
	}

	/**
	 * @param field_type $dt_aplicacao
	 */
	public function setDt_aplicacao($dt_aplicacao) {
		$this->dt_aplicacao = $dt_aplicacao;
	}

	/**
	 * @param field_type $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoreferencia
	 */
	public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}
	/**
	 * @return the $id_avaliacaoaplicacao
	 */
	public function getId_avaliacaoaplicacao() {
		return $this->id_avaliacaoaplicacao;
	}

	/**
	 * @param field_type $id_avaliacaoaplicacao
	 */
	public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
		$this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
	}


	
}
<?php

/**
 * Classe To para NucleoAssunto
 * @package models
 * @subpackage to
 *
 */
class NucleoAssuntoCoTO extends Ead1_TO_Dinamico {
	
	public $id_nucleoassuntoco;
	public $id_nucleoco;
	public $id_assuntoco;
	public $id_usuariocadastro;
	public $dt_cadastro;
	public $bl_ativo;
	/**
	 * @return the $id_nucleoassuntoco
	 */
	public function getId_nucleoassuntoco() {
		return $this->id_nucleoassuntoco;
	}

	/**
	 * @param field_type $id_nucleoassuntoco
	 */
	public function setId_nucleoassuntoco($id_nucleoassuntoco) {
		$this->id_nucleoassuntoco = $id_nucleoassuntoco;
	}

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @param field_type $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
	
	
}

?>
<?php
/**
 * Classe para encapsular a VWNucleoPessoaCo
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwNucleoPessoaCoTO extends Ead1_TO_Dinamico {
	
	const FUNCAO_RESPONSAVEL = 9;
	const FUNCAO_ATENDENTE = 8;
	const FUNCAO_ATENDENTE_SETOR = 10;
	
	public $id_nucleopessoaco;
	public $id_nucleoco;
	public $id_assuntoco;
	public $id_usuario;
	public $id_funcao;
	public $id_usuariocadastro;
	public $dt_cadastro;
	public $bl_prioritario;
	public $st_nomecompleto;
	public $st_funcao;
	public $id_entidade;
	public $st_assunto;
	public $id_nucleofinalidade;
	public $st_nucleofinalidade;
	
	/**
	 * @return the $id_nucleopessoaco
	 */
	public function getId_nucleopessoaco() {
		return $this->id_nucleopessoaco;
	}

	/**
	 * @param field_type $id_nucleopessoaco
	 */
	public function setId_nucleopessoaco($id_nucleopessoaco) {
		$this->id_nucleopessoaco = $id_nucleopessoaco;
	}

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	/**
     * @return integer $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
     * @param int $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $bl_prioritario
	 */
	public function getBl_prioritario() {
		return $this->bl_prioritario;
	}

	/**
	 * @param field_type $bl_prioritario
	 */
	public function setBl_prioritario($bl_prioritario) {
		$this->bl_prioritario = $bl_prioritario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $st_funcao
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}

	/**
	 * @param field_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	/**
     * @return integer $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
     * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return the $st_assunto
	 */
	public function getSt_assunto() {
		return $this->st_assunto;
	}

	/**
	 * @param field_type $st_assunto
	 */
	public function setSt_assunto($st_assunto) {
		$this->st_assunto = $st_assunto;
	}
	/**
	 * @return the $id_nucleofinalidade
	 */
	public function getId_nucleofinalidade() {
		return $this->id_nucleofinalidade;
	}

	/**
	 * @return the $st_nucleofinalidade
	 */
	public function getSt_nucleofinalidade() {
		return $this->st_nucleofinalidade;
	}

	/**
	 * @param field_type $id_nucleofinalidade
	 */
	public function setId_nucleofinalidade($id_nucleofinalidade) {
		$this->id_nucleofinalidade = $id_nucleofinalidade;
	}

	/**
	 * @param field_type $st_nucleofinalidade
	 */
	public function setSt_nucleofinalidade($st_nucleofinalidade) {
		$this->st_nucleofinalidade = $st_nucleofinalidade;
	}

}

?>
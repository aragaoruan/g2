<?php
/**
 * Classe para encapsular os dados de tipo de disciplina extracurricular.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoExtracurricularTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de disiciplina extracurricular.
	 * @var int
	 */
	public $id_tipoextracurricular;
	
	/**
	 * Nome do tipo de disciplina extracurricular.
	 * @var string
	 */
	public $st_tipoextracurricular;
	
	/**
	 * Descrição do tipo de disciplina extracurricular.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tipoextracurricular() {
		return $this->id_tipoextracurricular;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoextracurricular() {
		return $this->st_tipoextracurricular;
	}
	
	/**
	 * @param int $id_tipoextracurricular
	 */
	public function setId_tipoextracurricular($id_tipoextracurricular) {
		$this->id_tipoextracurricular = $id_tipoextracurricular;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipoextracurricular
	 */
	public function setSt_tipoextracurricular($st_tipoextracurricular) {
		$this->st_tipoextracurricular = $st_tipoextracurricular;
	}

}

?>
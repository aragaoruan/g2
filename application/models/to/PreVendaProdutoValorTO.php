<?php
/**
 * Classe para encapsular os dados de pre venda com valor de produto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class PreVendaProdutoValorTO extends Ead1_TO_Dinamico {

	public $id_prevenda;
	public $id_prevendaprodutovalor;
	public $id_produtovalor;
	
	/**
	 * @return unknown
	 */
	public function getId_prevenda() {
		return $this->id_prevenda;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_prevendaprodutovalor() {
		return $this->id_prevendaprodutovalor;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produtovalor() {
		return $this->id_produtovalor;
	}
	
	/**
	 * @param unknown_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda) {
		$this->id_prevenda = $id_prevenda;
	}
	
	/**
	 * @param unknown_type $id_prevendaprodutovalor
	 */
	public function setId_prevendaprodutovalor($id_prevendaprodutovalor) {
		$this->id_prevendaprodutovalor = $id_prevendaprodutovalor;
	}
	
	/**
	 * @param unknown_type $id_produtovalor
	 */
	public function setId_produtovalor($id_produtovalor) {
		$this->id_produtovalor = $id_produtovalor;
	}

}

?>
<?php

/**
 * TO para pesquisa de grade horaria
 * Class PesquisarGradeTO
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-03
 */
class PesquisarGradeTO extends Ead1_TO_Dinamico
{
    public $id_gradehoraria;
    public $bl_ativo;
    public $st_nomegradehoraria;
    public $dt_iniciogradehoraria;
    public $dt_fimgradehoraria;
    public $dt_cadastro;
    public $id_situacao;
    public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.GradeHoraria';
} 
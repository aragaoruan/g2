<?php
/**
 * Classe para encapsular os dados da view de área de conhecimento com série e nível de ensino.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwAreaConhecimentoNivelSerieTO extends Ead1_TO_Dinamico {

	/**
	 * Id da área.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Nome da área.
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Descrição da área.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Exclusão lógica.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Nome do nível de ensino.
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Id da série anterior.
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Nome da série.
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Situação
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return int
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param int $id_serieanterior
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}
	
	/**
	 * @param string $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param string $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

}

?>
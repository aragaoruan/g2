<?php
/**
 * Classe para encapsular os dados da view de dependência da disciplina de módulo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwDepedenciaDisciplinaModuloTO extends Ead1_TO_Dinamico {

	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id do módulo.
	 * @var int
	 */
	public $id_modulo;
	
	/**
	 * Id da disciplina pré-requisito.
	 * @var int
	 */
	public $id_disciplinaprerequisito;
	
	/**
	 * Nome da disciplina do pré requisito
	 * @var String
	 */
	public $st_disciplinaprerequisito;
	
	/**
	 * Id da trilha.
	 * @var int
	 */
	public $id_trilha;
	
	/**
	 * Id do tipo de trilha.
	 * @var int
	 */
	public $id_tipotrilha;
	
	/**
	 * Disciplinas simultâneas para menores de 18 anos.
	 * @var int
	 */
	public $nu_disciplinassimultaneasmen;
	
	/**
	 * Disciplinas simultâneas para maiores de 18 anos.
	 * @var int
	 */
	public $nu_disciplinassimultaneasmai;
	
	/**
	 * Id do tipo de trilha fixa.
	 * @var int
	 */
	public $id_tipotrilhafixa;
	
	/**
	 * Descrição do tipo de trilha fixa.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Tipo de trilha.
	 * @var string
	 */
	public $st_tipotrilha;
	
	/**
	 * Nome nivel ensino
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Id da entidade cadastro.
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * id do módulo anterior.
	 * @var int
	 */
	public $id_moduloanterior;
	
	/**
	 * Nome do módulo.
	 * @var string
	 */
	public $st_modulo;
	
	/**
	 * Nome do módulo anterior.
	 * @var string
	 */
	public $st_moduloanterior;
	
	/**
	 * Nome da disciplina.
	 * @var string
	 */
	public $st_disciplina;
	
	/**
	 * Série
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $id_disciplinaprerequisito
	 */
	public function getId_disciplinaprerequisito() {
		return $this->id_disciplinaprerequisito;
	}

	/**
	 * @return the $st_disciplinaprerequisito
	 */
	public function getSt_disciplinaprerequisito() {
		return $this->st_disciplinaprerequisito;
	}

	/**
	 * @return the $id_trilha
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}

	/**
	 * @return the $id_tipotrilha
	 */
	public function getId_tipotrilha() {
		return $this->id_tipotrilha;
	}

	/**
	 * @return the $nu_disciplinassimultaneasmen
	 */
	public function getNu_disciplinassimultaneasmen() {
		return $this->nu_disciplinassimultaneasmen;
	}

	/**
	 * @return the $nu_disciplinassimultaneasmai
	 */
	public function getNu_disciplinassimultaneasmai() {
		return $this->nu_disciplinassimultaneasmai;
	}

	/**
	 * @return the $id_tipotrilhafixa
	 */
	public function getId_tipotrilhafixa() {
		return $this->id_tipotrilhafixa;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_tipotrilha
	 */
	public function getSt_tipotrilha() {
		return $this->st_tipotrilha;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_moduloanterior
	 */
	public function getId_moduloanterior() {
		return $this->id_moduloanterior;
	}

	/**
	 * @return the $st_modulo
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}

	/**
	 * @return the $st_moduloanterior
	 */
	public function getSt_moduloanterior() {
		return $this->st_moduloanterior;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_modulo the $id_modulo to set
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param $id_disciplinaprerequisito the $id_disciplinaprerequisito to set
	 */
	public function setId_disciplinaprerequisito($id_disciplinaprerequisito) {
		$this->id_disciplinaprerequisito = $id_disciplinaprerequisito;
	}

	/**
	 * @param $st_disciplinaprerequisito the $st_disciplinaprerequisito to set
	 */
	public function setSt_disciplinaprerequisito($st_disciplinaprerequisito) {
		$this->st_disciplinaprerequisito = $st_disciplinaprerequisito;
	}

	/**
	 * @param $id_trilha the $id_trilha to set
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}

	/**
	 * @param $id_tipotrilha the $id_tipotrilha to set
	 */
	public function setId_tipotrilha($id_tipotrilha) {
		$this->id_tipotrilha = $id_tipotrilha;
	}

	/**
	 * @param $nu_disciplinassimultaneasmen the $nu_disciplinassimultaneasmen to set
	 */
	public function setNu_disciplinassimultaneasmen($nu_disciplinassimultaneasmen) {
		$this->nu_disciplinassimultaneasmen = $nu_disciplinassimultaneasmen;
	}

	/**
	 * @param $nu_disciplinassimultaneasmai the $nu_disciplinassimultaneasmai to set
	 */
	public function setNu_disciplinassimultaneasmai($nu_disciplinassimultaneasmai) {
		$this->nu_disciplinassimultaneasmai = $nu_disciplinassimultaneasmai;
	}

	/**
	 * @param $id_tipotrilhafixa the $id_tipotrilhafixa to set
	 */
	public function setId_tipotrilhafixa($id_tipotrilhafixa) {
		$this->id_tipotrilhafixa = $id_tipotrilhafixa;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $st_tipotrilha the $st_tipotrilha to set
	 */
	public function setSt_tipotrilha($st_tipotrilha) {
		$this->st_tipotrilha = $st_tipotrilha;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $id_entidadecadastro the $id_entidadecadastro to set
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param $id_moduloanterior the $id_moduloanterior to set
	 */
	public function setId_moduloanterior($id_moduloanterior) {
		$this->id_moduloanterior = $id_moduloanterior;
	}

	/**
	 * @param $st_modulo the $st_modulo to set
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}

	/**
	 * @param $st_moduloanterior the $st_moduloanterior to set
	 */
	public function setSt_moduloanterior($st_moduloanterior) {
		$this->st_moduloanterior = $st_moduloanterior;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	
		
	
}

?>
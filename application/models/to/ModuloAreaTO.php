<?php
/**
 * @author Arthur Cláudio 
 * @package models
 * @subpackage to
 */
class ModuloAreaTO extends Ead1_TO_Dinamico {
	
	public $id_moduloarea		= 0;
	public $id_modulo			= 0 ;
	public $id_areaconhecimento	= 0;
	
	/**
	 * @return the $id_moduloarea
	 */
	public function getId_moduloarea() {
		return $this->id_moduloarea;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @param $id_moduloarea the $id_moduloarea to set
	 */
	public function setId_moduloarea($id_moduloarea) {
		$this->id_moduloarea = $id_moduloarea;
	}

	/**
	 * @param $id_modulo the $id_modulo to set
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	
	
}
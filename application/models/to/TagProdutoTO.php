<?php
/**
 * Classe para encapsular os dados de tag de produto
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 13/12/2012
 * 
 * @package models
 * @subpackage to
 */
class TagProdutoTO extends Ead1_TO_Dinamico {

	/**
	 * Chave primária da tabela.
	 * @var int 
	 */
	public $id_produto;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_tag;
	
	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $dt_cadastro;
	
	/**
	 * @return int
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @param int $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}
	/**
	 * @return int
	 */
	public function getId_tag() {
		return $this->id_tag;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @param int $id_tag
	 */
	public function setId_tag($id_tag) {
		$this->id_tag = $id_tag;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param int $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de view vw_disciplinaareaconhecimentonivelserie
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwAreaConhecimentoDisciplinaSerieNivelTO extends Ead1_TO_Dinamico {
	
	/**
	 * Variavel que contem o id da disciplina
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Variavel que contem o nome da disciplina
	 * @var string
	 */
	public $st_disciplina;
	
	/**
	 * Variavel que comtem a descrição da disciplina
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Variavel que contem o id da area de conhecimento
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Variavel que contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Variavel que contem o nome da area de conhecimento
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Variavel que contem a situacao da disciplina
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Variavel que contem o nome do nivel de ensino
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * Variavel que contem o id do nivel de ensino
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Variavel que contem o id da serie
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Variavel que contem o nome da serie
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Variavel que contem o id da serie anterior
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Variavel que contem o id do tipo da disciplina
	 * @var int
	 */
	public $id_tipodisciplina;
	
	/**
	 * Variavel que contem o nome do tipo da disciplina
	 * @var string
	 */
	public $st_tipodisciplina;
	
	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @return the $id_serieanterior
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @return the $st_tipodisciplina
	 */
	public function getSt_tipodisciplina() {
		return $this->st_tipodisciplina;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $st_areaconhecimento the $st_areaconhecimento to set
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @param $id_serieanterior the $id_serieanterior to set
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}

	/**
	 * @param $id_tipodisciplina the $id_tipodisciplina to set
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @param $st_tipodisciplina the $st_tipodisciplina to set
	 */
	public function setSt_tipodisciplina($st_tipodisciplina) {
		$this->st_tipodisciplina = $st_tipodisciplina;
	}

	
	
}
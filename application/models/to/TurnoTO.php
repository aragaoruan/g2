<?php
/**
 * Classe para encapsular os dados de turno.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TurnoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do turno.
	 * @var int
	 */
	public $id_turno;
	
	/**
	 * Nome do turno.
	 * @var string
	 */
	public $st_turno;
	
	/**
	 * @return int
	 */
	public function getId_turno() {
		return $this->id_turno;
	}
	
	/**
	 * @return string
	 */
	public function getSt_turno() {
		return $this->st_turno;
	}
	
	/**
	 * @param int $id_turno
	 */
	public function setId_turno($id_turno) {
		$this->id_turno = $id_turno;
	}
	
	/**
	 * @param string $st_turno
	 */
	public function setSt_turno($st_turno) {
		$this->st_turno = $st_turno;
	}

}

?>
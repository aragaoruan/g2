<?php
/**
 * Classe para encapsular os dados de relacionamento entre projeto pedagógico, série e nível de ensino.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoPedagogicoSerieNivelEnsinoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do projetopedagogico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Contém o id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Contém o id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

}

?>
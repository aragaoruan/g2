<?php
/** 
 * Classe para encapsular os dados da tabela de avaliação agendamento na integração
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class AvalAgendaIntegracaoTO extends Ead1_TO_Dinamico{
	
	public $id_agendamentointegracao;
	public $id_avaliacaoagendamento;
	public $id_usuariocadastro;
	public $id_sistema;
	public $st_codsistema;
	public $dt_cadastro;
	
	/**
	 * @return the $id_agendamentointegracao
	 */
	public function getId_agendamentointegracao() {
		return $this->id_agendamentointegracao;
	}

	/**
	 * @param field_type $id_agendamentointegracao
	 */
	public function setId_agendamentointegracao($id_agendamentointegracao) {
		$this->id_agendamentointegracao = $id_agendamentointegracao;
	}

	/**
	 * @return the $id_avaliacaoagendamento
	 */
	public function getId_avaliacaoagendamento() {
		return $this->id_avaliacaoagendamento;
	}

	/**
	 * @param field_type $id_avaliacaoagendamento
	 */
	public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $st_codsistema
	 */
	public function getSt_codsistema() {
		return $this->st_codsistema;
	}

	/**
	 * @param field_type $st_codsistema
	 */
	public function setSt_codsistema($st_codsistema) {
		$this->st_codsistema = $st_codsistema;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
}
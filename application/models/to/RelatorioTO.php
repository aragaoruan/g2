<?php
/** 
 * Classe para encapsular os dados da tabela de relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class RelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_relatorio;
	public $id_entidade;
	public $id_usuario;
	public $id_tipoorigemrelatorio;
	public $id_funcionalidade;
	public $st_relatorio;
	public $st_origem;
	public $bl_geral;
	public $st_descricao;
	public $dt_cadastro;
	
	/**
	 * @return the $id_relatorio
	 */
	public function getId_relatorio() {
		return $this->id_relatorio;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_tipoorigemrelatorio
	 */
	public function getId_tipoorigemrelatorio() {
		return $this->id_tipoorigemrelatorio;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $st_relatorio
	 */
	public function getSt_relatorio() {
		return $this->st_relatorio;
	}

	/**
	 * @return the $st_origem
	 */
	public function getSt_origem() {
		return $this->st_origem;
	}

	/**
	 * @return the $bl_geral
	 */
	public function getBl_geral() {
		return $this->bl_geral;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param $id_relatorio the $id_relatorio to set
	 */
	public function setId_relatorio($id_relatorio) {
		$this->id_relatorio = $id_relatorio;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_tipoorigemrelatorio the $id_tipoorigemrelatorio to set
	 */
	public function setId_tipoorigemrelatorio($id_tipoorigemrelatorio) {
		$this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
	}

	/**
	 * @param $id_funcionalidade the $id_funcionalidade to set
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param $st_relatorio the $st_relatorio to set
	 */
	public function setSt_relatorio($st_relatorio) {
		$this->st_relatorio = $st_relatorio;
	}

	/**
	 * @param $st_origem the $st_origem to set
	 */
	public function setSt_origem($st_origem) {
		$this->st_origem = $st_origem;
	}

	/**
	 * @param $bl_geral the $bl_geral to set
	 */
	public function setBl_geral($bl_geral) {
		$this->bl_geral = $bl_geral;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
	
}
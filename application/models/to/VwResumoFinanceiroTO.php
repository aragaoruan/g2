<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwResumoFinanceiroTO extends Ead1_TO_Dinamico {

    public $id_lancamento;
    public $id_usuariolancamento;
    public $id_entidadelancamento;
    public $id_venda;
    public $id_acordo;
    public $bl_entrada;
    public $st_banco;
    public $st_emissor;
    public $dt_prevquitado;
    public $st_coddocumento;
    public $st_endereco;
    public $st_cidade;
    public $st_bairro;
    public $st_estadoprovincia;
    public $st_cep;
    public $bl_quitado;
    public $st_entradaparcela;
    public $id_meiopagamento;
    public $st_meiopagamento;
    public $nu_valor;
    public $nu_vencimento;
    public $dt_vencimento;
    public $st_situacao;
    public $id_usuario;
    public $st_nomecompleto;
    public $id_entidade;
    public $dt_quitado;
    public $nu_quitado;
    public $nu_parcela;
    public $id_textosistemarecibo;
    public $id_reciboconsolidado;
    public $st_nossonumero;
    public $st_codtransacaooperadora;
    public $bl_original;
    public $bl_ativo;
    public $nu_jurosboleto;
    public $nu_descontoboleto;
    public $st_codigobarras;
    public $st_urlboleto;
    public $id_transacaoexterna;

    /**
     * @return mixed
     */
    public function getid_transacaoexterna()
    {
        return $this->id_transacaoexterna;
    }

    /**
     * @param mixed $id_transacaoexterna
     */
    public function setid_transacaoexterna($id_transacaoexterna)
    {
        $this->id_transacaoexterna = $id_transacaoexterna;
        return $this;

    }


    /**
     * @return mixed
     */
    public function getst_urlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param mixed $st_urlboleto
     */
    public function setst_urlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_codigobarras()
    {
        return $this->st_codigobarras;
    }

    /**
     * @param mixed $st_codigobarras
     */
    public function setst_codigobarras($st_codigobarras)
    {
        $this->st_codigobarras = $st_codigobarras;
        return $this;

    }

    /**
     * @return the $id_textosistemarecibo
     */
    public function getId_textosistemarecibo() {
        return $this->id_textosistemarecibo;
    }

    /**
     * @return the $id_reciboconsolidado
     */
    public function getId_reciboconsolidado() {
        return $this->id_reciboconsolidado;
    }

    /**
     * @param field_type $id_textosistemarecibo
     */
    public function setId_textosistemarecibo($id_textosistemarecibo) {
        $this->id_textosistemarecibo = $id_textosistemarecibo;
    }

    /**
     * @param field_type $id_reciboconsolidado
     */
    public function setId_reciboconsolidado($id_reciboconsolidado) {
        $this->id_reciboconsolidado = $id_reciboconsolidado;
    }

    /**
     * @return the $nu_parcela
     */
    public function getNu_parcela() {
        return $this->nu_parcela;
    }

    /**
     * @param field_type $nu_parcela
     */
    public function setNu_parcela($nu_parcela) {
        $this->nu_parcela = $nu_parcela;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @return the $id_acordo
     */
    public function getId_acordo() {
        return $this->id_acordo;
    }

    /**
     * @return the $bl_entrada
     */
    public function getBl_entrada() {
        return $this->bl_entrada;
    }

    /**
     * @return the $st_banco
     */
    public function getSt_banco() {
        return $this->st_banco;
    }

    /**
     * @return the $st_emissor
     */
    public function getSt_emissor() {
        return $this->st_emissor;
    }

    /**
     * @return the $dt_prevquitado
     */
    public function getDt_prevquitado() {
        return $this->dt_prevquitado;
    }

    /**
     * @return the $st_coddocumento
     */
    public function getSt_coddocumento() {
        return $this->st_coddocumento;
    }

    /**
     * @return the $st_endereco
     */
    public function getSt_endereco() {
        return $this->st_endereco;
    }

    /**
     * @return the $st_cidade
     */
    public function getSt_cidade() {
        return $this->st_cidade;
    }

    /**
     * @return the $st_bairro
     */
    public function getSt_bairro() {
        return $this->st_bairro;
    }

    /**
     * @return the $st_estadoprovincia
     */
    public function getSt_estadoprovincia() {
        return $this->st_estadoprovincia;
    }

    /**
     * @return the $st_cep
     */
    public function getSt_cep() {
        return $this->st_cep;
    }

    /**
     * @return the $bl_quitado
     */
    public function getBl_quitado() {
        return $this->bl_quitado;
    }

    /**
     * @return the $st_entradaparcela
     */
    public function getSt_entradaparcela() {
        return $this->st_entradaparcela;
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento() {
        return $this->id_meiopagamento;
    }

    /**
     * @return the $st_meiopagamento
     */
    public function getSt_meiopagamento() {
        return $this->st_meiopagamento;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @return the $nu_vencimento
     */
    public function getNu_vencimento() {
        return $this->nu_vencimento;
    }

    /**
     * @return the $dt_vencimento
     */
    public function getDt_vencimento() {
        return $this->dt_vencimento;
    }

    /**
     * @return the $dt_quitado
     */
    public function getDt_quitado() {
        return $this->dt_quitado;
    }

    /**
     * @return the $nu_quitado
     */
    public function getNu_quitado() {
        return $this->nu_quitado;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param field_type $id_lancamento
     */
    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param field_type $id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

    /**
     * @param field_type $id_acordo
     */
    public function setId_acordo($id_acordo) {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @param field_type $bl_entrada
     */
    public function setBl_entrada($bl_entrada) {
        $this->bl_entrada = $bl_entrada;
    }

    /**
     * @param field_type $st_banco
     */
    public function setSt_banco($st_banco) {
        $this->st_banco = $st_banco;
    }

    /**
     * @param field_type $st_emissor
     */
    public function setSt_emissor($st_emissor) {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @param field_type $dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado) {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param field_type $st_coddocumento
     */
    public function setSt_coddocumento($st_coddocumento) {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @param field_type $st_endereco
     */
    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @param field_type $st_cidade
     */
    public function setSt_cidade($st_cidade) {
        $this->st_cidade = $st_cidade;
    }

    /**
     * @param field_type $st_bairro
     */
    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @param field_type $st_estadoprovincia
     */
    public function setSt_estadoprovincia($st_estadoprovincia) {
        $this->st_estadoprovincia = $st_estadoprovincia;
    }

    /**
     * @param field_type $st_cep
     */
    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
    }

    /**
     * @param field_type $bl_quitado
     */
    public function setBl_quitado($bl_quitado) {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param field_type $st_entradaparcela
     */
    public function setSt_entradaparcela($st_entradaparcela) {
        $this->st_entradaparcela = $st_entradaparcela;
    }

    /**
     * @param field_type $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento) {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param field_type $st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento) {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @param field_type $nu_valor
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param field_type $nu_vencimento
     */
    public function setNu_vencimento($nu_vencimento) {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @param field_type $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento) {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param field_type $st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $dt_quitado
     */
    public function setDt_quitado($dt_quitado) {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param field_type $nu_quitado
     */
    public function setNu_quitado($nu_quitado) {
        $this->nu_quitado = $nu_quitado;
    }

    public function getSt_nossonumero() {
        return $this->st_nossonumero;
    }

    public function getSt_codtransacaooperadora() {
        return $this->st_codtransacaooperadora;
    }

    public function setSt_nossonumero($st_nossonumero) {
        $this->st_nossonumero = $st_nossonumero;
    }

    public function setSt_codtransacaooperadora($st_codtransacaooperadora) {
        $this->st_codtransacaooperadora = $st_codtransacaooperadora;
    }

    public function getBl_original() {
        return $this->bl_original;
    }

    public function setBl_original($bl_original) {
        $this->bl_original = $bl_original;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param mixed $nu_descontoboleto
     */
    public function setNu_descontoboleto($nu_descontoboleto)
    {
        $this->nu_descontoboleto = $nu_descontoboleto;
    }

    /**
     * @return mixed
     */
    public function getNu_descontoboleto()
    {
        return $this->nu_descontoboleto;
    }

    /**
     * @param mixed $nu_jurosboleto
     */
    public function setNu_jurosboleto($nu_jurosboleto)
    {
        $this->nu_jurosboleto = $nu_jurosboleto;
    }

    /**
     * @return mixed
     */
    public function getNu_jurosboleto()
    {
        return $this->nu_jurosboleto;
    }

    /**
     * @return mixed
     */
    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @param mixed $id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @return mixed
     */
    public function getId_entidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @param mixed $id_entidadelancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }



}

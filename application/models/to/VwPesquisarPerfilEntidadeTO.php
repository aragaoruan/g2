<?php
/**
 * Classe para encapsular os dados da view de pesquisa de perfil para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPesquisarPerfilEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Id do perfil pai.
	 * @var int
	 */
	public $id_perfilpai;
	
	/**
	 * Nome do perfil.
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * Id da classe da entidade.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * Alias do perfil.
	 * @var string
	 */
	public $st_nomeperfilalias;
	
	/**
	 * Alias do perfil.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Classe da entidade.
	 * @var string
	 */
	public $st_entidadeclasse;
	
	/**
	 * Nome da entidade.
	 * @var string
	 */
	public $st_nomeentidade;
	
	
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfilpai() {
		return $this->id_perfilpai;
	}
	
	/**
	 * @return string
	 */
	public function getSt_entidadeclasse() {
		return $this->st_entidadeclasse;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeperfilalias() {
		return $this->st_nomeperfilalias;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	
	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param int $id_perfilpai
	 */
	public function setId_perfilpai($id_perfilpai) {
		$this->id_perfilpai = $id_perfilpai;
	}
	
	/**
	 * @param string $st_entidadeclasse
	 */
	public function setSt_entidadeclasse($st_entidadeclasse) {
		$this->st_entidadeclasse = $st_entidadeclasse;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}
	
	/**
	 * @param string $st_nomeperfilalias
	 */
	public function setSt_nomeperfilalias($st_nomeperfilalias) {
		$this->st_nomeperfilalias = $st_nomeperfilalias;
	}

}

?>
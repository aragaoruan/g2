<?php
/**
 * Classe para encapsular os dados da tb_afiliadoproduto.
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class AfiliadoProdutoTO extends Ead1_TO_Dinamico {
		
	public $id_afiliadoproduto;
	public $id_produto;
	public $id_contratoafiliado;
	public $id_categoria;
	public $id_comissionamento;
	public $bl_ativo;
	/**
	 * @return the $id_afiliadoproduto
	 */
	public function getId_afiliadoproduto() {
		return $this->id_afiliadoproduto;
	}

	/**
	 * @param field_type $id_afiliadoproduto
	 */
	public function setId_afiliadoproduto($id_afiliadoproduto) {
		$this->id_afiliadoproduto = $id_afiliadoproduto;
	}

	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @return the $id_categoria
	 */
	public function getId_categoria() {
		return $this->id_categoria;
	}

	/**
	 * @param field_type $id_categoria
	 */
	public function setId_categoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	/**
	 * @return the $id_comissionamento
	 */
	public function getId_comissionamento() {
		return $this->id_comissionamento;
	}

	/**
	 * @param field_type $id_comissionamento
	 */
	public function setId_comissionamento($id_comissionamento) {
		$this->id_comissionamento = $id_comissionamento;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
}

?>
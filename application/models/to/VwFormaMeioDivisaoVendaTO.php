<?php

/**
 * Classe de VwFormaMeioDivisaoVendaTO
 
 * @package models
 * @subpackage to
 */
 class VwFormaMeioDivisaoVendaTO extends Ead1_TO_Dinamico {


 	public $id_formapagamento;
 	public $st_formapagamento;
 	public $id_tipodivisaofinanceira;
 	public $st_tipodivisaofinanceira;
 	public $id_meiopagamento;
 	public $st_meiopagamento;
 	public $nu_parcelaquantidademin;
 	public $nu_parcelaquantidademax;
 	public $nu_juros;
 	public $nu_mvaxparcelas;
 	public $id_formapagamentoaplicacao;
 	public $id_entidade;
 	public $nu_valorliquido;
 	public $id_venda;
 	public $id_tipocalculojuros;
 	public $id_categoriacampanha;
 	public $id_campanhacomercial;
 	public $nu_valormin;
 	
 	
 	
	/**
	 * @return the $nu_valormin
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}

	/**
	 * @param field_type $nu_valormin
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}

	/**
	 * @return the $id_categoriacampanha
	 */
	public function getId_categoriacampanha() {
		return $this->id_categoriacampanha;
	}

	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @param field_type $id_categoriacampanha
	 */
	public function setId_categoriacampanha($id_categoriacampanha) {
		$this->id_categoriacampanha = $id_categoriacampanha;
	}

	/**
	 * @param field_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @return the $id_tipocalculojuros
	 */
	public function getId_tipocalculojuros() {
		return $this->id_tipocalculojuros;
	}

	/**
	 * @param field_type $id_tipocalculojuros
	 */
	public function setId_tipocalculojuros($id_tipocalculojuros) {
		$this->id_tipocalculojuros = $id_tipocalculojuros;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}

	/**
	 * @param field_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @return the $id_tipodivisaofinanceira
	 */
	public function getId_tipodivisaofinanceira() {
		return $this->id_tipodivisaofinanceira;
	}

	/**
	 * @param field_type $id_tipodivisaofinanceira
	 */
	public function setId_tipodivisaofinanceira($id_tipodivisaofinanceira) {
		$this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
	}

	/**
	 * @return the $st_tipodivisaofinanceira
	 */
	public function getSt_tipodivisaofinanceira() {
		return $this->st_tipodivisaofinanceira;
	}

	/**
	 * @param field_type $st_tipodivisaofinanceira
	 */
	public function setSt_tipodivisaofinanceira($st_tipodivisaofinanceira) {
		$this->st_tipodivisaofinanceira = $st_tipodivisaofinanceira;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @return the $st_meiopagamento
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}

	/**
	 * @param field_type $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}

	/**
	 * @return the $nu_parcelaquantidademin
	 */
	public function getNu_parcelaquantidademin() {
		return $this->nu_parcelaquantidademin;
	}

	/**
	 * @param field_type $nu_parcelaquantidademin
	 */
	public function setNu_parcelaquantidademin($nu_parcelaquantidademin) {
		$this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
	}

	/**
	 * @return the $nu_parcelaquantidademax
	 */
	public function getNu_parcelaquantidademax() {
		return $this->nu_parcelaquantidademax;
	}

	/**
	 * @param field_type $nu_parcelaquantidademax
	 */
	public function setNu_parcelaquantidademax($nu_parcelaquantidademax) {
		$this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
	}

	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros() {
		if($this->nu_juros)
			return number_format($this->nu_juros,0,'','');
	}

	/**
	 * @param field_type $nu_juros
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}

	/**
	 * @return the $nu_mvaxparcelas
	 */
	public function getNu_mvaxparcelas() {
		return $this->nu_mvaxparcelas;
	}

	/**
	 * @param field_type $nu_mvaxparcelas
	 */
	public function setNu_mvaxparcelas($nu_mvaxparcelas) {
		$this->nu_mvaxparcelas = $nu_mvaxparcelas;
	}

	/**
	 * @return the $id_formapagamentoaplicacao
	 */
	public function getId_formapagamentoaplicacao() {
		return $this->id_formapagamentoaplicacao;
	}

	/**
	 * @param field_type $id_formapagamentoaplicacao
	 */
	public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao) {
		$this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		
		if($this->nu_valorliquido)
			return number_format($this->nu_valorliquido, 2, '.','');
		
	}

	/**
	 * @param field_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

 	

}

?>
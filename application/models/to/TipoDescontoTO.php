<?php
/**
 * Classe para encapsular os dados de tipo de desconto.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDescontoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de desconto.
	 * @var int
	 */
	public $id_tipodesconto;
	
	/**
	 * Nome do tipo desconto.
	 * @var String
	 */
	public $st_tipodesconto;
	
	/**
	 * @return int
	 */
	public function getId_tipodesconto() {
		return $this->id_tipodesconto;
	}
	
	/**
	 * @return String
	 */
	public function getSt_tipodesconto() {
		return $this->st_tipodesconto;
	}
	
	/**
	 * @param int $id_tipodesconto
	 */
	public function setId_tipodesconto($id_tipodesconto) {
		$this->id_tipodesconto = $id_tipodesconto;
	}
	
	/**
	 * @param String $st_tipodesconto
	 */
	public function setSt_tipodesconto($st_tipodesconto) {
		$this->st_tipodesconto = $st_tipodesconto;
	}

	
}

?>
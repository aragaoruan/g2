<?php

/**
 * Classe para encapsular os dados de pessoa
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PessoaTO extends Ead1_TO_Dinamico
{

    const SITUACAO_ATIVO = 1; //1	Ativo

    /**
     * Atributo que contém o id da pessoa
     * @var int
     */
    public $id_usuario;

    /**
     * Atributo que contém o id da entidade da pessoa
     * @var int
     */
    public $id_entidade;

    /**
     * Atributo que contém o sexo da pessoa
     * @var string
     */
    public $st_sexo;

    /**
     * Atributo que contém o id do estado civil da pessoa
     * @var int
     */
    public $id_estadocivil;

    /**
     * Atributo que contém o nome de exibição da pessoa
     * @var string
     */
    public $st_nomeexibicao;

    /**
     * Atributo que contém a data de nascimento da pessoa
     * @var string|DateTime|\Zend_Date
     */
    public $dt_nascimento;

    /**
     * Atributo que contém o nome do pai da pessoa
     * @var string
     */
    public $st_nomepai;

    /**
     * Atributo que contém o nome da mae da pessoa
     * @var string
     */
    public $st_nomemae;

    /**
     * Atributo que contém o id do pais da pessoa
     * @var int
     */
    public $id_pais;

    /**
     * Atributo que contém a uf da pessoa
     * @var string
     */
    public $sg_uf;

    /**
     * Atributo que contém o id do municipio da pessoa
     * @var int
     */
    public $id_municipio;

    /**
     * Atributo que contém a data de cadastro da pessoa
     * @var string|DateTime|\Zend_Date
     */
    public $dt_cadastro;

    /**
     * Atributo que contém o id do usuario que cadastrou a pessoa
     * @var int
     */
    public $id_usuariocadastro;

    /**
     * Atributo que contém o passaporte da pessoa
     * @var string
     */
    public $st_passaporte;

    /**
     * Atributo que contém o id da situação da pessoa
     * @var int
     */
    public $id_situacao;

    /**
     * Atributo que contém o valor da situação da pessoa
     * @var bool
     */
    public $bl_ativo;

    /**
     * Atributo que contem o endereco url da imagem do avatar do usuario
     * @var string
     */
    public $st_urlavatar;

    /**
     * Atributo que contem a cidade de nascimento
     * @var string
     */
    public $st_cidade;

    /**
     * Código de identificação da carteirinha
     * @var string
     */
    public $st_identificacao;

    /**
     * Atributo que marca quando o cadastro foi atualizado, sendo assim possível sincronizar em outros lugares
     * @var int
     */
    public $bl_alterado;

    /**
     * @var string
     */
    public $st_tituloeleitor;

    /**
     * @var string
     */
    public $st_certificadoreservista;

    /**
     * @var int
     */
    public $id_municipionascimento;

    /**
     * @var string
     */
    public $sg_ufnascimento;

    public $st_zonaeleitoral;

    public $st_secaoeleitoral;

    public $st_municipioeleitoral;

    public $dt_expedicaocertificadoreservista;

    public $id_categoriaservicomilitar;

    public $st_reparticaoexpedidora;

    /**
     * @var int
     */
    public $id_tiposanguineo;

    /**
     * @var string
     */
    public $sg_fatorrh;

    /**
     * @return mixed
     */
    public function getSg_ufnascimento()
    {
        return $this->sg_ufnascimento;
    }

    /**
     * @param mixed $sg_ufnascimento
     */
    public function setSg_ufnascimento($sg_ufnascimento)
    {
        $this->sg_ufnascimento = $sg_ufnascimento;
    }

    /**
     * @return mixed
     */
    public function getId_municipionascimento()
    {
        return $this->id_municipionascimento;
    }

    /**
     * @param mixed $id_municipionascimento
     */
    public function setId_municipionascimento($id_municipionascimento)
    {
        $this->id_municipionascimento = $id_municipionascimento;
    }

    /**
     * @return mixed
     */
    public function getSt_certificadoreservista()
    {
        return $this->st_certificadoreservista;
    }

    /**
     * @param mixed $st_certificadoreservista
     */
    public function setSt_certificadoreservista($st_certificadoreservista)
    {
        $this->st_certificadoreservista = $st_certificadoreservista;
    }

    /**
     * @return mixed
     */
    public function getSt_tituloeleitor()
    {
        return $this->st_tituloeleitor;
    }

    /**
     * @param mixed $st_tituloeleitor
     */
    public function setSt_tituloeleitor($st_tituloeleitor)
    {
        $this->st_tituloeleitor = $st_tituloeleitor;
    }


    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_sexo()
    {
        return $this->st_sexo;
    }

    /**
     * @param string $st_sexo
     * @return $this
     */
    public function setSt_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_estadocivil()
    {
        return $this->id_estadocivil;
    }

    /**
     * @param int $id_estadocivil
     * @return $this
     */
    public function setId_estadocivil($id_estadocivil)
    {
        $this->id_estadocivil = $id_estadocivil;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @param string $st_nomeexibicao
     * @return $this
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;
    }

    /**
     * @return DateTime|string|Zend_Date
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @param DateTime|string|Zend_Date $dt_nascimento
     * @return $this
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    /**
     * @param string $st_nomepai
     * @return $this
     */
    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    /**
     * @param string $st_nomemae
     * @return $this
     */
    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_pais()
    {
        return $this->id_pais;
    }

    /**
     * @param int $id_pais
     * @return $this
     */
    public function setId_pais($id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_uf
     * @return $this
     */
    public function setsg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_municipio()
    {
        return $this->id_municipio;
    }

    /**
     * @param int $id_municipio
     * @return $this
     */
    public function setId_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    /**
     * @return DateTime|string|Zend_Date
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param DateTime|string|Zend_Date $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_passaporte()
    {
        return $this->st_passaporte;
    }

    /**
     * @param string $st_passaporte
     * @return $this
     */
    public function setSt_passaporte($st_passaporte)
    {
        $this->st_passaporte = $st_passaporte;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param string $st_urlavatar
     * @return $this
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param string $st_cidade
     * @return $this
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }

    /**
     * @param string $st_identificacao
     * @return $this
     */
    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getBl_alterado()
    {
        return $this->bl_alterado;
    }

    /**
     * @param int $bl_alterado
     * @return $this
     */
    public function setBl_alterado($bl_alterado)
    {
        $this->bl_alterado = $bl_alterado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_zonaeleitoral()
    {
        return $this->st_zonaeleitoral;
    }

    /**
     * @param mixed $st_zonaeleitoral
     */
    public function setSt_zonaeleitoral($st_zonaeleitoral)
    {
        $this->st_zonaeleitoral = $st_zonaeleitoral;
    }

    /**
     * @return mixed
     */
    public function getSt_secaoeleitoral()
    {
        return $this->st_secaoeleitoral;
    }

    /**
     * @param mixed $st_secaoeleitoral
     */
    public function setSt_secaoeleitoral($st_secaoeleitoral)
    {
        $this->st_secaoeleitoral = $st_secaoeleitoral;
    }

    /**
     * @return mixed
     */
    public function getSt_municipioeleitoral()
    {
        return $this->st_municipioeleitoral;
    }

    /**
     * @param mixed $st_municipioeleitoral
     */
    public function setSt_municipioeleitoral($st_municipioeleitoral)
    {
        $this->st_municipioeleitoral = $st_municipioeleitoral;
    }

    /**
     * @return mixed
     */
    public function getDt_expedicaocertificadoreservista()
    {
        return $this->dt_expedicaocertificadoreservista;
    }

    /**
     * @param mixed $dt_expedicaocertificadoreservista
     */
    public function setDt_expedicaocertificadoreservista($dt_expedicaocertificadoreservista)
    {
        $this->dt_expedicaocertificadoreservista = $dt_expedicaocertificadoreservista;
    }

    /**
     * @return mixed
     */
    public function getId_categoriaservicomilitar()
    {
        return $this->id_categoriaservicomilitar;
    }

    /**
     * @param mixed $id_categoriaservicomilitar
     */
    public function setId_categoriaservicomilitar($id_categoriaservicomilitar)
    {
        $this->id_categoriaservicomilitar = $id_categoriaservicomilitar;
    }

    /**
     * @return mixed
     */
    public function getSt_reparticaoexpedidora()
    {
        return $this->st_reparticaoexpedidora;
    }

    /**
     * @param mixed $st_reparticaoexpedidora
     */
    public function setSt_reparticaoexpedidora($st_reparticaoexpedidora)
    {
        $this->st_reparticaoexpedidora = $st_reparticaoexpedidora;
    }

    /**
     * @return int
     */
    public function getId_tiposanguineo()
    {
        return $this->id_tiposanguineo;
    }

    /**
     * @param int $id_tiposanguineo
     * @return $this
     */
    public function setId_tiposanguineo($id_tiposanguineo)
    {
        $this->id_tiposanguineo = $id_tiposanguineo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSg_fatorrh()
    {
        return $this->sg_fatorrh;
    }

    /**
     * @param string $sg_fatorrh
     * @return $this
     */
    public function setSg_fatorrh($sg_fatorrh)
    {
        $this->sg_fatorrh = $sg_fatorrh;
        return $this;
    }

}

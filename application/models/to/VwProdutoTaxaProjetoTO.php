<?php

class VwProdutoTaxaProjetoTO extends Ead1_TO_Dinamico{
	
	public $id_taxa;
	public $id_entidade;
	public $id_produto;
	public $id_projetopedagogico;
	public $id_tipoproduto;
	public $st_produto;
	public $nu_valor;
	public $nu_valormensal;
	
	/**
	 * @return unknown
	 */
	public function getId_taxa() {
		return $this->id_taxa;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}
	
	/**
	 * @param unknown_type id_taxa
	 */
	public function setId_taxa($id_taxa) {
		$this->id_taxa = $id_taxa;
	}
	
	/**
	 * @param unknown_type nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}
	
	/**
	 * @param unknown_type nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}
	
	/**
	 * @param unknown_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}
	
	/**
	 * @param unknown_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}
	
	
}
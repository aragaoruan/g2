<?php
/**
 * Classe que encapsula os dados da tb_setorfuncionario
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class SetorFuncionarioTO extends Ead1_TO_Dinamico {

	public $id_setorfuncionario;
	public $id_setor;
	public $id_usuario;
	public $id_cargo;
	public $id_funcao;
	public $st_ramal;
	
	/**
	 * @return the $id_setorfuncionario
	 */
	public function getId_setorfuncionario() {
		return $this->id_setorfuncionario;
	}

	/**
	 * @return the $id_setor
	 */
	public function getId_setor() {
		return $this->id_setor;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_cargo
	 */
	public function getId_cargo() {
		return $this->id_cargo;
	}

	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @return the $st_ramal
	 */
	public function getSt_ramal() {
		return $this->st_ramal;
	}

	/**
	 * @param field_type $id_setorfuncionario
	 */
	public function setId_setorfuncionario($id_setorfuncionario) {
		$this->id_setorfuncionario = $id_setorfuncionario;
	}

	/**
	 * @param field_type $id_setor
	 */
	public function setId_setor($id_setor) {
		$this->id_setor = $id_setor;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_cargo
	 */
	public function setId_cargo($id_cargo) {
		$this->id_cargo = $id_cargo;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

	/**
	 * @param field_type $st_ramal
	 */
	public function setSt_ramal($st_ramal) {
		$this->st_ramal = $st_ramal;
	}
}

?>
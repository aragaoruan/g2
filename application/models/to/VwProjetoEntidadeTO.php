<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwProjetoEntidadeTO extends Ead1_TO_Dinamico{

	public $id_projetopedagogico;
	public $id_situacao;
	public $st_descricao;
	public $bl_ativo;
	public $st_projetopedagogico;
	public $id_entidadecadastro;
	public $st_tituloexibicao;
	public $id_entidade;
	public $st_nomeentidade;
	public $st_razaosocial;


	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao(){ 
		return $this->st_descricao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico(){ 
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro(){ 
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao(){ 
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade(){ 
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_razaosocial
	 */
	public function getSt_razaosocial(){ 
		return $this->st_razaosocial;
	}


	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao){ 
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico){ 
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro){ 
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao){ 
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade){ 
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial){ 
		$this->st_razaosocial = $st_razaosocial;
	}

}
<?php
/**
 * Classe para encapsular os dados do Responsavel do Contrato.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContratoResponsavelTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o id do responsavel do contrato
	 * @var int
	 */
	public $id_contratoresponsavel;
	
	/**
	 * Contem o id do usuario responsavel
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do responsavel pessoa juridico
	 * @var int
	 */
	public $id_entidaderesponsavel;
	
	/**
	 * Contem o id do contrato
	 * @var int
	 */
	public $id_contrato;
	
	/**
	 * Contem o tipo de contrato do responsavel
	 * @var int
	 */
	public $id_tipocontratoresponsavel;
	
	/**
	 * Contem a porcentagem
	 * @var numeric
	 */
	public $nu_porcentagem;
	
	public $bl_ativo;
	
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_contratoresponsavel
	 */
	public function getId_contratoresponsavel() {
		return $this->id_contratoresponsavel;
	}

	/**
	 * @return the $id_entidaderesponsavel
	 */
	public function getId_entidaderesponsavel() {
		return $this->id_entidaderesponsavel;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_tipocontratoresponsavel
	 */
	public function getId_tipocontratoresponsavel() {
		return $this->id_tipocontratoresponsavel;
	}

	/**
	 * @return the $nu_porcentagem
	 */
	public function getNu_porcentagem() {
		return $this->nu_porcentagem;
	}

	/**
	 * @param int $id_contratoresponsavel
	 */
	public function setId_contratoresponsavel($id_contratoresponsavel) {
		$this->id_contratoresponsavel = $id_contratoresponsavel;
	}

	/**
	 * @param int $id_entidaderesponsavel
	 */
	public function setId_entidaderesponsavel($id_entidaderesponsavel) {
		$this->id_entidaderesponsavel = $id_entidaderesponsavel;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param int $id_tipocontratoresponsavel
	 */
	public function setId_tipocontratoresponsavel($id_tipocontratoresponsavel) {
		$this->id_tipocontratoresponsavel = $id_tipocontratoresponsavel;
	}

	/**
	 * @param numeric $nu_porcentagem
	 */
	public function setNu_porcentagem($nu_porcentagem) {
		$this->nu_porcentagem = $nu_porcentagem;
	}


	
}
<?php
/**
 * Classe para encapsular os dados da matricula e suas disciplinas
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class MatriculaDisciplinaTO extends Ead1_TO_Dinamico {

	CONST SITUACAO_CONCEDIDA = 65;
	CONST SITUACAO_ATIVA = 54;
	CONST EVOLUCAO_CONCLUIDA = 12;
	CONST EVOLUCAO_CURSANDO = 13;
	CONST EVOLUCAO_REPROVADO = 19;

    public $id_matriculadisciplina;
	public $id_evolucao;
	public $id_situacao;
	public $id_matricula;
	public $id_disciplina;
    public $id_situacaotiposervico;
	public $nu_aprovafinal;
	public $bl_obrigatorio;
	public $dt_conclusao;
	public $nu_semestre;
    public $dt_aptoagendamento;
    public $nu_cargahorariaaproveitamento;

    /**
     * @return mixed
     */
    public function getId_situacaotiposervico()
    {
        return $this->id_situacaotiposervico;
    }

    /**
     * @param mixed $id_situacaotiposervico
     */
    public function setId_situacaotiposervico($id_situacaotiposervico)
    {
        $this->id_situacaotiposervico = $id_situacaotiposervico;
    }

    /**
	 * @return the $id_matriculadisciplina
	 */
	public function getId_matriculadisciplina() {
		return $this->id_matriculadisciplina;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
     * @return int $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $nu_aprovafinal
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}

	/**
	 * @return the $bl_obrigatorio
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}

	/**
	 * @return the $dt_conclusao
	 */
	public function getDt_conclusao() {
		return $this->dt_conclusao;
	}

	/**
     * @param integer $id_matriculadisciplina
	 */
	public function setId_matriculadisciplina($id_matriculadisciplina) {
		$this->id_matriculadisciplina = $id_matriculadisciplina;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}

	/**
	 * @param field_type $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}

	/**
	 * @param field_type $dt_conclusao
	 */
	public function setDt_conclusao($dt_conclusao) {
		$this->dt_conclusao = $dt_conclusao;
	}

	public function getNu_semestre()
	{
		return $this->nu_semestre;
	}

	public function setNu_semestre($nu_semestre)
	{
		$this->nu_semestre = $nu_semestre;
	}

    /**
     * @return int
     */
    public function getDt_aptoagendamento()
    {
        return $this->dt_aptoagendamento;
    }

    /**
     * @param int $dt_aptoagendamento
     * @return $this
     */
    public function setDt_aptoagendamento($dt_aptoagendamento)
    {
        $this->dt_aptoagendamento = $dt_aptoagendamento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNu_cargahorariaaproveitamento()
    {
        return $this->nu_cargahorariaaproveitamento;
    }

    /**
     * @param mixed $nu_cargahorariaaproveitamento
     */
    public function setNu_cargahorariaaproveitamento($nu_cargahorariaaproveitamento)
    {
        $this->nu_cargahorariaaproveitamento = $nu_cargahorariaaproveitamento;
    }



}

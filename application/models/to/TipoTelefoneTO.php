<?php
/**
 * Classe para encapsular os dados de tipo de telefone.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoTelefoneTO extends Ead1_TO_Dinamico {
	
	const RESIDENCIAL = 1;
	const COMERCIAL = 2;
	const CELULAR = 3;

	/**
	 * Contém o id do tipo de telefone.
	 * @var int
	 */
	public $id_tipotelefone;
	
	/**
	 * Contém a descrição do tipo de telefone.
	 * @var string
	 */
	public $st_tipotelefone;
	
	/**
	 * @return int
	 */
	public function getId_tipotelefone() {
		return $this->id_tipotelefone;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipotelefone() {
		return $this->st_tipotelefone;
	}
	
	/**
	 * @param int $id_tipotelefone
	 */
	public function setId_tipotelefone($id_tipotelefone) {
		$this->id_tipotelefone = $id_tipotelefone;
	}
	
	/**
	 * @param string $st_tipotelefone
	 */
	public function setSt_tipotelefone($st_tipotelefone) {
		$this->st_tipotelefone = $st_tipotelefone;
	}

}

?>
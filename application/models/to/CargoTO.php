<?php
/**
 * Classe para encapsular os dados de Função para Cargo.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class CargoTO extends Ead1_TO_Dinamico {
	
	public $id_cargo;
	public $id_situacao;
	public $id_entidadecadastro;
	public $id_usuariocadastro;
	public $st_cargo;
	public $dt_cadastro;
	
	/**
	 *
	 * @return the $id_cargo
	 */
	public function getId_cargo() {
		return $this->id_cargo;
	}
	
	/**
	 *
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 *
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 *
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 *
	 * @return the $st_cargo
	 */
	public function getSt_cargo() {
		return $this->st_cargo;
	}
	
	/**
	 *
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 *
	 * @param field_type $id_cargo        	
	 */
	public function setId_cargo($id_cargo) {
		$this->id_cargo = $id_cargo;
	}
	
	/**
	 *
	 * @param field_type $id_situacao        	
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 *
	 * @param field_type $id_entidadecadastro        	
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 *
	 * @param field_type $id_usuariocadastro        	
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 *
	 * @param field_type $st_cargo        	
	 */
	public function setSt_cargo($st_cargo) {
		$this->st_cargo = $st_cargo;
	}
	
	/**
	 *
	 * @param field_type $dt_cadastro        	
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
}

?>
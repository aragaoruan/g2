<?php
class AproveitamentoImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_aproveitamentoimportacao;
	public $id_matriculaimportacao;
	public $id_aproveitamentodisciplina;
	public $id_sistemaimportacao;
	public $nu_codmatriculaorigem;
	public $nu_coddisciplinaorigem;
	public $st_resultadooriginal;
	public $st_instituicaoaproveitamento;
	public $st_cidadeinstituicaoaproveitamento;
	public $sg_ufinstituicaoaproveitamento;
	public $nu_aproveitamento;
	public $nu_codcursoorigem;
	public $nu_codareaorigem;
	
	/**
	 * @return the $id_aproveitamentoimportacao
	 */
	public function getId_aproveitamentoimportacao() {
		return $this->id_aproveitamentoimportacao;
	}

	/**
	 * @return the $id_matriculaimportacao
	 */
	public function getId_matriculaimportacao() {
		return $this->id_matriculaimportacao;
	}

	/**
	 * @return the $id_aproveitamentodisciplina
	 */
	public function getId_aproveitamentodisciplina() {
		return $this->id_aproveitamentodisciplina;
	}

	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @return the $nu_codmatriculaorigem
	 */
	public function getNu_codmatriculaorigem() {
		return $this->nu_codmatriculaorigem;
	}

	/**
	 * @return the $nu_coddisciplinaorigem
	 */
	public function getNu_coddisciplinaorigem() {
		return $this->nu_coddisciplinaorigem;
	}

	/**
	 * @return the $st_resultadooriginal
	 */
	public function getSt_resultadooriginal() {
		return $this->st_resultadooriginal;
	}

	/**
	 * @return the $st_instituicaoaproveitamento
	 */
	public function getSt_instituicaoaproveitamento() {
		return $this->st_instituicaoaproveitamento;
	}

	/**
	 * @return the $st_cidadeinstituicaoaproveitamento
	 */
	public function getSt_cidadeinstituicaoaproveitamento() {
		return $this->st_cidadeinstituicaoaproveitamento;
	}

	/**
	 * @return the $sg_ufinstituicaoaproveitamento
	 */
	public function getSg_ufinstituicaoaproveitamento() {
		return $this->sg_ufinstituicaoaproveitamento;
	}

	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_aproveitamento() {
		return $this->nu_aproveitamento;
	}

	/**
	 * @param field_type $id_aproveitamentoimportacao
	 */
	public function setId_aproveitamentoimportacao($id_aproveitamentoimportacao) {
		$this->id_aproveitamentoimportacao = $id_aproveitamentoimportacao;
	}
	
	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_codcursoorigem() {
		return $this->nu_codcursoorigem;
	}

	/**
	 * @param field_type $id_aproveitamentoimportacao
	 */
	public function setNu_codcursoorigem($nu_codcursoorigem) {
		$this->nu_codcursoorigem = $nu_codcursoorigem;
	}
	
	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_codareaorigem() {
		return $this->nu_codareaorigem;
	}

	/**
	 * @param field_type $id_aproveitamentoimportacao
	 */
	public function setNu_codareaorigem($nu_codareaorigem) {
		$this->nu_codareaorigem = $nu_codareaorigem;
	}

	/**
	 * @param field_type $id_matriculaimportacao
	 */
	public function setId_matriculaimportacao($id_matriculaimportacao) {
		$this->id_matriculaimportacao = $id_matriculaimportacao;
	}

	/**
	 * @param field_type $id_aproveitamentodisciplina
	 */
	public function setId_aproveitamentodisciplina($id_aproveitamentodisciplina) {
		$this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}

	/**
	 * @param field_type $nu_codmatriculaorigem
	 */
	public function setNu_codmatriculaorigem($nu_codmatriculaorigem) {
		$this->nu_codmatriculaorigem = $nu_codmatriculaorigem;
	}

	/**
	 * @param field_type $nu_coddisciplinaorigem
	 */
	public function setNu_coddisciplinaorigem($nu_coddisciplinaorigem) {
		$this->nu_coddisciplinaorigem = $nu_coddisciplinaorigem;
	}

	/**
	 * @param field_type $st_resultadooriginal
	 */
	public function setSt_resultadooriginal($st_resultadooriginal) {
		$this->st_resultadooriginal = $st_resultadooriginal;
	}

	/**
	 * @param field_type $st_instituicaoaproveitamento
	 */
	public function setSt_instituicaoaproveitamento($st_instituicaoaproveitamento) {
		$this->st_instituicaoaproveitamento = $st_instituicaoaproveitamento;
	}

	/**
	 * @param field_type $st_cidadeinstituicaoaproveitamento
	 */
	public function setSt_cidadeinstituicaoaproveitamento($st_cidadeinstituicaoaproveitamento) {
		$this->st_cidadeinstituicaoaproveitamento = $st_cidadeinstituicaoaproveitamento;
	}

	/**
	 * @param field_type $sg_ufinstituicaoaproveitamento
	 */
	public function setSg_ufinstituicaoaproveitamento($sg_ufinstituicaoaproveitamento) {
		$this->sg_ufinstituicaoaproveitamento = $sg_ufinstituicaoaproveitamento;
	}

	/**
	 * @param field_type $nu_aproveitamento
	 */
	public function setNu_aproveitamento($nu_aproveitamento) {
		$this->nu_aproveitamento = $nu_aproveitamento;
	}

}
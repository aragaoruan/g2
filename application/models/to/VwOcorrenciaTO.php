<?php

/**
 * Classe para encapsular os dados de Vwocorrência.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwOcorrenciaTO extends Ead1_TO_Dinamico
{

    /**
     * Chave da tabela
     * @var int
     */
    public $id_ocorrencia;

    /**
     * Matrícula do aluno (quando for ocorrencia de aluno)
     * @var int
     */
    public $id_matricula;

    /**
     * Evolução da ocorrência
     * @var int
     */
    public $id_evolucao;

    /**
     * Situação da ocorrência
     * @var int
     */
    public $id_situacao;

    /**
     * Pessoa que criou a ocorrencia
     * @var int
     */
    public $id_usuariointeressado;

    /**
     * Tipo de Ocorrência
     * @var int
     */
    public $id_categoriaocorrencia;

    /**
     * Assunto da Ocorrência
     * @var int
     */
    public $id_assuntoco;
    /**
     * Assunto Pai da Ocorrência
     * @var int
     */
    public $id_assuntocopai;

    /**
     * Sala de aula (caso o aluno tenha selecionado
     * @var int
     */
    public $id_saladeaula;

    /**
     * Título da ocorrência
     * @var string
     */
    public $st_titulo;

    /**
     * Título da ocorrência
     * @var string
     */
    public $st_urlportal;

    /**
     * Descrição da ocorrência
     * @var string
     */
    public $st_ocorrencia;

    /**
     * Texto da situação
     * @var String
     */
    public $st_situacao;

    /**
     * Texto da evolução
     * @var string
     */
    public $st_evolucao;

    /**
     * Texto do Assunto
     * @var string
     */
    public $st_assuntoco;

    /**
     * Data último tramite
     * @var string
     */
    public $dt_ultimotramite;

    /**
     * Tipo de ocorrência
     * @var string
     */
    public $st_categoriaocorrencia;

    /**
     * Id do usuário responsavel pela ocorrencia
     * @var int
     */
    public $id_usuarioresponsavel;

    /**
     * @var string
     */
    public $st_nomeresponsavel;

    /**
     * Id do tipo de ocorrência do assunto
     * @var int
     */
    public $id_tipoocorrencia;

    /**
     * @var int
     */
    public $id_entidade;

    /**
     * @var string
     */
    public $dt_atendimento;

    /**
     * @var string
     */
    public $st_email;

    /**
     * @var string
     */
    public $st_telefone;

    /**
     * @var string
     */
    public $st_projetopedagogico;

    /**
     * @var string
     */
    public $st_saladeaula;
    /**
     * Data de cadastro da ocorrencia
     * @var date
     */
    public $dt_cadastro;

    /**
     * Data de cadastro da ocorrencia
     * @var DateTime
     */
    public $st_cadastro;

    /**
     * Data de cadastro da ocorrencia
     * @var integer
     */
    public $id_projetopedagogico;

    /**
     * @return int
     */
    public function getid_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param int $id_ocorrencia
     */
    public function setid_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getid_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    /**
     * @param int $id_usuariointeressado
     */
    public function setid_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
    }

    /**
     * @return int
     */
    public function getid_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @param int $id_categoriaocorrencia
     */
    public function setid_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    /**
     * @return int
     */
    public function getid_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param int $id_assuntoco
     */
    public function setid_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    /**
     * @return int
     */
    public function getid_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    /**
     * @param int $id_assuntocopai
     */
    public function setid_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
    }

    /**
     * @return int
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return string
     */
    public function getst_titulo()
    {
        return $this->st_titulo;
    }

    /**
     * @param string $st_titulo
     */
    public function setst_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
    }

    /**
     * @return string
     */
    public function getst_urlportal()
    {
        return $this->st_urlportal;
    }

    /**
     * @param string $st_urlportal
     */
    public function setst_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
    }

    /**
     * @return string
     */
    public function getst_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    /**
     * @param string $st_ocorrencia
     */
    public function setst_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
    }

    /**
     * @return String
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param String $st_situacao
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return string
     */
    public function getst_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setst_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return string
     */
    public function getst_assuntoco()
    {
        return $this->st_assuntoco;
    }

    /**
     * @param string $st_assuntoco
     */
    public function setst_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    /**
     * @return string
     */
    public function getdt_ultimotramite()
    {
        return $this->dt_ultimotramite;
    }

    /**
     * @param string $dt_ultimotramite
     */
    public function setdt_ultimotramite($dt_ultimotramite)
    {
        $this->dt_ultimotramite = $dt_ultimotramite;
    }

    /**
     * @return string
     */
    public function getst_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    /**
     * @param string $st_categoriaocorrencia
     */
    public function setst_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
    }

    /**
     * @return int
     */
    public function getid_usuarioresponsavel()
    {
        return $this->id_usuarioresponsavel;
    }

    /**
     * @param int $id_usuarioresponsavel
     */
    public function setid_usuarioresponsavel($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
    }

    /**
     * @return string
     */
    public function getst_nomeresponsavel()
    {
        return $this->st_nomeresponsavel;
    }

    /**
     * @param string $st_nomeresponsavel
     */
    public function setst_nomeresponsavel($st_nomeresponsavel)
    {
        $this->st_nomeresponsavel = $st_nomeresponsavel;
    }

    /**
     * @return int
     */
    public function getid_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     */
    public function setid_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return string
     */
    public function getdt_atendimento()
    {
        return $this->dt_atendimento;
    }

    /**
     * @param string $dt_atendimento
     */
    public function setdt_atendimento($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
    }

    /**
     * @return string
     */
    public function getst_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     */
    public function setst_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return string
     */
    public function getst_telefone()
    {
        return $this->st_telefone;
    }

    /**
     * @param string $st_telefone
     */
    public function setst_telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    /**
     * @return string
     */
    public function getst_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setst_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getst_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setst_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return date
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param date $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return DateTime
     */
    public function getst_cadastro()
    {
        return $this->st_cadastro;
    }

    /**
     * @param DateTime $st_cadastro
     */
    public function setst_cadastro($st_cadastro)
    {
        $this->st_cadastro = $st_cadastro;
    }

    /**
     * @return int
     */
    public function getid_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setid_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }


}



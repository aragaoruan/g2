<?php
/**
 * Classe para encapsular os dados da tb_afiliadoprodutocomissionamento.
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class AfiliadoProdutoComissionamentoTO extends Ead1_TO_Dinamico {
		
	public $id_produtoafiliado;
	public $id_comissionamento;
	/**
	 * @return the $id_produtoafiliado
	 */
	public function getId_produtoafiliado() {
		return $this->id_produtoafiliado;
	}

	/**
	 * @param field_type $id_produtoafiliado
	 */
	public function setId_produtoafiliado($id_produtoafiliado) {
		$this->id_produtoafiliado = $id_produtoafiliado;
	}

	/**
	 * @return the $id_comissionamento
	 */
	public function getId_comissionamento() {
		return $this->id_comissionamento;
	}

	/**
	 * @param field_type $id_comissionamento
	 */
	public function setId_comissionamento($id_comissionamento) {
		$this->id_comissionamento = $id_comissionamento;
	}

	
	
}

?>
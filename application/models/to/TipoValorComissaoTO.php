<?php
/**
 * Classe para encapsular os dados de tipo de valor de comissão
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TipoValorComissaoTO extends Ead1_TO_Dinamico {
	
	public $id_tipovalorcomissao;
	public $st_tipovalorcomissao;
	
	/**
	 * @return unknown
	 */
	public function getId_tipovalorcomissao() {
		return $this->id_tipovalorcomissao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipocomissao() {
		return $this->st_tipovalorcomissao;
	}
	
	/**
	 * @param unknown_type $id_tipovalorcomissao
	 */
	public function setId_tipovalorcomissao($id_tipovalorcomissao) {
		$this->id_tipovalorcomissao = $id_tipovalorcomissao;
	}
	
	/**
	 * @param unknown_type $st_tipovalorcomissao
	 */
	public function setSt_tipovalorcomissao($st_tipovalorcomissao) {
		$this->st_tipovalorcomissao = $st_tipovalorcomissao;
	}
}
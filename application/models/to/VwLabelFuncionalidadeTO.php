<?php
/**
 * To da view vw_labelfuncionalidade
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwLabelFuncionalidadeTO extends Ead1_TO_Dinamico{
	
	/**
	 * Contem o id da funcionalidade
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Contem o nome/label da funcionalidade
	 * @var string
	 */
	public $st_funcionalidade;
	
	/**
	 * Contem o id da funcionalidade pai
	 * @var int
	 */
	public $id_funcionalidadepai;
	
	/**
	 * Contem o valor se a funcionalidade esta ativa
	 * @var int
	 */
	public $bl_funcionalidadeativo;
	
	/**
	 * Contem o id da situacao da funcionalidade
	 * @var int
	 */
	public $id_situacaofuncionalidade;
	
	/**
	 * Contem o caminho da classe flex
	 * @var string
	 */
	public $st_classeflex;
	
	/**
	 * Contem o icone da funcionalidade
	 * @var string
	 */
	public $st_urlicone;
	
	/**
	 * Contem o id do tipo da funcionalidade
	 * @var int
	 */
	public $id_tipofuncionalidade;
	
	/**
	 * Contem a ordem da funcionalidade
	 * @var int
	 */
	public $nu_ordem;
	
	/**
	 * Contem o caminho da classe do botao
	 * @var string
	 */
	public $st_classeflexbotao;
	
	/**
	 * Contem o valor se a funcionalidade esta ativa para pesquisa
	 * @var int
	 */
	public $bl_pesquisa;
	
	/**
	 * Diz se a funcionalidade esta ativa na entidade
	 * @var int
	 */
	public $bl_entidadefuncionalidadeativo;
	
	/**
	 * Contem o id da situacao da funcionalidade na entidade
	 * @var int
	 */
	public $id_situacaoentidadefuncionalidade;
	
	/**
	 * Diz se é obrigatorio
	 * @var int
	 */
	public $bl_obrigatorio;
	
	/**
	 * Diz se esta visivel na entidade
	 * @var int
	 */
	public $bl_visivel;
	
	/**
	 * Contem o id da entidade da funcionalidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $st_funcionalidade
	 */
	public function getSt_funcionalidade() {
		return $this->st_funcionalidade;
	}

	/**
	 * @return the $id_funcionalidadepai
	 */
	public function getId_funcionalidadepai() {
		return $this->id_funcionalidadepai;
	}

	/**
	 * @return the $bl_funcionalidadeativo
	 */
	public function getBl_funcionalidadeativo() {
		return $this->bl_funcionalidadeativo;
	}

	/**
	 * @return the $id_situacaofuncionalidade
	 */
	public function getId_situacaofuncionalidade() {
		return $this->id_situacaofuncionalidade;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @return the $st_urlicone
	 */
	public function getSt_urlicone() {
		return $this->st_urlicone;
	}

	/**
	 * @return the $id_tipofuncionalidade
	 */
	public function getId_tipofuncionalidade() {
		return $this->id_tipofuncionalidade;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @return the $st_classeflexbotao
	 */
	public function getSt_classeflexbotao() {
		return $this->st_classeflexbotao;
	}

	/**
	 * @return the $bl_pesquisa
	 */
	public function getBl_pesquisa() {
		return $this->bl_pesquisa;
	}

	/**
	 * @return the $bl_entidadefuncionalidadeativo
	 */
	public function getBl_entidadefuncionalidadeativo() {
		return $this->bl_entidadefuncionalidadeativo;
	}

	/**
	 * @return the $id_situacaoentidadefuncionalidade
	 */
	public function getId_situacaoentidadefuncionalidade() {
		return $this->id_situacaoentidadefuncionalidade;
	}

	/**
	 * @return the $bl_obrigatorio
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}

	/**
	 * @return the $bl_visivel
	 */
	public function getBl_visivel() {
		return $this->bl_visivel;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param string $st_funcionalidade
	 */
	public function setSt_funcionalidade($st_funcionalidade) {
		$this->st_funcionalidade = $st_funcionalidade;
	}

	/**
	 * @param int $id_funcionalidadepai
	 */
	public function setId_funcionalidadepai($id_funcionalidadepai) {
		$this->id_funcionalidadepai = $id_funcionalidadepai;
	}

	/**
	 * @param int $bl_funcionalidadeativo
	 */
	public function setBl_funcionalidadeativo($bl_funcionalidadeativo) {
		$this->bl_funcionalidadeativo = $bl_funcionalidadeativo;
	}

	/**
	 * @param int $id_situacaofuncionalidade
	 */
	public function setId_situacaofuncionalidade($id_situacaofuncionalidade) {
		$this->id_situacaofuncionalidade = $id_situacaofuncionalidade;
	}

	/**
	 * @param string $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

	/**
	 * @param string $st_urlicone
	 */
	public function setSt_urlicone($st_urlicone) {
		$this->st_urlicone = $st_urlicone;
	}

	/**
	 * @param int $id_tipofuncionalidade
	 */
	public function setId_tipofuncionalidade($id_tipofuncionalidade) {
		$this->id_tipofuncionalidade = $id_tipofuncionalidade;
	}

	/**
	 * @param int $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param string $st_classeflexbotao
	 */
	public function setSt_classeflexbotao($st_classeflexbotao) {
		$this->st_classeflexbotao = $st_classeflexbotao;
	}

	/**
	 * @param int $bl_pesquisa
	 */
	public function setBl_pesquisa($bl_pesquisa) {
		$this->bl_pesquisa = $bl_pesquisa;
	}

	/**
	 * @param int $bl_entidadefuncionalidadeativo
	 */
	public function setBl_entidadefuncionalidadeativo($bl_entidadefuncionalidadeativo) {
		$this->bl_entidadefuncionalidadeativo = $bl_entidadefuncionalidadeativo;
	}

	/**
	 * @param int $id_situacaoentidadefuncionalidade
	 */
	public function setId_situacaoentidadefuncionalidade($id_situacaoentidadefuncionalidade) {
		$this->id_situacaoentidadefuncionalidade = $id_situacaoentidadefuncionalidade;
	}

	/**
	 * @param int $bl_obrigatorio
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}

	/**
	 * @param int $bl_visivel
	 */
	public function setBl_visivel($bl_visivel) {
		$this->bl_visivel = $bl_visivel;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}
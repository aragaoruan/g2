<?php
/**
 * tb_livroentidadeco encapsulamento
 * @package models
 * @subpackage to
 */
class LivroEntidadeTO extends Ead1_TO_Dinamico {

	public $id_livroEntidade;
	public $id_livro;
	public $id_entidade;
	
	/**
	 * @return the $id_livroEntidade
	 */
	public function getId_livroEntidade() {
		return $this->id_livroEntidade;
	}

	/**
	 * @return the $id_livro
	 */
	public function getId_livro() {
		return $this->id_livro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_livroEntidade
	 */
	public function setId_livroEntidade($id_livroEntidade) {
		$this->id_livroEntidade = $id_livroEntidade;
	}

	/**
	 * @param field_type $id_livro
	 */
	public function setId_livro($id_livro) {
		$this->id_livro = $id_livro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
	
}

?>
<?php
/**
 * Classe para encapsular os dados do documento de título de eleitor.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentoTituloDeEleitorTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o número do título de eleitor.
	 * @var string
	 */
	public $st_numero;
	
	/**
	 * Contém a zona do título de eleitor.
	 * @var string
	 */
	public $st_zona;
	
	/**
	 * Contém a área do título de eleitor.
	 * @var string
	 */
	public $st_area;
	
	/**
	 * Contém a data de expedição do título de eleitor.
	 * @var string
	 */
	public $dt_dataexpedicao;
	
	/**
	 * @return string
	 */
	public function getDt_dataexpedicao() {
		return $this->dt_dataexpedicao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_numero() {
		return $this->st_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSt_zona() {
		return $this->st_zona;
	}
	
	/**
	 * @return string
	 */
	public function getSt_area() {
		return $this->st_area;
	}
	
	/**
	 * @param string $dt_dataexpedicao
	 */
	public function setDt_dataexpedicao($dt_dataexpedicao) {
		$this->dt_dataexpedicao = $dt_dataexpedicao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string st_numeroo
	 */
	public function setSt_numero($st_numero) {
		$this->st_numero = $st_numero;
	}
	
	/**
	 * @param string st_zonaa
	 */
	public function setSt_zona($st_zona) {
		$this->st_zona = $st_zona;
	}
	
	/**
	 * @param string $st_area
	 */
	public function setSt_area($st_area) {
		$this->st_area = $st_area;
	}

}

?>
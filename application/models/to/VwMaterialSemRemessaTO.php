<?php
/**
 * Classe de VwMaterialSemRemessaTO
 * @package models
 * @subpackage to
 */
class VwMaterialSemRemessaTO extends Ead1_TO_Dinamico {

	public $id_projetopedagogico;
	public $id_itemdematerial;
	public $st_itemdematerial;
	public $id_disciplina;
	public $st_disciplina;
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @return the $st_itemdematerial
	 */
	public function getSt_itemdematerial() {
		return $this->st_itemdematerial;
	}

	/**
	 * @param field_type $st_itemdematerial
	 */
	public function setSt_itemdematerial($st_itemdematerial) {
		$this->st_itemdematerial = $st_itemdematerial;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados de perfil
 * @author edermariano
 * @package models
 * @subpackage to
 */
class PerfilTO extends Ead1_TO_Dinamico{
	
	const Aluno	= 58;
	const PERFIL_PEDAGOGICO_OBSERVADOR = 3;
	const PERFIL_PEDAGOGICO_SUPORTE = 7;
	
	/**
	 * Atributo que contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;

	/**
	 * Contém o id do perfil pai.
	 * @var int
	 */
	public $id_perfilpai;
	
	/**
	 * Contém o nome do perfil.
	 * @var String
	 */
	public $st_nomeperfil;
	
	/**
	 * Define se o perfil está ou não ativo.
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Define a situação do perfil.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Atributo que contém o id da Entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id da entidade classe.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * Diz se o perfil é padrão.
	 * @var boolean
	 */
	public $bl_padrao;
	
/**
	 * Id do usuário que cadastrou o perfil.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Data de cadastro do perfil.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Id do perfil pedagógico
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * Id do sistema do perfil
	 * @var int
	 */
	public $id_sistema;
	
	/**
	 * @return int
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}
	
	/**
	 * @param int $id_perfilpedagogico
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}
	
	/**
	 * @param int $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @param boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $id_perfilpai
	 */
	public function getId_perfilpai() {
		return $this->id_perfilpai;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $id_perfilpai the $id_perfilpai to set
	 */
	public function setId_perfilpai($id_perfilpai) {
		$this->id_perfilpai = $id_perfilpai;
	}

	/**
	 * @param $st_nomeperfil the $st_nomeperfil to set
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}
<?php

/**
 * Classe que encapsula os dados da tb_pa_dadoscontato
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PADadosContatoTO extends Ead1_TO_Dinamico {

    public $dt_cadastro;
    public $id_assuntoco;
    public $id_categoriaocorrencia;
    public $id_entidade;
    public $id_pa_dadoscontato;
    public $id_textoinstrucao;
    public $id_textomensagem;
    public $id_usuariocadastro;

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getId_assuntoco() {
        return $this->id_assuntoco;
    }

    public function setId_assuntoco($id_assuntoco) {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function getId_categoriaocorrencia() {
        return $this->id_categoriaocorrencia;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia) {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function getId_pa_dadoscontato() {
        return $this->id_pa_dadoscontato;
    }

    public function setId_pa_dadoscontato($id_pa_dadoscontato) {
        $this->id_pa_dadoscontato = $id_pa_dadoscontato;
    }

    public function getId_textoinstrucao() {
        return $this->id_textoinstrucao;
    }

    public function setId_textoinstrucao($id_textoinstrucao) {
        $this->id_textoinstrucao = $id_textoinstrucao;
    }

    public function getId_textomensagem() {
        return $this->id_textomensagem;
    }

    public function setId_textomensagem($id_textomensagem) {
        $this->id_textomensagem = $id_textomensagem;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

}

?>
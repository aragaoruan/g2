<?php
/**
 * Classe para encapsular os dados da tabela de propriedades dos campos do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class PropriedadeCampoRelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_camporelatorio;
	public $id_tipopropriedadecamporel;
	public $st_valor;
	
	/**
	 * @return the $id_camporelatorio
	 */
	public function getId_camporelatorio() {
		return $this->id_camporelatorio;
	}

	/**
	 * @return the $id_tipopropriedadecamporel
	 */
	public function getId_tipopropriedadecamporel() {
		return $this->id_tipopropriedadecamporel;
	}

	/**
	 * @return the $st_valor
	 */
	public function getSt_valor() {
		return $this->st_valor;
	}

	/**
	 * @param $id_camporelatorio the $id_camporelatorio to set
	 */
	public function setId_camporelatorio($id_camporelatorio) {
		$this->id_camporelatorio = $id_camporelatorio;
	}

	/**
	 * @param $id_tipopropriedadecamporel the $id_tipopropriedadecamporel to set
	 */
	public function setId_tipopropriedadecamporel($id_tipopropriedadecamporel) {
		$this->id_tipopropriedadecamporel = $id_tipopropriedadecamporel;
	}

	/**
	 * @param $st_valor the $st_valor to set
	 */
	public function setSt_valor($st_valor) {
		$this->st_valor = $st_valor;
	}

	
	
}
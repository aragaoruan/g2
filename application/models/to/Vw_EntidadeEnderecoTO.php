<?php
/**
 * Classe para encapsular os dados da view de endereço para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class Vw_EntidadeEnderecoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @var boolean
	 */
	public $bl_enderecoentidadepadrao;
	
	/**
	 * Contém o id do endereço.
	 * @var int
	 */
	public $id_endereco;
	
	/**
	 * Contém o id do tipo de endereço.
	 * @var int
	 */
	public $id_tipoendereco;
	
	/**
	 * Contém o tipo de endereço.
	 * @var string
	 */
	public $st_tipoendereco;
	
	/**
	 * Contém a categoria de endereço.
	 * @var string
	 */
	public $st_categoriaendereco;
	
	/**
	 * Contém o endereço.
	 * @var string
	 */
	public $st_endereco;
	
	/**
	 * Contém o complemento do endereço.
	 * @var string
	 */
	public $st_complemento;
	
	/**
	 * Contém o número do endereço.
	 * @var string
	 */
	public $nu_numero;
	
	/**
	 * Contém a cidade.
	 * @var string
	 */
	public $st_cidade;
	
	/**
	 * Contém o id do município.
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Contém o nome do município.
	 * @var string
	 */
	public $st_nomemunicipio;
	
	/**
	 * Contém a UF.
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * Contém a província do estado.
	 * @var string
	 */
	public $st_estadoprovincia;
	
	/**
	 * Contém o CEP.
	 * @var string
	 */
	public $st_cep;
	
	/**
	 * Diz se o endereço é padrão.
	 * @var boolean
	 */
	public $bl_enderecopadrao;
	
	/**
	 * Diz se o endereço é ativo.
	 * @var boolean
	 */
	public $bl_enderecoativo;
	
	/**
	 * @return boolean
	 */
	public function getBl_enderecoativo() {
		return $this->bl_enderecoativo;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_enderecoentidadepadrao() {
		return $this->bl_enderecoentidadepadrao;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_enderecopadrao() {
		return $this->bl_enderecopadrao;
	}
	
	/**
	 * @return int
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}
	
	/**
	 * @return string
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_categoriaendereco() {
		return $this->st_categoriaendereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}
	
	/**
	 * @return string
	 */
	public function getSt_estadoprovincia() {
		return $this->st_estadoprovincia;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoendereco() {
		return $this->st_tipoendereco;
	}
	
	/**
	 * @param boolean $bl_enderecoativo
	 */
	public function setBl_enderecoativo($bl_enderecoativo) {
		$this->bl_enderecoativo = $bl_enderecoativo;
	}
	
	/**
	 * @param boolean $bl_enderecoentidadepadrao
	 */
	public function setBl_enderecoentidadepadrao($bl_enderecoentidadepadrao) {
		$this->bl_enderecoentidadepadrao = $bl_enderecoentidadepadrao;
	}
	
	/**
	 * @param boolean $bl_enderecopadrao
	 */
	public function setBl_enderecopadrao($bl_enderecopadrao) {
		$this->bl_enderecopadrao = $bl_enderecopadrao;
	}
	
	/**
	 * @param int $id_endereco
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param int $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}
	
	/**
	 * @param string $st_numero
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}
	
	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param string $st_categoriaendereco
	 */
	public function setSt_categoriaendereco($st_categoriaendereco) {
		$this->st_categoriaendereco = $st_categoriaendereco;
	}
	
	/**
	 * @param string $st_cep
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}
	
	/**
	 * @param string $st_cidade
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}
	
	/**
	 * @param string $st_complemento
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}
	
	/**
	 * @param string $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}
	
	/**
	 * @param string $st_estadoprovincia
	 */
	public function setSt_estadoprovincia($st_estadoprovincia) {
		$this->st_estadoprovincia = $st_estadoprovincia;
	}
	
	/**
	 * @param string $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}
	
	/**
	 * @param string $st_tipoendereco
	 */
	public function setSt_tipoendereco($st_tipoendereco) {
		$this->st_tipoendereco = $st_tipoendereco;
	}

}

?>
<?php
class PagamentoCartaoTO extends Ead1_TO_Dinamico {
	
	/**
	 * Id da venda para buscar seus lançamentos
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * Id da bandeira utilizada para o pagamento ( VISA | MASTERCARD )
	 * @var int
	 */
	public $id_cartaobandeira;
	
	/**
	 * Id da operadora utilizada para o pagamento ( CIELO | REDECARD )
	 * @var int
	 */
	public $id_cartaooperadora;
	
	
	/**
	 * Id da transação gerada para o pagamento dos lançamentos
	 * @var int
	 */
	public $tid;
	
	public $cod_seguranca;
	
	public $num_cartao1;

	public $num_cartao2;

	public $num_cartao3;
	
	public $num_cartao4;
	
	public $ano_validade;
	
	public $mes_validade;
	
	public $valor_total;
	
	public $quantidade_parcela;
	
	/**
	 * Nome do portador do cartão
	 * @var string
	 */
	public $st_portadorcartao;
	
	/**
	 * Número do gateway junto com a locaweb
	 * @var int
	 */
	public $num_gateway;

	/**
	 * Número de afiliação junto com a locaweb
	 * @var int
	 */
	public $num_afiliacao;
	
	
}
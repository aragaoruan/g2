<?php


class AvaliacaoParametrosTO extends Ead1_TO_Dinamico {

	public $id_avaliacaoaluno;
	public $id_avaliacaoparametros;
	public $id_parametrotcc;
	public $nu_valor;
	
	
	/**
	 * @return the $id_avaliacaoaluno
	 */
	public function getId_avaliacaoaluno() {
		return $this->id_avaliacaoaluno;
	}

	/**
	 * @return the $id_avaliacaoparametros
	 */
	public function getId_avaliacaoparametros() {
		return $this->id_avaliacaoparametros;
	}

	/**
	 * @return the $id_parametrotcc
	 */
	public function getId_parametrotcc() {
		return $this->id_parametrotcc;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @param field_type $id_avaliacaoaluno
	 */
	public function setId_avaliacaoaluno($id_avaliacaoaluno) {
		$this->id_avaliacaoaluno = $id_avaliacaoaluno;
	}

	/**
	 * @param field_type $id_avaliacaoparametros
	 */
	public function setId_avaliacaoparametros($id_avaliacaoparametros) {
		$this->id_avaliacaoparametros = $id_avaliacaoparametros;
	}

	/**
	 * @param field_type $id_parametrotcc
	 */
	public function setId_parametrotcc($id_parametrotcc) {
		$this->id_parametrotcc = $id_parametrotcc;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$nu_valor = str_replace(',', '.', $nu_valor);
		if(!$nu_valor){
			$nu_valor = 0;
		}
		$this->nu_valor = number_format($nu_valor, 2, '.', '');
	}

	
	
	
}

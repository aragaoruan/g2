<?php
/**
 * Classe para encapsular os dados da tabela de tipos de propriedades dos campos do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class TipoPropriedadeCampoRelTO extends Ead1_TO_Dinamico{
	
	public $id_tipopropriedadecamporel;
	public $st_tipopropriedadecamporel;
	
	/**
	 * @return the $id_tipopropriedadecamporel
	 */
	public function getId_tipopropriedadecamporel() {
		return $this->id_tipopropriedadecamporel;
	}

	/**
	 * @return the $st_tipopropriedadecamporel
	 */
	public function getSt_tipopropriedadecamporel() {
		return $this->st_tipopropriedadecamporel;
	}

	/**
	 * @param $id_tipopropriedadecamporel the $id_tipopropriedadecamporel to set
	 */
	public function setId_tipopropriedadecamporel($id_tipopropriedadecamporel) {
		$this->id_tipopropriedadecamporel = $id_tipopropriedadecamporel;
	}

	/**
	 * @param $st_tipopropriedadecamporel the $st_tipopropriedadecamporel to set
	 */
	public function setSt_tipopropriedadecamporel($st_tipopropriedadecamporel) {
		$this->st_tipopropriedadecamporel = $st_tipopropriedadecamporel;
	}
}
<?php
/**
 * Classe para encapsular os dados de tag de produto
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 13/12/2012
 * 
 * @package models
 * @subpackage to
 */
class TagTO extends Ead1_TO_Dinamico {

	/**
	 * Chave primária da tabela.
	 * @var int 
	 */
	public $id_entidade;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_tag;
	
	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $st_tag;
	
	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $dt_cadastro;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return int
	 */
	public function getId_tag() {
		return $this->id_tag;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getSt_tag() {
		return $this->st_tag;
	}
	
	/**
	 * @return int
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @param int $id_tag
	 */
	public function setId_tag($id_tag) {
		$this->id_tag = $id_tag;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param int $st_tag
	 */
	public function setSt_tag($st_tag) {
		$this->st_tag = $st_tag;
	}
	
	/**
	 * @param int $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

}

?>
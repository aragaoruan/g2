<?php
/**
 * Classe para encapsular os dados de classe para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeClasseTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade classe.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * Contém a classe da entidade.
	 * @var string
	 */
	public $st_entidadeclasse;
	
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @return string
	 */
	public function getSt_entidadeclasse() {
		return $this->st_entidadeclasse;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	
	/**
	 * @param string $st_entidadeclasse
	 */
	public function setSt_entidadeclasse($st_entidadeclasse) {
		$this->st_entidadeclasse = $st_entidadeclasse;
	}

}

?>
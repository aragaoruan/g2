<?php

/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
class VwPessoaTO extends Ead1_TO_Dinamico {

    public $id_usuario;
    public $st_login;
    public $st_loginentidade;
    public $st_nomecompleto;
    public $st_cpf;
    public $id_registropessoa;
    public $st_senha;
    public $st_senhaentidade;
    public $id_entidade;
    public $st_sexo;
    public $dt_nascimento;
    public $id_email;
    public $st_email;
    public $bl_emailpadrao;
    public $id_telefone;
    public $bl_telefonepadrao;
    public $nu_telefone;
    public $id_tipotelefone;
    public $st_tipotelefone;
    public $nu_ddd;
    public $nu_ddi;
    public $st_rg;
    public $st_orgaoexpeditor;
    public $dt_dataexpedicao;
    public $id_endereco;
    public $bl_enderecopadrao;
    public $st_endereco;
    public $st_cep;
    public $st_bairro;
    public $st_complemento;
    public $nu_numero;
    public $st_estadoprovincia;
    public $st_cidade;
    public $st_cidadenascimento;
    public $id_pais;
    public $id_municipio;
    public $id_municipionascimento;
    public $id_tipoendereco;
    public $sg_uf;
    public $sg_ufnascimento;
    public $id_estadocivil;
    public $st_estadocivil;
    public $st_nomepais;
    public $st_nomemunicipio;
    public $nu_dddfixo;
    public $nu_telefonefixo;
    public $st_nomemae;
    public $st_nomepai;
    public $id_telefonealternativo;
    public $nu_telefonealternativo;
    public $id_tipotelefonealternativo;
    public $st_tipotelefonealternativo;
    public $nu_dddalternativo;
    public $nu_ddialternativo;

    /**
	 * @return the $st_loginentidade
	 */
	public function getSt_loginentidade() {
		return $this->st_loginentidade;
	}

	/**
	 * @return the $st_senhaentidade
	 */
	public function getSt_senhaentidade() {
		return $this->st_senhaentidade;
	}

	/**
	 * @param field_type $st_loginentidade
	 */
	public function setSt_loginentidade($st_loginentidade) {
		$this->st_loginentidade = $st_loginentidade;
		return $this;
	}

	/**
	 * @param field_type $st_senhaentidade
	 */
	public function setSt_senhaentidade($st_senhaentidade) {
		$this->st_senhaentidade = $st_senhaentidade;
		return $this;
	}

	/**
     * @return the $nu_dddfixo
     */
    public function getNu_dddfixo() {
        return $this->nu_dddfixo;
    }

    /**
     * @return the $nu_telefonefixo
     */
    public function getNu_telefonefixo() {
        return $this->nu_telefonefixo;
    }

    /**
     * @param field_type $nu_dddfixo
     */
    public function setNu_dddfixo($nu_dddfixo) {
        $this->nu_dddfixo = $nu_dddfixo;
    }

    /**
     * @param field_type $nu_telefonefixo
     */
    public function setNu_telefonefixo($nu_telefonefixo) {
        $this->nu_telefonefixo = $nu_telefonefixo;
    }

    /**
     * @return the $st_nomemunicipio
     */
    public function getSt_nomemunicipio() {
        return $this->st_nomemunicipio;
    }

    /**
     * @param field_type $st_nomemunicipio
     */
    public function setSt_nomemunicipio($st_nomemunicipio) {
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    /**
     * @return the $st_nomepais
     */
    public function getSt_nomepais() {
        return $this->st_nomepais;
    }

    /**
     * @param field_type $st_nomepais
     */
    public function setSt_nomepais($st_nomepais) {
        $this->st_nomepais = $st_nomepais;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @return the $st_login
     */
    public function getSt_login() {
        return $this->st_login;
    }

    /**
     * @return the $id_estadocivil
     */
    public function getId_estadocivil() {
        return $this->id_estadocivil;
    }

    /**
     * @return the $st_estadocivil
     */
    public function getSt_estadocivil() {
        return $this->st_estadocivil;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @return the $id_registropessoa
     */
    public function getId_registropessoa() {
        return $this->id_registropessoa;
    }

    /**
     * @return the $st_senha
     */
    public function getSt_senha() {
        return $this->st_senha;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $st_sexo
     */
    public function getSt_sexo() {
        return $this->st_sexo;
    }

    /**
     * @return the $dt_nascimento
     */
    public function getDt_nascimento() {
        return $this->dt_nascimento;
    }

    /**
     * @return the $id_email
     */
    public function getId_email() {
        return $this->id_email;
    }

    /**
     * @return the $st_email
     */
    public function getSt_email() {
        return $this->st_email;
    }

    /**
     * @return the $bl_emailpadrao
     */
    public function getBl_emailpadrao() {
        return $this->bl_emailpadrao;
    }

    /**
     * @return the $id_telefone
     */
    public function getId_telefone() {
        return $this->id_telefone;
    }

    /**
     * @return the $bl_telefonepadrao
     */
    public function getBl_telefonepadrao() {
        return $this->bl_telefonepadrao;
    }

    /**
     * @return the $nu_telefone
     */
    public function getNu_telefone() {
        return $this->nu_telefone;
    }

    /**
     * @return the $id_tipotelefone
     */
    public function getId_tipotelefone() {
        return $this->id_tipotelefone;
    }

    /**
     * @return the $st_tipotelefone
     */
    public function getSt_tipotelefone() {
        return $this->st_tipotelefone;
    }

    /**
     * @return the $nu_ddd
     */
    public function getNu_ddd() {
        return $this->nu_ddd;
    }

    /**
     * @return the $nu_ddi
     */
    public function getNu_ddi() {
        return $this->nu_ddi;
    }

    /**
     * @return the $st_rg
     */
    public function getSt_rg() {
        return $this->st_rg;
    }

    /**
     * @return the $st_orgaoexpeditor
     */
    public function getSt_orgaoexpeditor() {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @return the $dt_dataexpedicao
     */
    public function getDt_dataexpedicao() {
        return $this->dt_dataexpedicao;
    }

    /**
     * @return the $id_endereco
     */
    public function getId_endereco() {
        return $this->id_endereco;
    }

    /**
     * @return the $bl_enderecopadrao
     */
    public function getBl_enderecopadrao() {
        return $this->bl_enderecopadrao;
    }

    /**
     * @return the $st_endereco
     */
    public function getSt_endereco() {
        return $this->st_endereco;
    }

    /**
     * @return the $st_cep
     */
    public function getSt_cep() {
        return $this->st_cep;
    }

    /**
     * @return the $st_bairro
     */
    public function getSt_bairro() {
        return $this->st_bairro;
    }

    /**
     * @return the $st_complemento
     */
    public function getSt_complemento() {
        return $this->st_complemento;
    }

    /**
     * @return the $nu_numero
     */
    public function getNu_numero() {
        return $this->nu_numero;
    }

    /**
     * @return the $st_estadoprovincia
     */
    public function getSt_estadoprovincia() {
        return $this->st_estadoprovincia;
    }

    /**
     * @return the $st_cidade
     */
    public function getSt_cidade() {
        return $this->st_cidade;
    }

    /**
     * @return the $st_cidadenascimento
     */
    public function getSt_cidadenascimento() {
        return $this->st_cidadenascimento;
    }

    /**
     * @return the $id_pais
     */
    public function getId_pais() {
        return $this->id_pais;
    }

    /**
     * @return the $id_municipio
     */
    public function getId_municipio() {
        return $this->id_municipio;
    }

    /**
     * @return the $id_municipionascimento
     */
    public function getId_municipionascimento() {
        return $this->id_municipionascimento;
    }

    /**
     * @return the $id_tipoendereco
     */
    public function getId_tipoendereco() {
        return $this->id_tipoendereco;
    }

    /**
     * @return the $sg_uf
     */
    public function getSg_uf() {
        return $this->sg_uf;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_login
     */
    public function setSt_login($st_login) {
        $this->st_login = $st_login;
    }

    /**
     * @param field_type $id_estadocivil
     */
    public function setId_estadocivil($id_estadocivil) {
        $this->id_estadocivil = $id_estadocivil;
    }

    /**
     * @param field_type $st_estadocivil
     */
    public function setSt_estadocivil($st_estadocivil) {
        $this->st_estadocivil = $st_estadocivil;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param field_type $id_registropessoa
     */
    public function setId_registropessoa($id_registropessoa) {
        $this->id_registropessoa = $id_registropessoa;
    }

    /**
     * @param field_type $st_senha
     */
    public function setSt_senha($st_senha) {
        $this->st_senha = $st_senha;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $st_sexo
     */
    public function setSt_sexo($st_sexo) {
        $this->st_sexo = $st_sexo;
    }

    /**
     * @param field_type $dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento) {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @param field_type $id_email
     */
    public function setId_email($id_email) {
        $this->id_email = $id_email;
    }

    /**
     * @param field_type $st_email
     */
    public function setSt_email($st_email) {
        $this->st_email = $st_email;
    }

    /**
     * @param field_type $bl_emailpadrao
     */
    public function setBl_emailpadrao($bl_emailpadrao) {
        $this->bl_emailpadrao = $bl_emailpadrao;
    }

    /**
     * @param field_type $id_telefone
     */
    public function setId_telefone($id_telefone) {
        $this->id_telefone = $id_telefone;
    }

    /**
     * @param field_type $bl_telefonepadrao
     */
    public function setBl_telefonepadrao($bl_telefonepadrao) {
        $this->bl_telefonepadrao = $bl_telefonepadrao;
    }

    /**
     * @param field_type $nu_telefone
     */
    public function setNu_telefone($nu_telefone) {
        $this->nu_telefone = $nu_telefone;
    }

    /**
     * @param field_type $id_tipotelefone
     */
    public function setId_tipotelefone($id_tipotelefone) {
        $this->id_tipotelefone = $id_tipotelefone;
    }

    /**
     * @param field_type $st_tipotelefone
     */
    public function setSt_tipotelefone($st_tipotelefone) {
        $this->st_tipotelefone = $st_tipotelefone;
    }

    /**
     * @param field_type $nu_ddd
     */
    public function setNu_ddd($nu_ddd) {
        $this->nu_ddd = $nu_ddd;
    }

    /**
     * @param field_type $nu_ddi
     */
    public function setNu_ddi($nu_ddi) {
        $this->nu_ddi = $nu_ddi;
    }

    /**
     * @param field_type $st_rg
     */
    public function setSt_rg($st_rg) {
        $this->st_rg = $st_rg;
    }

    /**
     * @param field_type $st_orgaoexpeditor
     */
    public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
    }

    /**
     * @param field_type $dt_dataexpedicao
     */
    public function setDt_dataexpedicao($dt_dataexpedicao) {
        $this->dt_dataexpedicao = $dt_dataexpedicao;
    }

    /**
     * @param field_type $id_endereco
     */
    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }

    /**
     * @param field_type $bl_enderecopadrao
     */
    public function setBl_enderecopadrao($bl_enderecopadrao) {
        $this->bl_enderecopadrao = $bl_enderecopadrao;
    }

    /**
     * @param field_type $st_endereco
     */
    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @param field_type $st_cep
     */
    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
    }

    /**
     * @param field_type $st_bairro
     */
    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @param field_type $st_complemento
     */
    public function setSt_complemento($st_complemento) {
        $this->st_complemento = $st_complemento;
    }

    /**
     * @param field_type $nu_numero
     */
    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
    }

    /**
     * @param field_type $st_estadoprovincia
     */
    public function setSt_estadoprovincia($st_estadoprovincia) {
        $this->st_estadoprovincia = $st_estadoprovincia;
    }

    /**
     * @param field_type $st_cidade
     */
    public function setSt_cidade($st_cidade) {
        $this->st_cidade = $st_cidade;
    }

    /**
     * @param field_type $id_pais
     */
    public function setId_pais($id_pais) {
        $this->id_pais = $id_pais;
    }

    /**
     * @param field_type $id_municipio
     */
    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
    }

    /**
     * @param field_type $id_municipionascimento
     */
    public function setId_municipionascimento($id_municipionascimento) {
        $this->id_municipionascimento = $id_municipionascimento;
    }

    /**
     * @param field_type $id_tipoendereco
     */
    public function setId_tipoendereco($id_tipoendereco) {
        $this->id_tipoendereco = $id_tipoendereco;
    }

    /**
     * @param field_type $sg_uf
     */
    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @param field_type $sg_ufnascimento
     */
    public function setSg_ufnascimento($sg_ufnascimento) {
        $this->sg_ufnascimento = $sg_ufnascimento;
    }

    public function getSg_ufnascimento() {
        return $this->sg_ufnascimento;
    }

    public function setSt_cidadenascimento($st_cidadenascimento) {
        $this->st_cidadenascimento = $st_cidadenascimento;
    }

    public function getSt_nomemae() {
        return $this->st_nomemae;
    }

    public function getSt_nomepai() {
        return $this->st_nomepai;
    }

    public function setSt_nomemae($st_nomemae) {
        $this->st_nomemae = $st_nomemae;
    }

    public function setSt_nomepai($st_nomepai) {
        $this->st_nomepai = $st_nomepai;
    }

    public function getId_telefonealternativo() {
        return $this->id_telefonealternativo;
    }

    public function getNu_telefonealternativo() {
        return $this->nu_telefonealternativo;
    }

    public function getId_tipotelefonealternativo() {
        return $this->id_tipotelefonealternativo;
    }

    public function getSt_tipotelefonealternativo() {
        return $this->st_tipotelefonealternativo;
    }

    public function getNu_dddalternativo() {
        return $this->nu_dddalternativo;
    }

    public function getNu_ddialternativo() {
        return $this->nu_ddialternativo;
    }

    public function setId_telefonealternativo($id_telefonealternativo) {
        $this->id_telefonealternativo = $id_telefonealternativo;
    }

    public function setNu_telefonealternativo($nu_telefonealternativo) {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
    }

    public function setId_tipotelefonealternativo($id_tipotelefonealternativo) {
        $this->id_tipotelefonealternativo = $id_tipotelefonealternativo;
    }

    public function setSt_tipotelefonealternativo($st_tipotelefonealternativo) {
        $this->st_tipotelefonealternativo = $st_tipotelefonealternativo;
    }

    public function setNu_dddalternativo($nu_dddalternativo) {
        $this->nu_dddalternativo = $nu_dddalternativo;
    }

    public function setNu_ddialternativo($nu_ddialternativo) {
        $this->nu_ddialternativo = $nu_ddialternativo;
    }

}

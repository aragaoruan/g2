<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class FormaDiaVencimentoTO extends Ead1_TO_Dinamico{

	public $id_formadiavencimento;
	public $id_formapagamento;
	public $nu_diavencimento;


	/**
	 * @return the $id_formadiavencimento
	 */
	public function getId_formadiavencimento(){ 
		return $this->id_formadiavencimento;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento(){ 
		return $this->id_formapagamento;
	}

	/**
	 * @return the $nu_diavencimento
	 */
	public function getNu_diavencimento(){ 
		return $this->nu_diavencimento;
	}


	/**
	 * @param field_type $id_formadiavencimento
	 */
	public function setId_formadiavencimento($id_formadiavencimento){ 
		$this->id_formadiavencimento = $id_formadiavencimento;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento){ 
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $nu_diavencimento
	 */
	public function setNu_diavencimento($nu_diavencimento){ 
		$this->nu_diavencimento = $nu_diavencimento;
	}

}
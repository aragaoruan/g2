<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class ProdutoProjetoPedagogicoTO extends Ead1_TO_Dinamico{

	public $id_projetopedagogico;
	public $id_produto;
	public $id_entidade;
	public $nu_tempoacesso;
	public $id_contratoregra;


	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto(){ 
		return $this->id_produto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}


	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto){ 
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @param field_type $nu_tempoacesso
	 */
	public function setNu_tempoacesso($nu_tempoacesso){ 
		$this->nu_tempoacesso = $nu_tempoacesso;
	}

	/**
	 * @param field_type $nu_tempoacesso
	 */
	public function getNu_tempoacesso(){ 
		return $this->nu_tempoacesso;
	}

	/**
	 * @return mixed
	 */
	public function getId_contratoregra()
	{
		return $this->id_contratoregra;
	}

	/**
	 * @param mixed $id_contratoregra
	 */
	public function setId_contratoregra($id_contratoregra)
	{
		$this->id_contratoregra = $id_contratoregra;
	}


}
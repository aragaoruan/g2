<?php
/**
 * Classe para encapsular os dados de relacionamento entre área de conhecimento e item de material.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ItemDeMaterialAreaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de material.
	 * @var int
	 */
	public $id_itemdematerial;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

}

?>
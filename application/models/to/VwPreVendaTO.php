<?php

class VwPreVendaTO extends Ead1_TO_Dinamico {

	public $id_prevenda;
	public $id_formapagamento;
	public $st_formapagamento;
	public $id_meiopagamento;
	public $st_meiopagamento;
	public $id_usuario;
	public $id_entidade;
	public $st_entidade;
	public $st_nomecompleto;
	public $st_cpf;
	public $st_email;
	public $nu_telefonecelular;
	public $nu_dddcelular;
	public $nu_ddicelular;
	public $nu_telefoneresidencial;
	public $nu_dddresidencial;
	public $nu_ddiresidencial;
	public $nu_telefonecomercial;
	public $nu_dddcomercial;
	public $nu_ddicomercial;
	public $st_rg;
	public $st_orgaoexpeditor;
	public $id_pais;
	public $st_nomepais;
	public $sg_uf;
	public $id_municipio;
	public $st_nomemunicipio;
	public $st_cep;
	public $st_endereco;
	public $st_bairro;
	public $st_complemento;
	public $nu_numero;
	public $st_sexo;
	public $dt_nascimento;
	public $id_paisnascimento;
	public $st_nomepaisnascimento; 
	public $sg_ufnascimento;
	public $id_municipionascimento;
	public $st_nomemunicipionascimetno;
	public $dt_cadastro;
	public $nu_lancamentos;
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_municipionascimento() {
		return $this->id_municipionascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_pais() {
		return $this->id_pais;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_paisnascimento() {
		return $this->id_paisnascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_prevenda() {
		return $this->id_prevenda;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_dddcelular() {
		return $this->nu_dddcelular;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_dddcomercial() {
		return $this->nu_dddcomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_dddresidencial() {
		return $this->nu_dddresidencial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_ddicelular() {
		return $this->nu_ddicelular;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_ddicomercial() {
		return $this->nu_ddicomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_ddiresidencial() {
		return $this->nu_ddiresidencial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_lancamentos() {
		return $this->nu_lancamentos;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_rg() {
		return $this->st_rg;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_telefonecelular() {
		return $this->nu_telefonecelular;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_telefonecomercial() {
		return $this->nu_telefonecomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_telefoneresidencial() {
		return $this->nu_telefoneresidencial;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_ufnascimento() {
		return $this->sg_ufnascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_bairro() {
		return $this->st_bairro;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_email() {
		return $this->st_email;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_entidade() {
		return $this->st_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomemunicipionascimetno() {
		return $this->st_nomemunicipionascimetno;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomepais() {
		return $this->st_nomepais;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomepaisnascimento() {
		return $this->st_nomepaisnascimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_sexo() {
		return $this->st_sexo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param unknown_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param unknown_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param unknown_type $id_municipionascimento
	 */
	public function setId_municipionascimento($id_municipionascimento) {
		$this->id_municipionascimento = $id_municipionascimento;
	}
	
	/**
	 * @param unknown_type $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}
	
	/**
	 * @param unknown_type $id_paisnascimento
	 */
	public function setId_paisnascimento($id_paisnascimento) {
		$this->id_paisnascimento = $id_paisnascimento;
	}
	
	/**
	 * @param unknown_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda) {
		$this->id_prevenda = $id_prevenda;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $nu_dddcelular
	 */
	public function setNu_dddcelular($nu_dddcelular) {
		$this->nu_dddcelular = $nu_dddcelular;
	}
	
	/**
	 * @param unknown_type $nu_dddcomercial
	 */
	public function setNu_dddcomercial($nu_dddcomercial) {
		$this->nu_dddcomercial = $nu_dddcomercial;
	}
	
	/**
	 * @param unknown_type $nu_dddresidencial
	 */
	public function setNu_dddresidencial($nu_dddresidencial) {
		$this->nu_dddresidencial = $nu_dddresidencial;
	}
	
	/**
	 * @param unknown_type $nu_ddicelular
	 */
	public function setNu_ddicelular($nu_ddicelular) {
		$this->nu_ddicelular = $nu_ddicelular;
	}
	
	/**
	 * @param unknown_type $nu_ddicomercial
	 */
	public function setNu_ddicomercial($nu_ddicomercial) {
		$this->nu_ddicomercial = $nu_ddicomercial;
	}
	
	/**
	 * @param unknown_type $nu_ddiresidencial
	 */
	public function setNu_ddiresidencial($nu_ddiresidencial) {
		$this->nu_ddiresidencial = $nu_ddiresidencial;
	}
	
	/**
	 * @param unknown_type $nu_lancamentos
	 */
	public function setNu_lancamentos($nu_lancamentos) {
		$this->nu_lancamentos = $nu_lancamentos;
	}
	
	/**
	 * @param unknown_type $st_numero
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}
	
	/**
	 * @param unknown_type st_rgg
	 */
	public function setSt_rg($st_rg) {
		$this->st_rg = $st_rg;
	}
	
	/**
	 * @param unknown_type $st_telefonecelular
	 */
	public function setNu_telefonecelular($nu_telefonecelular) {
		$this->nu_telefonecelular = $nu_telefonecelular;
	}
	
	/**
	 * @param unknown_type $st_telefonecomercial
	 */
	public function setNu_telefonecomercial($nu_telefonecomercial) {
		$this->nu_telefonecomercial = $nu_telefonecomercial;
	}
	
	/**
	 * @param unknown_type $st_telefoneresidencial
	 */
	public function setNu_telefoneresidencial($nu_telefoneresidencial) {
		$this->nu_telefoneresidencial = $nu_telefoneresidencial;
	}
	
	/**
	 * @param unknown_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param unknown_type $sg_ufnascimento
	 */
	public function setSg_ufnascimento($sg_ufnascimento) {
		$this->sg_ufnascimento = $sg_ufnascimento;
	}
	
	/**
	 * @param unknown_type $st_bairro
	 */
	public function setSt_bairro($st_bairro) {
		$this->st_bairro = $st_bairro;
	}
	
	/**
	 * @param unknown_type $st_cep
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}
	
	/**
	 * @param unknown_type $st_complemento
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}
	
	/**
	 * @param unknown_type $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}
	
	/**
	 * @param unknown_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}
	
	/**
	 * @param unknown_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}
	
	/**
	 * @param unknown_type $st_entidade
	 */
	public function setSt_entidade($st_entidade) {
		$this->st_entidade = $st_entidade;
	}
	
	/**
	 * @param unknown_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}
	
	/**
	 * @param unknown_type $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}
	
	/**
	 * @param unknown_type $st_nomemunicipionascimetno
	 */
	public function setSt_nomemunicipionascimetno($st_nomemunicipionascimetno) {
		$this->st_nomemunicipionascimetno = $st_nomemunicipionascimetno;
	}
	
	/**
	 * @param unknown_type $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais) {
		$this->st_nomepais = $st_nomepais;
	}
	
	/**
	 * @param unknown_type $st_nomepaisnascimento
	 */
	public function setSt_nomepaisnascimento($st_nomepaisnascimento) {
		$this->st_nomepaisnascimento = $st_nomepaisnascimento;
	}
	
	/**
	 * @param unknown_type $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}
	
	/**
	 * @param unknown_type $st_sexo
	 */
	public function setSt_sexo($st_sexo) {
		$this->st_sexo = $st_sexo;
	}

}

?>
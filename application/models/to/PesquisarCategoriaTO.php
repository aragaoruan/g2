<?php
/**
 * @package model
 * @subpackage to
 */
class PesquisarCategoriaTO extends VwCategoriaTO {
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.CadastrarCategoria';
}

?>
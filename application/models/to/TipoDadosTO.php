<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoDadosTO extends Ead1_TO_Dinamico{
	
	const PESSOAS = 1;
	const LOGIN = 2;

	public $id_tipodados;
	public $st_tipodados;


	/**
	 * @return the $id_tipodados
	 */
	public function getId_tipodados(){ 
		return $this->id_tipodados;
	}

	/**
	 * @return the $st_tipodados
	 */
	public function getSt_tipodados(){ 
		return $this->st_tipodados;
	}


	/**
	 * @param field_type $id_tipodados
	 */
	public function setId_tipodados($id_tipodados){ 
		$this->id_tipodados = $id_tipodados;
	}

	/**
	 * @param field_type $st_tipodados
	 */
	public function setSt_tipodados($st_tipodados){ 
		$this->st_tipodados = $st_tipodados;
	}

}
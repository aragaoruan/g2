<?php
/**
 * Classe para encapsular os dados de contrato e matricula.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContratoMatriculaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o id do contrato
	 * @var int
	 */
	public $id_contrato;
	
	/**
	 * contem o id da Matricula
	 * @var int
	 */
	public $id_matricula;
	
	/**
	 * @var unknown_type
	 */
	public $bl_ativo;
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param int $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param int $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	
	
	
}
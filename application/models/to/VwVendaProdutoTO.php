<?php
/**
 * Classe para encapsular os dados da View vw_vendaproduto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class VwVendaProdutoTO extends Ead1_TO_Dinamico{
	
	public $id_vendaproduto;
	public $id_venda;
	public $id_entidade;
	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $st_tipoproduto;
	public $id_campanhacomercial;
	public $nu_valor;
	public $nu_valorliquido;
	public $nu_desconto;

	public $id_tiposelecao;
	public $st_tiposelecao;

	/**
	 * @return mixed
	 */
	public function getid_tiposelecao()
	{
		return $this->id_tiposelecao;
	}

	/**
	 * @param mixed $id_tiposelecao
	 */
	public function setid_tiposelecao($id_tiposelecao)
	{
		$this->id_tiposelecao = $id_tiposelecao;
		return $this;

	}

	/**
	 * @return mixed
	 */
	public function getst_tiposelecao()
	{
		return $this->st_tiposelecao;
	}

	/**
	 * @param mixed $st_tiposelecao
	 */
	public function setst_tiposelecao($st_tiposelecao)
	{
		$this->st_tiposelecao = $st_tiposelecao;
		return $this;

	}



	/**
	 * @return the $id_vendaproduto
	 */
	public function getId_vendaproduto() {
		return $this->id_vendaproduto;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @return the $nu_desconto
	 */
	public function getNu_desconto() {
		return $this->nu_desconto;
	}

	/**
	 * @param field_type $id_vendaproduto
	 */
	public function setId_vendaproduto($id_vendaproduto) {
		$this->id_vendaproduto = $id_vendaproduto;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param field_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

	/**
	 * @param field_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @param field_type $nu_desconto
	 */
	public function setNu_desconto($nu_desconto) {
		$this->nu_desconto = $nu_desconto;
	}

	
}
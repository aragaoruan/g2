<?php
/**
 * Classe para encapsular as situações por tabelas e campos de todo o DB.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class SituacaoTO extends Ead1_TO_Dinamico {

    const NOVO_PORTAL = 182;

	/**
	 * Contém o id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém o label da situação.
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * Contém a tabela que recebe essa situação.
	 * @var string
	 */
	public $st_tabela;
	
	/**
	 * Contém o campo na tabela de destino que recebe o valor da situação.
	 * @var string
	 */
	public $st_campo;
	
	/**
	 * Contém a descrição da situação.
	 * @var string
	 */
	public $st_descricaosituacao;
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_campo() {
		return $this->st_campo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaosituacao() {
		return $this->st_descricaosituacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tabela() {
		return $this->st_tabela;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param string $st_campo
	 */
	public function setSt_campo($st_campo) {
		$this->st_campo = $st_campo;
	}
	
	/**
	 * @param string $st_descricaosituacao
	 */
	public function setSt_descricaosituacao($st_descricaosituacao) {
		$this->st_descricaosituacao = $st_descricaosituacao;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
	
	/**
	 * @param string $st_tabela
	 */
	public function setSt_tabela($st_tabela) {
		$this->st_tabela = $st_tabela;
	}

}

?>
<?php
/**
 * Classe de valor representando a tabela tb_cartaobandeira
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class CartaoBandeiraTO extends Ead1_TO_Dinamico {
	
	
	const BANDEIRA_VISA 		= 1;
	const BANDEIRA_MASTERCARD 	= 2;
	const BANDEIRA_AMEX 		= 3;
	const BANDEIRA_DINERS 		= 4;
	const BANDEIRA_ELO 		= 5;

	public $id_cartaobandeira;
	public $st_cartaobandeira;
	
	/**
	 * @return the $id_cartaobandeira
	 */
	public function getId_cartaobandeira() {
		return $this->id_cartaobandeira;
	}

	/**
	 * @return the $st_cartaobandeira
	 */
	public function getSt_cartaobandeira() {
		return $this->st_cartaobandeira;
	}

	/**
	 * @param $id_cartaobandeira the $id_cartaobandeira to set
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}

	/**
	 * @param $st_cartaobandeira the $st_cartaobandeira to set
	 */
	public function setSt_cartaobandeira($st_cartaobandeira) {
		$this->st_cartaobandeira = $st_cartaobandeira;
	}

}
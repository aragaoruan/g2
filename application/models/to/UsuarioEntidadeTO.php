<?php
/**
 * Classe de valor para representar o usuário vinculado há uma determinada entidade
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class UsuarioEntidadeTO extends UsuarioTO 
{
	public $st_nomeentidade;
	public $id_entidade;
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade(){

		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){

		return $this->id_entidade;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade( $st_nomeentidade ){

		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade( $id_entidade ){

		$this->id_entidade = $id_entidade;
	}

}
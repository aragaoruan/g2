<?php
/**
 * Classe para encapsular os dados de pesquisa de aproveitamento
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class PesquisarAproveitamentoTO extends Ead1_TO_Dinamico {

	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.CadastrarAproveitamento';
	
	public $id_disciplina;
	
	public $st_disciplinaoriginal;
	
	public $id_aproveitamentodisciplina;
	
	public $id_matricula;
	
	public $id_usuario;
	
	public $st_nomecompleto;
	
	public $st_notaoriginal;
	
	public $id_entidadematricula;
	
	public $id_entidade;
	
	public $st_tituloexibicao;
	
	/**
	 * @return unknown
	 */
	public function getId_entidadematricula() {
		return $this->id_entidadematricula;
	}
	
	/**
	 * @return unknown
	 */
	public function setId_entidadematricula( $idEntidadeMatricula ) {
		$this->id_entidadematricula	= $idEntidadeMatricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @return unknown
	 */
	public function setSt_tituloexibicao( $st_tituloexibicao ) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_aproveitamentodisciplina() {
		return $this->id_aproveitamentodisciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplinaoriginal() {
		return $this->st_disciplinaoriginal;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_notaoriginal() {
		return $this->st_notaoriginal;
	}
	
	/**
	 * @param unknown_type $id_aproveitamento
	 */
	public function setId_aproveitamentodisciplina($id_aproveitamentodisciplina) {
		$this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $st_disciplinaoriginal
	 */
	public function setSt_disciplinaoriginal($st_disciplinaoriginal) {
		$this->st_disciplinaoriginal = $st_disciplinaoriginal;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_notaoriginal
	 */
	public function setSt_notaoriginal($st_notaoriginal) {
		$this->st_notaoriginal = $st_notaoriginal;
	}
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}


}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VendaEnvolvidoTO extends Ead1_TO_Dinamico{

	public $id_vendaenvolvido;
	public $id_venda;
	public $id_usuario;
	public $id_nucleotm;
	public $id_funcao;
	public $bl_ativo;
	
	/**
	 * @return the $id_vendaenvolvido
	 */
	public function getId_vendaenvolvido() {
		return $this->id_vendaenvolvido;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_nucleotm
	 */
	public function getId_nucleotm() {
		return $this->id_nucleotm;
	}

	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_vendaenvolvido
	 */
	public function setId_vendaenvolvido($id_vendaenvolvido) {
		$this->id_vendaenvolvido = $id_vendaenvolvido;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm) {
		$this->id_nucleotm = $id_nucleotm;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
}
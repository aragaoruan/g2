<?php
class ImportacaoTO extends Ead1_TO_Dinamico{
	
	const TB_PESSOAIMPORTACAO 				= 'tb_pessoaimportacao';
	const TB_MATRICULAIMPORTACAO 			= 'tb_matriculaimportacao';
	const TB_APROVEITAMENTOIMPORTACAO 		= 'tb_aproveitamentoimportacao';
	const TB_AVALICAOIMPORTACAO 			= 'tb_avaliacaoimportacao';
	const TB_DOCUMENTOENTREGUEIMPORTACAO 	= 'tb_documentoentregueimportacao';
	const TB_LANCAMENTOIMPORTACAO 			= 'tb_lancamentoimportacao';
	const TB_RESPONSAVELIMPORTACAO 			= 'tb_responsavelimportacao';
	const TB_HISTORICOSITUACAOIMPORTACAO 	= 'tb_historicosituacaoimportacao';
	const TB_TIPOLANCAMENTOIMPORTACAO 		= 'tb_tipolancamentoimportacao';
	const TB_PROJETODISCIPLINAIMPORTACAO 	= 'tb_projetodisciplinaimportacao';
	
	public $st_tabela;
	public $bi_data;
	public $st_extencao;
	public $id_sistemaimportacao;
	
	/**
	 * @return the $st_tabela
	 */
	public function getSt_tabela() {
		return $this->st_tabela;
	}

	/**
	 * @return the $bi_data
	 */
	public function getBi_data() {
		return $this->bi_data;
	}

	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @param field_type $st_tabela
	 */
	public function setSt_tabela($st_tabela) {
		$this->st_tabela = $st_tabela;
	}

	/**
	 * @param field_type $bi_data
	 */
	public function setBi_data($bi_data) {
		$this->bi_data = $bi_data;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}
	/**
	 * @return the $st_extencao
	 */
	public function getSt_extensao() {
		return $this->st_extencao;
	}

	/**
	 * @param field_type $st_extencao
	 */
	public function setSt_extencao($st_extencao) {
		$this->st_extencao = $st_extencao;
	}


	
}
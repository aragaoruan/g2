<?php
/**
 * Classe para encapsular os dados de definição de relações entre entidades.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeRelacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade filha.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id da entidade pai.
	 * @var int
	 */
	public $id_entidadepai;
	
	/**
	 * Contém o id da entidade classe.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_entidadepai
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}

}

?>
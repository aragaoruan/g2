<?php
/**
 * Classe para encapsular os dados da view de endereço para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeEnderecoTO extends Ead1_TO_Dinamico {

	public $id_entidadeendereco;
	public $id_entidade;
	public $bl_enderecoentidadepadrao;
	public $id_endereco;
	public $id_tipoendereco;
	public $st_tipoendereco;
	public $st_endereco;
	public $st_complemento;
	public $nu_numero;
	public $st_cidade;
	public $id_municipio;
	public $st_nomemunicipio;
	public $sg_uf;
	public $st_estadoprovincia;
	public $st_cep;
	public $bl_ativo;
	public $st_nomepais;
	public $id_pais;
	public $st_bairro;
	
	/**
	 * @return the $id_entidadeendereco
	 */
	public function getId_entidadeendereco() {
		return $this->id_entidadeendereco;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_enderecoentidadepadrao
	 */
	public function getBl_enderecoentidadepadrao() {
		return $this->bl_enderecoentidadepadrao;
	}

	/**
	 * @return the $id_endereco
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}

	/**
	 * @return the $id_tipoendereco
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}

	/**
	 * @return the $st_tipoendereco
	 */
	public function getSt_tipoendereco() {
		return $this->st_tipoendereco;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento() {
		return $this->st_complemento;
	}

	/**
	 * @return the $st_numero
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}

	/**
	 * @return the $st_cidade
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}

	/**
	 * @return the $st_nomemunicipio
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $st_estadoprovincia
	 */
	public function getSt_estadoprovincia() {
		return $this->st_estadoprovincia;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep() {
		return $this->st_cep;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_nomepais
	 */
	public function getSt_nomepais() {
		return $this->st_nomepais;
	}

	/**
	 * @return the $id_pais
	 */
	public function getId_pais() {
		return $this->id_pais;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro() {
		return $this->st_bairro;
	}

	/**
	 * @param field_type $id_entidadeendereco
	 */
	public function setId_entidadeendereco($id_entidadeendereco) {
		$this->id_entidadeendereco = $id_entidadeendereco;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_enderecoentidadepadrao
	 */
	public function setBl_enderecoentidadepadrao($bl_enderecoentidadepadrao) {
		$this->bl_enderecoentidadepadrao = $bl_enderecoentidadepadrao;
	}

	/**
	 * @param field_type $id_endereco
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}

	/**
	 * @param field_type $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}

	/**
	 * @param field_type $st_tipoendereco
	 */
	public function setSt_tipoendereco($st_tipoendereco) {
		$this->st_tipoendereco = $st_tipoendereco;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_complemento
	 */
	public function setSt_complemento($st_complemento) {
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @param field_type $st_numero
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param field_type $st_cidade
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param field_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $st_estadoprovincia
	 */
	public function setSt_estadoprovincia($st_estadoprovincia) {
		$this->st_estadoprovincia = $st_estadoprovincia;
	}

	/**
	 * @param field_type $st_cep
	 */
	public function setSt_cep($st_cep) {
		$this->st_cep = $st_cep;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais) {
		$this->st_nomepais = $st_nomepais;
	}

	/**
	 * @param field_type $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}

	/**
	 * @param field_type $st_bairro
	 */
	public function setSt_bairro($st_bairro) {
		$this->st_bairro = $st_bairro;
	}

	
}

?>
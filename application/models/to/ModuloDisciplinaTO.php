<?php
/**
 * Classe para encapsular os dados de relacionamento entre disciplina e módulo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ModuloDisciplinaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do módulo.
	 * @var int
	 */
	public $id_modulo;

	
	/**
	 * Contém o id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Contém o id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Contém o id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;

	/**
	 * Exclusão lógico.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Define se a disciplina é pré requisito.
	 * @var boolean
	 */
	public $bl_obrigatoria;
	
	/**
	 * Contem o id da area de conhecimento
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Contem o numero da ordem
	 * @var int
	 */
	public $nu_ordem;
	
	public $nu_ponderacaocalculada;
	public $nu_ponderacaoaplicada;
	public $id_usuarioponderacao;
	public $dt_cadastroponderacao;
	public $id_modulodisciplina;
	public $nu_importancia;

    public $nu_cargahoraria;
    public $nu_obrigatorioalocacao;
    public $nu_disponivelapartirdo;
    public $nu_percentualsemestre;
	
	/**
	 * @return the $nu_ponderacaocalculada
	 */
	public function getNu_ponderacaocalculada() {
		return $this->nu_ponderacaocalculada;
	}

	/**
	 * @return the $nu_ponderacaoaplicada
	 */
	public function getNu_ponderacaoaplicada() {
		return $this->nu_ponderacaoaplicada;
	}

	/**
	 * @return the $id_usuarioponderacao
	 */
	public function getId_usuarioponderacao() {
		return $this->id_usuarioponderacao;
	}

	/**
	 * @return the $dt_cadastroponderacao
	 */
	public function getDt_cadastroponderacao() {
		return $this->dt_cadastroponderacao;
	}

	/**
	 * @return the $id_modulodisciplina
	 */
	public function getId_modulodisciplina() {
		return $this->id_modulodisciplina;
	}

	/**
	 * @return the $nu_importancia
	 */
	public function getNu_importancia() {
		return $this->nu_importancia;
	}

	/**
	 * @param field_type $nu_ponderacaocalculada
	 */
	public function setNu_ponderacaocalculada($nu_ponderacaocalculada) {
		$this->nu_ponderacaocalculada = $nu_ponderacaocalculada;
		return $this;
	}

	/**
	 * @param field_type $nu_ponderacaoaplicada
	 */
	public function setNu_ponderacaoaplicada($nu_ponderacaoaplicada) {
		$this->nu_ponderacaoaplicada = $nu_ponderacaoaplicada;
		return $this;
	}

	/**
	 * @param field_type $id_usuarioponderacao
	 */
	public function setId_usuarioponderacao($id_usuarioponderacao) {
		$this->id_usuarioponderacao = $id_usuarioponderacao;
		return $this;
	}

	/**
	 * @param field_type $dt_cadastroponderacao
	 */
	public function setDt_cadastroponderacao($dt_cadastroponderacao) {
		$this->dt_cadastroponderacao = $dt_cadastroponderacao;
		return $this;
	}

	/**
	 * @param field_type $id_modulodisciplina
	 */
	public function setId_modulodisciplina($id_modulodisciplina) {
		$this->id_modulodisciplina = $id_modulodisciplina;
		return $this;
	}

	/**
	 * @param field_type $nu_importancia
	 */
	public function setNu_importancia($nu_importancia) {
		$this->nu_importancia = $nu_importancia;
		return $this;
	}

	/**
	 * @return the $bl_obrigatoria
	 */
	public function getBl_obrigatoria() {
		return $this->bl_obrigatoria;
	}

	/**
	 * @param boolean $bl_obrigatoria
	 */
	public function setBl_obrigatoria($bl_obrigatoria) {
		$this->bl_obrigatoria = $bl_obrigatoria;
	}

	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param int $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

    /**
     * @param mixed $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return mixed
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    public function getNu_obrigatorioalocacao()
    {
        return $this->nu_obrigatorioalocacao;
    }

    public function setNu_obrigatorioalocacao($nu_obrigatorioalocacao)
    {
        $this->nu_obrigatorioalocacao = $nu_obrigatorioalocacao;
    }

    
    public function getNu_disponivelapartirdo()
    {
        return $this->nu_disponivelapartirdo;
    }

    public function setNu_disponivelapartirdo($nu_disponivelapartirdo)
    {
        $this->nu_disponivelapartirdo = $nu_disponivelapartirdo;
    }

    /**
     * @return mixed
     */
    public function getNu_percentualsemestre()
    {
        return $this->nu_percentualsemestre;
    }

    /**
     * @param mixed $nu_percentualsemestre
     */
    public function setNu_percentualsemestre($nu_percentualsemestre)
    {
        $this->nu_percentualsemestre = $nu_percentualsemestre;
    }





}

?>
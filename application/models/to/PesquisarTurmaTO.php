<?php
/**
 * Classe para encapsular os dados de pesquisa de Turma
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PesquisarTurmaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarTurma';
	
	public $id_turma;
	public $id_usuariocadastro;
	public $id_situacao;
	public $st_situacao;
	public $id_entidadecadastro;
	public $st_turma;
	public $st_tituloexibicao;
	public $nu_maxalunos;
	public $dt_inicioinscricao;
	public $dt_fiminscricao;
	public $dt_inicio;
	public $dt_fim;
	public $dt_cadastro;
	public $bl_ativo;


	/**
	 * @return the $id_turma
	 */
	public function getId_turma(){ 
		return $this->id_turma;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro(){ 
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $st_turma
	 */
	public function getSt_turma(){ 
		return $this->st_turma;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao(){ 
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $nu_maxalunos
	 */
	public function getNu_maxalunos(){ 
		return $this->nu_maxalunos;
	}

	/**
	 * @return the $dt_inicioinscricao
	 */
	public function getDt_inicioinscricao(){ 
		return $this->dt_inicioinscricao;
	}

	/**
	 * @return the $dt_fiminscricao
	 */
	public function getDt_fiminscricao(){ 
		return $this->dt_fiminscricao;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio(){ 
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim(){ 
		return $this->dt_fim;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma){ 
		$this->id_turma = $id_turma;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro){ 
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $st_turma
	 */
	public function setSt_turma($st_turma){ 
		$this->st_turma = $st_turma;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao){ 
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $nu_maxalunos
	 */
	public function setNu_maxalunos($nu_maxalunos){ 
		$this->nu_maxalunos = $nu_maxalunos;
	}

	/**
	 * @param field_type $dt_inicioinscricao
	 */
	public function setDt_inicioinscricao($dt_inicioinscricao){ 
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}

	/**
	 * @param field_type $dt_fiminscricao
	 */
	public function setDt_fiminscricao($dt_fiminscricao){ 
		$this->dt_fiminscricao = $dt_fiminscricao;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio){ 
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim){ 
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
}
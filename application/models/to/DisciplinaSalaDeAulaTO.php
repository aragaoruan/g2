<?php
/**
 * Classe para encapsular os dados de relacionamento entre disciplina e sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DisciplinaSalaDeAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id da sala de aula.
	 * @var int 
	 */
	public $id_saladeaula;
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados campanha comercial.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaComercialTO extends Ead1_TO_Dinamico {

	/**
	 * Id da campanha comercial.
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * Nome da campanha comercial.
	 * @var string
	 */
	public $st_campanhacomercial;
	
	/**
	 * Id do usuário que cadastrou a campanha.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Data de cadastro da campanha.
	 * @var String
	 */
	public $dt_cadastro;

	/**
	 * Data de início da campanha.
	 * @var String
	 */
	public $dt_inicio;
	
	/**
	 * Data de término da campanha.
	 * @var String
	 */
	public $dt_fim;
	
	/**
	 * Diz se vai aplicar desconto a campanha.
	 * @var Boolean
	 */
	public $bl_aplicardesconto;
	
	/**
	 * Diz se a disponibilidade da campanha é definida ou indefinida.
	 * @var Boolean
	 */
	public $bl_disponibilidade;
	
	/**
	 * Número da disponibilidade definida da campanha.
	 * @var int
	 */
	public $nu_disponibilidade;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Id do tipo de desconto.
	 * @var int
	 */
	public $id_tipodesconto;
	
	/**
	 * Descrição da campanha.
	 * @var String
	 */
	public $st_descricao;
	
	/**
	 * @var Boolean
	 */
	public $bl_ativo;
	/**
	 * @var Boolean
	 */
	public $bl_todosprodutos;
	
	/**
	 * @var Boolean
	 */
	public $bl_todasformas;
	
	/**
	 * @var int
	 */
	public $id_tipocampanha;

	public $nu_diasprazo;
	
	/**
	 * @return the $nu_diasprazo
	 */
	public function getNu_diasprazo() {
		return $this->nu_diasprazo;
	}

	/**
	 * @param field_type $nu_diasprazo
	 */
	public function setNu_diasprazo($nu_diasprazo) {
		$this->nu_diasprazo = $nu_diasprazo;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId_tipocampanha() {
		return $this->id_tipocampanha;
	}
	
	/**
	 * @param int $id_tipocampanha
	 */
	public function setId_tipocampanha($id_tipocampanha) {
		$this->id_tipocampanha = $id_tipocampanha;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_todasformas() {
		return $this->bl_todasformas;
	}
	
	/**
	 * @param unknown_type $bl_todasformas
	 */
	public function setBl_todasformas($bl_todasformas) {
		$this->bl_todasformas = $bl_todasformas;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_todosprodutos() {
		return $this->bl_todosprodutos;
	}
	
	/**
	 * @param unknown_type $bl_todosprodutos
	 */
	public function setBl_todosprodutos($bl_todosprodutos) {
		$this->bl_todosprodutos = $bl_todosprodutos;
	}
	/**
	 * @return Boolean
	 */
	public function getBl_aplicardesconto() {
		return $this->bl_aplicardesconto;
	}
	
	/**
	 * @return Boolean
	 */
	public function getBl_disponibilidade() {
		return $this->bl_disponibilidade;
	}
	
	/**
	 * @return String
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return String
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}
	
	/**
	 * @return String
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodesconto() {
		return $this->id_tipodesconto;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getNu_disponibilidade() {
		return $this->nu_disponibilidade;
	}
	
	/**
	 * @return String
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param Boolean $bl_aplicardesconto
	 */
	public function setBl_aplicardesconto($bl_aplicardesconto) {
		$this->bl_aplicardesconto = $bl_aplicardesconto;
	}
	
	/**
	 * @param Boolean $bl_disponibilidade
	 */
	public function setBl_disponibilidade($bl_disponibilidade) {
		$this->bl_disponibilidade = $bl_disponibilidade;
	}
	
	/**
	 * @param String $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param String $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}
	
	/**
	 * @param String $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_tipodesconto
	 */
	public function setId_tipodesconto($id_tipodesconto) {
		$this->id_tipodesconto = $id_tipodesconto;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param int $nu_disponibilidade
	 */
	public function setNu_disponibilidade($nu_disponibilidade) {
		$this->nu_disponibilidade = $nu_disponibilidade;
	}
	
	/**
	 * @param String $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	/**
	 * @return int
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return string
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}
	
	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param string $st_campanhacomercial
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}

}

?>
<?php

/**
 * Classe para encapsular dados do vw_salaconfiguracao
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwSalaConfiguracaoTO extends Ead1_TO_Dinamico {
	
	public $id_saladeaula;
	public $dt_cadastro;
	public $st_saladeaula;
	public $st_disciplina;
    public $id_disciplina;
	public $st_coordenadordisciplina;
	public $bl_ativa;
	public $id_usuariocadastro;
	public $id_modalidadesaladeaula;
	public $dt_inicioinscricao;
	public $dt_fiminscricao;
	public $dt_abertura;
	public $dt_encerramentoprofessor;
	public $st_localizacao;
	public $id_tiposaladeaula;
	public $dt_encerramento;
	public $id_entidade;
	public $id_periodoletivo;
	public $bl_usardoperiodoletivo;
	public $id_situacao;
	public $nu_maxalunos;
	public $nu_diasencerramento;
	public $bl_semencerramento;
	public $nu_diasaluno;
	public $bl_semdiasaluno;
	public $nu_alocados;
	public $nu_acesso;
	public $nu_semacesso;
	public $nu_maxextensao;
	public $nu_diasextensao;
	public $nu_semnota;
	public $nu_maiornota;
	public $nu_menornota;
	public $st_justificativaacima;
	public $st_justificativaabaixo;
	public $st_pontosnegativos;
	public $st_pontospositivos;
	public $st_pontosmelhorar;
	public $st_pontosresgate;
	public $st_conclusoesfinais;
	public $dt_encerramentocoordenador;
	public $dt_encerramentopedagogico;
	public $dt_encerramentofinanceiro;
	public $id_sistema;
	public $st_codsistemacurso; 
	public $id_tipodisciplina;
	
	
	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @param field_type $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @return the $st_coordenadordisciplina
	 */
	public function getSt_coordenadordisciplina() {
		return $this->st_coordenadordisciplina;
	}

	/**
	 * @param field_type $st_coordenadordisciplina
	 */
	public function setSt_coordenadordisciplina($st_coordenadordisciplina) {
		$this->st_coordenadordisciplina = $st_coordenadordisciplina;
	}

	/**
	 * @return the $st_pontosnegativos
	 */
	public function getSt_pontosnegativos() {
		return $this->st_pontosnegativos;
	}

	/**
	 * @param field_type $st_pontosnegativos
	 */
	public function setSt_pontosnegativos($st_pontosnegativos) {
		$this->st_pontosnegativos = $st_pontosnegativos;
	}

	/**
	 * @return the $st_pontospositivos
	 */
	public function getSt_pontospositivos() {
		return $this->st_pontospositivos;
	}

	/**
	 * @param field_type $st_pontospositivos
	 */
	public function setSt_pontospositivos($st_pontospositivos) {
		$this->st_pontospositivos = $st_pontospositivos;
	}

	/**
	 * @return the $st_pontosmelhorar
	 */
	public function getSt_pontosmelhorar() {
		return $this->st_pontosmelhorar;
	}

	/**
	 * @param field_type $st_pontosmelhorar
	 */
	public function setSt_pontosmelhorar($st_pontosmelhorar) {
		$this->st_pontosmelhorar = $st_pontosmelhorar;
	}

	/**
	 * @return the $st_pontosresgate
	 */
	public function getSt_pontosresgate() {
		return $this->st_pontosresgate;
	}

	/**
	 * @param field_type $st_pontosresgate
	 */
	public function setSt_pontosresgate($st_pontosresgate) {
		$this->st_pontosresgate = $st_pontosresgate;
	}

	/**
	 * @return the $st_conclusoesfinais
	 */
	public function getSt_conclusoesfinais() {
		return $this->st_conclusoesfinais;
	}

	/**
	 * @param field_type $st_conclusoesfinais
	 */
	public function setSt_conclusoesfinais($st_conclusoesfinais) {
		$this->st_conclusoesfinais = $st_conclusoesfinais;
	}

	/**
	 * @return the $bl_ativa
	 */
	public function getBl_ativa() {
		return $this->bl_ativa;
	}

	/**
	 * @param field_type $bl_ativa
	 */
	public function setBl_ativa($bl_ativa) {
		$this->bl_ativa = $bl_ativa;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_modalidadesaladeaula
	 */
	public function getId_modalidadesaladeaula() {
		return $this->id_modalidadesaladeaula;
	}

	/**
	 * @param field_type $id_modalidadesaladeaula
	 */
	public function setId_modalidadesaladeaula($id_modalidadesaladeaula) {
		$this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
	}

	/**
	 * @return the $dt_inicioinscricao
	 */
	public function getDt_inicioinscricao() {
		return $this->dt_inicioinscricao;
	}

	/**
	 * @param field_type $dt_inicioinscricao
	 */
	public function setDt_inicioinscricao($dt_inicioinscricao) {
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}

	/**
	 * @return the $dt_fiminscricao
	 */
	public function getDt_fiminscricao() {
		return $this->dt_fiminscricao;
	}

	/**
	 * @param field_type $dt_fiminscricao
	 */
	public function setDt_fiminscricao($dt_fiminscricao) {
		$this->dt_fiminscricao = $dt_fiminscricao;
	}

	/**
	 * @return the $dt_abertura
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}

	/**
	 * @param field_type $dt_abertura
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}

	/**
	 * @return the $dt_encerramentoprofessor
	 */
	public function getDt_encerramentoprofessor() {
		return $this->dt_encerramentoprofessor;
	}

	/**
	 * @param field_type $dt_encerramentoprofessor
	 */
	public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
		$this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
	}

	/**
	 * @return the $dt_encerramentocoordenador
	 */
	public function getDt_encerramentocoordenador() {
		return $this->dt_encerramentocoordenador;
	}

	/**
	 * @param field_type $dt_encerramentocoordenador
	 */
	public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
		$this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
	}

	/**
	 * @return the $dt_encerramentopedagogico
	 */
	public function getDt_encerramentopedagogico() {
		return $this->dt_encerramentopedagogico;
	}

	/**
	 * @param field_type $dt_encerramentopedagogico
	 */
	public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
		$this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
	}

	/**
	 * @return the $dt_encerramentofinanceiro
	 */
	public function getDt_encerramentofinanceiro() {
		return $this->dt_encerramentofinanceiro;
	}

	/**
	 * @param field_type $dt_encerramentofinanceiro
	 */
	public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
		$this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
	}

	/**
	 * @return the $st_localizacao
	 */
	public function getSt_localizacao() {
		return $this->st_localizacao;
	}

	/**
	 * @param field_type $st_localizacao
	 */
	public function setSt_localizacao($st_localizacao) {
		$this->st_localizacao = $st_localizacao;
	}

	/**
	 * @return the $st_justificativaacima
	 */
	public function getSt_justificativaacima() {
		return $this->st_justificativaacima;
	}

	/**
	 * @param field_type $st_justificativaacima
	 */
	public function setSt_justificativaacima($st_justificativaacima) {
		$this->st_justificativaacima = $st_justificativaacima;
	}

	/**
	 * @return the $st_justificativaabaixo
	 */
	public function getSt_justificativaabaixo() {
		return $this->st_justificativaabaixo;
	}

	/**
	 * @param field_type $st_justificativaabaixo
	 */
	public function setSt_justificativaabaixo($st_justificativaabaixo) {
		$this->st_justificativaabaixo = $st_justificativaabaixo;
	}

	/**
	 * @return the $id_tiposaladeaula
	 */
	public function getId_tiposaladeaula() {
		return $this->id_tiposaladeaula;
	}

	/**
	 * @param field_type $id_tiposaladeaula
	 */
	public function setId_tiposaladeaula($id_tiposaladeaula) {
		$this->id_tiposaladeaula = $id_tiposaladeaula;
	}

	/**
	 * @return the $dt_encerramento
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}

	/**
	 * @param field_type $dt_encerramento
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_periodoletivo
	 */
	public function getId_periodoletivo() {
		return $this->id_periodoletivo;
	}

	/**
	 * @param field_type $id_periodoletivo
	 */
	public function setId_periodoletivo($id_periodoletivo) {
		$this->id_periodoletivo = $id_periodoletivo;
	}

	/**
	 * @return the $bl_usardoperiodoletivo
	 */
	public function getBl_usardoperiodoletivo() {
		return $this->bl_usardoperiodoletivo;
	}

	/**
	 * @param field_type $bl_usardoperiodoletivo
	 */
	public function setBl_usardoperiodoletivo($bl_usardoperiodoletivo) {
		$this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $nu_maxalunos
	 */
	public function getNu_maxalunos() {
		return $this->nu_maxalunos;
	}

	/**
	 * @param field_type $nu_maxalunos
	 */
	public function setNu_maxalunos($nu_maxalunos) {
		$this->nu_maxalunos = $nu_maxalunos;
	}

	/**
	 * @return the $nu_diasencerramento
	 */
	public function getNu_diasencerramento() {
		return $this->nu_diasencerramento;
	}

	/**
	 * @param field_type $nu_diasencerramento
	 */
	public function setNu_diasencerramento($nu_diasencerramento) {
		$this->nu_diasencerramento = $nu_diasencerramento;
	}

	/**
	 * @return the $bl_semencerramento
	 */
	public function getBl_semencerramento() {
		return $this->bl_semencerramento;
	}

	/**
	 * @param field_type $bl_semencerramento
	 */
	public function setBl_semencerramento($bl_semencerramento) {
		$this->bl_semencerramento = $bl_semencerramento;
	}

	/**
	 * @return the $nu_diasaluno
	 */
	public function getNu_diasaluno() {
		return $this->nu_diasaluno;
	}

	/**
	 * @param field_type $nu_diasaluno
	 */
	public function setNu_diasaluno($nu_diasaluno) {
		$this->nu_diasaluno = $nu_diasaluno;
	}

	/**
	 * @return the $bl_semdiasaluno
	 */
	public function getBl_semdiasaluno() {
		return $this->bl_semdiasaluno;
	}

	/**
	 * @param field_type $bl_semdiasaluno
	 */
	public function setBl_semdiasaluno($bl_semdiasaluno) {
		$this->bl_semdiasaluno = $bl_semdiasaluno;
	}

	/**
	 * @return the $nu_alocados
	 */
	public function getNu_alocados() {
		return $this->nu_alocados;
	}

	/**
	 * @param field_type $nu_alocados
	 */
	public function setNu_alocados($nu_alocados) {
		$this->nu_alocados = $nu_alocados;
	}

	/**
	 * @return the $nu_acesso
	 */
	public function getNu_acesso() {
		return $this->nu_acesso;
	}

	/**
	 * @param field_type $nu_acesso
	 */
	public function setNu_acesso($nu_acesso) {
		$this->nu_acesso = $nu_acesso;
	}

	/**
	 * @return the $nu_semacesso
	 */
	public function getNu_semacesso() {
		return $this->nu_semacesso;
	}

	/**
	 * @param field_type $nu_semacesso
	 */
	public function setNu_semacesso($nu_semacesso) {
		$this->nu_semacesso = $nu_semacesso;
	}

	/**
	 * @return the $nu_maxextensao
	 */
	public function getNu_maxextensao() {
		return $this->nu_maxextensao;
	}

	/**
	 * @param field_type $nu_maxextensao
	 */
	public function setNu_maxextensao($nu_maxextensao) {
		$this->nu_maxextensao = $nu_maxextensao;
	}

	/**
	 * @return the $nu_diasextensao
	 */
	public function getNu_diasextensao() {
		return $this->nu_diasextensao;
	}

	/**
	 * @param field_type $nu_diasextensao
	 */
	public function setNu_diasextensao($nu_diasextensao) {
		$this->nu_diasextensao = $nu_diasextensao;
	}
	/**
	 * @return the $nu_semnota
	 */
	public function getNu_semnota() {
		return $this->nu_semnota;
	}

	/**
	 * @param field_type $nu_semnota
	 */
	public function setNu_semnota($nu_semnota) {
		$this->nu_semnota = $nu_semnota;
	}

	/**
	 * @return the $nu_maiornota
	 */
	public function getNu_maiornota() {
		return $this->nu_maiornota;
	}

	/**
	 * @param field_type $nu_maiornota
	 */
	public function setNu_maiornota($nu_maiornota) {
		$this->nu_maiornota = $nu_maiornota;
	}

	/**
	 * @return the $nu_menornota
	 */
	public function getNu_menornota() {
		return $this->nu_menornota;
	}

	/**
	 * @param field_type $nu_menornota
	 */
	public function setNu_menornota($nu_menornota) {
		$this->nu_menornota = $nu_menornota;
	}
	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $st_codsistemacurso
	 */
	public function getSt_codsistemacurso() {
		return $this->st_codsistemacurso;
	}

	/**
	 * @param field_type $st_codsistemacurso
	 */
	public function setSt_codsistemacurso($st_codsistemacurso) {
		$this->st_codsistemacurso = $st_codsistemacurso;
	}

    /**
     * @param mixed $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return mixed
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }




	
	
	
	
}

?>
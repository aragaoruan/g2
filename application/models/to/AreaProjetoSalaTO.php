<?php
/**
 * Classe para encapsular os dados de relacionamento entre área, projeto e sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 18/10/2010
 * 
 * @package models
 * @subpackage to
 */
class AreaProjetoSalaTO extends Ead1_TO_Dinamico {

	/**
	 * Chave primária da tabela.
	 * @var int 
	 */
	public $id_areaprojetosala;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;

	public $st_referencia;

    /**
     * Quantidade de dias que vai ter dentro daquele projeto
     * @var int
    */
    public $nu_diasacesso;

    /**
     * @param mixed $st_referencia
     */
    public function setSt_referencia($st_referencia)
    {
        $this->st_referencia = $st_referencia;
    }

    /**
     * @return mixed
     */
    public function getSt_referencia()
    {
        return $this->st_referencia;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaprojetosala
     */
    public function setId_areaprojetosala($id_areaprojetosala)
    {
        $this->id_areaprojetosala = $id_areaprojetosala;
    }

    /**
     * @return int
     */
    public function getId_areaprojetosala()
    {
        return $this->id_areaprojetosala;
    }

    /**
     * @param int $id_nivelensino
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
    }

    /**
     * @return int
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $nu_diasacesso
     */
    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
    }

    /**
     * @return int
     */
    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }




}
?>
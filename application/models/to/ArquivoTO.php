<?php
/**
 * Classe de valor para upload de arquivos 
 * Deve ser utilizada quando houver a necessidade de upload de arquivos entre o cliente e o servidor
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class ArquivoTO extends Ead1_TO_Dinamico {
	/**
	 * Nome do arquivo
	 * @var string
	 */
	public	$st_nomearquivo;
		
	/**
	 * Destino a ser salvo o arquivo
	 * @var string
	 */
	public  $st_caminhodiretorio;
	
	/**
	 * Extensão do arquivo  
	 * @var string
	 */
	public  $st_extensaoarquivo;
	
	/**
	 * Arquivo a ser salvo
	 */
	public  $st_conteudoarquivo;
	
	/**
	 * Nome do objeto a ser executado após a transferência do upload
	 * @var string
	 */
	public $st_objetocallback;
	
	/**
	 * Nome do método do objeto a ser executado após a transferência do upload
	 * @var unknown_type
	 */
	public $st_metodocallback;
	
	/**
	 * Url de acesso ao arquivo 
	 * @var string
	 */
	public $st_url;
	
	
	/**
	 * Em caso de Upload Normal, este array recebe o conteúdo da variável $_FILES
	 * @var array $_FILES
	 */
	public $ar_arquivo;
	
	/**
	 * @return the $ar_arquivo
	 */
	public function getAr_arquivo() {
		return $this->ar_arquivo;
	}

	/**
	 * @param multitype: $ar_arquivo
	 */
	public function setAr_arquivo($ar_arquivo) {
		$this->ar_arquivo = $ar_arquivo;
	}

	/**
	 * @return the $st_url
	 */
	public function getSt_url(){

		return $this->st_url;
	}

	/**
	 * @param $st_url the $st_url to set
	 */
	public function setSt_url( $st_url ){

		$this->st_url = $st_url;
	}
	
	/**
	 * @return the $st_objetocallback
	 */
	public function getSt_objetocallback(){

		return $this->st_objetocallback;
	}

	/**
	 * @return the $st_metodocallback
	 */
	public function getSt_metodocallback(){

		return $this->st_metodocallback;
	}

	/**
	 * @param $st_objetocallback the $st_objetocallback to set
	 */
	public function setSt_objetocallback( $st_objetocallback ){

		$this->st_objetocallback = $st_objetocallback;
	}

	/**
	 * @param $st_metodocallback the $st_metodocallback to set
	 */
	public function setSt_metodocallback( $st_metodocallback ){

		$this->st_metodocallback = $st_metodocallback;
	}

	/**
	 * @return the $st_nomearquivo
	 */
	public function getSt_nomearquivo(){

		return $this->st_nomearquivo;
	}

	/**
	 * @return the $st_caminhodiretorio
	 */
	public function getSt_caminhodiretorio(){

		return $this->st_caminhodiretorio;
	}

	/**
	 * @return the $st_extensaoarquivo
	 */
	public function getSt_extensaoarquivo(){

		return $this->st_extensaoarquivo;
	}

	/**
	 * @return the $st_conteudoarquivo
	 */
	public function getSt_conteudoarquivo(){

		return $this->st_conteudoarquivo;
	}

	/**
	 * @param $st_nomearquivo the $st_nomearquivo to set
	 */
	public function setSt_nomearquivo( $st_nomearquivo ){

		$this->st_nomearquivo = $st_nomearquivo;
	}

	/**
	 * @param $st_caminhodiretorio the $st_caminhodiretorio to set
	 */
	public function setSt_caminhodiretorio( $st_caminhodiretorio ){

		$this->st_caminhodiretorio = $st_caminhodiretorio;
	}

	/**
	 * @param $st_extensaoarquivo the $st_extensaoarquivo to set
	 */
	public function setSt_extensaoarquivo( $st_extensaoarquivo ){

		$this->st_extensaoarquivo = $st_extensaoarquivo;
	}

	/**
	 * @param $st_conteudoarquivo the $st_conteudoarquivo to set
	 */
	public function setSt_conteudoarquivo( $st_conteudoarquivo ){

		$this->st_conteudoarquivo = $st_conteudoarquivo;
	}

	
		
}
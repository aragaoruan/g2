<?php
/**
 * Classe para encapsular os dados de relacionamento entre disciplina e trilha.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TrilhaDisciplinaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da trilha.
	 * @var int
	 */
	public $id_trilha;
	
	/**
	 * Contém o id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Contém o id da disciplina pré-requisito.
	 * @var int
	 */
	public $id_disciplinaprerequisito;
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_disciplinaprerequisito() {
		return $this->id_disciplinaprerequisito;
	}
	
	/**
	 * @return int
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_disciplinaprerequisito
	 */
	public function setId_disciplinaprerequisito($id_disciplinaprerequisito) {
		$this->id_disciplinaprerequisito = $id_disciplinaprerequisito;
	}
	
	/**
	 * @param int $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}

}

?>
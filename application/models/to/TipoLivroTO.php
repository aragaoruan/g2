<?php
/**
 * Classe para encapsular os dados de TipoLivro.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class TipoLivroTO extends Ead1_TO_Dinamico {
	
	
	const IMPRESSO 	= 1; //1	Impresso
	const E_BOOK	= 2; //2	E-book
	
	
	public $id_tipolivro;
	public $st_tipolivro;
	
	
	/**
	 * @return the $id_tipolivro
	 */
	public function getId_tipolivro() {
		return $this->id_tipolivro;
	}

	/**
	 * @return the $st_tipolivro
	 */
	public function getSt_tipolivro() {
		return $this->st_tipolivro;
	}

	/**
	 * @param field_type $id_tipolivro
	 */
	public function setId_tipolivro($id_tipolivro) {
		$this->id_tipolivro = $id_tipolivro;
	}

	/**
	 * @param field_type $st_tipolivro
	 */
	public function setSt_tipolivro($st_tipolivro) {
		$this->st_tipolivro = $st_tipolivro;
	}




}




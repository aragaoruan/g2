<?php

/**
 * Classe para encapsular os dados da view de encerramento de sala de aula (vw_encerramentosalascurso)
 * @author Kayo Silva - kayo.silva@unyleya.com.br
 * @package models
 * @subpackage to
 * @since 2013-07-19
 * Alterado em 2013-10-22 por Kayo Silva
 */
class VwEncerramentoSalasCursoTO extends Ead1_TO_Dinamico
{

    public $id_saladeaula;
    public $id_entidade;
    public $id_disciplina;
    public $id_projetopedagogico;
    public $id_tipodisciplina;
    public $dt_encerramentocoordenador;
    public $dt_encerramentopedagogico;
    public $dt_encerramentofinanceiro;
    public $st_projetopedagogico;
    public $st_disciplina;
    public $QTD_ALUNOS_ENCERRAR;
    public $st_professor;
    public $id_encerramentosala;
    public $st_saladeaula;
    public $st_areaconhecimento;
    public $nu_cargahoraria;
    public $dt_encerramentoprofessor;
    public $nu_valor;
    public $nu_valorpago;
    public $id_usuarioprofessor;

    /**
     * 
     * @return date $this->dt_encerramentoprofessor
     */
    public function getDt_encerramentoprofessor ()
    {
        return $this->dt_encerramentoprofessor;
    }

    /**
     * 
     * @param date $dt_encerramentoprofessor
     */
    public function setDt_encerramentoprofessor ($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    /**
     * 
     * @return int id_saladeaula
     */
    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    /**
     * 
     * @param int $id_saladeaula
     */
    public function setId_saladeaula ($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * 
     * @return int id_saladeaula
     */
    public function getId_encerramentosala ()
    {
        return $this->id_encerramentosala;
    }

    /**
     * 
     * @param int $id_saladeaula
     */
    public function setId_encerramentosala ($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * 
     * @return int id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @param int $id_entidade
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * 
     * @return int
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * 
     * @param int $id_disciplina
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * 
     * @return int
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * 
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * 
     * @return int
     */
    public function getId_tipodisciplina ()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * 
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina ($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * 
     * @return date
     */
    public function getDt_encerramentocoordenador ()
    {
        return $this->dt_encerramentocoordenador;
    }

    /**
     * 
     * @param date $dt_encerramentocoordenador
     */
    public function setDt_encerramentocoordenador ($dt_encerramentocoordenador)
    {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    /**
     * 
     * @return date
     */
    public function getDt_encerramentopedagogico ()
    {
        return $this->dt_encerramentopedagogico;
    }

    /**
     * 
     * @param date $dt_encerramentopedagogico
     */
    public function setDt_encerramentopedagogico ($dt_encerramentopedagogico)
    {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    /**
     * 
     * @return date
     */
    public function getDt_encerramentofinanceiro ()
    {
        return $this->dt_encerramentofinanceiro;
    }

    /**
     * 
     * @param date $dt_encerramentofinanceiro
     */
    public function setDt_encerramentofinanceiro ($dt_encerramentofinanceiro)
    {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * 
     * @return string
     */
    public function getSt_projetopedagogico ()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * 
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico ($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * 
     * @return string
     */
    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * 
     * @return int
     */
    public function getQtd_alunos_encerrar ()
    {
        return $this->QTD_ALUNOS_ENCERRAR;
    }

    /**
     * 
     * @param int $qtd_alunos_encerrar
     */
    public function setQtd_alunos_encerrar ($qtd_alunos_encerrar)
    {
        $this->QTD_ALUNOS_ENCERRAR = $qtd_alunos_encerrar;
    }

    /**
     * 
     * @return string
     */
    public function getSt_professor ()
    {
        return $this->st_professor;
    }

    /**
     * 
     * @param string $st_professor
     */
    public function setSt_professor ($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula ()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula ($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento ()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento ($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @param mixed $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return mixed
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param mixed $nu_valorpago
     */
    public function setNu_valorpago($nu_valorpago)
    {
        $this->nu_valorpago = $nu_valorpago;
    }

    /**
     * @return mixed
     */
    public function getNu_valorpago()
    {
        return $this->nu_valorpago;
    }

    /**
     * @return mixed
     */
    public function getId_usuarioprofessor()
    {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param mixed $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor)
    {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }




}
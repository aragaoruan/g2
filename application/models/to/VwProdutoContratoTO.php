<?php
/**
 * 
 * Método que encapsula os dados da view de produto do contrato
 * @author Dimas Sulz
 */
class VwProdutoContratoTO extends Ead1_TO_Dinamico
{
	public $id_produto;
	public $id_tipoproduto;
	public $st_produto;
	public $bl_ativo;
	public $id_contrato;
	public $id_evolucao;
	public $id_entidade;
	public $st_nomeentidade;
	public $id_situacao;
	public $st_situacao;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $id_produtovalor;
	public $id_tipoprodutovalor;
	public $st_tipoprodutovalor;
	public $nu_valor;
	public $nu_valormensal;
	public $nu_basepropor;
	public $id_entidadematriz;
	public $id_venda;
	public $bl_turma;
	public $id_matricula;
	public $id_vendaproduto;
	public $id_turma;
	
/**
	 * @return the $id_turma
	 */
	public function getId_turma(){ 
		return $this->id_turma;
	}
	
	/**
	 * @return the $id_vendaproduto
	 */
	public function getId_vendaproduto(){ 
		return $this->id_vendaproduto;
	}

	/**
	 * @return the $bl_turma
	 */
	public function getBl_turma() {
		return $this->bl_turma;
	}
	
	/**
	 * @param $bl_turma the $bl_turma to set
	 */
	public function setBl_turma($bl_turma) {
		$this->bl_turma = $bl_turma;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $id_produtovalor
	 */
	public function getId_produtovalor() {
		return $this->id_produtovalor;
	}

	/**
	 * @return the $id_tipoprodutovalor
	 */
	public function getId_tipoprodutovalor() {
		return $this->id_tipoprodutovalor;
	}

	/**
	 * @return the $st_tipoprodutovalor
	 */
	public function getSt_tipoprodutovalor() {
		return $this->st_tipoprodutovalor;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $nu_valormensal
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}

	/**
	 * @return the $nu_basepropor
	 */
	public function getNu_basepropor() {
		return $this->nu_basepropor;
	}

	/**
	 * @return the $id_entidadematriz
	 */
	public function getId_entidadematriz() {
		return $this->id_entidadematriz;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_produtovalor
	 */
	public function setId_produtovalor($id_produtovalor) {
		$this->id_produtovalor = $id_produtovalor;
	}

	/**
	 * @param field_type $id_tipoprodutovalor
	 */
	public function setId_tipoprodutovalor($id_tipoprodutovalor) {
		$this->id_tipoprodutovalor = $id_tipoprodutovalor;
	}

	/**
	 * @param field_type $st_tipoprodutovalor
	 */
	public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
		$this->st_tipoprodutovalor = $st_tipoprodutovalor;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
	}

	/**
	 * @param field_type $nu_basepropor
	 */
	public function setNu_basepropor($nu_basepropor) {
		$this->nu_basepropor = $nu_basepropor;
	}

	/**
	 * @param field_type $id_entidadematriz
	 */
	public function setId_entidadematriz($id_entidadematriz) {
		$this->id_entidadematriz = $id_entidadematriz;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param int $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	

	/**
	 * @param field_type $id_vendaproduto
	 */
	public function setId_vendaproduto($id_vendaproduto){ 
		$this->id_vendaproduto = $id_vendaproduto;
	}
	
	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma){ 
		$this->id_turma = $id_turma;
	}


}
?>
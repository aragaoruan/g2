<?php
/**
 * Classe para encapsular os dados de núcleo de telemarketing.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class NucleotmTO extends Ead1_TO_Dinamico {

	public $id_nucleotm;
	
	public $id_entidade;
	
	public $id_entidadematriz;
	
	public $id_usuariocadastro;
	
	public $id_situacao;
	
	public $st_nucleotm;
	
	public $dt_cadastro;
	
	public $bl_ativo;
	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidadematriz() {
		return $this->id_entidadematriz;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nucleotm() {
		return $this->id_nucleotm;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nucleotm() {
		return $this->st_nucleotm;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_entidadematriz
	 */
	public function setId_entidadematriz($id_entidadematriz) {
		$this->id_entidadematriz = $id_entidadematriz;
	}
	
	/**
	 * @param unknown_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm) {
		$this->id_nucleotm = $id_nucleotm;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $st_nucleotm
	 */
	public function setSt_nucleotm($st_nucleotm) {
		$this->st_nucleotm = $st_nucleotm;
	}

}

?>
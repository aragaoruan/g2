<?php
/**
 * Classe para encapsular os dados da view de usuário da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaUsuarioTO extends Ead1_TO_Dinamico {
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do registro da pessoa.
	 * @var int
	 */
	public $id_registropessoa;
	
	/**
	 * CPF do usuário.
	 * @var string
	 */
	public $st_cpf;
	
	/**
	 * Login do usuário.
	 * @var string
	 */
	public $st_login;
	
	/**
	 * Senha do usuário.
	 * @var string
	 */
	public $st_senha;
	
	/**
	 * Nome completo da pessoa.
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Diz se o usuário é ativo.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Nome do pai da pessoa.
	 * @var string
	 */
	public $st_nomepai;
	
	/**
	 * Nome da mãe da pessoa.
	 * @var string
	 */
	public $st_nomemae;
	
	/**
	 * Passaporte da pessoa.
	 * @var string
	 */
	public $st_passaporte;
	
	/**
	 * Sexo da pessoa.
	 * @var string
	 */
	public $st_sexo;
	
	/**
	 * Id do usuário que cadastrou.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id do estado civil da pessoa.
	 * @var int
	 */
	public $id_estadocivil;
	
	/**
	 * Estado civil da pessoa.
	 * @var string
	 */
	public $st_estadocivil;
	
	/**
	 * Nome de exibição do usuário.
	 * @var string
	 */
	public $st_nomeexibicao;
	
	/**
	 * Data de cadastro do usuário.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Data de nascimento da pessoa.
	 * @var string
	 */
	public $dt_nascimento;
	
	/**
	 * Id do Município da pessoa.
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Município da pessoa.
	 * @var string
	 */
	public $st_nomemunicipio;
	
	/**
	 * Id do país da pessoa.
	 * @var int
	 */
	public $id_pais;
	
	/**
	 * País da pessoa.
	 * @var string
	 */
	public $st_nomepais;
	
	/**
	 * Sigla UF da pessoa.
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * UF da pessoa.
	 * @var string
	 */
	public $st_uf;
	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return string
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_estadocivil() {
		return $this->id_estadocivil;
	}
	
	/**
	 * @return int
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return int
	 */
	public function getId_pais() {
		return $this->id_pais;
	}
	
	/**
	 * @return int
	 */
	public function getId_registropessoa() {
		return $this->id_registropessoa;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return string
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_estadocivil() {
		return $this->st_estadocivil;
	}
	
	/**
	 * @return string
	 */
	public function getSt_login() {
		return $this->st_login;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeexibicao() {
		return $this->st_nomeexibicao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemae() {
		return $this->st_nomemae;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomepai() {
		return $this->st_nomepai;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomepais() {
		return $this->st_nomepais;
	}
	
	/**
	 * @return string
	 */
	public function getSt_passaporte() {
		return $this->st_passaporte;
	}
	
	/**
	 * @return string
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}
	
	/**
	 * @return string
	 */
	public function getSt_sexo() {
		return $this->st_sexo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_uf() {
		return $this->st_uf;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param string $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_estadocivil
	 */
	public function setId_estadocivil($id_estadocivil) {
		$this->id_estadocivil = $id_estadocivil;
	}
	
	/**
	 * @param int $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param int $id_pais
	 */
	public function setId_pais($id_pais) {
		$this->id_pais = $id_pais;
	}
	
	/**
	 * @param int $id_registropessoa
	 */
	public function setId_registropessoa($id_registropessoa) {
		$this->id_registropessoa = $id_registropessoa;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param string $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}
	
	/**
	 * @param string $st_estadocivil
	 */
	public function setSt_estadocivil($st_estadocivil) {
		$this->st_estadocivil = $st_estadocivil;
	}
	
	/**
	 * @param string $st_login
	 */
	public function setSt_login($st_login) {
		$this->st_login = $st_login;
	}
	
	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param string $st_nomeexibicao
	 */
	public function setSt_nomeexibicao($st_nomeexibicao) {
		$this->st_nomeexibicao = $st_nomeexibicao;
	}
	
	/**
	 * @param string $st_nomemae
	 */
	public function setSt_nomemae($st_nomemae) {
		$this->st_nomemae = $st_nomemae;
	}
	
	/**
	 * @param string $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}
	
	/**
	 * @param string $st_nomepai
	 */
	public function setSt_nomepai($st_nomepai) {
		$this->st_nomepai = $st_nomepai;
	}
	
	/**
	 * @param string $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais) {
		$this->st_nomepais = $st_nomepais;
	}
	
	/**
	 * @param string $st_passaporte
	 */
	public function setSt_passaporte($st_passaporte) {
		$this->st_passaporte = $st_passaporte;
	}
	
	/**
	 * @param string $st_senha
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}
	
	/**
	 * @param string $st_sexo
	 */
	public function setSt_sexo($st_sexo) {
		$this->st_sexo = $st_sexo;
	}
	
	/**
	 * @param string $st_uf
	 */
	public function setSt_uf($st_uf) {
		$this->st_uf = $st_uf;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de entrega de material.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class EntregaMaterialTO extends Ead1_TO_Dinamico{
	
	const ENTREGUE 			= 61;
	const DEVOLVIDO 		= 62;
	const NAO_ENTREGUE 		= 63;
	const PREPARANDO_ENVIO 	= 93;

	
	public $id_entregamaterial;
	public $id_matricula;
	public $id_usuariocadastro;
	public $id_itemdematerial;
	public $id_situacao;
	public $dt_entrega;
	public $dt_devolucao;
	public $bl_ativo;
	
	/**
	 * @return the $id_entregamaterial
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $dt_entrega
	 */
	public function getDt_entrega() {
		return $this->dt_entrega;
	}

	/**
	 * @return the $dt_devolucao
	 */
	public function getDt_devolucao() {
		return $this->dt_devolucao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $dt_entrega
	 */
	public function setDt_entrega($dt_entrega) {
		$this->dt_entrega = $dt_entrega;
	}

	/**
	 * @param field_type $dt_devolucao
	 */
	public function setDt_devolucao($dt_devolucao) {
		$this->dt_devolucao = $dt_devolucao;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
}
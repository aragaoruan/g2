<?php
/**
 * Classe para encapsular os dados de UF.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class UfTO extends Ead1_TO_Dinamico {

	/**
	 * Contém a sigla do estado.
	 * @var String
	 */
	public $sg_uf;
	
	/**
	 * Contém o nome do estado.
	 * @var String
	 */
	public $st_uf;
	
	/**
	 * Contém o código da capital da UF.
	 * @var int
	 */
	public $nu_codigocapital;
	
	/**
	 * Contém o número do código de IBGE.
	 * @var int
	 */
	public $nu_codigoibge;
	
	/**
	 * Contém o nome da região da UF.
	 * @var String
	 */
	public $st_nomeregiao;
	
	/**
	 * @return int
	 */
	public function getNu_codigocapital() {
		return $this->nu_codigocapital;
	}
	
	/**
	 * @return int
	 */
	public function getNu_codigoibge() {
		return $this->nu_codigoibge;
	}
	
	/**
	 * @return String
	 */
	public function getSt_nomeregiao() {
		return $this->st_nomeregiao;
	}
	
	/**
	 * @param int $nu_codigocapital
	 */
	public function setNu_codigocapital($nu_codigocapital) {
		$this->nu_codigocapital = $nu_codigocapital;
	}
	
	/**
	 * @param int $nu_codigoibge
	 */
	public function setNu_codigoibge($nu_codigoibge) {
		$this->nu_codigoibge = $nu_codigoibge;
	}
	
	/**
	 * @param String $st_nomeregiao
	 */
	public function setSt_nomeregiao($st_nomeregiao) {
		$this->st_nomeregiao = $st_nomeregiao;
	}
	/**
	 * @return String
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return String
	 */
	public function getSt_uf() {
		return $this->st_uf;
	}
	
	/**
	 * @param String $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param String $st_uf
	 */
	public function setSt_uf($st_uf) {
		$this->st_uf = $st_uf;
	}

}

?>
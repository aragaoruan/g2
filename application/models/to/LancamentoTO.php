<?php

/**
 * Classe para encapsular os dados de Lancamento.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 *
 */
class LancamentoTO extends Ead1_TO_Dinamico
{

    public $id_lancamento;
    public $nu_valor;
    public $nu_vencimento;
    public $id_tipolancamento;
    public $dt_cadastro;
    public $id_meiopagamento;
    public $id_usuariolancamento;
    public $id_entidadelancamento;
    public $id_entidade;
    public $id_usuariocadastro;
    public $st_banco;
    public $id_cartaoconfig;
    public $id_boletoconfig;
    public $bl_quitado;
    public $dt_vencimento;
    public $dt_quitado;
    public $dt_emissao;
    public $dt_prevquitado;
    public $st_emissor;
    public $st_coddocumento;
    public $nu_juros;
    public $nu_desconto;
    public $nu_quitado;
    public $nu_multa;
    public $dt_atualizado;
    public $bl_ativo = true;
    public $id_lancamentooriginal;
    public $st_renegociacao;
    public $id_acordo;
    public $dt_vencimentocheque;
    public $id_codcoligada;
    public $st_agencia;
    public $st_codconta;
    public $st_nossonumero;
    public $st_numcheque;
    public $st_statuslan;
    public $nu_verificacao;
    public $dt_vencimentobackup;
    public $bl_baixadofluxus;
    public $nu_tentativabraspag;
    public $st_retornoverificacao;
    public $nu_jurosboleto;
    public $nu_descontoboleto;
    public $nu_cartao;
    public $id_usuarioquitacao;
    public $bl_quitacaoretorno;
    public $bl_original;
    public $st_autorizacao;
    public $st_ultimosdigitos;
    public $id_sistemacobranca;
    public $id_transacaoexterna;
    public $nu_cobranca;
    public $nu_assinatura;

    /**
     * @return mixed
     */
    public function getNu_cobranca()
    {
        return $this->nu_cobranca;
    }

    /**
     * @param mixed $nu_cobranca
     */
    public function setNu_cobranca($nu_cobranca)
    {
        $this->nu_cobranca = $nu_cobranca;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNu_assinatura()
    {
        return $this->nu_assinatura;
    }

    /**
     * @param mixed $nu_assinatura
     */
    public function setNu_assinatura($nu_assinatura)
    {
        $this->nu_assinatura = $nu_assinatura;
        return $this;
    }

    const ENTRADA = 'entrada';
    const PARCELA = 'parcela';

    /**
     * @return mixed
     */
    public function getid_sistemacobranca()
    {
        return $this->id_sistemacobranca;
    }

    /**
     * @param mixed $id_sistemacobranca
     * @return LancamentoTO
     */
    public function setIdSistemacobranca($id_sistemacobranca)
    {
        $this->id_sistemacobranca = $id_sistemacobranca;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getnu_cartao()
    {
        return $this->nu_cartao;
    }

    /**
     * @param mixed $nu_cartao
     */
    public function setnu_cartao($nu_cartao)
    {
        $this->nu_cartao = $nu_cartao;
        return $this;

    }

    /**
     * @return integer
     */
    function getNu_tentativabraspag ()
    {
        return $this->nu_tentativabraspag;
    }

    /**
     * @return string
     */
    function getSt_retornoverificacao ()
    {
        return $this->st_retornoverificacao;
    }

    /**
     * @return string
     */
    function getSt_ultimosdigitos ()
    {
        return $this->st_ultimosdigitos;
    }

    /**
     * @param integer $nu_tentativabraspag
     */
    function setNu_tentativabraspag ($nu_tentativabraspag)
    {
        $this->nu_tentativabraspag = $nu_tentativabraspag;
    }

    /**
     * @param string $st_retornoverificacao
     */
    function setSt_retornoverificacao ($st_retornoverificacao)
    {
        $this->st_retornoverificacao = $st_retornoverificacao;
    }

    /**
     * @param string $st_ultimosdigitos
     */
    function setSt_ultimosdigitos ($st_ultimosdigitos)
    {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @param boolean $bl_original
     */
    public function setBl_original ($bl_original)
    {
        $this->bl_original = $bl_original;
    }

    public function getBl_original ()
    {
        return $this->bl_original;
    }

    /**
     * @return the $dt_vencimentobackup
     */
    public function getDt_vencimentobackup ()
    {
        return $this->dt_vencimentobackup;
    }

    /**
     * @param field_type $dt_vencimentobackup
     */
    public function setDt_vencimentobackup ($dt_vencimentobackup)
    {
        $this->dt_vencimentobackup = $dt_vencimentobackup;
        return $this;
    }

    /**
     * @return the $nu_verificacao
     */
    public function getNu_verificacao ()
    {
        return $this->nu_verificacao;
    }

    /**
     * @param field_type $nu_verificacao
     */
    public function setNu_verificacao ($nu_verificacao)
    {
        $this->nu_verificacao = $nu_verificacao;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento ()
    {
        return $this->id_lancamento;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $nu_vencimento
     */
    public function getNu_vencimento ()
    {
        return $this->nu_vencimento;
    }

    /**
     * @return the $id_tipolancamento
     */
    public function getId_tipolancamento ()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $dt_atualizado
     */
    public function getDt_atualizado ()
    {
        return $this->dt_atualizado;
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @return the $id_usuariolancamento
     */
    public function getId_usuariolancamento ()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $id_entidadelancamento
     */
    public function getId_entidadelancamento ()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $st_banco
     */
    public function getSt_banco ()
    {
        return $this->st_banco;
    }

    /**
     * @return the $id_cartaoconfig
     */
    public function getId_cartaoconfig ()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @return the $id_boletoconfig
     */
    public function getId_boletoconfig ()
    {
        return $this->id_boletoconfig;
    }

    /**
     * @return the $bl_quitado
     */
    public function getBl_quitado ()
    {
        return $this->bl_quitado;
    }

    /**
     * @return the $dt_vencimento
     */
    public function getDt_vencimento ()
    {
        return $this->dt_vencimento;
    }

    /**
     * @return the $dt_quitado
     */
    public function getDt_quitado ()
    {
        return $this->dt_quitado;
    }

    /**
     * @return the $dt_emissao
     */
    public function getDt_emissao ()
    {
        return $this->dt_emissao;
    }

    /**
     * @return the $dt_prevquitado
     */
    public function getDt_prevquitado ()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @return the $st_emissor
     */
    public function getSt_emissor ()
    {
        return $this->st_emissor;
    }

    /**
     * @return the $st_coddocumento
     */
    public function getSt_coddocumento ()
    {
        return $this->st_coddocumento;
    }

    /**
     * @return the $nu_juros
     */
    public function getNu_juros ()
    {
        return $this->nu_juros;
    }

    /**
     * @return the $nu_desconto
     */
    public function getNu_desconto ()
    {
        return $this->nu_desconto;
    }

    /**
     * @return the $nu_quitado
     */
    public function getNu_quitado ()
    {
        return $this->nu_quitado;
    }

    /**
     * @return the $nu_multa
     */
    public function getNu_multa ()
    {
        return $this->nu_multa;
    }

    /**
     * @param int $id_lancamento
     */
    public function setId_lancamento ($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param int $nu_valor
     */
    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param int $nu_vencimento
     */
    public function setNu_vencimento ($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @param int $id_tipolancamento
     */
    public function setId_tipolancamento ($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @param Zend_Date $dt_cadastro
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param Zend_Date $dt_atualizado
     */
    public function setDt_atualizado ($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @param int $id_meiopagamento
     */
    public function setId_meiopagamento ($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param int $id_usuariolancamento
     */
    public function setId_usuariolancamento ($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_entidadelancamento
     */
    public function setId_entidadelancamento ($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param string $st_banco
     */
    public function setSt_banco ($st_banco)
    {
        $this->st_banco = $st_banco;
    }

    /**
     * @param int $id_cartaoconfig
     */
    public function setId_cartaoconfig ($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
    }

    /**
     * @param int $id_boletoconfig
     */
    public function setId_boletoconfig ($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
    }

    /**
     * @param boolean $bl_quitado
     */
    public function setBl_quitado ($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param Zend_Date $dt_vencimento
     */
    public function setDt_vencimento ($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param Zend_Date $dt_quitado
     */
    public function setDt_quitado ($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param Zend_Date $dt_emissao
     */
    public function setDt_emissao ($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @param Zend_Date $dt_prevquitado
     */
    public function setDt_prevquitado ($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param string $st_emissor
     */
    public function setSt_emissor ($st_emissor)
    {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @param string $st_coddocumento
     */
    public function setSt_coddocumento ($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @param int $nu_juros
     */
    public function setNu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @param int $nu_desconto
     */
    public function setNu_desconto ($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @param int $nu_quitado
     */
    public function setNu_quitado ($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
    }

    /**
     * @param int $nu_multa
     */
    public function setNu_multa ($nu_multa)
    {
        $this->nu_multa = $nu_multa;
    }

    /**
     * @return int $id_lancamentooriginal
     */
    public function getId_lancamentooriginal ()
    {
        return $this->id_lancamentooriginal;
    }

    /**
     * @param int $id_lancamentooriginal
     */
    public function setId_lancamentooriginal ($id_lancamentooriginal)
    {
        $this->id_lancamentooriginal = $id_lancamentooriginal;
    }

    /**
     * @return string $st_renegociacao
     */
    public function getSt_renegociacao ()
    {
        return $this->st_renegociacao;
    }

    /**
     * @param string $st_renegociacao
     */
    public function setSt_renegociacao ($st_renegociacao)
    {
        $this->st_renegociacao = $st_renegociacao;
    }

    public function getId_acordo ()
    {
        return $this->id_acordo;
    }

    public function setId_acordo ($id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    public function getDt_vencimentocheque ()
    {
        return $this->dt_vencimentocheque;
    }

    public function setDt_vencimentocheque ($dt_vencimentocheque)
    {
        $this->dt_vencimentocheque = $dt_vencimentocheque;
    }

    public function getId_codcoligada ()
    {
        return $this->id_codcoligada;
    }

    public function setId_codcoligada ($id_codcoligada)
    {
        $this->id_codcoligada = $id_codcoligada;
    }

    public function getSt_agencia ()
    {
        return $this->st_agencia;
    }

    public function setSt_agencia ($st_agencia)
    {
        $this->st_agencia = $st_agencia;
    }

    public function getSt_codconta ()
    {
        return $this->st_codconta;
    }

    public function setSt_codconta ($st_codconta)
    {
        $this->st_codconta = $st_codconta;
    }

    public function getSt_nossonumero ()
    {
        return $this->st_nossonumero;
    }

    public function setSt_nossonumero ($st_nossonumero)
    {
        $this->st_nossonumero = $st_nossonumero;
    }

    public function getSt_numcheque ()
    {
        return $this->st_numcheque;
    }

    public function setSt_numcheque ($st_numcheque)
    {
        $this->st_numcheque = $st_numcheque;
    }

    public function getSt_statuslan ()
    {
        return $this->st_statuslan;
    }

    public function setSt_statuslan ($st_statuslan)
    {
        $this->st_statuslan = $st_statuslan;
    }

    /**
     * @param mixed $bl_baixadofluxus
     */
    public function setBl_baixadofluxus ($bl_baixadofluxus)
    {
        $this->bl_baixadofluxus = $bl_baixadofluxus;
    }

    /**
     * @return mixed
     */
    public function getBl_baixadofluxus ()
    {
        return $this->bl_baixadofluxus;
    }

    /**
     * @param mixed $nu_tentativabraspag
     */
    public function setNuTentativabraspag ($nu_tentativabraspag)
    {
        $this->nu_tentativabraspag = $nu_tentativabraspag;
    }

    /**
     * @return mixed
     */
    public function getNuTentativabraspag ()
    {
        return $this->nu_tentativabraspag;
    }

    /**
     * @param mixed $st_retornoverificacao
     */
    public function setStRetornoverificacao ($st_retornoverificacao)
    {
        $this->st_retornoverificacao = $st_retornoverificacao;
    }

    /**
     * @return mixed
     */
    public function getStRetornoverificacao ()
    {
        return $this->st_retornoverificacao;
    }

    /**
     * @param mixed $bl_quitacaoretorno
     */
    public function setBl_quitacaoretorno ($bl_quitacaoretorno)
    {
        $this->bl_quitacaoretorno = $bl_quitacaoretorno;
    }

    /**
     * @return mixed
     */
    public function getBl_quitacaoretorno ()
    {
        return $this->bl_quitacaoretorno;
    }

    /**
     * @param mixed $id_usuarioquitacao
     */
    public function setId_usuarioquitacao ($id_usuarioquitacao)
    {
        $this->id_usuarioquitacao = $id_usuarioquitacao;
    }

    /**
     * @return mixed
     */
    public function getId_usuarioquitacao ()
    {
        return $this->id_usuarioquitacao;
    }

    /**
     * @param mixed $nu_descontoboleto
     */
    public function setNu_descontoboleto ($nu_descontoboleto)
    {
        $this->nu_descontoboleto = $nu_descontoboleto;
    }

    /**
     * @return mixed
     */
    public function getNu_descontoboleto ()
    {
        return $this->nu_descontoboleto;
    }

    /**
     * @param mixed $nu_jurosboleto
     */
    public function setNu_jurosboleto ($nu_jurosboleto)
    {
        $this->nu_jurosboleto = $nu_jurosboleto;
    }

    /**
     * @return mixed
     */
    public function getNu_jurosboleto ()
    {
        return $this->nu_jurosboleto;
    }

    /**
     * @param string $st_autorizacao
     */
    public function setSt_autorizacao ($st_autorizacao)
    {
        $this->st_autorizacao = $st_autorizacao;
    }

    /**
     * @return string
     */
    public function getSt_autorizacao ()
    {
        return $this->st_autorizacao;
    }

    /**
     * @return mixed
     */
    public function getId_transacaoexterna()
    {
        return $this->id_transacaoexterna;
    }

    /**
     * @param mixed $id_transacaoexterna
     */
    public function setId_transacaoexterna($id_transacaoexterna)
    {
        $this->id_transacaoexterna = $id_transacaoexterna;
    }


}

<?php
/**
 * Classe para encapsular os dados de agência e conta da entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContaEntidadeTO extends Ead1_TO_Dinamico {

	public $id_contaentidade;
	public $id_entidade;
	public $st_banco;
	public $id_tipodeconta;
	public $st_digitoagencia;
	public $st_agencia;
	public $st_conta;
	public $bl_padrao;
	public $st_digitoconta;
	
	/**
	 * @return the $id_contaentidade
	 */
	public function getId_contaentidade() {
		return $this->id_contaentidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @return the $id_tipodeconta
	 */
	public function getId_tipodeconta() {
		return $this->id_tipodeconta;
	}

	/**
	 * @return the $st_digitoagencia
	 */
	public function getSt_digitoagencia() {
		return $this->st_digitoagencia;
	}

	/**
	 * @return the $st_agencia
	 */
	public function getSt_agencia() {
		return $this->st_agencia;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @return the $st_digitoconta
	 */
	public function getSt_digitoconta() {
		return $this->st_digitoconta;
	}

	/**
	 * @param field_type $id_contaentidade
	 */
	public function setId_contaentidade($id_contaentidade) {
		$this->id_contaentidade = $id_contaentidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	/**
	 * @param field_type $id_tipodeconta
	 */
	public function setId_tipodeconta($id_tipodeconta) {
		$this->id_tipodeconta = $id_tipodeconta;
	}

	/**
	 * @param field_type $st_digitoagencia
	 */
	public function setSt_digitoagencia($st_digitoagencia) {
		$this->st_digitoagencia = $st_digitoagencia;
	}

	/**
	 * @param field_type $st_agencia
	 */
	public function setSt_agencia($st_agencia) {
		$this->st_agencia = $st_agencia;
	}

	/**
	 * @param field_type $st_conta
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param field_type $st_digitoconta
	 */
	public function setSt_digitoconta($st_digitoconta) {
		$this->st_digitoconta = $st_digitoconta;
	}

}

?>
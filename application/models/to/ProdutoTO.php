<?php
/**
 * Classe para encapsular os dados de Produto.
 * 
 * [Quem utiliza]
 * 	@see ProdutoRO
 * 
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 21/02/2011
 * @package models
 * @subpackage to
 */
class ProdutoTO extends Ead1_TO_Dinamico {

	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $id_situacao;
	public $id_usuariocadastro;
	public $id_entidade;
	public $bl_ativo;
	public $bl_todasformas;
	public $bl_todascampanhas;
	public $nu_gratuito;
	public $id_produtoimagempadrao;
	public $bl_unico;
	
	public $st_descricao;
	public $st_observacoes;
	public $st_informacoesadicionais;
	public $st_estruturacurricular;
	public $st_subtitulo;
	public $st_slug;
	public $bl_destaque;
	public $dt_cadastro;
	public $dt_atualizado;
	public $dt_iniciopontosprom;
	public $dt_fimpontosprom;
	public $nu_pontos;
	public $nu_pontospromocional;
	public $nu_estoque;
	public $bl_todasentidades;
	public $id_modelovenda;
	
	
	/**
	 * @return the $id_modelovenda
	 */
	public function getId_modelovenda() {
		return $this->id_modelovenda;
	}

	/**
	 * @param field_type $id_modelovenda
	 */
	public function setId_modelovenda($id_modelovenda) {
		$this->id_modelovenda = $id_modelovenda;
		return $this;
	}

	/**
	 * @return the $bl_todasentidades
	 */
	public function getBl_todasentidades() {
		return $this->bl_todasentidades;
	}

	/**
	 * @param field_type $bl_todasentidades
	 */
	public function setBl_todasentidades($bl_todasentidades) {
		$this->bl_todasentidades = $bl_todasentidades;
		return $this;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_observacoes
	 */
	public function getSt_observacoes() {
		return $this->st_observacoes;
	}

	/**
	 * @return the $st_informacoesadicionais
	 */
	public function getSt_informacoesadicionais() {
		return $this->st_informacoesadicionais;
	}

	/**
	 * @return the $st_estruturacurricular
	 */
	public function getSt_estruturacurricular() {
		return $this->st_estruturacurricular;
	}

	/**
	 * @return the $st_subtitulo
	 */
	public function getSt_subtitulo() {
		return $this->st_subtitulo;
	}

	/**
	 * @return the $st_slug
	 */
	public function getSt_slug() {
		return $this->st_slug;
	}

	/**
	 * @return the $bl_destaque
	 */
	public function getBl_destaque() {
		return $this->bl_destaque;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $dt_atualizado
	 */
	public function getDt_atualizado() {
		return $this->dt_atualizado;
	}

	/**
	 * @return the $dt_iniciopontosprom
	 */
	public function getDt_iniciopontosprom() {
		return $this->dt_iniciopontosprom;
	}

	/**
	 * @return the $dt_fimpontosprom
	 */
	public function getDt_fimpontosprom() {
		return $this->dt_fimpontosprom;
	}

	/**
	 * @return the $nu_pontos
	 */
	public function getNu_pontos() {
		return $this->nu_pontos;
	}

	/**
	 * @return the $nu_pontospromocional
	 */
	public function getNu_pontospromocional() {
		return $this->nu_pontospromocional;
	}

	/**
	 * @return the $nu_estoque
	 */
	public function getNu_estoque() {
		return $this->nu_estoque;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
		return $this;
	}

	/**
	 * @param field_type $st_observacoes
	 */
	public function setSt_observacoes($st_observacoes) {
		$this->st_observacoes = $st_observacoes;
		return $this;
	}

	/**
	 * @param field_type $st_informacoesadicionais
	 */
	public function setSt_informacoesadicionais($st_informacoesadicionais) {
		$this->st_informacoesadicionais = $st_informacoesadicionais;
		return $this;
	}

	/**
	 * @param field_type $st_estruturacurricular
	 */
	public function setSt_estruturacurricular($st_estruturacurricular) {
		$this->st_estruturacurricular = $st_estruturacurricular;
		return $this;
	}

	/**
	 * @param field_type $st_subtitulo
	 */
	public function setSt_subtitulo($st_subtitulo) {
		$this->st_subtitulo = $st_subtitulo;
		return $this;
	}

	/**
	 * @param field_type $st_slug
	 */
	public function setSt_slug($st_slug) {
		$this->st_slug = $st_slug;
		return $this;
	}

	/**
	 * @param field_type $bl_destaque
	 */
	public function setBl_destaque($bl_destaque) {
		$this->bl_destaque = $bl_destaque;
		return $this;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
		return $this;
	}

	/**
	 * @param field_type $dt_atualizado
	 */
	public function setDt_atualizado($dt_atualizado) {
		$this->dt_atualizado = $dt_atualizado;
		return $this;
	}

	/**
	 * @param field_type $dt_iniciopontosprom
	 */
	public function setDt_iniciopontosprom($dt_iniciopontosprom) {
		$this->dt_iniciopontosprom = $dt_iniciopontosprom;
		return $this;
	}

	/**
	 * @param field_type $dt_fimpontosprom
	 */
	public function setDt_fimpontosprom($dt_fimpontosprom) {
		$this->dt_fimpontosprom = $dt_fimpontosprom;
		return $this;
	}

	/**
	 * @param field_type $nu_pontos
	 */
	public function setNu_pontos($nu_pontos) {
		$this->nu_pontos = $nu_pontos;
		return $this;
	}

	/**
	 * @param field_type $nu_pontospromocional
	 */
	public function setNu_pontospromocional($nu_pontospromocional) {
		$this->nu_pontospromocional = $nu_pontospromocional;
		return $this;
	}

	/**
	 * @param field_type $nu_estoque
	 */
	public function setNu_estoque($nu_estoque) {
		$this->nu_estoque = $nu_estoque;
		return $this;
	}

	/**
	 * @return the $bl_unico
	 */
	public function getBl_unico() {
		return $this->bl_unico;
	}

	/**
	 * @param field_type $bl_unico
	 */
	public function setBl_unico($bl_unico) {
		$this->bl_unico = $bl_unico;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $bl_todasformas
	 */
	public function getBl_todasformas() {
		return $this->bl_todasformas;
	}

	/**
	 * @return the $bl_todascampanhas
	 */
	public function getBl_todascampanhas() {
		return $this->bl_todascampanhas;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $bl_todasformas
	 */
	public function setBl_todasformas($bl_todasformas) {
		$this->bl_todasformas = $bl_todasformas;
	}

	/**
	 * @param field_type $bl_todascampanhas
	 */
	public function setBl_todascampanhas($bl_todascampanhas) {
		$this->bl_todascampanhas = $bl_todascampanhas;
	}
	/**
	 * @return the $nu_gratuito
	 */
	public function getNu_gratuito() {
		return $this->nu_gratuito;
	}

	/**
	 * @param field_type $nu_gratuito
	 */
	public function setNu_gratuito($nu_gratuito) {
		$this->nu_gratuito = $nu_gratuito;
	}
	/**
	 * @return the $id_produtoimagempadrao
	 */
	public function getId_produtoimagempadrao() {
		return $this->id_produtoimagempadrao;
	}

	/**
	 * @param field_type $id_produtoimagempadrao
	 */
	public function setId_produtoimagempadrao($id_produtoimagempadrao) {
		$this->id_produtoimagempadrao = $id_produtoimagempadrao;
	}




	
}
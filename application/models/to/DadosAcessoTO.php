<?php
/**
 * Classe para encapsular os dados de DadosAcesso
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class DadosAcessoTO extends Ead1_TO_Dinamico {

	public $id_dadosacesso;
	public $id_usuario;
	public $st_login;
	public $st_senha;
	public $dt_cadastro;
	public $bl_ativo;
	public $id_usuariocadastro;
	public $id_entidade;
	
	
	/**
	 * @return the $id_dadosacesso
	 */
	public function getId_dadosacesso() {
		return $this->id_dadosacesso;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_login
	 */
	public function getSt_login() {
		return $this->st_login;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_dadosacesso
	 */
	public function setId_dadosacesso($id_dadosacesso) {
		$this->id_dadosacesso = $id_dadosacesso;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_login
	 */
	public function setSt_login($st_login) {
		$this->st_login = $st_login;
	}

	/**
	 * @param field_type $st_senha
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
	
	
}


<?php
/**
 * Classe para encapsular os dados de tipo de divisão financeira.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDivisaoFinanceiraTO extends Ead1_TO_Dinamico {
	
	const ENTRADA = 1;
	const PARCELA = 2;

	/**
	 * Id do tipo de divisão financeira.
	 * @var int
	 */
	public $id_tipodivisaofinanceira;
	
	/**
	 * Nome do tipo de divisão financeira.
	 * @var string
	 */
	public $st_tipodivisaofinanceira;
	
	/**
	 * @return int
	 */
	public function getId_tipodivisaofinanceira() {
		return $this->id_tipodivisaofinanceira;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipodivisaofinanceira() {
		return $this->st_tipodivisaofinanceira;
	}
	
	/**
	 * @param int $id_tipodivisaofinanceira
	 */
	public function setId_tipodivisaofinanceira($id_tipodivisaofinanceira) {
		$this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
	}
	
	/**
	 * @param string $st_tipodivisaofinanceira
	 */
	public function setSt_tipodivisaofinanceira($st_tipodivisaofinanceira) {
		$this->st_tipodivisaofinanceira = $st_tipodivisaofinanceira;
	}

}

?>
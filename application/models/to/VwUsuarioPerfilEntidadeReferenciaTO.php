<?php
/**
 * Classe para encapsular os dados que apontam a referência da divisão pedagógica de uma pessoa com um perfil.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwUsuarioPerfilEntidadeReferenciaTO  extends UsuarioPerfilEntidadeReferenciaTO{
	
	public $st_nome;
	public $st_projetopedagogico;
	public $id_perfilpedagogico;
	
	/**
	 * @return the $st_nome
	 */
	public function getSt_nome() {
		return $this->st_nome;
	}

	/**
	 * @param field_type $st_nome
	 */
	public function setSt_nome($st_nome) {
		$this->st_nome = $st_nome;
	}
	
	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @param $st_projetopedagogico the $st_projetopedagogico to set
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	public function getId_perfilpedagogico()
	{
		return $this->id_perfilpedagogico;
	}

	public function setId_perfilpedagogico($id_perfilpedagogico)
	{
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}

	
}

?>
<?php
class EstadocivilImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_estadocivilimportacao;
	public $id_estadocivil;
	public $nu_codestadocivilorigem;
	public $st_estadocivilorigem;
	
	/**
	 * @return the $id_estadocivilimportacao
	 */
	public function getId_estadocivilimportacao() {
		return $this->id_estadocivilimportacao;
	}

	/**
	 * @return the $id_estadocivil
	 */
	public function getId_estadocivil() {
		return $this->id_estadocivil;
	}

	/**
	 * @return the $nu_codestadocivilorigem
	 */
	public function getNu_codestadocivilorigem() {
		return $this->nu_codestadocivilorigem;
	}

	/**
	 * @return the $st_estadocivilorigem
	 */
	public function getSt_estadocivilorigem() {
		return $this->st_estadocivilorigem;
	}

	/**
	 * @param field_type $id_estadocivilimportacao
	 */
	public function setId_estadocivilimportacao($id_estadocivilimportacao) {
		$this->id_estadocivilimportacao = $id_estadocivilimportacao;
	}

	/**
	 * @param field_type $id_estadocivil
	 */
	public function setId_estadocivil($id_estadocivil) {
		$this->id_estadocivil = $id_estadocivil;
	}

	/**
	 * @param field_type $nu_codestadocivilorigem
	 */
	public function setNu_codestadocivilorigem($nu_codestadocivilorigem) {
		$this->nu_codestadocivilorigem = $nu_codestadocivilorigem;
	}

	/**
	 * @param field_type $st_estadocivilorigem
	 */
	public function setSt_estadocivilorigem($st_estadocivilorigem) {
		$this->st_estadocivilorigem = $st_estadocivilorigem;
	}

	
}
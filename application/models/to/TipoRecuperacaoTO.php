<?php
/**
 * Classe para encapsular os dados de tipo de recuperação.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoRecuperacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de recuperação.
	 * @var int
	 */
	public $id_tiporecuperacao;
	
	/**
	 * Nome do tipo de recuperação.
	 * @var String
	 */
	public $st_tiporecuperacao;
	
	/**
	 * Descrição do  tipo de recuperação.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tiporecuperacao() {
		return $this->id_tiporecuperacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_tiporecuperacao() {
		return $this->st_tiporecuperacao;
	}
	
	/**
	 * @param int $id_tiporecuperacao
	 */
	public function setId_tiporecuperacao($id_tiporecuperacao) {
		$this->id_tiporecuperacao = $id_tiporecuperacao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param String $st_tiporecuperacao
	 */
	public function setSt_tiporecuperacao($st_tiporecuperacao) {
		$this->st_tiporecuperacao = $st_tiporecuperacao;
	}

}

?>
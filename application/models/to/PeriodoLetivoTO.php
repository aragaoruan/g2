<?php
/**
 * Classe para encapsular os dados de período letivo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PeriodoLetivoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do período letivo.
	 * @var int
	 */
	public $id_periodoletivo;
	
	/**
	 * Título do período letivo.
	 * @var string
	 */
	public $st_periodoletivo;
	
	/**
	 * Data de ínicio de inscrição do período letivo.
	 * @var string
	 */
	public $dt_inicioinscricao;
	
	/**
	 * Data do fim de inscrição do período letivo.
	 * @var string
	 */
	public $dt_fiminscricao;
	
	/**
	 * Data de abertura do período letivo.
	 * @var string
	 */
	public $dt_abertura;
	
	/**
	 * Data de encerramento do período letivo.
	 * @var string
	 */
	public $dt_encerramento;
	
	/**
	 * Id da entidade do período letivo.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário que cadastrou o período letivo.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * @return string
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}
	
	/**
	 * @return string
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}
	
	/**
	 * @return string
	 */
	public function getDt_fiminscricao() {
		return $this->dt_fiminscricao;
	}
	
	/**
	 * @return string
	 */
	public function getDt_inicioinscricao() {
		return $this->dt_inicioinscricao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_periodoletivo() {
		return $this->id_periodoletivo;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return string
	 */
	public function getSt_periodoletivo() {
		return $this->st_periodoletivo;
	}
	
	/**
	 * @param string $dt_abertura
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}
	
	/**
	 * @param string $dt_encerramento
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}
	
	/**
	 * @param string $dt_fiminscricao
	 */
	public function setDt_fiminscricao($dt_fiminscricao) {
		$this->dt_fiminscricao = $dt_fiminscricao;
	}
	
	/**
	 * @param string $dt_inicioinscricao
	 */
	public function setDt_inicioinscricao($dt_inicioinscricao) {
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_periodoletivo
	 */
	public function setId_periodoletivo($id_periodoletivo) {
		$this->id_periodoletivo = $id_periodoletivo;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param string $st_periodoletivo
	 */
	public function setSt_periodoletivo($st_periodoletivo) {
		$this->st_periodoletivo = $st_periodoletivo;
	}

}

?>
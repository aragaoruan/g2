<?php
/**
 * Classe de valor da tabela tb_avaliacaoimportacao
 * @author Arthur Cláudio de Almeida Pereira <arhtur.almeida@ead1.com.br>
 *
 */
class AvaliacaoImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_historicoimportacao;
	public $id_matriculaimportacao;
	public $id_avaliacaoaluno;
	public $id_sistemaimportacao;
	public $nu_codmatriculaorigem;
	public $nu_codavaliacaoorigem;
	public $nu_coddisciplinaorigem;
	public $nu_nota;
	public $dt_avaliacao;
	public $nu_codprojetoorigem;
	public $nu_codareaorigem;
	public $st_conceitoorigem;
}
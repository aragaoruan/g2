<?php
class DocumentoImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_documentoimportacao;
	public $id_documentos;
	public $nu_coddocumentoorigem;
	public $st_documentoorigem;
	
	/**
	 * @return the $id_documentoimportacao
	 */
	public function getId_documentoimportacao() {
		return $this->id_documentoimportacao;
	}

	/**
	 * @return the $id_documentos
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}

	/**
	 * @return the $nu_coddocumentoorigem
	 */
	public function getNu_coddocumentoorigem() {
		return $this->nu_coddocumentoorigem;
	}

	/**
	 * @return the $st_documentoorigem
	 */
	public function getSt_documentoorigem() {
		return $this->st_documentoorigem;
	}

	/**
	 * @param field_type $id_documentoimportacao
	 */
	public function setId_documentoimportacao($id_documentoimportacao) {
		$this->id_documentoimportacao = $id_documentoimportacao;
	}

	/**
	 * @param field_type $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}

	/**
	 * @param field_type $nu_coddocumentoorigem
	 */
	public function setNu_coddocumentoorigem($nu_coddocumentoorigem) {
		$this->nu_coddocumentoorigem = $nu_coddocumentoorigem;
	}

	/**
	 * @param field_type $st_documentoorigem
	 */
	public function setSt_documentoorigem($st_documentoorigem) {
		$this->st_documentoorigem = $st_documentoorigem;
	}

	
}
<?php
/**
 * Classe para vincular o tipo de endereço com a entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoEnderecoEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do tipo de endereço.
	 * @var int
	 */
	public $id_tipoendereco;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}

}

?>
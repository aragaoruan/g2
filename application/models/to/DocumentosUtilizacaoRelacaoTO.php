<?php
/**
 * Classe para encapsular os dados de relação entre documento e sua utilização.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentosUtilizacaoRelacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da utilização do documento.
	 * @var int
	 */
	public $id_documentosutilizacao;
	
	/**
	 * Id do documento.
	 * @var int
	 */
	public $id_documentos;
	
	/**
	 * @return int
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return int
	 */
	public function getId_documentosutilizacao() {
		return $this->id_documentosutilizacao;
	}
	
	/**
	 * @param int $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param int $id_documentosutilizacao
	 */
	public function setId_documentosutilizacao($id_documentosutilizacao) {
		$this->id_documentosutilizacao = $id_documentosutilizacao;
	}

}

?>
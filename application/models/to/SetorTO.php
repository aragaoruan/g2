<?php
/**
 * Classe para encapsular o Setor
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class SetorTO extends Ead1_TO_Dinamico{

	public $id_setor;
	public $id_funcaocadastro;
	public $id_usuariocadastro;
	public $id_entidadecadastro;
	public $id_cargocadastro;
	public $id_situacao;
	public $id_setorpai;
	public $st_setor;
	public $st_localizacao;
	public $dt_cadastro;
	public $bl_ativo;	
	public $ar_setoresfilho;
	
	/**
	 * @return the $id_setor
	 */
	public function getId_setor() {
		return $this->id_setor;
	}

	/**
	 * @return the $id_funcaocadastro
	 */
	public function getId_funcaocadastro() {
		return $this->id_funcaocadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_cargocadastro
	 */
	public function getId_cargocadastro() {
		return $this->id_cargocadastro;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_setorpai
	 */
	public function getId_setorpai() {
		return $this->id_setorpai;
	}

	/**
	 * @return the $st_setor
	 */
	public function getSt_setor() {
		return $this->st_setor;
	}

	/**
	 * @return the $st_localizacao
	 */
	public function getSt_localizacao() {
		return $this->st_localizacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_setor
	 */
	public function setId_setor($id_setor) {
		$this->id_setor = $id_setor;
	}

	/**
	 * @param field_type $id_funcaocadastro
	 */
	public function setId_funcaocadastro($id_funcaocadastro) {
		$this->id_funcaocadastro = $id_funcaocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $id_cargocadastro
	 */
	public function setId_cargocadastro($id_cargocadastro) {
		$this->id_cargocadastro = $id_cargocadastro;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_setorpai
	 */
	public function setId_setorpai($id_setorpai) {
		$this->id_setorpai = $id_setorpai;
	}

	/**
	 * @param field_type $st_setor
	 */
	public function setSt_setor($st_setor) {
		$this->st_setor = $st_setor;
	}

	/**
	 * @param field_type $st_localizacao
	 */
	public function setSt_localizacao($st_localizacao) {
		$this->st_localizacao = $st_localizacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return the $ar_setoresfilho
	 */
	public function getAr_setoresfilho() {
		return $this->ar_setoresfilho;
	}

	/**
	 * @param field_type $ar_setoresfilho
	 */
	public function setAr_setoresfilho($ar_setoresfilho) {
		$this->ar_setoresfilho = $ar_setoresfilho;
	}

	
}
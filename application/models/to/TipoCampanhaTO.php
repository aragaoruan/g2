<?php
/**
 * Classe para encapsular os dados de Tipo de Campanha.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class TipoCampanhaTO extends Ead1_TO_Dinamico{
	
	
	const VENDAS = 1;//	Vendas
	const PRODUTOS = 2; //	Produtos
	const PONTUALIDADE = 3; //	Pontualidade
	
	
	
	public $id_tipocampanha;
	public $st_tipocampanha;
	public $bl_ativo;
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_tipocampanha
	 */
	public function getId_tipocampanha() {
		return $this->id_tipocampanha;
	}

	/**
	 * @return the $st_tipocampanha
	 */
	public function getSt_tipocampanha() {
		return $this->st_tipocampanha;
	}

	/**
	 * @param field_type $id_tipocampanha
	 */
	public function setId_tipocampanha($id_tipocampanha) {
		$this->id_tipocampanha = $id_tipocampanha;
	}

	/**
	 * @param field_type $st_tipocampanha
	 */
	public function setSt_tipocampanha($st_tipocampanha) {
		$this->st_tipocampanha = $st_tipocampanha;
	}
}
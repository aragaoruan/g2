<?php
/**
 * Classe para encapsular os dados do conjunto de avaliacao do projetopedagogico
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoConjuntoProjetoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjuntoprojeto;
	public $id_avaliacaoconjunto;
	public $id_projetopedagogico;
	public $dt_inicio;
	public $dt_fim;
	/**
	 * @return the $id_avaliacaoconjuntoprojeto
	 */
	public function getId_avaliacaoconjuntoprojeto() {
		return $this->id_avaliacaoconjuntoprojeto;
	}

	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoprojeto
	 */
	public function setId_avaliacaoconjuntoprojeto($id_avaliacaoconjuntoprojeto) {
		$this->id_avaliacaoconjuntoprojeto = $id_avaliacaoconjuntoprojeto;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	
}
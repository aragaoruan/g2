<?php
/**
 * Classe para encapsular os dados de tipo de calculo da comissao
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TipoCalculoComTO extends Ead1_TO_Dinamico {
	
	public $id_tipocalculocom;
	public $st_tipocalculocom;
	
	/**
	 * @return unknown
	 */
	public function getId_tipocalculocom() {
		return $this->id_tipocalculocom;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipocalculocom() {
		return $this->st_tipocalculocom;
	}
	
	/**
	 * @param unknown_type $id_tipocalculocom
	 */
	public function setId_tipocalculocom($id_tipocalculocom) {
		$this->id_tipocalculocom = $id_tipocalculocom;
	}
	
	/**
	 * @param unknown_type $st_tipocalculocom
	 */
	public function setSt_tipocalculocom($st_tipocalculocom) {
		$this->st_tipocalculocom = $st_tipocalculocom;
	}
}
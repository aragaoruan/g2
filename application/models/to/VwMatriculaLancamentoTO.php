<?php

/**
 * Classe para encapsular os dados da view de VwMatriculaLancamentoTO
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class VwMatriculaLancamentoTO extends Ead1_TO_Dinamico {

    public $id_matricula;
    public $st_nomecompleto;
    public $bl_quitado;
    public $dt_cadastro;
    public $dt_emissao;
    public $dt_prevquitado;
    public $dt_quitado;
    public $dt_vencimento;
    public $id_entidade;
    public $id_lancamento;
    public $id_meiopagamento;
    public $st_meiopagamento;
    public $id_tipolancamento;
    public $id_usuariolancamento;
    public $st_usuariolancamento;
    public $nu_valor;
    public $bl_original;

    /**
     * @return the $id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $bl_quitado
     */
    public function getBl_quitado() {
        return $this->bl_quitado;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return the $dt_emissao
     */
    public function getDt_emissao() {
        return $this->dt_emissao;
    }

    /**
     * @return the $dt_prevquitado
     */
    public function getDt_prevquitado() {
        return $this->dt_prevquitado;
    }

    /**
     * @return the $dt_quitado
     */
    public function getDt_quitado() {
        return $this->dt_quitado;
    }

    /**
     * @return the $dt_vencimento
     */
    public function getDt_vencimento() {
        return $this->dt_vencimento;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento() {
        return $this->id_meiopagamento;
    }

    /**
     * @return the $st_meiopagamento
     */
    public function getSt_meiopagamento() {
        return $this->st_meiopagamento;
    }

    /**
     * @return the $id_tipolancamento
     */
    public function getId_tipolancamento() {
        return $this->id_tipolancamento;
    }

    /**
     * @return the $id_usuariolancamento
     */
    public function getId_usuariolancamento() {
        return $this->id_usuariolancamento;
    }

    /**
     * @return the $st_usuariolancamento
     */
    public function getSt_usuariolancamento() {
        return $this->st_usuariolancamento;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @param $id_matricula the $id_matricula to set
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param $st_nomecompleto the $st_nomecompleto to set
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param $bl_quitado the $bl_quitado to set
     */
    public function setBl_quitado($bl_quitado) {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param $dt_cadastro the $dt_cadastro to set
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param $dt_emissao the $dt_emissao to set
     */
    public function setDt_emissao($dt_emissao) {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @param $dt_prevquitado the $dt_prevquitado to set
     */
    public function setDt_prevquitado($dt_prevquitado) {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param $dt_quitado the $dt_quitado to set
     */
    public function setDt_quitado($dt_quitado) {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param $dt_vencimento the $dt_vencimento to set
     */
    public function setDt_vencimento($dt_vencimento) {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param $id_entidade the $id_entidade to set
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param $id_lancamento the $id_lancamento to set
     */
    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param $id_meiopagamento the $id_meiopagamento to set
     */
    public function setId_meiopagamento($id_meiopagamento) {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param $st_meiopagamento the $st_meiopagamento to set
     */
    public function setSt_meiopagamento($st_meiopagamento) {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @param $id_tipolancamento the $id_tipolancamento to set
     */
    public function setId_tipolancamento($id_tipolancamento) {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @param $id_usuariolancamento the $id_usuariolancamento to set
     */
    public function setId_usuariolancamento($id_usuariolancamento) {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @param $st_usuariolancamento the $st_usuariolancamento to set
     */
    public function setSt_usuariolancamento($st_usuariolancamento) {
        $this->st_usuariolancamento = $st_usuariolancamento;
    }

    /**
     * @param $nu_valor the $nu_valor to set
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function getBl_original() {
        return $this->bl_original;
    }

    public function setBl_original($bl_original) {
        $this->bl_original = $bl_original;
    }

}

<?php
/**
 * Classe para encapsular os dados de Avaliacao Disciplina.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoDisciplinaTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaodisciplina;
	public $id_disciplina;
	public $id_avaliacao;
	
	/**
	 * @return the $id_avaliacaodisciplina
	 */
	public function getId_avaliacaodisciplina() {
		return $this->id_avaliacaodisciplina;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @param $id_avaliacaodisciplina the $id_avaliacaodisciplina to set
	 */
	public function setId_avaliacaodisciplina($id_avaliacaodisciplina) {
		$this->id_avaliacaodisciplina = $id_avaliacaodisciplina;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_avaliacao the $id_avaliacao to set
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}
  
}
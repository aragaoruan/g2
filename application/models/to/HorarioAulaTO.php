<?php
/**
 * Classe para encapsular os dados de horário de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class HorarioAulaTO extends Ead1_TO_Dinamico {

	/**
	 * id do horário de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Nome do horário de aula.
	 * @var string
	 */
	public $st_horarioaula;
	
	/**
	 * Hora de início da aula.
	 * @var string
	 */
	public $hr_inicio;
	
	/**
	 * Horário de fim da aula.
	 * @var string
	 */
	public $hr_fim;
	
	/**
	 * Id do turno.
	 * @var int
	 */
	public $id_turno;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return string
	 */
	public function getHr_fim() {
		return $this->hr_fim;
	}
	
	/**
	 * @return string
	 */
	public function getHr_inicio() {
		return $this->hr_inicio;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_turno() {
		return $this->id_turno;
	}
	
	/**
	 * @return string
	 */
	public function getSt_horarioaula() {
		return $this->st_horarioaula;
	}
	
	/**
	 * @param string $hr_fim
	 */
	public function setHr_fim($hr_fim) {
		$this->hr_fim = $hr_fim;
	}
	
	/**
	 * @param string $hr_inicio
	 */
	public function setHr_inicio($hr_inicio) {
		$this->hr_inicio = $hr_inicio;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}
	
	/**
	 * @param int $id_turno
	 */
	public function setId_turno($id_turno) {
		$this->id_turno = $id_turno;
	}
	
	/**
	 * @param string $st_horarioaula
	 */
	public function setSt_horarioaula($st_horarioaula) {
		$this->st_horarioaula = $st_horarioaula;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de tipo de comissão
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TipoComissaoTO extends Ead1_TO_Dinamico {
	
	public $id_tipocomissao;
	public $st_tipocomissao;
	
	/**
	 * @return unknown
	 */
	public function getId_tipocomissao() {
		return $this->id_tipocomissao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipocomissao() {
		return $this->st_tipocomissao;
	}
	
	/**
	 * @param unknown_type $id_tipocomissao
	 */
	public function setId_tipocomissao($id_tipocomissao) {
		$this->id_tipocomissao= $id_tipocomissao;
	}
	
	/**
	 * @param unknown_type $st_tipocomissao
	 */
	public function setSt_funcao($st_tipocomissao) {
		$this->st_tipocomissao = $st_tipocomissao;
	}
}
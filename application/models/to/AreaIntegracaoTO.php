<?php
/**
 * Classe para encapsular os dados de area integração.
 * @author Eder Lamar
 * 
 * @package models
 * @subpackage to
 */
class AreaIntegracaoTO extends Ead1_TO_Dinamico{
	
	public $id_areaintegracao;
	public $id_areaconhecimento;
	public $id_sistema;
	public $id_usuariocadastro;
	public $st_codsistema;
	public $dt_cadastro;
	
	/**
	 * @return the $id_areaintegracao
	 */
	public function getId_areaintegracao() {
		return $this->id_areaintegracao;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_codsistema
	 */
	public function getSt_codsistema() {
		return $this->st_codsistema;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param $id_areaintegracao the $id_areaintegracao to set
	 */
	public function setId_areaintegracao($id_areaintegracao) {
		$this->id_areaintegracao = $id_areaintegracao;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $st_codsistema the $st_codsistema to set
	 */
	public function setSt_codsistema($st_codsistema) {
		$this->st_codsistema = $st_codsistema;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
}
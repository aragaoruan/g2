<?php
/**
 * Classe para encapsular os dados de tipo de calculo de avaliacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class TipoCalculoAvaliacaoTO extends Ead1_TO_Dinamico{
	
	public $id_tipocalculoavaliacao;
	public $st_tipocalculoavaliacao;
	
	/**
	 * @return the $id_tipocalculoavaliacao
	 */
	public function getId_tipocalculoavaliacao() {
		return $this->id_tipocalculoavaliacao;
	}

	/**
	 * @return the $st_tipocalculoavaliacao
	 */
	public function getSt_tipocalculoavaliacao() {
		return $this->st_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $id_tipocalculoavaliacao
	 */
	public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $st_tipocalculoavaliacao
	 */
	public function setSt_tipocalculoavaliacao($st_tipocalculoavaliacao) {
		$this->st_tipocalculoavaliacao = $st_tipocalculoavaliacao;
	}

}
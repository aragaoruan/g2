<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoTextoVariavelTO extends Ead1_TO_Dinamico{
	
	const SIMPLES	= 1;
	const COMPLEXA	= 2;
	const SELECAO	= 3;
	const INSERCAO	= 4;
	const MULTIPLOS_PARAMETROS = 5;

	public $id_tipotextovariavel;
	public $st_tipotextovariavel;

	/**
	 * @return the $id_tipotextovariavel
	 */
	public function getId_tipotextovariavel(){ 
		return $this->id_tipotextovariavel;
	}

	/**
	 * @return the $st_tipotextovariavel
	 */
	public function getSt_tipotextovariavel(){ 
		return $this->st_tipotextovariavel;
	}


	/**
	 * @param field_type $id_tipotextovariavel
	 */
	public function setId_tipotextovariavel($id_tipotextovariavel){ 
		$this->id_tipotextovariavel = $id_tipotextovariavel;
	}

	/**
	 * @param field_type $st_tipotextovariavel
	 */
	public function setSt_tipotextovariavel($st_tipotextovariavel){ 
		$this->st_tipotextovariavel = $st_tipotextovariavel;
	}

}
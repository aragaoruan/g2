<?php
/**
 * Classe para encapsular os dados da view de projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 22/10/2010
 * 
 * @package models
 * @subpackage to
 */
class VwProjetoPedagogicoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Contém o nome do projeto pedagógico.
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * Contém a descrição do projeto pedagógico.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Idade mínima para iniciar.
	 * @var int
	 */
	public $nu_idademinimainiciar;
	
	/**
	 * Tempo mínimo para concluir.
	 * @var int
	 */
	public $nu_tempominimoconcluir;
	
	/**
	 * Tempo máximo de conclusão.
	 * @var int
	 */
	public $nu_tempomaximoconcluir;
	
	/**
	 * Idade mínima para concluir.
	 * @var int
	 */
	public $nu_idademinimaconcluir;
	
	/**
	 * Tempo previsto para concluir.
	 * @var int
	 */
	public $nu_tempopreviconcluir;
	
	/**
	 * Exclusão lógica
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Título de exibição do projeto.
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * Diz se projeto pedagógico auto aloca alunos ou não.
	 * @var boolean
	 */
	public $bl_autoalocaraluno;
	
	/**
	 * Contém o id do projeto pedagógico origem.
	 * @var int
	 */
	public $id_projetopedagogicoorigem;
	
	/**
	 * Nome da versão do projeto pedagógico.
	 * @var string
	 */
	public $st_nomeversao;
	
	/**
	 * Contém o id da situação do projeto.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém o id da trilha.
	 * @var int
	 */
	public $id_trilha;
	
	/**
	 * Id do tipo do tipo de disiciplina extracurricular.
	 * @var int
	 */
	public $id_tipoextracurricular;
	
	/**
	 * Nota máxima de avaliação do projeto.
	 * @var float
	 */
	public $nu_notamaxima;
	
	/**
	 * Percentual para aprovação.
	 * @var float
	 */
	public $nu_percentualaprovacao;
	
	/**
	 * Valor da prova final.
	 * @var int
	 */
	public $nu_valorprovafinal;
	
	/**
	 * Id da entidade cadastradora do projeto pedagógico.
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * Diz se tem prova final ou não.
	 * @var boolean
	 */
	public $bl_provafinal;
	
	/**
	 * Diz se aceita salas sem prova final.
	 * @var boolean
	 */
	public $bl_salasemprovafinal;
	
	/**
	 * Id do tipo de prova final.
	 * @var int
	 */
	public $id_tipoprovafinal;
	
	/**
	 * Define se vai ter ou não recuperação somatório.
	 * @var boolean
	 */
	public $bl_recuperacaosomatorio;
	
	/**
	 * Tipo de aplicação para recuperação somatório.
	 * @var int
	 */
	public $id_recuperacaosomatoriotipo;
	
	/**
	 * Valor da prova de recuperação somatório.
	 * @var int
	 */
	public $nu_recuperacaosomatoriovalor;
	
	/**
	 * Percentual da nota mínimo para fazer recuperação somatório.
	 * @var float
	 */
	public $nu_recuperacaosomatoriopercentual;
	
	/**
	 * Se utiliza recuperação substitutiva parcial.
	 * @var boolean
	 */
	public $bl_recuperacaosubparcial;
	
	/**
	 * Número máximo de aplicações na recuperação substitutiva parcial.
	 * @var int
	 */
	public $nu_recuperacaosubparcialmaxaplicacoes;
	
	/**
	 * Define o máximo da prova da avaliação de recuperação substitutiva parcial que será aproveitada.
	 * @var int
	 */
	public $nu_recuperacaosubparcialmaxaprov;
	
	/**
	 * Percentual de nota mínimo para fazer recuperação substitutiva parcial.
	 * @var float
	 */
	public $nu_recuperacaosubparcialpercentual;
	
	/**
	 * Define se vai haver recuperação substitutiva parcial para prova final.
	 * @var boolean
	 */
	public $bl_recuperacaosubparcialfinal;
	
	/**
	 * Define se vai haver recuperação parcial para nota da disciplina.
	 * @var boolean
	 */
	public $bl_recuperacaosubparcialdisciplina;
	
	/**
	 * Define se tem recuperação substitutiva total.
	 * @var boolean
	 */
	public $bl_recuperacaosubtotal;
	
	/**
	 * Número máximo de aplicações na recuperação substitutiva total.
	 * @var int
	 */
	public $nu_recuperacaosubtotalmaxaplicacoes;
	
	/**
	 * Define o máximo da prova da avaliação de recuperação substitutiva total que será aproveitada.
	 * @var int
	 */
	public $nu_recuperacaosubtotalmaxaprov;
	
	/**
	 * Percentual de nota mínimo para fazer recuperação substitutiva total.
	 * @var float
	 */
	public $nu_recuperacaosubtotalpercentual;
	
	/**
	 * Define se vai haver recuperação total para prova final.
	 * @var boolean
	 */
	public $bl_recuperacaosubtotalfinal;
	
	/**
	 * Define se vai haver recuperação substitutiva total para nota da disciplina.
	 * @var boolean
	 */
	public $bl_recuperacaosubtotaldisciplina;
	
	/**
	 * Objetivo do projeto pedagógico.
	 * @var string
	 */
	public $st_objetivo;
	
	/**
	 * Extrutura curricular do projeto pedagógico.
	 * @var string
	 */
	public $st_estruturacurricular;
	
	/**
	 * Metodologia de avaliação do projeto pedagógico.
	 * @var string
	 */
	public $st_metodologiaavaliacao;
	
	/**
	 * Certificação do projeto pedagógico.
	 * @var string
	 */
	public $st_certificacao;
	
	/**
	 * Valor de apresentação do projeto pedagógico (Embed).
	 * @var string
	 */
	public $st_valorapresentacao;
	
	/**
	 * Duração do contrato do projeto.
	 * @var int
	 */
	public $nu_contratoduracao;
	
	/**
	 * Id do tipo de duração do contrato do projeto.
	 * @var int
	 */
	public $id_projetocontratoduracaotipo;
	
	/**
	 * Id do tipo de multa do contrato quando este é cancelado.
	 * @var int
	 */
	public $id_projetocontratomultatipo;
	
	/**
	 * Antes de quantos meses exite multa no cancelamento do contrato.
	 * @var int
	 */
	public $nu_mesesmulta;
	
	/**
	 * Diz se a multa é proporcional ao tempo de contrato.
	 * @var boolean
	 */
	public $bl_proporcaopormeses;
	
	/**
	 * Campo que define o valor da multa. Podendo ser tanto fixa tanto percentual.
	 * @var int
	 */
	public $nu_contratomultavalor;
	
	/**
	 * Valor do projeto pedagógico.
	 * @var float
	 */
	public $nu_valorprojetopedagogico;
	
	/**
	 * Valor da avaliação adicional do projeto pedagógico.
	 * @var float
	 */
	public $nu_valoravaliacaoadicional;
	
	/**
	 * Valor da recuperação do projeto pedagógico.
	 * @var float
	 */
	public $nu_valorrecuperacao;
	
	/**
	 * Carga horária do projeto pedagógico.
	 * @var int
	 */
	public $nu_cargahoraria;
	
/**
	 * Id do usuário que cadastrou o projeto pedagógico.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Data de cadastro do projeto pedagógico.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Diz se tem disciplina extracurricular.
	 * @var boolean
	 */
	public $bl_disciplinaextracurricular;
	
	/**
	 * Descrição do tipo de disciplina extracurricular.
	 * @var string
	 */
	public $st_descricaotipoextracurricular;
	
	/**
	 * Nome do tipo de disciplina extracurricular.
	 * @var string
	 */
	public $st_tipoextracurricular;
	
	/**
	 * Descrição do tipo de prova final.
	 * @var string
	 */
	public $st_descricaotipoprovafinal;
	
	/**
	 * Tipo de prova final.
	 * @var string
	 */
	public $st_tipoprovafinal;
	
	/**
	 * Descrição do tipo de aplicação de avaliação.
	 * @var string
	 */
	public $st_descricaotipoaplicacaoavaliacao;
	
	/**
	 * Tipo de aplicação de avaliação.
	 * @var string
	 */
	public $st_tipoaplicacaoavaliacao;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Nome completo do usuário.
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Login do usuário.
	 * @var string
	 */
	public $st_login;
	
	/**
	 * Id da medida de tempo usada.
	 * @var int
	 */
	public $id_medidatempoconclusao;
	
	/**
	 * @return int
	 */
	public function getId_medidatempoconclusao() {
		return $this->id_medidatempoconclusao;
	}
	
	/**
	 * @param int $id_medidatempoconclusao
	 */
	public function setId_medidatempoconclusao($id_medidatempoconclusao) {
		$this->id_medidatempoconclusao = $id_medidatempoconclusao;
	}
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_autoalocaraluno() {
		return $this->bl_autoalocaraluno;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_disciplinaextracurricular() {
		return $this->bl_disciplinaextracurricular;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_proporcaopormeses() {
		return $this->bl_proporcaopormeses;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_provafinal() {
		return $this->bl_provafinal;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosomatorio() {
		return $this->bl_recuperacaosomatorio;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubparcial() {
		return $this->bl_recuperacaosubparcial;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubparcialdisciplina() {
		return $this->bl_recuperacaosubparcialdisciplina;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubparcialfinal() {
		return $this->bl_recuperacaosubparcialfinal;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubtotal() {
		return $this->bl_recuperacaosubtotal;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubtotaldisciplina() {
		return $this->bl_recuperacaosubtotaldisciplina;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_recuperacaosubtotalfinal() {
		return $this->bl_recuperacaosubtotalfinal;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_salasemprovafinal() {
		return $this->bl_salasemprovafinal;
	}
	
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetocontratoduracaotipo() {
		return $this->id_projetocontratoduracaotipo;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetocontratomultatipo() {
		return $this->id_projetocontratomultatipo;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogicoorigem() {
		return $this->id_projetopedagogicoorigem;
	}
	
	/**
	 * @return int
	 */
	public function getId_recuperacaosomatoriotipo() {
		return $this->id_recuperacaosomatoriotipo;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoextracurricular() {
		return $this->id_tipoextracurricular;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoprovafinal() {
		return $this->id_tipoprovafinal;
	}
	
	/**
	 * @return int
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}
	
	/**
	 * @return int
	 */
	public function getNu_contratoduracao() {
		return $this->nu_contratoduracao;
	}
	
	/**
	 * @return int
	 */
	public function getNu_contratomultavalor() {
		return $this->nu_contratomultavalor;
	}
	
	/**
	 * @return int
	 */
	public function getNu_idademinimaconcluir() {
		return $this->nu_idademinimaconcluir;
	}
	
	/**
	 * @return int
	 */
	public function getNu_idademinimainiciar() {
		return $this->nu_idademinimainiciar;
	}
	
	/**
	 * @return int
	 */
	public function getNu_mesesmulta() {
		return $this->nu_mesesmulta;
	}
	
	/**
	 * @return float
	 */
	public function getNu_notamaxima() {
		return $this->nu_notamaxima;
	}
	
	/**
	 * @return float
	 */
	public function getNu_percentualaprovacao() {
		return $this->nu_percentualaprovacao;
	}
	
	/**
	 * @return float
	 */
	public function getNu_recuperacaosomatoriopercentual() {
		return $this->nu_recuperacaosomatoriopercentual;
	}
	
	/**
	 * @return int
	 */
	public function getNu_recuperacaosomatoriovalor() {
		return $this->nu_recuperacaosomatoriovalor;
	}
	
	/**
	 * @return int
	 */
	public function getNu_recuperacaosubparcialmaxaplicacoes() {
		return $this->nu_recuperacaosubparcialmaxaplicacoes;
	}
	
	/**
	 * @return int
	 */
	public function getNu_recuperacaosubparcialmaxaprov() {
		return $this->nu_recuperacaosubparcialmaxaprov;
	}
	
	/**
	 * @return float
	 */
	public function getNu_recuperacaosubparcialpercentual() {
		return $this->nu_recuperacaosubparcialpercentual;
	}
	
	/**
	 * @return int
	 */
	public function getNu_recuperacaosubtotalmaxaplicacoes() {
		return $this->nu_recuperacaosubtotalmaxaplicacoes;
	}
	
	/**
	 * @return int
	 */
	public function getNu_recuperacaosubtotalmaxaprov() {
		return $this->nu_recuperacaosubtotalmaxaprov;
	}
	
	/**
	 * @return float
	 */
	public function getNu_recuperacaosubtotalpercentual() {
		return $this->nu_recuperacaosubtotalpercentual;
	}
	
	/**
	 * @return int
	 */
	public function getNu_tempomaximoconcluir() {
		return $this->nu_tempomaximoconcluir;
	}
	
	/**
	 * @return int
	 */
	public function getNu_tempominimoconcluir() {
		return $this->nu_tempominimoconcluir;
	}
	
	/**
	 * @return int
	 */
	public function getNu_tempopreviconcluir() {
		return $this->nu_tempopreviconcluir;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valoravaliacaoadicional() {
		return $this->nu_valoravaliacaoadicional;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valorprojetopedagogico() {
		return $this->nu_valorprojetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getNu_valorprovafinal() {
		return $this->nu_valorprovafinal;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valorrecuperacao() {
		return $this->nu_valorrecuperacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_certificacao() {
		return $this->st_certificacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaotipoaplicacaoavaliacao() {
		return $this->st_descricaotipoaplicacaoavaliacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaotipoextracurricular() {
		return $this->st_descricaotipoextracurricular;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaotipoprovafinal() {
		return $this->st_descricaotipoprovafinal;
	}
	
	/**
	 * @return string
	 */
	public function getSt_estruturacurricular() {
		return $this->st_estruturacurricular;
	}
	
	/**
	 * @return string
	 */
	public function getSt_login() {
		return $this->st_login;
	}
	
	/**
	 * @return string
	 */
	public function getSt_metodologiaavaliacao() {
		return $this->st_metodologiaavaliacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeversao() {
		return $this->st_nomeversao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_objetivo() {
		return $this->st_objetivo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoaplicacaoavaliacao() {
		return $this->st_tipoaplicacaoavaliacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoextracurricular() {
		return $this->st_tipoextracurricular;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoprovafinal() {
		return $this->st_tipoprovafinal;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_valorapresentacao() {
		return $this->st_valorapresentacao;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param boolean $bl_autoalocaraluno
	 */
	public function setBl_autoalocaraluno($bl_autoalocaraluno) {
		$this->bl_autoalocaraluno = $bl_autoalocaraluno;
	}
	
	/**
	 * @param boolean $bl_disciplinaextracurricular
	 */
	public function setBl_disciplinaextracurricular($bl_disciplinaextracurricular) {
		$this->bl_disciplinaextracurricular = $bl_disciplinaextracurricular;
	}
	
	/**
	 * @param boolean $bl_proporcaopormeses
	 */
	public function setBl_proporcaopormeses($bl_proporcaopormeses) {
		$this->bl_proporcaopormeses = $bl_proporcaopormeses;
	}
	
	/**
	 * @param boolean $bl_provafinal
	 */
	public function setBl_provafinal($bl_provafinal) {
		$this->bl_provafinal = $bl_provafinal;
	}
	
	/**
	 * @param boolean $bl_recuperacaosomatorio
	 */
	public function setBl_recuperacaosomatorio($bl_recuperacaosomatorio) {
		$this->bl_recuperacaosomatorio = $bl_recuperacaosomatorio;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubparcial
	 */
	public function setBl_recuperacaosubparcial($bl_recuperacaosubparcial) {
		$this->bl_recuperacaosubparcial = $bl_recuperacaosubparcial;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubparcialdisciplina
	 */
	public function setBl_recuperacaosubparcialdisciplina($bl_recuperacaosubparcialdisciplina) {
		$this->bl_recuperacaosubparcialdisciplina = $bl_recuperacaosubparcialdisciplina;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubparcialfinal
	 */
	public function setBl_recuperacaosubparcialfinal($bl_recuperacaosubparcialfinal) {
		$this->bl_recuperacaosubparcialfinal = $bl_recuperacaosubparcialfinal;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubtotal
	 */
	public function setBl_recuperacaosubtotal($bl_recuperacaosubtotal) {
		$this->bl_recuperacaosubtotal = $bl_recuperacaosubtotal;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubtotaldisciplina
	 */
	public function setBl_recuperacaosubtotaldisciplina($bl_recuperacaosubtotaldisciplina) {
		$this->bl_recuperacaosubtotaldisciplina = $bl_recuperacaosubtotaldisciplina;
	}
	
	/**
	 * @param boolean $bl_recuperacaosubtotalfinal
	 */
	public function setBl_recuperacaosubtotalfinal($bl_recuperacaosubtotalfinal) {
		$this->bl_recuperacaosubtotalfinal = $bl_recuperacaosubtotalfinal;
	}
	
	/**
	 * @param boolean $bl_salasemprovafinal
	 */
	public function setBl_salasemprovafinal($bl_salasemprovafinal) {
		$this->bl_salasemprovafinal = $bl_salasemprovafinal;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 * @param int $id_projetocontratoduracaotipo
	 */
	public function setId_projetocontratoduracaotipo($id_projetocontratoduracaotipo) {
		$this->id_projetocontratoduracaotipo = $id_projetocontratoduracaotipo;
	}
	
	/**
	 * @param int $id_projetocontratomultatipo
	 */
	public function setId_projetocontratomultatipo($id_projetocontratomultatipo) {
		$this->id_projetocontratomultatipo = $id_projetocontratomultatipo;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_projetopedagogicoorigem
	 */
	public function setId_projetopedagogicoorigem($id_projetopedagogicoorigem) {
		$this->id_projetopedagogicoorigem = $id_projetopedagogicoorigem;
	}
	
	/**
	 * @param int $id_recuperacaosomatoriotipo
	 */
	public function setId_recuperacaosomatoriotipo($id_recuperacaosomatoriotipo) {
		$this->id_recuperacaosomatoriotipo = $id_recuperacaosomatoriotipo;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_tipoextracurricular
	 */
	public function setId_tipoextracurricular($id_tipoextracurricular) {
		$this->id_tipoextracurricular = $id_tipoextracurricular;
	}
	
	/**
	 * @param int $id_tipoprovafinal
	 */
	public function setId_tipoprovafinal($id_tipoprovafinal) {
		$this->id_tipoprovafinal = $id_tipoprovafinal;
	}
	
	/**
	 * @param int $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param int $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}
	
	/**
	 * @param int $nu_contratoduracao
	 */
	public function setNu_contratoduracao($nu_contratoduracao) {
		$this->nu_contratoduracao = $nu_contratoduracao;
	}
	
	/**
	 * @param int $nu_contratomultavalor
	 */
	public function setNu_contratomultavalor($nu_contratomultavalor) {
		$this->nu_contratomultavalor = $nu_contratomultavalor;
	}
	
	/**
	 * @param int $nu_idademinimaconcluir
	 */
	public function setNu_idademinimaconcluir($nu_idademinimaconcluir) {
		$this->nu_idademinimaconcluir = $nu_idademinimaconcluir;
	}
	
	/**
	 * @param int $nu_idademinimainiciar
	 */
	public function setNu_idademinimainiciar($nu_idademinimainiciar) {
		$this->nu_idademinimainiciar = $nu_idademinimainiciar;
	}
	
	/**
	 * @param int $nu_mesesmulta
	 */
	public function setNu_mesesmulta($nu_mesesmulta) {
		$this->nu_mesesmulta = $nu_mesesmulta;
	}
	
	/**
	 * @param float $nu_notamaxima
	 */
	public function setNu_notamaxima($nu_notamaxima) {
		$this->nu_notamaxima = $nu_notamaxima;
	}
	
	/**
	 * @param float $nu_percentualaprovacao
	 */
	public function setNu_percentualaprovacao($nu_percentualaprovacao) {
		$this->nu_percentualaprovacao = $nu_percentualaprovacao;
	}
	
	/**
	 * @param float $nu_recuperacaosomatoriopercentual
	 */
	public function setNu_recuperacaosomatoriopercentual($nu_recuperacaosomatoriopercentual) {
		$this->nu_recuperacaosomatoriopercentual = $nu_recuperacaosomatoriopercentual;
	}
	
	/**
	 * @param int $nu_recuperacaosomatoriovalor
	 */
	public function setNu_recuperacaosomatoriovalor($nu_recuperacaosomatoriovalor) {
		$this->nu_recuperacaosomatoriovalor = $nu_recuperacaosomatoriovalor;
	}
	
	/**
	 * @param int $nu_recuperacaosubparcialmaxaplicacoes
	 */
	public function setNu_recuperacaosubparcialmaxaplicacoes($nu_recuperacaosubparcialmaxaplicacoes) {
		$this->nu_recuperacaosubparcialmaxaplicacoes = $nu_recuperacaosubparcialmaxaplicacoes;
	}
	
	/**
	 * @param int $nu_recuperacaosubparcialmaxaprov
	 */
	public function setNu_recuperacaosubparcialmaxaprov($nu_recuperacaosubparcialmaxaprov) {
		$this->nu_recuperacaosubparcialmaxaprov = $nu_recuperacaosubparcialmaxaprov;
	}
	
	/**
	 * @param float $nu_recuperacaosubparcialpercentual
	 */
	public function setNu_recuperacaosubparcialpercentual($nu_recuperacaosubparcialpercentual) {
		$this->nu_recuperacaosubparcialpercentual = $nu_recuperacaosubparcialpercentual;
	}
	
	/**
	 * @param int $nu_recuperacaosubtotalmaxaplicacoes
	 */
	public function setNu_recuperacaosubtotalmaxaplicacoes($nu_recuperacaosubtotalmaxaplicacoes) {
		$this->nu_recuperacaosubtotalmaxaplicacoes = $nu_recuperacaosubtotalmaxaplicacoes;
	}
	
	/**
	 * @param int $nu_recuperacaosubtotalmaxaprov
	 */
	public function setNu_recuperacaosubtotalmaxaprov($nu_recuperacaosubtotalmaxaprov) {
		$this->nu_recuperacaosubtotalmaxaprov = $nu_recuperacaosubtotalmaxaprov;
	}
	
	/**
	 * @param float $nu_recuperacaosubtotalpercentual
	 */
	public function setNu_recuperacaosubtotalpercentual($nu_recuperacaosubtotalpercentual) {
		$this->nu_recuperacaosubtotalpercentual = $nu_recuperacaosubtotalpercentual;
	}
	
	/**
	 * @param int $nu_tempomaximoconcluir
	 */
	public function setNu_tempomaximoconcluir($nu_tempomaximoconcluir) {
		$this->nu_tempomaximoconcluir = $nu_tempomaximoconcluir;
	}
	
	/**
	 * @param int $nu_tempominimoconcluir
	 */
	public function setNu_tempominimoconcluir($nu_tempominimoconcluir) {
		$this->nu_tempominimoconcluir = $nu_tempominimoconcluir;
	}
	
	/**
	 * @param int $nu_tempopreviconcluir
	 */
	public function setNu_tempopreviconcluir($nu_tempopreviconcluir) {
		$this->nu_tempopreviconcluir = $nu_tempopreviconcluir;
	}
	
	/**
	 * @param float $nu_valoravaliacaoadicional
	 */
	public function setNu_valoravaliacaoadicional($nu_valoravaliacaoadicional) {
		$this->nu_valoravaliacaoadicional = $nu_valoravaliacaoadicional;
	}
	
	/**
	 * @param float $nu_valorprojetopedagogico
	 */
	public function setNu_valorprojetopedagogico($nu_valorprojetopedagogico) {
		$this->nu_valorprojetopedagogico = $nu_valorprojetopedagogico;
	}
	
	/**
	 * @param int $nu_valorprovafinal
	 */
	public function setNu_valorprovafinal($nu_valorprovafinal) {
		$this->nu_valorprovafinal = $nu_valorprovafinal;
	}
	
	/**
	 * @param float $nu_valorrecuperacao
	 */
	public function setNu_valorrecuperacao($nu_valorrecuperacao) {
		$this->nu_valorrecuperacao = $nu_valorrecuperacao;
	}
	
	/**
	 * @param string $st_certificacao
	 */
	public function setSt_certificacao($st_certificacao) {
		$this->st_certificacao = $st_certificacao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_descricaotipoaplicacaoavaliacao
	 */
	public function setSt_descricaotipoaplicacaoavaliacao($st_descricaotipoaplicacaoavaliacao) {
		$this->st_descricaotipoaplicacaoavaliacao = $st_descricaotipoaplicacaoavaliacao;
	}
	
	/**
	 * @param string $st_descricaotipoextracurricular
	 */
	public function setSt_descricaotipoextracurricular($st_descricaotipoextracurricular) {
		$this->st_descricaotipoextracurricular = $st_descricaotipoextracurricular;
	}
	
	/**
	 * @param string $st_descricaotipoprovafinal
	 */
	public function setSt_descricaotipoprovafinal($st_descricaotipoprovafinal) {
		$this->st_descricaotipoprovafinal = $st_descricaotipoprovafinal;
	}
	
	/**
	 * @param string $st_estruturacurricular
	 */
	public function setSt_estruturacurricular($st_estruturacurricular) {
		$this->st_estruturacurricular = $st_estruturacurricular;
	}
	
	/**
	 * @param string $st_login
	 */
	public function setSt_login($st_login) {
		$this->st_login = $st_login;
	}
	
	/**
	 * @param string $st_metodologiaavaliacao
	 */
	public function setSt_metodologiaavaliacao($st_metodologiaavaliacao) {
		$this->st_metodologiaavaliacao = $st_metodologiaavaliacao;
	}
	
	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param string $st_nomeversao
	 */
	public function setSt_nomeversao($st_nomeversao) {
		$this->st_nomeversao = $st_nomeversao;
	}
	
	/**
	 * @param string $st_objetivo
	 */
	public function setSt_objetivo($st_objetivo) {
		$this->st_objetivo = $st_objetivo;
	}
	
	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	
	/**
	 * @param string $st_tipoaplicacaoavaliacao
	 */
	public function setSt_tipoaplicacaoavaliacao($st_tipoaplicacaoavaliacao) {
		$this->st_tipoaplicacaoavaliacao = $st_tipoaplicacaoavaliacao;
	}
	
	/**
	 * @param string $st_tipoextracurricular
	 */
	public function setSt_tipoextracurricular($st_tipoextracurricular) {
		$this->st_tipoextracurricular = $st_tipoextracurricular;
	}
	
	/**
	 * @param string $st_tipoprovafinal
	 */
	public function setSt_tipoprovafinal($st_tipoprovafinal) {
		$this->st_tipoprovafinal = $st_tipoprovafinal;
	}
	
	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}
	
	/**
	 * @param string $st_valorapresentacao
	 */
	public function setSt_valorapresentacao($st_valorapresentacao) {
		$this->st_valorapresentacao = $st_valorapresentacao;
	}

}

?>
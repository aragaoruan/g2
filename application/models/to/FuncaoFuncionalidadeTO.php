<?php
/**
 * Classe para encapsular os dados de função com funcionalidade
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class FuncaoFuncionalidadeTO extends Ead1_TO_Dinamico {

	public $id_funcionalidade;
	public $id_funcao;
	
	/**
	 * @return unknown
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @param unknown_type $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
}

?>
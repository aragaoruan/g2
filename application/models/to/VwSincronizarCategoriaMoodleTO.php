<?php
/**
 * Classe para encapsular da vw
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwSincronizarCategoriaMoodleTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	public $st_codarea;
	public $id_entidade;
	public $st_nomecompleto;
	public $nu_perfil;
	public $id_entidadeintegracao;

	
	/**
	 * @return integer $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param integer $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return string $st_codsistemacurso
	 */
	public function getSt_codarea() {
		return $this->st_codarea;
	}

	/**
	 * @param string $st_codarea
	 */
	public function setSt_codarea($st_codarea) {
		$this->st_codarea = $st_codarea;
	}

	/**
	 * @return integer $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param integer $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @return string $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return integer $nu_perfil
	 */
	public function getNu_perfil() {
		return $this->nu_perfil;
	}

	/**
	 * @param integer $nu_perfil
	 */
	public function setNu_perfil($nu_perfil) {
		$this->nu_perfil = $nu_perfil;
	}

	/**
	 * @return integer
	 */
	public function getId_entidadeintegracao()
	{
		return $this->id_entidadeintegracao;
	}

	/**
	 * @param integer $id_entidadeintegracao
	 */
	public function setId_entidadeintegracao($id_entidadeintegracao)
	{
		$this->id_entidadeintegracao = $id_entidadeintegracao;
	}
	
}

?>
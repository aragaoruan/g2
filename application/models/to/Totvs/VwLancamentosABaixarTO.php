<?php
/**
 * @author ederlamar
 *
 */
class Totvs_VwLancamentosABaixarTO extends Ead1_TO_Dinamico{

	public $id_entidade;
	public $id_venda;
	public $id_lancamento;
	public $bl_quitado;
	public $dt_vencimento;
	public $nu_valor;
	public $IDLAN;
	public $STATUSLAN;
	public $DATABAIXA;
	public $VALORBAIXADO;
	public $VALORORIGINAL;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $bl_quitado
	 */
	public function getBl_quitado() {
		return $this->bl_quitado;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $IDLAN
	 */
	public function getIDLAN() {
		return $this->IDLAN;
	}

	/**
	 * @return the $STATUSLAN
	 */
	public function getSTATUSLAN() {
		return $this->STATUSLAN;
	}

	/**
	 * @return the $dt_baixa
	 */
	public function getDt_baixa() {
		return $this->dt_baixa;
	}

	/**
	 * @return the $nu_valorbaixado
	 */
	public function getNu_valorbaixado() {
		return $this->nu_valorbaixado;
	}

	/**
	 * @return the $VALORORIGINAL
	 */
	public function getVALORORIGINAL() {
		return $this->VALORORIGINAL;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $bl_quitado
	 */
	public function setBl_quitado($bl_quitado) {
		$this->bl_quitado = $bl_quitado;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $IDLAN
	 */
	public function setIDLAN($IDLAN) {
		$this->IDLAN = $IDLAN;
	}

	/**
	 * @param field_type $STATUSLAN
	 */
	public function setSTATUSLAN($STATUSLAN) {
		$this->STATUSLAN = $STATUSLAN;
	}

	/**
	 * @param field_type $DATABAIXA
	 */
	public function setDt_baixa($dt_baixa) {
		$this->dt_baixa = $dt_baixa;
	}

	/**
	 * @param field_type $nu_valorbaixado
	 */
	public function setNu_valorbaixado($nu_valorbaixado) {
		$this->nu_valorbaixado = $nu_valorbaixado;
	}

	/**
	 * @param field_type $VALORORIGINAL
	 */
	public function setVALORORIGINAL($VALORORIGINAL) {
		$this->VALORORIGINAL = $VALORORIGINAL;
	}

}

?>
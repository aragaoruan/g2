<?php
/**
 *
 */
class Totvs_VwAtualizarFluxusGestorTO extends Ead1_TO_Dinamico {

	const STATUS_EM_ABERTO = 0;
	const STATUS_BAIXADO = 1;
	const STATUS_CANCELADO = 2;
	const STATUS_BAIXADO_POR_ACORDO = 3;
	
	public $CONTRATO;
	public $PARCELA;
	public $CENTRO_CUSTO;
	public $ID_ACORDO_FILHO;
	public $NOME;
	public $NOME_FANTASIA;
	public $CPF_CNPJ;
	public $VALOR_ORIGINAL;
	public $VALOR_LIQUIDO;
	public $DATA_EMISSAO;
	public $DATA_VENCIMENTO;
	public $DATA_ALTERACAO;
	public $CATEGORIA;
	public $TIPO_LANCAMENTO;
	public $HISTORICO;
	public $COD_MEIO_PAGAMENTO;
	public $MEIO_PAGAMENTO;
	public $NUMERO_DOCUMENTO;
	public $CONTA_CAIXA;
	public $NUMERO_CHEQUE_EXTRATO;
	public $CODIGO_BANCO_EXTRATO;
	public $CODIGO_AGENCIA_EXTRATO;
	public $DATA_VENCIMENTO_CHEQUE_EXTRATO;
	public $id_venda;
	public $id_lancamento;
	public $id_entidade;
	public $id_lancamentoorigem;
	public $id_usuariolancamento;
	public $id_meiopagamento;
	public $IDLAN;
	public $IDLAN_ACORDO_PAI;
	public $CODCFO;
	public $CODCOLIGADA;
	public $STATUSLAN;
	public $DATA_BAIXA;
	public $VALOR_BAIXADO;
	public $NOSSO_NUMERO;
	
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_usuariolancamento
	 */
	public function getId_usuariolancamento() {
		return $this->id_usuariolancamento;
	}

	/**
	 * @return the $id_lancamentoorigem
	 */
	public function getId_lancamentoorigem() {
		return $this->id_lancamentoorigem;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $CONTRATO
	 */
	public function getCONTRATO() {
		return $this->CONTRATO;
	}

	/**
	 * @return the $PARCELA
	 */
	public function getPARCELA() {
		return $this->PARCELA;
	}
	
	/**
	 * @return the $CENTRO_CUSTO
	 */
	public function getCENTRO_CUSTO() {
		return $this->CENTRO_CUSTO;
	}
	
	/**
	 * @return the $ID_ACORDO_FILHO
	 */
	public function getID_ACORDOFILHO() {
		return $this->ID_ACORDO_FILHO;
	}
	
	/**
	 * @return the $NOME
	 */
	public function getNOME() {
		return $this->NOME;
	}
	
	/**
	 * @return the $NOME_FANTASIA
	 */
	public function getNOME_FANTASIA() {
		return $this->NOME_FANTASIA;
	}
	
	/**
	 * @return the $CPF_CNPJ
	 */
	public function getCPF_CNPJ() {
		return $this->CPF_CNPJ;
	}
	
	/**
	 * @return the $VALOR_LIQUIDO
	 */
	public function getVALOR_LIQUIDO() {
		return $this->VALOR_LIQUIDO;
	}
	
	/**
	 * @return the $DATA_EMISSAO
	 */
	public function getDATA_EMISSAO() {
		return $this->DATA_EMISSAO;
	}
	
	/**
	 * @return the $DATA_VENCIMENTO
	 */
	public function getDATA_VENCIMENTO() {
		return $this->DATA_VENCIMENTO;
	}
	
	/**
	 * @return the $NOSSO_NUMERO
	 */
	public function getNOSSO_NUMERO() {
		return $this->NOSSO_NUMERO;
	}
	
	/**
	 * @return the $DATA_ALTERACAO
	 */
	public function getDATA_ALTERACAO() {
		return $this->DATA_ALTERACAO;
	}
	
	/**
	 * @return the $CATEGORIA
	 */
	public function getCATEGORIA() {
		return $this->CATEGORIA;
	}
	
	/**
	 * @return the $TIPO_LANCAMENTO
	 */
	public function getTIPO_LANCAMENTO() {
		return $this->TIPO_LANCAMENTO;
	}
	
	/**
	 * @return the $HISTORICO
	 */
	public function getHISTORICO() {
		return $this->HISTORICO;
	}
	
	/**
	 * @return the $COD_MEIO_PAGAMENTO
	 */
	public function getCOD_MEIO_PAGAMENTO() {
		return $this->COD_MEIO_PAGAMENTO;
	}
	
	/**
	 * @return the $MEIO_PAGAMENTO
	 */
	public function getMEIO_PAGAMENTO() {
		return $this->MEIO_PAGAMENTO;
	}
	
	/**
	 * @return the $NUMERO_DOCUMENTO
	 */
	public function getNUMERO_DOCUMENTO() {
		return $this->NUMERO_DOCUMENTO;
	}
	
	/**
	 * @return the $CONTA_CAIXA
	 */
	public function getCONTA_CAIXA() {
		return $this->CONTA_CAIXA;
	}
	
	/**
	 * @return the $NUMERO_CHEQUE_EXTRATO
	 */
	public function getNUMERO_CHEQUE_EXTRATO() {
		return $this->NUMERO_CHEQUE_EXTRATO;
	}
	
	/**
	 * @return the $CODIGO_BANCO_EXTRATO
	 */
	public function getCODIGO_BANCO_EXTRATO() {
		return $this->CODIGO_BANCO_EXTRATO;
	}
	
	/**
	 * @return the $CODIGO_AGENCIA_EXTRATO
	 */
	public function getCODIGO_AGENCIA_EXTRATO() {
		return $this->CODIGO_AGENCIA_EXTRATO;
	}
	
	/**
	 * @return the $DATA_VENCIMENTO_CHEQUE_EXTRATO
	 */
	public function getDATA_VENCIMENTO_CHEQUE_EXTRATO() {
		return $this->DATA_VENCIMENTO_CHEQUE_EXTRATO;
	}

	/**
	 * @return the $IDLAN
	 */
	public function getIDLAN() {
		return $this->IDLAN;
	}

	/**
	 * @return the $IDLAN_ACORDO_PAI
	 */
	public function getIDLAN_ACORDO_PAI() {
		return $this->IDLAN_ACORDO_PAI;
	}

	/**
	 * @return the $CODCFO
	 */
	public function getCODCFO() {
		return $this->CODCFO;
	}

	/**
	 * @return the $CODCCUSTO
	 */
	public function getCODCCUSTO() {
		return $this->CODCCUSTO;
	}

	/**
	 * @return the $CODCOLIGADA
	 */
	public function getCODCOLIGADA() {
		return $this->CODCOLIGADA;
	}

	/**
	 * @return the $STATUSLAN
	 */
	public function getSTATUSLAN() {
		return $this->STATUSLAN;
	}

	/**
	 * @return the $DATABAIXA
	 */
	public function getDATA_BAIXA() {
		return $this->DATA_BAIXA;
	}

	/**
	 * @return the $VALORBAIXADO
	 */
	public function getVALOR_BAIXADO() {
		return $this->VALOR_BAIXADO;
	}

	/**
	 * @return the $VALORORIGINAL
	 */
	public function getVALOR_ORIGINAL() {
		return $this->VALOR_ORIGINAL;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_usuariolancamento
	 */
	public function setId_usuariolancamento($id_usuariolancamento) {
		$this->id_usuariolancamento = $id_usuariolancamento;
	}

	/**
	 * @param field_type $id_lancamentoorigem
	 */
	public function setId_lancamentoorigem($id_lancamentoorigem) {
		$this->id_lancamentoorigem = $id_lancamentoorigem;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param field_type $CONTRATO
	 */
	public function setCONTRATO($CONTRATO) {
		return $this->CONTRATO = $CONTRATO;
	}
	
	/**
	 * @param field_type $PARCELA
	 */
	public function setPARCELA($PARCELA) {
		return $this->PARCELA = $PARCELA;
	}
	
	/**
	 * @param field_type $CENTRO_CUSTO
	 */
	public function setCENTRO_CUSTO($CENTRO_CUSTO) {
		return $this->CENTRO_CUSTO = $CENTRO_CUSTO;
	}
	
	/**
	 * @param field_type $ID_ACORDO_FILHO
	 */
	public function setID_ACORDO_FILHO($ID_ACORDO_FILHO) {
		return $this->ID_ACORDO_FILHO = $ID_ACORDO_FILHO;
	}
	
	/**
	 * @param field_type $NOME
	 */
	public function setNOME($NOME) {
		return $this->NOME = $NOME;
	}
	
	/**
	 * @param field_type $NOME_FANTASIA
	 */
	public function setNOME_FANTASIA($NOME_FANTASIA) {
		return $this->NOME_FANTASIA = $NOME_FANTASIA;
	}
	
	/**
	 * @param field_type $CPF_CNPJ
	 */
	public function setCPF_CNPJ($CPF_CNPJ) {
		return $this->CPF_CNPJ = $CPF_CNPJ;
	}
	
	/**
	 * @param field_type $VALOR_ORIGINAL
	 */
	public function setVALOR_ORIGINAL($VALOR_ORIGINAL) {
		return $this->VALOR_ORIGINAL = $VALOR_ORIGINAL;
	}
	
	/**
	 * @param field_type $VALOR_LIQUIDO
	 */
	public function setVALOR_LIQUIDO($VALOR_LIQUIDO) {
		return $this->VALOR_LIQUIDO = $VALOR_LIQUIDO;
	}
	
	/**
	 * @param field_type $DATA_EMISSAO
	 */
	public function setDATA_EMISSAO($DATA_EMISSAO) {
		return $this->DATA_EMISSAO = $DATA_EMISSAO;
	}
	
	/**
	 * @param field_type $DATA_VENCIMENTO
	 */
	public function setDATA_VENCIMENTO($DATA_VENCIMENTO) {
		return $this->DATA_VENCIMENTO = $DATA_VENCIMENTO;
	}
	
	/**
	 * @param field_type $NOSSO_NUMERO
	 */
	public function setNOSSO_NUMERO($NOSSO_NUMERO) {
		return $this->NOSSO_NUMERO = $NOSSO_NUMERO;
	}
	
	/**
	 * @param field_type $DATA_ALTERACAO
	 */
	public function setDATA_ALTERACAO($DATA_ALTERACAO) {
		return $this->DATA_ALTERACAO = $DATA_ALTERACAO;
	}
	
	/**
	 * @param field_type $CATEGORIA
	 */
	public function setCATEGORIA($CATEGORIA) {
		return $this->CATEGORIA = $CATEGORIA;
	}
	
	/**
	 * @param field_type $TIPO_LANCAMENTO
	 */
	public function setTIPO_LANCAMENTO($TIPO_LANCAMENTO) {
		return $this->TIPO_LANCAMENTO = $TIPO_LANCAMENTO;
	}
	
	/**
	 * @param field_type $HISTORICO
	 */
	public function setHISTORICO($HISTORICO) {
		return $this->HISTORICO = $HISTORICO;
	}
	
	/**
	 * @param field_type $COD_MEIO_PAGAMENTO
	 */
	public function setCOD_MEIO_PAGAMENTO($COD_MEIO_PAGAMENTO) {
		return $this->COD_MEIO_PAGAMENTO = $COD_MEIO_PAGAMENTO;
	}
	
	/**
	 * @param field_type $MEIO_PAGAMENTO
	 */
	public function setMEIO_PAGAMENTO($MEIO_PAGAMENTO) {
		return $this->MEIO_PAGAMENTO = $MEIO_PAGAMENTO;
	}
	
	/**
	 * @param field_type $NUMERO_DOCUMENTO
	 */
	public function setNUMERO_DOCUMENTO($NUMERO_DOCUMENTO) {
		return $this->NUMERO_DOCUMENTO = $NUMERO_DOCUMENTO;
	}
	
	/**
	 * @param field_type $CONTA_CAIXA
	 */
	public function setCONTA_CAIXA($CONTA_CAIXA) {
		return $this->CONTA_CAIXA = $CONTA_CAIXA;
	}
	
	/**
	 * @param field_type $NUMERO_CHEQUE_EXTRATO
	 */
	public function setNUMERO_CHEQUE_EXTRATO($NUMERO_CHEQUE_EXTRATO) {
		return $this->NUMERO_CHEQUE_EXTRATO = $NUMERO_CHEQUE_EXTRATO;
	}
	
	/**
	 * @param field_type $CODIGO_BANCO_EXTRATO
	 */
	public function setCODIGO_BANCO_EXTRATO($CODIGO_BANCO_EXTRATO) {
		return $this->CODIGO_BANCO_EXTRATO = $CODIGO_BANCO_EXTRATO;
	}
	
	/**
	 * @param field_type $CODIGO_AGENCIA_EXTRATO
	 */
	public function setCODIGO_AGENCIA_EXTRATO($CODIGO_AGENCIA_EXTRATO) {
		return $this->CODIGO_AGENCIA_EXTRATO = $CODIGO_AGENCIA_EXTRATO;
	}
	
	/**
	 * @param field_type $DATA_VENCIMENTO_CHEQUE_EXTRATO
	 */
	public function setDATA_VENCIMENTO_CHEQUE_EXTRATO($DATA_VENCIMENTO_CHEQUE_EXTRATO) {
		return $this->DATA_VENCIMENTO_CHEQUE_EXTRATO = $DATA_VENCIMENTO_CHEQUE_EXTRATO;
	}

	/**
	 * @param field_type $IDLAN
	 */
	public function setIDLAN($IDLAN) {
		$this->IDLAN = $IDLAN;
	}

	/**
	 * @param field_type $IDLAN_ACORDO_PAI
	 */
	public function setIDLAN_ACORDO_PAI($IDLAN_ACORDO_PAI) {
		$this->IDLAN_ACORDO_PAI = $IDLAN_ACORDO_PAI;
	}

	/**
	 * @param field_type $CODCFO
	 */
	public function setCODCFO($CODCFO) {
		$this->CODCFO = $CODCFO;
	}

	/**
	 * @param field_type $CODCCUSTO
	 */
	public function setCODCCUSTO($CODCCUSTO) {
		$this->CODCCUSTO = $CODCCUSTO;
	}

	/**
	 * @param field_type $CODCOLIGADA
	 */
	public function setCODCOLIGADA($CODCOLIGADA) {
		$this->CODCOLIGADA = $CODCOLIGADA;
	}

	/**
	 * @param field_type $STATUSLAN
	 */
	public function setSTATUSLAN($STATUSLAN) {
		$this->STATUSLAN = $STATUSLAN;
	}

	/**
	 * @param field_type $DATA_BAIXA
	 */
	public function setDATA_BAIXA($DATA_BAIXA) {
		$this->DATA_BAIXA = $DATA_BAIXA;
	}

	/**
	 * @param field_type $VALOR_BAIXADO
	 */
	public function setVALOR_BAIXADO($VALOR_BAIXADO) {
		$this->VALOR_BAIXADO = $VALOR_BAIXADO;
	}

}

?>
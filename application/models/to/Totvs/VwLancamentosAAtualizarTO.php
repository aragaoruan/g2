<?php
/**
 * Classe que encapsula os dados da totvs.vw_lancamentosaatualizar
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 */
class Totvs_VwLancamentosAAtualizarTO extends Ead1_TO_Dinamico{

	public $id_entidade;
	public $id_venda;
	public $id_lancamento;
	public $st_codlancamento;
	public $dt_vencimento;
	public $nu_valor;
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $st_codlancamento
	 */
	public function getSt_codlancamento() {
		return $this->st_codlancamento;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $st_codlancamento
	 */
	public function setSt_codlancamento($st_codlancamento) {
		$this->st_codlancamento = $st_codlancamento;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}
}

?>
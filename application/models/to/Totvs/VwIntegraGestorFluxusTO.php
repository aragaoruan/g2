<?php
/**
 * Classe com dados da View de Integração do Gestor2 com o Fluxus 
 * @author Eder Lamar
 * @package to
 * @subpackage Totvs
 */
class Totvs_VwIntegraGestorFluxusTO extends Ead1_TO_Dinamico{
	
	
	public $id_venda;
	public $id_contrato;
	public $id_lancamento;
	public $bl_entrada;
	public $nu_ordem;
	public $id_entidade;
	public $st_caminho;
	public $CODCOLIGADA_FCFO;
	public $CODCFO_FCFO;
	public $NOMEFANTASIA_FCFO;
	public $NOME_FCFO;
	public $CGCCFO_FCFO;
	public $INSCRESTADUAL_FCFO;
	public $PAGREC_FCFO;
	public $RUA_FCFO;
	public $NUMERO_FCFO;
	public $COMPLEMENTO_FCFO;
	public $BAIRRO_FCFO;
	public $CIDADE_FCFO;
	public $CODETD_FCFO;
	public $CEP_FCFO;
	public $TELEFONE_FCFO;
	public $FAX_FCFO;
	public $EMAIL_FCFO;
	public $CONTATO_FCFO;
	public $ATIVO_FCFO;
	public $CODMUNICIPIO_FCFO;
	public $PESSOAFISOUJUR_FCFO;
	public $PAIS_FCFO;
	public $IDCFO_FCFO;
	public $CEI_FCFO;
	public $NACIONALIDADE_FCFO;
	public $TIPOCONTRIBUINTEINSS_FCFO;
	public $IDLAN;
	public $NUMERODOCUMENTO;
	public $PAGREC;
	public $STATUSLAN;
	public $DATAVENCIMENTO;
	public $DATAEMISSAO;
	public $DATAPREVBAIXA;
	public $DATABAIXA;
	public $VALORORIGINAL;
	public $VALORBAIXADO;
	public $VALORCAP;
	public $VALORJUROS;
	public $VALORDESCONTO;
	public $VALOROP1;
	public $VALOROP2;
	public $VALOROP3;
	public $VALORMULTA;
	public $JUROSDIA;
	public $CAPMENSAL;
	public $TAXASVENDOR;
	public $JUROSVENDOR;
	public $CODCXA;
	public $CODTDO;
	public $CODTB1FLX;
	public $CODTB2FLX;
	public $DATAOP2;
	public $CAMPOALFAOP1;
	public $CAMPOALFAOP2;
	public $HISTORICO;
	public $CODCCUSTO;
	public $NUMEROCHEQUE;
	public $DATAVENCIMENTO_CHEQUE;
	public $CODIGOBANCO;
	public $CODIGOAGENCIA;
	public $CONTACORRENTE;
	public $COMPENSADO;
        public $st_linkedserver;
	
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $bl_entrada
	 */
	public function getBl_entrada() {
		return $this->bl_entrada;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_caminho
	 */
	public function getSt_caminho() {
		return $this->st_caminho;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $bl_entrada
	 */
	public function setBl_entrada($bl_entrada) {
		$this->bl_entrada = $bl_entrada;
	}

	/**
	 * @param field_type $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param field_type $st_caminho
	 */
	public function setSt_caminho($st_caminho) {
		$this->st_caminho = $st_caminho;
	}

	/**
	 * @return the $CODCOLIGADA_FCFO
	 */
	public function getCODCOLIGADA_FCFO() {
		return $this->CODCOLIGADA_FCFO;
	}

	/**
	 * @return the $CODCFO_FCFO
	 */
	public function getCODCFO_FCFO() {
		return $this->CODCFO_FCFO;
	}

	/**
	 * @return the $NOMEFANTASIA_FCFO
	 */
	public function getNOMEFANTASIA_FCFO() {
		return $this->NOMEFANTASIA_FCFO;
	}

	/**
	 * @return the $NOME_FCFO
	 */
	public function getNOME_FCFO() {
		return $this->NOME_FCFO;
	}

	/**
	 * @return the $CGCCFO_FCFO
	 */
	public function getCGCCFO_FCFO() {
		return $this->CGCCFO_FCFO;
	}

	/**
	 * @return the $INSCRESTADUAL_FCFO
	 */
	public function getINSCRESTADUAL_FCFO() {
		return $this->INSCRESTADUAL_FCFO;
	}

	/**
	 * @return the $PAGREC_FCFO
	 */
	public function getPAGREC_FCFO() {
		return $this->PAGREC_FCFO;
	}

	/**
	 * @return the $RUA_FCFO
	 */
	public function getRUA_FCFO() {
		return $this->RUA_FCFO;
	}

	/**
	 * @return the $NUMERO_FCFO
	 */
	public function getNUMERO_FCFO() {
		return $this->NUMERO_FCFO;
	}

	/**
	 * @return the $COMPLEMENTO_FCFO
	 */
	public function getCOMPLEMENTO_FCFO() {
		return $this->COMPLEMENTO_FCFO;
	}

	/**
	 * @return the $BAIRRO_FCFO
	 */
	public function getBAIRRO_FCFO() {
		return $this->BAIRRO_FCFO;
	}

	/**
	 * @return the $CIDADE_FCFO
	 */
	public function getCIDADE_FCFO() {
		return $this->CIDADE_FCFO;
	}

	/**
	 * @return the $CODETD_FCFO
	 */
	public function getCODETD_FCFO() {
		return $this->CODETD_FCFO;
	}

	/**
	 * @return the $CEP_FCFO
	 */
	public function getCEP_FCFO() {
		return $this->CEP_FCFO;
	}

	/**
	 * @return the $TELEFONE_FCFO
	 */
	public function getTELEFONE_FCFO() {
		return $this->TELEFONE_FCFO;
	}

	/**
	 * @return the $FAX_FCFO
	 */
	public function getFAX_FCFO() {
		return $this->FAX_FCFO;
	}

	/**
	 * @return the $EMAIL_FCFO
	 */
	public function getEMAIL_FCFO() {
		return $this->EMAIL_FCFO;
	}

	/**
	 * @return the $CONTATO_FCFO
	 */
	public function getCONTATO_FCFO() {
		return $this->CONTATO_FCFO;
	}

	/**
	 * @return the $ATIVO_FCFO
	 */
	public function getATIVO_FCFO() {
		return $this->ATIVO_FCFO;
	}

	/**
	 * @return the $CODMUNICIPIO_FCFO
	 */
	public function getCODMUNICIPIO_FCFO() {
		return $this->CODMUNICIPIO_FCFO;
	}

	/**
	 * @return the $PESSOAFISOUJUR_FCFO
	 */
	public function getPESSOAFISOUJUR_FCFO() {
		return $this->PESSOAFISOUJUR_FCFO;
	}

	/**
	 * @return the $PAIS_FCFO
	 */
	public function getPAIS_FCFO() {
		return $this->PAIS_FCFO;
	}

	/**
	 * @return the $IDCFO_FCFO
	 */
	public function getIDCFO_FCFO() {
		return $this->IDCFO_FCFO;
	}

	/**
	 * @return the $CEI_FCFO
	 */
	public function getCEI_FCFO() {
		return $this->CEI_FCFO;
	}

	/**
	 * @return the $NACIONALIDADE_FCFO
	 */
	public function getNACIONALIDADE_FCFO() {
		return $this->NACIONALIDADE_FCFO;
	}

	/**
	 * @return the $TIPOCONTRIBUINTEINSS_FCFO
	 */
	public function getTIPOCONTRIBUINTEINSS_FCFO() {
		return $this->TIPOCONTRIBUINTEINSS_FCFO;
	}

	/**
	 * @return the $IDLAN
	 */
	public function getIDLAN() {
		return $this->IDLAN;
	}

	/**
	 * @return the $NUMERODOCUMENTO
	 */
	public function getNUMERODOCUMENTO() {
		return $this->NUMERODOCUMENTO;
	}

	/**
	 * @return the $PAGREC
	 */
	public function getPAGREC() {
		return $this->PAGREC;
	}

	/**
	 * @return the $STATUSLAN
	 */
	public function getSTATUSLAN() {
		return $this->STATUSLAN;
	}

	/**
	 * @return the $DATAVENCIMENTO
	 */
	public function getDATAVENCIMENTO() {
		return ($this->DATAVENCIMENTO instanceof DateTime ? "'{$this->DATAVENCIMENTO->format('Y-m-d')}'" : 'NULL' );
	}

	/**
	 * @return the $DATAEMISSAO
	 */
	public function getDATAEMISSAO() {
		return ($this->DATAEMISSAO instanceof DateTime ? "'{$this->DATAEMISSAO->format('Y-m-d')}'" : date("Y-m-d"));
	}

	/**
	 * @return the $DATAPREVBAIXA
	 */
	public function getDATAPREVBAIXA() {
		return ($this->DATAPREVBAIXA instanceof DateTime ? "'{$this->DATAPREVBAIXA->format('Y-m-d')}'" : 'NULL');
	}

	/**
	 * @return the $DATABAIXA
	 */
	public function getDATABAIXA() {
		return ($this->DATABAIXA instanceof DateTime ? "'{$this->DATABAIXA->format('Y-m-d')}'" : date("Y-m-d"));
	}

	/**
	 * @return the $VALORORIGINAL
	 */
	public function getVALORORIGINAL() {
		return $this->VALORORIGINAL;
	}

	/**
	 * @return the $VALORBAIXADO
	 */
	public function getVALORBAIXADO() {
		if($this->getSTATUSLAN() == 1 && $this->VALORBAIXADO == NULL){
			$this->VALORBAIXADO = $this->getVALORORIGINAL();
		}
		
		return ($this->VALORBAIXADO ? $this->VALORBAIXADO : 0);
	}

	/**
	 * @return the $VALORCAP
	 */
	public function getVALORCAP() {
		return ($this->VALORCAP ? $this->VALORCAP : 0);
	}

	/**
	 * @return the $VALORJUROS
	 */
	public function getVALORJUROS() {
		return ($this->VALORJUROS ? $this->VALORJUROS : 0);
	}

	/**
	 * @return the $VALORDESCONTO
	 */
	public function getVALORDESCONTO() {
		return ($this->VALORDESCONTO ? $this->VALORDESCONTO : 0);
	}

	/**
	 * @return the $VALOROP1
	 */
	public function getVALOROP1() {
		return ($this->VALOROP1 ? $this->VALOROP1 : 0);
	}

	/**
	 * @return the $VALOROP2
	 */
	public function getVALOROP2() {
		return ($this->VALOROP2 ? $this->VALOROP2 : 0);
	}

	/**
	 * @return the $VALOROP3
	 */
	public function getVALOROP3() {
		return ($this->VALOROP3 ? $this->VALOROP3 : 0);
	}

	/**
	 * @return the $VALORMULTA
	 */
	public function getVALORMULTA() {
		return ($this->VALORMULTA ? $this->VALORMULTA : 0);
	}

	/**
	 * @return the $JUROSDIA
	 */
	public function getJUROSDIA() {
		return ($this->JUROSDIA ? $this->JUROSDIA : 0);
	}

	/**
	 * @return the $CAPMENSAL
	 */
	public function getCAPMENSAL() {
		return ($this->CAPMENSAL ? $this->CAPMENSAL : 0);
	}

	/**
	 * @return the $TAXASVENDOR
	 */
	public function getTAXASVENDOR() {
		return ($this->TAXASVENDOR ? $this->TAXASVENDOR : 0);
	}

	/**
	 * @return the $JUROSVENDOR
	 */
	public function getJUROSVENDOR() {
		return ($this->JUROSVENDOR ? $this->JUROSVENDOR : 0);
	}

	/**
	 * @return the $CODCXA
	 */
	public function getCODCXA() {
		return $this->CODCXA;
	}

	/**
	 * @return the $CODTDO
	 */
	public function getCODTDO() {
		return $this->CODTDO;
	}

	/**
	 * @return the $CODTB1FLX
	 */
	public function getCODTB1FLX() {
		return $this->CODTB1FLX;
	}

	/**
	 * @return the $CODTB2FLX
	 */
	public function getCODTB2FLX() {
		return $this->CODTB2FLX;
	}

	/**
	 * @return the $DATAOP2
	 */
	public function getDATAOP2() {
		return ($this->DATAOP2 instanceof DateTime ? "'{$this->DATAOP2->format('Y-m-d')}'" : 'NULL');
	}

	/**
	 * @return the $CAMPOALFAOP1
	 */
	public function getCAMPOALFAOP1() {
		return $this->CAMPOALFAOP1;
	}

	/**
	 * @return the $CAMPOALFAOP2
	 */
	public function getCAMPOALFAOP2() {
		return $this->CAMPOALFAOP2;
	}

	/**
	 * @return the $HISTORICO
	 */
	public function getHISTORICO() {
		return $this->HISTORICO;
	}

	/**
	 * @return the $CODCCUSTO
	 */
	public function getCODCCUSTO() {
		return $this->CODCCUSTO;
	}

	/**
	 * @return the $NUMEROCHEQUE
	 */
	public function getNUMEROCHEQUE() {
		return ( $this->NUMEROCHEQUE ? $this->NUMEROCHEQUE : 'NULL');
	}

	/**
	 * @return the $DATAVENCIMENTO_CHEQUE
	 */
	public function getDATAVENCIMENTO_CHEQUE() {
		return ( $this->DATAVENCIMENTO_CHEQUE instanceof DateTime ? $this->DATAVENCIMENTO_CHEQUE->format('Y-m-d') : 'NULL');
	}

	/**
	 * @return the $CODIGOBANCO
	 */
	public function getCODIGOBANCO() {
		return ( $this->CODIGOBANCO ? $this->CODIGOBANCO : 'NULL');
	}

	/**
	 * @return the $CODIGOAGENCIA
	 */
	public function getCODIGOAGENCIA() {
		return ( $this->CODIGOAGENCIA ? $this->CODIGOAGENCIA : 'NULL');
	}

	/**
	 * @return the $CONTACORRENTE
	 */
	public function getCONTACORRENTE() {
		return ( $this->CONTACORRENTE ? $this->CONTACORRENTE : 'NULL');
	}

	/**
	 * @return the $COMPENSADO
	 */
	public function getCOMPENSADO() {
		return ( $this->COMPENSADO ? $this->COMPENSADO : 'NULL');
	}

	/**
	 * @param field_type $CODCOLIGADA_FCFO
	 */
	public function setCODCOLIGADA_FCFO($CODCOLIGADA_FCFO) {
		$this->CODCOLIGADA_FCFO = $CODCOLIGADA_FCFO;
	}

	/**
	 * @param field_type $CODCFO_FCFO
	 */
	public function setCODCFO_FCFO($CODCFO_FCFO) {
		$this->CODCFO_FCFO = $CODCFO_FCFO;
	}

	/**
	 * @param field_type $NOMEFANTASIA_FCFO
	 */
	public function setNOMEFANTASIA_FCFO($NOMEFANTASIA_FCFO) {
		$this->NOMEFANTASIA_FCFO = $NOMEFANTASIA_FCFO;
	}

	/**
	 * @param field_type $NOME_FCFO
	 */
	public function setNOME_FCFO($NOME_FCFO) {
		$this->NOME_FCFO = $NOME_FCFO;
	}

	/**
	 * @param field_type $CGCCFO_FCFO
	 */
	public function setCGCCFO_FCFO($CGCCFO_FCFO) {
		$this->CGCCFO_FCFO = $CGCCFO_FCFO;
	}

	/**
	 * @param field_type $INSCRESTADUAL_FCFO
	 */
	public function setINSCRESTADUAL_FCFO($INSCRESTADUAL_FCFO) {
		$this->INSCRESTADUAL_FCFO = $INSCRESTADUAL_FCFO;
	}

	/**
	 * @param field_type $PAGREC_FCFO
	 */
	public function setPAGREC_FCFO($PAGREC_FCFO) {
		$this->PAGREC_FCFO = $PAGREC_FCFO;
	}

	/**
	 * @param field_type $RUA_FCFO
	 */
	public function setRUA_FCFO($RUA_FCFO) {
		$this->RUA_FCFO = $RUA_FCFO;
	}

	/**
	 * @param field_type $NUMERO_FCFO
	 */
	public function setNUMERO_FCFO($NUMERO_FCFO) {
		$this->NUMERO_FCFO = $NUMERO_FCFO;
	}

	/**
	 * @param field_type $COMPLEMENTO_FCFO
	 */
	public function setCOMPLEMENTO_FCFO($COMPLEMENTO_FCFO) {
		$this->COMPLEMENTO_FCFO = $COMPLEMENTO_FCFO;
	}

	/**
	 * @param field_type $BAIRRO_FCFO
	 */
	public function setBAIRRO_FCFO($BAIRRO_FCFO) {
		$this->BAIRRO_FCFO = $BAIRRO_FCFO;
	}

	/**
	 * @param field_type $CIDADE_FCFO
	 */
	public function setCIDADE_FCFO($CIDADE_FCFO) {
		$this->CIDADE_FCFO = $CIDADE_FCFO;
	}

	/**
	 * @param field_type $CODETD_FCFO
	 */
	public function setCODETD_FCFO($CODETD_FCFO) {
		$this->CODETD_FCFO = $CODETD_FCFO;
	}

	/**
	 * @param field_type $CEP_FCFO
	 */
	public function setCEP_FCFO($CEP_FCFO) {
		$this->CEP_FCFO = $CEP_FCFO;
	}

	/**
	 * @param field_type $TELEFONE_FCFO
	 */
	public function setTELEFONE_FCFO($TELEFONE_FCFO) {
		$this->TELEFONE_FCFO = $TELEFONE_FCFO;
	}

	/**
	 * @param field_type $FAX_FCFO
	 */
	public function setFAX_FCFO($FAX_FCFO) {
		$this->FAX_FCFO = $FAX_FCFO;
	}

	/**
	 * @param field_type $EMAIL_FCFO
	 */
	public function setEMAIL_FCFO($EMAIL_FCFO) {
		$this->EMAIL_FCFO = $EMAIL_FCFO;
	}

	/**
	 * @param field_type $CONTATO_FCFO
	 */
	public function setCONTATO_FCFO($CONTATO_FCFO) {
		$this->CONTATO_FCFO = $CONTATO_FCFO;
	}

	/**
	 * @param field_type $ATIVO_FCFO
	 */
	public function setATIVO_FCFO($ATIVO_FCFO) {
		$this->ATIVO_FCFO = $ATIVO_FCFO;
	}

	/**
	 * @param field_type $CODMUNICIPIO_FCFO
	 */
	public function setCODMUNICIPIO_FCFO($CODMUNICIPIO_FCFO) {
		$this->CODMUNICIPIO_FCFO = $CODMUNICIPIO_FCFO;
	}

	/**
	 * @param field_type $PESSOAFISOUJUR_FCFO
	 */
	public function setPESSOAFISOUJUR_FCFO($PESSOAFISOUJUR_FCFO) {
		$this->PESSOAFISOUJUR_FCFO = $PESSOAFISOUJUR_FCFO;
	}

	/**
	 * @param field_type $PAIS_FCFO
	 */
	public function setPAIS_FCFO($PAIS_FCFO) {
		$this->PAIS_FCFO = $PAIS_FCFO;
	}

	/**
	 * @param field_type $IDCFO_FCFO
	 */
	public function setIDCFO_FCFO($IDCFO_FCFO) {
		$this->IDCFO_FCFO = $IDCFO_FCFO;
	}

	/**
	 * @param field_type $CEI_FCFO
	 */
	public function setCEI_FCFO($CEI_FCFO) {
		$this->CEI_FCFO = $CEI_FCFO;
	}

	/**
	 * @param field_type $NACIONALIDADE_FCFO
	 */
	public function setNACIONALIDADE_FCFO($NACIONALIDADE_FCFO) {
		$this->NACIONALIDADE_FCFO = $NACIONALIDADE_FCFO;
	}

	/**
	 * @param field_type $TIPOCONTRIBUINTEINSS_FCFO
	 */
	public function setTIPOCONTRIBUINTEINSS_FCFO($TIPOCONTRIBUINTEINSS_FCFO) {
		$this->TIPOCONTRIBUINTEINSS_FCFO = $TIPOCONTRIBUINTEINSS_FCFO;
	}

	/**
	 * @param field_type $IDLAN
	 */
	public function setIDLAN($IDLAN) {
		$this->IDLAN = $IDLAN;
	}

	/**
	 * @param field_type $NUMERODOCUMENTO
	 */
	public function setNUMERODOCUMENTO($NUMERODOCUMENTO) {
		$this->NUMERODOCUMENTO = $NUMERODOCUMENTO;
	}

	/**
	 * @param field_type $PAGREC
	 */
	public function setPAGREC($PAGREC) {
		$this->PAGREC = $PAGREC;
	}

	/**
	 * @param field_type $STATUSLAN
	 */
	public function setSTATUSLAN($STATUSLAN) {
		$this->STATUSLAN = $STATUSLAN;
	}

	/**
	 * @param field_type $DATAVENCIMENTO
	 */
	public function setDATAVENCIMENTO($DATAVENCIMENTO) {
		$this->DATAVENCIMENTO = $DATAVENCIMENTO;
	}

	/**
	 * @param field_type $DATAEMISSAO
	 */
	public function setDATAEMISSAO($DATAEMISSAO) {
		$this->DATAEMISSAO = $DATAEMISSAO;
	}

	/**
	 * @param field_type $DATAPREVBAIXA
	 */
	public function setDATAPREVBAIXA($DATAPREVBAIXA) {
		$this->DATAPREVBAIXA = $DATAPREVBAIXA;
	}

	/**
	 * @param field_type $DATABAIXA
	 */
	public function setDATABAIXA($DATABAIXA) {
		$this->DATABAIXA = $DATABAIXA;
	}

	/**
	 * @param field_type $VALORORIGINAL
	 */
	public function setVALORORIGINAL($VALORORIGINAL) {
		$this->VALORORIGINAL = $VALORORIGINAL;
	}

	/**
	 * @param field_type $VALORBAIXADO
	 */
	public function setVALORBAIXADO($VALORBAIXADO) {
		$this->VALORBAIXADO = $VALORBAIXADO;
	}

	/**
	 * @param field_type $VALORCAP
	 */
	public function setVALORCAP($VALORCAP) {
		$this->VALORCAP = $VALORCAP;
	}

	/**
	 * @param field_type $VALORJUROS
	 */
	public function setVALORJUROS($VALORJUROS) {
		$this->VALORJUROS = $VALORJUROS;
	}

	/**
	 * @param field_type $VALORDESCONTO
	 */
	public function setVALORDESCONTO($VALORDESCONTO) {
		$this->VALORDESCONTO = $VALORDESCONTO;
	}

	/**
	 * @param field_type $VALOROP1
	 */
	public function setVALOROP1($VALOROP1) {
		$this->VALOROP1 = $VALOROP1;
	}

	/**
	 * @param field_type $VALOROP2
	 */
	public function setVALOROP2($VALOROP2) {
		$this->VALOROP2 = $VALOROP2;
	}

	/**
	 * @param field_type $VALOROP3
	 */
	public function setVALOROP3($VALOROP3) {
		$this->VALOROP3 = $VALOROP3;
	}

	/**
	 * @param field_type $VALORMULTA
	 */
	public function setVALORMULTA($VALORMULTA) {
		$this->VALORMULTA = $VALORMULTA;
	}

	/**
	 * @param field_type $JUROSDIA
	 */
	public function setJUROSDIA($JUROSDIA) {
		$this->JUROSDIA = $JUROSDIA;
	}

	/**
	 * @param field_type $CAPMENSAL
	 */
	public function setCAPMENSAL($CAPMENSAL) {
		$this->CAPMENSAL = $CAPMENSAL;
	}

	/**
	 * @param field_type $TAXASVENDOR
	 */
	public function setTAXASVENDOR($TAXASVENDOR) {
		$this->TAXASVENDOR = $TAXASVENDOR;
	}

	/**
	 * @param field_type $JUROSVENDOR
	 */
	public function setJUROSVENDOR($JUROSVENDOR) {
		$this->JUROSVENDOR = $JUROSVENDOR;
	}

	/**
	 * @param field_type $CODCXA
	 */
	public function setCODCXA($CODCXA) {
		$this->CODCXA = $CODCXA;
	}

	/**
	 * @param field_type $CODTDO
	 */
	public function setCODTDO($CODTDO) {
		$this->CODTDO = $CODTDO;
	}

	/**
	 * @param field_type $CODTB1FLX
	 */
	public function setCODTB1FLX($CODTB1FLX) {
		$this->CODTB1FLX = $CODTB1FLX;
	}

	/**
	 * @param field_type $CODTB2FLX
	 */
	public function setCODTB2FLX($CODTB2FLX) {
		$this->CODTB2FLX = $CODTB2FLX;
	}

	/**
	 * @param field_type $DATAOP2
	 */
	public function setDATAOP2($DATAOP2) {
		$this->DATAOP2 = $DATAOP2;
	}

	/**
	 * @param field_type $CAMPOALFAOP1
	 */
	public function setCAMPOALFAOP1($CAMPOALFAOP1) {
		$this->CAMPOALFAOP1 = $CAMPOALFAOP1;
	}

	/**
	 * @param field_type $CAMPOALFAOP2
	 */
	public function setCAMPOALFAOP2($CAMPOALFAOP2) {
		$this->CAMPOALFAOP2 = $CAMPOALFAOP2;
	}

	/**
	 * @param field_type $HISTORICO
	 */
	public function setHISTORICO($HISTORICO) {
		$this->HISTORICO = $HISTORICO;
	}

	/**
	 * @param field_type $CODCCUSTO
	 */
	public function setCODCCUSTO($CODCCUSTO) {
		$this->CODCCUSTO = $CODCCUSTO;
	}

	/**
	 * @param field_type $NUMEROCHEQUE
	 */
	public function setNUMEROCHEQUE($NUMEROCHEQUE) {
		$this->NUMEROCHEQUE = $NUMEROCHEQUE;
	}

	/**
	 * @param field_type $DATAVENCIMENTO_CHEQUE
	 */
	public function setDATAVENCIMENTO_CHEQUE($DATAVENCIMENTO_CHEQUE) {
		$this->DATAVENCIMENTO_CHEQUE = $DATAVENCIMENTO_CHEQUE;
	}

	/**
	 * @param field_type $CODIGOBANCO
	 */
	public function setCODIGOBANCO($CODIGOBANCO) {
		$this->CODIGOBANCO = $CODIGOBANCO;
	}

	/**
	 * @param field_type $CODIGOAGENCIA
	 */
	public function setCODIGOAGENCIA($CODIGOAGENCIA) {
		$this->CODIGOAGENCIA = $CODIGOAGENCIA;
	}

	/**
	 * @param field_type $CONTACORRENTE
	 */
	public function setCONTACORRENTE($CONTACORRENTE) {
		$this->CONTACORRENTE = $CONTACORRENTE;
	}

	/**
	 * @param field_type $COMPENSADO
	 */
	public function setCOMPENSADO($COMPENSADO) {
		$this->COMPENSADO = $COMPENSADO;
	}
        
        public function getSt_linkedserver() {
            return $this->st_linkedserver;
        }

        public function setSt_linkedserver($st_linkedserver) {
            $this->st_linkedserver = $st_linkedserver;
        }

}
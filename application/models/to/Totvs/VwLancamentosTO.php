<?php
/**
 * @author ederlamar
 *
 */
class Totvs_VwLancamentosTO extends Ead1_TO_Dinamico {

	public $id_venda;
	public $id_lancamento;
	public $bl_quitado;
	public $dt_vencimento;
	public $nu_valor;
	public $id_usuariolancamento;
	public $st_cpf;
	public $st_nomecompleto;
	public $IDLAN;
	public $CODCFO;
	public $CODCCUSTO;
	public $CODCOLIGADA;
	public $STATUSLAN;
	public $DATABAIXA;
	public $VALORBAIXADO;
	public $VALORORIGINAL;
	
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $bl_quitado
	 */
	public function getBl_quitado() {
		return $this->bl_quitado;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $id_usuariolancamento
	 */
	public function getId_usuariolancamento() {
		return $this->id_usuariolancamento;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $IDLAN
	 */
	public function getIDLAN() {
		return $this->IDLAN;
	}

	/**
	 * @return the $CODCFO
	 */
	public function getCODCFO() {
		return $this->CODCFO;
	}

	/**
	 * @return the $CODCCUSTO
	 */
	public function getCODCCUSTO() {
		return $this->CODCCUSTO;
	}

	/**
	 * @return the $CODCOLIGADA
	 */
	public function getCODCOLIGADA() {
		return $this->CODCOLIGADA;
	}

	/**
	 * @return the $STATUSLAN
	 */
	public function getSTATUSLAN() {
		return $this->STATUSLAN;
	}

	/**
	 * @return the $DATABAIXA
	 */
	public function getDATABAIXA() {
		return $this->DATABAIXA;
	}

	/**
	 * @return the $VALORBAIXADO
	 */
	public function getVALORBAIXADO() {
		return $this->VALORBAIXADO;
	}

	/**
	 * @return the $VALORORIGINAL
	 */
	public function getVALORORIGINAL() {
		return $this->VALORORIGINAL;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $bl_quitado
	 */
	public function setBl_quitado($bl_quitado) {
		$this->bl_quitado = $bl_quitado;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $id_usuariolancamento
	 */
	public function setId_usuariolancamento($id_usuariolancamento) {
		$this->id_usuariolancamento = $id_usuariolancamento;
	}

	/**
	 * @param field_type $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $IDLAN
	 */
	public function setIDLAN($IDLAN) {
		$this->IDLAN = $IDLAN;
	}

	/**
	 * @param field_type $CODCFO
	 */
	public function setCODCFO($CODCFO) {
		$this->CODCFO = $CODCFO;
	}

	/**
	 * @param field_type $CODCCUSTO
	 */
	public function setCODCCUSTO($CODCCUSTO) {
		$this->CODCCUSTO = $CODCCUSTO;
	}

	/**
	 * @param field_type $CODCOLIGADA
	 */
	public function setCODCOLIGADA($CODCOLIGADA) {
		$this->CODCOLIGADA = $CODCOLIGADA;
	}

	/**
	 * @param field_type $STATUSLAN
	 */
	public function setSTATUSLAN($STATUSLAN) {
		$this->STATUSLAN = $STATUSLAN;
	}

	/**
	 * @param field_type $DATABAIXA
	 */
	public function setDATABAIXA($DATABAIXA) {
		$this->DATABAIXA = $DATABAIXA;
	}

	/**
	 * @param field_type $VALORBAIXADO
	 */
	public function setVALORBAIXADO($VALORBAIXADO) {
		$this->VALORBAIXADO = $VALORBAIXADO;
	}

	/**
	 * @param field_type $VALORORIGINAL
	 */
	public function setVALORORIGINAL($VALORORIGINAL) {
		$this->VALORORIGINAL = $VALORORIGINAL;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de view da sala de aula
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class VwSalaDeAulaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contém o id da sala de aula
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * Contém a data de cadastro
	 * @var date
	 */
	public $dt_cadastro;
	
	/**
	 * Contem o nome da sala de aula
	 * @var string
	 */
	public $st_saladeaula;
	
	/**
	 * Contém o valor boolean da sala de aula
	 * @var boolean
	 */
	public $bl_ativa;
	
	/**
	 * Contém o usúario que cadastrou a sala de aula
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Contém a modalidade da sala de aula
	 * @var int
	 */
	public $id_modalidadesaladeaula;
	
	/**
	 * Data de ínicio da inscrição sala de aula 
	 * @var date
	 */
	public $dt_inicioinscricao;
	
	/**
	 * Data de fim da inscrição sala de aula
	 * @var date
	 */
	public $dt_fiminscricao;
	
	/**
	 * Data de abertura da sala de aula
	 * @var date
	 */
	public $dt_abertura;
	
	/**
	 * Descrição da localização
	 * @var string
	 */
	public $st_localizacao;
	
	/**
	 * Contém o id do tipo de sala de aula
	 * @var int
	 */
	public $id_tiposaladeaula;
	
	/**
	 * Data de encerramento da sala de aula
	 * @var date
	 */
	public $dt_encerramento;
	
	/**
	 * Contém o valor da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o valor do período letivo
	 * @var int
	 */
	public $id_periodoletivo;
	
	/**
	 * Define se pode utilizar do período letivo
	 * @var boolean
	 */
	public $bl_usardoperiodoletivo;
	
	/**
	 * Contém o id da situação
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Nome da modalidade da sala de aula
	 * @var string
	 */
	public $st_modalidadesaladeaula;
	
	/**
	 * Id do usuário do professor da sala de aula
	 * @var int
	 */
	public $id_perfilreferencia;
	
	/**
	 * Contém o id do usuário
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da sala de aula que o professor é responsável
	 * @var int
	 */
	public $id_saladeaulaprofessor;
	
	/**
	 * Contém o nome do tipo de avaliação da sala de aula
	 * @var string
	 */
	public $st_tipoavaliacaosaladeaula;
	
	/**
	 * Contém a informação se e titular
	 * @var boolean
	 */
	public $bl_titular;
	
	/**
	 * Valor máximo do número de alunos 
	 * @var int
	 */
	public $nu_maximoalunos;
	
	/**
	 * Descrição
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Nome tipo de sala de aula
	 * @var string
	 */
	public $st_tiposaladeaula;
	
	/**
	 * Nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Descrição do período letivo
	 * @var string
	 */
	public $st_periodoletivo;
	
	/**
	 * Inicio da inscrição do período letivo
	 * @var date
	 */
	public $dt_inicioinscricaoletivo;
	
	/**
	 * Fim da inscrição do período letivo
	 * @var date
	 */
	public $dt_fiminscricaoletivo;
	
	/**
	 * Abertura do período letivo
	 * @var date
	 */
	public $dt_aberturaletivo;
	
	/**
	 * Encerramento do período letivo
	 * @var date
	 */
	public $dt_encerramentoletivo;
	
	/**
	 * Descrição da situação
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * Nome do professor
	 * @var string
	 */
	public $st_nomeprofessor;
	
	/**
	 * Contém o id da disciplina
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Contém o nome da disciplina
	 * @var string
	 */
	public $st_disciplina;
	
	/**
	 * Contém o TIPO da disciplina
	 * @var INT
	 */
	public $id_tipodisciplina;
	
	
	/**
	 * Contém o id do projeto pedagógico
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Contém o nome do projeto pedagógico
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * Contém o id da entidade pai
	 * @var int
	 * @var array
	 */
	public $id_entidadepai;

    public $id_categoriasala;
    public $st_categoriasala;
	
	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @param number $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $bl_ativa
	 */
	public function getBl_ativa() {
		return $this->bl_ativa;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_modalidadesaladeaula
	 */
	public function getId_modalidadesaladeaula() {
		return $this->id_modalidadesaladeaula;
	}

	/**
	 * @return the $dt_inicioinscricao
	 */
	public function getDt_inicioinscricao() {
		return $this->dt_inicioinscricao;
	}

	/**
	 * @return the $dt_fiminscricao
	 */
	public function getDt_fiminscricao() {
		return $this->dt_fiminscricao;
	}

	/**
	 * @return the $dt_abertura
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}

	/**
	 * @return the $st_localizacao
	 */
	public function getSt_localizacao() {
		return $this->st_localizacao;
	}

	/**
	 * @return the $id_tiposaladeaula
	 */
	public function getId_tiposaladeaula() {
		return $this->id_tiposaladeaula;
	}

	/**
	 * @return the $dt_encerramento
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_periodoletivo
	 */
	public function getId_periodoletivo() {
		return $this->id_periodoletivo;
	}

	/**
	 * @return the $bl_usardoperiodoletivo
	 */
	public function getBl_usardoperiodoletivo() {
		return $this->bl_usardoperiodoletivo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_modalidadesaladeaula
	 */
	public function getSt_modalidadesaladeaula() {
		return $this->st_modalidadesaladeaula;
	}

	/**
	 * @return the $id_perfilreferencia
	 */
	public function getId_perfilReferencia() {
		return $this->id_perfilreferencia;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_saladeaulaprofessor
	 */
	public function getId_saladeaulaprofessor() {
		return $this->id_saladeaulaprofessor;
	}

	/**
	 * @return the $st_tipoavaliacaosaladeaula
	 */
	public function getSt_tipoavaliacaosaladeaula() {
		return $this->st_tipoavaliacaosaladeaula;
	}

	/**
	 * @return the $bl_titular
	 */
	public function getBl_titular() {
		return $this->bl_titular;
	}

	/**
	 * @return the $nu_maximoalunos
	 */
	public function getNu_maximoalunos() {
		return $this->nu_maximoalunos;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_tiposaladeaula
	 */
	public function getSt_tiposaladeaula() {
		return $this->st_tiposaladeaula;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_periodoletivo
	 */
	public function getSt_periodoletivo() {
		return $this->st_periodoletivo;
	}

	/**
	 * @return the $dt_inicioinscricaoletivo
	 */
	public function getDt_inicioinscricaoletivo() {
		return $this->dt_inicioinscricaoletivo;
	}

	/**
	 * @return the $dt_fiminscricaoletivo
	 */
	public function getDt_fiminscricaoletivo() {
		return $this->dt_fiminscricaoletivo;
	}

	/**
	 * @return the $dt_aberturaletivo
	 */
	public function getDt_aberturaletivo() {
		return $this->dt_aberturaletivo;
	}

	/**
	 * @return the $dt_encerramentoletivo
	 */
	public function getDt_encerramentoletivo() {
		return $this->dt_encerramentoletivo;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $id_saladeaula the $id_saladeaula to set
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $st_saladeaula the $st_saladeaula to set
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param $bl_ativa the $bl_ativa to set
	 */
	public function setBl_ativa($bl_ativa) {
		$this->bl_ativa = $bl_ativa;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $id_modalidadesaladeaula the $id_modalidadesaladeaula to set
	 */
	public function setId_modalidadesaladeaula($id_modalidadesaladeaula) {
		$this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
	}

	/**
	 * @param $dt_inicioinscricao the $dt_inicioinscricao to set
	 */
	public function setDt_inicioinscricao($dt_inicioinscricao) {
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}

	/**
	 * @param $dt_fiminscricao the $dt_fiminscricao to set
	 */
	public function setDt_fiminscricao($dt_fiminscricao) {
		$this->dt_fiminscricao = $dt_fiminscricao;
	}

	/**
	 * @param $dt_abertura the $dt_abertura to set
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}

	/**
	 * @param $st_localizacao the $st_localizacao to set
	 */
	public function setSt_localizacao($st_localizacao) {
		$this->st_localizacao = $st_localizacao;
	}

	/**
	 * @param $id_tiposaladeaula the $id_tiposaladeaula to set
	 */
	public function setId_tiposaladeaula($id_tiposaladeaula) {
		$this->id_tiposaladeaula = $id_tiposaladeaula;
	}

	/**
	 * @param $dt_encerramento the $dt_encerramento to set
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_periodoletivo the $id_periodoletivo to set
	 */
	public function setId_periodoletivo($id_periodoletivo) {
		$this->id_periodoletivo = $id_periodoletivo;
	}

	/**
	 * @param $bl_usardoperiodoletivo the $bl_usardoperiodoletivo to set
	 */
	public function setBl_usardoperiodoletivo($bl_usardoperiodoletivo) {
		$this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_modalidadesaladeaula the $st_modalidadesaladeaula to set
	 */
	public function setSt_modalidadesaladeaula($st_modalidadesaladeaula) {
		$this->st_modalidadesaladeaula = $st_modalidadesaladeaula;
	}

	/**
	 * @param $id_perfilreferencia the $id_perfilreferencia to set
	 */
	public function setId_perfilReferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_saladeaulaprofessor the $id_saladeaulaprofessor to set
	 */
	public function setId_saladeaulaprofessor($id_saladeaulaprofessor) {
		$this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
	}

	/**
	 * @param $st_tipoavaliacaosaladeaula the $st_tipoavaliacaosaladeaula to set
	 */
	public function setSt_tipoavaliacaosaladeaula($st_tipoavaliacaosaladeaula) {
		$this->st_tipoavaliacaosaladeaula = $st_tipoavaliacaosaladeaula;
	}

	/**
	 * @param $bl_titular the $bl_titular to set
	 */
	public function setBl_titular($bl_titular) {
		$this->bl_titular = $bl_titular;
	}

	/**
	 * @param $nu_maximoalunos the $nu_maximoalunos to set
	 */
	public function setNu_maximoalunos($nu_maximoalunos) {
		$this->nu_maximoalunos = $nu_maximoalunos;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $st_tiposaladeaula the $st_tiposaladeaula to set
	 */
	public function setSt_tiposaladeaula($st_tiposaladeaula) {
		$this->st_tiposaladeaula = $st_tiposaladeaula;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $st_periodoletivo the $st_periodoletivo to set
	 */
	public function setSt_periodoletivo($st_periodoletivo) {
		$this->st_periodoletivo = $st_periodoletivo;
	}

	/**
	 * @param $dt_inicioinscricaoletivo the $dt_inicioinscricaoletivo to set
	 */
	public function setDt_inicioinscricaoletivo($dt_inicioinscricaoletivo) {
		$this->dt_inicioinscricaoletivo = $dt_inicioinscricaoletivo;
	}

	/**
	 * @param $dt_fiminscricaoletivo the $dt_fiminscricaoletivo to set
	 */
	public function setDt_fiminscricaoletivo($dt_fiminscricaoletivo) {
		$this->dt_fiminscricaoletivo = $dt_fiminscricaoletivo;
	}

	/**
	 * @param $dt_aberturaletivo the $dt_aberturaletivo to set
	 */
	public function setDt_aberturaletivo($dt_aberturaletivo) {
		$this->dt_aberturaletivo = $dt_aberturaletivo;
	}

	/**
	 * @param $dt_encerramentoletivo the $dt_encerramentoletivo to set
	 */
	public function setDt_encerramentoletivo($dt_encerramentoletivo) {
		$this->dt_encerramentoletivo = $dt_encerramentoletivo;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}
	
	/**
	 * @return the $st_nomeprofessor
	 */
	public function getSt_nomeprofessor() {
		return $this->st_nomeprofessor;
	}

	/**
	 * @param $st_nomeprofessor the $st_nomeprofessor to set
	 */
	public function setSt_nomeprofessor($st_nomeprofessor) {
		$this->st_nomeprofessor = $st_nomeprofessor;
	}
	
	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $st_projetopedagogico the $st_projetopedagogico to set
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	/**
	 * @return the $id_entidadepai
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}

	/**
	 * @param $id_entidadepai the $id_entidadepai to set
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}

    /**
     * @param mixed $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $st_categoriasala
     */
    public function setSt_categoriasala($st_categoriasala)
    {
        $this->st_categoriasala = $st_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_categoriasala()
    {
        return $this->st_categoriasala;
    }




	
}

?>
<?php

class VwPacoteMatriculaTO extends Ead1_TO_Dinamico {

	public $id_projetopedagogico;
	public $id_matricula;
	public $nu_remessa;
	public $id_entidade;
	public $st_nomecompleto;
	public $id_itemdematerial;
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $nu_remessa
	 */
	public function getNu_remessa() {
		return $this->nu_remessa;
	}

	/**
	 * @param field_type $nu_remessa
	 */
	public function setNu_remessa($nu_remessa) {
		$this->nu_remessa = $nu_remessa;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	
	
}

?>
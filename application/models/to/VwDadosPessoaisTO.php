<?php
/**
 * Classe de valor da view vw_dadospessoais 
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class VwDadosPessoaisTO extends Ead1_TO_Dinamico {
	
	public $id_usuario;
	public $st_cpf;
	public $st_nomecompleto;
	public $st_login;
	public $st_senha;
	public $id_entidade;
	public $st_urlavatar;
	public $id_email;
	public $st_email;


    public $st_nomeentidade;
	public $st_urlsite;

    /**
     * @return mixed
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param mixed $st_cpf
     */
    public function setst_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param mixed $st_nomecompleto
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_login()
    {
        return $this->st_login;
    }

    /**
     * @param mixed $st_login
     */
    public function setst_login($st_login)
    {
        $this->st_login = $st_login;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param mixed $st_senha
     */
    public function setst_senha($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param mixed $st_urlavatar
     */
    public function setst_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_email()
    {
        return $this->id_email;
    }

    /**
     * @param mixed $id_email
     */
    public function setid_email($id_email)
    {
        $this->id_email = $id_email;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_email()
    {
        return $this->st_email;
    }

    /**
     * @param mixed $st_email
     */
    public function setst_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param mixed $st_nomeentidade
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_urlsite()
    {
        return $this->st_urlsite;
    }

    /**
     * @param mixed $st_urlsite
     */
    public function setst_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
        return $this;

    }





}
<?php

/**
 * Classe para encapsular os dados da vw_recorrentevencendo
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 02/09/2014
 * 
 * @package models
 * @subpackage to
 */
class VwRecorrenteVencendoTO extends Ead1_TO_Dinamico {

    public $id_pedidointegracao;
    public $id_venda;
    public $dt_vencimentocartao;
    public $id_entidade;
    public $id_usuario;
    public $id_matricula;

    public function getId_pedidointegracao() {
        return $this->id_pedidointegracao;
    }

    public function getId_venda() {
        return $this->id_venda;
    }

    public function getDt_vencimentocartao() {
        return $this->dt_vencimentocartao;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_pedidointegracao($id_pedidointegracao) {
        $this->id_pedidointegracao = $id_pedidointegracao;
    }

    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

    public function setDt_vencimentocartao($dt_vencimentocartao) {
        $this->dt_vencimentocartao = $dt_vencimentocartao;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

}

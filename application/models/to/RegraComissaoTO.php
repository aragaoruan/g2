<?php
/**
 * Classe para encapsular os dados de Regra de comissão.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class RegraComissaoTO extends Ead1_TO_Dinamico {
	
	public $id_regracomissao;
	public $id_basecalculocom;
	public $id_tipocomissao;
	public $id_tipovalorcomissao;
	public $id_tipometa;
	public $id_comissaoentidade;
	public $id_usuariocadastro;
	public $nu_metamin;
	public $nu_metamax;
	public $nu_valorcomissao;
	public $dt_cadastro;
	public $dt_inicio;
	public $dt_fim;
	public $bl_ativo;
	
	/**
	 * @return int
	 */
	public function getId_regracomissao() {
		return $this->id_regracomissao;
	}
	
	/**
	 * @return int
	 */
	public function getId_basecalculocom() {
		return $this->id_basecalculocom;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipocomissao() {
		return $this->id_tipocomissao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipovalorcomissao() {
		return $this->id_tipovalorcomissao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipometa() {
		return $this->id_tipometa;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return float
	 */
	public function getNu_metamin() {
		return $this->nu_metamin;
	}
	
	/**
	 * @return float
	 */
	public function getNu_metamax() {
		return $this->nu_metamax;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valorcomissao() {
		return $this->nu_valorcomissao;
	}
	
	/**
	 * @return date
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return date
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @return date
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param int $id_regracomissao
	 */
	public function setId_regracomissao($id_regracomissao) {
		$this->id_regracomissao = $id_regracomissao;
	}
	
	/**
	 * @param int $id_basecalculocom
	 */
	public function setId_basecalculocom($id_basecalculocom) {
		$this->id_basecalculocom = $id_basecalculocom;
	}
	
	/**
	 * @param int $id_tipocomissao
	 */
	public function setId_tipocomissao($id_tipocomissao) {
		$this->id_tipocomissao = $id_tipocomissao;
	}
	
	/**
	 * @param int $id_tipovalorcomissao
	 */
	public function setId_tipovalorcomissao($id_tipovalorcomissao) {
		$this->id_tipovalorcomissao = $id_tipovalorcomissao;
	}
	
	/**
	 * @param int $id_tipometa
	 */
	public function setId_tipometa($id_tipometa) {
		$this->id_tipometa = $id_tipometa;
	}
	
	/**
	 * @param int $id_comissaoentidade
	 */
	public function setId_comissaoentidade($id_comissaoentidade) {
		$this->id_comissaoentidade = $id_comissaoentidade;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param float $nu_metamin
	 */
	public function setNu_metamin($nu_metamin) {
		$this->nu_metamin = $nu_metamin;
	}
	
	/**
	 * @param float $nu_metamax
	 */
	public function setNu_metamax($nu_metamax) {
		$this->nu_metamax = $nu_metamax;
	}
	
	/**
	 * @param float $nu_valorcomissao
	 */
	public function setNu_valorcomissao($nu_valorcomissao) {
		$this->nu_valorcomissao = $nu_valorcomissao;
	}
	
	/**
	 * @param date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param date $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	
	/**
	 * @param date $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
}
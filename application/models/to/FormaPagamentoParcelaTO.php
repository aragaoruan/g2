<?php
/**
 * Classe para encapsular os dados de parcelas de formas de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FormaPagamentoParcelaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da parcela de forma de pagamento.
	 * @var int
	 */
	public $id_formapagamentoparcela;
	
	/**
	 * Id da forma de pagamento.
	 * @var int
	 */
	public $id_formapagamento;
	
	/**
	 * Quantidade mínima de parcelas na forma de pagamento.
	 * @var int
	 */
	public $nu_parcelaquantidademin;
	
	/**
	 * Quantidade máxima de parcelas na forma de pagamento.
	 * @var int
	 */
	public $nu_parcelaquantidademax;
	
	/**
	 * Juros da parcela.
	 * @var int
	 */
	public $nu_juros;
	
	/**
	 * Define o valor mínimo para usar o número de parcelas.
	 * @var int
	 */
	public $nu_valormin;
	
	/**
	 * Contem o id do meio pagamento da parcela
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * @return int
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_formapagamentoparcela() {
		return $this->id_formapagamentoparcela;
	}
	
	/**
	 * @return int
	 */
	public function getNu_juros() {
		return $this->nu_juros;
	}
	
	/**
	 * @return int
	 */
	public function getNu_parcelaquantidademax() {
		return $this->nu_parcelaquantidademax;
	}
	
	/**
	 * @return int
	 */
	public function getNu_parcelaquantidademin() {
		return $this->nu_parcelaquantidademin;
	}
	
	/**
	 * @return int
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}
	
	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param int $id_formapagamentoparcela
	 */
	public function setId_formapagamentoparcela($id_formapagamentoparcela) {
		$this->id_formapagamentoparcela = $id_formapagamentoparcela;
	}
	
	/**
	 * @param int $nu_juros
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}
	
	/**
	 * @param int $nu_parcelaquantidademax
	 */
	public function setNu_parcelaquantidademax($nu_parcelaquantidademax) {
		$this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
	}
	
	/**
	 * @param int $nu_parcelaquantidademin
	 */
	public function setNu_parcelaquantidademin($nu_parcelaquantidademin) {
		$this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
	}
	
	/**
	 * @param int $nu_valormin
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}
	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}


}

?>
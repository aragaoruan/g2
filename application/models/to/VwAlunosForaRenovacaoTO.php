<?php

/**
 * Classe para encapsular os dados da vw_Alunosforarenovacao
 * @author Janilson Mendes - janilson.silva@unyleya.com.br
 * @package models
 * @subpackage to
 */
class VwAlunosForaRenovacaoTO extends Ead1_TO_Dinamico
{
    /**
     * @var integer $id_usuario
     */
    public $id_usuario;

    /**
     * @var string $st_nomecompleto
     */
    public $st_nomecompleto;

    /**
     * @var string $st_cpf
     */
    public $st_cpf;

    /**
     * @var string $st_email
     */
    public $st_email;

    /**
     * @var integer $nu_ddd
     */
    public $nu_ddd;

    /**
     * @var integer $nu_telefone
     */
    public $nu_telefone;

    /**
     * @var integer $nu_dddalternativo
     */
    public $nu_dddalternativo;

    /**
     * @var integer $nu_telefonealternativo
     */
    public $nu_telefonealternativo;

    /**
     * @var integer $id_matricula
     */
    public $id_matricula;

    /**
     * @var datetime $dt_matricula
     */
    public $dt_matricula;

    /**
     * @var integer $id_projetopedagogico
     */
    public $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     */
    public $st_projetopedagogico;

    /**
     * @var integer $id_entidade
     */
    public $id_entidade;

    /**
     * @var string $st_outrosmotivos
     */
    public $st_outrosmotivos;

    /**
     * @var boolean $bl_motivodocumentacao
     */
    public $bl_motivodocumentacao;

    /**
     * @var string $st_motivodocumentacao
     */
    public $st_motivodocumentacao;

    /**
     * @var boolean $bl_motivoacademico
     */
    public $bl_motivoacademico;

    /**
     * @var string $st_motivoacademico
     */
    public $st_motivoacademico;

    /**
     * @var boolean $bl_motivofinanceiro
     */
    public $bl_motivofinanceiro;

    /**
     * @var string $st_motivofinanceiro
     */
    public $st_motivofinanceiro;

    /**
     * @var datetime $dt_ultimaoferta
     */
    public $dt_ultimaoferta;

    /**
     * @var integer $id_venda
     */
    public $id_venda;

    /**
     * @var integer $nu_totalocorrencia
     */
    public $nu_totalocorrencia;

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAlunosForaRenovacaoTO
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    /**
     * @param int $nu_ddd
     * @return VwAlunosForaRenovacaoTO
     */
    public function setNu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @param int $nu_telefone
     * @return VwAlunosForaRenovacaoTO
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    /**
     * @param int $nu_dddalternativo
     * @return VwAlunosForaRenovacaoTO
     */
    public function setNu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    /**
     * @param int $nu_telefonealternativo
     * @return VwAlunosForaRenovacaoTO
     */
    public function setNu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAlunosForaRenovacaoTO
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param datetime $dt_matricula
     * @return VwAlunosForaRenovacaoTO
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwAlunosForaRenovacaoTO
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwAlunosForaRenovacaoTO
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_outrosmotivos()
    {
        return $this->st_outrosmotivos;
    }

    /**
     * @param string $st_outrosmotivos
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_outrosmotivos($st_outrosmotivos)
    {
        $this->st_outrosmotivos = $st_outrosmotivos;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivodocumentacao()
    {
        return $this->bl_motivodocumentacao;
    }

    /**
     * @param bool $bl_motivodocumentacao
     * @return VwAlunosForaRenovacaoTO
     */
    public function setBl_motivodocumentacao($bl_motivodocumentacao)
    {
        $this->bl_motivodocumentacao = $bl_motivodocumentacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivodocumentacao()
    {
        return $this->st_motivodocumentacao;
    }

    /**
     * @param string $st_motivodocumentacao
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_motivodocumentacao($st_motivodocumentacao)
    {
        $this->st_motivodocumentacao = $st_motivodocumentacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivoacademico()
    {
        return $this->bl_motivoacademico;
    }

    /**
     * @param bool $bl_motivoacademico
     * @return VwAlunosForaRenovacaoTO
     */
    public function setBl_motivoacademico($bl_motivoacademico)
    {
        $this->bl_motivoacademico = $bl_motivoacademico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivoacademico()
    {
        return $this->st_motivoacademico;
    }

    /**
     * @param string $st_motivoacademico
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_motivoacademico($st_motivoacademico)
    {
        $this->st_motivoacademico = $st_motivoacademico;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivofinanceiro()
    {
        return $this->bl_motivofinanceiro;
    }

    /**
     * @param bool $bl_motivofinanceiro
     * @return VwAlunosForaRenovacaoTO
     */
    public function setBl_motivofinanceiro($bl_motivofinanceiro)
    {
        $this->bl_motivofinanceiro = $bl_motivofinanceiro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivofinanceiro()
    {
        return $this->st_motivofinanceiro;
    }

    /**
     * @param string $st_motivofinanceiro
     * @return VwAlunosForaRenovacaoTO
     */
    public function setSt_motivofinanceiro($st_motivofinanceiro)
    {
        $this->st_motivofinanceiro = $st_motivofinanceiro;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param datetime $dt_ultimaoferta
     * @return VwAlunosForaRenovacaoTO
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     * @return VwAlunosForaRenovacaoTO
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_totalocorrencia()
    {
        return $this->nu_totalocorrencia;
    }

    /**
     * @param int $nu_totalocorrencia
     * @return VwAlunosForaRenovacaoTO
     */
    public function setNu_totalocorrencia($nu_totalocorrencia)
    {
        $this->nu_totalocorrencia = $nu_totalocorrencia;
        return $this;
    }
}

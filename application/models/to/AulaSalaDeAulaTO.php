<?php
/**
 * Classe para encapsular os dados de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 18/10/2010
 * 
 * @package models
 * @subpackage to
 */
class AulaSalaDeAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da aula.
	 * @var int
	 */
	public $id_aulasaladeaula;
	
	/**
	 * Id do dia da semana.
	 * @var int
	 */
	public $id_diasemana;
	
	/**
	 * Id do horário de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Id do tipo de aula.
	 * @var int
	 */
	public $id_tipoaula;
	
	/**
	 * Hora de início da aula.
	 * @var string
	 */
	public $hr_inicio;
	
	/**
	 * Hora de fim da aula.
	 * @var string
	 */
	public $hr_fim;
	
	/**
	 * Data da aula.
	 * @var string
	 */
	public $dt_data;
	
	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * @return int
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
	/**
	 * @return string
	 */
	public function getDt_data() {
		return $this->dt_data;
	}
	
	/**
	 * @return string
	 */
	public function getHr_fim() {
		return $this->hr_fim;
	}
	
	/**
	 * @return string
	 */
	public function getHr_inicio() {
		return $this->hr_inicio;
	}
	
	/**
	 * @return int
	 */
	public function getId_aulasaladeaula() {
		return $this->id_aulasaladeaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_diasemana() {
		return $this->id_diasemana;
	}
	
	/**
	 * @return int
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoaula() {
		return $this->id_tipoaula;
	}
	
	/**
	 * @param string $dt_data
	 */
	public function setDt_data($dt_data) {
		$this->dt_data = $dt_data;
	}
	
	/**
	 * @param string $hr_fim
	 */
	public function setHr_fim($hr_fim) {
		$this->hr_fim = $hr_fim;
	}
	
	/**
	 * @param string $hr_inicio
	 */
	public function setHr_inicio($hr_inicio) {
		$this->hr_inicio = $hr_inicio;
	}
	
	/**
	 * @param int $id_aulasaladeaula
	 */
	public function setId_aulasaladeaula($id_aulasaladeaula) {
		$this->id_aulasaladeaula = $id_aulasaladeaula;
	}
	
	/**
	 * @param int $id_diasemana
	 */
	public function setId_diasemana($id_diasemana) {
		$this->id_diasemana = $id_diasemana;
	}
	
	/**
	 * @param int $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}
	
	/**
	 * @param int $id_tipoaula
	 */
	public function setId_tipoaula($id_tipoaula) {
		$this->id_tipoaula = $id_tipoaula;
	}

}

?>
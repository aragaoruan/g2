<?php
/**
 * Classe para encapsular os dados de Convenio do Contrato.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContratoConvenioTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o id do contrato
	 * @var int
	 */
	public $id_contrato;
	
	/**
	 * Contem o id do convenio
	 * @var int
	 */
	public $id_convenio;
	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_convenio
	 */
	public function getId_convenio() {
		return $this->id_convenio;
	}

	/**
	 * @param int $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param int $id_convenio
	 */
	public function setId_convenio($id_convenio) {
		$this->id_convenio = $id_convenio;
	}

	
	
}
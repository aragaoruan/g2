<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwProdutoTextoSistemaTO extends Ead1_TO_Dinamico{

	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $id_situacao;
	public $id_usuariocadastro;
	public $id_entidade;
	public $bl_ativo;
	public $bl_todasformas;
	public $bl_todascampanhas;
	public $nu_gratuito;
	public $st_tipoproduto;
	public $id_textosistema;
	public $st_textosistema;
	public $st_texto;
	public $id_textocategoria;
	public $st_textocategoria;


	/**
	 * @return the $id_produto
	 */
	public function getId_produto(){ 
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto(){ 
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto(){ 
		return $this->id_tipoproduto;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $bl_todasformas
	 */
	public function getBl_todasformas(){ 
		return $this->bl_todasformas;
	}

	/**
	 * @return the $bl_todascampanhas
	 */
	public function getBl_todascampanhas(){ 
		return $this->bl_todascampanhas;
	}

	/**
	 * @return the $nu_gratuito
	 */
	public function getNu_gratuito(){ 
		return $this->nu_gratuito;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto(){ 
		return $this->st_tipoproduto;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema(){ 
		return $this->id_textosistema;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema(){ 
		return $this->st_textosistema;
	}

	/**
	 * @return the $st_texto
	 */
	public function getSt_texto(){ 
		return $this->st_texto;
	}

	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria(){ 
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_textocategoria
	 */
	public function getSt_textocategoria(){ 
		return $this->st_textocategoria;
	}


	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto){ 
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto){ 
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto){ 
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $bl_todasformas
	 */
	public function setBl_todasformas($bl_todasformas){ 
		$this->bl_todasformas = $bl_todasformas;
	}

	/**
	 * @param field_type $bl_todascampanhas
	 */
	public function setBl_todascampanhas($bl_todascampanhas){ 
		$this->bl_todascampanhas = $bl_todascampanhas;
	}

	/**
	 * @param field_type $nu_gratuito
	 */
	public function setNu_gratuito($nu_gratuito){ 
		$this->nu_gratuito = $nu_gratuito;
	}

	/**
	 * @param field_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto){ 
		$this->st_tipoproduto = $st_tipoproduto;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema){ 
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $st_textosistema
	 */
	public function setSt_textosistema($st_textosistema){ 
		$this->st_textosistema = $st_textosistema;
	}

	/**
	 * @param field_type $st_texto
	 */
	public function setSt_texto($st_texto){ 
		$this->st_texto = $st_texto;
	}

	/**
	 * @param field_type $id_textocategoria
	 */
	public function setId_textocategoria($id_textocategoria){ 
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param field_type $st_textocategoria
	 */
	public function setSt_textocategoria($st_textocategoria){ 
		$this->st_textocategoria = $st_textocategoria;
	}

}
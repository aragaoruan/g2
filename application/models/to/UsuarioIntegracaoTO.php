<?php

/**
 * Classe para encapsular dados da tabela tb_usuariointegracao
 * @author edermariano
 * @package models
 * @subpackage to
 */
class UsuarioIntegracaoTO extends Ead1_TO_Dinamico
{

    public $id_usuariointegracao;
    public $id_sistema;
    public $id_usuariocadastro;
    public $id_usuario;
    public $dt_cadastro;
    public $st_codusuario;
    public $st_senhaintegrada;
    public $st_loginintegrado;
    public $id_entidade;
    public $id_entidadeintegracao;

    /**
     * @return mixed
     */
    public function getId_usuariointegracao()
    {
        return $this->id_usuariointegracao;
    }

    /**
     * @param mixed $id_usuariointegracao
     * @return UsuarioIntegracaoTO
     */
    public function setId_usuariointegracao($id_usuariointegracao)
    {
        $this->id_usuariointegracao = $id_usuariointegracao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     * @return UsuarioIntegracaoTO
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $id_usuariocadastro
     * @return UsuarioIntegracaoTO
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     * @return UsuarioIntegracaoTO
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return UsuarioIntegracaoTO
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param mixed $st_codusuario
     * @return UsuarioIntegracaoTO
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @param mixed $st_senhaintegrada
     * @return UsuarioIntegracaoTO
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @param mixed $st_loginintegrado
     * @return UsuarioIntegracaoTO
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     * @return UsuarioIntegracaoTO
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param mixed $id_entidadeintegracao
     * @return UsuarioIntegracaoTO
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


}

<?php
/**
 * Classe para encapsular os dados do documento de reservista.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentoDeReservistaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o número do certificado.
	 * @var string
	 */
	public $st_numero;
	
	/**
	 * Contém a série do certificado.
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Contém a data do certificado.
	 * @var string
	 */
	public $dt_datareservista;
	
	/**
	 * Contém o id da uf do certificado.
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * Contém o código do IBGE do município.
	 * @var string
	 */
	public $id_municipio;
	
	/**
	 * @return string
	 */
	public function getDt_datareservista() {
		return $this->dt_datareservista;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return string
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_numero() {
		return $this->st_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @return string
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @param string $dt_datareservista
	 */
	public function setDt_datareservista($dt_datareservista) {
		$this->dt_datareservista = $dt_datareservista;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param string $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string st_numeroo
	 */
	public function setSt_numero($st_numero) {
		$this->st_numero = $st_numero;
	}
	
	/**
	 * @param string st_seriee
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}
	
	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

}

?>
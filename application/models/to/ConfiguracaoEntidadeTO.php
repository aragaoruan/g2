<?php

/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
class ConfiguracaoEntidadeTO extends Ead1_TO_Dinamico {

    public $id_configuracaoentidade;
    public $id_entidade;
    public $id_textosistemafichac;
    public $bl_bloqueiadisciplina;
    public $id_modelogradenotas;
    public $bl_encerrasemacesso;
    public $nu_encerramentoprofessor;
    public $nu_maxextensao;
    public $id_textosemsala;
    public $id_assuntoresgate;
    public $id_textoresgate;
    public $id_manualaluno;
    public $bl_emailunico;
    public $id_tabelapreco;
    public $st_informacoestecnicas;
    public $bl_primeiroacesso;
    public $id_textovencimentorecorrente;

    /**
     * @return the $bl_emailunico
     */
    public function getBl_emailunico() {
        return $this->bl_emailunico;
    }

    /**
     * @param field_type $bl_emailunico
     */
    public function setBl_emailunico($bl_emailunico) {
        $this->bl_emailunico = $bl_emailunico;
    }

    /**
     * @return the $id_configuracaoentidade
     */
    public function getId_configuracaoentidade() {
        return $this->id_configuracaoentidade;
    }

    /**
     * @return integer $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $id_textosistemafichac
     */
    public function getId_textosistemafichac() {
        return $this->id_textosistemafichac;
    }

    /**
     * @return the $id_modelogradenotas
     */
    public function getId_modelogradenotas() {
        return $this->id_modelogradenotas;
    }

    /**
     * @return the $bl_bloqueiadisciplina
     */
    public function getBl_bloqueiadisciplina() {
        return $this->bl_bloqueiadisciplina;
    }

    /**
     * @return the $bl_encerrasemacesso
     */
    public function getBl_encerrasemacesso() {
        return $this->bl_encerrasemacesso;
    }

    /**
     * @return the $nu_encerramentoprofessor
     */
    public function getNu_encerramentoprofessor() {
        return $this->nu_encerramentoprofessor;
    }

    /**
     * @return the $nu_maxextensao
     */
    public function getNu_maxextensao() {
        return $this->nu_maxextensao;
    }

    /**
     * @param field_type $id_configuracaoentidade
     */
    public function setId_configuracaoentidade($id_configuracaoentidade) {
        $this->id_configuracaoentidade = $id_configuracaoentidade;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $id_textosistemafichac
     */
    public function setId_textosistemafichac($id_textosistemafichac) {
        $this->id_textosistemafichac = $id_textosistemafichac;
    }

    /**
     * @param field_type $id_modelogradenotas
     */
    public function setId_modelogradenotas($id_modelogradenotas) {
        $this->id_modelogradenotas = $id_modelogradenotas;
    }

    /**
     * @param field_type $bl_bloqueiadisciplina
     */
    public function setBl_bloqueiadisciplina($bl_bloqueiadisciplina) {
        $this->bl_bloqueiadisciplina = $bl_bloqueiadisciplina;
    }

    /**
     * @param field_type $bl_encerrasemacesso
     */
    public function setBl_encerrasemacesso($bl_encerrasemacesso) {
        $this->bl_encerrasemacesso = $bl_encerrasemacesso;
    }

    /**
     * @param field_type $nu_encerramentoprofessor
     */
    public function setNu_encerramentoprofessor($nu_encerramentoprofessor) {
        $this->nu_encerramentoprofessor = $nu_encerramentoprofessor;
    }

    /**
     * @param field_type $nu_maxextensao
     */
    public function setNu_maxextensao($nu_maxextensao) {
        $this->nu_maxextensao = $nu_maxextensao;
    }

    /**
     * @return the $id_textosemsala
     */
    public function getId_textosemsala() {
        return $this->id_textosemsala;
    }

    /**
     * @param field_type $id_textosemsala
     */
    public function setId_textosemsala($id_textosemsala) {
        $this->id_textosemsala = $id_textosemsala;
    }

    /**
     * @return integer $id_assuntoresgate
     */
    public function getId_assuntoresgate() {
        return $this->id_assuntoresgate;
    }

    /**
     * @return the $id_textoresgate
     */
    public function getId_textoresgate() {
        return $this->id_textoresgate;
    }

    /**
     * @param field_type $id_assuntoresgate
     */
    public function setId_assuntoresgate($id_assuntoresgate) {
        $this->id_assuntoresgate = $id_assuntoresgate;
    }

    /**
     * @param field_type $id_textoresgate
     */
    public function setId_textoresgate($id_textoresgate) {
        $this->id_textoresgate = $id_textoresgate;
    }

    /**
     * @return the $id_manualaluno
     */
    public function getId_manualaluno() {
        return $this->id_manualaluno;
    }

    /**
     * @param field_type $id_manualaluno
     */
    public function setId_manualaluno($id_manualaluno) {
        $this->id_manualaluno = $id_manualaluno;
    }

    /**
     * @return the $id_tabelapreco
     */
    public function getId_tabelapreco() {
        return $this->id_tabelapreco;
    }

    /**
     * @param field_type $id_tabelapreco
     */
    public function setId_tabelapreco($id_tabelapreco) {
        $this->id_tabelapreco = $id_tabelapreco;
    }

    /**
     * @param mixed $st_informacoestecnicas
     */
    public function setSt_informacoestecnicas($st_informacoestecnicas) {
        $this->st_informacoestecnicas = $st_informacoestecnicas;
    }

    /**
     * @return mixed
     */
    public function getSt_informacoestecnicas() {
        return $this->st_informacoestecnicas;
    }

    public function getBl_primeiroacesso() {
        return $this->bl_primeiroacesso;
    }

    public function setBl_primeiroacesso($bl_primeiroacesso) {
        $this->bl_primeiroacesso = $bl_primeiroacesso;
    }

    public function getId_textovencimentorecorrente() {
        return $this->id_textovencimentorecorrente;
    }

    public function setId_textovencimentorecorrente($id_textovencimentorecorrente) {
        $this->id_textovencimentorecorrente = $id_textovencimentorecorrente;
    }

}

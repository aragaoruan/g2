<?php
/**
 * Classe para encapsular os dados de Conjunto de Avaliacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoConjuntoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjunto;
	public $id_tipoprova;
	public $id_situacao;
	public $id_tipocalculoavaliacao;
	public $dt_cadastro;
	public $id_usuariocadastro;
	public $id_entidade;
	public $st_avaliacaoconjunto;
	
	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_tipocalculoavaliacao
	 */
	public function getId_tipocalculoavaliacao() {
		return $this->id_tipocalculoavaliacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_avaliacaoconjunto
	 */
	public function getSt_avaliacaoconjunto() {
		return $this->st_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_tipocalculoavaliacao
	 */
	public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_avaliacaoconjunto
	 */
	public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
		$this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
	}

	
}
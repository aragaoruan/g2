<?php
class PesquisarTextoSistemaTO extends VwTextoSistemaTO{
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.entidade.cadastrar.CadastrarTextos';
}
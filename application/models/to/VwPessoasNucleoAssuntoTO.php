<?php
/**
 * Classe para encapsular os dados de vw_pessoasnucleoassunto.
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage to
 */
class VwPessoasNucleoAssuntoTO extends Ead1_TO_Dinamico {
		
	public $st_nomecompleto;
	public $id_usuario;
	public $id_nucleoco;
	
	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	
	
}

?>
<?php

/**
 * Classe para encapsular os dados de Venda.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VendaTO extends Ead1_TO_Dinamico
{

    const EVOLUCAO_CONFIRMADA = 10;
    const EVOLUCAO_AGUARDANDO_RECEBIMENTO = 9;
    const EVOLUCAO_NEGOCIADA = 17;
    const EVOLUCAO_AGUARDANDO_NEGOCIACAO = 7;
    const EVOLUCAO_EM_ANDAMENTO = 18;
    const EVOLUCAO_PROCESSAMENTO = 44;

    const SITUACAO_VENDA_ATIVA = 39;
    const SITUACAO_VENDA_INATIVA = 40;
    const SITUACAO_VENDA_PENDENTE = 46;

    const ORIGEM_CONTRATO = 1; //1 Contrato
    const ORIGEM_SITE = 2; //2 Site
    const ORIGEM_INTERNO = 3; //3 Interno

    public $id_enderecoentrega;

    /**
     * Contem o id da venda
     * @var int
     */
    public $id_venda;

    /**
     * Contem o id da forma de pagamento
     * @var int
     */
    public $id_formapagamento;

    /**
     * Contem o id da campanha comercial
     * @var int
     */
    public $id_campanhacomercial;
    /**
     * Contem o id da campanha comercial premio
     * @var int
     */
    public $id_campanhacomercialpremio;

    /**
     * Contem o id do tipo de campanha da venda
     * @var int
     */
    public $id_tipocampanha;

    /**
     * Contem o id da evolucao
     * @var int
     */
    public $id_evolucao;

    /**
     * Contem o id da situacao
     * @var int
     */
    public $id_situacao;

    /**
     * Contem o id do usuariocadastro
     * @var int
     */
    public $id_usuariocadastro;

    /**
     * Contem o id da entidade
     * @var int
     */
    public $id_entidade;

    /**
     * Contem a data de cadastro
     * @var zend_date
     */
    public $dt_cadastro;

    /**
     * Contem o numero das parcelas
     * @var int
     */
    public $nu_parcelas;

    /**
     * Contem o valor da porcentagem do desconto
     * @var int
     */
    public $nu_descontoporcentagem;

    /**
     * Contem o valor do desconto
     * @var int
     */
    public $nu_descontovalor;

    /**
     * Contem o valor dos juros
     * @var int
     */
    public $nu_juros;

    /**
     * Contem o valor liquido
     * @var int
     */
    public $nu_valorliquido;

    /**
     * Contem o valor brutp
     * @var int
     */
    public $nu_valorbruto;

    /**
     * diz se esta ativo
     * @var boolean
     */
    public $bl_ativo;

    /**
     * Id do usuário.
     * @var int
     */
    public $id_usuario;

    /**
     * Identificador se a venda é de contrato ou não
     * @var boolean
     */
    public $bl_contrato;

    /**
     * Contem o dia do vencimento
     * @var Number
     */
    public $nu_diamensalidade;

    /**
     * Contem o Id da prevenda
     * @var int
     */
    public $id_prevenda;

    public $id_protocolo;

    public $dt_agendamento;


    /**
     * Data da Confirmação da venda
     * @var datetime
     */
    public $dt_confirmacao;

    public $id_origemvenda;

    /**
     * @var number
     */
    public $id_contratoafiliado;

    /**
     * @var String
     */
    public $recorrente_orderid;

    /**
     * Observacao sobre a venda
     * @var String
     */
    public $st_observacao;

    public $nu_valoratualizado;


    public $id_cupom;

    public $id_atendente;

    public $id_operador;

    public $nu_valorcartacredito;

    public $id_campanhapontualidade;

    public $id_ocorrencia;

    public $dt_atualizado;

    public $id_uploadcontrato;

    public $nu_creditos;

    public $id_ocorrenciaaproveitamento;

    public $bl_renovacao;

    public $bl_transferidoentidade;

    public $bl_processamento;

    /**
     * @var int
     */
    public $id_afiliado;

    /**
     * @var int
     */
    public $id_nucleotelemarketing;

    /**
     * @var int
     */
    public $id_vendedor;

    /**
     * @var string
     */
    public $st_siteorigem;

    /**
     * @var boolean
     */
    public $bl_semsalarenovacao;

    /**
     * @var string
     */
    public $st_nomconvenio;

    /**
     * @var string
     */
    public $st_codpromocao;

    /**
     * @var string
     */
    public $utm_source;

    /**
     * @var string
     */
    public $utm_medium;

    /**
     * @var string
     */
    public $utm_campaign;

    /**
     * @var string
     */
    public $utm_term;

    /**
     * @var string
     */
    public $utm_content;

    /**
     * @var string
     */
    public $origem_organica;


    /**
     * @return integer
     */
    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @param integer $id_atendente
     * @return $this
     */
    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
        return $this;
    }


    /**
     * @return $id_cupom
     */
    public function getId_cupom()
    {
        return $this->id_cupom;
    }


    /**
     * @param $id_cupom
     * @return $this
     */
    public function setId_cupom($id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @return $id_enderecoentrega
     */
    public function getId_enderecoentrega()
    {
        return $this->id_enderecoentrega;
    }

    /**
     * @param int $id_enderecoentrega
     */
    public function setId_enderecoentrega($id_enderecoentrega)
    {
        $this->id_enderecoentrega = $id_enderecoentrega;
    }

    /**
     * @return int $id_contratoafiliado
     */
    public function getId_contratoafiliado()
    {
        return $this->id_contratoafiliado;
    }

    /**
     * @param int $id_contratoafiliado
     */
    public function setId_contratoafiliado($id_contratoafiliado)
    {
        $this->id_contratoafiliado = $id_contratoafiliado;
    }

    /**
     * @return $id_origemvenda
     */
    public function getId_origemvenda()
    {
        return $this->id_origemvenda;
    }

    /**
     * @param int $id_origemvenda
     */
    public function setId_origemvenda($id_origemvenda)
    {
        $this->id_origemvenda = $id_origemvenda;
    }

    /**
     * @return Zend_Date $dt_confirmacao
     */
    public function getDt_confirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @param datetime $dt_confirmacao
     */
    public function setDt_confirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
    }

    /**
     * @return int $id_protocolo
     */
    public function getId_protocolo()
    {
        return $this->id_protocolo;
    }

    /**
     * @param int $id_protocolo
     */
    public function setId_protocolo($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
    }


    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_operador()
    {
        return $this->id_operador;
    }

    /**
     * @param int $id_operador
     */
    public function setId_operador($id_operador)
    {
        $this->id_operador = $id_operador;
    }


    /**
     * @return int $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return int $id_formapagamento
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @return int $id_campanhacomercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return the $id_campanhacomercialpremio
     */
    public function getId_campanhacomercialpremio()
    {
        return $this->id_campanhacomercialpremio;
    }

    /**
     * @return int $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return int $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return int $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return Zend_Date $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return int $nu_descontoporcentagem
     */
    public function getNu_descontoporcentagem()
    {
        return $this->nu_descontoporcentagem;
    }

    /**
     * @return int $nu_descontovalor
     */
    public function getNu_descontovalor()
    {
        return $this->nu_descontovalor;
    }

    /**
     * @return int $nu_juros
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @return int $nu_valorliquido
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @return int $nu_valorbruto
     */
    public function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @return boolean $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @param int $id_formapagamento
     */
    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercialpremio
     */
    public function setId_campanhacomercialpremio($id_campanhacomercialpremio)
    {
        $this->id_campanhacomercialpremio = $id_campanhacomercialpremio;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param zend_date $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param int $nu_descontoporcentagem
     */
    public function setNu_descontoporcentagem($nu_descontoporcentagem)
    {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
    }

    /**
     * @param int $nu_descontovalor
     */
    public function setNu_descontovalor($nu_descontovalor)
    {
        $this->nu_descontovalor = $nu_descontovalor;
    }

    /**
     * @param int $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @param int $nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @param int $nu_valorbruto
     */
    public function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean $bl_contrato
     */
    public function getBl_contrato()
    {
        return $this->bl_contrato;
    }

    /**
     * @param $bl_contrato
     */
    public function setBl_contrato($bl_contrato)
    {
        $this->bl_contrato = $bl_contrato;
    }

    /**
     * @return int $id_tipocampanha
     */
    public function getId_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param int $id_tipocampanha
     */
    public function setId_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
    }

    /**
     * @return int $nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param int $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @return int $nu_diamensalidade
     */
    public function getNu_diamensalidade()
    {
        return $this->nu_diamensalidade;
    }

    /**
     * @param Number $nu_diamensalidade
     */
    public function setNu_diamensalidade($nu_diamensalidade)
    {
        $this->nu_diamensalidade = $nu_diamensalidade;
    }

    /**
     * @return int $id_prevenda
     */
    public function getId_prevenda()
    {
        return $this->id_prevenda;
    }

    /**
     * @param int $id_prevenda
     */
    public function setId_prevenda($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
    }

    /**
     * @return $dt_agendamento
     */
    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @param $dt_agendamento
     */
    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    /**
     * @return string $st_observacao
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param string $st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
    }


    public function getNu_valoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    public function setNu_valoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
    }

    /**
     * @param mixed $recorrente_orderid
     */
    public function setRecorrenteOrderid($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
    }

    /**
     * @return mixed
     */
    public function getRecorrenteOrderid()
    {
        return $this->recorrente_orderid;
    }

    public function getNu_valorcartacredito()
    {
        return $this->nu_valorcartacredito;
    }

    public function setNu_valorcartacredito($nu_valorcartacredito)
    {
        $this->nu_valorcartacredito = $nu_valorcartacredito;
    }

    public function getId_campanhapontualidade()
    {
        return $this->id_campanhapontualidade;
    }

    public function setId_campanhapontualidade($id_campanhapontualidade)
    {
        $this->id_campanhapontualidade = $id_campanhapontualidade;
    }

    /**
     * @return mixed
     */
    public function getid_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param mixed $id_ocorrencia
     */
    public function setid_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_atualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @param mixed $dt_atualizado
     */
    public function setdt_atualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_uploadcontrato()
    {
        return $this->id_uploadcontrato;
    }

    /**
     * @param mixed $id_uploadcontrato
     */
    public function setid_uploadcontrato($id_uploadcontrato)
    {
        $this->id_uploadcontrato = $id_uploadcontrato;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getNu_creditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @param mixed $nu_creditos
     */
    public function setNu_creditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
    }

    /**
     * @return mixed
     */
    public function getId_ocorrenciaaproveitamento()
    {
        return $this->id_ocorrenciaaproveitamento;
    }

    /**
     * @param mixed $id_ocorrenciaaproveitamento
     */
    public function setId_ocorrenciaaproveitamento($id_ocorrenciaaproveitamento)
    {
        $this->id_ocorrenciaaproveitamento = $id_ocorrenciaaproveitamento;
    }

    /**
     * @return mixed
     */
    public function getBl_renovacao()
    {
        return $this->bl_renovacao;
    }

    /**
     * @param mixed $bl_renovacao
     */
    public function setBl_renovacao($bl_renovacao)
    {
        $this->bl_renovacao = $bl_renovacao;
    }

    /**
     * @return mixed
     */
    public function getBl_transferidoentidade()
    {
        return $this->bl_transferidoentidade;
    }

    /**
     * @param mixed $bl_transferidoentidade
     */
    public function setBl_transferidoentidade($bl_transferidoentidade)
    {
        $this->bl_transferidoentidade = $bl_transferidoentidade;
    }

    /**
     * @return int
     */
    public function getId_afiliado()
    {
        return $this->id_afiliado;
    }

    /**
     * @param int $id_afiliado
     * @return $this
     */
    public function setId_afiliado($id_afiliado)
    {
        $this->id_afiliado = $id_afiliado;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_nucleotelemarketing()
    {
        return $this->id_nucleotelemarketing;
    }

    /**
     * @param int $id_nucleotelemarketing
     * @return $this
     */
    public function setId_nucleotelemarketing($id_nucleotelemarketing)
    {
        $this->id_nucleotelemarketing = $id_nucleotelemarketing;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_vendedor()
    {
        return $this->id_vendedor;
    }

    /**
     * @param int $id_vendedor
     * @return $this
     */
    public function setId_vendedor($id_vendedor)
    {
        $this->id_vendedor = $id_vendedor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_siteorigem()
    {
        return $this->st_siteorigem;
    }

    /**
     * @param string $st_siteorigem
     * @return $this
     */
    public function setSt_siteorigem($st_siteorigem)
    {
        $this->st_siteorigem = $st_siteorigem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBl_processamento()
    {
        return $this->bl_processamento;
    }

    /**
     * @param mixed $bl_processamento
     */
    public function setBl_processamento($bl_processamento)
    {
        $this->bl_processamento = $bl_processamento;
    }

    /**
     * @return bool
     */
    public function getBl_semsalarenovacao()
    {
        return $this->bl_semsalarenovacao;
    }

    /**
     * @param bool $bl_semsalarenovacao
     */
    public function setBl_semsalarenovacao($bl_semsalarenovacao)
    {
        $this->bl_semsalarenovacao = $bl_semsalarenovacao;
    }

    /**
     * @return string
     */
    public function getSt_nomconvenio()
    {
        return $this->st_nomconvenio;
    }

    /**
     * @param string $st_nomconvenio
     */
    public function setSt_nomconvenio($st_nomconvenio)
    {
        $this->st_nomconvenio = $st_nomconvenio;
    }

    /**
     * @return string
     */
    public function getSt_codpromocao()
    {
        return $this->st_codpromocao;
    }

    /**
     * @param string $st_codpromocao
     */
    public function setSt_codpromocao($st_codpromocao)
    {
        $this->st_codpromocao = $st_codpromocao;
    }

    /**
     * @return string
     */
    public function getUtm_source()
    {
        return $this->utm_source;
    }

    /**
     * @param string $utm_source
     */
    public function setUtm_source($utm_source)
    {
        $this->utm_source = $utm_source;
    }

    /**
     * @return string
     */
    public function getUtm_medium()
    {
        return $this->utm_medium;
    }

    /**
     * @param string $utm_medium
     */
    public function setUtm_medium($utm_medium)
    {
        $this->utm_medium = $utm_medium;
    }

    /**
     * @return string
     */
    public function getUtm_campaign()
    {
        return $this->utm_campaign;
    }

    /**
     * @param string $utm_campaign
     */
    public function setUtm_campaign($utm_campaign)
    {
        $this->utm_campaign = $utm_campaign;
    }

    /**
     * @return string
     */
    public function getUtm_term()
    {
        return $this->utm_term;
    }

    /**
     * @param string $utm_term
     */
    public function setUtm_term($utm_term)
    {
        $this->utm_term = $utm_term;
    }

    /**
     * @return string
     */
    public function getUtm_content()
    {
        return $this->utm_content;
    }

    /**
     * @param string $utm_content
     */
    public function setUtm_content($utm_content)
    {
        $this->utm_content = $utm_content;
    }

    /**
     * @return mixed
     */
    public function getOrigem_organica()
    {
        return $this->origem_organica;
    }

    /**
     * @param mixed $origem_organica
     */
    public function setOrigem_organica($origem_organica)
    {
        $this->origem_organica = $origem_organica;
    }


}

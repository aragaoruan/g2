<?php
/**
 * Classe para encapsular os dados da view de horario aula com o dia da semana
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwHorarioAulaDiaSemanaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Id do horário de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Id do dia da semana.
	 * @var int
	 */
	public $id_diasemana;
		
	/**
	 * Dia da semana.
	 * @var string
	 */
	public $st_diasemana;
	
	/**
	 * Abreviatura do dia da semana.
	 * @var string
	 */
	public $st_abreviatura;
	
	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $id_diasemana
	 */
	public function getId_diasemana() {
		return $this->id_diasemana;
	}

	/**
	 * @return the $st_diasemana
	 */
	public function getSt_diasemana() {
		return $this->st_diasemana;
	}

	/**
	 * @return the $st_abreviatura
	 */
	public function getSt_abreviatura() {
		return $this->st_abreviatura;
	}

	/**
	 * @param $id_horarioaula the $id_horarioaula to set
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param $id_diasemana the $id_diasemana to set
	 */
	public function setId_diasemana($id_diasemana) {
		$this->id_diasemana = $id_diasemana;
	}

	/**
	 * @param $st_diasemana the $st_diasemana to set
	 */
	public function setSt_diasemana($st_diasemana) {
		$this->st_diasemana = $st_diasemana;
	}

	/**
	 * @param $st_abreviatura the $st_abreviatura to set
	 */
	public function setSt_abreviatura($st_abreviatura) {
		$this->st_abreviatura = $st_abreviatura;
	}

	
}
<?php
/**
 * Classe para encapsular os dados de Tramite com entrega de material.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TramiteEntregaMaterialTO extends Ead1_TO_Dinamico {

	public $id_tramiteentregamaterial;
	public $id_tramite;
	public $id_entregamaterial;
	
	/**
	 * @return unknown
	 */
	public function getId_tramiteentregamaterial() {
		return $this->id_tramiteentregamaterial;
	}
	
	/**
	 * @param unknown_type $id_tramiteentregamaterial
	 */
	public function setId_tramiteentregamaterial($id_tramiteentregamaterial) {
		$this->id_tramiteentregamaterial = $id_tramiteentregamaterial;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tramite() {
		return $this->id_tramite;
	}
	
	/**
	 * @param unknown_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}
	
	/**
	 * @param unknown_type $id_tramite
	 */
	public function setId_tramite($id_tramite) {
		$this->id_tramite = $id_tramite;
	}

}

?>
<?php
/**
 * Classe para encapsular da tabela  de modelo de grade de notas
 * @package models
 * @subpackage to
 */
 
class ModeloGradeNotasTO extends Ead1_TO_Dinamico{
	
	const NORMAL = 1;
	const POR_TIPO = 2;

	public $id_modelogradenotas;
	public $st_modelogradenotas;

	/**
	 * @return the $id_modelogradenotas
	 */
	public function getId_modelogradenotas(){ 
		return $this->id_modelogradenotas;
	}

	/**
	 * @param field_type $id_modelogradenotas
	 */
	public function setId_modelogradenotas($id_modelogradenotas){ 
		$this->id_modelogradenotas = $id_modelogradenotas;
	}

	/**
	 * @return the $st_modelogradenotas
	 */
	public function getSt_modelogradenotas(){ 
		return $this->st_modelogradenotas;
	}

	/**
	 * @param field_type $st_modelogradenotas
	 */
	public function setSt_modelogradenotas($st_modelogradenotas){ 
		$this->st_modelogradenotas = $st_modelogradenotas;
	}


}
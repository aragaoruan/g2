<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 *
 */
class ArquivoRetornoTO extends Ead1_TO_Dinamico {

	public $id_arquivoretorno;
	public $st_arquivoretorno;
	public $id_usuariocadastro;
	public $id_entidade;
	public $dt_cadastro;
	public $bl_ativo = TRUE;
	public $st_banco;
	
	/**
	 * @return the $id_arquivoretorno
	 */
	public function getId_arquivoretorno() {
		return $this->id_arquivoretorno;
	}

	/**
	 * @return the $st_arquivoretorno
	 */
	public function getSt_arquivoretorno() {
		return $this->st_arquivoretorno;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @param field_type $id_arquivoretorno
	 */
	public function setId_arquivoretorno($id_arquivoretorno) {
		$this->id_arquivoretorno = $id_arquivoretorno;
	}

	/**
	 * @param field_type $st_arquivoretorno
	 */
	public function setSt_arquivoretorno($st_arquivoretorno) {
		$this->st_arquivoretorno = $st_arquivoretorno;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	
	
}

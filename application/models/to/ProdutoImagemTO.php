<?php
/**
 * Classe para encapsular dados da tb_produtoimagem
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class ProdutoImagemTO extends Ead1_TO_Dinamico {

	public $id_produtoimagem;
	public $id_produto;
	public $id_upload;
	public $bl_ativo;
	
	
	/**
	 * @return the $id_produtoimagem
	 */
	public function getId_produtoimagem() {
		return $this->id_produtoimagem;
	}

	/**
	 * @param field_type $id_produtoimagem
	 */
	public function setId_produtoimagem($id_produtoimagem) {
		$this->id_produtoimagem = $id_produtoimagem;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $id_upload
	 */
	public function getId_upload() {
		return $this->id_upload;
	}

	/**
	 * @param field_type $id_upload
	 */
	public function setId_upload($id_upload) {
		$this->id_upload = $id_upload;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
	
}

?>
<?php
/**
 * Classe que encapsula os dados da vw_vendalancamentoatrasado
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class VwVendaLancamentoAtrasadoTO extends Ead1_TO_Dinamico {

	const COM_OCORRENCIA = 115;
	const SEM_OCORRENCIA = 116; 
	
	public $dt_vencimento;
	public $id_entidadelancamento;
	public $id_evolucao;
	public $id_usuario;
	public $id_usuariolancamento;
	public $id_venda;
	public $nu_parcelas;
	public $nu_quitado;
	public $nu_valor;
	public $st_entidaderesponsavel;
	public $st_evolucao;
	public $st_nomecompleto;
	public $st_nomeresponsavel;
	public $id_entidade;
	public $nu_diasatraso;
	public $st_cpf;
	public $st_email;
	public $id_lancamento;
	public $id_produto;
	public $st_produto;
	public $id_situacaolancamento;
	public $id_situacaomatricula;
	public $st_situacaolancamento;
	public $st_situacaomatricula;
	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $id_entidadelancamento
	 */
	public function getId_entidadelancamento() {
		return $this->id_entidadelancamento;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_usuariolancamento
	 */
	public function getId_usuariolancamento() {
		return $this->id_usuariolancamento;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @return the $nu_quitado
	 */
	public function getNu_quitado() {
		return $this->nu_quitado;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $st_entidaderesponsavel
	 */
	public function getSt_entidaderesponsavel() {
		return $this->st_entidaderesponsavel;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_nomeresponsavel
	 */
	public function getSt_nomeresponsavel() {
		return $this->st_nomeresponsavel;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_diasatraso
	 */
	public function getNu_diasatraso() {
		return $this->nu_diasatraso;
	}

	/**
	 * @return the $st_cpf
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email() {
		return $this->st_email;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_situacaolancamento
	 */
	public function getId_situacaolancamento() {
		return $this->id_situacaolancamento;
	}

	/**
	 * @return the $id_situacaomatricula
	 */
	public function getId_situacaomatricula() {
		return $this->id_situacaomatricula;
	}

	/**
	 * @return the $st_situacaolancamento
	 */
	public function getSt_situacaolancamento() {
		return $this->st_situacaolancamento;
	}

	/**
	 * @return the $st_situacaomatricula
	 */
	public function getSt_situacaomatricula() {
		return $this->st_situacaomatricula;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $id_entidadelancamento
	 */
	public function setId_entidadelancamento($id_entidadelancamento) {
		$this->id_entidadelancamento = $id_entidadelancamento;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_usuariolancamento
	 */
	public function setId_usuariolancamento($id_usuariolancamento) {
		$this->id_usuariolancamento = $id_usuariolancamento;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $nu_parcelas
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @param field_type $nu_quitado
	 */
	public function setNu_quitado($nu_quitado) {
		$this->nu_quitado = $nu_quitado;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $st_entidaderesponsavel
	 */
	public function setSt_entidaderesponsavel($st_entidaderesponsavel) {
		$this->st_entidaderesponsavel = $st_entidaderesponsavel;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomeresponsavel
	 */
	public function setSt_nomeresponsavel($st_nomeresponsavel) {
		$this->st_nomeresponsavel = $st_nomeresponsavel;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $nu_diasatraso
	 */
	public function setNu_diasatraso($nu_diasatraso) {
		$this->nu_diasatraso = $nu_diasatraso;
	}

	/**
	 * @param field_type $st_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param field_type $id_situacaolancamento
	 */
	public function setId_situacaolancamento($id_situacaolancamento) {
		$this->id_situacaolancamento = $id_situacaolancamento;
	}

	/**
	 * @param field_type $id_situacaomatricula
	 */
	public function setId_situacaomatricula($id_situacaomatricula) {
		$this->id_situacaomatricula = $id_situacaomatricula;
	}

	/**
	 * @param field_type $st_situacaolancamento
	 */
	public function setSt_situacaolancamento($st_situacaolancamento) {
		$this->st_situacaolancamento = $st_situacaolancamento;
	}

	/**
	 * @param field_type $st_situacaomatricula
	 */
	public function setSt_situacaomatricula($st_situacaomatricula) {
		$this->st_situacaomatricula = $st_situacaomatricula;
	}

	
}

?>
<?php
/**
 * TO da tabela TipoFuncionalidade
 * @author edermariano
 * @package models
 * @subpackage to
 */
class TipoFuncionalidadeTO extends Ead1_TO_Dinamico {
	
	const TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU	= 6; 
	
	const MENU_PRINCIPAL 	= 1;
	const MENU_LATERAL 		= 2;
	const MENU_SUB_LATERAL 	= 3;
	const ABAS 				= 4;
	const GRUPOS 			= 5;
	const INTERNA 			= 7;
	const BOTAO 			= 8;
	
	
	public $id_tipofuncionalidade;
	public $st_tipofuncionalidade;
	
	/**
	 * @return the $id_tipofuncionalidade
	 */
	public function getId_tipofuncionalidade() {
		return $this->id_tipofuncionalidade;
	}

	/**
	 * @return the $st_tipofuncionalidade
	 */
	public function getSt_tipofuncionalidade() {
		return $this->st_tipofuncionalidade;
	}

	/**
	 * @param $id_tipofuncionalidade the $id_tipofuncionalidade to set
	 */
	public function setId_tipofuncionalidade($id_tipofuncionalidade) {
		$this->id_tipofuncionalidade = $id_tipofuncionalidade;
	}

	/**
	 * @param $st_tipofuncionalidade the $st_tipofuncionalidade to set
	 */
	public function setSt_tipofuncionalidade($st_tipofuncionalidade) {
		$this->st_tipofuncionalidade = $st_tipofuncionalidade;
	}

	
	
	
}
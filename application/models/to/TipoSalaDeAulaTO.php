<?php
/**
 * Classe para encapsular os dados de tipo de sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoSalaDeAulaTO extends Ead1_TO_Dinamico {

	const SALA_PERMANENTE 	= 1;
	const SALA_PERIODICA  	= 2;
	
	/**
	 * Id do tipo de sala de aula.
	 * @var int
	 */
	public $id_tiposaladeaula;
	
	/**
	 * Tipo de sala de aula.
	 * @var string
	 */
	public $st_tiposaladeaula;
	
	/**
	 * Descrição do tipo de sala de aula.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tiposaladeaula() {
		return $this->id_tiposaladeaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tiposaladeaula() {
		return $this->st_tiposaladeaula;
	}
	
	/**
	 * @param int $id_tiposaladeaula
	 */
	public function setId_tiposaladeaula($id_tiposaladeaula) {
		$this->id_tiposaladeaula = $id_tiposaladeaula;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tiposaladeaula
	 */
	public function setSt_tiposaladeaula($st_tiposaladeaula) {
		$this->st_tiposaladeaula = $st_tiposaladeaula;
	}
	

}

?>

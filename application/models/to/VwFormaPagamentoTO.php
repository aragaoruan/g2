<?php
class VwFormaPagamentoTO extends Ead1_TO_Dinamico{
	
	public $id_formapagamento;
	public $st_formapagamento;
	public $id_situacao;
	public $st_descricao;
	public $nu_entradavalormin;
	public $nu_entradavalormax;
	public $nu_valormin;
	public $nu_valormax;
	public $nu_valorminparcela;
	public $dt_cadastro;
	public $id_usuariocadastro;
	public $id_tipoformapagamentoparcela;
	public $bl_todosprodutos;
	public $st_situacao;
	public $id_entidade;
	public $nu_jurosmin;
	public $nu_jurosmax;
	public $nu_maxparcelas;
	public $nu_juros;
	public $nu_multa;
	
	/**
	 * @return unknown
	 */
	public function getNu_juros() {
		return $this->nu_juros;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_jurosmax() {
		return $this->nu_jurosmax;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_jurosmin() {
		return $this->nu_jurosmin;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_maxparcelas() {
		return $this->nu_maxparcelas;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_multa() {
		return $this->nu_multa;
	}
	
	/**
	 * @param unknown_type $nu_juros
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}
	
	/**
	 * @param unknown_type $nu_jurosmax
	 */
	public function setNu_jurosmax($nu_jurosmax) {
		$this->nu_jurosmax = $nu_jurosmax;
	}
	
	/**
	 * @param unknown_type $nu_jurosmin
	 */
	public function setNu_jurosmin($nu_jurosmin) {
		$this->nu_jurosmin = $nu_jurosmin;
	}
	
	/**
	 * @param unknown_type $nu_maxparcelas
	 */
	public function setNu_maxparcelas($nu_maxparcelas) {
		$this->nu_maxparcelas = $nu_maxparcelas;
	}
	
	/**
	 * @param unknown_type $nu_multa
	 */
	public function setNu_multa($nu_multa) {
		$this->nu_multa = $nu_multa;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $nu_entradavalormin
	 */
	public function getNu_entradavalormin() {
		return $this->nu_entradavalormin;
	}

	/**
	 * @return the $nu_entradavalormax
	 */
	public function getNu_entradavalormax() {
		return $this->nu_entradavalormax;
	}

	/**
	 * @return the $nu_valormin
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}

	/**
	 * @return the $nu_valormax
	 */
	public function getNu_valormax() {
		return $this->nu_valormax;
	}

	/**
	 * @return the $nu_valorminparcela
	 */
	public function getNu_valorminparcela() {
		return $this->nu_valorminparcela;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_tipoformapagamentoparcela
	 */
	public function getId_tipoformapagamentoparcela() {
		return $this->id_tipoformapagamentoparcela;
	}

	/**
	 * @return the $bl_todosprodutos
	 */
	public function getBl_todosprodutos() {
		return $this->bl_todosprodutos;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $nu_entradavalormin
	 */
	public function setNu_entradavalormin($nu_entradavalormin) {
		$this->nu_entradavalormin = $nu_entradavalormin;
	}

	/**
	 * @param field_type $nu_entradavalormax
	 */
	public function setNu_entradavalormax($nu_entradavalormax) {
		$this->nu_entradavalormax = $nu_entradavalormax;
	}

	/**
	 * @param field_type $nu_valormin
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}

	/**
	 * @param field_type $nu_valormax
	 */
	public function setNu_valormax($nu_valormax) {
		$this->nu_valormax = $nu_valormax;
	}

	/**
	 * @param field_type $nu_valorminparcela
	 */
	public function setNu_valorminparcela($nu_valorminparcela) {
		$this->nu_valorminparcela = $nu_valorminparcela;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_tipoformapagamentoparcela
	 */
	public function setId_tipoformapagamentoparcela($id_tipoformapagamentoparcela) {
		$this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
	}

	/**
	 * @param field_type $bl_todosprodutos
	 */
	public function setBl_todosprodutos($bl_todosprodutos) {
		$this->bl_todosprodutos = $bl_todosprodutos;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
}
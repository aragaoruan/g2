<?php
/**
 * Classe para encapsular os dados de tipo de produto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class TipoProdutoTO extends Ead1_TO_Dinamico {

	/**
	 * Constante para o tipo de produto do Projeto Pedagógico
	 * @var int
	 */
	const PROJETO_PEDAGOGICO = 1;
	
	/**
	 * Tipos de Produto
	 * Taxa
	 * @var int
	 */
	const TAXA = 2;
	
	/**
	 * Tipos de Produto
	 * Declarações
	 * @var int
	 */
	const DECLARACOES = 3;
	
	/**
	 * Tipos de Produto
	 * Material
	 * @var int
	 */
	const MATERIAL = 4;
	
	/**
	 * Tipos de Produto
	 * Avaliação
	 * @var int
	 */
	const AVALIACAO = 5;
	
	/**
	 * Tipos de Produto
	 * Livro
	 * @var int
	 */
	const LIVRO = 6;
	
	
	/**
	 * Tipo de Produto
	 * Combo
	 * @var int	 
	 */
	const COMBO = 7;
	
	/**
	 * Tipo de Produto
	 * Combo
	 * @var int
	 */
	const PRR = 8;

	/**
	 * Tipo de Simulado
	 * Combo
	 * @var int
	 */
	const SIMULADO = 9;
	
	/**
	 * Contem o id do tipo do produto
	 * @var int
	 */
	public $id_tipoproduto;
	
	/**
	 * Contem o nome da relacao da tabela
	 * @var string
	 */
	public $st_tabelarelacao;
	
	/**
	 * Contem o nome do tipo do produto
	 * @var string
	 */
	public $st_tipoproduto;
	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @param int $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @return the $st_tabelarelacao
	 */
	public function getSt_tabelarelacao() {
		return $this->st_tabelarelacao;
	}

	/**
	 * @param string $st_tabelarelacao
	 */
	public function setSt_tabelarelacao($st_tabelarelacao) {
		$this->st_tabelarelacao = $st_tabelarelacao;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @param string $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

	
}
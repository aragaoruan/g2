<?php
/**
 * Classe para encapsular os dados da tb_classeafiliado.
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class ContratoAfiliadoTO extends Ead1_TO_Dinamico {
		
	public $id_classeafiliado;
	public $id_contratoafiliado;
	public $st_contratoafiliado;
	public $id_usuariocadastro;
	public $id_usuarioresponsavel;
	public $id_entidadeafiliada;
	public $id_entidadecadastro;
	public $st_descricao;
	public $dt_cadastro;
	public $st_url;
	public $id_situacao;
	public $bl_ativo;
	
	/**
	 * @return the $id_classeafiliado
	 */
	public function getId_classeafiliado() {
		return $this->id_classeafiliado;
	}

	/**
	 * @param field_type $id_classeafiliado
	 */
	public function setId_classeafiliado($id_classeafiliado) {
		$this->id_classeafiliado = $id_classeafiliado;
	}

	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @return the $st_contratoafiliado
	 */
	public function getSt_contratoafiliado() {
		return $this->st_contratoafiliado;
	}

	/**
	 * @param field_type $st_contratoafiliado
	 */
	public function setSt_contratoafiliado($st_contratoafiliado) {
		$this->st_contratoafiliado = $st_contratoafiliado;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_usuarioresponsavel
	 */
	public function getId_usuarioresponsavel() {
		return $this->id_usuarioresponsavel;
	}

	/**
	 * @param field_type $id_usuarioresponsavel
	 */
	public function setId_usuarioresponsavel($id_usuarioresponsavel) {
		$this->id_usuarioresponsavel = $id_usuarioresponsavel;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @return the $id_entidadeafiliada
	 */
	public function getId_entidadeafiliada() {
		return $this->id_entidadeafiliada;
	}

	/**
	 * @param field_type $id_entidadeafiliada
	 */
	public function setId_entidadeafiliada($id_entidadeafiliada) {
		$this->id_entidadeafiliada = $id_entidadeafiliada;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @return the $st_url
	 */
	public function getSt_url() {
		return $this->st_url;
	}

	/**
	 * @param field_type $st_url
	 */
	public function setSt_url($st_url) {
		$this->st_url = $st_url;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
	
}

?>
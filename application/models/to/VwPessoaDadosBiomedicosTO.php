<?php
/**
 * Classe para encapsular os dados da view de dados biomédicos da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaDadosBiomedicosTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id da etnia.
	 * @var int
	 */
	public $id_etnia;
	
	/**
	 * Glicemia da pessoa.
	 * @var numeric
	 */
	public $nu_glicemia;
	
	/**
	 * Necessidades Especiais da pessoa.
	 * @var string
	 */
	public $st_necessidadesespeciais;
	
	/**
	 * Alergias da pessoa.
	 * @var string
	 */
	public $st_alergias;
	
	/**
	 * Deficiências da pessoa.
	 * @var string
	 */
	public $st_deficiencias;
	
	/**
	 * Tipo sanguíneo da pessoa.
	 * @var string
	 */
	public $sg_tiposanguineo;
	
	/**
	 * Id do tipo sanguíneo.
	 * @var int
	 */
	public $id_tiposanguineo;
	
	/**
	 * Etnia da pessoa.
	 * @var string
	 */
	public $st_etnia;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_etnia
	 */
	public function getId_etnia() {
		return $this->id_etnia;
	}

	/**
	 * @return the $nu_glicemia
	 */
	public function getNu_glicemia() {
		return $this->nu_glicemia;
	}

	/**
	 * @return the $st_necessidadesespeciais
	 */
	public function getSt_necessidadesespeciais() {
		return $this->st_necessidadesespeciais;
	}

	/**
	 * @return the $st_alergias
	 */
	public function getSt_alergias() {
		return $this->st_alergias;
	}

	/**
	 * @return the $st_deficiencias
	 */
	public function getSt_deficiencias() {
		return $this->st_deficiencias;
	}

	/**
	 * @return the $sg_tiposanguineo
	 */
	public function getSg_tiposanguineo() {
		return $this->sg_tiposanguineo;
	}

	/**
	 * @return the $id_tiposanguineo
	 */
	public function getId_tiposanguineo() {
		return $this->id_tiposanguineo;
	}

	/**
	 * @return the $st_etnia
	 */
	public function getSt_etnia() {
		return $this->st_etnia;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_etnia
	 */
	public function setId_etnia($id_etnia) {
		$this->id_etnia = $id_etnia;
	}

	/**
	 * @param numeric $nu_glicemia
	 */
	public function setNu_glicemia($nu_glicemia) {
		$this->nu_glicemia = $nu_glicemia;
	}

	/**
	 * @param string $st_necessidadesespeciais
	 */
	public function setSt_necessidadesespeciais($st_necessidadesespeciais) {
		$this->st_necessidadesespeciais = $st_necessidadesespeciais;
	}

	/**
	 * @param string $st_alergias
	 */
	public function setSt_alergias($st_alergias) {
		$this->st_alergias = $st_alergias;
	}

	/**
	 * @param string $st_deficiencias
	 */
	public function setSt_deficiencias($st_deficiencias) {
		$this->st_deficiencias = $st_deficiencias;
	}

	/**
	 * @param string $sg_tiposanguineo
	 */
	public function setSg_tiposanguineo($sg_tiposanguineo) {
		$this->sg_tiposanguineo = $sg_tiposanguineo;
	}

	/**
	 * @param int $id_tiposanguineo
	 */
	public function setId_tiposanguineo($id_tiposanguineo) {
		$this->id_tiposanguineo = $id_tiposanguineo;
	}

	/**
	 * @param string $st_etnia
	 */
	public function setSt_etnia($st_etnia) {
		$this->st_etnia = $st_etnia;
	}

	
}
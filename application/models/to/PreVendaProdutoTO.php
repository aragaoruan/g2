<?php
/**
 * Classe para encapsular os dados de pre venda com produto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class PreVendaProdutoTO extends Ead1_TO_Dinamico {

	public $id_prevenda;
	public $id_prevendaproduto;
	public $id_produto;
	
	/**
	 * @return unknown
	 */
	public function getId_prevenda() {
		return $this->id_prevenda;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_prevendaproduto() {
		return $this->id_prevendaproduto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @param unknown_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda) {
		$this->id_prevenda = $id_prevenda;
	}
	
	/**
	 * @param unknown_type $id_prevendaproduto
	 */
	public function setId_prevendaproduto($id_prevendaproduto) {
		$this->id_prevendaproduto = $id_prevendaproduto;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

}

?>
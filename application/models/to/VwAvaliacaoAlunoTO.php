<?php
/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 *
 * Obs.: O campo id_avaliacaoconjuntoreferencia é muito extenso e pode gerar erro
 * ao fazer uma consulta (String data, right truncation) por "estourar" o limite de
 * caracteres, porém só acontece no windows.
 */
class VwAvaliacaoAlunoTO extends Ead1_TO_Dinamico{

	public $id_avaliacaoaluno;
	public $id_matricula;
        public $st_nomecompleto;
	public $id_modulo;
	public $st_tituloexibicaomodulo;
	public $id_disciplina;
	public $st_tituloexibicaodisciplina;
	public $st_nota;
	public $id_avaliacaoagendamento;
	public $id_avaliacao;
	public $st_avaliacao;
	public $id_situacao;
	public $st_situacao;
	public $id_tipoprova;
    public $id_avaliacaoconjuntoreferencia;
	public $id_evolucao;
	public $st_evolucao;
	public $nu_notamax;
	public $id_tipocalculoavaliacao;
	public $id_tipoavaliacao;
	public $st_tipoavaliacao;
	public $id_avaliacaorecupera;
	public $nu_notamaxima;
	public $nu_percentualaprovacao;
	public $dt_agendamento;
	public $bl_ativo;
	public $dt_avaliacao;
	public $id_saladeaula;
	public $st_codusuario;
	public $id_avaliacaoconjunto; //erro
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $dt_defesa;
	public $id_tipodisciplina;
        public $id_upload;
        public $st_justificativa;
        public $id_matriculadisciplina;
        public $st_tituloavaliacao;
        public $id_entidadeatendimento;
        public $id_tiponota;
    public $st_notaava;
    public $id_categoriasala;
    public $st_notaconceitual;
    public $id_notaconceitual;

    /**
     * @param mixed $id_avaliacaoconjunto
     */
    public function setId_avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
    }

    /**
     * @return mixed
     */
    public function getId_avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    /**
     * @param mixed $id_tiponota
     */
    public function setId_tiponota($id_tiponota)
    {
        $this->id_tiponota = $id_tiponota;
    }

    /**
     * @return mixed
     */
    public function getId_tiponota()
    {
        return $this->id_tiponota;
    }

    /**
     * @param mixed $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @return mixed
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param mixed $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return mixed
     */
    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }




	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_avaliacaoaluno
	 */
	public function getId_avaliacaoaluno() {
		return $this->id_avaliacaoaluno;
	}

	/**
	 * @param field_type $id_avaliacaoaluno
	 */
	public function setId_avaliacaoaluno($id_avaliacaoaluno) {
		$this->id_avaliacaoaluno = $id_avaliacaoaluno;
	}

	/**
	 * @return the $id_conjuntoavaliacao
	 */
	public function getId_conjuntoavaliacao() {
		return $this->id_conjuntoavaliacao;
	}

	/**
	 * @param field_type $id_conjuntoavaliacao
	 */
	public function setId_conjuntoavaliacao($id_conjuntoavaliacao) {
		$this->id_conjuntoavaliacao = $id_conjuntoavaliacao;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @return the $st_codusuario
	 */
	public function getSt_codusuario() {
		return $this->st_codusuario;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @param field_type $st_codusuario
	 */
	public function setSt_codusuario($st_codusuario) {
		$this->st_codusuario = $st_codusuario;
	}

	/**
	 * @return unknown
	 */
	public function getDt_avaliacao() {
		return $this->dt_avaliacao;
	}

	/**
	 * @param unknown_type $dt_avaliacao
	 */
	public function setDt_avaliacao($dt_avaliacao) {
		$this->dt_avaliacao = $dt_avaliacao;
	}

	/**
	 * @return the $id_matricula
	 */
    public function getId_matricula()
    {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_modulo
	 */
    public function getId_modulo()
    {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_tituloexibicaomodulo
	 */
    public function getSt_tituloexibicaomodulo()
    {
		return $this->st_tituloexibicaomodulo;
	}

	/**
	 * @return the $id_disciplina
	 */
    public function getId_disciplina()
    {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_tituloexibicaodisciplina
	 */
    public function getSt_tituloexibicaodisciplina()
    {
		return $this->st_tituloexibicaodisciplina;
	}

	/**
	 * @return the $st_nota
	 */
    public function getSt_nota()
    {
		return $this->st_nota;
	}

	/**
	 * @return the $id_avaliacaoagendamento
	 */
    public function getId_avaliacaoagendamento()
    {
		return $this->id_avaliacaoagendamento;
	}

	/**
	 * @return the $id_avaliacao
	 */
    public function getId_avaliacao()
    {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
    public function getSt_avaliacao()
    {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $st_nomecompleto
	 */
    public function getSt_nomecompleto()
    {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_situacao
	 */
    public function getId_situacao()
    {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
    public function getSt_situacao()
    {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_tipoprova
	 */
    public function getId_tipoprova()
    {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
    public function getId_avalconjreferencia()
    {
		return $this->id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @return the $id_evolucao
	 */
    public function getId_evolucao()
    {
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
    public function getSt_evolucao()
    {
		return $this->st_evolucao;
	}

	/**
	 * @return the $nu_notamax
	 */
    public function getNu_notamax()
    {
		return $this->nu_notamax;
	}

	/**
	 * @return the $id_tipocalculoavaliacao
	 */
    public function getId_tipocalculoavaliacao()
    {
		return $this->id_tipocalculoavaliacao;
	}

	/**
	 * @return the $id_tipoavaliacao
	 */
    public function getId_tipoavaliacao()
    {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @return the $st_tipoavaliacao
	 */
    public function getSt_tipoavaliacao()
    {
		return $this->st_tipoavaliacao;
	}

	/**
	 * @return the $id_avaliacaorecupera
	 */
    public function getId_avaliacaorecupera()
    {
		return $this->id_avaliacaorecupera;
	}

	/**
	 * @return the $nu_notamaxima
	 */
    public function getNu_notamaxima()
    {
		return $this->nu_notamaxima;
	}

	/**
	 * @return the $nu_percentualaprovacao
	 */
    public function getNu_percentualaprovacao()
    {
		return $this->nu_percentualaprovacao;
	}

	/**
	 * @return the $dt_agendamento
	 */
    public function getDt_agendamento()
    {
		return $this->dt_agendamento;
	}

	/**
	 * @return the $dt_defesa
	 */
    public function getDt_defesa()
    {
		return $this->dt_defesa;
	}

	/**
	 * @return the $bl_ativo
	 */
    public function getBl_ativo()
    {
		return $this->bl_ativo;
	}


	/**
	 * @param field_type $id_matricula
	 */
    public function setId_matricula($id_matricula)
    {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_modulo
	 */
    public function setId_modulo($id_modulo)
    {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param field_type $st_tituloexibicaomodulo
	 */
    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
		$this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
	}

	/**
	 * @param field_type $id_disciplina
	 */
    public function setId_disciplina($id_disciplina)
    {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_tituloexibicaodisciplina
	 */
    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina)
    {
		$this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
	}

	/**
	 * @param field_type $st_nota
	 */
    public function setSt_nota($st_nota)
    {
		$this->st_nota = $st_nota;
	}

	/**
	 * @param field_type $id_avaliacaoagendamento
	 */
    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
    public function setId_avaliacao($id_avaliacao)
    {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
    public function setSt_avaliacao($st_avaliacao)
    {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
    public function setSt_nomecompleto($st_nomecompleto)
    {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $id_situacao
	 */
    public function setId_situacao($id_situacao)
    {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
    public function setSt_situacao($st_situacao)
    {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
    public function setId_tipoprova($id_tipoprova)
    {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoreferencia
	 */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param field_type $id_evolucao
	 */
    public function setId_evolucao($id_evolucao)
    {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $st_evolucao
	 */
    public function setSt_evolucao($st_evolucao)
    {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param field_type $nu_notamax
	 */
    public function setNu_notamax($nu_notamax)
    {
		$this->nu_notamax = $nu_notamax;
	}

	/**
	 * @param field_type $id_tipocalculoavaliacao
	 */
    public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao)
    {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $id_tipoavaliacao
	 */
    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param field_type $st_tipoavaliacao
	 */
    public function setSt_tipoavaliacao($st_tipoavaliacao)
    {
		$this->st_tipoavaliacao = $st_tipoavaliacao;
	}

	/**
	 * @param field_type $id_avaliacaorecupera
	 */
    public function setId_avaliacaorecupera($id_avaliacaorecupera)
    {
		$this->id_avaliacaorecupera = $id_avaliacaorecupera;
	}

	/**
	 * @param field_type $nu_notamaxima
	 */
    public function setNu_notamaxima($nu_notamaxima)
    {
		$this->nu_notamaxima = $nu_notamaxima;
	}

	/**
	 * @param field_type $nu_percentualaprovacao
	 */
    public function setNu_percentualaprovacao($nu_percentualaprovacao)
    {
		$this->nu_percentualaprovacao = $nu_percentualaprovacao;
	}

	/**
	 * @param field_type $dt_agendamento
	 */
    public function setDt_agendamento($dt_agendamento)
    {
		$this->dt_agendamento = $dt_agendamento;
	}

	/**
	 * @param field_type $dt_defesa
	 */
    public function setDt_defesa($dt_defesa)
    {
		$this->dt_defesa = $dt_defesa;
	}

	/**
	 * @param field_type $bl_ativo
	 */
    public function setBl_ativo($bl_ativo)
    {
		$this->bl_ativo = $bl_ativo;
	}


    /**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

        /**
	 * @param field_type $id_upload
	 */
	public function getId_upload() {
            return $this->id_upload;
	}

    /**
	 * @param $id_upload
	 */
	public function setId_upload($id_upload) {
		$this->id_upload = $id_upload;
	}

    /**
	 * @param field_type $st_justificativa
	 */
	public function getSt_justificativa() {
            return $this->st_justificativa;
	}

    /**
	 * @param $st_justificativa
	 */
	public function setSt_justificativa($st_justificativa) {
		$this->st_justificativa = $st_justificativa;
	}

    /**
     * @param mixed $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }

    /**
     * @return mixed
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param mixed $st_notaava
     */
    public function setSt_notaava($st_notaava)
    {
        $this->st_notaava = $st_notaava;
    }

    /**
     * @return mixed
     */
    public function getSt_notaava()
    {
        return $this->st_notaava;
    }

    /**
     * @return mixed
     */
    public function getId_categoriasala() {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala) {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param mixed $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }


    /**
     * @return mixed
     */
    public function getSt_notaconceitual()
    {
        return $this->st_notaconceitual;
    }

    /**
     * @param mixed $st_notaconceitual
     */
    public function setSt_notaconceitual($st_notaconceitual)
    {
        $this->st_notaconceitual = $st_notaconceitual;
    }

    /**
     * @return mixed
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param mixed $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }


}

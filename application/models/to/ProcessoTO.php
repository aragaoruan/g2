<?php
/**
 * tb_processo
 * @package models
 * @subpackage to
 */
class ProcessoTO extends Ead1_TO_Dinamico {

	const ATUALIZAR_FLUXUS 	= 1;
	const MENSAGEM_COBRANCA = 2;
    const AUTO_ALOCAR = 3;
    const IMPORTA_SALAS_MOODLE = 4;
	
	public $st_processo;
	public $id_processo;
	public $st_classe;
	
	/**
	 * @return the $st_processo
	 */
	public function getSt_processo() {
		return $this->st_processo;
	}

	/**
	 * @return the $id_processo
	 */
	public function getId_processo() {
		return $this->id_processo;
	}

	/**
	 * @return the $st_classe
	 */
	public function getSt_classe() {
		return $this->st_classe;
	}

	/**
	 * @param field_type $st_processo
	 */
	public function setSt_processo($st_processo) {
		$this->st_processo = $st_processo;
	}

	/**
	 * @param field_type $id_processo
	 */
	public function setId_processo($id_processo) {
		$this->id_processo = $id_processo;
	}

	/**
	 * @param field_type $st_classe
	 */
	public function setSt_classe($st_classe) {
		$this->st_classe = $st_classe;
	}



}

?>
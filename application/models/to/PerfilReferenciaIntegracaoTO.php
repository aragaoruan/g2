<?php

/**
 * @author Elcio
 *
 *
 */
class PerfilReferenciaIntegracaoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da tabela.
	 * @var int
	 */
	public $id_perfilreferenciaintegracao;

	/**
	 * Contém o id do sistema.
	 * @var int
	 */
	public $id_sistema;

	/**
	 * Contem o id do usuário que efetuou o cadastro
	 * @var int
	 */
	public $id_usuariocadastro;


    /**
     *
	 * Contem o ID do Perfil
	 * @var int
	 */
	public $id_perfilreferencia;

	/**
     *
	 * Código do objeto integrado no sistema integrado (Ex: Actor: codpapel)
	 * @var string
	 */
	public $st_codsistema;


    /**
     *
	 * Data de Cadastro do Registro
	 * @var date
	 */
	public $dt_cadastro;


    public $id_saladeaulaintegracao;

    /**
     * Flag
     * @var boolean
     */
    public $bl_encerrado;

	/**
	 * @return the $id_saladeaulaintegracao
	 */
	public function getId_saladeaulaintegracao() {
		return $this->id_saladeaulaintegracao;
	}

	/**
	 * @param field_type $id_saladeaulaintegracao
	 */
	public function setId_saladeaulaintegracao($id_saladeaulaintegracao) {
		$this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
	}

	/**
	 * @return the $id_perfilreferenciaintegracao
	 */
	public function getId_perfilreferenciaintegracao() {
		return $this->id_perfilreferenciaintegracao;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_perfilreferencia
	 */
	public function getId_perfilreferencia() {
		return $this->id_perfilreferencia;
	}

	/**
	 * @return the $st_codsistema
	 */
	public function getSt_codsistema() {
		return $this->st_codsistema;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param int $id_perfilreferenciaintegracao
	 */
	public function setId_perfilreferenciaintegracao($id_perfilreferenciaintegracao) {
		$this->id_perfilreferenciaintegracao = $id_perfilreferenciaintegracao;
	}

	/**
	 * @param int $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param int $id_perfilreferencia
	 */
	public function setId_perfilreferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}

	/**
	 * @param string $st_codsistema
	 */
	public function setSt_codsistema($st_codsistema) {
		$this->st_codsistema = $st_codsistema;
	}

	/**
	 * @param date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
    }
}

?>

<?php
/**
 * Classe para encapsular os dados da vw_ocorrenciaevolucao
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwOcorrenciaEvolucaoTO extends Ead1_TO_Dinamico {

	public $id_ocorrencia;
	public $id_funcao;
	public $id_evolucao;
	public $st_evolucao;
	public $id_nucleoco;
	
	/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

	/**
	 * @param field_type $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}

	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}
	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}


	
	
	
}

?>
<?php
/**
 * Classe para encapsular os dados de Tipo de Avaliacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class TipoAvaliacaoTO extends Ead1_TO_Dinamico{
	
	const NORMAL = 1;
	const RECUPERACAO = 2;
        const PRESENCIAL=4;
        const DISTANCIA=5;
	const TCC = 6;
	
	public $id_tipoavaliacao;
	public $st_tipoavaliacao;
	public $st_descricao;
	
	/**
	 * @return the $id_tipoavaliacao
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @return the $st_tipoavaliacao
	 */
	public function getSt_tipoavaliacao() {
		return $this->st_tipoavaliacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param field_type $id_tipoavaliacao
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param field_type $st_tipoavaliacao
	 */
	public function setSt_tipoavaliacao($st_tipoavaliacao) {
		$this->st_tipoavaliacao = $st_tipoavaliacao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
}
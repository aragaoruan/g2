<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class PreVendaTO extends Ead1_TO_Dinamico{

	public $id_prevenda;
	public $id_formapagamento;
	public $id_meiopagamento;
	public $id_usuario;
	public $id_entidade;
	public $st_nomecompleto;
	public $st_cpf;
	public $st_email;
	public $nu_telefonecelular;
	public $nu_dddcelular;
	public $nu_ddicelular;
	public $nu_telefoneresidencial;
	public $nu_dddresidencial;
	public $nu_ddiresidencial;
	public $nu_telefonecomercial;
	public $nu_dddcomercial;
	public $nu_ddicomercial;
	public $st_rg;
	public $st_orgaoexpeditor;
	public $id_pais;
	public $sg_uf;
	public $id_municipio;
	public $st_cep;
	public $st_endereco;
	public $st_bairro;
	public $st_complemento;
	public $nu_numero;
	public $st_sexo;
	public $dt_nascimento;
	public $id_paisnascimento;
	public $sg_ufnascimento;
	public $id_municipionascimento;
	public $dt_cadastro;
	public $nu_lancamentos;
	public $nu_juros;
	public $nu_desconto;
	public $nu_valorliquido;
	public $bl_ativo;
	public $id_tipoendereco;
	public $st_contato;
	public $st_instituicao;
	public $st_graduacao;
	public $st_localtrabalho;
	public $st_recomendacao;
	public $id_nivelensino;
	
	
	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @param field_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @return the $id_tipoendereco
	 */
	public function getId_tipoendereco() {
		return $this->id_tipoendereco;
	}

	/**
	 * @return the $id_prevenda
	 */
	public function getId_prevenda(){ 
		return $this->id_prevenda;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento(){ 
		return $this->id_formapagamento;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento(){ 
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto(){ 
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getSt_cpf(){ 
		return $this->st_cpf;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email(){ 
		return $this->st_email;
	}

	/**
	 * @return the $st_contato
	 */
	public function getSt_contato(){ 
		return $this->st_contato;
	}

	/**
	 * @return the $st_instituicao
	 */
	public function getSt_instituicao(){ 
		return $this->st_instituicao;
	}

	/**
	 * @return the $st_graduacao
	 */
	public function getSt_graduacao(){ 
		return $this->st_graduacao;
	}

	/**
	 * @return the $st_localtrabalho
	 */
	public function getSt_localtrabalho(){ 
		return $this->st_localtrabalho;
	}

	/**
	 * @return the $st_recomendacao
	 */
	public function getSt_recomendacao(){ 
		return $this->st_recomendacao;
	}

	/**
	 * @return the $st_telefonecelular
	 */
	public function getNu_telefonecelular(){ 
		return $this->nu_telefonecelular;
	}

	/**
	 * @return the $nu_dddcelular
	 */
	public function getNu_dddcelular(){ 
		return $this->nu_dddcelular;
	}

	/**
	 * @return the $nu_ddicelular
	 */
	public function getNu_ddicelular(){ 
		return $this->nu_ddicelular;
	}

	/**
	 * @return the $st_telefoneresidencial
	 */
	public function getNu_telefoneresidencial(){ 
		return $this->nu_telefoneresidencial;
	}

	/**
	 * @return the $nu_dddresidencial
	 */
	public function getNu_dddresidencial(){ 
		return $this->nu_dddresidencial;
	}

	/**
	 * @return the $nu_ddiresidencial
	 */
	public function getNu_ddiresidencial(){ 
		return $this->nu_ddiresidencial;
	}

	/**
	 * @return the $st_telefonecomercial
	 */
	public function getNu_telefonecomercial(){ 
		return $this->nu_telefonecomercial;
	}

	/**
	 * @return the $nu_dddcomercial
	 */
	public function getNu_dddcomercial(){ 
		return $this->nu_dddcomercial;
	}

	/**
	 * @return the $nu_ddicomercial
	 */
	public function getNu_ddicomercial(){ 
		return $this->nu_ddicomercial;
	}

	/**
	 * @return the $st_rg
	 */
	public function getSt_rg(){ 
		return $this->st_rg;
	}

	/**
	 * @return the $st_orgaoexpeditor
	 */
	public function getSt_orgaoexpeditor(){ 
		return $this->st_orgaoexpeditor;
	}

	/**
	 * @return the $id_pais
	 */
	public function getId_pais(){ 
		return $this->id_pais;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf(){ 
		return $this->sg_uf;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio(){ 
		return $this->id_municipio;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep(){ 
		return $this->st_cep;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco(){ 
		return $this->st_endereco;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro(){ 
		return $this->st_bairro;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento(){ 
		return $this->st_complemento;
	}

	/**
	 * @return the $st_numero
	 */
	public function getNu_numero(){ 
		return $this->nu_numero;
	}

	/**
	 * @return the $st_sexo
	 */
	public function getSt_sexo(){ 
		return $this->st_sexo;
	}

	/**
	 * @return the $dt_nascimento
	 */
	public function getDt_nascimento(){ 
		return $this->dt_nascimento;
	}

	/**
	 * @return the $id_paisnascimento
	 */
	public function getId_paisnascimento(){ 
		return $this->id_paisnascimento;
	}

	/**
	 * @return the $sg_ufnascimento
	 */
	public function getSg_ufnascimento(){ 
		return $this->sg_ufnascimento;
	}

	/**
	 * @return the $id_municipionascimento
	 */
	public function getId_municipionascimento(){ 
		return $this->id_municipionascimento;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $nu_lancamentos
	 */
	public function getNu_lancamentos(){ 
		return $this->nu_lancamentos;
	}

	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros(){ 
		return $this->nu_juros;
	}

	/**
	 * @return the $nu_desconto
	 */
	public function getNu_desconto(){ 
		return $this->nu_desconto;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido(){ 
		return $this->nu_valorliquido;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}
	
	/**
	 * @param field_type $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco) {
		$this->id_tipoendereco = $id_tipoendereco;
	}

	/**
	 * @param field_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda){ 
		$this->id_prevenda = $id_prevenda;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento){ 
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento){ 
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto){ 
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $nu_cpf
	 */
	public function setSt_cpf($st_cpf){ 
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email){ 
		$this->st_email = $st_email;
	}

	/**
	 * @param field_type $st_contato
	 */
	public function setSt_contato($st_contato){ 
		$this->st_contato = $st_contato;
	}

	/**
	 * @param field_type $st_instituicao
	 */
	public function setSt_instituicao($st_instituicao){ 
		$this->st_instituicao = $st_instituicao;
	}

	/**
	 * @param field_type $st_graduacao
	 */
	public function setSt_graduacao($st_graduacao){ 
		$this->st_graduacao = $st_graduacao;
	}

	/**
	 * @param field_type $st_localtrabalho
	 */
	public function setSt_localtrabalho($st_localtrabalho){ 
		$this->st_localtrabalho = $st_localtrabalho;
	}

	/**
	 * @param field_type $st_recomendacao
	 */
	public function setSt_recomendacao($st_recomendacao){ 
		$this->st_recomendacao = $st_recomendacao;
	}

	/**
	 * @param field_type $st_telefonecelular
	 */
	public function setNu_telefonecelular($nu_telefonecelular){ 
		$this->nu_telefonecelular = $nu_telefonecelular;
	}

	/**
	 * @param field_type $nu_dddcelular
	 */
	public function setNu_dddcelular($nu_dddcelular){ 
		$this->nu_dddcelular = $nu_dddcelular;
	}

	/**
	 * @param field_type $nu_ddicelular
	 */
	public function setNu_ddicelular($nu_ddicelular){ 
		$this->nu_ddicelular = $nu_ddicelular;
	}

	/**
	 * @param field_type $st_telefoneresidencial
	 */
	public function setNu_telefoneresidencial($nu_telefoneresidencial){ 
		$this->nu_telefoneresidencial = $nu_telefoneresidencial;
	}

	/**
	 * @param field_type $nu_dddresidencial
	 */
	public function setNu_dddresidencial($nu_dddresidencial){ 
		$this->nu_dddresidencial = $nu_dddresidencial;
	}

	/**
	 * @param field_type $nu_ddiresidencial
	 */
	public function setNu_ddiresidencial($nu_ddiresidencial){ 
		$this->nu_ddiresidencial = $nu_ddiresidencial;
	}

	/**
	 * @param field_type $st_telefonecomercial
	 */
	public function setNu_telefonecomercial($nu_telefonecomercial){ 
		$this->nu_telefonecomercial = $nu_telefonecomercial;
	}

	/**
	 * @param field_type $nu_dddcomercial
	 */
	public function setNu_dddcomercial($nu_dddcomercial){ 
		$this->nu_dddcomercial = $nu_dddcomercial;
	}

	/**
	 * @param field_type $nu_ddicomercial
	 */
	public function setNu_ddicomercial($nu_ddicomercial){ 
		$this->nu_ddicomercial = $nu_ddicomercial;
	}

	/**
	 * @param field_type st_rgg
	 */
	public function setSt_rg($st_rg){ 
		$this->st_rg = $st_rg;
	}

	/**
	 * @param field_type $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor){ 
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}

	/**
	 * @param field_type $id_pais
	 */
	public function setId_pais($id_pais){ 
		$this->id_pais = $id_pais;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf){ 
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio){ 
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param field_type $st_cep
	 */
	public function setSt_cep($st_cep){ 
		$this->st_cep = $st_cep;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco){ 
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_bairro
	 */
	public function setSt_bairro($st_bairro){ 
		$this->st_bairro = $st_bairro;
	}

	/**
	 * @param field_type $st_complemento
	 */
	public function setSt_complemento($st_complemento){ 
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @param field_type $st_numero
	 */
	public function setNu_numero($nu_numero){ 
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param field_type $st_sexo
	 */
	public function setSt_sexo($st_sexo){ 
		$this->st_sexo = $st_sexo;
	}

	/**
	 * @param field_type $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento){ 
		$this->dt_nascimento = $dt_nascimento;
	}

	/**
	 * @param field_type $id_paisnascimento
	 */
	public function setId_paisnascimento($id_paisnascimento){ 
		$this->id_paisnascimento = $id_paisnascimento;
	}

	/**
	 * @param field_type $sg_ufnascimento
	 */
	public function setSg_ufnascimento($sg_ufnascimento){ 
		$this->sg_ufnascimento = $sg_ufnascimento;
	}

	/**
	 * @param field_type $id_municipionascimento
	 */
	public function setId_municipionascimento($id_municipionascimento){ 
		$this->id_municipionascimento = $id_municipionascimento;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $nu_lancamentos
	 */
	public function setNu_lancamentos($nu_lancamentos){ 
		$this->nu_lancamentos = $nu_lancamentos;
	}

	/**
	 * @param field_type $nu_juros
	 */
	public function setNu_juros($nu_juros){ 
		$this->nu_juros = $nu_juros;
	}

	/**
	 * @param field_type $nu_desconto
	 */
	public function setNu_desconto($nu_desconto){ 
		$this->nu_desconto = $nu_desconto;
	}

	/**
	 * @param field_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido){ 
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

}
<?php
/**
 * Classe para encapsular os dados da view de gerar ocorrência
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwGerarOcorrenciaTO extends Ead1_TO_Dinamico {
		
		/**
		 * Chave da tabela
		 * @var int
		 */
		public $id_ocorrencia;   
		
		/**
		 * Matrícula do aluno (quando for ocorrencia de aluno)
		 * @var int
		 */
		public $id_matricula;  
		      
		/**
		 * Evolução da ocorrência
		 * @var int
		 */
		public $id_evolucao;  
				
		/**
		 * Situação da ocorrência
		 * @var int
		 */
		public $id_situacao; 
		
		/**
		 * Pessoa que criou a ocorrencia
		 * @var int
		 */
		public $id_usuariointeressado;  
		  
		/**
		 * Tipo de Ocorrência
		 * @var int
		 */
		public $id_categoriaocorrencia;   
		
		/**
		 * Assunto da Ocorrência
		 * @var int
		 */
		public $id_assuntoco; 
		
		/**
		 * Sala de aula (caso o aluno tenha selecionado
		 * @var int
		 */
		public $id_saladeaula;
		
		/**
		 * Título da ocorrência
		 * @var string
		 */
		public $st_titulo;
		    
		/**
		 * Descrição da ocorrência
		 * @var string
		 */
		public $st_ocorrencia;
		
		/**
		 * Texto da situação
		 * @var String
		 */
		public $st_situacao;
		
		/**
		 * Texto da evolução
		 * @var String
		 */
		public $st_evolucao;
		
		/**
		 * Texto do Assunto
		 * @var string
		 */
		public $st_assuntoco;
		
		/**
		 * Data último tramite
		 * @var Date
		 */
		public $dt_ultimotramite;
		
		
		/**
		 * Tipo de ocorrência
		 * @var String
		 */
		public $st_categoriaocorrencia;
		
		
	/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

	/**
	 * @param number $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param number $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param number $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param number $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $id_usuariointeressado
	 */
	public function getId_usuariointeressado() {
		return $this->id_usuariointeressado;
	}

	/**
	 * @param number $id_usuariointeressado
	 */
	public function setId_usuariointeressado($id_usuariointeressado) {
		$this->id_usuariointeressado = $id_usuariointeressado;
	}

	/**
	 * @return the $id_categoriaocorrencia
	 */
	public function getId_categoriaocorrencia() {
		return $this->id_categoriaocorrencia;
	}

	/**
	 * @param number $id_categoriaocorrencia
	 */
	public function setId_categoriaocorrencia($id_categoriaocorrencia) {
		$this->id_categoriaocorrencia = $id_categoriaocorrencia;
	}

	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @param number $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param number $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return the $st_titulo
	 */
	public function getSt_titulo() {
		return $this->st_titulo;
	}

	/**
	 * @param string $st_titulo
	 */
	public function setSt_titulo($st_titulo) {
		$this->st_titulo = $st_titulo;
	}

	/**
	 * @return the $st_ocorrencia
	 */
	public function getSt_ocorrencia() {
		return $this->st_ocorrencia;
	}

	/**
	 * @param string $st_ocorrencia
	 */
	public function setSt_ocorrencia($st_ocorrencia) {
		$this->st_ocorrencia = $st_ocorrencia;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param string $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @return the $st_assuntoco
	 */
	public function getSt_assuntoco() {
		return $this->st_assuntoco;
	}

	/**
	 * @param string $st_assuntoco
	 */
	public function setSt_assuntoco($st_assuntoco) {
		$this->st_assuntoco = $st_assuntoco;
	}

	/**
	 * @return the $dt_ultimotramite
	 */
	public function getDt_ultimotramite() {
		return $this->dt_ultimotramite;
	}

	/**
	 * @param Date $dt_ultimotramite
	 */
	public function setDt_ultimotramite($dt_ultimotramite) {
		$this->dt_ultimotramite = $dt_ultimotramite;
	}
	/**
	 * @return the $st_categoriaocorrencia
	 */
	public function getSt_categoriaocorrencia() {
		return $this->st_categoriaocorrencia;
	}

	/**
	 * @param string $st_categoriaocorrencia
	 */
	public function setSt_categoriaocorrencia($st_categoriaocorrencia) {
		$this->st_categoriaocorrencia = $st_categoriaocorrencia;
	}

}

?>
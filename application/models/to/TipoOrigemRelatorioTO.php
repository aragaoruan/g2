<?php
/**
 * Classe para encapsular os dados da tabela de tipos de origem dos relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class TipoOrigemRelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_tipoorigemrelatorio;
	public $st_tipoorigemrelatorio;
	
	/**
	 * @return the $id_tipoorigemrelatorio
	 */
	public function getId_tipoorigemrelatorio() {
		return $this->id_tipoorigemrelatorio;
	}

	/**
	 * @return the $st_tipoorigemrelatorio
	 */
	public function getSt_tipoorigemrelatorio() {
		return $this->st_tipoorigemrelatorio;
	}

	/**
	 * @param $id_tipoorigemrelatorio the $id_tipoorigemrelatorio to set
	 */
	public function setId_tipoorigemrelatorio($id_tipoorigemrelatorio) {
		$this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
	}

	/**
	 * @param $st_tipoorigemrelatorio the $st_tipoorigemrelatorio to set
	 */
	public function setSt_tipoorigemrelatorio($st_tipoorigemrelatorio) {
		$this->st_tipoorigemrelatorio = $st_tipoorigemrelatorio;
	}
}
<?php
/**
 * Classe para encapsular os dados da view de classe para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeClasseTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id da situação da entidade.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém o número do CNPJ da entidade.
	 * @var string
	 */
	public $st_cnpj;
	
	/**
	 * Contém o número da inscrição anual da entidade.
	 * @var string
	 */
	public $nu_inscricaoanual;
	
	/**
	 * Contém o nome da entidade.
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Contém a razão social da entidade.
	 * @var string
	 */
	public $st_razaosocial;
	
	/**
	 * Contém o caminho da imagem de logo da url da entidade.
	 * @var string
	 */
	public $st_urlimglogo;
	
	/**
	 * Diz se a entidade é ativa ou não.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Diz se a entidade acessa o sistema ou não.
	 * @var boolean
	 */
	public $bl_acessasistema;
	
	/**
	 * Contém o id da configuração da entidade.
	 * @var int
	 */
	public $id_configuracaoentidade;
	
	/**
	 * Contém o id da classe da entidade.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * Contém o id da entidade pai.
	 * @var int
	 */
	public $id_entidadepai;
	
	/**
	 * Contém a classe da entidade.
	 * @var string
	 */
	public $st_entidadeclasse;
	
	/**
	 * @return boolean
	 */
	public function getBl_acessasistema() {
		return $this->bl_acessasistema;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_configuracaoentidade() {
		return $this->id_configuracaoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cnpj() {
		return $this->st_cnpj;
	}
	
	/**
	 * @return string
	 */
	public function getNu_inscricaoanual() {
		return $this->nu_inscricaoanual;
	}
	
	/**
	 * @return string
	 */
	public function getSt_entidadeclasse() {
		return $this->st_entidadeclasse;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}
	
	/**
	 * @return string
	 */
	public function getSt_urlimglogo() {
		return $this->st_urlimglogo;
	}
	
	/**
	 * @param boolean $bl_acessasistema
	 */
	public function setBl_acessasistema($bl_acessasistema) {
		$this->bl_acessasistema = $bl_acessasistema;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_configuracaoentidade
	 */
	public function setId_configuracaoentidade($id_configuracaoentidade) {
		$this->id_configuracaoentidade = $id_configuracaoentidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	
	/**
	 * @param int $id_entidadepai
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param string $st_cnpj
	 */
	public function setSt_cnpj($st_cnpj) {
		$this->st_cnpj = $st_cnpj;
	}
	
	/**
	 * @param string $nu_inscricaoanual
	 */
	public function setNu_inscricaoanual($nu_inscricaoanual) {
		$this->nu_inscricaoanual = $nu_inscricaoanual;
	}
	
	/**
	 * @param string $st_entidadeclasse
	 */
	public function setSt_entidadeclasse($st_entidadeclasse) {
		$this->st_entidadeclasse = $st_entidadeclasse;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}
	
	/**
	 * @param string $st_urlimglogo
	 */
	public function setSt_urlimglogo($st_urlimglogo) {
		$this->st_urlimglogo = $st_urlimglogo;
	}

}

?>
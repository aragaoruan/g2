<?php
/**
 * Classe para encapsular dados da view de textos do sistema 
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class VwTextoSistemaTO extends Ead1_TO_Dinamico{
	
	public $id_textosistema;
	public $id_usuario;
	public $id_entidade;
	public $id_textoexibicao;
	public $id_textocategoria;
	public $st_textosistema;
	public $st_texto;
	public $dt_inicio;
	public $dt_fim;
	public $dt_cadastro;
	public $st_textoexibicao;    
	public $st_textocategoria;   
	public $id_situacao;   
	public $st_situacao;   
	public $st_descricaosituacao;   
	public $id_orientacaotexto;
	public $st_orientacaotexto;
	
	
	/**
	 * @return the $id_orientacaotexto
	 */
	public function getId_orientacaotexto(){

		return $this->id_orientacaotexto;
	}

	/**
	 * @return the $st_orientacaotexto
	 */
	public function getSt_orientacaotexto(){

		return $this->st_orientacaotexto;
	}

	/**
	 * @param $id_orientacaotexto the $id_orientacaotexto to set
	 */
	public function setId_orientacaotexto( $id_orientacaotexto ){

		$this->id_orientacaotexto = $id_orientacaotexto;
	}

	/**
	 * @param $st_orientacaotexto the $st_orientacaotexto to set
	 */
	public function setSt_orientacaotexto( $st_orientacaotexto ){

		$this->st_orientacaotexto = $st_orientacaotexto;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_textoexibicao
	 */
	public function getId_textoexibicao() {
		return $this->id_textoexibicao;
	}

	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria() {
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema() {
		return $this->st_textosistema;
	}

	/**
	 * @return the $st_texto
	 */
	public function getSt_texto() {
		return $this->st_texto;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param $id_textosistema the $id_textosistema to set
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_textoexibicao the $id_textoexibicao to set
	 */
	public function setId_textoexibicao($id_textoexibicao) {
		$this->id_textoexibicao = $id_textoexibicao;
	}

	/**
	 * @param $id_textocategoria the $id_textocategoria to set
	 */
	public function setId_textocategoria($id_textocategoria) {
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param $st_textosistema the $st_textosistema to set
	 */
	public function setSt_textosistema($st_textosistema) {
		$this->st_textosistema = $st_textosistema;
	}

	/**
	 * @param $st_texto the $st_texto to set
	 */
	public function setSt_texto($st_texto) {
		$this->st_texto = $st_texto;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_fim the $dt_fim to set
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $st_textoexibicao
	 */
	public function getSt_textoexibicao() {
		return $this->st_textoexibicao;
	}

	/**
	 * @return the $st_textocategoria
	 */
	public function getSt_textocategoria() {
		return $this->st_textocategoria;
	}

	/**
	 * @param $st_textoexibicao the $st_textoexibicao to set
	 */
	public function setSt_textoexibicao($st_textoexibicao) {
		$this->st_textoexibicao = $st_textoexibicao;
	}

	/**
	 * @param $st_textocategoria the $st_textocategoria to set
	 */
	public function setSt_textocategoria($st_textocategoria) {
		$this->st_textocategoria = $st_textocategoria;
	}
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_descricaosituacao
	 */
	public function getSt_descricaosituacao() {
		return $this->st_descricaosituacao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param $st_descricaosituacao the $st_descricaosituacao to set
	 */
	public function setSt_descricaosituacao($st_descricaosituacao) {
		$this->st_descricaosituacao = $st_descricaosituacao;
	}



}
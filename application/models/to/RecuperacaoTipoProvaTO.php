<?php
/**
 * Classe para encapsular os dados de tipo de prova de recuperação do projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class RecuperacaoTipoProvaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de prova.
	 * @var int
	 */
	public $id_tipoprova;
	
	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id do tipo de recuperação.
	 * @var int
	 */
	public $id_tiporecuperacao;
	
	/**
	 * Valor da taxa do tipo de prova.
	 * @var string
	 */
	public $nu_taxa;
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiporecuperacao() {
		return $this->id_tiporecuperacao;
	}
	
	/**
	 * @return string
	 */
	public function getNu_taxa() {
		return $this->nu_taxa;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}
	
	/**
	 * @param int $id_tiporecuperacao
	 */
	public function setId_tiporecuperacao($id_tiporecuperacao) {
		$this->id_tiporecuperacao = $id_tiporecuperacao;
	}
	
	/**
	 * @param string $nu_taxa
	 */
	public function setNu_taxa($nu_taxa) {
		$this->nu_taxa = $nu_taxa;
	}

}

?>
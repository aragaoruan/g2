<?php
/**
 * Classe para encapsular os dados de Função para Perfil.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class FuncaoTO extends Ead1_TO_Dinamico {
	
	const ATENDENTE = 1;
	const GERENTE = 2;
	const DISTRIBUIDOR = 3;
	const ATENDENTE_CA = 8;
	const RESPONSAVEL_CA = 9;
	const ATENDENTE_SETOR_CA = 10;

	
	public $id_funcao;
	public $st_funcao;
	public $id_tipofuncao;
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipofuncao() {
		return $this->id_tipofuncao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $id_tipofuncao
	 */
	public function setId_tipofuncao($id_tipofuncao) {
		$this->id_tipofuncao = $id_tipofuncao;
	}
	
	/**
	 * @param unknown_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}

}

?>
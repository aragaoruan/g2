<?php
/**
 * Classe de valor para os dados de configuração de e-mail
 * Armazena todos os dados necessários para configuração de uma conta de e-mail
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeidapereira@gmail.com >
 */
class EmailConfiguracaoTO	extends Ead1_TO_Dinamico {
	
	/**
	 * Endereço da conta de e-mail 
	 * Exemplo: conta@dominio.com
	 * @var string
	 */
	public $st_conta;
	
	/**
	 * Endereço do servidor IMAP para recebimento de mensagem 
	 * @var string
	 */
	public $st_imap;
	
	/**
	 * Endereço do servidor SMTP para envio de mensagem
	 * @var string
	 */
	public $st_smtp;
	
	/**
	 * Endereço do servidor POP para recebimento de mensagem
	 * @var string
	 */
	public $st_pop;
	
	/**
	 * Flag para controle de autenticacao segura ( SSL/TLS )
	 * @var bool
	 */
	public $bl_autenticacaosegura;
	
	/**
	 * Número da porta de entrada de e-mail
	 * @var int
	 */
	public $nu_portaentrada;
	
	/**
	 * Número da porta de saída de e-mai;
	 * @var int
	 */
	public $nu_portasaida;
	
	/**
	 * 
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * 
	 * @var string
	 */
	public $st_usuario;

	/**
	 * 
	 * @var string
	 */
	public $st_senha;
	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $st_imap
	 */
	public function getSt_imap() {
		return $this->st_imap;
	}

	/**
	 * @return the $st_smtp
	 */
	public function getSt_smtp() {
		return $this->st_smtp;
	}

	/**
	 * @return the $st_pop
	 */
	public function getSt_pop() {
		return $this->st_pop;
	}

	/**
	 * @return the $bl_autenticacaosegura
	 */
	public function getBl_autenticacaosegura() {
		return $this->bl_autenticacaosegura;
	}

	/**
	 * @return the $nu_portaentrada
	 */
	public function getNu_portaentrada() {
		return $this->nu_portaentrada;
	}

	/**
	 * @return the $nu_portasaida
	 */
	public function getNu_portasaida() {
		return $this->nu_portasaida;
	}

	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario() {
		return $this->st_usuario;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}

	/**
	 * @param $st_conta the $st_conta to set
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param $st_imap the $st_imap to set
	 */
	public function setSt_imap($st_imap) {
		$this->st_imap = $st_imap;
	}

	/**
	 * @param $st_smtp the $st_smtp to set
	 */
	public function setSt_smtp($st_smtp) {
		$this->st_smtp = $st_smtp;
	}

	/**
	 * @param $st_pop the $st_pop to set
	 */
	public function setSt_pop($st_pop) {
		$this->st_pop = $st_pop;
	}

	/**
	 * @param $bl_autenticacaosegura the $bl_autenticacaosegura to set
	 */
	public function setBl_autenticacaosegura($bl_autenticacaosegura) {
		$this->bl_autenticacaosegura = $bl_autenticacaosegura;
	}

	/**
	 * @param $nu_portaentrada the $nu_portaentrada to set
	 */
	public function setNu_portaentrada($nu_portaentrada) {
		$this->nu_portaentrada = $nu_portaentrada;
	}

	/**
	 * @param $nu_portasaida the $nu_portasaida to set
	 */
	public function setNu_portasaida($nu_portasaida) {
		$this->nu_portasaida = $nu_portasaida;
	}

	/**
	 * @param $st_usuario the $st_usuario to set
	 */
	public function setSt_usuario($st_usuario) {
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @param $st_senha the $st_senha to set
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
}
<?php
/**
 * Classe para encapsular os dados de documentos substitutos.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentosSubstituicaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id de documentos.
	 * @var int
	 */
	public $id_documentos;
	
	/**
	 * Id do documento substituto.
	 * @var int
	 */
	public $id_documentossubstituto;
	
	/**
	 * @return int
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return int
	 */
	public function getId_documentossubstituto() {
		return $this->id_documentossubstituto;
	}
	
	/**
	 * @param int $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param int $id_documentossubstituto
	 */
	public function setId_documentossubstituto($id_documentossubstituto) {
		$this->id_documentossubstituto = $id_documentossubstituto;
	}

}

?>
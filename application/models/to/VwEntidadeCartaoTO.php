<?php
/**
 * Classe para encapsular os dados da view de cartao para entidade.
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeCartaoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	public $id_cartaoconfig;
	public $id_cartaobandeira;
	public $st_cartaobandeira;
	public $id_cartaooperadora;
	public $st_cartaooperadora;
	public $st_contratooperadora;
	public $st_gateway;
	public $id_sistema;
	public $st_sistema;
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_cartaoconfig
	 */
	public function getId_cartaoconfig() {
		return $this->id_cartaoconfig;
	}

	/**
	 * @param field_type $id_cartaoconfig
	 */
	public function setId_cartaoconfig($id_cartaoconfig) {
		$this->id_cartaoconfig = $id_cartaoconfig;
	}

	/**
	 * @return the $id_cartaobandeira
	 */
	public function getId_cartaobandeira() {
		return $this->id_cartaobandeira;
	}

	/**
	 * @param field_type $id_cartaobandeira
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}

	/**
	 * @return the $st_cartaobandeira
	 */
	public function getSt_cartaobandeira() {
		return $this->st_cartaobandeira;
	}

	/**
	 * @param field_type $st_cartaobandeira
	 */
	public function setSt_cartaobandeira($st_cartaobandeira) {
		$this->st_cartaobandeira = $st_cartaobandeira;
	}

	/**
	 * @return the $id_cartaooperadora
	 */
	public function getId_cartaooperadora() {
		return $this->id_cartaooperadora;
	}

	/**
	 * @param field_type $id_cartaooperadora
	 */
	public function setId_cartaooperadora($id_cartaooperadora) {
		$this->id_cartaooperadora = $id_cartaooperadora;
	}

	/**
	 * @return the $st_cartaooperadora
	 */
	public function getSt_cartaooperadora() {
		return $this->st_cartaooperadora;
	}

	/**
	 * @param field_type $st_cartaooperadora
	 */
	public function setSt_cartaooperadora($st_cartaooperadora) {
		$this->st_cartaooperadora = $st_cartaooperadora;
	}

	/**
	 * @return the $st_contratooperadora
	 */
	public function getSt_contratooperadora() {
		return $this->st_contratooperadora;
	}

	/**
	 * @param field_type $st_contratooperadora
	 */
	public function setSt_contratooperadora($st_contratooperadora) {
		$this->st_contratooperadora = $st_contratooperadora;
	}

	/**
	 * @return the $st_gateway
	 */
	public function getSt_gateway() {
		return $this->st_gateway;
	}

	/**
	 * @param field_type $st_gateway
	 */
	public function setSt_gateway($st_gateway) {
		$this->st_gateway = $st_gateway;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $st_sistema
	 */
	public function getSt_sistema() {
		return $this->st_sistema;
	}

	/**
	 * @param field_type $st_sistema
	 */
	public function setSt_sistema($st_sistema) {
		$this->st_sistema = $st_sistema;
	}



}

?>
<?php

/**
 * Classe para encapsular os dados da view de geração de contrato
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwGerarContratoTO extends Ead1_TO_Dinamico
{

    public $id_contrato;
    public $id_matricula;
    public $id_usuarioaluno;
    public $st_nomecompletoaluno;
    public $st_cpfaluno;
    public $st_rgaluno;
    public $st_orgaoexpeditor;
    public $st_nomecompletoresponsavel;
    public $st_cpfresponsavel;
    //public $id_tipocontratoresponsavel;
    //public $st_tipocontratoresponsavel;
    public $id_tipocontratoresponsavel_financeiro;
    public $id_tipocontratoresponsavel_pedagogico;
    public $st_tipocontratoresponsavel_financeiro;
    public $st_tipocontratoresponsavel_pedagogico;
    public $nu_porcentagem;
    public $st_enderecoresponsavel;
    public $st_complementoresponsavel;
    public $st_cepresponsavel;
    public $st_cidaderesponsavel;
    public $st_bairroresponsavel;
    public $sg_ufresponsavel;
    public $dt_nascimentoaluno;
    public $st_municipioaluno;
    public $st_ufaluno;
    public $nu_dddaluno;
    public $nu_telefonealuno;
    public $st_projeto;
    public $st_nomeentidade;
    public $st_razaosocial;
    public $st_cnpj;
    public $st_urlimglogo;
    public $st_urlsite;
    public $dt_aceita;
    public $id_usuarioresponsavel;
    public $st_estadocivil;
    public $st_orgaoexpedidor;
    public $st_dataexpedicaoaluno;
    public $st_numerotitulo;
    public $st_areatitulo;
    public $st_zonatitulo;
    public $st_dataexpedicaotitulo;
    public $st_numeroreservita;
    public $st_numerocertidao;
    public $st_livrocertidao;
    public $st_folhascertidao;
    public $st_descricaocartorio;
    public $st_cargo;
    public $dt_nascimentoresponsavel;
    public $st_rgresp;
    public $st_orgaoexpedidorresp;
    public $st_dataexpedicaoresp;
    public $nu_telefoneresp;
    public $st_enderecoaluno;
    public $st_bairroaluno;
    public $st_cepaluno;
    public $st_cidadealuno;
    public $st_paisaluno;
    public $st_ufcompletoaluno;
    public $st_nacionalidadealuno;
    public $st_nomemae;
    public $st_nomepai;
    public $st_datahora;
    public $st_sexoaluno;
    public $nu_dia;
    public $nu_mes;
    public $nu_ano;
    public $id_entidade;
    public $st_polo;
    public $st_diretor;
    public $st_secretarioescolar;
    public $nu_diamensalidade;
    public $st_etnia;
    public $st_email;
    public $st_tiposanguineo;
    public $st_alergias;
    public $st_glicemia;
    public $nu_valorliquido_venda;
    public $nu_parcelas_venda;
    public $dt_confirmacao_venda;
    public $st_campanhapontualidade;
    public $st_porcentagemcampanhapontualidade;
    public $st_aceita;
    public $dt_transferencia;
    public $id_matriculaorigem;
    public $st_projetoorigem;
    public $id_matriculadestino;
    public $st_projetodestino;
    public $nu_cargahorariadestino;
    public $nu_valorliquidotransferencia;
    public $id_vendaaditivo;

    /**
     * @return the $id_contrato
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $id_usuarioaluno
     */
    public function getId_usuarioaluno()
    {
        return $this->id_usuarioaluno;
    }

    /**
     * @return the $st_nomecompletoaluno
     */
    public function getSt_nomecompletoaluno()
    {
        return $this->st_nomecompletoaluno;
    }

    /**
     * @return the $st_cpfaluno
     */
    public function getSt_cpfaluno()
    {
        return $this->st_cpfaluno;
    }

    /**
     * @return the $st_rgaluno
     */
    public function getSt_rgaluno()
    {
        return $this->st_rgaluno;
    }

    /**
     * @return the $st_orgaoexpeditor
     */
    public function getSt_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @return the $st_nomecompletoresponsavel
     */
    public function getSt_nomecompletoresponsavel()
    {
        return $this->st_nomecompletoresponsavel;
    }

    /**
     * @return the $st_cpfresponsavel
     */
    public function getSt_cpfresponsavel()
    {
        return $this->st_cpfresponsavel;
    }

    /**
     * @return the $nu_porcentagem
     */
    public function getNu_porcentagem()
    {
        return $this->nu_porcentagem;
    }

    /**
     * @return the $st_enderecoresponsavel
     */
    public function getSt_enderecoresponsavel()
    {
        return $this->st_enderecoresponsavel;
    }

    /**
     * @return the $st_complementoresponsavel
     */
    public function getSt_complementoresponsavel()
    {
        return $this->st_complementoresponsavel;
    }

    /**
     * @return the $st_cepresponsavel
     */
    public function getSt_cepresponsavel()
    {
        return $this->st_cepresponsavel;
    }

    /**
     * @return the $st_cidaderesponsavel
     */
    public function getSt_cidaderesponsavel()
    {
        return $this->st_cidaderesponsavel;
    }

    /**
     * @return the $st_bairroresponsavel
     */
    public function getSt_bairroresponsavel()
    {
        return $this->st_bairroresponsavel;
    }

    /**
     * @return the $sg_ufresponsavel
     */
    public function getSg_ufresponsavel()
    {
        return $this->sg_ufresponsavel;
    }

    /**
     * @return the $dt_nascimentoaluno
     */
    public function getDt_nascimentoaluno()
    {
        return $this->dt_nascimentoaluno;
    }

    /**
     * @return the $st_municipioaluno
     */
    public function getSt_municipioaluno()
    {
        return $this->st_municipioaluno;
    }

    /**
     * @return the $st_ufaluno
     */
    public function getSt_ufaluno()
    {
        return $this->st_ufaluno;
    }

    /**
     * @return the $nu_dddaluno
     */
    public function getNu_dddaluno()
    {
        return $this->nu_dddaluno;
    }

    /**
     * @return the $nu_telefonealuno
     */
    public function getNu_telefonealuno()
    {
        return $this->nu_telefonealuno;
    }

    /**
     * @return the $st_projeto
     */
    public function getSt_projeto()
    {
        return $this->st_projeto;
    }

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return the $st_razaosocial
     */
    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    /**
     * @return the $st_cnpj
     */
    public function getSt_cnpj()
    {
        return $this->st_cnpj;
    }

    /**
     * @return the $st_urlimglogo
     */
    public function getSt_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    /**
     * @return the $st_urlsite
     */
    public function getSt_urlsite()
    {
        return $this->st_urlsite;
    }

    /**
     * @param field_type $id_contrato
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $id_usuarioaluno
     */
    public function setId_usuarioaluno($id_usuarioaluno)
    {
        $this->id_usuarioaluno = $id_usuarioaluno;
    }

    /**
     * @param field_type $st_nomecompletoaluno
     */
    public function setSt_nomecompletoaluno($st_nomecompletoaluno)
    {
        $this->st_nomecompletoaluno = $st_nomecompletoaluno;
    }

    /**
     * @param field_type $st_cpfaluno
     */
    public function setSt_cpfaluno($st_cpfaluno)
    {
        $this->st_cpfaluno = $st_cpfaluno;
    }

    /**
     * @param field_type st_rgaluno
     */
    public function setSt_rgaluno($st_rgaluno)
    {
        $this->st_rgaluno = $st_rgaluno;
    }

    /**
     * @param field_type $st_orgaoexpeditor
     */
    public function setSt_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
    }

    /**
     * @param field_type $st_nomecompletoresponsavel
     */
    public function setSt_nomecompletoresponsavel($st_nomecompletoresponsavel)
    {
        $this->st_nomecompletoresponsavel = $st_nomecompletoresponsavel;
    }

    /**
     * @param field_type $st_cpfresponsavel
     */
    public function setSt_cpfresponsavel($st_cpfresponsavel)
    {
        $this->st_cpfresponsavel = $st_cpfresponsavel;
    }

    public function getId_tipocontratoresponsavel_financeiro()
    {
        return $this->id_tipocontratoresponsavel_financeiro;
    }

    public function setId_tipocontratoresponsavel_financeiro($id_tipocontratoresponsavel_financeiro)
    {
        $this->id_tipocontratoresponsavel_financeiro = $id_tipocontratoresponsavel_financeiro;
    }

    public function getId_tipocontratoresponsavel_pedagogico()
    {
        return $this->id_tipocontratoresponsavel_pedagogico;
    }

    public function setId_tipocontratoresponsavel_pedagogico($id_tipocontratoresponsavel_pedagogico)
    {
        $this->id_tipocontratoresponsavel_pedagogico = $id_tipocontratoresponsavel_pedagogico;
    }

    public function getSt_tipocontratoresponsavel_financeiro()
    {
        return $this->st_tipocontratoresponsavel_financeiro;
    }

    public function setSt_tipocontratoresponsavel_financeiro($st_tipocontratoresponsavel_financeiro)
    {
        $this->st_tipocontratoresponsavel_financeiro = $st_tipocontratoresponsavel_financeiro;
    }

    public function getSt_tipocontratoresponsavel_pedagogico()
    {
        return $this->st_tipocontratoresponsavel_pedagogico;
    }

    public function setSt_tipocontratoresponsavel_pedagogico($st_tipocontratoresponsavel_pedagogico)
    {
        $this->st_tipocontratoresponsavel_pedagogico = $st_tipocontratoresponsavel_pedagogico;
    }

    /**
     * @param field_type $nu_porcentagem
     */
    public function setNu_porcentagem($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
    }

    /**
     * @param field_type $st_enderecoresponsavel
     */
    public function setSt_enderecoresponsavel($st_enderecoresponsavel)
    {
        $this->st_enderecoresponsavel = $st_enderecoresponsavel;
    }

    /**
     * @param field_type $st_complementoresponsavel
     */
    public function setSt_complementoresponsavel($st_complementoresponsavel)
    {
        $this->st_complementoresponsavel = $st_complementoresponsavel;
    }

    /**
     * @param field_type $st_cepresponsavel
     */
    public function setSt_cepresponsavel($st_cepresponsavel)
    {
        $this->st_cepresponsavel = $st_cepresponsavel;
    }

    /**
     * @param field_type $st_cidaderesponsavel
     */
    public function setSt_cidaderesponsavel($st_cidaderesponsavel)
    {
        $this->st_cidaderesponsavel = $st_cidaderesponsavel;
    }

    /**
     * @param field_type $st_bairroresponsavel
     */
    public function setSt_bairroresponsavel($st_bairroresponsavel)
    {
        $this->st_bairroresponsavel = $st_bairroresponsavel;
    }

    /**
     * @param field_type $sg_ufresponsavel
     */
    public function setSg_ufresponsavel($sg_ufresponsavel)
    {
        $this->sg_ufresponsavel = $sg_ufresponsavel;
    }

    /**
     * @param field_type $dt_nascimentoaluno
     */
    public function setDt_nascimentoaluno($dt_nascimentoaluno)
    {
        $this->dt_nascimentoaluno = $dt_nascimentoaluno;
    }

    /**
     * @param field_type $st_municipioaluno
     */
    public function setSt_municipioaluno($st_municipioaluno)
    {
        $this->st_municipioaluno = $st_municipioaluno;
    }

    /**
     * @param field_type $st_ufaluno
     */
    public function setSt_ufaluno($st_ufaluno)
    {
        $this->st_ufaluno = $st_ufaluno;
    }

    /**
     * @param field_type $nu_dddaluno
     */
    public function setNu_dddaluno($nu_dddaluno)
    {
        $this->nu_dddaluno = $nu_dddaluno;
    }

    /**
     * @param field_type st_telefonealunoo
     */
    public function setNu_telefonealuno($nu_telefonealuno)
    {
        $this->nu_telefonealuno = $nu_telefonealuno;
    }

    /**
     * @param field_type $st_projeto
     */
    public function setSt_projeto($st_projeto)
    {
        $this->st_projeto = $st_projeto;
    }

    /**
     * @param field_type $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param field_type $st_razaosocial
     */
    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
    }

    /**
     * @param field_type $st_cnpj
     */
    public function setSt_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
    }

    /**
     * @param field_type $st_urlimglogo
     */
    public function setSt_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
    }

    /**
     * @param field_type $st_urlsite
     */
    public function setSt_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
    }

    public function getDt_aceita()
    {
        return $this->dt_aceita;
    }

    public function setDt_aceita($dt_aceita)
    {
        $this->dt_aceita = $dt_aceita;
    }

    public function getId_usuarioresponsavel()
    {
        return $this->id_usuarioresponsavel;
    }

    public function getSt_estadocivil()
    {
        return $this->st_estadocivil;
    }

    public function getSt_orgaoexpedidor()
    {
        return $this->st_orgaoexpedidor;
    }

    public function getSt_dataexpedicaoaluno()
    {
        return $this->st_dataexpedicaoaluno;
    }

    public function getSt_numerotitulo()
    {
        return $this->st_numerotitulo;
    }

    public function getSt_areatitulo()
    {
        return $this->st_areatitulo;
    }

    public function getSt_zonatitulo()
    {
        return $this->st_zonatitulo;
    }

    public function getSt_dataexpedicaotitulo()
    {
        return $this->st_dataexpedicaotitulo;
    }

    public function getSt_numeroreservita()
    {
        return $this->st_numeroreservita;
    }

    public function getSt_numerocertidao()
    {
        return $this->st_numerocertidao;
    }

    public function getSt_livrocertidao()
    {
        return $this->st_livrocertidao;
    }

    public function getSt_folhascertidao()
    {
        return $this->st_folhascertidao;
    }

    public function getSt_descricaocartorio()
    {
        return $this->st_descricaocartorio;
    }

    public function getSt_cargo()
    {
        return $this->st_cargo;
    }

    public function getDt_nascimentoresponsavel()
    {
        return $this->dt_nascimentoresponsavel;
    }

    public function getSt_rgresp()
    {
        return $this->st_rgresp;
    }

    public function getSt_orgaoexpedidorresp()
    {
        return $this->st_orgaoexpedidorresp;
    }

    public function getSt_dataexpedicaoresp()
    {
        return $this->st_dataexpedicaoresp;
    }

    public function getNu_telefoneresp()
    {
        return $this->nu_telefoneresp;
    }

    public function getSt_enderecoaluno()
    {
        return $this->st_enderecoaluno;
    }

    public function getSt_bairroaluno()
    {
        return $this->st_bairroaluno;
    }

    public function getSt_cepaluno()
    {
        return $this->st_cepaluno;
    }

    public function getSt_cidadealuno()
    {
        return $this->st_cidadealuno;
    }

    public function getSt_paisaluno()
    {
        return $this->st_paisaluno;
    }

    public function getSt_ufcompletoaluno()
    {
        return $this->st_ufcompletoaluno;
    }

    public function getSt_nacionalidadealuno()
    {
        return $this->st_nacionalidadealuno;
    }

    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    public function getSt_datahora()
    {
        return $this->st_datahora;
    }

    public function getSt_sexoaluno()
    {
        return $this->st_sexoaluno;
    }

    public function getNu_dia()
    {
        return $this->nu_dia;
    }

    public function getNu_mes()
    {
        return $this->nu_mes;
    }

    public function getNu_ano()
    {
        return $this->nu_ano;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getSt_polo()
    {
        return $this->st_polo;
    }

    public function getSt_diretor()
    {
        return $this->st_diretor;
    }

    public function getSt_secretarioescolar()
    {
        return $this->st_secretarioescolar;
    }

    public function getNu_diamensalidade()
    {
        return $this->nu_diamensalidade;
    }

    public function getSt_etnia()
    {
        return $this->st_etnia;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function getSt_tiposanguineo()
    {
        return $this->st_tiposanguineo;
    }

    public function getSt_alergias()
    {
        return $this->st_alergias;
    }

    public function getSt_glicemia()
    {
        return $this->st_glicemia;
    }

    public function getNu_valorliquido_venda()
    {
        return $this->nu_valorliquido_venda;
    }

    public function getNu_parcelas_venda()
    {
        return $this->nu_parcelas_venda;
    }

    public function getDt_confirmacao_venda()
    {
        return $this->dt_confirmacao_venda;
    }

    public function setId_usuarioresponsavel($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
    }

    public function setSt_estadocivil($st_estadocivil)
    {
        $this->st_estadocivil = $st_estadocivil;
    }

    public function setSt_orgaoexpedidor($st_orgaoexpedidor)
    {
        $this->st_orgaoexpedidor = $st_orgaoexpedidor;
    }

    public function setSt_dataexpedicaoaluno($st_dataexpedicaoaluno)
    {
        $this->st_dataexpedicaoaluno = $st_dataexpedicaoaluno;
    }

    public function setSt_numerotitulo($st_numerotitulo)
    {
        $this->st_numerotitulo = $st_numerotitulo;
    }

    public function setSt_areatitulo($st_areatitulo)
    {
        $this->st_areatitulo = $st_areatitulo;
    }

    public function setSt_zonatitulo($st_zonatitulo)
    {
        $this->st_zonatitulo = $st_zonatitulo;
    }

    public function setSt_dataexpedicaotitulo($st_dataexpedicaotitulo)
    {
        $this->st_dataexpedicaotitulo = $st_dataexpedicaotitulo;
    }

    public function setSt_numeroreservita($st_numeroreservita)
    {
        $this->st_numeroreservita = $st_numeroreservita;
    }

    public function setSt_numerocertidao($st_numerocertidao)
    {
        $this->st_numerocertidao = $st_numerocertidao;
    }

    public function setSt_livrocertidao($st_livrocertidao)
    {
        $this->st_livrocertidao = $st_livrocertidao;
    }

    public function setSt_folhascertidao($st_folhascertidao)
    {
        $this->st_folhascertidao = $st_folhascertidao;
    }

    public function setSt_descricaocartorio($st_descricaocartorio)
    {
        $this->st_descricaocartorio = $st_descricaocartorio;
    }

    public function setSt_cargo($st_cargo)
    {
        $this->st_cargo = $st_cargo;
    }

    public function setDt_nascimentoresponsavel($dt_nascimentoresponsavel)
    {
        $this->dt_nascimentoresponsavel = $dt_nascimentoresponsavel;
    }

    public function setSt_rgresp($st_rgresp)
    {
        $this->st_rgresp = $st_rgresp;
    }

    public function setSt_orgaoexpedidorresp($st_orgaoexpedidorresp)
    {
        $this->st_orgaoexpedidorresp = $st_orgaoexpedidorresp;
    }

    public function setSt_dataexpedicaoresp($st_dataexpedicaoresp)
    {
        $this->st_dataexpedicaoresp = $st_dataexpedicaoresp;
    }

    public function setNu_telefoneresp($nu_telefoneresp)
    {
        $this->nu_telefoneresp = $nu_telefoneresp;
    }

    public function setSt_enderecoaluno($st_enderecoaluno)
    {
        $this->st_enderecoaluno = $st_enderecoaluno;
    }

    public function setSt_bairroaluno($st_bairroaluno)
    {
        $this->st_bairroaluno = $st_bairroaluno;
    }

    public function setSt_cepaluno($st_cepaluno)
    {
        $this->st_cepaluno = $st_cepaluno;
    }

    public function setSt_cidadealuno($st_cidadealuno)
    {
        $this->st_cidadealuno = $st_cidadealuno;
    }

    public function setSt_paisaluno($st_paisaluno)
    {
        $this->st_paisaluno = $st_paisaluno;
    }

    public function setSt_ufcompletoaluno($st_ufcompletoaluno)
    {
        $this->st_ufcompletoaluno = $st_ufcompletoaluno;
    }

    public function setSt_nacionalidadealuno($st_nacionalidadealuno)
    {
        $this->st_nacionalidadealuno = $st_nacionalidadealuno;
    }

    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
    }

    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
    }

    public function setSt_datahora($st_datahora)
    {
        $this->st_datahora = $st_datahora;
    }

    public function setSt_sexoaluno($st_sexoaluno)
    {
        $this->st_sexoaluno = $st_sexoaluno;
    }

    public function setNu_dia($nu_dia)
    {
        $this->nu_dia = $nu_dia;
    }

    public function setNu_mes($nu_mes)
    {
        $this->nu_mes = $nu_mes;
    }

    public function setNu_ano($nu_ano)
    {
        $this->nu_ano = $nu_ano;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function setSt_polo($st_polo)
    {
        $this->st_polo = $st_polo;
    }

    public function setSt_diretor($st_diretor)
    {
        $this->st_diretor = $st_diretor;
    }

    public function setSt_secretarioescolar($st_secretarioescolar)
    {
        $this->st_secretarioescolar = $st_secretarioescolar;
    }

    public function setNu_diamensalidade($nu_diamensalidade)
    {
        $this->nu_diamensalidade = $nu_diamensalidade;
    }

    public function setSt_etnia($st_etnia)
    {
        $this->st_etnia = $st_etnia;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    public function setSt_tiposanguineo($st_tiposanguineo)
    {
        $this->st_tiposanguineo = $st_tiposanguineo;
    }

    public function setSt_alergias($st_alergias)
    {
        $this->st_alergias = $st_alergias;
    }

    public function setSt_glicemia($st_glicemia)
    {
        $this->st_glicemia = $st_glicemia;
    }

    public function setNu_valorliquido_venda($nu_valorliquido_venda)
    {
        $this->nu_valorliquido_venda = $nu_valorliquido_venda;
    }

    public function setNu_parcelas_venda($nu_parcelas_venda)
    {
        $this->nu_parcelas_venda = $nu_parcelas_venda;
    }

    public function setDt_confirmacao_venda($dt_confirmacao_venda)
    {
        $this->dt_confirmacao_venda = $dt_confirmacao_venda;
    }


    public function setSt_campanhapontualidade($st_campanhapontualidade)
    {
        $this->st_campanhapontualidade = $st_campanhapontualidade;
    }

    public function getSt_campanhapontualidade()
    {
        return $this->st_campanhapontualidade;
    }

    /**
     * @return mixed
     */
    public function getSt_porcentagemcampanhapontualidade()
    {
        return $this->st_porcentagemcampanhapontualidade;
    }

    /**
     * @param mixed $st_porcentagemcampanhapontualidade
     * @return $this
     */
    public function setSt_porcentagemcampanhapontualidade($st_porcentagemcampanhapontualidade)
    {
        $this->st_porcentagemcampanhapontualidade = $st_porcentagemcampanhapontualidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_aceita()
    {
        return $this->st_aceita;
    }

    /**
     * @param mixed $st_aceita
     */
    public function setSt_aceita($st_aceita)
    {
        $this->st_aceita = $st_aceita;
    }

    /**
     * @return mixed
     */
    public function getDt_transferencia()
    {
        return $this->dt_transferencia;
    }

    /**
     * @param mixed $dt_transferencia
     */
    public function setDt_transferencia($dt_transferencia)
    {
        $this->dt_transferencia = $dt_transferencia;
    }

    /**
     * @return mixed
     */
    public function getId_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    /**
     * @param mixed $id_matriculaorigem
     */
    public function setId_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
    }

    /**
     * @return mixed
     */
    public function getSt_projetoorigem()
    {
        return $this->st_projetoorigem;
    }

    /**
     * @param mixed $st_projetoorigem
     */
    public function setSt_projetoorigem($st_projetoorigem)
    {
        $this->st_projetoorigem = $st_projetoorigem;
    }

    /**
     * @return mixed
     */
    public function getId_matriculadestino()
    {
        return $this->id_matriculadestino;
    }

    /**
     * @param mixed $id_matriculadestino
     */
    public function setId_matriculadestino($id_matriculadestino)
    {
        $this->id_matriculadestino = $id_matriculadestino;
    }

    /**
     * @return mixed
     */
    public function getSt_projetodestino()
    {
        return $this->st_projetodestino;
    }

    /**
     * @param mixed $st_projetodestino
     */
    public function setSt_projetodestino($st_projetodestino)
    {
        $this->st_projetodestino = $st_projetodestino;
    }

    /**
     * @return mixed
     */
    public function getNu_cargahorariadestino()
    {
        return $this->nu_cargahorariadestino;
    }

    /**
     * @param mixed $nu_cargahorariadestino
     */
    public function setNu_cargahorariadestino($nu_cargahorariadestino)
    {
        $this->nu_cargahorariadestino = $nu_cargahorariadestino;
    }

    /**
     * @return mixed
     */
    public function getNu_valorliquidotransferencia()
    {
        return $this->nu_valorliquidotransferencia;
    }

    /**
     * @param mixed $nu_valorliquidotransferencia
     */
    public function setNu_valorliquidotransferencia($nu_valorliquidotransferencia)
    {
        $this->nu_valorliquidotransferencia = $nu_valorliquidotransferencia;
    }

    /**
     * @return mixed
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param mixed $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }


}

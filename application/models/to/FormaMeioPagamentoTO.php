<?php
/**
 * Classe para encapsular os dados de relacionamento entre forma e meio de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FormaMeioPagamentoTO extends Ead1_TO_Dinamico{

	/**
	 * Id da forma de pagamento.
	 * @var int
	 */
	public $id_formapagamento;
	
	/**
	 * Id do meio de pagamento.
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * Id do tipo de divisão financeira.
	 * @var int
	 */
	public $id_tipodivisaofinanceira;
	
	/**
	 * @return int
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodivisaofinanceira() {
		return $this->id_tipodivisaofinanceira;
	}
	
	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param int $id_tipodivisaofinanceira
	 */
	public function setId_tipodivisaofinanceira($id_tipodivisaofinanceira) {
		$this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
	}

}

?>
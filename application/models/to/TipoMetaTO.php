<?php
/**
 * Classe para encapsular os dados de tipo de meta
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class TipoMetaTO extends Ead1_TO_Dinamico {
	
	public $id_tipometa;
	public $st_tipometa;
	
	/**
	 * @return unknown
	 */
	public function getId_tipometa() {
		return $this->id_tipometa;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipometa() {
		return $this->st_tipometa;
	}
	
	/**
	 * @param unknown_type $id_tipometa
	 */
	public function setId_tipometa($id_tipometa) {
		$this->id_tipometa = $id_tipometa;
	}
	
	/**
	 * @param unknown_type $st_tipometa
	 */
	public function setSt_tipometa($st_tipometa) {
		$this->st_tipometa = $st_tipometa;
	}
}
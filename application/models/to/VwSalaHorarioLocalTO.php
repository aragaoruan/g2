<?php
/**
 * Classe para encapsular os dados de view de VwSalaDisciplinaTurma.
 * 
 * @package models
 * @subpackage to
 */
class VwSalaHorarioLocalTO extends Ead1_TO_Dinamico{
	
	public $id_saladeaula;
	public $id_horarioaula;
	public $id_localaula;
	public $st_horarioaula;
	public $st_localaula;
	
	
	/**
	 * @return the $st_horarioaula
	 */
	public function getSt_horarioaula() {
		return $this->st_horarioaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $st_horarioaula
	 */
	public function setSt_horarioaula($st_horarioaula) {
		$this->st_horarioaula = $st_horarioaula;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $id_localaula
	 */
	public function getId_localaula() {
		return $this->id_localaula;
	}

	/**
	 * @return the $st_localaula
	 */
	public function getSt_localaula() {
		return $this->st_localaula;
	}

	/**
	 * @param field_type $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param field_type $id_localaula
	 */
	public function setId_localaula($id_localaula) {
		$this->id_localaula = $id_localaula;
	}

	/**
	 * @param field_type $st_localaula
	 */
	public function setSt_localaula($st_localaula) {
		$this->st_localaula = $st_localaula;
	}


	
}
<?php
/**
 * Classe para encapsular os dados da view de turno com Horario da Aula.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 13/12/2010
 * 
 * @package models
 * @subpackage to
 */
class VwTurnoHorarioAulaTO extends Ead1_TO_Dinamico {
	
	/**
	 * id do horário de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Nome do horário de aula.
	 * @var string
	 */
	public $st_horarioaula;
	
	/**
	 * Nome do Turno.
	 * @var string
	 */
	public $st_turno;
	
	/**
	 * Hora de início da aula.
	 * @var string
	 */
	public $hr_inicio;
	
	/**
	 * Horário de fim da aula.
	 * @var string
	 */
	public $hr_fim;
	
	/**
	 * Id do turno.
	 * @var int
	 */
	public $id_turno;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do dia da semana.
	 * @var int
	 */
	public $id_diasemana;
	
	/**
	 * Contem o Arraynd
	 * @var array 
	 */
	public $arr_diasemana;
	
	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $st_horarioaula
	 */
	public function getSt_horarioaula() {
		return $this->st_horarioaula;
	}

	/**
	 * @return the $st_turno
	 */
	public function getSt_turno() {
		return $this->st_turno;
	}

	/**
	 * @return the $hr_inicio
	 */
	public function getHr_inicio() {
		return $this->hr_inicio;
	}

	/**
	 * @return the $hr_fim
	 */
	public function getHr_fim() {
		return $this->hr_fim;
	}

	/**
	 * @return the $id_turno
	 */
	public function getId_turno() {
		return $this->id_turno;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_diasemana
	 */
	public function getId_diasemana() {
		return $this->id_diasemana;
	}

	/**
	 * @return the $arr_diasemana
	 */
	public function getArr_diasemana() {
		return $this->arr_diasemana;
	}

	/**
	 * @param $id_horarioaula the $id_horarioaula to set
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param $st_horarioaula the $st_horarioaula to set
	 */
	public function setSt_horarioaula($st_horarioaula) {
		$this->st_horarioaula = $st_horarioaula;
	}

	/**
	 * @param $st_turno the $st_turno to set
	 */
	public function setSt_turno($st_turno) {
		$this->st_turno = $st_turno;
	}

	/**
	 * @param $hr_inicio the $hr_inicio to set
	 */
	public function setHr_inicio($hr_inicio) {
		$this->hr_inicio = $hr_inicio;
	}

	/**
	 * @param $hr_fim the $hr_fim to set
	 */
	public function setHr_fim($hr_fim) {
		$this->hr_fim = $hr_fim;
	}

	/**
	 * @param $id_turno the $id_turno to set
	 */
	public function setId_turno($id_turno) {
		$this->id_turno = $id_turno;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_diasemana the $id_diasemana to set
	 */
	public function setId_diasemana($id_diasemana) {
		$this->id_diasemana = $id_diasemana;
	}

	/**
	 * @param $arr_diasemana the $arr_diasemana to set
	 */
	public function setArr_diasemana($arr_diasemana) {
		$this->arr_diasemana  = $arr_diasemana;
	}


	
}
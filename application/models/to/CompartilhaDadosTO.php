<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class CompartilhaDadosTO extends Ead1_TO_Dinamico{

	public $id_entidadeorigem;
	public $id_entidadedestino;
	public $id_tipodados;


	/**
	 * @return the $id_entidadeorigem
	 */
	public function getId_entidadeorigem(){ 
		return $this->id_entidadeorigem;
	}

	/**
	 * @return the $id_entidadedestino
	 */
	public function getId_entidadedestino(){ 
		return $this->id_entidadedestino;
	}

	/**
	 * @return the $id_tipodados
	 */
	public function getId_tipodados(){ 
		return $this->id_tipodados;
	}


	/**
	 * @param field_type $id_entidadeorigem
	 */
	public function setId_entidadeorigem($id_entidadeorigem){ 
		$this->id_entidadeorigem = $id_entidadeorigem;
	}

	/**
	 * @param field_type $id_entidadedestino
	 */
	public function setId_entidadedestino($id_entidadedestino){ 
		$this->id_entidadedestino = $id_entidadedestino;
	}

	/**
	 * @param field_type $id_tipodados
	 */
	public function setId_tipodados($id_tipodados){ 
		$this->id_tipodados = $id_tipodados;
	}

}
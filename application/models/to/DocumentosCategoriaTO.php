<?php
/**
 * To da tabela de DocumentosCategoriaTO
 * @author edermariano
 * @package models
 * @subpackage to
 */ 
class DocumentosCategoriaTO extends Ead1_TO_Dinamico{
	
	public $st_documentoscategoria;
	public $id_documentoscategoria;
	public $bl_ativo;
	
	/**
	 * @return the $st_documentoscategoria
	 */
	public function getSt_documentoscategoria() {
		return $this->st_documentoscategoria;
	}

	/**
	 * @return the $id_documentoscategoria
	 */
	public function getId_documentoscategoria() {
		return $this->id_documentoscategoria;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $st_documentoscategoria the $st_documentoscategoria to set
	 */
	public function setSt_documentoscategoria($st_documentoscategoria) {
		$this->st_documentoscategoria = $st_documentoscategoria;
	}

	/**
	 * @param $id_documentoscategoria the $id_documentoscategoria to set
	 */
	public function setId_documentoscategoria($id_documentoscategoria) {
		$this->id_documentoscategoria = $id_documentoscategoria;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
}
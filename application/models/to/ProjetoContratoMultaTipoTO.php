<?php
/**
 * Classe para encapsular os dados de tipo de multa do contrato do projeto pedagógico, quando este contrato é cancelado.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoContratoMultaTipoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de multa do contrato do projeto pedagógico, quando o contrato é cancelado.
	 * @var int
	 */
	public $id_projetocontratomultatipo;
	
	/**
	 * Tipo de multa do contrato do projeto pedagógico, quando o contrato é cancelado.
	 * @var string
	 */
	public $st_projetocontratomultatipo;
	
	/**
	 * Descrição do tipo de multa do contrato do projeto pedagógico.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	/**
	 * @return int
	 */
	public function getId_projetocontratomultatipo() {
		return $this->id_projetocontratomultatipo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetocontratomultatipo() {
		return $this->st_projetocontratomultatipo;
	}
	
	/**
	 * @param int $id_projetocontratomultatipo
	 */
	public function setId_projetocontratomultatipo($id_projetocontratomultatipo) {
		$this->id_projetocontratomultatipo = $id_projetocontratomultatipo;
	}
	
	/**
	 * @param string $st_projetocontratomultatipo
	 */
	public function setSt_projetocontratomultatipo($st_projetocontratomultatipo) {
		$this->st_projetocontratomultatipo = $st_projetocontratomultatipo;
	}

}

?>
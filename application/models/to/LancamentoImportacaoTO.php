<?php
class LancamentoImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_lancamentoimportacao;
	public $id_lancamento;
	public $id_matriculaimportacao;
	public $nu_codmatriculaorigem;
	public $nu_valor;
	public $dt_vencimento;
	public $dt_quitado;
	public $nu_codtipolancamentoorigem;
	public $id_sistemaimportacao;
	public $nu_desconto;
	public $nu_juros;
	public $nu_multa;
	public $nu_correcaomonetaria;
	
	/**
	 * @return the $id_lancamentoimportacao
	 */
	public function getId_lancamentoimportacao() {
		return $this->id_lancamentoimportacao;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_matriculaimportacao
	 */
	public function getId_matriculaimportacao() {
		return $this->id_matriculaimportacao;
	}

	/**
	 * @return the $nu_codmatriculaorigem
	 */
	public function getNu_codmatriculaorigem() {
		return $this->nu_codmatriculaorigem;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}
	
	/**
	 * @return the $nu_desconto
	 */
	public function getNu_desconto() {
		return $this->nu_desconto;
	}
	
	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros() {
		return $this->nu_juros;
	}
	
	/**
	 * @return the $nu_multa
	 */
	public function getNu_multa() {
		return $this->nu_multa;
	}
	
	/**
	 * @return the $nu_correcaomonetaria
	 */
	public function getNu_correcaomonetaria() {
		return $this->nu_correcaomonetaria;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $dt_quitado
	 */
	public function getDt_quitado() {
		return $this->dt_quitado;
	}

	/**
	 * @return the $nu_codtipolancamentoorigem
	 */
	public function getNu_codtipolancamentoorigem() {
		return $this->nu_codtipolancamentoorigem;
	}
	
	/**
	 * @return the $nu_codtipolancamentoorigem
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @param field_type $id_lancamentoimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}
	
	/**
	 * @param field_type $id_lancamentoimportacao
	 */
	public function setId_lancamentoimportacao($id_lancamentoimportacao) {
		$this->id_lancamentoimportacao = $id_lancamentoimportacao;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $id_matriculaimportacao
	 */
	public function setId_matriculaimportacao($id_matriculaimportacao) {
		$this->id_matriculaimportacao = $id_matriculaimportacao;
	}

	/**
	 * @param field_type $nu_codmatriculaorigem
	 */
	public function setNu_codmatriculaorigem($nu_codmatriculaorigem) {
		$this->nu_codmatriculaorigem = $nu_codmatriculaorigem;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}
	
	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_desconto($nu_desconto) {
		$this->nu_desconto = $nu_desconto;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_multa($nu_multa) {
		$this->nu_multa = $nu_multa;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_correcaomonetaria($nu_correcaomonetaria) {
		$this->nu_correcaomonetaria = $nu_correcaomonetaria;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $dt_quitado
	 */
	public function setDt_quitado($dt_quitado) {
		$this->dt_quitado = $dt_quitado;
	}

	/**
	 * @param field_type $nu_codtipolancamentoorigem
	 */
	public function setNu_codtipolancamentoorigem($nu_codtipolancamentoorigem) {
		$this->nu_codtipolancamentoorigem = $nu_codtipolancamentoorigem;
	}

	
}
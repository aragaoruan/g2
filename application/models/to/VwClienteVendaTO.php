<?php
/**
 * Classe para encapsular os dados da view de cliente venda
 * @author dimassulz
 * @package models
 * @subpackage to 
 */
class VwClienteVendaTO extends Ead1_TO_Dinamico{

	public $id_textosistemarecibo;
	public $id_reciboconsolidado;
	
	/**
	 * Atributo que contém a evolução do contrato 
	 * @var int
	 */
	public $id_evolucaocontrato;
	
	/**
	 * Atributo que contém a situação do contrato
	 * @var int
	 */
	public $id_situacaocontrato;
	
	/**
	 * Atributo que contém o nome completo do Usuario
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Atributo que contém o CPF do Usuario
	 * @var string
	 */
	public $st_cpf;
	
	/**
	 * Atributo que contém o id do Usuario
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contem o id da venda
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * Contem o id da evolucao
	 * @var int
	 */
	public $id_evolucao;
	
	/**
	 * Contem a descrição da evolucao
	 * @var int
	 */
	public $st_evolucao;
	
	/**
	 * 
	 * Contem o valor liquido
	 * @var float
	 */
	public $nu_valorliquido;
	
	/**
	 * @var float
	 */
	public $nu_valorbruto;
	
	/**
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @var boolean
	 */
	public $bl_contrato;
	
	/**
	 * @return the $id_textosistemarecibo
	 */
	public function getId_textosistemarecibo() {
		return $this->id_textosistemarecibo;
	}

	/**
	 * @return the $id_reciboconsolidado
	 */
	public function getId_reciboconsolidado() {
		return $this->id_reciboconsolidado;
	}

	/**
	 * @param field_type $id_textosistemarecibo
	 */
	public function setId_textosistemarecibo($id_textosistemarecibo) {
		$this->id_textosistemarecibo = $id_textosistemarecibo;
	}

	/**
	 * @param field_type $id_reciboconsolidado
	 */
	public function setId_reciboconsolidado($id_reciboconsolidado) {
		$this->id_reciboconsolidado = $id_reciboconsolidado;
	}

	/**
	 * @return int
	 */
	public function getBl_contrato() {
		return $this->bl_contrato;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valorbruto() {
		return $this->nu_valorbruto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param float $nu_valorbruto
	 */
	public function setNu_valorbruto($nu_valorbruto) {
		$this->nu_valorbruto = $nu_valorbruto;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param string $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param int $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param int $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}
	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @param float $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}
	/**
	 * @return the $id_evolucaocontrato
	 */
	public function getId_evolucaocontrato() {
		return $this->id_evolucaocontrato;
	}

	/**
	 * @return the $id_situacaocontrato
	 */
	public function getId_situacaocontrato() {
		return $this->id_situacaocontrato;
	}

	/**
	 * @param $id_evolucaocontrato the $id_evolucaocontrato to set
	 */
	public function setId_evolucaocontrato($id_evolucaocontrato) {
		$this->id_evolucaocontrato = $id_evolucaocontrato;
	}

	/**
	 * @param $id_situacaocontrato the $id_situacaocontrato to set
	 */
	public function setId_situacaocontrato($id_situacaocontrato) {
		$this->id_situacaocontrato = $id_situacaocontrato;
	}
	
	/**
	 * @param $bl_contrato the $bl_contrato to set
	 */
	public function setBl_contrato($bl_contrato) {
		$this->bl_contrato = $bl_contrato;
	}



	
	
	
	
}
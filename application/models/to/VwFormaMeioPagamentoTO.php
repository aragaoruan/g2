<?php
/**
 * Classe para encapsular da tabela  
 * @author Eder Lamar Mariano 
 * @package models
 * @subpackage to
 */
class VwFormaMeioPagamentoTO extends Ead1_TO_Dinamico{
	
	public $id_formapagamentoaplicacao;     
	public $id_meiopagamento;     
	public $st_meiopagamento;     
	public $st_descricaomeiopagamento;     
	public $id_formapagamento;     
	public $st_formapagamento;     
	public $id_situacao;     
	public $st_descricao;     
	public $nu_entradavalormin;     
	public $nu_entradavalormax;     
	public $nu_valormin;     
	public $nu_valormax;     
	public $nu_valorminparcela;     
	public $dt_cadastro;     
	public $id_usuariocadastro;     
	public $id_tipoformapagamentoparcela;     
	public $id_entidade;     
	public $bl_todosprodutos;
	
	/**
	 * @return the $id_formapagamentoaplicacao
	 */
	public function getId_formapagamentoaplicacao() {
		return $this->id_formapagamentoaplicacao;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $st_meiopagamento
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}

	/**
	 * @return the $st_descricaomeiopagamento
	 */
	public function getSt_descricaomeiopagamento() {
		return $this->st_descricaomeiopagamento;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $nu_entradavalormin
	 */
	public function getNu_entradavalormin() {
		return $this->nu_entradavalormin;
	}

	/**
	 * @return the $nu_entradavalormax
	 */
	public function getNu_entradavalormax() {
		return $this->nu_entradavalormax;
	}

	/**
	 * @return the $nu_valormin
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}

	/**
	 * @return the $nu_valormax
	 */
	public function getNu_valormax() {
		return $this->nu_valormax;
	}

	/**
	 * @return the $nu_valorminparcela
	 */
	public function getNu_valorminparcela() {
		return $this->nu_valorminparcela;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_tipoformapagamentoparcela
	 */
	public function getId_tipoformapagamentoparcela() {
		return $this->id_tipoformapagamentoparcela;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_todosprodutos
	 */
	public function getBl_todosprodutos() {
		return $this->bl_todosprodutos;
	}

	/**
	 * @param $id_formapagamentoaplicacao the $id_formapagamentoaplicacao to set
	 */
	public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao) {
		$this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
	}

	/**
	 * @param $id_meiopagamento the $id_meiopagamento to set
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param $st_meiopagamento the $st_meiopagamento to set
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}

	/**
	 * @param $st_descricaomeiopagamento the $st_descricaomeiopagamento to set
	 */
	public function setSt_descricaomeiopagamento($st_descricaomeiopagamento) {
		$this->st_descricaomeiopagamento = $st_descricaomeiopagamento;
	}

	/**
	 * @param $id_formapagamento the $id_formapagamento to set
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param $st_formapagamento the $st_formapagamento to set
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $nu_entradavalormin the $nu_entradavalormin to set
	 */
	public function setNu_entradavalormin($nu_entradavalormin) {
		$this->nu_entradavalormin = $nu_entradavalormin;
	}

	/**
	 * @param $nu_entradavalormax the $nu_entradavalormax to set
	 */
	public function setNu_entradavalormax($nu_entradavalormax) {
		$this->nu_entradavalormax = $nu_entradavalormax;
	}

	/**
	 * @param $nu_valormin the $nu_valormin to set
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}

	/**
	 * @param $nu_valormax the $nu_valormax to set
	 */
	public function setNu_valormax($nu_valormax) {
		$this->nu_valormax = $nu_valormax;
	}

	/**
	 * @param $nu_valorminparcela the $nu_valorminparcela to set
	 */
	public function setNu_valorminparcela($nu_valorminparcela) {
		$this->nu_valorminparcela = $nu_valorminparcela;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $id_tipoformapagamentoparcela the $id_tipoformapagamentoparcela to set
	 */
	public function setId_tipoformapagamentoparcela($id_tipoformapagamentoparcela) {
		$this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $bl_todosprodutos the $bl_todosprodutos to set
	 */
	public function setBl_todosprodutos($bl_todosprodutos) {
		$this->bl_todosprodutos = $bl_todosprodutos;
	}

}
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TramiteVendaTO extends Ead1_TO_Dinamico{
	
	const TIPO_TRAMITE_SITUACAO = 5;
	const TIPO_TRAMITE_EVOLUCAO = 6;
	const TIPO_TRAMITE_LANCAMENTO = 7;

	public $id_tramitevenda;
	public $id_venda;
	public $id_tramite;


	/**
	 * @return the $id_tramitevenda
	 */
	public function getId_tramitevenda(){ 
		return $this->id_tramitevenda;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda(){ 
		return $this->id_venda;
	}

	/**
	 * @return the $id_tramite
	 */
	public function getId_tramite(){ 
		return $this->id_tramite;
	}


	/**
	 * @param field_type $id_tramitevenda
	 */
	public function setId_tramitevenda($id_tramitevenda){ 
		$this->id_tramitevenda = $id_tramitevenda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda){ 
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_tramite
	 */
	public function setId_tramite($id_tramite){ 
		$this->id_tramite = $id_tramite;
	}

}
<?php
/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */

class EnvioMensagemTO extends Ead1_TO_Dinamico{

	const ENVIADO = 22;
	const NAO_ENVIADO = 23;
	const ERRO_AO_ENVIAR = 24;

	public $id_enviomensagem;
	public $id_emailconfig;
	public $id_mensagem;
	public $id_tipoenvio;
	public $id_evolucao;
	public $dt_cadastro;
	public $dt_envio;
	public $dt_enviar;
	public $dt_tentativa;
	public $st_retorno;
	public $id_sistema;

	/**
	 * @return mixed
	 */
	public function getid_sistema()
	{
		return $this->id_sistema;
	}

	/**
	 * @param mixed $id_sistema
	 */
	public function setid_sistema($id_sistema)
	{
		$this->id_sistema = $id_sistema;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getst_retorno()
	{
		return $this->st_retorno;
	}

	/**
	 * @param mixed $st_retorno
	 */
	public function setst_retorno($st_retorno)
	{
		$this->st_retorno = $st_retorno;
		return $this;
	}




	/**
	 * @return the $id_enviomensagem
	 */
	public function getId_enviomensagem() {
		return $this->id_enviomensagem;
	}

	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @return the $id_mensagem
	 */
	public function getId_mensagem() {
		return $this->id_mensagem;
	}

	/**
	 * @return the $id_tipoenvio
	 */
	public function getId_tipoenvio() {
		return $this->id_tipoenvio;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $dt_envio
	 */
	public function getDt_envio() {
		return $this->dt_envio;
	}

	/**
	 * @return the $dt_enviar
	 */
	public function getDt_enviar() {
		return $this->dt_enviar;
	}

	/**
	 * @return the $dt_tentativa
	 */
	public function getDt_tentativa() {
		return $this->dt_tentativa;
	}

	/**
	 * @param field_type $id_enviomensagem
	 */
	public function setId_enviomensagem($id_enviomensagem) {
		$this->id_enviomensagem = $id_enviomensagem;
	}

	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @param field_type $id_mensagem
	 */
	public function setId_mensagem($id_mensagem) {
		$this->id_mensagem = $id_mensagem;
	}

	/**
	 * @param field_type $id_tipoenvio
	 */
	public function setId_tipoenvio($id_tipoenvio) {
		$this->id_tipoenvio = $id_tipoenvio;
	}

	/**
     * @param integer $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $dt_envio
	 */
	public function setDt_envio($dt_envio) {
		$this->dt_envio = $dt_envio;
	}

	/**
     * @param  $dt_enviar
	 */
	public function setDt_enviar($dt_enviar) {
		$this->dt_enviar = $dt_enviar;
	}

	/**
	 * @param field_type $dt_tentativa
	 */
	public function setDt_tentativa($dt_tentativa) {
		$this->dt_tentativa = $dt_tentativa;
	}

}

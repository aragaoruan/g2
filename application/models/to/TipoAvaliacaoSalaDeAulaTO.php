<?php
/**
 * Classe para encapsular os dados de tipo de avaliação de sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoAvaliacaoSalaDeAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de avaliação de sala de aula.
	 * @var int
	 */
	public $id_tipoavaliacaosaladeaula;
	
	/**
	 * Tipo de avaliação de sala de aula.
	 * @var string
	 */
	public $st_tipoavaliacaosaladeaula;
	
	/**
	 * @return int
	 */
	public function getId_tipoavaliacaosaladeaula() {
		return $this->id_tipoavaliacaosaladeaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoavaliacaosaladeaula() {
		return $this->st_tipoavaliacaosaladeaula;
	}
	
	/**
	 * @param int $id_tipoavaliacaosaladeaula
	 */
	public function setId_tipoavaliacaosaladeaula($id_tipoavaliacaosaladeaula) {
		$this->id_tipoavaliacaosaladeaula = $id_tipoavaliacaosaladeaula;
	}
	
	/**
	 * @param string $st_tipoavaliacaosaladeaula
	 */
	public function setSt_tipoavaliacaosaladeaula($st_tipoavaliacaosaladeaula) {
		$this->st_tipoavaliacaosaladeaula = $st_tipoavaliacaosaladeaula;
	}

}

?>
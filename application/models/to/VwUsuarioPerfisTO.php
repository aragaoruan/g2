<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwUsuarioPerfisTO extends Ead1_TO_Dinamico{

	public $st_nomecompleto;
	public $bl_ativo;
	public $id_perfil;
	public $id_usuario;


	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto(){ 
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil(){ 
		return $this->id_perfil;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}


	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto){ 
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_perfil
	 */
	public function setId_perfil($id_perfil){ 
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

}
<?php
/**
 * Classe para encapsular os dados de relação entre telefone e contatos da entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosTelefoneEntidadeContatoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do telefone.
	 * @var int
	 */
	public $id_telefone;
	
	/**
	 * Contém id do contato de entidade.
	 * @var int
	 */
	public $id_contatoentidade;
	
	/**
	 * Define se o telefone é padrão ou não.
	 * @var Boolean
	 */
	public $bl_padrao;
	
	/**
	 * @return Boolean
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @return int
	 */
	public function getId_contatoentidade() {
		return $this->id_contatoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_telefone() {
		return $this->id_telefone;
	}
	
	/**
	 * @param Boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	
	/**
	 * @param int $id_contatoentidade
	 */
	public function setId_contatoentidade($id_contatoentidade) {
		$this->id_contatoentidade = $id_contatoentidade;
	}
	
	/**
	 * @param int $id_telefone
	 */
	public function setId_telefone($id_telefone) {
		$this->id_telefone = $id_telefone;
	}

}

?>
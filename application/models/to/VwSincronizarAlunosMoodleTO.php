<?php
/**
 * Classe para encapsular da vw
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwSincronizarAlunosMoodleTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	public $id_alocacao;
	public $st_codsistemacurso;
	public $id_saladeaula;
	public $id_entidade;
	public $st_nomecompleto;
	public $st_saladeaula;
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @param field_type $id_alocacao
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @return the $st_codsistemacurso
	 */
	public function getSt_codsistemacurso() {
		return $this->st_codsistemacurso;
	}

	/**
	 * @param field_type $st_codsistemacurso
	 */
	public function setSt_codsistemacurso($st_codsistemacurso) {
		$this->st_codsistemacurso = $st_codsistemacurso;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}


	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}
	
	
	
	
	
}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwFundamentoMatriculaTO extends Ead1_TO_Dinamico{

	public $id_matricula;
	public $id_fundamentolegal;
	public $nu_numero;
	public $dt_publicacao;
	public $st_orgaoexped;
	public $dt_vigencia;
	public $id_situacao;
	public $dt_cadastro;
	public $id_usuariocadastro;
	public $id_tipofundamentolegal;
	public $id_entidade;
	public $bl_ativo;
	public $id_nivelensino;


	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_fundamentolegal
	 */
	public function getId_fundamentolegal(){ 
		return $this->id_fundamentolegal;
	}

	/**
	 * @return the $nu_numero
	 */
	public function getNu_numero(){ 
		return $this->nu_numero;
	}

	/**
	 * @return the $dt_publicacao
	 */
	public function getDt_publicacao(){ 
		return $this->dt_publicacao;
	}

	/**
	 * @return the $st_orgaoexped
	 */
	public function getSt_orgaoexped(){ 
		return $this->st_orgaoexped;
	}

	/**
	 * @return the $dt_vigencia
	 */
	public function getDt_vigencia(){ 
		return $this->dt_vigencia;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_tipofundamentolegal
	 */
	public function getId_tipofundamentolegal(){ 
		return $this->id_tipofundamentolegal;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino(){ 
		return $this->id_nivelensino;
	}


	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_fundamentolegal
	 */
	public function setId_fundamentolegal($id_fundamentolegal){ 
		$this->id_fundamentolegal = $id_fundamentolegal;
	}

	/**
	 * @param field_type $nu_numero
	 */
	public function setNu_numero($nu_numero){ 
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param field_type $dt_publicacao
	 */
	public function setDt_publicacao($dt_publicacao){ 
		$this->dt_publicacao = $dt_publicacao;
	}

	/**
	 * @param field_type $st_orgaoexped
	 */
	public function setSt_orgaoexped($st_orgaoexped){ 
		$this->st_orgaoexped = $st_orgaoexped;
	}

	/**
	 * @param field_type $dt_vigencia
	 */
	public function setDt_vigencia($dt_vigencia){ 
		$this->dt_vigencia = $dt_vigencia;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_tipofundamentolegal
	 */
	public function setId_tipofundamentolegal($id_tipofundamentolegal){ 
		$this->id_tipofundamentolegal = $id_tipofundamentolegal;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino){ 
		$this->id_nivelensino = $id_nivelensino;
	}

}
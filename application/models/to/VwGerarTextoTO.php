<?php
/**
 * Classe para encapsular dados da view de vw_gerartexto
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage to
 */
class VwGerarTextoTO extends Ead1_TO_Dinamico{
	
	public $id_textosistema;
	public $st_texto;
	public $id_textocategoria;
	public $st_textocategoria;
	public $st_metodo;
	public $st_textovariaveis;
	public $st_camposubstituir;
	public $st_mascara;
	
	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @return the $st_texto
	 */
	public function getSt_texto() {
		return $this->st_texto;
	}

	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria() {
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_textocategoria
	 */
	public function getSt_textocategoria() {
		return $this->st_textocategoria;
	}

	/**
	 * @return the $st_metodo
	 */
	public function getSt_metodo() {
		return $this->st_metodo;
	}

	/**
	 * @return the $st_textovariaveis
	 */
	public function getSt_textovariaveis() {
		return $this->st_textovariaveis;
	}

	/**
	 * @return the $st_camposubstituir
	 */
	public function getSt_camposubstituir() {
		return $this->st_camposubstituir;
	}

	/**
	 * @return the $st_mascara
	 */
	public function getSt_mascara() {
		return $this->st_mascara;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $st_texto
	 */
	public function setSt_texto($st_texto) {
		$this->st_texto = $st_texto;
	}

	/**
	 * @param field_type $id_textocategoria
	 */
	public function setId_textocategoria($id_textocategoria) {
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param field_type $st_textocategoria
	 */
	public function setSt_textocategoria($st_textocategoria) {
		$this->st_textocategoria = $st_textocategoria;
	}

	/**
	 * @param field_type $st_metodo
	 */
	public function setSt_metodo($st_metodo) {
		$this->st_metodo = $st_metodo;
	}

	/**
	 * @param field_type $st_textovariaveis
	 */
	public function setSt_textovariaveis($st_textovariaveis) {
		$this->st_textovariaveis = $st_textovariaveis;
	}

	/**
	 * @param field_type $st_camposubstituir
	 */
	public function setSt_camposubstituir($st_camposubstituir) {
		$this->st_camposubstituir = $st_camposubstituir;
	}

	/**
	 * @param field_type $st_mascara
	 */
	public function setSt_mascara($st_mascara) {
		$this->st_mascara = $st_mascara;
	}


}
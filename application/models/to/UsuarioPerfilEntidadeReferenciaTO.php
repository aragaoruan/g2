<?php

/**
 * Classe para encapsular os dados que apontam a referência da divisão pedagógica de uma pessoa com um perfil.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-24-01
 * @package models
 * @subpackage to
 */
class UsuarioPerfilEntidadeReferenciaTO extends Ead1_TO_Dinamico
{

    /**
     * Id.
     * @var int
     */
    public $id_perfilreferencia;

    /**
     * Id do usuário.
     * @var int
     */
    public $id_usuario;

    /**
     * Id do perfil.
     * @var int
     */
    public $id_perfil;

    /**
     * Id da entidade.
     * @var int
     */
    public $id_entidade;

    /**
     * Id da área de conhecimento.
     * @var int
     */
    public $id_areaconhecimento;

    /**
     * Id do projeto pedagogico.
     * @var int
     */
    public $id_projetopedagogico;

    /**
     * Id de sala de aula.
     * @var int
     */
    public $id_saladeaula;

    /**
     * Id da disciplina.
     * @var int
     */
    public $id_disciplina;

    /**
     * Exclusão lógica.
     * @var Boolean
     */
    public $bl_ativo;
    public $bl_titular;
    public $nu_porcentagem;
    public $id_livro;
    public $bl_autor;
    public $dt_inicio;
    public $dt_fim;
    public $id_titulacao;
    public $bl_desativarmoodle;

    public function getId_livro ()
    {
        return $this->id_livro;
    }

    public function setId_livro ($id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    public function getBl_autor ()
    {
        return $this->bl_autor;
    }

    public function setBl_autor ($bl_autor)
    {
        $this->bl_autor = $bl_autor;
        return $this;
    }

    /**
     * @return the $nu_porcentagem
     */
    public function getNu_porcentagem ()
    {
        return $this->nu_porcentagem;
    }

    /**
     * @param field_type $nu_porcentagem
     */
    public function setNu_porcentagem ($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
        return $this;
    }

    /**
     * @return the $id_perfilreferencia
     */
    public function getId_perfilReferencia ()
    {
        return $this->id_perfilreferencia;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $id_perfil
     */
    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $id_areaconhecimento
     */
    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return the $id_saladeaula
     */
    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $id_perfilreferencia the $id_perfilreferencia to set
     */
    public function setId_perfilReferencia ($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
    }

    /**
     * @param $id_usuario the $id_usuario to set
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param $id_perfil the $id_perfil to set
     */
    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @param $id_entidade the $id_entidade to set
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param $id_areaconhecimento the $id_areaconhecimento to set
     */
    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @param $id_projetopedagogico the $id_projetopedagogico to set
     */
    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param $id_saladeaula the $id_saladeaula to set
     */
    public function setId_saladeaula ($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @param $id_disciplina the $id_disciplina to set
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param $bl_ativo the $bl_ativo to set
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return the $bl_titular
     */
    public function getBl_titular ()
    {
        return $this->bl_titular;
    }

    /**
     * @param field_type $bl_titular
     */
    public function setBl_titular ($bl_titular)
    {
        $this->bl_titular = $bl_titular;
    }

    /**
     * @return mixed
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param mixed $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return mixed
     */
    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param mixed $dt_fim
     */
    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
    }

    /**
     * @return mixed
     */
    public function getId_titulacao()
    {
        return $this->id_titulacao;
    }

    /**
     * @param mixed $id_titulacao
     */
    public function setId_titulacao($id_titulacao)
    {
        $this->id_titulacao = $id_titulacao;
    }

    /**
     * @return mixed
     */
    public function getBl_desativarmoodle()
    {
        return $this->bl_desativarmoodle;
    }

    /**
     * @param mixed $bl_desativarmoodle
     */
    public function setBl_desativarmoodle($bl_desativarmoodle)
    {
        $this->bl_desativarmoodle = $bl_desativarmoodle;
    }


}

?>
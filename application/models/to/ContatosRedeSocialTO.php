<?php
/**
 * Classe para encapsular os dados de rede social.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosRedeSocialTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id da rede social.
	 * @var int
	 */
	public $id_redesocial;
	
	/**
	 * Atributo que contém o tipo da rede social.
	 * @var int
	 */
	public $id_tiporedesocial;
	
	/**
	 * Id do usuário na rede social.
	 * @var string
	 */
	public $st_id;
	
	/**
	 * @return int
	 */
	public function getId_redesocial() {
		return $this->id_redesocial;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiporedesocial() {
		return $this->id_tiporedesocial;
	}
	
	/**
	 * @return string
	 */
	public function getSt_id() {
		return $this->st_id;
	}
	
	/**
	 * @param int $id_redesocial
	 */
	public function setId_redesocial($id_redesocial) {
		$this->id_redesocial = $id_redesocial;
	}
	
	/**
	 * @param int $id_tiporedesocial
	 */
	public function setId_tiporedesocial($id_tiporedesocial) {
		$this->id_tiporedesocial = $id_tiporedesocial;
	}
	
	/**
	 * @param string $st_id
	 */
	public function setSt_id($st_id) {
		$this->st_id = $st_id;
	}

}

?>
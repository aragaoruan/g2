<?php
/**
 * Classe para encapsular os dados de TramiteOcorrencia.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class TramiteOcorrenciaTO extends Ead1_TO_Dinamico {

	public $id_tramiteocorrencia;
	public $id_tramite;
	public $id_ocorrencia;
	public $bl_interessado;
	
	/**
	 * @return the $id_tramiteocorrencia
	 */
	public function getId_tramiteocorrencia() {
		return $this->id_tramiteocorrencia;
	}

	/**
	 * @param field_type $id_tramiteocorrencia
	 */
	public function setId_tramiteocorrencia($id_tramiteocorrencia) {
		$this->id_tramiteocorrencia = $id_tramiteocorrencia;
	}

	/**
	 * @return the $id_tramite
	 */
	public function getId_tramite() {
		return $this->id_tramite;
	}

	/**
	 * @param field_type $id_tramite
	 */
	public function setId_tramite($id_tramite) {
		$this->id_tramite = $id_tramite;
	}

	/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

	/**
	 * @param field_type $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}
	/**
	 * @return the $bl_interessado
	 */
	public function getBl_interessado() {
		return $this->bl_interessado;
	}

	/**
	 * @param field_type $bl_interessado
	 */
	public function setBl_interessado($bl_interessado) {
		$this->bl_interessado = $bl_interessado;
	}


	
	
	
}

?>
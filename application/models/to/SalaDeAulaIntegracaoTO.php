<?php

/**
 * Classe para encapsular os dados de integração da sala de aula.
 * @author Eder Lamar Mariano
 * @package models
 * @subpackage to
 */
class SalaDeAulaIntegracaoTO extends Ead1_TO_Dinamico
{


    const SEMSALAEXTERNA = 'SEMSALA';
    const SALABLACKBOARD = 'AGUARDANDOBLACKBOARD';
    const SALABLACKBOARDAGUARDANDO = 'AGUARDANDOVERIFICACAO';

    public $id_saladeaulaintegracao;
    public $id_saladeaula;
    public $id_sistema;
    public $id_usuariocadastro;
    public $st_codsistemacurso;
    public $st_codsistemasala;
    public $st_codsistemareferencia;
    public $dt_cadastro;
    public $bl_conteudo;
    public $st_retornows;
    public $id_disciplinaintegracao;

    /**
     * @return mixed
     */
    public function getid_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param mixed $id_saladeaulaintegracao
     * @return SalaDeAulaIntegracaoTO
     */
    public function setid_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param mixed $id_saladeaula
     * @return SalaDeAulaIntegracaoTO
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     * @return SalaDeAulaIntegracaoTO
     */
    public function setid_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $id_usuariocadastro
     * @return SalaDeAulaIntegracaoTO
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param mixed $st_codsistemacurso
     * @return SalaDeAulaIntegracaoTO
     */
    public function setst_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_codsistemasala()
    {
        return $this->st_codsistemasala;
    }

    /**
     * @param mixed $st_codsistemasala
     * @return SalaDeAulaIntegracaoTO
     */
    public function setst_codsistemasala($st_codsistemasala)
    {
        $this->st_codsistemasala = $st_codsistemasala;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_codsistemareferencia()
    {
        return $this->st_codsistemareferencia;
    }

    /**
     * @param mixed $st_codsistemareferencia
     * @return SalaDeAulaIntegracaoTO
     */
    public function setst_codsistemareferencia($st_codsistemareferencia)
    {
        $this->st_codsistemareferencia = $st_codsistemareferencia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return SalaDeAulaIntegracaoTO
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_conteudo()
    {
        return $this->bl_conteudo;
    }

    /**
     * @param mixed $bl_conteudo
     * @return SalaDeAulaIntegracaoTO
     */
    public function setbl_conteudo($bl_conteudo)
    {
        $this->bl_conteudo = $bl_conteudo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_retornows()
    {
        return $this->st_retornows;
    }

    /**
     * @param mixed $st_retornows
     * @return SalaDeAulaIntegracaoTO
     */
    public function setst_retornows($st_retornows)
    {
        $this->st_retornows = $st_retornows;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    /**
     * @param mixed $id_disciplinaintegracao
     * @return SalaDeAulaIntegracaoTO
     */
    public function setid_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
        return $this;
    }



}

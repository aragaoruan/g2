<?php
/**
 * Classe para encapsular os dados completos de comissão
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class ComissaoCompletaTO extends Ead1_TO_Dinamico {
	
	public $id_comissaoentidade;
	public $id_entidade;
	public $id_funcao;
	public $id_tipocalculocom;
	public $st_funcao;
	public $st_tipofuncao;
	public $id_tipofuncao;
	
	/**
	 * @return unknown
	 */
	public function getId_comissaoentidade() {
		return $this->id_comissaoentidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipofuncao() {
		return $this->id_tipofuncao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipocalculocom() {
		return $this->id_tipocalculocom;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipofuncao() {
		return $this->id_tipocalculocom;
	}
	
	/**
	 * @param unknown_type $id_comissaoentidade
	 */
	public function setId_comissaoentidade($id_comissaoentidade) {
		$this->id_comissaoentidade = $id_comissaoentidade;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $id_tipocalculocom
	 */
	public function setId_tipocalculocom($id_tipocalculocom) {
		$this->id_tipocalculocom = $id_tipocalculocom;
	}
	
	/**
	 * @param unknown_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @param unknown_type $id_tipofuncao
	 */
	public function setId_tipofuncao($id_tipofuncao) {
		$this->id_tipofuncao = $id_tipofuncao;
	}
	
	/**
	 * @param unknown_type $st_tipofuncao
	 */
	public function setSt_tipofuncao($st_tipofuncao) {
		$this->st_tipofuncao = $st_tipofuncao;
	}
	
}
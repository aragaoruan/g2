<?php


class VwAvaliacaoAlunoHistoricoTO extends Ead1_TO_Dinamico {
	
	
	public $id_matricula;
	public $id_modulo;
	public $st_tituloexibicaomodulo;
	public $id_disciplina;
	public $st_tituloexibicaodisciplina;
	public $st_nota;
	public $id_avaliacaoagendamento;
	public $id_avaliacao;
	public $st_avaliacao;
	public $id_situacao;
	public $st_situacao;
	public $id_tipoprova;
	public $id_avaliacaoconjuntoreferencia;
	public $id_evolucao;
	public $st_evolucao;
	public $nu_notamax;
	public $id_tipocalculoavaliacao;
	public $id_tipoavaliacao;
	public $st_tipoavaliacao;
	public $id_avaliacaorecupera;
	public $nu_notamaxima;
	public $nu_percentualaprovacao;
	public $dt_agendamento;
	public $bl_ativo;
	public $dt_avaliacao;
	public $id_saladeaula;
	public $st_codusuario;
	public $id_situacaoavaliacao;
	public $st_situacaoavaliacao;
	public $st_usuariorevisor;
	/**
	 * @return the $st_usuariorevisor
	 */
	public function getSt_usuariorevisor() {
		return $this->st_usuariorevisor;
	}

	/**
	 * @param field_type $st_usuariorevisor
	 */
	public function setSt_usuariorevisor($st_usuariorevisor) {
		$this->st_usuariorevisor = $st_usuariorevisor;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @param field_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @return the $st_tituloexibicaomodulo
	 */
	public function getSt_tituloexibicaomodulo() {
		return $this->st_tituloexibicaomodulo;
	}

	/**
	 * @param field_type $st_tituloexibicaomodulo
	 */
	public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
		$this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @return the $st_tituloexibicaodisciplina
	 */
	public function getSt_tituloexibicaodisciplina() {
		return $this->st_tituloexibicaodisciplina;
	}

	/**
	 * @param field_type $st_tituloexibicaodisciplina
	 */
	public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
		$this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
	}

	/**
	 * @return the $st_nota
	 */
	public function getSt_nota() {
		return $this->st_nota;
	}

	/**
	 * @param field_type $st_nota
	 */
	public function setSt_nota($st_nota) {
		$this->st_nota = $st_nota;
	}

	/**
	 * @return the $id_avaliacaoagendamento
	 */
	public function getId_avaliacaoagendamento() {
		return $this->id_avaliacaoagendamento;
	}

	/**
	 * @param field_type $id_avaliacaoagendamento
	 */
	public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
	public function getId_avaliacaoconjuntoreferencia() {
		return $this->id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntoreferencia
	 */
	public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @return the $nu_notamax
	 */
	public function getNu_notamax() {
		return $this->nu_notamax;
	}

	/**
	 * @param field_type $nu_notamax
	 */
	public function setNu_notamax($nu_notamax) {
		$this->nu_notamax = $nu_notamax;
	}

	/**
	 * @return the $id_tipocalculoavaliacao
	 */
	public function getId_tipocalculoavaliacao() {
		return $this->id_tipocalculoavaliacao;
	}

	/**
	 * @param field_type $id_tipocalculoavaliacao
	 */
	public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
		$this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
	}

	/**
	 * @return the $id_tipoavaliacao
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @param field_type $id_tipoavaliacao
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @return the $st_tipoavaliacao
	 */
	public function getSt_tipoavaliacao() {
		return $this->st_tipoavaliacao;
	}

	/**
	 * @param field_type $st_tipoavaliacao
	 */
	public function setSt_tipoavaliacao($st_tipoavaliacao) {
		$this->st_tipoavaliacao = $st_tipoavaliacao;
	}

	/**
	 * @return the $id_avaliacaorecupera
	 */
	public function getId_avaliacaorecupera() {
		return $this->id_avaliacaorecupera;
	}

	/**
	 * @param field_type $id_avaliacaorecupera
	 */
	public function setId_avaliacaorecupera($id_avaliacaorecupera) {
		$this->id_avaliacaorecupera = $id_avaliacaorecupera;
	}

	/**
	 * @return the $nu_notamaxima
	 */
	public function getNu_notamaxima() {
		return $this->nu_notamaxima;
	}

	/**
	 * @param field_type $nu_notamaxima
	 */
	public function setNu_notamaxima($nu_notamaxima) {
		$this->nu_notamaxima = $nu_notamaxima;
	}

	/**
	 * @return the $nu_percentualaprovacao
	 */
	public function getNu_percentualaprovacao() {
		return $this->nu_percentualaprovacao;
	}

	/**
	 * @param field_type $nu_percentualaprovacao
	 */
	public function setNu_percentualaprovacao($nu_percentualaprovacao) {
		$this->nu_percentualaprovacao = $nu_percentualaprovacao;
	}

	/**
	 * @return the $dt_agendamento
	 */
	public function getDt_agendamento() {
		return $this->dt_agendamento;
	}

	/**
	 * @param field_type $dt_agendamento
	 */
	public function setDt_agendamento($dt_agendamento) {
		$this->dt_agendamento = $dt_agendamento;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $dt_avaliacao
	 */
	public function getDt_avaliacao() {
		return $this->dt_avaliacao;
	}

	/**
	 * @param field_type $dt_avaliacao
	 */
	public function setDt_avaliacao($dt_avaliacao) {
		$this->dt_avaliacao = $dt_avaliacao;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return the $st_codusuario
	 */
	public function getSt_codusuario() {
		return $this->st_codusuario;
	}

	/**
	 * @param field_type $st_codusuario
	 */
	public function setSt_codusuario($st_codusuario) {
		$this->st_codusuario = $st_codusuario;
	}

	/**
	 * @return the $id_situacaoavaliacao
	 */
	public function getId_situacaoavaliacao() {
		return $this->id_situacaoavaliacao;
	}

	/**
	 * @param field_type $id_situacaoavaliacao
	 */
	public function setId_situacaoavaliacao($id_situacaoavaliacao) {
		$this->id_situacaoavaliacao = $id_situacaoavaliacao;
	}

	/**
	 * @return the $st_situacaoavaliacao
	 */
	public function getSt_situacaoavaliacao() {
		return $this->st_situacaoavaliacao;
	}

	/**
	 * @param field_type $st_situacaoavaliacao
	 */
	public function setSt_situacaoavaliacao($st_situacaoavaliacao) {
		$this->st_situacaoavaliacao = $st_situacaoavaliacao;
	}


}

?>
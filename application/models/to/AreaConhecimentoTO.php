<?php
/**
 * Classe para encapsular os dados de área de conhecimento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AreaConhecimentoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Contém o id da situação da área.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém a descrição da área.
	 * @var String
	 */
	public $st_descricao;
	
	/**
	 * Diz se a área está excluída ou não.
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém o nome da área.
	 * @var String
	 */
	public $st_areaconhecimento;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
/**
	 * Id do usuário que cadastrou a entidade.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Data de cadastro da entidade.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Id da área conhecimento pai.
	 * @var int 
	 */
	public $id_areaconhecimentopai;
	
	public $id_tipoareaconhecimento;
	
	/**
	 * Título de exibição
	 * @var string
	 */
	public $st_tituloexibicao;
	
	
	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao(){

		return $this->st_tituloexibicao;
	}

	/**
	 * @param $st_tituloexibicao the $st_tituloexibicao to set
	 */
	public function setSt_tituloexibicao( $st_tituloexibicao ){

		$this->st_tituloexibicao = $st_tituloexibicao;
	}
	
	/**
	 * @return the $id_tipoareaconhecimento
	 */
	public function getId_tipoareaconhecimento(){

		return $this->id_tipoareaconhecimento;
	}

	/**
	 * @param $id_tipoareaconhecimento the $id_tipoareaconhecimento to set
	 */
	public function setId_tipoareaconhecimento( $id_tipoareaconhecimento ){

		$this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
	}

	/**
	 * @return int
	 */
	public function getId_areaconhecimentopai() {
		return $this->id_areaconhecimentopai;
	}
	
	/**
	 * @param int $id_areaconhecimentopai
	 */
	public function setId_areaconhecimentopai($id_areaconhecimentopai) {
		$this->id_areaconhecimentopai = $id_areaconhecimentopai;
	}
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return Boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @return String
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param Boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param String $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	
	/**
	 * @param String $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

}

?>
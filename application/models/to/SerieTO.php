<?php
/**
 * Classe para encapsular os dados de série.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class SerieTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Contém o nome da série.
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Contém o id da série anterior.
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return int
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}
	
	/**
	 * @return string
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param int $id_serieanterior
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}
	
	/**
	 * @param string $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

}

?>
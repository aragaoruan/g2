<?php
/**
 * Classe para encapsular os dados do historico para a geração de textos e declarações
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwHistoricoTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_projetopedagogico;
	public $id_areaconhecimentopai;
	public $st_areaconhecimentopai;
	public $id_areaconhecimento;
	public $st_areaconhecimento;
	public $id_serie;
	public $id_disciplina;
	public $nu_aprovafinal;
	public $nu_cargahoraria;
	public $sg_uf;
	public $st_cidade;
	public $dt_concluinte;
	public $st_serie;
	public $st_disciplina;
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_areaconhecimentopai
	 */
	public function getId_areaconhecimentopai() {
		return $this->id_areaconhecimentopai;
	}

	/**
	 * @return the $st_areaconhecimentopai
	 */
	public function getSt_areaconhecimentopai() {
		return $this->st_areaconhecimentopai;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $nu_aprovafinal
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}

	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_areaconhecimentopai
	 */
	public function setId_areaconhecimentopai($id_areaconhecimentopai) {
		$this->id_areaconhecimentopai = $id_areaconhecimentopai;
	}

	/**
	 * @param field_type $st_areaconhecimentopai
	 */
	public function setSt_areaconhecimentopai($st_areaconhecimentopai) {
		$this->st_areaconhecimentopai = $st_areaconhecimentopai;
	}

	/**
	 * @param field_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param field_type $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}

	/**
	 * @param field_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}
	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $st_cidade
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}

	/**
	 * @return the $dt_concluinte
	 */
	public function getDt_concluinte() {
		return $this->dt_concluinte;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $st_cidade
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}

	/**
	 * @param field_type $dt_concluinte
	 */
	public function setDt_concluinte($dt_concluinte) {
		$this->dt_concluinte = $dt_concluinte;
	}


	
}
<?php

class VwCampanhaProdutoTO extends Ead1_TO_Dinamico {

	public $id_campanhacomercial;
	
	public $st_campanhacomercial;
	
	public $id_produto;
	
	public $st_produto;
	
	public $id_tipoproduto;
	
	public $st_tipoproduto;
	
	/**
	 * @return unknown
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}
	
	/**
	 * @param unknown_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}
	
	/**
	 * @param unknown_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}
	
	/**
	 * @param unknown_type $st_campanhacomercial
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}
	
	/**
	 * @param unknown_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

}

?>
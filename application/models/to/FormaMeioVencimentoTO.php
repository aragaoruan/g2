<?php
/**
 * Classe de FormaMeioVencimentoTO
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
 class FormaMeioVencimentoTO extends Ead1_TO_Dinamico {


	public $id_formameiovencimento;
	public $id_meiopagamento;
	public $id_formapagamento;
	public $nu_diavencimento;


	/**
	 * @return the $id_formameiovencimento
	 */
	public function getId_formameiovencimento() {
		return $this->id_formameiovencimento;
	}

	/**
	 * @param field_type $id_formameiovencimento
	 */
	public function setId_formameiovencimento($id_formameiovencimento) {
		$this->id_formameiovencimento = $id_formameiovencimento;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @return the $nu_diavencimento
	 */
	public function getNu_diavencimento() {
		return $this->nu_diavencimento;
	}

	/**
	 * @param field_type $nu_diavencimento
	 */
	public function setNu_diavencimento($nu_diavencimento) {
		$this->nu_diavencimento = $nu_diavencimento;
	}


}

<?php
/**
 * Classe para encapsular dados de permissão.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PermissaoTO extends Ead1_TO_Dinamico {
	
	const PERMISSAO_ADICIONAR	= 1;

	/**
	 * Contém o id da permissão.
	 * @var int
	 */
	public $id_permissao;
	
	/**
	 * Contém a descrição do botão da permissão.
	 * @var String
	 */
	public $st_nomepermissao;
	
	/**
	 * Contém a descrição da permissão.
	 * @var String
	 */	
	public $st_descricao;

	/**
	 * Diz se a permissão está ou não ativa.
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém o id da situação da permissão.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * @return Boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_permissao() {
		return $this->id_permissao;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_nomepermissao() {
		return $this->st_nomepermissao;
	}
	
	/**
	 * @param Boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_permissao
	 */
	public function setId_permissao($id_permissao) {
		$this->id_permissao = $id_permissao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param String $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param String $st_nomepermissao
	 */
	public function setSt_nomepermissao($st_nomepermissao) {
		$this->st_nomepermissao = $st_nomepermissao;
	}

}

?>
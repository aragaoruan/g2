<?php
/**
 * Classe para encapsular os dados de livro para registro.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class LivroRegistroEntidadeTO extends Ead1_TO_Dinamico {

	public $id_livroregistroentidade;
	public $id_entidade;
	public $nu_folhas;
	public $nu_registros;
	public $nu_registroinicial;
	public $id_usuariocadastro;
	public $st_livroregistroentidade;
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_livroregistroentidade() {
		return $this->id_livroregistroentidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_folhas() {
		return $this->nu_folhas;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_registroinicial() {
		return $this->nu_registroinicial;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_registros() {
		return $this->nu_registros;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_livroregistroentidade() {
		return $this->st_livroregistroentidade;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_livroregistroentidade
	 */
	public function setId_livroregistroentidade($id_livroregistroentidade) {
		$this->id_livroregistroentidade = $id_livroregistroentidade;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $nu_folhas
	 */
	public function setNu_folhas($nu_folhas) {
		$this->nu_folhas = $nu_folhas;
	}
	
	/**
	 * @param unknown_type $nu_registroinicial
	 */
	public function setNu_registroinicial($nu_registroinicial) {
		$this->nu_registroinicial = $nu_registroinicial;
	}
	
	/**
	 * @param unknown_type $nu_registros
	 */
	public function setNu_registros($nu_registros) {
		$this->nu_registros = $nu_registros;
	}
	
	/**
	 * @param unknown_type $st_livroregistroentidade
	 */
	public function setSt_livroregistroentidade($st_livroregistroentidade) {
		$this->st_livroregistroentidade = $st_livroregistroentidade;
	}

}

?>
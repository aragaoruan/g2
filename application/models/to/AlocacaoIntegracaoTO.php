<?php

/**
 * Classe para encapsular os dados da tabela de alocacao na integração
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class AlocacaoIntegracaoTO extends Ead1_TO_Dinamico{

	public $id_alocacaointegracao;
	public $id_usuariocadastro;
	public $id_alocacao;
	public $id_sistema;
	public $dt_cadastro;
	public $st_codalocacao;
	public $bl_encerrado;
	public $st_codalocacaogrupo;
    public $id_entidadeintegracao;

    /**
     * @return mixed
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param mixed $id_entidadeintegracao
     * @return AlocacaoIntegracaoTO
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }



	/**
	 * @return the $st_codalocacaogrupo
	 */
	public function getSt_codalocacaogrupo() {
		return $this->st_codalocacaogrupo;
	}

	/**
	 * @param field_type $st_codalocacaogrupo
	 */
	public function setSt_codalocacaogrupo($st_codalocacaogrupo) {
		$this->st_codalocacaogrupo = $st_codalocacaogrupo;
	}

	/**
	 * @return the $id_alocacaointegracao
	 */
	public function getId_alocacaointegracao() {
		return $this->id_alocacaointegracao;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_codalocacao
	 */
	public function getSt_codalocacao() {
		return $this->st_codalocacao;
	}

	/**
	 * @param $id_alocacaointegracao the $id_alocacaointegracao to set
	 */
	public function setId_alocacaointegracao($id_alocacaointegracao) {
		$this->id_alocacaointegracao = $id_alocacaointegracao;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $id_alocacao the $id_alocacao to set
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $st_codalocacao the $st_codalocacao to set
	 */
	public function setSt_codalocacao($st_codalocacao) {
		$this->st_codalocacao = $st_codalocacao;
	}
	/**
	 * @return the $bl_encerrado
	 */
	public function getBl_encerrado() {
		return $this->bl_encerrado;
	}

	/**
	 * @param field_type $bl_encerrado
	 */
	public function setBl_encerrado($bl_encerrado) {
		$this->bl_encerrado = $bl_encerrado;
	}

}

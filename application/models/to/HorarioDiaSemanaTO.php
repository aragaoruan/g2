<?php
/**
 * Classe para encapsular os dados de relacionamento entre horário de aula e dia da seman.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class HorarioDiaSemanaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do horário de aula.
	 * @var int
	 */
	public $id_horarioaula;
	
	/**
	 * Id do dia da semana.
	 * @var int
	 */
	public $id_diasemana;
	
	/**
	 * @return int
	 */
	public function getId_diasemana() {
		return $this->id_diasemana;
	}
	
	/**
	 * @return int
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}
	
	/**
	 * @param int $id_diasemana
	 */
	public function setId_diasemana($id_diasemana) {
		$this->id_diasemana = $id_diasemana;
	}
	
	/**
	 * @param int $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

}

?>
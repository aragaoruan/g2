<?php
/**
 * Classe para encapsular os dados da view de produto de prévenda.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class VwPreVendaProdutoTO extends Ead1_TO_Dinamico {

	public $id_prevenda;
	public $id_prevendaproduto;
	public $id_produto;
	public $st_produto;
	public $st_tipoproduto;
	public $dt_inicio;
	public $dt_termino;
	public $id_produtovalor;
	public $nu_valor;
	public $nu_valormensal;
	
	/**
	 * @return unknown
	 */
	public function getId_prevendaproduto() {
		return $this->id_prevendaproduto;
	}
	
	/**
	 * @param unknown_type $id_prevendaproduto
	 */
	public function setId_prevendaproduto($id_prevendaproduto) {
		$this->id_prevendaproduto = $id_prevendaproduto;
	}

	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	
	/**
	 * @return unknown
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_prevenda() {
		return $this->id_prevenda;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produtovalor() {
		return $this->id_produtovalor;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}
	
	/**
	 * @param unknown_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	
	/**
	 * @param unknown_type $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}
	
	/**
	 * @param unknown_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda) {
		$this->id_prevenda = $id_prevenda;
	}
	
	/**
	 * @param unknown_type $id_produtovalor
	 */
	public function setId_produtovalor($id_produtovalor) {
		$this->id_produtovalor = $id_produtovalor;
	}
	
	/**
	 * @param unknown_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}
	
	/**
	 * @param unknown_type $nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
	}
	
	/**
	 * @param unknown_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}
	
	/**
	 * @param unknown_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

}

?>
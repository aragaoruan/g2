<?php
/**
 * Classe para encapsular os dados de dia de vencimento de tal meio de pagamento da venda.
 * @author Gabriel Vasconcelos - gabriel.vasconcelos@ead1.com.br
 * @package models
 * @subpackage to
 */
class VendaMeioVencimentoTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id da venda
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * Contem o id do lancamento
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * Chave primaria
	 * @var int
	 */
	public $id_vendameiovencimento;
	
	public $nu_diavencimento;
	
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $id_vendameiovencimento
	 */
	public function getId_vendameiovencimento() {
		return $this->id_vendameiovencimento;
	}

	/**
	 * @return the $nu_diavencimento
	 */
	public function getNu_diavencimento() {
		return $this->nu_diavencimento;
	}

	/**
	 * @param int $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param boolean $id_vendameiovencimento
	 */
	public function setId_vendameiovencimento($id_vendameiovencimento) {
		$this->id_vendameiovencimento = $id_vendameiovencimento;
	}

	/**
	 * @param boolean $nu_diavencimento
	 */
	public function setNu_diavencimento($nu_diavencimento) {
		$this->nu_diavencimento = $nu_diavencimento;
	}

	
}
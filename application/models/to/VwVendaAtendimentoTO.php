<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwVendaAtendimentoTO extends Ead1_TO_Dinamico{

	public $id_usuariocliente;
	public $st_nomecliente;
	public $id_usuarioatendente;
	public $st_nomeatendente;
	public $id_nucleotm;
	public $st_nucleotm;
	public $id_tramite;
	public $dt_ultimainteracao;
	public $id_evolucao;
	public $st_evolucao;
	public $id_situacao;
	public $st_situacao;
	public $id_entidade;
	public $dt_cadastro;
	public $id_venda;
	public $id_prevenda;
	public $bl_ativo;
	public $id_protocolo;
	public $dt_agendamento;


	/**
	 * @return the $id_usuariocliente
	 */
	public function getId_usuariocliente(){ 
		return $this->id_usuariocliente;
	}

	/**
	 * @return the $st_nomecliente
	 */
	public function getSt_nomecliente(){ 
		return $this->st_nomecliente;
	}

	/**
	 * @return the $id_usuarioatendente
	 */
	public function getId_usuarioatendente(){ 
		return $this->id_usuarioatendente;
	}

	/**
	 * @return the $st_nomeatendente
	 */
	public function getSt_nomeatendente(){ 
		return $this->st_nomeatendente;
	}

	/**
	 * @return the $id_nucleotm
	 */
	public function getId_nucleotm(){ 
		return $this->id_nucleotm;
	}

	/**
	 * @return the $st_nucleotm
	 */
	public function getSt_nucleotm(){ 
		return $this->st_nucleotm;
	}

	/**
	 * @return the $id_tramite
	 */
	public function getId_tramite(){ 
		return $this->id_tramite;
	}

	/**
	 * @return the $dt_ultimainteracao
	 */
	public function getDt_ultimainteracao(){ 
		return $this->dt_ultimainteracao;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao(){ 
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao(){ 
		return $this->st_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao(){ 
		return $this->st_situacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda(){ 
		return $this->id_venda;
	}

	/**
	 * @return the $id_prevenda
	 */
	public function getId_prevenda(){ 
		return $this->id_prevenda;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_protocolo
	 */
	public function getId_protocolo(){ 
		return $this->id_protocolo;
	}

	/**
	 * @return the $dt_agendamento
	 */
	public function getDt_agendamento(){ 
		return $this->dt_agendamento;
	}


	/**
	 * @param field_type $id_usuariocliente
	 */
	public function setId_usuariocliente($id_usuariocliente){ 
		$this->id_usuariocliente = $id_usuariocliente;
	}

	/**
	 * @param field_type $st_nomecliente
	 */
	public function setSt_nomecliente($st_nomecliente){ 
		$this->st_nomecliente = $st_nomecliente;
	}

	/**
	 * @param field_type $id_usuarioatendente
	 */
	public function setId_usuarioatendente($id_usuarioatendente){ 
		$this->id_usuarioatendente = $id_usuarioatendente;
	}

	/**
	 * @param field_type $st_nomeatendente
	 */
	public function setSt_nomeatendente($st_nomeatendente){ 
		$this->st_nomeatendente = $st_nomeatendente;
	}

	/**
	 * @param field_type $id_nucleotm
	 */
	public function setId_nucleotm($id_nucleotm){ 
		$this->id_nucleotm = $id_nucleotm;
	}

	/**
	 * @param field_type $st_nucleotm
	 */
	public function setSt_nucleotm($st_nucleotm){ 
		$this->st_nucleotm = $st_nucleotm;
	}

	/**
	 * @param field_type $id_tramite
	 */
	public function setId_tramite($id_tramite){ 
		$this->id_tramite = $id_tramite;
	}

	/**
	 * @param field_type $dt_ultimainteracao
	 */
	public function setDt_ultimainteracao($dt_ultimainteracao){ 
		$this->dt_ultimainteracao = $dt_ultimainteracao;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao){ 
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao){ 
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao){ 
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda){ 
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda){ 
		$this->id_prevenda = $id_prevenda;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $id_protocolo
	 */
	public function setId_protocolo($id_protocolo){ 
		$this->id_protocolo = $id_protocolo;
	}

	/**
	 * @param field_type $dt_agendamento
	 */
	public function setDt_agendamento($dt_agendamento){ 
		$this->dt_agendamento = $dt_agendamento;
	}

}
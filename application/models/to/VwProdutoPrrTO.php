<?php
/**
 * Classe para encapsular os dados da vw_produtoprr
 * @package models
 * @subpackage to
 */
class VwProdutoPrrTO extends Ead1_TO_Dinamico {

    public $id_produto;
    public $st_produto;
    public $id_tipoproduto;
    public $id_situacao;
    public $id_entidade;
    public $bl_ativo;
    public $nu_valor;
    public $dt_inicio;
    public $dt_termino;
    public $nu_valormensal;
    public $id_saladeaula;

    /**
     * @param mixed $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return mixed
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return mixed
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param mixed $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return mixed
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return mixed
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param mixed $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return mixed
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param mixed $id_tipoproduto
     */
    public function setId_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
    }

    /**
     * @return mixed
     */
    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @param mixed $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return mixed
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param mixed $st_produto
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
    }

    /**
     * @return mixed
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @param mixed $nu_valormensal
     */
    public function setNu_valormensal($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
    }

    /**
     * @return mixed
     */
    public function getNu_valormensal()
    {
        return $this->nu_valormensal;
    }

    /**
     * @param mixed $nu_valormensal
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return mixed
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }


}
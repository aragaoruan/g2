<?php
/**
 * Classe para encapsular os dados de pessoa e Perfil
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to 
 */
class PessoasPerfilTO extends Ead1_TO_Dinamico{

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Atributo que contém o nome completo do Usuario
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Diz a situação do perfil usuário entidade
	 * @var Boolean
	 */
	public $id_situacaousuarioperfilentidade;
	
	/**
	 * Data de início de vingência do usuário com o perfil nessa entidade.
	 * @var String
	 */
	public $dt_inicio;
	
	/**
	 * Data de término de vingência do usuário com o perfil nessa entidade.
	 * @var String
	 */
	public $dt_termino;
	
	/**
	 * Contém o id da situação da pessoa
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_situacaousuarioperfilentidade
	 */
	public function getId_situacaousuarioperfilentidade() {
		return $this->id_situacaousuarioperfilentidade;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $st_nomecompleto the $st_nomecompleto to set
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param $id_situacaousuarioperfilentidade the $id_situacaousuarioperfilentidade to set
	 */
	public function setId_situacaousuarioperfilentidade($id_situacaousuarioperfilentidade) {
		$this->id_situacaousuarioperfilentidade = $id_situacaousuarioperfilentidade;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_termino the $dt_termino to set
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
}
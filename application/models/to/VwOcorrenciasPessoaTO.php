<?php

/**
 * Classe para encapsular os dados da VwOcorrenciasPessoa.
 * @author denisexavier
 *
 * @package models
 * @subpackage to
 */
class VwOcorrenciasPessoaTO extends Ead1_TO_Dinamico
{


    /**
     * @var string
     */
    public $st_nomecompleto;

    /**
     * @var string
     */
    public $st_nomeinteressado;
    /**
     * @var string
     */
    public $st_emailinteressado;
    /**
     * @var integer
     */
    public $id_nucleoco;
    /**
     * @var integer
     */
    public $id_assuntoco;
    /**
     * @var integer
     */
    public $id_assuntocopai;
    /**
     * @var integer
     */
    public $id_usuario;
    /**
     * @var integer
     */
    public $id_funcao;
    public $bl_prioritario;
    /**
     * @var integer
     */
    public $id_tipoocorrencia;
    /**
     * @var integer
     */
    public $id_entidade;
    /**
     * @var integer
     */
    public $id_situacao;
    /**
     * @var string
     */
    public $st_nucleoco;
    /**
     * @var string
     */
    public $st_urlportal;
    /**
     * @var boolean
     */
    public $bl_ativo;
    /**
     * @var string
     */
    public $st_funcao;
    /**
     * @var integer
     */
    public $id_ocorrencia;
    /**
     * @var integer
     */
    public $id_matricula;
    /**
     * @var integer
     */
    public $id_evolucao;
    /**
     * @var integer
     */
    public $id_usuariointeressado;
    /**
     * @var integer
     */
    public $id_categoriaocorrencia;
    /**
     * @var integer
     */
    public $id_saladeaula;
    /**
     * @var string
     */
    public $st_titulo;
    /**
     * @var string
     */
    public $st_ocorrencia;
    /**
     * @var string
     */
    public $st_situacao;
    /**
     * @var string
     */
    public $st_evolucao;
    /**
     * @var string
     */
    public $st_assuntoco;
    /**
     * @var integer
     */
    public $id_atendente;
    /**
     * @var string
     */
    public $st_atendente;
    /**
     * @var string
     */
    public $st_categoriaocorrencia;
    /**
     * @var string
     */
    public $st_ultimotramite;

    /**
     * @var float
     */
    public $nu_horastraso;

    /**
     * @var float
     */
    public $nu_porcentagemmeta;
    /**
     * @var string
     */
    public $st_cor;

    /**
     * @var float
     */
    public $nu_ordem;

    /**
     * @var string
     */
    public $st_assuntopai;

    /**
     * @var float
     */
    public $nu_diasabertos;

    /**
     * @var mixed
     */
    public $dt_cadastroocorrencia;

    /**
     * @var boolean
     */
    public $bl_cancelamento;

    /**
     * @var boolean
     */
    public $bl_trancamento;
    /**
     * @var integer
     */
    public $id_evolucaomatricula;

    /**
     * @var mixed
     */
    public $dt_atendimento;

    /**
     * @return string
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomeinteressado()
    {
        return $this->st_nomeinteressado;
    }

    /**
     * @param string $st_nomeinteressado
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_nomeinteressado($st_nomeinteressado)
    {
        $this->st_nomeinteressado = $st_nomeinteressado;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_emailinteressado()
    {
        return $this->st_emailinteressado;
    }

    /**
     * @param string $st_emailinteressado
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_emailinteressado($st_emailinteressado)
    {
        $this->st_emailinteressado = $st_emailinteressado;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param int $id_assuntoco
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    /**
     * @param int $id_assuntocopai
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param int $id_funcao
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_prioritario()
    {
        return $this->bl_prioritario;
    }

    /**
     * @param mixed $bl_prioritario
     * @return VwOcorrenciasPessoaTO
     */
    public function setbl_prioritario($bl_prioritario)
    {
        $this->bl_prioritario = $bl_prioritario;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nucleoco()
    {
        return $this->st_nucleoco;
    }

    /**
     * @param string $st_nucleoco
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_urlportal()
    {
        return $this->st_urlportal;
    }

    /**
     * @param string $st_urlportal
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
        return $this;
    }

    /**
     * @return bool
     */
    public function isbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return VwOcorrenciasPessoaTO
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_funcao()
    {
        return $this->st_funcao;
    }

    /**
     * @param string $st_funcao
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param int $id_ocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    /**
     * @param int $id_usuariointeressado
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @param int $id_categoriaocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_titulo()
    {
        return $this->st_titulo;
    }

    /**
     * @param string $st_titulo
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    /**
     * @param string $st_ocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_assuntoco()
    {
        return $this->st_assuntoco;
    }

    /**
     * @param string $st_assuntoco
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @param int $id_atendente
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_atendente()
    {
        return $this->st_atendente;
    }

    /**
     * @param string $st_atendente
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_atendente($st_atendente)
    {
        $this->st_atendente = $st_atendente;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    /**
     * @param string $st_categoriaocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_ultimotramite()
    {
        return $this->st_ultimotramite;
    }

    /**
     * @param string $st_ultimotramite
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_ultimotramite($st_ultimotramite)
    {
        $this->st_ultimotramite = $st_ultimotramite;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_horastraso()
    {
        return $this->nu_horastraso;
    }

    /**
     * @param float $nu_horastraso
     * @return VwOcorrenciasPessoaTO
     */
    public function setnu_horastraso($nu_horastraso)
    {
        $this->nu_horastraso = $nu_horastraso;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_porcentagemmeta()
    {
        return $this->nu_porcentagemmeta;
    }

    /**
     * @param float $nu_porcentagemmeta
     * @return VwOcorrenciasPessoaTO
     */
    public function setnu_porcentagemmeta($nu_porcentagemmeta)
    {
        $this->nu_porcentagemmeta = $nu_porcentagemmeta;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cor()
    {
        return $this->st_cor;
    }

    /**
     * @param string $st_cor
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_cor($st_cor)
    {
        $this->st_cor = $st_cor;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param float $nu_ordem
     * @return VwOcorrenciasPessoaTO
     */
    public function setnu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_assuntopai()
    {
        return $this->st_assuntopai;
    }

    /**
     * @param string $st_assuntopai
     * @return VwOcorrenciasPessoaTO
     */
    public function setst_assuntopai($st_assuntopai)
    {
        $this->st_assuntopai = $st_assuntopai;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_diasabertos()
    {
        return $this->nu_diasabertos;
    }

    /**
     * @param float $nu_diasabertos
     * @return VwOcorrenciasPessoaTO
     */
    public function setnu_diasabertos($nu_diasabertos)
    {
        $this->nu_diasabertos = $nu_diasabertos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastroocorrencia()
    {
        return $this->dt_cadastroocorrencia;
    }

    /**
     * @param mixed $dt_cadastroocorrencia
     * @return VwOcorrenciasPessoaTO
     */
    public function setdt_cadastroocorrencia($dt_cadastroocorrencia)
    {
        $this->dt_cadastroocorrencia = $dt_cadastroocorrencia;
        return $this;
    }

    /**
     * @return bool
     */
    public function isbl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     * @return VwOcorrenciasPessoaTO
     */
    public function setbl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
        return $this;
    }

    /**
     * @return bool
     */
    public function isbl_trancamento()
    {
        return $this->bl_trancamento;
    }

    /**
     * @param bool $bl_trancamento
     * @return VwOcorrenciasPessoaTO
     */
    public function setbl_trancamento($bl_trancamento)
    {
        $this->bl_trancamento = $bl_trancamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucaomatricula()
    {
        return $this->id_evolucaomatricula;
    }

    /**
     * @param int $id_evolucaomatricula
     * @return VwOcorrenciasPessoaTO
     */
    public function setid_evolucaomatricula($id_evolucaomatricula)
    {
        $this->id_evolucaomatricula = $id_evolucaomatricula;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_atendimento()
    {
        return $this->dt_atendimento;
    }

    /**
     * @param mixed $dt_atendimento
     * @return VwOcorrenciasPessoaTO
     */
    public function setdt_atendimento($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
        return $this;
    }




}

?>
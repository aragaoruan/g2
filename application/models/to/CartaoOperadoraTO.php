<?php
/**
 * Classe de valor para a tabela tb_cartaooperadora
 * @author Arthur Cláudio de Almeida Pereira
 */
class CartaoOperadoraTO extends Ead1_TO_Dinamico {
	
	
	const CIELO = 1;
	const REDECARD = 2;
	

	public $id_cartaooperadora;
	public $st_cartaooperadora;
	
	/**
	 * @return the $id_cartaooperadora
	 */
	public function getId_cartaooperadora() {
		return $this->id_cartaooperadora;
	}

	/**
	 * @return the $st_cartaooperadora
	 */
	public function getSt_cartaooperadora() {
		return $this->st_cartaooperadora;
	}

	/**
	 * @param $id_cartaooperadora the $id_cartaooperadora to set
	 */
	public function setId_cartaooperadora($id_cartaooperadora) {
		$this->id_cartaooperadora = $id_cartaooperadora;
	}

	/**
	 * @param $st_cartaooperadora the $st_cartaooperadora to set
	 */
	public function setSt_cartaooperadora($st_cartaooperadora) {
		$this->st_cartaooperadora = $st_cartaooperadora;
	}

}
<?php

/**
 * Classe para encapsular os dados da view de email da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaEmailTO extends Ead1_TO_Dinamico
{

    /**
     * Contém o id de entidade.
     * @var int
     */
    public $id_entidade;

    /**
     * Contém o id do usuário.
     * @var int
     */
    public $id_usuario;

    /**
     * Contém o email da pessoa.
     * @var string
     */
    public $st_email;

    /**
     * Contém o id do email.
     * @var int
     */
    public $id_email;

    /**
     * Contém o id do perfil.
     * @var int
     */
    public $id_perfil;

    /**
     * Contém se é o padrão ou não
     * @var boolean
     */
    public $bl_padrao;

    /**
     *
     * @var boolean 
     */
    public $bl_ativo;

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return the $bl_padrao
     */
    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    /**
     * @param $bl_padrao the $bl_padrao to set
     */
    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
    }

    public function getId_email ()
    {
        return $this->id_email;
    }

    /**
     * @return int
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    /**
     * @return int
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return string
     */
    public function getSt_email ()
    {
        return $this->st_email;
    }

    /**
     * @param int $id_email
     */
    public function setId_email ($id_email)
    {
        $this->id_email = $id_email;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email ($st_email)
    {
        $this->st_email = $st_email;
    }

}

?>
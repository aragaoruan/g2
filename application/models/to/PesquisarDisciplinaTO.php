<?php
/**
 * Classe para encapsular os dados de pesquisa de disciplina
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class PesquisarDisciplinaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarDisciplina';
	
	/**
	 * Atributo que contém o id da disciplina
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Atributo que contém o nome da disciplina
	 * @var tring
	 */
	public $st_disciplina;
	
	/**
	 * Atributo que contém a descrição da disciplina
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Atributo que contém o id da área do conhecimento
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Atributo que contém o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Atribuco que contém a descriçãoi da área do conhecimento
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Atribuco que contém 
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Atributo que contém a descrição do nível de ensino
	 * @var String
	 */
	public $st_nivelensino;
	
	/**
	 * Atributo que contém o id do nível de ensino
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Atributo que contém o id da série
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Atributo que o id da série anterior
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Situação
	 * @var String
	 */
	public $st_situacao;
	
	/**
	 * Descrição Série
	 * @var String
	 */
	public $st_serie;
	
	/**
	 * Id do tipo da disciplina
	 * @var int
	 */
	public $id_tipodisciplina;
	
	/**
	 * Tipo de disciplina
	 * @var String
	 */
	public $st_tipodisciplina;
	
	/**
	 * Id do tipo de situação
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @param $st_areaconhecimento the $st_areaconhecimento to set
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @return the $id_serieanterior
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}

	/**
	 * @param $id_serieanterior the $id_serieanterior to set
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @param $id_tipodisciplina the $id_tipodisciplina to set
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @return the $st_tipodisciplina
	 */
	public function getSt_tipodisciplina() {
		return $this->st_tipodisciplina;
	}

	/**
	 * @param $st_tipodisciplina the $st_tipodisciplina to set
	 */
	public function setSt_tipodisciplina($st_tipodisciplina) {
		$this->st_tipodisciplina = $st_tipodisciplina;
	}
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}


		
}
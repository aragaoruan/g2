<?php
/**
 * tb_processamento
 * @package models
 * @subpackage to
 */
class ProcessamentoTO extends Ead1_TO_Dinamico {

	public $id_processamento;
	public $id_processo;
	public $dt_processamento;
	
	/**
	 * @return the $id_processamento
	 */
	public function getId_processamento() {
		return $this->id_processamento;
	}

	/**
	 * @return the $id_processo
	 */
	public function getId_processo() {
		return $this->id_processo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_atualizado() {
		return $this->dt_atualizado;
	}

	/**
	 * @param field_type $id_processamento
	 */
	public function setId_processamento($id_processamento) {
		$this->id_processamento = $id_processamento;
	}

	/**
	 * @param field_type $id_processo
	 */
	public function setId_processo($id_processo) {
		$this->id_processo = $id_processo;
	}

	/**
	 * @param field_type $dt_processamento
	 */
	public function setDt_processamento($dt_processamento) {
		$this->dt_processamento = $dt_processamento;
	}



}

?>
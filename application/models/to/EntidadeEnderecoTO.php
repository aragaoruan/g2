<?php
/**
 * Classe para vincular entidade com endereco.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeEnderecoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do endereço.
	 * @var int
	 */
	public $id_endereco;
	
	/**
	 * Diz se o endereço é padrão ou não.
	 * @var Boolean
	 */
	public $bl_padrao;
	
	/**
	 * Chave primaria da tabela
	 * @var int
	 */
	public $id_entidadeendereco;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_endereco
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @return the $id_entidadeendereco
	 */
	public function getId_entidadeendereco() {
		return $this->id_entidadeendereco;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_endereco
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}

	/**
	 * @param Boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param int $id_entidadeendereco
	 */
	public function setId_entidadeendereco($id_entidadeendereco) {
		$this->id_entidadeendereco = $id_entidadeendereco;
	}

}

?>
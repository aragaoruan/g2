<?php
/**
 * Classe para encapsular os dados da view de Avaliação e Agendamento
 * @author Eder Lamar
 * 
 * @package models
 * @subpackage to
 */
class VwSimuladoUsuarioTO extends Ead1_TO_Dinamico{
	
	public $id_usuario;
	public $dt_agendamento;
	public $st_avaliacao;
	public $id_entidade;
	public $st_caminho;
	public $st_login;
	public $st_senha;
        public $st_codigo;
        
        public function getSt_codigo() {
            return $this->st_codigo;
        }

        public function setSt_codigo($st_codigo) {
            $this->st_codigo = $st_codigo;
            return $this;
        }

        public function getSt_login() {
            return $this->st_login;
        }

        public function setSt_login($st_login) {
            $this->st_login = $st_login;
            return $this;
        }

        public function getSt_senha() {
            return $this->st_senha;
        }

        public function setSt_senha($st_senha) {
            $this->st_senha = $st_senha;
            return $this;
        }

        
        public function getId_usuario() {
            return $this->id_usuario;
        }

        public function setId_usuario($id_usuario) {
            $this->id_usuario = $id_usuario;
            return $this;
        }

        public function getDt_agendamento() {
            return $this->dt_agendamento;
        }

        public function setDt_agendamento($dt_agendamento) {
            $this->dt_agendamento = $dt_agendamento;
            return $this;
        }

        public function getSt_avaliacao() {
            return $this->st_avaliacao;
        }

        public function setSt_avaliacao($st_avaliacao) {
            $this->st_avaliacao = $st_avaliacao;
            return $this;
        }

        public function getId_entidade() {
            return $this->id_entidade;
        }

        public function setId_entidade($id_entidade) {
            $this->id_entidade = $id_entidade;
            return $this;
        }

        public function getSt_caminho() {
            return $this->st_caminho;
        }

        public function setSt_caminho($st_caminho) {
            $this->st_caminho = $st_caminho;
            return $this;
        }


}
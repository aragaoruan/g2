<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwPessoaEnderecoTO extends Ead1_TO_Dinamico{

	public $id_usuario;
	public $id_entidade;
	public $bl_padrao;
	public $st_categoriaendereco;
	public $id_tipoendereco;
	public $st_tipoendereco;
	public $id_endereco;
	public $st_endereco;
	public $st_complemento;
	public $nu_numero;
	public $st_bairro;
	public $st_cidade;
	public $st_estadoprovincia;
	public $st_cep;
	public $sg_uf;
	public $id_categoriaendereco;
	public $id_municipio;
	public $st_nomemunicipio;
	public $st_uf;
	public $id_pais;
	public $st_nomepais;
	public $bl_ativo;


	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao(){ 
		return $this->bl_padrao;
	}

	/**
	 * @return the $st_categoriaendereco
	 */
	public function getSt_categoriaendereco(){ 
		return $this->st_categoriaendereco;
	}

	/**
	 * @return the $id_tipoendereco
	 */
	public function getId_tipoendereco(){ 
		return $this->id_tipoendereco;
	}

	/**
	 * @return the $st_tipoendereco
	 */
	public function getSt_tipoendereco(){ 
		return $this->st_tipoendereco;
	}

	/**
	 * @return the $id_endereco
	 */
	public function getId_endereco(){ 
		return $this->id_endereco;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco(){ 
		return $this->st_endereco;
	}

	/**
	 * @return the $st_complemento
	 */
	public function getSt_complemento(){ 
		return $this->st_complemento;
	}

	/**
	 * @return the $st_numero
	 */
	public function getNu_numero(){ 
		return $this->nu_numero;
	}

	/**
	 * @return the $st_bairro
	 */
	public function getSt_bairro(){ 
		return $this->st_bairro;
	}

	/**
	 * @return the $st_cidade
	 */
	public function getSt_cidade(){ 
		return $this->st_cidade;
	}

	/**
	 * @return the $st_estadoprovincia
	 */
	public function getSt_estadoprovincia(){ 
		return $this->st_estadoprovincia;
	}

	/**
	 * @return the $st_cep
	 */
	public function getSt_cep(){ 
		return $this->st_cep;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf(){ 
		return $this->sg_uf;
	}

	/**
	 * @return the $id_categoriaendereco
	 */
	public function getId_categoriaendereco(){ 
		return $this->id_categoriaendereco;
	}

	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio(){ 
		return $this->id_municipio;
	}

	/**
	 * @return the $st_nomemunicipio
	 */
	public function getSt_nomemunicipio(){ 
		return $this->st_nomemunicipio;
	}

	/**
	 * @return the $st_uf
	 */
	public function getSt_uf(){ 
		return $this->st_uf;
	}

	/**
	 * @return the $id_pais
	 */
	public function getId_pais(){ 
		return $this->id_pais;
	}

	/**
	 * @return the $st_nomepais
	 */
	public function getSt_nomepais(){ 
		return $this->st_nomepais;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}


	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao){ 
		$this->bl_padrao = $bl_padrao;
	}

	/**
	 * @param field_type $st_categoriaendereco
	 */
	public function setSt_categoriaendereco($st_categoriaendereco){ 
		$this->st_categoriaendereco = $st_categoriaendereco;
	}

	/**
	 * @param field_type $id_tipoendereco
	 */
	public function setId_tipoendereco($id_tipoendereco){ 
		$this->id_tipoendereco = $id_tipoendereco;
	}

	/**
	 * @param field_type $st_tipoendereco
	 */
	public function setSt_tipoendereco($st_tipoendereco){ 
		$this->st_tipoendereco = $st_tipoendereco;
	}

	/**
	 * @param field_type $id_endereco
	 */
	public function setId_endereco($id_endereco){ 
		$this->id_endereco = $id_endereco;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco){ 
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_complemento
	 */
	public function setSt_complemento($st_complemento){ 
		$this->st_complemento = $st_complemento;
	}

	/**
	 * @param field_type $st_numero
	 */
	public function setNu_numero($nu_numero){ 
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param field_type $st_bairro
	 */
	public function setSt_bairro($st_bairro){ 
		$this->st_bairro = $st_bairro;
	}

	/**
	 * @param field_type $st_cidade
	 */
	public function setSt_cidade($st_cidade){ 
		$this->st_cidade = $st_cidade;
	}

	/**
	 * @param field_type $st_estadoprovincia
	 */
	public function setSt_estadoprovincia($st_estadoprovincia){ 
		$this->st_estadoprovincia = $st_estadoprovincia;
	}

	/**
	 * @param field_type $st_cep
	 */
	public function setSt_cep($st_cep){ 
		$this->st_cep = $st_cep;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf){ 
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $id_categoriaendereco
	 */
	public function setId_categoriaendereco($id_categoriaendereco){ 
		$this->id_categoriaendereco = $id_categoriaendereco;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio){ 
		$this->id_municipio = $id_municipio;
	}

	/**
	 * @param field_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio){ 
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

	/**
	 * @param field_type $st_uf
	 */
	public function setSt_uf($st_uf){ 
		$this->st_uf = $st_uf;
	}

	/**
	 * @param field_type $id_pais
	 */
	public function setId_pais($id_pais){ 
		$this->id_pais = $id_pais;
	}

	/**
	 * @param field_type $st_nomepais
	 */
	public function setSt_nomepais($st_nomepais){ 
		$this->st_nomepais = $st_nomepais;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

}
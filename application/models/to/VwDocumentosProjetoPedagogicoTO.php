<?php

class VwDocumentosProjetoPedagogicoTO extends Ead1_TO_Dinamico {

	public $id_projetopedagogico;
	
	public $id_documentos;
	
	public $st_projetopedagogico;
	
	public $st_documentos;
	
	/**
	 * @return unknown
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_documentos() {
		return $this->st_documentos;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param unknown_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $st_documentos
	 */
	public function setSt_documentos($st_documentos) {
		$this->st_documentos = $st_documentos;
	}
	
	/**
	 * @param unknown_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

}

?>
<?php

/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
class VwGerarVendaTextoTO extends Ead1_TO_Dinamico {

    public $id_venda;
    public $id_lancamento;
    public $id_acordo;
    public $st_vendacliente;
    public $nu_vendavalorbruto;
    public $nu_vendavalorliquido;
    public $nu_vendadescontoporc;
    public $nu_vendadescontovalor;
    public $nu_lancamentovalor;
    public $dt_lancamentovencimento;
    public $dt_lancamentodataquitado;
    public $st_lancamentomeiopagamento;
    public $st_lancamentoresponsavel;
    public $nu_quitado;
    public $dt_primeiroquitado;
    public $dt_ultimoquitado;
    public $st_mesanoprimeiroquitado;
    public $st_mesanoultimoquitado;
    public $st_periodoquitado;
    public $st_nomeentidade;
    public $id_usuario;
    public $st_ano;
    public $id_matricula;

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function getSt_ano() {
        return $this->st_ano;
    }

    public function setSt_ano($st_ano) {
        $this->st_ano = $st_ano;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return the $nu_quitado
     */
    public function getNu_quitado() {
        return $this->nu_quitado;
    }

    /**
     * @return the $dt_primeiroquitado
     */
    public function getDt_primeiroquitado() {
        return $this->dt_primeiroquitado;
    }

    /**
     * @return the $dt_ultimoquitado
     */
    public function getDt_ultimoquitado() {
        return $this->dt_ultimoquitado;
    }

    /**
     * @return the $st_mesanoprimeiroquitado
     */
    public function getSt_mesanoprimeiroquitado() {
        return $this->st_mesanoprimeiroquitado;
    }

    /**
     * @return the $st_mesanoultimoquitado
     */
    public function getSt_mesanoultimoquitado() {
        return $this->st_mesanoultimoquitado;
    }

    /**
     * @return the $st_periodoquitado
     */
    public function getSt_periodoquitado() {
        return $this->st_periodoquitado;
    }

    /**
     * @param field_type $nu_quitado
     */
    public function setNu_quitado($nu_quitado) {
        $this->nu_quitado = $nu_quitado;
    }

    /**
     * @param field_type $dt_primeiroquitado
     */
    public function setDt_primeiroquitado($dt_primeiroquitado) {
        $this->dt_primeiroquitado = $dt_primeiroquitado;
    }

    /**
     * @param field_type $dt_ultimoquitado
     */
    public function setDt_ultimoquitado($dt_ultimoquitado) {
        $this->dt_ultimoquitado = $dt_ultimoquitado;
    }

    /**
     * @param field_type $st_mesanoprimeiroquitado
     */
    public function setSt_mesanoprimeiroquitado($st_mesanoprimeiroquitado) {
        $this->st_mesanoprimeiroquitado = $st_mesanoprimeiroquitado;
    }

    /**
     * @param field_type $st_mesanoultimoquitado
     */
    public function setSt_mesanoultimoquitado($st_mesanoultimoquitado) {
        $this->st_mesanoultimoquitado = $st_mesanoultimoquitado;
    }

    /**
     * @param field_type $st_periodoquitado
     */
    public function setSt_periodoquitado($st_periodoquitado) {
        $this->st_periodoquitado = $st_periodoquitado;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    /**
     * @return the $id_acordo
     */
    public function getId_acordo() {
        return $this->id_acordo;
    }

    /**
     * @return the $st_vendacliente
     */
    public function getSt_vendacliente() {
        return $this->st_vendacliente;
    }

    /**
     * @return the $nu_vendavalorbruto
     */
    public function getNu_vendavalorbruto() {
        return $this->nu_vendavalorbruto;
    }

    /**
     * @return the $nu_vendavalorliquido
     */
    public function getNu_vendavalorliquido() {
        return $this->nu_vendavalorliquido;
    }

    /**
     * @return the $nu_vendadescontoporc
     */
    public function getNu_vendadescontoporc() {
        return $this->nu_vendadescontoporc;
    }

    /**
     * @return the $nu_vendadescontovalor
     */
    public function getNu_vendadescontovalor() {
        return $this->nu_vendadescontovalor;
    }

    /**
     * @return the $nu_lancamentovalor
     */
    public function getNu_lancamentovalor() {
        return $this->nu_lancamentovalor;
    }

    /**
     * @return the $dt_lancamentovencimento
     */
    public function getDt_lancamentovencimento() {
        return $this->dt_lancamentovencimento;
    }

    /**
     * @return the $dt_lancamentodataquitado
     */
    public function getDt_lancamentodataquitado() {
        return $this->dt_lancamentodataquitado;
    }

    /**
     * @return the $st_lancamentomeiopagamento
     */
    public function getSt_lancamentomeiopagamento() {
        return $this->st_lancamentomeiopagamento;
    }

    /**
     * @return the $st_lancamentoresponsavel
     */
    public function getSt_lancamentoresponsavel() {
        return $this->st_lancamentoresponsavel;
    }

    /**
     * @param field_type $id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

    /**
     * @param field_type $id_lancamento
     */
    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param field_type $id_acordo
     */
    public function setId_acordo($id_acordo) {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @param field_type $st_vendacliente
     */
    public function setSt_vendacliente($st_vendacliente) {
        $this->st_vendacliente = $st_vendacliente;
    }

    /**
     * @param field_type $nu_vendavalorbruto
     */
    public function setNu_vendavalorbruto($nu_vendavalorbruto) {
        $this->nu_vendavalorbruto = $nu_vendavalorbruto;
    }

    /**
     * @param field_type $nu_vendavalorliquido
     */
    public function setNu_vendavalorliquido($nu_vendavalorliquido) {
        $this->nu_vendavalorliquido = $nu_vendavalorliquido;
    }

    /**
     * @param field_type $nu_vendadescontoporc
     */
    public function setNu_vendadescontoporc($nu_vendadescontoporc) {
        $this->nu_vendadescontoporc = $nu_vendadescontoporc;
    }

    /**
     * @param field_type $nu_vendadescontovalor
     */
    public function setNu_vendadescontovalor($nu_vendadescontovalor) {
        $this->nu_vendadescontovalor = $nu_vendadescontovalor;
    }

    /**
     * @param field_type $nu_lancamentovalor
     */
    public function setNu_lancamentovalor($nu_lancamentovalor) {
        $this->nu_lancamentovalor = $nu_lancamentovalor;
    }

    /**
     * @param field_type $dt_lancamentovencimento
     */
    public function setDt_lancamentovencimento($dt_lancamentovencimento) {
        $this->dt_lancamentovencimento = $dt_lancamentovencimento;
    }

    /**
     * @param field_type $dt_lancamentodataquitado
     */
    public function setDt_lancamentodataquitado($dt_lancamentodataquitado) {
        $this->dt_lancamentodataquitado = $dt_lancamentodataquitado;
    }

    /**
     * @param field_type $st_lancamentomeiopagamento
     */
    public function setSt_lancamentomeiopagamento($st_lancamentomeiopagamento) {
        $this->st_lancamentomeiopagamento = $st_lancamentomeiopagamento;
    }

    /**
     * @param field_type $st_lancamentoresponsavel
     */
    public function setSt_lancamentoresponsavel($st_lancamentoresponsavel) {
        $this->st_lancamentoresponsavel = $st_lancamentoresponsavel;
    }

}
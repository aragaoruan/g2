<?php

/**
 * Classe para encapsular os dados da view de Listagem de Projetos
 * @author Denise Xavier <denise.xavier@unyleya.com>
 * @package models
 * @subpackage to
 */
class VwListagemProjetosSalaTO extends  Ead1_TO_Dinamico{

    public $st_projetopedagogico;
    public $id_usuario;
    public $id_entidade;
    public $id_perfil;
    public $id_perfilpedagogico;
    public $id_projetopedagogico;
    public $id_status;
    public $st_status;
    public $id_sistema;
    public $st_coordenador;

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return mixed
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param mixed $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @return mixed
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @param mixed $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return mixed
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param mixed $id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
    }

    /**
     * @return mixed
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return mixed
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return mixed
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param mixed $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }

    /**
     * @return mixed
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param mixed $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return mixed
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $st_coordenador
     */
    public function setSt_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
    }

    /**
     * @return mixed
     */
    public function getSt_coordenador()
    {
        return $this->st_coordenador;
    }



}
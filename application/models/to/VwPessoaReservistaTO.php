<?php
/**
 * Classe para encapsular os dados de view de documento de reservista da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaReservistaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Número do documento de reservista.
	 * @var string
	 */
	public $st_numero;
	
	/**
	 * Número da série do documento de reservista.
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Data do documento de reservista.
	 * @var string
	 */
	public $dt_datareservista;
	
	/**
	 * Id do município.
	 * @var int
	 */
	public $id_municipio;
	
	/**
	 * Município do documento de reservista.
	 * @var string
	 */
	public $st_nomemunicipio;
	
	/**
	 * Sigla do UF.
	 * @var string
	 */
	public $sg_uf;
	
	/**
	 * UF do documento de reservista.
	 * @var string
	 */
	public $st_uf;
	
	/**
	 * @return string
	 */
	public function getDt_datareservista() {
		return $this->dt_datareservista;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_numero() {
		return $this->st_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @return string
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}
	
	/**
	 * @return string
	 */
	public function getSt_uf() {
		return $this->st_uf;
	}
	
	/**
	 * @param string $dt_datareservista
	 */
	public function setDt_datareservista($dt_datareservista) {
		$this->dt_datareservista = $dt_datareservista;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string st_numeroo
	 */
	public function setSt_numero($st_numero) {
		$this->st_numero = $st_numero;
	}
	
	/**
	 * @param string st_seriee
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}
	
	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param string $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}
	
	/**
	 * @param string $st_uf
	 */
	public function setSt_uf($st_uf) {
		$this->st_uf = $st_uf;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de Tipo envolvido.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class TipoEnvolvidoTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id do tipo de envolvido
	 * @var int
	 */
	public $id_tipoenvolvido;
	
	/**
	 * Contem o nome do tipo de envolvido
	 * @var string
	 */
	public $st_tipoenvolvido;
	
	/**
	 * Contem a descricao
	 * @var string
	 */
	public $st_descricao;
	/**
	 * @return the $id_tipoenvolvido
	 */
	public function getId_tipoenvolvido() {
		return $this->id_tipoenvolvido;
	}

	/**
	 * @return the $st_tipoenvolvido
	 */
	public function getSt_tipoenvolvido() {
		return $this->st_tipoenvolvido;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param int $id_tipoenvolvido
	 */
	public function setId_tipoenvolvido($id_tipoenvolvido) {
		$this->id_tipoenvolvido = $id_tipoenvolvido;
	}

	/**
	 * @param string $st_tipoenvolvido
	 */
	public function setSt_tipoenvolvido($st_tipoenvolvido) {
		$this->st_tipoenvolvido = $st_tipoenvolvido;
	}

	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
}
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwEntregaDocumentoTO extends Ead1_TO_Dinamico{

	public $id_usuarioaluno;
	public $id_documentos;
	public $st_documentos;
	public $id_tipodocumento;
	public $id_documentosutilizacao;
	public $id_usuario;
	public $st_nomecompleto;
	public $id_contratoresponsavel;
	public $id_entregadocumentos;
	public $id_situacao;
	public $id_projetopedagogico;
	public $id_entidade;
	public $st_situacao;


	/**
	 * @return the $id_usuarioaluno
	 */
	public function getId_usuarioaluno(){ 
		return $this->id_usuarioaluno;
	}

	/**
	 * @return the $id_documentos
	 */
	public function getId_documentos(){ 
		return $this->id_documentos;
	}

	/**
	 * @return the $st_documentos
	 */
	public function getSt_documentos(){ 
		return $this->st_documentos;
	}

	/**
	 * @return the $id_tipodocumento
	 */
	public function getId_tipodocumento(){ 
		return $this->id_tipodocumento;
	}

	/**
	 * @return the $id_documentosutilizacao
	 */
	public function getId_documentosutilizacao(){ 
		return $this->id_documentosutilizacao;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto(){ 
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_contratoresponsavel
	 */
	public function getId_contratoresponsavel(){ 
		return $this->id_contratoresponsavel;
	}

	/**
	 * @return the $id_entregadocumentos
	 */
	public function getId_entregadocumentos(){ 
		return $this->id_entregadocumentos;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao(){ 
		return $this->st_situacao;
	}


	/**
	 * @param field_type $id_usuarioaluno
	 */
	public function setId_usuarioaluno($id_usuarioaluno){ 
		$this->id_usuarioaluno = $id_usuarioaluno;
	}

	/**
	 * @param field_type $id_documentos
	 */
	public function setId_documentos($id_documentos){ 
		$this->id_documentos = $id_documentos;
	}

	/**
	 * @param field_type $st_documentos
	 */
	public function setSt_documentos($st_documentos){ 
		$this->st_documentos = $st_documentos;
	}

	/**
	 * @param field_type $id_tipodocumento
	 */
	public function setId_tipodocumento($id_tipodocumento){ 
		$this->id_tipodocumento = $id_tipodocumento;
	}

	/**
	 * @param field_type $id_documentosutilizacao
	 */
	public function setId_documentosutilizacao($id_documentosutilizacao){ 
		$this->id_documentosutilizacao = $id_documentosutilizacao;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto){ 
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $id_contratoresponsavel
	 */
	public function setId_contratoresponsavel($id_contratoresponsavel){ 
		$this->id_contratoresponsavel = $id_contratoresponsavel;
	}

	/**
	 * @param field_type $id_entregadocumentos
	 */
	public function setId_entregadocumentos($id_entregadocumentos){ 
		$this->id_entregadocumentos = $id_entregadocumentos;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao){ 
		$this->st_situacao = $st_situacao;
	}

}
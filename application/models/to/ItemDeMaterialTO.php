<?php
/**
 * Classe para encapsular os dados de item de material.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ItemDeMaterialTO extends Ead1_TO_Dinamico {

	/**
	 * id do item de material.
	 * @var int
	 */
	public $id_itemdematerial;
	
	/**
	 * Nome do item de material.
	 * @var String
	 */
	public $st_itemdematerial;
	
	/**
	 * Id da situação do item.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Data de cadastro do item.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Id do usuário que cadastrou o item de material.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id do tipo de material do item.
	 * @var int
	 */
	public $id_tipodematerial;
	
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipodematerial() {
		return $this->id_tipodematerial;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return String
	 */
	public function getSt_itemdematerial() {
		return $this->st_itemdematerial;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param int $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_tipodematerial
	 */
	public function setId_tipodematerial($id_tipodematerial) {
		$this->id_tipodematerial = $id_tipodematerial;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param String $st_itemdematerial
	 */
	public function setSt_itemdematerial($st_itemdematerial) {
		$this->st_itemdematerial = $st_itemdematerial;
	}

}

?>
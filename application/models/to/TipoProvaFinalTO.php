<?php
/**
 * Classe para encapsular os dados de prova final.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoProvaFinalTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de prova final.
	 * @var int
	 */
	public $id_tipoprovafinal;
	
	/**
	 * Nome do tipo de prova final.
	 * @var string
	 */
	public $st_tipoprovafinal;
	
	/**
	 * Descrição do tipo de prova final.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tipoprovafinal() {
		return $this->id_tipoprovafinal;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoprovafinal() {
		return $this->st_tipoprovafinal;
	}
	
	/**
	 * @param int $id_tipoprovafinal
	 */
	public function setId_tipoprovafinal($id_tipoprovafinal) {
		$this->id_tipoprovafinal = $id_tipoprovafinal;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipoprovafinal
	 */
	public function setSt_tipoprovafinal($st_tipoprovafinal) {
		$this->st_tipoprovafinal = $st_tipoprovafinal;
	}

}

?>
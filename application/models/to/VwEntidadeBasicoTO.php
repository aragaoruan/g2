<?php
/**
 * Classe do TO da View vw_entidadebasico
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwEntidadeBasicoTO extends Ead1_TO_Dinamico{
	
	public $id_entidade;
	public $st_nomeentidade;
	public $bl_ativo;
	public $id_entidadepai;
	public $id_entidadeclasse;
	public $st_entidadeclasse;
	public $id_situacao;
	public $st_situacao;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_entidadepai
	 */
	public function getId_entidadepai() {
		return $this->id_entidadepai;
	}

	/**
	 * @return the $id_entidadeclasse
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}

	/**
	 * @return the $st_entidadeclasse
	 */
	public function getSt_entidadeclasse() {
		return $this->st_entidadeclasse;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $st_nomeentidade the $st_nomeentidade to set
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $id_entidadepai the $id_entidadepai to set
	 */
	public function setId_entidadepai($id_entidadepai) {
		$this->id_entidadepai = $id_entidadepai;
	}

	/**
	 * @param $id_entidadeclasse the $id_entidadeclasse to set
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}

	/**
	 * @param $st_entidadeclasse the $st_entidadeclasse to set
	 */
	public function setSt_entidadeclasse($st_entidadeclasse) {
		$this->st_entidadeclasse = $st_entidadeclasse;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

}
<?php

/**
 * Classe extendendo os dados de Produto Projeto Pedagogico.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoPedagogicoProdutoTO extends ProdutoProjetoPedagogicoTO {

	public $st_projetopedagogico;
	public $st_tituloexibicao;
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	
	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

}

?>
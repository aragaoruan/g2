<?php

/**
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class NucleoEvolucaoCoTO extends Ead1_TO_Dinamico
{

    public $id_funcao;
    public $id_evolucao;
    public $id_nucleoco;

    /**
     * @return int $id_funcao
     */
    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param int $id_funcao
     */
    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    /**
     * @return int $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int $id_nucleoco
     */
    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }


}

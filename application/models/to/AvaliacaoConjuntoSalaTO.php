<?php
/**
 * Classe para encapsular os dados do conjunto de avaliacao da sala
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoConjuntoSalaTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjuntosala;
	public $id_avaliacaoconjunto;
	public $id_saladeaula;
	public $dt_inicio;
	public $dt_fim;
	
	/**
	 * @return the $id_avaliacaoconjuntosala
	 */
	public function getId_avaliacaoconjuntosala() {
		return $this->id_avaliacaoconjuntosala;
	}

	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntosala
	 */
	public function setId_avaliacaoconjuntosala($id_avaliacaoconjuntosala) {
		$this->id_avaliacaoconjuntosala = $id_avaliacaoconjuntosala;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	
}
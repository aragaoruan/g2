<?php
/**
 * Classe para encapsular os dados da tb_classeafiliadocomissionamento.
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class ClasseAfiliadoComissionamentoTO extends Ead1_TO_Dinamico {
	
	public $id_classeafiliado;
	public $id_comissionamento;
	
	/**
	 * @return the $id_classeafiliado
	 */
	public function getId_classeafiliado() {
		return $this->id_classeafiliado;
	}

	/**
	 * @param field_type $id_classeafiliado
	 */
	public function setId_classeafiliado($id_classeafiliado) {
		$this->id_classeafiliado = $id_classeafiliado;
	}

	/**
	 * @return the $id_comissionamento
	 */
	public function getId_comissionamento() {
		return $this->id_comissionamento;
	}

	/**
	 * @param field_type $id_comissionamento
	 */
	public function setId_comissionamento($id_comissionamento) {
		$this->id_comissionamento = $id_comissionamento;
	}
}

?>
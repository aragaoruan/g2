<?php
/**
 * Classe que encapsula os dados da tb_categoria
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class CategoriaTO extends Ead1_TO_Dinamico
{

    public $id_categoria;
    public $st_nomeexibicao;
    public $st_categoria;
    public $id_usuariocadastro;
    public $id_entidadecadastro;
    public $id_uploadimagem;
    public $id_categoriapai;
    public $id_situacao;
    public $dt_cadastro;
    public $bl_ativo;
    public $ar_categoriasfilha;

    /**
     * @return the $id_categoria
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

    /**
     * @param field_type $id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    /**
     * @return the $st_nomeexibicao
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @param field_type $st_nomeexibicao
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
    }

    /**
     * @return the $st_categoria
     */
    public function getSt_categoria()
    {
        return $this->st_categoria;
    }

    /**
     * @param field_type $st_categoria
     */
    public function setSt_categoria($st_categoria)
    {
        $this->st_categoria = $st_categoria;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return the $id_entidadecadastro
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param field_type $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return the $id_uploadimagem
     */
    public function getId_uploadimagem()
    {
        return $this->id_uploadimagem;
    }

    /**
     * @param field_type $id_uploadimagem
     */
    public function setId_uploadimagem($id_uploadimagem)
    {
        $this->id_uploadimagem = $id_uploadimagem;
    }

    /**
     * @return the $id_categoriapai
     */
    public function getId_categoriapai()
    {
        return $this->id_categoriapai;
    }

    /**
     * @param field_type $id_categoriapai
     */
    public function setId_categoriapai($id_categoriapai)
    {
        $this->id_categoriapai = $id_categoriapai;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return the $ar_categoriasfilha
     */
    public function getAr_categoriasfilha()
    {
        return $this->ar_categoriasfilha;
    }

    /**
     * @param field_type $ar_categoriasfilha
     */
    public function setAr_categoriasfilha($ar_categoriasfilha)
    {
        $this->ar_categoriasfilha = $ar_categoriasfilha;
    }
}

?>
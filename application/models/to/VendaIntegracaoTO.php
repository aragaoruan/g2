<?php
/**
 * Classe para encapsular os dados de aplicações de VendaIntegracao.
 * @author Elcio Mauro Guimarães
 * 
 * @package models
 * @subpackage to
 */
class VendaIntegracaoTO extends Ead1_TO_Dinamico {
	
	/**
	 * Chave
	 * @var int
	 */
	public $id_vendaintegracao;
	
	/**
	 * Identificador do Sistema integrado.
	 * @var int
	 */
	public $id_sistema;
	
	
	/**
	 * Identificador da Venda no G2.
	 * @var int
	 */
	public $id_venda;
	
	
	/**
	 * @var int
	 */
	public $id_usuariocadastro;
	
	
	/**
	 * Identificador da Venda no Sistema de Origem.
	 * @var string
	 */
	public $st_vendaexterna;
	
	
	/**
	 * URL para receber o POST de identificação de conclusão da Venda.
	 * @var string
	 */
	public $st_urlconfirmacao;
	
	
	
	/**
	 * URL do Site de Origem da Venda
	 * @var string
	 */
	public $st_urlvendaexterna;
	
	
	/**
	 * @var date
	 */
	public $dt_cadastro;
	/**
	 * @return the $st_urlconfirmacao
	 */
	public function getSt_urlconfirmacao() {
		return $this->st_urlconfirmacao;
	}

	/**
	 * @param string $st_urlconfirmacao
	 */
	public function setSt_urlconfirmacao($st_urlconfirmacao) {
		$this->st_urlconfirmacao = $st_urlconfirmacao;
	}

	/**
	 * @return the $id_vendaintegracao
	 */
	public function getId_vendaintegracao() {
		return $this->id_vendaintegracao;
	}

	/**
	 * @param number $id_vendaintegracao
	 */
	public function setId_vendaintegracao($id_vendaintegracao) {
		$this->id_vendaintegracao = $id_vendaintegracao;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param number $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param number $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param number $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $st_vendaexterna
	 */
	public function getSt_vendaexterna() {
		return $this->st_vendaexterna;
	}

	/**
	 * @param string $st_vendaexterna
	 */
	public function setSt_vendaexterna($st_vendaexterna) {
		$this->st_vendaexterna = $st_vendaexterna;
	}

	/**
	 * @return the $st_urlvendaexterna
	 */
	public function getSt_urlvendaexterna() {
		return $this->st_urlvendaexterna;
	}

	/**
	 * @param string $st_urlvendaexterna
	 */
	public function setSt_urlvendaexterna($st_urlvendaexterna) {
		$this->st_urlvendaexterna = $st_urlvendaexterna;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}


}

?>
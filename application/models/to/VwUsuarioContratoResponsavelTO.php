<?php
/**
 * TO representativo da view vw_usuariocontratoresponsavel
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class VwUsuarioContratoResponsavelTO extends Ead1_TO_Dinamico{
	
	/**
	 * Contem o id do contrato responsavel
	 * @var int
	 */
	public $id_contratoresponsavel;
	
	/**
	 * Contem o id do usuario
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contem o nome completo do usuario
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Contem o id do contrato
	 * @var int
	 */
	public $id_contrato;
	
	/**
	 * Contem o id do tipo de responsavel do contrato
	 * @var int
	 */
	public $id_tipocontratoresponsavel;
	
	/**
	 * Contem a porcentagem do responsavel
	 * @var numeric
	 */
	public $nu_porcentagem;
	
	/**
	 * Contem o valor se esta ativo como responsavel no contrato
	 * @var int
	 */
	public $bl_ativo;

	/**
	 * Contem o valor do id da venda
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * @return the $id_contratoresponsavel
	 */
	public function getId_contratoresponsavel() {
		return $this->id_contratoresponsavel;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_tipocontratoresponsavel
	 */
	public function getId_tipocontratoresponsavel() {
		return $this->id_tipocontratoresponsavel;
	}

	/**
	 * @return the $nu_porcentagem
	 */
	public function getNu_porcentagem() {
		return $this->nu_porcentagem;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param int $id_contratoresponsavel
	 */
	public function setId_contratoresponsavel($id_contratoresponsavel) {
		$this->id_contratoresponsavel = $id_contratoresponsavel;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param int $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param int $id_tipocontratoresponsavel
	 */
	public function setId_tipocontratoresponsavel($id_tipocontratoresponsavel) {
		$this->id_tipocontratoresponsavel = $id_tipocontratoresponsavel;
	}

	/**
	 * @param numeric $nu_porcentagem
	 */
	public function setNu_porcentagem($nu_porcentagem) {
		$this->nu_porcentagem = $nu_porcentagem;
	}

	/**
	 * @param int $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param $id_venda the $id_venda to set
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}


	
	
	
}
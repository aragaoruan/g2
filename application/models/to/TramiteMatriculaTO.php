<?php
/**
 * Classe para encapsular os dados de Tramite da Matricula
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage to
 */
class TramiteMatriculaTO extends Ead1_TO_Dinamico{

	const SITUACAO = 1;
	const EVOLUCAO = 2;
	const APROVEITAMENTO_DISCIPLINA = 3;
	const MANUAL = 9;
	const TRANSFERENCIA = 11;
    const LIBERARTCC = 15;
    const AUTOMATICO = 22;

	public $id_tramitematricula;
	public $id_tramite;
	public $id_matricula;

	/**
	 * @return the $id_tramitematricula
	 */
	public function getId_tramitematricula() {
		return $this->id_tramitematricula;
	}

	/**
	 * @return the $id_tramite
	 */
	public function getId_tramite() {
		return $this->id_tramite;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_tramitematricula
	 */
	public function setId_tramitematricula($id_tramitematricula) {
		$this->id_tramitematricula = $id_tramitematricula;
	}

	/**
     * @param integer $id_tramite
	 */
	public function setId_tramite($id_tramite) {
		$this->id_tramite = $id_tramite;
	}

	/**
     * @param integer $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

}

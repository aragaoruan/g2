<?php
/**
 * Classe para encapsular os dados da tabela de importacao de pessoa
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class PessoaImportacaoTO extends Ead1_TO_Dinamico{
	
	public	$id_pessoaimportacao;
	public	$id_usuarioimportado;
	public	$id_entidadeimportado;
	public	$id_sistemaimportacao;
	public  $nu_codpessoaorigem;
	
	public  $st_rg;
	public  $st_numerocertidaonascimento;
	public  $st_seriereservista;
	public  $st_numeroeleitor;
	public  $st_zonaeleitor;
	public  $st_telefone;

	public  $nu_codmatricula;
	public  $nu_cpf;
	public  $nu_codetnia;
	public	$nu_codresponsavel;
	public 	$nu_telefone;

	public  $st_numeroreservista;
	public  $st_codestadocivilorigem;
	public  $st_nomecompleto;
	public  $st_sexo;
	public  $st_nomepai;
	public  $st_nomemae;
	public  $st_paisnascimento;
	public  $st_municipionascimento;
	public  $st_passaporte;
	public  $st_orgaoexpeditorrg;
	public  $st_livrocertidaonascimento;
	public  $st_folhacertidaonascimento;
	public  $st_descricaocartorio;
	public  $st_municipioreservista;
	public  $st_areaeleitor;
	public  $st_email;
	public  $st_paisendereco;
	public  $st_bairroendereco;
	public  $st_endereco;
	public  $st_complementoendereco;
	public  $st_numeroendereco;
	public  $st_municipioendereco;
	public  $st_cependereco;
	public  $st_tiposanguineo;
	public  $st_glicemia;
	public  $st_alergias;
	public  $st_necessidadesespeciais;
	public  $st_profissao;
	public  $st_fatorrh;
	
	public  $dt_nascimento;
	public  $dt_dataexpedicaorg;
	public  $dt_dataexpedicaocertidao;
	public  $dt_dataexpedicaoeleitor;

	public  $sg_ufnascimento;
	public  $sg_ufreservista;
	public  $sg_ufendereco;
	/**
	 * @return the $id_pessoaimportacao
	 */
	public function getId_pessoaimportacao() {
		return $this->id_pessoaimportacao;
	}

	/**
	 * @return the $id_usuarioimportado
	 */
	public function getId_usuarioimportado() {
		return $this->id_usuarioimportado;
	}

	/**
	 * @return the $id_entidadeimportado
	 */
	public function getId_entidadeimportado() {
		return $this->id_entidadeimportado;
	}

	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @return the $nu_codpessoaorigem
	 */
	public function getNu_codpessoaorigem() {
		return $this->nu_codpessoaorigem;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getNu_cpf() {
		return $this->nu_cpf;
	}

	/**
	 * @return the $st_sexo
	 */
	public function getSt_sexo() {
		return $this->st_sexo;
	}

	/**
	 * @return the $dt_nascimento
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}

	/**
	 * @return the $st_nomepai
	 */
	public function getSt_nomepai() {
		return $this->st_nomepai;
	}

	/**
	 * @return the $st_nomemae
	 */
	public function getSt_nomemae() {
		return $this->st_nomemae;
	}

	/**
	 * @return the $st_paisnascimento
	 */
	public function getSt_paisnascimento() {
		return $this->st_paisnascimento;
	}

	/**
	 * @return the $sg_ufnascimento
	 */
	public function getSg_ufnascimento() {
		return $this->sg_ufnascimento;
	}

	/**
	 * @return the $st_municipionascimento
	 */
	public function getSt_municipionascimento() {
		return $this->st_municipionascimento;
	}

	/**
	 * @return the $st_passaporte
	 */
	public function getSt_passaporte() {
		return $this->st_passaporte;
	}

	/**
	 * @return the $nu_estadocivilorigem
	 */
	public function getNu_estadocivilorigem() {
		return $this->nu_estadocivilorigem;
	}

	/**
	 * @return the $st_rg
	 */
	public function getSt_rg() {
		return $this->st_rg;
	}

	/**
	 * @return the $st_orgaoexpeditorrg
	 */
	public function getSt_orgaoexpeditorrg() {
		return $this->st_orgaoexpeditorrg;
	}

	/**
	 * @return the $dt_dataexpedicaorg
	 */
	public function getDt_dataexpedicaorg() {
		return $this->dt_dataexpedicaorg;
	}

	/**
	 * @return the $st_numerocertidaonascimento
	 */
	public function getSt_numerocertidaonascimento() {
		return $this->st_numerocertidaonascimento;
	}

	/**
	 * @return the $st_livrocertidaonascimento
	 */
	public function getSt_livrocertidaonascimento() {
		return $this->st_livrocertidaonascimento;
	}

	/**
	 * @return the $st_folhacertidaonascimento
	 */
	public function getSt_folhacertidaonascimento() {
		return $this->st_folhacertidaonascimento;
	}

	/**
	 * @return the $dt_dataexpedicaocertidao
	 */
	public function getDt_dataexpedicaocertidao() {
		return $this->dt_dataexpedicaocertidao;
	}

	/**
	 * @return the $st_descricaocartorio
	 */
	public function getSt_descricaocartorio() {
		return $this->st_descricaocartorio;
	}

	/**
	 * @return the $nu_numeroreservista
	 */
	public function getSt_numeroreservista() {
		return $this->st_numeroreservista;
	}

	/**
	 * @return the $st_seriereservista
	 */
	public function getSt_seriereservista() {
		return $this->st_seriereservista;
	}

	/**
	 * @return the $sg_ufreservista
	 */
	public function getSg_ufreservista() {
		return $this->sg_ufreservista;
	}

	/**
	 * @return the $st_municipioreservista
	 */
	public function getSt_municipioreservista() {
		return $this->st_municipioreservista;
	}

	/**
	 * @return the $st_numeroeleitor
	 */
	public function getSt_numeroeleitor() {
		return $this->st_numeroeleitor;
	}

	/**
	 * @return the $st_zonaeleitor
	 */
	public function getSt_zonaeleitor() {
		return $this->st_zonaeleitor;
	}

	/**
	 * @return the $st_areaeleitor
	 */
	public function getSt_areaeleitor() {
		return $this->st_areaeleitor;
	}

	/**
	 * @return the $dt_dataexpedicaoeleitor
	 */
	public function getDt_dataexpedicaoeleitor() {
		return $this->dt_dataexpedicaoeleitor;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email() {
		return $this->st_email;
	}

	/**
	 * @return the $nu_telefone
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}

	/**
	 * @return the $st_paisendereco
	 */
	public function getSt_paisendereco() {
		return $this->st_paisendereco;
	}

	/**
	 * @return the $st_cependereco
	 */
	public function getSt_cependereco() {
		return $this->st_cependereco;
	}

	/**
	 * @return the $sg_ufendereco
	 */
	public function getSg_ufendereco() {
		return $this->sg_ufendereco;
	}

	/**
	 * @return the $st_bairroendereco
	 */
	public function getSt_bairroendereco() {
		return $this->st_bairroendereco;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @return the $st_complementoendereco
	 */
	public function getSt_complementoendereco() {
		return $this->st_complementoendereco;
	}

	/**
	 * @return the $st_numeroendereco
	 */
	public function getSt_numeroendereco() {
		return $this->st_numeroendereco;
	}

	/**
	 * @return the $st_municipioendereco
	 */
	public function getSt_municipioendereco() {
		return $this->st_municipioendereco;
	}

	/**
	 * @return the $nu_codetnia
	 */
	public function getNu_codetnia() {
		return $this->nu_codetnia;
	}

	/**
	 * @return the $st_tiposanguineo
	 */
	public function getSt_tiposanguineo() {
		return $this->st_tiposanguineo;
	}

	/**
	 * @return the $st_glicemia
	 */
	public function getSt_glicemia() {
		return $this->st_glicemia;
	}

	/**
	 * @return the $st_alergias
	 */
	public function getSt_alergias() {
		return $this->st_alergias;
	}

	/**
	 * @return the $st_necessidadesespeciais
	 */
	public function getSt_necessidadesespeciais() {
		return $this->st_necessidadesespeciais;
	}

	/**
	 * @return the $st_profissao
	 */
	public function getSt_profissao() {
		return $this->st_profissao;
	}

	/**
	 * @param field_type $id_pessoaimportacao
	 */
	public function setId_pessoaimportacao($id_pessoaimportacao) {
		$this->id_pessoaimportacao = $id_pessoaimportacao;
	}

	/**
	 * @param field_type $id_usuarioimportado
	 */
	public function setId_usuarioimportado($id_usuarioimportado) {
		$this->id_usuarioimportado = $id_usuarioimportado;
	}

	/**
	 * @param field_type $id_entidadeimportado
	 */
	public function setId_entidadeimportado($id_entidadeimportado) {
		$this->id_entidadeimportado = $id_entidadeimportado;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}

	/**
	 * @param field_type $nu_codpessoaorigem
	 */
	public function setNu_codpessoaorigem($nu_codpessoaorigem) {
		$this->nu_codpessoaorigem = $nu_codpessoaorigem;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $nu_cpf
	 */
	public function setNu_cpf($nu_cpf) {
		$this->nu_cpf = $nu_cpf;
	}

	/**
	 * @param field_type $st_sexo
	 */
	public function setSt_sexo($st_sexo) {
		$this->st_sexo = $st_sexo;
	}

	/**
	 * @param field_type $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}

	/**
	 * @param field_type $st_nomepai
	 */
	public function setSt_nomepai($st_nomepai) {
		$this->st_nomepai = $st_nomepai;
	}

	/**
	 * @param field_type $st_nomemae
	 */
	public function setSt_nomemae($st_nomemae) {
		$this->st_nomemae = $st_nomemae;
	}

	/**
	 * @param field_type $st_paisnascimento
	 */
	public function setSt_paisnascimento($st_paisnascimento) {
		$this->st_paisnascimento = $st_paisnascimento;
	}

	/**
	 * @param field_type $sg_ufnascimento
	 */
	public function setSg_ufnascimento($sg_ufnascimento) {
		$this->sg_ufnascimento = $sg_ufnascimento;
	}

	/**
	 * @param field_type $st_municipionascimento
	 */
	public function setSt_municipionascimento($st_municipionascimento) {
		$this->st_municipionascimento = $st_municipionascimento;
	}

	/**
	 * @param field_type $st_passaporte
	 */
	public function setSt_passaporte($st_passaporte) {
		$this->st_passaporte = $st_passaporte;
	}

	/**
	 * @param field_type $nu_estadocivilorigem
	 */
	public function setNu_estadocivilorigem($nu_estadocivilorigem) {
		$this->nu_estadocivilorigem = $nu_estadocivilorigem;
	}

	/**
	 * @param field_type st_rgg
	 */
	public function setSt_rg($st_rg) {
		$this->st_rg = $st_rg;
	}

	/**
	 * @param field_type $st_orgaoexpeditorrg
	 */
	public function setSt_orgaoexpeditorrg($st_orgaoexpeditorrg) {
		$this->st_orgaoexpeditorrg = $st_orgaoexpeditorrg;
	}

	/**
	 * @param field_type $dt_dataexpedicaorg
	 */
	public function setDt_dataexpedicaorg($dt_dataexpedicaorg) {
		$this->dt_dataexpedicaorg = $dt_dataexpedicaorg;
	}

	/**
	 * @param field_type st_numerocertidaonascimentoo
	 */
	public function setSt_numerocertidaonascimento($st_numerocertidaonascimento) {
		$this->st_numerocertidaonascimento = $st_numerocertidaonascimento;
	}

	/**
	 * @param field_type $st_livrocertidaonascimento
	 */
	public function setSt_livrocertidaonascimento($st_livrocertidaonascimento) {
		$this->st_livrocertidaonascimento = $st_livrocertidaonascimento;
	}

	/**
	 * @param field_type $st_folhacertidaonascimento
	 */
	public function setSt_folhacertidaonascimento($st_folhacertidaonascimento) {
		$this->st_folhacertidaonascimento = $st_folhacertidaonascimento;
	}

	/**
	 * @param field_type $dt_dataexpedicaocertidao
	 */
	public function setDt_dataexpedicaocertidao($dt_dataexpedicaocertidao) {
		$this->dt_dataexpedicaocertidao = $dt_dataexpedicaocertidao;
	}

	/**
	 * @param field_type $st_descricaocartorio
	 */
	public function setSt_descricaocartorio($st_descricaocartorio) {
		$this->st_descricaocartorio = $st_descricaocartorio;
	}

	/**
	 * @param field_type $nu_numeroreservista
	 */
	public function setSt_numeroreservista($st_numeroreservista) {
		$this->st_numeroreservista = $st_numeroreservista;
	}

	/**
	 * @param field_type st_seriereservistaa
	 */
	public function setSt_seriereservista($st_seriereservista) {
		$this->st_seriereservista = $st_seriereservista;
	}

	/**
	 * @param field_type $sg_ufreservista
	 */
	public function setSg_ufreservista($sg_ufreservista) {
		$this->sg_ufreservista = $sg_ufreservista;
	}

	/**
	 * @param field_type $st_municipioreservista
	 */
	public function setSt_municipioreservista($st_municipioreservista) {
		$this->st_municipioreservista = $st_municipioreservista;
	}

	/**
	 * @param field_type st_numeroeleitorr
	 */
	public function setSt_numeroeleitor($st_numeroeleitor) {
		$this->st_numeroeleitor = $st_numeroeleitor;
	}

	/**
	 * @param field_type st_zonaeleitorr
	 */
	public function setSt_zonaeleitor($st_zonaeleitor) {
		$this->st_zonaeleitor = $st_zonaeleitor;
	}

	/**
	 * @param field_type $st_areaeleitor
	 */
	public function setSt_areaeleitor($st_areaeleitor) {
		$this->st_areaeleitor = $st_areaeleitor;
	}

	/**
	 * @param field_type $dt_dataexpedicaoeleitor
	 */
	public function setDt_dataexpedicaoeleitor($dt_dataexpedicaoeleitor) {
		$this->dt_dataexpedicaoeleitor = $dt_dataexpedicaoeleitor;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}

	/**
	 * @param field_type $nu_telefone
	 */
	public function setNu_telefone( $nu_telefone ) {
		$this->nu_telefone = $nu_telefone;
	}

	/**
	 * @param field_type $st_paisendereco
	 */
	public function setSt_paisendereco($st_paisendereco) {
		$this->st_paisendereco = $st_paisendereco;
	}

	/**
	 * @param field_type $st_cependereco
	 */
	public function setSt_cependereco($st_cependereco) {
		$this->st_cependereco = $st_cependereco;
	}

	/**
	 * @param field_type $sg_ufendereco
	 */
	public function setSg_ufendereco($sg_ufendereco) {
		$this->sg_ufendereco = $sg_ufendereco;
	}

	/**
	 * @param field_type $st_bairroendereco
	 */
	public function setSt_bairroendereco($st_bairroendereco) {
		$this->st_bairroendereco = $st_bairroendereco;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_complementoendereco
	 */
	public function setSt_complementoendereco($st_complementoendereco) {
		$this->st_complementoendereco = $st_complementoendereco;
	}

	/**
	 * @param field_type $st_numeroendereco
	 */
	public function setSt_numeroendereco($st_numeroendereco) {
		$this->st_numeroendereco = $st_numeroendereco;
	}

	/**
	 * @param field_type $st_municipioendereco
	 */
	public function setSt_municipioendereco($st_municipioendereco) {
		$this->st_municipioendereco = $st_municipioendereco;
	}

	/**
	 * @param field_type $nu_codetnia
	 */
	public function setNu_codetnia($nu_codetnia) {
		$this->nu_codetnia = $nu_codetnia;
	}

	/**
	 * @param field_type $st_tiposanguineo
	 */
	public function setSt_tiposanguineo($st_tiposanguineo) {
		$this->st_tiposanguineo = $st_tiposanguineo;
	}

	/**
	 * @param field_type $st_glicemia
	 */
	public function setSt_glicemia($st_glicemia) {
		$this->st_glicemia = $st_glicemia;
	}

	/**
	 * @param field_type $st_alergias
	 */
	public function setSt_alergias($st_alergias) {
		$this->st_alergias = $st_alergias;
	}

	/**
	 * @param field_type $st_necessidadesespeciais
	 */
	public function setSt_necessidadesespeciais($st_necessidadesespeciais) {
		$this->st_necessidadesespeciais = $st_necessidadesespeciais;
	}

	/**
	 * @param field_type $st_profissao
	 */
	public function setSt_profissao($st_profissao) {
		$this->st_profissao = $st_profissao;
	}


}
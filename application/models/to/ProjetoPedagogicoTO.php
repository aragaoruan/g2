<?php

/**
 * Classe para encapsular os dados de projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoPedagogicoTO extends Ead1_TO_Dinamico
{

    public $st_perfilprojeto;
    public $st_coordenacao;
    public $st_horario;
    public $st_publicoalvo;
    public $st_mercadotrabalho;
    public $id_projetopedagogico;
    public $st_descricao;
    public $nu_idademinimainiciar;
    public $bl_ativo;
    public $nu_tempominimoconcluir;
    public $nu_idademinimaconcluir;
    public $nu_tempomaximoconcluir;
    public $nu_tempopreviconcluir;
    public $st_projetopedagogico;
    public $st_tituloexibicao;
    public $bl_autoalocaraluno;
    public $id_situacao;
    public $id_trilha;
    public $id_projetopedagogicoorigem;
    public $st_nomeversao;
    public $id_tipoextracurricular;
    public $nu_notamaxima;
    public $nu_percentualaprovacao;
    public $nu_valorprovafinal;
    public $id_entidadecadastro;
    public $bl_provafinal;
    public $bl_salasemprovafinal;
    public $st_objetivo;
    public $st_estruturacurricular;
    public $st_metodologiaavaliacao;
    public $st_certificacao;
    public $st_valorapresentacao;
    public $nu_cargahoraria;
    public $dt_cadastro;
    public $id_usuariocadastro;
    public $bl_disciplinaextracurricular;
    public $id_usuario;
    public $st_conteudoprogramatico;
    public $id_medidatempoconclusao;
    public $id_contratoregra;
    public $id_livroregistroentidade;
    public $nu_prazoagendamento;
    public $bl_turma;
    public $bl_gradepronta;
    public $dt_criagrade;
    public $id_usuariograde;
    public $id_horarioaula;
    public $bl_disciplinacomplementar;
    public $st_apelido;
    public $id_anexoementa;
    public $bl_enviaremail;
    public $nu_percentualaprovacaovariavel;
    public $bl_renovar;
    public $bl_possuiprova;
    public $st_boasvindascurso;
    public $st_imagem;
    public $nu_semestre;
    public $st_reconhecimento;
    public $st_codprovaintegracao;
    public $st_provaintegracao;
    public $nu_horasatividades;
    public $id_grauacademico;
    public $id_areaconhecimentomec;

    /**
     * Indica se será usado um atendimento virtual como o ChatBot
     * @var boolean
     */
    public $bl_atendimentovirtual;


    /**
     * Identifica qual Projeto Pedagógico pode acessar o Portal
     * @var boolean $bl_novoportal
     */
    public $bl_novoportal;

    /**
     * @return bool
     */
    public function getBl_atendimentovirtual()
    {
        return $this->bl_atendimentovirtual;
    }

    /**
     * @param bool $bl_atendimentovirtual
     */
    public function setBl_atendimentovirtual($bl_atendimentovirtual)
    {
        $this->bl_atendimentovirtual = $bl_atendimentovirtual;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_novoportal()
    {
        return $this->bl_novoportal;
    }

    /**
     * @param bool $bl_novoportal
     * @return ProjetoPedagogicoTO
     */
    public function setBl_novoportal($bl_novoportal)
    {
        $this->bl_novoportal = $bl_novoportal;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getid_anexoementa()
    {
        return $this->id_anexoementa;
    }

    /**
     * @param $id_anexoementa
     * @return $this
     */
    public function setid_anexoementa($id_anexoementa)
    {
        $this->id_anexoementa = $id_anexoementa;
        return $this;

    }

    /**
     * @return integer
     */
    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    /**
     * @param integer $id_horarioaula
     */
    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
    }

    /**
     * @return $st_perfilprojeto
     */
    public function getSt_perfilprojeto()
    {
        return $this->st_perfilprojeto;
    }

    /**
     * @return $st_coordenacao
     */
    public function getSt_coordenacao()
    {
        return $this->st_coordenacao;
    }

    /**
     * @return $st_horario
     */
    public function getSt_horario()
    {
        return $this->st_horario;
    }

    /**
     * @return $st_publicoalvo
     */
    public function getSt_publicoalvo()
    {
        return $this->st_publicoalvo;
    }

    /**
     * @return $st_mercadotrabalho
     */
    public function getSt_mercadotrabalho()
    {
        return $this->st_mercadotrabalho;
    }

    /**
     * @param $st_perfilprojeto
     */
    public function setSt_perfilprojeto($st_perfilprojeto)
    {
        $this->st_perfilprojeto = $st_perfilprojeto;
    }

    /**
     * @param $st_coordenacao
     */
    public function setSt_coordenacao($st_coordenacao)
    {
        $this->st_coordenacao = $st_coordenacao;
    }

    /**
     * @param $st_horario
     */
    public function setSt_horario($st_horario)
    {
        $this->st_horario = $st_horario;
    }

    /**
     * @param $st_publicoalvo
     */
    public function setSt_publicoalvo($st_publicoalvo)
    {
        $this->st_publicoalvo = $st_publicoalvo;
    }

    /**
     * @param $st_mercadotrabalho
     */
    public function setSt_mercadotrabalho($st_mercadotrabalho)
    {
        $this->st_mercadotrabalho = $st_mercadotrabalho;
    }

    /**
     * @return int
     */
    public function getId_livroregistroentidade()
    {
        return $this->id_livroregistroentidade;
    }

    /**
     * @param int $id_livroregistroentidade
     */
    public function setId_livroregistroentidade($id_livroregistroentidade)
    {
        $this->id_livroregistroentidade = $id_livroregistroentidade;
    }

    /**
     * @return int
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param $id_contratoregra
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
    }

    /**
     * @return $id_medidatempoconclusao
     */
    public function getId_medidatempoconclusao()
    {
        return $this->id_medidatempoconclusao;
    }

    /**
     * @param $id_medidatempoconclusao
     */
    public function setId_medidatempoconclusao($id_medidatempoconclusao)
    {
        $this->id_medidatempoconclusao = $id_medidatempoconclusao;
    }

    /**
     * @return $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return $st_descricao
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @return $nu_idademinimainiciar
     */
    public function getNu_idademinimainiciar()
    {
        return $this->nu_idademinimainiciar;
    }

    /**
     * @return $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return $bl_turma
     */
    public function getBl_turma()
    {
        return $this->bl_turma;
    }

    /**
     * @return $nu_tempominimoconcluir
     */
    public function getNu_tempominimoconcluir()
    {
        return $this->nu_tempominimoconcluir;
    }

    /**
     * @return $nu_idademinimaconcluir
     */
    public function getNu_idademinimaconcluir()
    {
        return $this->nu_idademinimaconcluir;
    }

    /**
     * @return $nu_tempomaximoconcluir
     */
    public function getNu_tempomaximoconcluir()
    {
        return $this->nu_tempomaximoconcluir;
    }

    /**
     * @return $nu_tempopreviconcluir
     */
    public function getNu_tempopreviconcluir()
    {
        return $this->nu_tempopreviconcluir;
    }

    /**
     * @return $st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @return $st_tituloexibicao
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @return $bl_autoalocaralunos
     */
    public function getBl_autoalocaraluno()
    {
        return $this->bl_autoalocaraluno;
    }

    /**
     * @return $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return $id_trilha
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @return $id_projetopedagogicoorigem
     */
    public function getId_projetopedagogicoorigem()
    {
        return $this->id_projetopedagogicoorigem;
    }

    /**
     * @return $st_nomeversao
     */
    public function getSt_nomeversao()
    {
        return $this->st_nomeversao;
    }

    /**
     * @return $id_tipoextracurricular
     */
    public function getId_tipoextracurricular()
    {
        return $this->id_tipoextracurricular;
    }

    /**
     * @return $nu_notamaxima
     */
    public function getNu_notamaxima()
    {
        return $this->nu_notamaxima;
    }

    /**
     * @return $nu_percentualaprovacao
     */
    public function getNu_percentualaprovacao()
    {
        return $this->nu_percentualaprovacao;
    }

    /**
     * @return $nu_valorprovafinal
     */
    public function getNu_valorprovafinal()
    {
        return $this->nu_valorprovafinal;
    }

    /**
     * @return $id_entidadecadastro
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @return $bl_provafinal
     */
    public function getBl_provafinal()
    {
        return $this->bl_provafinal;
    }

    /**
     * @return $bl_salasemprovafinal
     */
    public function getBl_salasemprovafinal()
    {
        return $this->bl_salasemprovafinal;
    }

    /**
     * @return $st_objetivo
     */
    public function getSt_objetivo()
    {
        return $this->st_objetivo;
    }

    /**
     * @return $st_estruturacurricular
     */
    public function getSt_estruturacurricular()
    {
        return $this->st_estruturacurricular;
    }

    /**
     * @return $st_metodologiaavaliacao
     */
    public function getSt_metodologiaavaliacao()
    {
        return $this->st_metodologiaavaliacao;
    }

    /**
     * @return $st_certificacao
     */
    public function getSt_certificacao()
    {
        return $this->st_certificacao;
    }

    /**
     * @return $st_valorapresentacao
     */
    public function getSt_valorapresentacao()
    {
        return $this->st_valorapresentacao;
    }

    /**
     * @return $nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @return $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return $bl_disciplinaextracurricular
     */
    public function getBl_disciplinaextracurricular()
    {
        return $this->bl_disciplinaextracurricular;
    }

    /**
     * @return $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return $st_conteudoprogramatico
     */
    public function getSt_conteudoprogramatico()
    {
        return $this->st_conteudoprogramatico;
    }

    /**
     * @param $id_projetopedagogico $id_projetopedagogico to set
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param $st_descricao $st_descricao to set
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @param $nu_idademinimainiciar $nu_idademinimainiciar to set
     */
    public function setNu_idademinimainiciar($nu_idademinimainiciar)
    {
        $this->nu_idademinimainiciar = $nu_idademinimainiciar;
    }

    /**
     * @param $bl_ativo $bl_ativo to set
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param $bl_turma $bl_turma to set
     */
    public function setBl_turma($bl_turma)
    {
        $this->bl_turma = $bl_turma;
    }

    /**
     * @param $nu_tempominimoconcluir $nu_tempominimoconcluir to set
     */
    public function setNu_tempominimoconcluir($nu_tempominimoconcluir)
    {
        $this->nu_tempominimoconcluir = $nu_tempominimoconcluir;
    }

    /**
     * @param $nu_idademinimaconcluir $nu_idademinimaconcluir to set
     */
    public function setNu_idademinimaconcluir($nu_idademinimaconcluir)
    {
        $this->nu_idademinimaconcluir = $nu_idademinimaconcluir;
    }

    /**
     * @param $nu_tempomaximoconcluir $nu_tempomaximoconcluir to set
     */
    public function setNu_tempomaximoconcluir($nu_tempomaximoconcluir)
    {
        $this->nu_tempomaximoconcluir = $nu_tempomaximoconcluir;
    }

    /**
     * @param $nu_tempopreviconcluir $nu_tempopreviconcluir to set
     */
    public function setNu_tempopreviconcluir($nu_tempopreviconcluir)
    {
        $this->nu_tempopreviconcluir = $nu_tempopreviconcluir;
    }

    /**
     * @param $st_projetopedagogico $st_projetopedagogico to set
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @param $st_tituloexibicao $st_tituloexibicao to set
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    /**
     * @param $bl_autoalocaralunos $bl_autoalocaralunos to set
     */
    public function setBl_autoalocaraluno($bl_autoalocaraluno)
    {
        $this->bl_autoalocaraluno = $bl_autoalocaraluno;
    }

    /**
     * @param $id_situacao $id_situacao to set
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param $id_trilha $id_trilha to set
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
    }

    /**
     * @param $id_projetopedagogicoorigem $id_projetopedagogicoorigem to set
     */
    public function setId_projetopedagogicoorigem($id_projetopedagogicoorigem)
    {
        $this->id_projetopedagogicoorigem = $id_projetopedagogicoorigem;
    }

    /**
     * @param $st_nomeversao $st_nomeversao to set
     */
    public function setSt_nomeversao($st_nomeversao)
    {
        $this->st_nomeversao = $st_nomeversao;
    }

    /**
     * @param $id_tipoextracurricular $id_tipoextracurricular to set
     */
    public function setId_tipoextracurricular($id_tipoextracurricular)
    {
        $this->id_tipoextracurricular = $id_tipoextracurricular;
    }

    /**
     * @param $nu_notamaxima $nu_notamaxima to set
     */
    public function setNu_notamaxima($nu_notamaxima)
    {
        $this->nu_notamaxima = $nu_notamaxima;
    }

    /**
     * @param $nu_percentualaprovacao $nu_percentualaprovacao to set
     */
    public function setNu_percentualaprovacao($nu_percentualaprovacao)
    {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
    }

    /**
     * @param $nu_valorprovafinal $nu_valorprovafinal to set
     */
    public function setNu_valorprovafinal($nu_valorprovafinal)
    {
        $this->nu_valorprovafinal = $nu_valorprovafinal;
    }

    /**
     * @param $id_entidadecadastro $id_entidadecadastro to set
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @param $bl_provafinal $bl_provafinal to set
     */
    public function setBl_provafinal($bl_provafinal)
    {
        $this->bl_provafinal = $bl_provafinal;
    }

    /**
     * @param $bl_salasemprovafinal $bl_salasemprovafinal to set
     */
    public function setBl_salasemprovafinal($bl_salasemprovafinal)
    {
        $this->bl_salasemprovafinal = $bl_salasemprovafinal;
    }

    /**
     * @param $st_objetivo $st_objetivo to set
     */
    public function setSt_objetivo($st_objetivo)
    {
        $this->st_objetivo = $st_objetivo;
    }

    /**
     * @param $st_estruturacurricular $st_estruturacurricular to set
     */
    public function setSt_estruturacurricular($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
    }

    /**
     * @param $st_metodologiaavaliacao $st_metodologiaavaliacao to set
     */
    public function setSt_metodologiaavaliacao($st_metodologiaavaliacao)
    {
        $this->st_metodologiaavaliacao = $st_metodologiaavaliacao;
    }

    /**
     * @param $st_certificacao $st_certificacao to set
     */
    public function setSt_certificacao($st_certificacao)
    {
        $this->st_certificacao = $st_certificacao;
    }

    /**
     * @param $st_valorapresentacao $st_valorapresentacao to set
     */
    public function setSt_valorapresentacao($st_valorapresentacao)
    {
        $this->st_valorapresentacao = $st_valorapresentacao;
    }

    /**
     * @param $nu_cargahoraria $nu_cargahoraria to set
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @param $dt_cadastro $dt_cadastro to set
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param $id_usuariocadastro $id_usuariocadastro to set
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param $bl_disciplinaextracurricular $bl_disciplinaextracurricular to set
     */
    public function setBl_disciplinaextracurricular($bl_disciplinaextracurricular)
    {
        $this->bl_disciplinaextracurricular = $bl_disciplinaextracurricular;
    }

    /**
     * @param $id_usuario $id_usuario to set
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param $st_conteudoprogramatico $st_conteudoprogramatico to set
     */
    public function setSt_conteudoprogramatico($st_conteudoprogramatico)
    {
        $this->st_conteudoprogramatico = $st_conteudoprogramatico;
    }

    /**
     * @return $nu_prazoagendamento
     */
    public function getNu_prazoagendamento()
    {
        return $this->nu_prazoagendamento;
    }

    /**
     * @param $nu_prazoagendamento
     */
    public function setNu_prazoagendamento($nu_prazoagendamento)
    {
        $this->nu_prazoagendamento = $nu_prazoagendamento;
    }

    /**
     * @param mixed $bl_gradepronta
     */
    public function setBl_gradepronta($bl_gradepronta)
    {
        $this->bl_gradepronta = $bl_gradepronta;
    }

    /**
     * @return mixed
     */
    public function getBl_gradepronta()
    {
        return $this->bl_gradepronta;
    }

    /**
     * @param mixed $id_usuariograde
     */
    public function setId_usuariograde($id_usuariograde)
    {
        $this->id_usuariograde = $id_usuariograde;
    }

    /**
     * @return mixed
     */
    public function getId_usuariograde()
    {
        return $this->id_usuariograde;
    }

    /**
     * @param mixed $dt_criagrade
     */
    public function setDt_criagrade($dt_criagrade)
    {
        $this->dt_criagrade = $dt_criagrade;
    }

    /**
     * @return mixed
     */
    public function getDt_criagrade()
    {
        return $this->dt_criagrade;
    }

    /**
     * @param mixed $dt_criagrade
     */
    public function setSt_apelido($st_apelido)
    {
        $this->st_apelido = $st_apelido;
    }

    /**
     * @return mixed
     */
    public function getSt_apelido()
    {
        return $this->st_apelido;
    }

    /**
     * @param mixed $bl_disciplinacomplementar
     */
    public function setBl_disciplinacomplementar($bl_disciplinacomplementar)
    {
        $this->bl_disciplinacomplementar = $bl_disciplinacomplementar;
    }

    /**
     * @return mixed
     */
    public function getBl_disciplinacomplementar()
    {
        return $this->bl_disciplinacomplementar;
    }

    /**
     * @return integer
     */
    public function getBl_enviaremail()
    {
        return $this->id_horarioaula;
    }

    /**
     * @param boolean $bl_enviaremail
     */
    public function setBl_enviaremail($bl_enviaremail)
    {
        $this->bl_enviaremail = $bl_enviaremail;
    }

    public function getNu_percentualaprovacaovariavel()
    {
        return $this->nu_percentualaprovacaovariavel;
    }

    public function setNu_percentualaprovacaovariavel($nu_percentualaprovacaovariavel)
    {
        $this->nu_percentualaprovacaovariavel = $nu_percentualaprovacaovariavel;
    }

    public function getBl_renovar()
    {
        return $this->bl_renovar;
    }

    public function setBl_renovar($bl_renovar)
    {
        $this->bl_renovar = $bl_renovar;
    }

    /**
     * @return integer
     */
    public function getBl_possuiprova()
    {
        return $this->bl_possuiprova;
    }

    /**
     * @param boolean $bl_possuiprova
     */
    public function setBl_possuiprova($bl_possuiprova)
    {
        $this->bl_possuiprova = $bl_possuiprova;
    }
    /**
     * @return mixed
     */
    public function getSt_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param mixed $st_imagem
     */
    public function setSt_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
    }

    /**
     * @return mixed
     */
    public function getSt_boasvindascurso()
    {
        return $this->st_boasvindascurso;
    }

    /**
     * @param mixed $st_boasvindascurso
     */
    public function setSt_boasvindascurso($st_boasvindascurso)
    {
        $this->st_boasvindascurso = $st_boasvindascurso;
    }

    public function getNu_semestre()
    {
        return $this->nu_semestre;
    }

    public function setNu_semestre($nu_semestre)
    {
        $this->nu_semestre = $nu_semestre;
    }

    /**
     * @return mixed
     */
    public function getSt_reconhecimento()
    {
        return $this->st_reconhecimento;
    }

    /**
     * @param mixed $st_reconhecimento
     */
    public function setSt_reconhecimento($st_reconhecimento)
    {
        $this->st_reconhecimento = $st_reconhecimento;
    }

    /**
     * @return int
     */
    public function getNu_horasatividades()
    {
        return $this->nu_horasatividades;
    }

    /**
     * @return mixed
     */
    public function getSt_codprovaintegracao()
    {
        return $this->st_codprovaintegracao;
    }

    /**
     * @param mixed $st_codprovaintegracao
     */
    public function setSt_codprovaintegracao($st_codprovaintegracao)
    {
        $this->st_codprovaintegracao = $st_codprovaintegracao;
    }

    /**
     * @return mixed
     */
    public function getSt_provaintegracao()
    {
        return $this->st_provaintegracao;
    }

    /**
     * @param mixed $st_provaintegracao
     */
    public function setSt_provaintegracao($st_provaintegracao)
    {
        $this->st_provaintegracao = $st_provaintegracao;
    }

    /**
     * @param int $nu_horasatividades
     * @return $this
     */
    public function setNu_horasatividades($nu_horasatividades)
    {
        $this->nu_horasatividades = $nu_horasatividades;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_grauacademico()
    {
        return $this->id_grauacademico;
    }

    /**
     * @param integer $id_grauacademico
     */
    public function setId_grauacademico($id_grauacademico)
    {
        $this->id_grauacademico = $id_grauacademico;
    }

    /**
     * @return integer
     */
    public function getId_areaconhecimentomec()
    {
        return $this->id_areaconhecimentomec;
    }

    /**
     * @param integer $id_areaconhecimentomec
     * @return $this;
     */
    public function setId_areaconhecimentomec($id_areaconhecimentomec)
    {
        $this->id_areaconhecimentomec = $id_areaconhecimentomec;
        return $this;
    }

}

<?php
/**
 * Classe para encapsular os dados de EntidadeEmail.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class EntidadeEmailTO extends Ead1_TO_Dinamico{
	
	public $id_entidadeemail;
	public $id_entidade;
	public $id_emailconfig;
	public $st_email;
	
	/**
	 * @return the $id_entidadeemail
	 */
	public function getId_entidadeemail() {
		return $this->id_entidadeemail;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email() {
		return $this->st_email;
	}

	/**
	 * @param field_type $id_entidadeemail
	 */
	public function setId_entidadeemail($id_entidadeemail) {
		$this->id_entidadeemail = $id_entidadeemail;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}
}
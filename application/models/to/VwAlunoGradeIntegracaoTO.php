<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwAlunoGradeIntegracaoTO extends Ead1_TO_Dinamico
{

    public $dt_abertura;
    public $id_usuario;
    public $st_nomecompleto;
    public $st_cpf;
    public $st_nomeexibicao;
    public $st_urlavatar;
    public $id_matricula;
    public $id_projetopedagogico;
    public $st_tituloexibicaoprojeto;
    public $id_modulo;
    public $st_tituloexibicaomodulo;
    public $id_disciplina;
    public $st_disciplina;
    public $st_imagemdisciplina;
    public $id_saladeaula;
    public $st_codusuario;
    public $st_loginintegrado;
    public $st_senhaintegrada;
    public $st_codsistemacurso;
    public $st_codalocacao;
    public $id_sistema;
    public $st_codsistemaent;
    public $st_integracao;
    public $dt_encerramento;
    public $st_caminho;
    public $st_professor;
    public $id_status;
    public $st_status;
    public $st_coordenador;
    public $dt_encerramentosala;
    public $id_situacaotcc;
    public $id_entidade;
    public $id_tipodisciplina;
    public $st_saladeaula;
    public $id_categoriasala;
    public $bl_encerrado;
    public $id_alocacao;
    public $id_evolucao;
    public $id_aproparcial;
    public $nu_diasacesso;
    public $id_entidadesala;
    public $id_entidadeintegracao;
    public $nu_ordenacao;

    /**
     * @return mixed
     */
    public function getst_imagemdisciplina()
    {
        return $this->st_imagemdisciplina;
    }

    /**
     * @param mixed $st_imagemdisciplina
     */
    public function setst_imagemdisciplina($st_imagemdisciplina)
    {
        $this->st_imagemdisciplina = $st_imagemdisciplina;
        return $this;

    }

    /**
     * @return the $st_caminho
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param field_type $st_caminho
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @return the $st_nomeexibicao
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @return the $st_urlavatar
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return the $st_tituloexibicaoprojeto
     */
    public function getSt_tituloexibicaoprojeto()
    {
        return $this->st_tituloexibicaoprojeto;
    }

    /**
     * @return the $id_modulo
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @return the $st_tituloexibicaomodulo
     */
    public function getSt_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return the $st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @return the $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @return the $st_codusuario
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @return the $st_loginintegrado
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @return the $st_senhaintegrada
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @return the $st_codsistemacurso
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @return the $st_codalocacao
     */
    public function getSt_codalocacao()
    {
        return $this->st_codalocacao;
    }

    /**
     * @return the $id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @return the $st_codsistemaent
     */
    public function getSt_codsistemaent()
    {
        return $this->st_codsistemaent;
    }

    /**
     * @return the $st_integracao
     */
    public function getSt_integracao()
    {
        return $this->st_integracao;
    }

    /**
     * @return the $dt_abertura
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @return the $dt_encerramento
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param field_type $st_nomeexibicao
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
    }

    /**
     * @param field_type $st_urlavatar
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param field_type $st_tituloexibicaoprojeto
     */
    public function setSt_tituloexibicaoprojeto($st_tituloexibicaoprojeto)
    {
        $this->st_tituloexibicaoprojeto = $st_tituloexibicaoprojeto;
    }

    /**
     * @param field_type $id_modulo
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
    }

    /**
     * @param field_type $st_tituloexibicaomodulo
     */
    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
    }

    /**
     * @param field_type $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param field_type $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @param field_type $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @param field_type $st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
    }

    /**
     * @param field_type $st_loginintegrado
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
    }

    /**
     * @param field_type $st_senhaintegrada
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
    }

    /**
     * @param field_type $st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }

    /**
     * @param field_type $st_codalocacao
     */
    public function setSt_codalocacao($st_codalocacao)
    {
        $this->st_codalocacao = $st_codalocacao;
    }

    /**
     * @param field_type $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @param field_type $st_codsistemaent
     */
    public function setSt_codsistemaent($st_codsistemaent)
    {
        $this->st_codsistemaent = $st_codsistemaent;
    }

    /**
     * @param field_type $st_integracao
     */
    public function setSt_integracao($st_integracao)
    {
        $this->st_integracao = $st_integracao;
    }

    /**
     * @param field_type $dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    /**
     * @param field_type $dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    /**
     * @return the $st_professor
     */
    public function getSt_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param field_type $st_professor
     */
    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    /**
     * @return the $id_status
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @return the $st_status
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param field_type $id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
    }

    /**
     * @param field_type $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }

    /**
     * @return the $st_coordenador
     */
    public function getSt_coordenador()
    {
        return $this->st_coordenador;
    }

    /**
     * @param field_type $st_coordenador
     */
    public function setSt_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
    }

    /**
     * @return the $dt_encerramentosala
     */
    public function getDt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    /**
     * @param field_type $dt_encerramentosala
     */
    public function setDt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
    }

    /**
     * @param field_type $id_situacaotcc
     */
    public function setId_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
    }

    /**
     * @return the $id_situacaotcc
     */
    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return the $id_tipodisciplina
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param field_type $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return the $st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param field_type $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
    }

    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param mixed $bl_aproparcial
     */
    public function setId_aproparcial($id_aproparcial)
    {
        $this->id_aproparcial = $id_aproparcial;
    }

    /**
     * @return mixed
     */
    public function getId_aproparcial()
    {
        return $this->id_aproparcial;
    }

    /**
     * @return mixed
     */
    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    /**
     * @param mixed $nu_diasacesso
     */
    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
    }

    public function getId_entidadesala()
    {
        return $this->id_entidadesala;
    }

    public function setId_entidadesala($id_entidadesala)
    {
        $this->id_entidadesala = $id_entidadesala;
    }

    /**
     * @return mixed
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param mixed $id_entidadeintegracao
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
    }

    /**
     * @return mixed
     */
    public function getNu_ordenacao()
    {
        return $this->nu_ordenacao;
    }

    /**
     * @param mixed $nu_ordenacao
     */
    public function setNu_ordenacao($nu_ordenacao)
    {
        $this->nu_ordenacao = $nu_ordenacao;
        return $this;
    }
}

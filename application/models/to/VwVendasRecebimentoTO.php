<?php

/**
 * Classe para encapsular os dados de VwVendasRecebimento.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-11-01
 * @package models
 * @subpackage to
 */
class VwVendasRecebimentoTO extends Ead1_TO_Dinamico
{

    public $id_venda;

    public $id_usuario;

    public $st_nomecompleto;

    public $id_entidade;

    public $id_formapagamentoaplicacao;


    /**
     * @return the $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param $st_nomecompleto the $st_nomecompleto to set
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return integer
     */
    public function getId_formapagamentoaplicacao()
    {
        return $this->id_formapagamentoaplicacao;
    }

    /**
     * @param integer $id_formapagamentoaplicacao
     */
    public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao)
    {
        $this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
    }
}
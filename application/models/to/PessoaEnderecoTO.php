<?php
/**
 * Classe para encapsular os dados de endereço da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class PessoaEnderecoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do endereço.
	 * @var int
	 */
	public $id_endereco;
	
	/**
	 * Diz se o endereço é ou não padrão.
	 * @var Boolean
	 */
	public $bl_padrao = false;
	
	/**
	 * @return int
	 */
	public function getId_endereco() {
		return $this->id_endereco;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return Boolean
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}
	
	/**
	 * @param Boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	/**
	 * @param int $id_endereco
	 */
	public function setId_endereco($id_endereco) {
		$this->id_endereco = $id_endereco;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

}

?>
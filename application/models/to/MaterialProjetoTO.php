<?php
/**
 * Classe para encapsular os dados de projeto e material
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class MaterialProjetoTO extends Ead1_TO_Dinamico {
	
	public $id_materialprojeto;
	public $id_projetopedagogico;
	public $id_usuariocadastro;
	public $id_itemdematerial;
	public $dt_cadastro;
	
	/**
	 * @return the $id_materialprojeto
	 */
	public function getId_materialprojeto() {
		return $this->id_materialprojeto;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_materialprojeto
	 */
	public function setId_materialprojeto($id_materialprojeto) {
		$this->id_materialprojeto = $id_materialprojeto;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
}
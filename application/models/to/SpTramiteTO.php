<?php
/**
 * Classe que encapsula os dados retornados pela sp_tramite
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class SpTramiteTO extends Ead1_TO_Dinamico{
	
	public $st_tramite;
	public $id_tipotramite;
	public $st_tipotramite;
	public $id_categoriatramite;
	public $st_categoriatramite;
	public $id_usuario;
	public $id_entidade;
	public $dt_cadastro;
	public $id_campo;
	
	/**
	 * @return the $st_tramite
	 */
	public function getSt_tramite() {
		return $this->st_tramite;
	}

	/**
	 * @return the $id_tipotramite
	 */
	public function getId_tipotramite() {
		return $this->id_tipotramite;
	}

	/**
	 * @return the $st_tipotramite
	 */
	public function getSt_tipotramite() {
		return $this->st_tipotramite;
	}

	/**
	 * @return the $id_categoriatramite
	 */
	public function getId_categoriatramite() {
		return $this->id_categoriatramite;
	}

	/**
	 * @return the $st_categoriatramite
	 */
	public function getSt_categoriatramite() {
		return $this->st_categoriatramite;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $id_campo
	 */
	public function getId_campo() {
		return $this->id_campo;
	}

	/**
	 * @param $st_tramite the $st_tramite to set
	 */
	public function setSt_tramite($st_tramite) {
		$this->st_tramite = $st_tramite;
	}

	/**
	 * @param $id_tipotramite the $id_tipotramite to set
	 */
	public function setId_tipotramite($id_tipotramite) {
		$this->id_tipotramite = $id_tipotramite;
	}

	/**
	 * @param $st_tipotramite the $st_tipotramite to set
	 */
	public function setSt_tipotramite($st_tipotramite) {
		$this->st_tipotramite = $st_tipotramite;
	}

	/**
	 * @param $id_categoriatramite the $id_categoriatramite to set
	 */
	public function setId_categoriatramite($id_categoriatramite) {
		$this->id_categoriatramite = $id_categoriatramite;
	}

	/**
	 * @param $st_categoriatramite the $st_categoriatramite to set
	 */
	public function setSt_categoriatramite($st_categoriatramite) {
		$this->st_categoriatramite = $st_categoriatramite;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $id_campo the $id_campo to set
	 */
	public function setId_campo($id_campo) {
		$this->id_campo = $id_campo;
	}

}
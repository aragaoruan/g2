<?php

/**
 * Classe para encapsular os dados da tb_logacesso
 * @author denisexavier
 * @package models
 * @subpackage to
 */
class LogAcessoTO extends Ead1_TO
{

    const PRINCIPAL = 312;
    const ALUNO = 313;
    const CENTRAL_DE_ATENCAO = 314;
    const AUTOATENDIMENTO = 316;
    const MEUS_DADOS = 317;
    const GRADE_DE_NOTAS = 319;
    const SERVICO_DE_ATENDIMENTO_AO_ALUNO = 325;
    const ALOCACAO = 329;
    const AGENDAMENTO_DE_AVALIACAO = 330;
    const SELECIONAR_PERFIL = 339;
    const DECLARACAO = 393;
    const CRONOGRAMA = 447;
    const ACESSO_SALA_DE_AULA = 448;


    public $id_logacesso;
    public $id_funcionalidade;
    public $id_perfil;
    public $id_usuario;
    public $id_saladeaula;
    public $id_entidade;
    public $dt_cadastro;


    /**
     * @return int $id_logacesso
     */
    public function getId_logacesso()
    {
        return $this->id_logacesso;
    }

    /**
     * @param int $id_logacesso
     */
    public function setId_logacesso($id_logacesso)
    {
        $this->id_logacesso = $id_logacesso;
    }

    /**
     * @return int $id_funcionalidade
     */
    public function getId_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @param int $id_funcionalidade
     */
    public function setId_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
    }

    /**
     * @return int $id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return int $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return Zend_Date $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param Zend_Date $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }


}

?>
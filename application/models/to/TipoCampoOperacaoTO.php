<?php
/**
 * Classe para encapsular os dados da tabela de vínculos de operação com tipos de campos do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class TipoCampoOperacaoTO extends Ead1_TO_Dinamico{
	
	public $id_tipocampo;
	public $st_classeflex;
	public $id_operacaorelatorio;
	
	/**
	 * @return the $id_tipocampo
	 */
	public function getId_tipocampo() {
		return $this->id_tipocampo;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @return the $id_operacaorelatorio
	 */
	public function getId_operacaorelatorio() {
		return $this->id_operacaorelatorio;
	}

	/**
	 * @param $id_tipocampo the $id_tipocampo to set
	 */
	public function setId_tipocampo($id_tipocampo) {
		$this->id_tipocampo = $id_tipocampo;
	}

	/**
	 * @param $st_classeflex the $st_classeflex to set
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

	/**
	 * @param $id_operacaorelatorio the $id_operacaorelatorio to set
	 */
	public function setId_operacaorelatorio($id_operacaorelatorio) {
		$this->id_operacaorelatorio = $id_operacaorelatorio;
	}
	
}
<?php
/**
 * Classe para encapsular os dados de relação entre documento e sua categoria.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentosCategoriaRelacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da categoria.
	 * @var int
	 */
	public $id_documentoscategoria;
	
	/**
	 * Id de documentos.
	 * @var int
	 */
	public $id_documentos;
	
	/**
	 * @return int
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}
	
	/**
	 * @return int
	 */
	public function getId_documentoscategoria() {
		return $this->id_documentoscategoria;
	}
	
	/**
	 * @param int $id_documentos
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}
	
	/**
	 * @param int $id_documentoscategoria
	 */
	public function setId_documentoscategoria($id_documentoscategoria) {
		$this->id_documentoscategoria = $id_documentoscategoria;
	}

}

?>
<?php

class PacoteTO extends Ead1_TO_Dinamico {

	public $id_pacote;
	public $id_usuariocadastro;
	public $id_matricula;
	public $id_remessa;
	public $dt_cadastro;
	/**
	 * @return the $id_pacote
	 */
	public function getId_pacote() {
		return $this->id_pacote;
	}

	/**
	 * @param field_type $id_pacote
	 */
	public function setId_pacote($id_pacote) {
		$this->id_pacote = $id_pacote;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_remessa
	 */
	public function getId_remessa() {
		return $this->id_remessa;
	}

	/**
	 * @param field_type $id_remessa
	 */
	public function setId_remessa($id_remessa) {
		$this->id_remessa = $id_remessa;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
	
}

?>
<?php
/**
 * Classe para encapsular os dados de trilha.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TrilhaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da trilha.
	 * @var int
	 */
	public $id_trilha;
	
	/**
	 * Contém o id do tipo de trilha.
	 * @var int
	 */
	public $id_tipotrilha;
	
	/**
	 * Número de disciplinas simultâneas. Menor de 18 anos.
	 * @var int
	 */
	public $nu_disciplinassimultaneasmen;
	
	/**
	 * Número de disciplinas simultâneas. Maior de 18 anos.
	 * @var int
	 */
	public $nu_disciplinassimultaneasmai;
	
	/**
	 * Id do tipo de trilha fixa, se for fixa.
	 * @var int
	 */
	public $id_tipotrilhafixa;
	
	
	public $nu_disciplinaspendencia;
	
	/**
	 * @return int
	 */
	public function getId_tipotrilhafixa() {
		return $this->id_tipotrilhafixa;
	}
	
	/**
	 * @param int $id_tipotrilhafixa
	 */
	public function setId_tipotrilhafixa($id_tipotrilhafixa) {
		$this->id_tipotrilhafixa = $id_tipotrilhafixa;
	}
	/**
	 * @return int
	 */
	public function getId_tipotrilha() {
		return $this->id_tipotrilha;
	}
	
	/**
	 * @return int
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}
	
	/**
	 * @return int
	 */
	public function getNu_disciplinassimultaneasmai() {
		return $this->nu_disciplinassimultaneasmai;
	}
	
	/**
	 * @return int
	 */
	public function getNu_disciplinassimultaneasmen() {
		return $this->nu_disciplinassimultaneasmen;
	}
	
	/**
	 * @param int $id_tipotrilha
	 */
	public function setId_tipotrilha($id_tipotrilha) {
		$this->id_tipotrilha = $id_tipotrilha;
	}
	
	/**
	 * @param int $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}
	
	/**
	 * @param int $nu_disciplinassimultaneasmai
	 */
	public function setNu_disciplinassimultaneasmai($nu_disciplinassimultaneasmai) {
		$this->nu_disciplinassimultaneasmai = $nu_disciplinassimultaneasmai;
	}
	
	/**
	 * @param int $nu_disciplinassimultaneasmen
	 */
	public function setNu_disciplinassimultaneasmen($nu_disciplinassimultaneasmen) {
		$this->nu_disciplinassimultaneasmen = $nu_disciplinassimultaneasmen;
	}
	/**
	 * @return the $nu_disciplinaspendencia
	 */
	public function getNu_disciplinaspendencia() {
		return $this->nu_disciplinaspendencia;
	}

	/**
	 * @param field_type $nu_disciplinaspendencia
	 */
	public function setNu_disciplinaspendencia($nu_disciplinaspendencia) {
		$this->nu_disciplinaspendencia = $nu_disciplinaspendencia;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados de view de entrega de documento na matricula.
 * @author Eder Lamar Mariano - edermariano@gmail.com
 * @package models
 * @subpackage to
 */
class VwMatriculaEntregaDocumentoTO extends Ead1_TO_Dinamico{
	
	public $id_documentos;
	public $st_documentos;
	public $id_tipodocumento;
	public $id_documentosutilizacao;
	public $id_usuario;
	public $st_nomecompleto;
	public $id_matricula;
	public $id_contrato;
	public $id_entregadocumentos;
	public $id_situacao;
	public $id_projetopedagogico;
	public $id_contratoresponsavel;
	public $st_situacao;
	
	/**
	 * @return unknown
	 */
	public function getId_contratoresponsavel() {
		return $this->id_contratoresponsavel;
	}
	
	/**
	 * @param unknown_type $id_contratoresponsavel
	 */
	public function setId_contratoresponsavel($id_contratoresponsavel) {
		$this->id_contratoresponsavel = $id_contratoresponsavel;
	}

	
	/**
	 * @return the $id_documentos
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}

	/**
	 * @return the $st_documentos
	 */
	public function getSt_documentos() {
		return $this->st_documentos;
	}

	/**
	 * @return the $id_tipodocumento
	 */
	public function getId_tipodocumento() {
		return $this->id_tipodocumento;
	}

	/**
	 * @return the $id_documentosutilizacao
	 */
	public function getId_documentosutilizacao() {
		return $this->id_documentosutilizacao;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompletoaluno
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}


	/**
	 * @return the $id_entregadocumentos
	 */
	public function getId_entregadocumentos() {
		return $this->id_entregadocumentos;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param $id_documentos the $id_documentos to set
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}

	/**
	 * @param $st_documentos the $st_documentos to set
	 */
	public function setSt_documentos($st_documentos) {
		$this->st_documentos = $st_documentos;
	}

	/**
	 * @param $id_tipodocumento the $id_tipodocumento to set
	 */
	public function setId_tipodocumento($id_tipodocumento) {
		$this->id_tipodocumento = $id_tipodocumento;
	}

	/**
	 * @param $id_documentosutilizacao the $id_documentosutilizacao to set
	 */
	public function setId_documentosutilizacao($id_documentosutilizacao) {
		$this->id_documentosutilizacao = $id_documentosutilizacao;
	}

	/**
	 * @param $id_usuarioaluno the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $st_nomecompleto the $st_nomecompleto to set
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param $id_matricula the $id_matricula to set
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param $id_contrato the $id_contrato to set
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}


	/**
	 * @param $id_entregadocumentos the $id_entregadocumentos to set
	 */
	public function setId_entregadocumentos($id_entregadocumentos) {
		$this->id_entregadocumentos = $id_entregadocumentos;
	}


	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}


}
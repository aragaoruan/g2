<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwEnvioMensagemTO extends Ead1_TO_Dinamico{

	public $id_enviomensagem;
	public $id_mensagem;
	public $id_tipoenvio;
	public $id_emailconfig;
	public $id_evolucao;
	public $dt_cadastro;
	public $dt_envio;
	public $dt_enviar;
	public $dt_tentativa;
	public $id_enviodestinatario;
	public $id_usuario;
	public $st_nome;
	public $st_endereco;
	public $id_tipodestinatario;
	public $st_mensagem;
	public $st_texto;
	public $st_evolucao;
	public $st_tipoenvio;
        public $st_caminhoanexo;
        public $id_entidade;
        public $nu_telefone;


	/**
	 * @return the $id_enviomensagem
	 */
	public function getId_enviomensagem(){ 
		return $this->id_enviomensagem;
	}

	/**
	 * @return the $id_mensagem
	 */
	public function getId_mensagem(){ 
		return $this->id_mensagem;
	}

	/**
	 * @return the $id_tipoenvio
	 */
	public function getId_tipoenvio(){ 
		return $this->id_tipoenvio;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao(){ 
		return $this->id_evolucao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $dt_envio
	 */
	public function getDt_envio(){ 
		return $this->dt_envio;
	}

	/**
	 * @return the $dt_enviar
	 */
	public function getDt_enviar(){ 
		return $this->dt_enviar;
	}

	/**
	 * @return the $dt_tentativa
	 */
	public function getDt_tentativa(){ 
		return $this->dt_tentativa;
	}

	/**
	 * @return the $id_enviodestinatario
	 */
	public function getId_enviodestinatario(){ 
		return $this->id_enviodestinatario;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nome
	 */
	public function getSt_nome(){ 
		return $this->st_nome;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco(){ 
		return $this->st_endereco;
	}

	/**
	 * @return the $id_tipodestinatario
	 */
	public function getId_tipodestinatario(){ 
		return $this->id_tipodestinatario;
	}

	/**
	 * @return the $st_mensagem
	 */
	public function getSt_mensagem(){ 
		return $this->st_mensagem;
	}

	/**
	 * @return the $st_texto
	 */
	public function getSt_texto(){ 
		return $this->st_texto;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao(){ 
		return $this->st_evolucao;
	}

	/**
	 * @return the $st_tipoenvio
	 */
	public function getSt_tipoenvio(){ 
		return $this->st_tipoenvio;
	}


	/**
	 * @param field_type $id_enviomensagem
	 */
	public function setId_enviomensagem($id_enviomensagem){ 
		$this->id_enviomensagem = $id_enviomensagem;
	}

	/**
	 * @param field_type $id_mensagem
	 */
	public function setId_mensagem($id_mensagem){ 
		$this->id_mensagem = $id_mensagem;
	}

	/**
	 * @param field_type $id_tipoenvio
	 */
	public function setId_tipoenvio($id_tipoenvio){ 
		$this->id_tipoenvio = $id_tipoenvio;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao){ 
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $dt_envio
	 */
	public function setDt_envio($dt_envio){ 
		$this->dt_envio = $dt_envio;
	}

	/**
	 * @param field_type $dt_enviar
	 */
	public function setDt_enviar($dt_enviar){ 
		$this->dt_enviar = $dt_enviar;
	}

	/**
	 * @param field_type $dt_tentativa
	 */
	public function setDt_tentativa($dt_tentativa){ 
		$this->dt_tentativa = $dt_tentativa;
	}

	/**
	 * @param field_type $id_enviodestinatario
	 */
	public function setId_enviodestinatario($id_enviodestinatario){ 
		$this->id_enviodestinatario = $id_enviodestinatario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_nome
	 */
	public function setSt_nome($st_nome){ 
		$this->st_nome = $st_nome;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco){ 
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $id_tipodestinatario
	 */
	public function setId_tipodestinatario($id_tipodestinatario){ 
		$this->id_tipodestinatario = $id_tipodestinatario;
	}

	/**
	 * @param field_type $st_mensagem
	 */
	public function setSt_mensagem($st_mensagem){ 
		$this->st_mensagem = $st_mensagem;
	}

	/**
	 * @param field_type $st_texto
	 */
	public function setSt_texto($st_texto){ 
		$this->st_texto = $st_texto;
	}

	/**
	 * @param field_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao){ 
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @param field_type $st_tipoenvio
	 */
	public function setSt_tipoenvio($st_tipoenvio){ 
		$this->st_tipoenvio = $st_tipoenvio;
	}
	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}
        
        /**
         * @param mixed $st_caminhoanexo
         */
        public function setStCaminhoanexo($st_caminhoanexo)
        {
            $this->st_caminhoanexo = $st_caminhoanexo;
        }

        /**
         * @return mixed
         */
        public function getStCaminhoanexo()
        {
            return $this->st_caminhoanexo;
        }

        /**
	 * @return the $id_entidade
	 */
        public function getId_entidade()
        {
            return $this->id_entidade;
        }
        
        /**
	 * @return the $nu_telefone
	 */
        public function getNu_telefone()
        {
            return $this->nu_telefone;
        }
        
        /**
	 * @param field_type $id_entidade
	 */
        public function setId_entidade($id_entidade)
        {
            $this->id_entidade = $id_entidade;
        }
        
        /**
	 * @param field_type $nu_telefone
	 */
        public function setNu_telefone($nu_telefone)
        {
            $this->nu_telefone = $nu_telefone;
        }
    
}

<?php
/**
 * Classe para encapsular os dados de Contrato Envolvido.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContratoEnvolvidoTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id do tipo de envolvido no contrato
	 * @var int
	 */
	public $id_tipoenvolvido;
	
	/**
	 * Contem o id do contrato envolvido
	 * @var int
	 */
	public $id_contrato;
	
	/**
	 * Contem o id do usuario
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	/**
	 * @return the $id_tipoenvolvido
	 */
	public function getId_tipoenvolvido() {
		return $this->id_tipoenvolvido;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato() {
		return $this->id_contrato;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param int $id_tipoenvolvido
	 */
	public function setId_tipoenvolvido($id_tipoenvolvido) {
		$this->id_tipoenvolvido = $id_tipoenvolvido;
	}

	/**
	 * @param int $id_contrato
	 */
	public function setId_contrato($id_contrato) {
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}
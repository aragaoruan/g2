<?php
/**
 * Classe para encapsular os dados de tipo de trilha de fixa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoTrilhaFixaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do tipo de trilha fixa.
	 * @var int
	 */
	public $id_tipotrilhafixa;
	
	/**
	 * Contém o tipo de trilha fixa.
	 * @var string
	 */
	public $st_tipotrilhafixa;
	
	/**
	 * Contém a descrição do tipo de trilha fixa.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Id do tipo de trilha.
	 * @var int
	 */
	public $id_tipotrilha;
	
	/**
	 * @return int
	 */
	public function getId_tipotrilha() {
		return $this->id_tipotrilha;
	}
	
	/**
	 * @param int $id_tipotrilha
	 */
	public function setId_tipotrilha($id_tipotrilha) {
		$this->id_tipotrilha = $id_tipotrilha;
	}
	/**
	 * @return int
	 */
	public function getId_tipotrilhafixa() {
		return $this->id_tipotrilhafixa;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipotrilhafixa() {
		return $this->st_tipotrilhafixa;
	}
	
	/**
	 * @param int $id_tipotrilhafixa
	 */
	public function setId_tipotrilhafixa($id_tipotrilhafixa) {
		$this->id_tipotrilhafixa = $id_tipotrilhafixa;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipotrilhafixa
	 */
	public function setSt_tipotrilhafixa($st_tipotrilhafixa) {
		$this->st_tipotrilhafixa = $st_tipotrilhafixa;
	}

}

?>
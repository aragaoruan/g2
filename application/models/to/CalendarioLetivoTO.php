<?php
/**
 * Classe para encapsular os dados de calendário letivo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CalendarioLetivoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do calendário letivo.
	 * @var int
	 */
	public $id_calendarioletivo;
	
	/**
	 * Ano do calendário letivo.
	 * @var float
	 */
	public $nu_ano;
	
	/**
	 * Id da entidade do calendário letivo.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return int
	 */
	public function getId_calendarioletivo() {
		return $this->id_calendarioletivo;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return float
	 */
	public function getNu_ano() {
		return $this->nu_ano;
	}
	
	/**
	 * @param int $id_calendarioletivo
	 */
	public function setId_calendarioletivo($id_calendarioletivo) {
		$this->id_calendarioletivo = $id_calendarioletivo;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param float $nu_ano
	 */
	public function setNu_ano($nu_ano) {
		$this->nu_ano = $nu_ano;
	}

}

?>
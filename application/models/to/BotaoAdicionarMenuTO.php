<?php
/**
 * Classe TO dos botões do Menu
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class BotaoAdicionarMenuTO extends Ead1_TO_Dinamico{
    var $id_entidade;
    var $id_perfil;
    var $id_funcionalidade;
    var $st_urlicone;
    var $st_nomeperfil;
    var $st_funcionalidade;
    var $bl_pesquisa;
    var $st_label;
    var $id_funcionalidadepai;
    var $bl_ativo;
    var $st_classeflex;
    var $st_tipofuncionalidade;
    var $id_situacao;
    var $id_sistema;
    var $nu_ordem;
    var $st_classeflexbotao;
    var $bl_obrigatorio;
    var $bl_visivel;
    var $bl_lista;
    var $st_ajuda;
    var $st_urlcaminho;
    var $st_urlcaminhoedicao;
    var $bl_funcionalidadevisivel;
    var $bl_relatorio;
    var $filhos;
    var $st_target;
    var $id_perfilpai;
    var $id_tipofuncionalidade;

    public function getst_target() {
        return $this->st_target;
    }

    public function getid_perfilpai() {
        return $this->id_perfilpai;
    }

    public function getid_tipofuncionalidade() {
        return $this->id_tipofuncionalidade;
    }

    public function setSt_target($st_target) {
        $this->st_target = $st_target;
    }

    public function setId_perfilpai($id_perfilpai) {
        $this->id_perfilpai = $id_perfilpai;
    }

    public function setId_tipofuncionalidade($id_tipofuncionalidade) {
        $this->id_tipofuncionalidade = $id_tipofuncionalidade;
    }

        public function getid_perfil() {
        return $this->id_perfil;
    }

    public function getid_funcionalidade() {
        return $this->id_funcionalidade;
    }

    public function getst_urlicone() {
        return $this->st_urlicone;
    }

    public function getst_nomeperfil() {
        return $this->st_nomeperfil;
    }

    public function getst_funcionalidade() {
        return $this->st_funcionalidade;
    }

    public function getbl_pesquisa() {
        return $this->bl_pesquisa;
    }

    public function getst_label() {
        return $this->st_label;
    }

    public function getid_funcionalidadepai() {
        return $this->id_funcionalidadepai;
    }

    public function getbl_ativo() {
        return $this->bl_ativo;
    }

    public function getst_classeflex() {
        return $this->st_classeflex;
    }

    public function getst_tipofuncionalidade() {
        return $this->st_tipofuncionalidade;
    }

    public function getid_situacao() {
        return $this->id_situacao;
    }

    public function getid_sistema() {
        return $this->id_sistema;
    }

    public function getnu_ordem() {
        return $this->nu_ordem;
    }

    public function getst_classeflexbotao() {
        return $this->st_classeflexbotao;
    }

    public function getbl_obrigatorio() {
        return $this->bl_obrigatorio;
    }

    public function getbl_visivel() {
        return $this->bl_visivel;
    }

    public function getbl_lista() {
        return $this->bl_lista;
    }

    public function getst_ajuda() {
        return $this->st_ajuda;
    }

    public function getst_urlcaminho() {
        return $this->st_urlcaminho;
    }

    public function getst_urlcaminhoedicao() {
        return $this->st_urlcaminhoedicao;
    }

    public function getbl_funcionalidadevisivel() {
        return $this->bl_funcionalidadevisivel;
    }

    public function getbl_relatorio() {
        return $this->bl_relatorio;
    }

    public function getFilhos() {
        return $this->filhos;
    }

    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    public function setId_funcionalidade($id_funcionalidade) {
        $this->id_funcionalidade = $id_funcionalidade;
    }

    public function setSt_urlicone($st_urlicone) {
        $this->st_urlicone = $st_urlicone;
    }

    public function setSt_nomeperfil($st_nomeperfil) {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    public function setSt_funcionalidade($st_funcionalidade) {
        $this->st_funcionalidade = $st_funcionalidade;
    }

    public function setBl_pesquisa($bl_pesquisa) {
        $this->bl_pesquisa = $bl_pesquisa;
    }

    public function setSt_label($st_label) {
        $this->st_label = $st_label;
    }

    public function setId_funcionalidadepai($id_funcionalidadepai) {
        $this->id_funcionalidadepai = $id_funcionalidadepai;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setSt_classeflex($st_classeflex) {
        $this->st_classeflex = $st_classeflex;
    }

    public function setSt_tipofuncionalidade($st_tipofuncionalidade) {
        $this->st_tipofuncionalidade = $st_tipofuncionalidade;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_sistema($id_sistema) {
        $this->id_sistema = $id_sistema;
    }

    public function setNu_ordem($nu_ordem) {
        $this->nu_ordem = $nu_ordem;
    }

    public function setSt_classeflexbotao($st_classeflexbotao) {
        $this->st_classeflexbotao = $st_classeflexbotao;
    }

    public function setBl_obrigatorio($bl_obrigatorio) {
        $this->bl_obrigatorio = $bl_obrigatorio;
    }

    public function setBl_visivel($bl_visivel) {
        $this->bl_visivel = $bl_visivel;
    }

    public function setBl_lista($bl_lista) {
        $this->bl_lista = $bl_lista;
    }

    public function setSt_ajuda($st_ajuda) {
        $this->st_ajuda = $st_ajuda;
    }

    public function setSt_urlcaminho($st_urlcaminho) {
        $this->st_urlcaminho = $st_urlcaminho;
    }

    public function setSt_urlcaminhoedicao($st_urlcaminhoedicao) {
        $this->st_urlcaminhoedicao = $st_urlcaminhoedicao;
    }

    public function setBl_funcionalidadevisivel($bl_funcionalidadevisivel) {
        $this->bl_funcionalidadevisivel = $bl_funcionalidadevisivel;
    }

    public function setBl_relatorio($bl_relatorio) {
        $this->bl_relatorio = $bl_relatorio;
    }

    public function setFilhos($filhos) {
        $this->filhos = $filhos;
    }

        public function setid_entidade($id_entidade) {
    
        $this->id_entidade = $id_entidade;
    }
}
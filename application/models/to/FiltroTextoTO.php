<?php
class FiltroTextoTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que diz qual o label do filtro
	 * @var String
	 */
	public $st_label;
	
	/**
	 * Atributo que diz qual o tipo do filtro
	 * @var String
	 */
	public $st_tipo;

	/**
	 * Atributo que diz qual o valor do filtro
	 * @var Mixed
	 */
	public $valor = array();
	
	/**
	 * Atributo que diz qual a propriedade
	 * @var Mixed
	 */
	public $st_propriedade;
	
	/**
	 * Atributo que diz qual a chave para retorno
	 * @var String
	 */
	public $st_variavel;
	
	/**
	 * Contem o name para factory
	 * @var String
	 */
	public $st_name;
	

	/**
	 * Método para set de filtros
	 * @param String $label
	 * @param String $propriedade
	 * @param String $tipo
	 * @param String $variavel
	 * @param mixed $valor
	 */
	public function setFiltro($label, $propriedade, $tipo, $variavel, $valor = null){
		$this->setSt_label($label);
		$this->setSt_propriedade($propriedade);
		$this->setSt_tipo($tipo);
		$this->setValor($valor);
		$this->setSt_variavel($variavel);
	}
	
	/**
	 * @return the $st_label
	 */
	public function getSt_label() {
		return $this->st_label;
	}

	/**
	 * @return the $st_tipo
	 */
	public function getSt_tipo() {
		return $this->st_tipo;
	}

	/**
	 * @return the $valor
	 */
	public function getValor() {
		return $this->valor;
	}

	/**
	 * @return the $st_propriedade
	 */
	public function getSt_propriedade() {
		return $this->st_propriedade;
	}

	/**
	 * @return the $st_variavel
	 */
	public function getSt_variavel() {
		return $this->st_variavel;
	}

	/**
	 * @param String $st_label
	 */
	public function setSt_label($st_label) {
		$this->st_label = $st_label;
	}

	/**
	 * @param String $st_tipo
	 */
	public function setSt_tipo($st_tipo) {
		$this->st_tipo = $st_tipo;
	}

	/**
	 * @param Mixed $valor
	 */
	public function setValor($valor) {
		$this->valor = $valor;
	}

	/**
	 * @param Mixed $st_propriedade
	 */
	public function setSt_propriedade($st_propriedade) {
		$this->st_propriedade = $st_propriedade;
	}

	/**
	 * @param String $st_variavel
	 */
	public function setSt_variavel($st_variavel) {
		$this->st_variavel = $st_variavel;
	}
	/**
	 * @return the $st_name
	 */
	public function getSt_name() {
		return $this->st_name;
	}

	/**
	 * @param String $st_name
	 */
	public function setSt_name($st_name) {
		$this->st_name = $st_name;
	}


	
}
<?php

/**
 * Classe para encapsular os dados de view de telefone para pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaTelefoneTO extends Ead1_TO_Dinamico
{

    /**
     * Id da entidade.
     * @var int
     */
    public $id_entidade;

    /**
     * id do usuário.
     * @var int
     */
    public $id_usuario;

    /**
     * Id do telefone.
     * @var int
     */
    public $id_telefone;

    /**
     * Número do telefone.
     * @var string
     */
    public $nu_telefone;

    /**
     * Id do tipo de telefone.
     * @var int
     */
    public $id_tipotelefone;

    /**
     * Tipo de telefone.
     * @var string
     */
    public $st_tipotelefone;

    /**
     * Diz se o telefone é padrão.
     * @var boolean
     */
    public $bl_padrao;

    /**
     * Número DDD do telefone.
     * @var int
     */
    public $nu_ddd;

    /**
     * Número DDI do telefone.
     * @var int
     */
    public $nu_ddi;

    /**
     * Nome exibição
     * @var string 
     */
    public $st_nomeexibicao;

    /**
     * 
     * @return string
     */
    public function getSt_nomeexibicao ()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * 
     * @param string $st_nomeexibicao
     */
    public function setSt_nomeexibicao ($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
    }

    /**
     * @return boolean
     */
    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    /**
     * @return int
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_telefone ()
    {
        return $this->id_telefone;
    }

    /**
     * @return int
     */
    public function getId_tipotelefone ()
    {
        return $this->id_tipotelefone;
    }

    /**
     * @return int
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return int
     */
    public function getNu_ddd ()
    {
        return $this->nu_ddd;
    }

    /**
     * @return int
     */
    public function getNu_ddi ()
    {
        return $this->nu_ddi;
    }

    /**
     * @return string
     */
    public function getNu_telefone ()
    {
        return $this->nu_telefone;
    }

    /**
     * @return string
     */
    public function getSt_tipotelefone ()
    {
        return $this->st_tipotelefone;
    }

    /**
     * @param boolean $bl_padrao
     */
    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_telefone
     */
    public function setId_telefone ($id_telefone)
    {
        $this->id_telefone = $id_telefone;
    }

    /**
     * @param int $id_tipotelefone
     */
    public function setId_tipotelefone ($id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param int $nu_ddd
     */
    public function setNu_ddd ($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
    }

    /**
     * @param int $nu_ddi
     */
    public function setNu_ddi ($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
    }

    /**
     * @param string $st_telefone
     */
    public function setNu_telefone ($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
    }

    /**
     * @param string $st_tipotelefone
     */
    public function setSt_tipotelefone ($st_tipotelefone)
    {
        $this->st_tipotelefone = $st_tipotelefone;
    }

}

?>
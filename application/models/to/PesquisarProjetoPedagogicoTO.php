<?php
/** 
 * @author DIMAS SULZ
 * 
 * 
 */
class PesquisarProjetoPedagogicoTO extends Ead1_TO_Pesquisar
{
    /**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarProjetoPedagogico';
	
	/**
	 * 
	 * Contem o id do projeto pedagógico
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * 
	 * Contem a descrição do projeto
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * 
	 * Contem o nome do projeto pedagógico
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * 
	 * Contem o titulo de exibição do projeto pedagógico
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * 
	 * Contem o nome da versão do projeto pedagógico
	 * @var string
	 */
	public $st_nomeversao;
	
	/**
	 * 
	 * Contem a situação projeto pedagógico
	 * @var string
	 */
	public $id_situacao;
	
	/**
	 * 
	 * Contem a situação projeto pedagógico
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $st_nomeversao
	 */
	public function getSt_nomeversao() {
		return $this->st_nomeversao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $st_nomeversao
	 */
	public function setSt_nomeversao($st_nomeversao) {
		$this->st_nomeversao = $st_nomeversao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}


}
?>
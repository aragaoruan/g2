<?php
/**
 * Classe para encapsular os dados de AcessoAfiliado.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class AcessoAfiliadoTO extends Ead1_TO_Dinamico {
	
	public $id_acessoafiliado;
	public $id_contratoafiliado;
	public $dt_cadastro;
	
	/**
	 * @return the $id_acessoafiliado
	 */
	public function getId_acessoafiliado() {
		return $this->id_acessoafiliado;
	}

	/**
	 * @param field_type $id_acessoafiliado
	 */
	public function setId_acessoafiliado($id_acessoafiliado) {
		$this->id_acessoafiliado = $id_acessoafiliado;
	}

	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}


}




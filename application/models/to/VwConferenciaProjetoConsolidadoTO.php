<?php
/**
 * Classe para encapsular os dados de Vwocorrência.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwConferenciaProjetoConsolidadoTO extends Ead1_TO_Dinamico
{

    public $id_turma;
    public $st_turma;
    public $id_projetopedagogico;
    public $st_projetopedagogico;
    public $id_entidade;
    public $bl_confirmada;

    public function set_blConfirmada($bl_confirmada)
    {
        $this->bl_confirmada = $bl_confirmada;
    }

    public function get_blConfirmada()
    {
        return $this->bl_confirmada;
    }

    public function set_idEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function get_idEntidade()
    {
        return $this->id_entidade;
    }

    public function set_idProjetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function get_idProjetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function set_idTurma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    public function get_idTurma()
    {
        return $this->id_turma;
    }

    public function set_stProjetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function get_stProjetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function set_stTurma($st_turma)
    {
        $this->st_turma = $st_turma;
    }

    public function get_stTurma()
    {
        return $this->st_turma;
    }
}

?>
<?php

/**
 * Classe para encapsular os dados para a pesquisa de Aluno
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class PesquisarAlunoTO extends VwMatriculaTO {

    public $st_dataconcluinte;
    public $st_status;
    public $bl_documentacao;
    public $id_evolucao;
    public $st_codcertificacao;

    //Filtros para a pesquisa
    public $dt_inicio;
    public $dt_termino;
    public $id_evolucaocertificacao;
    public $bl_disciplinacomplementar;

    //filtros para pesquisa com datas
    public $dt_iniciocertificadogerado;
    public $dt_terminocertificadogerado;
    public $dt_inicioenviadocertificadora;
    public $dt_terminoenviadocertificadora;
    public $dt_inicioretornadocertificadora;
    public $dt_terminoretornadocertificadora;
    public $dt_inicioenviadoaluno;
    public $dt_terminoenviadoaluno;


    /**
     * @return the $st_dataconcluinte
     */
    public function getSt_dataconcluinte() {
        return $this->st_dataconcluinte;
    }

    /**
     * @return the $st_status
     */
    public function getSt_status() {
        return $this->st_status;
    }

    /**
     * @return the $dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @return the $dt_termino
     */
    public function getDt_termino() {
        return $this->dt_termino;
    }

    /**
     * @param field_type $st_dataconcluinte
     */
    public function setSt_dataconcluinte($st_dataconcluinte) {
        $this->st_dataconcluinte = $st_dataconcluinte;
    }

    /**
     * @param field_type $st_status
     */
    public function setSt_status($st_status) {
        $this->st_status = $st_status;
    }

    /**
     * @param field_type $dt_inicio
     */
    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @param field_type $dt_termino
     */
    public function setDt_termino($dt_termino) {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return the $bl_documentacao
     */
    public function getBl_documentacao() {
        return $this->bl_documentacao;
    }

    /**
     * @param field_type $bl_documentacao
     */
    public function setBl_documentacao($bl_documentacao) {
        $this->bl_documentacao = $bl_documentacao;
    }

    /**
     * @return the $id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @param field_type $id_evolucao
     */
    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return mixed
     */
    public function getId_evolucaocertificacao() {
        return $this->id_evolucaocertificacao;
    }

    /**
     * @param mixed $id_evolucaocertificacao
     */
    public function setId_evolucaocertificacao($id_evolucaocertificacao) {
        $this->id_evolucaocertificacao = $id_evolucaocertificacao;
    }

    /**
     * @return string st_codcertificacao
     */
    public function getSt_codcertificacao() {
        return $this->st_codcertificacao;
    }

    /**
     * @param st_codcertificacao
     */
    public function setSt_codcertificacao($st_codcertificacao) {
        $this->st_codcertificacao = $st_codcertificacao;
    }

    /**
     * @return string bl_disciplinacomplementar
     */
    public function getBl_disciplinacomplementar() {
        return $this->bl_disciplinacomplementar;
    }

    /**
     * @param bl_disciplinacomplementar
     */
    public function setBl_disciplinacomplementar($bl_disciplinacomplementar) {
        $this->bl_disciplinacomplementar = $bl_disciplinacomplementar;
    }

    public function getDt_iniciocertificadogerado()
    {
        return $this->dt_iniciocertificadogerado;
    }

    public function setDt_iniciocertificadogerado($dt_iniciocertificadogerado)
    {
        $this->dt_iniciocertificadogerado = $dt_iniciocertificadogerado;
    }

    public function getDt_terminocertificadogerado()
    {
        return $this->dt_terminocertificadogerado;
    }

    public function setDt_terminocertificadogerado($dt_terminocertificadogerado)
    {
        $this->dt_terminocertificadogerado = $dt_terminocertificadogerado;
    }

    public function getDt_inicioenviadocertificadora()
    {
        return $this->dt_inicioenviadocertificadora;
    }

    public function setDt_inicioenviadocertificadora($dt_inicioenviadocertificadora)
    {
        $this->dt_inicioenviadocertificadora = $dt_inicioenviadocertificadora;
    }

    public function getDt_terminoenviadocertificadora()
    {
        return $this->dt_terminoenviadocertificadora;
    }

    public function setDt_terminoenviadocertificadora($dt_terminoenviadocertificadora)
    {
        $this->dt_terminoenviadocertificadora = $dt_terminoenviadocertificadora;
    }

    public function getDt_inicioretornadocertificadora()
    {
        return $this->dt_inicioretornadocertificadora;
    }

    public function setDt_inicioretornadocertificadora($dt_inicioretornadocertificadora)
    {
        $this->dt_inicioretornadocertificadora = $dt_inicioretornadocertificadora;
    }

    public function getDt_terminoretornadocertificadora()
    {
        return $this->dt_terminoretornadocertificadora;
    }

    public function setDt_terminoretornadocertificadora($dt_terminoretornadocertificadora)
    {
        $this->dt_terminoretornadocertificadora = $dt_terminoretornadocertificadora;
    }

    public function getDt_inicioenviadoaluno()
    {
        return $this->dt_inicioenviadoaluno;
    }

    public function setDt_inicioenviadoaluno($dt_inicioenviadoaluno)
    {
        $this->dt_inicioenviadoaluno = $dt_inicioenviadoaluno;
    }

    public function getDt_terminoenviadoaluno()
    {
        return $this->dt_terminoenviadoaluno;
    }

    public function setDt_terminoenviadoaluno($dt_terminoenviadoaluno)
    {
        $this->dt_terminoenviadoaluno = $dt_terminoenviadoaluno;
    }






}

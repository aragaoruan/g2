<?php
/**
 * Classe para encapsular os dados completos de Função para Perfil.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class FuncaoCompletoTO extends Ead1_TO_Dinamico {
	
	public $id_funcao;
	public $st_funcao;
	public $id_tipofuncao;
	public $st_tipofuncao;
	
	/**
	 * @return unknown
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	
	/**
	 * @return unknown
	 */
	public function getId_tipofuncao() {
		return $this->id_tipofuncao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tipofuncao() {
		return $this->st_tipofuncao;
	}
	
	/**
	 * @param unknown_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	
	/**
	 * @param unknown_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @param unknown_type $id_tipofuncao
	 */
	public function setId_tipofuncao($id_tipofuncao) {
		$this->id_tipofuncao = $id_tipofuncao;
	}
	
	/**
	 * @param unknown_type $st_tipofuncao
	 */
	public function setSt_tipofuncao($st_tipofuncao) {
		$this->st_tipofuncao = $st_tipofuncao;
	}

}

?>
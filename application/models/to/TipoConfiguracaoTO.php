<?php
/**
 * Classe para encapsular os dados de tipo de configuração para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoConfiguracaoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do tipo de configuração.
	 * @var int
	 */
	public $id_tipoconfiguracao;
	
	/**
	 * Contém o nome do tipo de configuração.
	 * @var String
	 */
	public $st_tipoconfiguracao;
	
	/**
	 * Contém a descrição do tipo de configuração.
	 * @var String
	 */
	public $st_descricao;
	
	/**
	 * Categoria do tipo de configuração.
	 * @var String
	 */
	public $st_categoria;
	
	/**
	 * Valor padrão do tipo de configuração.
	 * @var String
	 */
	public $st_padrao;
	
	/**
	 * @return String
	 */
	public function getSt_categoria() {
		return $this->st_categoria;
	}
	
	/**
	 * @return String
	 */
	public function getSt_padrao() {
		return $this->st_padrao;
	}
	
	/**
	 * @param String $st_categoria
	 */
	public function setSt_categoria($st_categoria) {
		$this->st_categoria = $st_categoria;
	}
	
	/**
	 * @param String $st_padrao
	 */
	public function setSt_padrao($st_padrao) {
		$this->st_padrao = $st_padrao;
	}
	/**
	 * @return int
	 */
	public function getId_tipoconfiguracao() {
		return $this->id_tipoconfiguracao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_tipoconfiguracao() {
		return $this->st_tipoconfiguracao;
	}
	
	/**
	 * @param int $id_tipoconfiguracao
	 */
	public function setId_tipoconfiguracao($id_tipoconfiguracao) {
		$this->id_tipoconfiguracao = $id_tipoconfiguracao;
	}
	
	/**
	 * @param String $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param String $st_tipoconfiguracao
	 */
	public function setSt_tipoconfiguracao($st_tipoconfiguracao) {
		$this->st_tipoconfiguracao = $st_tipoconfiguracao;
	}

}

?>
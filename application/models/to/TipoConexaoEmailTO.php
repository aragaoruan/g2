<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TipoConexaoEmailTO extends Ead1_TO_Dinamico{

	public $id_tipoconexaoemail;
	public $st_tipoconexaoemail;


	/**
	 * @return the $id_tipoconexaoemail
	 */
	public function getId_tipoconexaoemail(){ 
		return $this->id_tipoconexaoemail;
	}

	/**
	 * @return the $st_tipoconexaoemail
	 */
	public function getSt_tipoconexaoemail(){ 
		return $this->st_tipoconexaoemail;
	}


	/**
	 * @param field_type $id_tipoconexaoemail
	 */
	public function setId_tipoconexaoemail($id_tipoconexaoemail){ 
		$this->id_tipoconexaoemail = $id_tipoconexaoemail;
	}

	/**
	 * @param field_type $st_tipoconexaoemail
	 */
	public function setSt_tipoconexaoemail($st_tipoconexaoemail){ 
		$this->st_tipoconexaoemail = $st_tipoconexaoemail;
	}

}
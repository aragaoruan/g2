<?php
/**
 * Classe para encapsular os dados de Tipo de Contrato do responsavel.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class TipoContratoResponsavelTO extends Ead1_TO_Dinamico {
	
	const TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO	= 1;
	const TIPO_CONTRATO_RESPONSAVEL_PEDAGOGICO	= 2;
	
	/**
	 * Contem o id do tipo de contrato do responsavel
	 * @var int
	 */
	public $id_tipocontratoresponsavel;
	
	/**
	 * Contem o nome do tipo de contrato do responsavel
	 * @var string
	 */
	public $st_tipocontratoresponsavel;
	
	/**
	 * @return the $id_tipocontratoresponsavel
	 */
	public function getId_tipocontratoresponsavel() {
		return $this->id_tipocontratoresponsavel;
	}

	/**
	 * @return the $st_tipocontratoresponsavel
	 */
	public function getSt_tipocontratoresponsavel() {
		return $this->st_tipocontratoresponsavel;
	}

	/**
	 * @param int $id_tipocontratoresponsavel
	 */
	public function setId_tipocontratoresponsavel($id_tipocontratoresponsavel) {
		$this->id_tipocontratoresponsavel = $id_tipocontratoresponsavel;
	}

	/**
	 * @param string $st_tipocontratoresponsavel
	 */
	public function setSt_tipocontratoresponsavel($st_tipocontratoresponsavel) {
		$this->st_tipocontratoresponsavel = $st_tipocontratoresponsavel;
	}

	
	
}
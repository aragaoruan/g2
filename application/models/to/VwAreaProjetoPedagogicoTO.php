<?php
/**
 * Classe para encapsular os dados da view de área, disciplin e projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwAreaProjetoPedagogicoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Nome da área de conhecimento.
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Descrição da área.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Id do módulo.
	 * @var int
	 */
	public $id_modulo;
	
	/**
	 * Id do módulo anterior.
	 * @var int
	 */
	public $id_moduloanterior;
	
	/**
	 * Nome do módulo.
	 * @var string
	 */
	public $st_modulo;
	
	/**
	 * Título de exibição do módulo.
	 * @var string
	 */
	public $st_tituloexibicaomodulo;
	
	/**
	 * Descrição do projeto pedagógico.
	 * @var string
	 */
	public $st_descricaoprojetopedagogico;
	
	/**
	 * Nome do projeto pedagógico.
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * Título de exibição do projeto pedagógico.
	 * @var string
	 */
	public $st_tituloexibicaoprojetopedagogico;
	
	/**
	 * Id da entidade cadastro do projeto pedagógico.
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * Id do módulo pai.
	 * @var int
	 */
	public $id_modulopai;
	
	/**
	 * Nome do módulo pai.
	 * @var string
	 */
	public $st_modulopai;
	
	/**
	 * Título de exibição do módulo pai.
	 * @var string
	 */
	public $st_tituloexibicaopai;
	
	/**
	 * Nome da disciplina.
	 * @var string
	 */
	public $st_disciplina;
	
	/**
	 * Título de exibição da disciplina.
	 * @var string
	 */
	public $st_tituloexibicaodisciplina;
	
	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id da entidade da disciplina.
	 * @var int
	 */
	public $id_entidadedisciplina;
	
	/**
	 * Id da situação da disciplina.
	 * @var int
	 */
	public $id_situacaodisciplina;
	
	/**
	 * Id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param string $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadedisciplina() {
		return $this->id_entidadedisciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return int
	 */
	public function getId_moduloanterior() {
		return $this->id_moduloanterior;
	}
	
	/**
	 * @return int
	 */
	public function getId_modulopai() {
		return $this->id_modulopai;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacaodisciplina() {
		return $this->id_situacaodisciplina;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaoprojetopedagogico() {
		return $this->st_descricaoprojetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return string
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_modulopai() {
		return $this->st_modulopai;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicaodisciplina() {
		return $this->st_tituloexibicaodisciplina;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicaomodulo() {
		return $this->st_tituloexibicaomodulo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicaopai() {
		return $this->st_tituloexibicaopai;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicaoprojetopedagogico() {
		return $this->st_tituloexibicaoprojetopedagogico;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 * @param int $id_entidadedisciplina
	 */
	public function setId_entidadedisciplina($id_entidadedisciplina) {
		$this->id_entidadedisciplina = $id_entidadedisciplina;
	}
	
	/**
	 * @param int $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param int $id_moduloanterior
	 */
	public function setId_moduloanterior($id_moduloanterior) {
		$this->id_moduloanterior = $id_moduloanterior;
	}
	
	/**
	 * @param int $id_modulopai
	 */
	public function setId_modulopai($id_modulopai) {
		$this->id_modulopai = $id_modulopai;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param int $id_situacaodisciplina
	 */
	public function setId_situacaodisciplina($id_situacaodisciplina) {
		$this->id_situacaodisciplina = $id_situacaodisciplina;
	}
	
	/**
	 * @param string $st_descricaoprojetopedagogico
	 */
	public function setSt_descricaoprojetopedagogico($st_descricaoprojetopedagogico) {
		$this->st_descricaoprojetopedagogico = $st_descricaoprojetopedagogico;
	}
	
	/**
	 * @param string $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param string $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param string $st_modulopai
	 */
	public function setSt_modulopai($st_modulopai) {
		$this->st_modulopai = $st_modulopai;
	}
	
	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	
	/**
	 * @param string $st_tituloexibicaodisciplina
	 */
	public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
		$this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
	}
	
	/**
	 * @param string $st_tituloexibicaomodulo
	 */
	public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
		$this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
	}
	
	/**
	 * @param string $st_tituloexibicaopai
	 */
	public function setSt_tituloexibicaopai($st_tituloexibicaopai) {
		$this->st_tituloexibicaopai = $st_tituloexibicaopai;
	}
	
	/**
	 * @param string $st_tituloexibicaoprojetopedagogico
	 */
	public function setSt_tituloexibicaoprojetopedagogico($st_tituloexibicaoprojetopedagogico) {
		$this->st_tituloexibicaoprojetopedagogico = $st_tituloexibicaoprojetopedagogico;
	}

}

?>
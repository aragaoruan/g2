<?php
/**
 * TO da view vw_vendausuario
 * @author edermariano
 *
 */
class VwVendaUsuarioTO extends Ead1_TO_Dinamico{
	
	public $id_venda; 
	public $id_usuario; 
    public $st_nomecompleto; 
    public $st_cpf;
    public $id_formapagamento;
    public $st_formapagamento;
    public $id_campanhacomercial;
    public $st_campanhacomercial;
    public $id_evolucao;
    public $st_evolucao;
    public $id_situacao;
    public $st_situacao;
    public $bl_ativo;
    public $id_usuariocadastro;
    public $st_nomecompletocadastrador;
    public $nu_valorliquido;
    public $nu_valorbruto;
    public $nu_descontoporcentagem;
    public $nu_descontovalor;
    public $nu_juros;
    public $nu_parcelas;
    public $bl_contrato;
    public $id_entidade;
    public $dt_cadastro;
    public $id_prevenda;
    
    public $st_linkloja;
    public $st_conversao;
    public $id_categoriacampanha;
    public $st_categoriacampanha;
    public $st_nomeentidade;
    
    
    
    
    
	/**
	 * @return the $id_categoriacampanha
	 */
	public function getId_categoriacampanha() {
		return $this->id_categoriacampanha;
	}

	/**
	 * @return the $st_categoriacampanha
	 */
	public function getSt_categoriacampanha() {
		return $this->st_categoriacampanha;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $id_categoriacampanha
	 */
	public function setId_categoriacampanha($id_categoriacampanha) {
		$this->id_categoriacampanha = $id_categoriacampanha;
	}

	/**
	 * @param field_type $st_categoriacampanha
	 */
	public function setSt_categoriacampanha($st_categoriacampanha) {
		$this->st_categoriacampanha = $st_categoriacampanha;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @return the $st_conversao
	 */
	public function getSt_conversao() {
		return $this->st_conversao;
	}

	/**
	 * @param field_type $st_conversao
	 */
	public function setSt_conversao($st_conversao) {
		$this->st_conversao = $st_conversao;
	}

	/**
	 * @return the $st_linkloja
	 */
	public function getSt_linkloja() {
		return $this->st_linkloja;
	}

	/**
	 * @param field_type $st_linkloja
	 */
	public function setSt_linkloja($st_linkloja) {
		$this->st_linkloja = $st_linkloja;
	}

	/**
	 * @return the $id_usuario
	 */
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param $id_venda the $id_venda to set
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param $st_nomecompleto the $st_nomecompleto to set
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}

	/**
	 * @param $nu_cpf the $nu_cpf to set
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @param $id_formapagamento the $id_formapagamento to set
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @return the $st_formapagamento
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}

	/**
	 * @param $st_formapagamento the $st_formapagamento to set
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}

	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @param $id_campanhacomercial the $id_campanhacomercial to set
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @return the $st_campanhacomercial
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}

	/**
	 * @param $st_campanhacomercial the $st_campanhacomercial to set
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param $id_evolucao the $id_evolucao to set
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param $st_evolucao the $st_evolucao to set
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $st_nomecompletocadastrador
	 */
	public function getSt_nomecompletocadastrador() {
		return $this->st_nomecompletocadastrador;
	}

	/**
	 * @param $st_nomecompletocadastrador the $st_nomecompletocadastrador to set
	 */
	public function setSt_nomecompletocadastrador($st_nomecompletocadastrador) {
		$this->st_nomecompletocadastrador = $st_nomecompletocadastrador;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @param $nu_valorliquido the $nu_valorliquido to set
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @return the $nu_valorbruto
	 */
	public function getNu_valorbruto() {
		return $this->nu_valorbruto;
	}

	/**
	 * @param $nu_valorbruto the $nu_valorbruto to set
	 */
	public function setNu_valorbruto($nu_valorbruto) {
		$this->nu_valorbruto = $nu_valorbruto;
	}

	/**
	 * @return the $nu_descontoporcentagem
	 */
	public function getNu_descontoporcentagem() {
		return $this->nu_descontoporcentagem;
	}

	/**
	 * @param $nu_descontoporcentagem the $nu_descontoporcentagem to set
	 */
	public function setNu_descontoporcentagem($nu_descontoporcentagem) {
		$this->nu_descontoporcentagem = $nu_descontoporcentagem;
	}

	/**
	 * @return the $nu_descontovalor
	 */
	public function getNu_descontovalor() {
		return $this->nu_descontovalor;
	}

	/**
	 * @param $nu_descontovalor the $nu_descontovalor to set
	 */
	public function setNu_descontovalor($nu_descontovalor) {
		$this->nu_descontovalor = $nu_descontovalor;
	}

	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros() {
		return $this->nu_juros;
	}

	/**
	 * @param $nu_juros the $nu_juros to set
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @param $nu_parcelas the $nu_parcelas to set
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @return the $bl_contrato
	 */
	public function getBl_contrato() {
		return $this->bl_contrato;
	}

	/**
	 * @param $bl_contrato the $bl_contrato to set
	 */
	public function setBl_contrato($bl_contrato) {
		$this->bl_contrato = $bl_contrato;
	}
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $id_prevenda
	 */
	public function getId_prevenda() {
		return $this->id_prevenda;
	}

	/**
	 * @param field_type $id_prevenda
	 */
	public function setId_prevenda($id_prevenda) {
		$this->id_prevenda = $id_prevenda;
	}



}
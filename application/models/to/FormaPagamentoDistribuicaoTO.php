<?php
/**
 * Classe para encapsular os dados de controle de distribuição dos valores do meio de pagamento nas formas de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FormaPagamentoDistribuicaoTO extends Ead1_TO_Dinamico{

	/**
	 * Id da forma de pagamento.
	 * @var int
	 */
	public $id_formapagamento;
	
	/**
	 * Id do meio de pagamento.
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * Controla a porcentagem mínima aceita de um meio de pagamento na forma de pagamento.
	 * @var float
	 */
	public $nu_valormin;
	
	/**
	 * Controla a porcentagem mínima aceita de um meio de pagamento na forma de pagamento
	 * @var float 
	 */
	public $nu_valormax;
	
	/**
	 * @return int
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return int
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valormax() {
		return $this->nu_valormax;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}
	
	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param float $nu_valormax
	 */
	public function setNu_valormax($nu_valormax) {
		$this->nu_valormax = $nu_valormax;
	}
	
	/**
	 * @param float $nu_valormin
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}

}

?>
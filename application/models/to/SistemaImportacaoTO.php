<?php
class SistemaImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_sistemaimportacao;
	public $st_sistemaimportacao;
	
	/**
	 * @return the $id_sistemaimportacao
	 */
	public function getId_sistemaimportacao() {
		return $this->id_sistemaimportacao;
	}

	/**
	 * @return the $st_sistemaimportacao
	 */
	public function getSt_sistemaimportacao() {
		return $this->st_sistemaimportacao;
	}

	/**
	 * @param field_type $id_sistemaimportacao
	 */
	public function setId_sistemaimportacao($id_sistemaimportacao) {
		$this->id_sistemaimportacao = $id_sistemaimportacao;
	}

	/**
	 * @param field_type $st_sistemaimportacao
	 */
	public function setSt_sistemaimportacao($st_sistemaimportacao) {
		$this->st_sistemaimportacao = $st_sistemaimportacao;
	}

	
}
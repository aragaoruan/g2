<?php
/**
 * Classe para encapsular os dados de pesquisa da área do conhecimento
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package models
 * @subpackage to
 */
class PesquisarAreaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.pedagogico.cadastrar.CadastrarAreaConhecimento';
	/**
	 * Contém o id da área do conhecimento
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Contém o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o nome da área do conhecimento
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Contém a descrição da área do conhecimento
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Contém a informação se está ativo ou não a área
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contém o nome do nivel de ensino
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * Contém o id do nivel de ensino
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Contém o id da serie
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Contém o id da séria anterior
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Contém o nome da série
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Contém o id da situação 
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém a situação
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $id_serieanterior
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $st_areaconhecimento the $st_areaconhecimento to set
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $id_serieanterior the $id_serieanterior to set
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}
	
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}


}
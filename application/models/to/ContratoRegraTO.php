<?php

/**
 * Classe de valor da tabela tb_contratoregra
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class ContratoRegraTO extends Ead1_TO_Dinamico {
	
	public $id_contratoregra;
	public $st_contratoregra;
	public $nu_contratoduracao;
	public $nu_mesesmulta;
	public $bl_proporcaomes;
	public $id_projetocontratoduracaotipo;
	public $id_entidade;
	public $nu_contratomultavalor;
	public $bl_renovarcontrato;
	public $nu_mesesmensalidade;
	public $nu_tempoextensao;
	public $id_contratomodelo;
	public $id_extensaomodelo;
    public $id_modelocarteirinha;

    /**
     * @param mixed $id_modelocarteirinha
     */
    public function setid_modelocarteirinha($id_modelocarteirinha)
    {
        $this->id_modelocarteirinha = $id_modelocarteirinha;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_modelocarteirinha()
    {
        return $this->id_modelocarteirinha;
    }



	
	/**
	 * @return the $id_contratoregra
	 */
	public function getId_contratoregra() {
		return $this->id_contratoregra;
	}

	/**
	 * @return the $st_contratoregra
	 */
	public function getSt_contratoregra() {
		return $this->st_contratoregra;
	}

	/**
	 * @return the $nu_contratoduracao
	 */
	public function getNu_contratoduracao() {
		return $this->nu_contratoduracao;
	}

	/**
	 * @return the $nu_mesesmulta
	 */
	public function getNu_mesesmulta() {
		return $this->nu_mesesmulta;
	}

	/**
	 * @return the $bl_proporcaomes
	 */
	public function getBl_proporcaomes() {
		return $this->bl_proporcaomes;
	}

	/**
	 * @return the $id_projetocontratoduracaotipo
	 */
	public function getId_projetocontratoduracaotipo() {
		return $this->id_projetocontratoduracaotipo;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_contratomultavalor
	 */
	public function getNu_contratomultavalor() {
		return $this->nu_contratomultavalor;
	}

	/**
	 * @return the $bl_renovarcontrato
	 */
	public function getBl_renovarcontrato() {
		return $this->bl_renovarcontrato;
	}

	/**
	 * @return the $nu_mesesmensalidade
	 */
	public function getNu_mesesmensalidade() {
		return $this->nu_mesesmensalidade;
	}

	/**
	 * @param $id_contratoregra the $id_contratoregra to set
	 */
	public function setId_contratoregra($id_contratoregra) {
		$this->id_contratoregra = $id_contratoregra;
	}

	/**
	 * @param $st_contratoregra the $st_contratoregra to set
	 */
	public function setSt_contratoregra($st_contratoregra) {
		$this->st_contratoregra = $st_contratoregra;
	}

	/**
	 * @param $nu_contratoduracao the $nu_contratoduracao to set
	 */
	public function setNu_contratoduracao($nu_contratoduracao) {
		$this->nu_contratoduracao = $nu_contratoduracao;
	}

	/**
	 * @param $nu_mesesmulta the $nu_mesesmulta to set
	 */
	public function setNu_mesesmulta($nu_mesesmulta) {
		$this->nu_mesesmulta = $nu_mesesmulta;
	}

	/**
	 * @param $bl_proporcaomes the $bl_proporcaomes to set
	 */
	public function setBl_proporcaomes($bl_proporcaomes) {
		$this->bl_proporcaomes = $bl_proporcaomes;
	}

	/**
	 * @param $id_projetocontratoduracaotipo the $id_projetocontratoduracaotipo to set
	 */
	public function setId_projetocontratoduracaotipo($id_projetocontratoduracaotipo) {
		$this->id_projetocontratoduracaotipo = $id_projetocontratoduracaotipo;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $nu_contratomultavalor the $nu_contratomultavalor to set
	 */
	public function setNu_contratomultavalor($nu_contratomultavalor) {
		$this->nu_contratomultavalor = $nu_contratomultavalor;
	}

	/**
	 * @param $bl_renovarcontrato the $bl_renovarcontrato to set
	 */
	public function setBl_renovarcontrato($bl_renovarcontrato) {
		$this->bl_renovarcontrato = $bl_renovarcontrato;
	}

	/**
	 * @param $nu_mesesmensalidade the $nu_mesesmensalidade to set
	 */
	public function setNu_mesesmensalidade($nu_mesesmensalidade) {
		$this->nu_mesesmensalidade = $nu_mesesmensalidade;
	}
	/**
	 * @return the $nu_tempoextensao
	 */
	public function getNu_tempoextensao() {
		return $this->nu_tempoextensao;
	}

	/**
	 * @param field_type $nu_tempoextensao
	 */
	public function setNu_tempoextensao($nu_tempoextensao) {
		$this->nu_tempoextensao = $nu_tempoextensao;
	}
	/**
	 * @return the $id_extensaomodelo
	 */
	public function getId_extensaomodelo() {
		return $this->id_extensaomodelo;
	}

	/**
	 * @param field_type $id_extensaomodelo
	 */
	public function setId_extensaomodelo($id_extensaomodelo) {
		$this->id_extensaomodelo = $id_extensaomodelo;
	}



	
	
}
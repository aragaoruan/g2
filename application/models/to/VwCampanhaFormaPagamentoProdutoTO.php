<?php
/**
 * Classe que encapsula dados da view de campanha, forma de pagamento e produto.
 * @autor Eder Lamar
 * @package models
 * @subpackage to
 */
class VwCampanhaFormaPagamentoProdutoTO extends Ead1_TO_Dinamico {

	public $id_campanhacomercial;
	public $st_campanhacomercial;
	public $bl_aplicardesconto;
	public $bl_ativo;
	public $bl_disponibilidade;
	public $dt_cadastro;
	public $dt_inicio;
	public $dt_fim;
	public $id_entidade;
	public $id_situacao;
	public $id_tipodesconto;
	public $id_usuariocadastro;
	public $id_tipocampanha;
	public $nu_disponibilidade;
	public $st_descricao;
	public $bl_todosprodutos;
	public $bl_todasformas;
	public $id_formapagamento;
	public $st_formapagamento;
	public $id_produto;
	public $st_produto;
	
	/**
	 * @return unknown
	 */
	public function getBl_aplicardesconto() {
		return $this->bl_aplicardesconto;
	}
	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getBl_disponibilidade() {
		return $this->bl_disponibilidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getBl_todasformas() {
		return $this->bl_todasformas;
	}
	
	/**
	 * @return unknown
	 */
	public function getBl_todosprodutos() {
		return $this->bl_todosprodutos;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_produto() {
		return $this->id_produto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipocampanha() {
		return $this->id_tipocampanha;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_tipodesconto() {
		return $this->id_tipodesconto;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_disponibilidade() {
		return $this->nu_disponibilidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_formapagamento() {
		return $this->st_formapagamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}
	
	/**
	 * @param unknown_type $bl_aplicardesconto
	 */
	public function setBl_aplicardesconto($bl_aplicardesconto) {
		$this->bl_aplicardesconto = $bl_aplicardesconto;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $bl_disponibilidade
	 */
	public function setBl_disponibilidade($bl_disponibilidade) {
		$this->bl_disponibilidade = $bl_disponibilidade;
	}
	
	/**
	 * @param unknown_type $bl_todasformas
	 */
	public function setBl_todasformas($bl_todasformas) {
		$this->bl_todasformas = $bl_todasformas;
	}
	
	/**
	 * @param unknown_type $bl_todosprodutos
	 */
	public function setBl_todosprodutos($bl_todosprodutos) {
		$this->bl_todosprodutos = $bl_todosprodutos;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}
	
	/**
	 * @param unknown_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	
	/**
	 * @param unknown_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	
	/**
	 * @param unknown_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}
	
	/**
	 * @param unknown_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param unknown_type $id_tipocampanha
	 */
	public function setId_tipocampanha($id_tipocampanha) {
		$this->id_tipocampanha = $id_tipocampanha;
	}
	
	/**
	 * @param unknown_type $id_tipodesconto
	 */
	public function setId_tipodesconto($id_tipodesconto) {
		$this->id_tipodesconto = $id_tipodesconto;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $nu_disponibilidade
	 */
	public function setNu_disponibilidade($nu_disponibilidade) {
		$this->nu_disponibilidade = $nu_disponibilidade;
	}
	
	/**
	 * @param unknown_type $st_campanhacomercial
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}
	
	/**
	 * @param unknown_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param unknown_type $st_formapagamento
	 */
	public function setSt_formapagamento($st_formapagamento) {
		$this->st_formapagamento = $st_formapagamento;
	}
	
	/**
	 * @param unknown_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

}

?>
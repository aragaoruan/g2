<?php
/**
* Classe para encapsular tabela tb_categoriasala
* @package models
* @subpackage to
*/

class CategoriaSalaTO extends Ead1_TO_Dinamico{

    public $id_categoriasala;

    /**
     * @param mixed $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $st_categoriasala
     */
    public function setSt_categoriasala($st_categoriasala)
    {
        $this->st_categoriasala = $st_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_categoriasala()
    {
        return $this->st_categoriasala;
    }
    public $st_categoriasala;


}
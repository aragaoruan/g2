<?php

/**
 * Classe para encapsular os dados da view de geração de contrato
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class VwPrimeiroAcessoTO extends Ead1_TO_Dinamico {

    public $dt_dataexpedicao;
    public $dt_nascimento;
    public $id_email;
    public $id_endereco;
    public $id_municipio;
    public $id_municipionascimento;
    public $id_pais;
    public $id_telefone;
    public $id_telefonealternativo;
    public $id_usuario;
    public $nu_ddd;
    public $nu_dddalternativo;
    public $nu_ddi;
    public $nu_ddialternativo;
    public $nu_numero;
    public $nu_telefone;
    public $nu_telefonealternativo;
    public $sg_uf;
    public $sg_ufnascimento;
    public $st_bairro;
    public $st_cep;
    public $st_cidade;
    public $st_cidadenascimento;
    public $st_complemento;
    public $st_cpf;
    public $st_email;
    public $st_endereco;
    public $st_estadoprovincia;
    public $st_login;
    public $st_nomecompleto;
    public $st_nomemunicipio;
    public $st_nomepais;
    public $st_orgaoexpeditor;
    public $st_rg;
    public $id_venda;
    public $id_entidade;

    public function getDt_dataexpedicao() {
        return $this->dt_dataexpedicao;
    }

    public function getDt_nascimento() {
        return $this->dt_nascimento;
    }

    public function getId_email() {
        return $this->id_email;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function getId_municipio() {
        return $this->id_municipio;
    }

    public function getId_municipionascimento() {
        return $this->id_municipionascimento;
    }

    public function getId_pais() {
        return $this->id_pais;
    }

    public function getId_telefone() {
        return $this->id_telefone;
    }

    public function getId_telefonealternativo() {
        return $this->id_telefonealternativo;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getNu_ddd() {
        return $this->nu_ddd;
    }

    public function getNu_dddalternativo() {
        return $this->nu_dddalternativo;
    }

    public function getNu_ddi() {
        return $this->nu_ddi;
    }

    public function getNu_ddialternativo() {
        return $this->nu_ddialternativo;
    }

    public function getNu_numero() {
        return $this->nu_numero;
    }

    public function getNu_telefone() {
        return $this->nu_telefone;
    }

    public function getNu_telefonealternativo() {
        return $this->nu_telefonealternativo;
    }

    public function getSg_uf() {
        return $this->sg_uf;
    }

    public function getSg_ufnascimento() {
        return $this->sg_ufnascimento;
    }

    public function getSt_bairro() {
        return $this->st_bairro;
    }

    public function getSt_cep() {
        return $this->st_cep;
    }

    public function getSt_cidade() {
        return $this->st_cidade;
    }

    public function getSt_cidadenascimento() {
        return $this->st_cidadenascimento;
    }

    public function getSt_complemento() {
        return $this->st_complemento;
    }

    public function getSt_cpf() {
        return $this->st_cpf;
    }

    public function getSt_email() {
        return $this->st_email;
    }

    public function getSt_endereco() {
        return $this->st_endereco;
    }

    public function getSt_estadoprovincia() {
        return $this->st_estadoprovincia;
    }

    public function getSt_login() {
        return $this->st_login;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_nomemunicipio() {
        return $this->st_nomemunicipio;
    }

    public function getSt_nomepais() {
        return $this->st_nomepais;
    }

    public function getSt_orgaoexpeditor() {
        return $this->st_orgaoexpeditor;
    }

    public function getSt_rg() {
        return $this->st_rg;
    }

    /**
     * @return mixed
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param mixed $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    public function setDt_dataexpedicao($dt_dataexpedicao) {
        $this->dt_dataexpedicao = $dt_dataexpedicao;
    }

    public function setDt_nascimento($dt_nascimento) {
        $this->dt_nascimento = $dt_nascimento;
    }

    public function setId_email($id_email) {
        $this->id_email = $id_email;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }

    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
    }

    public function setId_municipionascimento($id_municipionascimento) {
        $this->id_municipionascimento = $id_municipionascimento;
    }

    public function setId_pais($id_pais) {
        $this->id_pais = $id_pais;
    }

    public function setId_telefone($id_telefone) {
        $this->id_telefone = $id_telefone;
    }

    public function setId_telefonealternativo($id_telefonealternativo) {
        $this->id_telefonealternativo = $id_telefonealternativo;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setNu_ddd($nu_ddd) {
        $this->nu_ddd = $nu_ddd;
    }

    public function setNu_dddalternativo($nu_dddalternativo) {
        $this->nu_dddalternativo = $nu_dddalternativo;
    }

    public function setNu_ddi($nu_ddi) {
        $this->nu_ddi = $nu_ddi;
    }

    public function setNu_ddialternativo($nu_ddialternativo) {
        $this->nu_ddialternativo = $nu_ddialternativo;
    }

    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
    }

    public function setNu_telefone($nu_telefone) {
        $this->nu_telefone = $nu_telefone;
    }

    public function setNu_telefonealternativo($nu_telefonealternativo) {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
    }

    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }

    public function setSg_ufnascimento($sg_ufnascimento) {
        $this->sg_ufnascimento = $sg_ufnascimento;
    }

    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
    }

    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
    }

    public function setSt_cidade($st_cidade) {
        $this->st_cidade = $st_cidade;
    }

    public function setSt_cidadenascimento($st_cidadenascimento) {
        $this->st_cidadenascimento = $st_cidadenascimento;
    }

    public function setSt_complemento($st_complemento) {
        $this->st_complemento = $st_complemento;
    }

    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    public function setSt_email($st_email) {
        $this->st_email = $st_email;
    }

    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    public function setSt_estadoprovincia($st_estadoprovincia) {
        $this->st_estadoprovincia = $st_estadoprovincia;
    }

    public function setSt_login($st_login) {
        $this->st_login = $st_login;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setSt_nomemunicipio($st_nomemunicipio) {
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    public function setSt_nomepais($st_nomepais) {
        $this->st_nomepais = $st_nomepais;
    }

    public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
    }

    public function setSt_rg($st_rg) {
        $this->st_rg = $st_rg;
    }

}

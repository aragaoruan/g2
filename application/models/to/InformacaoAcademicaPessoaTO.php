<?php
/**
 * Classe para encapsular os dados de informação acadêmica da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class InformacaoAcademicaPessoaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Variavel que contem a chave primaria da tabela
	 * @var int
	 */
	public $id_informacaoacademicapessoa;

	/**
	 * Atributo que contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Atributo que contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Atributo que contém o id do nivel de ensino da pessoa.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Atributo que contém o curso da pessoa.
	 * @var string
	 */
	public $st_curso;
	
	/**
	 * Atributo que contém o nome da instituição da pessoa.
	 * @var string
	 */
	public $st_nomeinstituicao;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_curso() {
		return $this->st_curso;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeinstituicao() {
		return $this->st_nomeinstituicao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_curso
	 */
	public function setSt_curso($st_curso) {
		$this->st_curso = $st_curso;
	}
	
	/**
	 * @param string $st_nomeinstituicao
	 */
	public function setSt_nomeinstituicao($st_nomeinstituicao) {
		$this->st_nomeinstituicao = $st_nomeinstituicao;
	}
	/**
	 * @return the $id_informacaoacademicapessoa
	 */
	public function getId_informacaoacademicapessoa() {
		return $this->id_informacaoacademicapessoa;
	}

	/**
	 * @param $id_informacaoacademicapessoa the $id_informacaoacademicapessoa to set
	 */
	public function setId_informacaoacademicapessoa($id_informacaoacademicapessoa) {
		$this->id_informacaoacademicapessoa = $id_informacaoacademicapessoa;
	}


}

?>
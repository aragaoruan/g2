<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class ProdutoTextoSistemaTO extends Ead1_TO_Dinamico{

	public $id_textosistema;
	public $id_entidade;
	public $id_produto;


	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema(){ 
		return $this->id_textosistema;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto(){ 
		return $this->id_produto;
	}


	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema){ 
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto){ 
		$this->id_produto = $id_produto;
	}

}
<?php
/**
 * Classe para encapsular os dados de ProdutoLivro.
 * @author Elcio Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class ProdutoLivroTO extends Ead1_TO_Dinamico {
	
	public $id_entidade;
	public $id_produto;
	public $id_livro;
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_livro
	 */
	public function getId_livro() {
		return $this->id_livro;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $id_livro
	 */
	public function setId_livro($id_livro) {
		$this->id_livro = $id_livro;
	}



}




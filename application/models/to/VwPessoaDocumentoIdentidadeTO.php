<?php
/**
 * Classe para encapsular os dados da view de documento de identidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaDocumentoIdentidadeTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	
	public $id_entidade;
	
	public $st_nomeexibicao;
	
	public $st_sexo;
	
	public $dt_nascimento;
	
	public $st_nomepai;
	
	public $st_nomemae;
	
	public $id_pais;
	
	public $sg_uf;
	
	public $id_municipio;
	
	public $dt_cadastro;
	
	public $id_usuariocadastro;
	
	public $st_passaporte;
	
	public $id_situacao;
	
	public $bl_ativo;
	
	public $st_rg;
	
	public $st_orgaoexpeditor;
	
	public $dt_dataexpedicao;
	

}

?>
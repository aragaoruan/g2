<?php

class PacoteEntregaTO extends Ead1_TO_Dinamico {

	public $id_pacoteentrega;
	public $id_pacote;
	public $id_entregamaterial;
	/**
	 * @return the $id_pacoteentrega
	 */
	public function getId_pacoteentrega() {
		return $this->id_pacoteentrega;
	}

	/**
	 * @param field_type $id_pacoteentrega
	 */
	public function setId_pacoteentrega($id_pacoteentrega) {
		$this->id_pacoteentrega = $id_pacoteentrega;
	}

	/**
	 * @return the $id_pacote
	 */
	public function getId_pacote() {
		return $this->id_pacote;
	}

	/**
	 * @param field_type $id_pacote
	 */
	public function setId_pacote($id_pacote) {
		$this->id_pacote = $id_pacote;
	}

	/**
	 * @return the $id_entregamaterial
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}

	/**
	 * @param field_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}

	
}
<?php
/**
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwCategoriaOcorrenciaTO extends CategoriaOcorrenciaTO {
		public $st_situacao;
		public $st_tipoocorrencia;

		
		/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

		/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

		/**
	 * @return the $st_tipoocorrencia
	 */
	public function getSt_tipoocorrencia() {
		return $this->st_tipoocorrencia;
	}

		/**
	 * @param field_type $st_tipoocorrencia
	 */
	public function setSt_tipoocorrencia($st_tipoocorrencia) {
		$this->st_tipoocorrencia = $st_tipoocorrencia;
	}

		
		
}

?>
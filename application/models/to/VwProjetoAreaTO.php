<?php
/**
 * Classe para encapsular os dados da view de área e Projeto Pedagógico
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwProjetoAreaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Id do Projeto Pedagogico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Nome da área de conhecimento.
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Nome do Projeto Pedagogico.
	 * @var string
	 */
	public $st_projetopedagogico;
	
	/**
	 * Nome de Exibição do Projeto Pedagogico.
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * tipo do Projeto Pedagogico.
	 * @var string
	 */
	public $tipo;
	
	/**
	 * Id da Entidade
	 * @var string
	 */
	public $id_entidade;
	
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $st_areaconhecimento the $st_areaconhecimento to set
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param $st_projetopedagogico the $st_projetopedagogico to set
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param $st_tituloexibicao the $st_tituloexibicao to set
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}
	/**
	 * @return the $tipo
	 */
	public function getTipo() {
		return $this->tipo;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $tipo
	 */
	public function setTipo($tipo) {
		$this->tipo = $tipo;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}


	
}
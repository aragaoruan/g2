<?php
/**
 * Classe para encapsular os dados de view de professor.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 30/11/2010
 * 
 * @package models
 * @subpackage to
 */
class VwReferenciaSalaDeAulaProfessorTO extends Ead1_TO_Dinamico {

	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Número de máximo de alunos do professor daquela sala de aula.
	 * @var int
	 */
	public $nu_maximoalunos;
	
	/**
	 * Id do tipo de avaliação de sala de aula.
	 * @var int
	 */
	public $id_tipoavaliacaosaladeaula;
	
	/**
	 * Diz se aquele professor é titular.
	 * @var int
	 */
	public $bl_titular;
	
	/**
	 * Id do professor da sala de aula.
	 * @var int
	 */
	public $id_saladeaulaprofessor;
	
	/**
	 * Id do perfil de referência da entidade.
	 * @var int
	 */
	public $id_perfilreferencia;
	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $nu_maximoalunos
	 */
	public function getNu_maximoalunos() {
		return $this->nu_maximoalunos;
	}

	/**
	 * @return the $id_tipoavaliacaosaladeaula
	 */
	public function getId_tipoavaliacaosaladeaula() {
		return $this->id_tipoavaliacaosaladeaula;
	}

	/**
	 * @return the $bl_titular
	 */
	public function getBl_titular() {
		return $this->bl_titular;
	}

	/**
	 * @return the $id_saladeaulaprofessor
	 */
	public function getId_saladeaulaprofessor() {
		return $this->id_saladeaulaprofessor;
	}

	/**
	 * @return the $id_perfilreferencia
	 */
	public function getId_perfilReferencia() {
		return $this->id_perfilreferencia;
	}

	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param int $nu_maximoalunos
	 */
	public function setNu_maximoalunos($nu_maximoalunos) {
		$this->nu_maximoalunos = $nu_maximoalunos;
	}

	/**
	 * @param int $id_tipoavaliacaosaladeaula
	 */
	public function setId_tipoavaliacaosaladeaula($id_tipoavaliacaosaladeaula) {
		$this->id_tipoavaliacaosaladeaula = $id_tipoavaliacaosaladeaula;
	}

	/**
	 * @param int $bl_titular
	 */
	public function setBl_titular($bl_titular) {
		$this->bl_titular = $bl_titular;
	}

	/**
	 * @param int $id_saladeaulaprofessor
	 */
	public function setId_saladeaulaprofessor($id_saladeaulaprofessor) {
		$this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
	}

	/**
	 * @param int $id_perfilreferencia
	 */
	public function setId_perfilReferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}


}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TextoVariaveisTO extends Ead1_TO_Dinamico{

	public $st_textovariaveis;
	public $id_textovariaveis;
	public $id_textocategoria;
	public $st_camposubstituir;
	public $st_mascara;
	public $id_tipotextovariavel;


	/**
	 * @return the $id_textovariaveis
	 */
	public function getId_textovariaveis(){ 
		return $this->id_textovariaveis;
	}

	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria(){ 
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_camposubstituir
	 */
	public function getSt_camposubstituir(){ 
		return $this->st_camposubstituir;
	}

	/**
	 * @return the $st_textovariaveis
	 */
	public function getSt_textovariaveis(){ 
		return $this->st_textovariaveis;
	}

	/**
	 * @return the $st_mascara
	 */
	public function getSt_mascara(){ 
		return $this->st_mascara;
	}

	/**
	 * @return the $id_tipotextovariavel
	 */
	public function getId_tipotextovariavel(){ 
		return $this->id_tipotextovariavel;
	}


	/**
	 * @param field_type $id_textovariaveis
	 */
	public function setId_textovariaveis($id_textovariaveis){ 
		$this->id_textovariaveis = $id_textovariaveis;
	}

	/**
	 * @param field_type $id_textocategoria
	 */
	public function setId_textocategoria($id_textocategoria){ 
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param field_type $st_camposubstituir
	 */
	public function setSt_camposubstituir($st_camposubstituir){ 
		$this->st_camposubstituir = $st_camposubstituir;
	}

	/**
	 * @param field_type $st_textovariaveis
	 */
	public function setSt_textovariaveis($st_textovariaveis){ 
		$this->st_textovariaveis = $st_textovariaveis;
	}

	/**
	 * @param field_type $st_mascara
	 */
	public function setSt_mascara($st_mascara){ 
		$this->st_mascara = $st_mascara;
	}

	/**
	 * @param field_type $id_tipotextovariavel
	 */
	public function setId_tipotextovariavel($id_tipotextovariavel){ 
		$this->id_tipotextovariavel = $id_tipotextovariavel;
	}

}
<?php
/**
 * Classe que encapsula dados do professor de uma disciplina
 * @package models
 * @subpackage to
 */
class VwProfessorDisciplinaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contem o id do usuario
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contem o nome completo do usuario
	 * @var string
	 */
	public $st_nomecompleto;
	/**
	 * Contem o id da sala de aula
	 * @var int
	 */
	public $id_tipodisciplina;

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param int $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	
}
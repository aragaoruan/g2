<?php
/**
 * Classe para encapsular os dados que definem as recuperações do projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoRecuperacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id do tipo de recuperação.
	 * @var int
	 */
	public $id_tiporecuperacao;
	
	/**
	 * Valor máximo da nota de recuperação.
	 * @var int
	 */
	public $nu_valor;
	
	/**
	 * Percentual mínimo para fazer recuperação.
	 * @var float
	 */
	public $nu_percentmin;
	
	/**
	 * Máximo de aplicações que o aluno pode fazer de uma recuperação.
	 * @var int
	 */
	public $nu_maximoaplica;
	
	/**
	 * Qual máximo da nota de recuperação que o aluno pode aproveitar.
	 * @var int
	 */
	public $nu_maximoaproveita;
	
	/**
	 * Taxa cobrada pela aplicação da recuperação.
	 * @var int
	 */
	public $nu_taxa;
	
	/**
	 * Diz se a aplicação é pós-prova ou pré-prova.
	 * @var Boolean
	 */
	public $bl_aplicacao;
	
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiporecuperacao() {
		return $this->id_tiporecuperacao;
	}
	
	/**
	 * @return int
	 */
	public function getNu_maximoaplica() {
		return $this->nu_maximoaplica;
	}
	
	/**
	 * @return int
	 */
	public function getNu_maximoaproveita() {
		return $this->nu_maximoaproveita;
	}
	
	/**
	 * @return float
	 */
	public function getNu_percentmin() {
		return $this->nu_percentmin;
	}
	
	/**
	 * @return int
	 */
	public function getNu_taxa() {
		return $this->nu_taxa;
	}
	
	/**
	 * @return int
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $id_tiporecuperacao
	 */
	public function setId_tiporecuperacao($id_tiporecuperacao) {
		$this->id_tiporecuperacao = $id_tiporecuperacao;
	}
	
	/**
	 * @param int $nu_maximoaplica
	 */
	public function setNu_maximoaplica($nu_maximoaplica) {
		$this->nu_maximoaplica = $nu_maximoaplica;
	}
	
	/**
	 * @param int $nu_maximoaproveita
	 */
	public function setNu_maximoaproveita($nu_maximoaproveita) {
		$this->nu_maximoaproveita = $nu_maximoaproveita;
	}
	
	/**
	 * @param float $nu_percentmin
	 */
	public function setNu_percentmin($nu_percentmin) {
		$this->nu_percentmin = $nu_percentmin;
	}
	
	/**
	 * @param int $nu_taxa
	 */
	public function setNu_taxa($nu_taxa) {
		$this->nu_taxa = $nu_taxa;
	}
	
	/**
	 * @param int $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}
	/**
	 * @return the $bl_aplicacao
	 */
	public function getBl_aplicacao() {
		return $this->bl_aplicacao;
	}

	/**
	 * @param Boolean $bl_aplicacao
	 */
	public function setBl_aplicacao($bl_aplicacao) {
		$this->bl_aplicacao = $bl_aplicacao;
	}


}

?>
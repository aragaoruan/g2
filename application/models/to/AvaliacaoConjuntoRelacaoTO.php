<?php
/**
 * Classe para encapsular os dados de relacao do conjunto de avaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AvaliacaoConjuntoRelacaoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjuntorelacao;
	public $id_avaliacao;
	public $id_avaliacaoconjunto;
	public $id_avaliacaorecupera;
	
	/**
	 * @return the $id_avaliacaoconjuntorelacao
	 */
	public function getId_avaliacaoconjuntorelacao() {
		return $this->id_avaliacaoconjuntorelacao;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $id_avaliacaorecupera
	 */
	public function getId_avaliacaorecupera() {
		return $this->id_avaliacaorecupera;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntorelacao
	 */
	public function setId_avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao) {
		$this->id_avaliacaoconjuntorelacao = $id_avaliacaoconjuntorelacao;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $id_avaliacaorecupera
	 */
	public function setId_avaliacaorecupera($id_avaliacaorecupera) {
		$this->id_avaliacaorecupera = $id_avaliacaorecupera;
	}

	
}
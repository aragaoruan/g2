<?php
/**
 * tb_assuntoco
 * @package models
 * @subpackage to
 */
class AssuntoCoTO extends Ead1_TO_Dinamico {

	const TIPO_OCORRENCIA_ALUNO = 1;
	const TIPO_OCORRENCIA_TUTOR = 2;
	
	public $id_assuntoco;
	public $id_situacao;
	public $id_assuntocopai;
	public $id_entidadecadastro;
	public $id_usuariocadastro;
	public $id_tipoocorrencia;
	public $st_assuntoco;
	public $bl_autodistribuicao;
	public $bl_abertura;
	public $dt_cadastro;
	public $bl_ativo;
	public $id_textosistema;
	public $bl_cancelamento;
	public $bl_trancamento;
	
	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_assuntocopai
	 */
	public function getId_assuntocopai() {
		return $this->id_assuntocopai;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_tipoocorrencia
	 */
	public function getId_tipoocorrencia() {
		return $this->id_tipoocorrencia;
	}

	/**
	 * @return the $st_assuntoco
	 */
	public function getSt_assuntoco() {
		return $this->st_assuntoco;
	}

	/**
	 * @return the $bl_autodistribuicao
	 */
	public function getBl_autodistribuicao() {
		return $this->bl_autodistribuicao;
	}

	/**
	 * @return the $bl_abertura
	 */
	public function getBl_abertura() {
		return $this->bl_abertura;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
     * @param integer $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_assuntocopai
	 */
	public function setId_assuntocopai($id_assuntocopai) {
		$this->id_assuntocopai = $id_assuntocopai;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_tipoocorrencia
	 */
	public function setId_tipoocorrencia($id_tipoocorrencia) {
		$this->id_tipoocorrencia = $id_tipoocorrencia;
	}

	/**
	 * @param field_type $st_assuntoco
	 */
	public function setSt_assuntoco($st_assuntoco) {
		$this->st_assuntoco = $st_assuntoco;
	}

	/**
	 * @param field_type $bl_autodistribuicao
	 */
	public function setBl_autodistribuicao($bl_autodistribuicao) {
		$this->bl_autodistribuicao = $bl_autodistribuicao;
	}

	/**
	 * @param field_type $bl_abertura
	 */
	public function setBl_abertura($bl_abertura) {
		$this->bl_abertura = $bl_abertura;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}
	/**
	 * @return the $bl_cancelamento
	 */
	public function getBl_cancelamento() {
		return $this->bl_cancelamento;
	}

	/**
	 * @param field_type $bl_cancelamento
	 */
	public function setBl_cancelamento($bl_cancelamento) {
		$this->bl_cancelamento = $bl_cancelamento;
	}

	/**
	 * @return the $bl_trancamento
	 */
	public function getBl_trancamento() {
		return $this->bl_trancamento;
	}

	/**
	 * @param field_type $bl_trancamento
	 */
	public function setBl_trancamento($bl_trancamento) {
		$this->bl_trancamento = $bl_trancamento;
	}



}

?>
<?php

/**
 * Classe para encapsular os dados de aula.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class MatriculaTO extends Ead1_TO_Dinamico
{

    const EVOLUCAO_AGUARDANDO_CONFIRMACAO = 5;
    const EVOLUCAO_MATRICULA_CURSANDO = 6;
    const EVOLUCAO_CONCLUINTE = 15;
    const EVOLUCAO_CERTIFICADO = 16;
    const EVOLUCAO_RETORNADO = 26;
    const EVOLUCAO_SEM_RENOVACAO = 25;
    const EVOLUCAO_TRANSFERIDO = 20;
    const EVOLUCAO_TRANCADO = 21;
    CONST EVOLUCAO_CANCELADO = 27;
    CONST EVOLUCAO_BLOQUEADO = 40;
    const EVOLUCAO_ANULADO = 41;
    const SITUACAO_NAO_ATIVADA = 49;
    const SITUACAO_MATRICULA_ATIVA = 50;
    const SITUACAO_AGENDAMENTO_NAO_APTO = 119;
    const SITUACAO_AGENDAMENTO_APTO = 120;
    const EVOLUCAO_TRANSFERIR_ENTIDADE = 47;
    const EVOLUCAO_CERT_APTO_GERAR_CERTIFICADO = 61;
    const EVOLUCAO_CERT_CERTIFICADO_GERADO = 62;

    /**
     * Variavel que contem a chave primaria da tabela
     * @var int
     */
    public $id_matricula;

    /**
     * variavel que contem o id do projeto pedagogico
     * @var int
     */
    public $id_projetopedagogico;

    /**
     * Variavel que contem o id do usuario
     * @var int
     */
    public $id_usuario;

    /**
     * Variavel que contem o id do periodo letivo
     * @var int
     */
    public $id_periodoletivo;

    /**
     * Variavel que contem o usuario que cadastrou
     * @var int
     */
    public $id_usuariocadastro;

    /**
     * Variavel que contem o id da evolucao do usuario
     * @var int
     */
    public $id_evolucao;

    /**
     * Variavel que contem o id da situacao do usuario na matricula
     * @var int
     */
    public $id_situacao;

    /**
     * Variavel que contem o id do produto vendido
     * @var int
     */
    public $id_vendaproduto;

    /**
     * Variavel que contem o id da entidade em que o usuario foi matriculado
     * @var int
     */
    public $id_entidadematricula;

    /**
     * Variavel que contem o id da entidade que atendeu o aluno
     * @var int
     */
    public $id_entidadeatendimento;

    /**
     * Variavel que contem a entidade matriz do aluno
     * @var int
     */
    public $id_entidadematriz;

    /**
     * Variavel que contem a data de cadastro do aluno
     * @var zend_date
     */
    public $dt_cadastro;

    /**
     * Variavel que contem a data de inicio do aluno
     * @var zend_date
     */
    public $dt_inicio;

    /**
     * Variavel que contem o valor se o aluno esta ativo ou nao
     * @var boolean
     */
    public $bl_ativo;

    /**
     * Variavel que contem a data de conclusão do aluno
     * @var DateTime
     */
    public $dt_concluinte;

    /**
     * Variável que contem se é matricula institucional ou não
     * @var Boolean
     */
    public $bl_institucional;

    /**
     * Variável que contem o código(id) da turma
     * @var int
     */
    public $id_turma;

    /**
     * Contém id_matricula origem para casos de transferência.
     * @var int
     */
    public $id_matriculaorigem;

    /**
     * Contém id_situacao do agendamento
     * @var int
     */
    public $id_situacaoagendamento;

    /**
     * Contém a data de termino da matrícula do aluno
     * @var date
     */
    public $dt_termino;

    /**
     * Marca se a matrícula tem documentação completa para ser gerado o certificado
     * @var $bl_documentacao
     */
    public $bl_documentacao;

    public $st_codcertificacao;

    /**
     * Contém a Id da evolucao da certificacao da matrícula do aluno
     * @var id_evolucaocertificacao
     */
    public $id_evolucaocertificacao;

    /**
     * @var int $dt_aptoagendamento
     */
    public $dt_aptoagendamento;

    /**
     * Marca a situação parcial do agendamento para pós graduação
     * @var int $id_situacaoagendamentoparcial
     */
    public $id_situacaoagendamentoparcial;

    /**
     * @var integer $nu_semestreatual
     * @Column(name="nu_semestreatual", type="integer", nullable=true, length=1)
     */
    public $nu_semestreatual;


    /**
     * @return the $bl_documentacao
     */
    public function getBl_documentacao()
    {
        return $this->bl_documentacao;
    }

    /**
     * @param field_type $bl_documentacao
     */
    public function setBl_documentacao($bl_documentacao)
    {
        $this->bl_documentacao = $bl_documentacao;
    }

    /**
     * @param int $id_situacaoagendamento
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
    }

    /**
     * @return int
     */
    public function getId_Situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @return int $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $id_matriculaorigem
     */
    public function getId_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    /**
     * @return int $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $id_periodoletivo
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_vendaproduto
     */
    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @return the $id_entidadematricula
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @return the $id_entidadeatendimento
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @return the $id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $dt_concluinte
     */
    public function getDt_concluinte()
    {
        return $this->dt_concluinte;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param int $id_matriculaorigem
     */
    public function setId_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param int $id_periodoletivo
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param int $id_vendaproduto
     */
    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
    }

    /**
     * @param int $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @param int $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @param int $id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
    }

    /**
     * @param zend_date $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param zend_date $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param DateTime $dt_concluinte
     */
    public function setDt_concluinte($dt_concluinte)
    {
        $this->dt_concluinte = $dt_concluinte;
    }

    /**
     * @return the $bl_institucional
     */
    public function getBl_institucional()
    {
        return $this->bl_institucional;
    }

    /**
     * @param boolean $bl_institucional
     */
    public function setBl_institucional($bl_institucional)
    {
        $this->bl_institucional = $bl_institucional;
    }

    /**
     * @return the $id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     *
     * @param date $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return date Data de termino da matricula do aluno
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @return string st_codcertificacao
     */
    public function getSt_codcertificacao()
    {
        return $this->st_codcertificacao;
    }

    /**
     * @param st_codcertificacao
     */
    public function setSt_codcertificacao($st_codcertificacao)
    {
        $this->st_codcertificacao = $st_codcertificacao;
        return $this;
    }

    /**
     * @return id_evolucaocertificacao
     */
    public function getId_evolucaocertificacao()
    {
        return $this->id_evolucaocertificacao;
    }

    /**
     * @param id_evolucaocertificacao $id_evolucaocertificacao
     */
    public function setId_evolucaocertificacao($id_evolucaocertificacao)
    {
        $this->id_evolucaocertificacao = $id_evolucaocertificacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getdt_aptoagendamento()
    {
        return $this->dt_aptoagendamento;
    }

    /**
     * @param int $dt_aptoagendamento
     * @return $this
     */
    public function setdt_aptoagendamento($dt_aptoagendamento)
    {
        $this->dt_aptoagendamento = $dt_aptoagendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamentoparcial()
    {
        return $this->id_situacaoagendamentoparcial;
    }

    /**
     * @param int $id_situacaoagendamentoparcial
     */
    public function setId_situacaoagendamentoparcial($id_situacaoagendamentoparcial)
    {
        $this->id_situacaoagendamentoparcial = $id_situacaoagendamentoparcial;
    }

    /**
     * @return int
     */
    public function getNu_semestreatual()
    {
        return $this->nu_semestreatual;
    }

    public function setNu_semestreatual($nu_semestreatual)
    {
        $this->nu_semestreatual = $nu_semestreatual;
    }


}

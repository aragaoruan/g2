<?php
/**
 * Classe para encapsular os dados do tipo do valor do produto
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class TipoProdutoValorTO extends Ead1_TO_Dinamico{
	
	public $id_tipoprodutovalor;
	public $st_tipoprodutovalor;
	public $st_descricao;
	
	/**
	 * @return the $id_tipoprodutovalor
	 */
	public function getId_tipoprodutovalor() {
		return $this->id_tipoprodutovalor;
	}

	/**
	 * @return the $st_tipoprodutovalor
	 */
	public function getSt_tipoprodutovalor() {
		return $this->st_tipoprodutovalor;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param field_type $id_tipoprodutovalor
	 */
	public function setId_tipoprodutovalor($id_tipoprodutovalor) {
		$this->id_tipoprodutovalor = $id_tipoprodutovalor;
	}

	/**
	 * @param field_type $st_tipoprodutovalor
	 */
	public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
		$this->st_tipoprodutovalor = $st_tipoprodutovalor;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
}
<?php
/**
 * Classe para encapsular os dados de view de agência e conta da entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeContaTO extends Ead1_TO_Dinamico {

	public $id_contaentidade;
	public $id_entidade;
	public $st_agencia;
	public $st_conta;
	public $id_banco;
	public $st_nomebanco;
	public $id_tipodeconta;
	public $st_tipodeconta;
	public $st_bancoconta;
	public $bl_padrao;
	
	/**
	 * @return the $id_contaentidade
	 */
	public function getId_contaentidade() {
		return $this->id_contaentidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_agencia
	 */
	public function getSt_agencia() {
		return $this->st_agencia;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $id_banco
	 */
	public function getId_banco() {
		return $this->id_banco;
	}

	/**
	 * @return the $st_nomebanco
	 */
	public function getSt_nomebanco() {
		return $this->st_nomebanco;
	}

	/**
	 * @return the $id_tipodeconta
	 */
	public function getId_tipodeconta() {
		return $this->id_tipodeconta;
	}

	/**
	 * @return the $st_tipodeconta
	 */
	public function getSt_tipodeconta() {
		return $this->st_tipodeconta;
	}

	/**
	 * @return the $st_bancoconta
	 */
	public function getSt_bancoconta() {
		return $this->st_bancoconta;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @param field_type $id_contaentidade
	 */
	public function setId_contaentidade($id_contaentidade) {
		$this->id_contaentidade = $id_contaentidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_agencia
	 */
	public function setSt_agencia($st_agencia) {
		$this->st_agencia = $st_agencia;
	}

	/**
	 * @param field_type $st_conta
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param field_type $id_banco
	 */
	public function setId_banco($id_banco) {
		$this->id_banco = $id_banco;
	}

	/**
	 * @param field_type $st_nomebanco
	 */
	public function setSt_nomebanco($st_nomebanco) {
		$this->st_nomebanco = $st_nomebanco;
	}

	/**
	 * @param field_type $id_tipodeconta
	 */
	public function setId_tipodeconta($id_tipodeconta) {
		$this->id_tipodeconta = $id_tipodeconta;
	}

	/**
	 * @param field_type $st_tipodeconta
	 */
	public function setSt_tipodeconta($st_tipodeconta) {
		$this->st_tipodeconta = $st_tipodeconta;
	}

	/**
	 * @param field_type $st_bancoconta
	 */
	public function setSt_bancoconta($st_bancoconta) {
		$this->st_bancoconta = $st_bancoconta;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

}

?>
<?php
/**
 * Classe para encapsular dados da tabela de textos do sistema
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class TextoSistemaTO extends Ead1_TO_Dinamico{

	public $id_textosistema;
	public $id_usuario;
	public $id_entidade;
	public $id_textoexibicao;
	public $id_textocategoria;
	public $id_situacao;
	public $st_textosistema;
	public $st_texto;
	public $dt_inicio;
	public $dt_fim;
	public $dt_cadastro;
	public $id_orientacaotexto;
    public $bl_cabecalho;
	public $id_cabecalho;
	public $id_rodape;
    public $bl_edicao = 1;


	/**
	 * @return the $id_orientacaotexto
	 */
	public function getId_orientacaotexto(){

		return $this->id_orientacaotexto;
	}

	/**
	 * @param $id_orientacaotexto the $id_orientacaotexto to set
	 */
	public function setId_orientacaotexto( $id_orientacaotexto ){

		$this->id_orientacaotexto = $id_orientacaotexto;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_textoexibicao
	 */
	public function getId_textoexibicao() {
		return $this->id_textoexibicao;
	}

	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria() {
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema() {
		return $this->st_textosistema;
	}

	/**
	 * @return the $st_texto
	 */
	public function getSt_texto() {
		return $this->st_texto;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param $id_textosistema the $id_textosistema to set
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_textoexibicao the $id_textoexibicao to set
	 */
	public function setId_textoexibicao($id_textoexibicao) {
		$this->id_textoexibicao = $id_textoexibicao;
	}

	/**
	 * @param $id_textocategoria the $id_textocategoria to set
	 */
	public function setId_textocategoria($id_textocategoria) {
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param $st_textosistema the $st_textosistema to set
	 */
	public function setSt_textosistema($st_textosistema) {
		$this->st_textosistema = $st_textosistema;
	}

	/**
	 * @param $st_texto the $st_texto to set
	 */
	public function setSt_texto($st_texto) {
		$this->st_texto = $st_texto;
	}

	/**
	 * @param $dt_inicio the $dt_inicio to set
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param $dt_fim the $dt_fim to set
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

    public  function getBl_cabecalho() {
    	return $this->bl_cabecalho;
    }

    public  function setBl_cabecalho($bl_cabecalho) {
         $this->bl_cabecalho = $bl_cabecalho;
    }

	public function getId_cabecalho()
	{
		return $this->id_cabecalho;
	}

	public function setId_cabecalho($id_cabecalho)
	{
		$this->id_cabecalho = $id_cabecalho;
	}

	public function getId_rodape()
	{
		return $this->id_rodape;
	}

	public function setId_rodape($id_rodape)
	{
		$this->id_rodape = $id_rodape;
	}

    /**
     * @return string
     */
    public function getBl_edicao()
    {
        return $this->bl_edicao;
    }

    /**
     * @param string $bl_edicao
     */
    public function setBl_edicao($bl_edicao)
    {
        $this->bl_edicao = $bl_edicao;
    }
}

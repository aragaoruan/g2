<?php
/**
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class ProdutoPrrTO extends Ead1_TO_Dinamico{

        public $id_saladeaula;
        public $id_produto;
        public $id_entidade;

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return mixed
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param mixed $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return mixed
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }


}
<?php


/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class VwMensagemCobrancaRoboTO extends Ead1_TO_Dinamico {

	
	public $id_venda;
	public $nu_valorliquido;
	public $id_lancamento;
	public $dt_vencimento;
	public $nu_diasatraso;
	public $id_textosistema;
	public $st_textosistema;
	public $id_entidade;
	public $id_usuario;
	
	
	
	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $nu_valorliquido
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $nu_diasatraso
	 */
	public function getNu_diasatraso() {
		return $this->nu_diasatraso;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema() {
		return $this->st_textosistema;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}

	/**
	 * @param field_type $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @param field_type $nu_diasatraso
	 */
	public function setNu_diasatraso($nu_diasatraso) {
		$this->nu_diasatraso = $nu_diasatraso;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $st_textosistema
	 */
	public function setSt_textosistema($st_textosistema) {
		$this->st_textosistema = $st_textosistema;
	}

	
	
	
}



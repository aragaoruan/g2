<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class EntidadeFinanceiroTO extends Ead1_TO_Dinamico
{

    public $id_entidadefinanceiro;
    public $id_entidade;
    public $bl_automatricula;
    public $id_textosistemarecibo;
    public $bl_protocolo;
    public $st_linkloja;
    public $nu_avisoatraso;
    public $id_textoavisoatraso;
    public $nu_diaspagamentoentrada;
    public $id_reciboconsolidado;
    public $id_textochequesdevolvidos;
    public $nu_multacomcarta;
    public $nu_multasemcarta;
    public $id_textoimpostorenda;

    /**
     * @return the $st_linkloja
     */
    public function getSt_linkloja()
    {
        return $this->st_linkloja;
    }

    /**
     * @param field_type $st_linkloja
     */
    public function setSt_linkloja($st_linkloja)
    {
        $this->st_linkloja = $st_linkloja;
    }

    /**
     * @return the $nu_diaspagamentoentrada
     */
    public function getNu_diaspagamentoentrada()
    {
        return $this->nu_diaspagamentoentrada;
    }

    /**
     * @param field_type $nu_diaspagamentoentrada
     */
    public function setNu_diaspagamentoentrada($nu_diaspagamentoentrada)
    {
        $this->nu_diaspagamentoentrada = $nu_diaspagamentoentrada;
    }

    /**
     * @return the $id_entidadefinanceiro
     */
    public function getId_entidadefinanceiro()
    {
        return $this->id_entidadefinanceiro;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $bl_automatricula
     */
    public function getBl_automatricula()
    {
        return $this->bl_automatricula;
    }

    /**
     * @return the $id_textosistemarecibo
     */
    public function getId_textosistemarecibo()
    {
        return $this->id_textosistemarecibo;
    }

    /**
     * @return the $bl_protocolo
     */
    public function getBl_protocolo()
    {
        return $this->bl_protocolo;
    }


    /**
     * @param field_type $id_entidadefinanceiro
     */
    public function setId_entidadefinanceiro($id_entidadefinanceiro)
    {
        $this->id_entidadefinanceiro = $id_entidadefinanceiro;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $bl_automatricula
     */
    public function setBl_automatricula($bl_automatricula)
    {
        $this->bl_automatricula = $bl_automatricula;
    }

    /**
     * @param field_type $id_textosistemarecibo
     */
    public function setId_textosistemarecibo($id_textosistemarecibo)
    {
        $this->id_textosistemarecibo = $id_textosistemarecibo;
    }

    /**
     * @param field_type $bl_protocolo
     */
    public function setBl_protocolo($bl_protocolo)
    {
        $this->bl_protocolo = $bl_protocolo;
    }

    /**
     * @return the $id_textoavisoatraso
     */
    public function getId_textoavisoatraso()
    {
        return $this->id_textoavisoatraso;
    }

    /**
     * @param field_type $id_textoavisoatraso
     */
    public function setId_textoavisoatraso($id_textoavisoatraso)
    {
        $this->id_textoavisoatraso = $id_textoavisoatraso;
    }

    /**
     * @return mixed
     */
    public function getId_textochequesdevolvidos()
    {
        return $this->id_textochequesdevolvidos;
    }

    /**
     * @param mixed $id_textochequesdevolvidos
     */
    public function setId_textochequesdevolvidos($id_textochequesdevolvidos)
    {
        $this->id_textochequesdevolvidos = $id_textochequesdevolvidos;
    }

    /**
     * @return mixed
     */
    public function getId_textoimpostorenda()
    {
        return $this->id_textoimpostorenda;
    }

    /**
     * @param mixed $id_textoimpostorenda
     */
    public function setId_textoimpostorenda($id_textoimpostorenda)
    {
        $this->id_textoimpostorenda = $id_textoimpostorenda;
    }

    /**
     * @return mixed
     */
    public function getNu_avisoatraso()
    {
        return $this->nu_avisoatraso;
    }

    /**
     * @param mixed $nu_avisoatraso
     */
    public function setNu_avisoatraso($nu_avisoatraso)
    {
        $this->nu_avisoatraso = $nu_avisoatraso;
    }

    /**
     * @return mixed
     */
    public function getId_reciboconsolidado()
    {
        return $this->id_reciboconsolidado;
    }

    /**
     * @param mixed $id_reciboconsolidado
     */
    public function setId_reciboconsolidado($id_reciboconsolidado)
    {
        $this->id_reciboconsolidado = $id_reciboconsolidado;
    }

}
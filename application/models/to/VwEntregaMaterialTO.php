<?php
/**
 * Classe para encapsular os dados da view de entrega de material.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntregaMaterialTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $id_modulo;
	public $st_modulo;
	public $id_disciplina;
	public $st_disciplina;
	public $id_situacaodisciplina;
	public $st_situacaodisciplina;
	public $id_itemdematerial;
	public $st_itemdematerial;
	public $id_situacaoentrega;
	public $st_situacaoentrega;
	public $id_entregamaterial;
	public $bl_ativo;
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_modulo
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $id_situacaodisciplina
	 */
	public function getId_situacaodisciplina() {
		return $this->id_situacaodisciplina;
	}

	/**
	 * @return the $st_situacaodisciplina
	 */
	public function getSt_situacaodisciplina() {
		return $this->st_situacaodisciplina;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @return the $st_itemdematerial
	 */
	public function getSt_itemdematerial() {
		return $this->st_itemdematerial;
	}

	/**
	 * @return the $id_situacaoentrega
	 */
	public function getId_situacaoentrega() {
		return $this->id_situacaoentrega;
	}

	/**
	 * @return the $st_situacaoentrega
	 */
	public function getSt_situacaoentrega() {
		return $this->st_situacaoentrega;
	}

	/**
	 * @return the $id_entregamaterial
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param field_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $id_situacaodisciplina
	 */
	public function setId_situacaodisciplina($id_situacaodisciplina) {
		$this->id_situacaodisciplina = $id_situacaodisciplina;
	}

	/**
	 * @param field_type $st_situacaodisciplina
	 */
	public function setSt_situacaodisciplina($st_situacaodisciplina) {
		$this->st_situacaodisciplina = $st_situacaodisciplina;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	/**
	 * @param field_type $st_itemdematerial
	 */
	public function setSt_itemdematerial($st_itemdematerial) {
		$this->st_itemdematerial = $st_itemdematerial;
	}

	/**
	 * @param field_type $id_situacaoentrega
	 */
	public function setId_situacaoentrega($id_situacaoentrega) {
		$this->id_situacaoentrega = $id_situacaoentrega;
	}

	/**
	 * @param field_type $st_situacaoentrega
	 */
	public function setSt_situacaoentrega($st_situacaoentrega) {
		$this->st_situacaoentrega = $st_situacaoentrega;
	}

	/**
	 * @param field_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}


	

}
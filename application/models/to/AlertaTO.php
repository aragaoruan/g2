<?php

/**
 * Description of AlertaTO
 *
 * @author Yannick Naquis Roulé
 */
class AlertaTO extends Ead1_TO_Dinamico{
    
    var $id_mensagempadrao;
    var $id_matricula;
    var $dt_cadastro;
    
    public function getId_mensagempadrao() {
        return $this->id_mensagempadrao;
    }

    public function setId_mensagempadrao($id_mensagempadrao) {
        $this->id_mensagempadrao = $id_mensagempadrao;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }


}

?>

<?php
/**
 * Classe para encapsular os dados de desconto da campanha comercial.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaDescontoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do desconto da campanha.
	 * @var int
	 */
	public $id_campanhadesconto;
	
	/**
	 * Valor minimo.
	 * @var float
	 */
	public $nu_valormin;
	
	/**
	 * Valor máximo.
	 * @var float
	 */
	public $nu_valormax;
	
	/**
	 * Id da campanha comercial.
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * Id do meio de pagamento.
	 * @var int
	 */
	public $id_meiopagamento;
	
	/**
	 * Valor mínimo do desconto
	 * @var float
	 */
	public $nu_descontominimo;
	
	/**
	 * Valor máximo do desconto.
	 * @var float
	 */
	public $nu_descontomaximo;
	
	/**
	 * @return int
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return int
	 */
	public function getId_campanhadesconto() {
		return $this->id_campanhadesconto;
	}
	
	/**
	 * @return int
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}
	
	/**
	 * @return float
	 */
	public function getNu_descontomaximo() {
		return $this->nu_descontomaximo;
	}
	
	/**
	 * @return float
	 */
	public function getNu_descontominimo() {
		return $this->nu_descontominimo;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valormax() {
		return $this->nu_valormax;
	}
	
	/**
	 * @return float
	 */
	public function getNu_valormin() {
		return $this->nu_valormin;
	}
	
	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param int $id_campanhadesconto
	 */
	public function setId_campanhadesconto($id_campanhadesconto) {
		$this->id_campanhadesconto = $id_campanhadesconto;
	}
	
	/**
	 * @param int $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}
	
	/**
	 * @param float $nu_descontomaximo
	 */
	public function setNu_descontomaximo($nu_descontomaximo) {
		$this->nu_descontomaximo = $nu_descontomaximo;
	}
	
	/**
	 * @param float $nu_descontominimo
	 */
	public function setNu_descontominimo($nu_descontominimo) {
		$this->nu_descontominimo = $nu_descontominimo;
	}
	
	/**
	 * @param float $nu_valormax
	 */
	public function setNu_valormax($nu_valormax) {
		$this->nu_valormax = $nu_valormax;
	}
	
	/**
	 * @param float $nu_valormin
	 */
	public function setNu_valormin($nu_valormin) {
		$this->nu_valormin = $nu_valormin;
	}

}

?>
<?php
/** 
 * Classe para encapsular os dados da tabela de campos do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class CampoRelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_camporelatorio;
	public $id_relatorio;
	public $st_camporelatorio;
	public $st_titulocampo;
	public $bl_filtro;
	public $bl_exibido;
	public $nu_ordem;
	
	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}
	
	/**
	 * @param $nu_ordem the $nu_ordem to set
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}
	
	/**
	 * @return the $id_camporelatorio
	 */
	public function getId_camporelatorio() {
		return $this->id_camporelatorio;
	}

	/**
	 * @return the $id_relatorio
	 */
	public function getId_relatorio() {
		return $this->id_relatorio;
	}

	/**
	 * @return the $st_camporelatorio
	 */
	public function getSt_camporelatorio() {
		return $this->st_camporelatorio;
	}

	/**
	 * @return the $st_titulocampo
	 */
	public function getSt_titulocampo() {
		return $this->st_titulocampo;
	}

	/**
	 * @return the $bl_filtro
	 */
	public function getBl_filtro() {
		return $this->bl_filtro;
	}

	/**
	 * @return the $bl_exibido
	 */
	public function getBl_exibido() {
		return $this->bl_exibido;
	}

	/**
	 * @param $id_camporelatorio the $id_camporelatorio to set
	 */
	public function setId_camporelatorio($id_camporelatorio) {
		$this->id_camporelatorio = $id_camporelatorio;
	}

	/**
	 * @param $id_relatorio the $id_relatorio to set
	 */
	public function setId_relatorio($id_relatorio) {
		$this->id_relatorio = $id_relatorio;
	}

	/**
	 * @param $st_camporelatorio the $st_camporelatorio to set
	 */
	public function setSt_camporelatorio($st_camporelatorio) {
		$this->st_camporelatorio = $st_camporelatorio;
	}

	/**
	 * @param $st_titulocampo the $st_titulocampo to set
	 */
	public function setSt_titulocampo($st_titulocampo) {
		$this->st_titulocampo = $st_titulocampo;
	}

	/**
	 * @param $bl_filtro the $bl_filtro to set
	 */
	public function setBl_filtro($bl_filtro) {
		$this->bl_filtro = $bl_filtro;
	}

	/**
	 * @param $bl_exibido the $bl_exibido to set
	 */
	public function setBl_exibido($bl_exibido) {
		$this->bl_exibido = $bl_exibido;
	}


}
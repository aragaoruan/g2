<?php
/**
 * Classe para encapsular os dados da view de permissão de perfil para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPerfilPermissaoFuncionalidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Contém o nome do perfil.
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * Contém o id do perfil pai.
	 * @var int
	 */
	public $id_perfilpai;
	
	/**
	 * Diz se o perfil está ativo.
	 * @var boolean
	 */
	public $bl_ativoperfil;
	
	/**
	 * Contém o id da situação do perfil.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contém a descrição da situação.
	 * @var string
	 */
	public $st_descricaosituacao;
	
	/**
	 * Contém o id da permissão.
	 * @var int
	 */
	public $id_permissao;
	
	/**
	 * Contém o nome da permissão.
	 * @var string
	 */
	public $st_nomepermissao;
	
	/**
	 * Contém a descrição.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Diz se a permissão está ativa.
	 * @var boolean
	 */
	public $bl_ativopermissao;
	
	/**
	 * Contém o id da funcionalidade.
	 * @var int
	 */
	public $id_funcionalidade;
	
	/**
	 * Contém o id da funcionalidade pai.
	 * @var int
	 */
	public $id_funcionalidadepai;
	
	/**
	 * Contém a classe do flex.
	 * @var string
	 */
	public $st_classeflex;
	
	/**
	 * Contém a funcionalidade.
	 * @var string
	 */
	public $st_funcionalidade;
	
	/**
	 * verifica se o perfil é selecionado
	 * @var boolean
	 */
	public $bl_selecionado;
	
	/**
	 * 
	 * @var boolean
	 */
	public $bl_ativofuncionalidade;
	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}
	/**
	 * @return the $bl_selecionado
	 */
	public function getBl_selecionado() {
		return $this->bl_selecionado;
	}

	/**
	 * @return the $id_perfilpai
	 */
	public function getId_perfilpai() {
		return $this->id_perfilpai;
	}

	/**
	 * @return the $bl_ativoperfil
	 */
	public function getBl_ativoperfil() {
		return $this->bl_ativoperfil;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_descricaosituacao
	 */
	public function getSt_descricaosituacao() {
		return $this->st_descricaosituacao;
	}

	/**
	 * @return the $id_permissao
	 */
	public function getId_permissao() {
		return $this->id_permissao;
	}

	/**
	 * @return the $st_nomepermissao
	 */
	public function getSt_nomepermissao() {
		return $this->st_nomepermissao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $bl_ativopermissao
	 */
	public function getBl_ativopermissao() {
		return $this->bl_ativopermissao;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $id_funcionalidadepai
	 */
	public function getId_funcionalidadepai() {
		return $this->id_funcionalidadepai;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @return the $st_funcionalidade
	 */
	public function getSt_funcionalidade() {
		return $this->st_funcionalidade;
	}

	/**
	 * @return the $bl_ativofuncionalidade
	 */
	public function getBl_ativofuncionalidade() {
		return $this->bl_ativofuncionalidade;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param $bl_selecionado the $bl_selecionado to set
	 */
	public function setBl_selecionado($bl_selecionado) {
		$this->bl_selecionado = $bl_selecionado;
	}

	/**
	 * @param $st_nomeperfil the $st_nomeperfil to set
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param $id_perfilpai the $id_perfilpai to set
	 */
	public function setId_perfilpai($id_perfilpai) {
		$this->id_perfilpai = $id_perfilpai;
	}

	/**
	 * @param $bl_ativoperfil the $bl_ativoperfil to set
	 */
	public function setBl_ativoperfil($bl_ativoperfil) {
		$this->bl_ativoperfil = $bl_ativoperfil;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_descricaosituacao the $st_descricaosituacao to set
	 */
	public function setSt_descricaosituacao($st_descricaosituacao) {
		$this->st_descricaosituacao = $st_descricaosituacao;
	}

	/**
	 * @param $id_permissao the $id_permissao to set
	 */
	public function setId_permissao($id_permissao) {
		$this->id_permissao = $id_permissao;
	}

	/**
	 * @param $st_nomepermissao the $st_nomepermissao to set
	 */
	public function setSt_nomepermissao($st_nomepermissao) {
		$this->st_nomepermissao = $st_nomepermissao;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $bl_ativopermissao the $bl_ativopermissao to set
	 */
	public function setBl_ativopermissao($bl_ativopermissao) {
		$this->bl_ativopermissao = $bl_ativopermissao;
	}

	/**
	 * @param $id_funcionalidade the $id_funcionalidade to set
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param $id_funcionalidadepai the $id_funcionalidadepai to set
	 */
	public function setId_funcionalidadepai($id_funcionalidadepai) {
		$this->id_funcionalidadepai = $id_funcionalidadepai;
	}

	/**
	 * @param $st_classeflex the $st_classeflex to set
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}

	/**
	 * @param $st_funcionalidade the $st_funcionalidade to set
	 */
	public function setSt_funcionalidade($st_funcionalidade) {
		$this->st_funcionalidade = $st_funcionalidade;
	}

	/**
	 * @param $bl_ativofuncionalidade the $bl_ativofuncionalidade to set
	 */
	public function setBl_ativofuncionalidade($bl_ativofuncionalidade) {
		$this->bl_ativofuncionalidade = $bl_ativofuncionalidade;
	}


}

?>
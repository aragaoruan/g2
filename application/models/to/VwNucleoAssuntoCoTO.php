<?php

/**
 * Classe para encapsular os dados da vw_nucleoassuntoco
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwNucleoAssuntoCoTO extends Ead1_TO_Dinamico
{

	public $st_assuntoco;
	public $bl_ativo;
	public $bl_abertura;
	public $st_situacao;
	public $bl_autodistribuicao;
	public $id_nucleoassuntoco;
	public $id_nucleoco;
	public $st_tipoocorrencia;
	public $st_assuntopai;
	public $id_assuntoco;
	public $id_tipoocorrencia;
   	public $id_textonotificacao;
   	public $id_entidade;
   	public $bl_notificaatendente;
	public $id_nucleofinalidade;
	public $id_assuntocategoria;
    public $bl_encerramento;

	/**
	 * @return string $st_assuntoco
	 */
	public function getSt_assuntoco() {
		return $this->st_assuntoco;
	}

    /**
     * @param string $st_assuntoco
     */
    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    /**
     * @return bool $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return bool $bl_abertura
     */
    public function getBl_abertura()
    {
        return $this->bl_abertura;
    }

    /**
     * @param bool $bl_abertura
     */
    public function setBl_abertura($bl_abertura)
    {
        $this->bl_abertura = $bl_abertura;
    }

    /**
     * @return bool $st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return bool $bl_autodistribuicao
     */
    public function getBl_autodistribuicao()
    {
        return $this->bl_autodistribuicao;
    }

    /**
     * @param bool $bl_autodistribuicao
     */
    public function setBl_autodistribuicao($bl_autodistribuicao)
    {
        $this->bl_autodistribuicao = $bl_autodistribuicao;
    }

    /**
     * @return int $id_nucleoassuntoco
     */
    public function getId_nucleoassuntoco()
    {
        return $this->id_nucleoassuntoco;
    }

    /**
     * @param int $id_nucleoassuntoco
     */
    public function setId_nucleoassuntoco($id_nucleoassuntoco)
    {
        $this->id_nucleoassuntoco = $id_nucleoassuntoco;
    }

    /**
     * @return int $id_nucleoco
     */
    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    /**
     * @return string $st_tipoocorrencia
     */
    public function getSt_tipoocorrencia()
    {
        return $this->st_tipoocorrencia;
    }

    /**
     * @param string $st_tipoocorrencia
     */
    public function setSt_tipoocorrencia($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
    }

    /**
     * @return string $st_assuntopai
     */
    public function getSt_assuntopai()
    {
        return $this->st_assuntopai;
    }

    /**
     * @param string $st_assuntopai
     */
    public function setSt_assuntopai($st_assuntopai)
    {
        $this->st_assuntopai = $st_assuntopai;
    }

    /**
     * @return int $id_assuntoco
     */
    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param int $id_assuntoco
     */
    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    /**
     * @return int $id_tipoocorrencia
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     */
    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    /**
     * @return int $id_textonotificacao
     */
    public function getId_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    /**
     * @param int $id_textonotificacao
     */
    public function setId_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return bool $bl_notificaatendente
     */
    public function getBl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param bool $bl_notificaatendente
     */
    public function setBl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
    }

    /**
     * @return int $id_nucleofinalidade
     */
    public function getId_nucleofinalidade()
    {
        return $this->id_nucleofinalidade;
    }

	/**
	 * @param int $id_nucleofinalidade
	 */
	public function setId_nucleofinalidade($id_nucleofinalidade)
		{$this->id_nucleofinalidade = $id_nucleofinalidade;
	}

    /**
     * @return int $id_assuntocategoria
     */
    public function getId_assuntocategoria()
    {
        return $this->id_assuntocategoria;
    }

    /**
     * @param int $id_assuntocategoria
     */
    public function setId_assuntocategoria($id_assuntocategoria)
    {
        $this->id_assuntocategoria = $id_assuntocategoria;
    }
    /**
     * @return mixed
     */
    public function getBl_encerramento()
    {
        return $this->bl_encerramento;
    }

    /**
     * @param mixed $bl_encerramento
     */
    public function setBl_encerramento($bl_encerramento)
    {
        $this->bl_encerramento = $bl_encerramento;
    }

}

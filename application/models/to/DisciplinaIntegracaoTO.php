<?php

/**
 * Classe para encapsular os dados de integração da disciplina.
 * @author Eder Lamar Mariano
 * @package models
 * @subpackage to
 */
class DisciplinaIntegracaoTO extends Ead1_TO_Dinamico
{

    public $id_disciplinaintegracao;
    public $id_disciplina;
    public $id_sistema;
    public $id_usuariocadastro;
    public $st_codsistema;
    public $dt_cadastro;
    public $id_entidade;
    public $bl_ativo;
    public $st_salareferencia;
    public $id_entidadeintegracao;

    /**
     * @return mixed
     */
    public function getid_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    /**
     * @param mixed $id_disciplinaintegracao
     * @return DisciplinaIntegracaoTO
     */
    public function setid_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param mixed $id_disciplina
     * @return DisciplinaIntegracaoTO
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     * @return DisciplinaIntegracaoTO
     */
    public function setid_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $id_usuariocadastro
     * @return DisciplinaIntegracaoTO
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param mixed $st_codsistema
     * @return DisciplinaIntegracaoTO
     */
    public function setst_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return DisciplinaIntegracaoTO
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     * @return DisciplinaIntegracaoTO
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     * @return DisciplinaIntegracaoTO
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getst_salareferencia()
    {
        return $this->st_salareferencia;
    }

    /**
     * @param mixed $st_salareferencia
     * @return DisciplinaIntegracaoTO
     */
    public function setst_salareferencia($st_salareferencia)
    {
        $this->st_salareferencia = $st_salareferencia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param mixed $id_entidadeintegracao
     * @return DisciplinaIntegracaoTO
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


}

<?php
/**
 * Classe para encapsular os dados da tb_modelovitrine
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class ModeloVitrineTO extends Ead1_TO_Dinamico {

	public $id_modelovitrine;
	public $st_modelovitrine;
	
	
	
	/**
	 * @return the $id_modelovitrine
	 */
	public function getId_modelovitrine() {
		return $this->id_modelovitrine;
	}

	/**
	 * @param field_type $id_modelovitrine
	 */
	public function setId_modelovitrine($id_modelovitrine) {
		$this->id_modelovitrine = $id_modelovitrine;
	}

	/**
	 * @return the $st_modelovitrine
	 */
	public function getSt_modelovitrine() {
		return $this->st_modelovitrine;
	}

	/**
	 * @param field_type $st_modelovitrine
	 */
	public function setSt_modelovitrine($st_modelovitrine) {
		$this->st_modelovitrine = $st_modelovitrine;
	}

	
	
}

?>
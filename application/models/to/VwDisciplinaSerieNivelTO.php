<?php

/**
 * Classe para encapsular os dados da view de disciplina com série e nível de ensino.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwDisciplinaSerieNivelTO extends Ead1_TO_Dinamico
{

    /**
     * Id da disciplina.
     * @var int
     */
    public $id_disciplina;

    public $id_entidade;

    /**
     * Id da série.
     * @var int
     */
    public $id_serie;

    /**
     * Id do nível de ensino.
     * @var int
     */
    public $id_nivelensino;

    /**
     * Série.
     * @var string
     */
    public $st_serie;

    /**
     * Nome da disciplina.
     * @var string
     */
    public $st_disciplina;

    /**
     * Id da série anterior.
     * @var int
     */
    public $id_serieanterior;

    /**
     * Código idenficador da disciplina.
     * @var string
     */
    public $nu_identificador;

    /**
     * Carga horária da disciplina.
     * @var int
     */
    public $nu_cargahoraria;

    /**
     * Diz se a disciplina está ativa.
     * @var boolean
     */
    public $bl_ativa;

    /**
     * Id da situação da disciplina.
     * @var int
     */
    public $id_situacao;

    /**
     * Nível de ensino.
     * @var string
     */
    public $st_nivelensino;

    /**
     * Descrição da disciplina.
     * @var string
     */
    public $st_descricao;

    public $nu_repeticao;


    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativa()
    {
        return $this->bl_ativa;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @return int
     */
    public function getId_serie()
    {
        return $this->id_serie;
    }

    /**
     * @return int
     */
    public function getId_serieanterior()
    {
        return $this->id_serieanterior;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @return string
     */
    public function getNu_identificador()
    {
        return $this->nu_identificador;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_nivelensino()
    {
        return $this->st_nivelensino;
    }

    /**
     * @return string
     */
    public function getSt_serie()
    {
        return $this->st_serie;
    }

    /**
     * @param boolean $bl_ativa
     */
    public function setBl_ativa($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param int $id_nivelensino
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
    }

    /**
     * @param int $id_serie
     */
    public function setId_serie($id_serie)
    {
        $this->id_serie = $id_serie;
    }

    /**
     * @param int $id_serieanterior
     */
    public function setId_serieanterior($id_serieanterior)
    {
        $this->id_serieanterior = $id_serieanterior;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @param string $nu_identificador
     */
    public function setNu_identificador($nu_identificador)
    {
        $this->nu_identificador = $nu_identificador;
    }

    /**
     * @param string $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @param string $st_nivelensino
     */
    public function setSt_nivelensino($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
    }

    /**
     * @param string $st_serie
     */
    public function setSt_serie($st_serie)
    {
        $this->st_serie = $st_serie;
    }


    public function getNu_repeticao()
    {
        return $this->nu_repeticao;
    }

    public function setNu_repeticao($nu_repeticao)
    {
        $this->nu_repeticao = $nu_repeticao;
    }


}


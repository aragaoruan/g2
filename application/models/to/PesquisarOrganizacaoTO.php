<?php
/**
 * Classe para encapsular os dados de endereço.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PesquisarOrganizacaoTO extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_instituicao;
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_polo;

	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_grupo;
	
	/**
	 * Atributo que diz qual o filtro da pesquisa
	 * @var boolean
	 */
	public $bl_nucleo;
	
	/**
	 * Atributo que contem o id da situação
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Atributo que contem o numero do cnpj
	 * @var int
	 */
	public $st_cnpj;
	
	/**
	 * Atributo que contem a razao social
	 * @var string
	 */
	public $st_razaosocial;
	
	/**
	 * Atributo que contem o nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Atributo que contem o id da entidade cadatradora
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	
	/**
	 * @return the $st_cnpj
	 */
	public function getSt_cnpj() {
		return $this->st_cnpj;
	}

	/**
	 * @return the $st_razaosocial
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}

	/**
	 * @param field_type $st_cnpj
	 */
	public function setSt_cnpj($st_cnpj) {
		$this->st_cnpj = $st_cnpj;
	}

	/**
	 * @param field_type $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}

	/**
	 * @return the $bl_instituicao
	 */
	public function getBl_instituicao() {
		return $this->bl_instituicao;
	}

	/**
	 * @return the $bl_polo
	 */
	public function getBl_polo() {
		return $this->bl_polo;
	}

	/**
	 * @return the $bl_grupo
	 */
	public function getBl_grupo() {
		return $this->bl_grupo;
	}

	/**
	 * @return the $bl_nucleo
	 */
	public function getBl_nucleo() {
		return $this->bl_nucleo;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param $bl_instituicao the $bl_instituicao to set
	 */
	public function setBl_instituicao($bl_instituicao) {
		$this->bl_instituicao = $bl_instituicao;
	}

	/**
	 * @param $bl_polo the $bl_polo to set
	 */
	public function setBl_polo($bl_polo) {
		$this->bl_polo = $bl_polo;
	}

	/**
	 * @param $bl_grupo the $bl_grupo to set
	 */
	public function setBl_grupo($bl_grupo) {
		$this->bl_grupo = $bl_grupo;
	}

	/**
	 * @param $bl_nucleo the $bl_nucleo to set
	 */
	public function setBl_nucleo($bl_nucleo) {
		$this->bl_nucleo = $bl_nucleo;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}


	
}


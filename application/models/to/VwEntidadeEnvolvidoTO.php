<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwEntidadeEnvolvidoTO extends Ead1_TO_Dinamico{

	public $id_envolvidoentidade;
	public $id_usuario;
	public $id_tipoenvolvido;
	public $id_entidade;
	public $bl_ativo;
	public $st_nomecompleto;
	public $st_tipoenvolvido;


	/**
	 * @return the $id_envolvidoentidade
	 */
	public function getId_envolvidoentidade(){ 
		return $this->id_envolvidoentidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $id_tipoenvolvido
	 */
	public function getId_tipoenvolvido(){ 
		return $this->id_tipoenvolvido;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto(){ 
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_tipoenvolvido
	 */
	public function getSt_tipoenvolvido(){ 
		return $this->st_tipoenvolvido;
	}


	/**
	 * @param field_type $id_envolvidoentidade
	 */
	public function setId_envolvidoentidade($id_envolvidoentidade){ 
		$this->id_envolvidoentidade = $id_envolvidoentidade;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_tipoenvolvido
	 */
	public function setId_tipoenvolvido($id_tipoenvolvido){ 
		$this->id_tipoenvolvido = $id_tipoenvolvido;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto){ 
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_tipoenvolvido
	 */
	public function setSt_tipoenvolvido($st_tipoenvolvido){ 
		$this->st_tipoenvolvido = $st_tipoenvolvido;
	}

}
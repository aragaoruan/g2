<?php
/**
 * Classe para encapsular os dados de Lancamento da venda.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class LancamentoVendaTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id da venda
	 * @var int
	 */
	public $id_venda;

	/**
	 * Contem o id do lancamento
	 * @var int
	 */
	public $id_lancamento;

	/**
	 * Contem o valor se tem entrada
	 * @var boolean
	 */
	public $bl_entrada;

    public $nu_ordem;
    /**
     * Contem o id da vendaaditivo
     * @var int
     */
    public $id_vendaaditivo;

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $bl_entrada
	 */
	public function getBl_entrada() {
		return $this->bl_entrada;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @param int $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @param int $id_lancamento
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param boolean $bl_entrada
	 */
	public function setBl_entrada($bl_entrada) {
		$this->bl_entrada = $bl_entrada;
	}

	/**
	 * @param boolean $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

    /**
     * @return int
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }


}

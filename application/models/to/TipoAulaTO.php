<?php
/**
 * Classe para encapsular os dados de tipo de sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de aula.
	 * @var int
	 */
	public $id_tipoaula;
	
	/**
	 * Tipo de aula.
	 * @var string
	 */
	public $st_tipoaula;
	
	/**
	 * @return int
	 */
	public function getId_tipoaula() {
		return $this->id_tipoaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoaula() {
		return $this->st_tipoaula;
	}
	
	/**
	 * @param int $id_tipoaula
	 */
	public function setId_tipoaula($id_tipoaula) {
		$this->id_tipoaula = $id_tipoaula;
	}
	
	/**
	 * @param string $st_tipoaula
	 */
	public function setSt_tipoaula($st_tipoaula) {
		$this->st_tipoaula = $st_tipoaula;
	}

}

?>
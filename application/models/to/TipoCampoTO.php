<?php
/**
 * Classe para encapsular os dados da tabela de tipos de campos do relatórios
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class TipoCampoTO extends Ead1_TO_Dinamico{
	
	public $id_tipocampo;
	public $st_tipocampo;
	
	/**
	 * @return the $id_tipocampo
	 */
	public function getId_tipocampo() {
		return $this->id_tipocampo;
	}

	/**
	 * @return the $st_tipocampo
	 */
	public function getSt_tipocampo() {
		return $this->st_tipocampo;
	}

	/**
	 * @param $id_tipocampo the $id_tipocampo to set
	 */
	public function setId_tipocampo($id_tipocampo) {
		$this->id_tipocampo = $id_tipocampo;
	}

	/**
	 * @param $st_tipocampo the $st_tipocampo to set
	 */
	public function setSt_tipocampo($st_tipocampo) {
		$this->st_tipocampo = $st_tipocampo;
	}

}
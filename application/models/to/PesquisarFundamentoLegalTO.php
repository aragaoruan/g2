<?php
/**
 * Classe para encapsular os dados de pesquisa de Fundamento Legal
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class PesquisarFundamentoLegalTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o Numero.
	 * @var int
	 */
	public $nu_numero;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Id do tipo de Fundamento legal Portaria.
	 * @var int
	 */
	public $id_portaria;
		
	/**
	 * Id do tipo de Fundamento legal Lei.
	 * @var int
	 */
	public $id_lei;
		
	/**
	 * Id do tipo de Fundamento legal Resolução.
	 * @var int
	 */
	public $id_resolucao;
		
	/**
	 * Id do tipo de Fundamento legal Parecer.
	 * @var int
	 */
	public $id_parecer;
			
	/**
	 * Id da Entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return the $st_numero
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_portaria
	 */
	public function getId_portaria() {
		return $this->id_portaria;
	}

	/**
	 * @return the $id_lei
	 */
	public function getId_lei() {
		return $this->id_lei;
	}

	/**
	 * @return the $id_resolucao
	 */
	public function getId_resolucao() {
		return $this->id_resolucao;
	}

	/**
	 * @return the $id_parecer
	 */
	public function getId_parecer() {
		return $this->id_parecer;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $st_numero the $st_numero to set
	 */
	public function setNu_numero($nu_numero) {
		$this->nu_numero = $nu_numero;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_portaria the $id_portaria to set
	 */
	public function setId_portaria($id_portaria) {
		$this->id_portaria = $id_portaria;
	}

	/**
	 * @param $id_lei the $id_lei to set
	 */
	public function setId_lei($id_lei) {
		$this->id_lei = $id_lei;
	}

	/**
	 * @param $id_resolucao the $id_resolucao to set
	 */
	public function setId_resolucao($id_resolucao) {
		$this->id_resolucao = $id_resolucao;
	}

	/**
	 * @param $id_parecer the $id_parecer to set
	 */
	public function setId_parecer($id_parecer) {
		$this->id_parecer = $id_parecer;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}
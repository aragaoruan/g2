<?php

/**
 * Classe para encapsular da tabela  - Não utilizada por mim (ELcio) ainda.
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class VwUsuarioPerfilPedagogicoTO extends Ead1_TO_Dinamico
{

    public $st_nomecompleto;
    public $id_usuario;
    public $st_login;
    public $st_senha;
    public $st_email;
    public $st_nomeexibicao;
    public $st_urlavatar;
    public $dt_nascimento;
    public $id_perfil;
    public $id_entidade;
    public $st_nomeperfil;
    public $st_perfilpedagogico;
    public $id_perfilpedagogico;
    public $dt_inicio;
    public $dt_termino;
    public $id_projetopedagogico;
    public $id_trilha;
    public $st_tituloexibicao;
    public $id_saladeaula;
    public $st_saladeaula;
    public $id_matricula;
    public $st_nomeentidade;
    public $st_razaosocial;
    public $bl_bloqueiadisciplina;
    public $st_urlacesso;
    public $st_senhaentidade;
    public $id_usuariomoodle;
    public $id_sistema;
    public $st_codsistema;
    public $id_entidadeportal;
    public $bl_atendimentovirtual;
    public $st_chaveacesso;
    public $st_conexao;

    /**
     * @return mixed
     */
    public function getSt_chaveacesso()
    {
        return $this->st_chaveacesso;
    }

    /**
     * @param mixed $st_chaveacesso
     */
    public function setSt_chaveacesso($st_chaveacesso)
    {
        $this->st_chaveacesso = $st_chaveacesso;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_conexao()
    {
        return $this->st_conexao;
    }

    /**
     * @param mixed $st_conexao
     */
    public function setSt_conexao($st_conexao)
    {
        $this->st_conexao = $st_conexao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBl_atendimentovirtual()
    {
        return $this->bl_atendimentovirtual;
    }

    /**
     * @param mixed $bl_atendimentovirtual
     */
    public function setBl_atendimentovirtual($bl_atendimentovirtual)
    {
        $this->bl_atendimentovirtual = $bl_atendimentovirtual;
        return $this;
    }

    /**
     * @return $st_urlacesso
     */
    public function getSt_urlacesso()
    {
        return $this->st_urlacesso;
    }

    /**
     * @param $st_urlacesso
     */
    public function setSt_urlacesso($st_urlacesso)
    {
        $this->st_urlacesso = $st_urlacesso;
    }

    /**
     * @return $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return $st_login
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @return $st_senha
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @return $st_email
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @return  $st_nomeexibicao
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @return  $st_urlavatar
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @return  $dt_nascimento
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @return  $id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @return $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return $st_nomeperfil
     */
    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @return $st_perfilpedagogico
     */
    public function getSt_perfilpedagogico()
    {
        return $this->st_perfilpedagogico;
    }

    /**
     * @return $id_perfilpedagogico
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @return $dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @return $dt_termino
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @return $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return $id_trilha
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @return $st_tituloexibicao
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @return $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @return $st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @return $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return $st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return $st_razaosocial
     */
    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    /**
     * @return $bl_bloqueiadisciplina
     */
    public function getBl_bloqueiadisciplina()
    {
        return $this->bl_bloqueiadisciplina;
    }

    /**
     * @return string
     */
    public function getSt_senhaentidade()
    {
        return $this->st_senhaentidade;
    }


    /**
     * @param $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param $st_login
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
    }

    /**
     * @param $st_senha
     */
    public function setSt_senha($st_senha)
    {

        if (!empty($st_senha)) {
            if (strlen($st_senha) < 32) {
                $st_senha = md5($st_senha);
            }
        }

        $this->st_senha = $st_senha;
    }

    /**
     * @param $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @param $st_nomeexibicao
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
    }

    /**
     * @param $st_urlavatar
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
    }

    /**
     * @param $dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @param $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @param $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param $st_nomeperfil
     */
    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    /**
     * @param $st_perfilpedagogico
     */
    public function setSt_perfilpedagogico($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
    }

    /**
     * @param $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @param $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @param $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @param $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param $id_trilha
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
    }

    /**
     * @param $st_tituloexibicao
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    /**
     * @param $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @param $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @param $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param $st_razaosocial
     */
    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
    }

    /**
     * @param $bl_bloqueiadisciplina
     */
    public function setBl_bloqueiadisciplina($bl_bloqueiadisciplina)
    {
        $this->bl_bloqueiadisciplina = $bl_bloqueiadisciplina;
    }

    /**
     * @param string $st_senhaentidade
     */
    public function setSt_senhaentidade($st_senhaentidade)
    {
        $this->st_senhaentidade = $st_senhaentidade;
    }

    /**
     * @param $id_usuariomoodle
     */
    public function setId_usuariomoodle($id_usuariomoodle)
    {
        $this->id_usuariomoodle = $id_usuariomoodle;
    }

    /**
     * @param $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @param $id_usuariomoodle
     */
    public function getId_usuariomoodle($id_usuariomoodle)
    {
        $this->id_usuariomoodle = $id_usuariomoodle;
    }

    /**
 * @param string $id_sistema
 */
    public function getId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @param string $st_codsistema
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @param string $id_entidadeportal
     */
    public function getId_entidadeportal()
    {
        return $this->id_entidadeportal;
    }

    /**
     * @param string $id_entidadeportal
     */
    public function setId_entidadeportal($id_entidadeportal)
    {
        $this->id_entidadeportal = $id_entidadeportal;
    }

}

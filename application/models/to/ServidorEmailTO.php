<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class ServidorEmailTO extends Ead1_TO_Dinamico{

	public $id_servidoremail;
	public $st_servidoremail;


	/**
	 * @return the $id_servidoremail
	 */
	public function getId_servidoremail(){ 
		return $this->id_servidoremail;
	}

	/**
	 * @return the $st_servidoremail
	 */
	public function getSt_servidoremail(){ 
		return $this->st_servidoremail;
	}


	/**
	 * @param field_type $id_servidoremail
	 */
	public function setId_servidoremail($id_servidoremail){ 
		$this->id_servidoremail = $id_servidoremail;
	}

	/**
	 * @param field_type $st_servidoremail
	 */
	public function setSt_servidoremail($st_servidoremail){ 
		$this->st_servidoremail = $st_servidoremail;
	}

}
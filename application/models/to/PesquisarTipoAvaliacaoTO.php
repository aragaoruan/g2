<?php
/**
 * Classe para encapsular os dados tipoAvaliacao.
 * @author Felipe Beserra de Araujo - felipebusiness@globo.com
 * @since 16/06/2011 às 16:50
 * 
 * @package models
 * @subpackage to
 */

class PesquisarTipoAvaliacaoTO extends Ead1_TO_Dinamico {
	
	public $id_tipoavaliacao;
	public $st_tipoavaliacao;
	public $st_descricao;
	
	/**
	 * @return the $id_tipoavaliacao
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @return the $st_tipoavaliacao
	 */
	public function getSt_tipoavaliacao() {
		return $this->st_tipoavaliacao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param $id_tipoavaliacao the $id_tipoavaliacao to set
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param $st_tipoavaliacao the $st_tipoavaliacao to set
	 */
	public function setSt_tipoavaliacao($st_tipoavaliacao) {
		$this->st_tipoavaliacao = $st_tipoavaliacao;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	
	

}

?>
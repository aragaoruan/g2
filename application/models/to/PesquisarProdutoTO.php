<?php
/**
 * Classe para encapsular os dados produto.
 * @author Felipe Beserra de Araujo - felipebusiness@globo.com
 * @since 09/06/2011 às 15:43
 * 
 * @package models
 * @subpackage to
 */

class PesquisarProdutoTO extends Ead1_TO_Dinamico {
	
	public $id_produto;
	public $st_produto;
	public $id_tipoproduto;
	public $st_tipoproduto;
	public $id_entidade;
	public $bl_ativo;
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.CadastrarProduto';
	
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @param $id_produto the $id_produto to set
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param $st_produto the $st_produto to set
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @param $id_tipoproduto the $id_tipoproduto to set
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_classeflex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @param field_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param String $st_classeflex
	 */
	public function setSt_classeflex($st_classeflex) {
		$this->st_classeflex = $st_classeflex;
	}



	
	

}

?>
<?php
/**
 * Classe para encapsular os dados de view de VwSalaDisciplinaTurma.
 * 
 * @package models
 * @subpackage to
 */
class VwSalaDisciplinaTurmaTO extends Ead1_TO_Dinamico{
	
	public $id_saladeaula;
	public $st_saladeaula;
	public $nu_maxalunos;
	public $nu_alocados;
	public $id_projetopedagogico;
	public $id_trilha;
	public $id_matricula;
	public $id_disciplina;
	public $id_matriculadisciplina;
	public $id_alocacao;
	
	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	/**
	 * @return the $id_matriculadisciplina
	 */
	public function getId_matriculadisciplina() {
		return $this->id_matriculadisciplina;
	}

	/**
	 * @param $id_matriculadisciplina the $id_matriculadisciplina to set
	 */
	public function setId_matriculadisciplina($id_matriculadisciplina) {
		$this->id_matriculadisciplina = $id_matriculadisciplina;
	}
	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @param $id_alocacao the $id_alocacao to set
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}
	/**
	 * @return the $nu_maxalunos
	 */
	public function getNu_maxalunos() {
		return $this->nu_maxalunos;
	}

	/**
	 * @return the $nu_alocados
	 */
	public function getNu_alocados() {
		return $this->nu_alocados;
	}

	/**
	 * @param field_type $nu_maxalunos
	 */
	public function setNu_maxalunos($nu_maxalunos) {
		$this->nu_maxalunos = $nu_maxalunos;
	}

	/**
	 * @param field_type $nu_alocados
	 */
	public function setNu_alocados($nu_alocados) {
		$this->nu_alocados = $nu_alocados;
	}
	/**
	 * @return the $id_trilha
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}

	/**
	 * @param field_type $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}





	
}
<?php
/**
 * Classe para encapsular dados da tabela de categoria dos textos 
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class TextoCategoriaTO extends Ead1_TO_Dinamico{
	
	const DADOS_PESSOAIS = 5;
	const CLASSE_AFILIADO = 9;
	const ENTIDADELOJA = 11;
	
	public $id_textocategoria;
	public $st_textocategoria;
	public $st_descricao;
	public $st_metodo;
	
	/**
	 * @return the $id_textocategoria
	 */
	public function getId_textocategoria() {
		return $this->id_textocategoria;
	}

	/**
	 * @return the $st_textocategoria
	 */
	public function getSt_textocategoria() {
		return $this->st_textocategoria;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $st_metodo
	 */
	public function getSt_metodo() {
		return $this->st_metodo;
	}

	/**
	 * @param field_type $id_textocategoria
	 */
	public function setId_textocategoria($id_textocategoria) {
		$this->id_textocategoria = $id_textocategoria;
	}

	/**
	 * @param field_type $st_textocategoria
	 */
	public function setSt_textocategoria($st_textocategoria) {
		$this->st_textocategoria = $st_textocategoria;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $st_metodo
	 */
	public function setSt_metodo($st_metodo) {
		$this->st_metodo = $st_metodo;
	}

}
<?php
/**
 * Classe que encapsula os dados da tb_categoriaentidade
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class CategoriaEntidadeTO extends Ead1_TO_Dinamico {

	public $id_categoria;
	public $id_entidade;
	public $id_usuariocadastro;
	public $dt_cadastro;
	
	/**
	 * @return the $id_categoria
	 */
	public function getId_categoria() {
		return $this->id_categoria;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_categoria
	 */
	public function setId_categoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
	
}

?>
<?php

/**
 * Created by PhpStorm.
 * User: Elcio Guimarães
 * Date: 09/10/2017
 * Time: 14:38
 */
class VwCampanhaComercial extends Ead1_TO_Dinamico
{

    /**
     * @var integer $id_campanhacomercial
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     */
    private $st_campanhacomercial;

    /**
     *
     * @var boolean $bl_aplicardesconto
     */
    private $bl_aplicardesconto;

    /**
     *
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_disponibilidade
     */
    private $bl_disponibilidade;

    /**
     * @var date $dt_cadastro
     */
    private $dt_cadastro;

    /**
     * @var date $dt_fim
     */
    private $dt_fim;

    /**
     * @var date $dt_inicio
     */
    private $dt_inicio;

    /**
     * @var integer $id_entidade
     */
    private $id_entidade;

    /**
     * @var string $st_nomeentidade
     */
    private $st_nomeentidade;

    /**
     * @var integer $id_situacao
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     */
    private $st_situacao;

    /**
     * @var integer $id_tipodesconto
     */
    private $id_tipodesconto;

    /**
     * @var string $st_tipodesconto
     */
    private $st_tipodesconto;

    /**
     * @var integer $id_usuariocadastro
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_tipocampanha
     */
    private $id_tipocampanha;

    /**
     * @var string $st_tipocampanha
     */
    private $st_tipocampanha;

    /**
     * @var integer $nu_disponibilidade
     */
    private $nu_disponibilidade;

    /**
     * @var string $st_descricao
     */
    private $st_descricao;

    /**
     *
     * @var boolean $bl_todosprodutos
     */
    private $bl_todosprodutos;

    /**
     *
     * @var boolean $bl_todasformas
     */
    private $bl_todasformas;

    /**
     * @var integer $id_categoriacampanha
     */
    private $id_categoriacampanha;

    /**
     * @var string $st_categoriacampanha
     */
    private $st_categoriacampanha;

    /**
     * @var integer $nu_valordesconto
     */
    private $nu_valordesconto;

    /**
     * @var integer $nu_diasprazo
     */
    private $nu_diasprazo;

    /**
     * @var integer $id_premio
     */
    private $id_premio;

    /**
     * @var string $st_prefixocupompremio
     */
    private $st_prefixocupompremio;

    /**
     * @var integer $id_finalidadecampanha
     */
    private $id_finalidadecampanha;

    /**
     * @var string $st_finalidadecampanha
     */
    private $st_finalidadecampanha;

    /**
     * @var string $st_link
     */
    private $st_link;

    /**
     * @var string $st_imagem
     */
    private $st_imagem;

    /**
     *
     * @var boolean $bl_portaldoaluno
     */
    private $bl_portaldoaluno;

    /**
     *
     * @var boolean $bl_mobile
     */
    private $bl_mobile;

    /**
     *
     * @var boolean $bl_vigente
     */
    private $bl_vigente;

    /**
     * @return int
     */
    public function getid_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setid_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @param string $st_campanhacomercial
     */
    public function setst_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_aplicardesconto()
    {
        return $this->bl_aplicardesconto;
    }

    /**
     * @param mixed $bl_aplicardesconto
     */
    public function setbl_aplicardesconto($bl_aplicardesconto)
    {
        $this->bl_aplicardesconto = $bl_aplicardesconto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_disponibilidade()
    {
        return $this->bl_disponibilidade;
    }

    /**
     * @param bool $bl_disponibilidade
     */
    public function setbl_disponibilidade($bl_disponibilidade)
    {
        $this->bl_disponibilidade = $bl_disponibilidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param mixed $dt_fim
     */
    public function setdt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;

    }

    /**
     * @return date
     */
    public function getdt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param date $dt_inicio
     */
    public function setdt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipodesconto()
    {
        return $this->id_tipodesconto;
    }

    /**
     * @param int $id_tipodesconto
     */
    public function setid_tipodesconto($id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipodesconto()
    {
        return $this->st_tipodesconto;
    }

    /**
     * @param string $st_tipodesconto
     */
    public function setst_tipodesconto($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param int $id_tipocampanha
     */
    public function setid_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipocampanha()
    {
        return $this->st_tipocampanha;
    }

    /**
     * @param string $st_tipocampanha
     */
    public function setst_tipocampanha($st_tipocampanha)
    {
        $this->st_tipocampanha = $st_tipocampanha;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_disponibilidade()
    {
        return $this->nu_disponibilidade;
    }

    /**
     * @param int $nu_disponibilidade
     */
    public function setnu_disponibilidade($nu_disponibilidade)
    {
        $this->nu_disponibilidade = $nu_disponibilidade;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     */
    public function setst_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_todosprodutos()
    {
        return $this->bl_todosprodutos;
    }

    /**
     * @param mixed $bl_todosprodutos
     */
    public function setbl_todosprodutos($bl_todosprodutos)
    {
        $this->bl_todosprodutos = $bl_todosprodutos;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @param mixed $bl_todasformas
     */
    public function setbl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_categoriacampanha()
    {
        return $this->id_categoriacampanha;
    }

    /**
     * @param mixed $id_categoriacampanha
     */
    public function setid_categoriacampanha($id_categoriacampanha)
    {
        $this->id_categoriacampanha = $id_categoriacampanha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_categoriacampanha()
    {
        return $this->st_categoriacampanha;
    }

    /**
     * @param mixed $st_categoriacampanha
     */
    public function setst_categoriacampanha($st_categoriacampanha)
    {
        $this->st_categoriacampanha = $st_categoriacampanha;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_valordesconto()
    {
        return $this->nu_valordesconto;
    }

    /**
     * @param int $nu_valordesconto
     */
    public function setnu_valordesconto($nu_valordesconto)
    {
        $this->nu_valordesconto = $nu_valordesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_diasprazo()
    {
        return $this->nu_diasprazo;
    }

    /**
     * @param int $nu_diasprazo
     */
    public function setnu_diasprazo($nu_diasprazo)
    {
        $this->nu_diasprazo = $nu_diasprazo;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_premio()
    {
        return $this->id_premio;
    }

    /**
     * @param int $id_premio
     */
    public function setid_premio($id_premio)
    {
        $this->id_premio = $id_premio;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_prefixocupompremio()
    {
        return $this->st_prefixocupompremio;
    }

    /**
     * @param string $st_prefixocupompremio
     */
    public function setst_prefixocupompremio($st_prefixocupompremio)
    {
        $this->st_prefixocupompremio = $st_prefixocupompremio;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_finalidadecampanha()
    {
        return $this->id_finalidadecampanha;
    }

    /**
     * @param int $id_finalidadecampanha
     */
    public function setid_finalidadecampanha($id_finalidadecampanha)
    {
        $this->id_finalidadecampanha = $id_finalidadecampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_finalidadecampanha()
    {
        return $this->st_finalidadecampanha;
    }

    /**
     * @param string $st_finalidadecampanha
     */
    public function setst_finalidadecampanha($st_finalidadecampanha)
    {
        $this->st_finalidadecampanha = $st_finalidadecampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_link()
    {
        return $this->st_link;
    }

    /**
     * @param string $st_link
     */
    public function setst_link($st_link)
    {
        $this->st_link = $st_link;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param string $st_imagem
     */
    public function setst_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_portaldoaluno()
    {
        return $this->bl_portaldoaluno;
    }

    /**
     * @param mixed $bl_portaldoaluno
     */
    public function setbl_portaldoaluno($bl_portaldoaluno)
    {
        $this->bl_portaldoaluno = $bl_portaldoaluno;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_mobile()
    {
        return $this->bl_mobile;
    }

    /**
     * @param mixed $bl_mobile
     */
    public function setbl_mobile($bl_mobile)
    {
        $this->bl_mobile = $bl_mobile;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_vigente()
    {
        return $this->bl_vigente;
    }

    /**
     * @param bool $bl_vigente
     */
    public function setbl_vigente($bl_vigente)
    {
        $this->bl_vigente = $bl_vigente;
        return $this;

    }


}

<?php
/**
 * Classe para encapsular os dados de Campanha Produto.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaProdutoTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contem o id da campanha comercial
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * Contem o id do produto
	 * @var int
	 */
	public $id_produto;
	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @param int $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	
}
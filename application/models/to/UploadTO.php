<?php
/**
 * Classe para encapsular os dados de upload.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
 class UploadTO extends Ead1_TO_Dinamico {
		
 		public $id_upload;
		public $st_upload;
		
		
		
		/**
	 * @return the $id_upload
	 */
	public function getId_upload() {
		return $this->id_upload;
	}

		/**
	 * @param field_type $id_upload
	 */
	public function setId_upload($id_upload) {
		$this->id_upload = $id_upload;
	}

		/**
	 * @return the $st_upload
	 */
	public function getSt_upload() {
		return $this->st_upload;
	}

		/**
	 * @param field_type $st_upload
	 */
	public function setSt_upload($st_upload) {
		$this->st_upload = $st_upload;
	}


		
	

		
		
}

?>
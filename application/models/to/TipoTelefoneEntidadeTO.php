<?php
/**
 * Classe para vincula o tipo de telefone com entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoTelefoneEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do tipo de telefone.
	 * @var int 
	 */
	public $id_tipotelefone;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipotelefone() {
		return $this->id_tipotelefone;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_tipotelefone
	 */
	public function setId_tipotelefone($id_tipotelefone) {
		$this->id_tipotelefone = $id_tipotelefone;
	}

}

?>
<?php
/** 
 * Classe para encapsular os dados da mensagem motivacional.
 * @author Domício de Araújo Medeiros - domicio.medeiros@gmail.com
 * 
 * @pakage models
 * @subpackage to
 */

class MensagemMotivacionalTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contém o id da mensagem motivacional .
	 * @var int
	 */
	public $id_mensagemmotivacional;
	
	/**
	 * Contém o id do usuário responsável pela alteração.
	 * @var int
	 */
	
	public $id_usuariocadastrado;
	
	/**
	 * Contém a mensagem motivacional.
	 * @var String
	 */
	public $st_mensagemmotivacional;
	
	/**
	 * Contém a data do cadastro.
	 * @var Date
	 */
	public $dt_cadastro;
	
	/**
	 * @return the $id_mensagemmotivacional
	 */
	public function getId_mensagemmotivacional() {
		return $this->id_mensagemmotivacional;
	}

	/**
	 * @return the $id_usuariocadastrado
	 */
	public function getId_usuariocadastrado() {
		return $this->id_usuariocadastrado;
	}

	/**
	 * @return the $st_mensagemmotivacional
	 */
	public function getSt_mensagemmotivacional() {
		return $this->st_mensagemmotivacional;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $id_mensagemmotivacional
	 */
	public function setId_mensagemmotivacional($id_mensagemmotivacional) {
		$this->id_mensagemmotivacional = $id_mensagemmotivacional;
	}

	/**
	 * @param field_type $id_usuariocadastrado
	 */
	public function setId_usuariocadastrado($id_usuariocadastrado) {
		$this->id_usuariocadastrado = $id_usuariocadastrado;
	}

	/**
	 * @param field_type $st_mensagemmotivacional
	 */
	public function setSt_mensagemmotivacional($st_mensagemmotivacional) {
		$this->st_mensagemmotivacional = $st_mensagemmotivacional;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

}
?>
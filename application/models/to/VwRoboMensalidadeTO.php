<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwRoboMensalidadeTO extends Ead1_TO_Dinamico{

	public $id_matricula;
	public $id_contrato;
	public $id_venda;
	public $nu_valormensal;
	public $nu_diamensalidade;
	public $st_nomeentidade;
	public $nu_mesesmensalidade;
	public $dt_inicio;
	public $id_usuario;
	public $nu_porcentagem;
	public $id_entidade;
	public $id_contratoregra;
	public $nu_bolsa;
	public $id_usuariocadastro;


	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_contrato
	 */
	public function getId_contrato(){ 
		return $this->id_contrato;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda(){ 
		return $this->id_venda;
	}

	/**
	 * @return the $nu_valormensal
	 */
	public function getNu_valormensal(){ 
		return $this->nu_valormensal;
	}

	/**
	 * @return the $nu_diamensalidade
	 */
	public function getNu_diamensalidade(){ 
		return $this->nu_diamensalidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade(){ 
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $nu_mesesmensalidade
	 */
	public function getNu_mesesmensalidade(){ 
		return $this->nu_mesesmensalidade;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio(){ 
		return $this->dt_inicio;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario(){ 
		return $this->id_usuario;
	}

	/**
	 * @return the $nu_porcentagem
	 */
	public function getNu_porcentagem(){ 
		return $this->nu_porcentagem;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $id_contratoregra
	 */
	public function getId_contratoregra(){ 
		return $this->id_contratoregra;
	}

	/**
	 * @return the $nu_bolsa
	 */
	public function getNu_bolsa(){ 
		return $this->nu_bolsa;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}


	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_contrato
	 */
	public function setId_contrato($id_contrato){ 
		$this->id_contrato = $id_contrato;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda){ 
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal){ 
		$this->nu_valormensal = $nu_valormensal;
	}

	/**
	 * @param field_type $nu_diamensalidade
	 */
	public function setNu_diamensalidade($nu_diamensalidade){ 
		$this->nu_diamensalidade = $nu_diamensalidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade){ 
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $nu_mesesmensalidade
	 */
	public function setNu_mesesmensalidade($nu_mesesmensalidade){ 
		$this->nu_mesesmensalidade = $nu_mesesmensalidade;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio){ 
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario){ 
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $nu_porcentagem
	 */
	public function setNu_porcentagem($nu_porcentagem){ 
		$this->nu_porcentagem = $nu_porcentagem;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_contratoregra
	 */
	public function setId_contratoregra($id_contratoregra){ 
		$this->id_contratoregra = $id_contratoregra;
	}

	/**
	 * @param field_type $nu_bolsa
	 */
	public function setNu_bolsa($nu_bolsa){ 
		$this->nu_bolsa = $nu_bolsa;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

}
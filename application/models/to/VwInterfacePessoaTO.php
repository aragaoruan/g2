<?php
/**
 * Classe para encapsular os dados da view de interface de pessoa
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class VwInterfacePessoaTO extends Ead1_TO_Dinamico{
	
	public $id_interfacepessoa;
	public $id_usuario;
	public $id_entidade;
	public $id_tipointerfacepessoa;
	public $st_color;
	
	/**
	 * @return the $id_interfacepessoa
	 */
	public function getId_interfacepessoa() {
		return $this->id_interfacepessoa;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_tipointerfacepessoa
	 */
	public function getId_tipointerfacepessoa() {
		return $this->id_tipointerfacepessoa;
	}

	/**
	 * @return the $st_color
	 */
	public function getSt_color() {
		return $this->st_color;
	}

	/**
	 * @param $id_interfacepessoa the $id_interfacepessoa to set
	 */
	public function setId_interfacepessoa($id_interfacepessoa) {
		$this->id_interfacepessoa = $id_interfacepessoa;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_tipointerfacepessoa the $id_tipointerfacepessoa to set
	 */
	public function setId_tipointerfacepessoa($id_tipointerfacepessoa) {
		$this->id_tipointerfacepessoa = $id_tipointerfacepessoa;
	}

	/**
	 * @param $st_color the $st_color to set
	 */
	public function setSt_color($st_color) {
		$this->st_color = $st_color;
	}
}
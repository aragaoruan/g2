<?php
/**
 * Classe TO da View vw_modulodisciplinaprerequisito
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwModuloDisciplinaPreRequisitoTO extends Ead1_TO_Dinamico{
	
	public $id_projetopedagogico;
	public $id_tipotrilha;
	public $st_tipotrilha;
	public $id_modulo;
	public $st_modulotituloexibicao;
	public $id_disciplina;
	public $id_disciplinaprerequisito;
	public $st_disciplinatituloexibicao;
	public $id_serie;
	public $st_serie;
	public $id_nivelensino;
	public $st_nivelensino;
	
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_modulotituloexibicao
	 */
	public function getSt_modulotituloexibicao() {
		return $this->st_modulotituloexibicao;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_disciplinaprerequisito
	 */
	public function getId_disciplinaprerequisito() {
		return $this->id_disciplinaprerequisito;
	}

	/**
	 * @return the $st_disciplinatituloexibicao
	 */
	public function getSt_disciplinatituloexibicao() {
		return $this->st_disciplinatituloexibicao;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @param $id_projetopedagogico the $id_projetopedagogico to set
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param $id_modulo the $id_modulo to set
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param $st_modulotituloexibicao the $st_modulotituloexibicao to set
	 */
	public function setSt_modulotituloexibicao($st_modulotituloexibicao) {
		$this->st_modulotituloexibicao = $st_modulotituloexibicao;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_disciplinaprerequisito the $id_disciplinaprerequisito to set
	 */
	public function setId_disciplinaprerequisito($id_disciplinaprerequisito) {
		$this->id_disciplinaprerequisito = $id_disciplinaprerequisito;
	}

	/**
	 * @param $st_disciplinatituloexibicao the $st_disciplinatituloexibicao to set
	 */
	public function setSt_disciplinatituloexibicao($st_disciplinatituloexibicao) {
		$this->st_disciplinatituloexibicao = $st_disciplinatituloexibicao;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	/**
	 * @return the $id_tipotrilha
	 */
	public function getId_tipotrilha() {
		return $this->id_tipotrilha;
	}

	/**
	 * @return the $st_tipotrilha
	 */
	public function getSt_tipotrilha() {
		return $this->st_tipotrilha;
	}

	/**
	 * @param $id_tipotrilha the $id_tipotrilha to set
	 */
	public function setId_tipotrilha($id_tipotrilha) {
		$this->id_tipotrilha = $id_tipotrilha;
	}

	/**
	 * @param $st_tipotrilha the $st_tipotrilha to set
	 */
	public function setSt_tipotrilha($st_tipotrilha) {
		$this->st_tipotrilha = $st_tipotrilha;
	}


	
	
	
}
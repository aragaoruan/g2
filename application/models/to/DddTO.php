<?php
/**
 * Classe para encapsular os dados de DDD.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DddTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o número de ddd.
	 * @var int
	 */
	public $nu_ddd;
	
	/**
	 * @return int
	 */
	public function getNu_ddd() {
		return $this->nu_ddd;
	}
	
	/**
	 * @param int $nu_ddd
	 */
	public function setNu_ddd($nu_ddd) {
		$this->nu_ddd = $nu_ddd;
	}

}

?>
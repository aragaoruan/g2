<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class MatriculaNivelSerieTO extends Ead1_TO_Dinamico{

	public $id_matricula;
	public $id_projetopedagogico;
	public $id_serie;
	public $id_nivelensino;


	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie(){ 
		return $this->id_serie;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino(){ 
		return $this->id_nivelensino;
	}


	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie){ 
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino){ 
		$this->id_nivelensino = $id_nivelensino;
	}

}
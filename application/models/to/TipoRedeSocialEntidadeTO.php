<?php
/**
 * Classe para vincular o tipo de rede social com entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoRedeSocialEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id do tipo de rede social.
	 * @var int
	 */
	public $id_tiporedesocial;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiporedesocial() {
		return $this->id_tiporedesocial;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_tiporedesocial
	 */
	public function setId_tiporedesocial($id_tiporedesocial) {
		$this->id_tiporedesocial = $id_tiporedesocial;
	}

}

?>
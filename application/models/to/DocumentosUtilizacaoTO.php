<?php
/**
 * Classe para encapsular os dados de utilização de documento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentosUtilizacaoTO extends Ead1_TO_Dinamico {
	
	/**
	 * Id da utilização do documento.
	 * @var int
	 */
	public $id_documentosutilizacao;
	
	/**
	 * Utilização do documento.
	 * @var String
	 */
	public $st_documentosutilizacao;
	
	/**
	 * 
	 * @var bool
	 */
	public $bl_ativo;
	
	
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return int
	 */
	public function getId_documentosutilizacao() {
		return $this->id_documentosutilizacao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_documentosutilizacao() {
		return $this->st_documentosutilizacao;
	}
	
	/**
	 * @param int $id_documentosutilizacao
	 */
	public function setId_documentosutilizacao($id_documentosutilizacao) {
		$this->id_documentosutilizacao = $id_documentosutilizacao;
	}
	
	/**
	 * @param String $st_documentosutilizacao
	 */
	public function setSt_documentosutilizacao($st_documentosutilizacao) {
		$this->st_documentosutilizacao = $st_documentosutilizacao;
	}

}

?>
<?php
/**
 * Classe para encapsular dados da vw_produtovenda
 * @author edermariano
 * @package models
 * @subpackage to
 */
class VwProdutoVendaTO extends Ead1_TO_Dinamico
{
    public $id_produto;
    public $st_produto;
    public $id_tipoproduto;
    public $st_tipoproduto;
    public $id_venda;
    public $nu_valorbruto;
    public $nu_valorliquido;
    public $nu_descontoporcentagem;
    public $nu_descontovalor;
    public $id_campanhacomercial;
    public $id_campanhacomercialpremio;
    public $st_campanhacomercial;
    public $id_vendaproduto;
    public $id_tipoprodutovalor;
    public $st_tipoprodutovalor;
    public $bl_ativo;
    public $st_produtocombo;
    public $id_avaliacaoaplicacao;
    public $id_usuario;
    public $id_avaliacao;
    public $id_entidade;
    public $dt_cadastro;
    public $id_modelovenda;
    public $st_modelovenda;
    public $id_matricula;
    
    
    
    /**
	 * @return the $id_modelovenda
	 */
	public function getId_modelovenda() {
		return $this->id_modelovenda;
	}

	/**
	 * @return the $st_modelovenda
	 */
	public function getSt_modelovenda() {
		return $this->st_modelovenda;
	}

	/**
	 * @param field_type $id_modelovenda
	 */
	public function setId_modelovenda($id_modelovenda) {
		$this->id_modelovenda = $id_modelovenda;
		return $this;
	}

	/**
	 * @param field_type $st_modelovenda
	 */
	public function setSt_modelovenda($st_modelovenda) {
		$this->st_modelovenda = $st_modelovenda;
		return $this;
	}

	public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

        
    public function setSt_produtocombo($st_produtocombo)
    {
        $this->st_produtocombo = $st_produtocombo;
    }

    public function getSt_produtocombo()
    {
        return $this->st_produtocombo;
    }
	
    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo() {
            return $this->bl_ativo;
    }

    /**
     * @return unknown
     */
    public function getId_tipoprodutovalor() {
            return $this->id_tipoprodutovalor;
    }

    /**
     * @return unknown
     */
    public function getSt_tipoprodutovalor() {
            return $this->st_tipoprodutovalor;
    }

    /**
     * @param unknown_type $id_tipoprodutovalor
     */
    public function setId_tipoprodutovalor($id_tipoprodutovalor) {
            $this->id_tipoprodutovalor = $id_tipoprodutovalor;
    }

    /**
     * @param unknown_type $st_tipoprodutovalor
     */
    public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
            $this->st_tipoprodutovalor = $st_tipoprodutovalor;
    }


    /**
     * @return the $id_produto
     */
    public function getId_produto() {
            return $this->id_produto;
    }

    /**
     * @return the $st_produto
     */
    public function getSt_produto() {
            return $this->st_produto;
    }

    /**
     * @return the $id_tipoproduto
     */
    public function getId_tipoproduto() {
            return $this->id_tipoproduto;
    }

    /**
     * @return the $st_tipoproduto
     */
    public function getSt_tipoproduto() {
            return $this->st_tipoproduto;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda() {
            return $this->id_venda;
    }

    /**
     * @return the $nu_valorbruto
     */
    public function getNu_valorbruto() {
            return $this->nu_valorbruto;
    }

    /**
     * @return the $nu_valorliquido
     */
    public function getNu_valorliquido() {
            return $this->nu_valorliquido;
    }

    /**
     * @return the $nu_descontoporcentagem
     */
    public function getNu_descontoporcentagem() {
            return $this->nu_descontoporcentagem;
    }

    /**
     * @return the $nu_descontovalor
     */
    public function getNu_descontovalor() {
            return $this->nu_descontovalor;
    }

    /**
     * @return the $id_campanhacomercial
     */
    public function getId_campanhacomercial() {
            return $this->id_campanhacomercial;
    }

    /**
     * @return the $id_campanhacomercialpremio
     */
    public function getId_campanhacomercialpremio() {
            return $this->id_campanhacomercialpremio;
    }

    /**
     * @return the $st_campanhacomercial
     */
    public function getSt_campanhacomercial() {
            return $this->st_campanhacomercial;
    }

    /**
     * @param $id_produto the $id_produto to set
     */
    public function setId_produto($id_produto) {
            $this->id_produto = $id_produto;
    }

    /**
     * @param $st_produto the $st_produto to set
     */
    public function setSt_produto($st_produto) {
            $this->st_produto = $st_produto;
    }

    /**
     * @param $id_tipoproduto the $id_tipoproduto to set
     */
    public function setId_tipoproduto($id_tipoproduto) {
            $this->id_tipoproduto = $id_tipoproduto;
    }

    /**
     * @param $st_tipoproduto the $st_tipoproduto to set
     */
    public function setSt_tipoproduto($st_tipoproduto) {
            $this->st_tipoproduto = $st_tipoproduto;
    }

    /**
     * @param $id_venda the $id_venda to set
     */
    public function setId_venda($id_venda) {
            $this->id_venda = $id_venda;
    }

    /**
     * @param $nu_valorbruto the $nu_valorbruto to set
     */
    public function setNu_valorbruto($nu_valorbruto) {
            $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @param $nu_valorliquido the $nu_valorliquido to set
     */
    public function setNu_valorliquido($nu_valorliquido) {
            $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @param $nu_descontoporcentagem the $nu_descontoporcentagem to set
     */
    public function setNu_descontoporcentagem($nu_descontoporcentagem) {
            $this->nu_descontoporcentagem = $nu_descontoporcentagem;
    }

    /**
     * @param $nu_descontovalor the $nu_descontovalor to set
     */
    public function setNu_descontovalor($nu_descontovalor) {
            $this->nu_descontovalor = $nu_descontovalor;
    }

    /**
     * @param $id_campanhacomercial the $id_campanhacomercial to set
     */
    public function setId_campanhacomercial($id_campanhacomercial) {
            $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @param $id_campanhacomercialpremio the $id_campanhacomercialpremio to set
     */
    public function setId_campanhacomercialpremio($id_campanhacomercialpremio) {
            $this->id_campanhacomercialpremio = $id_campanhacomercialpremio;
    }

    /**
     * @param $st_campanhacomercial the $st_campanhacomercial to set
     */
    public function setSt_campanhacomercial($st_campanhacomercial) {
            $this->st_campanhacomercial = $st_campanhacomercial;
    }

    /**
     * @return the $id_vendaproduto
     */
    public function getId_vendaproduto() {
            return $this->id_vendaproduto;
    }

    /**
     * @param field_type $id_vendaproduto
     */
    public function setId_vendaproduto($id_vendaproduto) {
            $this->id_vendaproduto = $id_vendaproduto;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
            $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return mixed
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param mixed $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }



	
}
<?php
/**
 * Classe para encapsular os dados da view de Disciplina, Area Conhecimento e Serie Nivel de Ensino
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwDisciplinaAreaConhecimentoNivelSerieTO extends Ead1_TO_Dinamico {
	
	/**
	 * Contém o id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Contém o nome da disciplina.
	 * @var string
	 */
	public $st_disciplina;
	
	
	/**
	 * Contém a carga horária da disciplina.
	 * @var int
	 */
	public $nu_cargahoraria;
	
	/**
	 * Contém a descrição da disciplina.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Contém o id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
		
	/**
	 * Contém o nome da área.
	 * @var String
	 */
	public $st_areaconhecimento;
		
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Diz se a área está excluída ou não.
	 * @var Boolean
	 */
	public $bl_ativo;
	
	/**
	 * Atributo que contém o id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Atributo que contém o nível de ensino.
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * Contém o id da série.
	 * @var int
	 */
	public $id_serie;
	
	/**
	 * Contém o nome da série.
	 * @var string
	 */
	public $st_serie;
	
	/**
	 * Contém o id da série anterior.
	 * @var int
	 */
	public $id_serieanterior;
	
	/**
	 * Contém o id do módulo
	 * @var int
	 */
	public $id_modulo;
	
	/**
	 * Contém o nome do modulo
	 * @var string
	 */
	public $st_modulo;
	
	/**
	 * Contém o titulo do modulo
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * Contém o id do tipo de disciplina
	 * @var int
	 */
	public $id_tipodisciplina;
	
	/**
	 * Contém o texto do tipo de disciplina
	 * @var string
	 */
	public $st_tipodisciplina;
	
	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @param $nu_cargahoraria the $nu_cargahoraria to set
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @return the $id_serieanterior
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $st_disciplina the $st_disciplina to set
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $id_areaconhecimento the $id_areaconhecimento to set
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param $st_areaconhecimento the $st_areaconhecimento to set
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $id_nivelensino the $id_nivelensino to set
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}

	/**
	 * @param $st_nivelensino the $st_nivelensino to set
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}

	/**
	 * @param $id_serie the $id_serie to set
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param $st_serie the $st_serie to set
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @param $id_serieanterior the $id_serieanterior to set
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}
	/**
	 * @return the $id_modulo
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}

	/**
	 * @return the $st_modulo
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @return the $st_tipodisciplina
	 */
	public function getSt_tipodisciplina() {
		return $this->st_tipodisciplina;
	}

	/**
	 * @param int $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}

	/**
	 * @param string $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}

	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param int $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @param string $st_tipodisciplina
	 */
	public function setSt_tipodisciplina($st_tipodisciplina) {
		$this->st_tipodisciplina = $st_tipodisciplina;
	}


	
	
}
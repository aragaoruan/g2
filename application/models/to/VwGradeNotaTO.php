<?php

/**
 * Classe para encapsular os dados da view de grade e nota
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * 
 * @package models
 * @subpackage to
 */
class VwGradeNotaTO extends Ead1_TO_Dinamico {

    public $dt_abertura;
    public $id_projetopedagogico;
    public $st_projetopedagogico;
    public $st_saladeaula;
    public $st_notatcc;
    public $st_notaead;
    public $st_notafinal;
    public $id_disciplina;
    public $st_disciplina;
    public $id_tipodisciplina;
    public $st_tituloexibicaodisciplina;
    public $id_matricula;
    public $nu_notafinal;
    public $id_evolucao;
    public $st_evolucao;
    public $id_situacao;
    public $st_situacao;
    public $nu_cargahoraria;
    public $id_saladeaula;
    public $dt_encerramento;
    public $nu_notatotal;
    public $st_status;
    public $bl_status;
    public $id_avaliacaoconjuntoreferencia;

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getDt_abertura() {
        return $this->dt_abertura;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    public function getSt_notatcc() {
        return $this->st_notatcc;
    }

    public function getSt_notaead() {
        return $this->st_notaead;
    }

    public function getSt_notafinal() {
        return $this->st_notafinal;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getSt_tituloexibicaodisciplina() {
        return $this->st_tituloexibicaodisciplina;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function getNu_notafinal() {
        return $this->nu_notafinal;
    }

    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getDt_encerramento() {
        return $this->dt_encerramento;
    }

    public function getNu_notatotal() {
        return $this->nu_notatotal;
    }

    public function getSt_status() {
        return $this->st_status;
    }

    public function getBl_status() {
        return $this->bl_status;
    }

    public function setDt_abertura($dt_abertura) {
        $this->dt_abertura = $dt_abertura;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function setSt_notatcc($st_notatcc) {
        $this->st_notatcc = $st_notatcc;
    }

    public function setSt_notaead($st_notaead) {
        $this->st_notaead = $st_notaead;
    }

    public function setSt_notafinal($st_notafinal) {
        $this->st_notafinal = $st_notafinal;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function setNu_notafinal($nu_notafinal) {
        $this->nu_notafinal = $nu_notafinal;
    }

    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
    }

    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setDt_encerramento($dt_encerramento) {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function setNu_notatotal($nu_notatotal) {
        $this->nu_notatotal = $nu_notatotal;
    }

    public function setSt_status($st_status) {
        $this->st_status = $st_status;
    }

    public function setBl_status($bl_status) {
        $this->bl_status = $bl_status;
    }

}

<?php

/**
 * Classe para encapsular os dados da view de gerar declaração
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @since 26/06/2010
 *
 * @package models
 * @subpackage to
 */
class VwGerarDeclaracaoTO extends Ead1_TO_Dinamico
{

    public $id_matricula;

    public $id_usuarioaluno;

    public $st_nomecompletoaluno;

    public $st_cpfaluno;

    public $st_rgaluno;

    public $st_orgaoexpeditor;

    public $dt_nascimentoaluno;

    public $st_municipioaluno;

    public $st_ufaluno;

    public $nu_dddaluno;

    public $nu_telefonealuno;

    public $st_projeto;

    public $nu_lei;

    public $nu_resolucao;

    public $nu_parecer;

    public $nu_portaria;

    public $st_nomeentidade;

    public $st_razaosocial;

    public $st_cnpj;

    public $st_urlimglogo;

    public $st_urlsite;

    public $st_nivelensino;

    public $dt_dataexpedicaoaluno;

    public $dt_concluintealuno;

    public $dia;

    public $mes;

    public $ano;

    public $st_ufcompleto;

    public $st_nacionalidade;

    public $st_polo;

    public $st_diretora;

    public $st_secretarioescolar;

    public $dt_datahora;

    public $nu_registrolivro;

    public $nu_folharegistro;

    public $nu_livroregistro;

    public $dt_dataregistro;

    public $st_endereco;

    public $nu_cep;

    public $st_sexo;

    public $st_filiacao;

    public $id_fundamentolei;

    public $id_fundamentoresolucao;

    public $id_fundamentoparecer;

    public $id_fundamentoportaria;

    public $st_coordenadorprojeto;

    public $st_titulomonografia;

    public $sexo_aluno;
    public $st_data;
    public $st_dia;
    public $st_mes;
    public $st_ano;
    public $dt_primeirasala;
    public $dt_previsaofim;
    public $st_codigorasteamento;

    public $st_areaconhecimento;
    public $dt_agendamento;
    public $st_horarioaulaagendamento;
    public $st_enderecoagendamento;
    public $st_municipioufagendamento;
    public $st_disciplinaagendamento;
    public $id_disciplinaagendamento;

    public $grau_academico_projeto;

    public function getSexo_aluno()
    {
        return $this->sexo_aluno;
    }

    public function setSexo_aluno($sexo_aluno)
    {
        $this->sexo_aluno = $sexo_aluno;
    }

    public function getSt_data()
    {
        return $this->st_data;
    }

    public function setSt_data($st_data)
    {
        $this->st_data = $st_data;
    }

    public function getSt_dia()
    {
        return $this->st_dia;
    }

    public function setSt_dia($st_dia)
    {
        $this->st_dia = $st_dia;
    }

    public function getSt_mes()
    {
        return $this->st_mes;
    }

    public function setSt_mes($st_mes)
    {
        $this->st_mes = $st_mes;
    }

    public function getSt_ano()
    {
        return $this->st_ano;
    }

    public function setSt_ano($st_ano)
    {
        $this->st_ano = $st_ano;
    }

    public function getDt_primeirasala()
    {
        return $this->dt_primeirasala;
    }

    public function setDt_primeirasala($dt_primeirasala)
    {
        $this->dt_primeirasala = $dt_primeirasala;
    }

    public function getDt_previsaofim()
    {
        return $this->dt_previsaofim;
    }

    public function setDt_previsaofim($dt_previsaofim)
    {
        $this->dt_previsaofim = $dt_previsaofim;
    }


    /**
     * @return unknown
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @return unknown
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * @return unknown
     */
    public function getDt_datahora()
    {
        return $this->dt_datahora;
    }

    /**
     * @return unknown
     */
    public function getDt_dataregistro()
    {
        return $this->dt_dataregistro;
    }

    /**
     * @return unknown
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @return unknown
     */
    public function getNu_cep()
    {
        return $this->nu_cep;
    }

    /**
     * @return unknown
     */
    public function getNu_folharegistro()
    {
        return $this->nu_folharegistro;
    }

    /**
     * @return unknown
     */
    public function getNu_livroregistro()
    {
        return $this->nu_livroregistro;
    }

    /**
     * @return unknown
     */
    public function getNu_registrolivro()
    {
        return $this->nu_registrolivro;
    }

    /**
     * @return unknown
     */
    public function getSt_diretora()
    {
        return $this->st_diretora;
    }

    /**
     * @return unknown
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @return unknown
     */
    public function getSt_filiacao()
    {
        return $this->st_filiacao;
    }

    /**
     * @return unknown
     */
    public function getSt_nacionalidade()
    {
        return $this->st_nacionalidade;
    }

    /**
     * @return unknown
     */
    public function getSt_polo()
    {
        return $this->st_polo;
    }

    /**
     * @return unknown
     */
    public function getSt_secretarioescolar()
    {
        return $this->st_secretarioescolar;
    }

    /**
     * @return unknown
     */
    public function getSt_sexo()
    {
        return $this->st_sexo;
    }

    /**
     * @return unknown
     */
    public function getSt_ufcompleto()
    {
        return $this->st_ufcompleto;
    }

    /**
     * @param unknown_type $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @param unknown_type $dia
     */
    public function setDia($dia)
    {
        $this->dia = $dia;
    }

    /**
     * @param unknown_type $dt_datahora
     */
    public function setDt_datahora($dt_datahora)
    {
        $this->dt_datahora = $dt_datahora;
    }

    /**
     * @param unknown_type $dt_dataregistro
     */
    public function setDt_dataregistro($dt_dataregistro)
    {
        $this->dt_dataregistro = $dt_dataregistro;
    }

    /**
     * @param unknown_type $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @param unknown_type $nu_cep
     */
    public function setNu_cep($nu_cep)
    {
        $this->nu_cep = $nu_cep;
    }

    /**
     * @param unknown_type $nu_folharegistro
     */
    public function setNu_folharegistro($nu_folharegistro)
    {
        $this->nu_folharegistro = $nu_folharegistro;
    }

    /**
     * @param unknown_type $nu_livroregistro
     */
    public function setNu_livroregistro($nu_livroregistro)
    {
        $this->nu_livroregistro = $nu_livroregistro;
    }

    /**
     * @param unknown_type $nu_registrolivro
     */
    public function setNu_registrolivro($nu_registrolivro)
    {
        $this->nu_registrolivro = $nu_registrolivro;
    }

    /**
     * @param unknown_type $st_diretora
     */
    public function setSt_diretora($st_diretora)
    {
        $this->st_diretora = $st_diretora;
    }

    /**
     * @param unknown_type $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @param unknown_type $st_filiacao
     */
    public function setSt_filiacao($st_filiacao)
    {
        $this->st_filiacao = $st_filiacao;
    }

    /**
     * @param unknown_type $st_nacionalidade
     */
    public function setSt_nacionalidade($st_nacionalidade)
    {
        $this->st_nacionalidade = $st_nacionalidade;
    }

    /**
     * @param unknown_type $st_polo
     */
    public function setSt_polo($st_polo)
    {
        $this->st_polo = $st_polo;
    }

    /**
     * @param unknown_type $st_secretarioescolar
     */
    public function setSt_secretarioescolar($st_secretarioescolar)
    {
        $this->st_secretarioescolar = $st_secretarioescolar;
    }

    /**
     * @param unknown_type $st_sexo
     */
    public function setSt_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
    }

    /**
     * @param unknown_type $st_ufcompleto
     */
    public function setSt_ufcompleto($st_ufcompleto)
    {
        $this->st_ufcompleto = $st_ufcompleto;
    }


    /**
     * @return unknown
     */
    public function getDt_concluintealuno()
    {
        return $this->dt_concluintealuno;
    }

    /**
     * @return unknown
     */
    public function getDt_dataexpedicaoaluno()
    {
        return $this->dt_dataexpedicaoaluno;
    }

    /**
     * @return unknown
     */
    public function getSt_nivelensino()
    {
        return $this->st_nivelensino;
    }

    /**
     * @param unknown_type $dt_concluintealuno
     */
    public function setDt_concluintealuno($dt_concluintealuno)
    {
        $this->dt_concluintealuno = $dt_concluintealuno;
    }

    /**
     * @param unknown_type $dt_dataexpedicaoaluno
     */
    public function setDt_dataexpedicaoaluno($dt_dataexpedicaoaluno)
    {
        $this->dt_dataexpedicaoaluno = $dt_dataexpedicaoaluno;
    }

    /**
     * @param unknown_type $st_nivelensino
     */
    public function setSt_nivelensino($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
    }


    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return the $id_usuarioaluno
     */
    public function getId_usuarioaluno()
    {
        return $this->id_usuarioaluno;
    }

    /**
     * @return the $st_nomecompletoaluno
     */
    public function getSt_nomecompletoaluno()
    {
        return $this->st_nomecompletoaluno;
    }

    /**
     * @return the $st_cpfaluno
     */
    public function getSt_cpfaluno()
    {
        return $this->st_cpfaluno;
    }

    /**
     * @return the $st_rgaluno
     */
    public function getSt_rgaluno()
    {
        return $this->st_rgaluno;
    }

    /**
     * @return the $st_orgaoexpeditor
     */
    public function getSt_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @return the $dt_nascimentoaluno
     */
    public function getDt_nascimentoaluno()
    {
        return $this->dt_nascimentoaluno;
    }

    /**
     * @return the $st_municipioaluno
     */
    public function getSt_municipioaluno()
    {
        return $this->st_municipioaluno;
    }

    /**
     * @return the $st_ufaluno
     */
    public function getSt_ufaluno()
    {
        return $this->st_ufaluno;
    }

    /**
     * @return the $nu_dddaluno
     */
    public function getNu_dddaluno()
    {
        return $this->nu_dddaluno;
    }

    /**
     * @return the $nu_telefonealuno
     */
    public function getNu_telefonealuno()
    {
        return $this->nu_telefonealuno;
    }

    /**
     * @return the $st_projeto
     */
    public function getSt_projeto()
    {
        return $this->st_projeto;
    }

    /**
     * @return the $nu_lei
     */
    public function getNu_lei()
    {
        return $this->nu_lei;
    }

    /**
     * @return the $nu_resolucao
     */
    public function getNu_resolucao()
    {
        return $this->nu_resolucao;
    }

    /**
     * @return the $nu_parecer
     */
    public function getNu_parecer()
    {
        return $this->nu_parecer;
    }

    /**
     * @return the $nu_portaria
     */
    public function getNu_portaria()
    {
        return $this->nu_portaria;
    }

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return the $st_razaosocial
     */
    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    /**
     * @return the $st_cnpj
     */
    public function getSt_cnpj()
    {
        return $this->st_cnpj;
    }

    /**
     * @return the $st_urlimglogo
     */
    public function getSt_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    /**
     * @return the $st_urlsite
     */
    public function getSt_urlsite()
    {
        return $this->st_urlsite;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param field_type $id_usuarioaluno
     */
    public function setId_usuarioaluno($id_usuarioaluno)
    {
        $this->id_usuarioaluno = $id_usuarioaluno;
    }

    /**
     * @param field_type $st_nomecompletoaluno
     */
    public function setSt_nomecompletoaluno($st_nomecompletoaluno)
    {
        $this->st_nomecompletoaluno = $st_nomecompletoaluno;
    }

    /**
     * @param field_type $st_cpfaluno
     */
    public function setSt_cpfaluno($st_cpfaluno)
    {
        $this->st_cpfaluno = $st_cpfaluno;
    }

    /**
     * @param field_type st_rgalunoo
     */
    public function setSt_rgaluno($st_rgaluno)
    {
        $this->st_rgaluno = $st_rgaluno;
    }

    /**
     * @param field_type $st_orgaoexpeditor
     */
    public function setSt_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
    }

    /**
     * @param field_type $dt_nascimentoaluno
     */
    public function setDt_nascimentoaluno($dt_nascimentoaluno)
    {
        $this->dt_nascimentoaluno = $dt_nascimentoaluno;
    }

    /**
     * @param field_type $st_municipioaluno
     */
    public function setSt_municipioaluno($st_municipioaluno)
    {
        $this->st_municipioaluno = $st_municipioaluno;
    }

    /**
     * @param field_type $st_ufaluno
     */
    public function setSt_ufaluno($st_ufaluno)
    {
        $this->st_ufaluno = $st_ufaluno;
    }

    /**
     * @param field_type $nu_dddaluno
     */
    public function setNu_dddaluno($nu_dddaluno)
    {
        $this->nu_dddaluno = $nu_dddaluno;
    }

    /**
     * @param field_type st_telefonealunoo
     */
    public function setNu_telefonealuno($nu_telefonealuno)
    {
        $this->nu_telefonealuno = $nu_telefonealuno;
    }

    /**
     * @param field_type $st_projeto
     */
    public function setSt_projeto($st_projeto)
    {
        $this->st_projeto = $st_projeto;
    }

    /**
     * @param field_type st_leii
     */
    public function setNu_lei($nu_lei)
    {
        $this->nu_lei = $nu_lei;
    }

    /**
     * @param field_type st_resolucaoo
     */
    public function setNu_resolucao($nu_resolucao)
    {
        $this->nu_resolucao = $nu_resolucao;
    }

    /**
     * @param field_type nu_parecerr
     */
    public function setNu_parecer($nu_parecer)
    {
        $this->nu_parecer = $nu_parecer;
    }

    /**
     * @param field_type nu_portariaa
     */
    public function setNu_portaria($nu_portaria)
    {
        $this->nu_portaria = $nu_portaria;
    }

    /**
     * @param field_type $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param field_type $st_razaosocial
     */
    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
    }

    /**
     * @param field_type $st_cnpj
     */
    public function setSt_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
    }

    /**
     * @param field_type $st_urlimglogo
     */
    public function setSt_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
    }

    /**
     * @param field_type $st_urlsite
     */
    public function setSt_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
    }

    /**
     * @return the $id_fundamentolei
     */
    public function getId_fundamentolei()
    {
        return $this->id_fundamentolei;
    }

    /**
     * @return the $id_fundamentoresolucao
     */
    public function getId_fundamentoresolucao()
    {
        return $this->id_fundamentoresolucao;
    }

    /**
     * @return the $id_fundamentoparecer
     */
    public function getId_fundamentoparecer()
    {
        return $this->id_fundamentoparecer;
    }

    /**
     * @return the $id_fundamentoportaria
     */
    public function getId_fundamentoportaria()
    {
        return $this->id_fundamentoportaria;
    }

    /**
     * @param field_type $id_fundamentolei
     */
    public function setId_fundamentolei($id_fundamentolei)
    {
        $this->id_fundamentolei = $id_fundamentolei;
    }

    /**
     * @param field_type $id_fundamentoresolucao
     */
    public function setId_fundamentoresolucao($id_fundamentoresolucao)
    {
        $this->id_fundamentoresolucao = $id_fundamentoresolucao;
    }

    /**
     * @param field_type $id_fundamentoparecer
     */
    public function setId_fundamentoparecer($id_fundamentoparecer)
    {
        $this->id_fundamentoparecer = $id_fundamentoparecer;
    }

    /**
     * @param field_type $id_fundamentoportaria
     */
    public function setId_fundamentoportaria($id_fundamentoportaria)
    {
        $this->id_fundamentoportaria = $id_fundamentoportaria;
    }

    public function getSt_coordenadorprojeto()
    {
        return $this->st_coordenadorprojeto;
    }

    public function setSt_coordenadorprojeto($st_coordenadorprojeto)
    {
        $this->st_coordenadorprojeto = $st_coordenadorprojeto;
    }

    public function getSt_titulomonografia()
    {
        return $this->st_titulomonografia;
    }

    public function setSt_titulomonografia($st_titulomonografia)
    {
        $this->st_titulomonografia = $st_titulomonografia;
    }

    public function getSt_codigorasteamento()
    {
        return $this->st_codigorasteamento;
    }

    public function setSt_codigorasteamento($st_codigorasteamento)
    {
        $this->st_codigorasteamento = $st_codigorasteamento;
    }

    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    public function getSt_horarioaulaagendamento()
    {
        return $this->st_horarioaulaagendamento;
    }

    public function setSt_horarioaulaagendamento($st_horarioaulaagendamento)
    {
        $this->st_horarioaulaagendamento = $st_horarioaulaagendamento;
    }

    public function getSt_enderecoagendamento()
    {
        return $this->st_enderecoagendamento;
    }

    public function setSt_enderecoagendamento($st_enderecoagendamento)
    {
        $this->st_enderecoagendamento = $st_enderecoagendamento;
    }

    public function getSt_municipioufagendamento()
    {
        return $this->st_municipioufagendamento;
    }

    public function setSt_municipioufagendamento($st_municipioufagendamento)
    {
        $this->st_municipioufagendamento = $st_municipioufagendamento;
    }

    public function getSt_disciplinaagendamento()
    {
        return $this->st_disciplinaagendamento;
    }

    public function setSt_disciplinaagendamento($st_disciplinaagendamento)
    {
        $this->st_disciplinaagendamento = $st_disciplinaagendamento;
    }

    public function getId_disciplinaagendamento()
    {
        return $this->id_disciplinaagendamento;
    }

    public function setId_disciplinaagendamento($id_disciplinaagendamento)
    {
        $this->id_disciplinaagendamento = $id_disciplinaagendamento;
    }

    public function getGrau_academico_projeto()
    {
        return $this->grau_academico_projeto;
    }

    public function setGrau_academico_projeto($grau_academico_projeto)
    {
        $this->grau_academico_projeto = $grau_academico_projeto;
    }

}

?>
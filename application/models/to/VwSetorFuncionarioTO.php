<?php
/**
 * Classe que encapsula os dados da tb_setorfuncionario
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class VwSetorFuncionarioTO extends SetorFuncionarioTO {
	
	public $st_setor;
	public $st_nomecompleto;
	public $st_cargo;
	public $st_funcao;
	
	/**
	 * @return the $st_setor
	 */
	public function getSt_setor() {
		return $this->st_setor;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_cargo
	 */
	public function getSt_cargo() {
		return $this->st_cargo;
	}

	/**
	 * @return the $st_funcao
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}

	/**
	 * @param field_type $st_setor
	 */
	public function setSt_setor($st_setor) {
		$this->st_setor = $st_setor;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_cargo
	 */
	public function setSt_cargo($st_cargo) {
		$this->st_cargo = $st_cargo;
	}

	/**
	 * @param field_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
}

?>
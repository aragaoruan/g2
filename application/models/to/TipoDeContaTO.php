<?php
/**
 * Classe para encapsular os dados de tipo de conta.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoDeContaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do tipo de conta.
	 * @var int
	 */
	public $id_tipodeconta;
	
	/**
	 * Contém a descrição do tipo de conta.
	 * @var string
	 */
	public $st_tipodeconta;
	
	/**
	 * @return int
	 */
	public function getId_tipodeconta() {
		return $this->id_tipodeconta;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipodeconta() {
		return $this->st_tipodeconta;
	}
	
	/**
	 * @param int $id_tipodeconta
	 */
	public function setId_tipodeconta($id_tipodeconta) {
		$this->id_tipodeconta = $id_tipodeconta;
	}
	
	/**
	 * @param string $st_tipodeconta
	 */
	public function setSt_tipodeconta($st_tipodeconta) {
		$this->st_tipodeconta = $st_tipodeconta;
	}

}

?>
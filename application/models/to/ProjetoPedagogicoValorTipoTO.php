<?php
/**
 * Classe para encapsular os dados de tipo de valor do contrato do projeto pedagógico.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoPedagogicoValorTipoTO extends Ead1_TO_Dinamico {

	/**
	 * Id do tipo de valor do contrato do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogicovalortipo;
	
	/**
	 * Tipo de valor do contrato do projeto pedagógico.
	 * @var string
	 */
	public $st_projetopedagogicovalortipo;
	
	/**
	 * Descrição do tipo.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogicovalortipo() {
		return $this->id_projetopedagogicovalortipo;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_projetopedagogicovalortipo() {
		return $this->st_projetopedagogicovalortipo;
	}
	
	/**
	 * @param int $id_projetopedagogicovalortipo
	 */
	public function setId_projetopedagogicovalortipo($id_projetopedagogicovalortipo) {
		$this->id_projetopedagogicovalortipo = $id_projetopedagogicovalortipo;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_projetopedagogicovalortipo
	 */
	public function setSt_projetopedagogicovalortipo($st_projetopedagogicovalortipo) {
		$this->st_projetopedagogicovalortipo = $st_projetopedagogicovalortipo;
	}

}

?>
<?php
/**
 * Classe para encapsular as possibilidades de registro da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class RegistroPessoaTO extends Ead1_TO_Dinamico {
	
	
	const MENOR_DE_IDADE = 1;
	const ESTRANTEIRO_REZIDENTE = 2;
	const ESTRANTEIRO_NAO_REZIDENTE = 3;
	const BRASILEIRO_MAIOR_DE_IDADE = 4;

	/**
	 * Contém o id do registro da pessoa.
	 * @var int
	 */
	public $id_registropessoa;
	
	/**
	 * Contém a descrição do registro da pessoa.
	 * @var string
	 */
	public $st_registropessoa;
	
	/**
	 * @return int
	 */
	public function getId_registropessoa() {
		return $this->id_registropessoa;
	}
	
	/**
	 * @return string
	 */
	public function getSt_registropessoa() {
		return $this->st_registropessoa;
	}
	
	/**
	 * @param int $id_registropessoa
	 */
	public function setId_registropessoa($id_registropessoa) {
		$this->id_registropessoa = $id_registropessoa;
	}
	
	/**
	 * @param string $st_registropessoa
	 */
	public function setSt_registropessoa($st_registropessoa) {
		$this->st_registropessoa = $st_registropessoa;
	}
	
	public function mensageiro(){
		return new Ead1_Mensageiro($this);
	}

}

?>
<?php
/**
 * Classe para encapsular os dados da view de rede social da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaRedeSocialTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade da pessoa.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário da pessoa.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do tipo de rede social.
	 * @var int
	 */
	public $id_tiporedesocial;
	
	/**
	 * Nome do tipo de rede social.
	 * @var string
	 */
	public $st_tiporedesocial;
	
	/**
	 * Id do usuário na rede social.
	 * @var string
	 */
	public $st_id;
	
	/**
	 * Id da rede social da pessoa.
	 * @var int
	 */
	public $id_redesocial;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_redesocial() {
		return $this->id_redesocial;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiporedesocial() {
		return $this->id_tiporedesocial;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_id() {
		return $this->st_id;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tiporedesocial() {
		return $this->st_tiporedesocial;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_redesocial
	 */
	public function setId_redesocial($id_redesocial) {
		$this->id_redesocial = $id_redesocial;
	}
	
	/**
	 * @param int $id_tiporedesocial
	 */
	public function setId_tiporedesocial($id_tiporedesocial) {
		$this->id_tiporedesocial = $id_tiporedesocial;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_id
	 */
	public function setSt_id($st_id) {
		$this->st_id = $st_id;
	}
	
	/**
	 * @param string $st_tiporedesocial
	 */
	public function setSt_tiporedesocial($st_tiporedesocial) {
		$this->st_tiporedesocial = $st_tiporedesocial;
	}

}

?>
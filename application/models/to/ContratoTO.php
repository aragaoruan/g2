<?php

/**
 * Classe para encapsular os dados de contrato.
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage to
 */
class ContratoTO extends Ead1_TO_Dinamico
{

    const SITUACAO_PENDENTE = 47;
    const SITUACAO_ATIVO = 44;
    const SITUACAO_INATIVO = 48;
    const SITUACAO_RENOVADO = 85;
    const EVOLUCAO_AGUARDANDO_CONFIRMACAO = 1;

    public $id_contrato;
    public $bl_ativo;
    public $id_usuario;
    public $dt_cadastro;
    public $id_entidade;
    public $id_usuariocadastro;
    public $id_evolucao;
    public $id_situacao;
    public $id_venda;
    public $id_contratoregra;
    public $id_contratomodelo;
    public $id_textosistema;
    public $dt_ativacao;
    public $dt_termino;
    public $nu_bolsa;
    public $nu_codintegracao;

    /**
     * @return \Zend_Date
     */
    public function getDt_ativacao()
    {
        return $this->dt_ativacao;
    }

    /**
     * @return \Zend_Date
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @param \Zend_Date $dt_ativacao
     */
    public function setDt_ativacao($dt_ativacao)
    {
        $this->dt_ativacao = $dt_ativacao;
    }

    /**
     * @param \Zend_Date $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }


    /**
     * @return int $id_contrato
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @return int $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return int $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return \Zend_Date $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return int $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return int $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return int $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return int $id_contratoregra
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @return int $id_contratomodelo
     */
    public function getId_contratomodelo()
    {
        return $this->id_contratomodelo;
    }

    /**
     * @return int $id_textosistema
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param int $id_contrato
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @param int $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param int $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @param int $id_contratoregra
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
    }

    /**
     * @param int $id_contratomodelo
     */
    public function setId_contratomodelo($id_contratomodelo)
    {
        $this->id_contratomodelo = $id_contratomodelo;
    }

    /**
     * @param int $id_textosistema
     */
    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
    }

    /**
     * @return number $nu_bolsa
     */
    public function getNu_bolsa()
    {
        return $this->nu_bolsa;
    }

    /**
     * @param number $nu_bolsa
     */
    public function setNu_bolsa($nu_bolsa)
    {
        $this->nu_bolsa = $nu_bolsa;
    }

    /**
     * @return number $nu_codintegracao
     */
    public function getNu_codintegracao()
    {
        return $this->nu_codintegracao;
    }

    /**
     * @param number $nu_codintegracao
     */
    public function setNu_codintegracao($nu_codintegracao)
    {
        $this->nu_codintegracao = $nu_codintegracao;
    }


}
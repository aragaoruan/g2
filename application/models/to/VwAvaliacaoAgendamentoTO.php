<?php
/**
 * Classe para encapsular os dados da view de Avaliação e Agendamento
 * @author Eder Lamar
 * 
 * @package models
 * @subpackage to
 */
class VwAvaliacaoAgendamentoTO extends Ead1_TO_Dinamico{

    public $id_avaliacaoagendamento;
    public $id_usuario;
    public $st_nomecompleto;
    public $st_avaliacao;
    public $id_tipoprova;
    public $st_tipoprova;
    public $dt_agendamento;
    public $id_situacao;
    public $st_situacao;
    public $id_avaliacaoaplicacao;
    public $id_entidade;
    public $st_nomeentidade;
    public $id_horarioaula;
    public $st_horarioaula;
    public $id_projetopedagogico;
    public $st_projetopedagogico;
    public $st_codsistema;
    public $id_matricula;
    public $id_disciplina;


	/**
	 * @return the $id_avaliacaoagendamento
	 */
	public function getId_avaliacaoagendamento() {
		return $this->id_avaliacaoagendamento;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $id_tipoprova
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}

	/**
	 * @return the $st_tipoprova
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}

	/**
	 * @return the $dt_agendamento
	 */
	public function getDt_agendamento() {
		return $this->dt_agendamento;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_avaliacaoaplicacao
	 */
	public function getId_avaliacaoaplicacao() {
		return $this->id_avaliacaoaplicacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_horarioaula
	 */
	public function getId_horarioaula() {
		return $this->id_horarioaula;
	}

	/**
	 * @return the $st_horarioaula
	 */
	public function getSt_horarioaula() {
		return $this->st_horarioaula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @param field_type $id_avaliacaoagendamento
	 */
	public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
		$this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param field_type $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}

	/**
	 * @param field_type $st_tipoprova
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

	/**
	 * @param field_type $dt_agendamento
	 */
	public function setDt_agendamento($dt_agendamento) {
		$this->dt_agendamento = $dt_agendamento;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_avaliacaoaplicacao
	 */
	public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
		$this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $id_horarioaula
	 */
	public function setId_horarioaula($id_horarioaula) {
		$this->id_horarioaula = $id_horarioaula;
	}

	/**
	 * @param field_type $st_horarioaula
	 */
	public function setSt_horarioaula($st_horarioaula) {
		$this->st_horarioaula = $st_horarioaula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}
	/**
	 * @return the $st_codsistema
	 */
	public function getSt_codsistema() {
		return $this->st_codsistema;
	}

	/**
	 * @param field_type $st_codsistema
	 */
	public function setSt_codsistema($st_codsistema) {
		$this->st_codsistema = $st_codsistema;
	}

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }


}
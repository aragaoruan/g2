<?php

/**
 * Classe para encapsular da tabela
 * @package models
 * @subpackage to
 */
class VwSalaDeAulaPerfilProjetoIntegracaoTO extends Ead1_TO_Dinamico
{

    public $st_projetopedagogico;
    public $id_usuario;
    public $st_nomecompleto;
    public $st_cpf;
    public $st_login;
    public $st_senha;
    public $id_entidade;
    public $st_nomeentidade;
    public $st_codsistema;
    public $st_codusuario;
    public $st_senhaintegrada;
    public $st_loginintegrado;
    public $id_saladeaula;
    public $id_saladeaulaprofessor;
    public $nu_maximoalunos;
    public $id_tiposaladeaula;
    public $st_tiposaladeaula;
    public $st_codsistemacurso;
    public $st_codsistemasala;
    public $st_codsistemareferencia;
    public $id_perfil;
    public $st_nomeperfil;
    public $id_perfilpedagogico;
    public $st_perfilpedagogico;
    public $id_disciplina;
    public $st_disciplina;
    public $id_areaconhecimento;
    public $st_areaconhecimento;
    public $st_abertura;
    public $st_encerramento;
    public $id_projetopedagogico;
    public $st_status;
    public $id_status;
    public $id_sistema;
    public $dt_inicioinscricao;
    public $dt_fiminscricao;
    public $bl_ativosala;
    public $st_caminho;
    public $st_tutor;
    public $id_categoriasala;
    public $id_tipodisciplina;
    public $st_alocados;
    public $st_semacesso;

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $st_abertura
     */
    public function getSt_abertura()
    {
        return $this->st_abertura;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @return the $st_login
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @return the $st_senha
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return the $st_codsistema
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @return the $st_codusuario
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @return the $st_senhaintegrada
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @return the $st_loginintegrado
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @return the $id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @return the $id_saladeaulaprofessor
     */
    public function getId_saladeaulaprofessor()
    {
        return $this->id_saladeaulaprofessor;
    }

    /**
     * @return the $nu_maximoalunos
     */
    public function getNu_maximoalunos()
    {
        return $this->nu_maximoalunos;
    }

    /**
     * @return the $id_tiposaladeaula
     */
    public function getId_tiposaladeaula()
    {
        return $this->id_tiposaladeaula;
    }

    /**
     * @return the $st_tiposaladeaula
     */
    public function getSt_tiposaladeaula()
    {
        return $this->st_tiposaladeaula;
    }

    /**
     * @return the $st_codsistemacurso
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @return the $st_codsistemasala
     */
    public function getSt_codsistemasala()
    {
        return $this->st_codsistemasala;
    }

    /**
     * @return the $st_codsistemareferencia
     */
    public function getSt_codsistemareferencia()
    {
        return $this->st_codsistemareferencia;
    }

    /**
     * @return the $id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @return the $st_nomeperfil
     */
    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @return the $id_perfilpedagogico
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @return the $st_perfilpedagogico
     */
    public function getSt_perfilpedagogico()
    {
        return $this->st_perfilpedagogico;
    }

    /**
     * @return the $id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return the $st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @return the $id_areaconhecimento
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @return the $st_areaconhecimento
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return the $st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }


    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $st_abertura
     */
    public function setSt_abertura($st_abertura)
    {
        $this->st_abertura = $st_abertura;
    }

    /**
     * @param field_type $st_encerramento
     */
    public function setSt_encerramento($st_encerramento)
    {
        $this->st_encerramento = $st_encerramento;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @param field_type $st_login
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
    }

    /**
     * @param field_type $st_senha
     */
    public function setSt_senha($st_senha)
    {
        $this->st_senha = $st_senha;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param field_type $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @param field_type $st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
    }

    /**
     * @param field_type $st_senhaintegrada
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
    }

    /**
     * @param field_type $st_loginintegrado
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
    }

    /**
     * @param field_type $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @param field_type $id_saladeaulaprofessor
     */
    public function setId_saladeaulaprofessor($id_saladeaulaprofessor)
    {
        $this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
    }

    /**
     * @param field_type $nu_maximoalunos
     */
    public function setNu_maximoalunos($nu_maximoalunos)
    {
        $this->nu_maximoalunos = $nu_maximoalunos;
    }

    /**
     * @param field_type $id_tiposaladeaula
     */
    public function setId_tiposaladeaula($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
    }

    /**
     * @param field_type $st_tiposaladeaula
     */
    public function setSt_tiposaladeaula($st_tiposaladeaula)
    {
        $this->st_tiposaladeaula = $st_tiposaladeaula;
    }

    /**
     * @param field_type $st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }

    /**
     * @param field_type $st_codsistemasala
     */
    public function setSt_codsistemasala($st_codsistemasala)
    {
        $this->st_codsistemasala = $st_codsistemasala;
    }

    /**
     * @param field_type $st_codsistemareferencia
     */
    public function setSt_codsistemareferencia($st_codsistemareferencia)
    {
        $this->st_codsistemareferencia = $st_codsistemareferencia;
    }

    /**
     * @param field_type $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @param field_type $st_nomeperfil
     */
    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    /**
     * @param field_type $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @param field_type $st_perfilpedagogico
     */
    public function setSt_perfilpedagogico($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
    }

    /**
     * @param field_type $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @param field_type $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @param field_type $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @param field_type $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @param field_type $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param field_type $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return the $st_encerramento
     */
    public function getSt_encerramento()
    {
        return $this->st_encerramento;
    }

    /**
     * @return the $st_status
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @return the $id_status
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param field_type $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }

    /**
     * @param field_type $id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
    }

    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    public function getBl_ativosala()
    {
        return $this->bl_ativosala;
    }

    public function setBl_ativosala($bl_ativosala)
    {
        $this->bl_ativosala = $bl_ativosala;
    }

    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    /**
     * @param mixed $st_caminho
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
    }

    /**
     * @return mixed
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param mixed $st_tutor
     */
    public function setSt_tutor($st_tutor)
    {
        $this->st_tutor = $st_tutor;
    }

    /**
     * @return mixed
     */
    public function getSt_tutor()
    {
        return $this->st_tutor;
    }

    /**
     * @return mixed
     */
    public function getid_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $id_categoriasala
     */
    public function setid_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param mixed $id_tipodisciplina
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return string
     */
    public function getStAlocados()
    {
        return $this->st_alocados;
    }

    /**
     * @param string $st_alocados
     */
    public function setStAlocados($st_alocados)
    {
        $this->st_alocados = $st_alocados;
    }

    /**
     * @return string
     */
    public function getStSemacesso()
    {
        return $this->st_semacesso;
    }

    /**
     * @param string $st_semacesso
     */
    public function setStSemacesso($st_semacesso)
    {
        $this->st_semacesso = $st_semacesso;
    }




}
<?php

class VwMensagemCobrancaTO extends Ead1_TO_Dinamico {

	public $id_mensagemcobranca;
	public $bl_ativo;
	public $nu_diasatraso;
	public $id_textosistema;
	public $st_textosistema;
	public $id_entidade;
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_mensagemcobranca
	 */
	public function getId_mensagemcobranca() {
		return $this->id_mensagemcobranca;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $nu_diasatraso
	 */
	public function getNu_diasatraso() {
		return $this->nu_diasatraso;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema() {
		return $this->id_textosistema;
	}

	/**
	 * @return the $st_textosistema
	 */
	public function getSt_textosistema() {
		return $this->st_textosistema;
	}

	/**
	 * @param field_type $id_mensagemcobranca
	 */
	public function setId_mensagemcobranca($id_mensagemcobranca) {
		$this->id_mensagemcobranca = $id_mensagemcobranca;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $nu_diasatraso
	 */
	public function setNu_diasatraso($nu_diasatraso) {
		$this->nu_diasatraso = $nu_diasatraso;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema) {
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $st_textosistema
	 */
	public function setSt_textosistema($st_textosistema) {
		$this->st_textosistema = $st_textosistema;
	}

	
}

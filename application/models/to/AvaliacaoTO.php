<?php

/**
 * Classe para encapsular os dados de Avaliacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage to
 */
class AvaliacaoTO extends Ead1_TO_Dinamico
{

    public $id_avaliacao;
    public $st_avaliacao;
    public $id_tipoavaliacao;
    public $nu_valor;
    public $dt_cadastro;
    public $id_usuariocadastro;
    public $id_entidade;
    public $id_situacao;
    public $id_tiporecuperacao;
    public $st_descricao;
    public $nu_quantquestoes;
    public $st_linkreferencia;
    public $st_codigoref;
    public $bl_recuperacao;


    /**
     * @return the $id_avaliacao
     */
    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }


    public function getSt_avaliacao()
    {
        return $this->st_avaliacao;
    }

    /**
     * @return the $id_tipoavaliacao
     */
    public function getId_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_tiporecuperacao
     */
    public function getId_tiporecuperacao()
    {
        return $this->id_tiporecuperacao;
    }

    /**
     * @return the $st_descricao
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @return the $nu_quantquestoes
     */
    public function getNu_quantquestoes()
    {
        return $this->nu_quantquestoes;
    }

    /**
     * @return the $st_linkreferencia
     */
    public function getSt_linkreferencia()
    {
        return $this->st_linkreferencia;
    }

    /**
     * @param field_type $id_avaliacao
     */
    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
    }


    public function setSt_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
    }

    /**
     * @param field_type $id_tipoavaliacao
     */
    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
    }

    /**
     * @param field_type $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param field_type $id_tiporecuperacao
     */
    public function setId_tiporecuperacao($id_tiporecuperacao)
    {
        $this->id_tiporecuperacao = $id_tiporecuperacao;
    }

    /**
     * @param field_type $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @param field_type $nu_quantquestoes
     */
    public function setNu_quantquestoes($nu_quantquestoes)
    {
        $this->nu_quantquestoes = $nu_quantquestoes;
        return $this;
    }

    /**
     * @param field_type $st_linkreferencia
     */
    public function setSt_linkreferencia($st_linkreferencia)
    {
        $this->st_linkreferencia = $st_linkreferencia;
        return $this;
    }

    public function getBl_recuperacao()
    {
        return $this->bl_recuperacao;
    }

    public function setBl_recuperacao($bl_recuperacao)
    {
        $this->bl_recuperacao = $bl_recuperacao;
    }

}
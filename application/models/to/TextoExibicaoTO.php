<?php
/**
 * Classe para encapsular dados da tabela de exibição dos textos 
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class TextoExibicaoTO extends Ead1_TO_Dinamico{
	

	const IMPRESSO 	= 1;
	const EMAIL		= 2;
	const SISTEMA	= 3;
	const SMS		= 4;
	
	public $id_textoexibicao;
	public $st_textoexibicao;
	public $st_descricao;
	
	/**
	 * @return the $id_textoexibicao
	 */
	public function getId_textoexibicao() {
		return $this->id_textoexibicao;
	}

	/**
	 * @return the $st_textoexibicao
	 */
	public function getSt_textoexibicao() {
		return $this->st_textoexibicao;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param $id_textoexibicao the $id_textoexibicao to set
	 */
	public function setId_textoexibicao($id_textoexibicao) {
		$this->id_textoexibicao = $id_textoexibicao;
	}

	/**
	 * @param $st_textoexibicao the $st_textoexibicao to set
	 */
	public function setSt_textoexibicao($st_textoexibicao) {
		$this->st_textoexibicao = $st_textoexibicao;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
   
}
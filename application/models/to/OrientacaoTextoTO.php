<?php
/**
 * Classe de valor da tabela tb_orientacaotexto
 * @author Arthur
 *
 */
class OrientacaoTextoTO extends Ead1_TO_Dinamico {
	
	
	/*
	 * Constantes relativas aos registros da tabela
	 */
	const RETRATO	= 1;
	const PAISAGEM	= 2;

	public $id_orientacaotexto;
	public $st_orientacaotexto;
	
	
	/**
	 * @return the $id_orientacaotexto
	 */
	public function getId_orientacaotexto(){

		return $this->id_orientacaotexto;
	}

	/**
	 * @return the $st_orientacaotexto
	 */
	public function getSt_orientacaotexto(){

		return $this->st_orientacaotexto;
	}

	/**
	 * @param $id_orientacaotexto the $id_orientacaotexto to set
	 */
	public function setId_orientacaotexto( $id_orientacaotexto ){

		$this->id_orientacaotexto = $id_orientacaotexto;
	}

	/**
	 * @param $st_orientacaotexto the $st_orientacaotexto to set
	 */
	public function setSt_orientacaotexto( $st_orientacaotexto ){

		$this->st_orientacaotexto = $st_orientacaotexto;
	}


	
}
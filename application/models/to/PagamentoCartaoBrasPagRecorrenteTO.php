<?php
class PagamentoCartaoBrasPagRecorrenteTO extends Ead1_TO_Dinamico {
	
	/**
	 * Intervalo de Pagamento
	 */
	const INTERVALO_MENSAL 		= 1;
	const INTERVALO_TRIMESTRAL 	= 3;
	const INTERVALO_SEMESTRAL 	= 6;
	const INTERVALO_ANUAL 		= 12;
	
	/**
	 * Tipo de pagamento
	 */
	const TIPO_PAGAMENTO_A_VISTA 			= 0; //0 = à vista
	const TIPO_PAGAMENTO_PARCELADO_LOJA 	= 1; //1 = parcelamento pela loja
	const TIPO_PAGAMENTO_PARCELADO_CARTAO 	= 2; //2 = parcelamento pelo emissor do cartão
	
	var $erro = array (
			'110' => 'O Valor precisa ser maior que ZERO.',
			'111' => 'O Número do Cartão é OBRIGATÓRIO.',
			'112' => 'A Validade do Cartão é OBRIGATÓRIA.',		
			'113' => 'Validade do Cartão INVÁLIDA.',		
			'114' => 'Quantidade de Parcelas INVÁLIDO.',		
			'115' => 'Meio de pagamento inválido, entre em contato com a Instituição.',		
			'118' => 'Tipo de Transação INVÁLIDA.',		
			'121' => 'O Nome é OBRIGATÓRIO.',		
			'126' => 'Método de Pagamento não habilitado, entre em contato com a Instituição.',		
			'128' => 'Tipo de Transação INVÁLIDA.',	
			'134' => 'Endereço de E-mail INVÁLIDO.',		
			'199' => 'Erro INDEFINIDO.'	
			);
	
	
	public $id_tipopagamento;
	
	/**
	 * Id da venda para buscar seus lançamentos
	 * @var int
	 */
	public $id_venda;
	
	public $id_intervalo;
	
	/**
	 * Id da bandeira utilizada para o pagamento ( VISA | MASTERCARD | AMEX)
	 * @var int
	 */
	public $id_cartaobandeira;
	
	/**
	 * Id da operadora utilizada para o pagamento ( CIELO | REDECARD )
	 * @var int
	 */
	public $id_cartaooperadora;
	
	/**
	 * Id da transação gerada para o pagamento dos lançamentos
	 * @var int
	 */
	public $id_transacao;
	
	/**
	 * Código de Segurança
	 * @var String
	 */
	public $st_codigoseguranca;
	
	/**
	 * Núemro do cartão de crédito
	 * @var String
	 */
	public $st_cartao;
	
	/**
	 * Ano de validade
	 * @var int
	 */
	public $nu_anovalidade;
	
	/**
	 * Mês de validade
	 * @var int
	 */
	public $nu_mesvalidade;
	
	
	/**
	 * Valor total, formato 100 = R$ 1,00
	 * @var int
	 */
	public $nu_valortotal;
	
	
	public $dt_inicio;
	
	
	public $dt_termino;
	
	
	
	/**
	 * Quantidade de Parcelas
	 * @var int
	 */
	public $nu_parcelas;
	
	/**
	 * Nome do portador do cartão
	 * @var string
	 */
	public $st_nomeimpresso;
	/**
	 * @return the $id_tipopagamento
	 */
	public function getId_tipopagamento() {
		return $this->id_tipopagamento;
	}

	/**
	 * @param field_type $id_tipopagamento
	 */
	public function setId_tipopagamento($id_tipopagamento) {
		$this->id_tipopagamento = $id_tipopagamento;
	}

	/**
	 * @return the $id_intervalo
	 */
	public function getId_intervalo() {
		return $this->id_intervalo;
	}

	/**
	 * @param field_type $id_intervalo
	 */
	public function setId_intervalo($id_intervalo) {
		$this->id_intervalo = $id_intervalo;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}


	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param number $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	
	/**
	 * @return the $id_cartaooperadora
	 */
	public function getId_cartaooperadora() {
		return $this->id_cartaooperadora;
	}
	
	/**
	 * @param number $id_cartaooperadora
	 */
	public function setId_cartaooperadora($id_cartaooperadora) {
		$this->id_cartaooperadora = $id_cartaooperadora;
	}
	
	/**
	 * @return the $id_cartaobandeira
	 */
	public function getId_cartaobandeira() {
		
		return $this->id_cartaobandeira;
		
	}

	/**
	 * @param number $id_cartaobandeira
	 */
	public function setId_cartaobandeira($id_cartaobandeira) {
		$this->id_cartaobandeira = $id_cartaobandeira;
	}


	/**
	 * @return the $id_transacao
	 */
	public function getId_transacao() {
		return $this->id_transacao;
	}

	/**
	 * @param number $id_transacao
	 */
	public function setId_transacao($id_transacao) {
		$this->id_transacao = $id_transacao;
	}

	/**
	 * @return the $st_codigoseguranca
	 */
	public function getSt_codigoseguranca() {
		return $this->st_codigoseguranca;
	}

	/**
	 * @param string $st_codigoseguranca
	 */
	public function setSt_codigoseguranca($st_codigoseguranca) {
		$this->st_codigoseguranca = $st_codigoseguranca;
	}

	/**
	 * @return the $st_cartao
	 */
	public function getSt_cartao() {
		return $this->st_cartao;
	}

	/**
	 * @param string $st_cartao
	 */
	public function setSt_cartao($st_cartao) {
		$this->st_cartao = $st_cartao;
	}

	/**
	 * @return the $nu_anovalidade
	 */
	public function getNu_anovalidade() {
		return $this->nu_anovalidade;
	}

	/**
	 * @param number $nu_anovalidade
	 */
	public function setNu_anovalidade($nu_anovalidade) {
		$this->nu_anovalidade = $nu_anovalidade;
	}

	/**
	 * @return the $nu_mesvalidade
	 */
	public function getNu_mesvalidade() {
		
		return str_pad($this->nu_mesvalidade, 2, "0", STR_PAD_LEFT);
		
	}

	/**
	 * @param number $nu_mesvalidade
	 */
	public function setNu_mesvalidade($nu_mesvalidade) {
		$this->nu_mesvalidade = $nu_mesvalidade;
	}

	/**
	 * @return the $nu_valortotal
	 */
	public function getNu_valortotal() {
		return number_format($this->nu_valortotal,2,',','');
	}

	/**
	 * @param number $nu_valortotal
	 */
	public function setNu_valortotal($nu_valortotal) {
		$this->nu_valortotal = $nu_valortotal;
	}

	/**
	 * @return the $nu_parcelas
	 */
	public function getnu_parcelas() {
		
		if(!$this->nu_parcelas)
			$this->setnu_parcelas(1);
		
		
		return $this->nu_parcelas;
	}

	/**
	 * @param number $nu_parcelas
	 */
	public function setnu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @return the $st_nomeimpresso
	 */
	public function getSt_nomeimpresso() {
		return $this->st_nomeimpresso;
	}

	/**
	 * @param string $st_nomeimpresso
	 */
	public function setSt_nomeimpresso($st_nomeimpresso) {
		$this->st_nomeimpresso = $st_nomeimpresso;
	}

	
	
}
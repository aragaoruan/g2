<?php
/**
 * Classe para encapsular os dados do vínculo de projeto com produto e taxa.
 * @author Eder Lamar Mariano
 * 
 * @package models
 * @subpackage to
 */
class ProdutoTaxaProjetoTO extends Ead1_TO_Dinamico {

	public $id_taxa;
	public $id_entidade;
	public $id_projetopedagogico;
	public $id_produto;
	
	/**
	 * @return the $id_taxa
	 */
	public function getId_taxa() {
		return $this->id_taxa;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_taxa
	 */
	public function setId_taxa($id_taxa) {
		$this->id_taxa = $id_taxa;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

}
?>
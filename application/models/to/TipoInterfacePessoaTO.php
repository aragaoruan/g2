<?php
/** 
 * Classe para encapsular os dados da tabela de tipos de interface da pessoa
 * @author Eder Lamar
 * @package models
 * @subpackage to
 */
class TipoInterfacePessoaTO extends Ead1_TO_Dinamico{
	 public $id_tipointerfacepessoa;
	 public $id_sistema;
	 public$st_tipointerfacepessoa;
	 
	/**
	 * @return the $id_tipointerfacepessoa
	 */
	public function getId_tipointerfacepessoa() {
		return $this->id_tipointerfacepessoa;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $st_tipointerfacepessoa
	 */
	public function getSt_tipointerfacepessoa() {
		return $this->st_tipointerfacepessoa;
	}

	/**
	 * @param $id_tipointerfacepessoa the $id_tipointerfacepessoa to set
	 */
	public function setId_tipointerfacepessoa($id_tipointerfacepessoa) {
		$this->id_tipointerfacepessoa = $id_tipointerfacepessoa;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $st_tipointerfacepessoa the $st_tipointerfacepessoa to set
	 */
	public function setSt_tipointerfacepessoa($st_tipointerfacepessoa) {
		$this->st_tipointerfacepessoa = $st_tipointerfacepessoa;
	}
}
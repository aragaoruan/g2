<?php
/**
 * Classe para encapsular os dados da view de contato de telefone para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwEntidadeContatoTelefoneTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o id de contato entidade.
	 * @var int
	 */
	public $id_contatoentidade;
	
	/**
	 * Contém o id do telefone.
	 * @var int
	 */
	public $id_telefone;
	
	/**
	 * Contém o id do tipo do telefone.
	 * @var int
	 */
	public $id_tipotelefone;
	
	/**
	 * Contém o nome do tipo de telefone.
	 * @var string
	 */
	public $st_tipotelefone;
	
	/**
	 * Contém o número do DDI.
	 * @var int
	 */
	public $nu_ddi;
	
	/**
	 * Contém o número do DDD.
	 * @var int
	 */
	public $nu_ddd;
	
	/**
	 * Contém o número do telefone.
	 * @var string
	 */
	public $nu_telefone;
	
	/**
	 * Contém a descrição do telefone.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_contatoentidade() {
		return $this->id_contatoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_telefone() {
		return $this->id_telefone;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipotelefone() {
		return $this->id_tipotelefone;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ddd() {
		return $this->nu_ddd;
	}
	
	/**
	 * @return int
	 */
	public function getNu_ddi() {
		return $this->nu_ddi;
	}
	
	/**
	 * @return string
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipotelefone() {
		return $this->st_tipotelefone;
	}
	
	/**
	 * @param int $id_contatoentidade
	 */
	public function setId_contatoentidade($id_contatoentidade) {
		$this->id_contatoentidade = $id_contatoentidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_telefone
	 */
	public function setId_telefone($id_telefone) {
		$this->id_telefone = $id_telefone;
	}
	
	/**
	 * @param int $id_tipotelefone
	 */
	public function setId_tipotelefone($id_tipotelefone) {
		$this->id_tipotelefone = $id_tipotelefone;
	}
	
	/**
	 * @param int $nu_ddd
	 */
	public function setNu_ddd($nu_ddd) {
		$this->nu_ddd = $nu_ddd;
	}
	
	/**
	 * @param int $nu_ddi
	 */
	public function setNu_ddi($nu_ddi) {
		$this->nu_ddi = $nu_ddi;
	}
	
	/**
	 * @param string $st_telefone
	 */
	public function setNu_telefone($nu_telefone) {
		$this->nu_telefone = $nu_telefone;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipotelefone
	 */
	public function setSt_tipotelefone($st_tipotelefone) {
		$this->st_tipotelefone = $st_tipotelefone;
	}

}

?>
<?php

/**
 * Classe de VwProdutoTaxaTO
 
 * @package nome_pacote
 * @subpackage nome_subpacote
 */

class VwProdutoTaxaTO extends Ead1_TO_Dinamico {

	public $id_produto;
	public $st_produto;
	public $bl_ativo;
	public $id_tipoproduto;
	public $st_tipoproduto;
	public $st_tabelarelacao;
	public $id_taxa;
	public $st_taxa;
	public $id_venda;
	public $id_matricula;
	public $id_entidade;
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $st_produto
	 */
	public function getSt_produto() {
		return $this->st_produto;
	}

	/**
	 * @param field_type $st_produto
	 */
	public function setSt_produto($st_produto) {
		$this->st_produto = $st_produto;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @return the $id_tipoproduto
	 */
	public function getId_tipoproduto() {
		return $this->id_tipoproduto;
	}

	/**
	 * @param field_type $id_tipoproduto
	 */
	public function setId_tipoproduto($id_tipoproduto) {
		$this->id_tipoproduto = $id_tipoproduto;
	}

	/**
	 * @return the $st_tipoproduto
	 */
	public function getSt_tipoproduto() {
		return $this->st_tipoproduto;
	}

	/**
	 * @param field_type $st_tipoproduto
	 */
	public function setSt_tipoproduto($st_tipoproduto) {
		$this->st_tipoproduto = $st_tipoproduto;
	}

	/**
	 * @return the $st_tabelarelacao
	 */
	public function getSt_tabelarelacao() {
		return $this->st_tabelarelacao;
	}

	/**
	 * @param field_type $st_tabelarelacao
	 */
	public function setSt_tabelarelacao($st_tabelarelacao) {
		$this->st_tabelarelacao = $st_tabelarelacao;
	}

	/**
	 * @return the $id_taxa
	 */
	public function getId_taxa() {
		return $this->id_taxa;
	}

	/**
	 * @param field_type $id_taxa
	 */
	public function setId_taxa($id_taxa) {
		$this->id_taxa = $id_taxa;
	}

	/**
	 * @return the $st_taxa
	 */
	public function getSt_taxa() {
		return $this->st_taxa;
	}

	/**
	 * @param field_type $st_taxa
	 */
	public function setSt_taxa($st_taxa) {
		$this->st_taxa = $st_taxa;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}


}

?>
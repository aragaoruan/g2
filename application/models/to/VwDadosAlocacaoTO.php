<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class VwDadosAlocacaoTO extends Ead1_TO_Dinamico{

	public $id_matricula;
	public $id_projetopedagogico;
	public $nu_disciplinassimultaneasmai;
	public $nu_disciplinassimultaneasmen;
	public $nu_disciplinassimultaneaspen;
	public $nu_disciplinasalocadas;


	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico(){ 
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $nu_disciplinassimultaneasmai
	 */
	public function getNu_disciplinassimultaneasmai(){ 
		return $this->nu_disciplinassimultaneasmai;
	}

	/**
	 * @return the $nu_disciplinassimultaneasmen
	 */
	public function getNu_disciplinassimultaneasmen(){ 
		return $this->nu_disciplinassimultaneasmen;
	}

	/**
	 * @return the $nu_disciplinassimultaneaspen
	 */
	public function getNu_disciplinassimultaneaspen(){ 
		return $this->nu_disciplinassimultaneaspen;
	}

	/**
	 * @return the $nu_disciplinasalocadas
	 */
	public function getNu_disciplinasalocadas(){ 
		return $this->nu_disciplinasalocadas;
	}


	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico){ 
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $nu_disciplinassimultaneasmai
	 */
	public function setNu_disciplinassimultaneasmai($nu_disciplinassimultaneasmai){ 
		$this->nu_disciplinassimultaneasmai = $nu_disciplinassimultaneasmai;
	}

	/**
	 * @param field_type $nu_disciplinassimultaneasmen
	 */
	public function setNu_disciplinassimultaneasmen($nu_disciplinassimultaneasmen){ 
		$this->nu_disciplinassimultaneasmen = $nu_disciplinassimultaneasmen;
	}

	/**
	 * @param field_type $nu_disciplinassimultaneaspen
	 */
	public function setNu_disciplinassimultaneaspen($nu_disciplinassimultaneaspen){ 
		$this->nu_disciplinassimultaneaspen = $nu_disciplinassimultaneaspen;
	}

	/**
	 * @param field_type $nu_disciplinasalocadas
	 */
	public function setNu_disciplinasalocadas($nu_disciplinasalocadas){ 
		$this->nu_disciplinasalocadas = $nu_disciplinasalocadas;
	}

}
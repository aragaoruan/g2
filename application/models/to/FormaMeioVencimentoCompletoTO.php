<?php
/**
 * Classe para encapsular os dados de relacionamento entre forma e meio de pagamento com mais dados.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FormaMeioVencimentoCompletoTO extends FormaMeioVencimentoTO{

	/**
	 * Nome do meio de pagamento.
	 * @var string
	 */
	public $st_meiopagamento;
	
	/**
	 * @return string
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}
	
	/**
	 * @param string $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}
}

?>
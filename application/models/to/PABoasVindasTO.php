<?php

/**
 * Classe que encapsula os dados da tb_pa_boasvindas
 * @package models
 * @subpackage to
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PABoasVindasTO extends Ead1_TO_Dinamico {

    public $id_pa_boasvindas;
    public $id_textosistema;
    public $id_entidade;
    public $id_usuariocadastro;
    public $dt_cadastro;

    public function getId_pa_boasvindas() {
        return $this->id_pa_boasvindas;
    }

    public function setId_pa_boasvindas($id_pa_boasvindas) {
        $this->id_pa_boasvindas = $id_pa_boasvindas;
    }

    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    public function setId_textosistema($id_textosistema) {
        $this->id_textosistema = $id_textosistema;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

}

?>
<?php
/**
 * Classe para encapsular os dados de tipo de trilha.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoTrilhaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Constante mapeada diretamente da tabela (tb_tipotrilha)
	 */
	 const TIPO_TRILHA_FIXA		= 1;
	 const TIPO_TRILHA_DINAMICA	= 4;

	/**
	 * Contém o id do tipo de trilha.
	 * @var int
	 */
	public $id_tipotrilha;
	
	/**
	 * Descrição do tipo da trilha.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * Contém o nome do tipo da trilha.
	 * @var string
	 */
	public $st_tipotrilha;
	
	
	/**
	 * @return int
	 */
	public function getId_tipotrilha() {
		return $this->id_tipotrilha;
	}
	
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipotrilha() {
		return $this->st_tipotrilha;
	}
	
	/**
	 * @param int $id_tipotrilha
	 */
	public function setId_tipotrilha($id_tipotrilha) {
		$this->id_tipotrilha = $id_tipotrilha;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipotrilha
	 */
	public function setSt_tipotrilha($st_tipotrilha) {
		$this->st_tipotrilha = $st_tipotrilha;
	}

}

?>
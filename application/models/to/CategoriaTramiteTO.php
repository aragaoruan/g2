<?php
/**
 * Classe para encapsular os dados de Categoria de Tramite.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CategoriaTramiteTO extends Ead1_TO_Dinamico{
	
	const MATRICULA = 1;
	const MATERIAL = 2;
	const VENDA = 3;
	
	public $id_categoriatramite;
	public $st_categoriatramite;
	public $st_tabela;
	public $st_campo;
	public $id_tipomanual;
	
	/**
	 * @return the $id_categoriatramite
	 */
	public function getId_categoriatramite() {
		return $this->id_categoriatramite;
	}

	/**
	 * @return the $st_categoriatramite
	 */
	public function getSt_categoriatramite() {
		return $this->st_categoriatramite;
	}

	/**
	 * @return the $st_tabela
	 */
	public function getSt_tabela() {
		return $this->st_tabela;
	}

	/**
	 * @return the $st_campo
	 */
	public function getSt_campo() {
		return $this->st_campo;
	}

	/**
	 * @param field_type $id_categoriatramite
	 */
	public function setId_categoriatramite($id_categoriatramite) {
		$this->id_categoriatramite = $id_categoriatramite;
	}

	/**
	 * @param field_type $st_categoriatramite
	 */
	public function setSt_categoriatramite($st_categoriatramite) {
		$this->st_categoriatramite = $st_categoriatramite;
	}

	/**
	 * @param field_type $st_tabela
	 */
	public function setSt_tabela($st_tabela) {
		$this->st_tabela = $st_tabela;
	}

	/**
	 * @param field_type $st_campo
	 */
	public function setSt_campo($st_campo) {
		$this->st_campo = $st_campo;
	}

	/**
	 * @return the $id_tipomanual
	 */
	public function getId_tipomanual() {
		return $this->id_tipomanual;
	}

	/**
	 * @param $id_tipomanual the $id_tipomanual to set
	 */
	public function setId_tipomanual($id_tipomanual) {
		$this->id_tipomanual = $id_tipomanual;
	}
}
<?php
/**
 * @author ederlamar
 *
 */
class ErroTO extends Ead1_TO_Dinamico {

	public $id_erro;
	public $id_usuario;
	public $id_entidade;
	public $id_sistema;
	public $st_mensagem;
	public $dt_cadastro;
	public $st_origem;
	public $st_codigo;
	public $bl_lido;
	public $bl_corrigido;
	
	/**
	 * @return the $id_erro
	 */
	public function getId_erro() {
		return $this->id_erro;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $st_mensagem
	 */
	public function getSt_mensagem() {
		return $this->st_mensagem;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_origem
	 */
	public function getSt_origem() {
		return $this->st_origem;
	}

	/**
	 * @return the $st_codigo
	 */
	public function getSt_codigo() {
		return $this->st_codigo;
	}

	/**
	 * @return the $bl_lido
	 */
	public function getBl_lido() {
		return $this->bl_lido;
	}

	/**
	 * @return the $bl_corrigido
	 */
	public function getBl_corrigido() {
		return $this->bl_corrigido;
	}

	/**
	 * @param field_type $id_erro
	 */
	public function setId_erro($id_erro) {
		$this->id_erro = $id_erro;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param field_type $st_mensagem
	 */
	public function setSt_mensagem($st_mensagem) {
		$this->st_mensagem = $st_mensagem;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $st_origem
	 */
	public function setSt_origem($st_origem) {
		$this->st_origem = $st_origem;
	}

	/**
	 * @param field_type $st_codigo
	 */
	public function setSt_codigo($st_codigo) {
		$this->st_codigo = $st_codigo;
	}

	/**
	 * @param field_type $bl_lido
	 */
	public function setBl_lido($bl_lido) {
		$this->bl_lido = $bl_lido;
	}

	/**
	 * @param field_type $bl_corrigido
	 */
	public function setBl_corrigido($bl_corrigido) {
		$this->bl_corrigido = $bl_corrigido;
	}
	
}
?>
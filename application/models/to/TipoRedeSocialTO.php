<?php
/**
 * Classe para encapsular os dados de tipo de rede social.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * Data de Criação: 13/09/2010
 */

class TipoRedeSocialTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do tipo de rede social.
	 * @var int
	 */
	public $id_tiporedesocial;
	
	/**
	 * Contém o nome da rede social.
	 * @var string
	 */
	public $st_tiporedesocial;
	
	/**
	 * @return int
	 */
	public function getId_tiporedesocial() {
		return $this->id_tiporedesocial;
	}
	
	/**
	 * @param int $id_tiporedesocial
	 */
	public function setId_tiporedesocial($id_tiporedesocial) {
		$this->id_tiporedesocial = $id_tiporedesocial;
	}
	
	/**
	 * @return the $st_tiporedesocial
	 */
	public function getSt_tiporedesocial() {
		return $this->st_tiporedesocial;
	}

	/**
	 * @param $st_tiporedesocial the $st_tiporedesocial to set
	 */
	public function setSt_tiporedesocial($st_tiporedesocial) {
		$this->st_tiporedesocial = $st_tiporedesocial;
	}


}

?>
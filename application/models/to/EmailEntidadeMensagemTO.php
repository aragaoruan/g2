<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class EmailEntidadeMensagemTO extends Ead1_TO_Dinamico{

	public $id_emailconfig;
	public $id_entidade;
	public $id_usuariocadastro;
	public $id_mensagempadrao;
	public $id_textosistema;
	public $bl_ativo;
	public $dt_cadastro;


	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig(){ 
		return $this->id_emailconfig;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_mensagempadrao
	 */
	public function getId_mensagempadrao(){ 
		return $this->id_mensagempadrao;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema(){ 
		return $this->id_textosistema;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig){ 
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_mensagempadrao
	 */
	public function setId_mensagempadrao($id_mensagempadrao){ 
		$this->id_mensagempadrao = $id_mensagempadrao;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema){ 
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

}
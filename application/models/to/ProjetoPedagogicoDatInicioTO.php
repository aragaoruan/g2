<?php
/**
 * Classe que encapsula o id do projeto pedagogico e a data de inicio
 * @todo Esta classe não representa nenhuma tabela, serve apenas para comunicação
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoPedagogicoDatInicioTO extends Ead1_TO_Dinamico{
	
	/**
	 * Contem o id do projeto pedagogico
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Contem a data de inicio do ProjetoPedagogico
	 * @var DateTime
	 */
	public $dt_inicio;
	
	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param DateTime $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	
	
}
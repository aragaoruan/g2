<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VendaProdutoTO extends Ead1_TO_Dinamico
{


    /**
     * @var integer
     */
    public $id_vendaproduto;

    /**
     * @var integer
     */
    public $id_produto;

    /**
     * @var float
     */
    public $nu_desconto;

    /**
     * @var integer
     */
    public $id_venda;

    /**
     * @var integer
     */
    public $id_campanhacomercial;

    /**
     * @var float
     */
    public $nu_valorliquido;

    /**
     * @var float
     */
    public $nu_valorbruto;

    /**
     * @var mixed
     */
    public $dt_cadastro;

    /**
     * @var integer
     */
    public $id_matricula;

    /**
     * @var integer
     */
    public $id_turma;

    /**
     * @var integer
     */
    public $id_avaliacaoaplicacao;

    /**
     * @var integer
     */
    public $id_produtocombo;

    /**
     * @var integer
     */
    public $id_tiposelecao;

    /**
     * @var boolean
     */
    public $bl_ativo;

    /**
     * @var string $dt_ingresso
     * @Column(name="dt_ingresso", type="string", nullable=true)
     */
    public $dt_ingresso;

    /**
     * @return int
     */
    public function getid_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param int $id_vendaproduto
     */
    public function setid_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
    }

    /**
     * @return int
     */
    public function getid_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     */
    public function setid_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return float
     */
    public function getnu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param float $nu_desconto
     */
    public function setnu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return int
     */
    public function getid_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setid_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getid_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setid_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @return float
     */
    public function getnu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param float $nu_valorliquido
     */
    public function setnu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @return float
     */
    public function getnu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @param float $nu_valorbruto
     */
    public function setnu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getid_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setid_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getid_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @param int $id_avaliacaoaplicacao
     */
    public function setid_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    /**
     * @return int
     */
    public function getid_produtocombo()
    {
        return $this->id_produtocombo;
    }

    /**
     * @param int $id_produtocombo
     */
    public function setid_produtocombo($id_produtocombo)
    {
        $this->id_produtocombo = $id_produtocombo;
    }

    /**
     * @return int
     */
    public function getid_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param int $id_tiposelecao
     */
    public function setid_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
    }

    /**
     * @return bool
     */
    public function isbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return string
     */
    public function getDt_ingresso()
    {
        return $this->dt_ingresso;
    }

    /**
     * @param string $dt_ingresso
     */
    public function setDt_ingresso($dt_ingresso)
    {
        $this->dt_ingresso = $dt_ingresso;
    }


}

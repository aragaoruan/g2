<?php
/**
 * Classe para encapsular os dados de atalho de funcionalidade pra pessoa.
 * 
 * @package models
 * @subpackage to
 */
class AtalhoPessoaTO extends Ead1_TO_Dinamico {
	
	public $id_atalhopessoa;
	public $id_entidade;
	public $id_usuario;
	public $id_funcionalidade;
	
	/**
	 * @return int
	 */
	public function getId_atalhopessoa() {
		return $this->id_atalhopessoa;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return int
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}
	
	/**
	 * @param int $id_atalhopessoa
	 */
	public function setId_atalhopessoa($id_atalhopessoa) {
		$this->id_atalhopessoa = $id_atalhopessoa;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param int $id_funcionalidade
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}
}
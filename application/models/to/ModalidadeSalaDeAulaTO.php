<?php
/**
 * Classe para encapsular os dados de modalidade de sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ModalidadeSalaDeAulaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da modalidade de sala de aula.
	 * @var int
	 */
	public $id_modalidadesaladeaula;
	
	/**
	 * Nome da modalidade da sala de aula.
	 * @var string
	 */
	public $st_modalidadesaladeaula;
	
	/**
	 * @return int
	 */
	public function getId_modalidadesaladeaula() {
		return $this->id_modalidadesaladeaula;
	}
	
	/**
	 * @return string
	 */
	public function getSt_modalidadesaladeaula() {
		return $this->st_modalidadesaladeaula;
	}
	
	/**
	 * @param int $id_modalidadesaladeaula
	 */
	public function setId_modalidadesaladeaula($id_modalidadesaladeaula) {
		$this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
	}
	
	/**
	 * @param string $st_modalidadesaladeaula
	 */
	public function setSt_modalidadesaladeaula($st_modalidadesaladeaula) {
		$this->st_modalidadesaladeaula = $st_modalidadesaladeaula;
	}

}

?>
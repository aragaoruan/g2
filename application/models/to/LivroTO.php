<?php

/**
 * Classe para encapsular da tabela  
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com 
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-27-01
 * @package models
 * @subpackage to
 */
class LivroTO extends Ead1_TO_Dinamico
{

    public $id_livro;
    public $id_situacao;
    public $id_entidadecadastro;
    public $id_usuariocadastro;
    public $id_tipolivro;
    public $id_livrocolecao;
    public $st_livro;
    public $st_isbn;
    public $st_codigocontrole;
    public $bl_ativo;
    public $st_edicao;
    public $nu_anolancamento;
    public $nu_pagina;
    public $nu_peso;

    public function getSt_edicao ()
    {
        return $this->st_edicao;
    }

    public function setSt_edicao ($st_edicao)
    {
        $this->st_edicao = $st_edicao;
        return $this;
    }

    public function getNu_anolancamento ()
    {
        return $this->nu_anolancamento;
    }

    public function setNu_anolancamento ($nu_anolancamento)
    {
        $this->nu_anolancamento = $nu_anolancamento;
        return $this;
    }

    public function getNu_pagina ()
    {
        return $this->nu_pagina;
    }

    public function setNu_pagina ($nu_pagina)
    {
        $this->nu_pagina = $nu_pagina;
        return $this;
    }

    public function getNu_peso ()
    {
        return $this->nu_peso;
    }

    public function setNu_peso ($nu_peso)
    {
        $this->nu_peso = $nu_peso;
        return $this;
    }

    /**
     * @return the $id_livro
     */
    public function getId_livro ()
    {
        return $this->id_livro;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_entidadecadastro
     */
    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_tipolivro
     */
    public function getId_tipolivro ()
    {
        return $this->id_tipolivro;
    }

    /**
     * @return the $id_livrocolecao
     */
    public function getId_livrocolecao ()
    {
        return $this->id_livrocolecao;
    }

    /**
     * @return the $st_livro
     */
    public function getSt_livro ()
    {
        return $this->st_livro;
    }

    /**
     * @return the $st_isbn
     */
    public function getSt_isbn ()
    {
        return $this->st_isbn;
    }

    /**
     * @return the $st_codigocontrole
     */
    public function getSt_codigocontrole ()
    {
        return $this->st_codigocontrole;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $id_livro
     */
    public function setId_livro ($id_livro)
    {
        $this->id_livro = $id_livro;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param field_type $id_entidadecadastro
     */
    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @param field_type $id_usuariocadastro
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param field_type $id_tipolivro
     */
    public function setId_tipolivro ($id_tipolivro)
    {
        $this->id_tipolivro = $id_tipolivro;
    }

    /**
     * @param field_type $id_livrocolecao
     */
    public function setId_livrocolecao ($id_livrocolecao)
    {
        $this->id_livrocolecao = $id_livrocolecao;
    }

    /**
     * @param field_type $st_livro
     */
    public function setSt_livro ($st_livro)
    {
        $this->st_livro = $st_livro;
    }

    /**
     * @param field_type $st_isbn
     */
    public function setSt_isbn ($st_isbn)
    {
        $this->st_isbn = $st_isbn;
    }

    /**
     * @param field_type $st_codigocontrole
     */
    public function setSt_codigocontrole ($st_codigocontrole)
    {
        $this->st_codigocontrole = $st_codigocontrole;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

}
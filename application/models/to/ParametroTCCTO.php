<?php


/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 * @subpackage to
 */
class ParametroTCCTO extends Ead1_TO_Dinamico {

	public $id_parametrotcc;
	public $id_projetopedagogico;
	public $st_parametrotcc;
	public $nu_peso;
	public $bl_ativo;
	
	
	
	/**
	 * @return the $id_parametrotcc
	 */
	public function getId_parametrotcc() {
		return $this->id_parametrotcc;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_parametrotcc
	 */
	public function getSt_parametrotcc() {
		return $this->st_parametrotcc;
	}

	/**
	 * @return the $nu_peso
	 */
	public function getNu_peso() {
		return $this->nu_peso;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $id_parametrotcc
	 */
	public function setId_parametrotcc($id_parametrotcc) {
		$this->id_parametrotcc = $id_parametrotcc;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_parametrotcc
	 */
	public function setSt_parametrotcc($st_parametrotcc) {
		$this->st_parametrotcc = $st_parametrotcc;
	}

	/**
	 * @param field_type $nu_peso
	 */
	public function setNu_peso($nu_peso) {
		$this->nu_peso = $nu_peso;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
	

}

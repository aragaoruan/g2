<?php
class PesquisarVendaAtendimentoTO extends VwVendaAtendimentoTO{
	
	//Filtros
	public $dt_inicio;
	public $dt_termino;
	public $id_funcao;
	//---
	
	//Evoluções para remover da Pesquisa
	public $arr_notinevolucao;
	//---
	
	//where montado para casos extremos
	//Filipe Toffolos POG!
	public $st_where;
	//---

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}
	/**
	 * @return the $id_funcao
	 */
	public function getId_funcao() {
		return $this->id_funcao;
	}

	/**
	 * @param field_type $id_funcao
	 */
	public function setId_funcao($id_funcao) {
		$this->id_funcao = $id_funcao;
	}
	/**
	 * @return the $arr_notinevolucao
	 */
	public function getArr_notinevolucao() {
		return $this->arr_notinevolucao;
	}

	/**
	 * @param field_type $arr_notinevolucao
	 */
	public function setArr_notinevolucao($arr_notinevolucao) {
		$this->arr_notinevolucao = $arr_notinevolucao;
	}
	/**
	 * @return the $st_where
	 */
	public function getSt_where() {
		return $this->st_where;
	}

	/**
	 * @param field_type $st_where
	 */
	public function setSt_where($st_where) {
		$this->st_where = $st_where;
	}



	
	
}
<?php
class ResponsavelImportacaoTO extends Ead1_TO_Dinamico{
	
	public $id_responsavelimportacao;
	public $nu_codresponsavelorigem;
	public $st_nomecompleto;
	public $nu_cpf;
	public $dt_nascimento;
	public $st_rg;
	public $st_orgaoexpeditorrg;
	public $dt_dataexpedicaorg;
	public $st_email;
	public $nu_telefone;
	public $st_paisendereco;
	public $st_cependereco;
	public $sg_ufendereco;
	public $st_bairroendereco;
	public $st_endereco;
	public $st_complementoendereco;
	public $st_numeroendereco;
	public $st_municipioendereco;
	public $id_entidadeimportado;
	
	/**
	 * @return the $id_responsavelimportacao
	 */
	public function getId_responsavelimportacao() {
		return $this->id_responsavelimportacao;
	}

	/**
	 * @return the $nu_codresponsavelorigem
	 */
	public function getNu_codresponsavelorigem() {
		return $this->nu_codresponsavelorigem;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $nu_cpf
	 */
	public function getNu_cpf() {
		return $this->nu_cpf;
	}

	/**
	 * @return the $dt_nascimento
	 */
	public function getDt_nascimento() {
		return $this->dt_nascimento;
	}

	/**
	 * @return the $st_rg
	 */
	public function getSt_rg() {
		return $this->st_rg;
	}

	/**
	 * @return the $st_orgaoexpeditorrg
	 */
	public function getSt_orgaoexpeditorrg() {
		return $this->st_orgaoexpeditorrg;
	}

	/**
	 * @return the $dt_dataexpedicaorg
	 */
	public function getDt_dataexpedicaorg() {
		return $this->dt_dataexpedicaorg;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email() {
		return $this->st_email;
	}

	/**
	 * @return the $st_telefone
	 */
	public function getNu_telefone() {
		return $this->nu_telefone;
	}

	/**
	 * @return the $st_paisendereco
	 */
	public function getSt_paisendereco() {
		return $this->st_paisendereco;
	}

	/**
	 * @return the $st_cependereco
	 */
	public function getSt_cependereco() {
		return $this->st_cependereco;
	}

	/**
	 * @return the $sg_ufendereco
	 */
	public function getSg_ufendereco() {
		return $this->sg_ufendereco;
	}

	/**
	 * @return the $st_bairroendereco
	 */
	public function getSt_bairroendereco() {
		return $this->st_bairroendereco;
	}

	/**
	 * @return the $st_endereco
	 */
	public function getSt_endereco() {
		return $this->st_endereco;
	}

	/**
	 * @return the $st_complementoendereco
	 */
	public function getSt_complementoendereco() {
		return $this->st_complementoendereco;
	}

	/**
	 * @return the $st_numeroendereco
	 */
	public function getSt_numeroendereco() {
		return $this->st_numeroendereco;
	}

	/**
	 * @return the $st_municipioendereco
	 */
	public function getSt_municipioendereco() {
		return $this->st_municipioendereco;
	}

	/**
	 * @param field_type $id_responsavelimportacao
	 */
	public function setId_responsavelimportacao($id_responsavelimportacao) {
		$this->id_responsavelimportacao = $id_responsavelimportacao;
	}
	
	/**
	 * @return the $id_entidadeimportado
	 */
	public function getId_entidadeimportado() {
		return $this->id_entidadeimportado;
	}

	/**
	 * @param field_type $id_responsavelimportacao
	 */
	public function setId_entidadeimportado($idEntidadeImportado) {
		$this->id_entidadeimportado = $idEntidadeImportado;
	}

	/**
	 * @param field_type $nu_codresponsavelorigem
	 */
	public function setNu_codresponsavelorigem($nu_codresponsavelorigem) {
		$this->nu_codresponsavelorigem = $nu_codresponsavelorigem;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type nu_cpf
	 */
	public function setNu_cpf($nu_cpf) {
		$this->nu_cpf = $nu_cpf;
	}

	/**
	 * @param field_type $dt_nascimento
	 */
	public function setDt_nascimento($dt_nascimento) {
		$this->dt_nascimento = $dt_nascimento;
	}

	/**
	 * @param field_type st_rg
	 */
	public function setSt_rg($st_rg) {
		$this->st_rg = $st_rg;
	}

	/**
	 * @param field_type $st_orgaoexpeditorrg
	 */
	public function setSt_orgaoexpeditorrg($st_orgaoexpeditorrg) {
		$this->st_orgaoexpeditorrg = $st_orgaoexpeditorrg;
	}

	/**
	 * @param field_type $dt_dataexpedicaorg
	 */
	public function setDt_dataexpedicaorg($dt_dataexpedicaorg) {
		$this->dt_dataexpedicaorg = $dt_dataexpedicaorg;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}

	/**
	 * @param field_type $st_telefone
	 */
	public function setNu_telefone($nu_telefone) {
		$this->nu_telefone = $nu_telefone;
	}

	/**
	 * @param field_type $st_paisendereco
	 */
	public function setSt_paisendereco($st_paisendereco) {
		$this->st_paisendereco = $st_paisendereco;
	}

	/**
	 * @param field_type $st_cependereco
	 */
	public function setSt_cependereco($st_cependereco) {
		$this->st_cependereco = $st_cependereco;
	}

	/**
	 * @param field_type $sg_ufendereco
	 */
	public function setSg_ufendereco($sg_ufendereco) {
		$this->sg_ufendereco = $sg_ufendereco;
	}

	/**
	 * @param field_type $st_bairroendereco
	 */
	public function setSt_bairroendereco($st_bairroendereco) {
		$this->st_bairroendereco = $st_bairroendereco;
	}

	/**
	 * @param field_type $st_endereco
	 */
	public function setSt_endereco($st_endereco) {
		$this->st_endereco = $st_endereco;
	}

	/**
	 * @param field_type $st_complementoendereco
	 */
	public function setSt_complementoendereco($st_complementoendereco) {
		$this->st_complementoendereco = $st_complementoendereco;
	}

	/**
	 * @param field_type $st_numeroendereco
	 */
	public function setSt_numeroendereco($st_numeroendereco) {
		$this->st_numeroendereco = $st_numeroendereco;
	}

	/**
	 * @param field_type $st_municipioendereco
	 */
	public function setSt_municipioendereco($st_municipioendereco) {
		$this->st_municipioendereco = $st_municipioendereco;
	}

}
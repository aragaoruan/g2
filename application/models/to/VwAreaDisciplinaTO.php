<?php
/**
 * Classe para encapsular os dados da view de área com disciplina.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwAreaDisciplinaTO extends Ead1_TO_Dinamico {

	/**
	 * Id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Nome da área de conhecimento.
	 * @var string
	 */
	public $st_areaconhecimento;
	
	/**
	 * Descrição da área.
	 * @var string
	 */	
	public $st_descricaoarea;
	
	/**
	 * Descrição da disciplina.
	 * @var string
	 */
	public $st_descricaodisciplina;
	
	/**
	 * Número identificador da disciplina.
	 * @var string
	 */
	public $nu_identificador;
	
	/**
	 * Carga horária da disciplina.
	 * @var int
	 */
	public $nu_cargahoraria;
	
	/**
	 * Exclusão lógica na disciplina.
	 * @var boolean
	 */
	public $bl_ativa;
	
	/**
	 * Exclusão lógica da área.
	 * @var boolean
	 */
	public $bl_ativoarea;
	
	/**
	 * Situação da área.
	 * @var int
	 */
	public $id_situacaoarea;
	
	/**
	 * Situação da disciplina.
	 * @var int
	 */
	public $id_situacaodisciplina;
	
	/**
	 * Título de exibição da disciplina.
	 * @var string
	 */
	public $st_tituloexibicao;
	
	/**
	 * @return boolean
	 */
	public function getBl_ativa() {
		return $this->bl_ativa;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_ativoarea() {
		return $this->bl_ativoarea;
	}
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacaoarea() {
		return $this->id_situacaoarea;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacaodisciplina() {
		return $this->id_situacaodisciplina;
	}
	
	/**
	 * @return int
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}
	
	/**
	 * @return string
	 */
	public function getNu_identificador() {
		return $this->nu_identificador;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaoarea() {
		return $this->st_descricaoarea;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaodisciplina() {
		return $this->st_descricaodisciplina;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @param boolean $bl_ativa
	 */
	public function setBl_ativa($bl_ativa) {
		$this->bl_ativa = $bl_ativa;
	}
	
	/**
	 * @param boolean $bl_ativoarea
	 */
	public function setBl_ativoarea($bl_ativoarea) {
		$this->bl_ativoarea = $bl_ativoarea;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param int $id_situacaoarea
	 */
	public function setId_situacaoarea($id_situacaoarea) {
		$this->id_situacaoarea = $id_situacaoarea;
	}
	
	/**
	 * @param int $id_situacaodisciplina
	 */
	public function setId_situacaodisciplina($id_situacaodisciplina) {
		$this->id_situacaodisciplina = $id_situacaodisciplina;
	}
	
	/**
	 * @param int $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}
	
	/**
	 * @param string $nu_identificador
	 */
	public function setNu_identificador($nu_identificador) {
		$this->nu_identificador = $nu_identificador;
	}
	
	/**
	 * @param string $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	
	/**
	 * @param string $st_descricaoarea
	 */
	public function setSt_descricaoarea($st_descricaoarea) {
		$this->st_descricaoarea = $st_descricaoarea;
	}
	
	/**
	 * @param string $st_descricaodisciplina
	 */
	public function setSt_descricaodisciplina($st_descricaodisciplina) {
		$this->st_descricaodisciplina = $st_descricaodisciplina;
	}
	
	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	
}

?>
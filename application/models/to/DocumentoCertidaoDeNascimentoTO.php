<?php
/**
 * Classe para encapsular os dados da certidão de nascimento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentoCertidaoDeNascimentoTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o número de certidão de nascimento.
	 * @var string
	 */
	public $st_numero;
	
	/**
	 * Contém o número do livro.
	 * @var string
	 */
	public $st_livro;
	
	/**
	 * Contém o número das folhas.
	 * @var string
	 */
	public $st_folhas;
	
	/**
	 * Contém a data de expedição da certidão.
	 * @var string
	 */
	public $dt_dataexpedicao;
	
	/**
	 * Contém a descrição do cartório.
	 * @var string
	 */
	public $st_descricaocartorio;
	
	/**
	 * @return string
	 */
	public function getDt_dataexpedicao() {
		return $this->dt_dataexpedicao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_numero() {
		return $this->st_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricaocartorio() {
		return $this->st_descricaocartorio;
	}
	
	/**
	 * @return string
	 */
	public function getSt_folhas() {
		return $this->st_folhas;
	}
	
	/**
	 * @return string
	 */
	public function getSt_livro() {
		return $this->st_livro;
	}
	
	/**
	 * @param string $dt_dataexpedicao
	 */
	public function setDt_dataexpedicao($dt_dataexpedicao) {
		$this->dt_dataexpedicao = $dt_dataexpedicao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string st_numeroo
	 */
	public function setSt_numero($st_numero) {
		$this->st_numero = $st_numero;
	}
	
	/**
	 * @param string $st_descricaocartorio
	 */
	public function setSt_descricaocartorio($st_descricaocartorio) {
		$this->st_descricaocartorio = $st_descricaocartorio;
	}
	
	/**
	 * @param string $st_folhas
	 */
	public function setSt_folhas($st_folhas) {
		$this->st_folhas = $st_folhas;
	}
	
	/**
	 * @param string $st_livro
	 */
	public function setSt_livro($st_livro) {
		$this->st_livro = $st_livro;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de livro de registro.
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class LivroRegistroTO extends Ead1_TO_Dinamico {

	public $id_usuariocadastro;
	
	public $id_livroregistroentidade;
	
	public $id_matricula;
	
	public $nu_registro;
	
	public $nu_folha;
	
	public $nu_livro;
	
	public $dt_cadastro;
	
	/**
	 * @return unknown
	 */
	public function getId_livroregistroentidade() {
		return $this->id_livroregistroentidade;
	}
	
	/**
	 * @param unknown_type $id_livroregistroentidade
	 */
	public function setId_livroregistroentidade($id_livroregistroentidade) {
		$this->id_livroregistroentidade = $id_livroregistroentidade;
	}

	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_folha() {
		return $this->nu_folha;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_livro() {
		return $this->nu_livro;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_registro() {
		return $this->nu_registro;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $nu_folha
	 */
	public function setNu_folha($nu_folha) {
		$this->nu_folha = $nu_folha;
	}
	
	/**
	 * @param unknown_type $nu_livro
	 */
	public function setNu_livro($nu_livro) {
		$this->nu_livro = $nu_livro;
	}
	
	/**
	 * @param unknown_type $nu_registro
	 */
	public function setNu_registro($nu_registro) {
		$this->nu_registro = $nu_registro;
	}

}
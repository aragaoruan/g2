<?php
/**
 * @author ederlamar
 *
 */
class VwAlocacaoTO extends Ead1_TO_Dinamico {

	public $id_usuario;
	public $id_alocacao;
	public $id_saladeaula;
    public $id_entidadeatendimento;

    /**
     * @param mixed $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @return mixed
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }
	public $bl_institucional;
	public $nu_perfilalunoobs;
	
	/**
	 * @return int $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return int $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_alocacao
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
	/**
	 * @return the $bl_institucional
	 */
	public function getBl_institucional() {
		return $this->bl_institucional;
	}

	/**
	 * @param field_type $bl_institucional
	 */
	public function setBl_institucional($bl_institucional) {
		$this->bl_institucional = $bl_institucional;
	}

	/**
	 * @return the $nu_perfilalunoobs
	 */
	public function getNu_perfilalunoobs() {
		return $this->nu_perfilalunoobs;
	}

	/**
	 * @param field_type $nu_perfilalunoobs
	 */
	public function setNu_perfilalunoobs($nu_perfilalunoobs) {
		$this->nu_perfilalunoobs = $nu_perfilalunoobs;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados de sistema
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class SistemaTO extends Ead1_TO_Dinamico {

	const GESTOR 				= 1;
	const ACTOR 				= 2;
	const FLUXUS 				= 3;
	const PORTAL_ALUNO 			= 4;
	const SISTEMA_AVALIACAO 	= 5;
	const MOODLE 				= 6;
	const BRASPAG 				= 7;
	const LOCAWEB 				= 8;
	const WORDPRESS_ECOMMERCE 	= 9;
	const BRASPAG_RECORRENTE	= 10;
	const AMAIS					= 11;
	const HENRY7				= 12;
	const LEYA_BOOKSTORE	    = 13;
    const TILTSMS	    		= 14;
    const BLACKBOARD	    	= 15;
    const BAIAOCMS	    		= 17;
    const DIARIO_ASSOCIADOS 	= 18;
    const FACEBOOK	    		= 26;
    const GOOGLE	    		= 27;
	const LOCASMS	    		= 25;

	/**
	 * Id do sistema.
	 * @var int
	 */
	public $id_sistema;
	
	/**
	 * Nome do sistema.
	 * @var String
	 */
	public $st_sistema;
	
	/**
	 * Exclusão lógico.
	 * @var Boolean
	 */
	public $bl_ativo;

	/**
	 * @var string $st_conexao
	 */
	public $st_conexao;

	/**
	 * @var string $st_chaveacesso
	 */
	public $st_chaveacesso;

	/**
	 * @return Boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}
	
	/**
	 * @return String
	 */
	public function getSt_sistema() {
		return $this->st_sistema;
	}
	
	/**
	 * @param Boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}
	
	/**
	 * @param String $st_sistema
	 */
	public function setSt_sistema($st_sistema) {
		$this->st_sistema = $st_sistema;
	}

	/**
	 * Método que retorna o nome do sistema pelo id definidos pelas constantes da classe
	 * @param int $idSistema
	 */
	public static function getSistema($idSistema, $texto = true){
		$ref = new ReflectionClass(new SistemaTO());
		$sistemas = array_flip($ref->getConstants());
		return ($texto ? implode(" ", explode("_", $sistemas[$idSistema])) : $sistemas[$idSistema] );
	}

	/**
	 * @return string
	 */
	public function getst_conexao()
	{
		return $this->st_conexao;
	}

	/**
	 * @param string $st_conexao
	 */
	public function setst_conexao($st_conexao)
	{
		$this->st_conexao = $st_conexao;
		return $this;

	}

	/**
	 * @return string
	 */
	public function getst_chaveacesso()
	{
		return $this->st_chaveacesso;
	}

	/**
	 * @param string $st_chaveacesso
	 */
	public function setst_chaveacesso($st_chaveacesso)
	{
		$this->st_chaveacesso = $st_chaveacesso;
		return $this;

	}
}
<?php
/**
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package model
 * @subpackage to
 */
class PesquisarClasseAfiliadoTO extends VwClasseAfiliadoTO {
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.telemarketing.cadastrar.CadastrarClasseAfiliado';
}

?>
<?php
/**
 * Classe de encapsulamento da tabela tb_tipoocorrencia
 *
 * @package models
 * @subpackage to
 */
class TipoOcorrenciaTO extends Ead1_TO_Dinamico {

	
	const ALUNO 	= 1;
	const TUTOR 	= 2;
	const INTERNO 	= 3;
	
	
	
	public $id_tipoocorrencia;
	public $st_tipoocorrencia;
	
	/**
	 * @return the $id_tipoocorrencia
	 */
	public function getId_tipoocorrencia() {
		return $this->id_tipoocorrencia;
	}

	/**
	 * @return the $st_tipoocorrencia
	 */
	public function getSt_tipoocorrencia() {
		return $this->st_tipoocorrencia;
	}

	/**
	 * @param field_type $id_tipoocorrencia
	 */
	public function setId_tipoocorrencia($id_tipoocorrencia) {
		$this->id_tipoocorrencia = $id_tipoocorrencia;
	}

	/**
	 * @param field_type $st_tipoocorrencia
	 */
	public function setSt_tipoocorrencia($st_tipoocorrencia) {
		$this->st_tipoocorrencia = $st_tipoocorrencia;
	}

}

?>
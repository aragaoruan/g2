<?php
/**
 * @package models
 * @subpackage to
 */
class VwAreaProjetoProdutoHoldingTO extends Ead1_TO_Dinamico{	

	public $id_areaconhecimento;
	public $st_areaconhecimento;
	public $st_tituloexibicaoarea;
	public $id_projetopedagogico;
	public $st_projetopedagogico;
	public $st_tituloexibicaoprojeto;
	public $id_entidade;
	public $st_nomeentidade;
	public $sg_uf;
	public $id_produto;
	public $vnu_parcelas;
	public $nu_valorparcela;
	public $nu_valorentrada;
	public $id_entidadematriz;
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @param field_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @param field_type $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @return the $st_tituloexibicaoarea
	 */
	public function getSt_tituloexibicaoarea() {
		return $this->st_tituloexibicaoarea;
	}

	/**
	 * @param field_type $st_tituloexibicaoarea
	 */
	public function setSt_tituloexibicaoarea($st_tituloexibicaoarea) {
		$this->st_tituloexibicaoarea = $st_tituloexibicaoarea;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicaoprojeto
	 */
	public function getSt_tituloexibicaoprojeto() {
		return $this->st_tituloexibicaoprojeto;
	}

	/**
	 * @param field_type $st_tituloexibicaoprojeto
	 */
	public function setSt_tituloexibicaoprojeto($st_tituloexibicaoprojeto) {
		$this->st_tituloexibicaoprojeto = $st_tituloexibicaoprojeto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @return the $vnu_parcelas
	 */
	public function getVnu_parcelas() {
		return $this->vnu_parcelas;
	}

	/**
	 * @param field_type $vnu_parcelas
	 */
	public function setVnu_parcelas($vnu_parcelas) {
		$this->vnu_parcelas = $vnu_parcelas;
	}

	/**
	 * @return the $nu_valorparcela
	 */
	public function getNu_valorparcela() {
		return $this->nu_valorparcela;
	}

	/**
	 * @param field_type $nu_valorparcela
	 */
	public function setNu_valorparcela($nu_valorparcela) {
		$this->nu_valorparcela = $nu_valorparcela;
	}

	/**
	 * @return the $nu_valorentrada
	 */
	public function getNu_valorentrada() {
		return $this->nu_valorentrada;
	}

	/**
	 * @param field_type $nu_valorentrada
	 */
	public function setNu_valorentrada($nu_valorentrada) {
		$this->nu_valorentrada = $nu_valorentrada;
	}

	/**
	 * @return the $id_entidadematriz
	 */
	public function getId_entidadematriz() {
		return $this->id_entidadematriz;
	}

	/**
	 * @param field_type $id_entidadematriz
	 */
	public function setId_entidadematriz($id_entidadematriz) {
		$this->id_entidadematriz = $id_entidadematriz;
	}


}
<?php
class VwAproveitamentoDisciplinaInternoTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_matriculaorigem;
	public $id_entidadematriculaorigem;
	public $st_nomeentidadematriculaorigem;
	public $sg_uf;
	public $id_municipio;
	public $st_nomemunicipio;
	public $st_projetopedagogico;
	public $st_notaoriginal;
	public $id_disciplina;
	public $st_disciplina;
	public $nu_cargahoraria;
	public $id_situacao;
	public $st_situacao;
	public $id_evolucao;
	public $nu_aprovafinal;
	public $dt_conclusao;
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_matriculaorigem
	 */
	public function getId_matriculaorigem() {
		return $this->id_matriculaorigem;
	}

	/**
	 * @return the $id_entidadematriculaorigem
	 */
	public function getId_entidadematriculaorigem() {
		return $this->id_entidadematriculaorigem;
	}

	/**
	 * @return the $st_nomeentidadematriculaorigem
	 */
	public function getSt_nomeentidadematriculaorigem() {
		return $this->st_nomeentidadematriculaorigem;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @return the $st_nomemunicipio
	 */
	public function getSt_nomemunicipio() {
		return $this->st_nomemunicipio;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_notaoriginal
	 */
	public function getSt_notaoriginal() {
		return $this->st_notaoriginal;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_disciplina
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}

	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $nu_aprovafinal
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}

	/**
	 * @return the $dt_conclusao
	 */
	public function getDt_conclusao() {
		return $this->dt_conclusao;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_matriculaorigem
	 */
	public function setId_matriculaorigem($id_matriculaorigem) {
		$this->id_matriculaorigem = $id_matriculaorigem;
	}

	/**
	 * @param field_type $id_entidadematriculaorigem
	 */
	public function setId_entidadematriculaorigem($id_entidadematriculaorigem) {
		$this->id_entidadematriculaorigem = $id_entidadematriculaorigem;
	}

	/**
	 * @param field_type $st_nomeentidadematriculaorigem
	 */
	public function setSt_nomeentidadematriculaorigem($st_nomeentidadematriculaorigem) {
		$this->st_nomeentidadematriculaorigem = $st_nomeentidadematriculaorigem;
	}

	/**
	 * @param field_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}

	/**
	 * @param field_type $st_nomemunicipio
	 */
	public function setSt_nomemunicipio($st_nomemunicipio) {
		$this->st_nomemunicipio = $st_nomemunicipio;
	}

	/**
	 * @param field_type $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
	}

	/**
	 * @param field_type $st_notaoriginal
	 */
	public function setSt_notaoriginal($st_notaoriginal) {
		$this->st_notaoriginal = $st_notaoriginal;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}

	/**
	 * @param field_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}

	/**
	 * @param field_type $dt_conclusao
	 */
	public function setDt_conclusao($dt_conclusao) {
		$this->dt_conclusao = $dt_conclusao;
	}
	/**
	 * @return the $id_municipio
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}

	/**
	 * @param field_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}


	
}
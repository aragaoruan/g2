<?php
/**
 * Classe para encapsular os dados da tb_comissionamentomeio.
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class ComissionamentoMeioTO extends Ead1_TO_Dinamico {
	
	public $id_comissionamento;
	public $id_meiopagamento;
	
	/**
	 * @return the $id_comissionamento
	 */
	public function getId_comissionamento() {
		return $this->id_comissionamento;
	}

	/**
	 * @param field_type $id_comissionamento
	 */
	public function setId_comissionamento($id_comissionamento) {
		$this->id_comissionamento = $id_comissionamento;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}


}

?>
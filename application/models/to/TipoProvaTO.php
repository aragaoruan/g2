<?php
/**
 * Classe para encapsular os dados de prova final.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class TipoProvaTO extends Ead1_TO_Dinamico {

	/**
	 * Constantes
	 * 1	Projeto
	 * 2	Sala de Aula
	 */
	const PROJETO 		= 1;
	const SALA_DE_AULA 	= 2;
	
	
	
	/**
	 * Id do tipo de prova.
	 * @var int
	 */
	public $id_tipoprova;
	
	/**
	 * Nome do tipo de prova.
	 * @var string
	 */
	public $st_tipoprova;
	
	/**
	 * Descrição do tipo de prova final.
	 * @var string
	 */
	public $st_descricao;
	
	/**
	 * @return int
	 */
	public function getId_tipoprova() {
		return $this->id_tipoprova;
	}
	
	/**
	 * @return string
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_tipoprova() {
		return $this->st_tipoprova;
	}
	
	/**
	 * @param int $id_tipoprova
	 */
	public function setId_tipoprova($id_tipoprova) {
		$this->id_tipoprova = $id_tipoprova;
	}
	
	/**
	 * @param string $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param string $st_tipoprovafinal
	 */
	public function setSt_tipoprova($st_tipoprova) {
		$this->st_tipoprova = $st_tipoprova;
	}

}

?>
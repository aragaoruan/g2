<?php
/**
 * Classe para encapsular os dados de envolvido da entidade.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class EnvolvidoEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Id do envolvido na entidade.
	 * @var int
	 */
	public $id_envolvidoentidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do tipo do envolvido.
	 * @var int
	 */
	public $id_tipoenvolvido;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	public $bl_ativo;
	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_envolvidoentidade() {
		return $this->id_envolvidoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipoenvolvido() {
		return $this->id_tipoenvolvido;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_envolvidoentidade
	 */
	public function setId_envolvidoentidade($id_envolvidoentidade) {
		$this->id_envolvidoentidade = $id_envolvidoentidade;
	}
	
	/**
	 * @param int $id_tipoenvolvido
	 */
	public function setId_tipoenvolvido($id_tipoenvolvido) {
		$this->id_tipoenvolvido = $id_tipoenvolvido;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

}

?>
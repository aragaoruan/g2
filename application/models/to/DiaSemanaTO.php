<?php
/**
 * Classe para encapsular os dados de dia da seman.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DiaSemanaTO extends Ead1_TO_Dinamico {

	/**
	 * Id do dia da semana.
	 * @var int
	 */
	public $id_diasemana;
	
	/**
	 * Dia da semana.
	 * @var string
	 */
	public $st_diasemana;
	
	/**
	 * Abreviatura do dia da semana.
	 * @var string
	 */
	public $st_abreviatura;
	
	/**
	 * @return int
	 */
	public function getId_diasemana() {
		return $this->id_diasemana;
	}
	
	/**
	 * @return string
	 */
	public function getSt_abreviatura() {
		return $this->st_abreviatura;
	}
	
	/**
	 * @return string
	 */
	public function getSt_diasemana() {
		return $this->st_diasemana;
	}
	
	/**
	 * @param int $id_diasemana
	 */
	public function setId_diasemana($id_diasemana) {
		$this->id_diasemana = $id_diasemana;
	}
	
	/**
	 * @param string $st_abreviatura
	 */
	public function setSt_abreviatura($st_abreviatura) {
		$this->st_abreviatura = $st_abreviatura;
	}
	
	/**
	 * @param string $st_diasemana
	 */
	public function setSt_diasemana($st_diasemana) {
		$this->st_diasemana = $st_diasemana;
	}

}

?>
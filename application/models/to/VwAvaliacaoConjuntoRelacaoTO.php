<?php
/**
 * Classe para encapsular os dados da view de vw_avaliacaoconjuntorelacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwAvaliacaoConjuntoRelacaoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoconjuntorelacao;
	public $id_avaliacao;
	public $st_avaliacao;
	public $id_avaliacaorecupera;
	public $st_avaliacaorecupera;
	public $id_avaliacaoconjunto;
	public $nu_valor;
	public $id_tipoavaliacao;
	public $st_tipoavaliacao;
	public $id_tiporecuperacao;
	public $st_tiporecuperacao;
	/**
	 * @return the $id_avaliacaoconjuntorelacao
	 */
	public function getId_avaliacaoconjuntorelacao() {
		return $this->id_avaliacaoconjuntorelacao;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $id_avaliacaorecupera
	 */
	public function getId_avaliacaorecupera() {
		return $this->id_avaliacaorecupera;
	}

	/**
	 * @return the $st_avaliacaorecupera
	 */
	public function getSt_avaliacaorecupera() {
		return $this->st_avaliacaorecupera;
	}

	/**
	 * @return the $id_avaliacaoconjunto
	 */
	public function getId_avaliacaoconjunto() {
		return $this->id_avaliacaoconjunto;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $id_tipoavaliacao
	 */
	public function getId_tipoavaliacao() {
		return $this->id_tipoavaliacao;
	}

	/**
	 * @return the $st_tipoavaliacao
	 */
	public function getSt_tipoavaliacao() {
		return $this->st_tipoavaliacao;
	}

	/**
	 * @return the $id_tiporecuperacao
	 */
	public function getId_tiporecuperacao() {
		return $this->id_tiporecuperacao;
	}

	/**
	 * @return the $st_tiporecuperacao
	 */
	public function getSt_tiporecuperacao() {
		return $this->st_tiporecuperacao;
	}

	/**
	 * @param field_type $id_avaliacaoconjuntorelacao
	 */
	public function setId_avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao) {
		$this->id_avaliacaoconjuntorelacao = $id_avaliacaoconjuntorelacao;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $st_avaliacao
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param field_type $id_avaliacaorecupera
	 */
	public function setId_avaliacaorecupera($id_avaliacaorecupera) {
		$this->id_avaliacaorecupera = $id_avaliacaorecupera;
	}

	/**
	 * @param field_type $st_avaliacaorecupera
	 */
	public function setSt_avaliacaorecupera($st_avaliacaorecupera) {
		$this->st_avaliacaorecupera = $st_avaliacaorecupera;
	}

	/**
	 * @param field_type $id_avaliacaoconjunto
	 */
	public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
		$this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $id_tipoavaliacao
	 */
	public function setId_tipoavaliacao($id_tipoavaliacao) {
		$this->id_tipoavaliacao = $id_tipoavaliacao;
	}

	/**
	 * @param field_type $st_tipoavaliacao
	 */
	public function setSt_tipoavaliacao($st_tipoavaliacao) {
		$this->st_tipoavaliacao = $st_tipoavaliacao;
	}

	/**
	 * @param field_type $id_tiporecuperacao
	 */
	public function setId_tiporecuperacao($id_tiporecuperacao) {
		$this->id_tiporecuperacao = $id_tiporecuperacao;
	}

	/**
	 * @param field_type $st_tiporecuperacao
	 */
	public function setSt_tiporecuperacao($st_tiporecuperacao) {
		$this->st_tiporecuperacao = $st_tiporecuperacao;
	}

}
<?php

/**
 * Classe para encapsular vw de pesquisa de aplicadores de prova
 * @author Denise Xavier - denise.xavier@unyleya.com.br
 * @package models
 * @subpackage to
 */
 
class VwPesquisarAplicadorProvaTO extends Ead1_TO_Dinamico{

	public $id_aplicadorprova;
    public $id_entidadeaplicador;
	public $id_usuarioaplicador;
	public $id_entidadecadastro;
	public $st_aplicadorprova;
	public $bl_ativo;
	public $id_tipopessoa;
	public $st_tipopessoa;
	public $st_local;

    /**
     * @param mixed $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return mixed
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return mixed
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param mixed $id_entidadeaplicador
     */
    public function setId_entidadeaplicador($id_entidadeaplicador)
    {
        $this->id_entidadeaplicador = $id_entidadeaplicador;
    }

    /**
     * @return mixed
     */
    public function getId_entidadeaplicador()
    {
        return $this->id_entidadeaplicador;
    }

    /**
     * @param mixed $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return mixed
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param mixed $id_tipopessoa
     */
    public function setId_tipopessoa($id_tipopessoa)
    {
        $this->id_tipopessoa = $id_tipopessoa;
    }

    /**
     * @return mixed
     */
    public function getId_tipopessoa()
    {
        return $this->id_tipopessoa;
    }

    /**
     * @param mixed $id_usuarioaplicador
     */
    public function setId_usuarioaplicador($id_usuarioaplicador)
    {
        $this->id_usuarioaplicador = $id_usuarioaplicador;
    }

    /**
     * @return mixed
     */
    public function getId_usuarioaplicador()
    {
        return $this->id_usuarioaplicador;
    }

    /**
     * @param mixed $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return mixed
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @param mixed $st_local
     */
    public function setSt_local($st_local)
    {
        $this->st_local = $st_local;
    }

    /**
     * @return mixed
     */
    public function getSt_local()
    {
        return $this->st_local;
    }

    /**
     * @param mixed $st_tipopessoa
     */
    public function setSt_tipopessoa($st_tipopessoa)
    {
        $this->st_tipopessoa = $st_tipopessoa;
    }

    /**
     * @return mixed
     */
    public function getSt_tipopessoa()
    {
        return $this->st_tipopessoa;
    }
	
	

	

	
}
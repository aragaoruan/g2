<?php

class MaterialRemessaTO extends Ead1_TO_Dinamico {

	public $id_materialremessa;
	public $id_remessa;
	public $id_itemdematerial;
	/**
	 * @return the $id_materialremessa
	 */
	public function getId_materialremessa() {
		return $this->id_materialremessa;
	}

	/**
	 * @param field_type $id_materialremessa
	 */
	public function setId_materialremessa($id_materialremessa) {
		$this->id_materialremessa = $id_materialremessa;
	}

	/**
	 * @return the $id_remessa
	 */
	public function getId_remessa() {
		return $this->id_remessa;
	}

	/**
	 * @param field_type $id_remessa
	 */
	public function setId_remessa($id_remessa) {
		$this->id_remessa = $id_remessa;
	}

	/**
	 * @return the $id_itemdematerial
	 */
	public function getId_itemdematerial() {
		return $this->id_itemdematerial;
	}

	/**
	 * @param field_type $id_itemdematerial
	 */
	public function setId_itemdematerial($id_itemdematerial) {
		$this->id_itemdematerial = $id_itemdematerial;
	}

	
}

?>
<?php

/**
 * Classe para encapsular os dados de Tramite com entrega de material.
 * @author edermariano
 *
 * @package models
 * @subpackage to
 */
class TramitePreVendaTO extends Ead1_TO_Dinamico
{

    public $id_tramiteprevenda;
    public $id_tramite;
    public $id_prevenda;

    /**
     * @return mixed
     */
    public function getId_tramiteprevenda()
    {
        return $this->id_tramiteprevenda;
    }

    /**
     * @param mixed $id_tramiteprevenda
     */
    public function setId_tramiteprevenda($id_tramiteprevenda)
    {
        $this->id_tramiteprevenda = $id_tramiteprevenda;
    }

    /**
     * @return mixed
     */
    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    /**
     * @param mixed $id_tramite
     */
    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
    }

    /**
     * @return mixed
     */
    public function getId_prevenda()
    {
        return $this->id_prevenda;
    }

    /**
     * @param mixed $id_prevenda
     */
    public function setId_prevenda($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
    }

}

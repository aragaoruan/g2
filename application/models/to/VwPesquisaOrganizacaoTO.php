<?php
/**
 * Classe para encapsular os dados de view de pesquisa de instituição.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPesquisaOrganizacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id da entidade cadastradora.
	 * @var int
	 */
	public $id_entidadecadastro;
	
	/**
	 * Nome da instituição.
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Razão social da instituição.
	 * @var string
	 */
	public $st_razaosocial;
	
	/**
	 * Id da Situação da instituição.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Situação da instituição.
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * Exclusão lógica na instituição.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Id da classe da entidade.
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * @return int
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}
	
	/**
	 * @param int $id_entidadeclasse
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}
	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

}

?>
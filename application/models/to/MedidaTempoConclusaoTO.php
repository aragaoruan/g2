<?php
/**
 * Classe para encapsular os dados de medida de tempo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class MedidaTempoConclusaoTO extends Ead1_TO_Dinamico {

	/**
	 * Id da medida.
	 * @var int
	 */
	public $id_medidatempoconclusao;
	
	/**
	 * Nome da medida.
	 * @var String
	 */
	public $st_medidatempoconclusao;
	
	/**
	 * @return int
	 */
	public function getId_medidatempoconclusao() {
		return $this->id_medidatempoconclusao;
	}
	
	/**
	 * @return String
	 */
	public function getSt_medidatempoconclusao() {
		return $this->st_medidatempoconclusao;
	}
	
	/**
	 * @param int $id_medidatempoconclusao
	 */
	public function setId_medidatempoconclusao($id_medidatempoconclusao) {
		$this->id_medidatempoconclusao = $id_medidatempoconclusao;
	}
	
	/**
	 * @param String $st_medidatempoconclusao
	 */
	public function setSt_medidatempoconclusao($st_medidatempoconclusao) {
		$this->st_medidatempoconclusao = $st_medidatempoconclusao;
	}

}

?>
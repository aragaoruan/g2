<?php
/**
 * Classe para encapsular os dados da view que traz as informações de configurações dos relatórios.
 * @author Eder Lamar edermariano@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class VwDadosRelatorioTO extends Ead1_TO_Dinamico{
	
	public $id_relatorio;
	public $id_tipoorigemrelatorio;
	public $id_funcionalidade;
	public $id_entidade;
	public $id_usuario;
	public $st_relatorio;
	public $st_origem;
	public $bl_geral;
	public $dt_cadastro;
	public $st_descricao;
	public $id_camporelatorio;
	public $st_camporelatorio;
	public $st_titulocampo;
	public $bl_filtro;
	public $bl_exibido;
	public $id_operacaorelatorio;
	public $id_tipocampo;
	public $id_interface;
	public $id_tipopropriedadecamporel;
	public $st_tipopropriedadecamporel;
	public $st_valor;
	public $nu_ordem;
	
	/**
	 * @return the $id_relatorio
	 */
	public function getId_relatorio() {
		return $this->id_relatorio;
	}
	
	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @return the $id_tipoorigemrelatorio
	 */
	public function getId_tipoorigemrelatorio() {
		return $this->id_tipoorigemrelatorio;
	}

	/**
	 * @return the $id_funcionalidade
	 */
	public function getId_funcionalidade() {
		return $this->id_funcionalidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $st_relatorio
	 */
	public function getSt_relatorio() {
		return $this->st_relatorio;
	}

	/**
	 * @return the $st_origem
	 */
	public function getSt_origem() {
		return $this->st_origem;
	}

	/**
	 * @return the $bl_geral
	 */
	public function getBl_geral() {
		return $this->bl_geral;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $id_camporelatorio
	 */
	public function getId_camporelatorio() {
		return $this->id_camporelatorio;
	}

	/**
	 * @return the $st_camporelatorio
	 */
	public function getSt_camporelatorio() {
		return $this->st_camporelatorio;
	}

	/**
	 * @return the $st_titulocampo
	 */
	public function getSt_titulocampo() {
		return $this->st_titulocampo;
	}

	/**
	 * @return the $bl_filtro
	 */
	public function getBl_filtro() {
		return $this->bl_filtro;
	}

	/**
	 * @return the $bl_exibido
	 */
	public function getBl_exibido() {
		return $this->bl_exibido;
	}

	/**
	 * @return the $id_operacaorelatorio
	 */
	public function getId_operacaorelatorio() {
		return $this->id_operacaorelatorio;
	}

	/**
	 * @return the $id_tipocampo
	 */
	public function getId_tipocampo() {
		return $this->id_tipocampo;
	}

	/**
	 * @return the $id_interface
	 */
	public function getId_interface() {
		return $this->id_interface;
	}

	/**
	 * @return the $id_tipopropriedadecamporel
	 */
	public function getId_tipopropriedadecamporel() {
		return $this->id_tipopropriedadecamporel;
	}

	/**
	 * @return the $st_tipopropriedadecamporel
	 */
	public function getSt_tipopropriedadecamporel() {
		return $this->st_tipopropriedadecamporel;
	}

	/**
	 * @return the $st_valor
	 */
	public function getSt_valor() {
		return $this->st_valor;
	}

	/**
	 * @param $id_relatorio the $id_relatorio to set
	 */
	public function setId_relatorio($id_relatorio) {
		$this->id_relatorio = $id_relatorio;
	}

	/**
	 * @param $nu_ordem the $nu_ordem to set
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param $id_tipoorigemrelatorio the $id_tipoorigemrelatorio to set
	 */
	public function setId_tipoorigemrelatorio($id_tipoorigemrelatorio) {
		$this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
	}

	/**
	 * @param $id_funcionalidade the $id_funcionalidade to set
	 */
	public function setId_funcionalidade($id_funcionalidade) {
		$this->id_funcionalidade = $id_funcionalidade;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_usuario the $id_usuario to set
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param $st_relatorio the $st_relatorio to set
	 */
	public function setSt_relatorio($st_relatorio) {
		$this->st_relatorio = $st_relatorio;
	}

	/**
	 * @param $st_origem the $st_origem to set
	 */
	public function setSt_origem($st_origem) {
		$this->st_origem = $st_origem;
	}

	/**
	 * @param $bl_geral the $bl_geral to set
	 */
	public function setBl_geral($bl_geral) {
		$this->bl_geral = $bl_geral;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param $st_descricao the $st_descricao to set
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param $id_camporelatorio the $id_camporelatorio to set
	 */
	public function setId_camporelatorio($id_camporelatorio) {
		$this->id_camporelatorio = $id_camporelatorio;
	}

	/**
	 * @param $st_camporelatorio the $st_camporelatorio to set
	 */
	public function setSt_camporelatorio($st_camporelatorio) {
		$this->st_camporelatorio = $st_camporelatorio;
	}

	/**
	 * @param $st_titulocampo the $st_titulocampo to set
	 */
	public function setSt_titulocampo($st_titulocampo) {
		$this->st_titulocampo = $st_titulocampo;
	}

	/**
	 * @param $bl_filtro the $bl_filtro to set
	 */
	public function setBl_filtro($bl_filtro) {
		$this->bl_filtro = $bl_filtro;
	}

	/**
	 * @param $bl_exibido the $bl_exibido to set
	 */
	public function setBl_exibido($bl_exibido) {
		$this->bl_exibido = $bl_exibido;
	}

	/**
	 * @param $id_operacaorelatorio the $id_operacaorelatorio to set
	 */
	public function setId_operacaorelatorio($id_operacaorelatorio) {
		$this->id_operacaorelatorio = $id_operacaorelatorio;
	}

	/**
	 * @param $id_tipocampo the $id_tipocampo to set
	 */
	public function setId_tipocampo($id_tipocampo) {
		$this->id_tipocampo = $id_tipocampo;
	}

	/**
	 * @param $id_interface the $id_interface to set
	 */
	public function setId_interface($id_interface) {
		$this->id_interface = $id_interface;
	}

	/**
	 * @param $id_tipopropriedadecamporel the $id_tipopropriedadecamporel to set
	 */
	public function setId_tipopropriedadecamporel($id_tipopropriedadecamporel) {
		$this->id_tipopropriedadecamporel = $id_tipopropriedadecamporel;
	}

	/**
	 * @param $st_tipopropriedadecamporel the $st_tipopropriedadecamporel to set
	 */
	public function setSt_tipopropriedadecamporel($st_tipopropriedadecamporel) {
		$this->st_tipopropriedadecamporel = $st_tipopropriedadecamporel;
	}

	/**
	 * @param $st_valor the $st_valor to set
	 */
	public function setSt_valor($st_valor) {
		$this->st_valor = $st_valor;
	}
}
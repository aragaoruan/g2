<?php
class VwHistoricoGeralTO extends Ead1_TO_Dinamico{
	
	public $id_matricula;
	public $id_projetopedagogico;
	public $st_tituloexibicao;
	public $id_serie;
	public $st_serie;
	public $id_serieanterior;
	public $id_areaconhecimento;
	public $st_areaconhecimento;
	public $id_tipoareaconhecimento;
	public $id_disciplina;
	public $nu_aprovafinal;
	public $nu_notamaxima;
	public $nu_ordem;
	public $nu_cargahoraria;
	public $id_evolucao;
	public $st_evolucao;
	public $st_cargahorariaaproveitamento;
	public $st_notaoriginalaproveitamento;
	public $st_instituicaoaproveitamento;
	public $sg_ufaproveitamento;
	public $dt_conclusaoaproveitamento;
	public $id_areaagregadora;
	public $st_areaagregadora;
	
	
	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}
	
	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return the $st_serie
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}

	/**
	 * @return the $id_serieanterior
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $id_tipoareaconhecimento
	 */
	public function getId_tipoareaconhecimento() {
		return $this->id_tipoareaconhecimento;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $nu_aprovafinal
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}

	/**
	 * @return the $nu_notamaxima
	 */
	public function getNu_notamaxima() {
		return $this->nu_notamaxima;
	}

	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @return the $st_cargahorariaaproveitamento
	 */
	public function getSt_cargahorariaaproveitamento() {
		return $this->st_cargahorariaaproveitamento;
	}

	/**
	 * @return the $st_notaoriginalaproveitamento
	 */
	public function getSt_notaoriginalaproveitamento() {
		return $this->st_notaoriginalaproveitamento;
	}

	/**
	 * @return the $st_instituicaoaproveitamento
	 */
	public function getSt_instituicaoaproveitamento() {
		return $this->st_instituicaoaproveitamento;
	}

	/**
	 * @return the $sg_ufaproveitamento
	 */
	public function getSg_ufaproveitamento() {
		return $this->sg_ufaproveitamento;
	}

	/**
	 * @return the $dt_conclusaoaproveitamento
	 */
	public function getDt_conclusaoaproveitamento() {
		return $this->dt_conclusaoaproveitamento;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}

	/**
	 * @param field_type $id_serieanterior
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}

	/**
	 * @param field_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}

	/**
	 * @param field_type $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}

	/**
	 * @param field_type $id_tipoareaconhecimento
	 */
	public function setId_tipoareaconhecimento($id_tipoareaconhecimento) {
		$this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}

	/**
	 * @param field_type $nu_notamaxima
	 */
	public function setNu_notamaxima($nu_notamaxima) {
		$this->nu_notamaxima = $nu_notamaxima;
	}

	/**
	 * @param field_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}

	/**
	 * @param field_type $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @param field_type $st_cargahorariaaproveitamento
	 */
	public function setSt_cargahorariaaproveitamento($st_cargahorariaaproveitamento) {
		$this->st_cargahorariaaproveitamento = $st_cargahorariaaproveitamento;
	}

	/**
	 * @param field_type $st_notaoriginalaproveitamento
	 */
	public function setSt_notaoriginalaproveitamento($st_notaoriginalaproveitamento) {
		$this->st_notaoriginalaproveitamento = $st_notaoriginalaproveitamento;
	}

	/**
	 * @param field_type $st_instituicaoaproveitamento
	 */
	public function setSt_instituicaoaproveitamento($st_instituicaoaproveitamento) {
		$this->st_instituicaoaproveitamento = $st_instituicaoaproveitamento;
	}

	/**
	 * @param field_type $sg_ufaproveitamento
	 */
	public function setSg_ufaproveitamento($sg_ufaproveitamento) {
		$this->sg_ufaproveitamento = $sg_ufaproveitamento;
	}

	/**
	 * @param field_type $dt_conclusaoaproveitamento
	 */
	public function setDt_conclusaoaproveitamento($dt_conclusaoaproveitamento) {
		$this->dt_conclusaoaproveitamento = $dt_conclusaoaproveitamento;
	}
	/**
	 * @return the $id_areaagregadora
	 */
	public function getId_areaagregadora() {
		return $this->id_areaagregadora;
	}

	/**
	 * @return the $st_areaagregadora
	 */
	public function getSt_areaagregadora() {
		return $this->st_areaagregadora;
	}

	/**
	 * @param field_type $id_areaagregadora
	 */
	public function setId_areaagregadora($id_areaagregadora) {
		$this->id_areaagregadora = $id_areaagregadora;
	}

	/**
	 * @param field_type $st_areaagregadora
	 */
	public function setSt_areaagregadora($st_areaagregadora) {
		$this->st_areaagregadora = $st_areaagregadora;
	}
	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @param field_type $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
	}

	/**
	 * @param int $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	
}
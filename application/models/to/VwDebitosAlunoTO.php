<?php

/**
 * Classe de VwDebitosAlunoTO
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage to
 */
class VwDebitosAlunoTO extends Ead1_TO_Dinamico {

	public $id_venda;
	public $id_usuariovenda;
	public $st_nomecompleto;
	public $nu_valor;
	public $dt_vencimento;
	public $st_meiopagamento;
	public $id_textoavisoatraso;
	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}

	/**
	 * @return the $id_usuariovenda
	 */
	public function getId_usuariovenda() {
		return $this->id_usuariovenda;
	}

	/**
	 * @param field_type $id_usuariovenda
	 */
	public function setId_usuariovenda($id_usuariovenda) {
		$this->id_usuariovenda = $id_usuariovenda;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
	}

	/**
	 * @return the $st_meiopagamento
	 */
	public function getSt_meiopagamento() {
		return $this->st_meiopagamento;
	}

	/**
	 * @param field_type $st_meiopagamento
	 */
	public function setSt_meiopagamento($st_meiopagamento) {
		$this->st_meiopagamento = $st_meiopagamento;
	}

	/**
	 * @return the $id_textoavisoatraso
	 */
	public function getId_textoavisoatraso() {
		return $this->id_textoavisoatraso;
	}

	/**
	 * @param field_type $id_textoavisoatraso
	 */
	public function setId_textoavisoatraso($id_textoavisoatraso) {
		$this->id_textoavisoatraso = $id_textoavisoatraso;
	}

	
}

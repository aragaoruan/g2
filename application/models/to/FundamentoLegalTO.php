<?php
/**
 * Classe para encapsular os dados de fundamento legal.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class FundamentoLegalTO extends Ead1_TO_Dinamico {

	/**
	 * Id do fundamento legal.
	 * @var int
	 */
	public $id_fundamentolegal;
	
	/**
	 * Número do fundamento legal.
	 * @var int
	 */
	public $nu_numero;
	
	/**
	 * Data de vigência.
	 * @var String
	 */
	public $dt_vigencia;
	
	/**
	 * Data de cadastro do fundamento legal.
	 * @var String
	 */
	public $dt_cadastro;
	
	/**
	 * Id do usuário que cadastrou o fundamento legal.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id da situação.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Data de publicação do fundamento legal.
	 * @var String
	 */
	public $dt_publicacao;
	
	/**
	 * órgão expedidor do fundamento legal.
	 * @var string
	 */
	public $st_orgaoexped;
	
	/**
	 * Id do tipo de fundamento legal.
	 * @var int
	 */
	public $id_tipofundamentolegal;
	
	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Variavel que diz se esta ativo ou nao
	 * @var boolean
	 */
	public $bl_ativo;
	
/**
	 * Id de nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Nível de ensino.
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @return int
	 */
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	/**
	 * @return String
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return String
	 */
	public function getDt_publicacao() {
		return $this->dt_publicacao;
	}
	
	/**
	 * @return String
	 */
	public function getDt_vigencia() {
		return $this->dt_vigencia;
	}
	
	/**
	 * @return int
	 */
	public function getId_fundamentolegal() {
		return $this->id_fundamentolegal;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_tipofundamentolegal() {
		return $this->id_tipofundamentolegal;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return int
	 */
	public function getNu_numero() {
		return $this->nu_numero;
	}
	
	/**
	 * @return string
	 */
	public function getSt_orgaoexped() {
		return $this->st_orgaoexped;
	}
	
	/**
	 * @param String $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param String $dt_publicacao
	 */
	public function setDt_publicacao($dt_publicacao) {
		$this->dt_publicacao = $dt_publicacao;
	}
	
	/**
	 * @param String $dt_vigencia
	 */
	public function setDt_vigencia($dt_vigencia) {
		$this->dt_vigencia = $dt_vigencia;
	}
	
	/**
	 * @param int $id_fundamentolegal
	 */
	public function setId_fundamentolegal($id_fundamentolegal) {
		if($id_fundamentolegal === 0){
			$this->id_fundamentolegal = NULL;
		}else{
			$this->id_fundamentolegal = $id_fundamentolegal;
		}
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		if($id_situacao === 0){
			$this->id_situacao = NULL;
		}else{
			$this->id_situacao = $id_situacao;
		}
	}
	
	/**
	 * @param int $id_tipofundamentolegal
	 */
	public function setId_tipofundamentolegal($id_tipofundamentolegal) {
		if($id_tipofundamentolegal === 0){
			$this->id_tipofundamentolegal = NULL;
		}else{
			$this->id_tipofundamentolegal = $id_tipofundamentolegal;
		}
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		if($id_usuariocadastro === 0){
			$this->id_usuariocadastro = NULL;
		}else{
			$this->id_usuariocadastro = $id_usuariocadastro;
		}
	}
	
	/**
	 * @param int $st_numero
	 */
	public function setNu_numero($nu_numero) {
		if($nu_numero === 0){
			$this->nu_numero = NULL;
		}else{
			$this->nu_numero = $nu_numero;
		}
	}
	
	/**
	 * @param string $st_orgaoexped
	 */
	public function setSt_orgaoexped($st_orgaoexped) {
		if(trim($st_orgaoexped) == ""){
			$this->st_orgaoexped = NULL;
		}else{
			$this->st_orgaoexped = $st_orgaoexped;
		}
	}

}

?>
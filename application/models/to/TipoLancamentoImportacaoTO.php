<?php
/**
 * Classe de valor da tabela tb_tipolancamentoimportacao
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class TipoLancamentoImportacaoTO extends Ead1_TO_Dinamico {
	
	public $id_produto;
	public $nu_codtipolancamentoorigem;
	public $st_tipolancamentoorigem;
	
	public function setId_produto($idProduto) {
		$this->id_produto	= $idProduto;
	}
	
	public function setNu_codtipolancamentoorigem($nuCodTipoLancamentoOrigem) {
		$this->nu_codtipolancamentoorigem	= $nuCodTipoLancamentoOrigem ;
	}
	public function setSt_tipolancamentoorigem($stTipoLancamentoOrigem) {
		$this->st_tipolancamentoorigem	= $stTipoLancamentoOrigem;
	}

	public function getId_produto() {
		return $this->id_produto;
	}
	
	public function getNu_codtipolancamentoorigem() {
		return $this->nu_codtipolancamentoorigem;
	}
	public function getSt_tipolancamentoorigem() {
		return $this->st_tipolancamentoorigem;
	}
}
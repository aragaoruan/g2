<?php
/**
 * Classe para encapsular os dados da tb_classeafiliado.
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class ClasseAfiliadoTO extends Ead1_TO_Dinamico {
		
	const SITUACAO_ATIVO     = 102;
	const SITUACAO_INATIVO   = 103;
	const SITUACAO_CANCELADO = 104;
	const SITUACAO_PENDENTE  = 106;
	const SITUACAO_RECUSADO  = 107;
	
	public $id_classeafiliado;
	public $id_tipopessoa;
	public $id_tipovalorcomissao;
	public $id_situacao;
	public $id_textosistemacontrato;
	public $id_entidade;
	public $id_usuariocadastro;
	public $st_classeafiliado;
	public $st_descricao;
	public $dt_cadastro;
	/**
	 * @return the $id_classeafiliado
	 */
	public function getId_classeafiliado() {
		return $this->id_classeafiliado;
	}

	/**
	 * @param field_type $id_classeafiliado
	 */
	public function setId_classeafiliado($id_classeafiliado) {
		$this->id_classeafiliado = $id_classeafiliado;
	}

	/**
	 * @return the $id_tipopessoa
	 */
	public function getId_tipopessoa() {
		return $this->id_tipopessoa;
	}

	/**
	 * @param field_type $id_tipopessoa
	 */
	public function setId_tipopessoa($id_tipopessoa) {
		$this->id_tipopessoa = $id_tipopessoa;
	}

	/**
	 * @return the $id_tipovalorcomissao
	 */
	public function getId_tipovalorcomissao() {
		return $this->id_tipovalorcomissao;
	}

	/**
	 * @param field_type $id_tipovalorcomissao
	 */
	public function setId_tipovalorcomissao($id_tipovalorcomissao) {
		$this->id_tipovalorcomissao = $id_tipovalorcomissao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $id_textosistemacontrato
	 */
	public function getId_textosistemacontrato() {
		return $this->id_textosistemacontrato;
	}

	/**
	 * @param field_type $id_textosistemacontrato
	 */
	public function setId_textosistemacontrato($id_textosistemacontrato) {
		$this->id_textosistemacontrato = $id_textosistemacontrato;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $st_classeafiliado
	 */
	public function getSt_classeafiliado() {
		return $this->st_classeafiliado;
	}

	/**
	 * @param field_type $st_classeafiliado
	 */
	public function setSt_classeafiliado($st_classeafiliado) {
		$this->st_classeafiliado = $st_classeafiliado;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	
	
}

?>
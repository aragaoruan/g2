<?php
/**
 * Classe para encapsular os dados das grids de declarações e certificados
 * @author João Gabriel - jgvasconcelos16@gmail.com
 * @package models
 * @subpackage to
 */
class VwGridTO extends Ead1_TO_Dinamico {

	public $id_matricula;
	public $id_projetopedagogico;
	public $id_modulo;
	public $st_modulo;
	public $id_serie;
	public $id_disciplina;
	public $nu_aprovafinal;
	public $nu_cargahoraria;
	public $dt_concluinte;
	public $sg_uf;
	public $st_cidade;
	public $st_disciplina;
	public $st_serie;
	public $st_razaosocial;
	public $id_areaagregadora;
	public $st_areaagregadora;
	public $st_evolucao;
	
	/**
	 * @return unknown
	 */
	public function getDt_concluinte() {
		return $this->dt_concluinte;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_cidade() {
		return $this->st_cidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_razaosocial() {
		return $this->st_razaosocial;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}
	
	/**
	 * @param unknown_type $dt_concluinte
	 */
	public function setDt_concluinte($dt_concluinte) {
		$this->dt_concluinte = $dt_concluinte;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param unknown_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param unknown_type $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param unknown_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}
	
	/**
	 * @param unknown_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}
	
	/**
	 * @param unknown_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param unknown_type $st_cidade
	 */
	public function setSt_cidade($st_cidade) {
		$this->st_cidade = $st_cidade;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param unknown_type $st_razaosocial
	 */
	public function setSt_razaosocial($st_razaosocial) {
		$this->st_razaosocial = $st_razaosocial;
	}
	
	/**
	 * @param unknown_type $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}
	
	/**
	 * @param unknown_type $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}
	/**
	 * @return the $id_areaagregadora
	 */
	public function getId_areaagregadora() {
		return $this->id_areaagregadora;
	}

	/**
	 * @return the $st_areaagregadora
	 */
	public function getSt_areaagregadora() {
		return $this->st_areaagregadora;
	}

	/**
	 * @param field_type $id_areaagregadora
	 */
	public function setId_areaagregadora($id_areaagregadora) {
		$this->id_areaagregadora = $id_areaagregadora;
	}

	/**
	 * @param field_type $st_areaagregadora
	 */
	public function setSt_areaagregadora($st_areaagregadora) {
		$this->st_areaagregadora = $st_areaagregadora;
	}


}

?>
<?php

/**
 * Classe para encapsular da tabela
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class MensagemPadraoTO extends Ead1_TO_Dinamico {

    const MATRICULA = 1;
    const DADOS_ACESSO = 2;
    const ENVIO_BOLETO = 3;
    const ENVIO_CARTAO = 4;
    const CONFIRMACAO_VENDA = 5;
    const COBRANCA = 6;
    const VENDA_EBOOK = 7;
    const LIBERAR_TCC = 8;
    const RECIBO_TCC = 9;
    const DADOS_ACESSO_SMS = 11;
    const AGENDAMENTO = 12;
    const RECIBO_TCC_ALUNO = 17;
    const AVISO_AGENDAMENTO = 18;
    const AVISO_AGENDAMENTO_RECUPERACAO = 19;
    const NOTIFICACAO_AGENDAMENTO_ALUNO = 21;
    const AVISO_VENCIMENTO_CARTAO_RECORRENTE = 25;
    const ALTERACAO_CADASTRO_USUARIO = 31;
    const ENVIO_COD_RASTREAMENTO_CERTIFICADO = 37;
    const ENVIO_CUPOM_PREMIO = 36;
    const ENVIO_EMAIL_CONFIRMACAO_RENOVACAO = 39;
    const ENVIO_CARTAO_ACORDO = 40;
    const AVISO_SOBRE_AGENDAMENTO = 43;
    const DEVOLUCAO_TCC_ALUNO = 45;
    const AVALIACAO_TCC_ALUNO = 46;
    const DEVOLUCAO_TCC_TUTOR = 48;
    const RENOVACAO_NEGOCIACAO_INADIMPLENCIA = 50;
    const ALOCACAO_PRR = 49;
    const AVISO_RECORRENTE_ATRASADO = 54;
    const AVISO_RECORRENTE_PAGO = 56;

    public $id_mensagempadrao;
    public $id_tipoenvio;
    public $st_mensagempadrao;
    public $st_default;

    /**
     * @return the $id_mensagempadrao
     */
    public function getId_mensagempadrao() {
        return $this->id_mensagempadrao;
    }

    /**
     * @return the $id_tipoenvio
     */
    public function getId_tipoenvio() {
        return $this->id_tipoenvio;
    }

    /**
     * @return the $st_mensagempadrao
     */
    public function getSt_mensagempadrao() {
        return $this->st_mensagempadrao;
    }

    /**
     * @return the $st_default
     */
    public function getSt_default() {
        return $this->st_default;
    }

    /**
     * @param field_type $id_mensagempadrao
     */
    public function setId_mensagempadrao($id_mensagempadrao) {
        $this->id_mensagempadrao = $id_mensagempadrao;
    }

    /**
     * @param field_type $id_tipoenvio
     */
    public function setId_tipoenvio($id_tipoenvio) {
        $this->id_tipoenvio = $id_tipoenvio;
    }

    /**
     * @param field_type $st_mensagempadrao
     */
    public function setSt_mensagempadrao($st_mensagempadrao) {
        $this->st_mensagempadrao = $st_mensagempadrao;
    }

    /**
     * @param field_type $st_default
     */
    public function setSt_default($st_default) {
        $this->st_default = $st_default;
    }

}

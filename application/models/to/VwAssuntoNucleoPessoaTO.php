<?php
/**
 * Classe para encapsular os dados de vw_assuntonucleupessoa.
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage to
 */
class VwAssuntoNucleoPessoaTO extends Ead1_TO_Dinamico {

	public $st_nomeentidade;
	public $st_assuntopai;
	public $id_assuntopai;
	public $st_subassunto;
	public $id_subassunto;
	public $id_assunto;
	public $id_entidade;
	public $id_entidadecadastro;
	public $id_usuario;
	public $st_nomecompleto;
	public $st_nucleoco;
	public $st_funcao;
	public $id_nucleopessoaco;
	public $id_nucleoco;
    public $id_funcao;
    public $st_tipoocorrencia;
    public $bl_todos;

    /**
     * @param mixed $bl_todos
     */
    public function setBl_todos($bl_todos)
    {
        $this->bl_todos = $bl_todos;
    }

    /**
     * @return mixed
     */
    public function getBl_todos()
    {
        return $this->bl_todos;
    }

    /**
     * @param mixed $st_tipoocorrencia
     */
    public function setSt_tipoocorrencia($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
    }

    /**
     * @return mixed
     */
    public function getSt_tipoocorrencia()
    {
        return $this->st_tipoocorrencia;
    }

    /**
     * @param mixed $id_funcao
     */
    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    /**
     * @return mixed
     */
    public function getId_funcao()
    {
        return $this->id_funcao;
    }
	
	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @return the $st_assuntopai
	 */
	public function getSt_assuntopai() {
		return $this->st_assuntopai;
	}

	/**
	 * @param field_type $st_assuntopai
	 */
	public function setSt_assuntopai($st_assuntopai) {
		$this->st_assuntopai = $st_assuntopai;
	}

	/**
	 * @return the $id_assuntopai
	 */
	public function getId_assuntopai() {
		return $this->id_assuntopai;
	}

	/**
	 * @param field_type $id_assuntopai
	 */
	public function setId_assuntopai($id_assuntopai) {
		$this->id_assuntopai = $id_assuntopai;
	}

	/**
	 * @return the $st_subassunto
	 */
	public function getSt_subassunto() {
		return $this->st_subassunto;
	}

	/**
	 * @param field_type $st_subassunto
	 */
	public function setSt_subassunto($st_subassunto) {
		$this->st_subassunto = $st_subassunto;
	}

	/**
	 * @return the $id_subassunto
	 */
	public function getId_subassunto() {
		return $this->id_subassunto;
	}

	/**
	 * @param field_type $id_subassunto
	 */
	public function setId_subassunto($id_subassunto) {
		$this->id_subassunto = $id_subassunto;
	}

	/**
	 * @return the $id_assunto
	 */
	public function getId_assunto() {
		return $this->id_assunto;
	}

	/**
	 * @param field_type $id_assunto
	 */
	public function setId_assunto($id_assunto) {
		$this->id_assunto = $id_assunto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $st_nucleoco
	 */
	public function getSt_nucleoco() {
		return $this->st_nucleoco;
	}

	/**
	 * @param field_type $st_nucleoco
	 */
	public function setSt_nucleoco($st_nucleoco) {
		$this->st_nucleoco = $st_nucleoco;
	}

	/**
	 * @return the $st_funcao
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}

	/**
	 * @param field_type $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}

	/**
	 * @return the $id_nucleopessoaco
	 */
	public function getId_nucleopessoaco() {
		return $this->id_nucleopessoaco;
	}

	/**
	 * @param field_type $id_nucleopessoaco
	 */
	public function setId_nucleopessoaco($id_nucleopessoaco) {
		$this->id_nucleopessoaco = $id_nucleopessoaco;
	}

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}

	
	
}

?>
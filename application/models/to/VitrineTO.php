<?php
/**
 * Classe para encapsular os dados da tb_vitrine
 * @author Denise Xavier denise.xavier07@gmail.com
 * @package models
 * @subpackage to
 */
class VitrineTO extends Ead1_TO_Dinamico {

	public $id_vitrine;
	public $st_vitrine;
	public $id_entidadecadastro;
	public $id_modelovitrine;
	public $id_contratoafiliado;
	public $id_usuariocadastro;
	public $bl_ativo;
	
	
	/**
	 * @return the $id_vitrine
	 */
	public function getId_vitrine() {
		return $this->id_vitrine;
	}

	/**
	 * @param field_type $id_vitrine
	 */
	public function setId_vitrine($id_vitrine) {
		$this->id_vitrine = $id_vitrine;
	}

	/**
	 * @return the $st_vitrine
	 */
	public function getSt_vitrine() {
		return $this->st_vitrine;
	}

	/**
	 * @param field_type $st_vitrine
	 */
	public function setSt_vitrine($st_vitrine) {
		$this->st_vitrine = $st_vitrine;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @return the $id_modelovitrine
	 */
	public function getId_modelovitrine() {
		return $this->id_modelovitrine;
	}

	/**
	 * @param field_type $id_modelovitrine
	 */
	public function setId_modelovitrine($id_modelovitrine) {
		$this->id_modelovitrine = $id_modelovitrine;
	}

	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}


	
	
	
}

?>
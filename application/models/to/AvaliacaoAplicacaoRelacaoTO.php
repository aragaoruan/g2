<?php
/**
 * Classe para encapsular os dados da relacao da aplicacao de avaliacao.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class AvaliacaoAplicacaoRelacaoTO extends Ead1_TO_Dinamico{
	
	public $id_avaliacaoaplicacao;
	public $id_avaliacao;
	
	/**
	 * @return the $id_avaliacaoaplicacao
	 */
	public function getId_avaliacaoaplicacao() {
		return $this->id_avaliacaoaplicacao;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @param field_type $id_avaliacaoaplicacao
	 */
	public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
		$this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

}
<?php
/**
 * Classe para encapsular os dados da view de transferência de disciplinas.
 * @author Eder Lamar
 * 
 * @package models
 * @subpackage to
 */
class VwAproveitarAvaliacaoTO extends Ead1_TO_Dinamico{
	
	public $id_disciplina;
	public $id_disciplinaorigem;
	public $id_matricula;
	public $id_matriculaorigem;
	public $id_avaliacao;
	public $st_avaliacao;
	public $st_tituloexibicaodisciplina;
	public $st_tituloexibicaodisciplinaorigem;
	public $st_nota;
	public $st_notaorigem;
	public $id_avaliacaoorigem;
	public $id_avaliacaoalunoorigem;
	public $id_avaliacaoconjuntoreferencia;
	
	/**
	 * @return the $id_avaliacaoorigem
	 */
	public function getId_avaliacaoorigem() {
		return $this->id_avaliacaoorigem;
	}
	
	
	/**
	 * @return the $id_avaliacaoconjuntoreferencia
	 */
	public function getId_avaliacaoconjuntoreferencia() {
		return $this->id_avaliacaoconjuntoreferencia;
	}
	
	/**
	 * @return the $id_avaliacaoalunoorigem
	 */
	public function getId_avaliacaoalunoorigem() {
		return $this->id_avaliacaoalunoorigem;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @return the $id_disciplinaorigem
	 */
	public function getId_disciplinaorigem() {
		return $this->id_disciplinaorigem;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao() {
		return $this->id_avaliacao;
	}

	/**
	 * @return the $st_avaliacao
	 */
	public function getSt_avaliacao() {
		return $this->st_avaliacao;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_matriculaorigem
	 */
	public function getId_matriculaorigem() {
		return $this->id_matriculaorigem;
	}

	/**
	 * @return the $st_nota
	 */
	public function getSt_nota() {
		return $this->st_nota;
	}

	/**
	 * @return the $st_notaorigem
	 */
	public function getSt_notaorigem() {
		return $this->st_notaorigem;
	}

	/**
	 * @return the $st_tituloexibicaodisciplina
	 */
	public function getSt_tituloexibicaodisciplina() {
		return $this->st_tituloexibicaodisciplina;
	}

	/**
	 * @return the $st_tituloexibicaodisciplinaorigem
	 */
	public function getSt_tituloexibicaodisciplinaorigem() {
		return $this->st_tituloexibicaodisciplinaorigem;
	}

	/**
	 * @param $id_avaliacaoorigem the $id_avaliacaoorigem to set
	 */
	public function setId_avaliacaoorigem($id_avaliacaoorigem) {
		$this->id_avaliacaoorigem = $id_avaliacaoorigem;
	}

	/**
	 * @param $id_avaliacaoconjuntoreferencia the $id_avaliacaoconjuntoreferencia to set
	 */
	public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
		$this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
	}

	/**
	 * @param $id_avaliacaoalunoorigem the $id_avaliacaoalunoorigem to set
	 */
	public function setId_avaliacaoalunoorigem($id_avaliacaoalunoorigem) {
		$this->id_avaliacaoalunoorigem = $id_avaliacaoalunoorigem;
	}

	/**
	 * @param $id_disciplina the $id_disciplina to set
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param $id_disciplinaorigem the $id_disciplinaorigem to set
	 */
	public function setId_disciplinaorigem($id_disciplinaorigem) {
		$this->id_disciplinaorigem = $id_disciplinaorigem;
	}

	/**
	 * @param $id_avaliacao the $id_avaliacao to set
	 */
	public function setId_avaliacao($id_avaliacao) {
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param $st_avaliacao the $st_avaliacao to set
	 */
	public function setSt_avaliacao($st_avaliacao) {
		$this->st_avaliacao = $st_avaliacao;
	}

	/**
	 * @param $id_matricula the $id_matricula to set
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param $id_matriculaorigem the $id_matriculaorigem to set
	 */
	public function setId_matriculaorigem($id_matriculaorigem) {
		$this->id_matriculaorigem = $id_matriculaorigem;
	}

	/**
	 * @param $st_nota the $st_nota to set
	 */
	public function setSt_nota($st_nota) {
		$this->st_nota = $st_nota;
	}

	/**
	 * @param $st_nota the $st_notaorigem to set
	 */
	public function setSt_notaorigem($st_notaorigem) {
		$this->st_notaorigem = $st_notaorigem;
	}

	/**
	 * @param $st_tituloexibicaodisciplina the $st_tituloexibicaodisciplina to set
	 */
	public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
		$this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
	}

	/**
	 * @param $st_tituloexibicaodisciplinaorigem the $st_tituloexibicaodisciplinaorigem to set
	 */
	public function setSt_tituloexibicaodisciplinaorigem($st_tituloexibicaodisciplinaorigem) {
		$this->st_tituloexibicaodisciplinaorigem = $st_tituloexibicaodisciplinaorigem;
	}
            
}
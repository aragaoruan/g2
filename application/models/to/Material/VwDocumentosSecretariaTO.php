<?php
/**
 * Classe de transferência de dados da VIEW vw_documentos_secretaria
 * @author Arthur Cláudio de Almeida Pereira - arthur.almeida@ead1.com.br
 * @package to
 * @subpackage Material
 */
class Material_VwDocumentosSecretariaTO	extends Ead1_TO_Dinamico {
	
	public $id_documentos;
	public $bl_digitaliza;
	public $st_documentos;
	public $bl_obrigatorio;
	public $bl_ativo;
	public $nu_quantidade;
	public $id_entidade;
	public $id_usuariocadastro;
	public $id_tipodocumento;
	public $st_tipodocumento;
	public $id_situacao;
	public $st_situacao;
	
	
	/**
	 * @return the $bl_obrigatorio
	 */
	public function getBl_obrigatorio() {
		return $this->bl_obrigatorio;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @param $bl_obrigatorio the $bl_obrigatorio to set
	 */
	public function setBl_obrigatorio($bl_obrigatorio) {
		$this->bl_obrigatorio = $bl_obrigatorio;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @return the $id_documentos
	 */
	public function getId_documentos() {
		return $this->id_documentos;
	}

	/**
	 * @return the $bl_digitaliza
	 */
	public function getBl_digitaliza() {
		return $this->bl_digitaliza;
	}

	/**
	 * @return the $st_documentos
	 */
	public function getSt_documentos() {
		return $this->st_documentos;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $nu_quantidade
	 */
	public function getNu_quantidade() {
		return $this->nu_quantidade;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $idu_usuariocadastro
	 */
	public function getIdu_usuariocadastro() {
		return $this->idu_usuariocadastro;
	}

	/**
	 * @return the $id_tipodocumento
	 */
	public function getId_tipodocumento() {
		return $this->id_tipodocumento;
	}

	/**
	 * @return the $st_tipodocumento
	 */
	public function getSt_tipodocumento() {
		return $this->st_tipodocumento;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $id_documentos the $id_documentos to set
	 */
	public function setId_documentos($id_documentos) {
		$this->id_documentos = $id_documentos;
	}

	/**
	 * @param $bl_digitaliza the $bl_digitaliza to set
	 */
	public function setBl_digitaliza($bl_digitaliza) {
		$this->bl_digitaliza = $bl_digitaliza;
	}

	/**
	 * @param $st_documentos the $st_documentos to set
	 */
	public function setSt_documentos($st_documentos) {
		$this->st_documentos = $st_documentos;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	/**
	 * @param $nu_quantidade the $nu_quantidade to set
	 */
	public function setNu_quantidade($nu_quantidade) {
		$this->nu_quantidade = $nu_quantidade;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $idu_usuariocadastro the $idu_usuariocadastro to set
	 */
	public function setIdu_usuariocadastro($idu_usuariocadastro) {
		$this->idu_usuariocadastro = $idu_usuariocadastro;
	}

	/**
	 * @param $id_tipodocumento the $id_tipodocumento to set
	 */
	public function setId_tipodocumento($id_tipodocumento) {
		$this->id_tipodocumento = $id_tipodocumento;
	}

	/**
	 * @param $st_tipodocumento the $st_tipodocumento to set
	 */
	public function setSt_tipodocumento($st_tipodocumento) {
		$this->st_tipodocumento = $st_tipodocumento;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	
}
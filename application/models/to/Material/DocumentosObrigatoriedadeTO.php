<?php
/**
 * Classe de TO do objeto DocumentosObrigatoriedade 
 * @author Arthur Cláudio de Almeida Pereira - arthur.almeida@ead1.com.br
 * @package to
 * @subpackage Material
 */
class Material_DocumentosObrigatoriedadeTO extends Ead1_TO_Dinamico {

	public $id_documentosobrigatoriedade;
	public $st_documentosobrigatoriedade;
	public $id_usuariocadastro;
	public $bl_ativo;
	
	/**
	 * @return the $id_documentosobrigatoriedade
	 */
	public function getId_documentosobrigatoriedade() {
		return $this->id_documentosobrigatoriedade;
	}

	/**
	 * @return the $st_documentosobrigatoriedade
	 */
	public function getSt_documentosobrigatoriedade() {
		return $this->st_documentosobrigatoriedade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param $id_documentosobrigatoriedade the $id_documentosobrigatoriedade to set
	 */
	public function setId_documentosobrigatoriedade($id_documentosobrigatoriedade) {
		$this->id_documentosobrigatoriedade = $id_documentosobrigatoriedade;
	}

	/**
	 * @param $st_documentosobrigatoriedade the $st_documentosobrigatoriedade to set
	 */
	public function setSt_documentosobrigatoriedade($st_documentosobrigatoriedade) {
		$this->st_documentosobrigatoriedade = $st_documentosobrigatoriedade;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $bl_ativo the $bl_ativo to set
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	
}
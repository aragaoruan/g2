<?php
/**
 * Classe de encapsulamemnto dos atributos da view vw_tipodematerial
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 * @package to
 * @subpackage Material
 */
class Material_VwTipoDeMaterialTO	extends Ead1_TO_Dinamico {
	
	/**
	 * Id da entidade do tipo de material 
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do tipo de material
	 * @var int
	 */
	public $id_tipodematerial;
	
	/**
	 * Nome do tipo de material
	 * @var string
	 */
	public $st_tipodematerial;
	
	/**
	 * Id da situação do tipo de material
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Nome da situação do tipo de material
	 * @var string
	 */
	public $st_situacao;
	
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_tipodematerial
	 */
	public function getId_tipodematerial() {
		return $this->id_tipodematerial;
	}

	/**
	 * @return the $st_tipodematerial
	 */
	public function getSt_tipodematerial() {
		return $this->st_tipodematerial;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_tipodematerial the $id_tipodematerial to set
	 */
	public function setId_tipodematerial($id_tipodematerial) {
		$this->id_tipodematerial = $id_tipodematerial;
	}

	/**
	 * @param $st_tipodematerial the $st_tipodematerial to set
	 */
	public function setSt_tipodematerial($st_tipodematerial) {
		$this->st_tipodematerial = $st_tipodematerial;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	
	
	
	
}
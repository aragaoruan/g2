<?php
/**
 * Classe para encapsular os dados da vw_vitrine
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @package models
 * @subpackage to
 */
class VwProdutoVendaUnicaTO extends Ead1_TO_Dinamico {

	public $id_produto;
	public $st_email;
	public $id_vendaproduto;
	
	/**
	 * @return the $id_vendaproduto
	 */
	public function getId_vendaproduto() {
		return $this->id_vendaproduto;
	}

	/**
	 * @param field_type $id_vendaproduto
	 */
	public function setId_vendaproduto($id_vendaproduto) {
		$this->id_vendaproduto = $id_vendaproduto;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $st_email
	 */
	public function getSt_email() {
		return $this->st_email;
	}

	/**
	 * @param field_type $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param field_type $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}

	
	
	
	
}


<?php
class PesquisarFormaPagamentoXParcelaTO extends Ead1_TO_Dinamico{
	
	//Contem o Valor da venda para calculo das parcelas
	public $nu_valorvenda;
	public $nu_parcelas;
	public $nu_valorparcela;
	public $st_valorparcela;
	//-----
	public $id_formapagamento;
	public $id_entidade;
	public $nu_valorminparcela;
	public $id_formapagamentoaplicacao;
	public $id_meiopagamento;
	public $id_formapagamentoparcela;
	public $nu_parcelaquantidademin;
	public $nu_parcelaquantidademax;
	public $nu_juros;
	
	/**
	 * @return the $nu_valorvenda
	 */
	public function getNu_valorvenda() {
		return $this->nu_valorvenda;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_valorminparcela
	 */
	public function getNu_valorminparcela() {
		return $this->nu_valorminparcela;
	}

	/**
	 * @return the $id_formapagamentoaplicacao
	 */
	public function getId_formapagamentoaplicacao() {
		return $this->id_formapagamentoaplicacao;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $id_formapagamentoparcela
	 */
	public function getId_formapagamentoparcela() {
		return $this->id_formapagamentoparcela;
	}

	/**
	 * @return the $nu_parcelaquantidademin
	 */
	public function getNu_parcelaquantidademin() {
		return $this->nu_parcelaquantidademin;
	}

	/**
	 * @return the $nu_parcelaquantidademax
	 */
	public function getNu_parcelaquantidademax() {
		return $this->nu_parcelaquantidademax;
	}

	/**
	 * @return the $nu_juros
	 */
	public function getNu_juros() {
		return $this->nu_juros;
	}

	/**
	 * @param field_type $nu_valorvenda
	 */
	public function setNu_valorvenda($nu_valorvenda) {
		$this->nu_valorvenda = $nu_valorvenda;
	}

	/**
	 * @param field_type $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $nu_valorminparcela
	 */
	public function setNu_valorminparcela($nu_valorminparcela) {
		$this->nu_valorminparcela = $nu_valorminparcela;
	}

	/**
	 * @param field_type $id_formapagamentoaplicacao
	 */
	public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao) {
		$this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
	}

	/**
	 * @param field_type $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
	}

	/**
	 * @param field_type $id_formapagamentoparcela
	 */
	public function setId_formapagamentoparcela($id_formapagamentoparcela) {
		$this->id_formapagamentoparcela = $id_formapagamentoparcela;
	}

	/**
	 * @param field_type $nu_parcelaquantidademin
	 */
	public function setNu_parcelaquantidademin($nu_parcelaquantidademin) {
		$this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
	}

	/**
	 * @param field_type $nu_parcelaquantidademax
	 */
	public function setNu_parcelaquantidademax($nu_parcelaquantidademax) {
		$this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
	}

	/**
	 * @param field_type $nu_juros
	 */
	public function setNu_juros($nu_juros) {
		$this->nu_juros = $nu_juros;
	}
	/**
	 * @return the $nu_parcelas
	 */
	public function getNu_parcelas() {
		return $this->nu_parcelas;
	}

	/**
	 * @return the $nu_valorparcela
	 */
	public function getNu_valorparcela() {
		return $this->nu_valorparcela;
	}

	/**
	 * @return the $st_valorparcela
	 */
	public function getSt_valorparcela() {
		return $this->st_valorparcela;
	}

	/**
	 * @param field_type $nu_parcelas
	 */
	public function setNu_parcelas($nu_parcelas) {
		$this->nu_parcelas = $nu_parcelas;
	}

	/**
	 * @param field_type $nu_valorparcela
	 */
	public function setNu_valorparcela($nu_valorparcela) {
		$this->nu_valorparcela = $nu_valorparcela;
	}

	/**
	 * @param field_type $st_valorparcela
	 */
	public function setSt_valorparcela($st_valorparcela) {
		$this->st_valorparcela = $st_valorparcela;
	}


	
}
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class EntregaDeclaracaoTO extends Ead1_TO_Dinamico{
	
	const SITUACAO_PENDENTE = 79;
	const SITUACAO_ENTREGUE = 80;

	public $id_entregadeclaracao;
	public $id_matricula;
	public $id_usuariocadastro;
	public $id_situacao;
	public $id_venda;
	public $id_textosistema;
	public $dt_solicitacao;
	public $dt_envio;
	public $dt_entrega;
	public $dt_cadastro;


	/**
	 * @return the $id_entregadeclaracao
	 */
	public function getId_entregadeclaracao(){ 
		return $this->id_entregadeclaracao;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao(){ 
		return $this->id_situacao;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda(){ 
		return $this->id_venda;
	}

	/**
	 * @return the $id_textosistema
	 */
	public function getId_textosistema(){ 
		return $this->id_textosistema;
	}

	/**
	 * @return the $dt_solicitacao
	 */
	public function getDt_solicitacao(){ 
		return $this->dt_solicitacao;
	}

	/**
	 * @return the $dt_envio
	 */
	public function getDt_envio(){ 
		return $this->dt_envio;
	}

	/**
	 * @return the $dt_entrega
	 */
	public function getDt_entrega(){ 
		return $this->dt_entrega;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_entregadeclaracao
	 */
	public function setId_entregadeclaracao($id_entregadeclaracao){ 
		$this->id_entregadeclaracao = $id_entregadeclaracao;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao){ 
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $id_venda
	 */
	public function setId_venda($id_venda){ 
		$this->id_venda = $id_venda;
	}

	/**
	 * @param field_type $id_textosistema
	 */
	public function setId_textosistema($id_textosistema){ 
		$this->id_textosistema = $id_textosistema;
	}

	/**
	 * @param field_type $dt_solicitacao
	 */
	public function setDt_solicitacao($dt_solicitacao){ 
		$this->dt_solicitacao = $dt_solicitacao;
	}

	/**
	 * @param field_type $dt_envio
	 */
	public function setDt_envio($dt_envio){ 
		$this->dt_envio = $dt_envio;
	}

	/**
	 * @param field_type $dt_entrega
	 */
	public function setDt_entrega($dt_entrega){ 
		$this->dt_entrega = $dt_entrega;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

}
<?php
/**
 * Classe para encapsular os dados da view de disciplina com módulo.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwAreaDisciplinaModuloSerieNivelTO extends Ead1_TO_Dinamico {

	public $id_areaconhecimento;
	
	public $st_areaconhecimento;
	
	public $id_disciplina;
	
	public $st_disciplina;
	
	public $st_descricao;
	
	public $id_entidade;
	
	public $bl_ativo;
	
	public $st_nivelensino;
	
	public $id_nivelensino;
	
	public $id_serie;
	
	public $st_serie;
	
	public $id_serieanterior;
	
	public $id_modulo;
	
	public $st_modulo;
	
	public $st_tituloexibicao;
	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_modulo() {
		return $this->id_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_serie() {
		return $this->id_serie;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_serieanterior() {
		return $this->id_serieanterior;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_modulo() {
		return $this->st_modulo;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_serie() {
		return $this->st_serie;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_modulo
	 */
	public function setId_modulo($id_modulo) {
		$this->id_modulo = $id_modulo;
	}
	
	/**
	 * @param unknown_type $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param unknown_type $id_serie
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param unknown_type $id_serieanterior
	 */
	public function setId_serieanterior($id_serieanterior) {
		$this->id_serieanterior = $id_serieanterior;
	}
	
	/**
	 * @param unknown_type $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
	}
	
	/**
	 * @param unknown_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_modulo
	 */
	public function setSt_modulo($st_modulo) {
		$this->st_modulo = $st_modulo;
	}
	
	/**
	 * @param unknown_type $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param unknown_type $st_serie
	 */
	public function setSt_serie($st_serie) {
		$this->st_serie = $st_serie;
	}
	
	/**
	 * @param unknown_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

}

?>
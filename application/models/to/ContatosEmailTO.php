<?php
/**
 * Classe para encapsular os dados de email
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatosEmailTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id do email.
	 * @var int
	 */
	public $id_email;
	
	/**
	 * Atributo que contém o email.
	 * @var string
	 */
	public $st_email;
	
	/**
	 * @return int
	 */
	public function getId_email() {
		return $this->id_email;
	}
	
	/**
	 * @return string
	 */
	public function getSt_email() {
		return $this->st_email;
	}
	
	/**
	 * @param int $id_email
	 */
	public function setId_email($id_email) {
		$this->id_email = $id_email;
	}
	
	/**
	 * @param string $st_email
	 */
	public function setSt_email($st_email) {
		$this->st_email = $st_email;
	}

}

?>
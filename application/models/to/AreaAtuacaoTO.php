<?php
/**
 * Classe para encapsular os dados de área de atuação.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AreaAtuacaoTO extends Ead1_TO_Dinamico {

	/**
	 * Atributo que contém o id da área de atuação.
	 * @var int
	 */
	public $id_areaatuacao;
	
	/**
	 * Atributo que contém a área de atuação.
	 * @var string
	 */
	public $st_areaatuacao;
	
	/**
	 * @return int
	 */
	public function getId_areaatuacao() {
		return $this->id_areaatuacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_areaatuacao() {
		return $this->st_areaatuacao;
	}
	
	/**
	 * @param int $id_areaatuacao
	 */
	public function setId_areaatuacao($id_areaatuacao) {
		$this->id_areaatuacao = $id_areaatuacao;
	}
	
	/**
	 * @param string $st_areaatuacao
	 */
	public function setSt_areaatuacao($st_areaatuacao) {
		$this->st_areaatuacao = $st_areaatuacao;
	}

}

?>
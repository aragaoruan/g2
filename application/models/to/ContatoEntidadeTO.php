<?php
/**
 * Classe para encapsular os dados de contatos para entidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContatoEntidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id de contato da entidade.
	 * @var int
	 */
	public $id_contatoentidade;
	
	/**
	 * Contém o cargo que o contato ocupa na entidade.
	 * @var String
	 */
	public $st_funcao;
	
	/**
	 * Entidade que está sendo cadastrada.
	 * @var int
	 */
	public $id_entidadecontato;
	
	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @return int
	 */
	public function getId_contatoentidade() {
		return $this->id_contatoentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return String
	 */
	public function getSt_funcao() {
		return $this->st_funcao;
	}
	
	/**
	 * @param int $id_contatoentidade
	 */
	public function setId_contatoentidade($id_contatoentidade) {
		$this->id_contatoentidade = $id_contatoentidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param String $st_funcao
	 */
	public function setSt_funcao($st_funcao) {
		$this->st_funcao = $st_funcao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidadecontato() {
		return $this->id_entidadecontato;
	}
	
	/**
	 * @param int $id_entidadecontato
	 */
	public function setId_entidadecontato($id_entidadecontato) {
		$this->id_entidadecontato = $id_entidadecontato;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados da view de gerar declaração
 * @author Yannick Naquis Roulé
 * @since 27/11/2013
 * 
 * @package models
 * @subpackage to
 */
class VwGerarDeclaracaoImpostoRendaTO extends Ead1_TO_Dinamico {

    public $id_usuario;
    public $id_endereco;
    public $st_cpf;
    public $st_nomecompleto;
    public $st_endereco;
    public $nu_numero;
    public $st_bairro;
    public $st_cidade;
    public $st_cep;
    public $st_nomeentidade;
    public $id_matricula;
    public $id_lancamento;
    public $id_venda;
    public $st_ano;
    public $dt_atual;

    public function getDt_atual() {
        return $this->dt_atual;
    }

    public function setDt_atual($dt_atual) {
        $this->dt_atual = $dt_atual;
    }

    public function getSt_ano() {
        return $this->st_ano;
    }

    public function setSt_ano($st_ano) {
        $this->st_ano = $st_ano;
    }
    
    public function getId_venda() {
        return $this->id_venda;
    }

    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

        public function getId_lancamento() {
        return $this->id_lancamento;
    }

    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    
    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }
    
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function getSt_endereco() {
        return $this->st_endereco;
    }

    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    public function getNu_numero() {
        return $this->nu_numero;
    }

    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
    }

    public function getSt_bairro() {
        return $this->st_bairro;
    }

    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
    }

    public function getSt_cidade() {
        return $this->st_cidade;
    }

    public function setSt_cidade($st_cidade) {
        $this->st_cidade = $st_cidade;
    }

    public function getSt_cep() {
        return $this->st_cep;
    }

    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }
    
    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

}

?>
<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class AproveitamentoMatriculaDisciplinaTO extends Ead1_TO_Dinamico{

	public $id_aproveitamentodisciplina;
	public $id_matricula;
	public $id_disciplina;
	public $st_notaoriginal;
	public $st_cargahoraria;
	public $nu_anoconclusao;
	public $id_apmatriculadisciplina;
	public $st_disciplinaoriginal;
	public $id_serie;
	public $nu_aproveitamento;


	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_aproveitamento(){ 
		return $this->nu_aproveitamento;
	}

	/**
	 * @return the $id_aproveitamentodisciplina
	 */
	public function getId_aproveitamentodisciplina(){ 
		return $this->id_aproveitamentodisciplina;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula(){ 
		return $this->id_matricula;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina(){ 
		return $this->id_disciplina;
	}

	/**
	 * @return the $st_notaoriginal
	 */
	public function getSt_notaoriginal(){ 
		return $this->st_notaoriginal;
	}

	/**
	 * @return the $st_cargahoraria
	 */
	public function getSt_cargahoraria(){ 
		return $this->st_cargahoraria;
	}
	
	/**
	 * @return the $nu_anoconclusao
	 */
	public function getNu_anoconclusao(){ 
		return $this->nu_anoconclusao;
	}
	
	/**
	 * @return the $id_apmatriculadisciplina
	 */
	public function getId_apmatriculadisciplina(){ 
		return $this->id_apmatriculadisciplina;
	}
	
	/**
	 * @return the $st_disciplinaoriginal
	 */
	public function getSt_disciplinaoriginal(){ 
		return $this->st_disciplinaoriginal;
	}
	
	/**
	 * @return the $id_serie
	 */
	public function getId_serie(){ 
		return $this->id_serie;
	}


	/**
	 * @param field_type $id_aproveitamentodisciplina
	 */
	public function setId_aproveitamentodisciplina($id_aproveitamentodisciplina){ 
		$this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula){ 
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina){ 
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @param field_type $st_notaoriginal
	 */
	public function setSt_notaoriginal($st_notaoriginal){ 
		$this->st_notaoriginal = $st_notaoriginal;
	}

	/**
	 * @param field_type $st_cargahoraria
	 */
	public function setSt_cargahoraria($st_cargahoraria){ 
		$this->st_cargahoraria = $st_cargahoraria;
	}

	/**
	 * @param field_type $nu_anoconclusao
	 */
	public function setNu_anoconclusao($nu_anoconclusao){ 
		$this->nu_anoconclusao = $nu_anoconclusao;
	}

	/**
	 * @param field_type $id_apmatriculadisciplina
	 */
	public function setId_apmatriculadisciplina($id_apmatriculadisciplina){ 
		$this->id_apmatriculadisciplina = $id_apmatriculadisciplina;
	}

	/**
	 * @param field_type $st_disciplinaoriginal
	 */
	public function setSt_disciplinaoriginal($st_disciplinaoriginal){ 
		$this->st_disciplinaoriginal = $st_disciplinaoriginal;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie){ 
		$this->id_serie = $id_serie;
	}

	/**
	 * @param field_type $nu_aproveitamento
	 */
	public function setNu_aproveitamento($nu_aproveitamento){ 
		$this->nu_aproveitamento = $nu_aproveitamento;
	}

}
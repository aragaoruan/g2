<?php
/** 
 * Classe para encapsular os dados da tabela de alocacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class LancamentoIntegracaoTO extends Ead1_TO_Dinamico{
	
	public $id_lancamentointegracao;     
	public $id_lancamento;     
	public $id_sistema;     
	public $id_entidade;     
	public $id_usuariocadastro;     
	public $st_codlancamento;     
	public $st_codresponsavel;     
	public $dt_cadastro;
	public $dt_sincronizado;
	
	/**
	 * @return the $id_lancamentointegracao
	 */
	public function getId_lancamentointegracao() {
		return $this->id_lancamentointegracao;
	}

	/**
	 * @return the $id_lancamento
	 */
	public function getId_lancamento() {
		return $this->id_lancamento;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_codlancamento
	 */
	public function getSt_codlancamento() {
		return $this->st_codlancamento;
	}

	/**
	 * @return the $st_codresponsavel
	 */
	public function getSt_codresponsavel() {
		return $this->st_codresponsavel;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param $id_lancamentointegracao the $id_lancamentointegracao to set
	 */
	public function setId_lancamentointegracao($id_lancamentointegracao) {
		$this->id_lancamentointegracao = $id_lancamentointegracao;
	}

	/**
	 * @param $id_lancamento the $id_lancamento to set
	 */
	public function setId_lancamento($id_lancamento) {
		$this->id_lancamento = $id_lancamento;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $id_usuariocadastro the $id_usuariocadastro to set
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param $st_codlancamento the $st_codlancamento to set
	 */
	public function setSt_codlancamento($st_codlancamento) {
		$this->st_codlancamento = $st_codlancamento;
	}

	/**
	 * @param $st_codresponsavel the $st_codresponsavel to set
	 */
	public function setSt_codresponsavel($st_codresponsavel) {
		$this->st_codresponsavel = $st_codresponsavel;
	}

	/**
	 * @param $dt_cadastro the $dt_cadastro to set
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	/**
	 * @return the $dt_sincronizado
	 */
	public function getDt_sincronizado() {
		return $this->dt_sincronizado;
	}

	/**
	 * @param field_type $dt_sincronizado
	 */
	public function setDt_sincronizado($dt_sincronizado) {
		$this->dt_sincronizado = $dt_sincronizado;
	}



	
}
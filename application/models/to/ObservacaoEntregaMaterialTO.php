<?php
/**
 * Classe para encapsular os dados de observacao da entrega de material.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class ObservacaoEntregaMaterialTO extends Ead1_TO_Dinamico{
	
	public $id_observacao;
	public $id_entregamaterial;
	
	/**
	 * @return the $id_observacao
	 */
	public function getId_observacao() {
		return $this->id_observacao;
	}

	/**
	 * @return the $id_entregamaterial
	 */
	public function getId_entregamaterial() {
		return $this->id_entregamaterial;
	}

	/**
	 * @param field_type $id_observacao
	 */
	public function setId_observacao($id_observacao) {
		$this->id_observacao = $id_observacao;
	}

	/**
	 * @param field_type $id_entregamaterial
	 */
	public function setId_entregamaterial($id_entregamaterial) {
		$this->id_entregamaterial = $id_entregamaterial;
	}

	
}
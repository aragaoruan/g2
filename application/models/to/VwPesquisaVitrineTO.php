<?php
/**
 * Classe para encapsular os dados da view de vitrine
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage to
 */
class VwPesquisaVitrineTO extends Ead1_TO_Dinamico{
	
	public $id_contratoafiliado;
	public $st_contratoafiliado;
	public $id_entidadeafiliada;
	public $id_entidadecadastro;
	public $id_usuarioresponsavel;
	public $dt_cadastro;
	public $st_url;
	public $st_descricao;
	public $id_tipopessoa;
	public $id_situacao;
	public $st_classeafiliado;
	public $st_nomeentidade;
	public $st_nomecompleto;
	public $st_situacao;
	public $st_tipopessoa;
	
	/**
	 * @return the $id_contratoafiliado
	 */
	public function getId_contratoafiliado() {
		return $this->id_contratoafiliado;
	}

	/**
	 * @return the $st_contratoafiliado
	 */
	public function getSt_contratoafiliado() {
		return $this->st_contratoafiliado;
	}

	/**
	 * @return the $id_entidadeafiliada
	 */
	public function getId_entidadeafiliada() {
		return $this->id_entidadeafiliada;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_usuarioresponsavel
	 */
	public function getId_usuarioresponsavel() {
		return $this->id_usuarioresponsavel;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $st_url
	 */
	public function getSt_url() {
		return $this->st_url;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @return the $id_tipopessoa
	 */
	public function getId_tipopessoa() {
		return $this->id_tipopessoa;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_classeafiliado
	 */
	public function getSt_classeafiliado() {
		return $this->st_classeafiliado;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_tipopessoa
	 */
	public function getSt_tipopessoa() {
		return $this->st_tipopessoa;
	}

	/**
	 * @param field_type $id_contratoafiliado
	 */
	public function setId_contratoafiliado($id_contratoafiliado) {
		$this->id_contratoafiliado = $id_contratoafiliado;
	}

	/**
	 * @param field_type $st_contratoafiliado
	 */
	public function setSt_contratoafiliado($st_contratoafiliado) {
		$this->st_contratoafiliado = $st_contratoafiliado;
	}

	/**
	 * @param field_type $id_entidadeafiliada
	 */
	public function setId_entidadeafiliada($id_entidadeafiliada) {
		$this->id_entidadeafiliada = $id_entidadeafiliada;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}

	/**
	 * @param field_type $id_usuarioresponsavel
	 */
	public function setId_usuarioresponsavel($id_usuarioresponsavel) {
		$this->id_usuarioresponsavel = $id_usuarioresponsavel;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $st_url
	 */
	public function setSt_url($st_url) {
		$this->st_url = $st_url;
	}

	/**
	 * @param field_type $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @param field_type $id_tipopessoa
	 */
	public function setId_tipopessoa($id_tipopessoa) {
		$this->id_tipopessoa = $id_tipopessoa;
	}

	/**
	 * @param field_type $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param field_type $st_classeafiliado
	 */
	public function setSt_classeafiliado($st_classeafiliado) {
		$this->st_classeafiliado = $st_classeafiliado;
	}

	/**
	 * @param field_type $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param field_type $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param field_type $st_tipopessoa
	 */
	public function setSt_tipopessoa($st_tipopessoa) {
		$this->st_tipopessoa = $st_tipopessoa;
	}
}
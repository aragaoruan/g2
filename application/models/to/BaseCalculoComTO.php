<?php
/**
 * Classe para encapsular os dados de base de calculo da comissao
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class BaseCalculoComTO extends Ead1_TO_Dinamico {
	
	public $id_basecalculocom;
	public $st_basecalculocom;
	
	/**
	 * @return unknown
	 */
	public function getId_basecalculocom() {
		return $this->id_basecalculocom;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_basecalculocom() {
		return $this->st_basecalculocom;
	}
	
	/**
	 * @param unknown_type $id_basecalculocom
	 */
	public function setId_basecalculocom($id_basecalculocom) {
		$this->id_basecalculocom = $id_basecalculocom;
	}
	
	/**
	 * @param unknown_type $st_basecalculocom
	 */
	public function setSt_basecalculocom($st_basecalculocom) {
		$this->st_basecalculocom = $st_basecalculocom;
	}
}
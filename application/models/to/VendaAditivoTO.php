<?php

/**
 * Classe para encapsular os dados de venda aditivo.
 * @author Rafael Leite - rafael.leite@unyleya.com.br
 *
 * @package models
 * @subpackage to
 */
class VendaAditivoTO extends Ead1_TO_Dinamico
{
    public $id_vendaaditivo;
    public $id_venda;
    public $id_matricula;
    public $id_usuario;
    public $dt_cadastro;
    public $nu_valorcursoorigem;
    public $nu_desconto;
    public $nu_valorapagar;
    public $id_meiopagamento;
    public $id_campanhacomercial;
    public $nu_quantidadeparcelas;
    public $nu_valortransferencia;
    public $dt_primeiraparcela;
    public $id_atendente;
    public $st_observacao;
    public $bl_cancelamento;
    public $nu_valorproporcional;


    /**
     * @return int
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return dateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getNu_valorcursoorigem()
    {
        return $this->nu_valorcursoorigem;
    }

    public function setNu_valorcursoorigem($nu_valorcursoorigem)
    {
        $this->nu_valorcursoorigem = $nu_valorcursoorigem;
        return $this;
    }


    /**
     * @return decimal
     */
    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param decimal $nu_desconto
     */
    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return decimal
     */
    public function getNu_valorapagar()
    {
        return $this->nu_valorapagar;
    }

    /**
     * @param decimal $nu_valorapagar
     */
    public function setNu_valorapagar($nu_valorapagar)
    {
        $this->nu_valorapagar = $nu_valorapagar;
    }

    /**
     * @return int
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param int $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return int
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @return int
     */
    public function getNu_quantidadeparcelas()
    {
        return $this->nu_quantidadeparcelas;
    }

    /**
     * @param int $nu_quantidadeparcelas
     */
    public function setNu_quantidadeparcelas($nu_quantidadeparcelas)
    {
        $this->nu_quantidadeparcelas = $nu_quantidadeparcelas;
    }

    /**
     * @return decimal
     */
    public function getNu_valortransferencia()
    {
        return $this->nu_valortransferencia;
    }

    /**
     * @param decimal $nu_valortransferencia
     */
    public function setNu_valortransferencia($nu_valortransferencia)
    {
        $this->nu_valortransferencia = $nu_valortransferencia;
    }

    /**
     * @return dateTime
     */
    public function getDt_primeiraparcela()
    {
        return $this->dt_primeiraparcela;
    }

    /**
     * @param dateTime $dt_primeiraparcela
     */
    public function setDt_primeiraparcela($dt_primeiraparcela)
    {
        $this->dt_primeiraparcela = $dt_primeiraparcela;
    }

    /**
     * @return int
     */
    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @param int $id_atendente
     */
    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param string $st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
    }

    /**
     * @return mixed
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param mixed $bl_cancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }


    /**
     * @return decimal
     */
    public function getNu_valorproporcional()
    {
        return $this->nu_valorproporcional;
    }

    /**
     * @param decimal $nu_valorproporcional
     */
    public function setNu_valorproporcional($nu_valorproporcional)
    {
        $this->nu_valorproporcional = $nu_valorproporcional;
    }


}

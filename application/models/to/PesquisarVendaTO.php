<?php

class PesquisarVendaTO extends Ead1_TO_Pesquisar {

	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.financeiro.cadastrar.venda.CadastrarVenda';
	
	/**
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * @var boolean
	 */
	public $bl_contrato;
	
	/**
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * @var int 
	 */
	public $id_situacao;
	
	/**
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * @var int
	 */
	public $id_evolucao;
	
	/**
	 * @var string
	 */
	public $st_evolucao;
	
	/**
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * @var string 
	 */
	public $st_cpf;
	
	public $nu_valorliquido;
	
	public $nu_valorbruto;
	
	/**
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * @var Zend_Date
	 */
	public $dt_cadastro;
	
	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param Zend_Date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	
	/**
	 * @return boolean
	 */
	public function getBl_contrato() {
		return $this->bl_contrato;
	}
	
	/**
	 * @return int
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return int
	 */
	public function getId_venda() {
		return $this->id_venda;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valorbruto() {
		return $this->nu_valorbruto;
	}
	
	/**
	 * @return unknown
	 */
	public function getNu_valorliquido() {
		return $this->nu_valorliquido;
	}
	
	/**
	 * @return string
	 */
	public function getSt_cpf() {
		return $this->st_cpf;
	}
	
	/**
	 * @return string
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return string
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}
	
	/**
	 * @param boolean $bl_contrato
	 */
	public function setBl_contrato($bl_contrato) {
		$this->bl_contrato = $bl_contrato;
	}
	
	/**
	 * @param int $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param int $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}
	
	/**
	 * @param unknown_type $nu_valorbruto
	 */
	public function setNu_valorbruto($nu_valorbruto) {
		$this->nu_valorbruto = $nu_valorbruto;
	}
	
	/**
	 * @param unknown_type $nu_valorliquido
	 */
	public function setNu_valorliquido($nu_valorliquido) {
		$this->nu_valorliquido = $nu_valorliquido;
	}
	
	/**
	 * @param string $nu_cpf
	 */
	public function setSt_cpf($st_cpf) {
		$this->st_cpf = $st_cpf;
	}
	
	/**
	 * @param string $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}
	
	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

}

?>
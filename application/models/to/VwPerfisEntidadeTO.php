<?php
/**
 * Classe para encapsular os dados da view dos perfis da entidade
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwPerfisEntidadeTO extends Ead1_TO_Dinamico {
	/**
	 * Contém o id da entidade que efetuou o cadastro do perfil
	 * @var unknown_type
	 */
	public $id_entidadecadastro;
	
	/**
	 * Contem o id do perfil
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Contem o nome do perfil
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * Nome do perfil na entidade.
	 * @var string
	 */
	public $st_nomeperfilentidade;
	
	/**
	 * Contem o id da situacao do perfil
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Contem o nome da situacao
	 * @var string
	 */
	public $st_situacao;
	
	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contem o nome da entidade
	 * @var string
	 */
	public $st_nomentidade;
	
	/**
	 * Contem o id do sistema
	 * @var int
	 */
	public $id_sistema;
	
	/**
	 * Contem o id do perfil pedagógico
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * Contem o nome do sistema
	 * @var strong
	 */
	public $st_sistema;
	
	/**
	 * Contem o id da classe da entidade
	 * @var int
	 */
	public $id_entidadeclasse;
	
	/**
	 * Contem o nome da classe da entidade
	 * @var string
	 */
	public $st_entidadeclasse;
	
	/**
	 * Contem o valor que indica se é padrao
	 * @var boolean
	 */
	public $bl_padrao;
	
	/**
	 * Indica se o perfil foi excluído logicamento.
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Nome do perfil pedagogico
	 * @var boolean
	 */
	public $st_perfilpedagogico;
	
	
	/**
	 * @param boolean $st_perfilpedagogico
	 */
	public function setSt_perfilpedagogico($st_perfilpedagogico) {
		$this->st_perfilpedagogico = $st_perfilpedagogico;
	}

	/**
	 * @return boolean
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}

	
	/**
	 * @return string
	 */
	public function getSt_nomeperfilentidade() {
		return $this->st_nomeperfilentidade;
	}
	
	/**
	 * @param string $st_nomeperfilentidade
	 */
	public function setSt_nomeperfilentidade($st_nomeperfilentidade) {
		$this->st_nomeperfilentidade = $st_nomeperfilentidade;
	}
	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomentidade
	 */
	public function getSt_nomentidade() {
		return $this->st_nomentidade;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @return the $st_sistema
	 */
	public function getSt_sistema() {
		return $this->st_sistema;
	}

	/**
	 * @return the $id_entidadeclasse
	 */
	public function getId_entidadeclasse() {
		return $this->id_entidadeclasse;
	}

	/**
	 * @return the $st_entidadeclasse
	 */
	public function getSt_entidadeclasse() {
		return $this->st_entidadeclasse;
	}
	
	/**
	 * @return the $st_perfilpedagogico
	 */
	public function getSt_perfilpedagogico() {
		return $this->st_perfilpedagogico;
	}

	/**
	 * @param $id_perfil the $id_perfil to set
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param $st_nomeperfil the $st_nomeperfil to set
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $st_situacao the $st_situacao to set
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param $st_nomentidade the $st_nomentidade to set
	 */
	public function setSt_nomentidade($st_nomentidade) {
		$this->st_nomentidade = $st_nomentidade;
	}

	/**
	 * @param $id_sistema the $id_sistema to set
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param $st_sistema the $st_sistema to set
	 */
	public function setSt_sistema($st_sistema) {
		$this->st_sistema = $st_sistema;
	}

	/**
	 * @param $id_entidadeclasse the $id_entidadeclasse to set
	 */
	public function setId_entidadeclasse($id_entidadeclasse) {
		$this->id_entidadeclasse = $id_entidadeclasse;
	}

	/**
	 * @param $st_entidadeclasse the $st_entidadeclasse to set
	 */
	public function setSt_entidadeclasse($st_entidadeclasse) {
		$this->st_entidadeclasse = $st_entidadeclasse;
	}
	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @param boolean $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}
	/**
	 * @return the $id_perfilpedagogico
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}

	/**
	 * @param $id_perfilpedagogico the $id_perfilpedagogico to set
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}
	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @param field_type $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
	}




	
}
<?php
/**
 * Classe para encapsular os dados de Tipo de Lancamento.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class TipoLancamentoTO extends Ead1_TO_Dinamico {

	const CREDITO = 1;
	const LANCAMENTO = 2;
	
	/**
	 * Contem o id do tipo de lancamento
	 * @var int
	 */
	public $id_tipolancamento;
	
	/**
	 * Contem o nome do tipo de lancamento
	 * @var string
	 */
	public $st_tipolancamento;
	/**
	 * @return the $id_tipolancamento
	 */
	public function getId_tipolancamento() {
		return $this->id_tipolancamento;
	}

	/**
	 * @return the $st_tipolancamento
	 */
	public function getSt_tipolancamento() {
		return $this->st_tipolancamento;
	}

	/**
	 * @param int $id_tipolancamento
	 */
	public function setId_tipolancamento($id_tipolancamento) {
		$this->id_tipolancamento = $id_tipolancamento;
	}

	/**
	 * @param string $st_tipolancamento
	 */
	public function setSt_tipolancamento($st_tipolancamento) {
		$this->st_tipolancamento = $st_tipolancamento;
	}

	
	
}
<?php

/**
 * Classe para encapsular os dados de tipo da vw_encerramentoalunos
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwEncerramentoAlunosTO extends Ead1_TO_Dinamico {

	const TODOS = 0;
	const NAO_REPASSADO = 1;
	const COORDENADOR = 2;
	const PEDAGOGICO = 3;
	const PAGAMENTO = 4;
	const PAGOS = 5;
    const PAGAMENTO_RECUSADO = 6;


	public $id_alocacao;
	public $id_saladeaula;
	public $id_usuario;
	public $id_matricula;
	public $id_disciplina;
	public $id_matriculadisciplina;
	public $st_nota;
	public $st_nomecompleto;
	public $id_logacesso;
	public $id_usuarioprofessor;
	public $dt_encerramentoprofessor;
	public $id_usuariocoordenador;
	public $dt_encerramentocoordenador;
	public $id_usuariopedagogico;
	public $dt_encerramentopedagogico;
	public $id_usuariofinanceiro;
	public $dt_encerramentofinanceiro;
	public $id_sistema;
	public $id_entidade;
	public $nu_perfilalunoencerrado;
	public $id_tipodisciplina;
	public $id_upload;
	public $nu_cargahoraria;
    public $dt_recusa;
    public $st_motivorecusa;
    public $id_tiponota;
    public $id_situacaotcc;

	/**
	 * @return the $id_tipodisciplina
	 */
	public function getId_tipodisciplina() {
		return $this->id_tipodisciplina;
	}

	/**
	 * @return the $id_upload
	 */
	public function getId_upload() {
		return $this->id_upload;
	}

	/**
	 * @param field_type $id_tipodisciplina
	 */
	public function setId_tipodisciplina($id_tipodisciplina) {
		$this->id_tipodisciplina = $id_tipodisciplina;
	}

	/**
	 * @param field_type $id_upload
	 */
	public function setId_upload($id_upload) {
		$this->id_upload = $id_upload;
	}

	/**
	 * @return the $id_alocacao
	 */
	public function getId_alocacao() {
		return $this->id_alocacao;
	}

	/**
	 * @param field_type $id_alocacao
	 */
	public function setId_alocacao($id_alocacao) {
		$this->id_alocacao = $id_alocacao;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_disciplina
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}

	/**
	 * @param field_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

	/**
	 * @return the $id_matriculadisciplina
	 */
	public function getId_matriculadisciplina() {
		return $this->id_matriculadisciplina;
	}

	/**
	 * @param field_type $id_matriculadisciplina
	 */
	public function setId_matriculadisciplina($id_matriculadisciplina) {
		$this->id_matriculadisciplina = $id_matriculadisciplina;
	}

	/**
	 * @return the $st_nota
	 */
	public function getSt_nota() {

		if($this->st_nota){
			$this->st_nota = number_format($this->st_nota, 2, '.', '');
		}

		return $this->st_nota;
	}

	/**
	 * @param field_type $st_nota
	 */
	public function setSt_nota($st_nota) {
		$this->st_nota = $st_nota;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @param field_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @return the $id_logacesso
	 */
	public function getId_logacesso() {
		return $this->id_logacesso;
	}

	/**
	 * @param field_type $id_logacesso
	 */
	public function setId_logacesso($id_logacesso) {
		$this->id_logacesso = $id_logacesso;
	}

	/**
	 * @return the $id_usuarioprofessor
	 */
	public function getId_usuarioprofessor() {
		return $this->id_usuarioprofessor;
	}

	/**
	 * @param field_type $id_usuarioprofessor
	 */
	public function setId_usuarioprofessor($id_usuarioprofessor) {
		$this->id_usuarioprofessor = $id_usuarioprofessor;
	}

	/**
	 * @return the $dt_encerramentoprofessor
	 */
	public function getDt_encerramentoprofessor() {
		return $this->dt_encerramentoprofessor;
	}

	/**
	 * @param field_type $dt_encerramentoprofessor
	 */
	public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
		$this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
	}

	/**
	 * @return the $id_usuariocoordenador
	 */
	public function getId_usuariocoordenador() {
		return $this->id_usuariocoordenador;
	}

	/**
	 * @param field_type $id_usuariocoordenador
	 */
	public function setId_usuariocoordenador($id_usuariocoordenador) {
		$this->id_usuariocoordenador = $id_usuariocoordenador;
	}

	/**
	 * @return the $dt_encerramentocoordenador
	 */
	public function getDt_encerramentocoordenador() {
		return $this->dt_encerramentocoordenador;
	}

	/**
	 * @param field_type $dt_encerramentocoordenador
	 */
	public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
		$this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
	}

	/**
	 * @return the $id_usuariopedagogico
	 */
	public function getId_usuariopedagogico() {
		return $this->id_usuariopedagogico;
	}

	/**
	 * @param field_type $id_usuariopedagogico
	 */
	public function setId_usuariopedagogico($id_usuariopedagogico) {
		$this->id_usuariopedagogico = $id_usuariopedagogico;
	}

	/**
	 * @return the $dt_encerramentopedagogico
	 */
	public function getDt_encerramentopedagogico() {
		return $this->dt_encerramentopedagogico;
	}

	/**
	 * @param field_type $dt_encerramentopedagogico
	 */
	public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
		$this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
	}

	/**
	 * @return the $id_usuariofinanceiro
	 */
	public function getId_usuariofinanceiro() {
		return $this->id_usuariofinanceiro;
	}

	/**
	 * @param field_type $id_usuariofinanceiro
	 */
	public function setId_usuariofinanceiro($id_usuariofinanceiro) {
		$this->id_usuariofinanceiro = $id_usuariofinanceiro;
	}

	/**
	 * @return the $dt_encerramentofinanceiro
	 */
	public function getDt_encerramentofinanceiro() {
		return $this->dt_encerramentofinanceiro;
	}

	/**
	 * @param field_type $dt_encerramentofinanceiro
	 */
	public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
		$this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema() {
		return $this->id_sistema;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema) {
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $nu_perfilalunoencerrado
	 */
	public function getNu_perfilalunoencerrado() {
		return $this->nu_perfilalunoencerrado;
	}

	/**
	 * @param field_type $nu_perfilalunoencerrado
	 */
	public function setNu_perfilalunoencerrado($nu_perfilalunoencerrado) {
		$this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
	}
	/**
	 * @return the $nu_cargahoraria
	 */
	public function getNu_cargahoraria() {
		return $this->nu_cargahoraria;
	}

	/**
	 * @param field_type $nu_cargahoraria
	 */
	public function setNu_cargahoraria($nu_cargahoraria) {
		$this->nu_cargahoraria = $nu_cargahoraria;
	}

	public function setDt_recusa($dt_recusa)
	{
		$this->dt_recusa = $dt_recusa;
	}

	public function getDt_recusa()
	{
		return $this->dt_recusa;
	}


	public function setSt_motivorecusa($st_motivorecusa)
	{
		$this->st_motivorecusa = $st_motivorecusa;
	}

	public function getSt_motivorecusa()
	{
		return $this->st_motivorecusa;
	}

	public function setId_tiponota($id_tiponota)
	{
		$this->id_tiponota = $id_tiponota;
	}

	public function getId_tiponota()
	{
		return $this->id_tiponota;
	}

	/**
	 * @return mixed
	 */
	public function getId_situacaotcc()
	{
		return $this->id_situacaotcc;
	}

	/**
	 * @param mixed $id_situacaotcc
	 */
	public function setId_situacaotcc($id_situacaotcc)
	{
		$this->id_situacaotcc = $id_situacaotcc;
	}

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }


}

?>

<?php
/**
 * Classe para encapsular os dados de protocólo.
 * @author edermariano
 * 
 * @package models
 * @subpackage to
 */
class ProtocoloTO extends Ead1_TO_Dinamico {

	public $id_protocolo;
	
	public $id_entidade;
	
	public $id_usuariocadastro;
	
	public $dt_cadastro;
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_protocolo() {
		return $this->id_protocolo;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param unknown_type $id_protocolo
	 */
	public function setId_protocolo($id_protocolo) {
		$this->id_protocolo = $id_protocolo;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

}

?>
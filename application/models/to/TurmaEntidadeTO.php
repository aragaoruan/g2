<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class TurmaEntidadeTO extends Ead1_TO_Dinamico{

	public $id_turmaentidade;
	public $id_usuariocadastro;
	public $id_turma;
	public $id_entidade;
	public $dt_cadastro;
	public $bl_ativo;


	/**
	 * @return the $id_turmaentidade
	 */
	public function getId_turmaentidade(){ 
		return $this->id_turmaentidade;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_turma
	 */
	public function getId_turma(){ 
		return $this->id_turma;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade(){ 
		return $this->id_entidade;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo(){ 
		return $this->bl_ativo;
	}


	/**
	 * @param field_type $id_turmaentidade
	 */
	public function setId_turmaentidade($id_turmaentidade){ 
		$this->id_turmaentidade = $id_turmaentidade;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $id_turma
	 */
	public function setId_turma($id_turma){ 
		$this->id_turma = $id_turma;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade){ 
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

	/**
	 * @param field_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo){ 
		$this->bl_ativo = $bl_ativo;
	}

}
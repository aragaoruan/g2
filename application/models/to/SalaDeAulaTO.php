<?php
/**
 * Classe para encapsular os dados de sala de aula.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class SalaDeAulaTO extends Ead1_TO_Dinamico {

    const SALA_NORMAL = 1;
    const SALA_PRR = 2;

	/**
	 * Id da sala de aula.
	 * @var int
	 */
	public $id_saladeaula;
	
	public $nu_diasextensao;
	/**
	 * Nome da sala de aula.
	 * @var string
	 */
	public $st_saladeaula;
	
	/**
	 * Data de cadastro da sala de aula.
	 * @var string
	 */
	public $dt_cadastro;
	
	/**
	 * Exclusão lógica.
	 * @var boolean
	 */
	public $bl_ativa;
	
	/**
	 * Id do usuário que cadastrou a sala de aula.
	 * @var int
	 */
	public $id_usuariocadastro;
	
	/**
	 * Id da modalidade da sala de aula.
	 * @var int
	 */
	public $id_modalidadesaladeaula;
	
	/**
	 * Data de início da inscrição de sala de aula.
	 * @var string
	 */
	public $dt_inicioinscricao;
	
	/**
	 * Data de fim da inscrição da sala de aula.
	 * @var string
	 */
	public $dt_fiminscricao;
	
	/**
	 * Data de abertura da sala de aula.
	 * @var string
	 */
	public $dt_abertura;
	
	/**
	 * Data de encerramento da sala de aula.
	 * @var string
	 */
	public $dt_encerramento;
	
	/**
	 * Localização física da sala de aula.
	 * @var string
	 */
	public $st_localizacao;
	
	/**
	 * Id do tipo de sala de aula.
	 * @var int
	 */
	public $id_tiposaladeaula;
	
	/**
	 * Id da entidade da sala  de aula.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do período letivo.
	 * @var int
	 */
	public $id_periodoletivo;
	
	/**
	 * Diz se vai usar as datas de inscrição, abertura e encerramento do período letivo.
	 * @var boolean
	 */
	public $bl_usardoperiodoletivo;
	
	/**
	 * Id da situação da sala de aula.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Número máximo de alunos
	 * @var int 
	 */
	public $nu_maxalunos;
	
	/**
	 * Número de dias que o aluno pode acessar a sala
	 * @var int 
	 */
	public $nu_diasaluno;
	
	/**
	 * Número de dias de aceso após o encerramento
	 * @var int
	 */
	public $nu_diasencerramento;
	
	/**
	 * Marca se a quantidade de dias para acesso do aluno pode ser indefinido
	 * @var boolean
	 */
	public $bl_semencerramento;
	
	/**
	 * Define que não a dia tempo limite para o aluno cursar a disciplina individualmente
	 * @var boolean
	 */
	public $bl_semdiasaluno;
	
	/**
	 * Pontos negativos identificados pelo professor
	 * @var String
	 */
	public $st_pontosnegativos;
	
	/**
	 * Pontos positivos identificados pelo professor
	 * @var String
	 */
	public $st_pontospositivos;
	
	/**
	 * Pontos a melhorar identificados pelo professor
	 * @var String
	 */
	public $st_pontosmelhorar;
	
	/**
	 * Pontos relacionados ao resgate identificados pelo professor
	 * @var String
	 */
	public $st_pontosresgate;
	
	/**
	 * Conclusoes Finais do Professor
	 * @var String
	 */
	public $st_conclusoesfinais;
	
	/**
	 * Justificativas para os alunos acima da media
	 * @var String
	 */
	public $st_justificativaacima;
	
	/**
	 * Justificativas para os alunos abaixo da media
	 * @var String
	 */
	public $st_justificativaabaixo;

    /**
     * Grava a data que uma atualização foi feita na sala de aula
     * @var DateTime
     */
    public $dt_atualiza;

    public $id_categoriasala;

    public $id_usuarioatualiza;

    public $bl_todasentidades;

	public $bl_ofertaexcepcional;

	public $bl_vincularturma;

	/**
	 * Grava o id da moodle referente a sala de aula
	 * @var DateTime
	 */
	public $id_entidadeintegracao;


    /**
	 * @return the $nu_diasextensao
	 */
	public function getNu_diasextensao() {
		return $this->nu_diasextensao;
	}

	/**
	 * @param field_type $nu_diasextensao
	 */
	public function setNu_diasextensao($nu_diasextensao) {
		$this->nu_diasextensao = $nu_diasextensao;
	}

	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	/**
	 * @return boolean
	 */
	public function getBl_ativa() {
		return $this->bl_ativa;
	}
	
	/**
	 * @return boolean
	 */
	public function getBl_usardoperiodoletivo() {
		return $this->bl_usardoperiodoletivo;
	}
	
	/**
	 * @return string
	 */
	public function getDt_abertura() {
		return $this->dt_abertura;
	}
	
	/**
	 * @return string
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return string
	 */
	public function getDt_encerramento() {
		return $this->dt_encerramento;
	}
	
	/**
	 * @return string
	 */
	public function getDt_fiminscricao() {
		return $this->dt_fiminscricao;
	}
	
	/**
	 * @return string
	 */
	public function getDt_inicioinscricao() {
		return $this->dt_inicioinscricao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_modalidadesaladeaula() {
		return $this->id_modalidadesaladeaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_periodoletivo() {
		return $this->id_periodoletivo;
	}
	
	/**
	 * @return int
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_tiposaladeaula() {
		return $this->id_tiposaladeaula;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return string
	 */
	public function getSt_localizacao() {
		return $this->st_localizacao;
	}
	
	/**
	 * @return string
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}
	
	/**
	 * @param boolean $bl_ativa
	 */
	public function setBl_ativa($bl_ativa) {
		$this->bl_ativa = $bl_ativa;
	}
	
	/**
	 * @param boolean $bl_usardoperiodoletivo
	 */
	public function setBl_usardoperiodoletivo($bl_usardoperiodoletivo) {
		$this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
	}
	
	/**
	 * @param string $dt_abertura
	 */
	public function setDt_abertura($dt_abertura) {
		$this->dt_abertura = $dt_abertura;
	}
	
	/**
	 * @param string $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param string $dt_encerramento
	 */
	public function setDt_encerramento($dt_encerramento) {
		$this->dt_encerramento = $dt_encerramento;
	}
	
	/**
	 * @param string $dt_fiminscricao
	 */
	public function setDt_fiminscricao($dt_fiminscricao) {
		$this->dt_fiminscricao = $dt_fiminscricao;
	}
	
	/**
	 * @param string $dt_inicioinscricao
	 */
	public function setDt_inicioinscricao($dt_inicioinscricao) {
		$this->dt_inicioinscricao = $dt_inicioinscricao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_modalidadesaladeaula
	 */
	public function setId_modalidadesaladeaula($id_modalidadesaladeaula) {
		$this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
	}
	
	/**
	 * @param int $id_periodoletivo
	 */
	public function setId_periodoletivo($id_periodoletivo) {
		$this->id_periodoletivo = $id_periodoletivo;
	}
	
	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}
	
	/**
	 * @param int $id_tiposaladeaula
	 */
	public function setId_tiposaladeaula($id_tiposaladeaula) {
		$this->id_tiposaladeaula = $id_tiposaladeaula;
	}
	
	/**
	 * @param int $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param string $st_localizacao
	 */
	public function setSt_localizacao($st_localizacao) {
		$this->st_localizacao = $st_localizacao;
	}
	
	/**
	 * @param string $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}
	/**
	 * @return the $nu_maxalunos
	 */
	public function getNu_maxalunos() {
		return $this->nu_maxalunos;
	}

	/**
	 * @param number $nu_maxalunos
	 */
	public function setNu_maxalunos($nu_maxalunos) {
		$this->nu_maxalunos = $nu_maxalunos;
	}
	/**
	 * @return the $nu_diasaluno
	 */
	public function getNu_diasaluno() {
		return $this->nu_diasaluno;
	}

	/**
	 * @param number $nu_diasaluno
	 */
	public function setNu_diasaluno($nu_diasaluno) {
		$this->nu_diasaluno = $nu_diasaluno;
	}

	/**
	 * @return the $nu_diasencerramento
	 */
	public function getNu_diasencerramento() {
		return $this->nu_diasencerramento;
	}

	/**
	 * @param number $nu_diasencerramento
	 */
	public function setNu_diasencerramento($nu_diasencerramento) {
		$this->nu_diasencerramento = $nu_diasencerramento;
	}

	/**
	 * @return the $bl_semdiasaluno
	 */
	public function getBl_semdiasaluno() {
		return $this->bl_semdiasaluno;
	}

	/**
	 * @param boolean $bl_semdiasaluno
	 */
	public function setBl_semdiasaluno($bl_semdiasaluno) {
		$this->bl_semdiasaluno = $bl_semdiasaluno;
	}

	/**
	 * @return the $bl_semencerramento
	 */
	public function getBl_semencerramento() {
		return $this->bl_semencerramento;
	}

	/**
	 * @param boolean $bl_semencerramento
	 */
	public function setBl_semencerramento($bl_semencerramento) {
		$this->bl_semencerramento = $bl_semencerramento;
	}
	/**
	 * @return the $st_pontosnegativos
	 */
	public function getSt_pontosnegativos() {
		return $this->st_pontosnegativos;
	}

	/**
	 * @param string $st_pontosnegativos
	 */
	public function setSt_pontosnegativos($st_pontosnegativos) {
		$this->st_pontosnegativos = $st_pontosnegativos;
	}

	/**
	 * @return the $st_pontospositivos
	 */
	public function getSt_pontospositivos() {
		return $this->st_pontospositivos;
	}

	/**
	 * @param string $st_pontospositivos
	 */
	public function setSt_pontospositivos($st_pontospositivos) {
		$this->st_pontospositivos = $st_pontospositivos;
	}

	/**
	 * @return the $st_pontosmelhorar
	 */
	public function getSt_pontosmelhorar() {
		return $this->st_pontosmelhorar;
	}

	/**
	 * @param string $st_pontosmelhorar
	 */
	public function setSt_pontosmelhorar($st_pontosmelhorar) {
		$this->st_pontosmelhorar = $st_pontosmelhorar;
	}

	/**
	 * @return the $st_pontosresgate
	 */
	public function getSt_pontosresgate() {
		return $this->st_pontosresgate;
	}

	/**
	 * @param string $st_pontosresgate
	 */
	public function setSt_pontosresgate($st_pontosresgate) {
		$this->st_pontosresgate = $st_pontosresgate;
	}

	/**
	 * @return the $st_conclusoesfinais
	 */
	public function getSt_conclusoesfinais() {
		return $this->st_conclusoesfinais;
	}

	/**
	 * @param string $st_conclusoesfinais
	 */
	public function setSt_conclusoesfinais($st_conclusoesfinais) {
		$this->st_conclusoesfinais = $st_conclusoesfinais;
	}

	/**
	 * @return the $st_justificativaacima
	 */
	public function getSt_justificativaacima() {
		return $this->st_justificativaacima;
	}

	/**
	 * @param string $st_justificativaacima
	 */
	public function setSt_justificativaacima($st_justificativaacima) {
		$this->st_justificativaacima = $st_justificativaacima;
	}

	/**
	 * @return the $st_justificativaabaixo
	 */
	public function getSt_justificativaabaixo() {
		return $this->st_justificativaabaixo;
	}

	/**
	 * @param string $st_justificativaabaixo
	 */
	public function setSt_justificativaabaixo($st_justificativaabaixo) {
		$this->st_justificativaabaixo = $st_justificativaabaixo;
	}

    /**
     * @param $dt_atualiza
     */
    public function setDt_atualiza($dt_atualiza)
    {
        $this->dt_atualiza = $dt_atualiza;
    }

    /**
     * @param $id_categoriasala
     */
    public function getDt_atualiza()
    {
        return $this->dt_atualiza;
    }

    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param mixed $id_usuarioatualiza
     */
    public function setId_usuarioatualiza($id_usuarioatualiza)
    {
        $this->id_usuarioatualiza = $id_usuarioatualiza;
    }

    /**
     * @return mixed
     */
    public function getId_usuarioatualiza()
    {
        return $this->id_usuarioatualiza;
    }

    public function getBl_todasentidades() {
        return $this->bl_todasentidades;
    }

    public function setBl_todasentidades($bl_todasentidades) {
        $this->bl_todasentidades = $bl_todasentidades;
    }

	public function getBl_ofertaexcepcional()
	{
		return $this->bl_ofertaexcepcional;
	}

	public function setBl_ofertaexcepcional($bl_ofertaexcepcional)
	{
		$this->bl_ofertaexcepcional = $bl_ofertaexcepcional;
	}

    /**
     * @return mixed
     */
    public function getBl_vincularturma()
    {
        return $this->bl_vincularturma;
    }

    /**
     * @param mixed $bl_vincularturma
     */
    public function setBl_vincularturma($bl_vincularturma)
    {
        $this->bl_vincularturma = $bl_vincularturma;
    }

	/**
	 * @return DateTime
	 */
	public function getId_entidadeintegracao()
	{
		return $this->id_entidadeintegracao;
	}

	/**
	 * @param DateTime $id_entidadeintegracao
	 */
	public function setId_entidadeintegracao($id_entidadeintegracao)
	{
		$this->id_entidadeintegracao = $id_entidadeintegracao;
	}


}


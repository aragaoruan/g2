<?php
class CampanhaVendaProdutoTO extends Ead1_TO_Dinamico{
	
	/**
	 * 
	 * Id da campanha comercial
	 * @var int
	 */
	public $id_campanhacomercial;
	/**
	 * 
	 * Id da entidade (sessão)
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * 
	 * Id da situação
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * 
	 * Nome da situação
	 * @var String
	 */
	public $st_situacao;
	
	/**
	 * 
	 * Quantidade de disponibilidade
	 * @var int|numeric
	 */
	public $nu_disponibilidade;
	
	/**
	 * 
	 * Tituo da campanha comercial
	 * @var String
	 */
	public $st_campanhacomercial;
	
	/**
	 * 
	 * Descrição da campanha comercial
	 * @var int
	 */
	public $st_descricao;
	
	/**
	 * 
	 * Data de inicio da campanha
	 * @var Date
	 */
	public $dt_inicio;
	
	/**
	 * 
	 * Data de fim da campanha
	 * @var Date
	 */
	public $dt_fim;
	
	/**
	 * 
	 * Verifica se e ou não todas as formas de pagamento
	 * @var Boolean
	 */
	public $bl_todasformas;
	
	/**
	 * 
	 * Verifica se e ou não todos os produtos
	 * @var Boolean
	 */
	public $bl_todosprodutos;
	
	/**
	 * 
	 * Contém o id do tipo de campanha
	 * @var int
	 */
	public $id_tipocampanha;
	
	/**
	 * 
	 * Contém a o nome do tipo de campanha
	 * @var String
	 */
	public $st_tipocampanha;
	
	/**
	 * 
	 * Contém o id da forma de pagamento
	 * @var int
	 */
	public $id_formapagamento;
	
	/**
	 * 
	 * Contém o id do produto
	 * @var int
	 */
	public $id_produto;
	
	
	/**
	 * 
	 * Contém o id da venda
	 * @var int
	 */
	public $id_venda;
	
	/**
	 * @return the $id_campanhacomercial
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}

	/**
	 * @param int $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param String $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $nu_disponibilidade
	 */
	public function getNu_disponibilidade() {
		return $this->nu_disponibilidade;
	}

	/**
	 * @param int $nu_disponibilidade
	 */
	public function setNu_disponibilidade($nu_disponibilidade) {
		$this->nu_disponibilidade = $nu_disponibilidade;
	}

	/**
	 * @return the $st_campanhacomercial
	 */
	public function getSt_campanhacomercial() {
		return $this->st_campanhacomercial;
	}

	/**
	 * @param String $st_campanhacomercial
	 */
	public function setSt_campanhacomercial($st_campanhacomercial) {
		$this->st_campanhacomercial = $st_campanhacomercial;
	}

	/**
	 * @return the $st_descricao
	 */
	public function getSt_descricao() {
		return $this->st_descricao;
	}

	/**
	 * @param int $st_descricao
	 */
	public function setSt_descricao($st_descricao) {
		$this->st_descricao = $st_descricao;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @param Date $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @return the $dt_fim
	 */
	public function getDt_fim() {
		return $this->dt_fim;
	}

	/**
	 * @param Date $dt_fim
	 */
	public function setDt_fim($dt_fim) {
		$this->dt_fim = $dt_fim;
	}

	/**
	 * @return the $bl_todasformas
	 */
	public function getBl_todasformas() {
		return $this->bl_todasformas;
	}

	/**
	 * @param Boolean $bl_todasformas
	 */
	public function setBl_todasformas($bl_todasformas) {
		$this->bl_todasformas = $bl_todasformas;
	}

	/**
	 * @return the $bl_todosprodutos
	 */
	public function getBl_todosprodutos() {
		return $this->bl_todosprodutos;
	}

	/**
	 * @param Boolean $bl_todosprodutos
	 */
	public function setBl_todosprodutos($bl_todosprodutos) {
		$this->bl_todosprodutos = $bl_todosprodutos;
	}

	/**
	 * @return the $id_tipocampanha
	 */
	public function getId_tipocampanha() {
		return $this->id_tipocampanha;
	}

	/**
	 * @param int $id_tipocampanha
	 */
	public function setId_tipocampanha($id_tipocampanha) {
		$this->id_tipocampanha = $id_tipocampanha;
	}

	/**
	 * @return the $st_tipocampanha
	 */
	public function getSt_tipocampanha() {
		return $this->st_tipocampanha;
	}

	/**
	 * @param String $st_tipocampanha
	 */
	public function setSt_tipocampanha($st_tipocampanha) {
		$this->st_tipocampanha = $st_tipocampanha;
	}

	/**
	 * @return the $id_formapagamento
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}

	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @param int $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
	}

	/**
	 * @param int $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
	}


}
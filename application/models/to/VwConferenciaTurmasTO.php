<?php

/**
 * Classe para encapsular os dados de Vwocorrência.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class   VwConferenciaTurmasTO extends Ead1_TO_Dinamico
{

    public $id_turma;
    public $id_projetopedagogico;
    public $id_entidade;
    public $id_disciplina;
    public $st_disciplina;
    public $st_saladeaula;
    public $st_projetopedagogico;
    public $dt_inicioinscricao;
    public $dt_fiminscricao;
    public $dt_abertura;
    public $dt_encerramento;
    public $st_professor;
    public $bl_conteudo;
    public $st_sistema;
    public $st_coordenadorpp;
    public $st_codsistema;
    public $st_salareferencia;
    public $st_processada;
    public $st_nomemoodle;

    public function set_blConteudo($bl_conteudo)
    {
        $this->bl_conteudo = $bl_conteudo;
    }

    public function get_blConteudo()
    {
        return $this->bl_conteudo;
    }

    public function set_dtAbertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    public function get_dtAbertura()
    {
        return $this->dt_abertura;
    }

    public function set_dtEncerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function get_dtEncerramento()
    {
        return $this->dt_encerramento;
    }

    public function set_dtFiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    public function get_dtFiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    public function set_dtInicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    public function get_dtInicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    public function set_idDisciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function get_idDisciplina()
    {
        return $this->id_disciplina;
    }

    public function set_idEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function get_idEntidade()
    {
        return $this->id_entidade;
    }

    public function set_idProjetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function get_idProjetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function set_idTurma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    public function get_idTurma()
    {
        return $this->id_turma;
    }

    public function set_stDisciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function get_stDisciplina()
    {
        return $this->st_disciplina;
    }

    public function set_stProfessor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    public function get_stProfessor()
    {
        return $this->st_professor;
    }

    public function set_stSaladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function get_stSaladeaula()
    {
        return $this->st_saladeaula;
    }

    public function set_stSistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
    }

    public function get_stSistema()
    {
        return $this->st_sistema;
    }

    public function setStProjetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getStProjetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_coordenadorpp($st_coordenadorpp)
    {
        $this->st_coordenadorpp = $st_coordenadorpp;
    }

    public function getSt_coordenadorpp()
    {
        return $this->st_coordenadorpp;
    }

    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    public function setSt_salareferencia($st_salareferencia)
    {
        $this->st_salareferencia = $st_salareferencia;
    }

    public function getSt_salareferencia()
    {
        return $this->st_salareferencia;
    }

    public function setSt_processada($st_processada)
    {
        $this->st_processada = $st_processada;
    }

    public function getSt_processada()
    {
        return $this->st_processada;
    }

    public function get_stNomemoodle()
    {
        return $this->st_nomemoodle;
    }

    public function set_stNomemoodle($st_nomemoodle)
    {
        $this->st_nomemoodle = $st_nomemoodle;
    }

}

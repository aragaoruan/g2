<?php
/**
 * Classe para encapsular os dados de agência e conta da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class ContaPessoaTO extends Ead1_TO_Dinamico {

	public $id_contapessoa;
	public $id_usuario;
	public $id_tipodeconta;
	public $id_entidade;
	public $st_banco;
	public $st_digitoagencia;
	public $st_agencia;
	public $st_digitoconta;
	public $st_conta;
	public $bl_padrao;
	
	/**
	 * @return the $id_contapessoa
	 */
	public function getId_contapessoa() {
		return $this->id_contapessoa;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_tipodeconta
	 */
	public function getId_tipodeconta() {
		return $this->id_tipodeconta;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @return the $st_digitoagencia
	 */
	public function getSt_digitoagencia() {
		return $this->st_digitoagencia;
	}

	/**
	 * @return the $st_agencia
	 */
	public function getSt_agencia() {
		return $this->st_agencia;
	}

	/**
	 * @return the $st_digitoconta
	 */
	public function getSt_digitoconta() {
		return $this->st_digitoconta;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $bl_padrao
	 */
	public function getBl_padrao() {
		return $this->bl_padrao;
	}

	/**
	 * @param field_type $id_contapessoa
	 */
	public function setId_contapessoa($id_contapessoa) {
		$this->id_contapessoa = $id_contapessoa;
	}

	/**
	 * @param field_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param field_type $id_tipodeconta
	 */
	public function setId_tipodeconta($id_tipodeconta) {
		$this->id_tipodeconta = $id_tipodeconta;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param field_type $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	/**
	 * @param field_type $st_digitoagencia
	 */
	public function setSt_digitoagencia($st_digitoagencia) {
		$this->st_digitoagencia = $st_digitoagencia;
	}

	/**
	 * @param field_type $st_agencia
	 */
	public function setSt_agencia($st_agencia) {
		$this->st_agencia = $st_agencia;
	}

	/**
	 * @param field_type $st_digitoconta
	 */
	public function setSt_digitoconta($st_digitoconta) {
		$this->st_digitoconta = $st_digitoconta;
	}

	/**
	 * @param field_type $st_conta
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param field_type $bl_padrao
	 */
	public function setBl_padrao($bl_padrao) {
		$this->bl_padrao = $bl_padrao;
	}

}

?>
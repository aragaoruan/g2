<?php

class PesquisarTipoMaterialTO extends Ead1_TO_Dinamico{
	
	public $id_tipoMaterial;
	public $st_tipoMaterial;
	public $id_situacao;
	public $id_entidade;
	
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex = 'br.com.ead1.gestor2.view.conteudo.secretaria.cadastrar.material.CadastrarTipoMaterial';
	
	
	/**
	 * @return the $id_tipoMaterial
	 */
	public function getId_tipoMaterial() {
		return $this->id_tipoMaterial;
	}

	/**
	 * @return the $st_tipoMaterial
	 */
	public function getSt_tipoMaterial() {
		return $this->st_tipoMaterial;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param $id_tipoMaterial the $id_tipoMaterial to set
	 */
	public function setId_tipoMaterial($id_tipoMaterial) {
		$this->id_tipoMaterial = $id_tipoMaterial;
	}

	/**
	 * @param $st_tipoMaterial the $st_tipoMaterial to set
	 */
	public function setSt_tipoMaterial($st_tipoMaterial) {
		$this->st_tipoMaterial = $st_tipoMaterial;
	}

	/**
	 * @param $id_situacao the $id_situacao to set
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @param $id_entidade the $id_entidade to set
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
}

?>
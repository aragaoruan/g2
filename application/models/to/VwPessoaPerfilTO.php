<?php
/**
 * Classe para encapsular os dados de view de perfil para pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaPerfilTO extends Ead1_TO_Dinamico {

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Nome da entidade.
	 * @var String
	 */
	public $st_nomentidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Id do perfil.
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Id da Situação do perfil.
	 * @var int
	 */
	public $id_situacao;
	
	/**
	 * Id do perfil pai.
	 * @var int
	 */
	public $id_perfilpai;
	
	/**
	 * Nome completo da pessoa.
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Id da situação do perfil para aquela pessoa.
	 * @var int
	 */
	public $id_situacaousuarioperfilentidade;
	
	/**
	 * Data de início do perfil.
	 * @var string
	 */
	public $dt_inicio;
	
	/**
	 * Data de término do perfil.
	 * @var string
	 */
	public $dt_termino;
	
	/**
	 * Id da Situação da pessoa.
	 * @var int
	 */
	public $id_situacaopessoa;
	
	/**
	 * Id da Situação do perfil.
	 * @var int
	 */
	public $id_situacaoperfil;
	
	/**
	 * Id do perfil pedagógico.
	 * @var int
	 */
	public $id_perfilpedagogico;
	
	/**
	 * String da Situação da pessoa.
	 * @var String
	 */
	public $st_situacaopessoa;
	
	/**
	 * String da Situação do perfil.
	 * @var String
	 */
	public $st_situacaoperfil;
	
	
	/**
	 * @return string
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomentidade;
	}
	
	/**
	 * @return string
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}
	
	/**
	 * @return string
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}
	
	/**
	 * @return int
	 */
	public function getId_perfilpai() {
		return $this->id_perfilpai;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacaopessoa() {
		return $this->id_situacaopessoa;
	}
	
	/**
	 * @return int
	 */
	public function getId_situacaousuarioperfilentidade() {
		return $this->id_situacaousuarioperfilentidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomentidade = $st_nomeentidade;
	}
	
	/**
	 * @param string $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}
	
	/**
	 * @param string $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}
	
	/**
	 * @param int $id_perfilpai
	 */
	public function setId_perfilpai($id_perfilpai) {
		$this->id_perfilpai = $id_perfilpai;
	}
	
	/**
	 * @param int $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}
	
	/**
	 * @param int $id_situacaopessoa
	 */
	public function setId_situacaopessoa($id_situacaopessoa) {
		$this->id_situacaopessoa = $id_situacaopessoa;
	}
	
	/**
	 * @param int $id_situacaousuarioperfilentidade
	 */
	public function setId_situacaousuarioperfilentidade($id_situacaousuarioperfilentidade) {
		$this->id_situacaousuarioperfilentidade = $id_situacaousuarioperfilentidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	/**
	 * @return the $st_situacaopessoa
	 */
	public function getSt_situacaopessoa() {
		return $this->st_situacaopessoa;
	}

	/**
	 * @return the $st_situacaoperfil
	 */
	public function getSt_situacaoperfil() {
		return $this->st_situacaoperfil;
	}

	/**
	 * @param $st_situacaopessoa the $st_situacaopessoa to set
	 */
	public function setSt_situacaopessoa($st_situacaopessoa) {
		$this->st_situacaopessoa = $st_situacaopessoa;
	}

	/**
	 * @param $st_situacaoperfil the $st_situacaoperfil to set
	 */
	public function setSt_situacaoperfil($st_situacaoperfil) {
		$this->st_situacaoperfil = $st_situacaoperfil;
	}
	/**
	 * @return the $id_situacaoperfil
	 */
	public function getId_situacaoperfil() {
		return $this->id_situacaoperfil;
	}

	/**
	 * @param $id_situacaoperfil the $id_situacaoperfil to set
	 */
	public function setId_situacaoperfil($id_situacaoperfil) {
		$this->id_situacaoperfil = $id_situacaoperfil;
	}
	/**
	 * @return the $id_perfilpedagogico
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}

	/**
	 * @param $id_perfilpedagogico the $id_perfilpedagogico to set
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}




}

?>
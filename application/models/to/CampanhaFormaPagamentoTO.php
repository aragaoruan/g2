<?php
/**
 * Classe para encapsular os dados de relacionamento entre campanha e formas de pagamento.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class CampanhaFormaPagamentoTO extends Ead1_TO_Dinamico{

	/**
	 * id da forma de pagamento.
	 * @var int
	 */
	public $id_formapagamento;
	
	/*
	 * Id da campanha comercial.
	 * @var int
	 */
	public $id_campanhacomercial;
	
	/**
	 * @return unknown
	 */
	public function getId_campanhacomercial() {
		return $this->id_campanhacomercial;
	}
	
	/**
	 * @return int
	 */
	public function getId_formapagamento() {
		return $this->id_formapagamento;
	}
	
	/**
	 * @param unknown_type $id_campanhacomercial
	 */
	public function setId_campanhacomercial($id_campanhacomercial) {
		$this->id_campanhacomercial = $id_campanhacomercial;
	}
	
	/**
	 * @param int $id_formapagamento
	 */
	public function setId_formapagamento($id_formapagamento) {
		$this->id_formapagamento = $id_formapagamento;
	}

}

?>
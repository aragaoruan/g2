<?php
/**
 * Classe para encapsular da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com 
 * @package models
 * @subpackage to
 */
 
class AvaliacaoIntegracaoTO extends Ead1_TO_Dinamico{

	public $id_avaliacaointegracao;
	public $id_avaliacao;
	public $id_sistema;
	public $id_usuariocadastro;
	public $st_codsistema;
	public $dt_cadastro;


	/**
	 * @return the $id_avaliacaointegracao
	 */
	public function getId_avaliacaointegracao(){ 
		return $this->id_avaliacaointegracao;
	}

	/**
	 * @return the $id_avaliacao
	 */
	public function getId_avaliacao(){ 
		return $this->id_avaliacao;
	}

	/**
	 * @return the $id_sistema
	 */
	public function getId_sistema(){ 
		return $this->id_sistema;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro(){ 
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_codsistema
	 */
	public function getSt_codsistema(){ 
		return $this->st_codsistema;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro(){ 
		return $this->dt_cadastro;
	}


	/**
	 * @param field_type $id_avaliacaointegracao
	 */
	public function setId_avaliacaointegracao($id_avaliacaointegracao){ 
		$this->id_avaliacaointegracao = $id_avaliacaointegracao;
	}

	/**
	 * @param field_type $id_avaliacao
	 */
	public function setId_avaliacao($id_avaliacao){ 
		$this->id_avaliacao = $id_avaliacao;
	}

	/**
	 * @param field_type $id_sistema
	 */
	public function setId_sistema($id_sistema){ 
		$this->id_sistema = $id_sistema;
	}

	/**
	 * @param field_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro){ 
		$this->id_usuariocadastro = $id_usuariocadastro;
	}

	/**
	 * @param field_type $st_codsistema
	 */
	public function setSt_codsistema($st_codsistema){ 
		$this->st_codsistema = $st_codsistema;
	}

	/**
	 * @param field_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro){ 
		$this->dt_cadastro = $dt_cadastro;
	}

}
<?php
/**
 * Classe que encapsula dados do professor de uma sala de aula
 * @autor Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */
class VwProfessorSalaDeAulaTO extends Ead1_TO_Dinamico{
	
	/**
	 * Contem o id da entidade
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contem o nome da entidade
	 * @var string
	 */
	public $st_nomeentidade;
	
	/**
	 * Contem o id do usuario
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contem o id da referencia do perfil do usuario na entidade
	 * @var int
	 */
	public $id_perfilreferencia;
	
	/**
	 * Contem o nome completo do usuario
	 * @var string
	 */
	public $st_nomecompleto;
	
	/**
	 * Contem o id do perfil
	 * @var int
	 */
	public $id_perfil;
	
	/**
	 * Contem o nome do perfil
	 * @var string
	 */
	public $st_nomeperfil;
	
	/**
	 * Contem o valor que identifica se o professor é titular
	 * @var boolean
	 */
	public $bl_titular;
	
	/**
	 * Contem o valor que identifica se o professor esta ativo na sala de aula
	 * @var boolean
	 */
	public $bl_ativo;
	
	/**
	 * Contem o id da sala de aula
	 * @var int
	 */
	public $id_saladeaula;
	
	/**
	 * Contem o nome da sala de aula
	 * @var string
	 */
	public $st_saladeaula;
	
	/**
	 * Contem o id do tipo de avaliacao da sala de aula
	 * @var int
	 */
	public $id_tipoavaliacaosaladeaula;
	
	/**
	 * Contem o nome do tipo de avaliacao da sala de aula
	 * @var string
	 */
	public $st_tipoavaliacaosaladeaula;
	
	/**
	 * Contem o id da sala de aula do professor
	 * @var int
	 */
	public $id_saladeaulaprofessor;
	
	/**
	 * Contem o numero maximo de alunos do professor
	 * @var numeric
	 */
	public $nu_maximoalunos;
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $id_perfilreferencia
	 */
	public function getId_perfilReferencia() {
		return $this->id_perfilreferencia;
	}

	/**
	 * @return the $st_nomecompleto
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}

	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $bl_titular
	 */
	public function getBl_titular() {
		return $this->bl_titular;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $id_tipoavaliacaosaladeaula
	 */
	public function getId_tipoavaliacaosaladeaula() {
		return $this->id_tipoavaliacaosaladeaula;
	}

	/**
	 * @return the $st_tipoavaliacaosaladeaula
	 */
	public function getSt_tipoavaliacaosaladeaula() {
		return $this->st_tipoavaliacaosaladeaula;
	}

	/**
	 * @return the $id_saladeaulaprofessor
	 */
	public function getId_saladeaulaprofessor() {
		return $this->id_saladeaulaprofessor;
	}

	/**
	 * @return the $nu_maximoalunos
	 */
	public function getNu_maximoalunos() {
		return $this->nu_maximoalunos;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
	}

	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	/**
	 * @param int $id_perfilreferencia
	 */
	public function setId_perfilReferencia($id_perfilreferencia) {
		$this->id_perfilreferencia = $id_perfilreferencia;
	}

	/**
	 * @param string $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}

	/**
	 * @param int $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param string $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param boolean $bl_titular
	 */
	public function setBl_titular($bl_titular) {
		$this->bl_titular = $bl_titular;
	}

	/**
	 * @param int $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param string $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param int $id_tipoavaliacaosaladeaula
	 */
	public function setId_tipoavaliacaosaladeaula($id_tipoavaliacaosaladeaula) {
		$this->id_tipoavaliacaosaladeaula = $id_tipoavaliacaosaladeaula;
	}

	/**
	 * @param string $st_tipoavaliacaosaladeaula
	 */
	public function setSt_tipoavaliacaosaladeaula($st_tipoavaliacaosaladeaula) {
		$this->st_tipoavaliacaosaladeaula = $st_tipoavaliacaosaladeaula;
	}

	/**
	 * @param int $id_saladeaulaprofessor
	 */
	public function setId_saladeaulaprofessor($id_saladeaulaprofessor) {
		$this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
	}

	/**
	 * @param numeric $nu_maximoalunos
	 */
	public function setNu_maximoalunos($nu_maximoalunos) {
		$this->nu_maximoalunos = $nu_maximoalunos;
	}
	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}


	
}
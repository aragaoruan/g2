<?php
/**
 * Classe para encapsular os dados da view de aproveitamento de disciplina.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwAproveitamentoTO extends Ead1_TO_Dinamico {

	public $id_aproveitamentodisciplina;
	
	public $st_nomecompleto;
	
	public $id_usuario;
	
	public $id_municipio;
	
	public $sg_uf;
	
	public $id_usuariocadastro;
	
	public $st_instituicao;
	
	public $dt_conclusao;
	
	public $st_curso;
	
	public $st_disciplina;
	
	public $dt_cadastro;
	
	public $st_notaoriginal;
	
	public $st_cargahoraria;
	
	public $bl_ativo;
	
	public $id_disciplina;
	
	public $id_matricula;
	
	public $nu_aprovafinal;
	
	public $st_disciplinaaproveitada;
	
	public $st_cargahorariadisciplina;
	
	public $nu_anoconclusao;
	
	public $id_apmatriculadisciplina;
	
	public $st_disciplinaoriginal;
	
	public $id_serie;
	
	public $nu_aproveitamento;


	/**
	 * @return the $nu_aproveitamento
	 */
	public function getNu_aproveitamento(){ 
		return $this->nu_aproveitamento;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_cargahorariadisciplina() {
		return $this->st_cargahorariadisciplina;
	}
	
	/**
	 * @param unknown_type $st_cargahorariadisciplina
	 */
	public function setSt_cargahorariadisciplina($st_cargahorariadisciplina) {
		$this->st_cargahorariadisciplina = $st_cargahorariadisciplina;
	}

	
	/**
	 * @return unknown
	 */
	public function getSt_disciplinaaproveitada() {
		return $this->st_disciplinaaproveitada;
	}
	
	/**
	 * @param unknown_type $st_disciplinaaproveitada
	 */
	public function setSt_disciplinaaproveitada($st_disciplinaaproveitada) {
		$this->st_disciplinaaproveitada = $st_disciplinaaproveitada;
	}

	
	/**
	 * @return unknown
	 */
	public function getNu_aprovafinal() {
		return $this->nu_aprovafinal;
	}
	
	/**
	 * @param unknown_type $nu_aprovafinal
	 */
	public function setNu_aprovafinal($nu_aprovafinal) {
		$this->nu_aprovafinal = $nu_aprovafinal;
	}

	
	/**
	 * @return unknown
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getDt_conclusao() {
		return $this->dt_conclusao;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_aproveitamentodisciplina() {
		return $this->id_aproveitamentodisciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_municipio() {
		return $this->id_municipio;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return unknown
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}
	
	/**
	 * @return unknown
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_cargahoraria() {
		return $this->st_cargahoraria;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_curso() {
		return $this->st_curso;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_disciplina() {
		return $this->st_disciplina;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_instituicao() {
		return $this->st_instituicao;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_nomecompleto() {
		return $this->st_nomecompleto;
	}
	
	/**
	 * @return unknown
	 */
	public function getSt_notaoriginal() {
		return $this->st_notaoriginal;
	}
	
	/**
	 * @return the $nu_anoconclusao
	 */
	public function getNu_anoconclusao(){ 
		return $this->nu_anoconclusao;
	}
	
	/**
	 * @return the $id_apmatriculadisciplina
	 */
	public function getId_apmatriculadisciplina(){ 
		return $this->id_apmatriculadisciplina;
	}
	
	/**
	 * @return the $st_disciplinaoriginal
	 */
	public function getSt_disciplinaoriginal(){ 
		return $this->st_disciplinaoriginal;
	}
	
	/**
	 * @return the $id_serie
	 */
	public function getId_serie(){ 
		return $this->id_serie;
	}
	
	/**
	 * @param unknown_type $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
	}
	
	/**
	 * @param unknown_type $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @param unknown_type $dt_conclusao
	 */
	public function setDt_conclusao($dt_conclusao) {
		$this->dt_conclusao = $dt_conclusao;
	}
	
	/**
	 * @param unknown_type $id_aproveitamentodisciplina
	 */
	public function setId_aproveitamentodisciplina($id_aproveitamentodisciplina) {
		$this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
	}
	
	/**
	 * @param unknown_type $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}
	
	/**
	 * @param unknown_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}
	
	/**
	 * @param unknown_type $id_municipio
	 */
	public function setId_municipio($id_municipio) {
		$this->id_municipio = $id_municipio;
	}
	
	/**
	 * @param unknown_type $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param unknown_type $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
	}
	
	/**
	 * @param unknown_type $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
	}
	
	/**
	 * @param unknown_type $st_cargahoraria
	 */
	public function setSt_cargahoraria($st_cargahoraria) {
		$this->st_cargahoraria = $st_cargahoraria;
	}
	
	/**
	 * @param unknown_type $st_curso
	 */
	public function setSt_curso($st_curso) {
		$this->st_curso = $st_curso;
	}
	
	/**
	 * @param unknown_type $st_disciplina
	 */
	public function setSt_disciplina($st_disciplina) {
		$this->st_disciplina = $st_disciplina;
	}
	
	/**
	 * @param unknown_type $st_instituicao
	 */
	public function setSt_instituicao($st_instituicao) {
		$this->st_instituicao = $st_instituicao;
	}
	
	/**
	 * @param unknown_type $st_nomecompleto
	 */
	public function setSt_nomecompleto($st_nomecompleto) {
		$this->st_nomecompleto = $st_nomecompleto;
	}
	
	/**
	 * @param unknown_type $st_notaoriginal
	 */
	public function setSt_notaoriginal($st_notaoriginal) {
		$this->st_notaoriginal = $st_notaoriginal;
	}
	
	/**
	 * @param field_type $nu_anoconclusao
	 */
	public function setNu_anoconclusao($nu_anoconclusao){ 
		$this->nu_anoconclusao = $nu_anoconclusao;
	}
	
	/**
	 * @param field_type $id_apmatriculadisciplina
	 */
	public function setId_apmatriculadisciplina($id_apmatriculadisciplina){ 
		$this->id_apmatriculadisciplina = $id_apmatriculadisciplina;
	}

	/**
	 * @param field_type $st_disciplinaoriginal
	 */
	public function setSt_disciplinaoriginal($st_disciplinaoriginal){ 
		$this->st_disciplinaoriginal = $st_disciplinaoriginal;
	}

	/**
	 * @param field_type $id_serie
	 */
	public function setId_serie($id_serie){ 
		$this->id_serie = $id_serie;
	}
	
	/**
	 * @param field_type $nu_aproveitamento
	 */
	public function setNu_aproveitamento($nu_aproveitamento){ 
		$this->nu_aproveitamento = $nu_aproveitamento;
	}

}

?>
<?php
/**
 * Classe para encapsular os dados de Vwocorrênciaaguardando.
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage to
 */
class VwOcorrenciaAguardandoTO extends Ead1_TO_Dinamico {
		
		public $id_ocorrencia;   
		public $id_matricula;  
		public $id_evolucao;  
		public $id_situacao; 
		public $id_usuariointeressado;  
		public $id_categoriaocorrencia;   
		public $id_assuntoco; 
		public $id_saladeaula;
		public $st_titulo;
		public $dt_cadastro;
		public $st_ocorrencia;
		public $st_situacao;
		public $st_evolucao;
		public $st_assuntoco;
		public $dt_ultimotramite;
		public $st_ultimotramite;
		public $st_categoriaocorrencia;
		public $id_usuarioresponsavel;
		public $st_nomeresponsavel;
		public $id_tipoocorrencia;
		public $id_entidade;
		public $st_nomeinteressado;
		public $id_nucleoco;

		

	/**
	 * @return the $id_nucleoco
	 */
	public function getId_nucleoco() {
		return $this->id_nucleoco;
	}

	/**
	 * @param field_type $id_nucleoco
	 */
	public function setId_nucleoco($id_nucleoco) {
		$this->id_nucleoco = $id_nucleoco;
	}
	
	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
		
	/**
	 * @return the $st_nomeresponsavel
	 */
	public function getSt_nomeresponsavel() {
		return $this->st_nomeresponsavel;
	}

		/**
	 * @param field_type $st_nomeresponsavel
	 */
	public function setSt_nomeresponsavel($st_nomeresponsavel) {
		$this->st_nomeresponsavel = $st_nomeresponsavel;
	}
		
	/**
	 * @return the $st_nomeinteressado
	 */
	public function getSt_nomeinteressado() {
		return $this->st_nomeinteressado;
	}

		/**
	 * @param field_type $st_nomeinteressado
	 */
	public function setSt_nomeinteressado($st_nomeinteressado) {
		$this->st_nomeinteressado = $st_nomeinteressado;
	}
		
	/**
	 * @return the $st_ultimotramite
	 */
	public function getSt_ultimotramite() {
		return $this->st_ultimotramite;
	}

		/**
	 * @param field_type $st_ultimotramite
	 */
	public function setSt_ultimotramite($st_ultimotramite) {
		$this->st_ultimotramite = $st_ultimotramite;
	}

	/**
	 * @return the $id_ocorrencia
	 */
	public function getId_ocorrencia() {
		return $this->id_ocorrencia;
	}

	/**
	 * @param number $id_ocorrencia
	 */
	public function setId_ocorrencia($id_ocorrencia) {
		$this->id_ocorrencia = $id_ocorrencia;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param number $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @return the $id_evolucao
	 */
	public function getId_evolucao() {
		return $this->id_evolucao;
	}

	/**
	 * @param number $id_evolucao
	 */
	public function setId_evolucao($id_evolucao) {
		$this->id_evolucao = $id_evolucao;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @param number $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
	}

	/**
	 * @return the $id_usuariointeressado
	 */
	public function getId_usuariointeressado() {
		return $this->id_usuariointeressado;
	}

	/**
	 * @param number $id_usuariointeressado
	 */
	public function setId_usuariointeressado($id_usuariointeressado) {
		$this->id_usuariointeressado = $id_usuariointeressado;
	}

	/**
	 * @return the $id_categoriaocorrencia
	 */
	public function getId_categoriaocorrencia() {
		return $this->id_categoriaocorrencia;
	}

	/**
	 * @param number $id_categoriaocorrencia
	 */
	public function setId_categoriaocorrencia($id_categoriaocorrencia) {
		$this->id_categoriaocorrencia = $id_categoriaocorrencia;
	}

	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @param number $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @param number $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @return the $st_titulo
	 */
	public function getSt_titulo() {
		return $this->st_titulo;
	}

	/**
	 * @param string $st_titulo
	 */
	public function setSt_titulo($st_titulo) {
		$this->st_titulo = $st_titulo;
	}

	/**
	 * @return the $st_ocorrencia
	 */
	public function getSt_ocorrencia() {
		return $this->st_ocorrencia;
	}

	/**
	 * @param string $st_ocorrencia
	 */
	public function setSt_ocorrencia($st_ocorrencia) {
		$this->st_ocorrencia = $st_ocorrencia;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
	}

	/**
	 * @return the $st_evolucao
	 */
	public function getSt_evolucao() {
		return $this->st_evolucao;
	}

	/**
	 * @param string $st_evolucao
	 */
	public function setSt_evolucao($st_evolucao) {
		$this->st_evolucao = $st_evolucao;
	}

	/**
	 * @return the $st_assuntoco
	 */
	public function getSt_assuntoco() {
		return $this->st_assuntoco;
	}

	/**
	 * @param string $st_assuntoco
	 */
	public function setSt_assuntoco($st_assuntoco) {
		$this->st_assuntoco = $st_assuntoco;
	}

	/**
	 * @return the $dt_ultimotramite
	 */
	public function getDt_ultimotramite() {
		return $this->dt_ultimotramite;
	}

	/**
	 * @param Date $dt_ultimotramite
	 */
	public function setDt_ultimotramite($dt_ultimotramite) {
		$this->dt_ultimotramite = $dt_ultimotramite;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param Date $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
	}
	
	/**
	 * @return the $st_categoriaocorrencia
	 */
	public function getSt_categoriaocorrencia() {
		return $this->st_categoriaocorrencia;
	}

	/**
	 * @param string $st_categoriaocorrencia
	 */
	public function setSt_categoriaocorrencia($st_categoriaocorrencia) {
		$this->st_categoriaocorrencia = $st_categoriaocorrencia;
	}

	/**
	 * @return the $id_usuarioresponsavel
	 */
	public function getId_usuarioresponsavel() {
		return $this->id_usuarioresponsavel;
	}

	/**
	 * @param number $id_usuarioresponsavel
	 */
	public function setId_usuarioresponsavel($id_usuarioresponsavel) {
		$this->id_usuarioresponsavel = $id_usuarioresponsavel;
	}
	/**
	 * @return the $id_tipoocorrencia
	 */
	public function getId_tipoocorrencia() {
		return $this->id_tipoocorrencia;
	}

	/**
	 * @param number $id_tipoocorrencia
	 */
	public function setId_tipoocorrencia($id_tipoocorrencia) {
		$this->id_tipoocorrencia = $id_tipoocorrencia;
	}



	
	
  

		
		
		
		
}

?>
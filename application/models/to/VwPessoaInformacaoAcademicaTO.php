<?php
/**
 * Classe para encapsular os dados de view de informação acadêmica da pessoa.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class VwPessoaInformacaoAcademicaTO extends Ead1_TO_Dinamico {
	
	/**
	 * Variavel que contem o id da informacao academica da pessoa
	 * @var int
	 */
	public $id_informacaoacademicapessoa;

	/**
	 * Id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Curso que a pesso cursou.
	 * @var string
	 */
	public $st_curso;
	
	/**
	 * Nome da instituição que cursou.
	 * @var string
	 */
	public $st_nomeinstituicao;
	
	/**
	 * Id do nível de ensino.
	 * @var int
	 */
	public $id_nivelensino;
	
	/**
	 * Nível de ensino da pessoa.
	 * @var string
	 */
	public $st_nivelensino;
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_curso() {
		return $this->st_curso;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}
	
	/**
	 * @return string
	 */
	public function getSt_nomeinstituicao() {
		return $this->st_nomeinstituicao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string $st_curso
	 */
	public function setSt_curso($st_curso) {
		$this->st_curso = $st_curso;
	}
	
	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
	}
	
	/**
	 * @param string $st_nomeinstituicao
	 */
	public function setSt_nomeinstituicao($st_nomeinstituicao) {
		$this->st_nomeinstituicao = $st_nomeinstituicao;
	}
	/**
	 * @return the $id_informacaoacademicapessoa
	 */
	public function getId_informacaoacademicapessoa() {
		return $this->id_informacaoacademicapessoa;
	}

	/**
	 * @param $id_informacaoacademicapessoa the $id_informacaoacademicapessoa to set
	 */
	public function setId_informacaoacademicapessoa($id_informacaoacademicapessoa) {
		$this->id_informacaoacademicapessoa = $id_informacaoacademicapessoa;
	}


}

?>
<?php
/**
 * Classe para encapsular os dados do documento da identidade.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class DocumentoIdentidadeTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id do usuário.
	 * @var int
	 */
	public $id_usuario;
	
	/**
	 * Contém o id da entidade.
	 * @var int
	 */
	public $id_entidade;
	
	/**
	 * Contém o número de RG da identidade.
	 * @var string
	 */
	public $st_rg;
	
	/**
	 * Contém o órgão expeditor do documento.
	 * @var string
	 */
	public $st_orgaoexpeditor;
	
	/**
	 * Contém a data de expedição do documento.
	 * @var string
	 */
	public $dt_dataexpedicao;
	
	/**
	 * @return string
	 */
	public function getDt_dataexpedicao() {
		return $this->dt_dataexpedicao;
	}
	
	/**
	 * @return int
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}
	
	/**
	 * @return int
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}
	
	/**
	 * @return string
	 */
	public function getSt_rg() {
		return $this->st_rg;
	}
	
	/**
	 * @return string
	 */
	public function getSt_orgaoexpeditor() {
		return $this->st_orgaoexpeditor;
	}
	
	/**
	 * @param string $dt_dataexpedicao
	 */
	public function setDt_dataexpedicao($dt_dataexpedicao) {
		$this->dt_dataexpedicao = $dt_dataexpedicao;
	}
	
	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}
	
	/**
	 * @param int $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}
	
	/**
	 * @param string st_rgg
	 */
	public function setSt_rg($st_rg) {
		$this->st_rg = $st_rg;
	}
	
	/**
	 * @param string $st_orgaoexpeditor
	 */
	public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
		$this->st_orgaoexpeditor = $st_orgaoexpeditor;
	}

}

?>
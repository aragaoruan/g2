<?php
/**
 * Classe para encapsular os dados que definem área para disciplinas extracurriculares e seus valores.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * @package models
 * @subpackage to
 */
class ProjetoExtracurricularTO extends Ead1_TO_Dinamico {

	/**
	 * Id do projeto pedagógico.
	 * @var int
	 */
	public $id_projetopedagogico;
	
	/**
	 * Id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * Número de créditos obrigatórios.
	 * @var int
	 */
	public $nu_obrigatorios;
	
	/**
	 * Número de créditos optativos.
	 * @var int
	 */
	public $nu_optativos;
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}
	
	/**
	 * @return int
	 */
	public function getNu_obrigatorios() {
		return $this->nu_obrigatorios;
	}
	
	/**
	 * @return int
	 */
	public function getNu_optativos() {
		return $this->nu_optativos;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}
	
	/**
	 * @param int $nu_obrigatorios
	 */
	public function setNu_obrigatorios($nu_obrigatorios) {
		$this->nu_obrigatorios = $nu_obrigatorios;
	}
	
	/**
	 * @param int $nu_optativos
	 */
	public function setNu_optativos($nu_optativos) {
		$this->nu_optativos = $nu_optativos;
	}

}

?>
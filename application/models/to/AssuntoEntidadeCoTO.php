<?php
/**
 * tb_assuntoentidadeco encapsulamento
 * @package models
 * @subpackage to
 */
class AssuntoEntidadeCoTO extends Ead1_TO_Dinamico {

	public $id_assuntoentidadeco;
	public $id_assuntoco;
	public $id_entidade;
	
	/**
	 * @return the $id_assuntoentidadeco
	 */
	public function getId_assuntoentidadeco() {
		return $this->id_assuntoentidadeco;
	}

	/**
	 * @return the $id_assuntoco
	 */
	public function getId_assuntoco() {
		return $this->id_assuntoco;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @param field_type $id_assuntoentidadeco
	 */
	public function setId_assuntoentidadeco($id_assuntoentidadeco) {
		$this->id_assuntoentidadeco = $id_assuntoentidadeco;
	}

	/**
	 * @param field_type $id_assuntoco
	 */
	public function setId_assuntoco($id_assuntoco) {
		$this->id_assuntoco = $id_assuntoco;
	}

	/**
	 * @param field_type $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	
	
}

?>
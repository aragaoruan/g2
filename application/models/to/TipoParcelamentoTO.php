<?php
/**
 * Classe para encapsular os dados de tipo de parcelamento.
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage to
 */

class TipoParcelamentoTO extends Ead1_TO_Dinamico {

	/**
	 * Contem o id do tipo de parcelamento
	 * @var int
	 */
	public $id_tipoparcelamento;
	
	/**
	 * Contem o nome do tipo de parcelamento
	 * @var string
	 */
	public $st_tipoparcelamento;
	/**
	 * @return the $id_tipoparcelamento
	 */
	public function getId_tipoparcelamento() {
		return $this->id_tipoparcelamento;
	}

	/**
	 * @return the $st_tipoparcelamento
	 */
	public function getSt_tipoparcelamento() {
		return $this->st_tipoparcelamento;
	}

	/**
	 * @param int $id_tipoparcelamento
	 */
	public function setId_tipoparcelamento($id_tipoparcelamento) {
		$this->id_tipoparcelamento = $id_tipoparcelamento;
	}

	/**
	 * @param string $st_tipoparcelamento
	 */
	public function setSt_tipoparcelamento($st_tipoparcelamento) {
		$this->st_tipoparcelamento = $st_tipoparcelamento;
	}

	
}
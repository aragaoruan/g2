<?php
/**
 * Classe para encapsular os dados de relacionamento entre disciplina e área.
 * @author João Gabriel G. F. Vasconcelos - jgvasconcelos@gmail.com
 * 
 * @package models
 * @subpackage to
 */
class AreaDisciplinaTO extends Ead1_TO_Dinamico {

	/**
	 * Contém o id da disciplina.
	 * @var int
	 */
	public $id_disciplina;
	
	/**
	 * Contém o id da área de conhecimento.
	 * @var int
	 */
	public $id_areaconhecimento;
	
	/**
	 * @return int
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}
	
	/**
	 * @return int
	 */
	public function getId_disciplina() {
		return $this->id_disciplina;
	}
	
	/**
	 * @param int $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
	}
	
	/**
	 * @param int $id_disciplina
	 */
	public function setId_disciplina($id_disciplina) {
		$this->id_disciplina = $id_disciplina;
	}

}

?>
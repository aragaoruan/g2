<?php
/**
 * Regras de negócio para classes relacionadas à Vitrine de Produtos
 * @package models
 * @subpackage bo
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 */
class VitrineBO extends Ead1_BO {
	
	public $mensageiro;
	
	/**
	 * @var VitrineDAO
	 */
	private $dao;
	
	public function __construct(){
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new VitrineDAO();
	}

	/**
	 * @param VwPesquisaVitrineTO $vwPesquisaVitrine
	 * @return Ead1_Mensageiro
	 */
	public function pesquisarAfiliadoResponsavel(VwPesquisaVitrineTO $vwPesquisaVitrine){
		$vwPesquisaVitrine->setId_entidadecadastro($vwPesquisaVitrine->getSessao()->id_entidade);
		return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($this->dao->pesquisarAfiliadoResponsavel($vwPesquisaVitrine), new VwPesquisaVitrineTO()));
	}
	
	/**
	 * Método que decide se salva ou edita uma vitrine
	 * @param VitrineTO $vitrine
	 * @return Ead1_Mensageiro
	 */
	public function salvarVitrine(VitrineTO $vitrine, CategoriaTO $categoria = null, array $produtos = null){
		if($vitrine->getId_vitrine()){
			$this->mensageiro->setMensageiro($this->editarVitrine($vitrine, $categoria, $produtos));
		} else {
			$this->mensageiro->setMensageiro($this->cadastrarVitrine($vitrine, $categoria, $produtos));
		}
		return $this->mensageiro; 
	}
	
	/**
	 * Meotodo que Cadastra Vitrine
	 * Metodo atribui o id_entidadecadastro, id_usuariocadastro
	 * não necessitando fazer isso no Flex ou em outros metodos
	 * 
	 * @param VitrineTO $vitrine
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	private function cadastrarVitrine(VitrineTO $vitrine, CategoriaTO $categoria = null, array $produtos = null){
		$this->dao->beginTransaction();
		try{
			
			//***********Vitrine****************
			if(!$vitrine->getId_entidadecadastro()){
				$vitrine->setId_entidadecadastro($vitrine->getSessao()->id_entidade);
			}
				
			if(!$vitrine->getId_usuariocadastro()){
				$vitrine->setId_usuariocadastro($vitrine->getSessao()->id_usuario);
			}

			$vitrine->setBl_ativo(true);
			$vitrine->fetch(false, true, true);
			
			if($vitrine->getId_vitrine()){
				throw new Zend_Exception('Já existe uma vitrine cadastrada com o mesmo Título!');
			}
			
			$id_vitrine = $this->dao->cadastrarVitrine($vitrine);

			if(!$id_vitrine){
				throw new Zend_Exception("Erro ao cadastrar Vitrine {$vitrine->getSt_vitrine()}!");
			}else{
				$vitrine->setId_vitrine($id_vitrine);
			}
			
			//********Produtos da Vitrine***********
			if($categoria instanceof  CategoriaTO && $categoria->getId_categoria()){
				//se $categoria != null e $produtos == null, é pq todos os produtos foram selecionados, 
				//dai faz o for com o retorno do ProdutoDAO::retornarCategoriaProduto($categoria)
				//se $produtos != null, faz o for apenas com os produtos selecionados
				if($produtos != null){
					foreach ($produtos as $prod){
						$vitrineProduto = new VitrineProdutoTO();
						//Id_categoria só é preenchido caso a opção marcar todas for selecionada no flex
						//$vitrineProduto->setId_categoria($categoria->getId_categoria()); 
						$vitrineProduto->setId_produto($prod->getId_produto());
						$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
						$vitrineProduto->setBl_ativo(true);
						$this->mensageiro = $this->salvarVitrineProduto($vitrineProduto);
						if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception("Erro ao salvar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
						}
					}
				} else {
					//Retorna todos os produtos da categoria passada como parâmetro
					$produtosDAO = new ProdutoDAO();
					$categProdutos = Ead1_TO_Dinamico::encapsularTo($produtosDAO->retornarCategoriaProduto($categoria), new CategoriaProdutoTO());
					foreach ($categProdutos as $categProd){
						$vitrineProduto = new VitrineProdutoTO();
						$vitrineProduto->setId_categoria($categoria->getId_categoria());
						$vitrineProduto->setId_produto($categProd->getId_produto());
						$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
						$vitrineProduto->setBl_ativo(true);
						$this->mensageiro = $this->salvarVitrineProduto($vitrineProduto);
						if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception("Erro ao salvar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
						}
					}
				}
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($vitrine, Ead1_IMensageiro::SUCESSO, 'Vitrine salva com sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que edita uma vitrine
	 * @param VitrineTO $vitrine
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	private function editarVitrine(VitrineTO $vitrine, CategoriaTO $categoria = null, array $produtos = null){
		$this->dao->beginTransaction();
		try{
			
			$vitrineVerifica = new VitrineTO();
			$vitrineVerifica->setBl_ativo(1);
			$vitrineVerifica->setSt_vitrine($vitrine->getSt_vitrine());
			$vitrineVerifica->setId_contratoafiliado($vitrine->getId_contratoafiliado());
			$vitrineVerifica->setId_entidadecadastro($vitrine->getId_entidadecadastro());
			$vitrineVerifica->setId_modelovitrine($vitrine->getId_modelovitrine());
			
			$vitrineVerifica->fetch(false, true, true);
			
			if(($vitrineVerifica->getId_vitrine() != null) && ($vitrine->getId_vitrine() != $vitrineVerifica->getId_vitrine())){
				throw new Zend_Exception("Já existe uma vitrine cadastrada com esse título!");
			}
			
			
			if(!$this->dao->editarVitrine($vitrine)){
				throw new Zend_Exception("Erro ao editar Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
			}
			
			//********Produtos da Vitrine***********
			if($categoria && $categoria->getId_categoria()){
				//se $categoria != null e $produtos == null, é pq todos os produtos foram selecionados,
				//dai faz o for com o retorno do ProdutoDAO::retornarCategoriaProduto($categoria)
				//se $produtos != null, faz o for apenas com os produtos selecionados
				
				//Remove todos os produtos da vitrine antes de inseri-los novamente
				$vitrineProduto = new VitrineProdutoTO();
				$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
				$this->removerProdutosDaVitrine($vitrineProduto);
				
				if($produtos != null){
					
					foreach ($produtos as $prod){
						//Cria um Produto da Vitrine e seta os valores da pesquisa
						$vitrineProduto = new VitrineProdutoTO();
						$vitrineProduto->setId_produto($prod->getId_produto());
						$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
						//Pesquisa o Produto da Vitrine
						$vitrineProduto = $this->retornarVitrineProduto($vitrineProduto);
						
						//Se houver retorno da pesquisa tenta a reativação, senão insere um novo
						if($vitrineProduto->getId_vitrineproduto()){
						//Se estiver desativado o reativa
							if(!$vitrineProduto->getBl_ativo()){
								$vitrineProduto->setBl_ativo(true);
								$this->mensageiro = $this->editarVitrineProduto($vitrineProduto);
							}
						}else {
							$vitrineProduto = new VitrineProdutoTO();
							$vitrineProduto->setId_produto($prod->getId_produto());
							$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
							$vitrineProduto->setBl_ativo(true);
							$this->mensageiro = $this->salvarVitrineProduto($vitrineProduto);
							if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
								throw new Zend_Exception("Erro ao salvar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
							}
						}
						
						if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception("Erro ao editar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
						}
					}
				} else {
					//Retorna todos os produtos da categoria passada como parâmetro
					$produtosDAO = new ProdutoDAO();
					$categProdutos = Ead1_TO_Dinamico::encapsularTo($produtosDAO->retornarCategoriaProduto($categoria), new CategoriaProdutoTO());
					foreach ($categProdutos as $categProd){

						//Cria um Produto da Vitrine e seta os valores da pesquisa
						$vitrineProduto = new VitrineProdutoTO();
						$vitrineProduto->setId_categoria($categoria->getId_categoria());
						$vitrineProduto->setId_produto($categProd->getId_produto());
						$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
						//Pesquisa o Produto da Vitrine
						$vitrineProduto = $this->retornarVitrineProduto($vitrineProduto);

						//Se houver retorno da pesquisa tenta a reativação, senão insere um novo
						if($vitrineProduto->getId_vitrineproduto()){
						//Se estiver desativado o reativa
							if(!$vitrineProduto->getBl_ativo()){
								$vitrineProduto->setBl_ativo(true);
								$this->mensageiro = $this->editarVitrineProduto($vitrineProduto);
							}
						} else {
							$vitrineProduto = new VitrineProdutoTO();
							$vitrineProduto->setId_categoria($categoria->getId_categoria());
							$vitrineProduto->setId_produto($prod->getId_produto());
							$vitrineProduto->setId_vitrine($vitrine->getId_vitrine());
							$vitrineProduto->setBl_ativo(true);
							$this->mensageiro = $this->salvarVitrineProduto($vitrineProduto);
							if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
								throw new Zend_Exception("Erro ao salvar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
							}
						}
						
						if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception("Erro ao editar Produtos da Vitrine {$vitrine->getId_vitrine()} - {$vitrine->getSt_vitrine()}!");
						}
						
					}
				}
			}			
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($vitrine, Ead1_IMensageiro::SUCESSO, 'Vitrine Editada com Sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	public function removerVitrine(VitrineTO $vitrine){
		try{
			$this->dao->beginTransaction();
				
			if(!$vitrine->getId_vitrine()){
				$this->mensageiro->setMensageiro("Não é possível remover sem o id_vitrine.", Ead1_IMensageiro::AVISO);
			}else {
				$vitrine->fetch(true, true, true);
				if($vitrine->getId_vitrine()){
					$vitrine->setBl_ativo(false);
				
					$this->mensageiro = $this->editarVitrine($vitrine);
					
					if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
						$this->mensageiro->setMensageiro("Erro ao remover vitrine!",Ead1_IMensageiro::ERRO,$vitrine);
					}	
				}
			}
				
			$this->dao->commit();
				
		}catch(Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage() . $e->getTraceAsString(), Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna vitrines
	 * @param VitrineTO $vitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarVitrine(VitrineTO $vitrine){
		try{
			$retorno = $this->dao->retornarVitrine($vitrine);

			if($retorno->count() < 1){
				$this->mensageiro->setMensageiro('Nenhuma Vitrine Encontrada!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			} else {
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VitrineTO()),Ead1_IMensageiro::SUCESSO);
			}
			
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Vitrines',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna vitrines passando como parâmetro um VwVitrine
	 * @param VwVitrineTO $vwVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarArVitrineTO(VwVitrineTO $vwVitrine){
		try{
			$retorno = array(); 
			$vws = $this->dao->retornarVwVitrine($vwVitrine);
	
			if($vws->count() < 1){
				$this->mensageiro->setMensageiro('Nenhuma Vitrine Encontrada!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			} else {
				foreach($vws as $vw){
					$vitrineTO = new VitrineTO();
					$vitrineTO->setId_vitrine($vw['id_vitrine']);
					$vitrineTO->fetch(true, true, true);
					$retorno[] = $vitrineTO;
				}
				$this->mensageiro->setMensageiro($retorno, Ead1_IMensageiro::SUCESSO);
			}
				
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Vitrines' . $e->getMessage(),Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que decide se insere ou edita um Produto da Vitrine
	 * @param VitrineProdutoTO $vitrineProduto
	 * @return Ead1_Mensageiro
	 */
	public function salvarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		if($vitrineProduto->getId_vitrineproduto()){
			return $this->editarVitrineProduto($vitrineProduto);
		} else {
			return $this->cadastrarVitrineProduto($vitrineProduto);
		}
	}
	
	/**
	 * Método que cadastra um Produto da Vitrine
	 * @param VitrineProdutoTO $vitrineProduto
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	private function cadastrarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		$this->dao->beginTransaction();
		try{
				
			$vitrineProduto->setBl_ativo(true);
			$id_vitrineProduto = $this->dao->cadastrarVitrineProduto($vitrineProduto);
	
			if(!$id_vitrineProduto){
				throw new Zend_Exception("Erro ao cadastrar Produto da Vitrine - id({$vitrineProduto->getId_vitrine()})!");
			}else{
				$vitrineProduto->setId_vitrineproduto($id_vitrineProduto);
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($vitrineProduto, Ead1_IMensageiro::SUCESSO, 'Produto da Vitrine salvo com sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que edita um Produto da Vitrine 
	 * @param VitrineProdutoTO $vitrineProduto
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	private function editarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		$this->dao->beginTransaction();
		try{
			if(!$this->dao->editarVitrineProduto($vitrineProduto)){
				throw new Zend_Exception("Erro ao editar Produto da Vitrine - id({$vitrineProduto->getId_vitrineproduto()})!");
			}
	
			$this->dao->commit();
			$this->mensageiro->setMensageiro($vitrineProduto, Ead1_IMensageiro::SUCESSO, 'Produto da Vitrine Editado com Sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna Produtos da Vitrine
	 * @param VitrineProdutoTO $vitrineProduto
	 * @return Ead1_Mensageiro
	 */
	public function retornarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		try{
			$retorno = $this->dao->retornarVitrineProduto($vitrineProduto);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhuma Vitrine Encontrada!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VitrineProdutoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Vitrines',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que remove desativa todos o(s) produto(s) de uma determinada Vitrine, 
	 * de acordo com o filtro realizado através do VitrineProdutoTO 
	 * @param VitrineProdutoTO $vitrineProduto
	 * @return Ead1_Mensageiro
	 */
	private function removerProdutosDaVitrine(VitrineProdutoTO $vitrineProduto) {
		try{
			$this->dao->beginTransaction();
			
			if(!$vitrineProduto->getId_vitrine()){
				$this->mensageiro->setMensageiro("Não é possível remover sem o id_vitrine.", Ead1_IMensageiro::AVISO);
			}else {
				$this->mensageiro = $this->retornarVitrineProduto($vitrineProduto);
				
				if($this->mensageiro->getTipo() === Ead1_IMensageiro::SUCESSO){
					
					$produtosDaVitrine = $this->mensageiro->getMensagem();

					foreach ($produtosDaVitrine as $prodVitr){

						$prodVitr->setBl_ativo(false);
						$this->mensageiro = $this->editarVitrineProduto($prodVitr); 
						
						if($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception($this->mensageiro->getFirstMensagem());
						}
					}
					
					$this->mensageiro->setMensageiro("Produtos da Vitrine removidos com sucesso!", Ead1_IMensageiro::SUCESSO);
				}
			}
			
			$this->dao->commit();
			
		}catch(Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage() . $e->getTraceAsString(), Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que busca VwVitrine no banco de dados
	 * @param VwVitrineTO $vwVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwVitrine(VwVitrineTO $vwVitrine){
		try{
			$retorno = $this->dao->retornarVwVitrine($vwVitrine);
			if($retorno->count() < 1){
				$this->mensageiro->setMensageiro('Nenhuma VwVitrine Encontrada!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVitrineTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar VwVitrines',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que busca os modelos de vitrine
	 * @param ModeloVitrineTO $modeloVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarModeloVitrine(ModeloVitrineTO $modeloVitrine){
		try{
			$retorno = $this->dao->retornarModeloVitrine($modeloVitrine);
			if($retorno->count() < 1){
				$this->mensageiro->setMensageiro('Nenhum Modelo de Vitrine Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ModeloVitrineTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Modelos de Vitrines',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
}

?>
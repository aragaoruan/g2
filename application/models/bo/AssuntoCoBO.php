<?php

/**
 * Regras de negócio para assunto
 * @package models
 * @subpackage bo
 */
class AssuntoCoBO extends Ead1_BO
{

    /**
     *
     * @var Entity TipoAvaliacao 
     */
    protected $repositoryName = 'G2\Entity\TipoOcorrencia';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * 
     * @param array $where
     * @param array $order
     * @param integer $limit
     * @param integer $offset
     * @return object
     */
    public function findBy ($where = array(), $order = array(), $limit = 100, $offset = 0)
    {
        return $this->em->getRepository($this->repositoryName)->findBy($where, $order, $limit, $offset);
    }

    /**
     * Método com regras de negócio para cadastro de assunto com ou sem vínculo de entidades
     * 
     * @see AssuntoCoRO::salvarAssunto();
     * 
     * @param AssuntoCoTO $assuntoTO
     * @param array $assuntoEntidadeCoTO [OPTIONAL] array de AssuntoEntidadeCoTO
     * @return Ead1_Mensageiro
     */
    public function salvarAssunto (AssuntoCoTO $assuntoTO, array $arEntidadeTO = null)
    {
        $dao = new AssuntoCoDAO();
        try {
            $dao->beginTransaction();

            $assuntoTO->setId_entidadecadastro($assuntoTO->getSessao()->id_entidade);
            $assuntoTO->setId_usuariocadastro($assuntoTO->getSessao()->id_usuario);

            $arNaoObrigatorios[] = 'id_assuntoco';
            $arNaoObrigatorios[] = 'id_assuntocopai';
            $arNaoObrigatorios[] = 'bl_autodistribuicao';
            $arNaoObrigatorios[] = 'bl_abertura';
            $arNaoObrigatorios[] = 'bl_ativo';
            $arNaoObrigatorios[] = 'id_assuntocopai';
            $arNaoObrigatorios[] = 'dt_cadastro';
            $arNaoObrigatorios[] = 'id_textosistema';
            $arNaoObrigatorios[] = 'bl_cancelamento';
            $arNaoObrigatorios[] = 'bl_trancamento';

            $this->verificaCamposObrigatorios($assuntoTO, $arNaoObrigatorios);

            if ($assuntoTO->getId_assuntoco()) {
                $dao->editarAssunto($assuntoTO);
            } else {
                $assuntoTO->setId_assuntoco($dao->cadastrarAssunto($assuntoTO));
            }

            if (is_array($arEntidadeTO)) {
                $this->salvarArAssuntoEntidade($arEntidadeTO, $assuntoTO);
            }
            $dao->commit();
            return new Ead1_Mensageiro($assuntoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro("Erro ao cadastrar Assunto. " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que cadastra os vínculos de assunto com entidade
     * @see AssuntoCoBO::salvarAssunto();
     * 
     * @param mixed $arEntidadeTO array de Tos do tipo EntidadeTO ou um único TO deste tipo
     * @return Ead1_Mensageiro
     */
    public function salvarArAssuntoEntidade (array $arEntidadeTO, AssuntoCoTO $assuntoTO)
    {
        $dao = new AssuntoCoDAO();
        try {
            $dao->beginTransaction();
            if (!$assuntoTO->getId_assuntoco()) {
                throw new Zend_Exception("A chave do assunto não foi encontrada.");
            }
            $dao->deletarAssuntoEntidadeCoPorAssunto($assuntoTO->getId_assuntoco());
            foreach ($arEntidadeTO as $entidadeTO) {
                $assuntoEntidadeCoTo = new AssuntoEntidadeCoTO();
                $assuntoEntidadeCoTo->setId_entidade($entidadeTO->getId_entidade());
                $assuntoEntidadeCoTo->setId_assuntoco($assuntoTO->getId_assuntoco());
                $dao->cadastrarAssuntoEntidadeCo($assuntoEntidadeCoTo);
            }

            $dao->commit();
            return new Ead1_Mensageiro("Vínculo realizado com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro("Erro ao realizar Vínculo de assunto com entidade!", Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna as entidades do assunto em questão
     * @see AssuntoCoRO::retornarArAssuntoEntidade()
     * @param AssuntoCoTO $assuntoTO
     * @return Ead1_Mensageiro
     */
    public function retornarArAssuntoEntidade (AssuntoCoTO $assuntoTO)
    {
        if (!$assuntoTO->getId_assuntoco()) {
            return new Ead1_Mensageiro("É necessário informar um assunto.", Ead1_IMensageiro::ERRO);
        }
        $dao = new AssuntoCoDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornarArAssuntoEntidade($assuntoTO), new AssuntoEntidadeCoTO()));
    }

    /**
     * @param VwAssuntoCoTo $VwAssuntoCoTo
     * @return Ead1_Mensageiro
     */
    public function retornaVwAssuntoCoPai (VwAssuntoCoTo $VwAssuntoCoTo)
    {
        $VwAssuntoCoTo->setSt_assuntoco(null);
        $VwAssuntoCoTo->setBl_ativo(1);
        $VwAssuntoCoTo->setId_entidade($VwAssuntoCoTo->getSessao()->id_entidade);
        $dao = new AssuntoCoDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornaVwAssuntoCoPai($VwAssuntoCoTo), $VwAssuntoCoTo));
    }

    /**
     * @param VwAssuntoCoTo $VwAssuntoCoTo
     * @return Ead1_Mensageiro
     */
    public function retornarAssuntosTipoAluno (AssuntoCoTO $assuntoCoTo)
    {
        try {
            $assuntoCoTo->setId_entidade($assuntoCoTo->getSessao()->id_entidade);
            $assuntoCoTo->setId_tipoocorrencia(TipoOcorrenciaTO::ALUNO);
            $assuntoCoTo->setBl_ativo(true);
            $dao = new AssuntoCoDAO();

            $retorno = $dao->retornarAssuntosTipoAluno($assuntoCoTo);

            if ($retorno) {
                return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, $assuntoCoTo), Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro('Nenhum registro encontrado!', Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param TipoOcorrenciaTO $tipoocorrencia
     * @return Ead1_Mensageiro
     */
    public function retornaTipoOcorrencia (TipoOcorrenciaTO $tipoocorrencia)
    {
        $dao = new AssuntoCoDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornaTipoOcorrencia($tipoocorrencia), $tipoocorrencia));
    }

    /**
     * Método com as regras de négocio para deletar(físico) um assunto
     * @param AssuntoCoTO $assuntoCoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function deletaAssuntoCo (AssuntoCoTO $assuntoCoTO)
    {
        if (!$assuntoCoTO->getId_assuntoco()) {
            return new Ead1_Mensageiro("É necessário informar um assunto.", Ead1_IMensageiro::ERRO);
        }
        $dao = new AssuntoCoDAO();
        try {
            $dao->beginTransaction();
            $dao->deletarAssuntoCo($assuntoCoTO);
            $dao->commit();
            return new Ead1_Mensageiro("Assunto deletado com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro("Erro ao deletar assunto: " . $e, Ead1_IMensageiro::ERRO);
        }
    }

        /**
         * Metodo responsavel por retornar assuntos relacionados a uma determinada entidade
         * @param VwAssuntoEntidadeTO $VwAssuntoEntidadeTO
         * @return \Ead1_Mensageiro
         * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
         */
        public function retornarVwAssuntoEntidade(VwAssuntoEntidadeTO $VwAssuntoEntidadeTO){
            $dao = new AssuntoCoDAO();
            $VwAssuntoEntidadeTO->setId_entidadecadastro($VwAssuntoEntidadeTO->getSessao()->id_entidade);        
            
            return new Ead1_Mensageiro( 
                    Ead1_TO_Dinamico::encapsularTo(
                    $dao->retornarVwAssuntoEntidade($VwAssuntoEntidadeTO), new VwAssuntoEntidadeTO()), Ead1_IMensageiro::SUCESSO);
}

        /**
         * Metodo responsavel por atualizar o compartilhamento de um assunto por 
         * uma entidade.
         * 
         * @param array $arrVwAssuntoEntidadeTO
         * @param EntidadeTO $entidadeTO
         * @return \Ead1_Mensageiro
         * 
         * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
         */
        public function atualizarCompartilhamentoAssunto(array $arrVwAssuntoEntidadeTO, EntidadeTO $entidadeTO){
            $mensagem = new Ead1_Mensageiro();
            $ormAssuntoEntidade = new AssuntoEntidadeCoORM();
            $dao = new AssuntoCoDAO();

            try{
                $dao->beginTransaction();
                
                foreach($arrVwAssuntoEntidadeTO as $vwAssuntoEntidadeTO){
                    
                    if($vwAssuntoEntidadeTO->getId_assuntoentidadeco()){
                        $assuntoEntidadeCoTO = new AssuntoEntidadeCoTO();
                        $assuntoEntidadeCoTO->setId_assuntoco($vwAssuntoEntidadeTO->getId_assunto());
                        $assuntoEntidadeCoTO->setId_entidade($entidadeTO->getId_entidade());
                        
                        if($assuntoEntidadeCoTO->fetch(false, true, false)){
                            $ormAssuntoEntidade->update($assuntoEntidadeCoTO->toArrayInsert(), $ormAssuntoEntidade->montarWhere($assuntoEntidadeCoTO));
                        }else{
                            $ormAssuntoEntidade->insert($assuntoEntidadeCoTO->toArrayInsert());
                        }
                        
                    }else{
                        if($vwAssuntoEntidadeTO->getId_assunto() and $entidadeTO->getId_entidade()){
                            $assuntoEntidadeCoTO = new AssuntoEntidadeCoTO();
                            $assuntoEntidadeCoTO->setId_assuntoco($vwAssuntoEntidadeTO->getId_assunto());
                            $assuntoEntidadeCoTO->setId_entidade($entidadeTO->getId_entidade());

                            $ormAssuntoEntidade->delete($ormAssuntoEntidade->montarWhere($assuntoEntidadeCoTO, false, true));
                        }
                    }
                }
                
                $dao->commit();
                
                $mensagem->setCodigo(Ead1_Mensageiro_Service::SUCESSO);
                $mensagem->addMensagem('Salvamento de vínculos realizados com sucesso!');
                return $mensagem;
                
            }catch(Zend_Exception $e){
                $dao->rollBack();
                
                $mensagem->addMensagem('Erro ao tentar a salvar os vínculos!' . $e);
                $mensagem->setCodigo(Ead1_Mensageiro_Service::ERRO);
                
                return $mensagem;
            }
        }
}

?>
<?php

/**
 * Classe com regras de negócio para o Uso das Classes de WebService da A+
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 20/02/2013
 * @package models
 * @subpackage bo
 */
class AmaisBO extends Ead1_BO {

    /**
     * Método que Salva os dados de um Aluno no sistema Amais
     * @param AlocacaoTO $aTO
     * @param SalaDeAulaTO $sTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuario(AlocacaoTO $aTO, SalaDeAulaTO $sTO) {

        $dao = new Ead1_DAO();
        $dao->beginTransaction();

        try {

            $vwAlocacaoTO = new VwAlocacaoTO();
            $vwAlocacaoTO->setId_alocacao($aTO->getId_alocacao());

            $vwAlocacaoTO->fetch(false, true, true);
            if (!$vwAlocacaoTO->getId_usuario()) {
                throw new Zend_Exception("Aluno não alocado!");
            }

            $usuario = new UsuarioTO();
            $usuario->setId_usuario($vwAlocacaoTO->getId_usuario());
            $usuario->fetch(true, true, true);

            $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($usuario->getId_usuario());
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::AMAIS);
            $usuarioIntegracaoTO->setId_entidade($sTO->getId_entidade());
            $usuarioIntegracaoTO->fetch(false, true, true);
            if (!$usuarioIntegracaoTO->getId_usuariointegracao()) {

                $pessoaDao = new PessoaDAO();
                $emails = $pessoaDao->retornaEmailPerfil($usuario->getId_usuario(), $sTO->getId_entidade());

                $ato = new Amais_ContaTO();
                $ato->setP(Amais_ContaTO::PLANO_ANUAL);
                $ato->setEm($emails->st_email);
                $ato->setCpf($usuario->getSt_cpf());
                $ato->setIv(new Zend_Date());
                $ato->setN($usuario->getSt_nomecompleto());
                $ato->setS(rand(11111, 99999));

                $fim = new Zend_Date();
                $fim->addDay($sTO->getNu_diasaluno());
                $ato->setFv($fim);

                $ws = new Amais_Cadastro($sTO->getId_entidade());
                $retorno = $ws->cadastrar($ato);
                if ($retorno['tipo'] != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($retorno['Mensagem']);
                }

                $novoUsIntTO = new UsuarioIntegracaoTO();
                $novoUsIntTO->setId_usuario($usuario->getId_usuario());
                $novoUsIntTO->setId_sistema(SistemaTO::AMAIS);
                $novoUsIntTO->setId_entidade($sTO->getId_entidade());
                $novoUsIntTO->setSt_loginintegrado($usuario->getSt_cpf());
                $novoUsIntTO->setSt_senhaintegrada($ato->getS());
                $novoUsIntTO->setId_usuariocadastro(($novoUsIntTO->getSessao()->id_usuario ? $novoUsIntTO->getSessao()->id_usuario : 1));
                $novoUsIntTO->setSt_codusuario('OK');
                $id_usuariointegracao = $usuarioIntegracaoORM->insert($novoUsIntTO->toArrayInsert());
                if (!$id_usuariointegracao) {
                    throw new Zend_Exception('Erro ao cadastrar o Usuário Integração.');
                }
            } // if(!$usuarioIntegracaoTO->getId_usuariointegracao()){


            $alocacaoIntegracaoTO = new AlocacaoIntegracaoTO();
            $alocacaoIntegracaoTO->setId_sistema(SistemaTO::AMAIS);
            $alocacaoIntegracaoTO->setId_alocacao($aTO->getId_alocacao());
            $alocacaoIntegracaoTO->fetch(null, true, true);
            if (!$alocacaoIntegracaoTO->getId_alocacaointegracao()) {
                $alocacaoIntegracaoTO->setId_usuariocadastro(($alocacaoIntegracaoTO->getSessao()->id_usuario ? $alocacaoIntegracaoTO->getSessao()->id_usuario : 1));
                $alocacaoIntegracaoTO->setSt_codalocacao('OK');
                $alocacaoIntegracaoORM = new AlocacaoIntegracaoORM();
                $alocacaoIntegracaoTO->setId_alocacaointegracao($alocacaoIntegracaoORM->insert($alocacaoIntegracaoTO->toArrayInsert()));
            }

            $dao->commit();
            return new Ead1_Mensageiro('Dados do Usuário salvos com sucesso! ', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $dao->rollBack();
            if(isset($retorno['retorno'])){
            	$ex = $retorno;
            } else {
            	$ex = $e;
            }
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $ex);
        }
    }

    /**
     * Método que salva os dados de um Aluno não cadastrado no sistema Amais
     * @param PessoaTO $pTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarPessoa(PessoaTO $pTO) {

        $dao = new Ead1_DAO();
        $dao->beginTransaction();
        try {
            $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($pTO->getId_usuario());
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::AMAIS);
            $usuarioIntegracaoTO->setId_entidade($pTO->getId_entidade());
            $usuarioIntegracaoTO->fetch(false, true, true);
            
            if (!$usuarioIntegracaoTO->getId_usuariointegracao()) {
                $vwpessoaTO = new VwPessoaTO();
                $vwpessoaTO->setId_usuario($pTO->getId_usuario());
                $vwpessoaTO->setId_entidade($pTO->getId_entidade());
                $vwpessoaTO->fetch(false, true, true);
                
                $ato = new Amais_ContaTO();
                $ato->setP(Amais_ContaTO::PLANO_ANUAL);
                $ato->setEm($vwpessoaTO->getSt_email());
                $ato->setCpf($vwpessoaTO->getSt_cpf());
                $ato->setN($vwpessoaTO->getSt_nomecompleto());
                $ato->setS(rand(11111, 99999));
                
                $ws = new Amais_Cadastro($pTO->getId_entidade());
                $retorno = $ws->cadastrar($ato);
                if ($retorno['tipo'] != Ead1_IMensageiro::SUCESSO) {
                   throw new Zend_Exception($retorno['Mensagem']);
                }
                
                $novoUsIntTO = new UsuarioIntegracaoTO();
                $novoUsIntTO->setId_usuario($vwpessoaTO->getId_usuario());
                $novoUsIntTO->setId_sistema(SistemaTO::AMAIS);
                $novoUsIntTO->setId_entidade($pTO->getId_entidade());
                $novoUsIntTO->setSt_loginintegrado($vwpessoaTO->getSt_cpf());
                $novoUsIntTO->setSt_senhaintegrada($ato->getS());
                $novoUsIntTO->setId_usuariocadastro(($novoUsIntTO->getSessao()->id_usuario ? $novoUsIntTO->getSessao()->id_usuario : 1));
                $novoUsIntTO->setSt_codusuario('OK');
                
                $id_usuariointegracao = $usuarioIntegracaoORM->insert($novoUsIntTO->toArrayInsert());
                if (!$id_usuariointegracao)
                    throw new Zend_Exception('Erro ao cadastrar o Usuário Integração.');
            }
            $dao->commit();
            return new Ead1_Mensageiro('Dados do Usuário salvos com sucesso! ', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

}
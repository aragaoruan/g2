<?php 
/**
 * Classe com regras de negócio para interações de Material
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/11/2010
 * 
 * @package models
 * @subpackage bo
 */
class MaterialBO extends Ead1_BO{
	
	public $mensageiro;
	/**
	 * @var MaterialDAO
	 */
	private $dao;
	private $deletado;
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->dao 			= new MaterialDAO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita o Tipo do Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		
		try{
			if($tdmTO->getId_TipoDeMaterial()){
				return $this->editarTipoDeMaterial($tdmTO);
			}
			return $this->cadastrarTipoDeMaterial($tdmTO);
		}catch (Exception $exc) {
			return new Ead1_Mensageiro('Erro de aplicação: '. $exc->getMessage(), Ead1_IMensageiro::ERRO);
		}
	}
		
	/**
	 * Metodo que decide se Cadastra ou Exclui o vinculo do Item de Material com a Area
	 * @param ItemDeMaterialAreaTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$orm = new ItemDeMaterialAreaORM();
		if($orm->consulta($idmaTO,true)){
			return $this->deletarItemDeMaterialArea($idmaTO);
		}
		return $this->cadastrarItemDeMaterialArea($idmaTO);
	}
		
	/**
	 * Metodo que decide se Cadastra ou Exclui o vinculo do Item de Material com a Disciplina
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		$orm = new ItemDeMaterialDisciplinaORM();
		if($orm->consulta($idmdTO,true)){
			return $this->deletarItemDeMaterialDisciplina($idmdTO);
		}
		return $this->cadastrarItemDeMaterialDisciplina($idmdTO);
	}
	
	/**
	 * 
	 * Método que salva as área de conhecimentos do item de material
	 * @param array $arrItemDeMaterialAreaTO
	 * @throws InvalidArgumentException
	 * @throws DomainException
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayItemDeMaterialArea(array $arrItemDeMaterialAreaTO){
		try{
			
			$this->dao->beginTransaction();
				foreach($arrItemDeMaterialAreaTO as $itemDeMaterialAreaTO){
					if(!($itemDeMaterialAreaTO instanceof ItemDeMaterialAreaTO)){
						throw new InvalidArgumentException('O Parametro Recebido não é uma Instancia da Classe ItemDeMaterialAreaTO!');
					}
					if($this->salvarItemDeMaterialArea($itemDeMaterialAreaTO)->tipo !== Ead1_IMensageiro::SUCESSO){
						throw new DomainException('Ocorreu um erro ao salvar o item de material na área!');
					}
				}
			$this->dao->commit();
			
		}catch (Exception $e){
			
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Areas de Conhecimento do Item de Material! ERRO: '.$e->getMessage(),Ead1_IMensageiro::ERRO,$e->getMessage());
			
		}
		return $this->mensageiro->setMensageiro('área(s) de Conhecimento do Item de Material cadastrada(s) com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Salva a(s) disciplina(s) que irão ser vínculadas ao item de material
	 * @param array $arrItemDeMaterialDisciplinaTO
	 * @throws InvalidArgumentException
	 * @throws DomainException
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayItemDeMaterialDisciplina(array $arrItemDeMaterialDisciplinaTO){
		try{
			$this->dao->beginTransaction();
			$this->deletado = false;
			foreach($arrItemDeMaterialDisciplinaTO as $itemDeMaterialDisciplinaTO){
				if(!($itemDeMaterialDisciplinaTO instanceof ItemDeMaterialDisciplinaTO)){
					throw new InvalidArgumentException('O parâmetro contido no array não é do tipo válido (ItemDeMaterialDisciplinaTO)!');
				}
				if($this->deletado === false){
					$this->deletado = true;
					$novoItemDeMaterialDisciplinaTO = new ItemDeMaterialDisciplinaTO();
					$novoItemDeMaterialDisciplinaTO->setId_itemdematerial($itemDeMaterialDisciplinaTO->getId_itemdematerial());
					if($this->deletarItemDeMaterialDisciplina($novoItemDeMaterialDisciplinaTO)->tipo !== Ead1_IMensageiro::SUCESSO){
						throw new DomainException("Ocorreu um erro no processo para salvar as disciplinas do material [Deletar]!");
					}
				}
				if($this->cadastrarItemDeMaterialDisciplina($itemDeMaterialDisciplinaTO)->tipo !== Ead1_IMensageiro::SUCESSO){
					throw new DomainException("Ocorreu um erro no processo para salvar as disciplinas do material [Cadastrar]!");	
				}
			}
			$this->dao->commit();
			
		}catch (Exception $e){
			
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao cadastrar a(s) disciplina(s)! ERRO: '.$e->getMessage(),Ead1_IMensageiro::ERRO,$e->getMessage());
			
		}
		return $this->mensageiro->setMensageiro('Disciplina(s) vínculada(s) ao item de material!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que salvar ou Edita o Tipo de Material
	 * @param array(TipoDeMaterialTO $arrTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayTipoDeMaterial($arrTO){
		$mensageiro = new Ead1_Mensageiro();
		$this->dao->beginTransaction();
		try{
			if(!is_array($arrTO)){
				THROW new Zend_Exception('O Parametro Recebido não é Array!');
			}
			foreach($arrTO as $chave => $tdmTO){
				if(!($tdmTO instanceof TipoDeMaterialTO)){
					THROW new Zend_Exception('O Parametro Recebido não é uma Instancia da Classe TipoDeMaterialTO!');
				}
				if($tdmTO->getId_tipodematerial()){
					$this->editarTipoDeMaterial($tdmTO);
					$mensageiro->addMensagem($tdmTO);
				}else{
					$this->cadastrarTipoDeMaterial($tdmTO);
					$to = $this->mensageiro->getMensagem();
					$mensageiro->addMensagem($to[0]);
				}
			}
			$this->dao->commit();
			$mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
			return $mensageiro;
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			THROW new Zend_Exception($e->getMessage());
			return $mensageiro->setMensageiro('Erro ao Salvar Tipo de Material!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra ou edita a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntregaMaterial(EntregaMaterialTO $emTO){
		if($emTO->getId_entregamaterial()){
			return $this->editarEntregaMaterial($emTO);
		}
		return $this->cadastrarEntregaMaterial($emTO);
	}
	
	/**
	 * Metodo que Cadastra o Tipo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		$tdmTO->setId_entidade($tdmTO->getSessao()->id_entidade);
		$id_tipoMaterial = $this->dao->cadastrarTipoDeMaterial($tdmTO);
		if(!$id_tipoMaterial){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Tipo do Material!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		} 
		$tdmTO->setId_tipodematerial($id_tipoMaterial);
		return $this->mensageiro->setMensageiro($tdmTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que cadastra o projeto pedagogico do material
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMaterialProjeto(MaterialProjetoTO $mpTO){
		try{
			$mpTO->setId_usuariocadastro($mpTO->getSessao()->id_usuario);
			$id_materialprojeto = $this->dao->cadastrarMaterialProjeto($mpTO);
			$mpTO->setId_materialprojeto($id_materialprojeto);
			return $this->mensageiro->setMensageiro($mpTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Projeto Pedagógico do Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Cadastra o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterial(ItemDeMaterialTO $idmTO){
		
		$this->dao->beginTransaction();
		
		try {
			$idmTO->setId_usuariocadastro($idmTO->getSessao()->id_usuario);
			$id_itemMaterial = $this->dao->cadastrarItemDeMaterial($idmTO);
			
			if($id_itemMaterial == false) {
				throw new DomainException('Erro ao inserir o item de material');
			}
			$idmTO->setId_itemdematerial($id_itemMaterial);
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($idmTO,Ead1_IMensageiro::SUCESSO, $this->dao->excecao);
			
		} catch(Exception $e) {
			
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro durante a inserção: '. $e->getMessage(),Ead1_IMensageiro::ERRO);
			
		}
		
	}
	
	/**
	 * Metodo que Vincula a Area ao Item de Material
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$this->dao->beginTransaction();
		
		try {
			
			$id_itemMaterialArea = $this->dao->cadastrarItemDeMaterialArea($idmaTO);
			
			if($id_itemMaterialArea === false){
				throw new DomainException('Não foi possível vincular a área ao item de material');
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Vínculo com área de conhecimento realizado com sucesso',Ead1_IMensageiro::SUCESSO);
			
		} catch(Exception $exc) {
			
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao vincular o item de material a área de conhecimento!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
			
		}
	}
	
	/**
	 * Metodo que Vincula a Disciplina ao Item de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		if(!$this->dao->cadastrarItemDeMaterialDisciplina($idmdTO)){
			return $this->mensageiro->setMensageiro('Erro ao Vincular a Disciplina ao Item de Material!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Disciplina Vinculada ao Item de Material com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que cadastra a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaMaterial(EntregaMaterialTO $emTO){
		try{
			$this->dao->beginTransaction();
			$toDelete = new EntregaMaterialTO();
			$toDelete->setId_matricula($emTO->getId_matricula());
			$toDelete->setId_itemdematerial($emTO->getId_itemdematerial());
			$toDelete->setBl_ativo(false);
			$where = 'id_itemdematerial = '.$emTO->getId_itemdematerial().' AND id_matricula = '.$emTO->getId_matricula();
			$this->dao->editarEntregaMaterial($toDelete,$where);
			$emTO->dt_entrega	= date( 'Y-m-d H:i:s' );
			$emTO->setId_usuariocadastro($emTO->getSessao()->id_usuario);
			$emTO->setBl_ativo(true);
			$id_entregamaterial = $this->dao->cadastrarEntregaMaterial($emTO);
			$emTO->setId_entregamaterial($id_entregamaterial);
			$tramiteBO = new TramiteBO();
			$tramiteBO->cadastrarTramiteEntregaMaterial($emTO);
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($emTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra e vincula uma observação a uma entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaMaterialObservacao(EntregaMaterialTO $emTO, ObservacaoTO $oTO){
		$this->dao->beginTransaction();
		try{
			if(!$emTO->getId_entregamaterial()){
				$toDelete = new EntregaMaterialTO();
				$toDelete->setId_matricula($emTO->getId_matricula());
				$toDelete->setId_itemdematerial($emTO->getId_itemdematerial());
				$toDelete->setBl_ativo(false);
				$this->dao->editarEntregaMaterial($toDelete);
				$emTO->setBl_ativo(true);
				$emTO->setDt_entrega(date('Y-m-d'));
				$emTO->setId_usuariocadastro($emTO->getSessao()->id_usuario);
				$id_entregamaterial = $this->dao->cadastrarEntregaMaterial($emTO);
				$emTO->setId_entregamaterial($id_entregamaterial);
			}
			$oTO->setId_entidade($oTO->getSessao()->id_entidade);
			$oTO->setId_usuario($oTO->getSessao()->id_usuario);
			$oTO->setDt_cadastro(date('Y-m-d'));
			$id_observacao = $this->dao->cadastrarObservacao($oTO);
			$oemTO = new ObservacaoEntregaMaterialTO();
			$oemTO->setId_entregamaterial($emTO->getId_entregamaterial());
			$oemTO->setId_observacao($id_observacao);
			$this->dao->cadastrarObservacaoEntregaMaterial($oemTO);
			$tramiteBO = new TramiteBO();
			$tramiteBO->cadastrarTramiteDevolucaoMaterial($emTO);
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($oemTO,Ead1_IMensageiro::SUCESSO, $oTO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a observacao
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarObservacao(ObservacaoTO $oTO){
		try{
			if(!$oTO->getId_entidade()){
				$oTO->setId_entidade($oTO->getSessao()->id_entidade);
			}
			$oTO->setId_usuario($oTO->getSessao()->id_usuario);
			$oTO->setDt_cadastro(date('Y-m-d'));
			$id_observacao = $this->dao->cadastrarObservacao($oTO);
			$oTO->setId_observacao($id_observacao);
			return $this->mensageiro->setMensageiro($oTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Observação',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a observacao da entrega de material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO){
		try{
			$this->dao->cadastrarObservacaoEntregaMaterial($oemTO);
			return $this->mensageiro->setMensageiro('Observação da Entrega de Material Cadastrada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Observação da Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Edita o Tipo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function editarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		if(!$tdmTO->getId_entidade()){
			$tdmTO->setId_entidade($tdmTO->getSessao()->id_entidade);
		}
		if(!$this->dao->editarTipoDeMaterial($tdmTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Tipo de Material!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro($tdmTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Edita o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function editarItemDeMaterial(ItemDeMaterialTO $idmTO){
		if(!$this->dao->editarItemDeMaterial($idmTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Item de Material!',Ead1_IMensageiro::ERRO,$this->dao->execao);
		}
		return $this->mensageiro->setMensageiro('Item de Material Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}

	/**
	 * Metodo que edita a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $emTO){
		try{
			$this->dao->editarEntregaMaterial($emTO);
			return $this->mensageiro->setMensageiro('Entrega de Material Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar a Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que edita a entrega de material e cadastra uma observacao
	 * @param EntregaMaterialTO $emTO
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterialDevolucao(EntregaMaterialTO $emTO){
		$this->dao->beginTransaction();
		try{
			$emTO->setId_situacao(62);
			$where = 'id_matricula = '.$emTO->getId_matricula().' AND id_itemdematerial = '.$emTO->getId_itemdematerial().' AND bl_ativo = 1';
			$this->dao->editarEntregaMaterial($emTO,$where);
			$tramiteBO = new TramiteBO();
			$tramiteBO->cadastrarTramiteEntregaMaterial($emTO);
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Entrega de Material Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Editar a Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Exclui o TIpo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		
		try{
			$this->dao->beginTransaction();
			$tdmTO->setId_entidade($tdmTO->getSessao()->id_entidade);
			
			$rowsetItemMaterial	= $this->dao->retornarItemDeMaterial( new ItemDeMaterialTO( array( 'id_tipodematerial' => $tdmTO->id_tipodematerial  ) ) );
			
			if( $rowsetItemMaterial->count() > 0 ){
				throw new ErrorException( 'Não é possível excluir o tipo de material. Existem itens de material vinculados ao tipo de material selecionado.' );
			}
			
			if($this->dao->deletarTipoDeMaterial($tdmTO) == 0){
				throw new DomainException('Não foi possível excluir o tipo de material');
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Tipo de material excluído com sucesso!',Ead1_IMensageiro::SUCESSO);
		} catch(Exception $e) {
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao excluir o tipo de Material: '. $e->getMessage(),Ead1_IMensageiro::ERRO);
		}
	}
	
	/**
	 * Metodo que Exclui o Projeto do Material
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarMaterialProjeto(MaterialProjetoTO $mpTO){
		try{
			$this->dao->deletarMaterialProjeto($mpTO);
			return $this->mensageiro->setMensageiro('Projeto Pedagógico Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Projeto Pedagógico!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Exclue o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterial(ItemDeMaterialTO $idmTO){
		if(!$this->dao->deletarItemDeMaterial($idmTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Item de Material!',Ead1_IMensageiro::ERRO,$this->dao->execao);
		}
		return $this->mensageiro->setMensageiro('Item de Material Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que exclui o vínculo da área com o item de material
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$this->dao->beginTransaction();
		try{
			if($this->dao->deletarItemDeMaterialArea($idmaTO) == 0){
				throw new DomainException('Erro ao excluir o vínculo da área com o item de material!');
			}
			
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Vínculo excluído com ssucesso!',Ead1_IMensageiro::SUCESSO);
			
		} catch (Exception $exc) {
			
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro durante a exclusão do vínculo. Erro: '.$exc->getMessage() ,Ead1_IMensageiro::ERRO);
		}
		
	}
	
	/**
	 * Metodo que Exclue o Vinculo da Disciplina com o Item de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		if(!$this->dao->deletarItemDeMaterialDisciplina($idmdTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo da Disciplina com o Item de Material!',Ead1_IMensageiro::ERRO,$this->dao->execao);
		}
		return $this->mensageiro->setMensageiro('Vinculo do Item de Material com a Disciplina Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que verifica se existem vinculos com o item de material e exclui
	 * @param ItemDeMaterialTO $to
	 * @return Ead1_Mensageiro
	 */
	public function verificaExcluiItemDeMaterial(ItemDeMaterialTO $to){
		try{
			$this->dao->beginTransaction();
			$entregaMaterialORM = new EntregaMaterialORM();
			if($entregaMaterialORM->consulta(new EntregaMaterialTO(array('id_itemdematerial' => $to->getId_itemdematerial())),false,true)){
				THROW new Zend_Validate_Exception('O Item de Material não pode ser Excluido pois Existem Entregas de Material Vinculadas a este Item');
			}
			$this->dao->deletarItemDeMaterialArea(new ItemDeMaterialAreaTO(array('id_itemdematerial' => $to->getId_itemdematerial())));
			$this->dao->deletarItemDeMaterialDisciplina(new ItemDeMaterialDisciplinaTO(array('id_itemdematerial' => $to->getId_itemdematerial())));
			$this->dao->deletarItemDeMaterial($to);
			$this->dao->commit();
			$this->mensageiro->setMensageiro("Item de Material Excluido com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::ERRO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("Erro ao Excluir Item de Material",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna os tipos de materiais existentes da entidade logada
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDeMaterial(){
		
		try{
			$vwTipoMaterialTO	= new Material_VwTipoDeMaterialTO();
			$vwTipoMaterialTO->setId_entidade($vwTipoMaterialTO->getSessao()->id_entidade);
			$rowSET 			= $this->dao->retornarTipoDeMaterial($vwTipoMaterialTO);
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($rowSET->toArray(),new Material_VwTipoDeMaterialTO()),Ead1_IMensageiro::SUCESSO); 
		} catch(Exception $e) {
			return $this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::ERRO);
		}
		
	}
	
	/**
	 * Metodo que Retorna os Items de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterial(ItemDeMaterialTO $idmTO){
		$item = $this->dao->retornarItemDeMaterial($idmTO);
		if(is_object($item)){
			$item = $item->toArray();
		}else{
			unset($item);
		}
		if(!$item){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($item, new ItemDeMaterialTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna as Areas dos Items de Material
	 * @param ItemDeMaterialTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		$item = $this->dao->retornarItemDeMaterialArea($idmaTO);
		if(is_object($item)){
			$item = $item->toArray();
		}else{
			unset($item);
		}
		if(!$item){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($item, new ItemDeMaterialAreaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna as Disciplinas dos Items de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		$item = $this->dao->retornarItemDeMaterialDisciplina($idmdTO);
		if(is_object($item)){
			$item = $item->toArray();
		}else{
			unset($item);
		}
		if(!$item){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($item, new ItemDeMaterialDisciplinaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntregaMaterial(EntregaMaterialTO $emTO){
		try{
			$material = $this->dao->retornarEntregaMaterial($emTO)->toArray();
			if(empty($material)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($material, new EntregaMaterialTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a Observacao
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarObservacao(ObservacaoTO $oTO){
		try{
			$observacao = $this->dao->retornarObservacao($oTO)->toArray();
			if(empty($observacao)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($observacao, new ObservacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Observação',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO){
		try{
			$observacao = $this->dao->retornarObservacaoEntregaMaterial($oemTO)->toArray();
			if(empty($observacao)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($observacao, new ObservacaoEntregaMaterialTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Observação da Entrega de Material',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Retorna a view do material com o projeto pedagogico
	 * @param VwMaterialProjetoPedagogicoTO $vwMppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMaterialProjetoPedagogico(VwMaterialProjetoPedagogicoTO $vwMppTO){
		try{
			$dados = $this->dao->retornarVwMaterialProjetoPedagogico($vwMppTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwMaterialProjetoPedagogicoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico.',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Retorna as disciplinas do item de material como disciplinaTO
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDisciplinasItemDeMaterial(ItemDeMaterialDisciplinaTO $idmdTO){
		try{
			$disciplinaDAO = new DisciplinaDAO();
			$materialDAO = new MaterialDAO();
			$materialDisciplina = $materialDAO->retornarItemDeMaterialDisciplina($idmdTO)->toArray();
			if(empty($materialDisciplina)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$where = 'id_disciplina in (';
			$arrWhere = array();
			foreach ($materialDisciplina as $disciplina){
				$arrWhere[$disciplina['id_disciplina']] = $disciplina['id_disciplina']; 
			}
			$implode = implode(', ', $arrWhere);
			$where .= $implode;
			$where .= ')';
			$disciplinas = $disciplinaDAO->retornaDisciplina(new DisciplinaTO(),$where)->toArray();
			if(empty($disciplinas)){
				return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas',Ead1_IMensageiro::ERRO,'Erro de FK entre das tabelas tb_disciplina e tb_itemdematerialdisciplina');
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplinas, new DisciplinaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Retorna os projetos do item de material como ProjetoPedagogicoTO
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProjetosMaterial(MaterialProjetoTO $mpTO){
		try{
			$projetoDAO = new ProjetoPedagogicoDAO();
			$materialDAO = new MaterialDAO();
			$materialProjeto = $materialDAO->retornarMaterialProjeto($mpTO)->toArray();
			if(empty($materialProjeto)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$where = 'id_projetopedagogico in (';
			$arrWhere = array();
			foreach ($materialProjeto as $projeto){
				$arrWhere[$projeto['id_projetopedagogico']] = $projeto['id_projetopedagogico']; 
			}
			$implode = implode(', ', $arrWhere);
			$where .= $implode;
			$where .= ')';
			$projetos = $projetoDAO->retornarProjetoPedagogico(new ProjetoPedagogicoTO(),$where)->toArray();
			if(empty($projetos)){
				return $this->mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico',Ead1_IMensageiro::ERRO,'Erro de FK entre das tabelas tb_projetopedagogico e tb_materialprojeto');
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($projetos, new ProjetoPedagogicoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Retorna as áreas de conhecimentos disponíveis para vincular aos itens de material
	 */
	public function retornarAreaConhecimento() {
		try{
			$materialDAO	= new MaterialDAO();
			$areaROWSET		= $materialDAO->retornarAreaConhecimento();
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($areaROWSET->toArray(), new AreaConhecimentoTO()), Ead1_IMensageiro::SUCESSO);
		} catch (Exception $exc) {
			return $this->mensageiro('Erro de aplicação: '+$exc->getMessage() , Ead1_IMensageiro::SUCESSO);
		}
	}

	/**
	 * Metodo que retorna os dados da view vw_entregamaterial para pesquisa e retorna apenas a ultima entrega de material
	 * @param VwEntregaMaterialTO $vwEmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntregaMaterial(VwEntregaMaterialTO $vwEmTO){
		try{
			$vwEmTO->setBl_ativo(null);
			$arrMaterial = $this->dao->retornarVwEntregaMaterial($vwEmTO)->toArray();
			if(empty($arrMaterial)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$arrSemEntrega = array();
			$arrUltimaEntrega = array();
			$arrOrdemFinal = array();
			foreach($arrMaterial as $index => $material){
				if($material['id_entregamaterial'] == null){
					$arrSemEntrega[] = $material;
				}else{
					if(!array_key_exists($material['id_disciplina'], $arrUltimaEntrega)){
						$arrUltimaEntrega[$material['id_disciplina']] = 0;
					}
					if($arrUltimaEntrega[$material['id_disciplina']] < $material['id_entregamaterial']){
						$arrUltimaEntrega[$material['id_disciplina']] = $material['id_entregamaterial'];
						$arrOrdemFinal[$material['id_disciplina']] = $material;
					}
				}
			}
			$arrFinal = array_merge($arrSemEntrega,$arrOrdemFinal);
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrFinal, new VwEntregaMaterialTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Material!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param VwObservacaoEntregaMaterialTO $vwOemTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwObservacaoEntregaMaterial(VwObservacaoEntregaMaterialTO $vwOemTO){
		try{
			$observacao = $this->dao->retornarVwObservacaoEntregaMaterial($vwOemTO)->toArray();
			if(empty($observacao)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
				return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($observacao, new VwObservacaoEntregaMaterialTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Observações!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
}
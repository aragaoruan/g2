<?php
/**
 * Classe com regras de negócio para interações de Setores
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @since 26/02/2013
 * 
 * @package models
 * @subpackage bo
 */
class SetorBO extends Ead1_BO{
	
	public $mensageiro;
	
	public $dao;
	
	public function __construct(){
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new SetorDAO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita Setor
	 * @param SetorTO $to
	 * @param array $arCargosTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvarSetor(SetorTO $to){
		$this->dao->beginTransaction();
		try{
			if($to->getId_setor()){
				$mensageiro = $this->mensageiro->setMensageiro($this->editarSetor($to));
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception("Erro ao atualizar Setor!");
				}
			} else {
				$mensageiro = $this->mensageiro->setMensageiro($this->cadastrarSetor($to));
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception("Erro ao cadastrar Setor!");
				}
			}
			$this->mensageiro = $mensageiro;
			$this->dao->commit();
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("Erro ao salvar setor - {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Meotodo que Cadastra Setor
	 * Metodo atribui o id_entidadecadastro, id_usuariocadastro e dt_cadastro 
	 * não necessitando fazer isso no Flex ou em outros metodos
	 * @param SetorTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarSetor(SetorTO $to){
		$this->dao->beginTransaction();
		try{
			if(!$to->getId_entidadecadastro()){
				$to->setId_entidadecadastro($to->getSessao()->id_entidade);
			}
			
			if(!$to->getId_usuariocadastro()){
				$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			}
			
			if(!$to->getId_funcaocadastro()){
				
				$vwTO = new VwSetorCargoFuncaoUsuarioTO();
				$vwTO->setId_usuario($to->getSessao()->id_usuario);
				$vwTO->fetch(false, true, true);
				$to->setId_funcaocadastro($vwTO->getId_funcao());
			}
			$to->setDt_cadastro(new Zend_Date(null));
			$to->setBl_ativo(true);
			
			$id_setor = $this->dao->cadastrarSetor($to);
			
			if(!$id_setor){
				throw new Zend_Exception("Erro ao cadastrar Setor {$to->getId_setor()} - {$to->getSt_setor()}!");
			}else{
				$to->setId_setor($id_setor);
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Setor salvo com sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();			
			$this->mensageiro->setMensageiro($e->getMessage() . $e->getCode(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Meotodo que Edita Setor
	 * @param SetorTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function editarSetor(SetorTO $to){
		$this->dao->beginTransaction();
		try{
			if(!$this->dao->editarSetor($to)){
				throw new Zend_Exception("Erro ao editar Setor {$to->getId_setor()} - {$to->getSt_setor()}!");
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Setor Editado com Sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que Desvincula o Setor, fazendo a exclusão lógica, passando o bl_ativo para false
	 * @param SetorTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function desvincularSetor(SetorTO $to){
		$this->dao->beginTransaction();
		try{
			$to->setBl_ativo(false);
			if(!$this->dao->editarSetor($to)){
				throw new Zend_Exception("Erro ao desvincular o Setor {$to->getId_setor()} - {$to->getSt_setor()}!");
			}
			 
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Setor Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $dao->excecao);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna setores
	 * @param SetorTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarSetor(SetorTO $to){
		try{
			if(!$to->getId_entidadecadastro()){
				$to->setId_entidadecadastro($to->getSessao()->id_entidade);
			}
			
			$to->setBl_ativo(true);
			
			$retorno = $this->dao->retornarSetor($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhum Setor Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new SetorTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Setor',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
		
	}
	
	/**
	 * Metodo que retorna uma árvore de setores na forma de objetos a partir de um setor
	 * Se for passado null como parametro, ele retornará uma arvore de TO com todas os setores cadastrados
	  * @param SetorTO $to
	  * @return Ead1_Mensageiro
	  */
	public function retornarArvoreSetor(SetorTO $to = null){
		if($to === null){
			$to = new SetorTO();
			$to->setId_setor(null);
			$to->setBl_ativo(true);
		}
		
		$arSetoresFilhos = array();
		$arSetoresFilhos = $this->retornarFilhosSetor($to);
		$to->setAr_setoresfilho($arSetoresFilhos);
			
		if(empty($arSetoresFilhos)){
			$this->mensageiro->setMensageiro("Nenhum setor encontrado!", Ead1_IMensageiro::AVISO);
		}else {
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna uma árvore de setores na forma de objetos a partir de um setor
	 * @param SetorTO $to
	 * @return void|multitype:Ambigous <Ead1_TO_Dinamico, false, boolean, multitype:>
	 */
	public function retornarFilhosSetor(SetorTO $to){
	
		$to->setBl_ativo(true);
	
		$setoresFilho = new SetorTO();
		$setoresFilho->setId_setorpai($to->getId_setor());
	
		$where = $to->getId_setor() ? '' :  "bl_ativo = 1 AND id_setorpai is null";
	
		$setoresFilho = $this->dao->retornarSetor($setoresFilho, $where);
	
		$retorno = array();
	
		if($setoresFilho){
			foreach ($setoresFilho as $setorFilho){
				$setor = new SetorTO();
				$setor->setId_setor($setorFilho['id_setor']);
				$setorFilho = $setor->fetch(true,true,true);
				$setorFilho->setAr_setoresfilho($this->retornarFilhosSetor($setorFilho));
				
				$retorno[] = $setorFilho;
			}
			return $retorno;
		}else {
			return;
		}
	}
	
	/**
	 * Metodo que retorna um array de setores pai para preencher o cbSetorPai
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarArraySetoresPai(){
			
		$to = new SetorTO();
		return $this->retornarSetor($to);
	
	}

	/**
	 * Método recursivo que formata o nome das setores simulando uma árvore e retorna no array passado como parâmetro $retorno
	 * @see SetorBO::retornarArraySetoresPai()
	 * @param SetorTO $to
	 * @param mixed $marca
	 * @param array $retorno
	 */
	private function formatarArray(SetorTO $to, $marca, array &$retorno){
		if(!$to->getId_setorpai()){
			$marca = '';
		}
		
		if($to->getId_setor()){
			$to->setSt_setor($marca . $to->getSt_setor());
			$retorno[] = $to;
		}
		
		if($to->getAr_setoresfilho()){
			foreach($to->getAr_setoresfilho() as $toFilho){
				$this->formatarArray($toFilho, $marca . '    ', $retorno);
			}
		}else{
			return;
		}
	
	}
	
	/**
	 * Método que retorna VwSetorCargoFuncaoUsuarioTO
	 * @param VwSetorCargoFuncaoUsuarioTO $to
	 */
	public function retornarVwSetorCargoFuncaoUsuario(VwSetorCargoFuncaoUsuarioTO $to){
		try{
			if(!$to->getId_usuario()){
				$to->setId_usuario($to->getSessao()->id_usuario);
			}
				
			$retorno = $this->dao->retornarVwSetorCargoFuncaoUsuario($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhuma VwSetorCargoFuncaoUsuario Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwSetorCargoFuncaoUsuarioTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar VwSetorCargoFuncaoUsuarioTO',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita SetorFuncionario
	 * @param SetorFuncionarioTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvarSetorFuncionario(SetorFuncionarioTO $to){
		$this->dao->beginTransaction();
		try{
			if($to->getId_setorfuncionario()){
				$mensageiro = $this->mensageiro->setMensageiro($this->editarSetorFuncionario($to));
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception("Erro ao atualizar vinculo Setor Funcionario!");
				}
			} else {
				$mensageiro = $this->mensageiro->setMensageiro($this->cadastrarSetorFuncionario($to));
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception("Erro ao cadastrar vinculo Setor Funcionario!");
				}
			}
			$this->mensageiro = $mensageiro;
			$this->dao->commit();
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("Erro ao salvar vinculo funcionario - {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que cadastra vinculo entre setor e funcionario
	 *   
	 * @param SetorFuncionarioTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarSetorFuncionario(SetorFuncionarioTO $to){
		$this->dao->beginTransaction();
		try{
				
			$id_setorfuncionario = $this->dao->cadastrarSetorFuncionario($to);
				
			if(!$id_setorfuncionario){
				throw new Zend_Exception("Erro ao cadastrar vinculo Setor Funcionario {$to->getId_setorfuncionario()}!");
			}else{
				$to->setId_setorfuncionario($id_setorfuncionario);
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Vinculo Setor Funcionario salvo com sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage() . $e->getCode(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Meotodo que Edita Setor Funcionario
	 * @param SetorFuncionarioTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function editarSetorFuncionario(SetorFuncionarioTO $to){
		$this->dao->beginTransaction();
		try{
			if(!$this->dao->editarSetorFuncionario($to)){
				throw new Zend_Exception("Erro ao editar vinculo Setor Funcionario {$to->getId_setorfuncionario()}!");
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Vinculo Setor Funcionario Editado com Sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que Exclui o Setor do Funcionario
	 * @param SetorFuncionarioTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function desvincularSetorFuncionario(SetorFuncionarioTO $to){
		$this->dao->beginTransaction();
		try{
			$this->dao->desvincularSetorFuncionario($to);
	
			$this->dao->commit();
			$this->mensageiro->setMensageiro('Setor Funcionario Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna setores
	 * @param SetorTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarSetorFuncionario(SetorFuncionarioTO $to){
		try{
				
			$retorno = $this->dao->retornarSetorFuncionario($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhum Setor Funcionario Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new SetorFuncionarioTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Setor Funcionario',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna VwSetorFuncionarioTO
	 * @param VwSetorFuncionarioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSetorFuncionario(VwSetorFuncionarioTO $to){
		try{
	
			$retorno = $this->dao->retornarVwSetorFuncionario($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhum View Setor Funcionario Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwSetorFuncionarioTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar View Setor Funcionario',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
// 		Zend_Debug::dump($this->mensageiro,__CLASS__.'('.__LINE__.')'); exit;
		return $this->mensageiro;
	}
}
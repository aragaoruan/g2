<?php

/**
 * Classe com regras de negócio para Remessa
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 05/06/2012
 * @package models
 * @subpackage bo
 */
class RemessaBO extends Ead1_BO {
	
	
	/**
	 * @var RemessaDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new RemessaDAO();
	}
	
	
	
	
	/**
	 * Método de listagem de Remessa
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarRemessa(RemessaTO $remessaTO){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarRemessa($remessaTO), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Remessas: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * Método de listagem de Remessa com Material
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarRemessaComMaterial(RemessaTO $remessaTO){
		
		try {
			
			$retornoremessa = false;
			$x = 0;
			$remessas = $this->dao->listarRemessa($remessaTO);
			foreach($remessas as $remessa){
 				$retornoremessa[] = array($remessa, $this->dao->listarRemessaMaterial(new VwRemessaMaterialTO(array('id_remessa'=>$remessa->getid_remessa()))));
			}
			return new Ead1_Mensageiro($retornoremessa, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Remessas: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	/**
	 * Método de listagem de Material sem Remessa
	 * @param VwMaterialSemRemessaTO $matSemRemessaTO
	 * @return Ambigous <boolean, multitype:>
	 */
	public function listarMaterialSemRemessa(VwMaterialSemRemessaTO $matSemRemessaTO){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarMaterialSemRemessa($matSemRemessaTO), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Remessas: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * Método de listagem de Material da Remessa
	 * @param VwRemessaMaterialTO $remessaMaterialTO
	 * @return Ambigous <boolean, multitype:>
	 */
	public function listarRemessaMaterial(VwRemessaMaterialTO $remessaMaterialTO){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarRemessaMaterial($remessaMaterialTO), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Remessas: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * Inativa uma remessa
	 * @param RemessaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function inativarRemessa(RemessaTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->inativarRemessa($to);
			$this->dao->commit();
			return new Ead1_Mensageiro('Remessa inativada com sucesso!', Ead1_IMensageiro::SUCESSO);
			
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o Inativar a Remessa: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
		
	}
	
	/**
	 * Cadastra o material de uma remessa
	 * @param array $arMatreTO - array de MaterialRemessaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMaterialRemessa(array $arMatreTO){
		if(!empty($arMatreTO)){
			$this->dao->beginTransaction();
			try {
				
				foreach($arMatreTO as $matreTO){
					if($matreTO instanceof MaterialRemessaTO){
						$matreTO->setId_materialremessa($this->dao->cadastrarMaterialRemessa($matreTO));
					} else {
						throw new Zend_Exception("É necessário enviar um array de MaterialRemessaTO");
					}
				}
				
				$this->dao->commit();
				return new Ead1_Mensageiro($arMatreTO, Ead1_IMensageiro::SUCESSO);
				
			} catch (Zend_Exception $e) {
				$this->dao->rollBack();
				return new Ead1_Mensageiro("Erro ao salvar o Material da Remessa: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
			}
		}
	}
	
	/**
	 * Exclui um MaterialRemessa
	 * @param MaterialRemessaTO $to
	 * @param array $where
	 * @return boolean
	 */
	public function deletarMaterialRemessa(MaterialRemessaTO $to, $where = null){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->deletarMaterialRemessa($to, $where);
			$this->dao->commit();
			return new Ead1_Mensageiro("Vínculo do Material com a Remessa excluído com sucesso.", Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao excluir o vinculo do Material com a Remessa: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a Remessa
	 * @param RemessaTO $reTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarRemessa(RemessaTO $reTO){
	
		//$mensageiro = false;
		$this->dao->beginTransaction();
		try {
			
			$reTO->setId_usuariocadastro($reTO->getSessao()->id_usuario);
			$reTO->setId_entidade($reTO->getSessao()->id_entidade);
			
			if($reTO->getId_remessa()){
				$mensageiro = $this->editarRemessa($reTO);
			} else {
				$mensageiro =  $this->cadastrarRemessa($reTO);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar a Remessa: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}
	
	/**
	 * Método que verifica se existe Remessa ativa como Número informado.
	 * Remessa ativa: Remessa onde a dt_inicio é menor q o getdate() e a dt_fim é null
	 * @param RemessaTO $remessaTO
	 * @throws Zend_Exception
	 * @see RemessaBO::cadastrarRemessa();
	 * @see RemessaBO::editarRemessa();
	 */
	public function verificaRemessaAtiva(RemessaTO $remessaTO){

		$veRemessaTO = new RemessaTO();
		$remessaORM  = new RemessaORM();
		
		
		$where = array(
				 'nu_remessa = '.$remessaTO->getNu_remessa()
				,'id_situacao = 81'
				,'dt_inicio < getDate()', 'dt_fim is null'
		);
		
		$mensagem = '';
		if($remessaTO->getId_remessa()){
			$where[] = 'id_remessa != '.$remessaTO->getId_remessa();
		}
		//Zend_Debug::dump($where,__CLASS__.'('.__LINE__.')');exit;
		$result = $remessaORM->getUltimo($where);
		if($result){
			$veRemessaTO = new RemessaTO($result->toArray());
		}
		if($veRemessaTO->getId_remessa()){
			throw new Zend_Exception("Já existe uma Remessa ativa cadastrada com o número ".$remessaTO->getNu_remessa().".");
		}
	}
	
	/**
	 * Método que cadastra uma Remessa
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see RemessaBO::salvarRemessa();
	 */
	public function cadastrarRemessa(RemessaTO $remessaTO){
		$this->dao->beginTransaction();
		try {
			
			
			$this->verificaRemessaAtiva($remessaTO);
			
			$remessaTO->setId_remessa($this->dao->cadastrarRemessa($remessaTO));
			$remessaTO->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($remessaTO, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar a Remessa, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	/**
	 * Método que edita uma Remessa
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see RemessaBO::salvarRemessa();
	 */
	public function editarRemessa(RemessaTO $remessaTO){
		$this->dao->beginTransaction();
		try {
			
			$this->verificaRemessaAtiva($remessaTO);
			
			$this->dao->editarRemessa($remessaTO);
			$remessaTO->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($remessaTO, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar a Remessa, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
}

?>
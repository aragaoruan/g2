<?php 
/**
 * Classe com regras de negócio para interações de Forma de Pagamento
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage bo
 */
class CalendarioLetivoBO extends Ead1_BO{
	
	public $mensageiro;
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new CalendarioLetivoDAO();
	}
	
	
	/**
	 * Metodo que cadastra o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCalendarioLetivo(CalendarioLetivoTO $clTO){
		$clTO->setId_entidade($clTO->getSessao()->id_entidade);
		$id_calendarioletivo = $this->dao->cadastrarCalendarioLetivo($clTO);
		if(!$id_calendarioletivo){
			return $this->mensageiro->setMensageiro('Erro ao cadastrar o calendário letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$clTO->setId_calendarioletivo($id_calendarioletivo);
		return $this->mensageiro->setMensageiro($clTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que cadastra o período letivo
	 * @param PeriodoLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$plTO->setId_entidade($plTO->getSessao()->id_entidade);
		$plTO->setId_usuariocadastro($plTO->getSessao()->id_usuario);
		$regra = $this->regraPerioLetivo($plTO);
		if(is_object($regra)){
			return $regra;
		}
		$id_periodoletivo = $this->dao->cadastrarPeriodoLetivo($plTO);
		if(!$id_periodoletivo){
			return $this->mensageiro->setMensageiro('Erro ao cadastrar o período letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$plTO->setId_periodoletivo($id_periodoletivo);
		return $this->mensageiro->setMensageiro($plTO->tipaTOFlex(),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que cadastra o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDiaLetivo(DiaLetivoTO $dlTO){
		$clTO = new CalendarioLetivoTO();
		$clTO->setId_entidade($dlTO->getSessao()->id_entidade);
		$data = $dlTO->getDt_data();
		$data = explode('/',$data);
		$clTO->setNu_ano($data[2]);
		$calendario = $this->dao->retornarCalendarioLetivo($clTO);
		if(!$calendario){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Calendario Letivo![Dia Letivo]',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$calendario = $calendario->toArray();
		if(empty($calendario)){
			$clTO->setId_entidade($clTO->getSessao()->id_entidade);
			$id_calendarioletivo = $this->dao->cadastrarCalendarioLetivo($clTO);
			if(!$id_calendarioletivo){
				return $this->mensageiro->setMensageiro('Erro ao Cadastrar Calendario Letivo![Dia Letivo]',Ead1_IMensageiro::ERRO,$this->dao->excecao);
			}
			$dlTO->setId_calendarioletivo($id_calendarioletivo);
		}
		$id_dialetivo = $this->dao->cadastrarDiaLetivo($dlTO);
		if(!$id_dialetivo){
			return $this->mensageiro->setMensageiro('Erro ao cadastrar o dia letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$dlTO->setId_dialetivo($id_dialetivo);
		return $this->mensageiro->setMensageiro('Dia letivo cadastrado com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que edita o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCalendarioLetivo(CalendarioLetivoTO $clTO){
		if(!$this->dao->editarCalendarioLetivo($clTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar o calendário letivo!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Calendário letivo editado com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que edita o período letivo
	 * @param PeriodoLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function editarPeriodoLetivo(PeriodoLetivoTO $plTO){
		$regra = $this->regraPerioLetivo($plTO);
		if(is_object($regra)){
			return $regra;
		}
		if(!$this->dao->editarPeriodoLetivo($plTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar o período letivo!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Período letivo editado com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que edita o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function editarDiaLetivo(DiaLetivoTO $dlTO){
		if(!$this->dao->editarDiaLetivo($dlTO)){
			return $this->mensageiro->setMensageiro('Erro ao editar o dia letivo!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Dia letivo editado com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que exclui o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCalendarioLetivo(CalendarioLetivoTO $clTO){
		if(!$this->dao->deletarCalendarioLetivo($clTO)){
			return $this->mensageiro->setMensageiro('Erro ao excluir o calendário letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Calendário letivo excluido com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que exclui o período letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarPeriodoLetivo(PeriodoLetivoTO $plTO){
		if($this->dao->pequisarPeriodoLetivoSalaDeAula($plTO)->count()){
			return $this->mensageiro->setMensageiro('Não é possível excluir o período letivo pois existe(m) vínculo(s) deste com sala(s) de aula!',Ead1_IMensageiro::AVISO);			
		}
		if(!$this->dao->deletarPeriodoLetivo($plTO)){
			return $this->mensageiro->setMensageiro('Erro ao excluir o período letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Período letivo excluido com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que exclui o dia letivo
	 * @param DiaLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDiaLetivo(DiaLetivoTO $plTO){
		if(!$this->dao->deletarDiaLetivo($plTO)){
			return $this->mensageiro->setMensageiro('Erro ao excluir o dia letivo!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Dia letivo excluido com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Método que retorna o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCalendarioLetivo(CalendarioLetivoTO $clTO){
		if(!$clTO->getId_entidade()){
			$clTO->setId_entidade($clTO->getSessao()->id_entidade);
		}
		$calendario = $this->dao->retornarCalendarioLetivo($clTO);
		if(is_object($calendario)){
			$calendario = $calendario->toArray();
		}else{
			unset($calendario);
		}
		if(!$calendario){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($calendario, new CalendarioLetivoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Método que retorna o período letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO){
		if(!$plTO->getId_entidade()){
			$plTO->setId_entidade($plTO->getSessao()->id_entidade);
		}
		$periodo = $this->dao->retornarPeriodoLetivo($plTO);
		if(is_object($periodo)){
			$periodo = $periodo->toArray();
		}else{
			unset($periodo);
		}
		if(!$periodo){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($periodo, new PeriodoLetivoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Método que retorna o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDiaLetivo(DiaLetivoTO $dlTO){
		$dia = $this->dao->retornarDiaLetivo($dlTO);
		if(is_object($dia)){
			$dia = $dia->toArray();
		}else{
			unset($dia);
		}
		if(!$dia){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dia, new DiaLetivoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que contem Regras do Periodo Letivo
	 * @param PeriodoLetivoTO $plTO
	 * @return Ead1_Mensageiro | boolean true
	 */
	public function regraPerioLetivo(PeriodoLetivoTO $plTO){
		$inicioInscricao = $this->converteDataBanco($plTO->getDt_inicioinscricao());
		$fimInscricao = $this->converteDataBanco($plTO->getDt_fiminscricao());
		$aberturaInscricao = $this->converteDataBanco($plTO->getDt_abertura());
		$encerramentoInscricao = $this->converteDataBanco($plTO->getDt_encerramento());
		$erro = '';
		if(!$this->comparaData($inicioInscricao,$fimInscricao)){
			$erro = 1;
			$this->mensageiro->addMensagem('O Fim da Inscrição não pode ser Menor que a Data de Início da Inscrição!');
		}
		if(!$this->comparaData($aberturaInscricao,$encerramentoInscricao)){
			$erro = 1;
			$this->mensageiro->addMensagem('A Data de Encerramento não pode ser Menor que a Data de Abertura!');
		}
		if(!$this->comparaData($inicioInscricao,$aberturaInscricao) || !$this->comparaData($inicioInscricao,$encerramentoInscricao)){
			$erro = 1;
			$this->mensageiro->addMensagem('A Data de Início da Inscrição não pode ser Maior que a Data de Abertura ou de Encerramento!');
		}
		if(!$this->comparaData($fimInscricao,$aberturaInscricao) || !$this->comparaData($fimInscricao,$encerramentoInscricao)){
			$erro = 1;
			$this->mensageiro->addMensagem('A Data de Fim da Inscrição não pode ser Maior que a Data de Abertura ou de Encerramento');
		}
		if($erro){
			$this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
			return $this->mensageiro;
		}
		return true;
	}
}
?>
<?php
/**
 * Classe com regras de negócio para interações de erros
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 18/07/2013
 * @package models
 * @subpackage bo
 */
class ErroBO extends Ead1_BO {

    public function salvar(ErroTO $erro){
        $dao = new ErroDAO();
        $dao->beginTransaction();
        try{
            if(!$erro->getId_entidade()){
                throw new Zend_Validate_Exception('O id_entidade é obrigatorio e não foi passado');
            }
            if(!$erro->getId_sistema()){
                throw new Zend_Validate_Exception('O id_sistema é obrigatorio e não foi passado');
            }
            if(!$erro->getSt_mensagem()){
                throw new Zend_Validate_Exception('O st_mensagem é obrigatorio e não foi passado');
            }
            if(!$erro->getSt_origem()){
                throw new Zend_Validate_Exception('O st_origem é obrigatorio e não foi passado');
            }

            if(!$erro->getId_processo()){
                throw new Zend_Validate_Exception('O id_processo é obrigatorio e não foi passado');
            }

            $erro->setId_erro($dao->cadastrarErro($erro));
            $dao->commit();
            return  $erro;
        }catch (Exception $e){
            $dao->rollBack();
        }

    }

}
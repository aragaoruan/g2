<?php


/**
 * Classe com regras de negócio para integração de sistemas
 * @author Elcio Mauro Guimarães
 * @since 10/03/2014
 *
 * @package models
 * @subpackage bo
 */
class IntegracaoBO extends Ead1_BO {
	
	
	var $debug = false;
	
	/**
	 * Faz a integração das salas de aula da entidade e sistema informados
	 * @param int $id_entidade
	 * @param int $id_sistema
	 * @throws Exception
	 * @return Ead1_Mensageiro
	 */
	public function integrarSalas($id_entidade, $id_sistema){
		
		try {
			
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->integrarSalas();
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

	
	/**
	 * Salva a Sala de Aula
	 * @param SalaDeAulaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarSala(SalaDeAulaTO $to){
		
		try {
			
			$si = new SalaDeAulaIntegracaoTO();
			$si->setId_saladeaula($to->getId_saladeaula());
			$si->fetch(false, true, true);
                        $id_sistema = $si->getId_sistema();
			if($id_sistema){
				switch ($si->getId_sistema()) {
					case SistemaTO::BLACKBOARD:
						$bo = new BlackBoardBO($to->getId_entidade());
						$bo->debug = $this->debug;
						return $bo->salvarCurso($to);
					break;
					default:
						return new Ead1_Mensageiro("Sistema integração ainda não foi acoplada à Integração HTML", Ead1_IMensageiro::AVISO);
					break;
				}
			} else {
				return new Ead1_Mensageiro("A Sala de aula ".$to->getId_saladeaula() . " - " . $to->getSt_saladeaula()." não tem integração", Ead1_IMensageiro::AVISO);
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

	
	/**
	 * Atualiza os Usuários e senhas nas Integrações
	 * @param DadosAcessoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarUsuarioSenha(DadosAcessoTO $to){
		
		try {
			
			$atualizados = null;
			$ui = new UsuarioIntegracaoTO();
            $ui->setId_usuario($to->getId_usuario());
            $ui->setId_entidade($to->getId_entidade());
            $arrui = $ui->fetch(false, false, false);
            if($arrui){
				foreach($arrui as $aui){
					if($aui instanceof UsuarioIntegracaoTO){
						
						switch ($aui->getId_sistema()) {
							case SistemaTO::BLACKBOARD:
								
								$bo = new BlackBoardBO($aui->getId_entidade());
								$muser = $bo->getUser($aui->getId_entidade(), $aui->getId_usuario());
								if($muser->getTipo()==Ead1_IMensageiro::SUCESSO){
									$user = $muser->getFirstMensagem();
									$user->setPassword($to->getSt_senha());
									$bo->debug = $this->debug;
									$mensageiro = $bo->salvarUsuario($user, false);
									if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
										$atualizados[] = new Ead1_Mensageiro("Sistema BlackBoard: Senha Atualizada", Ead1_IMensageiro::SUCESSO);
									} else {
										$atualizados[] = new Ead1_Mensageiro("Sistema BlackBoard: ".$mensageiro->getFirstMensagem(), $mensageiro->getTipo());
									}
									
								} else {
									$atualizados[] = new Ead1_Mensageiro("Sistema BlackBoard: ".$muser->getFirstMensagem(), Ead1_IMensageiro::AVISO);
								}
								
								break;
							default:
								$atualizados[] = new Ead1_Mensageiro("Sistema ".$aui->getId_sistema().": IntegracaoBO::salvarUsuarioSenha ainda não foi acoplada à Integração HTML", Ead1_IMensageiro::AVISO);
							break;
						}
						
						
					}//if($aui instanceof UsuarioIntegracaoTO){
				}//foreach($arrui as $aui){
				return new Ead1_Mensageiro($atualizados, Ead1_IMensageiro::SUCESSO);
			}//if($arrui){
			return new Ead1_Mensageiro("Nenhum registro encontrado para Atualização", Ead1_IMensageiro::AVISO);
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}
	
	
	/**
	 * Verifica se as Salas de Aula da Entidade foram sincronizadas
	 * @param int $id_entidade
	 * @param int $id_sistema
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function verificarSalas($id_entidade, $id_sistema){
		
		try {
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->verificarSalas();
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}
	
	/**
     * Integra os Alunos ao BlackBoard
     * @param $id_entidade
     * @param $id_sistema
     * @return Ead1_Mensageiro
     */
    public function integrarAlunos($id_entidade, $id_sistema,$horas = null){

		try {
			
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->integrarAlunos($horas);
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}
	
	public function integrarObservador($id_entidade, $id_sistema){
		
		try {
			
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->integrarObservador();
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

	
	public function integrarProfessores($id_entidade, $id_sistema){
		
		try {
			
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->integrarProfessores();
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

    /**
     * @description Inativar um Usuário do Curso
     * @param int $id_entidade
     * @param int $id_sistema
     * @param int $id_alocacao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
	public function inativarUsuarioCurso($id_entidade, $id_sistema, $id_alocacao = null) {
		
		try {
			
			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:
					$bo = new BlackBoardBO($id_entidade);
					$bo->debug = $this->debug;
					return $bo->inativarUsuarioCurso($id_alocacao);
				break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
				break;
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		
	}

	/**
	 *
	 * Integra a Entidade com algum sistema externo
	 * @param $id_entidade
	 * @param $id_sistema
	 * @param $parameros reservada para quando precisar enviar dados extras
	 * @return Ead1_Mensageiro
	 */
	public function integrarEntidade($id_entidade, $id_sistema, array $parameros = null){

		try {

			if(!$id_entidade || !$id_sistema)
				throw new Zend_Exception("Informe o id_entidade e o id_sistema");


			switch ($id_sistema) {
				case SistemaTO::BLACKBOARD:

					$entity = $this->em->getRepository('G2\Entity\EntidadeIntegracao')->findOneBy(array('id_sistema'=>15, 'id_entidade'=>$id_entidade));
					if($entity){
						return new \Ead1_Mensageiro("Essa Entidade já está Integrada ao BlackBoard", \Ead1_IMensageiro::SUCESSO);
					}

					$entity = $this->em->getRepository('G2\Entity\EntidadeIntegracao')->findOneBy(array('id_sistema'=>15));
					if(!$entity){
						throw new Zend_Exception("Nenhuma integração BlackBoard encontrada para cópia");
					}
					if($entity instanceof G2\Entity\EntidadeIntegracao){

						$nentity = clone $entity;
						$nentity->setid_entidade($id_entidade);
						$nentity->setid_entidadeintegracao(null);
						$this->em->persist($nentity);
						$this->em->flush();

					}

					return new \Ead1_Mensageiro("Entidade Integrada com Sucesso!", \Ead1_IMensageiro::SUCESSO);

					break;
				default:
					throw new Zend_Exception("Essa integração ainda não foi acoplada à Integração HTML");
					break;
			}

		} catch (Exception $e) {
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}

	}

}

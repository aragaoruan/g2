<?php

/**
 * Classe com regras de negócio para as configurações da entidade
 * @author Dimas Sulz
 * @since 10/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class ConfiguracaoEntidadeBO extends Ead1_BO {

    public $dao;

    public function __construct() {
        $this->dao = new ConfiguracaoEntidadeDAO();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * 
     * Salva o(s) texto(s) do sistema, define se é para cadastrar ou editar
     * @param TextoSistemaTO $textoSistemaTO
     * @return Ead1_Mensageiro
     */
    public function salvarTextoSistema(TextoSistemaTO $textoSistemaTO) {
        try {
            if ($textoSistemaTO->getId_textosistema()) {
                return $this->editarTextoSistema($textoSistemaTO);
            } else {
                return $this->cadastrarTextoSistema($textoSistemaTO);
            }
        } catch (Exception $e) {
            return new $this->mensageiro->setMensageiro('Erro de aplicação: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que salva a configuracao da entidade
     * @param ConfiguracaoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to) {
        if ($to->getId_configuracaoentidade()) {
            return $this->editarConfiguracaoEntidade($to);
        }
        return $this->cadastrarConfiguracaoEntidade($to);
    }

    /**
     * Metodo que salva a configuracao financeira da entidade
     * @param EntidadeFinanceiroTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeFinanceiro(EntidadeFinanceiroTO $to) {
        if ($to->getId_entidadefinanceiro()) {
            return $this->editarEntidadeFinanceiro($to);
        }
        return $this->cadastrarEntidadeFinanceiro($to);
    }

    /**
     * 
     * Cadastra o(s) texto(s) do sistema
     * @param TextoSistemaTO $textoSistemaTO
     * @throws InvalidArgumentException
     * @throws DomainException
     * @return Ead1_Mensageiro
     */
    public function cadastrarTextoSistema(TextoSistemaTO $textoSistemaTO) {
        try {
            $this->dao->beginTransaction();
            if (!$textoSistemaTO->getId_entidade()) {
                $textoSistemaTO->setId_entidade($textoSistemaTO->getSessao()->id_entidade);
            }
            if (!$textoSistemaTO->getId_usuario()) {
                $textoSistemaTO->setId_usuario($textoSistemaTO->getSessao()->id_usuario);
            }

            if (!$textoSistemaTO->getSt_texto()) {
                throw new InvalidArgumentException("É necessário informar um título de texto!");
            }

            if (!$textoSistemaTO->getDt_inicio()) {
                throw new InvalidArgumentException("É necessário informar uma data de início!");
            }

            $textoSistema = $this->dao->cadastrarTextoSistema($textoSistemaTO);
            if (!$textoSistema) {
                throw new DomainException("Não foi possível cadastrar o texto do sistema!");
            }
            $this->dao->commit();
            $textoSistemaTO->setId_textosistema($textoSistema);
            return $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao cadastrar o texto do sistema. ERRO: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que cadastra a configuracao da entidade
     * @param ConfiguracaoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to) {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $id_configuracaoentidade = $this->dao->cadastrarConfiguracaoEntidade($to);
            $to->setId_configuracaoentidade($id_configuracaoentidade);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar as Configurações da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra a configuracao financeira da entidade
     * @param EntidadeFinanceiroTO $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidadeFinanceiro(EntidadeFinanceiroTO $to) {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $id_entidadefinanceiro = $this->dao->cadastrarEntidadeFinanceiro($to);
            $to->setId_entidadefinanceiro($id_entidadefinanceiro);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar as Configurações Financeiras da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Salva um EmailEntidadeMensagemTO
     * @param EmailEntidadeMensagemTO $emailEntidadeMensagemTO
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    protected function cadastrarEmailEntidadeMensagem(EmailEntidadeMensagemTO $emailEntidadeMensagemTO) {

        if (empty($emailEntidadeMensagemTO->id_entidade) || $emailEntidadeMensagemTO->id_entidade == false) {
            $emailEntidadeMensagemTO->id_entidade = $emailEntidadeMensagemTO->getSessao()->id_entidade;
        }

        if (empty($emailEntidadeMensagemTO->id_usuariocadastro) || $emailEntidadeMensagemTO->id_usuariocadastro == false) {
            $emailEntidadeMensagemTO->id_usuariocadastro = $emailEntidadeMensagemTO->getSessao()->id_usuario;
        }

        if (empty($emailEntidadeMensagemTO->id_emailconfig) || $emailEntidadeMensagemTO->id_emailconfig == false) {
            throw new DomainException('Informe a conta de envio');
        }

        $emailEntidadeMensagemTO->bl_ativo = true;
        $emailEntidadeMensagemTO->dt_cadastro = date('Y-m-d H:i:s');

        return $this->dao->cadastrarEmailEntidadeMensagem($emailEntidadeMensagemTO);
    }

    /**
     * Remove os vínculos de para uma entidade e mensagem 
     * da tabela tb_emailentidademensagem
     * @param int $idEntidade
     * @param int $idMensagemPadrao
     * @throws DomainException
     * @return bool
     */
    protected function deletarEmailEntidadeMensagem($idEntidade, $idMensagemPadrao) {
        if (empty($idEntidade) || $idEntidade == false) {
            throw new DomainException('Informe a entidade para remover os vínculos de  mensagem');
        }

        if (empty($idMensagemPadrao) || $idMensagemPadrao == false) {
            throw new DomainException('Informe a mensagem padrão para remover os vínculos de  mensagem');
        }
        return $this->dao->deletarEmailEntidadeMensagem($idEntidade, $idMensagemPadrao);
    }

    /**
     * 
     * Edita o(s) texto(s) do sistema
     * @param TextoSistemaTO $textoSistemaTO
     * @throws DomainException
     * @return Ead1_Mensageiro
     */
    public function editarTextoSistema(TextoSistemaTO $textoSistemaTO) {
        try {
            $this->dao->beginTransaction();
            if (!$textoSistemaTO->getId_entidade()) {
                $textoSistemaTO->setId_entidade($textoSistemaTO->getSessao()->id_entidade);
            }
            if (!$textoSistemaTO->getId_usuario()) {
                $textoSistemaTO->setId_usuario($textoSistemaTO->getSessao()->id_usuario);
            }

            $textoSitema = $this->dao->editarTextoSistema($textoSistemaTO);
            if (!$textoSitema) {
                throw new DomainException("Não foi possível editar o texto do sistema!");
            }
            $this->dao->commit();
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao editar o texto do sistema. ERRO: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }

        return $this->mensageiro->setMensageiro("Texto do sistema editado com sucesso!", Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita a configuracao da entidade
     * @param ConfiguracaoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function editarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to) {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $this->dao->editarConfiguracaoEntidade($to);
            $this->mensageiro->setMensageiro('Configurações da Instituição Editadas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar as Configurações da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que edita a configuracao financeira da entidade
     * @param EntidadeFinanceiroTO $to
     * @return Ead1_Mensageiro
     */
    public function editarEntidadeFinanceiro(EntidadeFinanceiroTO $to) {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $this->dao->editarEntidadeFinanceiro($to);
            $this->mensageiro->setMensageiro('Configurações Financeiras da Instituição Editadas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar as Configurações Financeiras da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Salva uma lista de EmailEntidadeMensagemTO
     * Todos os registros devem ser da mesma entidade
     * Caso algum dos registros não possua entidade é pego o id_entidade da sessão
     * 
     * 
     * @param array $arrEmailEntidadeMensagemTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrEmailEntidadeMensagem(array $arrEmailEntidadeMensagemTO) {

        try {
            $this->dao->beginTransaction();
            $arrEntidade = array();

            if (!is_array($arrEmailEntidadeMensagemTO) || empty($arrEmailEntidadeMensagemTO)) {
                throw new DomainException('A lista de mensagens informada não é válida.');
            }

            foreach ($arrEmailEntidadeMensagemTO as $emailEntidadeMensagemTO) {
                if ($emailEntidadeMensagemTO->id_entidade != 0) {
                    if (!empty($arrEntidade) && !in_array($emailEntidadeMensagemTO->id_entidade, $arrEntidade)) {
                        throw new DomainException('A lista de mensagens a serem salvas deve pertencer há uma única entidade');
                    }
                    $this->deletarEmailEntidadeMensagem($emailEntidadeMensagemTO->id_entidade, $emailEntidadeMensagemTO->id_mensagempadrao);
                    $arrEntidade[] = $emailEntidadeMensagemTO->id_entidade;
                }
                $this->cadastrarEmailEntidadeMensagem($emailEntidadeMensagemTO);
            }

            $this->dao->commit();

            $this->mensageiro->setMensageiro('Registro salvo com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainException) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($domainException->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exception) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao salvar a lista de mensagens', Ead1_IMensageiro::ERRO, $exception->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Retorno da view de textos do sistema
     * @param VwTextoSistemaTO $vwTextoSistemaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoSistema(VwTextoSistemaTO $vwTextoSistemaTO) {
        try {
            $retorno = $this->dao->retornarTextoSistema($vwTextoSistemaTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) texto(s) do sistema! ERRO: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwTextoSistemaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorno do texto exibição
     * @param TextoExibicaoTO $textoExibicaoTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoExibicao(TextoExibicaoTO $textoExibicaoTO) {
        try {
            $retorno = $this->dao->retornarTextoExibicao($textoExibicaoTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar a exibição do texto! ERRO: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TextoExibicaoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorno do texto categoria
     * @param TextoCategoriaTO $textoCategoriaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoCategoria(TextoCategoriaTO $textoCategoriaTO) {
        try {
            $retorno = $this->dao->retornarTextoCategoria($textoCategoriaTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar a categoria do texto! ERRO: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TextoCategoriaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorno do texto exbição categoria
     * @param TextoExibicaoCategoriaTO $textoExibicaoCategoriaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoExbicaoCategoria(TextoExibicaoTO $textoExibicaoTO) {
        try {
            $retorno = $this->dao->retornarTextoExbicaoCategoria($textoExibicaoTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o texto de exibição da categoria do texto! ERRO: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TextoCategoriaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorna as orientações disponíveis para os textos do sistema
     * As orientações existentes são: Retrato e Paisagem 
     * @param OrientacaoTextoTO $orientacaoTextoTO
     * @return Ead1_Mensageiro
     */
    public function retornarOrientacaoTexto(OrientacaoTextoTO $orientacaoTextoTO) {

        try {
            $orientacaoRS = $this->dao->retornarOrientacaoTexto($orientacaoTextoTO);

            if ($orientacaoRS->count() < 1) {
                throw new DomainException('Nenhum registro de orientação de texto encontrado');
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($orientacaoRS, new OrientacaoTextoTO()));
        } catch (DomainException $domainExc) {
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao listar as orientações de texto', Ead1_IMensageiro::AVISO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Retorna as mensagem padrões do sistema 
     * @param MensagemPadraoTO $mensagemPadraoTO
     * @return Ead1_Mensageiro
     */
    public function retornarMensagemPadrao(MensagemPadraoTO $mensagemPadraoTO=null) {

        try {

            $mensagemPadraoRS = $this->dao->retornarMensagemPadrao($mensagemPadraoTO);

            if ($mensagemPadraoRS->count() < 1) {
                throw new DomainException('Nenhum registro de mensagem padrão encontrado');
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($mensagemPadraoRS, new MensagemPadraoTO()));
        } catch (DomainException $domainExc) {
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro('Erro ao listar as mensagens padrões', Ead1_IMensageiro::AVISO, $domainExc->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * 
     * @param EmailEntidadeMensagemTO $emailEntidadeMensagemTO
     * @return Ead1_Mensageiro
     */
    public function retornarEmailEntidadeMensagem(EmailEntidadeMensagemTO $emailEntidadeMensagemTO) {

        try {
            $emailEntidadeMensagemRS = $this->dao->retornarEmailEntidadeMensagem($emailEntidadeMensagemTO);

            if ($emailEntidadeMensagemRS->count() < 1) {
                throw new DomainException(' Nenhum registro de mensagem encontrado');
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($emailEntidadeMensagemRS, new EmailEntidadeMensagemTO()), Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainExc) {
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exception) {
            $this->mensageiro->setMensageiro('Erro ao listar as mensagem', Ead1_IMensageiro::ERRO, $exception->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a configuração da entidade 
     * @param ConfiguracaoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function retornarConfiguracaoEntidade(ConfiguracaoEntidadeTO $ceTO) {
        try {
            $ceTO->setId_entidade(($ceTO->getId_entidade() ? $ceTO->getId_entidade() : $ceTO->getSessao()->id_entidade));
            $ceTO->setBl_bloqueiadisciplina(null);
            $dados = $this->dao->retornarConfiguracaoEntidade($ceTO)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Não Foi Encontrada Nenhuma Configuração para a Instituição!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ConfiguracaoEntidadeTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Configurações da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna modelo de grade de notas 
     * @param ModeloGradeNotasTO $modeloGradeNotasTO
     * @return Ead1_Mensageiro
     */
    public function retornarModeloGradeNotas(ModeloGradeNotasTO $modeloGradeNotasTO) {
        try {
            $dados = $this->dao->retornarModeloGradeNotas($modeloGradeNotasTO)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Não Foi Encontrada Nenhum Modelo de Grade de Notas!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ModeloGradeNotasTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Modelo de Grade de Notas!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a configuração financeira da entidade 
     * @param EntidadeFinanceiroTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeFinanceiro(EntidadeFinanceiroTO $to) {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarEntidadeFinanceiro($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Não Foi Encontrada Nenhuma Configuração Financeira para a Instituição!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EntidadeFinanceiroTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Configurações Financeira da Instituição!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna o array 
     * @param UploadTO $uploadTO
     * @param array $where
     * @throws Zend_Exception
     */
    public function retornarUploadManualAluno(UploadTO $uploadTO) {
        try {
            if (!$uploadTO->getId_upload()) {
                throw new Zend_Exception('O atributo id_upload não pode ser vazio!');
            }
            $uploadTO->fetch(true, true, true);
            $this->mensageiro->setMensageiro($uploadTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Upload do Manual do Aluno.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Retorna o upload da tabela preço da entidade
     * @param UploadTO $uploadTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarUploadTabelaPreco(UploadTO $uploadTO) {
        try {
            if (!$uploadTO->getId_upload()) {
                throw new Zend_Exception('O atributo id_upload não pode ser vazio!');
            }
            $uploadTO->fetch(true, true, true);
            $this->mensageiro->setMensageiro($uploadTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Upload da Tabela Preço.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

}
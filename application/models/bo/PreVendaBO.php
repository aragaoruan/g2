<?php
/**
 * Classe com regras de negócio para interações de Pré-Venda
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class PreVendaBO extends Ead1_BO {

	public $mensageiro;
	
	/**
	 * @var PreVendaDAO
	 */
	private $dao;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new PreVendaDAO();
	}
	
	/**
	 * Metodo cadastra a pre venda
	 * @param PreVendaTO $pvTO
	 * @param array de PreVendaProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVenda(PreVendaTO $pvTO, array $arrPreVendaProdutoTO = null){
		
		$this->dao->beginTransaction();
		try{
            // Atribui entidade da sessao, caso nao for definido entidade
			if (!$pvTO->getId_entidade())
				$pvTO->setId_entidade($pvTO->getSessao()->id_entidade);
            // Atribui data atual, caso nao for definido dt_cadstro
            if (!$pvTO->dt_cadastro)
                $pvTO->setDt_cadastro(date('Y:m:d H:i:s'));
            // Atribui ativo como 1, caso nao for definido bl_ativo
            if (!$pvTO->bl_ativo)
                $pvTO->setBl_ativo(1);

            // Validar CPF
			if ($pvTO->getSt_cpf()){
				if(!$this->validaCPF($this->somenteNumero($pvTO->getSt_cpf()))){
					THROW new Zend_Validate_Exception('O CPF Fornecido é Inválido!');
				}
			}
            // Validar email
			if (!$this->validaEmail($pvTO->getSt_email()))
				THROW new Zend_Validate_Exception('O Email Fornecido é Inválido!');

            // Salvar PreVenda
			$id_prevenda = $this->dao->cadastrarPreVenda($pvTO);
			$pvTO->setId_prevenda($id_prevenda);
			$pvTO->fetch(false, true, true);

			if($arrPreVendaProdutoTO){
				foreach ($arrPreVendaProdutoTO as $preVendaProdutoTO){
					if(!($preVendaProdutoTO instanceof PreVendaProdutoTO)){
						THROW new Zend_Validate_Exception('Os produtos não foram enviados de forma correta!');
					}
					$preVendaProdutoTO->setId_prevenda($pvTO->getId_prevenda());
				}				

				$mensageiro = $this->cadastrarPreVendaProduto($arrPreVendaProdutoTO);
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
						THROW new Zend_Exception($mensageiro->getFirstMensagem());
				}
			}
			
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($pvTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::ERRO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Pré-Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo edita a pre venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function editarPreVenda(PreVendaTO $pvTO){
		try{
			if($pvTO->getSt_cpf()){
				if(!$this->validaCPF($this->somenteNumero($pvTO->getSt_cpf()))){
					THROW new Zend_Validate_Exception('O CPF Fornecido é Inválido!');
				}
			}
			if(!$this->validaEmail($pvTO->getSt_email())){
				THROW new Zend_Validate_Exception('O Email Fornecido é Inválido!');
			}
			$this->dao->editarPreVenda($pvTO);
			return $this->mensageiro->setMensageiro('Pré-Venda Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Pré-Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo cadastrar a pre venda com produto valor
	 * @param $arrProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVendaProdutoValor($arrProduto){
		try{
			$pvpvTO = new PreVendaProdutoValorTO();
			$pvpvTO->setId_prevenda($arrProduto[0]->getId_prevenda());
			if(!$this->dao->excluirPreVendaProdutoValor($pvpvTO)){
				throw new Zend_Exception('Erro ao excluir produtos antigos');
			}
			foreach ($arrProduto as $preVendaProdutoValorTO){
				if(!$this->dao->cadastrarPreVendaProdutoValor($preVendaProdutoValorTO)){
					throw new Zend_Exception('Erro ao fazer insert');
				}
			}
			return $this->mensageiro->setMensageiro('Produto(s) salvo(s) com sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Pré-Venda com produto valor!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo cadastrar a pre venda com produto
	 * @param $arrProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVendaProduto($arrProduto){
		try{
			$pvpTO = new PreVendaProdutoTO();
			$pvpTO->setId_prevenda($arrProduto[0]->getId_prevenda());
			foreach ($arrProduto as $preVendaProdutoTO){
				if(!$this->dao->cadastrarPreVendaProduto($preVendaProdutoTO)){
					throw new Zend_Exception('Erro ao fazer insert');
				}
			}
			return $this->mensageiro->setMensageiro('Produto(s) salvo(s) com sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			Zend_Debug::dump($e->getMessage(),__CLASS__.'('.__LINE__.')');exit;
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Produto da Pré-venda!',Ead1_IMensageiro::ERRO, $e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar a pre venda com produto valor
	 * @param PreVendaProdutoValorTO $pvpvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaProdutoValor(VwPreVendaProdutoValorTO $pvpvTO){
		try{
			$dados = $this->dao->retornarPreVendaProdutoValor($pvpvTO);
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Produto Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPreVendaProdutoValorTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Produtos!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar a pre venda com produto
	 * @param PreVendaProdutoTO $pvpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaProduto(VwPreVendaProdutoTO $pvpTO){
		try{
			$dados = $this->dao->retornarPreVendaProduto($pvpTO);
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Produto Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPreVendaProdutoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Produtos!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar a pre venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVenda(PreVendaTO $pvTO){
		try{
			$dados = $this->dao->retornarPreVenda($pvTO);
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Pré-Venda Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new PreVendaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Pré-Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Método que retorna as pre vendas disponiveis ao distribuidor logado
	 * @param VwPrevendaDistribuicaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwPreVendaDistribuicao(VwPrevendaDistribuicaoTO $to){
		try{
			$to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
			$orm = new VwPrevendaDistribuicaoORM();
			$where = '';
			
			$id_nucleotm = $to->getId_nucleotm();
			if($id_nucleotm){
				$to->setId_nucleotm(null);
				$where = $orm->montarWherePesquisa($to,false,true);
				$where .= ($where ? " AND " : "")." (id_nucleotm = ".$id_nucleotm." or id_nucleotm is null) ";
				$to->setId_nucleotm($id_nucleotm);
			} else {
				$where = $orm->montarWherePesquisa($to,false,true);
			}
			
			if($to->getDt_inicio()){
				$where .= ($where ? " AND " : "")." dt_cadastro >= '".$to->getDt_inicio()->toString('Y-MM-dd')."'";
			}
			if($to->getDt_termino()){
				$where .= ($where ? " AND " : "")." dt_cadastro <= '".$to->getDt_termino()->toString('Y-MM-dd')."'";
			}
			
			$dados = $this->dao->retornarVwPreVendaDistribuicao($to,$where)->toArray();
			if(empty($dados)){
				//return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Distribuição Encontrada!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPrevendaDistribuicaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Distribuição da Pré-Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna uma lista das pessoas que batem com as informações de nome completo cpf e email
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarListaPessoasPreVenda(PreVendaTO $pvTO){
		try{
			$orm = new VwPessoaORM();
			$pvTO->setId_entidade(($pvTO->getId_entidade() ? $pvTO->getId_entidade() : $pvTO->getSessao()->id_entidade));
			$where = $orm->select()->from(array('vw' => 'vw_pessoa'))
								->setIntegrityCheck(false)
								->joinInner(array('pv' => 'tb_prevenda'),
								'pv.st_nomecompleto = vw.st_nomecompleto or pv.st_cpf = vw.st_cpf or pv.st_email = vw.st_email',"")
								->where(
										'vw.id_entidade = '.$pvTO->getId_entidade()
										.' AND pv.id_prevenda = '.$pvTO->getId_prevenda()
										.($pvTO->getId_usuario() ? ' AND vw.id_usuario != '.$pvTO->getId_usuario() : '')
										);
			$pessoaDAO = new PessoaDAO();
			$dados = $pessoaDAO->retornarVwPessoa(new VwPessoaTO(),$where)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encotnrado!',Ead1_IMensageiro::AVISO);
			}else{
				$arrDadosResposta = array();
				foreach ($dados as $dado){
					$arrDadosResposta[$dado['id_usuario']] = $dado;
				}
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrDadosResposta, new VwPessoaTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Lista de Pessoas!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna a soma das possiveis duplicidades de pessoas e a soma de suas vendas pendentes
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaCountPessoasVendaPendentes(PreVendaTO $pvTO){
		try{
			$orm = new VwPessoaORM();
			$pvTO->setId_entidade(($pvTO->getId_entidade() ? $pvTO->getId_entidade() : $pvTO->getSessao()->id_entidade));
			$where = $orm->select()->from(array('vw' => 'vw_pessoa'),array('id_usuario'))
								->setIntegrityCheck(false)
								->joinInner(array('pv' => 'tb_prevenda'),
								'pv.st_nomecompleto = vw.st_nomecompleto or pv.st_cpf = vw.st_cpf or pv.st_email = vw.st_email',"")
								->where('vw.id_entidade = '.$pvTO->getId_entidade().' AND pv.id_prevenda = '.$pvTO->getId_prevenda()
										.($pvTO->getId_usuario() ? ' AND vw.id_usuario != '.$pvTO->getId_usuario():''));
			$pessoaDAO = new PessoaDAO();
			$retornoPessoas = $pessoaDAO->retornarVwPessoa(new VwPessoaTO(),$where)->toArray();
			$pessoas = array();
			if(!empty($retornoPessoas)){
				foreach ($retornoPessoas as $pessoa){
					$pessoas[$pessoa['id_usuario']] = $pessoa['id_usuario'];
				}
			}
			$masterTO = new MasterTO();
			$vendasPendentes = array();
			if($pvTO->getId_usuario()){
				$produtoDAO = new ProdutoDAO();
				$EVOLUCAO_CONFIRMADA = 10;
				$where = 'id_entidade = '.$pvTO->getId_entidade().
					 ' AND id_evolucao != '.$EVOLUCAO_CONFIRMADA.' AND id_usuario = '.$pvTO->getId_usuario();
				$vendasPendentes = $produtoDAO->retornarVendaUsuario(new VwVendaUsuarioTO(),$where)->toArray();
			}
			$masterTO->nu_pessoas = count($pessoas);
			$masterTO->nu_vendas = count($vendasPendentes);
			$this->mensageiro->setMensageiro($masterTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar a Quantidade de Possiveis Duplicidades e Vendas Pendentes!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna a pesquisa de PreVenda por periodo
	 * @param PesquisarPreVendaTO $ppvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaPeriodo(PesquisarPreVendaTO $ppvTO){
		try{
			if(!$ppvTO->getId_entidade()){
				$ppvTO->setId_entidade($ppvTO->getSessao()->id_entidade);
			}
			$arrWhere = array();
			$ppvTO->tipaTOConsulta();
			if($ppvTO->getDt_inicio()){
				$dt_inicio = new Zend_Date($ppvTO->getDt_inicio(),Zend_Date::ISO_8601);
				$arrWhere[] = " dt_cadastro >= '".$dt_inicio->toString('yyyy-MM-dd')."' ";
			}
			if($ppvTO->getDt_termino()){
				$dt_termino = new Zend_Date($ppvTO->getDt_termino(),Zend_Date::ISO_8601);
				$arrWhere[] = " dt_cadastro <= '".$dt_termino->toString('yyyy-MM-dd')."' ";
			}
			$arrWhere[] = "id_entidade = ".$ppvTO->getId_entidade();
			$where = implode(' AND ', $arrWhere);
			$dados = $this->dao->retornarPreVenda(new PreVendaTO(),$where)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhuma Pré-Venda Encontrada!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new PreVendaTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Pré-Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna as formas de Pagamento pelo Meio de Pagamento da pre venda
	 * @param PesquisarFormaPagamentoXParcelaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarComboFormaPagamentoPreVenda(PesquisarFormaPagamentoXParcelaTO $to){
		try{
			$formaPagamentoDAO = new FormaPagamentoDAO();
			$formaPagamentoORM = new FormaPagamentoORM();
			if(!$to->getId_entidade()){
				$to->setId_entidade($to->getSessao()->id_entidade);
			}
			$select = $formaPagamentoORM->select()
														->setIntegrityCheck(false)
														->from(array('fp' => 'tb_formapagamento'),
																		array('fp.id_formapagamento','fp.id_entidade','fp.nu_valorminparcela'))
														->joinInner(array('fpar' => 'tb_formapagamentoaplicacaorelacao'), 'fpar.id_formapagamento = fp.id_formapagamento',
																		NULL)
														->joinInner(array('fpa' => 'tb_formapagamentoaplicacao'), 'fpar.id_formapagamentoaplicacao = fpa.id_formapagamentoaplicacao',
																		array('fpa.id_formapagamentoaplicacao'))
														->joinInner(array('fpp' => 'tb_formapagamentoparcela'), 'fpp.id_formapagamento = fp.id_formapagamento',
																		array('fpp.id_meiopagamento','fpp.id_formapagamentoparcela','fpp.nu_parcelaquantidademin','fpp.nu_parcelaquantidademax','fpp.nu_juros'))
														->where('fpa.id_formapagamentoaplicacao = 3 AND 
																fpp.id_meiopagamento = '.$to->getId_meiopagamento().' AND
																 fp.id_entidade = '.$to->getId_entidade());
			
			$dados = $formaPagamentoDAO->retornarFormaPagamento(new FormaPagamentoTO(),$select)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Não Existe uma Forma de Pagamento da Pré Venda Cadastrada Para este Meio de Pagamento!',Ead1_IMensageiro::AVISO);
			}else{
				$dados = Ead1_TO_Dinamico::encapsularTo($dados, new PesquisarFormaPagamentoXParcelaTO());
				$arrParcelaJuros = array();
				$nu_minparcelas = null;
				$nu_maxparcelas = null;
				//Organizando juros por parcela
				foreach ($dados as $dado){
					$nu_minparcelas = (!$nu_minparcelas ? $dado->getNu_parcelaquantidademin() : $nu_minparcelas);
					$nu_maxparcelas = (!$nu_maxparcelas ? $dado->getNu_parcelaquantidademax() : $nu_maxparcelas);
					$nu_valorminparcela = $dado->getNu_valorminparcela();
					if($dado->getNu_parcelaquantidademin() < $nu_minparcelas){
						$nu_minparcelas = $dado->getNu_parcelaquantidademin();
					}
					if($dado->getNu_parcelaquantidademax() < $nu_maxparcelas){
						$nu_maxparcelas = $dado->getNu_parcelaquantidademax();
					}
					for($i = $dado->getNu_parcelaquantidademin(); $i <= $dado->getNu_parcelaquantidademax(); $i++){
						$arrParcelaJuros['parcela'][$i]['to'] = $dado;
						$arrParcelaJuros['parcela'][$i]['juros'] = $dado->getNu_juros();
					}
				}
				$arrParcelas = array();
				$nu_valor = $to->getNu_valorvenda();
				for($nu_parcela = $nu_minparcelas ;$nu_parcela <= $nu_maxparcelas; $nu_parcela++){
					$nu_valor += (($nu_valor * $arrParcelaJuros['parcela'][$nu_parcela]['juros'])/100);
					$nu_valorparcela = ($nu_valor/($nu_parcela ? $nu_parcela : 1));
					if($nu_valorparcela >= $arrParcelaJuros['parcela'][$nu_parcela]['to']->getNu_valorminparcela()){
						$parcelaTO = new PesquisarFormaPagamentoXParcelaTO();
						$parcelaTO->setId_formapagamento($arrParcelaJuros['parcela'][$nu_parcela]['to']->getId_formapagamento());
						$parcelaTO->setId_entidade($arrParcelaJuros['parcela'][$nu_parcela]['to']->getId_entidade());
						$parcelaTO->setId_formapagamentoaplicacao($arrParcelaJuros['parcela'][$nu_parcela]['to']->getId_formapagamentoaplicacao());
						$parcelaTO->setId_formapagamentoparcela($arrParcelaJuros['parcela'][$nu_parcela]['to']->getId_formapagamentoparcela());
						$parcelaTO->setNu_juros($arrParcelaJuros['parcela'][$nu_parcela]['juros']);
						$parcelaTO->setNu_parcelaquantidademax($nu_maxparcelas);
						$parcelaTO->setNu_parcelaquantidademin($nu_minparcelas);
						$parcelaTO->setNu_valorminparcela($arrParcelaJuros['parcela'][$nu_parcela]['to']->getNu_valorminparcela());
						$parcelaTO->setId_meiopagamento($arrParcelaJuros['parcela'][$nu_parcela]['to']->getId_meiopagamento());
						$parcelaTO->setNu_valorparcela(round($nu_valorparcela,2));
						$parcelaTO->setNu_parcelas($nu_parcela);
						$parcelaTO->setNu_valorvenda(round($parcelaTO->getNu_valorparcela() * ($parcelaTO->getNu_parcelas() == 0 ? 1 : $parcelaTO->getNu_parcelas()),2));
						$parcelaTO->setSt_valorparcela(($nu_parcela == 0 ? 'A Vista ' : $nu_parcela.' x ').
														Zend_Locale_Format::toNumber($parcelaTO->getNu_valorparcela(),
																					array('number_format' => 'R$ #,##0.00')).
														($arrParcelaJuros['parcela'][$nu_parcela]['juros'] ? ' c/ juros' : ''));
						$arrParcelas[] = $parcelaTO;
					}else{
						break;
					}
				}
				$this->mensageiro->setMensageiro($arrParcelas,Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Formas de Pagamento!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo exclui produto
	 * @param PreVendaProdutoValorTO $pvpvTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirPreVendaProdutoValor(PreVendaProdutoValorTO $pvpvTO){
		try{
			$this->dao->excluirPreVendaProdutoValor($pvpvTO);
			return $this->mensageiro->setMensageiro('Produto Excluído',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Produto!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo exclui produto da pré venda
	 * @param PreVendaProdutoTO $pvpTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirPreVendaProduto(PreVendaProdutoTO $pvpTO){
		try{
			$this->dao->excluirPreVendaProduto($pvpTO);
			return $this->mensageiro->setMensageiro('Produto Excluído',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Produto!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que verifica dados do usuário a partir do CPF.
	 * @param UsuarioTO $uTO
	 * @return Ead1_Mensageiro
	 */
	public function verificarCpfPreVenda(UsuarioTO $uTO){
		try{
			$preVendaTO = new PreVendaTO();
			$preVendaTO->setSt_cpf($uTO->getSt_cpf());
			$pessoaBO = new PessoaBO();
			$usuario = $pessoaBO->retornaUsuario($uTO);
			if(!$usuario){
				//return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado usuário encontrado com esse CPF!',Ead1_IMensageiro::AVISO);
			}
			$uTO = $usuario;
			$preVendaTO->setId_usuario($uTO->getId_usuario());
			$preVendaTO->setSt_nomecompleto($uTO->getSt_nomecompleto());
			$pessoa = $pessoaBO->retornaPessoa($uTO);
			if(empty($pessoa)){
				return $this->mensageiro->setMensageiro($preVendaTO,Ead1_IMensageiro::SUCESSO);
			}
			$preVendaTO->setId_paisnascimento($pessoa['id_pais']);
			$preVendaTO->setId_municipionascimento($pessoa['id_municipio']);
			$preVendaTO->setSg_ufnascimento($pessoa['sg_uf']);
			$preVendaTO->setSt_sexo($pessoa['st_sexo']);
			$preVendaTO->setDt_nascimento(new Zend_Date($pessoa['dt_nascimento'],Zend_Date::ISO_8601));
			$preVendaTO->setId_entidade($pessoa['id_entidade']);
			$email = $pessoaBO->retornaEmails($uTO);
			if(!empty($email)){
				$vwEmailTO = new VwPessoaEmailTO();
				$vwEmailTO = $email[0];
				$preVendaTO->setSt_email($vwEmailTO->getSt_email());
			}
			$telefones = $pessoaBO->retornaTelefones($uTO);
			if(!empty($telefones)){
				foreach($telefones as $vwPessoaTelefoneTO){
					switch ($tipo = $vwPessoaTelefoneTO->getSt_tipotelefone()){
						case $tipo == 'Celular':
							$preVendaTO->setNu_dddcelular($vwPessoaTelefoneTO->getNu_ddd());
							$preVendaTO->setNu_telefonecelular($vwPessoaTelefoneTO->getNu_telefone());
							$preVendaTO->setNu_ddicelular($vwPessoaTelefoneTO->getNu_ddi());
						break;
						case $tipo == 'Residencial':
							$preVendaTO->setNu_dddresidencial($vwPessoaTelefoneTO->getNu_ddd());
							$preVendaTO->setNu_telefoneresidencial($vwPessoaTelefoneTO->getNu_telefone());
							$preVendaTO->setNu_ddiresidencial($vwPessoaTelefoneTO->getNu_ddi());
						break;
						default:
							$preVendaTO->setNu_dddcomercial($vwPessoaTelefoneTO->getNu_ddd());
							$preVendaTO->setNu_telefonecomercial($vwPessoaTelefoneTO->getNu_telefone());
							$preVendaTO->setNu_ddicomercial($vwPessoaTelefoneTO->getNu_ddi());
						break;
					}
				}
			}
			$identidade = $pessoaBO->retornaIdentidade($uTO);
			if(!empty($identidade)){
				$documentoIdentidadeTO = new DocumentoIdentidadeTO();
				$documentoIdentidadeTO = $identidade;
				$preVendaTO->setSt_rg($documentoIdentidadeTO->getSt_rg());
				$preVendaTO->setSt_orgaoexpeditor($documentoIdentidadeTO->getSt_orgaoexpeditor());
			}
			$enderecos = $pessoaBO->retornaEnderecos($uTO);
			if(!empty($enderecos)){
				foreach($enderecos as $endereco){
					if($endereco->getBl_padrao()){
						$preVendaTO->setSt_endereco($endereco->getSt_endereco());
						$preVendaTO->setSt_complemento($endereco->getSt_complemento());
						$preVendaTO->setNu_numero($endereco->getNu_numero());
						$preVendaTO->setSt_bairro($endereco->getSt_bairro());
						$preVendaTO->setId_pais($endereco->getId_pais());
						$preVendaTO->setId_municipio($endereco->getId_municipio());
						$preVendaTO->setSg_uf($endereco->getSg_uf());
						$preVendaTO->setSt_cep($endereco->getSt_cep());
						$preVendaTO->setId_tipoendereco($endereco->getId_tipoendereco());
					}
				}
			}
			return $this->mensageiro->setMensageiro($preVendaTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro verificar CPF!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
}

?>
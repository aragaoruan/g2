<?php
/**
 * Classe com regras de negócio para interações de Area de Conhecimento
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 29/09/2010
 * 
 * @package models
 * @subpackage bo
 */
class AreaBO extends Ead1_BO{
	
	public $mensageiro;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	private $areasPercorridas = array();
	
	private $areas;
		
	/**
	 * Metodo que Decide se Cadastra ou Edita Seria e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$orm = new AreaConhecimentoSerieNivelEnsinoORM();
		if($orm->consulta($acsneTO,true)){
			return $this->deletarAreaSerieNivel($acsneTO);
		}
		return $this->cadastrarAreaSerieNivel($acsneTO);
	}
	
	/**
	 * MEtodo que cadastra e exclui vinculos de Area de Conhecimento com Serie Nivel de Ensino
	 * @param array(AreaConhecimentoSerieNivelEnsinoTO $arrTO)
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayAreaSerieNivel($arrTO){
		$dao = new AreaDAO();
		if(!is_array($arrTO)){
			return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!',Ead1_IMensageiro::AVISO);
		}
		$delected = false;
		$dao->beginTransaction();
		try{
			foreach($arrTO as $acsneTO){
				if(!$delected){
					$delected = true;
					$delacsneTO = new AreaConhecimentoSerieNivelEnsinoTO();
					$delacsneTO->setId_areaconhecimento($acsneTO->getId_areaconhecimento());
					$delacsneTO->setId_nivelensino($acsneTO->getId_nivelensino());
					$dao->deletarAreaSerieNivel($delacsneTO);
				}
				if(!($acsneTO instanceof AreaConhecimentoSerieNivelEnsinoTO)){
					Throw new Zend_Exception('O Objeto não é uma Instancia da Classe AreaConhecimentoSerieNivelEnsinoTO!');
				}
				$dao->cadastrarAreaSerieNivel($acsneTO);
			}
			$dao->commit();
			return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$dao->rollBack();
			$this->mensageiro->setCodigo($dao->excecao);
			$this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
			$this->mensageiro->setMensagem("Erro ao Salvar Séries da Escolaridade!");
			return $this->mensageiro;
		}
	}
	
	/**
	 * Metodo que cadastra Area de Conhecimanto
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArea(AreaConhecimentoTO $acTO){
		$dao = new AreaDAO();
		if(!$acTO->getId_entidade()){
			$acTO->setId_entidade($acTO->getSessao()->id_entidade);
		}
		$acTO->setId_usuariocadastro((int)$acTO->getSessao()->id_usuario);
		$id_areaconhecimento = $dao->cadastrarArea($acTO);
		if(!$id_areaconhecimento){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Area de Conhecimento!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		$acTO->setId_areaconhecimento($id_areaconhecimento);
		
		$wsBO = new WsAreaBO(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
		$wsBO->cadastrarArea($acTO);
		
		return $this->mensageiro->setMensageiro($acTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Cadastra Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$dao = new AreaDAO();
		if(!$dao->cadastrarAreaSerieNivel($acsneTO)){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Série e Nível de Ensino!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Série da Área de Conhecimento Cadastrada com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	

	/**
	 * Metodo que cadastra entidades da área.
	 * @param array $arrayDeEntidades
	 * @param AreaConhecimentoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaEntidade($arrayDeEntidades, AreaConhecimentoTO $aTO){
		try{
			$dao = new AreaDAO();
			$aeTO = new AreaEntidadeTO();
			$aeTO->setId_areaconhecimento($aTO->getId_areaconhecimento());
			if(!$dao->excluirAreaEntidade($aeTO)){
				throw new Zend_Exception('Erro ao deletar '.Ead1_LabelFuncionalidade::getLabel(3).' antigas');
			}
			foreach ($arrayDeEntidades as $entidade){
				if(!$dao->cadastrarAreaEntidade($entidade)){
					throw new Zend_Exception('Erro no insert');
				}
			}
			return $this->mensageiro->setMensageiro(Ead1_LabelFuncionalidade::getLabel(3)."(s) cadastrado(s) com sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar '.Ead1_LabelFuncionalidade::getLabel(3).' para '.Ead1_LabelFuncionalidade::getLabel(20).'!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo Que Edita Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function editarArea(AreaConhecimentoTO $acTO){
		if(!$acTO->getId_entidade()){
			$acTO->setId_entidade($acTO->getSessao()->id_entidade);
		}
		$dao = new AreaDAO();
		if(!$dao->editarArea($acTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Area de Conhecimento!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Área do Conhecimento Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
		
	/**
	 * Metodo que Exclui a Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaConhecimento( AreaConhecimentoTO $areaConhecimentoTO ){
		
		$areaConhecimentoDAO	=	new AreaDAO();
		$areaConhecimentoDAO->beginTransaction();
		
		try{
			
			if( empty( $areaConhecimentoTO->id_areaconhecimento ) ) {
				throw new InvalidArgumentException( 'Para excluir uma área de conhecimento informe o código' );
			}
			
			$subAreas	= $areaConhecimentoDAO->retornaArea( new AreaConhecimentoTO( array( 'id_areaconhecimentopai' => $areaConhecimentoTO->id_areaconhecimento ) ) );
			
			if( $subAreas->count() > 0 ) {
				throw new DomainException( 'Não é possível excluir uma área de conhecimento que possua sub - áreas' );
			}
			
			$areaTO							= new AreaConhecimentoTO();
			$areaTO->id_areaconhecimento	= $areaConhecimentoTO->id_areaconhecimento;
			$areaTO->bl_ativo				= 0;
			
			if( $areaConhecimentoDAO->editarArea( $areaTO ) === false ) {
				throw new DomainException( 'Erro ao remover a área de conhecimento. Erro: '. $areaConhecimentoDAO->excecao );
			}
						
			$areaConhecimentoDAO->commit();
			$this->mensageiro->setMensageiro( 'Área de conhecimento removida com sucesso.', Ead1_IMensageiro::SUCESSO );
		} catch( Exception $exc ) {
			$areaConhecimentoDAO->rollBack();
			$this->mensageiro->setMensageiro( $exc->getMessage(), Ead1_IMensageiro::ERRO );
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que Edita Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$dao = new AreaDAO();
		if(!$dao->deletarAreaSerieNivel($acsneTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Série e Nível de Ensino!',Ead1_IMensageiro::ERRO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Area deletada com sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	
	/**
	 * Metodo que retorna Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArea(AreaConhecimentoTO $acTO){
		if(!$acTO->getId_entidade()){
			$acTO->setId_entidade($acTO->getSessao()->id_entidade);
		}
		$dao = new AreaDAO();
		$area = $dao->retornaArea($acTO);
		if(is_object($area)){
			$area = $area->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar Area de Conhecimento!',Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		if(empty($area)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($area, new AreaConhecimentoTO()),Ead1_IMensageiro::SUCESSO);
	}

	/**
	 * Metodo que retorna entidades de área
	 * @param AreaEntidadeTO $aeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAreaEntidade(AreaEntidadeTO $aeTO) {
		try{
			$dao = new AreaDAO();
			$dados = $dao->retornarAreaEntidade($aeTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de '.Ead1_LabelFuncionalidade::getLabel(3).' Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro($dados,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar '.Ead1_LabelFuncionalidade::getLabel(3).'!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Meotodo que retorna Nivel de Ensino e Serie da Area de Conhecimento
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$dao = new AreaDAO();
		$serieNivel = $dao->retornaAreaSerieNivel($acsneTO);
		if(is_object($serieNivel)){
		$serieNivel = $serieNivel->toArray();
		}else{
			unset($serieNivel);
		}
		if(!$serieNivel){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($serieNivel, new VwAreaConhecimentoNivelSerieTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Meotodo que retorna A Area e Projeto pedagogico
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwAreaProjetoPedagogico(VwProjetoAreaTO $paTO){
		$dao = new AreaDAO();
		$app = $dao->retornaVwAreaProjetoPedagogico($paTO);
		if(is_object($app)){
			$app = $app->toArray();
		}else{
			unset($app);
		}
		if(!$app){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($app, new VwProjetoAreaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna a Area de Conhecimento para o Componente da Arvore de Area de Conhecimento no Flex
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArvoreAreaConhecimento(AreaConhecimentoTO $acTO){
		try{
			if(!$acTO->getId_entidade()){
				$acTO->setId_entidade($acTO->getSessao()->id_entidade);
			}
			$entidadeTO = new EntidadeTO();
			$entidadeTO->setId_entidade($acTO->getId_entidade());
			$dao = new AreaDAO();
			$acTO->setBl_ativo(1);
			$arrAreas = $dao->retornarAreasPorEntidade($entidadeTO)->toArray();
			if(empty($arrAreas)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$arrAreas = Ead1_TO_Dinamico::encapsularTo($arrAreas, new AreaConhecimentoTO());
			$areas = array();
			$areasPai = array();
			foreach ($arrAreas as $index => $areaTO){
				if(!$areaTO->getId_areaconhecimentopai()){
					$areasPai[$areaTO->getId_areaconhecimento()]['to'] = $areaTO;
					$areasPai[$areaTO->getId_areaconhecimento()]['label'] = $areaTO->getSt_areaconhecimento();
					$areasPai[$areaTO->getId_areaconhecimento()]['children'] = null;
					$areasPai[$areaTO->getId_areaconhecimento()]['tipo'] = 'Area Conhecimento';
					continue;
				}
				$this->areas[$areaTO->getId_areaconhecimento()] = $areaTO; 
			}
			if(!empty($this->areas)){
				foreach ($areasPai as $index => $pai){
					$areasPai[$index]['children'] = $this->areaRecursiva($pai['to']);
				}
			}
			return $this->mensageiro->setMensageiro($areasPai,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Arvore de Areas de Conhecimento.',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que Retorna area de conhecimento recursivamente
	 * @param AreaConhecimentoTO $acTO
	 * @return object $dados;
	 */
 	private function areaRecursiva(AreaConhecimentoTO $acTO, $where = null){
 		$arrRecurssivo = array();
 		if($this->areas){
			foreach ($this->areas as $index => $areaTO){
				if($areaTO->getId_areaconhecimentopai() == $acTO->getId_areaconhecimento()){
					$arrRecurssivo[$index]['to'] = $areaTO;
					$arrRecurssivo[$index]['label'] = $areaTO->getSt_areaconhecimento();
					$arrRecurssivo[$index]['tipo'] = 'Area Conhecimento';
					$arrRecurssivo[$index]['children'] = $this->areaRecursiva($areaTO);
				}
			}
 		}
		return (empty($arrRecurssivo) ? null : $arrRecurssivo);
 	}
 	
 	/**
 	 * Metodo que Retorna O Projeto Pedagógico Pela Area de Conhecimento para o Componente da Arvore no Flex
 	 * @param $acTO
 	 * @return Ead1_Mensageiro
 	 */
 	public function retornaArvoreAreaProjetoPedagogico(AreaConhecimentoTO $acTO){
 		if(!$acTO->getId_entidade()){
			$acTO->setId_entidade($acTO->getSessao()->id_entidade);
		}
		try{
	 		$acTO->setBl_ativo(1);
	 		$arvore = (array) $this->areaProjetoRecursiva($acTO);
	 		return $this->mensageiro->setMensageiro($arvore,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Arvore!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
 	}
	
 	/**
 	 * Metodo que retorna a area e o projeto recursivamente
 	 * @param AreaConhecimentoTO $acTO
 	 * @return $dados
 	 */
 	private function areaProjetoRecursiva(AreaConhecimentoTO $acTO){
 		try{
	 		$dao = new AreaDAO();
	 		$obj = $dao->retornaArea($acTO);
	 		if($obj->count() > 0){
	 			$dados = array();
	 			foreach($obj as $chave => $dado){
			 		if(!in_array($dado->id_areaconhecimento,$this->areasPercorridas)){
				 		$ac_TO = new AreaConhecimentoTO();
				 		$ac_TO->setId_entidade($acTO->getId_entidade());
				 		$ac_TO->setBl_ativo(1);
				 		$ac_TO->setId_areaconhecimentopai($dado->id_areaconhecimento);
				 		$dados[$dado->id_areaconhecimento]['label'] = $dado->st_areaconhecimento;
				 		$dados[$dado->id_areaconhecimento]['tipo'] = 'Área de Conhecimento';
				 		$retorno[0] = $dado->toArray();
		 				$dados[$dado->id_areaconhecimento]['to'] = Ead1_TO_Dinamico::encapsularTo($retorno, new AreaConhecimentoTO(), true);
		 				$ap_TO = new VwProjetoAreaTO();
		 				$ap_TO->setId_areaconhecimento($dado->id_areaconhecimento);
		 				//o
		 				$areaRecursiva = (array)$this->areaRecursiva($ac_TO);
		 				$projetoRecursivo = (array)$this->retornaArrayProjetoArea($ap_TO);
		 				if(empty($areaRecursiva) && empty($projetoRecursivo)){
			 				$dados[$dado->id_areaconhecimento]['children'] = null;
		 				}elseif(empty($areaRecursiva) && !empty($projetoRecursivo)){
			 				$dados[$dado->id_areaconhecimento]['children'] = $projetoRecursivo;
		 				}elseif(!empty($areaRecursiva) && empty($projetoRecursivo)){
			 				$dados[$dado->id_areaconhecimento]['children'] = $areaRecursiva;
		 				}else{
			 				$arrMerged = array_merge($areaRecursiva,$projetoRecursivo);
			 				$dados[$dado->id_areaconhecimento]['children'] = $arrMerged;
		 				}
			 			$dados[$dado->id_areaconhecimento] = (object) $dados[$dado->id_areaconhecimento];
			 			$this->areasPercorridas[] = $dado->id_areaconhecimento;
			 		}
	 			}
	 		}else{
	 			return null;
	 		}
	 		return $dados;
 		}catch (Zend_Exception $e){
 			THROW new Zend_Exception($e->getMessage());
 		}
 	}
 	
 	/**
 	 * Metodo Recursivo que retorna um Array para o Metodo areaProjetoRecursiva
 	 * @param AreaProjetoPedagogicoTO $appTO
 	 * @return $dados
 	 */
 	private function retornaArrayProjetoArea(VwProjetoAreaTO $paTO){
 		try{
	 		$dao = new AreaDAO();
	 		$pa = $dao->retornaVwAreaProjetoPedagogico($paTO);
	 		if($pa->count() > 0){
		 		$dados = array();
		 		foreach($pa as $chave => $projetoArea){
		 			$dados[$chave]['label'] = $projetoArea->st_tituloexibicao;
		 			$retorno[0] = $projetoArea->toArray();
		 			$dados[$chave]['to'] = Ead1_TO_Dinamico::encapsularTo($retorno, new VwProjetoAreaTO(), true);
		 			$dados[$chave]['tipo'] = 'Projeto Pedagógico';
		 			$dados[$chave]['children'] = null;
		 			$dados[$chave] = (object) $dados[$chave];
		 		}
	 		}else{
	 			$dados = null;
	 		}
	 		return $dados;
 		}catch (Zend_Exception $e){
 			THROW new Zend_Exception($e->getMessage());
 		}
 	}
 	
 	public function retornarTipoAreaConhecimento( TipoAreaConhecimentoTO $tipoAreaConhecimentoTO ) {
 		
 		try {
 			
 			$areaDAO	= new AreaDAO();
 			$rowset		= $areaDAO->retornarTipoAreaConhecimento( $tipoAreaConhecimentoTO );
 			
 			if( $rowset->count() < 1 ) {
 				throw new ErrorException( 'Não foi encontrada nenhum tipo de área de conhecimento' );
 			}
 			
 			$this->mensageiro->setMensageiro( Ead1_TO_Dinamico::encapsularTo( $rowset, new TipoAreaConhecimentoTO() ), Ead1_IMensageiro::SUCESSO  );
 		} catch( Exception $exc ) {
 			$this->mensageiro->setMensageiro( 'Erro ao listar os tipos de conhecimento. '. $exc->getMessage(), Ead1_IMensageiro::ERRO  );
 		}
 		
 		return $this->mensageiro;
 	}
}
<?php

/**
 * Classe com regras de negócio para interações de Núcleo
 * @author João Gabriel - jgvasconcelos16@gmail.com
 * @since 11/11/2011
 *
 * @package models
 * @subpackage bo
 */
class NucleotmBO extends Ead1_BO
{

    /**
     * @var NucleotmDAO
     */
    private $dao;

    public $mensageiro;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new NucleotmDAO();
    }

    /**
     * Metodo que retorna núcleo
     * @param NucleotmTO $nTO
     * @return Ead1_Mensageiro
     */
    public function retornarNucleoCompleto(NucleotmTO $nTO)
    {
        try {
            $where = 'n.bl_ativo = 1 ';
            if ($nTO->getId_entidade()) {
                $where .= 'and n.id_entidade = ' . $nTO->getId_entidade();
            }
            if ($nTO->getId_entidadematriz()) {
                $where .= ' and n.id_entidadematriz = ' . $nTO->getId_entidadematriz();
            }
            $dados = $this->dao->retornarNucleoCompleto($nTO, $where)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Núcleo Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new NucleotmCompletoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Núcleo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna funcionários do núcleo
     * 
     * Modificado por Rafael Bruno (RBD) em 11/12/2014
     * 
     * @param NucleoFuncionariotmTO $nfTO
     * @return Ead1_Mensageiro
     */
    public function retornarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO)
    {
        try {
            $where = " nf.bl_ativo = 1 AND n.id_entidade = {$nfTO->getSessao()->id_entidade}";
            $where .= ($nfTO->getId_nucleotm() ? " AND nf.id_nucleotm = " . $nfTO->getId_nucleotm() : "");
            $where .= ($nfTO->getId_funcao() ? " AND nf.id_funcao = " . $nfTO->getId_funcao() : "");
            $where .= (" AND s.id_situacao = ". \G2\Constante\Situacao::TB_NUCELO_FUNCIONARIO_TM_ATIVO);

            $dados = $this->dao->retornarFuncionarioNucleo($nfTO, $where);
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Funcionário Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados->toArray(), new NucleoFuncionariotmCompletoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Funcionários!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna funcionários do núcleo de uma determinada entidade
     * @param NucleoFuncionariotmTO $nfTO
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarFuncionarioNucleoEntidade(NucleoFuncionariotmTO $nfTO, EntidadeTO $eTO)
    {
        try {
            $dados = array();
            $nucleos = $this->retornarNucleo(new NucleotmTO(array('id_entidade' => $eTO->getId_entidade())));
            $id_nucleos = array();
            foreach ($nucleos as $nucleo) {
                $id_nucleos[$nucleo['id_nucleotm']] = $nucleo['id_nucleotm'];
            }
            $where = " nf.bl_ativo = 1 " . ($nfTO->getId_nucleotm() ? " AND n.id_nucleotm = " . $nfTO->getId_nucleotm() : "")
                . ($nfTO->getId_funcao() ? " AND nf.id_funcao = " . $nfTO->getId_funcao() : "")
                . " AND n.id_nucleotm in (" . implode(", ", $id_nucleos) . ")";
            $dados = $this->dao->retornarFuncionarioNucleo($nfTO, $where)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhum Registro de Funcionário Encontrado!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new NucleoFuncionariotmCompletoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Funcionários!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna produtos do núcleo
     * @param NucleoProdutoTO $npTO
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoNucleo(NucleoProdutoTO $npTO)
    {
        try {
            $dados = $this->dao->retornarProdutoNucleo($npTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Produto para este Núcleo Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new NucleoProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Produtos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna os nucleos
     * @param NucleotmTO $to
     * @return Array $dados
     */
    public function retornarNucleo(NucleotmTO $to, $where = null)
    {
        $dados = array();
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarNucleo($to, $where)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhum Núcleo Encontrado!');
            }
        } catch (Zend_Validate_Exception $e) {
            THROW $e;
        } catch (Zend_Exception $e) {
            THROW $e;
        }
        return $dados;
    }

    /**
     * Método que retorna os nucleos da funcao da pessoa logada
     * @param NucleotmTO $nucleoTO
     * @param FuncaoTO $funcaoTO
     * @return Array $dados
     */
    public function retornarNucleoFuncao(NucleotmTO $nucleoTO, FuncaoTO $funcaoTO)
    {
        $nucleoTO->setId_entidade(($nucleoTO->getId_entidade() ? $nucleoTO->getId_entidade() : $nucleoTO->getSessao()->id_entidade));
        $where = "id_entidade = " . $nucleoTO->getId_entidade() . " AND id_nucleotm in (
					select id_nucleotm from tb_nucleofuncionariotm where id_usuario = " . $nucleoTO->getSessao()->id_usuario . "
					 AND bl_ativo = 1 AND id_funcao = " . $funcaoTO->getId_funcao() . "
				)";
        return $this->retornarNucleo($nucleoTO, $where);
    }


    /**
     * Metodo cadastra núcleo
     * @param NucleotmTO $nTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarNucleo(NucleotmTO $nTO)
    {
        try {
            if (!$nTO->getId_entidade()) {
                $nTO->setId_entidade($nTO->getSessao()->id_entidade);
            }
            if (!$nTO->getId_entidadematriz()) {
                $nTO->setId_entidadematriz($nTO->getSessao()->id_entidade);
            }
            if (!$nTO->getId_usuariocadastro()) {
                $nTO->setId_usuariocadastro($nTO->getSessao()->id_usuario);
            }
            $id_nucleotm = $this->dao->cadastrarNucleo($nTO);
            if (!$id_nucleotm) {
                throw new Zend_Exception('Erro ao fazer insert');
            }
            $nTO->setId_nucleotm($id_nucleotm);
            return $this->mensageiro->setMensageiro($nTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Núcleo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo cadastra funcionário de núcleo
     * @param NucleoFuncionariotmTO $nfTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO)
    {
        try {
            if (!$nfTO->getId_usuariocadastro()) {
                $nfTO->setId_usuariocadastro($nfTO->getSessao()->id_usuario);
            }
            $id_nucleofuncionariotm = $this->dao->cadastrarFuncionarioNucleo($nfTO);
            if (!$id_nucleofuncionariotm) {
                throw new Zend_Exception('Erro ao fazer insert');
            }
            $nfTO->setId_nucleofuncionariotm($id_nucleofuncionariotm);
            return $this->mensageiro->setMensageiro($nfTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Funcionário!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo cadastra produtos de núcleo
     * @param NucleotmTO $nTO
     * @param array $arrNucleoProduto
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoNucleo(NucleotmTO $nTO, $arrNucleoProduto)
    {
        try {
            $npTO = new NucleoProdutoTO();
            $npTO->setId_nucleotm($nTO->getId_nucleotm());
            if (!$this->dao->excluirNucleoProduto($npTO)) {
                throw new Zend_Exception('Erro ao deletar núcleos antigos');
            }
            foreach ($arrNucleoProduto as $produto) {
                $produto->id_usuariocadastro = $produto->getSessao()->id_usuario;
                $produto->id_nucleotm = $nTO->getId_nucleotm();
                if (!$this->dao->cadastrarProdutoNucleo($produto)) {
                    throw new Zend_Exception('Erro no insert');
                }
            }
            return $this->mensageiro->setMensageiro("Produto(s) cadastrado(s) com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Produtos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo edita núcleo
     * @param NucleotmTO $nTO
     * @return Ead1_Mensageiro
     */
    public function editarNucleo(NucleotmTO $nTO)
    {
        try {
            if (!$this->dao->editarNucleo($nTO)) {
                throw new Zend_Exception('Erro ao fazer update');
            }
            return $this->mensageiro->setMensageiro('Núcleo editado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Núcleo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo edita funcionário de núcleo
     * @param NucleoFuncionariotmTO $nfTO
     * @return Ead1_Mensageiro
     */
    public function editarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO)
    {
        try {
            if (!$this->dao->editarFuncionarioNucleo($nfTO)) {
                throw new Zend_Exception('Erro ao fazer update');
            }
            return $this->mensageiro->setMensageiro('Funcionário editado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Funcionário!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }
}

?>
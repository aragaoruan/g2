<?php

/**
 * Classe de regra de negócio de mensagem
 * @author eduardoromao
 */


use G2\Constante\ItemConfiguracao;
use G2\Constante\Entidade;

class MensagemBO extends Ead1_BO
{

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro ();
        $this->_dao = new MensagemDAO ();
    }

    const MAX_MENSAGENS_POR_CONFIGURACAO = 50;


    public $mensageiro;

    /**
     * Contem a classe de abstração de Mensagem
     * @var MensagemDAO
     */
    private $_dao;

    /**
     * Contem o Log do Envio das Mensagens
     * @var array
     */
    private $_log;
    private $zendexceppadraomsg = " não informada para a geração de mensagem de ";

    /**
     * @return the $_log
     */
    public function getLog()
    {
        return $this->_log;
    }

    /**
     * @param $vwAvisoBoleto
     * @return Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function enviarNotificacaoMensagemCobranca(\G2\Entity\VwAvisoBoleto $vwAvisoBoleto)
    {

        $textoSistemaTO = new TextoSistemaTO();
        $textoSistemaTO->setId_textosistema($vwAvisoBoleto->getId_textosistema()->getId_textosistema());
        $textoSistemaTO->fetch(true, true, true);

        $lancamentoVendaTO = new \LancamentoVendaTO();
        $lancamentoVendaTO->setId_lancamento($vwAvisoBoleto->getId_lancamento()->getIdLancamento());
        $lancamentoVendaTO->fetch(false, true, true);

        $vendaTO = new \VendaTO();
        if ($lancamentoVendaTO->getId_venda()) {
            $vendaTO->setId_venda($lancamentoVendaTO->getId_venda());
            $vendaTO->fetch(true, true, true);
        } else {
            throw new \Exception('A venda do lancamento ' . $vwAvisoBoleto->getId_lancamento()->getIdLancamento() . ' não foi encontrada.');
        }

        $arParametros = array(
            'id_venda' => $lancamentoVendaTO->getId_venda(),
            'id_entidade' => $vendaTO->getId_entidade(),
            'id_lancamento' => $vwAvisoBoleto->getId_lancamento()->getIdLancamento()
        );

        $emailConfigTO = new EmailConfigTO();
        $emailConfigTO->setId_entidade($vendaTO->getId_entidade());
        $emailConfigTO->fetch(false, true, true);

        $negocio = new G2\Negocio\Negocio();

        $mc = $negocio->findOneBy('\G2\Entity\EnvioCobranca',
            array(
                'id_mensagemcobranca' => $vwAvisoBoleto->getId_mensagemcobranca()->getId_mensagemcobranca(),
                'id_lancamento' => $vwAvisoBoleto->getId_lancamento()->getIdLancamento()
            )
        );

        if (!is_null($mc) && $mc->getId_enviocobranca()) {
            throw new \Exception('Mensagem de cobrança - ' . $textoSistemaTO->getSt_textosistema() . ' já cadastrado.');
        }

        $mensageiro = $this->enviarNotificacaoTextoSistema($textoSistemaTO, $arParametros, $emailConfigTO->getId_emailconfig(), $vendaTO->getId_usuario(), $vendaTO->getId_entidade());

        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO && $mensageiro->getCodigo()->getId_mensagem()) {

            $envioCobranca = new \G2\Entity\EnvioCobranca();

            $envioCobranca->setId_lancamento($negocio->getReference('G2\Entity\Lancamento', $vwAvisoBoleto->getId_lancamento()->getIdLancamento()));
            $envioCobranca->setId_mensagem($negocio->getReference('G2\Entity\Mensagem', $mensageiro->getCodigo()->getId_mensagem()));
            $envioCobranca->setId_mensagemcobranca($negocio->getReference('G2\Entity\MensagemCobranca', $vwAvisoBoleto->getId_mensagemcobranca()->getId_mensagemcobranca()));

            $envioCobranca = $negocio->save($envioCobranca);

            if (!$envioCobranca->getId_enviocobranca()) {
                throw new \Exception('Erro ao salvar EnvioCobranca');
            }
            /**
             * Modificação para a issue GII-4290 PA - Receber em 'Avisos Recebidos' no Portal Mensagem de Cobrança enviada pelo G2
             */
            //buscando a referencia para envio da mensagem ao portal do aluno
            $tb_mensagem = $negocio->getReference('G2\Entity\Mensagem', $mensageiro->getCodigo()->getId_mensagem());
            $mensagemPortal = new \G2\Negocio\MensagemPortal();
            //método que salvo o envio da mensagem para o portal do aluno
            $dataEnvio = new \DateTime();
            $tbEnvioMsg = $mensagemPortal->saveEnvioMensagem($tb_mensagem, array('dt_enviar' => $dataEnvio->format('Y-m-d')));
            //salvando o destinatario da mensagem.
            if ($tbEnvioMsg instanceof \G2\Entity\EnvioMensagem) {

                $matriculaLancamento = $negocio->findOneBy('G2\Entity\VwMatriculaLancamento',
                    array(
                        'id_venda' => $lancamentoVendaTO->getId_venda(),
                        'id_lancamento' => $vwAvisoBoleto->getId_lancamento()->getIdLancamento()
                    )
                );

                $envioDestinatario = $mensagemPortal->saveEnvioDestinatario($tbEnvioMsg, $tb_mensagem, $matriculaLancamento);
                if (is_array($envioDestinatario)) {
                    if (isset($envioDestinatario['type']) && $envioDestinatario['type'] == 'error') {
                        throw new \Exception($envioDestinatario['text']);
                    }
                }


            } else {
                throw new \Exception('Erro ao salvar EnvioMensagem. Erro:' . $tbEnvioMsg['text']);
            }
        } else {
            throw new \Exception('Erro ao salvar EnvioMensagem. Erro:' . $mensageiro->getFirstMensagem());
        }

        return $mensageiro;
    }

    /**
     *
     * Método que envia uma notificação a um usuário de acordo com um texto de sistema. Os parametros são um TO de texto com o id_textosistema,
     * um array de parametros generico que pode vir um id_ocorrencia, ou id_matricula, etc de acordo com o tipo de texto que vai gerar,
     * o id_emailconfig e o id_usuario
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @param int $id_emailconfig
     * @param int $id_usuario
     * @param int $id_entidade
     * @throws Zend_Exception
     */
    public function enviarNotificacaoTextoSistema(
        TextoSistemaTO $textoSistemaTO,
        array $params,
        $id_emailconfig,
        $id_usuario,
        $id_entidade = null,
        $saladeaulaPRR = null,
        $bl_enviaragora = null
    )
    {
        $textosSistemaBO = new TextoSistemaBO ();
        $textoSistemaTO = $textosSistemaBO->gerarTexto($textoSistemaTO, $params)
            ->subtractMensageiro()->getFirstMensagem();

        $mensagemTO = new MensagemTO ();
        $mensagemTO->setSt_mensagem(($textoSistemaTO->getSt_textosistema()));
        $mensagemTO->setSt_texto(($textoSistemaTO->getSt_texto()));
        $mensagemTO->setId_usuariocadastro((int)$mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setId_entidade((is_null($id_entidade) ? $mensagemTO->getSessao()->id_entidade : $id_entidade));
        if (array_key_exists('st_caminhoanexo', $params)) {
            $mensagemTO->setStCaminhoanexo($params['st_caminhoanexo']);
        }

//        Zend_Debug::dump($mensagemTO);die;
        $envioMensagemTO = new EnvioMensagemTO ();
        $envioMensagemTO->setId_tipoenvio(TipoEnvioTO::EMAIL);
        $envioMensagemTO->setId_emailconfig($id_emailconfig);

        $vwPessoaTO = new VwPessoaTO ();
        $vwPessoaTO->setId_usuario($id_usuario);
        $vwPessoaTO->setId_entidade((is_null($id_entidade) ? $vwPessoaTO->getSessao()->id_entidade : $id_entidade));
        $pessoaBO = new PessoaBO ();
        if ($pessoa = $pessoaBO->retornarVwPessoa($vwPessoaTO)) {
            $vwPessoaTO = Ead1_TO_Dinamico::encapsularTo($pessoa, new VwPessoaTO());
            $vwPessoaTO = $vwPessoaTO [0];
        } else {
            throw new Exception('Erro ao Retornar Pessoa.');
        }
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        $envioDestinatarioTO->setId_usuario($vwPessoaTO->getId_usuario());
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setSt_nome($vwPessoaTO->getSt_nomecompleto());
        $envioDestinatarioTO->setSt_endereco($vwPessoaTO->getSt_email());
        $arrEnvioDestinatario [] = $envioDestinatarioTO;

        if ($saladeaulaPRR) {
            return $this->procedimentoCadastrarMensagemEnvioDestinatarios($envioMensagemTO, $mensagemTO,
                $arrEnvioDestinatario, true);
        }

        if ($bl_enviaragora) {
            return $this->procedimentoCadastrarMensagemEnvioDestinatarios($envioMensagemTO, $mensagemTO, $arrEnvioDestinatario, true);
        }

        return $this->procedimentoCadastrarMensagemEnvioDestinatarios($envioMensagemTO, $mensagemTO, $arrEnvioDestinatario, false);
    }

    /**
     * Procedimento que cadastra a Mensagem, o Envio da Mensagem e Seus Destinatários, e se desejado Envia a Mensagem
     * @param EnvioMensagemTO $envioMensagemTO
     * @param MensagemTO $mensagemTO
     * @param array $arrEnvioDestinatario - EnvioDestinatarioTO
     * @param Boolean $bl_enviarAgora - envia logo apos o processo
     */
    public function procedimentoCadastrarMensagemEnvioDestinatarios(EnvioMensagemTO $envioMensagemTO, MensagemTO $mensagemTO, $arrEnvioDestinatario, $bl_enviarAgora = false)
    {
        $this->_dao->beginTransaction();
        try {
            $erro = false;

            $this->validaCadastroDestinatariosEnvioMensagem($envioMensagemTO, $arrEnvioDestinatario);
            $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
            $mensagemTO->setSt_mensagem($mensagemTO->getSt_mensagem());

            $mensagemTO = $this->cadastrarMensagem($mensagemTO);

            if ($mensagemTO->getTipo() == \Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception("Erro ao cadastrar mensagem");
            }

            $mensagemTO = $mensagemTO->subtractMensageiro()->getFirstMensagem();
            $envioMensagemTO->setId_mensagem($mensagemTO->getId_mensagem());
            $envioMensagemTO->setDt_envio($bl_enviarAgora ? new Zend_Date () : $envioMensagemTO->getDt_envio());

            $tmpEnvioMensagemTO = $this->cadastrarEnvioMensagem($envioMensagemTO)->subtractMensageiro()->getFirstMensagem();
            $envioMensagemTO->setId_enviomensagem($tmpEnvioMensagemTO->getId_enviomensagem());
            foreach ($arrEnvioDestinatario as $index => $envioDestinatarioTO) {
                $envioDestinatarioTO->setId_enviomensagem($envioMensagemTO->getId_enviomensagem());
                $arrEnvioDestinatario [$index] = $this->cadastrarEnvioDestinatario($envioDestinatarioTO)->subtractMensageiro()->getFirstMensagem();
            }
            $this->_dao->commit();
            $this->mensageiro->setMensageiro("Mensagem Cadastrada com Sucesso.", Ead1_IMensageiro::SUCESSO, $mensagemTO);
        } catch (Zend_Validate_Exception $e) {
            $this->_dao->rollBack();
            $erro = true;
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->_dao->rollBack();
            $erro = true;
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        if ($bl_enviarAgora && !$erro) {
            $this->procedimentoEnviarMensagem('id_enviomensagem = ' . $envioMensagemTO->getId_enviomensagem());
        }
        return $this->mensageiro;
    }

    /**
     * Procedimento de Envio de Mensagens
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function procedimentoEnviarMensagem($where = null)
    {
        try {
            $where = $where ? $where : "id_evolucao = " . EnvioMensagemTO::NAO_ENVIADO . " AND (dt_enviar <= getdate() OR dt_enviar is NULL) and (dt_tentativa <= (getdate() - 0.08) or dt_tentativa is null)";
            $mensagensEnvio = $this->_dao->retornarVwEnvioMensagem(new VwEnvioMensagemTO(), $where)->toArray();
            if (empty($mensagensEnvio)) {
                THROW new Zend_Validate_Exception("Nenhum Email a Ser Enviado.");
            }
            $mensagensEnvio = Ead1_TO_Dinamico::encapsularTo($mensagensEnvio, new VwEnvioMensagemTO());
            //Organiza Envio
            $arrEnvio = array();
            $arrContFila = array();
            foreach ($mensagensEnvio as $mensagens) {
                if (isset($arrContFila [$mensagens->getId_emailconfig()] ['quantidade'])) {
                    if ($arrContFila [$mensagens->getId_emailconfig()] ['quantidade'] >= MensagemBO::MAX_MENSAGENS_POR_CONFIGURACAO) {
                        continue;
                    }
                } else {
                    $arrContFila [$mensagens->getId_emailconfig()] ['quantidade'] = 0;
                }
                $envioMensagemTO = new EnvioMensagemTO ();
                $arrEnvio [$mensagens->getId_enviomensagem()] ['vwEnvioMensagemTO'] = $mensagens;
                $arrEnvio [$mensagens->getId_enviomensagem()] ['envioMensagemTO'] = $envioMensagemTO->mergeTO($envioMensagemTO, $mensagens, false, true);
                $arrEnvio [$mensagens->getId_enviomensagem()] ['destinatarios'] [$mensagens->getId_enviodestinatario()] = $mensagens;
                $arrContFila [$mensagens->getId_emailconfig()] ['quantidade'] += 1;
            }
            $this->_log = array();
            $this->_log ['sucesso'] = array();
            $this->_log ['erro'] = array();
            //Envia
            foreach ($arrEnvio as $index => $envio) {
                try {
                    $envioMensagemTO = $envio ['envioMensagemTO'];
                    switch ($envioMensagemTO->getId_tipoenvio()) {
                        case TipoEnvioTO::EMAIL :
                            $this->enviarEmail($envioMensagemTO, $envio ['destinatarios']);
                            break;
                        case TipoEnvioTO::SMS :
                            $vto = clone $envioMensagemTO;
                            $vto->fetch(true, true, true);
                            if ($vto->getId_evolucao() != EnvioMensagemTO::ENVIADO) {
                                $this->enviarSMS($envioMensagemTO, $envio ['destinatarios']);
                            } else {
                                continue;
                            }

                            break;
                        default :
                            throw new Zend_Exception("Tipo de Envio Não Implementado.");
                            break;
                    }

                    $envioMensagemTO->setDt_envio(new Zend_Date());
                    $envioMensagemTO->setId_evolucao(EnvioMensagemTO::ENVIADO);
                    $envioMensagemTO->setst_retorno("Enviado");
                    $this->editarEnvioMensagem($envioMensagemTO)->subtractMensageiro();
                    $this->_log ['sucesso'] [$index] ['recipe'] = $arrEnvio [$index];
                    $this->_log ['sucesso'] [$index] ['mensagem'] [] = "Enviado";
                } catch (Zend_Mail_Exception $e) {
                    $this->_log ['erro'] [$index] ['recipe'] = $arrEnvio [$index];
                    $this->_log ['erro'] [$index] ['bl_erro'] = false;
                    $this->_log ['erro'] [$index] ['mensagem'] [] = $e->getMessage();
                    $this->mensageiro->setMensageiro("Erro ao Enviar de Email.", Ead1_IMensageiro::ERRO, $e->getMessage());
                } catch (Ead1_Mail_Exception $e) {
                    $this->_log ['erro'] [$index] ['bl_erro'] = false;
                    $this->_log ['erro'] [$index] ['recipe'] = $arrEnvio [$index];
                    $this->_log ['erro'] [$index] ['mensagem'] [] = $e->getMessage();
                    $this->mensageiro->setMensageiro("Erro no Configuração de Envio de Email.", Ead1_IMensageiro::ERRO, $e->getMessage());
                } catch (Zend_Db_Exception $e) {
                    $this->_log ['erro'] [$index] ['bl_erro'] = false;
                    $this->_log ['erro'] [$index] ['recipe'] = $arrEnvio [$index];
                    $this->_log ['erro'] [$index] ['mensagem'] [] = $e->getMessage();
                    $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
                } catch (Exception $e) {
                    $this->_log ['erro'] [$index] ['bl_erro'] = true;
                    $this->_log ['erro'] [$index] ['recipe'] = $arrEnvio [$index];
                    $this->_log ['erro'] [$index] ['mensagem'] [] = $e->getMessage();
                    $this->mensageiro->setMensageiro("Erro ao Enviar Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
                }
            }
            if (empty($this->_log ['erro'])) {
                $this->mensageiro->setMensageiro("Mensagens Enviadas com Sucesso(" . count($this->_log ['sucesso']) . ")!", Ead1_IMensageiro::SUCESSO);
            } else if (!empty($this->_log ['erro']) && !empty($this->_log ['sucesso'])) {
                $this->mensageiro->setMensageiro('(' . count($this->_log ['sucesso']) . ') Mensagens Enviadas e (' . count($this->_log ['erro']) . ') Não Enviadas');
            } else {
                $this->mensageiro->setMensageiro("Erro ao Enviar as Mensagens(" . count($this->_log ['erro']) . ").");
            }
            $tmpMensageiro = clone $this->mensageiro;
            //Alterando a Evolução do Envio das Mensagens
            if (!empty($this->_log ['erro'])) {
                foreach ($this->_log ['erro'] as $erro) {
                    if ($erro ['bl_erro']) {
                        $this->editarEnvioMensagemErro($erro ['recipe']['envioMensagemTO'], $erro ['mensagem'])->subtractMensageiro();
                    }
                }
            }
            $this->mensageiro = $tmpMensageiro;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Enviar Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que gera as mensagens padrão  de processos do sistema para a entidade
     * @param int $id_mensagempadrao
     * @param Ead1_TO_Dinamico $to - TO com os dados De acordo com a Mensagem Padrão
     * @param int $id_tipoenvio
     * @param int $bl_enviarAgora
     * @return Ead1_Mensageiro
     */
    public function procedimentoGerarEnvioMensagemPadraoEntidade($id_mensagempadrao, Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL, $bl_enviarAgora = false, $id_entidademensagem = 0, $params = array())
    {
        try {

            switch ($id_mensagempadrao) {
                case MensagemPadraoTO::MATRICULA :
                    $resposta = $this->gerarMensagemAtivacaoMatricula($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::DADOS_ACESSO :
                    $resposta = $this->gerarMensagemDadosAcesso($to, $id_tipoenvio, $id_entidademensagem);
                    break;
                case MensagemPadraoTO::DADOS_ACESSO_SMS:
                    $resposta = $this->gerarMensagemDadosAcesso($to, $id_tipoenvio, $id_entidademensagem);
                    break;
                case MensagemPadraoTO::ENVIO_BOLETO :
                    $resposta = $this->gerarMensagemEnvioBoleto($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::ENVIO_CARTAO :
                    $resposta = $this->gerarMensagemEnvioCartao($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::CONFIRMACAO_VENDA:
                    $resposta = $this->gerarMensagemConfirmacaoVenda($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::COBRANCA:
                    $resposta = $this->gerarMensagemCobranca($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::VENDA_EBOOK:
                    $resposta = $this->gerarMensagemEnvioEBook($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::LIBERAR_TCC:
                    $resposta = $this->gerarMensagemLiberacaoTcc($to, $id_tipoenvio); //alteração AC-280 envio de mensagem para liberação de tcc
                    break;
                case MensagemPadraoTO::RECIBO_TCC_ALUNO:
                    $resposta = $this->gerarMensagemEnvioTcc($to, $id_tipoenvio); //alteração AC-282 envio de e-mail para aviso de Envio de tcc
                    break;
                case MensagemPadraoTO::DEVOLUCAO_TCC_ALUNO:
                    $resposta = $this->gerarMensagemDevolucaoTcc($to);
                    break;
                case MensagemPadraoTO::AVALIACAO_TCC_ALUNO:
                    $resposta = $this->gerarMensagemAvaliacaoTcc($to);
                    break;
                case MensagemPadraoTO::DEVOLUCAO_TCC_TUTOR:
                    $resposta = $this->gerarMensagemDevolucaoTcc($to);
                    break;
                case MensagemPadraoTO::AGENDAMENTO:
                    $resposta = $this->gerarMensagemEnvioAgendamento($to, $id_tipoenvio); //COM-159 envio de e-mail para aviso de agendamento de simulado
                    break;
                case MensagemPadraoTO::RECIBO_TCC:
                    $resposta = $this->gerarMensagemEnvioTccProfessor($to, $id_tipoenvio); //alteração AC-282 envio de e-mail para aviso de Envio de tcc
                    break;
                case MensagemPadraoTO::AVISO_VENCIMENTO_CARTAO_RECORRENTE:
                    $resposta = $this->gerarMensagemVencimentoRecorrente($to, $id_tipoenvio); //alteração FIN-16165 envio de e-mail para aviso de Vencimento de Cartao Recorrente
                    break;
                case MensagemPadraoTO::ALTERACAO_CADASTRO_USUARIO:
                    $resposta = $this->gerarMensagemDadosAcesso($to, $id_tipoenvio, $id_entidademensagem, MensagemPadraoTO::ALTERACAO_CADASTRO_USUARIO);
                    break;
                case MensagemPadraoTO::ENVIO_COD_RASTREAMENTO_CERTIFICADO:
                    $resposta = $this->gerarMensagemRastreamentoCertificado($to, $id_tipoenvio, $id_entidademensagem, MensagemPadraoTO::ENVIO_COD_RASTREAMENTO_CERTIFICADO);
                    break;
                case MensagemPadraoTO::AVISO_AGENDAMENTO:
                    $resposta = $this->gerarMensagemEnvioNotificacaoAgendamentoAluno($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::ENVIO_CUPOM_PREMIO:
                    $resposta = $this->gerarMensagemEnvioCupomPremio($to, $id_tipoenvio, $params);
                    break;
                case MensagemPadraoTO::ENVIO_EMAIL_CONFIRMACAO_RENOVACAO:
                    $resposta = $this->gerarMensagemEnvioEmailConfirmacaoRenovacao($to);
                    break;
                case MensagemPadraoTO::ENVIO_CARTAO_ACORDO:
                    $resposta = $this->gerarMensagemEnvioCartaoAcordo($to, $id_tipoenvio);
                    break;
                case MensagemPadraoTO::AVISO_SOBRE_AGENDAMENTO:
                    $resposta = $this->gerarMensagemAvisoAgendamento($to);
                    break;
                case MensagemPadraoTO::RENOVACAO_NEGOCIACAO_INADIMPLENCIA:
                    $resposta = $this->gerarMensagemEnvioAlunosForaRenovacao($to);
                    break;
                case MensagemPadraoTO::AVISO_RECORRENTE_ATRASADO:
                    $resposta = $this->gerarMensagemAvisoRecorrenteAtrasado($to, \G2\Constante\TipoEnvio::EMAIL, $params);
                    break;
                case MensagemPadraoTO::AVISO_RECORRENTE_PAGO:
                    $resposta = $this->gerarMensagemAvisoRecorrentePago($to, \G2\Constante\TipoEnvio::EMAIL, $params);
                    break;
                default:
                    throw new Zend_Exception("procedimentoGerarEnvioMensagemPadraoEntidade: Mensagem Padrão não implementada.");
                    break;
            }

            if ($id_mensagempadrao != MensagemPadraoTO::ENVIO_EMAIL_CONFIRMACAO_RENOVACAO) {
                $this->procedimentoCadastrarMensagemEnvioDestinatarios($resposta['envioMensagemTO'], $resposta['mensagemTO'], $resposta['arrEnvioDestinatarioTO'], $bl_enviarAgora)->subtractMensageiro();
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Não Foi Possível Gerar Email: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }

    /**
     * Método que monta a mensagem contendo o link para pagamento via Cartão de Crédito
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemEnvioCartao(VendaTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        // Buscando os dados atuais da venda pela PK
        $to->fetch(true, true, true);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::HTML :
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::ENVIO_CARTAO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para pagamento via cartão de Crédito.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_venda' => $to->getId_venda(), 'id_entidade' => $to->getId_entidade());
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO ();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("Pagamento via Cartão de Crédito - " . $entidade->getSt_nomeentidade());

        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     * Método que monta a mensagem contendo o link para pagamento via Cartão de Crédito de um acordo
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemEnvioCartaoAcordo(VwResumoFinanceiroTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {
        // Buscando os dados atuais da venda pela PK
        $to->fetch(false, true, true);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();

        switch ($id_tipoenvio) {
            case TipoEnvioTO::HTML :
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::ENVIO_CARTAO_ACORDO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para pagamento via cartão de Crédito.");
                }

                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());;
                $arParametros = array(
                    'id_venda' => $to->getId_venda(),
                    'id_acordo' => $to->getId_acordo(),
                    'id_lancamento' => $to->getId_lancamento(),
                    'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();

                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO ();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("Pagamento via Cartão de Crédito (Acordo) - " . $entidade->getSt_nomeentidade());

        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


    /**
     * Método que monta a mensagem conteudo Boletos para Pagamento
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemEnvioBoleto(VendaTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::HTML :
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::ENVIO_BOLETO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Boletos de Pagamento.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_venda' => $to->getId_venda(), 'id_entidade' => $to->getId_entidade());
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw ZendException("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO ();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("Boletos de Pagamento - " . $entidade->getSt_nomeentidade());

        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     * Método que monta a mensagem conteudo os links para acesso aos e-book
     * @param VendaTO $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemEnvioEBook(VendaTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::HTML :
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::VENDA_EBOOK, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para venda de e-Book.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_venda' => $to->getId_venda(), 'id_entidade' => $to->getId_entidade());
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $mensagemTO->setId_usuariocadastro($to->getId_usuario());
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO ();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("E-Book: Links para Download - " . $entidade->getSt_nomeentidade());

        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     * Método que monta a Mensagem de Cobrança
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwMensagemCobrancaRoboTO $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return MensagemTO
     */
    public function gerarMensagemCobranca(VwMensagemCobrancaRoboTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $envioMensagemTO = new EnvioMensagemTO();
        $mensagemTO = new MensagemTO();
        $envioDestinatarioTO = new EnvioDestinatarioTO();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::HTML :
            case TipoEnvioTO::EMAIL :


                $dados = $this->_dao->retornarEmailEntidadeMensagem(
                    new EmailEntidadeMensagemTO(
                        array('id_mensagempadrao' => MensagemPadraoTO::COBRANCA
                        , 'id_entidade' => $this->msgPadraoPai($to->getId_entidade())
                        , 'bl_ativo' => 1
                        )
                    )
                )->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Mensagens de Cobrança.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());

                $arParametros = array('id_venda' => $to->getId_venda(), 'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $to->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("Mensagem de Cobrança - " . $entidade->getSt_nomeentidade());

        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     *
     * Gera mensagens de ativação de matrículas a partir de um array de MatriculaTO
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array $arTO
     * @param const $id_tipoenvio
     * @return Ead1_Mensageiro
     */
    public function enviarArrayMensagemAtivacaoMatricula(array $arTO, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $resposta = false;

        try {

            if (!empty($arTO)) {
                foreach ($arTO as $to) {


                    $resposta[] = $this->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::MATRICULA, $to, TipoEnvioTO::EMAIL, false);
                }
            }

            if (!$resposta) {
                return new Ead1_Mensageiro('Nenhuma Mensagem Gerada.', Ead1_IMensageiro::AVISO);
            } else {
                return new Ead1_Mensageiro($resposta, Ead1_IMensageiro::SUCESSO);
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que monta a mensagem de ativação da matrícula
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemAtivacaoMatricula(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $this->validaTOMensagemAtivacaoMatricula($to);
        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();

        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::MATRICULA, 'id_entidade' => $this->msgPadraoPai($to->getId_entidadeatendimento()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Ativação de Matrícula.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidadeatendimento());
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidadeatendimento());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                $envioDestinatarioTO->setId_matricula($to->getId_matricula());
                break;
            default :
                throw new Zend_Validate_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Ativação de Matrícula");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     * Método que monta a mensagem de ativação da matrícula
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return array
     */
    public function gerarMensagemDadosAcesso(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL, $id_entidademensagem = 0, $idMensagemPadrao = MensagemPadraoTO::DADOS_ACESSO)
    {
        /*to retorna id entidade
         *
         * */

        $this->validaTOMensagemDadosAcesso($to);
        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();


        if (!$to->getId_entidade()) {
            $to->setId_entidade($to->getSessao()->id_entidade);
        }
        // Valida se a mensagem deve vir da entidade Pai. e Atribui o id da entidade aonde está cadastrado o texto de sistema
        $id_entidademensagem = $this->msgPadraoPai($to->getId_entidade());

        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :

                $dados = $this->_dao->retornarEmailEntidadeMensagem(
                    new EmailEntidadeMensagemTO(
                        array(
                            'id_mensagempadrao' => $idMensagemPadrao,
                            'id_entidade' => ($id_entidademensagem ? $id_entidademensagem : $to->getId_entidade()),
                            'bl_ativo' => 1
                        )
                    )
                )->toArray();

                if (empty($dados)) {
                    //criar funcionalidade para envio de e-mail default
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição Recuperação de Dados de Acesso.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);

                $textoSistemaTO = new TextoSistemaTO();
                $textoSistemaTO->setId_textosistema($emailEntidadeMensagemTO->getId_textosistema());
                $textoSistemaTO->fetch(false, true, true);
                if ($textoSistemaTO->getId_textocategoria() != TextoCategoriaTO::DADOS_PESSOAIS) {
                    THROW new Zend_Validate_Exception("Mensagem padrão da Entidade tem de ser categoria de dados pessoais.");
                }
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_entidade' => $to->getId_entidade(), 'id_usuario' => $to->getId_usuario());

                $id_entidademensagem = $to->getId_entidade();

                if ($to instanceof UsuarioTO) :
                    $arParametros ['st_senha'] = $to->getSt_senha();
                    unset($to->id_entidade);

                endif;

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidadeatendimento() ? $to->getId_entidadeatendimento() : $id_entidademensagem);
                $mensagemTO->setId_usuariocadastro($to->getId_usuario());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;


            case TipoEnvioTO::SMS:

                $this->validaTOMensagemDadosAcessoSMS($to);

                $dados = $this->_dao->retornarSMSEntidadeMensagem(
                    new SMSEntidadeMensagemTO(
                        array('id_mensagempadrao' => MensagemPadraoTO::DADOS_ACESSO_SMS
                        , 'id_entidade' => ($id_entidademensagem ? $id_entidademensagem : $to->id_entidade), 'bl_ativo' => 1)))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição Recuperação de Dados de Acesso via SMS.");
                }
                $smsEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new SMSEntidadeMensagemTO(), true);

                $textoSistemaTO = new TextoSistemaTO();
                $textoSistemaTO->setId_textosistema($smsEntidadeMensagemTO->getId_textosistema());
                $textoSistemaTO->fetch(false, true, true);
                if ($textoSistemaTO->getId_textocategoria() != TextoCategoriaTO::DADOS_PESSOAIS) {
                    THROW new Zend_Validate_Exception("Mensagem padrão da Entidade tem de ser categoria de dados pessoais.");
                }

                $envioMensagemTO->setId_emailconfig(null);
                $arParametros = array('id_entidade' => $to->getId_entidade(), 'id_usuario' => $to->getId_usuario());

                $textoSistemaBO = new TextoSistemaBO ();
                $mensagem = $textoSistemaBO->gerarTexto(new TextoSistemaTO(array('id_textosistema' => $smsEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario());
                $mensagemTO->setSt_texto($mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());

                $vwPessoaTO = new VwPessoaTelefoneTO();
                $vwPessoaTO->setId_entidade($to->getId_entidade());
                $vwPessoaTO->setId_usuario($to->getId_usuario());
                $vwPessoaTO->setId_tipotelefone(TipoTelefoneTO::CELULAR);
                $vwPessoaTO->fetch(false, true, true);

                if ($vwPessoaTO->getId_tipotelefone() == TipoTelefoneTO::CELULAR) {

                    $vwPessoaTO->setNu_ddi($vwPessoaTO->getNu_ddi() ? $vwPessoaTO->getNu_ddi() : 55);
                    $envioDestinatarioTO->setNu_telefone($vwPessoaTO->getNu_ddi() . $vwPessoaTO->getNu_ddd() . $vwPessoaTO->getNu_telefone());
                    $envioDestinatarioTO->setSt_nome($vwPessoaTO->getSt_nomecompleto());
                }

                if ($envioDestinatarioTO->getNu_telefone() == false) {
                    throw new Zend_Exception("Nenhum telefone Celular encontrado!");
                }

                if ($vwPessoaTO->getNu_ddd() == false || $vwPessoaTO->getNu_ddd() <= 10) {
                    throw new Zend_Exception("DDD inválido!");
                }

                break;
            default :
                throw new Zend_Exception("gerarMensagemDadosAcesso: Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem($idMensagemPadrao == MensagemPadraoTO::ALTERACAO_CADASTRO_USUARIO ? 'E-mail de alteração cadastro usuário' : 'Dados de Acesso');
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    public function gerarMensagemEnvioCupomPremio(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL, $params)
    {
        $envioMensagemTO = new EnvioMensagemTO();
        $mensagemTO = new MensagemTO();
        $envioDestinatarioTO = new EnvioDestinatarioTO();

        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::ENVIO_CUPOM_PREMIO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Envio de Cupom Prêmio.");
                }

                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $params)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $mensagemTO->setId_usuariocadastro($to->getId_usuario());
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;

            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $entidade = new EntidadeTO ();
        $entidade->setId_entidade($to->getId_entidade());
        $entidade->fetch(true, true, true);
        $mensagemTO->setSt_mensagem("Cupom Prêmio da entidade - " . $entidade->getSt_nomeentidade());
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;


    }

    public function gerarMensagemConfirmacaoVenda(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {
        $envioMensagemTO = new EnvioMensagemTO();
        $mensagemTO = new MensagemTO();
        $envioDestinatarioTO = new EnvioDestinatarioTO();

        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $idEntidade = $to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade;
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
                    'id_mensagempadrao' => MensagemPadraoTO::CONFIRMACAO_VENDA,
                    'id_entidade' => $this->msgPadraoPai($idEntidade),
                    'bl_ativo' => 1
                )))->toArray();

                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para 
                    Confirmação de Venda. Entidade Id: " . $idEntidade);
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_venda' => $to->getId_venda());

                $textoSistemaBO = new TextoSistemaBO();
                $textoSistemaTO = new TextoSistemaTO();
                $textoSistemaTO->setId_textosistema($emailEntidadeMensagemTO->getId_textosistema());
                $textoSistemaTO->fetch(true, true, true);

                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array(
                    'id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema()
                )), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO
                    ->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
                $mensagemTO
                    ->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $to->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);

                $vendaLancamentoTO = new VwVendaLancamentoTO();
                $vendaLancamentoTO->setId_venda($to->getId_venda());
                $vendaLancamentoTO->setId_meiopagamento(\MeioPagamentoTO::BOLETO);

                $vorm = new VwVendaLancamentoORM();
                $lancamentos = $vorm->consulta($vendaLancamentoTO, false, false, array('nu_ordem'));

                if ($lancamentos) {
                    $caminho = \G2\Negocio\BoletoPHP::caminhoPDF
                        . DIRECTORY_SEPARATOR . $to->getId_venda()
                        . DIRECTORY_SEPARATOR
                        . \G2\Negocio\BoletoPHP::nomePDF;

                    $mensagemTO->setStCaminhoanexo($caminho);
                }

                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem($textoSistemaTO->getSt_textosistema());
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /**
     * Método que valida o TO para geração de Mensagem de Ativação de Matricula
     * @param Ead1_TO_Dinamico $to
     * @throws Zend_Exception
     */
    protected function validaTOMensagemAtivacaoMatricula(Ead1_TO_Dinamico $to)
    {
        $msg = "";
        if (!($to instanceof MatriculaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg)
                . "ativação de matricula não é uma instancia de MatriculaTO.";
        }

        $msg_init = $this->zendexceppadraomsg . "ativação de matricula.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if (!($to->getId_entidadeatendimento())) {
            $msg = "Entidade de atendimento" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    /**
     * Método que valida o TO para geração de Mensagem de recuperação de Dados de Acesso
     * @param Ead1_TO_Dinamico $to
     * @throws Zend_Exception
     */
    protected function validaTOMensagemDadosAcesso(Ead1_TO_Dinamico $to)
    {
        $msg = null;
        if (!($to instanceof PessoaTO) && !($to instanceof UsuarioTO) && !($to instanceof VwPessoaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg)
                . "recuperação de dados de acesso via SMS não é uma instancia de PessoaTO ou UsuarioTO.";
        }

        $msg_init = $this->zendexceppadraomsg . "recuperação de dados de acesso.";
        if (!($to->getId_entidade())) {
            $msg = "Instituição" . $msg_init;
        }
        if (!($to->getId_usuario())) {
            $msg = "Usuário" . str_replace("informada", "informado", $msg_init);
        }

        if ($msg)
            throw new Zend_Exception($msg);

    }

    /**
     * Método que valida o TO para geração de Mensagem de recuperação de Dados de Acesso via SMS
     * @param Ead1_TO_Dinamico $to
     * @throws Zend_Exception
     */
    protected function validaTOMensagemDadosAcessoSMS(Ead1_TO_Dinamico $to)
    {
        $msg = "";
        if (!($to instanceof PessoaTO) && !($to instanceof VwPessoaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg)
                . "recuperação de dados de acesso via SMS não é uma instancia de PessoaTO ou VwPessoaTO.";
        }

        $msg_init = $this->zendexceppadraomsg . "recuperação de dados de acesso.";
        if (!($to->getId_entidade())) {
            $msg = "Instituição" . $msg_init;
        }
        if (!($to->getId_usuario())) {
            $msg = "Usuário" . str_replace("informada", "informado", $msg_init);
        }

        if ($msg)
            throw new Zend_Exception($msg);

    }

    /**
     * Método que valida o TO para geração de Mensagem de Confirmação de Venda que não seja matrícula
     * @param Ead1_TO_Dinamico $to
     * @throws Zend_Exception
     * @author Rafael Rocha / Denise Xavier
     */
    private function validaTOMensagemConfirmacaoVenda(Ead1_TO_Dinamico $to)
    {

        $msg = "";
        if (!($to instanceof VwAlunosSalaTO)) {
            $msg = "O TO " . str_replace("informada", "recebido", $this->zendexceppadraomsg)
                . "confirmação de venda não é uma instância de VendaTO.";
        }

        if (!($to->getId_venda())) {
            $msg = "ID da venda não fornecido para o procedimento";
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    /**
     * Método que edita o envio de Mensagem para Erro
     * @param EnvioMensagemTO $envioMensagemTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    private function editarEnvioMensagemErro(EnvioMensagemTO $envioMensagemTO, $mensagem = null)
    {
        try {
            if ($mensagem && is_array($mensagem)) {
                $mensagem = implode(" - ", $mensagem);
            }

            if (!$envioMensagemTO->getId_enviomensagem()) {
                throw new Zend_Exception("O id_enviomensagem não veio setado para Editar o Envio como Erro.");
            }
            $envioMensagemTO->setDt_envio(null);
            $envioMensagemTO->setDt_tentativa(new Zend_Date());
            $envioMensagemTO->setId_evolucao(EnvioMensagemTO::ERRO_AO_ENVIAR);
            $envioMensagemTO->setSt_retorno($mensagem);


            $this->editarEnvioMensagem($envioMensagemTO)->subtractMensageiro();

            //notifica o gestor sobre o erro ao enviar o e-mail
            $mensagemNegocio = new \G2\Negocio\Mensagem();
            $mensagemNegocio->notificarErroEnvioMensagem($envioMensagemTO->toArray());

        } catch (Zend_Db_Exception $e) {
            throw new Zend_Exception("Erro De Banco de Dados: Não foi Possível Editar o Envio de Mensagem como Erro.", null, $e);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Método que envia o Email
     * @param EnvioMensagemTO $envioMensagemTO
     * @param array $arrVwEnvioMensagemTO
     * @return bool
     * @throws Exception
     */
    protected function enviarEmail(EnvioMensagemTO $envioMensagemTO, array $arrVwEnvioMensagemTO)
    {
        try {
            $this->validaEnvioDestinatarioEnvioMensagem($envioMensagemTO, $arrVwEnvioMensagemTO);

            $mensagemNegocio = new \G2\Negocio\Mensagem();
            $mensagemNegocio->verificaCofiguracaoEmail($envioMensagemTO->getId_emailconfig());

            $factory = new Ead1_Mail_Config_Factory('id_emailconfig = ' . $envioMensagemTO->getId_emailconfig());
            $mail = new Zend_Mail('UTF-8');
            foreach ($arrVwEnvioMensagemTO as $vwEnvioMensagemTO) {
                switch ($vwEnvioMensagemTO->getId_tipodestinatario()) {
                    case TipoDestinatarioTO::DESTINATARIO :
                        $mail->addTo($vwEnvioMensagemTO->getSt_endereco(), $vwEnvioMensagemTO->getSt_nome());
                        break;
                    case TipoDestinatarioTO::BCC :
                        $mail->addBcc($vwEnvioMensagemTO->getSt_endereco());
                        break;
                    case TipoDestinatarioTO::CC :
                        $mail->addCc($vwEnvioMensagemTO->getSt_endereco(), $vwEnvioMensagemTO->getSt_nome());
                        break;
                    default :
                        throw new Zend_Exception("Tipo De Destinatário Não Implementado.");
                        break;
                }
            }
            $mail->setBodyHtml($vwEnvioMensagemTO->getSt_texto());
            $mail->setSubject($vwEnvioMensagemTO->getSt_mensagem());

            if (file_exists($vwEnvioMensagemTO->getStCaminhoanexo())) {
                $file = $vwEnvioMensagemTO->getStCaminhoanexo();
                $at = new Zend_Mime_Part(file_get_contents($file));
                $at->filename = basename($file);
                $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $at->encoding = Zend_Mime::ENCODING_BASE64;
                $mail->addAttachment($at);
            }

            //@todo Fixo para verificação de Emails Enviados
            //$mail->addBcc("valida.inscricao@gmail.com");
            $factory->sendMail($mail);
            return true;
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao tentar enviar. " . $e->getMessage());
            return false;
        }
    }

    /**
     * Método que envia o SMS
     * @param EnvioMensagemTO $envioMensagemTO
     * @param array $arrVwEnvioMensagemTO - Destinatários
     * @throws Zend_Exception
     * @return Boolean
     */
    protected function enviarSMS(EnvioMensagemTO $envioMensagemTO, array $arrVwEnvioMensagemTO)
    {

        try {

            $this->validaEnvioDestinatarioEnvioMensagem($envioMensagemTO, $arrVwEnvioMensagemTO);

            foreach ($arrVwEnvioMensagemTO as $vwEnvioMensagemTO) {
                $entidadeIntegracao = new EntidadeIntegracaoTO();
                $entidadeIntegracao->setId_sistema(\G2\Constante\Sistema::LOCASMS);
                $entidadeIntegracao->setId_entidade($vwEnvioMensagemTO->getId_entidade());

                $enti = new EntidadeBO();
                $mei = $enti->retornaEntidadeIntegracao($entidadeIntegracao);
                if ($mei->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mei->getFirstMensagem());
                }
                $ei = $mei->getFirstMensagem();
//                $vwEnvioMensagemTO->setnu_telefone('553799122189');
                $smsTo = new Ead1_TO_SMS();
                $smsTo->setAction(Ead1_TO_SMS::SENDSMS)
                    ->setUrlApi($ei->getSt_caminho())
                    ->setLogin($ei->getSt_codsistema())
                    ->setSenha($ei->getSt_codchave())
                    ->setSendTo($vwEnvioMensagemTO->getnu_telefone())
                    ->setMensagem($vwEnvioMensagemTO->getSt_texto());
                $sms = \Ead1\SMS\SMS::send($smsTo);
                if (!$sms->status) {
                    throw new Exception("Erro ao enviar SMS. " . $sms->msg);
                }


            }
            return true;
        } catch (Exception $e) {
            throw $e;
            return false;
        }
    }

    /**
     * Método que valida os destinatarios a serem cadastrados
     * @param EnvioMensagemTO $envioMensagemTO
     * @param array $arrEnvioDestinatarioTO
     * @throws Zend_Validate_Exception
     */
    public function validaCadastroDestinatariosEnvioMensagem(EnvioMensagemTO $envioMensagemTO, array $arrEnvioDestinatarioTO)
    {
        if (empty($arrEnvioDestinatarioTO)) {
            THROW new Zend_Validate_Exception("Não foi informado nenhum destinatário para a mensagem.");
        }
        $validate = new Zend_Validate_EmailAddress ();
        $temDestinatario = false;
        foreach ($arrEnvioDestinatarioTO as $envioDestinatarioTO) {
            if (!($envioDestinatarioTO instanceof EnvioDestinatarioTO)) {
                throw new Zend_Exception("Um dos objetos de destinatários não é uma instancia de EnvioDestinatarioTO.");
            }
            //var_dump($envioDestinatarioTO);
            if (!$envioDestinatarioTO->getId_usuario() && (!$envioDestinatarioTO->getSt_endereco() || !$envioDestinatarioTO->getSt_nome())) {
                $erro_msg = "Não foi informado nenhum";
                $erro_datas = "";
                if (!$envioDestinatarioTO->getId_usuario())
                    $erro_datas .= " usuário ";
                if (!$envioDestinatarioTO->getSt_endereco()) {
                    if ($erro_datas != "")
                        $erro_datas .= ", ";
                    $erro_datas .= " e-mail ";
                }
                if (!$envioDestinatarioTO->getSt_nome()) {
                    if ($erro_datas != "")
                        $erro_datas .= "e ";
                    $erro_datas .= " nome ";
                }
                $erro_msg .= $erro_datas . "para o destinatário.";
                THROW new Zend_Validate_Exception($erro_msg);
            }

            switch ($envioMensagemTO->getId_tipoenvio()) {
                case TipoEnvioTO::EMAIL :
                    if (!$envioDestinatarioTO->getId_usuario() && !$validate->isValid($envioDestinatarioTO->getSt_endereco())) {
                        THROW new Zend_Validate_Exception("Email de destinatário inválido.");
                    }
                    if (!$envioMensagemTO->getId_emailconfig()) {
                        THROW new Zend_Validate_Exception("Configuração de e-mail não informada.");
                    }
                    break;

                case TipoEnvioTO::SMS :
                    if (!$envioDestinatarioTO->getNu_telefone()) {
                        THROW new Zend_Validate_Exception("Número de telefone não informado.");
                    }
                    break;
                case TipoEnvioTO::HTML :
                    break;
                default :
                    THROW new Zend_Validate_Exception("Tipo de envio não implementado.");
                    break;
            }
            if ($envioDestinatarioTO->getId_tipodestinatario() == TipoDestinatarioTO::DESTINATARIO) {
                $temDestinatario = true;
            }
        }
        if (!$temDestinatario) {
            THROW new Zend_Validate_Exception("Não foi informado nenhum destinatário.");
        }
    }

    /**
     * Método que valida os destinatarios e envio da mensagem para envio
     * @param EnvioMensagemTO $envioMensagemTO
     * @param array $arrVwEnvioMensagemTO - Destinatários
     * @throws Zend_Validate_Exception
     */
    public function validaEnvioDestinatarioEnvioMensagem(EnvioMensagemTO $envioMensagemTO, array $arrVwEnvioMensagemTO)
    {
        if (empty($arrVwEnvioMensagemTO)) {
            THROW new Zend_Validate_Exception("Não foi Informado Nenhum Destinatário para a Mensagem.");
        }
//        $validate = new Zend_Validate_EmailAddress ();
        $temDestinatario = false;
        foreach ($arrVwEnvioMensagemTO as $vwEnvioMensagemTO) {
            if (!($vwEnvioMensagemTO instanceof VwEnvioMensagemTO)) {
                throw new Zend_Exception("Um Dos Objetos de Destinatários Não é uma Instancia de VwEnvioMensagemTO.");
            }
            switch ($envioMensagemTO->getId_tipoenvio()) {
                case TipoEnvioTO::EMAIL :
//                    if (!$validate->isValid($vwEnvioMensagemTO->getSt_endereco())) {
                    if (!$this->validaEmailAvancado($vwEnvioMensagemTO->getSt_endereco())) {
                        THROW new Zend_Validate_Exception("Email de Destinatário Inválido.");
                    }
                    if (!$envioMensagemTO->getId_emailconfig()) {
                        THROW new Zend_Validate_Exception("Configuração de Email Não Informada.");
                    }
                    break;
                case TipoEnvioTO::SMS :
                    if (!$vwEnvioMensagemTO->getNu_telefone()) {
                        THROW new Zend_Validate_Exception("Número de Telefone não informado.");
                    }
                    break;
                default :
                    THROW new Zend_Validate_Exception("Tipo de Envio Não Implementado.");
                    break;
            }
            if ($vwEnvioMensagemTO->getId_tipodestinatario() == TipoDestinatarioTO::DESTINATARIO) {
                $temDestinatario = true;
            }
        }
        if (!$temDestinatario) {
            THROW new Zend_Validate_Exception("Não foi Informado Nenhum Destinatário.");
        }
    }

    /**
     * Método que cadastra a Mensagem
     * @param MensagemTO $mensagemTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarMensagem(MensagemTO $mensagemTO)
    {
        try {
            $mensagemTO->setId_entidade($mensagemTO->getId_entidade() ? $mensagemTO->getId_entidade() : $mensagemTO->getSessao()->id_entidade);
            $mensagemTO->setId_usuariocadastro($mensagemTO->getId_usuariocadastro() ? $mensagemTO->getId_usuariocadastro() : $mensagemTO->getSessao()->id_usuario);
            $id_mensagem = $this->_dao->cadastrarMensagem($mensagemTO);
//            Zend_Debug::dump($id_mensagem);die;
            $mensagemTO->setId_mensagem((int)$id_mensagem);
            $this->mensageiro->setMensageiro($mensagemTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra o Envio da Mensagem
     * @param EnvioMensagemTO $envioMensagemTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEnvioMensagem(EnvioMensagemTO $envioMensagemTO)
    {
        try {
            $envioMensagemTO->setDt_enviar($envioMensagemTO->getDt_enviar() ? $envioMensagemTO->getDt_enviar() : new Zend_Date ());
            $id_enviomensagem = $this->_dao->cadastrarEnvioMensagem($envioMensagemTO);
            $envioMensagemTO->setId_enviomensagem($id_enviomensagem);
            $this->mensageiro->setMensageiro($envioMensagemTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Envio da Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra os destinatarios do Envio
     * @param EnvioDestinatarioTO $envioDestinatarioTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEnvioDestinatario(EnvioDestinatarioTO $envioDestinatarioTO)
    {
        try {
            $id_enviodestinatario = $this->_dao->cadastrarEnvioDestinatario($envioDestinatarioTO);
            $envioDestinatarioTO->setId_enviodestinatario($id_enviodestinatario);
            $this->mensageiro->setMensageiro($envioDestinatarioTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Destinatário da Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que Edita o Envio da Mensagem
     * @param EnvioMensagemTO $envioMensagemTO
     * @return Ead1_Mensageiro
     */
    public function editarEnvioMensagem(EnvioMensagemTO $envioMensagemTO)
    {
        try {
            $envioMensagemTO->setDt_tentativa(new Zend_Date());
            $this->_dao->editarEnvioMensagem($envioMensagemTO);
            $this->mensageiro->setMensageiro($envioMensagemTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar Envio da Mensagem.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados da view de envio de mensagem
     * @param VwEnvioMensagemTO $to
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornarVwEnvioMensagem(VwEnvioMensagemTO $to, $where = null)
    {
        try {
            $dados = $this->_dao->retornarVwEnvioMensagem($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhuma Mensagem Encontrada a Ser Enviada.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEnvioMensagemTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar as Mensagens a Serem Enviadas.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /*
     * Método criado para enviar e-mail para aluno assim que o professor libera seu TCC.
     * Débora Castro - deboracastro.pm@gmail.com
     * 31/07/2013 - AC-280
     */

    public function gerarMensagemLiberacaoTcc(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $this->validaTOMensagemLiberacaoTcc($to);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::LIBERAR_TCC, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Liberação de TCC.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                $envioDestinatarioTO->setId_matricula($to->getId_matricula());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Liberação para Publicação de TCC");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /*
     * Validação de TO para método de envio de e-mail (Envio de TCC)
     * 31/07/2013 - AC-282
     */

    protected function validaTOMensagemLiberacaoTcc(VwAlunosSalaTO $to)
    {

        $msg = "";
        if (!($to instanceof VwAlunosSalaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "liberação de TCC não é uma instancia de VwAlunosSalaTO";
        }

        $msg_init = $this->zendexceppadraomsg . " liberação de TCC.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if (!($to->getId_entidade())) {
            $msg = "Entidade" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    public function gerarMensagemEnvioTcc(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $this->validaTOMensagemEnvioTcc($to);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::RECIBO_TCC_ALUNO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Envio de TCC.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                $envioDestinatarioTO->setId_matricula($to->getId_matricula());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Envio de TCC");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    public function gerarMensagemDevolucaoTcc($to)
    {
        $liberacao = "";
        $objLiberacao = new Zend_Session_Namespace('liberacao');
        if (!empty($objLiberacao->id_funcionalidade)) $liberacao = $objLiberacao->id_funcionalidade;

        $envioMensagemTO = new EnvioMensagemTO;
        $mensagemTO = new MensagemTO;
        $envioDestinatarioTO = new EnvioDestinatarioTO;
        $arrEnvioDestinatarioTO = array();
//        Zend_Debug::dump($to->id_situacaotcc);die();
        if ($to->id_situacaotcc == \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO) {
            $id_mensagempadrao = MensagemPadraoTO::DEVOLUCAO_TCC_ALUNO;
            $mensagem = 'Devolução de TCC ao aluno';
            $envioDestinatarioTO->setId_usuario($to->getId_usuario());
            $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
            $envioDestinatarioTO->setId_matricula($to->getId_matricula());

        } else if ($to->id_situacaotcc == \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR) {
            $id_mensagempadrao = MensagemPadraoTO::DEVOLUCAO_TCC_TUTOR;
            $mensagem = 'Devolução de TCC ao tutor';
            if ($liberacao === 'coordenador') {
                //seto o professor do projeto
                $envioDestinatarioTO->setId_usuario($to->getId_professor());
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_matricula($to->getId_matricula());
            } else if ($liberacao === 'pedagogico') {
                //Seta o professor do projeto
                $arrDestinatarios['id_professor'] = $to->getId_professor();

                //busca o coordenador do projeto
                $negocio = new G2\Negocio\Negocio();
                $enEncerramentoSala = $negocio->findOneBy('\G2\Entity\EncerramentoSala',
                    array('id_saladeaula' => $to->getId_saladeaula()), array('dt_encerramentocoordenador' => 'DESC')
                );

                //Seta o coordenador do projeto
                if ($enEncerramentoSala instanceof \G2\Entity\EncerramentoSala) {
                    $arrDestinatarios['id_coordenador'] =
                        $enEncerramentoSala->getId_usuariocoordenador()->getId_usuario();
                }

                //seta os parametros para cada usuário que será mandado o email
                foreach ($arrDestinatarios as $destinatario) {
                    $envioDestinatariosTO = new EnvioDestinatarioTO;
                    $envioDestinatariosTO->setId_usuario($destinatario);
                    $envioDestinatariosTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                    $envioDestinatariosTO->setId_matricula($to->getId_matricula());

                    $arrEnvioDestinatarioTO[] = $envioDestinatariosTO;
                }

            } else {
                throw new Zend_Validate_Exception("Tipo de liberação não encontrada.");
            }
        } else {
            throw new Zend_Validate_Exception("Situação não encontrada.");
        }

        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
            'id_mensagempadrao' => $id_mensagempadrao,
            'id_entidade' => $this->msgPadraoPai($to->getId_entidade()),
            'bl_ativo' => 1)))->toArray();
        if (empty($dados)) {
            throw new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Devolução de TCC.");
        }
        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO, true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
        $arParametros = array(
            'id_matricula' => $to->getId_matricula(),
            'id_usuario' => $to->getId_usuario(),
            'id_entidade' => $to->getId_entidade());

        $textoSistemaBO = new TextoSistemaBO;
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array(
            'id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())),
            $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);

        $mensagemTO->setSt_mensagem($mensagem);
        $envioMensagemTO->setId_tipoenvio(\G2\Constante\TipoEnvio::EMAIL);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta['envioMensagemTO'] = $envioMensagemTO;

        if (!empty($arrEnvioDestinatarioTO)) {
            $resposta['arrEnvioDestinatarioTO'] = $arrEnvioDestinatarioTO;
        } else {
            $resposta['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        }
        $resposta['mensagemTO'] = $mensagemTO;

        return $resposta;
    }

    public function gerarMensagemAvaliacaoTcc($to)
    {
        $envioMensagemTO = new EnvioMensagemTO;
        $mensagemTO = new MensagemTO;
        $envioDestinatarioTO = new EnvioDestinatarioTO;

        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::AVALIACAO_TCC_ALUNO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
        if (empty($dados)) {
            throw new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Envio de TCC.");
        }
        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO, true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
        $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());

        $textoSistemaBO = new TextoSistemaBO;
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuario());
        $envioDestinatarioTO->setId_matricula($to->getId_matricula());

        $mensagemTO->setSt_mensagem("TCC recebido pelo Orientador");
        $envioMensagemTO->setId_tipoenvio(\G2\Constante\TipoEnvio::EMAIL);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta['envioMensagemTO'] = $envioMensagemTO;
        $resposta['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta['mensagemTO'] = $mensagemTO;

        return $resposta;
    }

    /*
     * Validação de TO para método de envio de e-mail (Envio de TCC)
     * 31/07/2013 - AC-282
     * Alterar para VW usada
     */

    protected function validaTOMensagemEnvioTcc(VwAlunosSalaTO $to)
    {

        $msg = "";
        if (!($to instanceof VwAlunosSalaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "liberação de TCC não é uma instancia de VwAlunosSalaTO";
        }

        $msg_init = $this->zendexceppadraomsg . "liberação de TCC.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if (!($to->getId_entidade())) {
            $msg = "Entidade" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    /*
     * Método criado para enviar e-mail para aluno que cadrastou agendamento de simulado o avaliação.
     * Yannick Naquis Roulé - contact@karmacrea.com
     * 16/08/2013 - COM-159
     */

    public function gerarMensagemEnvioAgendamento(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {
        //echo "gerarMensagemEnvioAgendamento";
        $this->validaTOMensagemEnvioAgendamento($to);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $emailEntidadeMensagemTO = new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::AGENDAMENTO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => true));
                $dados = $this->_dao->retornarEmailEntidadeMensagem($emailEntidadeMensagemTO)->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma mensagem definida na instituição para envio do mail de confirmação.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array(
                    'dt_agendamento' => $to->getDt_agendamento(),
                    'st_avaliacao' => $to->getSt_avaliacao(),
                    'st_nome' => $to->getSt_nomecompleto(),
                    'id_avaliacaoagendamento' => $to->getId_avaliacaoagendamento(),
                    'id_entidade' => $to->getId_entidade(),
                    'st_usuario' => $to->getSt_login(),
                    'st_senha' => $to->getSt_senha());
                //var_dump($arParametros);
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                $envioDestinatarioTO->setSt_nome($to->getSt_nomecompleto());
                $envioDestinatarioTO->setSt_endereco($to->getSt_email());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }

        $mensagemTO->setSt_mensagem("Confirmação inscrição simulado");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    /*
     * Validação de TO para método de envio de agendamento de simulado o avaliação
     * 16/08/2013 - COM-159
     * Alterar para VW usada
     */

    protected function validaTOMensagemEnvioAgendamento(VwGerarTextoAgendamentoTO $to)
    {

        $msg = "";
        if (!($to instanceof VwGerarTextoAgendamentoTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "envio de agendamento não é uma instancia de VwGerarTextoAgendamentoTO";
        }

        $msg_init = $this->zendexceppadraomsg . "confirmação de agendamento.";
        if (!($to->getId_entidade())) {
            $msg = "Entidade" . $msg_init;
        }
        if (!($to->getDt_agendamento())) {
            $msg = "Data de agendamento" . $msg_init;
        }
        if (!($to->getSt_avaliacao())) {
            $msg = "Avaliação" . $msg_init;
        }
        if (!($to->getSt_nomecompleto())) {
            $msg = "Nome" . str_replace("informada", "informado", $msg_init);
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }


    public function gerarMensagemEnvioTccProfessor(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $this->validaTOMensagemEnvioTcc($to);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::RECIBO_TCC, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Envio de TCC.");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_professor());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Envio de TCC");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    public function gerarMensagemEnvioNotificacaoAgendamentoAluno(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::NOTIFICACAO_AGENDAMENTO_ALUNO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Notificação de Agendamento");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());
//
                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();
//
                $mensagemTO->setId_entidade($mensagemTO->getSessao()->id_entidade);
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Agendamento de Prova");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }

    public function gerarMensagemVencimentoRecorrente(Ead1_TO_Dinamico $to, $id_tipoenvio)
    {

        $envioMensagemTO = new EnvioMensagemTO();
        $mensagemTO = new MensagemTO();
        $envioDestinatarioTO = new EnvioDestinatarioTO();

        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::AVISO_VENCIMENTO_CARTAO_RECORRENTE, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();

        if (empty($dados)) {
            THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para Aviso de Vencimento de Cartão Recorrente.");
        }
        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
        $arParametros = array('id_venda' => $to->getId_venda());

        $textoSistemaBO = new TextoSistemaBO ();
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $to->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $mensagemTO->setSt_mensagem("Vencimento do Cartão utilizado no pagamento.");

        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuario());
        $envioDestinatarioTO->setId_matricula($to->getId_matricula());
        $envioDestinatarioTO->setId_evolucao(\G2\Entity\VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA);

        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


    public function gerarMensagemRastreamentoCertificado(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {

        $this->validaTOMensagemEnvioCodRastreioCertificado($to);

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        switch ($id_tipoenvio) {
            case TipoEnvioTO::EMAIL :
                $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array('id_mensagempadrao' => MensagemPadraoTO::ENVIO_COD_RASTREAMENTO_CERTIFICADO, 'id_entidade' => $this->msgPadraoPai($to->getId_entidade()), 'bl_ativo' => 1)))->toArray();
                if (empty($dados)) {
                    THROW new Zend_Validate_Exception("Nenhuma Mensagem Definida na Instituição para envio de código de rastreamento");
                }
                $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
                $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
                $arParametros = array('id_matricula' => $to->getId_matricula(), 'id_entidade' => $to->getId_entidade());

                $textoSistemaBO = new TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $mensagemTO->setId_entidade($to->getId_entidade());
                $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
                $mensagemTO->setSt_texto($html_mensagem);
                $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
                $envioDestinatarioTO->setId_usuario($to->getId_usuario());
                $envioDestinatarioTO->setId_matricula($to->getId_matricula());
                break;
            default :
                throw new Zend_Exception("Tipo de Envio Não Implementado.");
                break;
        }
        $mensagemTO->setSt_mensagem("Envio de código de rastreamento - Certificado");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);
        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;
        return $resposta;
    }


    /**
     * Valida instancia e campos obrigatorios para envio de e-mail com campos obrigatorios
     * @param VwMatriculaTO $to
     * @throws Zend_Exception
     */
    protected function validaTOMensagemEnvioCodRastreioCertificado(VwMatriculaTO $to)
    {

        $msg = "";
        if (!($to instanceof VwMatriculaTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "envio de agendamento não é uma instancia de VwMatriculaTO";
        }

        $msg_init = $this->zendexceppadraomsg . "envio de código de rastreamento do certificado.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    /**
     * Envia email de confirmação da renovação.
     * @param MatriculaTO $to
     * @return Ead1_Mensageiro|\G2\Negocio\Mensageiro
     */
    public function gerarMensagemEnvioEmailConfirmacaoRenovacao(MatriculaTO $to)
    {
        $vendaGraduacao = new \G2\Negocio\VendaGraduacao();
        return $vendaGraduacao->enviaEmailRenovacaoGraduacao(array('id_entidade' => $this->msgPadraoPai($to->getId_entidadematriz()), 'id_usuario' => $to->getId_usuario()), $to->getId_matricula(), MensagemPadraoTO::ENVIO_EMAIL_CONFIRMACAO_RENOVACAO);
    }


    /**
     * Envio de mensagem de aviso prévio do agendamento
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @return mixed
     * @throws Zend_Validate_Exception
     */
    public function gerarMensagemAvisoAgendamento(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {
        $this->validaTOMensagemAvisoAgendamento($to);
        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
            'id_mensagempadrao' => MensagemPadraoTO::AVISO_SOBRE_AGENDAMENTO,
            'id_entidade' => $this->msgPadraoPai($to->getId_entidade()),
            'bl_ativo' => 1
        )));

        if (!is_object($dados)) {
            throw new Exception('Erro ao retornar Mensagem Padrão - Aviso agendamento');
        } elseif (empty($dados->toArray())) {
            THROW new Zend_Validate_Exception("Nenhuma mensagem definida na instituição para aviso prévio de agendamento.");
        } else {
            $dados = $dados->toArray();
        }
        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
        $arParametros = array('id_matricula' => $to->getId_matricula(),
            'id_entidade' => $to->getId_entidade());

        if ($to->getId_disciplina()) {
            $arParametros['id_disciplinaagendamento'] = $to->getId_disciplina();
        }

        $textoSistemaBO = new TextoSistemaBO ();
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(
                array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema()))
            , $arParametros)
            ->subtractMensageiro()->getFirstMensagem()->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuario());
        $envioDestinatarioTO->setId_matricula($to->getId_matricula());

        $mensagemTO->setSt_mensagem("Aviso de agendamento de prova");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


    protected function validaTOMensagemAvisoAgendamento(Ead1_TO_Dinamico $to)
    {
        $msg = "";
        if (!($to instanceof VwAvaliacaoAgendamentoTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "aviso de agendamento não é uma instancia de VwAvaliacaoAgendamentoTO.";
        }

        $msg_init = $this->zendexceppadraomsg . "aviso de agendamento.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }


    /**
     * Envio de mensagem d
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @return mixed
     * @throws Zend_Validate_Exception
     */
    public function gerarMensagemEnvioAlunosForaRenovacao(Ead1_TO_Dinamico $to, $id_tipoenvio = TipoEnvioTO::EMAIL)
    {
        $this->validaTOMensagemEnvioAlunosForaRenovacao($to);
        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
            'id_mensagempadrao' => MensagemPadraoTO::RENOVACAO_NEGOCIACAO_INADIMPLENCIA,
            'id_entidade' => $this->msgPadraoPai($to->getId_entidade()),
            'bl_ativo' => 1)));

        if (!is_object($dados)) {
            throw new Exception('Erro ao retornar Mensagem Padrão - aviso de alunos aptos a renovação');
        } else if (empty($dados->toArray())) {
            throw new Zend_Validate_Exception("Nenhuma mensagem definida na instituição para aviso de alunos aptos a renovação.");
        }

        $dados = $dados->toArray();

        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());
        $arParametros = array('id_matricula' => $to->getId_matricula(),
            'id_entidade' => $to->getId_entidade(),
            'id_venda' => $to->getId_venda());

        if ($to->getId_disciplina()) {
            $arParametros['id_disciplinaagendamento'] = $to->getId_disciplina();
        }

        $textoSistemaBO = new TextoSistemaBO ();
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(
            array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema())), $arParametros)
            ->subtractMensageiro()->getFirstMensagem()->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuario());
        $envioDestinatarioTO->setId_matricula($to->getId_matricula());

        $mensagemTO->setSt_mensagem("Renovação - Negociação Inadimplência");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


    protected function validaTOMensagemEnvioAlunosForaRenovacao(Ead1_TO_Dinamico $to)
    {
        $msg = "";
        if (!($to instanceof VwAlunosForaRenovacaoTO)) {
            $msg = "O TO" . str_replace("informada", "recebido", $this->zendexceppadraomsg) . "aviso de alunos aptos a renovação não é uma instancia de VwAlunosForaRenovacaoTO.";
        }

        $msg_init = $this->zendexceppadraomsg . "aviso de alunos aptos a renovação.";
        if (!($to->getId_matricula())) {
            $msg = "Matricula" . $msg_init;
        }
        if ($msg)
            throw new Zend_Exception($msg);
    }

    public function msgPadraoPai($id_entidade)
    {
        $negocio = new G2\Negocio\Negocio();
        $id_entidade = $id_entidade ? $id_entidade : $negocio->getId_entidade();
        $entidadePai = $negocio->findOneBy('G2\Entity\Entidade',
            array('id_entidade' => $id_entidade));
        $id_entidadePai = ($entidadePai->getId_entidadecadastro()->getId_entidade() ?
            $entidadePai->getId_entidadecadastro()->getId_entidade() :
            $entidadePai->getId_entidade());

        $herdarMsgPadrao = $negocio->findOneBy('G2\Entity\EsquemaConfiguracaoItem',
            array("id_itemconfiguracao" => ItemConfiguracao::HERDAR_MSG_PADRAO_ENTIDADE_MAE,
                "id_esquemaconfiguracao" => $entidadePai->getId_esquemaconfiguracao()->getId_esquemaconfiguracao()));

        return ($herdarMsgPadrao->getSt_valor() == "1" ? $id_entidadePai : $entidadePai->getId_entidade());
    }

    /**
     * Envio de mensagem de aviso quando uma parcela recorrente está em atraso
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @return mixed
     * @throws Zend_Validate_Exception
     */
    public function gerarMensagemAvisoRecorrenteAtrasado(LancamentoTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL, $params = array())
    {

        if (!$to->getId_entidade()) {
            throw new Exception('O Lançamento precisa vir completo para o método gerarMensagemAvisoRecorrenteAtrasado');
        }

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
            'id_mensagempadrao' => MensagemPadraoTO::AVISO_RECORRENTE_ATRASADO,
            'id_entidade' => $to->getId_entidade(),
            'bl_ativo' => 1
        )));


        if (!is_object($dados)) {
            throw new Exception('Erro ao retornar Mensagem Padrão - Aviso recorrente atrasado');
        } elseif (empty($dados->toArray())) {
            THROW new Zend_Validate_Exception("Nenhuma mensagem definida na entidade {$to->getId_entidade()} para aviso recorrente atrasado.");
        } else {
            $dados = $dados->toArray();
        }
        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());

        $arParametros = array(
            'id_lancamento' => $to->getId_lancamento(),
            'id_venda' => (!empty($params['id_venda']) ? $params['id_venda'] : null),
            'id_entidade' => $to->getId_entidade()
        );

        $textoSistemaBO = new TextoSistemaBO ();
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(
                array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema()))
            , $arParametros)
            ->subtractMensageiro()
            ->getFirstMensagem()
            ->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuariolancamento());

        $mensagemTO->setSt_mensagem("Parcela em atraso");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


    /**
     * Envio de mensagem de aviso quando é feita a baixa de uma parcela recorrente
     * @param Ead1_TO_Dinamico $to
     * @param int $id_tipoenvio
     * @return mixed
     * @throws Zend_Validate_Exception
     */
    public function gerarMensagemAvisoRecorrentePago(LancamentoTO $to, $id_tipoenvio = TipoEnvioTO::EMAIL, $params = array())
    {

        if (!$to->getId_entidade()) {
            throw new Exception('O Lançamento precisa vir completo para o método gerarMensagemAvisoRecorrentePago');
        }

        if (empty($params['id_venda'])) {
            throw new Exception('Inforome o id_venda no parâmetro $params');
        }

        $envioMensagemTO = new EnvioMensagemTO ();
        $mensagemTO = new MensagemTO ();
        $envioDestinatarioTO = new EnvioDestinatarioTO ();
        $dados = $this->_dao->retornarEmailEntidadeMensagem(new EmailEntidadeMensagemTO(array(
            'id_mensagempadrao' => MensagemPadraoTO::AVISO_RECORRENTE_PAGO,
            'id_entidade' => $to->getId_entidade(),
            'bl_ativo' => 1
        )));

        if (!is_object($dados)) {
            throw new Exception('Erro ao retornar Mensagem Padrão - Aviso recorrente pago');
        } elseif (empty($dados->toArray())) {
            THROW new Zend_Validate_Exception("Nenhuma mensagem definida na entidade {$to->getId_entidade()} para aviso recorrente pago.");
        } else {
            $dados = $dados->toArray();
        }

        $emailEntidadeMensagemTO = Ead1_TO_Dinamico::encapsularTo($dados, new EmailEntidadeMensagemTO(), true);
        $envioMensagemTO->setId_emailconfig($emailEntidadeMensagemTO->getId_emailconfig());

        $arParametros = array(
            'id_lancamento' => $to->getId_lancamento(),
            'id_venda' => (!empty($params['id_venda']) ? $params['id_venda'] : null),
            'id_entidade' => $to->getId_entidade()
        );

        $textoSistemaBO = new TextoSistemaBO ();
        $html_mensagem = $textoSistemaBO->gerarHTML(new TextoSistemaTO(
                array('id_textosistema' => $emailEntidadeMensagemTO->getId_textosistema()))
            , $arParametros)
            ->subtractMensageiro()
            ->getFirstMensagem()
            ->getSt_texto();

        $mensagemTO->setId_entidade($to->getId_entidade());
        $mensagemTO->setId_usuariocadastro($to->getId_usuario() ? $to->getId_usuario() : $mensagemTO->getSessao()->id_usuario);
        $mensagemTO->setSt_texto($html_mensagem);
        $envioDestinatarioTO->setId_tipodestinatario(TipoDestinatarioTO::DESTINATARIO);
        $envioDestinatarioTO->setId_usuario($to->getId_usuariolancamento());

        $mensagemTO->setSt_mensagem("Parcela recebida");
        $envioMensagemTO->setId_tipoenvio($id_tipoenvio);
        $envioMensagemTO->setDt_enviar(new Zend_Date());
        $envioMensagemTO->setId_evolucao(EnvioMensagemTO::NAO_ENVIADO);

        $resposta ['envioMensagemTO'] = $envioMensagemTO;
        $resposta ['arrEnvioDestinatarioTO'] [] = $envioDestinatarioTO;
        $resposta ['mensagemTO'] = $mensagemTO;

        return $resposta;
    }


}

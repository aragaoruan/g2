<?php
/**
 * Classe com regras de negócio para interações de Núcleo
 * @author João Gabriel - jgvasconcelos16@gmail.com
 * @since 29/11/2011
 * 
 * @package models
 * @subpackage bo
 */
class ProtocoloBO extends Ead1_BO {

	/**
	 * @var ProtocoloDAO
	 */
	public $dao;
	
	public $mensageiro;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new ProtocoloDAO();
	}
	
	/**
	 * Metodo que retorna protocolo
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProtocolo(ProtocoloTO $pTO) {
		try{
			$dados = $this->dao->retornarProtocolo($pTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Protocolo Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProtocoloTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Protocolo!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna protocolo com o nome do usuário
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProtocoloUsuario(ProtocoloTO $pTO) {
		try{
			$dados = $this->dao->retornarProtocoloUsuario($pTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Protocolo Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProtocoloUsuarioTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Protocolo!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo cadastra protocólo
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProtocolo(ProtocoloTO $pTO){
		try{
			if(!$pTO->getId_entidade()){
				$pTO->setId_entidade($pTO->getSessao()->id_entidade);
			}
			if(!$pTO->getId_usuariocadastro()){
				$pTO->setId_usuariocadastro($pTO->getSessao()->id_usuario);
			}
			$id_protocolo = $this->dao->cadastrarProtocolo($pTO);
			if(!$id_protocolo){
				throw new Zend_Exception('Erro ao fazer insert');
			}
			$pTO->setId_protocolo($id_protocolo);
			return $this->mensageiro->setMensageiro($pTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Protocólo!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
}

?>

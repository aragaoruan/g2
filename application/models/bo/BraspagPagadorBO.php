<?php


class BraspagPagadorBO extends Ead1_BO implements Ead1_IPagamentoCartao  {

    private $integracao = false;


    /**
     * seta a integração
     * @throws Zend_Exception
     */
    private function setIntegracao($id_entidade = null){

        $this->integracao = new EntidadeIntegracaoTO();
        $this->integracao->setId_entidade($id_entidade ? $id_entidade : $this->integracao->getSessao()->id_entidade);
        $this->integracao->setId_sistema(SistemaTO::BRASPAG);
        $this->integracao->fetch(false,true, true);

        if(!$this->integracao->getSt_codchave()){
            throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
        }

    }

    /**
     * Retorna a Integração
     * @return EntidadeIntegracaoTO
     */
    private function getIntegracao(){
        return $this->integracao;
    }

    /**
     * Conecta ao WebService da operadora ou gateway de pagamento
     */
    public function __construct($id_entidade = null){
        $this->setIntegracao($id_entidade);
    }


    /**
     * Solicita uma autorização de pagamento
     * @param array $dadosCartao - Array com dados do Cartão que será convertido no TO correspondente
     * @return Ead1_Mensageiro
     */
    public function autorizar(array $dadosCartao){

        try {
            $integracao = $this->getIntegracao();


            if(!$integracao){
                throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
            }

            $entCartao = new VwEntidadeCartaoTO();
            $entCartao->setId_entidade($integracao->getId_entidade());
            $entCartao->setId_cartaobandeira($dadosCartao['id_cartaobandeira']);
            $entCartao->fetch(false,true,true);

            $cartao 	= new PagamentoCartaoBrasPagTO();
            $cartao->montaToDinamico($dadosCartao);
            $cartao->setId_cartaooperadora($entCartao->getId_cartaooperadora());

// 			Zend_Debug::dump($cartao,__CLASS__.'('.__LINE__.')');exit;

            if(!$cartao->getId_cartaobandeira()){
                throw new Zend_Exception('Não foi encontrada a relação REDE -> BANDEIRA para este pagamento');
            }

            $venda = new VendaTO();
            $venda->setId_venda($cartao->getId_venda());
            $venda->fetch(true,true,true);

            $usuario = new VwPessoaTO();
            $usuario->setId_usuario($venda->getId_usuario());
            $usuario->setId_entidade($venda->getId_entidade());
            $usuario->fetch(false,true,true);


            $atTO = new AuthorizeTransactionTO();

            $atTO->setUrlBraspag($integracao->getSt_caminho());

            $atTO->setMerchantId($integracao->getSt_codchave());
            $atTO->setOrderId($venda->getId_venda());

            $atTO->setCustomerIdentity($venda->getId_usuario());
            $atTO->setCustomerName($usuario->getSt_nomecompleto());
            $atTO->setCustomerEmail($usuario->getSt_email());
            $atTO->setPaymentMethod($cartao->getId_cartaobandeira());
            $atTO->setAmount((int)$cartao->getNu_valortotal());

            $atTO->setNumberOfPayments($cartao->getnu_parcelas());
            $atTO->setPaymentPlan(($cartao->getnu_parcelas() == 1 ? '0' : '1'));

            $atTO->setCardHolder($cartao->getSt_nomeimpresso());
            $atTO->setCardNumber($cartao->getSt_cartao());
            $atTO->setCardSecurityCode($cartao->getSt_codigoseguranca());
            $atTO->setCardExpirationDate($cartao->getNu_mesvalidade().'/'.$cartao->getNu_anovalidade());



            /** Cadastrando a Transação Financeira **/
            $tTO = new TransacaoFinanceiraTO();
            $tTO->setId_cartaobandeira($entCartao->getId_cartaobandeira());
            $tTO->setId_cartaooperadora($entCartao->getId_cartaooperadora());
            $tTO->setId_venda($venda->getId_venda());

            $tTO->setNu_parcelas($cartao->getnu_parcelas());
            $tTO->setSt_ultimosdigitos(substr($cartao->getSt_cartao(), '12', '15'));
            $tTO->setSt_titularcartao($cartao->getSt_nomeimpresso());

            $tfBO = new TransacaoFinanceiraBO();
            $metra = $tfBO->cadastrarTransacaoFinanceira($tTO);

            if($metra->getTipo()!=Ead1_IMensageiro::SUCESSO){
                throw new Zend_Exception("Erro ao cadastrar a transação financeira, favor tentar novamente mais tarde.");
            }
            $pagador 	= new Braspag_Pagador();
            $retorno 	= $pagador->autorizar($atTO);
            $resultado 	= $retorno->getMensagem();

            if(!isset($resultado['AuthorizeTransactionResult'])){
                return $retorno;
            }

            $nu_status = $resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['Status'];
            if($nu_status == 2){
                $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
                $em = $bootstrap->getResource('doctrine')->getEntityManager();

                $st_codigoresposta = $resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['ReturnCode'];
                $st_codigoresposta = strlen($st_codigoresposta) == 1 ? '0'.$st_codigoresposta : $st_codigoresposta;
                $respostaTransacao = $em->getRepository('G2\Entity\RespostaAutorizacao')->findOneBy(array('st_codigoresposta' => $st_codigoresposta));
                if ($respostaTransacao)
                    $tTO->setId_respostaautorizacao($respostaTransacao->getId_respostaautorizacao());
            }

            $tTO->setUn_transacaofinanceira($resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['BraspagTransactionId']);
            $tTO->setUn_orderid($resultado['AuthorizeTransactionResult']['OrderData']['BraspagOrderId']);
            $tTO->setSt_codtransacaooperadora($resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['AcquirerTransactionId']);
            $tTO->setSt_codtransacaogateway($resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['AuthorizationCode']);
            $tTO->setId_cartaobandeira($entCartao->getId_cartaobandeira());
            $tTO->setId_cartaooperadora($entCartao->getId_cartaooperadora());
            $tTO->setSt_mensagem(serialize($resultado));
            $tTO->setId_venda($cartao->getId_venda());
            $tTO->setNu_valorcielo($resultado['AuthorizeTransactionResult']['PaymentDataCollection']['PaymentDataResponse']['Amount']/100);
            $tTO->setNu_status($nu_status);

            /** Atualizando a Transação Financeira em caso de resposta da Braspag**/
            $tfBO->editarTransacaoFinanceira($tTO);

            return new Ead1_Mensageiro($tTO, $retorno->getTipo(), $tTO);

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, (isset($tTO) ? $tTO : null));
        }

    }

    /**
     * Verifica uma solicitação de pagamento
     */
    public function verificar(TransacaoFinanceiraTO $to){

        $orderData = null;

        try {
            $integracao = $this->getIntegracao();

            if(!$integracao){
                throw new Zend_Validate_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
            }

            if(!$to->getId_venda()){
                throw new Zend_Validate_Exception("A Venda não foi informada no TransacaoFinanceiraTO!");
            }

            $venda = new VendaTO();
            $venda->setId_venda($to->getId_venda());
            $venda->fetch(true, true, true);

            if(!$venda->getNu_valorliquido()){
                throw new Zend_Validate_Exception("A Venda não foi encontrada!");
            }
            $urlbraspag = (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_PRODUCAO ? 'https://www.pagador.com.br/services/pagadorQuery.asmx?WSDL' : 'https://homologacao.pagador.com.br/services/pagadorQuery.asmx?WSDL');

            $pagador = new Braspag_Pagador();
            if($to->getUn_orderid()==false){

                $order = $this->verificarPagamentoIdVenda($to->getId_venda());
                if($order->getTipo()!=Ead1_IMensageiro::SUCESSO){
                    return new Ead1_Mensageiro($order->getFirstMensagem(), Ead1_IMensageiro::ERRO);
                }
                if(count($order->getMensagem())==1){
                    $orderData = $order->getFirstMensagem();
                } else {
                    foreach($order->getMensagem() as $compra){
                        $orderData = $compra;
                        if(isset($compra['Status']) &&  $compra['Status']==1){
                            break;
                        }
                    }
                }
            } else {

                $ccto = new GetOrderPagadorTO();
                $ccto->setUrlBraspag($urlbraspag);
                $ccto->setMerchantId($integracao->getSt_codchave());
                $ccto->setBraspagOrderId($to->getUn_orderid());
                $order = $pagador->GetOrderData($ccto);

                if($order->getTipo()==Ead1_IMensageiro::SUCESSO){
                    $order = $order->getMensagem();
                    if(isset($order['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse'])){
                        $orderData = $order['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse'];
                    } else {
                        return new Ead1_Mensageiro("Venda não encontrada na Braspag", Ead1_IMensageiro::ERRO);
                    }
                } else {
                    return new Ead1_Mensageiro($order->getFirstMensagem(), Ead1_IMensageiro::ERRO);
                }
            }

            if(!$orderData)
                return new Ead1_Mensageiro("Venda não encontrada na Braspag", Ead1_IMensageiro::ERRO);

            if($orderData['Status']==1){
                return new Ead1_Mensageiro($orderData, Ead1_IMensageiro::SUCESSO, 'Pagamento Autorizado');
            } elseif($orderData['Status']>=2) {
                return new Ead1_Mensageiro($orderData, Ead1_IMensageiro::SUCESSO, 'Pagamento Negado');
            } else {
                return new Ead1_Mensageiro($orderData, Ead1_IMensageiro::SUCESSO, 'Pagamento ainda não processado');
            }

        } catch (Zend_Validate_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }


    /**
     *
     * Verifica o Pagamento na Braspag pelo id_venda
     * @param int $id_venda
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function verificarPagamentoIdVenda($id_venda){

        try {

            $arTran = null;

            $integracao = $this->getIntegracao();

            if(!$integracao){
                throw new Zend_Validate_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
            }

            if(!$id_venda){
                throw new Zend_Validate_Exception("A Venda não foi informada!");
            }

            $venda = new VendaTO();
            $venda->setId_venda($id_venda);
            $venda->fetch(true, true, true);

            if(!$venda->getNu_valorliquido()){
                throw new Zend_Validate_Exception("A Venda não foi encontrada!");
            }
            $urlbraspag = (Ead1_Ambiente::getAmbiente() == Ead1_Ambiente::AMBIENTE_PRODUCAO ? 'https://www.pagador.com.br/services/pagadorQuery.asmx?WSDL' : 'https://homologacao.pagador.com.br/services/pagadorQuery.asmx?WSDL');

            $ccto = new GetOrderIdDataPagadorTO();
            $ccto->setUrlBraspag($urlbraspag);
            $ccto->setMerchantId($integracao->getSt_codchave());
            $ccto->setOrderId($id_venda);

            $pagador = new Braspag_Pagador();
            $meorder   = $pagador->GetOrderIdData($ccto);
            if($meorder->getTipo()==Ead1_IMensageiro::SUCESSO){
                $order   = $meorder->getFirstMensagem();
            } else {
                throw new Exception($meorder->getFirstMensagem());
            }

            if(isset($order['GetOrderIdDataResult']['OrderIdDataCollection']['OrderIdTransactionResponse']['CorrelationId'])){
                $arTran[0]['BraspagOrderId'] 		= $order['GetOrderIdDataResult']['OrderIdDataCollection']['OrderIdTransactionResponse']['BraspagOrderId'];
                $arTran[0]['BraspagTransactionId'] 	= $order['GetOrderIdDataResult']['OrderIdDataCollection']['OrderIdTransactionResponse']['BraspagTransactionId']['guid'];
            } elseif(isset($order['GetOrderIdDataResult']['OrderIdDataCollection']['OrderIdTransactionResponse'][0])){
                foreach($order['GetOrderIdDataResult']['OrderIdDataCollection']['OrderIdTransactionResponse'] as $response){
                    $arTran[] = array('BraspagOrderId'=>$response['BraspagOrderId'], 'BraspagTransactionId'=>$response['BraspagTransactionId']['guid'] );
                }
            }

            $arrRetorno = null;

            if($arTran){
                foreach ($arTran as $tran){

                    $to = new GetOrderPagadorTO();
                    $to->setBraspagOrderId($tran['BraspagOrderId']);
                    $to->setMerchantId($integracao->getSt_codchave());
                    $to->setUrlBraspag($urlbraspag);

                    $orderData = $pagador->GetOrderData($to)->getFirstMensagem();

                    if(isset($orderData['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse']['BraspagTransactionId'])){
                        $arrRetorno[] = $orderData['GetOrderDataResult']['TransactionDataCollection']['OrderTransactionDataResponse'];
                    }
// 					Zend_Debug::dump($tran,__CLASS__.'('.__LINE__.')');
// 					Zend_Debug::dump($orderData,__CLASS__.'('.__LINE__.')');


                }
            } else {
                throw new Zend_Validate_Exception("A Venda não foi encontrada na Braspag!");
            }

            if($arrRetorno) {
                return new Ead1_Mensageiro($arrRetorno, Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Zend_Validate_Exception("Os dados de pagamento da Venda não foram encontrados na Braspag!");
            }

        } catch (Zend_Validate_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Solicita um cancelamento de pagamento
     */
    public function cancelar(TransacaoFinanceiraTO $to, VendaTO $venda){

        try {

            $integracao = $this->getIntegracao();

            if(!$integracao){
                throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
            }

            $vcctTO = new VoidCreditCardTransactionTO();
            $vcctTO->setMerchantId($integracao->getSt_codchave());
            $vcctTO->setAmount($venda->getNu_valorliquido());
            $vcctTO->setBraspagTransactionId($to->getUn_transacaofinanceira());
            $vcctTO->setUrlBraspag($integracao->getSt_caminho());


            $pagador = new Braspag_Pagador();
            $retorno = $pagador->cancelar($vcctTO);
            if($retorno->getTipo()!=Ead1_IMensageiro::SUCESSO){
                throw new Zend_Exception($retorno->getMensagem());
            }

            return $retorno;
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }

    /**
     * Solicita o estorno de um pagamento
     */
    public function estornar(){

        return new Ead1_Mensageiro('Não implementado!', Ead1_IMensageiro::ERRO);

    }


}

<?php
/**
 * Classe que encapsula dados de turma
 * @author eduardoromao
 */
class TurmaBO extends Ead1_BO
{

    private $_dao;

    public function __construct()
    {
        $this->_dao = new TurmaDAO();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que salva um array de turmamatricula
     * @param array $arrTurmaMatriculaTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaMatricula($arrTurmaMatriculaTO)
    {
        try {
            $this->_dao->beginTransaction();
            $mensageiro = null;
            if (empty($arrTurmaMatriculaTO)) {
                $mensageiro = new Ead1_Mensageiro("Nenhuma Matrícula Selecionada!", Ead1_IMensageiro::AVISO);
            }
            $delected = false;
            $orm = new TurmaMatriculaORM();
            foreach ($arrTurmaMatriculaTO as $to) {
                if (!$delected) {
                    $delected = true;
                    $mensageiro = $this->editarTurmaMatricula(new TurmaMatriculaTO(array('bl_ativo' => false)), "id_turma = " . $to->getId_turma());
                    $mensageiro->subtractMensageiro();
                }
                $toConsulta = clone $to;
                $toConsulta->setBl_ativo(null);
                $tempTO = $orm->consulta($toConsulta, false, true);
                if ($tempTO) {
                    $to->setId_turmamatricula($tempTO->getId_turmamatricula());
                }
                $mensageiro = $this->salvarTurmaMatricula($to);
                $mensageiro->subtractMensageiro();
            }
            $this->_dao->commit();
            $this->mensageiro->setMensageiro("Matrículas da Turma Salvas com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->_dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), $mensageiro->getTipo(), $mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Método que salva um array de turmaentidade
     * @param array $arrTurmaEntidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaEntidade($arrTurmaEntidadeTO)
    {
        try {
            $this->_dao->beginTransaction();
            $mensageiro = null;
            if (empty($arrTurmaEntidadeTO)) {
                $mensageiro = new Ead1_Mensageiro("Nenhuma Instituição Selecionada!", Ead1_IMensageiro::AVISO);
            }
            $delected = false;
            $orm = new TurmaEntidadeORM();
            foreach ($arrTurmaEntidadeTO as $to) {
                if (!$delected) {
                    $delected = true;
                    $mensageiro = $this->editarTurmaEntidade(new TurmaEntidadeTO(array('bl_ativo' => false)), "id_turma = " . $to->getId_turma());
                    $mensageiro->subtractMensageiro();
                }
                $toConsulta = clone $to;
                $toConsulta->setBl_ativo(null);
                $tempTO = $orm->consulta($toConsulta, false, true);
                if ($tempTO) {
                    $to->setId_turmaentidade($tempTO->getId_turmaentidade());
                }
                $mensageiro = $this->salvarTurmaEntidade($to);
                $mensageiro->subtractMensageiro();
            }
            $this->_dao->commit();
            $this->mensageiro->setMensageiro("Instituções da Turma Salvas com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->_dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), $mensageiro->getTipo(), $mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Método que salva um array de turmaprojeto
     * @param array $arrTurmaProjetoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaProjeto($arrTurmaProjetoTO)
    {
        try {
            $this->_dao->beginTransaction();
            $mensageiro = null;
            if (empty($arrTurmaProjetoTO)) {
                $mensageiro = new Ead1_Mensageiro("Nenhum Projeto Pedagógico Selecionado!", Ead1_IMensageiro::AVISO);
            }
            $orm = new TurmaProjetoORM();
            $delected = false;
            foreach ($arrTurmaProjetoTO as $to) {
                if (!$delected) {
                    $delected = true;
                    $this->_dao->editarTurmaProjeto(new TurmaProjetoTO(array('bl_ativo' => false)), "id_turma = " . $to->getId_turma());
                }
                $toConsulta = clone $to;
                $toConsulta->setBl_ativo(null);
                $tempTO = $orm->consulta($toConsulta, false, true);
                if ($tempTO) {
                    $to->setId_turmaprojeto($tempTO->getId_turmaprojeto());
                }
                $mensageiro = $this->salvarTurmaProjeto($to);
                $mensageiro->subtractMensageiro();
            }
            $this->_dao->commit();
            $this->mensageiro->setMensageiro("Projetos Pedagógicos da Turma Salvos com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->_dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), $mensageiro->getTipo(), $mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que salva a Turma
     * @param TurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarTurma(TurmaTO $to)
    {
        return ($to->getId_turma() ? $this->editarTurma($to) : $this->cadastrarTurma($to));
    }

    /**
     * Método que salva o vinculo da matricula com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaMatricula(TurmaMatriculaTO $to)
    {
        return ($to->getId_turmamatricula() ? $this->editarTurmaMatricula($to) : $this->cadastrarTurmaMatricula($to));
    }

    /**
     * Método que salva o vinculo da entidade com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaEntidade(TurmaEntidadeTO $to)
    {
        return ($to->getId_turmaentidade() ? $this->editarTurmaEntidade($to) : $this->cadastrarTurmaEntidade($to));
    }

    /**
     * Método que salva o vinculo do projeto com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaProjeto(TurmaProjetoTO $to)
    {
        return ($to->getId_turmaprojeto() ? $this->editarTurmaProjeto($to) : $this->cadastrarTurmaProjeto($to));
    }

    /**
     * Método que cadastra a turma
     * @param TurmaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarTurma(TurmaTO $to)
    {
        try {
            $to->setId_entidadecadastro($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $to->getSessao()->id_entidade);
            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);
            $id_turma = $this->_dao->cadastrarTurma($to);
            $to->setId_turma($id_turma);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Turma.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra o vinculo da turma com matricula
     * @param TurmaMatriculaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarTurmaMatricula(TurmaMatriculaTO $to)
    {
        try {
            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);

            $id_turmamatricula = $this->_dao->cadastrarTurmaMatricula($to);
            $to->setId_turmamatricula($id_turmamatricula);
            $this->_dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Vincular Matricula com a Turma.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra o vinculo da turma com Entidade
     * @param TurmaEntidadeTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarTurmaEntidade(TurmaEntidadeTO $to)
    {
        try {
            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);
            $id_turmaentidade = $this->_dao->cadastrarTurmaEntidade($to);
            $to->setId_turmaentidade($id_turmaentidade);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Vincular Instituição com a Turma.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra o vinculo da turma com projeto
     * @param TurmaProjetoTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarTurmaProjeto(TurmaProjetoTO $to)
    {
        try {
            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);
            $id_turmaprojeto = $this->_dao->cadastrarTurmaProjeto($to);
            $to->setId_turmaprojeto($id_turmaprojeto);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Vincular Projeto Pedagógico a Turma.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que edita a turma
     * @param TurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function editarTurma(TurmaTO $to)
    {
        try {
            $this->_dao->editarTurma($to);
            $this->mensageiro->setMensageiro("Turma Editada com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar a Turma!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que edita o vinculo da turma com a matricula
     * @param TurmaMatriculaTO $to
     * @return Ead1_Mensageiro
     */
    public function editarTurmaMatricula(TurmaMatriculaTO $to)
    {
        try {
            $this->_dao->editarTurmaMatricula($to);
            $this->mensageiro->setMensageiro("Vinculo da Turma com a Matricula Editado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar vinculo da Turma com a Matricula!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que edita o vinculo da turma com a matricula
     * @param TurmaEntidadeTO $to
     * @param String $where
     * @return Ead1_Mensageiro
     */
    public function editarTurmaEntidade(TurmaEntidadeTO $to, $where)
    {
        try {
            $this->_dao->editarTurmaEntidade($to, $where);
            $this->mensageiro->setMensageiro("Vinculo da Turma com a Instituição Editado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar vinculo da Turma com a Instituição!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que edita o vinculo da turma com o Projeto
     * @param TurmaProjetoTO $to
     * @return Ead1_Mensageiro
     */
    public function editarTurmaProjeto(TurmaProjetoTO $to)
    {
        try {
            $this->_dao->editarTurmaProjeto($to);
            $this->mensageiro->setMensageiro("Vinculo da Turma com o Projeto Pedagógico Editado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar vinculo da Turma com o Projeto Pedagógico!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a turma
     * @param TurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurma(TurmaTO $to)
    {
        try {
            $to->setId_entidadecadastro($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $to->getSessao()->id_entidade);

            $dados = $this->_dao->retornarTurma($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TurmaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Turma.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os vinculos da turma com a Matricula
     * @param TurmaMatriculaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaMatricula(TurmaMatriculaTO $to)
    {
        try {
            $dados = $this->_dao->retornarTurmaMatricula($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TurmaMatriculaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Vinculos da Turma com a Matricula.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os vinculos da turma com a Entidade
     * @param TurmaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaEntidade(TurmaEntidadeTO $to)
    {
        try {
            $dados = $this->_dao->retornarTurmaEntidade($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TurmaEntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Vinculos da Turma com a Instituição.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os vinculos da turma com o projeto
     * @param TurmaProjetoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaProjeto(TurmaProjetoTO $to)
    {
        try {
            $dados = $this->_dao->retornarTurmaProjeto($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TurmaProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Vinculos da Turma com o Projeto Pedagógico.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os vinculos da turma com o projeto e mais entidades
     * @param VwConferenciaTurmasTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarConferenciaTurmas(VwConferenciaTurmasTO $to, $where = null)
    {
        try {
            $to->set_idEntidade($to->getSessao()->id_entidade);
            $dados = $this->_dao->retornarVwConferenciaTurmas($to, $where);

            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwConferenciaTurmasTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Vinculos da Turma com o Projeto Pedagógico.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    public function retornarProjetosTurmas(VwConferenciaProjetoConsolidadoTO $to, $where = null, $order = null)
    {
        try {
            //$to->setId_entidade($to->set_idEntidade() ? $to->set_idEntidade() : $to->getSessao()->id_entidade);
            $to->set_idEntidade($to->getSessao()->id_entidade);

            $dados = $this->_dao->retornarVwConferenciaProjetoConsolidado($to, $where, $order);

            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwConferenciaProjetoConsolidadoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Vinculos da Turma com o Projeto Pedagógico.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

}

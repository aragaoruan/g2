<?php

/**
 * @author Denise
 * Classe de regra de negócios para os logs
 *
 */
class LogBO extends Ead1_BO {
	
	
	public $mensageiro;
	
	
	public function __construct(){
		
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		
	}
	

	
	/**
	 * Método que salva um log. 
	 * @param LogAcessoTO $log
	 * @param array $params
	 * @return Ead1_Mensageiro
	 */
	public function gravaLog(LogAcessoTO $log){

        if(Ead1_Sessao::getSessaoUsuario()->st_gravalog=='nao'){
            $this->mensageiro->setMensageiro("Log setado para não ser salvo.",Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        }
        //recebe o id_funcionalidade
        try{
            if(!$log->getId_funcionalidade()){
                $this->mensageiro->setMensageiro('O id_funcionalidade é obrigatório e não foram passados!',Ead1_IMensageiro::AVISO);
            }else{
                $negocio = new \G2\Negocio\LogAcesso();

                $logAcesso = array(
                    'id_entidade'=>Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_perfil'=>Ead1_Sessao::getSessaoPerfil()->id_perfil,
                    'id_usuario'=>Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_funcionalidade'=>$log->getId_funcionalidade(),
                    'id_saladeaula'=>$log->getId_saladeaula()
                );

                $log = $negocio->salvaLogAcesso($logAcesso);

                if($log){
                    $this->mensageiro->setMensageiro('Log Salvo com Sucesso', Ead1_IMensageiro::SUCESSO);
                }
            }
        }catch (Zend_Validate_Exception $e){
            $this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
        }catch (Exception $e){
            $this->mensageiro->setMensageiro("Erro ao salvar o log: ".$e->getMessage() ,Ead1_IMensageiro::ERRO,$e->getMessage());
        }

        return $this->mensageiro;
	}
	
	public function retornaLog(){
		
	}
	
	/**
	 * Método de listagem de VwLogMatricula
	 * @param VwLogMatriculaTO $to
	 * @param $dt_cadastroinicio
	 * @param $dt_cadastrofim
	 * @return Ambigous <boolean, multitype:>
	 * @see LogRO::listarVwLogMatricula();
	 */
	public function listarVwLogMatricula(VwLogMatriculaTO $to, $dt_cadastroinicio, $dt_cadastrofim){
		$dao = new LogDAO();
		
		try {
			$orm = new VwLogMatriculaORM();
			$where = $orm->montarWhere($to);
			if (!empty($where)) {
				$where .= " and ";
			}
			$convertedDataInicio = new Zend_Date( new Zend_Date($dt_cadastroinicio), Zend_Date::ISO_8601 );
			$convertedDataInicio->setHour(00);
			$convertedDataInicio->setMinute(00);
			$convertedDataInicio->setSecond(00);
			$convertedDataFim = new Zend_Date( new Zend_Date($dt_cadastrofim), Zend_Date::ISO_8601 );
			$convertedDataFim->setHour(23);
			$convertedDataFim->setMinute(59);
			$convertedDataFim->setSecond(59);
			$where .= " dt_cadastro >= '".$this->converteDataBanco($convertedDataInicio, false, true)."' and dt_cadastro <= '".$this->converteDataBanco($convertedDataFim, true, true)."'";
			$retorno = $dao->listarVwLogMatricula($to, $where);
			if($retorno){
				return new Ead1_Mensageiro($retorno, Ead1_IMensageiro::SUCESSO);
			} else {
				return new Ead1_Mensageiro('Nenhum registro encontrado!', Ead1_IMensageiro::AVISO);
			}
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro: '.$e->getMessage(), Ead1_IMensageiro::ERRO);
		}

	}
	
	
}

?>
<?php
/**
 * Classe com regras de negócio para interações de comissão
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class ComissaoBO extends Ead1_BO {

	/**
	 * @var ComissaoDAO
	 */
	private $dao;
	
	public $mensageiro;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new ComissaoDAO();
	}
	
	/**
	 * Metodo que retorna comissão com função e categoria
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissaoEntidadeFuncao(ComissaoEntidadeTO $ceTO) {
		try{
			if(!$ceTO->getId_entidade()){
				$ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarComissaoEntidadeFuncao($ceTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Comissão Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ComissaoCompletaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna comissão com função e categoria
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissaoEntidade(ComissaoEntidadeTO $ceTO) {
		try{
			if(!$ceTO->getId_entidade()){
				$ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarComissaoEntidade($ceTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Comissão Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ComissaoEntidadeTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de comissão
	 * @param TipoComissaoTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoComissao(TipoComissaoTO $tcTO) {
		try{
			$dados = $this->dao->retornarTipoComissao($tcTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Tipo de Comissão Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoComissaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de valor de comissão
	 * @param TipoValorComissaoTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoValorComissao(TipoValorComissaoTO $tvcTO) {
		try{
			$dados = $this->dao->retornarTipoValorComissao($tvcTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Tipo de Valor de Comissão Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoValorComissaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Valor de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de meta.
	 * @param TipoMetaTO $tmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoMeta(TipoMetaTO $tmTO) {
		try{
			$dados = $this->dao->retornarTipoMeta($tmTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Tipo de Meta Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoMetaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Meta!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna base de cálculo de comissão.
	 * @param BaseCalculoComTO $bccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarBaseCalculoCom(BaseCalculoComTO $bccTO) {
		try{
			$dados = $this->dao->retornarBaseCalculoCom($bccTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Base de Cálculo Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new BaseCalculoComTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Base de Cálculo!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de cálculo de comissão.
	 * @param TipoCalculoComTO $tccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCalculoCom(TipoCalculoComTO $tccTO) {
		try{
			$dados = $this->dao->retornarTipoCalculoCom($tccTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Tipo de Cálculo Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoCalculoComTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Cálculo!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarRegraComissao(RegraComissaoTO $rcTO) {
		try{
			$dados = $this->dao->retornarRegraComissao($rcTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Regra de Comissão Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new RegraComissaoTO()), Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Regra de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra comissão.
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		try{
			if(!$ceTO->getId_entidade()){
				$ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
			}
			$id_comissaoentidade = $this->dao->cadastrarComissaoEntidade($ceTO);
			$ceTO->setId_comissaoentidade($id_comissaoentidade);
			return $this->mensageiro->setMensageiro($ceTO, Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarRegraComissao(RegraComissaoTO $rcTO){
		try{
			$id_regracomissao = $this->dao->cadastrarRegraComissao($rcTO);
			$rcTO->setId_regracomissao($id_regracomissao);
			return $this->mensageiro->setMensageiro($rcTO, Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Regra de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Cadastra as regras para comissão
	 * @param array $arrayRegras
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArrayRegraComissao($arrayRegras){
		try {
			if(!empty($arrayRegras)){
				$regraDeletar = new RegraComissaoTO();
				$regraDeletar->setId_comissaoentidade($arrayRegras[0]->getId_comissaoentidade());
				$this->dao->excluirRegraComissao($regraDeletar);
				foreach ($arrayRegras as $regra){
					$regra->setId_regracomissao(0);
					$regra->setId_usuariocadastro($regraDeletar->getSessao()->id_usuario);
					$this->dao->cadastrarRegraComissao($regra);
				}
			}
			return $this->mensageiro->setMensageiro('Regra(s) cadastradas com sucesso!', Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Regra(s) de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que deleta regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirRegraComissao(RegraComissaoTO $rcTO){
		try{
			$this->dao->excluirRegraComissao($rcTO);
			return $this->mensageiro->setMensageiro('Regra deletada com sucesso.', Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Deletar Regra de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita comissão.
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function editarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		try{
			if($this->dao->editarComissaoEntidade($ceTO)){
				return $this->mensageiro->setMensageiro('Comissão editada com sucesso.', Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita regra de comissão.
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function editarRegraComissao(RegraComissaoTO $rcTO){
		try{
			$this->dao->editarRegraComissao($rcTO);
			return $this->mensageiro->setMensageiro('Regra editada com sucesso.', Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Regra de Comissão!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
}
<?php

/**
 * Classe com regras de negócio para interações de login
 * @author edermariano
 *
 * @package models
 * @subpackage bo
 */
class LoginBO extends Ead1_BO
{

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Atributo que contém todas as funcionalidades
     * @var array
     */
    public $funcionalidades;

    /**
     * Método que contém a árvore com as funcionalidades
     * @var array
     */
    public $arvoreFuncionalidades;

    /**
     * Método que realiza login e envia dados de entidades e perfils para o usuário logado
     * @param LoginTO $to
     */
    public function logar(LoginTO $to, $simulado = false)
    {

        try {
            $dao = new LoginDAO();
            if ($simulado) {
                if (Ead1_Mensageiro::su()) {
                    $rsp = $dao->verificarUsuarioSimulado($to->getUsuario());
                } else {
                    return new Ead1_Mensageiro("Usuário não pode ser simulado pois o usuário logado não é SU.", Ead1_IMensageiro::ERRO);
                }
            } else {
                $rsp = $dao->verificarUsuario($to->getUsuario(), $to->getSenha());
            }

            if ($rsp) {
                $usuarioPerfilEntidades = $dao->retornaUsuarioPerfilEntidade($rsp->id_usuario);

                $arEntidades = false;
                if (!$simulado) {
                    //Zend_Auth::getInstance()->getStorage()->write($rsp->id_usuario);
                    Zend_Auth::getInstance()->getStorage()->write($rsp->id_usuario);
                }

                $sessao = new Zend_Session_Namespace('geral');
                $sessao->id_usuario = $rsp->id_usuario;


                if (!empty($usuarioPerfilEntidades)) {
                    foreach ($usuarioPerfilEntidades as $chave => $valor) {
                        if ($valor['id_sistema'] == 1) {
                            if ($sessao->id_usuario != Zend_Auth::getInstance()->getIdentity()) {
                                $sessao->nomeUsuario = $sessao->nomeUsuario . ' simulando: ' . $valor['st_nomecompleto'];
                            } else {
                                $sessao->nomeUsuario = $valor['st_nomecompleto'];
                            }
                            $arEntidades[$valor['id_entidade']]['st_nomeentidade'] = Ead1_Encode::applicationEncode($valor['st_nomeentidade']);
                            $arEntidades[$valor['id_entidade']]['id_entidade'] = $valor['id_entidade'];
                            $arEntidades[$valor['id_entidade']]['st_urllogoentidade'] = (($valor['st_urlimglogo']) ? $valor['st_urlimglogo'] : 'http://actor.ead1.com.br/layout/padrao/imagens/logo_75x75.png');
                            $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['id_entidade'] = $valor['id_entidade'];
                            $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['id_perfil'] = $valor['id_perfil'];
                            $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['st_nomeperfil'] = (!empty($valor['st_perfilalias']) ? Ead1_Encode::applicationEncode($valor['st_perfilalias']) : Ead1_Encode::applicationEncode($valor['st_nomeperfil']));
                        }
                    }
                } else {
                    return new Ead1_Mensageiro("Não foi encontrado vínculo deste usuário com algum perfil ou entidade.", Ead1_IMensageiro::ERRO);
                }

                return new Ead1_Mensageiro($arEntidades, Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro("Usuário e/ou senha inválidos.", Ead1_IMensageiro::ERRO);
            }
        } catch (Exception $exc) {
            return new Ead1_Mensageiro("Erro do Sistema! Comunique a EAD1! ERRO: " . $exc->getMessage() . ".", Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     *
     * Efetua o Login do Usuário via Token
     * @param $st_token
     * @return Ead1_Mensageiro
     */
    public function logarPorToken($st_token)
    {

        try {


            $epu = $this->em->getRepository('G2\Entity\VwUsuarioPerfilEntidade')->findOneBy(array('st_token' => $st_token));

            if ($epu instanceof \G2\Entity\VwUsuarioPerfilEntidade) {

                $sessao = new Zend_Session_Namespace('geral');
                $sessao->id_usuario = $epu->getid_usuario();
                $sessao->id_entidade = $epu->getId_entidade();
                $sessao->id_perfil = $epu->getId_perfil();
                $sessao->st_nomeperfil = $epu->getSt_nomeperfil();
                $sessao->nomeUsuario = $epu->getst_nomecompleto();

                Zend_Auth::getInstance()->getStorage()->write($epu->getid_usuario());

                return new \Ead1_Mensageiro("Usuário logado com sucesso!", \Ead1_IMensageiro::SUCESSO);

            } else {

                throw new \Exception("Usuário não encontrado");
            }

        } catch (Exception $exc) {
            return new Ead1_Mensageiro("Erro do Sistema! Comunique a Unyleya! ERRO: " . $exc->getMessage() . ".", Ead1_IMensageiro::ERRO, $exc);
        }
    }

    public function usuarioLogado()
    {
        $sessao = Zend_Session::namespaceGet('geral');
        try {

            $loginBO = new LoginBO();

            if (isset($sessao['id_entidade']) && isset($sessao['id_usuario']) && isset($sessao['id_perfil'])) {
                return true;
            } else {
                return false;
            }
        } catch (Zend_Exception $e) {

        }
    }


    public function pegarPerfisUsuarioLogado()
    {

        try {

            $dao = new LoginDAO();
            $sessao = new Zend_Session_Namespace('geral');


            $usuarioPerfilEntidades = $dao->retornaUsuarioPerfilEntidade(Zend_Auth::getInstance()->getIdentity());

            $arEntidades = false;

            if (!empty($usuarioPerfilEntidades)) {
                foreach ($usuarioPerfilEntidades as $chave => $valor) {
                    if ($valor['id_sistema'] == 1) {
                        if ($sessao->id_usuario != Zend_Auth::getInstance()->getIdentity()) {
                            $sessao->nomeUsuario = $sessao->nomeUsuario . ' simulando: ' . $valor['st_nomecompleto'];
                        } else {
                            $sessao->nomeUsuario = $valor['st_nomecompleto'];
                        }
                        $arEntidades[$valor['id_entidade']]['st_nomeentidade'] = Ead1_Encode::applicationEncode($valor['st_nomeentidade']);
                        $arEntidades[$valor['id_entidade']]['id_entidade'] = $valor['id_entidade'];
                        $arEntidades[$valor['id_entidade']]['st_urllogoentidade'] = (($valor['st_urlimglogo']) ? $valor['st_urlimglogo'] : 'http://actor.ead1.com.br/layout/padrao/imagens/logo_75x75.png');
                        $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['id_entidade'] = $valor['id_entidade'];
                        $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['id_perfil'] = $valor['id_perfil'];
                        $arEntidades[$valor['id_entidade']]['perfil'][$valor['id_perfil']]['st_nomeperfil'] = (!empty($valor['st_perfilalias']) ? $valor['st_perfilalias'] : $valor['st_nomeperfil']);
                    }
                }
            } else {
                return new Ead1_Mensageiro("Não foi encontrado vínculo deste usuário com algum perfil ou entidade.", Ead1_IMensageiro::ERRO);
            }

            return new Ead1_Mensageiro($arEntidades, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro("Erro do Sistema! Comunique a EAD1! ERRO: " . $exc->getMessage() . ".", Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Método que tenta fazer o login a partir de uma sessão do Zend_Session
     */
    public function logarDaSessao()
    {
        $sessao = Zend_Session::namespaceGet('geral');
        try {

            if (isset($sessao['id_entidade']) && isset($sessao['id_usuario']) && isset($sessao['id_perfil'])) {

                $selecionarPerfilTO = new SelecionarPerfilTO();
                $selecionarPerfilTO->setId_entidade($sessao['id_entidade']);
                $selecionarPerfilTO->setId_usuario($sessao['id_usuario']);
                $selecionarPerfilTO->setId_perfil($sessao['id_perfil']);

                $mensageiro = new Ead1_Mensageiro($this->selecaoPerfil($selecionarPerfilTO), Ead1_IMensageiro::SUCESSO);
            } else {

                $mensageiro = new Ead1_Mensageiro("Usuário não está logado", Ead1_IMensageiro::ERRO);
            }
            return $mensageiro;
        } catch (Zend_Exception $e) {
            return false;
        }
    }

    /**
     * Método que retorna a árvore de menu
     * @param SelecionarPerfilTO $to
     */
    public function selecaoPerfil(SelecionarPerfilTO $to)
    {

        $pTO = new PerfilTO();
        $pTO->setId_perfil($to->getId_perfil());

        $pTO->fetch(false, true, true);

        $sessao = new Zend_Session_Namespace('geral');

        $sessao->id_entidade = $to->getId_entidade();
        $sessao->id_perfil = $to->getId_perfil();
        $sessao->st_nomeperfil = $pTO->getSt_nomeperfil();
        $sessao->id_linhadenegocio = null;
        $sessao->bl_provapordisciplina = 0;

        $negocio = new \G2\Negocio\Negocio();

        // Seta se as avalicoes sao por disciplina ou prova global
        $en_confg = $negocio->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
            'id_entidade' => $to->getId_entidade(),
            'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::AVALIACAO_POR_DISCIPLINA
        ));
        if ($en_confg instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao) {
            $sessao->bl_provapordisciplina = (int)$en_confg->getSt_valor();
        }


        //Seta linha de negocio da entidade
        $negocio = new G2\Negocio\EsquemaConfiguracao();
        $entidade = $negocio->find('\G2\Entity\Entidade', (int)$to->getId_entidade());

        if ($entidade instanceof \G2\Entity\Entidade && $entidade->getId_entidade()) {
            if ($entidade->getId_esquemaconfiguracao() instanceof \G2\Entity\EsquemaConfiguracao) {
                $linhanegocio = $negocio->findOneBy('\G2\Entity\EsquemaConfiguracaoItem'
                    , array('id_esquemaconfiguracao' => (int)$entidade->getId_esquemaconfiguracao()->getid_esquemaconfiguracao()
                    , 'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO));

                if ($linhanegocio instanceof \G2\Entity\EsquemaConfiguracaoItem && $linhanegocio->getst_valor()) {
                    $to->setId_linhadenegocio((int)$linhanegocio->getst_valor());
                    $sessao->id_linhadenegocio = $to->getId_linhadenegocio();
                }


            }
        }

        $dao = new LoginDAO();
        $dadosUsuario = $dao->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);

        if (!is_array($dadosUsuario))
            $dadosUsuario = array();


        foreach ($dadosUsuario as $chave => $valor) {
            if (substr($chave, 0, 3) == 'st_' && !mb_check_encoding($valor, 'utf-8'))
                $dadosUsuario[$chave] = Ead1_Encode::applicationEncode($valor);
        }
        $dadosUsuario['id_linhadenegocio'] = $to->getId_linhadenegocio();
        $dadosUsuario['bl_provapordisciplina'] = $sessao->bl_provapordisciplina;
        $dao = new MenuDAO();
        $dados = $dao->mostrarMenuPrincipal($sessao->id_perfil, $sessao->id_entidade);
        $this->funcionalidades = $dao->mostrarMenuPorPerfilEntidade($sessao->id_perfil, $sessao->id_entidade);

        foreach ($dados as $chave => $funcionalidade) {
            $sessaoLabelFuncionalidade = new Zend_Session_Namespace('labelFuncionalidade');
            $sessaoLabelFuncionalidade->funcionalidades[$funcionalidade['id_funcionalidade']]['funcionalidade'] = $funcionalidade['st_funcionalidade'];
            $sessaoLabelFuncionalidade->funcionalidades[$funcionalidade['id_funcionalidade']]['label'] = $funcionalidade['st_label'];

            $this->arvoreFuncionalidades[$chave] = $funcionalidade;
            $this->arvoreFuncionalidades[$chave]['filhos'] = $this->montarMenuRecursivo($funcionalidade);
        }
        $dadosUsuario['menuprincipal'] = Ead1_TO_Dinamico::encapsularTo($this->arvoreFuncionalidades, new PerfilMenuPrincipalTO());

        return new PerfilMenuTO($dadosUsuario);
    }

    public function setaSessaoGeral($id_usuario, $nomeUsuario, $id_entidade, $id_perfil)
    {
        try {
            $sessao = new Zend_Session_Namespace('geral');

            $sessao->id_usuario = (!empty($id_usuario) ? $id_usuario : $sessao->id_usuario);
            $sessao->id_usuario = (!empty($id_usuario) ? $id_usuario : $sessao->id_usuario);
            $sessao->id_entidade = (!empty($id_entidade) ? $id_entidade : $sessao->id_entidade);
            $sessao->id_perfil = (!empty($id_perfil) ? $id_perfil : $sessao->id_perfil);
            $sessao->nomeUsuario = (!empty($nomeUsuario) ? $nomeUsuario : $sessao->id_usuario);

            return new Ead1_Mensageiro('Sessão restaurada.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao restaurar sessão.', Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que monta o menu recursivo
     * @param mixed $funcionalidadePai
     */
    private function montarMenuRecursivo($funcionalidadePai)
    {
        foreach ($this->funcionalidades as $chave => $funcionalidadeAtual) {
            if ($funcionalidadeAtual['id_funcionalidadepai'] == $funcionalidadePai['id_funcionalidade']) {

                $sessaoLabelFuncionalidade = new Zend_Session_Namespace('labelFuncionalidade');
                $sessaoLabelFuncionalidade->funcionalidades[$funcionalidadeAtual['id_funcionalidade']]['funcionalidade'] = $funcionalidadeAtual['st_funcionalidade'];
                $sessaoLabelFuncionalidade->funcionalidades[$funcionalidadeAtual['id_funcionalidade']]['label'] = $funcionalidadeAtual['st_label'];

                if (!isset($funcionalidadePai['filhos'])) {
                    $funcionalidadePai['filhos'] = null;
                }
                $funcionalidadeAtual['filhos'] = $this->montarMenuRecursivo($funcionalidadeAtual);
                if (isset($funcionalidadeAtual['st_tipofuncionalidade'])) {
                    $arFuncionalidadeAtual[0] = $funcionalidadeAtual;
                    $funcionalidadeAtualTO = Ead1_TO_Dinamico::encapsularTo($arFuncionalidadeAtual, new $funcionalidadeAtual['st_tipofuncionalidade'], true);
                }
                $funcionalidadePai['filhos'][] = $funcionalidadeAtualTO;
            }
        }
        return (isset($funcionalidadePai['filhos']) ? $funcionalidadePai['filhos'] : null);
    }

    /**
     * Método que encapsula TOs
     * @param String $classe
     * @param mixed $dados
     */
    public function encapsularMenu($classe, $dados)
    {
        $dadosAtual[0] = $dados;
        return Ead1_TO_Dinamico::encapsularTo($dadosAtual, new $classe, true);
    }

    /**
     * Metodo que gera Login do Usuario
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function geradorDeLogin(UsuarioTO $uTO)
    {
        $pBO = new PessoaBO();
        do {
            $nome = strtolower($uTO->getSt_nomecompleto());
            $nome = substr($nome, 0, 15);
            $limpo = strtolower($this->substituirCaracteresEspeciaisAtualizado($nome));

            if (strpos($limpo, ' ')) {
                $arrNome = explode(' ', $limpo);
                $login = '';
                foreach ($arrNome as $n) {
                    if (strlen($n) > 3) {
                        $login .= $n;
                    }
                }
            } else {
                $login = $limpo . $this->geradorHash(2, true);
            }
            $login .= $this->geradorHash(4);
            $uTO->setSt_login($login);
            unset($nome);
            unset($limpo);
            unset($arrNome);
            unset($login);
        } while (!$pBO->verificaLogin($uTO));
        return $uTO;
    }

    /**
     * Gera senha aleatório
     * @return string
     */
    public function geradorDeSenha()
    {
        $senha = $this->geradorHash(12);
        return $senha;
    }

    /**
     * Método de Recuperar a Senha Pelo Email e Login ou CPF
     * @param VwPessoaTO $vwPessoaTO
     */
    public function recuperarSenhaPessoa(VwPessoaTO $vwPessoaTO)
    {
        try {
            $msgCodigo = '';
            if ($vwPessoaTO->getSt_cpf()) {
                $filtro = new Zend_Filter_Digits();
                $vwPessoaTO->setSt_cpf($filtro->filter($vwPessoaTO->getSt_cpf()));
                if (!$this->validaCPF($vwPessoaTO->getSt_cpf())) {
                    THROW new Zend_Validate_Exception("CPF Inválido!");
                }
            }
            if ($vwPessoaTO->getSt_login()) {
                if (!$this->validaLoginUsuario($vwPessoaTO->getSt_login()) && $this->validaLogin($vwPessoaTO->getSt_login())) {
                    THROW new Zend_Validate_Exception("Login Inválido!");
                }
            }
            if ($vwPessoaTO->getSt_email()) {
                $validate = new Zend_Validate_EmailAddress();
                if (!$validate->isValid($vwPessoaTO->getSt_email())) {
                    THROW new Zend_Validate_Exception("Email Inválido!");
                }
            }
            $id_entidade = $vwPessoaTO->getId_entidade();
            $pessoaDAO = new PessoaDAO();
            $other = array();
            if ($vwPessoaTO->getSt_login()) {
//                $other[] = "st_login like '" . $vwPessoaTO->getSt_login() . "'";
                $where['st_login'] = $vwPessoaTO->getSt_login();
            }
            if ($vwPessoaTO->getSt_cpf()) {
//                $other[] = "st_cpf like '" . $vwPessoaTO->getSt_cpf() . "'";
                $where['st_cpf'] = $vwPessoaTO->getSt_cpf();
            }
            if ($vwPessoaTO->getId_entidade()) {
//                $other[] = "id_entidade = " . $vwPessoaTO->getId_entidade();
                $where['id_entidade'] = $vwPessoaTO->getId_entidade();
            }

            if ($vwPessoaTO->getSt_email()) {
//                $other[] = "id_entidade = " . $vwPessoaTO->getId_entidade();
                $where['st_email'] = $vwPessoaTO->getSt_email();
            }

//            $where = "st_email like '" . $vwPessoaTO->getSt_email() . "' AND (" . implode(" AND ", $other) . ")";
            $dados = $pessoaDAO->retornarUsuarioRecuperarSenha(new VwPessoaTO(), $where);

            if (!$dados->count()) {
                if (!$vwPessoaTO->getId_entidade()) {
                    THROW new Zend_Validate_Exception("Nenhum Usuário Encontrado.");
                } else {
                    unset($other[count($other) - 1]);
//                    $where = "st_email like '" . $vwPessoaTO->getSt_email() . "' AND (" . implode(" AND ", $other) . ")";
                    $dados = $pessoaDAO->retornarUsuarioRecuperarSenha($vwPessoaTO, $where);
                    if (!$dados->count() || empty($dados)) {
                        THROW new Zend_Validate_Exception("Nenhum Usuário Encontrado.");
                    }
                }
            }

            $dados = $dados->toArray();

            $vwPessoaTO = Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaTO(), true);

            $pessoaTO = new PessoaTO();
            $pessoaTO->mergeTO($pessoaTO, $vwPessoaTO);

            //@todo necessario para não editar o nome do usuario com charset errado, verificar isso depois....
            $pessoaTO->setSt_nomeexibicao(Ead1_Encode::applicationEncode($vwPessoaTO->getSt_nomecompleto()));


            $mensagemBO = new MensagemBO();
            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO, $pessoaTO, TipoEnvioTO::EMAIL, true, $id_entidade)->subtractMensageiro();

            $this->mensageiro->setMensageiro("O E-mail de recuperação de senha foi enviado com sucesso.", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $msgCodigo);
        }
        return $this->mensageiro;
    }

    /**
     * Método que valida os parametros recebidos na Action recuperar senha
     * @param Mixed $dataAutenticacao
     * @param Mixed $idUsuario
     * @param Mixed $idEntidade
     * @throws Zend_Validate_Exception
     */
    public function validaParamsRecuperarSenha($dataAutenticacao, $idUsuario, $idEntidade)
    {
        if (is_null($dataAutenticacao) || !is_string($dataAutenticacao)) {
            THROW new Zend_Validate_Exception("Data de Autenticação Inválida.");
        }
        if (!$idUsuario || !$idEntidade || !is_int($idEntidade) || !is_int($idUsuario)) {
            THROW new Zend_Validate_Exception("Parametros Inválidos.");
        }
        $dataFinal = new Zend_Date(null, Zend_Date::ISO_8601);
        $dataFinal = $dataFinal->addDay(8);
        $dataAuth = new Zend_Date(str_replace('.', '-', $dataAutenticacao), Zend_Date::ISO_8601);
        if (!$dataAuth->isEarlier($dataFinal)) {
            THROW new Zend_Validate_Exception("A Data de Validade de este Email de Recuperação de Senha Expirou.");
        }
    }

    /**
     * Retorna os dados do usuárop
     * @param int $idUsuario
     * @param int $idEntidade
     * @param int $idPerfil
     * @return Ead1_Mensageiro
     */
    public function retornaDadosUsuario($idUsuario, $idEntidade, $idPerfil)
    {

        try {

            $loginDAO = new LoginDAO();
            $dadosUsuario = $loginDAO->retornaDadosUsuario($idUsuario, $idEntidade, $idPerfil);
            return new Ead1_Mensageiro(new UsuarioTO($dadosUsuario), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Método que retorna o Usuário, baseado em usuario e senha ou e-mail e senha dependendo da entidade
     * @param LoginTO $to
     * @param int $id_entidade
     * @return Ead1_Mensageiro
     */
    public function retornaVwPessoaLogin(LoginTO $to, $id_entidade = null)
    {


        try {
            $dao = new PessoaORM();
            $pessoaBO = new PessoaBO();
            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setSt_senha($to->getSenha());

            if ($id_entidade) {
                $vwPessoaTO->setId_entidade($id_entidade);
                $vwPessoaTO->setSt_login($to->getUsuario());
                $vwPessoaTO->setSt_email($to->getUsuario());
            } else {
                $vwPessoaTO->setSt_login($to->getUsuario());
            }

            $wherelogin = $dao->getAdapter()->quoteInto('st_login = ?', $vwPessoaTO->getSt_login());
            $whereloginentidade = $dao->getAdapter()->quoteInto('st_loginentidade = ?', $vwPessoaTO->getSt_login());

            $wheresenha = $dao->getAdapter()->quoteInto('st_senha = ?', $to->getSenha());
            $wheresenhaentidade = $dao->getAdapter()->quoteInto('st_senhaentidade = ?', $to->getSenha(false));


            if ($vwPessoaTO->getSt_email()) {
                $whereemail = $dao->getAdapter()->quoteInto('st_email = ?', $vwPessoaTO->getSt_email());
            } else {
                $whereemail = $dao->getAdapter()->quoteInto('st_email = ?', $vwPessoaTO->getSt_login());
            }

            $where = "(
	            			(($whereemail or $wherelogin) AND $wheresenha) 
	            			OR 
	            			(($whereemail or $whereloginentidade) AND $wheresenhaentidade)
            			)";
            if ($id_entidade) {
                $where .= " AND (id_entidade = $id_entidade)";
            }

            $arrpessoa = $pessoaBO->retornarVwPessoa($vwPessoaTO, $where);
            if ($arrpessoa) {
                $pessoa = Ead1_TO_Dinamico::encapsularTo($arrpessoa, new VwPessoaTO());
                return new Ead1_Mensageiro($pessoa[0], Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro('Usuário não encontrado!', Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

}

function url_exists($url)
{
    // Version 4.x supported
    $handle = curl_init($url);
    if (false === $handle) {
        return false;
    }
    curl_setopt($handle, CURLOPT_HEADER, false);
    curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
    curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15")); // request as if Firefox
    curl_setopt($handle, CURLOPT_NOBODY, true);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
    $connectable = curl_exec($handle);
    curl_close($handle);
    return $connectable;
}

<?php

/**
 * Classe com regras de negócio para interações de Sala de Aula
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 11/11/2010
 *
 * @package models
 * @subpackage bo
 */
class SalaDeAulaBO extends Ead1_BO
{

    const ID_USUARIO_PERFIL_ENTIDADE_REFERENCIA = 15;
    public $mensageiro;

    //Id ao cadastrar professor na tabela usuario perfil entidade referencia
    /**
     * @var SalaDeAulaDAO
     */
    private $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new SalaDeAulaDAO();
    }

    /**
     * Metodo que cadastra a disciplina da sala de aula, caso aja mais vinculos os exclui para ficar 1 só
     * @param DisciplinaSalaDeAulaTO $dsdaTO
     * @return Ead1_Mensageiro
     */
    public function salvarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO)
    {
        $orm = new DisciplinaSalaDeAulaORM();
        $toDelete = new DisciplinaSalaDeAulaTO();
        $toDelete->setId_saladeaula($dsdaTO->getId_saladeaula());
        if (!$this->dao->deletarDisciplinaSalaDeAula($toDelete)) {
            return $this->mensageiro->setMensageiro('Erro ao Salvar Disciplina da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->cadastrarDisciplinaSalaDeAula($dsdaTO);
    }

    /**
     * Metodo que Vincula a Disciplina a Sala de Aula
     * @param DisciplinaSalaDeAulaTO $dsdaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO)
    {
        if (!$this->dao->cadastrarDisciplinaSalaDeAula($dsdaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao vincular sala de aula à disciplina.', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Sucesso ao vincular sala de aula à disciplina.', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Desaloca todas as salas de uma matrícula.
     * @param int $id_matricula
     */
    public function desalocarSalasMatricula($id_matricula)
    {
        try {
            $vwSalaDisciplinaAlocacaoTO = new VwSalaDisciplinaAlocacaoTO();
            $vwSalaDisciplinaAlocacaoTO->setId_matricula($id_matricula);
            $where = 'id_matricula = ' . $vwSalaDisciplinaAlocacaoTO->getId_matricula();
            $where .= 'and id_alocacao is not null';
            $salas = $this->dao->retornarVwSalaDisciplinaAlocacao($vwSalaDisciplinaAlocacaoTO, $where)->toArray();
            if (!empty($salas)) {
                $salas = Ead1_TO_Dinamico::encapsularTo($salas, new VwSalaDisciplinaAlocacaoTO());
                foreach ($salas as $sala) {
                    $alocacaoTO = new AlocacaoTO();
                    $alocacaoTO->setId_alocacao($sala->getId_alocacao());
                    $alocacaoTO->setId_matriculadisciplina($sala->getId_matriculadisciplina());
                    $mensageiro = $this->inativarAlocacao($alocacaoTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        return $mensageiro;
                    }
                }
            }
            return $this->mensageiro->setMensageiro('Salas desalocadas da matrícula com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao desalocar salas da matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Inativa alocação no sistema e no moodle
     * @param AlocacaoTO $aTOConsulta
     * @return Ead1_Mensageiro
     * @deprecated use \G2\Negocio\Alocacao::desalocarAluno instead
     */
    public function inativarAlocacao(AlocacaoTO $aTOConsulta)
    {
        $negocio = new \G2\Negocio\Alocacao();
        $aTOConsulta->fetch(false, true, true);
        if ($aTOConsulta->getId_alocacao()) {
            return $negocio->desalocarAluno(array(
                'id_alocacao' => $aTOConsulta->getId_alocacao()
            ));
        }
        return new \Ead1_Mensageiro('', \Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que reintegra todos os alunos de uma sala de aula
     * Este método não possúi transação
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param SalaDeAulaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function reintegrarAlocacaoAlunoSalaDeAula(SalaDeAulaTO $to)
    {

        try {

            if (!$to->getId_saladeaula()) {
                throw new Zend_Exception('O ID da Sala de Aula não foi informado!');
            }
            $vwTO = new VwAlunosSalaTO();
            $vwTO->setId_saladeaula($to->getId_saladeaula());
            $mesala = $this->retornarVwAlunosSala($vwTO);

            if ($mesala->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception('Ocorreu o erro ao recuperar os Alunos: ' . $mesala->getFirstMensagem());
            }

            $alunos = $mesala->getMensagem();

            if (empty($alunos)) {
                throw new Zend_Exception('Nenhum Aluno encontrado!');
            }

            //busca os dados da sala
            $to->fetch(true, true, true);

            $integrados = 0;
            $naointegrados = 0;
            $semalocacao = 0;
            foreach ($alunos as $aluno) {

                $aTO = new AlocacaoTO();
                $aTO->setId_matriculadisciplina($aluno->getId_matriculadisciplina());
                $aTO->setId_saladeaula($aluno->getId_saladeaula());
                $aTO->setBl_ativo(true);
                $aTO->fetch(false, true, true);

                if ($aTO->getId_alocacao()) {
                    $reintegracao = $this->reintegrarAlocacaoAluno($aTO, $to->getId_entidadeintegracao());
                    if ($reintegracao->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $integrados++;
                    } else {
                        $naointegrados++;
                    }
                } else {
                    $semalocacao++;
                }
            }

            $mensagem = '';

            if ($integrados) {
                $mensagem .= $integrados . ' Alunos reintegrados. ';
            }
            if ($naointegrados) {
                $mensagem .= $naointegrados . ' Alunos não reintegrados. ';
            }
            if ($semalocacao) {
                $mensagem .= $semalocacao . ' Alunos não tinham Alocação no sistema. ';
            }

            return new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os alunos de uma sala de aula
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwAlunosSalaTO $to
     * @param Boolean $parametro_log - indica se é para salvar log ou não quando o link de acesso for utilizado, padrão false
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwAlunosSala(VwAlunosSalaTO $to, $parametro_log = false)
    {
        try {
            $salas = $this->dao->retornarVwAlunosSala($to);
            if (!$salas) {
                return $this->mensageiro->setMensageiro('Nenhum registro encontrado.', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }

            $config = Ead1_Ambiente::geral();
            foreach ($salas as $sala) {
                $sala->setst_urlacesso($config->st_url_portal . '/portal/login/autenticacao-direta?p=' . $sala->getId_usuario()
                    . '&m=' . $to->getSessao()->id_usuario
                    . '&e=' . $sala->getId_entidadematricula() . '&u=' . $sala->getSt_urlacesso() . ($parametro_log ? '' : '&l=nao'));
            }

            return $this->mensageiro->setMensageiro($salas, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar os alunos da sala de aula.', Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que reintegra um aluno a uma sala de aula
     * @param AlocacaoTO $aTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function reintegrarAlocacaoAluno(AlocacaoTO $aTO, $idEntidadeIntegracao = null)
    {

        $this->dao->beginTransaction();
        try {

            if (!$aTO->getId_alocacao()) {
                throw new Zend_Exception('O ID da Alocação não foi informado!');
            }
            $aTO->fetch(true, true, true);

            $aiTO = new AlocacaoIntegracaoTO();
            $aiTO->setId_alocacao($aTO->getId_alocacao());
            $aiTO->setid_entidadeintegracao($idEntidadeIntegracao);
            $aiTO->fetch(false, true, true);


            if ($aiTO->getId_alocacaointegracao()) {
                $this->dao->deletarAlocacaoIntegracao($aiTO->getId_alocacaointegracao());
            }

            $mensageiro = $this->salvarIntegracaoAlunoWS($aTO);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                // Favor não mudarem - Elcio Mauro Guimarães
                $this->dao->rollBack();
                return $mensageiro;
            }

            $this->dao->commit();
            return new Ead1_Mensageiro('Aluno reintegrado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Faz a integração do aluno
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param AlocacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function salvarIntegracaoAlunoWS(AlocacaoTO $aTO)
    {
        try {

            $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracaoTO->setId_saladeaula($aTO->getId_saladeaula());
            $salaDeAulaIntegracaoTO->fetch(false, true, true);

            $salaDeAulaTO = new SalaDeAulaTO();
            $salaDeAulaTO->setId_saladeaula($aTO->getId_saladeaula());
            $salaDeAulaTO->fetch(true, true, true);

            $vwAlocacaoTO = new VwAlocacaoTO();
            $vwAlocacaoTO->setId_alocacao($aTO->getId_alocacao());
            $vwAlocacaoTO->fetch(false, true, true);

            if ($salaDeAulaTO->getId_modalidadesaladeaula() == \G2\Constante\ModalidadeSalaDeAula::PRESENCIAL) {

                $entidadeIntegracaoTO = new EntidadeIntegracaoTO();
                $entidadeIntegracaoTO->setId_entidade($salaDeAulaTO->getId_entidade());
                $entidadeIntegracaoTO->setId_sistema(SistemaTO::HENRY7);
                $entidadeIntegracaoTO->fetch(true, true, true);

                // TEM INTEGRAÇÃO COM HENRY7 ?
                if ($entidadeIntegracaoTO->getId_entidadeintegracao()) {
                    if ($this->dao->executaAlocaAlunoH7($salaDeAulaTO, $vwAlocacaoTO))
                        $msgHenry7 = 'Aluno liberado no Henry7';
                    else
                        $msgHenry7 = '';
                }
            }


            switch ($salaDeAulaIntegracaoTO->getId_sistema()) {

                case SistemaTO::ACTOR:
                    $wsBO = new WsBO(Ead1_Ambiente::geral()->wsURL . 'services/matricula.php');
                    $mensageiro = $wsBO->cadastrarMatricula($aTO);
                    if ($mensageiro instanceOf Ead1_Mensageiro) {
                        throw new Zend_Exception("Aluno não alocado no Sistema Actor. " . $mensageiro->getCodigo());
                    }
                    break;

                case SistemaTO::MOODLE:

                    $ws = new MoodleAlocacaoWebServices($salaDeAulaTO->getId_entidade(), $salaDeAulaTO->getId_entidadeintegracao());


                    // Verifica se existe pessoa cadastrada na entidade da sala de aula, e caso não exista importa o cadastro do usuário.
                    $vwPessoaTO = new VwPessoaTO();
                    $vwPessoaTO->setId_usuario($vwAlocacaoTO->getId_usuario());
                    $vwPessoaTO->setId_entidade($salaDeAulaTO->getId_entidade());
                    $vwPessoaTO->fetch(false, true, true);
                    //verifica se o usuário não foi encontrado na entidade e importa
                    if (!$vwPessoaTO->getSt_nomecompleto()) {
                        $pessoaBO = new PessoaBO();
                        $pessoaBO->importarUsuarioEntidade($vwAlocacaoTO->getId_usuario(),
                            $vwAlocacaoTO->getId_entidadeatendimento(),
                            $salaDeAulaTO->getId_entidade(), $vwAlocacaoTO->getId_usuario());
                    }

                    $mensageiro = $ws->cadastrarAlocacaoIntegracao($aTO->getId_alocacao(), $salaDeAulaTO->getId_entidade());

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception("Exception: " . $mensageiro->getFirstMensagem());
                    }

                    break;

                case SistemaTO::AMAIS:

                    $AmaisBO = new AmaisBO();
                    $mensageiro = $AmaisBO->cadastrarUsuario($aTO, $salaDeAulaTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        // Favor não mudarem - Elcio Mauro Guimarães
                        return $mensageiro;
                    }
                    break;
                case SistemaTO::BLACKBOARD:

                    $aluno = $this->dao->retornarDadosMatriculaBlackBoard($aTO->getId_matriculadisciplina());
                    if (!$aluno) {
                        throw new Zend_Exception("Aluno não encontrado ou Sala de Aula ainda não integrada!");
                    }

                    $curso = json_decode($aluno['st_retornows']);
                    if (!$curso) {
                        throw new Zend_Exception("Não foi possível pegar os dados do Curso do BlackBoard!");
                    }


                    $bb = new BlackBoardBO($salaDeAulaTO->getId_entidade());

                    $pro = new ProjetoPedagogicoTO();
                    $pro->montaToDinamico($aluno);

                    $aps = new VwAreaProjetoSalaTO();
                    $aps->montaToDinamico($aluno);
                    /**
                     * Criamos o Grupo no BB com o Nome do Projeto Pedagógico caso o mesmo não exista
                     */
                    if ($aps->getSt_referencia() == false) {

                        $aps->fetch(false, true, true);

                        $vo = new BlackBoard_VO_GroupVO();
                        $vo->setCourseId($curso->id);
                        $vo->setTitle($pro->getSt_projetopedagogico());
                        $vo->setDescription($pro->getSt_descricao());
                        $vo->setAvailable('true');
                        $mgrupo = $bb->salvarGrupo($vo, $aps->getId_areaprojetosala());
                        if ($mgrupo->getTipo() != Ead1_IMensageiro::SUCESSO) {
                            throw new BlackBoard_Exception($mgrupo->getFirstMensagem());
                        }

                        $ap = new AreaProjetoSalaTO();
                        $ap->setId_areaprojetosala($aps->getId_areaprojetosala());
                        $ap->fetch(true, true, true);

                        $st_referencia = $ap->getSt_referencia();

                    } else {
                        $st_referencia = $aps->getSt_referencia();
                    }

                    if (!$st_referencia)
                        throw new \Zend_Exception("SalaDeAulaBO:652 -  Código do Grupo não encontrado!");

                    $matricula = $bb->processoMatricularAluno($aluno, $vwAlocacaoTO, $st_referencia);
                    if ($matricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        // Favor não mudarem - Elcio Mauro Guimarães
                        return $matricula;
                    }
                    break;

                case SistemaTO::HENRY7:
                    /**
                     * TODO: Integrar o sistema de catracas aqui também
                     */
                    break;
                default:
                    throw new Exception("Sistema não identificado.");
                    break;
            }

            return new Ead1_Mensageiro('Aluno integrado com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Ocorreu um erro ao Alocar o aluno. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Integraçao de Professores
     * @param SalaDeAulaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function reintegrarProfessoresSalaDeAula(SalaDeAulaTO $to)
    {

        try {

            //TODO BUSCAR ALUNOS
            if (!$to->getId_saladeaula()) {
                throw new Zend_Exception('O ID da Sala de Aula não foi informado!');
            }

            $pe = new PerfilBO();

            $pTO = new PerfilTO();
            $pTO->setId_perfilpedagogico(PerfilPedagogicoTO::PROFESSOR);
            $pTO->setId_entidade($to->getId_entidade());

            $perfil = $pe->retornaPerfil($pTO)->getFirstMensagem();
            if ($perfil instanceof PerfilTO) {

                $pito = new VwUsuarioPerfilEntidadeReferenciaTO();
                $pito->setId_saladeaula($to->getId_saladeaula());
                $pito->setId_perfil($perfil->getId_perfil());
                $pito->setId_entidade($to->getId_entidade());
                $pito->setBl_ativo(true);
                $pbo = new ProjetoPedagogicoBO();

                $professores = $pbo->retornarVwUsuarioPerfilEntidadeReferencia($pito)->getMensagem();
                $gerenciarSalaNegocio = new \G2\Negocio\GerenciaSalas();

                foreach ($professores as $professor) {
                    if ($professor instanceof VwUsuarioPerfilEntidadeReferenciaTO) {

                        $uperTO = new UsuarioPerfilEntidadeReferenciaTO();
                        $uperTO->montaToDinamico($professor->toArray());

                        $orm = new SalaDeAulaIntegracaoORM();
                        $salaDeAulaIntegracaoTO = $orm->consulta(new SalaDeAulaIntegracaoTO(
                            array(
                                'id_saladeaula' => $uperTO->getId_saladeaula()
                            )), false, true);

                        if ($salaDeAulaIntegracaoTO instanceof SalaDeAulaIntegracaoTO) {
                            switch ($salaDeAulaIntegracaoTO->getId_sistema()) {
                                case SistemaTO::ACTOR:
                                    $sdapTO = new SalaDeAulaProfessorTO();
                                    $sdapTO->setId_perfilReferencia($professor->getId_perfilReferencia());
                                    $gerenciarSalaNegocio->_processoIntegrarProfessorActor($uperTO, $sdapTO);
                                    break;
                                case SistemaTO::MOODLE:
                                    $gerenciarSalaNegocio->_processoIntegrarProfessorMoodle(
                                        $uperTO->getId_usuario(),
                                        $to->getId_saladeaula()
                                    );
                                    break;
                                case SistemaTO::BLACKBOARD:
                                    $gerenciarSalaNegocio->_processoIntegrarProfessorBlackBoard(
                                        $uperTO,
                                        $salaDeAulaIntegracaoTO
                                    );
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            return new Ead1_Mensageiro("Integração de Professores concluída", Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @deprecated METODO COPIADO PARA A NEGOCIO G2\Negocio\GerenciaSalas
     * Processo que Integra o Professor ao Actor
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @throws Zend_Exception
     */
    private function _processoIntegrarProfessorActor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
        try {
            $papelWebServices = new PapelWebServices();
            $retornoWS = $papelWebServices->vincularPapelProfessor($uperTO, $sdapTO);
            //Atualiza Papel se já existir
            if ($retornoWS['retorno'] == 'aviso' && $retornoWS['papel']['codpapel'] != '') {
                $retornoWS = $papelWebServices->atualizaPapel(array(
                    'codpapel' => $retornoWS['papel']['codpapel']
                , 'flastatus' => 'A'
                , 'flamaster' => ($sdapTO->bl_titular ? 'S' : 'N')
                ));
            }
            $perfilReferenciaIntegracaoTO = new PerfilReferenciaIntegracaoTO();
            $perfilReferenciaIntegracaoTO->setId_perfilreferencia($uperTO->id_perfilreferencia);
            $perfilReferenciaIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
            $perfilReferenciaIntegracaoTO->setId_usuariocadastro($uperTO->id_usuario);
            $perfilReferenciaIntegracaoTO->setSt_codsistema($retornoWS['papel']['codpapel']);
            $perfilReferenciaIntegracaoTO->fetch(false, true, true);
            //Verifica se o Usuario Ja tem Papel
            if (!$perfilReferenciaIntegracaoTO->id_perfilreferenciaintegracao) {
                $perfilReferenciaIntegracaoORM = new PerfilReferenciaIntegracaoORM();
                $perfilReferenciaIntegracaoORM->insert($perfilReferenciaIntegracaoTO->toArrayInsert());
            }
        } catch (Zend_Exception $e) {
            throw new Zend_Exception('Erro ao executar as rotinas no WebService: ' . $e->getMessage());
        }
    }

    /**
     * @deprecated METODO COPIADO PARA A NEGOCIO G2\Negocio\GerenciaSalas
     * Processo que Integra o Professor ao Moodle
     * @param int $idUsuario
     * @param int $idSalaDeAula
     * @throws Zend_Exception
     */
    private function _processoIntegrarProfessorMoodle($idUsuario, $idSalaDeAula)
    {
        $ws = new MoodleAlocacaoWebServices();
        $mensageiro = $ws->cadastrarPapel(
            $idUsuario,
            $idSalaDeAula,
            MoodleAlocacaoWebServices::ROLEID_PROFESSOR,
            null,
            false,
            'processoIntegrarProfessorMoodle');
        $mensageiro->subtractMensageiro();
    }

    /**
     * @deprecated METODO COPIADO PARA A NEGOCIO G2\Negocio\GerenciaSalas
     * @param UsuarioPerfilEntidadeReferenciaTO $up
     * @param SalaDeAulaIntegracaoTO $si
     */
    private function _processoIntegrarProfessorBlackBoard(UsuarioPerfilEntidadeReferenciaTO $up, SalaDeAulaIntegracaoTO $si)
    {

        $bb = new BlackBoardBO($up->getId_entidade());
        $mensageiro = $bb->processoCadastrarProfessor($up, $si);
        $mensageiro->subtractMensageiro();
    }

    /**
     * Metodo que salva um array de SalaDeAulaEntidadeTO
     * @param Array $arrSalaDeAulaEntidadeTO
     * @throws Zend_Exception
     */
    public function salvarArraySalaDeAulaEntidade($arrSalaDeAulaEntidadeTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!is_array($arrSalaDeAulaEntidadeTO)) {
                THROW new Zend_Exception('O Parametro Recebido Não é um Array!');
            }
            $delected = false;
            foreach ($arrSalaDeAulaEntidadeTO as $salaDeAulaEntidadeTO) {
                if (!($salaDeAulaEntidadeTO instanceof SalaDeAulaEntidadeTO)) {
                    THROW new Zend_Exception('Um dos Items do Array Não é uma instancia de SalaDeAulaEntidadeTO');
                }
                if (!$delected) {
                    $delected = true;
                    $salaDeAulaEntidadeTODelected = new SalaDeAulaEntidadeTO();
                    $salaDeAulaEntidadeTODelected->setId_saladeaula($salaDeAulaEntidadeTO->getId_saladeaula());
                    $this->dao->deletarSalaDeAulaEntidade($salaDeAulaEntidadeTODelected);
                }
                $this->dao->cadastrarSalaDeAulaEntidade($salaDeAulaEntidadeTO);
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro('Instituições Vinculadas a Sala de Aula com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular Instituições a Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to)
    {
        try {
            $this->dao->cadastrarSalaDeAulaEntidade($to);
            $this->mensageiro->setMensageiro('Instituição Vinculada a Sala de Aula com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Voncular Instituição a Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Cadastra várias salas com seus projetos e disciplina.
     * @param array $arrSalas
     * @return Ead1_Mensageiro
     */
    public function cadastrarArraySalas(array $arrSalas)
    {


        $arrAvaliacoes = array();
        try {
            $negocio = new \G2\Negocio\Negocio();
            $negocio->beginTransaction();

            if (!empty($arrSalas)) {
                $avaliacaoBO = new AvaliacaoBO();
                $projetoPedagogicoBO = new ProjetoPedagogicoBO();

                //$sala[0] -> SalaDeAulaTO
                //$sala[1] -> DisciplinaSalaDeAulaTO
                //$sala[2] -> Array de AreaProjetoSalaTO
                //$sala[3] -> UsuarioPerfilEntidadeReferenciaTO
                //$sala[4] -> AvaliacaoConjuntoReferenciaTO
                //$sala[5] -> Array de SalaDeAulaEntidadeTO
                //$sala[7] -> Array de TurmaSala

                foreach ($arrSalas as $key => $sala) {
                    $mensageiro = $this->cadastrarDadosBasicosSalaDeAula($sala[0], $sala[1])->subtractMensageiro();
                    $sala[0] = $mensageiro->getFirstMensagem();
                    $sala[6]->setId_saladeaula($mensageiro->getFirstMensagem()->getId_saladeaula());
                    foreach ($sala[2] as $areaProjetoSalaTO) {
                        $areaProjetoSalaTO->setId_saladeaula($sala[0]->id_saladeaula);
                    }

                    $this->salvarArrayAreaProjetoSala($sala[2])->subtractMensageiro();
                    $sala[4]->id_saladeaula = $sala[0]->id_saladeaula;
                    $arrAvaliacoes[] = $sala[4];
                    $avaliacaoBO->salvarArrAvaliacaoConjuntoReferencia($arrAvaliacoes)->subtractMensageiro();

                    $sala[3]->id_saladeaula = $sala[0]->id_saladeaula;
                    if ($sala[3]->getId_usuario()) {
                        $projetoPedagogicoBO->salvarCoordenador($sala[3])->subtractMensageiro();
                    }
                    $this->cadastrarSalaDeAulaEntidadeArvore($sala[5], $sala[0])->subtractMensageiro();

                    $arrSalas[$key][0] = $sala[0];
                    $arrSalas[$key][6] = $sala[6];
//                    Zend_Debug::dump($this->mensageiro->setMensageiro($arrSalas, Ead1_IMensageiro::SUCESSO));die;

                    if (array_key_exists(7, $arrSalas[$key]) && $arrSalas[$key][7]) {
                        $negocioTurmaSala = new \G2\Negocio\TurmaSala();
                        $negocioTurmaSala->salvaArrayTurmaSala($arrSalas[$key][7], $sala[0]->id_saladeaula);
                    }

                }
            }
            $negocio->commit();
            return $this->mensageiro->setMensageiro($arrSalas, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Salas:  ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * ========================== Depreciado =============================
     * Método de cadastro de sala de aula já utiliza o Docrtine, mudança foi feita para que
     * possa gerar logs.
     *
     *
     * Metodo que cadastra dados Basicos da Sala de Aula, menos as datas.
     * @param SalaDeAulaTO $sdaTO
     * @param DisciplinaSalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro
     * @deprecated
     * @atualização Neemias Santos <neemias.santos@unyleya.com.br>
     */
    public function cadastrarDadosBasicosSalaDeAula(SalaDeAulaTO $sdaTO, DisciplinaSalaDeAulaTO $dsaTO)
    {

        $sdaTO->setBl_ativa(1);
        $sdaTO->setId_entidade($sdaTO->getSessao()->id_entidade);
        $sdaTO->setId_usuariocadastro($sdaTO->getSessao()->id_usuario);
        $id_saladeaula = $this->dao->cadastrarSalaDeAula($sdaTO);
        if (!$id_saladeaula) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        } else {
            $dsaTO->setId_saladeaula($id_saladeaula);
            if ($this->dao->vincularDisciplinaSalaDeAula($dsaTO)) {
                $sdaTO->setId_saladeaula($id_saladeaula);
                return $this->mensageiro->setMensageiro($sdaTO, Ead1_IMensageiro::SUCESSO);
            } else {
                return $this->mensageiro->setMensageiro('Erro ao Cadastrar Sala de Aula! Não foi possível realizar o vínculo de Disciplina!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
            }
        }
    }

    /**
     * Metodo que Cadastra e Exclui as Permissões na Area, Projeto e Sala de Aula;
     * @param array ( AreaProjetoSalaTO $apsTO )
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function salvarArrayAreaProjetoSala($arrTO)
    {
        $arr = array();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $delected = false;
        $proximo = false;
        $arr['area'] = null;
        $arr['area'] = array();
        $arr['projeto'] = null;
        $arr['projeto'] = array();
        $arr['nivel'] = null;
        $arr['nivel'] = array();
        $this->dao->beginTransaction();
        try {
            foreach ($arrTO as $chave => $apsTO) {
                if (!$delected) {
                    $delected = true;
                    $delapsTO = new AreaProjetoSalaTO();
                    $delapsTO->setId_saladeaula($apsTO->getId_saladeaula());
                    if (!$this->dao->deletarAreaProjetoSala($delapsTO)) {
                        Throw new Zend_Exception('Erro ao Excluir Permissões da Sala de Aula!');
                    }
                }
                if (!($apsTO instanceof AreaProjetoSalaTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe AreaProjetoSalaTO!');
                }
                if (!$apsTO->getId_saladeaula()) {
                    THROW new Zend_Exception('O id_saladeaula não veio Setado!');
                }
                $contador = 0;
                if ($apsTO->getId_areaconhecimento()) {
                    if (!in_array($apsTO->getId_areaconhecimento(), $arr['area'])) {
                        $arr['area'][] = $apsTO->getId_areaconhecimento();
                    } else {
                        $proximo = true;
                    }
                    $contador++;
                }
                if ($apsTO->getId_nivelensino()) {
                    if (!in_array($apsTO->getId_nivelensino(), $arr['nivel'])) {
                        $arr['nivel'][] = $apsTO->getId_nivelensino();
                    } else {
                        $proximo = true;
                    }
                    $contador++;
                }
                if ($apsTO->getId_projetopedagogico()) {
                    if (!in_array($apsTO->getId_projetopedagogico(), $arr['projeto'])) {
                        $arr['projeto'][] = $apsTO->getId_projetopedagogico();
                    } else {
                        $proximo = true;
                    }
                    $contador++;
                }
                if ($contador > 1) {
                    THROW new Zend_Exception('Erro na Tipagem do AreaProjetoSalaTO!');
                }
                if ($contador == 0) {
                    THROW new Zend_Exception('Não Veio Nenhum Parametro Setado para o vínculo da Área com o Projeto Pedagógico!');
                }
                if ($proximo) {
                    $proximo = false;
                    continue;
                }
                $id_areaprojetosala = $this->dao->cadastrarAreaProjetoSala($apsTO);
                if (!$id_areaprojetosala) {
                    THROW new Zend_Exception('Erro ao Cadastrar Permissões!');
                }
                $apsTO->setId_areaprojetosala($id_areaprojetosala);
                unset($id_areaprojetosala);
                $this->mensageiro->addMensagem($apsTO->tipaTOFlex());
            }

            $this->dao->commit();
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);


            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que cadastra entidades da sala.
     * @param array $arrayDeEntidades
     * @param SalaDeAulaTO $sTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarSalaDeAulaEntidadeArvore($arrayDeEntidades, SalaDeAulaTO $sTO)
    {
        try {
            $seTO = new SalaDeAulaEntidadeTO();
            $seTO->setId_saladeaula($sTO->getId_saladeaula());
            if (!$this->dao->deletarSalaDeAulaEntidade($seTO)) {
                throw new Zend_Exception('Erro ao deletar ' . Ead1_LabelFuncionalidade::getLabel(3) . ' antigas');
            }
            foreach ($arrayDeEntidades as $entidade) {
                if (!$entidade->getId_saladeaula()) {
                    $entidade->setId_saladeaula($sTO->getId_saladeaula());
                }
                if (!$this->dao->cadastrarSalaDeAulaEntidade($entidade)) {
                    throw new Zend_Exception('Erro no insert');
                }
            }
            return $this->mensageiro->setMensageiro(Ead1_LabelFuncionalidade::getLabel(3) . "(s) cadastrado(s) com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar ' . Ead1_LabelFuncionalidade::getLabel(3) . ' para ' . Ead1_LabelFuncionalidade::getLabel(23) . '!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Cadastra as Datas da Sala de Aula, (edita a sala de aula).
     * @param SalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDatasSalaDeAula(SalaDeAulaTO $sdaTO)
    {
        if ($sdaTO->bl_usardoperiodoletivo === false) {
            $regra = $this->regraPerioLetivo($sdaTO);
            if ($regra !== true) {
                return $regra;
            }
        }
        $dt_abertura = $sdaTO->getDt_abertura();
        $dt_abertura->addHour(1);
        $sdaTO->setDt_abertura($dt_abertura);

        if (!$this->editarSalaDeAula($sdaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Datas da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Datas da Sala de Aula Cadastradas com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que contem Regras de cadastro das datas da Sala de Aula
     * @param SalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro | boolean true
     */
    public function regraPerioLetivo(SalaDeAulaTO $sdaTO)
    {
        $inicioInscricao = $this->converteDataBanco($sdaTO->getDt_inicioinscricao());
        $fimInscricao = $this->converteDataBanco($sdaTO->getDt_fiminscricao());
        $aberturaInscricao = $this->converteDataBanco($sdaTO->getDt_abertura());
        $encerramentoInscricao = $this->converteDataBanco($sdaTO->getDt_encerramento());
        $erro = '';
        if ($fimInscricao != null) {
            if (!$this->comparaData($inicioInscricao, $fimInscricao)) {
                $erro = 1;
                $this->mensageiro->addMensagem('O Fim da Inscrição não pode ser Menor que a Data de Início da Inscrição!');
            }
        }
        if ($encerramentoInscricao != null) {
            if (!$this->comparaData($aberturaInscricao, $encerramentoInscricao)) {
                $erro = 1;
                $this->mensageiro->addMensagem('A Data de Encerramento não pode ser Menor que a Data de Abertura!');
            }
            if (!$this->comparaData($inicioInscricao, $aberturaInscricao) || !$this->comparaData($inicioInscricao, $encerramentoInscricao)) {
                $erro = 1;
                $this->mensageiro->addMensagem('A Data de Início da Inscrição não pode ser Maior que a Data de Abertura ou de Encerramento!');
            }
        }

        if ($erro) {
            $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
            $this->mensageiro->setCodigo($sdaTO);
            return $this->mensageiro;
        }
        return true;
    }

    /**
     * Metodo que Edita Sala de Aula
     * @param SalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function editarSalaDeAula(SalaDeAulaTO $sdaTO)
    {
        $this->dao->beginTransaction();
        try {

            $sdaTO->setDt_atualiza(Zend_Date::now());
            $sdaTO->setId_usuarioatualiza($sdaTO->getSessao()->id_usuario);

            if (!$this->dao->editarSalaDeAula($sdaTO)) {
                $this->mensageiro->setMensageiro('Erro ao Editar Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
            } else {
                $sdaTO->fetch(true, true, true);
                $integracao = new IntegracaoBO();
                $mensageiro = $integracao->salvarSala($sdaTO);
                if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    throw new Zend_Exception($mensageiro->getFirstMensagem());
                }

            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Sala de Aula Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Vincula o Professor a Sala de Aula
     *
     * @atualização Neemias Santos <neemias.santos@unyleya.com.br>
     * Método foi alterado para que gere log usando Doctrine
     *
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function cadastrarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$uperTO->getId_entidade()) {
            $uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
        }

//		unset($sdapTO->bl_titular);
//		return $this->procedimentoSalaDeAulaProfessor($uperTO,$sdapTO);
        return $this->procedimentoSalvarSalaDeAulaProfessor($uperTO, $sdapTO);
    }

    /**
     * Metodo que faz o Procedimento de Cadastrar um Perfil de Professor ao Usuario, caso não tenha, e depois vincular-lo a uma sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoSalvarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$uperTO->getId_saladeaula()) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O id_saladeaula não veio setado!');
        }
        if (!$uperTO->getId_usuario()) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O id_usuario não veio setado!');
        }
        if (is_null($sdapTO->getBl_titular())) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O bl_titular não veio setado!');
        }
        $this->dao->beginTransaction();
        try {
            $pDAO = new PerfilDAO();
            $pTO = new PerfilTO();
            $pTO->setId_entidade($uperTO->getId_entidade());
            //@todo setado como perfil pedagogico padrao - professor
            $pTO->setId_perfilpedagogico(1);
            $pTO->setBl_ativo(true);
            $pTO->setId_perfil($uperTO->getId_perfil());
            $perfis = $pDAO->retornaPerfil($pTO);
            $perfis = $perfis->toArray();
            if (empty($perfis)) {
                $this->mensageiro->setCodigo($this->dao->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                THROW new Zend_Exception('Esta Instituição Não Possui Perfil Pedagógico de Professor!');
            }
            $id_perfil = $perfis[0]['id_perfil'];
            $upeTO = new UsuarioPerfilEntidadeTO();
            $upeTO->setId_entidade($uperTO->getId_entidade());
            $upeTO->setId_usuario($uperTO->getId_usuario());
            $upeTO->setId_perfil($id_perfil);
            $upeTO->setBl_ativo(true);
            $perfilProfessor = $pDAO->retornaUsuarioPerfilEntidade($upeTO);
            $perfilProfessor = $perfilProfessor->toArray();
            if (empty($perfilProfessor)) {
                //@todo ajustar a Situacao de acordo com a tabela
                $upeTO->setId_situacao(1);
                $upeTO->setDt_cadastro(date('Y-m-d'));
                $upeTO->setDt_inicio(date('Y-m-d'));
                $upeTO->setId_entidadeidentificacao($upeTO->getSessao()->id_entidade);
                $uperTO->setId_perfil($upeTO->getId_perfil());
            } else {
                if ($perfilProfessor[0]['id_usuario'] != $uperTO->getId_usuario()) {
                    $this->mensageiro->setCodigo('O id_usuario é diferente do id_usuario do Perfil!');
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    THROW new Zend_Exception('Erro ao Verificar o Perfil de Professor!');
                } else {
                    $uperTO->setId_perfil($perfilProfessor[0]['id_perfil']);
                }
            }
            $newUperTO = new UsuarioPerfilEntidadeReferenciaTO();
            $newUperTO->setId_entidade($uperTO->getId_entidade());
            $newUperTO->setId_saladeaula($uperTO->getId_saladeaula());
            $newUperTO->setId_usuario($uperTO->getId_usuario());
            $newUperTO->setId_perfil($uperTO->getId_perfil());
            $referencia = $this->dao->retornarUsuarioPerfilEntidadeReferenciaSala($newUperTO);
            $referencia = $referencia->toArray();
            if (empty($referencia)) {
                $id_usuarioPerfilEntidadeReferencia = $this->dao->cadastrarUsuarioPerfilEntidadeReferenciaSala($uperTO);
                $uperTO->setId_perfilReferencia($id_usuarioPerfilEntidadeReferencia);
                $sdapTO->setId_perfilReferencia($uperTO->getId_perfilReferencia());
            } else {
                $id_usuarioPerfilEntidadeReferencia = $referencia[0]['id_perfilreferencia'];
                $uperTO->setId_perfilReferencia($id_usuarioPerfilEntidadeReferencia);
                $sdapTO->setId_perfilReferencia($uperTO->getId_perfilReferencia());
                $newUperTO = new UsuarioPerfilEntidadeReferenciaTO();
                $newUperTO->setBl_ativo(true);
                $newUperTO->setId_entidade($uperTO->getId_entidade());
                $newUperTO->setId_saladeaula($uperTO->getId_saladeaula());
                $newUperTO->setId_perfil($uperTO->getId_perfil());
                $newUperTO->setId_usuario($uperTO->getId_usuario());
                $newUperTO->setBl_titular($uperTO->getBl_titular());
                $this->dao->editarUsuarioPerfilEntidadeReferencia($newUperTO);
            }

            $this->dao->commit();
            $orm = new SalaDeAulaIntegracaoORM();
            $salaDeAulaIntegracaoTO = $orm->consulta(new SalaDeAulaIntegracaoTO(array('id_saladeaula' => $uperTO->getId_saladeaula())), false, true);
            if ($salaDeAulaIntegracaoTO) {
                switch ($salaDeAulaIntegracaoTO->getId_sistema()) {
                    case SistemaTO::ACTOR:
                        $this->_processoIntegrarProfessorActor($uperTO, $sdapTO);
                        break;
                    case SistemaTO::MOODLE:
                        $this->_processoIntegrarProfessorMoodle($uperTO->getId_usuario(), $salaDeAulaIntegracaoTO->getId_saladeaula());
                        break;
                    case SistemaTO::BLACKBOARD:
                        $this->_processoIntegrarProfessorBlackBoard($uperTO, $salaDeAulaIntegracaoTO);
                        break;
                    default:
                        break;
                }
            }

            return $this->mensageiro->setMensageiro((isset($shlTO) ? $shlTO : "Operação executada com Sucesso"), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Vincula o Horario e Local à Sala de Aulax
     * @param SalaHorarioLocalTO $shlTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarSalaHorarioLocal(SalaHorarioLocalTO $shlTO, UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaHorarioLocalTO $oldSalaHorarioLocalTO = null)
    {

        if (!$shlTO->getId_saladeaula()) {
            return $this->mensageiro->setMensageiro('Erro: Id da Sala Inválido!', Ead1_IMensageiro::AVISO, 'O id_saladeaula não veio setado!');
        }

        $shlTO->setId_usuariocadastro($shlTO->getSessao()->id_usuario);

        if (!$shlTO->getId_usuariocadastro()) {
            return $this->mensageiro->setMensageiro('Erro: Usuário Inválido', Ead1_IMensageiro::AVISO, 'O id_usuariocadastro não veio setado!');
        }

        if (is_null($shlTO->getId_horarioaula())) {
            return $this->mensageiro->setMensageiro('Erro: id do horário inválido!', Ead1_IMensageiro::AVISO, 'O id_horarioaula não veio setado!');
        }

        if (is_null($shlTO->getId_localaula())) {
            return $this->mensageiro->setMensageiro('Erro: id do local inválido!', Ead1_IMensageiro::AVISO, 'O id_horarioaula não veio setado!');
        }

        if ($this->dao->verificaConflitoHorario($shlTO, $uperTO)) {
            return $this->mensageiro->setMensageiro('Erro: conflito no horário do professor', Ead1_IMensageiro::AVISO, 'Existe um conflito nos horários da sala e do professor!');
        }

        try {
            if ($oldSalaHorarioLocalTO != null) {
                if (!$this->dao->deletarSalaHorarioLocal($oldSalaHorarioLocalTO)) {
                    throw new Zend_Exception('Erro ao atualizar o vínculo da sala de aula com o horário!');
                }
            }

            if (!$this->dao->cadastrarSalaHorarioLocal($shlTO, $uperTO)) {
                throw new Zend_Exception('Erro ao vincular esta Sala de Aula a esse Horario nesse Local');
            }
            return $this->mensageiro->setMensageiro('Vínculo da sala com horário e local gravado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao vincular esta Sala de Aula a esse Horario nesse Local', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Cadastra as Aulas da Sala de Aula
     * @param AulaSalaDeAulaTO $asdaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO)
    {
        $id_aulaSalaDeAula = $this->dao->cadastrarAulaSalaDeAula($asdaTO);
        if (!$id_aulaSalaDeAula) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Aula da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $asdaTO->setId_aulasaladeaula($id_aulaSalaDeAula);
        return $this->mensageiro->setMensageiro($asdaTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que vincula Sala de Aula com a Area de Conhecimento ou Nivel de Ensino ou Projeto Pedagogico
     * @param AreaProjetoSalaTO $apsTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        $contador = 0;
        if ($apsTO->getId_areaconhecimento()) {
            $contador++;
        }
        if ($apsTO->getId_saladeaula()) {
            $contador++;
        }
        if ($apsTO->getId_nivelensino()) {
            $contador++;
        }
        if ($apsTO->getId_projetopedagogico()) {
            $contador++;
        }
        if ($contador > 1) {
            return $this->mensageiro->setMensageiro('Só pode Ser Vinculado 1 Tipo por Vez', Ead1_IMensageiro::AVISO);
        }
        $id_areaprojetosala = $this->dao->cadastrarAreaProjetoSala($apsTO);
        if (!$id_areaprojetosala) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $apsTO->setId_areaprojetosala($id_areaprojetosala);
        return $this->mensageiro->setMensageiro($apsTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Vincula a Referencia do Perfil do Usuario a *Sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuarioPerfilEntidadeReferenciaSala(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        if (!$uperTO->getId_entidade()) {
            $uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
        }
        $id_usuarioPerfilEntidadeReferencia = $this->dao->cadastrarUsuarioPerfilEntidadeReferenciaSala($uperTO);
        if (!$id_usuarioPerfilEntidadeReferencia) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Referencia do Perfil a Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $uperTO->setId_perfilreferencia($id_usuarioPerfilEntidadeReferencia);
        return $this->mensageiro->setMensageiro($uperTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra a Alocação do Aluno
     * @param AlocacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAlocacao(AlocacaoTO $aTO)
    {
        try {
            $aTO->setId_usuariocadastro($aTO->getSessao()->id_usuario);
            $id_alocacao = $this->dao->cadastrarAlocacao($aTO);
            $aTO->setId_alocacao($id_alocacao);
            return $this->mensageiro->setMensageiro($aTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Alocação', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que faz o Procedimento de Cadastrar um Perfil de Professor ao Usuario, caso não tenha, e depois vincular-lo a uma sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$uperTO->getId_saladeaula()) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O id_saladeaula não veio setado!');
        }
        if (!$uperTO->getId_usuario()) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O id_usuario não veio setado!');
        }
        if (is_null($sdapTO->getBl_titular())) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Sala de Aula!', Ead1_IMensageiro::AVISO, 'O bl_titular não veio setado!');
        }
        $this->dao->beginTransaction();
        try {
            $pDAO = new PerfilDAO();
            $pTO = new PerfilTO();
            $pTO->setId_entidade($uperTO->getId_entidade());
            $pTO->setId_perfilpedagogico(1);
            $perfis = $pDAO->retornaPerfil($pTO);
            if (!is_object($perfis)) {
                $this->mensageiro->setCodigo($this->dao->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                throw new Exception('Erro ao Retornar Perfis do Sistema!');
            }
            $perfis = $perfis->toArray();
            if (empty($perfis)) {
                $this->mensageiro->setCodigo($this->dao->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                throw new Exception('Esta Instituição Não Possui Perfil Pedagógico de Professor!');
            }
            $id_perfil = $perfis[0]['id_perfil'];
            if (!$id_perfil) {
                $this->mensageiro->setCodigo('Não retornou o id_perfil');
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                throw new Exception('Esta Instituição Não Possui Perfil Pedagógico de Professor!');
            }
            $upeTO = new UsuarioPerfilEntidadeTO();
            $upeTO->setId_entidade($uperTO->getId_entidade());
            $upeTO->setId_usuario($uperTO->getId_usuario());
            $upeTO->setId_perfil($id_perfil);
            $upeTO->setBl_ativo(true);
            $perfilProfessor = $pDAO->retornaUsuarioPerfilEntidade($upeTO);
            if (!is_object($perfilProfessor)) {
                $this->mensageiro->setCodigo($pDAO->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                throw new Exception('Erro ao Retornar Perfil do Usuário!');
            }
            $perfilProfessor = $perfilProfessor->toArray();
            if (empty($perfilProfessor)) {
                //@todo ajustar a Situacao de acordo com a tabela
                $upeTO->setId_situacao(SalaDeAulaBO::ID_USUARIO_PERFIL_ENTIDADE);
                $upeTO->setDt_cadastro(date('Y-m-d'));
                $upeTO->setDt_inicio(date('Y-m-d'));
                $upeTO->setId_entidadeidentificacao($upeTO->getSessao()->id_entidade);
                //--
                if (!$pDAO->cadastrarUsuarioPerfilEntidade($upeTO)) {
                    $this->mensageiro->setCodigo($pDAO->excecao);
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    throw new Exception('Erro ao Atribuir Perfil de Professor!');
                }
                $uperTO->setId_perfil($upeTO->getId_perfil());
            } else {
                if ($perfilProfessor[0]['id_usuario'] != $uperTO->getId_usuario()) {
                    $this->mensageiro->setCodigo('O id_usuario é diferente do id_usuario do Perfil!');
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    throw new Exception('Erro ao Verificar o Perfil de Professor!');
                } else {
                    $uperTO->setId_perfil($perfilProfessor[0]['id_perfil']);
                }
            }
            $referencia = $this->dao->retornarUsuarioPerfilEntidadeReferenciaSala($uperTO);
            //--
            if (!is_object($referencia)) {
                $this->mensageiro->setCodigo($this->dao->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                throw new Exception('Erro ao Retornar a Referencia dos Perfis!');
            }
            $referencia = $referencia->toArray();
            $id_usuarioPerfilEntidadeReferencia = $this->dao->cadastrarUsuarioPerfilEntidadeReferenciaSala($uperTO);
            if (empty($referencia)) {
                if (!$id_usuarioPerfilEntidadeReferencia) {
                    $this->mensageiro->setCodigo($this->dao->excecao);
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    throw new Exception('Erro ao Vincular a Referencia do Perfil do Usuário!');
                }
                $uperTO->setId_usuarioperfilentidadereferencia($id_usuarioPerfilEntidadeReferencia);
                $sdapTO->setId_usuarioperfilentidadereferencia($uperTO->getId_usuarioperfilentidadereferencia());
            } else {
                $id_perfilReferencia = $referencia[0]['id_perfilreferencia'];
                if (!$id_usuarioPerfilEntidadeReferencia) {
                    $this->mensageiro->setCodigo('Não Retornou o id_perfilreferencia');
                    $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                    throw new Exception('Erro ao Retornar Vinculo da Referencia do Perfil!');
                }
                $uperTO->setId_perfilReferencia($id_perfilReferencia);
                $sdapTO->setId_perfilReferencia($uperTO->getId_perfilReferencia());
            }
            $vwrsdapTO = new VwReferenciaSalaDeAulaProfessorTO();
            $vwrsdapTO->setId_entidade($uperTO->getId_entidade());
            $vwrsdapTO->setId_perfil($uperTO->getId_perfil());
            $vwrsdapTO->setId_saladeaula($uperTO->getId_saladeaula());
            $vwrsdapTO->setBl_titular($sdapTO->getBl_titular());
            if (Ead1_TO::is_boolean($sdapTO->getBl_titular())) {
                $referenciaProfessor = $this->dao->retornarReferenciaSalaDeAulaProfessor($vwrsdapTO);
                if (!is_object($referenciaProfessor)) {
                    $this->mensageiro->setCodigo($this->dao->excecao);
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    throw new Exception('Erro ao Retornar a Referencia da Sala de Aula dos Professores!');
                }
                $referenciaProfessor = $referenciaProfessor->toArray();
                if (!empty($referenciaProfessor)) {
                    if (Ead1_TO::is_boolean($uperTO->getBl_ativo())) {
                        $this->mensageiro->setCodigo($this->dao->excecao);
                        $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                        throw new Exception('Só pode Haver um Professor Titular por Sala de Aula!');
                    }
                }
            }
            $vwrsdapTO->setId_usuario($uperTO->getId_usuario());
            $vwrsdapTO->setId_usuarioperfilentidadereferencia($uperTO->getId_usuarioperfilentidadereferencia());
            $referenciaProfessor = $this->dao->retornarReferenciaSalaDeAulaProfessor($vwrsdapTO);
            if (!is_object($referenciaProfessor)) {
                $this->mensageiro->setCodigo($this->dao->excecao);
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                throw new Exception('Erro ao Retornar a Referencia da Sala de Aula do Professor!');
            }
            $referenciaProfessor = $referenciaProfessor->toArray();
            if (!empty($referenciaProfessor)) {
                $isprof = '';
                foreach ($referenciaProfessor as $professor) {
                    if ($professor['id_usuario'] == $uperTO->getId_usuario()) {
                        $isprof = 1;
                    }
                }
            } else {
                $id_salaDeAulaProfessor = $this->dao->cadastrarSalaDeAulaProfessor($sdapTO);
                if (!$id_salaDeAulaProfessor) {
                    $this->mensageiro->setCodigo($this->dao->excecao);
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    throw new Exception('Erro ao Cadastrar o Professor na Sala de Aula!');
                }
                $sdapTO->setId_saladeaulaprofessor($id_salaDeAulaProfessor);
            }

            if ($isprof) {
                $this->dao->commit();
                return $this->procedimentoEditarSalaDeAulaProfessor($sdapTO);
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($sdapTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Edita a Sala de Aula do Professor Para o Procedimento SalvarSalaDeAulaProfessor
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoEditarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$this->dao->editarSalaDeAulaProfessor($sdapTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Sala de Aula do Professor!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Sala de Aula do Professor Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function editarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to)
    {
        try {
            $this->dao->editarSalaDeAulaEntidade($to);
            $this->mensageiro->setMensageiro('Vinculo da Instituição com a Sala de Aula Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Vinculo da Instituição com a Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * MEtodo que Edita a Aula da Sala de Aula
     * @param AulaSalaDeAulaTO $asdaTO
     * @return Ead1_Mensageiro
     */
    public function editarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO)
    {
        if (!$this->dao->editarAulaSalaDeAula($asdaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Aula da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Aula da Sala de Aula Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita o bl_ativa do Vinculo da Referencia do Perfil do Usuario a *Sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return Ead1_Mensageiro
     */
    public function editarUsuarioPerfilEntidadeReferenciaSala(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        if (!$uperTO->getId_entidade()) {
            $uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
        }
        if (!$this->dao->editarUsuarioPerfilEntidadeReferenciaSala($uperTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Executar Operação!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Operação Executada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita a Alocação do Aluno
     * @param AlocacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function editarAlocacao(AlocacaoTO $aTO)
    {
        try {
            $this->dao->editarAlocacao($aTO);
            return $this->mensageiro->setMensageiro('Alocação Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Alocação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita a Sala de Aula do Professor
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function editarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
//		if(!$uperTO->getId_entidade()){
//			$uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
//		}
//		if(Ead1_TO::is_boolean($sdapTO->getBl_titular())){
//			$vwrsdapTO = new VwReferenciaSalaDeAulaProfessorTO();
//			$vwrsdapTO->setId_entidade($uperTO->getId_entidade());
//			$vwrsdapTO->setId_saladeaula($uperTO->getId_saladeaula());
//			$vwrsdapTO->setBl_titular($sdapTO->getBl_titular());
//			$referenciaProfessor = $this->dao->retornarReferenciaSalaDeAulaProfessor($vwrsdapTO);
//			if(!is_object($referenciaProfessor)){
//				return $this->mensageiro->setMensageiro('Erro ao Retornar a Referencia da Sala de Aula do Professor!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
//			}
//			$referenciaProfessor = $referenciaProfessor->toArray();
//			if(!empty($referenciaProfessor)){
//				if($referenciaProfessor[0]['id_usuarioperfilentidadereferencia'] != $sdapTO->getId_usuarioperfilentidadereferencia()){
//					return $this->mensageiro->setMensageiro('Só pode Haver um Professor Titular por Sala de Aula!',Ead1_IMensageiro::AVISO);
//				}
//			}
//		}
        unset($sdapTO->bl_titular);
        if (!$this->dao->editarSalaDeAulaProfessor($sdapTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Sala de Aula do Professor!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Sala de Aula do Professor Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui o Professor da Sala de Aula
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function deletarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$this->dao->deletarSalaDeAulaProfessor($sdapTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Professor da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que exclui a entidade da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function deletarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to)
    {
        try {
            $this->dao->deletarSalaDeAulaEntidade($to);
            $this->mensageiro->setMensageiro('Vinculo da Instituição com a Sala de Aula Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo da Instituição com a Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Exclui vinculo da Sala de Aula com a Area de Conhecimento ou Projeto Pedagogico ou Nivel de Ensino
     * @param AreaProjetoSalaTO $apsTO
     * @return Ead1_Mensageiro
     */
    public function deletarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        if (!$this->dao->deletarAreaProjetoSala($apsTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Vinculo Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Remove uma sala de aula
     * Altera o bl_ativo para 0
     * @todo verificar regras de negócio para exclusão de sala de aula
     * @param SalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro
     */
    public function deletarSalaDeAula(SalaDeAulaTO $salaAulaTO)
    {


        return $this->mensageiro->setMensageiro('Não é possível excluir sala de aula. Favor entrar em contato com o suporte.', Ead1_IMensageiro::AVISO);
    }

    /**
     * Metodo que Exclui o Vinculo da Disciplina com a Sala de Aula
     * @param DisciplinaSalaDeAulaTO $dsdaTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO)
    {
        if (!$this->dao->deletarDisciplinaSalaDeAula($dsdaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo da Sala de Aula com a Disciplina!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Vinculo Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Exclúi a integração da Sala de Aula
     * @param int $id_saladeaulaintegracao
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function deletarSalaDeAulaIntegracao($id_saladeaulaintegracao)
    {
        if (!$this->dao->deletarSalaDeAulaIntegracao($id_saladeaulaintegracao)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo da Sala de Aula com a Integração!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Vinculo Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Inativa o Professor do vinculo da Sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return Ead1_Mensageiro
     */
    public function deletarProfessorSalaDeAula(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        //o
        if (!$uperTO->getId_entidade()) {
            $uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
        }
        /**
         * Atualizando o STATUS do PAPEL no WEBSERVICE
         */
        try {
            $PerfilReferenciaIntegracaoTO = new PerfilReferenciaIntegracaoTO();
            $PerfilReferenciaIntegracaoTO->setId_perfilreferencia($uperTO->getId_perfilReferencia());
            $PerfilReferenciaIntegracaoTO->fetch(false, true, true);
            if ($PerfilReferenciaIntegracaoTO->id_perfilreferenciaintegracao) {
                switch ($PerfilReferenciaIntegracaoTO->getId_sistema()) {
                    case SistemaTO::ACTOR:
                        $papelWebServices = new PapelWebServices();
                        $retornoWS = $papelWebServices->atualizaPapel(array(
                            'codpapel' => $PerfilReferenciaIntegracaoTO->st_codsistema
                        , 'flastatus' => 'I'
                        , 'flamaster' => 'N'
                        ));
                        break;
                }
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Professor da Sala de Aula! ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        $uperTO->setBl_ativo(false);
        if (!$this->dao->editarUsuarioPerfilEntidadeReferencia($uperTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Professor da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Professor Excluido da Sala de Aula com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que remove o vinculo entre um horario e uma sala de aula
     * @param SalaHorarioLocalTO $shlTO
     * @return Ead1_Mensageiro
     */
    public function deletarSalaHorarioLocal(SalaHorarioLocalTO $shlTO)
    {

        try {

            if (!$this->dao->deletarSalaHorarioLocal($shlTO)) {
                return $this->mensageiro->setMensageiro('Erro ao excluir o horário da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao excluir Professor da Sala de Aula! ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro->setMensageiro('Horario removido da Sala de  Aula com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna Sala de Aula
     * @param SalaDeAulaTO $sdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAula(SalaDeAulaTO $sdaTO)
    {
        if (!$sdaTO->getId_entidade()) {
            $sdaTO->setId_entidade($sdaTO->getSessao()->id_entidade);
        }
        $sala = $this->dao->retornarSalaDeAula($sdaTO);
        if (is_object($sala)) {
            $sala = $sala->toArray();
        } else {
            unset($sala);
        }
        if (!$sala) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Sala Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($sala, new SalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna o Professor da Sala de Aula
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$sdapTO->getId_entidade()) {
            $sdapTO->setId_entidade($sdapTO->getSessao()->id_entidade);
        }
        $professor = $this->dao->retornarSalaDeAulaProfessor($sdapTO);
        if (is_object($professor)) {
            $professor = $professor->toArray();
        } else {
            unset($professor);
        }
        if (!$professor) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($professor, new SalaDeAulaProfessorTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna os horários da Sala de Aula
     * @param SalaHorarioLocalTO $shlTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaHorarioLocal(VwSalaHorarioLocalTO $shlTO)
    {

        $horarios = $this->dao->retornarVwSalaHorarioLocal($shlTO);

        if (is_object($horarios)) {
            $horarios = $horarios->toArray();
        } else {
            unset($horarios);
        }
        if (!$horarios) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($horarios, new VwSalaHorarioLocalTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna o Professor da Sala de Aula e a referencia
     * @param SalaDeAulaProfessorTO $sdapTO
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAulaProfessorReferencia(SalaDeAulaProfessorTO $sdapTO)
    {
        if (!$sdapTO->getId_entidade()) {
            $sdapTO->setId_entidade($sdapTO->getSessao()->id_entidade);
        }
        $professor = $this->dao->retornarSalaDeAulaProfessor($sdapTO);
        if (is_object($professor)) {
            $professor = $professor->toArray();
        } else {
            unset($professor);
        }
        if (!$professor) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        $arrProf = array();
        foreach ($professor as $chave => $prof) {
            $arrProf[$prof['id_perfilreferencia']] = $prof;
        }
        $uper = new UsuarioPerfilEntidadeReferenciaTO();
        $uper->setId_saladeaula($sdapTO->getId_saladeaula());
        $referencia = $this->dao->retornarUsuarioPerfilEntidadeReferenciaSala($uper);
        if (is_object($referencia)) {
            $referencia = $referencia->toArray();
        } else {
            unset($referencia);
        }
        if (!$referencia) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        $arrReferencia = array();
        foreach ($referencia as $chave => $ref) {
            $arrReferencia[$ref['id_perfilreferencia']] = $ref;
        }

//		$this->mensageiro->addMensagem(Ead1_TO_Dinamico::encapsularTo($professor, new SalaDeAulaProfessorTO()));
    }

    /**
     * Metodo que Retorna Vinculos da Sala de Aula
     * @param AreaProjetoSalaTO $apsTO
     * @return Ead1_Mensageiro
     */
    public function retornarAreaProjetoSala(AreaProjetoSalaTO $apsTO)
    {
        $vapsTO = new VwAreaProjetoSalaTO();
        $vapsTO->setId_areaconhecimento($apsTO->getId_areaconhecimento());
        $vapsTO->setId_saladeaula($apsTO->getId_saladeaula());
        $vapsTO->setId_nivelensino($apsTO->getId_nivelensino());
        $vapsTO->setId_projetopedagogico($apsTO->getId_projetopedagogico());
        $vinculo = $this->dao->retornarVwAreaProjetoSala($vapsTO);
        if (is_object($vinculo)) {
            $vinculo = $vinculo->toArray();
        } else {
            unset($vinculo);
        }
        if (!$vinculo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($vinculo, new VwProjetoAreaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna as Modalidades da Sala de Aula
     * @param ModalidadeSalaDeAulaTO $msdaTO
     * @param boolean $pesquisa
     * @return Ead1_Mensageiro
     */
    public function retornarModalidadeSalaDeAula(ModalidadeSalaDeAulaTO $msdaTO, $pesquisa = false)
    {
        $modalidade = $this->dao->retornarModalidadeSalaDeAula($msdaTO);
        if (!$modalidade) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($modalidade, 'st_modalidadesaladeaula', 'id_modalidadesaladeaula');
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($modalidade->toArray(), new ModalidadeSalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna a Disciplina da Sala de Aula
     * @param DisciplinaSalaDeAulaTO $dsdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO)
    {
        $disciplinaSala = $this->dao->retornarDisciplinaSalaDeAula($dsdaTO);
        if (is_object($disciplinaSala)) {
            $disciplinaSala = $disciplinaSala->toArray();
        } else {
            unset($disciplinaSala);
        }
        if (!$disciplinaSala) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplinaSala, new DisciplinaSalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna o Tipo da Aula
     * @param TipoAulaTO $taTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAula(TipoAulaTO $taTO)
    {
        $tipo = $this->dao->retornarTipoAula($taTO);
        if (is_object($tipo)) {
            $tipo = $tipo->toArray();
        } else {
            unset($tipo);
        }
        if (!$tipo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo, new TipoAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna a Aula da Sala de Aula
     * @param AulaSalaDeAulaTO $asdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO)
    {
        $aula = $this->dao->retornarAulaSalaDeAula($asdaTO);
        if (is_object($aula)) {
            $aula = $aula->toArray();
        } else {
            unset($aula);
        }
        if (!$aula) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($aula, new AulaSalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que Retorna o Período Letivo.
     * @param PeriodoLetivo $plTO
     * @param boolean $pesquisa
     * @return Ead1_Mensageiro
     */
    public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO, $pesquisa = false)
    {
        $periodo = $this->dao->retornarPeriodoLetivo($plTO);
        if (!$periodo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($periodo, 'st_periodoletivo', 'id_periodoletivo');
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($periodo->toArray(), new PeriodoLetivoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna o Tipo da Sala de Aula
     * @param TipoSalaDeAulaTO $tsdaTO
     * @param boolean $pesquisa
     * @return Ead1_Mensageiro
     */
    public function retornarTipoSalaDeAula(TipoSalaDeAulaTO $tsdaTO, $pesquisa = false)
    {
        $tipo = $this->dao->retornarTipoSalaDeAula($tsdaTO);
        if (!$tipo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($tipo, 'st_tiposaladeaula', 'id_tiposaladeaula');
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo->toArray(), new TipoSalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna o Tipo de Avaliação da Sala de Aula
     * @param TipoAvaliacaoSalaDeAulaTO $tasdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAvaliacaoSalaDeAula(TipoAvaliacaoSalaDeAulaTO $tasdaTO)
    {
        $tipo = $this->dao->retornarTipoAvaliacaoSalaDeAula($tasdaTO);
        if (is_object($tipo)) {
            $tipo = $tipo->toArray();
        } else {
            unset($tipo);
        }
        if (!$tipo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo, new TipoAvaliacaoSalaDeAulaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Alocação do Aluno
     * @param AlocacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarAlocacao(AlocacaoTO $aTO)
    {
        try {
            $arrAlocacao = $this->dao->retornarAlocacao($aTO)->toArray();
            if (empty($arrAlocacao)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrAlocacao, new AlocacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Alocação', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna configuração de uma ou mais salas de aula.
     * @param VwSalaConfiguracaoTO $vwSalaConfiguracaoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaConfiguracao(VwSalaConfiguracaoTO $vwSalaConfiguracaoTO)
    {
        try {
            $arrSalaConfiguracao = $this->dao->retornaVwSalaConfiguracao($vwSalaConfiguracaoTO)->toArray();
            if (empty($arrSalaConfiguracao)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Configuração de Sala Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrSalaConfiguracao, new VwSalaConfiguracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Configuração da Sala', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna dados detalhadados de uma sala de aula a encerrar.
     * @param int $id_saladeaula
     * @return Ead1_Mensageiro
     */
    public function retornarDadosDetalhamentoSala($id_saladeaula)
    {
        try {
            $vwSalaConfiguracaoTO = new VwSalaConfiguracaoTO(array('id_saladeaula' => $id_saladeaula));
            $dadosConfiguracao = $this->dao->retornaVwSalaConfiguracao($vwSalaConfiguracaoTO)->toArray();
            $vw_salaconfiguracao = Ead1_TO_Dinamico::encapsularTo($dadosConfiguracao, new VwSalaConfiguracaoTO());
            $dados['vw_salaconfiguracao'] = $vw_salaconfiguracao[0];

            $vwEncerramentoAlunosTO = new VwEncerramentoAlunosTO(array('id_saladeaula' => $id_saladeaula));
            $dadosAlunos = $this->dao->retornaAlunosEncerramento($vwEncerramentoAlunosTO)->toArray();
            $dados['vw_encerramentoalunos'] = Ead1_TO_Dinamico::encapsularTo($dadosAlunos, new VwEncerramentoAlunosTO());

            $arReturn = $dados['vw_encerramentoalunos'];

            foreach ($arReturn as $k => $vw) {
                if ($vw->getDt_encerramentocoordenador()) {
                    $vw->setDt_encerramentocoordenador($vw->getDt_encerramentocoordenador()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentocoordenador('');
                }

                if ($vw->getDt_encerramentofinanceiro() != null) {
                    $vw->setDt_encerramentofinanceiro($vw->getDt_encerramentofinanceiro()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentofinanceiro('');
                }

                if ($vw->getDt_encerramentopedagogico()) {
                    $vw->setDt_encerramentopedagogico($vw->getDt_encerramentopedagogico()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentopedagogico('');
                }

                if ($vw->getDt_encerramentoprofessor()) {
                    $vw->setDt_encerramentoprofessor($vw->getDt_encerramentoProfessor()->toString('dd/MM/Y'));
                } else {
                    $vw->setDt_encerramentoprofessor('');
                }
                $arReturn[$k] = $vw;
            }

            $dados['vw_encerramentoalunos'] = $arReturn;

            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados de Detalhamento da Sala', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Retorna o Vinculo da Referencia do Perfil do Usuario a *Sala de Aula
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @return Ead1_Mensageiro
     */
    public function retornarUsuarioPerfilEntidadeReferenciaSala(UsuarioPerfilEntidadeReferenciaTO $uperTO)
    {
        if (!$uperTO->getId_entidade()) {
            $uperTO->setId_entidade($uperTO->getSessao()->id_entidade);
        }
        $referencia = $this->dao->retornarUsuarioPerfilEntidadeReferenciaSala($uperTO);
        if (is_object($referencia)) {
            $referencia = $referencia->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Referencia do Perfil do Usuário!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($referencia)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($referencia, new UsuarioPerfilEntidadeReferenciaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o professor de uma sala de aula
     * @param VwProfessorSalaDeAulaTO $vwPsdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarProfessorSalaDeAula(VwProfessorSalaDeAulaTO $vwPsdaTO)
    {
        if (!$vwPsdaTO->getId_entidade()) {
            $vwPsdaTO->setId_entidade($vwPsdaTO->getSessao()->id_entidade);
        }
        $professor = $this->dao->retornarProfessorSalaDeAula($vwPsdaTO);
        if (is_object($professor)) {
            $professor = $professor->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Professores da Sala de Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($professor)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($professor, new VwProfessorSalaDeAulaTO()));
    }

    /**
     * Metodo que retorna as Salas de Aula disponiveis ao aluno
     * @param VwSalaDisciplinaProjetoTO $vwSdpTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaDisciplinaProjeto(VwSalaDisciplinaProjetoTO $vwSdpTO)
    {
        try {
            $salas = $this->dao->retornarVwSalaDisciplinaProjeto($vwSdpTO)->toArray();
            if (empty($salas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($salas, new VwSalaDisciplinaProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Sala de Aula das Disciplinas', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as Salas de Aula disponiveis ao aluno
     * @param VwSalaDisciplinaAlocacaoTO $vwSdaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaDisciplinaAlocacao(VwSalaDisciplinaAlocacaoTO $vwSdaTO)
    {
        try {
            $salas = $this->dao->retornarVwSalaDisciplinaAlocacao($vwSdaTO);
            if (empty($salas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Registro para Alocação Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($salas, new VwSalaDisciplinaAlocacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Sala de Aula das Disciplinas', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as Instituições da sala de aula
     * @param SalaDeAulaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to)
    {
        try {
            $dados = $this->dao->retornarSalaDeAulaEntidade($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new SalaDeAulaEntidadeTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Instituições da Sala de Aula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que seta o professor titular da sala de aula professor
     * @param VwProfessorSalaDeAulaTO $vwsdapTO
     * @return Ead1_Mensageiro
     */
    public function setarProfessorTitularSalaDeAula(VwProfessorSalaDeAulaTO $vwsdapTO)
    {
        if (!$vwsdapTO->getId_entidade()) {
            $vwsdapTO->setId_entidade($vwsdapTO->getSessao());
        }
        if (!$vwsdapTO->getId_saladeaula() || !$vwsdapTO->getId_saladeaulaprofessor()) {
            return $this->mensageiro->setMensageiro('Erro ao Setar Professor Titular!', Ead1_IMensageiro::ERRO, ' O id da Sala de Aula ou o id da Sala de Aula Professor não vieram setados!');
        }
        try {
            $newVwPsdaTO = new VwProfessorSalaDeAulaTO();
            $newVwPsdaTO->setId_entidade($vwsdapTO->getId_entidade());
            $newVwPsdaTO->setId_saladeaula($vwsdapTO->getId_saladeaula());
            $professores = $this->dao->retornarProfessorSalaDeAula($newVwPsdaTO);
            $professores = $professores->toArray();
            if (empty($professores)) {
                THROW new Zend_Exception('Nenhum Professor Encontrado!');
            }
            foreach ($professores as $chave => $professor) {
                if ($professor['id_saladeaulaprofessor'] != $vwsdapTO->getId_saladeaulaprofessor()) {
                    $sdapTO = new SalaDeAulaProfessorTO();
                    $sdapTO->setId_saladeaulaprofessor($professor['id_saladeaulaprofessor']);
                    $sdapTO->setBl_titular(false);
                    $sdapTO->setId_perfilReferencia($professor['id_perfilreferencia']);
                    $this->dao->editarSalaDeAulaProfessor($sdapTO);
                } else {
                    $sdapTO = new SalaDeAulaProfessorTO();
                    $sdapTO->setId_saladeaulaprofessor($vwsdapTO->getId_saladeaulaprofessor());
                    $sdapTO->setBl_titular(true);
                    $sdapTO->setId_perfilReferencia($professor['id_perfilreferencia']);
                    $this->dao->editarSalaDeAulaProfessor($sdapTO);
                }
            }
            return $this->mensageiro->setMensageiro('Professor Titular Setado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Setar Professor Titular da Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Chama WebService para várias Salas de Aula
     * @param $arrSalas
     * @param $idSistema
     */
    public function wsSalasDeAula(array $arrSalas, $idSistema, $array_integracao = false)
    {
        try {
            if (!empty($arrSalas)) {
                foreach ($arrSalas as $salaDeAulaTO) {
                    $this->wsSalaDeAula($salaDeAulaTO, $idSistema, $array_integracao ? $array_integracao[$salaDeAulaTO->getId_saladeaula()] : false)->subtractMensageiro();
                }
            }
            return $this->mensageiro->setMensageiro('Salas Integradas com Sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Integrar Salas.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Método que chama o WebService para a sala de aula
     * @param SalaDeAulaTO $sdaTO
     * @param int $idSistema
     * @return Ead1_Mensageiro
     */
    public function wsSalaDeAula(SalaDeAulaTO $sdaTO, $idSistema, $id_disciplinaintegracao = false)
    {
        try {

            switch ($idSistema) {
                case SistemaTO::ACTOR;
                    $wsBo = new WsBO(Ead1_Ambiente::geral()->wsURL . 'services/saladeaula.php');
                    $chamadaWs = $wsBo->editarSalaDeAula($sdaTO);
                    break;
                case SistemaTO::MOODLE;
                    $vwSalaDeAulaTO = new VwSalaDeAulaTO();
                    $vwSalaDeAulaTO->setId_saladeaula($sdaTO->getId_saladeaula());

                    if (!$sdaTO->getId_entidadeintegracao()) {
                        throw  new Exception("Entidade integração não vinculada a sala de aula.");
                    }

                    $idEntidadeIntegracao = $sdaTO->getId_entidadeintegracao();

                    $ws = new MoodleCursosWebServices(null, $idEntidadeIntegracao);
                    $id_discint = false;

                    if ($id_disciplinaintegracao) {
                        $id_discint = array($sdaTO->getId_saladeaula() => $id_disciplinaintegracao);
                    }
                    $fetchVw = $vwSalaDeAulaTO->fetch();
                    if ($fetchVw) {
                        $chamadaWs = $ws->cadastrarCurso($fetchVw, $id_discint);
                    } else {
                        return new Ead1_Mensageiro("Sala de aula não encontrada", Ead1_IMensageiro::AVISO);
                    }
                    break;
                case SistemaTO::AMAIS;
                    $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
                    $salaDeAulaIntegracaoTO->setId_saladeaula($sdaTO->getId_saladeaula());
                    $salaDeAulaIntegracaoTO->setId_usuariocadastro($salaDeAulaIntegracaoTO->getSessao()->id_usuario);
                    $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::AMAIS);
                    $salaDeAulaIntegracaoTO->setSt_codsistemacurso(SalaDeAulaIntegracaoTO::SEMSALAEXTERNA);
                    return $this->cadastrarSaladeAulaIntegracao($salaDeAulaIntegracaoTO);
                    break;
                case SistemaTO::BLACKBOARD;


                    $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
                    $salaDeAulaIntegracaoTO->setId_saladeaula($sdaTO->getId_saladeaula());
                    $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::BLACKBOARD);
                    $salaDeAulaIntegracaoTO->fetch(false, true, true);
                    if ($salaDeAulaIntegracaoTO->getId_saladeaulaintegracao()) {
                        return new Ead1_Mensageiro("Essa sala já possui integração!", Ead1_IMensageiro::AVISO);
                    }

                    $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
                    $salaDeAulaIntegracaoTO->setId_saladeaula($sdaTO->getId_saladeaula());
                    $salaDeAulaIntegracaoTO->setId_usuariocadastro($salaDeAulaIntegracaoTO->getSessao()->id_usuario);
                    $salaDeAulaIntegracaoTO->setId_sistema(SistemaTO::BLACKBOARD);
                    $salaDeAulaIntegracaoTO->setSt_codsistemacurso(SalaDeAulaIntegracaoTO::SALABLACKBOARD);
                    $mensageiro = $this->cadastrarSaladeAulaIntegracao($salaDeAulaIntegracaoTO);
                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $mensageiro->setMensagem("Integração para o BlackBoard salva com sucesso! Aguarde até o Sistema liberar o Acesso.");
                    }
                    return $mensageiro;
                    break;
                default:
                    $chamadaWs = new Ead1_Mensageiro("Sistema não disponível!", Ead1_IMensageiro::ERRO);
            }


            if ($chamadaWs->getTipo() == Ead1_IMensageiro::SUCESSO) {
                return $chamadaWs;
            } else {
                return $this->mensageiro->setMensageiro($chamadaWs->getMensagem(), Ead1_IMensageiro::ERRO, $chamadaWs->getCodigo());
            }
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Cadastra uma Sala de Aula na Integração
     * @param SalaDeAulaIntegracaoTO $salaDeAulaIntegracaoTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarSaladeAulaIntegracao(SalaDeAulaIntegracaoTO $salaDeAulaIntegracaoTO)
    {

        $this->dao->beginTransaction();
        try {
            $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
            $salaDeAulaIntegracaoTO->setId_saladeaulaintegracao($salaDeAulaIntegracaoORM->insert($salaDeAulaIntegracaoTO->toArrayInsert()));
            $this->dao->commit();
            return new Ead1_Mensageiro($salaDeAulaIntegracaoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna integração da sala de aula
     * @see SalaDeAulaRO::retornarSalaDeAulaIntegracao();
     * @param SalaDeAulaIntegracaoTO $sdaiTO
     * @return Ead1_Mensageiro
     */
    public function retornarSalaDeAulaIntegracao(SalaDeAulaIntegracaoTO $sdaiTO)
    {
        try {
            $dao = new SalaDeAulaDAO();
            $dados = $dao->retornarSalaDeAulaIntegracao($sdaiTO)->toArray();
            if (!empty($dados)) {
                $mensageiro = new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new SalaDeAulaIntegracaoTO(), true));
            } else {
                $mensageiro = new Ead1_Mensageiro(new SalaDeAulaIntegracaoTO(), Ead1_IMensageiro::SUCESSO);
            }
            return $mensageiro;
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao retornar integração.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as salas de aulas com integração do professor
     * @param VwSalaDeAulaProfessorIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaDeAulaProfessorIntegracao(VwSalaDeAulaProfessorIntegracaoTO $to, $order = null)
    {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $dados = $this->dao->retornarVwSalaDeAulaProfessorIntegracao($to, null, $order)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhuma Sala de Aula Encontrada!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwSalaDeAulaProfessorIntegracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar as Salas de Aula!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as salas de aulas com integração do observador institucional e outros perfis.
     * @param VwSalaDeAulaPerfilProjetoIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwSalaDeAulaPerfilProjetoIntegracao(VwSalaDeAulaPerfilProjetoIntegracaoTO $to, $order = null, $count = null, $offset = null, $where = '')
    {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $dados = $this->dao->retornarVwSalaDeAulaPerfilProjetoIntegracao($to, $order, $count, $offset, $where)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhuma Sala de Aula Encontrada!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwSalaDeAulaPerfilProjetoIntegracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar as Salas de Aula!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Cadê a DOCUMENTAÇÃO DO MÉTODO?????????
     */
    public function retornarVwSalaDisciplinaProjetoAlocadas(VwSalaDisciplinaProjetoTO $vwSdpTO)
    {
        try {
            $salas = $this->dao->retornarVwSalaDisciplinaProjetoAlocadas($vwSdpTO)->toArray();
            if (empty($salas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($salas, new VwSalaDisciplinaProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Sala de Aula das Disciplinas', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna Projetos de uma Sala de aula
     * @param VwAreaProjetoSalaTO $vwAreaProjetoSala
     * @return Ead1_Mensageiro
     */
    public function retornarVwAreaProjetoSala(VwAreaProjetoSalaTO $vwAreaProjetoSala)
    {
        try {
            $salas = $this->dao->retornarVwAreaProjetoSala($vwAreaProjetoSala)->toArray();
            if (empty($salas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Projetos na Sala de Aula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($salas, new VwAreaProjetoSalaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Projetos na Sala de Aula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function reintegrarAlunos()
    {

        $erros = array();
        $sucessos = array();
        try {
            $dados = $this->dao->retornaVwReintegrarAlunos(new VwReintegrarAlunosTO());
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum aluno para ser sincronizado!");
            } else {
                $arrVwSincronizar = Ead1_TO_Dinamico::encapsularTo($dados, new VwSincronizarAlunosMoodleTO());
                $moodle = new MoodleAlocacaoWebServices();

                foreach ($arrVwSincronizar as $key => $to) {
                    $mensagem = $moodle->cadastrarAlocacaoIntegracao($to->getId_alocacao());
                    if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $sucessos[$key]['nome'] = $to->getSt_nomecompleto();
                        $sucessos[$key]['sala'] = $to->getSt_saladeaula();
                    } else {
                        $erros[$key]['nome'] = $to->getSt_nomecompleto();
                        $erros[$key]['sala'] = $to->getSt_saladeaula();
                        $erros[$key]['erro'] = $mensagem->getFirstMensagem();
                    }
                }
            }
            $arrMensagem['erros'] = $erros;
            $arrMensagem['sucessos'] = $sucessos;
            $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao sincronizar aluno no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * @description Método que executa alocação automática solicitada pelo Felipe com base na vw_alunosautoalocar
     * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
     * @return array|\Ead1_Mensageiro
     */
    public function sincronizarAutoAlocar()
    {
        try {
            $to = new MatriculaTO();
            $matriculas = $this->dao->retornaArrMatriculaVwAlunosAutoAlocar($to->getSessao()->id_entidade);
            $success = array();
            $error = array();

            if (is_array($matriculas)) {
                foreach ($matriculas as $matricula) {

                    $to = new MatriculaTO();
                    $to->setId_matricula($matricula['id_matricula']);
                    $to->fetch(true, true, true);

                    $ng_matricula = new \G2\Negocio\Matricula();
                    $mensageiro = $ng_matricula->alocarAutomatico($to, true, null);

                    // Salvar a data da tentativa de alocar o aluno
                    if (!empty($matricula['id_matriculadisciplina'])) {
                        /** @var \G2\Entity\MatriculaDisciplina $en_matriculadisciplina */
                        $en_matriculadisciplina = $ng_matricula->find('\G2\Entity\MatriculaDisciplina', $matricula['id_matriculadisciplina']);
                        $en_matriculadisciplina->setDt_tentativaalocacao(new DateTime());
                        $ng_matricula->save($en_matriculadisciplina);
                    }

                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $success[] = $matricula;
                    } else {
                        $matricula['message'] = $mensageiro->getText();
                        $error[] = $matricula;
                    }
                }
            }

            return array(
                'results' => $matriculas,
                'error' => $error,
                'success' => $success
            );
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que chama o ws de importacao de conteudos para as salas do moodle
     * @param SalaDeAulaTO $salaDeAula
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function sincronizarConteudoMoodle(SalaDeAulaTO $salaDeAula)
    {
        try {
            if (!$salaDeAula->getId_saladeaula()) {
                throw new Zend_Exception("O id_saladeaula é obrigatório e não foi passado!");
            }
            $moodle = new MoodleCursosWebServices();
            $resultado = $moodle->importarConteudoSalaDeAula($salaDeAula);

            if ($resultado->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $salaDeAulaIntegracao = new SalaDeAulaIntegracaoTO();
                $salaDeAulaIntegracao->setId_saladeaula($salaDeAula->getId_saladeaula());
                $salaDeAulaIntegracao->setId_sistema(SistemaTO::MOODLE);
                $salaDeAulaIntegracao->fetch(false, true, true);
                $salaDeAulaIntegracao->setBl_conteudo(true);

                $retorno = $this->editarSalaDeAulaIntegracao($salaDeAulaIntegracao);
            } else {
                throw new Zend_Exception($resultado->getFirstMensagem());
            }

            return new Ead1_Mensageiro('Conteúdo importado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $ze) {
            return new Ead1_Mensageiro($ze->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que edita a sala de aula integraçao
     * @param SalaDeAulaIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function editarSalaDeAulaIntegracao(SalaDeAulaIntegracaoTO $to)
    {
        try {
            $this->dao->editarSalaDeAulaIntegracao($to);
            $this->mensageiro->setMensageiro('SaladeAulaIntegracao Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar SaladeAulaIntegracao!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Executa auto alocação por aluno
     * @param MatriculaTO $matriculaTO
     * @return Ead1_Mensageiro
     *
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function sincronizarAutoAlocarPorAluno(MatriculaTO $matriculaTO)
    {

        try {

            $ng_matricula = new \G2\Negocio\Matricula();
            $mensageiro = $ng_matricula->alocarAutomatico($matriculaTO);

            return $mensageiro;
        } catch (Zend_Exception $e) {
            return new $e;
        }
    }

    public function liberarSituacaoTcc($dados)
    {
        try {
            if (empty($dados['id_alocacao'])):
                throw new Zend_Exception('Nenhum aluno selecionado!');
            else:
                foreach ($dados['id_alocacao'] as $id_alocacao) {

                    $to = new AlocacaoTO();
                    $to->setId_saladeaula($dados['id_saladeaula']);
                    $to->setId_situacaotcc(AlocacaoTO::SITUACAOTCC_APTO);
                    $where = "id_alocacao=" . $id_alocacao;
                    $validacao = $this->dao->editarAlocacao($to, $where);

                    if (!$validacao) {
                        throw new Zend_Exception('Liberação não efetuada!');
                    }

                    $boT = new \G2\Negocio\Tramite();
                    $st_tramite = 'Liberação de envio de TCC.';
                    $retorno = $boT->salvarTramiteTCC($st_tramite);

                    if ($retorno->getId()) {
                        $boT->salvarTramiteLiberarTCC($retorno->getId(), (int)$id_alocacao, AlocacaoTO::SITUACAOTCC_APTO);
                    }
                }

                return $this->mensageiro->setMensageiro('Liberação Efetuada e E-mail Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);

            endif;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /*
     * Metódo para controle de liberação de TCC
     * Atualiza campo id_situacaotcc (121 e 122: apto e não apto)
     * Débora Castro - 30/07/2013 Jira (AC-280)
     * @update - inserir cadastro de tramite (AC-1075) - Débora Castro
     */

    public function enviarEmailLiberacaoTcc(VwAlunosSalaTO $to)
    {
        try {

            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::LIBERAR_TCC, $to, TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->mensageiro->setMensageiro('Email Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro:', Ead1_IMensageiro::ERRO, $mensageiro->getFirstMensagem());
        }

        return $this->mensageiro;
    }

    /**
     * @param $dados
     * @return Ead1_Mensageiro
     * @throws Exception
     * @deprecated
     */
    public function cancelarSituacaoTcc($dados)
    {

        try {
            if (empty($dados['id_alocacao'])):
                throw new Zend_Exception('Nenhum aluno selecionado!');
            else:
                $to = new AlocacaoTO();
                $to->setId_saladeaula($dados['id_saladeaula']);
                $to->setId_situacaotcc(AlocacaoTO::SITUACAOTCC_APTO);
                $where = "id_alocacao=" . $dados['id_alocacao'];
                $validacao = $this->dao->editarAlocacao($to, $where);

                $avaliacaoTO = new AvaliacaoAlunoTO();
                $avaliacaoBO = new AvaliacaoBO();
                $avaliacaoTO->setId_avaliacaoaluno($dados['id_avaliacaoaluno']);
                $avaliacaoTO->fetch(false, true, true);
                $avaliacaoTO->setSt_justificativa($dados['st_justificativa']);
                $avaliacaoTO->setSt_nota(null, false);
                $avaliacaoTO->setId_upload(null);
                $avaliacaoBO->editarAvaliacaoAluno($avaliacaoTO, 'id_avaliacaoaluno = ' . $avaliacaoTO->getId_avaliacaoaluno(), null, true);

                if (!$validacao) {
                    throw new Zend_Exception('Cancelamento não efetuado!');
                }

                $boT = new \G2\Negocio\Tramite();
                $st_tramite = 'Devolução de TCC.';

                // Tipo de Tramite 'Devolucao de TCC' somente se tiver a justificativa
                if (isset($dados['st_justificativa']) && trim($dados['st_justificativa']) != '')
                    $st_tramite .= (' Motivo: ' . $dados['st_justificativa']);

                $retorno = $boT->salvarTramiteTCC($st_tramite);

                if ($retorno->getId()) {
                    $boT->salvarTramiteLiberarTCC($retorno->getId(), (int)$dados['id_alocacao'], AlocacaoTO::SITUACAOTCC_APTO);
                }

                $toEmail = new VwAlunosSalaTO();
                $toEmail->setId_saladeaula($dados['id_saladeaula']);
                $toEmail->setId_alocacao($dados['id_alocacao']);
                $toEmail->setId_entidade($toEmail->getSessao()->id_entidade);

                $consulta = $this->retornarVwAlunosSala($toEmail);
                if ($consulta->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Nenhum aluno encontrado!');
                }

                $mensagemBO = new MensagemBO();
                $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::LIBERAR_TCC, $consulta->getFirstMensagem(), TipoEnvioTO::EMAIL, false);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Erro pra enviar e-mail');
                }

                $this->mensageiro->setMensageiro('Cancelamento efetuado com sucesso!', Ead1_IMensageiro::SUCESSO);
            endif;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Retorna categorias da sala de aula
     * @param CategoriaSalaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarCategoriaSala(CategoriaSalaTO $to, $pesquisa = false)
    {
        $categorias = $this->dao->retornarCategoriaSala($to);
        if (is_object($categorias)) {
            $categorias = $categorias->toArray();
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($categorias, 'st_categoriasala', 'id_categoriasala');
        }
        if (!$categorias) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa categoria encontrada!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($categorias, new CategoriaSalaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Desaloca matrícula de uma disciplina.
     * @param int $id_matricula
     * @param int $id_disciplina
     */
    public function desalocarSalasMatriculaDisciplina($id_matricula, $id_disciplinas)
    {
        try {
            $vwSalaDisciplinaAlocacaoTO = new VwSalaDisciplinaAlocacaoTO();
            $vwSalaDisciplinaAlocacaoTO->setId_matricula($id_matricula);
            $where = 'id_matricula = ' . $vwSalaDisciplinaAlocacaoTO->getId_matricula();
            $where .= 'and id_alocacao is not null';
            if ($id_disciplinas) {
                $where .= ' AND id_disciplina not in  (' . implode(' , ', $id_disciplinas) . ')';
            }
            $salas = $this->dao->retornarVwSalaDisciplinaAlocacao($vwSalaDisciplinaAlocacaoTO, $where)->toArray();
            if (!empty($salas)) {
                $salas = Ead1_TO_Dinamico::encapsularTo($salas, new VwSalaDisciplinaAlocacaoTO());
                foreach ($salas as $sala) {
                    $alocacaoTO = new AlocacaoTO();
                    $alocacaoTO->setId_alocacao($sala->getId_alocacao());
                    $alocacaoTO->setId_matriculadisciplina($sala->getId_matriculadisciplina());
                    $mensageiro = $this->inativarAlocacao($alocacaoTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        return $mensageiro;
                    }
                }
            }
            return $this->mensageiro->setMensageiro('Salas desalocadas da matrícula com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao desalocar salas da matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Marca quais alocacoes seram desativadas posteriormente
     * @param $id_matricula
     * @return Ead1_Mensageiro
     */
    public function desalocarMatriculaDisciplinaTransferenciaCurso($id_matricula)
    {
        try {
            $vwSalaDisciplinaAlocacaoTO = new VwSalaDisciplinaAlocacaoTO();
            $vwSalaDisciplinaAlocacaoTO->setId_matricula($id_matricula);
            $where = 'vw.id_matricula = ' . $vwSalaDisciplinaAlocacaoTO->getId_matricula();
            $where .= ' AND vw.id_alocacao IS NOT NULL';

            //Mesmo as disciplinas que serão aproveitadas, serão desalocadas caso ele tenha não tenha sido encerrado
            /*if ($id_disciplinas) {
                $where .= ' AND vw.id_disciplina NOT IN  (' . implode(' , ', $id_disciplinas) . ')';
            }*/

            //retorna apenas as salas que não foram encerradas.
            $salas = $this->dao->retornaDisciplinasParaDesalocarNaTransferenciaCurso($vwSalaDisciplinaAlocacaoTO, $where)->toArray();
            if (!empty($salas)) {
                foreach ($salas as $sala) {
                    $this->desalocarAlunoTransferenciaCurso($sala);
                }
            }

            return $this->mensageiro->setMensageiro('Salas marcadas para serem desalocadas da matrícula com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao marcar salas para serem desalocadas da matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Desaloca matrícula de uma disciplina via robo.
     */
    public function desalocarSalasMatriculaDisciplinaRobo()
    {
        try {

            $alocacaoTO = new AlocacaoTO();
            $where = ' bl_desalocar = 1';

            $alocacoes = $this->dao->retornarAlocacao($alocacaoTO, $where);


            if (!empty($alocacoes)) {
                $alocacoes = Ead1_TO_Dinamico::encapsularTo($alocacoes, new AlocacaoTO());
                foreach ($alocacoes as $alocacao) {
                    $mensageiro = $this->inativarAlocacao($alocacao);

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        return $mensageiro;
                    } else {
                        $alocacao->setBl_desalocar(0);
                        $this->dao->editarAlocacao($alocacao);
                    }
                }
            }
            return $this->mensageiro->setMensageiro('Salas desalocadas da matrícula com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao desalocar salas da matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que aloca um aluno na mesma sala de aula da qual ele foi transferido e teve aproveitamento de nota
     * @param MatriculaTO $matriculaTO
     * @param bool $bl_naoalocar
     * @param $id_disciplinas
     * @return Ead1_Mensageiro
     */
    public function alocarTransferencia(MatriculaTO $matriculaTO, $bl_naoalocar = false, $id_disciplinas)
    {
        $dao = new MatriculaDAO();

        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception("Matrícula não identificada!");
            }
            if (!is_array($id_disciplinas)) {
                throw new Zend_Exception('Disciplinas não identificadas para alocação na mesma sala após transferência');
            }
            if (!$matriculaTO->getId_matriculaorigem()) {
                throw new Zend_Exception("Essa matrícula não foi transferida e essa operação não é permitida!");
            }

            $alocados = array();
            $dados = array();

            $matriculaTO->fetch(true, true, true);

            $projetoPedagogicoTO = new ProjetoPedagogicoTO();
            $projetoPedagogicoTO->setId_projetopedagogico($matriculaTO->getId_projetopedagogico());
            $projetoPedagogicoTO->fetch(true, true, true);

            foreach ($id_disciplinas as $id_disciplina) {
                $vw_alunossala = new VwAlunosSalaTO();
                $vw_alunossala->setId_matricula($matriculaTO->getId_matriculaorigem());
                $vw_alunossala->setId_usuario($matriculaTO->getId_usuario());
                $vw_alunossala->setId_disciplina((int)$id_disciplina);
                $vw_alunossala->setId_categoriasala(\G2\Constante\CategoriaSala::NORMAL);
                $vw_alunossala->fetch(false, true, true);

                $matriculaDisciplina = new MatriculaDisciplinaTO();
                $matriculaDisciplina->setId_disciplina((int)$id_disciplina);
                $matriculaDisciplina->setId_matricula($matriculaTO->getId_matricula());
                $matriculaDisciplina->fetch(false, true, true);

                if ($vw_alunossala->getId_saladeaula() && $matriculaDisciplina->getId_matriculadisciplina()) {
                    $alocacaoTO = new AlocacaoTO();
                    $alocacaoTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO);
                    $alocacaoTO->setId_situacao(\G2\Constante\Situacao::TB_ALOCACAO_ATIVA);
                    $alocacaoTO->setId_saladeaula($vw_alunossala->getId_saladeaula());
                    $alocacaoTO->setId_categoriasala(SalaDeAulaTO::SALA_NORMAL);
                    $alocacaoTO->setId_usuariocadastro($matriculaTO->getId_usuario());
                    $alocacaoTO->setId_matriculadisciplina($matriculaDisciplina->getId_matriculadisciplina());

                    $mensageiroAlocacao = $this->salvarAlocacaoAluno($alocacaoTO, $bl_naoalocar);
                    if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $alocados[] = $matriculaDisciplina->getId_matriculadisciplina();
                        $dados['sucesso'][] = $mensageiroAlocacao->getMensagem();
                    } else {
                        $dados['erro'][] = $mensageiroAlocacao->getMensagem();
                    }
                }

            }
            return new Ead1_Mensageiro("Alocação de transferencia na mesma sala bem sucedidas", Ead1_IMensageiro::SUCESSO, $dados);
        } catch (Zend_Validate_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que verifica e salva a alocacao do aluno
     * @param AlocacaoTO $aTO
     * @param $bl_naoalocar Não aloca o aluno na sala integração para evitar sobrecarga do sistema
     * @param $venda Caso a venda seja passada, atualiza o nu_semestre da tb_matriculadisciplina (apenas para entidades com linha de negocio = GRADUAÇÃO)
     * @return Ead1_Mensageiro
     */
    public function salvarAlocacaoAluno(AlocacaoTO $aTO, $bl_naoalocar = false, $venda = false)
    {
        try {
            $aTOConsulta = new AlocacaoTO();

            if (!$aTO->getId_matriculadisciplina()) {
                throw new Zend_Exception('O TO não veio com o id_matriculadisciplina setado!');
            }

            if (!$aTO->getId_categoriasala()) {
                throw new Zend_Exception('O TO não veio com o id_categoriasala setado!');
            }

            $aTOConsulta->setId_matriculadisciplina($aTO->getId_matriculadisciplina());
            $aTOConsulta->setBl_ativo(true);

            if ($aTO->getId_categoriasala() == SalaDeAulaTO::SALA_PRR) {
                //Verifica se já tem alocação para esta disciplina na categoria PRR (2).
                $aTOConsulta->setId_categoriasala(SalaDeAulaTO::SALA_PRR);

                $mensageiro = $this->inativarAlocacao($aTOConsulta);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $mensageiro;
                }
            } elseif ($aTO->getId_categoriasala() == SalaDeAulaTO::SALA_NORMAL) {
                $aTOConsulta->setId_categoriasala(SalaDeAulaTO::SALA_NORMAL);
                $mensageiro = $this->inativarAlocacao($aTOConsulta);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $mensageiro;
                }
            }
            $aTO->setId_usuariocadastro((!$aTO->getSessao()->id_usuario ? $aTO->getId_usuario() : $aTO->getSessao()->id_usuario));
            $aTO->setId_alocacao(null);
            $aTO->setBl_ativo(true);
            $aTO->setId_situacao(AlocacaoTO::SITUACAO_ATIVA);
            $aTO->setId_evolucao(AlocacaoTO::EVOLUCAO_CURSANDO);
            $aTO->setId_alocacao($this->dao->cadastrarAlocacao($aTO));

            $maTO = new MatriculaDisciplinaTO();
            $maTO->setId_matriculadisciplina($aTO->getId_matriculadisciplina());
            $maTO->setId_evolucao(MatriculaDisciplinaTO::EVOLUCAO_CURSANDO);
            $maTO->setId_situacao(MatriculaDisciplinaTO::SITUACAO_ATIVA);
            $maTO->setNu_aprovafinal(null);

            if ($venda) {
                $maTO->setNu_semestre($venda->getNu_semestre());
            }

            if ($this->dao->atualizaMatriculaDisciplina($maTO) === false) {
                throw new Zend_Exception($this->dao->excecao->getMessage());
            }

            $this->verificaNotasAlocacaoAntiga($aTO);

            $mensagemWs = '';
            /*
             * Ponto de integração com demais sistemas
             * Após salvar o registro da alocação, serão feitas a integrações
             */
            if (!$bl_naoalocar) {
                //essa integração é referente ao sistema (LMS) que está setado para a sala
                $menintegracao = $this->salvarIntegracaoAlunoWS($aTO);

                //Se a integração falhar, ele grava na tabela de erros o que aconteceu durante a alocacao do moodle
                if ($menintegracao->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $maTO->fetch(true, true, true);
                    $vwMatriculaDisciplina = new VwMatriculaDisciplinaTO();
                    $vwMatriculaDisciplina->setId_matriculadisciplina($maTO->getId_matriculadisciplina());
                    $vwMatriculaDisciplina->fetch(false, true, true);

                    $erroTO = new ErroTO();
                    $erroTO->setId_entidade(($maTO->getSessao()->id_entidade ? $maTO->getSessao()->id_entidade : null));
                    $erroTO->setId_sistema(SistemaTO::GESTOR);
                    $erroTO->setId_usuario(($erroTO->getSessao()->id_usuario ? $erroTO->getSessao()->id_usuario : null));
                    $erroTO->setId_processo(ProcessoTO::AUTO_ALOCAR); //AutoAlocar
                    $erroTO->setSt_mensagem(($menintegracao->getFirstMensagem()));
                    $erroTO->setSt_codigo($maTO->getId_matriculadisciplina());
                    $erroTO->setSt_origem(('Disciplina: ' . $vwMatriculaDisciplina->getSt_disciplina()));

                    $erroBO = new ErroBO();
                    $erroBO->salvar($erroTO);
                }
                //a mensagem de retorno dessa integração será associada à mensagem de sucesso da alocação
                $mensagemWs = $menintegracao->getFirstMensagem();
            }

            return $this->mensageiro->setMensageiro('Alocação Salva com Sucesso! ' . $mensagemWs, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao alocar aluno. ' . $mensagemWs, Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método busca notas da alocação a ser desativada para inativa-las
     * @param AlocacaoTO $aTO
     * @return string
     */
    public function verificaNotasAlocacaoAntiga(AlocacaoTO $aTO)
    {
        try {
            if ($aTO->getId_matriculadisciplina()) {
                $avaDAO = new AvaliacaoDAO();
                $vw = new VwAvaliacaoAlunoTO();
                $vw->setId_categoriasala($aTO->getId_categoriasala());
                $vw->setId_matriculadisciplina($aTO->getId_matriculadisciplina());
                $avaliacoes = $avaDAO->retornarIdAvaliacaoAluno($vw);

                if (!empty($avaliacoes)) {
                    foreach ($avaliacoes->toArray() as $avaliacao) {
                        if ($avaliacao['id_avaliacaoaluno'] != null):
                            $avaliacaoTO = new AvaliacaoAlunoTO();
                            $avaliacaoTO->setId_avaliacaoaluno($avaliacao['id_avaliacaoaluno']);
                            $avaliacaoTO->setBl_ativo(false);
                            $avaDAO->editarAvaliacaoAluno($avaliacaoTO);
                        endif;
                    }
                }
                $matriculaDisciplina = new MatriculaDisciplinaTO();
                $matriculaDisciplina->setId_matriculadisciplina($aTO->getId_matriculadisciplina());
                $matriculaDisciplina->fetch(true, true, true);
                $matriculaDisciplina->setNu_aprovafinal(0);
                $salaDeAulaDao = new SalaDeAulaDAO();
                $salaDeAulaDao->atualizaMatriculaDisciplina($matriculaDisciplina);
            }
        } catch (Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     *
     * Retorna os dados para o Secretaria -> Enc. Financeiro
     * @param $tipo_liberacao
     * @param $id_categoriasala
     * @param $id_tipodisciplina
     * @param $id_professor
     * @param bool $bl_recusa
     * @return bool|null
     * @throws Exception
     */
    public function retornarEncerramento($tipo_liberacao, $id_categoriasala, $id_tipodisciplina, $id_professor, $bl_recusa = false)
    {

        try {

            $bo = new SecretariaBO();
            $vw = new VwEncerramentoSalasTO();

            //validar o tipo de disciplina
            if ($id_tipodisciplina != TipoDisciplinaTO::TCC) {
                $bl_recusa = null;
            }

            if (isset($id_professor)) {
                $vw->setId_usuarioprofessor($id_professor);
            }

            $vw->setId_tipodisciplina($id_tipodisciplina);

            switch ($tipo_liberacao) {
                case 'pagos':
                    $vw->setId_categoriasala($id_categoriasala);
                    $return = $bo->retornarVwEncerramentoSalas_Pagos($vw, $bl_recusa);
                    break;
                case 'financeiro':
                    $vw->setId_categoriasala($id_categoriasala);
                    $return = $bo->retornarVwEncerramentoSalas_Financeiro($vw, $bl_recusa);
                    break;
                case 'coordenador':
                    $return = $bo->retornarVwEncerramentoSalas_Coordenador($vw, $bl_recusa);
                    break;
                case 'pedagogico':
                    $vw->setId_categoriasala($id_categoriasala);
                    if ($vw->getId_tipodisciplina() == TipoDisciplinaTO::TCC) {
                        $bl_recusado = $bl_recusa;
                        $return = $bo->retornarVwEncerramentoSalas_Pedagogico($vw, $bl_recusado);
                    } else {
                        $return = $bo->retornarVwEncerramentoSalas_Pedagogico($vw);
                    }
                    break;
                default:
                    $return = false;
                    break;
            }

            if ($return == false) {
                return false;
            }


            $arReturn = $return->getMensagem();
            $arTipo = $return->getTipo();

            if ($arTipo == Ead1_IMensageiro::SUCESSO) {
                foreach ($arReturn as $k => $vw) {
                    if (empty($vw->getId_aluno())) {
                        continue;
                    }

                    if ($vw->getDt_encerramentocoordenador()) {
                        $vw->setDt_encerramentocoordenador($vw->getDt_encerramentocoordenador()->toString('dd/MM/Y'));
                    } else {
                        $vw->setDt_encerramentocoordenador('');
                    }

                    if ($vw->getDt_encerramentofinanceiro()) {
                        $vw->setDt_encerramentofinanceiro($vw->getDt_encerramentofinanceiro()->toString('dd/MM/Y'));
                    } else {
                        $vw->setDt_encerramentofinanceiro('');
                    }

                    if ($vw->getDt_encerramentopedagogico()) {
                        $vw->setDt_encerramentopedagogico($vw->getDt_encerramentopedagogico()->toString('dd/MM/Y'));
                    } else {
                        $vw->setDt_encerramentopedagogico('');
                    }

                    if ($vw->getDt_encerramentoprofessor()) {
                        $vw->setDt_encerramentoprofessor($vw->getDt_encerramentoProfessor()->toString('dd/MM/Y'));
                    } else {
                        $vw->setDt_encerramentoprofessor('');
                    }

                    if ($id_tipodisciplina == \G2\Constante\TipoDisciplina::TCC) {
                        $url_interacao = $bo->gerarLinkResumoInteracoes($vw);
                        $vw->setSt_urlinteracoes($url_interacao->text);
                        if ($vw->getSt_nota() != 'Desalocado')
                            $vw->setSt_nota(intval($vw->getSt_nota()));
                    }

                    $arReturn[$k] = $vw;
                }
            } else {
                $arReturn = null;
            }
            return $arReturn;

        } catch (Exception $e) {
            throw $e;
        }

    }

    /**
     * Retorna Projetos de uma Sala de aula - mostrando o perfil de coordenador apenas
     * @param VwAreaProjetoSalaTO $vwAreaProjetoSala
     * @return Ead1_Mensageiro
     */
    public function retornarVwAreaProjetoSalaGestao(VwAreaProjetoSalaTO $vwAreaProjetoSala)
    {
        try {
            $salas = $this->dao->retornarVwAreaProjetoSalaGestao($vwAreaProjetoSala)->toArray();
            if (empty($salas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Projetos na Sala de Aula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($salas, new VwAreaProjetoSalaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Projetos na Sala de Aula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * @param $data
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function desalocarAlunoTransferenciaCurso($data)
    {
        if (empty($data['id_alocacao'])) {
            return new \Ead1_Mensageiro('Não foi encontrada uma alocação válida nesta sala!', \Ead1_IMensageiro::AVISO);
        }

        try {
            $alocacaoTO = new AlocacaoTO();
            $alocacaoTO->setId_alocacao($data['id_alocacao']);
            $alocacaoTO->fetch(false, true, true);

            //Se tiver o id da sala de aula é porque retornou dados o fetch
            if ($alocacaoTO->getId_saladeaula()) {
                $id_alocacao = $alocacaoTO->getId_alocacao();
                $id_saladeaula = $alocacaoTO->getId_saladeaula();
                $id_sistema = null;

                // Buscar alocações duplicadas
                $alocoesDuplucadasTO = new AlocacaoTO();
                $alocoesDuplucadasTO->setId_matriculadisciplina($alocacaoTO->getId_matriculadisciplina());
                $alocoesDuplucadasTO->setBl_ativo(1);
                $alocoesDuplucadasTO->setId_categoriasala($alocacaoTO->getId_categoriasala());

                $alocacaoORM = new AlocacaoORM();
                $arrAlocacoesDuplucadasTO = $alocacaoORM->consulta($alocoesDuplucadasTO);

                if (!empty($arrAlocacoesDuplucadasTO)) {
                    // Desativar todas as alocações encontradas
                    foreach ($arrAlocacoesDuplucadasTO as $alocacaoDupĺicadaTO) {
                        $alocacaoDupĺicadaTO->setBl_ativo(false);
                        $alocacaoORM->update($alocacaoDupĺicadaTO->toArrayUpdate(
                            true,
                            array("id_alocacao")
                        ),
                            "id_alocacao = {$alocacaoDupĺicadaTO->getId_alocacao()}"
                        );
                        // Busca a matriculadisciplina
                        $matriculadisciplinaTO = new MatriculaDisciplinaTO();
                        $matriculadisciplinaTO->setId_matriculadisciplina($alocacaoDupĺicadaTO->getId_matriculadisciplina());
                        $matriculadisciplinaTO->fetch(true, true, true);
                        if (!$matriculadisciplinaTO->getId_matricula()) {
                            throw new \Zend_Exception("Matricula disciplina não encontrada.");
                        }

                        // Alterar Evolução da Matricula Disciplina para não alocado e Situação para Pendente.
                        $matriculadisciplinaTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO);
                        $matriculadisciplinaTO->setId_situacao(\G2\Constante\Situacao::TB_MATRICULADISCIPLINA_PENDENTE);

                        $matriculadisciplinaORM = new MatriculaDisciplinaORM();
                        $matriculadisciplinaORM->update($matriculadisciplinaTO->toArrayUpdate(
                            false,
                            array("id_matriculadisciplina")
                        ),
                            "id_matriculadisciplina = {$alocacaoDupĺicadaTO->getId_matriculadisciplina()}"
                        );
                    }
                }

                // Alterar Evolução da Matricula Disciplina para Não Cursado caso seja desalocado
                $salaintegracaoTO = new SalaDeAulaIntegracaoTO();
                $salaintegracaoTO->setId_saladeaula($id_saladeaula);
                $salaintegracaoTO->fetch(false, true, true);

                $id_sistema = null;
                // checa se a variável $salaintegracao é um objeto válido
                if ($salaintegracaoTO instanceof SalaDeAulaIntegracaoTO) {
                    // caso passe no teste, chama a função getId_sistema
                    $id_sistema = $salaintegracaoTO->getId_sistema();
                    if (!$id_sistema) {
                        throw new \Zend_Exception("ID do sistema inexistente!");
                    }
                }

                switch ($id_sistema) {
                    case \G2\Constante\Sistema::MOODLE:

                        //busca a matriculadisciplina
                        $matriculadisciplinaUTo = new MatriculaDisciplinaTO();
                        $matriculadisciplinaUTo->setId_matriculadisciplina($alocacaoTO->getId_matriculadisciplina());
                        $matriculadisciplinaUTo->fetch(false, true, true);
                        if (!$matriculadisciplinaUTo->getId_matricula()) {
                            throw new \Zend_Exception("Matricula disciplina não encontrada.");
                        }

                        /** @var \G2\Entity\Matricula $en_matricula */
                        $negocio = new \G2\Negocio\Negocio();
                        $en_matricula = $negocio->find('\G2\Entity\Matricula', $matriculadisciplinaUTo->getId_matricula());

                        if (!($en_matricula instanceof \G2\Entity\Matricula)) {
                            throw new \Zend_Exception("Matricula não encontrada.");
                        }

                        $vwAlocacaoTO = new VwAlocacaoTO();
                        $vwAlocacaoTO->setId_alocacao($id_alocacao);
                        $vwAlocacaoTO->fetch(false, true, true);

                        if (!$id_usuario = $vwAlocacaoTO->getId_usuario()) {
                            throw new \Zend_Exception("Aluno não alocado!");
                        }

                        $saladeaulaTO = new SalaDeAulaTO();
                        $saladeaulaTO->setId_saladeaula($id_saladeaula);
                        $saladeaulaTO->fetch(false, true, true);

                        if (!$saladeaulaTO->getId_entidade()) {
                            throw new \Zend_Exception('Sala de aula não encontrada.');
                        }
                        // Suspender aluno de TODAS as salas que ele está alocado desde que a sala não esteja
                        //  compartilhada com outra matrícula
                        $ws_moodle = new \MoodleAlocacaoWebServices($saladeaulaTO->getId_entidade());
                        $ws_moodle->suspenderAlunoSalas($en_matricula, array(
                            'id_saladeaula' => $id_saladeaula
                        ));

                        break;
                    case \G2\Constante\Sistema::BLACKBOARD:
                        $bo = new \IntegracaoBO();
                        $bo->inativarUsuarioCurso($this->sessao->id_entidade, $id_sistema, $id_alocacao);
                        break;
                    default:
                        break;
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Alocações inativadas com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro(
                'Erro ao inativar Alocação. Motivo: ' . $e->getMessage(),
                \Ead1_IMensageiro::ERRO,
                $e->getMessage()
            );
        }

        return $mensageiro;
    }

}

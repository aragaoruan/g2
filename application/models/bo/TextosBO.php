<?php
/**
 * Classe que contém as interações para textos do sistema
 * @author edermariano
 *
 */
class TextosBO extends Ead1_BO
{
    /**
     * @var TextosDAO
     */
    private $dao;

    public function __construct() {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new TextosDAO();
    }

    /**
     * Metodo que Retorna os textos do sistema
     * @param TextoSistemaTO $tsTO
     * @param boolean $bl_sementidade - retorna todos texto sem setar o id entidade
     * @return Ead1_Mensageiro
     */
    public function retornarTextoSistema(TextoSistemaTO $tsTO, $bl_sementidade = false) {
        try {
            if (!$tsTO->getId_entidade() && $bl_sementidade) {
                $tsTO->setId_entidade($tsTO->getSessao()->id_entidade);
            }

            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarTextoSistema($tsTO));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Texto Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new TextoSistemaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de textos do sistema.
     * @param VwTextoSistemaTO $vwtsTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwTextoSistema(VwTextoSistemaTO $vwtsTO) {
        try {
            if (empty($vwtsTO->id_entidade)) {
                $vwtsTO->id_entidade = $vwtsTO->getSessao()->id_entidade;
            }

            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwTextoSistema($vwtsTO));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Texto Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new VwTextoSistemaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Texto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de geração de declaração.
     * @param VwGerarDeclaracaoTO $vwgdTO
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarDeclaracao(VwGerarDeclaracaoTO $vwgdTO, $where = null) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGerarDeclaracao($vwgdTO, $where));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Dado de Declaração Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new VwGerarDeclaracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados de Declaração!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de geração de ocorrência.
     * @param VwGerarOcorrenciaTO $vwGerarOcorrenciaTO
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarOcorrencia(VwGerarOcorrenciaTO $vwGerarOcorrenciaTO, $where = null) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray(
                $this->dao->retornarVwGerarOcorrencia($vwGerarOcorrenciaTO, $where)
            );
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Dado de Ocorrência Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new VwGerarOcorrenciaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados de Ocorrência!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de geração de texto de ClasseAfiliado
     * @param ClasseAfiliadoTO $classeAfiliadoTO
     * @param unknown_type $where
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarClasseAfiliado(ClasseAfiliadoTO $classeAfiliadoTO, $where = null) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray(
                $this->dao->retornarClasseAfiliado($classeAfiliadoTO, $where)
            );
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Dado de Classe Afiliado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new ClasseAfiliadoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados de Classe Afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a categoria do texto
     * @param TextoCategoriaTO $tcTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoCategoria(TextoCategoriaTO $tcTO) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarTextoCategoria($tcTO));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Texto Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new TextoCategoriaTO(), true), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Categoria do Texto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as variaveis do texto
     * @param TextoVariaveisTO $tvTO
     * @return Ead1_Mensageiro
     */
    public function retornarTextoVariaveis(TextoVariaveisTO $tvTO) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarTextoVariaveis($tvTO));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Variável de Texto Encontrada!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new TextoVariaveisTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Variaveis do Texto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view dos textos do sistema
     * @param VwGerarTextoTO $vwGtTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarTexto(VwGerarTextoTO $vwGtTO) {
        try {
            $texto = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGerarTexto($vwGtTO));
            if (empty($texto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Texto Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($texto, new VwGerarTextoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a view de Texto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de gerar contratos
     * @param VwGerarContratoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarContrato(VwGerarContratoTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGerarContrato($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhuma Informação de Contrato Encontrada.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGerarContratoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados do Contrato.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a view de dados pessoais
     * @param VwDadosPessoaisTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwDadosPessoais(VwDadosPessoaisTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwDadosPessoais($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum dado Pessoal Encontrado.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwDadosPessoaisTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados Pessoais.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a view de gerar texto da venda
     * @param VwGerarVendaTextoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarVendaTexto(VwGerarVendaTextoTO $to, $where = null) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray(
                $this->dao->retornarVwGerarVendaTexto($to, $where)
            );
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum dado de Venda Encontrado.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGerarVendaTextoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados da Venda.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a view de gerar texto da venda
     * @param VwGerarVendaTextoLancamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarVendaTextoLancamento(VwGerarVendaTextoLancamentoTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray(
                $this->dao->retornarVwGerarVendaTextoLancamento($to)
            );
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum dado de Lancamento Encontrado.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGerarVendaTextoLancamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados do Lancamento.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna a view de grid?
     * @param VwGridTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGrid(VwGridTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGrid($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum dado de Grid Encontrado.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGridTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados da Grid.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados de historico do Aluno
     * @param VwHistoricoGeralTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwHistoricoGeral(VwHistoricoGeralTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwHistoricoGeral($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum Dado Encontrado Para Gerar Histórico do Aluno.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwHistoricoGeralTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados do Histórico do Aluno.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados de historico do Aluno
     * @param VwHistoricoGeralTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGridHistorico(VwGridHistoricoTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGridHistorico($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum Dado Encontrado Para Gerar Histórico do Aluno.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGridHistoricoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados do Histórico do Aluno.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados de historico do Aluno
     * @param VwHistoricoGeralTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGridEstabelecimento(VwGridEstabelecimentoTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGridEstabelecimento($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum Dado Encontrado Para Gerar a Grid de Estabelecimento do Aluno.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGridEstabelecimentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados do Estabelecimento do Aluno.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados do cabecalho
     * @param VwGerarCabecalhoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwGerarCabecalho(VwGerarCabecalhoTO $to) {
        try {
            $dados = parent::resultToArrayIfObjectOrReturnEmptyArray($this->dao->retornarVwGerarCabecalho($to));
            if (empty($dados)) {
                throw new Zend_Validate_Exception("Nenhum dado Encontrado para poder Gerar Cabeçalho.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwGerarCabecalhoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Dados Para Montar Cabeçalho.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que exclui o texto sistema
     * @param TextoSistemaTO $tsTO
     * @return Ead1_Mensageiro
     */
    public function deletarTextoSistema(TextoSistemaTO $tsTO) {
        try {
            $tsTO->setId_entidade(($tsTO->getId_entidade() ? $tsTO->getId_entidade() : $tsTO->getSessao()->id_entidade));
            $this->dao->deletarTextoSistema($tsTO);
            $this->mensageiro->setMensageiro('Texto Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Não é Possivel Excluir este Texto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    public function trataMedia($valor) {
        if (empty($valor)) {
            return '--';
        }
        if ($valor / 2) {
            if (is_string($valor) && (substr($valor, -2, -1) == ',' || substr($valor, -3, -2) == ',')) {
                $valor = str_replace(',', '.', $valor);
            }
            return $valor;
        } else {
            return $valor;
        }
    }
}

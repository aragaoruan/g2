<?php

/**
 * Classe com regras de negÃ³cio para interaÃ§Ãµes de Entidade
 * @author Eduardo RomÃ£o - ejushiro@gmail.com
 * @since 27/09/2010
 *
 * @package models
 * @subpackage bo
 */
class EntidadeBO extends Ead1_BO
{

    public $mensageiro;
    public $dao;
    private $entidades;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new EntidadeDAO();
    }

    /**
     * Metodo que decide se cadastra ou edita Conta Bancaria da Entidade
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContaEntidade(ContaEntidadeTO $ceTO)
    {
        if ($ceTO->getId_contaentidade()) {
            return $this->editarContaEntidade($ceTO);
        }
        return $this->cadastrarContaEntidade($ceTO);
    }

    /**
     * Metodo que salvar um array de funcionalidades da entidade alterando o bl ativo
     * @param Array $arrEntidadeFuncionalidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrEntidadeFuncionalidadeUsuario($arrEntidadeFuncionalidadeTO)
    {
        $this->dao->beginTransaction();
        try {

            if (!is_array($arrEntidadeFuncionalidadeTO)) {
                THROW new Zend_Exception('O Parametro recebido nÃ£o Ã© um array!');
            }

            $tempTO = $arrEntidadeFuncionalidadeTO[0];


            if (!($tempTO instanceof EntidadeFuncionalidadeTO)) {
                THROW new Zend_Exception('Um dos Objetos do Array nÃ£o Ã© uma instancia de EntidadeFuncionalidadeTO');
            }
            $entidadeFuncionalidadeEdicaoTO = new EntidadeFuncionalidadeTO();
            $entidadeFuncionalidadeEdicaoTO->setId_entidade(($tempTO->getId_entidade() ? $tempTO->getId_entidade() : $tempTO->getSessao()->id_entidade));
            $where = 'id_entidade = ' . $entidadeFuncionalidadeEdicaoTO->getId_entidade() . ' AND id_funcionalidade not in(select id_funcionalidade from tb_funcionalidade where id_tipofuncionalidade = ' . TipoFuncionalidadeTO::TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU . ')';
            $entidadeFuncionalidadeEdicaoTO->setBl_ativo(null);
            $entidadeFuncionalidadeEdicaoTO->setBl_visivel(false);
            $this->salvarEntidadeFuncionalidadeBotao($entidadeFuncionalidadeEdicaoTO->getId_entidade());
            $this->dao->editarEntidadeFuncionalidade($entidadeFuncionalidadeEdicaoTO, $where);
            $arrFuncionalidades = $this->dao->retornarEntidadeFuncionalidade(new EntidadeFuncionalidadeTO(), $where)->toArray();
            $arrFuncionalidades = Ead1_TO_Dinamico::encapsularTo($arrFuncionalidades, new EntidadeFuncionalidadeTO());
            $funcionalidades = array();
            if (!empty($arrFuncionalidades)) {
                foreach ($arrFuncionalidades as $funcionalidade) {
                    $funcionalidades[$funcionalidade->getId_funcionalidade()] = $funcionalidade;
                }
            }
            foreach ($arrEntidadeFuncionalidadeTO as $entidadeFuncionalidadeTO) {
                if (!($entidadeFuncionalidadeTO instanceof EntidadeFuncionalidadeTO)) {
                    THROW new Zend_Exception('Um dos Objetos do Array nÃ£o Ã© uma instancia de EntidadeFuncionalidadeTO');
                }
                if (!$entidadeFuncionalidadeTO->getId_entidade()) {
                    $entidadeFuncionalidadeTO->setId_entidade($entidadeFuncionalidadeTO->getSessao()->id_entidade);
                }
                //Setando variaveis como null para que nÃ£o sejam editadas.
                $entidadeFuncionalidadeTO->setBl_obrigatorio(null);
                $entidadeFuncionalidadeTO->setId_situacao(null);
                $entidadeFuncionalidadeTO->setBl_ativo(null);
                if (array_key_exists($entidadeFuncionalidadeTO->getId_funcionalidade(), $funcionalidades)) {
                    $where = 'id_entidade = ' . $entidadeFuncionalidadeEdicaoTO->getId_entidade() . ' AND id_funcionalidade = ' . $entidadeFuncionalidadeTO->getId_funcionalidade();
                    $this->dao->editarEntidadeFuncionalidade($entidadeFuncionalidadeTO, $where);
                } else {
                    $this->dao->cadastrarEntidadeFuncionalidade($entidadeFuncionalidadeTO);
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Funcionalidades Salvas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Funcionalidades!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salvar um array de funcionalidades da entidade alterando o bl ativo
     * @param Array $arrEntidadeFuncionalidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrEntidadeFuncionalidadeSuperUsuario($arrEntidadeFuncionalidadeTO)
    {

        $this->dao->beginTransaction();
        try {

            if (!is_array($arrEntidadeFuncionalidadeTO)) {
                THROW new Zend_Exception('O Parametro recebido nÃ£o Ã© um array!');
            }

            $tempTO = $arrEntidadeFuncionalidadeTO[0];


            if (!($tempTO instanceof EntidadeFuncionalidadeTO)) {
                THROW new Zend_Exception('Um dos Objetos do Array nÃ£o Ã© uma instancia de EntidadeFuncionalidadeTO');
            }
            $entidadeFuncionalidadeEdicaoTO = new EntidadeFuncionalidadeTO();
            $entidadeFuncionalidadeEdicaoTO->setId_entidade(($tempTO->getId_entidade() ? $tempTO->getId_entidade() : $tempTO->getSessao()->id_entidade));
            $where = 'id_entidade = ' . $entidadeFuncionalidadeEdicaoTO->getId_entidade() . ' AND id_funcionalidade not in (select id_funcionalidade from tb_funcionalidade where id_tipofuncionalidade = ' . TipoFuncionalidadeTO::TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU . ')';
            $entidadeFuncionalidadeEdicaoTO->setBl_ativo(false);
            $entidadeFuncionalidadeEdicaoTO->setBl_visivel(null);
            $this->salvarEntidadeFuncionalidadeBotao($entidadeFuncionalidadeEdicaoTO->getId_entidade());
            $this->dao->editarEntidadeFuncionalidade($entidadeFuncionalidadeEdicaoTO, $where);
            $arrFuncionalidades = $this->dao->retornarEntidadeFuncionalidade(new EntidadeFuncionalidadeTO(), $where)->toArray();
            $arrFuncionalidades = Ead1_TO_Dinamico::encapsularTo($arrFuncionalidades, new EntidadeFuncionalidadeTO());
            $funcionalidades = array();
            if (!empty($arrFuncionalidades)) {
                foreach ($arrFuncionalidades as $funcionalidade) {
                    $funcionalidades[$funcionalidade->getId_funcionalidade()] = $funcionalidade;
                }
            }
            foreach ($arrEntidadeFuncionalidadeTO as $entidadeFuncionalidadeTO) {
                if (!($entidadeFuncionalidadeTO instanceof EntidadeFuncionalidadeTO)) {
                    THROW new Zend_Exception('Um dos Objetos do Array nÃ£o Ã© uma instancia de EntidadeFuncionalidadeTO');
                }
                if (!$entidadeFuncionalidadeTO->getId_entidade()) {
                    $entidadeFuncionalidadeTO->setId_entidade($entidadeFuncionalidadeTO->getSessao()->id_entidade);
                }
                //Setando variaveis como null para que nÃ£o sejam editadas.
                $entidadeFuncionalidadeTO->setBl_obrigatorio(null);
                $entidadeFuncionalidadeTO->setId_situacao(null);
                $entidadeFuncionalidadeTO->setBl_visivel(null);
                if (array_key_exists($entidadeFuncionalidadeTO->getId_funcionalidade(), $funcionalidades)) {
                    $where = 'id_entidade = ' . $entidadeFuncionalidadeEdicaoTO->getId_entidade() . ' AND id_funcionalidade = ' . $entidadeFuncionalidadeTO->getId_funcionalidade();
                    $this->dao->editarEntidadeFuncionalidade($entidadeFuncionalidadeTO, $where);
                } else {
                    $entidadeFuncionalidadeTO->setId_situacao((!$entidadeFuncionalidadeTO->getId_situacao() ? EntidadeFuncionalidadeTO::SITUACAO_ATIVA : $entidadeFuncionalidadeTO->getId_situacao()));
                    $this->dao->cadastrarEntidadeFuncionalidade($entidadeFuncionalidadeTO);
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Funcionalidades Salvas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Funcionalidades!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salva os botoes para as funcionalidades da entidade
     * @param int $id_entidade
     * @throws Zend_Exception
     */
    private function salvarEntidadeFuncionalidadeBotao($id_entidade)
    {
        try {
            $this->dao->beginTransaction();
            //ID's Fixos
            $ID_SITUACAO_ENTIDADE_FUNCIONALIDADE = 5;
            //------------------
            $perfilDAO = new PerfilDAO();
            $entidadeFuncionalidadeTO = new EntidadeFuncionalidadeTO();
            if (!$id_entidade) {
                $id_entidade = $entidadeFuncionalidadeTO->getSessao()->id_entidade;
            }
            $entidadeFuncionalidadeTO->setId_entidade($id_entidade);
            $entidadeFuncionalidadeTO->setBl_ativo(true);
            $entidadeFuncionalidadeTO->setBl_visivel(true);
            $entidadeFuncionalidadeTO->setBl_obrigatorio(false);
            $entidadeFuncionalidadeTO->setId_situacao($ID_SITUACAO_ENTIDADE_FUNCIONALIDADE);
            $funcionalidadeTO = new FuncionalidadeTO();
            $funcionalidadeTO->setBl_ativo(null);
            $funcionalidadeTO->setId_tipofuncionalidade(TipoFuncionalidadeTO::TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU);

            $arrBotoes = $perfilDAO->retornaFuncionalidade($funcionalidadeTO)->toArray();
            if (empty($arrBotoes)) {
                THROW new Zend_Exception('Nenhuma funcionalidade do tipo BOTAO ADICIONAR MENU encontrada!');
            }
            $arrBotoes = Ead1_TO_Dinamico::encapsularTo($arrBotoes, new FuncionalidadeTO());
            $where = 'id_entidade = ' . $id_entidade . ' AND id_funcionalidade in (select id_funcionalidade from tb_funcionalidade where id_tipofuncionalidade = ' . TipoFuncionalidadeTO::TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU . ')';
            $this->dao->deletarEntidadeFuncionalidade(new EntidadeFuncionalidadeTO(), $where);
            foreach ($arrBotoes as $botao) {
                $entidadeFuncionalidadeTO->setId_funcionalidade($botao->getId_funcionalidade());
                $this->dao->cadastrarEntidadeFuncionalidade($entidadeFuncionalidadeTO);
            }
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
    }

    /**
     * Metodo que salva o endereco padrao da entidade
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEnderecoPadrao(EntidadeEnderecoTO $eeTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$eeTO->getId_entidade()) {
                $eeTO->setId_entidade($eeTO->getSessao()->id_entidade);
            }
            $where = 'id_entidade = ' . $eeTO->getId_entidade();
            $toEdicao = new EntidadeEnderecoTO();
            $toEdicao->setBl_padrao(false);
            $this->dao->editarEntidadeEndereco($toEdicao, $where);
            $where = 'id_entidadeendereco = ' . $eeTO->getId_entidadeendereco();
            $eeTO->setBl_padrao(true);
            $this->dao->editarEntidadeEndereco($eeTO, $where);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('EndereÃ§o de CorrespondÃªncia Salvo com Sucesso!', Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar EndereÃ§o de CorrespondÃªncia!', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que salva a conta padrao da entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContaEntidadePadrao(ContaEntidadeTO $ceTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $where = 'id_entidade = ' . $ceTO->getId_entidade();
            $toEdicao = new ContaEntidadeTO();
            $toEdicao->setBl_padrao(false);
            $this->dao->editarContaEntidade($toEdicao, $where);
            $where = 'id_contaentidade = ' . $ceTO->getId_contaentidade();
            $ceTO->setBl_padrao(true);
            $this->dao->editarContaEntidade($ceTO, $where);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Conta da InstituiÃ§Ã£o PadrÃ£o Salva com Sucesso!', Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Conta PadrÃ£o!', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Salva o Email da InstituiÃ§Ã£o
     * @param EntidadeEmailTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEmail(EntidadeEmailTO $eeTO)
    {
        if ($eeTO->getId_entidadeemail()) {
            return $this->editarEntidadeEmail($eeTO);
        }
        return $this->cadastrarEntidadeEmail($eeTO);
    }

    /**
     * Metodo que salva o endereco de uma entidade (NÃ£o retorna TO) e vincula a entidade
     * @param EnderecoTO $eTO
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEndereco(EnderecoTO $eTO, EntidadeEnderecoTO $eeTO)
    {
        try {
            $this->dao->beginTransaction();
            if ($eTO->getId_endereco()) {
                $this->dao->editarEndereco($eTO);
            } else {
                $eTO->setBl_ativo(1);
                $id_endereco = $this->dao->cadastrarEndereco($eTO);
                $eTO->setId_endereco($id_endereco);
                $entidadeEnderecoTO = new EntidadeEnderecoTO();
                $entidadeEnderecoTO->setId_endereco($eTO->getId_endereco());
                $entidadeEnderecoTO->setBl_padrao(false);
                $entidadeEnderecoTO->setId_entidade($eeTO->getId_entidade());
                $id_entidadeendereco = $this->dao->cadastrarEntidadeEndereco($entidadeEnderecoTO);
                $entidadeEnderecoTO->setId_entidadeendereco($id_entidadeendereco);
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('EndereÃ§o Salvo com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar EndereÃ§o', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra o Email da InstituiÃ§Ã£o
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidadeEmail(EntidadeEmailTO $eeTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$eeTO->getId_entidade()) {
                $eeTO->setId_entidade($eeTO->getSessao()->id_entidade);
            }
            $verificador = $this->dao->retornarEntidadeEmail(new EntidadeEmailTO(), 'id_entidade = ' . $eeTO->getId_entidade())->toArray();
            $id_entidadeemail = $this->dao->cadastrarEntidadeEmail($eeTO);
            $eeTO->setId_entidadeemail($id_entidadeemail);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($eeTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Email', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Entidade
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidade(EntidadeTO $eTO)
    {
        try {
            $this->dao->beginTransaction();
            $dao = new EntidadeDAO();
            $eTO->setId_entidadecadastro($eTO->getSessao()->id_entidade);
            $eTO->setNu_inscricaoestadual(($this->somenteNumero($eTO->getNu_inscricaoestadual())));
            $eTO->setSt_cnpj($this->somenteNumero($eTO->getSt_cnpj()));
            $eTO->setId_usuariocadastro(($eTO->getSessao()->id_usuario ? $eTO->getSessao()->id_usuario : $eTO->getId_usuariocadastro()));
            $eTO->setSt_nomeentidade($eTO->getSt_nomeentidade());
            $eTO->setSt_razaosocial($eTO->getSt_razaosocial());
            $eTO->setSt_urlsite($eTO->getSt_urlsite());
            $eTO->setId_esquemaconfiguracao(null);
            $eTO->bl_ativo = 1;
            $eTO->setId_situacao(2);
            $id_entidade = $dao->cadastrarEntidade($eTO);
            $eTO->setId_entidade($id_entidade);
            //$this->mensageiro->addMensagem($eTO);
            //$this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($eTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Instituição!' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * @atualizacao Alteração para método salvar via Doctrine para gerar o log das informações.
     *
     *
     *
     * Procedimento de salvar dados da entidade para cadastro de entidade resumido.
     * @param EntidadeTO $entidadeTO
     * @param ContatosTelefoneTO $contatosTelefoneTO
     * @param ContatosTelefoneEntidadeTO $contatosTelefoneEntidadeTO
     * @param EntidadeEnderecoTO $entidadeEnderecoTO
     * @param EnderecoTO $enderecoTO
     * @param ContaEntidadeTO $contaEntidadeTO
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function procedimentoSalvarEntidadeResumido(EntidadeTO $entidadeTO, ContatosTelefoneTO $contatosTelefoneTO = null, ContatosTelefoneEntidadeTO $contatosTelefoneEntidadeTO = null, EntidadeEnderecoTO $entidadeEnderecoTO = null, EnderecoTO $enderecoTO = null, ContaEntidadeTO $contaEntidadeTO = null)
    {

        try {
            $this->dao->beginTransaction();
            if ($entidadeTO->getId_entidade()) {
                $returnEntidade = $this->editarEntidade($entidadeTO);
            } else {
                $returnEntidade = $this->cadastrarEntidade($entidadeTO);
            }

            if ($returnEntidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $obj = $returnEntidade->getFirstMensagem();
                if ($contatosTelefoneTO != null && $contatosTelefoneEntidadeTO != null) {
                    $contatosTelefoneEntidadeTO->setId_entidade($obj->id_entidade);
                    $contatosTelefoneEntidadeTO->setBl_padrao(true);

                    $this->procedimentoSalvarContatosTelefoneEntidade($entidadeTO, $contatosTelefoneTO, $contatosTelefoneEntidadeTO);
                }

                if ($enderecoTO instanceof EnderecoTO && $enderecoTO->getSt_cep() != "") {
                    $entidadeEnderecoTO->setId_entidade($obj->id_entidade);
                    $entidadeEnderecoTO->setBl_padrao(true);

                    $this->procedimentoSalvarEnderecoEntidade($obj, $entidadeEnderecoTO, $enderecoTO);
                }

                if ($contaEntidadeTO != null) {
                    $contaEntidadeTO->setId_entidade($obj->id_entidade);
                    $contaEntidadeTO->setBl_padrao(true);

                    $this->procedimentoSalvarContaEntidade($contaEntidadeTO);
                }
            }

            $this->dao->commit();
            return $this->mensageiro->setMensageiro(array($entidadeTO, $contatosTelefoneTO, $contatosTelefoneEntidadeTO, $entidadeEnderecoTO, $enderecoTO, $contaEntidadeTO));
        } catch (Zend_Exception $e) {
            Zend_Debug::dump($e);
            exit;
            $this->dao->rollBack();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Procedimento que salva telefone de entidade.
     * @param EntidadeTO $entTO
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefoneEntidadeTO $cteTO
     * @return array
     */
    public function procedimentoSalvarContatosTelefoneEntidade(EntidadeTO $entTO, ContatosTelefoneTO $ctTO, ContatosTelefoneEntidadeTO $cteTO)
    {
        if (!$ctTO->getId_telefone()) {
            $mensageiro = $this->cadastrarTelefoneEntidade($entTO, $ctTO, $cteTO);
        } else {
            $mensageiro = $this->editarTelefoneEntidade($ctTO);
        }

        return array($ctTO, $cteTO);
    }

    /**
     * Procedimento que salva endereço de entidade.
     * @param EntidadeTO $entTO
     * @param EntidadeEnderecoTO $eeTO
     * @param EnderecoTO $endTO
     * @return array
     */
    public function procedimentoSalvarEnderecoEntidade(EntidadeTO $entTO, EntidadeEnderecoTO $eeTO, EnderecoTO $endTO)
    {
        if (!$endTO->getId_endereco()) {
            $mensageiro = $this->cadastrarEnderecoEntidade($entTO, $endTO, $eeTO);
        } else {
            $mensageiro = $this->editarEnderecoEntidade($endTO, $eeTO);
        }

//        Zend_Debug::dump($mensageiro);
//        die;
        if ($mensageiro->getType() == Ead1_IMensageiro::SUCESSO)
            return array($endTO, $eeTO);
    }

    /**
     * Procedimento que salva conta de entidade.
     * @param ContaEntidadeTO $ceTO
     * @return ContaEntidadeTO
     */
    public function procedimentoSalvarContaEntidade(ContaEntidadeTO $ceTO)
    {
        if (!$ceTO->getId_contaentidade()) {
            $mensageiro = $this->cadastrarContaEntidade($ceTO);
        } else {
            $mensageiro = $this->editarContaEntidade($ceTO);
        }
        return $mensageiro;
    }

    /**
     * Retorna dados gerais da entidade.
     * @param EntidadeTO $entidadeTO
     * @return Ead1_Mensageiro
     */
    public function retornarDadosEntidade(EntidadeTO $entidadeTO)
    {

        $pessoaBO = new PessoaBO();
        $entidadeTO->setSt_cnpj($pessoaBO->somenteNumero($entidadeTO->getSt_cnpj()));
        $dadosEntidade = $this->retornaEntidade($entidadeTO);
        if ($dadosEntidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensagem = $dadosEntidade->getMensagem();
            $entidadeTO = $mensagem[0];
            $mensageiro = new Ead1_Mensageiro(array($entidadeTO));
            $dadosConta = $this->retornaContaEntidade($entidadeTO);
            if ($dadosConta) {
                $mensageiro->addMensagem($dadosConta->getFirstMensagem());
            } else {
                $mensageiro->addMensagem("Sem Dados de Conta!");
            }

            $dadosTelefone = $this->retornaTelefoneEntidade($entidadeTO);
            if ($dadosTelefone) {
                $mensageiro->addMensagem($dadosTelefone->getFirstMensagem());
            } else {
                $mensageiro->addMensagem("Sem Dados de Telefone!");
            }

            $dadosEndereco = $this->retornaEnderecoEntidade($entidadeTO);
            if ($dadosEndereco) {
                $mensageiro->addMensagem($dadosEndereco->getFirstMensagem());
            } else {
                $mensageiro->addMensagem("Sem Dados de Endereço!");
            }
            return $mensageiro;
        } else {
            return $this->mensageiro->setMensageiro($entidadeTO);
        }
    }

    /**
     * Metodo que Cadastra EndereÃ§o da Entidade
     * @param $entTO
     * @param $endTO
     * @param $eeTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEnderecoEntidade(EntidadeTO $entTO, EnderecoTO $endTO, EntidadeEnderecoTO $eeTO)
    {
        $dao = new EntidadeDAO();
//		$teeTO = new TipoEnderecoEntidadeTO();
        /* if(!$this->validaCep($endTO->getSt_cep())){
          return $this->mensageiro->setMensageiro('Cep Invalido!',Ead1_IMensageiro::AVISO);
          } */
//		$teeTO->setId_entidade($entTO->getId_entidade());
//		$teeTO->setId_tipoendereco($endTO->getId_tipoendereco());

        $endTO->getSt_cep($this->limpaCep($endTO->getSt_cep()));
        $eeTO->getId_entidade() == ''? $eeTO->setId_entidade($entTO->getId_entidade()):'';
        $dao->beginTransaction();
        try {
            $id_endereco = $dao->cadastrarEndereco($endTO);
            if (!$id_endereco) {
                throw new Exception('Erro ao Cadastrar Endereço da Entidade!');
            }
            $endTO->setId_endereco($id_endereco);
            $eeTO->setId_endereco($id_endereco);
            if (!$dao->cadastrarEntidadeEndereco($eeTO)) {
                throw new Exception('Erro ao Vincular o Endereço a Entidade!');
            }
            $dao->commit();
            $this->mensageiro->addMensagem($endTO);
            $this->mensageiro->addMensagem($eeTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Endereço da Entidade!' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage() . ' SQL: ' . $dao->excecao);
        }
    }

    /**
     * Metodo que cadastra Telefones da Entidade
     * @param $entTO
     * @param $ctTO
     * @param $cteTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTelefoneEntidade(EntidadeTO $entTO, ContatosTelefoneTO $ctTO, ContatosTelefoneEntidadeTO $cteTO = null)
    {

        try {
            $dao = new EntidadeDAO();

            $dao->beginTransaction();

            if ($cteTO == null) {
                $cteTO = new ContatosTelefoneEntidadeTO();
            }
            $ctTO->setNu_telefone($this->somenteNumero($ctTO->getNu_telefone()));

            if (!$this->validaNumero($ctTO->getNu_telefone())) {
                throw new InvalidArgumentException('Número de Telefone ('.$ctTO->getNu_telefone().') Inválido!');
            }

            $id_telefone = $dao->cadastrarTelefoneEntidade($entTO, $ctTO, $cteTO);
            if (!$id_telefone) {
                throw new Exception($dao->excecao);
            }

            $ctTO->setId_telefone($id_telefone);
            $cteTO->setId_entidade($entTO->getId_entidade());
            $cteTO->setId_telefone($id_telefone);
            $this->mensageiro->addMensagem($ctTO);
            $this->mensageiro->addMensagem($cteTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra contato da entidade
     * @param $ceTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $dao = new EntidadeDAO();
        if (!$ceTO->getId_entidade()) {
            $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
        }
        $id_contatoentidade = $dao->cadastrarContatoEntidade($ceTO);
        if (!$id_contatoentidade) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Contato!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        $ceTO->setId_contatoentidade($id_contatoentidade);
        return $this->mensageiro->setMensageiro($ceTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra Conta Bancaria da Entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContaEntidade(ContaEntidadeTO $ceTO)
    {
        try {
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $id_contaentidade = $this->dao->cadastrarContaEntidade($ceTO);
            $ceTO->setId_contaentidade($id_contaentidade);

            return $this->mensageiro->setMensageiro($ceTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conta Bancaria!' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra relaÃ§Ã£o de entidade
     * @param EntidadeRelacaoTO $erTO
     * @param array (ProjetoPedagogicoTO $projeto)
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidadeRelacao(EntidadeRelacaoTO $erTO, $arrProjetos)
    {
        try {
            $erTO->setId_entidadepai($erTO->getSessao()->id_entidade);
            if (!$this->dao->cadastrarEntidadeRelacao($erTO)) {
                throw new DomainException('Erro ao cadastrar relaÃ§Ã£o de entidade.');
            }
            $projetoEntidadeTO = new ProjetoEntidadeTO();
            $projetoEntidadeTO->setDt_inicio(new Zend_Date());
            $projetoEntidadeTO->setId_situacao(51);
            $projetoEntidadeTO->setId_usuariocadastro($projetoEntidadeTO->getSessao()->id_usuario);
            $projetoEntidadeTO->setBl_ativo(true);
            foreach ($arrProjetos as $projeto) {
                $projetoEntidadeTO->setId_entidade($erTO->getId_entidade());
                $projetoEntidadeTO->setId_projetopedagogico($projeto->getId_projetopedagogico());
                if (!$this->dao->cadastrarProjetoEntidade($projetoEntidadeTO)) {
                    throw new DomainException('Erro ao cadastrar Projeto na InstituiÃ§Ã£o');
                }
            }
            return $this->mensageiro->setMensageiro($erTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar RelaÃ§Ã£o de entidade!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita o Email da InstituiÃ§Ã£o
     * @param EntidadeEmailTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function editarEntidadeEmail(EntidadeEmailTO $eeTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$eeTO->getId_entidade()) {
                $eeTO->setId_entidade($eeTO->getSessao()->id_entidade);
            }
            $orm = new EntidadeEmailORM();
            $consultaEntidadeEmailTO = $orm->consulta(new EntidadeEmailTO(array('id_entidadeemail' => $eeTO->getId_entidadeemail())), true, true);
            if (!$consultaEntidadeEmailTO) {
                THROW new Zend_Exception('NÃ£o foi encontrado o email para ediÃ§Ã£o!');
            }
            if ($consultaEntidadeEmailTO->getBl_padrao() && !$eeTO->getBl_ativo()) {
                $eeTO->setBl_padrao(false);
                $this->dao->editarEntidadeEmail($eeTO);
                $entidadeEmailTOAtivo = $orm->consulta(new EntidadeEmailTO(array('id_entidade' => $eeTO->getId_entidade(), 'bl_ativo' => 1)), false, true);
                if ($entidadeEmailTOAtivo) {
                    $entidadeEmailTOAtivo->setBl_padrao(1);
                    $this->dao->editarEntidadeEmail($entidadeEmailTOAtivo);
                }
            } else {
                $eeTO->setBl_padrao(null);
                $this->dao->editarEntidadeEmail($eeTO);
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Email Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Editar Email!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita dados da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     * @throw Zend_Exception
     */
    public function editarEntidade(EntidadeTO $entTO)
    {
        try {
            $this->dao->editarEntidade($entTO);
            return $this->mensageiro->setMensageiro($entTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que edita dados da relaÃ§Ã£o de Entidade
     * @param EntidadeRelacaoTO $erTO
     * @param array (ProjetoPedagogicoTO $projeto)
     * @return Ead1_Mensageiro
     */
    public function editarEntidadeRelacao(EntidadeRelacaoTO $erTO, $arrProjetos)
    {
        try {
            if (!$this->dao->editarEntidadeRelacao($erTO)) {
                throw new Zend_Exception('Erro ao editar relação de entidade.');
            }
            $projetoEntidadeTO = new ProjetoEntidadeTO();
            $projetoEntidadeTO->setId_entidade($erTO->getId_entidade());
            $where = 'id_projetoentidade in ( select id_projetoentidade from tb_projetoentidade as pe, tb_entidaderelacao as e, tb_projetopedagogico as pr
									where pe.id_entidade = e.id_entidade
									and e.id_entidade = ' . $projetoEntidadeTO->getId_entidade() . '
									and e.id_entidadepai = ' . $erTO->getId_entidadepai() . '
									and pr.id_entidadecadastro = e.id_entidadepai
									and pe.id_projetopedagogico = pr.id_projetopedagogico ) ';
            if (!$this->dao->deletarProjetoEntidade($projetoEntidadeTO, $where)) {
                throw new Zend_Exception('Erro ao deletar cursos antigos de entidade.');
            }
            $projetoEntidadeTO->setDt_inicio(new Zend_Date());
            $projetoEntidadeTO->setId_situacao(51);
            $projetoEntidadeTO->setId_usuariocadastro($projetoEntidadeTO->getSessao()->id_usuario);
            $projetoEntidadeTO->setBl_ativo(true);
            foreach ($arrProjetos as $projeto) {
                $projetoEntidadeTO->setId_projetopedagogico($projeto->getId_projetopedagogico());
                if (!$this->dao->cadastrarProjetoEntidade($projetoEntidadeTO)) {
                    throw new Zend_Exception('Erro ao cadastrar Projeto na Instituição. Erro: ' . $this->dao->excecao);
                }
            }
            return $this->mensageiro->setMensageiro($erTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Relação de entidade!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita EndereÃ§o da Entidade
     * @param EnderecoTO $endTO
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function editarEnderecoEntidade(EnderecoTO $endTO, EntidadeEnderecoTO $eeTO)
    {
        $dao = new EntidadeDAO();
        /* if(!$this->validaCep($endTO->getSt_cep())){
          return $this->mensageiro->setMensageiro('Cep Invalido!',Ead1_IMensageiro::AVISO);
          } */
        $endTO->getSt_cep($this->limpaCep($endTO->getSt_cep()));
        if (!$dao->editarEnderecoEntidade($endTO, $eeTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Endereço!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Endereço Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita telefone da entidade
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefoneEntidadeTO $cteTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefoneEntidade(ContatosTelefoneTO $ctTO)
    {
        $dao = new EntidadeDAO();
        $ctTO->setNu_telefone($this->somenteNumero($ctTO->getNu_telefone()));
        if (!$this->validaNumero($ctTO->getNu_telefone())) {
            return $this->mensageiro->setMensageiro('Número de Telefone ('.$ctTO->getNu_telefone().') Inválido!', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->editarContatoTelefone($ctTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Telefone', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Telefone Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * MÃ©todo que edita relaÃ§Ã£o de telefone com entidade.
     * @param ContatosTelefoneEntidadeTO $cteTO
     * @return Ead1_Mensageiro
     */
    public function editarContatosTelefoneEntidade(ContatosTelefoneEntidadeTO $cteTO)
    {
        $dao = new EntidadeDAO();
        if ($cteTO->getBl_padrao()) {
            $contatosTelefoneEntidadeTO = new ContatosTelefoneEntidadeTO();
            $contatosTelefoneEntidadeTO->setId_entidade($cteTO->getId_entidade());
            $contatosTelefoneEntidadeTO->setBl_padrao(0);
            if (!$dao->editarContatosTelefoneEntidade($contatosTelefoneEntidadeTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Editar Telefone', Ead1_IMensageiro::ERRO, $dao->excecao);
            }
        }
        $where = 'id_entidade = ' . $cteTO->getId_entidade();
        if ($cteTO->getId_telefone()) {
            $where .= ' and id_telefone = ' . $cteTO->getId_telefone();
        }
        if (!$dao->editarContatosTelefoneEntidade($cteTO, $where)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Telefone PadrÃ£o', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Telefone padrÃ£o salvo', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita contato da entidade
     * @param $ceTO
     * @return Ead1_Mensageiro
     */
    public function editarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        $dao = new EntidadeDAO();
        if (!$dao->editarContatoEntidade($ceTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Contato!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Contato Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita Telefone do Contato da Entidade
     * @param ContatosTelefoneEntidadeContatoTO $ctecTO
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefoneContato(ContatosTelefoneEntidadeContatoTO $ctecTO, ContatosTelefoneTO $ctTO)
    {
        $dao = new EntidadeDAO();
        if (!$this->validaNumero($ctTO->getNu_telefone())) {
            return $this->mensageiro->setMensageiro('Número de Telefone ('.$ctTO->getNu_telefone().') Inválido!', Ead1_IMensageiro::AVISO);
        }
        $ctTO->setNu_telefone($this->somenteNumero($ctTO->getNu_telefone()));
        if (!$dao->editarTelefoneContato($ctecTO, $ctTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Telefone!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Telefone Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita Conta Bancaria da Entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function editarContaEntidade(ContaEntidadeTO $ceTO)
    {
        try {
            $dao = new EntidadeDAO();
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }

            $result = $dao->editarContaEntidade($ceTO);

            return $this->mensageiro->setMensageiro($ceTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Conta Bancaria!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Entidade
     * @param EntidadeTO $entTO
     * @param Mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornaEntidade(EntidadeTO $entTO, $where = null)
    {
        try {
            $entidade = $this->dao->retornaEntidade($entTO, $where)->toArray();
            if (empty($entidade)) {
                THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($entidade, new EntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Instituições.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a Classe da Entidade
     * @param EntidadeRelacaoTO $erTO
     * @param boolean $entidadePaiSessao
     * @return Ead1_Mensageiro
     */
    public function retornaClasseDaEntidade(EntidadeRelacaoTO $erTO, $entidadePaiSessao = false)
    {
        $dao = new EntidadeDAO();
        if ($entidadePaiSessao) {
            $erTO->setId_entidadepai($erTO->getSessao()->id_entidade);
        }
        $classeEntidade = $dao->retornaClasseEntidade($erTO);
        if (is_object($classeEntidade)) {
            $classeEntidade = $classeEntidade->toArray();
        } else {
            unset($classeEntidade);
        }
        if (!$classeEntidade) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Classe Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($classeEntidade, new VwEntidadeClasseTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna classes de Entidade
     * @param EntidadeClasseTO $ecTO
     * @return Ead1_Mensageiro
     */
    public function retornaClasses(EntidadeClasseTO $ecTO)
    {
        $dao = new EntidadeDAO();
        $classes = $dao->retornaClasses($ecTO);
        if (is_object($classes)) {
            $classes = $classes->toArray();
        } else {
            unset($classes);
        }
        if (!$classes) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($classes, new EntidadeClasseTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna Endereço da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaEnderecoEntidade(EntidadeTO $entTO)
    {
        $dao = new EntidadeDAO();
        $endereco = $dao->retornaEnderecoEntidade($entTO);
        if (is_object($endereco)) {
            $endereco = $endereco->toArray();
        } else {
            unset($endereco);
        }
        if (!$endereco) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($endereco, new EnderecoEntidadeEnderecoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna Telefones da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaTelefoneEntidade(EntidadeTO $entTO)
    {
        $dao = new EntidadeDAO();
        $telefones = $dao->retornaTelefoneEntidade($entTO);
        if (is_object($telefones)) {
            $telefones = $telefones->toArray();
        } else {
            unset($telefones);
        }
        if (!$telefones) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($telefones, new VwEntidadeTelefoneTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna todas as funcionalidades da entidade independente do bl_ativo ou visivel, retorna pelo id da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @return Ead1_Mensageiro
     */
    public function retornarTodasEntidadeFuncionalidades(EntidadeFuncionalidadeTO $efTO)
    {
        try {
            $dao = new EntidadeDAO();
            if (!$efTO->getId_entidade()) {
                $efTO->setId_entidade($efTO->getSessao()->id_entidade);
            }
            $where = 'id_entidade = ' . $efTO->getId_entidade();
            $dados = $dao->retornarEntidadeFuncionalidade(new EntidadeFuncionalidadeTO(), $where)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EntidadeFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Funcionalidades da InstituiÃ§Ã£o!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as funcionalidades da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeFuncionalidade(EntidadeFuncionalidadeTO $efTO)
    {
        try {
            $dao = new EntidadeDAO();
            if (!$efTO->getId_entidade()) {
                $efTO->setId_entidade($efTO->getSessao()->id_entidade);
            }
            $dados = $dao->retornarEntidadeFuncionalidade($efTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EntidadeFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Funcionalidades da InstituiÃ§Ã£o!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Retorna view de Telefones da Entidade
     * @param VwEntidadeTelefoneTO $vwetTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeTelefone(VwEntidadeTelefoneTO $vwetTO)
    {
        $dao = new EntidadeDAO();
        $telefones = $dao->retornarVwEntidadeTelefone($vwetTO);
        if (is_object($telefones)) {
            $telefones = $telefones->toArray();
        } else {
            unset($telefones);
        }
        if (!$telefones) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de telefone Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($telefones, new VwEntidadeTelefoneTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Meotod que retorna Contatos da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaContatosEntidade(EntidadeTO $entTO)
    {
        $dao = new EntidadeDAO();
        $ceTO = new VwEntidadeContatoTO();
        $ceTO->setId_entidadecontato($entTO->getId_entidade());
        $contatos = $dao->retornaContatoEntidade($ceTO);
        if (is_object($contatos)) {
            $contatos = $contatos->toArray();
        } else {
            unset($contatos);
        }
        if (!$contatos) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($contatos, new VwEntidadeContatoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna Telefones do Contato da Entidade
     * @param EntidadeTO $entTO
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function retornaTelefoneContatoEntidade(EntidadeTO $entTO, ContatoEntidadeTO $ceTO)
    {
        $dao = new EntidadeDAO();
        $telefonesContato = $dao->retornaTelefoneContatoEntidade($entTO, $ceTO);
        if (is_object($telefonesContato)) {
            $telefonesContato = $telefonesContato->toArray();
        } else {
            unset($telefonesContato);
        }
        if (!$telefonesContato) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($telefonesContato, new VwEntidadeContatoTelefoneTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna projetos pedagÃ³gicos da Entidade
     * @param ProjetoEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoEntidade(ProjetoEntidadeTO $peTO)
    {
        try {
            $dao = new EntidadeDAO();
            $retorno = array();
            $projetosEntidade = $dao->retornarProjetoEntidade($peTO)->toArray();
            foreach ($projetosEntidade as $index => $projetoEntidade) {
                $projetoPedagogicoTO = new ProjetoPedagogicoTO();
                $projetoPedagogicoTO->setId_projetopedagogico($projetoEntidade['id_projetopedagogico']);
                $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
                $projeto = $projetoPedagogicoDAO->retornarProjetoPedagogico($projetoPedagogicoTO)->toArray();
                if (empty($projeto)) {
                    THROW new Zend_Exception('NÃ£o retornou o projeto certo');
                }
                $projeto = Ead1_TO_Dinamico::encapsularTo($projeto, new ProjetoPedagogicoTO(), true);
                $retorno[$index] = $projeto;
            }
            return $this->mensageiro->setMensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Projetos!', Ead1_IMensageiro::AVISO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Contas Bancarias da Entidade
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornaContaEntidade(EntidadeTO $eTO)
    {
        $dao = new EntidadeDAO();
        $contasBancarias = $dao->retornaContaEntidade($eTO);
        if (is_object($contasBancarias)) {
            $contasBancarias = $contasBancarias->toArray();
        } else {
            unset($contasBancarias);
        }
        if (!$contasBancarias) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($contasBancarias, new VwEntidadeContaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * MÃ©todo que retorna as entidades filhas de determinada classe ou de todas as classes
     * @param int $idEntidade
     * @param int $idEntidadeClasse
     */
    public function retornaEntidadesFilhas($idEntidade, $idEntidadeClasse = null, $idPerfil = null)
    {
        $dao = new EntidadeDAO();
        $entidades = $dao->retornaEntidadesFilhas($idEntidade, $idEntidadeClasse, $idPerfil);
        if ($entidades) {
            if (empty($entidades)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                foreach ($entidades as $entidade) {
                    if ($entidade['id_entidade'] != $entidade['id_entidadepai']) {
                        $entidades = $dao->retornaEntidadesFilhas($idEntidade, $idEntidadeClasse);
                    }
                }
                $retorno = $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($entidades, new VwEntidadeBasicoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } else {
            $retorno = $this->mensageiro->setMensageiro('Erro ao Retornar Entidades Filhas!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $retorno;
    }

    /**
     * MÃ©todo que organiza as entidades recursivas em um Ãºnico array
     * @param int $idEntidade
     * @param mixed $dados
     */
    private function entidadesFilhasRecursivas($idEntidade, $idEntidadeClasse = null, $idPerfil = null)
    {
        $dao = new EntidadeDAO();
        $dados = $dao->retornaEntidadesFilhas($idEntidade, $idEntidadeClasse, $idPerfil);
        if ($dados) {
            foreach ($dados as $entidade) {
                if ($idEntidade != $entidade->id_entidade) {
                    $this->entidadesFilhas[] = $entidade->toArray();
                    $this->entidadesFilhasRecursivas($entidade['id_entidade']);
                }
            }
        }
        return $this->entidadesFilhas;
    }

    /**
     * Retorna a entidade e todas as entidades filhas da entidade da sessÃ£o atual
     * @return Ead1_Mensageiro
     */
    public function retornaEntidadeRecursivo()
    {

        try {

            $entidadeTO = new EntidadeTO();
            $idEntidadePai = $entidadeTO->getSessao()->id_entidade;

            if (empty($idEntidadePai)) {
                throw new DomainException('Informe o cÃ³digo da entidade pai a ser buscada');
            }

            $entidadeTO->setId_entidade($idEntidadePai);
            $entidadeDAO = new EntidadeDAO();
            $entidadePaiRow = $entidadeDAO->retornaEntidade($entidadeTO)->current();
            $arEntidadesFilhas = $entidadeDAO->retornaEntidadesFilhas($entidadePaiRow->id_entidade);
            $arEntidades[] = $entidadePaiRow->toArray();
            $arEntidades = array_merge($arEntidades, $arEntidadesFilhas);

            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($arEntidades, new EntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function retornarArvoreEntidade(VwEntidadeRecursivaTO $vwerTO)
    {
        try {
            $dao = new EntidadeDAO();
            if (!$vwerTO->getId_entidade()) {
                $vwerTO->setId_entidade($vwerTO->getSessao()->id_entidade);
            }
            $where = " cadeiaentidades like '%|" . $vwerTO->getId_entidade() . "|%'";
            $dados = $dao->retornarVwEntidadeRecursiva($vwerTO, $where, 'nu_nivelhierarquia ASC');
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrEntidades = Ead1_TO_Dinamico::encapsularTo($dados, new EntidadeTO());
            $entidadesPai = array();
            $arrAux = array();
            $primeiroNivel = true;
            //Foreach para ajeitar as entidades em um formato que a tree do flex entenda.
            foreach ($arrEntidades as $entidadeTO) {
                if ($primeiroNivel) {
                    $entidadesPai[$entidadeTO->getId_entidade()]['to'] = $entidadeTO;
                    $entidadesPai[$entidadeTO->getId_entidade()]['label'] = $entidadeTO->getSt_nomeentidade();
                    $entidadesPai[$entidadeTO->getId_entidade()]['children'] = null;
                    $entidadesPai[$entidadeTO->getId_entidade()]['tipo'] = 'Entidade';
                    $primeiroNivel = false;
                    continue;
                }
                $arrAux[$entidadeTO->getId_entidade()] = $entidadeTO;
            }
            if (!empty($arrAux)) {
                foreach ($entidadesPai as $index => $pai) {
                    $entidadesPai[$index]['children'] = $this->entidadeRecursiva($pai['to'], $arrAux);
                }
            }
            return $this->mensageiro->setMensageiro($entidadesPai, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Entidades!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Retorna entidade recursivamente
     * @param EntidadeTO $eTO
     * @return object $dados;
     */
    private function entidadeRecursiva(EntidadeTO $eTO, $arrAux)
    {
        $arrRecurssivo = array();
        if ($arrAux) {
            foreach ($arrAux as $index => $entidadeTO) {
                if ($entidadeTO->getId_entidadepai() == $eTO->getId_entidade()) {
                    $arrRecurssivo[$index]['to'] = $entidadeTO;
                    $arrRecurssivo[$index]['label'] = $entidadeTO->getSt_nomeentidade();
                    $arrRecurssivo[$index]['tipo'] = 'Entidade';
                    $arrRecurssivo[$index]['children'] = $this->entidadeRecursiva($entidadeTO, $arrAux);
                }
            }
        }
        return (empty($arrRecurssivo) ? null : $arrRecurssivo);
    }

    /**
     * Metodo que retorna a view de endereÃ§o da entidade
     * @param VwEntidadeEnderecoTO $vwEeTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeEndereco(VwEntidadeEnderecoTO $vwEeTO)
    {
        try {
            $dao = new EntidadeDAO();
            $dados = $dao->retornarVwEntidadeEndereco($vwEeTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEntidadeEnderecoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar EndereÃ§o da InstituiÃ§Ã£o!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os emails da instituiÃ§Ã£o
     * @param EntidadeEmailTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeEmail(EntidadeEmailTO $eeTO)
    {
        try {
            $dao = new EntidadeDAO();
            $dados = $dao->retornarEntidadeEmail($eeTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EntidadeEmailTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Emails da InstituiÃ§Ã£o!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as Entidades da Matrícula
     * @param MatriculaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadesMatricula(MatriculaTO $to)
    {
        try {
            if (!$to->getId_matricula()) {
                THROW new Zend_Exception("Não foi informado o Id da Matricula");
            }
            $matriculaBO = new MatriculaBO();
            $to = $matriculaBO->retornarMatricula($to)->subtractMensageiro()->getFirstMensagem();
            $where = "id_entidade in ( {$to->getId_entidadeatendimento()},{$to->getId_entidadematricula()}, {$to->getId_entidadematriz()} )";
            $entidades = $this->retornaEntidade(new EntidadeTO(), $where)->subtractMensageiro()->getMensagem();
            $this->mensageiro->setMensageiro($entidades, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Instituições.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que deleta a conta da entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function deletarContaEntidade(ContaEntidadeTO $ceTO)
    {
        try {
            $dao = new EntidadeDAO();
            if ($ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $dao->deletarContaEntidade($ceTO);
            return $this->mensageiro->setMensageiro('Conta Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Conta!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que deleta telefone de entidade.
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function deletarContatoTelefoneEntidade(ContatosTelefoneTO $ctTO)
    {
        try {
            $cteTO = new ContatosTelefoneEntidadeTO();
            $cteTO->setId_telefone($ctTO->getId_telefone());
            $dao = new EntidadeDAO();
            if (!$dao->deletarContatoTelefoneEntidade($cteTO)) {
                throw new DomainException('Erro ao deletar relaÃ§Ã£o de pessoa jurÃ­dica e telefone');
            }
            if (!$dao->deletarContatoTelefone($ctTO)) {
                throw new DomainException('Erro ao deletar telefone');
            }
            return $this->mensageiro->setMensageiro('Telefone deletado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao deletar telefone!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que deleta contato de entidade.
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function deletarContatoEntidade(ContatoEntidadeTO $ceTO)
    {
        try {
            $dao = new EntidadeDAO();
            if (!$dao->deletarContatoEntidade($ceTO)) {
                throw new DomainException('Erro ao deletar contato de pessoa jurÃ­dica');
            }
            return $this->mensageiro->setMensageiro('Contato deletado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao deletar contato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna os dados de uma EntidadeInteracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @return Ead1_Mensageiro
     */
    public function retornaEntidadeIntegracao(EntidadeIntegracaoTO $entidadeIntegracao)
    {
        try {

            $dao = new EntidadeDAO();
            if (!$entidadeIntegracao->getId_entidade()) {
                $entidadeIntegracao->setId_entidade($entidadeIntegracao->getSessao()->id_entidade);
            }

            $ei = $entidadeIntegracao->encapsularTo($dao->retornaEntidadeIntegracao($entidadeIntegracao), $entidadeIntegracao);
            if (!$ei)
                throw new Zend_Exception("Nenhuma integração encontrada");

            return new Ead1_Mensageiro($ei, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao retornar Integração da Organização: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
//            return new Ead1_Mensageiro('Erro ao retornar Integração da ' . Ead1_LabelFuncionalidade::getLabel(3).': '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que decide se cadastra ou atualiza uma EntidadeInteracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @return Ead1_Mensageiro
     */
    public function salvaEntidadeIntegracao(EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $dao = new EntidadeDAO();
        if (!$entidadeIntegracao->getId_entidade()) {
            $entidadeIntegracao->setId_entidade($entidadeIntegracao->getSessao()->id_entidade);
        }
        try {
            $dao->beginTransaction();
            if ($entidadeIntegracao->getId_entidadeintegracao())
                $dao->editarEntidadeIntegracao($entidadeIntegracao);
            else
                $entidadeIntegracao->setId_entidadeintegracao($dao->salvarEntidadeIntegracao($entidadeIntegracao));
            $dao->commit();
            return new Ead1_Mensageiro($entidadeIntegracao, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro("Erro ao cadastrar Entidade Interação. " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que salva o arquivo do Manual do Aluno
     * @param $arquivo
     * @return Ead1_Mensageiro
     * @deprecated use \G2\Negocio\ConfiguracaoEntidade::salvarManualDoAluno() instead
     */
    public function salvarManualAluno($arquivo)
    {
        $negocio = new \G2\Negocio\ConfiguracaoEntidade();
        return $negocio->salvarManualDoAluno($arquivo);
    }

    /**
     * Método que retorna a vw de entidades compartilhadas
     * @param VwEntidadeCompartilhaDadosTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeCompartilhaDados(VwEntidadeCompartilhaDadosTO $vw)
    {
        $dao = new EntidadeDAO();
        $retorno = $dao->retornarVwEntidadeCompartilhaDados($vw);
        $mensageiro = new Ead1_Mensageiro();
        if (empty($retorno)) {
            $mensageiro->setMensageiro('Nenhuma entidade encontrada!', Ead1_IMensageiro::AVISO);
        } else
            $mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwEntidadeCompartilhaDadosTO()), Ead1_IMensageiro::SUCESSO);
        return $mensageiro;
    }

    /**
     * Método que salva o arquivo da Tabela de Preços
     * @param ArquivoTO $arquivo
     * @return Ead1_Mensageiro
     * @moved
     */
    public function salvarTabelaPreco($arquivo)
    {
        $negocio = new \G2\Negocio\ConfiguracaoEntidade();
        return $negocio->salvarAnexoTabelaPreco($arquivo);
//        $this->dao->beginTransaction();
//        try {
//            $configEntidadeTO = new ConfiguracaoEntidadeTO();
//            if (!is_dir('upload' . DIRECTORY_SEPARATOR . $configEntidadeTO->getSessao()->id_entidade)) {
//                mkdir("/upload/" . $configEntidadeTO->getSessao()->id_entidade);
//            }
//            exec('sudo chmod upload' . DIRECTORY_SEPARATOR . $configEntidadeTO->getSessao()->id_entidade . DIRECTORY_SEPARATOR . '0777');
//            if ($arquivo instanceof ArquivoTO) {
//                $nome = explode('.', $arquivo->st_nomearquivo);
//                $arquivo->setSt_nomearquivo('tabelapreco_' . $configEntidadeTO->getSessao()->id_entidade);
//                $retorno = $this->upaloadBasico($arquivo);
//            } elseif (!empty($arquivo['name'])) {
//                $renomear = substr($arquivo['name'], -3);
//
//                $arquivo['name'] = 'tabelapreco_' . $configEntidadeTO->getSessao()->id_entidade . '.' . $renomear;
//
//                $adapter = new Zend_File_Transfer_Adapter_Http();
//                $adapter->addFilter('Rename', $arquivo['name']);
//                $adapter->setDestination('upload' . DIRECTORY_SEPARATOR . $configEntidadeTO->getSessao()->id_entidade . DIRECTORY_SEPARATOR);
//
//                if (!$adapter->receive()) {
//                    $messages = $adapter->getMessages();
//                    echo implode("\n", $messages);
//                }
//
//                $upload = new Zend_File_Transfer();
//                $upload->receive();
//            };
//
//            $uploadTO = new UploadTO();
//            $uploadTO->setSt_upload($arquivo instanceof ArquivoTO ? $arquivo->getSt_nomearquivo() . '.' . $arquivo->getSt_extensaoarquivo() : $arquivo['name']);
//
//            $daoUpload = new UploadDAO();
//            $id_tabelapreco = $daoUpload->cadastrarUpload($uploadTO); //verificar se está correto
//
//            if ($id_tabelapreco === false) {
//                return $this->mensageiro->setMensageiro("Erro ao fazer upload da Tabela de Preços! ", Ead1_IMensageiro::ERRO);
//            } else {
//
//                $configEntidadeTO->setId_entidade($configEntidadeTO->getSessao()->id_entidade);
//                $configEntidadeTO->fetch(true, true, true);
//
//                $configEntidadeTO->setId_tabelapreco($id_tabelapreco);
//                $configEntidadeBO = new ConfiguracaoEntidadeBO();
//
//                $mensageiro = $configEntidadeBO->editarConfiguracaoEntidade($configEntidadeTO);
//
//                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
//                    return $this->mensageiro->setMensageiro("Erro ao editar Configuração da Entidade!", Ead1_IMensageiro::ERRO);
//                }
//            }
//
//            $this->dao->commit();
//            $this->mensageiro->setMensageiro($uploadTO->fetch(false, true, true), Ead1_IMensageiro::SUCESSO, $uploadTO);
//        } catch (Exception $e) {
//            $this->dao->rollBack();
//            $this->mensageiro->setMensageiro('Erro no Upload da Tabela de Preços: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
//        }
//
//        return $this->mensageiro;
    }

}

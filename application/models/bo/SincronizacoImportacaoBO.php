<?php
class SincronizacoImportacaoBO extends ImportacaoBO{
	
	
	public function __construct(){
		parent::__construct();
		$municipios = self::returnClassInstance(new Ead1_RO())->municipios(new MunicipioTO());
		$this->arrMunicipioTO = ($municipios->getTipo() != Ead1_IMensageiro::SUCESSO ? array() : $municipios->getMensagem());
		$ufs = self::returnClassInstance(new Ead1_RO())->uf();
		$this->arrUfTO = ($ufs->getTipo() != Ead1_IMensageiro::SUCESSO ? array() : $ufs->getMensagem());
		$pais = self::returnClassInstance(new Ead1_RO())->pais();
		$this->arrPaisTO = ($pais->getTipo() != Ead1_IMensageiro::SUCESSO ? array() : $pais->getMensagem());
		$this->_log = array(); 
	}
	
	/**
	 * Variavel que contem um array com  o log da importacao
	 * @var array
	 */
	protected $_log = array();


	/**
	 * Variavel que contem um array de municipioTO
	 * @var array
	 */
	protected $arrMunicipioTO;
	
	/**
	 * Variavel que contem um array de UfTO
	 * @var array
	 */
	protected $arrUfTO;
	
	/**
	 * Variavel que contem um array de PaisTO
	 * @var array
	 */
	protected $arrPaisTO;
	
	/**
	 * Metodo que retorna o pais pelo nome
	 * @param String $st_nomepais
	 * @return PaisTO | false
	 */
	protected function _retornaPaisPeloNome($st_nomepais){
		if(empty($this->arrPaisTO)){
			return false;
		}
		foreach ($this->arrPaisTO as $paisTO){
			if($this->_substituirCaracteresEspeciais(strtolower($paisTO->getSt_nomepais())) == $this->_substituirCaracteresEspeciais(strtolower($st_nomepais))){
				return $paisTO;
			}
		}
		return false;
	}
	
	/**
	 * Metodo que retorna o municipio pelo nome
	 * @param String $st_nomemunicipio
	 * @return MunicipioTO | false
	 */
	protected function _retornaMunicipioPeloNome($st_nomemunicipio){
		if(empty($this->arrMunicipioTO)){
			return false;
		}
		foreach ($this->arrMunicipioTO as $municipioTO){
			if($this->_substituirCaracteresEspeciais(strtolower($municipioTO->getSt_nomemunicipio())) == $this->_substituirCaracteresEspeciais(strtolower($st_nomemunicipio))){
				return $municipioTO;
			}
		}
		return false;
	}
	
	/**
	 * Metodo que retorna a uf pela UF
	 * @param String $sg_uf
	 * @return UfTO | false
	 */
	protected function _retornaUfPelaUF($sg_uf){
		if(empty($this->arrUfTO)){
			return false;
		}
		foreach ($this->arrUfTO as $ufTO){
			if($this->_substituirCaracteresEspeciais(strtolower($ufTO->getSg_uf())) == $this->_substituirCaracteresEspeciais(strtolower($sg_uf))){
				return $ufTO;
			}
		}
		return false;
	}

	/**
	 * Metodo que separa o DDI, DDD e Telefone de uma String
	 * @param String $numerocompleto
	 * @return array $dados
	 */
	protected function trataTelefone($numerocompleto){
		$numerocompleto = $this->somenteNumero($numerocompleto);
		$dados['ddi'] = null;
		$dados['ddd'] = null;
		$dados['numero'] = null;
		if(strlen($numerocompleto) <= 4){
			$dados['numero'] = $numerocompleto;
		}else{
			$dados['ddi'] = substr($numerocompleto, 0,2);
			$dados['ddd'] = substr($numerocompleto, 2,2);
			$dados['numero'] = substr($numerocompleto, 4);
		}
		return $dados;
	}
	
	/**
	 * Metodo estatico que retorna a instancia passada como parametro 
	 * Serve para poder utilizar os metodos da classe na mesma linha de codigo
	 * @param Object $class
	 * @return Object
	 */
	protected static function returnClassInstance($class){
		return $class;
	}
}
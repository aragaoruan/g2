<?php

/**
 * Classe com regras de negócio para interações de Avaliação
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 *
 * @package models
 * @subpackage bo
 */
class AvaliacaoBO extends Ead1_BO
{

    public $mensageiro;

    /**
     * @var AvaliacaoDAO
     */
    private $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new AvaliacaoDAO();
    }

    /**
     * Metodo que decide se cadastra ou edita a avaliacao
     * @param AvaliacaoTO $aTO
     * @param AvaliacaoIntegracaoTO $AvaliacaoIntegracoes
     * @param array $arrDisciplinas
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacao(AvaliacaoTO $aTO, $AvaliacaoIntegracoes, array $arrDisciplinas)
    {
        $this->dao->beginTransaction();
        try {
            if ($aTO->getId_avaliacao()) {
                $adTO = new AvaliacaoDisciplinaTO();
                $adTO->setId_avaliacao($aTO->getId_avaliacao());
                $this->dao->excluirAvaliacaoDisciplina($adTO);

                $this->mensageiro = $this->editarAvaliacao($aTO, $AvaliacaoIntegracoes, $arrDisciplinas);
            } else {
                $this->mensageiro = $this->cadastrarAvaliacao($aTO, $AvaliacaoIntegracoes, $arrDisciplinas);
            }

            if ($this->mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception($this->mensageiro->getMensagem());
            }

            $this->dao->commit();
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao salvar a Avaliação: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Cadastra várias avaliações.
     * @param array $arrAvaliacaoAluno
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacoesAluno(array $arrAvaliacaoAluno, $verificarConcluite = true)
    {
        if (!empty($arrAvaliacaoAluno)) {
            foreach ($arrAvaliacaoAluno as $avaliacaoAlunoTO) {
                $mensageiro = $this->salvarAvaliacaoAluno($avaliacaoAlunoTO, null, $verificarConcluite);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $mensageiro;
                }
            }
        }
        return new Ead1_Mensageiro('Notas Aproveitadas com Sucesso.');
    }

    /**
     * Metodo que cadastra avaliações para os alunos
     * @param AvaliacaoAlunoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO, $id_aluno = null, $verificarConcluite = true, $conceitual = null)
    {
        $this->dao->beginTransaction();
        try {
            $aaTO->setId_usuariocadastro(
                $aaTO->getSessao()->id_usuario ? $aaTO->getSessao()->id_usuario : Ead1_Sessao::getSessaoUsuario()->id_usuario
            );

            if (!$aaTO->getId_usuariocadastro()) {
                $aaTO->setId_usuariocadastro($id_aluno);
            }
            if (!$aaTO->getId_usuariocadastro()) {
                $m = new MatriculaTO();
                $m->setId_matricula($aaTO->getId_matricula());
                $m->fetch(true, true, true);
                $aaTO->setId_usuariocadastro($m->getId_usuario());
            }

            $aaTO->setBl_ativo(true);
            $excluirAaTO = new AvaliacaoAlunoTO();
            $excluirAaTO->setId_avaliacao($aaTO->getId_avaliacao());
            $excluirAaTO->setId_avaliacaoconjuntoreferencia($aaTO->getId_avaliacaoconjuntoreferencia());
            $excluirAaTO->setId_matricula($aaTO->getId_matricula());
            if ($aaTO->getId_situacao() == AvaliacaoAlunoTO::REVISADA) {
                $excluirAaTO->setId_situacao(AvaliacaoAlunoTO::REVISADA);
            } else {
                $excluirAaTO->setId_situacao(AvaliacaoAlunoTO::INATIVO);
            }
            $excluirAaTO->setBl_ativo(true);

            if (is_null($aaTO->getId_tiponota())) {
                $aaTO->setId_tiponota(AvaliacaoAlunoTO::NOTA_NORMAL);
            }

            $this->excluirNotaAvaliacao($excluirAaTO)->subtractMensageiro();

            if ($aaTO->getId_situacao() == AvaliacaoAlunoTO::REVISADA) {
                $aaTO->setId_situacao(AvaliacaoAlunoTO::ATIVO);
            } else {
                $aaTO->setId_situacao($aaTO->getId_situacao() ? $aaTO->getId_situacao() : AvaliacaoAlunoTO::ATIVO);
            }

            $aaTO->setId_avaliacaoaluno(false);

            $negocio = new \G2\Negocio\Avaliacao();
            //alterações para sincronizar notas
            $entity = $negocio->salvarAvaliacaoAluno($aaTO->toArray());

            if ($conceitual['id_tipoavaliacao'] == \G2\Constante\TipoAvaliacao::CONCEITUAL) {
                $negocio->atualizarEvolucao($entity, $conceitual);
            }

            if (!$entity instanceof \G2\Entity\AvaliacaoAluno) {
                throw new Zend_Exception('Erro ao salvar avaliação do aluno. [Error: ' . $entity . ' ]');
            }
            //$mensageiro = $negocio->processaNotaDisciplina($negocio->toToEntity($aaTO));
            $mensageiro = $negocio->processaNotaDisciplina($entity,$verificarConcluite);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }
            $aaTO->setId_avaliacaoaluno($entity->getId_avaliacaoaluno());

            // Movido para \G2\Negocio\Avaliacao.php::processaNotaDisciplina
            // $mensageiroProvaFinal = $this->verificaProvaFinal($aaTO);

            $this->dao->commit();
            return $this->mensageiro->setMensageiro("Nota aplicada com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Nota para esta Avaliação!' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Altera as datas dt_defesa das avaliações passadas como parâmetro.
     * @param array $arrAvaliacaoAluno
     * @return Ead1_Mensageiro
     */
    public function entregarDefesasTCC($arrAvaliacaoAluno)
    {
        if (!empty($arrAvaliacaoAluno)) {
            foreach ($arrAvaliacaoAluno as $vwAvaliacaoAlunoTO) {
                $avaliacaoAlunoTO = new AvaliacaoAlunoTO();
                $avaliacaoAlunoTO->setId_avaliacaoaluno($vwAvaliacaoAlunoTO->id_avaliacaoaluno);
                $avaliacaoAlunoTO->setDt_defesa($vwAvaliacaoAlunoTO->dt_defesa);
                $avaliacaoAlunoTO->setId_avaliacao($vwAvaliacaoAlunoTO->id_avaliacao);
                $avaliacaoAlunoTO->setId_matricula($vwAvaliacaoAlunoTO->id_matricula);
                $avaliacaoAlunoTO->setId_avaliacaoconjuntoreferencia($vwAvaliacaoAlunoTO->id_avaliacaoconjuntoreferencia);
                $avaliacaoAlunoTO->setBl_ativo(true);
                $mensageiro = $this->editarAvaliacaoAluno($avaliacaoAlunoTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $mensageiro;
                }

                $avaliacaoNegocio = new \G2\Negocio\Avaliacao();
                $serialize = new \Ead1\Doctrine\EntitySerializer($avaliacaoNegocio->getem());
                $avaliacaoAluno = $serialize->arrayToEntity(
                    $avaliacaoAlunoTO->toArray(),
                    new \G2\Entity\AvaliacaoAluno()
                );
                $mensageiro = $avaliacaoNegocio->processaNotaDisciplina($avaliacaoAluno);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $mensageiro;
                }
            }
        }

        return $this->mensageiro->setMensageiro('Defesa(s) TCC entregue(s) com Sucesso!');
    }

    /**
     * Metodo que edita a avaliacao aluno
     * @param AvaliacaoAlunoTO $aaTO
     * @param array $arrDisciplinas
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO, $where = null, array $camposIgnorados = null, $aceitaNull = false)
    {

        $this->dao->beginTransaction();

        try {
            $this->dao->editarAvaliacaoAluno($aaTO, null, $camposIgnorados, $aceitaNull);
            $this->mensageiro->setMensageiro('Avaliação Aluno editada com sucesso!', Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
        } catch (Exception $e) {
            $this->dao->rollBack();
            throw $e;
            $this->mensageiro->setMensageiro('Erro ao Editar Avaliação Aluno!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Método que verifica se a Integração e inativa o aluno
     * @param MatriculaDisciplinaTO $matriculaDisciplinaTO
     * @return Ead1_Mensageiro
     */
    public function processoConcluiAlunoIntegracao(MatriculaDisciplinaTO $matriculaDisciplinaTO)
    {
        try {
            $orm = new AlocacaoIntegracaoORM();
            $select = $orm->select()
                ->from(array('ai' => 'tb_alocacaointegracao', array('*')))
                ->join(array('al' => 'tb_alocacao'), 'al.id_alocacao = ai.id_alocacao', null)
                ->join(array('md' => 'tb_matriculadisciplina'), 'al.id_matriculadisciplina = md.id_matriculadisciplina', NULL)
                ->where("md.id_matricula = " . $matriculaDisciplinaTO->getId_matricula() . " AND md.id_disciplina = " . $matriculaDisciplinaTO->getId_disciplina());
            $dados = $orm->fetchAll($select)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro(" ", Ead1_IMensageiro::SUCESSO);
            }
            $integracao = Ead1_TO_Dinamico::encapsularTo($dados, new AlocacaoIntegracaoTO(), true);
            switch ($integracao->getId_sistema()) {
                case SistemaTO::ACTOR:
                    $service = new PapelWebServices();
                    $response = $service->atualizaMatricula(array('codmatricula' => $integracao->getSt_codalocacao(), 'flastatus' => 2));
                    break;
                default:
                    THROW new Zend_Exception("Integração Não Implementada, Consulte o Administrador do Sistema.");
                    break;
            }
            $this->mensageiro->setMensageiro($response['mensagem'], Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Editar Integração da Alocação do Aluno.", Ead1_IMensageiro::ERRO, $e->getMessage());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que decide se cadastra ou edita o conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        if ($acTO->getId_avaliacaoconjunto()) {
            return $this->editarAvaliacaoConjunto($acTO);
        }
        return $this->cadastrarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a relacao do conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        if ($acrTO->getId_avaliacaoconjuntorelacao()) {
            return $this->editarAvaliacaoConjuntoRelacao($acrTO);
        }
        return $this->cadastrarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que decide se cadastra ou edita um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        if ($aaTO->getId_avaliacaoagendamento()) {
            return $this->editarAvaliacaoAgendamento($aaTO);
        }
        return $this->cadastrarAvaliacaoAgendamento($aaTO);
    }

    /**
     * Metodo que decide se cadastra ou edita um conjunto de avaliacao do projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        if ($acpTO->getId_avaliacaoconjuntoprojeto()) {
            return $this->editarAvaliacaoConjuntoProjeto($acpTO);
        }
        return $this->cadastrarAvaliacaoConjuntoProjeto($acpTO);
    }

    /**
     * Metodo que decide se cadastra ou edita um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        if ($acsTO->getId_avaliacaoconjuntosala()) {
            return $this->editarAvaliacaoConjuntoSala($acsTO);
        }
        return $this->cadastrarAvaliacaoConjuntoSala($acsTO);
    }

    /**
     * Método que salva a Integração da avaliacao
     * @param AvaliacaoIntegracaoTO $AvaliacaoIntegracaoTO
     * @param AvaliacaoTO $avaliacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoIntegracao($AvaliacaoIntegracaoTO, AvaliacaoTO $avaliacaoTO)
    {
        try {
            if (!$avaliacaoTO->getId_avaliacao()) {
                THROW new Zend_Exception('O id_avaliacao Não veio setado!');
            }

            $this->dao->beginTransaction();

            if (empty($AvaliacaoIntegracaoTO)) {
                $this->dao->deletarAvaliacaoIntegracao(new AvaliacaoIntegracaoTO(), "id_avaliacao = " . $avaliacaoTO->getId_avaliacao());
            } else {
                $delected = false;

                if (!$delected) {
                    $delected = true;
                    $this->dao->deletarAvaliacaoIntegracao(new AvaliacaoIntegracaoTO(), "id_avaliacao = " . $AvaliacaoIntegracaoTO->getId_avaliacao() . " AND id_sistema = " . $AvaliacaoIntegracaoTO->getId_sistema());
                }
                $AvaliacaoIntegracaoTO->setId_avaliacao($AvaliacaoIntegracaoTO->getId_avaliacao());
                $AvaliacaoIntegracaoTO->setId_usuariocadastro($AvaliacaoIntegracaoTO->getSessao()->id_usuario);

                $where = "id_avaliacao = " . $AvaliacaoIntegracaoTO->getId_avaliacao();
                $exist = $this->dao->retornarAvaliacaoIntegracao($AvaliacaoIntegracaoTO, $where);
                if ($exist->count()) {
                    $this->dao->editarAvaliacaoIntegracao($AvaliacaoIntegracaoTO, $where);
                } else {
                    $this->dao->cadastrarAvaliacaoIntegracao($AvaliacaoIntegracaoTO);
                }
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro('Integração Salva com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Salvar Integração das Avaliações!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que decide se cadastra ou edita a aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        if ($apTO->getId_avaliacaoaplicacao()) {
            return $this->editarAvaliacaoAplicacao($apTO);
        }
        return $this->cadastrarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a referencia do conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        if ($acrTO->getId_avaliacaoconjuntoreferencia()) {
            return $this->editarAvaliacaoConjuntoReferencia($acrTO);
        }
        return $this->cadastrarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que cadastra/edita a avaliacao aplicacao e recadastra a avaliacao aplicacao relacao
     * @param AvaliacaoAplicacaoTO $aaTO
     * @param array $arrAvaliacaoAplicacaoRelacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO, array $arrAvaliacaoAplicacaoRelacaoTO)
    {
        try {
            $this->dao->beginTransaction();
            $mensageiro = $this->salvarAvaliacaoAplicacao($aaTO);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                THROW new Zend_Exception($mensageiro->getCodigo());
            } else if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                THROW new Zend_Exception($mensageiro->getMensagem());
            }
            $delected = false;
            foreach ($arrAvaliacaoAplicacaoRelacaoTO as $aarTO) {
                if (!$delected) {
                    $this->dao->deletarAvaliacaoAplicacaoRelacao(new AvaliacaoAplicacaoRelacaoTO(), 'id_avaliacaoaplicacao = ' . $aaTO->getId_avaliacaoaplicacao());
                    $delected = true;
                }
                if (!($aarTO instanceof AvaliacaoAplicacaoRelacaoTO)) {
                    THROW new Zend_Exception('Um dos items do array não é uma instancia de AvaliacaoAplicacaoRelacaoTO');
                }
                $aarTO->setId_avaliacaoaplicacao($aaTO->getId_avaliacaoaplicacao());
                $this->dao->cadastrarAvaliacaoAplicacaoRelacao($aarTO);
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($aaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar a Aplica√ßão da Avaliação e suas Rela√ß√µes', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salva a avaliacaoConjunto e o array de rela√ß√µes
     * @param AvaliacaoConjuntoTO $acTO
     * @param array $arrAvaliacaoConjuntoRelacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoConjuntoRelacao(AvaliacaoConjuntoTO $acTO, array $arrAvaliacaoConjuntoRelacaoTO)
    {
        try {
            $this->dao->beginTransaction();
            $mensageiro = $this->salvarAvaliacaoConjunto($acTO);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                THROW new Zend_Exception($mensageiro->getCodigo());
            }
            foreach ($arrAvaliacaoConjuntoRelacaoTO as $acrTO) {
                if (!($acrTO instanceof AvaliacaoConjuntoRelacaoTO)) {
                    THROW new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoRelacaoTO');
                }
                $acrTO->setId_avaliacaoconjunto($acTO->getId_avaliacaoconjunto());
                $mensageiro = $this->salvarAvaliacaoConjuntoRelacao($acrTO);
                if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    THROW new Zend_Exception($mensageiro->getCodigo());
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($acTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar o Conjunto de Avaliações e suas Relações', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function editarArrAvaliacaoConjuntoRelacao(AvaliacaoConjuntoTO $acTO, array $arrAvaliacaoConjuntoRelacaoTO)
    {
        try {
            $this->dao->beginTransaction();
            $mensageiro = $this->editarAvaliacaoConjunto($acTO);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getCodigo());
            }

            $this->dao->deletarAvaliacaoConjuntoRelacao(new AvaliacaoConjuntoRelacaoTO(), 'id_avaliacaoconjunto = ' . $acTO->id_avaliacaoconjunto);

            foreach ($arrAvaliacaoConjuntoRelacaoTO as $acrTO) {
                if (!($acrTO instanceof AvaliacaoConjuntoRelacaoTO)) {
                    throw new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoRelacaoTO');
                }
                $acrTO->setId_avaliacaoconjuntorelacao(NULL);
                $acrTO->setId_avaliacaoconjunto($acTO->getId_avaliacaoconjunto());
                $mensageiro = $this->salvarAvaliacaoConjuntoRelacao($acrTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    THROW new Zend_Exception($mensageiro->getCodigo());
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($acTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar o Conjunto de Avalia√ß√µes e suas Rela√ß√µes', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salva as referencias do conjunto de avaliacao
     * @param $arrAvaliacaoConjuntoRefenciaTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoConjuntoReferencia($arrAvaliacaoConjuntoRefenciaTO)
    {
        try {
            $this->dao->beginTransaction();
            $dt_hoje = new Zend_Date(null, Zend_Date::ISO_8601);
            foreach ($arrAvaliacaoConjuntoRefenciaTO as $acrTO) {
                if (!($acrTO instanceof AvaliacaoConjuntoReferenciaTO)) {
                    THROW new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoReferenciaTO');
                }
// 				if(!$this->comparaData($dt_hoje, $acrTO->getDt_inicio())){
// 					THROW new Zend_Validate_Exception("Uma das Datas de Início é Inferior a Data de Hoje.");
// 				}
                if ($acrTO->getDt_fim()) {
                    if (!$this->comparaData($acrTO->getDt_inicio(), $acrTO->getDt_fim())) {
                        THROW new Zend_Validate_Exception("A Data de Término Não pode Ser Menor que a Data de Início.");
                    }
                }
                $mensageiro = $this->salvarAvaliacaoConjuntoReferencia($acrTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    THROW new Zend_Exception($mensageiro->getCodigo());
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Referencias do Conjunto de Avaliação Salvas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Referencias do Conjunto de Avaliação', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a avaliacao disciplina
     * @param AvaliacaoDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO)
    {
        try {
            $id_avaliacaodisciplina = $this->dao->cadastrarAvaliacaoDisciplina($adTO);
            $adTO->setId_avaliacao($id_avaliacaodisciplina);
            return $this->mensageiro->setMensageiro($adTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
// 			throw $e;
            Zend_Debug::dump($e);
            exit;
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar vínculo de Avaliação com Disciplina!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a avaliacao
     * @param AvaliacaoTO $aTO
     * @param AvaliacaoIntegracaoTO $aiTO
     * @param array $arrDisciplinas
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacao(AvaliacaoTO $aTO, $aiTO, array $arrDisciplinas)
    {
        try {

            $aTO->setId_usuariocadastro($aTO->getSessao()->id_usuario);
            if (!$aTO->getId_entidade())
                $aTO->setId_entidade($aTO->getSessao()->id_entidade);

            $id_avaliacao = $this->dao->cadastrarAvaliacao($aTO);
            $aTO->setId_avaliacao($id_avaliacao);

            $this->salvarAvaliacaoIntegracao($aiTO, $aTO);

            foreach ($arrDisciplinas as $avaliacaoDisciplina) {
                $adTO = new AvaliacaoDisciplinaTO();
                $adTO->setId_avaliacao($id_avaliacao);
                $adTO->setId_disciplina($avaliacaoDisciplina->id_disciplina);
                $this->cadastrarAvaliacaoDisciplina($adTO);
            }

            return $this->mensageiro->setMensageiro($aTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        try {
            $acTO->setId_usuariocadastro($acTO->getSessao()->id_usuario);
            if (!$acTO->getId_entidade()) {
                $acTO->setId_entidade($acTO->getSessao()->id_entidade);
            }
            $id_avaliacaoconjunto = $this->dao->cadastrarAvaliacaoConjunto($acTO);
            $acTO->setId_avaliacaoconjunto($id_avaliacaoconjunto);
            return $this->mensageiro->setMensageiro($acTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a relacao de um Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        try {
            $id_avaliacaoconjuntorelacao = $this->dao->cadastrarAvaliacaoConjuntoRelacao($acrTO);
            $acrTO->setId_avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao);
            return $this->mensageiro->setMensageiro($acrTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Relação do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            if (!$apTO->getId_entidade()) {
                $apTO->setId_entidade($apTO->getSessao()->id_entidade);
            }
            $apTO->setId_usuariocadastro($apTO->getSessao()->id_usuario);
            $id_avaliacaoaplicacao = $this->dao->cadastrarAvaliacaoAplicacao($apTO);
            $apTO->setId_avaliacaoaplicacao($id_avaliacaoaplicacao);
            return $this->mensageiro->setMensageiro($apTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Aplica√ßão da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        try {
            $mensagemWs = '';
            $vwAvaliacaoAplicacaoAgendamentoTO = Ead1_TO_Dinamico::mergeTO(new VwAvaliacaoAplicacaoAgendamentoTO(), clone $aaTO);
            $vwAvaliacaoAplicacaoAgendamentoTO->dt_agendamento = null;
            $vwAvaliacaoAplicacaoAgendamentoTO->id_situacao = null;
            $vwAvaliacaoAplicacaoAgendamentoTO->bl_ativo = null;
            $mensageiro = $this->retornarVwAvaliacaoAplicacaoAgendamento($vwAvaliacaoAplicacaoAgendamentoTO);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $mensagem = $mensageiro->getFirstMensagem() != null ? $mensageiro->getFirstMensagem() : 'Não foi encontrado nenhuma Avaliação disponível para agendamento';
                throw new DomainException($mensagem);
            }
            $vwAvaliacaoAplicacaoAgendamentoTO = $mensageiro->getFirstMensagem();

            if ($vwAvaliacaoAplicacaoAgendamentoTO->bl_unica != false) {
                $diasSemana = $this->retornaDiasSemanaAplicacaoAvaliacao($vwAvaliacaoAplicacaoAgendamentoTO->id_horarioaula);
                if (!array_key_exists($aaTO->dt_agendamento->getWeekday()->toString(Zend_Date::WEEKDAY_DIGIT), $diasSemana)) {
                    throw new DomainException('A data de agendamento deve ser entre os dias da semana: ' . implode(', ', $diasSemana));
                }
            }

            $aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
            $aaTO->setId_situacao(AvaliacaoAgendamentoTO::SITUACAO_AGENDADO);
            $id_avaliacaoagendamento = $this->dao->cadastrarAvaliacaoAgendamento($aaTO);
            $aaTO->setId_avaliacaoagendamento($id_avaliacaoagendamento);

            $wsBO = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
            $mensageiroWS = null;
            if ($wsBO->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO)) {
                $mensageiroWS = $wsBO->cadastrarAvaliacaoAluno($aaTO);
                if ($mensageiroWS->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $mensagemWs = 'Erro de Integracao: ' . $mensageiroWS->getFirstMensagem();
                    throw new Zend_Exception($mensageiroWS->getCurrent());
                }
            }

            return $this->mensageiro->setMensageiro($aaTO, Ead1_IMensageiro::SUCESSO, $mensageiroWS);
        } catch (DomainException $domainExc) {
            return $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro($mensagemWs ? $mensagemWs : 'Erro ao Cadastrar o Agendamento da Avaliação! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        try {
            $id_avaliacaoconjuntoprojeto = $this->dao->cadastrarAvaliacaoConjuntoProjeto($acpTO);
            $acpTO->setId_avaliacaoconjuntoprojeto($id_avaliacaoconjuntoprojeto);
            return $this->mensageiro->setMensageiro($acpTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Agendamento da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        try {
            $id_avaliacaoconjuntosala = $this->dao->cadastrarAvaliacaoConjuntoSala($acsTO);
            $acsTO->setId_avaliacaoconjuntosala($id_avaliacaoconjuntosala);
            return $this->mensageiro->setMensageiro($acsTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de  da Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a referencia de um conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        try {
            $id_avaliacaoconjuntoreferencia = $this->dao->cadastrarAvaliacaoConjuntoReferencia($acrTO);
            $acrTO->setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia);
            return $this->mensageiro->setMensageiro($acrTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Referencia do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a Relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        try {
            $this->dao->cadastrarAvaliacaoAplicacaoRelacao($aprTO);
            return $this->mensageiro->setMensageiro('Relatório da Avaliação da Avaliação Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de Avaliação do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita a avaliacao
     * @param AvaliacaoTO $aTO
     * @param AvaliacaoIntegracaoTO $AvaliacaoIntegracoes
     * @param array $arrDisciplinas
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacao(AvaliacaoTO $aTO, $AvaliacaoIntegracoes, array $arrDisciplinas)
    {

        $this->dao->beginTransaction();

        try {
            $this->dao->editarAvaliacao($aTO);
            $this->salvarAvaliacaoIntegracao($AvaliacaoIntegracoes, $aTO);
            $this->dao->deletarAvaliacaoDisciplina(new AvaliacaoDisciplinaTO(), ' id_avaliacao = ' . $aTO->getId_avaliacao());
            foreach ($arrDisciplinas as $avaliacaoDisciplina) {
                $adTO = new AvaliacaoDisciplinaTO();
                $adTO->setId_avaliacao($aTO->getId_avaliacao());
                $adTO->setId_disciplina($avaliacaoDisciplina->id_disciplina);
                $mensageiro = $this->cadastrarAvaliacaoDisciplina($adTO);
                if ($mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                    throw new ErrorException($mensageiro->mensagem[0]);
                }
            }

            $this->mensageiro->setMensageiro('Avaliação editada com sucesso!', Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
        } catch (Exception $e) {
            $this->dao->rollBack();
            throw $e;
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que edita um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        try {
            $this->dao->editarAvaliacaoConjunto($acTO);
            return $this->mensageiro->setMensageiro('Conjunto de Avaliação Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita uma relacao de Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        try {
            $this->dao->editarAvaliacaoConjuntoRelacao($acrTO);
            return $this->mensageiro->setMensageiro('Relatório do Conjunto de Avaliação Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Relatório do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            $this->dao->editarAvaliacaoAplicacao($apTO);
            return $this->mensageiro->setMensageiro('Aplicação da Avaliação Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Aplicação de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita um agendamento de uma avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        try {
            $this->dao->editarAvaliacaoAgendamento($aaTO);
            return $this->mensageiro->setMensageiro('Agendamento da Avaliação Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Agendamento de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        try {
            $this->dao->editarAvaliacaoConjuntoSala($acsTO);
            return $this->mensageiro->setMensageiro('Conjunto de Avaliação da Sala de Aula Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Conjunto de Avaliação da Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        try {
            $this->dao->editarAvaliacaoConjuntoProjeto($acpTO);
            return $this->mensageiro->setMensageiro('Conjunto de Avaliação de Projeto Pedagógico Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Conjunto de Avaliação Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita uma referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        try {
            $this->dao->editarAvaliacaoConjuntoReferencia($acrTO);
            return $this->mensageiro->setMensageiro('Referencia do Conjunto de Avaliação Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Referencia do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui a avaliacao
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacao(AvaliacaoTO $aTO)
    {
        try {
            $this->dao->deletarAvaliacao($aTO);
            return $this->mensageiro->setMensageiro('Avaliação Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui o conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        try {
            $this->dao->deletarAvaliacaoConjunto($acTO);
            return $this->mensageiro->setMensageiro('Conjunto de Avaliação Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui a Relação do conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        try {
            $this->dao->deletarAvaliacaoConjuntoRelacao($acrTO);
            return $this->mensageiro->setMensageiro('Relação do Conjunto de Avaliação Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Relação do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            $this->dao->deletarAvaliacaoAplicacao($apTO);
            return $this->mensageiro->setMensageiro('Aplica√ßão da Avaliação Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Aplica√ßão da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita uma aplicacao de uma avaliacao colocando apenas o bl_ativo para 0
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function desativarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$apTO->getId_avaliacaoaplicacao()) {
                THROW new Zend_Exception('O id_avaliacaoaplicacao não pode vir vazio!');
            }
            $orm = new AvaliacaoAplicacaoORM();
            $orm->update(array('bl_ativo' => 0), 'id_avaliacaoaplicacao = ' . $apTO->getId_avaliacaoaplicacao());
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Aplica√ßão da Avaliação Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Excluir Aplica√ßão da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui a Relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        try {
            $this->dao->deletarAvaliacaoAplicacaoRelacao($aprTO);
            return $this->mensageiro->setMensageiro('Relação da Aplica√ßão da Avaliação Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Relação da Aplica√ßão da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        try {
            $this->dao->deletarAvaliacaoAgendamento($aaTO);
            return $this->mensageiro->setMensageiro('Agendamento da Avaliação Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Agendamento da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        try {
            $this->dao->deletarAvaliacaoConjuntoProjeto($acpTO);
            return $this->mensageiro->setMensageiro('Conjunto da Avaliação de Sala de Aula Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto da Avaliação de Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        try {
            $this->dao->deletarAvaliacaoConjuntoSala($acsTO);
            return $this->mensageiro->setMensageiro('Conjunto da Avaliação de Projeto Pedag√≥gico Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto da Avaliação de Projeto Pedag√≥gico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui a referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        try {
            $this->dao->deletarAvaliacaoConjuntoReferencia($acrTO);
            return $this->mensageiro->setMensageiro('Referencia do Conjunto da Avaliação Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Referencia do Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Deleta os vínculos entre Avaliação e disciplina
     * @param AvaliacaoDisciplinaTO $avaliacaoDisciplinaTO
     */
    public function deletarAvaliacaoDisciplina(AvaliacaoDisciplinaTO $avaliacaoDisciplinaTO)
    {
        try {
            $this->dao->deletarAvaliacaoDisciplina($avaliacaoDisciplinaTO, ' id_avaliacao = ' . $avaliacaoDisciplinaTO->getId_avaliacao());
            return $this->mensageiro->setMensageiro('Disciplina da Avaliação removida com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Disciplina da Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que verifica se existem vinculos com a avaliacao e exclui
     * @param AvaliacaoTO $to
     * @return Ead1_Mensageiro
     */
    public function verificaExcluiAvaliacao(AvaliacaoTO $to)
    {
        try {
            $this->dao->beginTransaction();
            //$arrParams = array('id_avaliacao' => $to->getId_avaliacao());
            $this->dao->deletarAvaliacaoIntegracao(new AvaliacaoIntegracaoTO(), "id_avaliacao = " . $to->getId_avaliacao());
            $this->dao->deletarAvaliacaoDisciplina(new AvaliacaoDisciplinaTO(), "id_avaliacao = " . $to->getId_avaliacao());
            $this->dao->deletarAvaliacao($to);
            $this->dao->commit();
            $this->mensageiro->setMensageiro("Avaliação Excluida com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro("A Avaliação não pode ser Excluida!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que verifica se existem vinculos com a avaliacao conjunto e exclui
     * @param AvaliacaoConjuntoTO $to
     * @return Ead1_Mensageiro
     */
    public function verificaExcluiAvaliacaoConjunto(AvaliacaoConjuntoTO $to)
    {
        try {
            $this->dao->beginTransaction();
            $arrParams = array('id_avaliacaoconjunto' => $to->getId_avaliacaoconjunto());
            $conjuntoReferenciaORM = new AvaliacaoConjuntoReferenciaORM();
            if ($conjuntoReferenciaORM->consulta(new AvaliacaoConjuntoReferenciaTO($arrParams), false, true)) {
                THROW new Zend_Validate_Exception("Não é Possivel Excluir o Conjunto de Avaliação pois Existem Referencias Vinculadas a Ele!");
            }
            $this->dao->deletarAvaliacaoConjuntoRelacao(new AvaliacaoConjuntoRelacaoTO(), "id_avaliacaoconjunto = " . $to->getId_avaliacaoconjunto());
            $this->dao->deletarAvaliacaoConjunto($to);
            $this->dao->commit();
            $this->mensageiro->setMensageiro("Conjunto de Avaliação Excluido com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro("O Conjunto de Avaliação não pode ser Excluido!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna Avaliação
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacao(AvaliacaoTO $aTO)
    {
        try {
            if (!$aTO->getId_entidade()) {
                $aTO->setId_entidade($aTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarAvaliacao($aTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Avaliação disciplina
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoDisciplina(AvaliacaoTO $aTO)
    {
        try {
            $vwAvaliacaoDisciplinaTO = new VwAvaliacaoDisciplinaTO();
            $vwAvaliacaoDisciplinaTO->setId_avaliacao($aTO->getId_avaliacao());
            $dados = $this->dao->retornarAvaliacaoDisciplina($vwAvaliacaoDisciplinaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar vínculos de Disciplina com Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna tipo de Avaliação
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAvaliacao(TipoAvaliacaoTO $taTO)
    {
        try {
            $dados = $this->dao->retornarTipoAvaliacao($taTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a relacao do conjunto de avaliacoes
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoConjuntoRelacao($acrTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoRelacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna tipo de calculo de avaliacao
     * @param TipoCalculoAvaliacaoTO $tcaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoCalculoAvaliacao(TipoCalculoAvaliacaoTO $tcaTO)
    {
        try {
            $dados = $this->dao->retornarTipoCalculoAvaliacao($tcaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoCalculoAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        try {
            if (!$acTO->getId_entidade()) {
                $acTO->setId_entidade($acTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarAvaliacaoConjunto($acTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        try {
            if (!$apTO->getId_entidade()) {
                $apTO->setId_entidade($apTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarAvaliacaoAplicacao($apTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAplicacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Aplica√ßão da Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Relação de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoAplicacaoRelacao($aprTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAplicacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Relação da Aplica√ßão da Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoAgendamento($aaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAgendamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Agendamento da Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoConjuntoSala($acsTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoSalaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalica√ßão da Sala de Aula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoConjuntoProjeto($acpTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalica√ßão de Projeto Pedag√≥gico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a referencia conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoConjuntoReferencia($acrTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoReferenciaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Referencia do Conjunto de Avalica√ßão!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna AvaliacaoIntegracao
     * @param AvaliacaoIntegracaoTO $aiTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $aiTO)
    {
        try {
            $dados = $this->dao->retornarAvaliacaoIntegracao($aiTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoIntegracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Avalicação Integracao!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, agendamento, aluno, projeto pedag√≥gico, entidade
     * @param VwAvaliacaoAgendamentoTO $vaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAgendamento(VwAvaliacaoAgendamentoTO $vaaTO)
    {
        return new Ead1_Mensageiro($vaaTO->fetch(false, false, false, true, true));
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, aplicacao e agendamento
     * @param VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO
     * @see AvaliacaoRO::retornarVwAvaliacaoAplicacaoAgendamento()
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAplicacaoAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO)
    {
        //var_dump($vaaaTO);
        try {
            $dados = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento($vaaaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAplicacaoAgendamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as avaliacos disponiveis para agendamento
     * @param VwAvaliacaoAplicacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornaAvaliacaoAplicacaoDisponiveisAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $to)
    {
        try {
            $dados = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            //Verificando se existe já algum agendamento para o Dia a agendar
            $arrDadosVerifica = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento(
                new VwAvaliacaoAplicacaoAgendamentoTO(
                    array(
                        'id_matricula' => $to->getId_matricula()
                        //,'id_situacao' => AvaliacaoAgendamentoTO::SITUACAO_AGENDADO
                    )
                ), array('dt_agendamento >= GETDATE()', 'id_matricula = ' . $to->getId_matricula())
            )->toArray();
            if (!empty($arrDadosVerifica)) {
                foreach ($dados as $index => $dado) {
                    foreach ($arrDadosVerifica as $verifica) {
                        if (!empty($verifica['dt_agendamento'])) {
                            if ($dado['id_avaliacao'] == $verifica['id_avaliacao']) {
                                unset($dados[$index]);
                            }
                        }
                    }
                }
            }
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrDadosAvaliacao = $this->dao->retornarVwAvaliacaoAluno(
                new VwAvaliacaoAlunoTO(
                    array('id_matricula' => $to->getId_matricula(),
                        'id_tipoavaliacao' => TipoAvaliacaoTO::RECUPERACAO)
                )
            )->toArray();
            if (!empty($arrDadosAvaliacao)) {
                foreach ($dados as $index => $dado) {
                    foreach ($arrDadosAvaliacao as $avaliacao) {
                        if ($dado['id_avaliacao'] == $avaliacao['id_avaliacao'] && $avaliacao['id_tipoavaliacao'] == TipoAvaliacaoTO::RECUPERACAO) {
                            foreach ($dados as $confere) {
                                if ($confere['id_avaliacao'] == $avaliacao['id_avaliacaorecupera'] && empty($avaliacao['st_nota'])) {
                                    unset($dados[$index]);
                                }
                            }
                        }
                    }
                }
            }

            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAplicacaoAgendamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Pesquisa as defesas TCC
     * @param VwAvaliacaoAlunoTO $vwAvaliacaoAlunoTO
     * @param boolean $bl_entregue
     * @return Ead1_Mensageiro
     */
    public function pesquisarDefesaTCC(VwAvaliacaoAlunoTO $vwAvaliacaoAlunoTO, $bl_entregue = false)
    {
        $where = '';
        if ($vwAvaliacaoAlunoTO->getId_projetopedagogico()) {
            $where = "id_projetopedagogico = " . $vwAvaliacaoAlunoTO->getId_projetopedagogico() . ' and ';
        }
        $where .= "id_avaliacaoaluno is not null and dt_defesa is " . ($bl_entregue ? 'not null' : 'null') . " and id_tipodisciplina = " . TipoDisciplinaTO::TCC . "
			and id_tipoavaliacao = " . TipoAvaliacaoTO::TCC;
        if ($vwAvaliacaoAlunoTO->getSt_nomecompleto()) {
            $where .= " and st_nomecompleto like '%" . $vwAvaliacaoAlunoTO->getSt_nomecompleto() . "%'";
        }
        try {
            $dados = $this->dao->retornarVwAvaliacaoAluno($vwAvaliacaoAlunoTO, $where)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Defesa Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAlunoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, matricula, notas e modulo
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @see AvaliacaoRO::retornarVwAvaliacaoAluno()
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAluno(VwAvaliacaoAlunoTO $vaaTO, $blTodos = false)
    {
        try {
            if ($blTodos) {
                unset($vaaTO->bl_ativo);
            }

            $dados = $this->dao->retornarVwAvaliacaoAluno($vaaTO)->toArray();
            //ID_FIXOS
            $TIPO_AVALIACAO_RECUPERACAO = 2;
            //
            foreach ($dados as $index => $dado) {
                if ($dado['id_tipoavaliacao'] == $TIPO_AVALIACAO_RECUPERACAO) {
                    foreach ($dados as $valida) {
                        if ($dado['id_avaliacaorecupera'] == $valida['id_avaliacao']) {
                            if ($valida['st_nota']) {
                                $dado['bl_editavel'] = 1;
                            } else {
                                $dado['bl_editavel'] = 0;
                            }
                        }
                    }
                } else {
                    $dado['bl_editavel'] = 1;
                }
                $dados[$index] = $dado;
            }
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAlunoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, matricula, notas e modulo para o histórico
     * @param VwAvaliacaoAlunoHistoricoTO $vaaTO
     * @see AvaliacaoRO::retornarVwAvaliacaoAlunoHistorico()
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAlunoHistorico(VwAvaliacaoAlunoHistoricoTO $vaaTO)
    {
        try {

            $vaaTO->setBl_ativo(true);
            $dados = $this->dao->retornarVwAvaliacaoAlunoHistorico($vaaTO)->toArray();

            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAlunoHistoricoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um conjunto de disciplina, matricula, notas e modulo
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @see AvaliacaoRO::retornarVwAvaliacaoAluno()
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAlunoDisciplina(VwAvaliacaoAlunoTO $vaaTO)
    {
        try {
            $dados = array();
            $dadosModulo = $this->dao->retornarVwAvaliacaoAlunoModulo($vaaTO)->toArray();
            if (empty($dadosModulo)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Avaliação Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $dadosModulo = Ead1_TO_Dinamico::encapsularTo($dadosModulo, new VwAvaliacaoAlunoTO());
            foreach ($dadosModulo as $index1 => $dadoModulo) {
                $vaaTO->setId_modulo($dadoModulo->getId_modulo());
                $dadosDisciplina = $this->dao->retornarVwAvaliacaoAlunoDisciplina($vaaTO)->toArray();
                $dados[$index1]['modulo'] = $dadoModulo;
                $dadosDisciplina = Ead1_TO_Dinamico::encapsularTo($dadosDisciplina, new VwAvaliacaoAlunoTO());
                foreach ($dadosDisciplina as $index2 => $dadoDisciplina) {
                    $dadoDisciplina->setSt_nota(0);
                    $vaaTO->setId_disciplina($dadoDisciplina->getId_disciplina());
                    $dadosAvaliacao = $this->dao->retornarVwAvaliacaoAluno($vaaTO)->toArray();
                    foreach ($dadosAvaliacao as $dadoAvaliacao) {
                        if ($dadoAvaliacao['st_nota'] != null) {
                            $dadoDisciplina->setSt_nota($dadoDisciplina->getSt_nota() + $dadoAvaliacao['st_nota']);
                        } else {
                            $dadoDisciplina->setSt_nota('');
                        }
                    }
                    $dados[$index1]['disciplinas'][$index2] = $dadoDisciplina;
                }
            }
            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de avaliacao
     * @param VwPesquisaAvaliacaoTO $vwpaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwPesquisaAvaliacao(VwPesquisaAvaliacaoTO $vwpaTO)
    {
        try {
            if (!$vwpaTO->getId_entidade()) {
                $vwpaTO->setId_entidade($vwpaTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarVwPesquisaAvaliacao($vwpaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPesquisaAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as Rela√ß√µes das Avalia√ß√µes da aplica√ßão no formato de PesquisaAvaliacaoTO
     * @param AvaliacaoAplicacaoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacoesAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO)
    {
        try {
            $this->dao->beginTransaction();
            $pesquisarDAO = new PesquisarDAO();
            $relacoes = $this->dao->retornarAvaliacaoAplicacaoRelacao(new AvaliacaoAplicacaoRelacaoTO(), 'id_avaliacaoaplicacao = ' . $aaTO->getId_avaliacaoaplicacao())->toArray();
            if (empty($relacoes)) {
                $this->dao->commit();
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $where = '';
            $arrWhere = array();
            foreach ($relacoes as $relacao) {
                $arrWhere[$relacao['id_avaliacao']] = $relacao['id_avaliacao'];
            }
            $implode = implode(' ,', $arrWhere);
            $where = 'id_avaliacao in (' . $implode . ')';
            $avaliacoes = $pesquisarDAO->retornarPesquisaAvaliacao(new PesquisaAvaliacaoTO(), $where)->toArray();
            if (empty($avaliacoes)) {
                THROW new Zend_Exception('Os ids das Avalia√ß√µes na tb_avaliacaoaplicacaorelacao estão diferentes ');
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($avaliacoes, new PesquisaAvaliacaoTO()));
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Retornar a Relação da Aplica√ßão das Avalia√ß√µes');
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjunto
     * @param VwAvaliacaoConjuntoTO $vwacTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjunto(VwAvaliacaoConjuntoTO $vwacTO)
    {
        try {
            if (!$vwacTO->getId_entidade()) {
                $vwacTO->setId_entidade($vwacTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarVwAvaliacaoConjunto($vwacTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntorelacao
     * @param VwAvaliacaoConjuntoRelacaoTO $vwacrTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjuntoRelacao(VwAvaliacaoConjuntoRelacaoTO $vwacrTO, $where = null)
    {
        try {
            $dados = $this->dao->retornarVwAvaliacaoConjuntoRelacao($vwacrTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoRelacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação Relação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntoreferencia
     * @param VwAvaliacaoConjuntoReferenciaTO $vwacrTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjuntoReferencia(VwAvaliacaoConjuntoReferenciaTO $vwacrTO, $where = null)
    {
        try {
            $dados = $this->dao->retornarVwAvaliacaoConjuntoReferencia($vwacrTO)->toArray();

            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }

            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoReferenciaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação Referencia!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui logicamente o registro da tb_avaliacaoaluno
     * @param AvaliacaoAlunoTO $aaTO
     * @return Ead1_Mensageiro
     * @throws \Doctrine\DBAL\DBALException
     */
    public function excluirNotaAvaliacao(AvaliacaoAlunoTO $aaTO)
    {
        try {
            $negocio = new \G2\Negocio\AvaliacaoAluno();

            $where = " WHERE id_avaliacaoconjuntoreferencia = " . $aaTO->getId_avaliacaoconjuntoreferencia()
                . " and id_avaliacao = " . $aaTO->getId_avaliacao()
                . " AND bl_ativo = 1  AND id_matricula = " . $aaTO->getId_matricula()
                . " AND id_situacao = " . AvaliacaoAlunoTO::ATIVO;

            $update = "UPDATE tb_avaliacaoaluno SET ";

            $columnsUpdate = $aaTO->toArrayUpdate(false, array('id_avaliacaoaluno', 'id_matricula', 'id_avaliacao'));
            if ($columnsUpdate) {
                $arr = [];
                foreach ($columnsUpdate as $key => $value) {
                    $arr[] = $key . "='" . $value . "'";
                }

                $update .= implode(", ", $arr);
                $update .= $where;

                $stmt = $negocio->getem()->getConnection()
                    ->prepare($update);
                $result = $stmt->execute();

                if ($result) {
                    return new Ead1_Mensageiro('Nota excluída com sucesso!', Ead1_IMensageiro::SUCESSO);
                } else {
                    throw new Zend_Exception('Erro ao excluir Nota!');
                }

            }

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao retornar conjunto de avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui disciplina de Avaliação.
     * @param AvaliacaoDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function excluirAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO)
    {
        try {
            $dados = $this->dao->excluirAvaliacaoDisciplina($adTO);
            if ($dados) {
                return new Ead1_Mensageiro('Disciplina excluída com sucesso!', Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Zend_Exception('Erro ao excluir Disciplina!');
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao excluir Disciplina!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function retornaDiasSemanaAplicacaoAvaliacao($idHorarioAula)
    {

        $horarioAulaBO = new HorarioAulaBO();
        $vwHorarioAulaDiaSemanaTO = new VwHorarioAulaDiaSemanaTO(array('id_horarioaula' => $idHorarioAula));
        $mensageiro = $horarioAulaBO->retornarHorarioAulaDiaSemana($vwHorarioAulaDiaSemanaTO);
        $diasSemana = array();
        $textoSemana = array();

        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $diasSemana = $mensageiro->mensagem;
        }

        foreach ($diasSemana as $vwHorarioAulaDiaSemanaTO) {
            $textoSemana[$vwHorarioAulaDiaSemanaTO->id_diasemana] = $vwHorarioAulaDiaSemanaTO->st_diasemana;
        }

        return $textoSemana;
    }

    /**
     * Método que retorna o vinculo de Integração da Avaliação com o Modelo de Avaliação do Sistema de Avaliacao
     * @param AvaliacaoTO $avaliacaoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVinculoAvaliacaoModeloAvaliacaoSA(AvaliacaoTO $avaliacaoTO)
    {
        try {
            if (!$avaliacaoTO->getId_avaliacao()) {
                THROW new Zend_Exception('O id_avaliacao não veio setado!');
            }
            $arrIntegracao = $this->dao->retornarAvaliacaoIntegracaoTO(new AvaliacaoIntegracaoTO(
                array('id_avaliacao' => $avaliacaoTO->getId_avaliacao(),
                    'id_sistema' => SistemaTO::SISTEMA_AVALIACAO)
            ))->toArray();
            if (empty($arrIntegracao)) {
                THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
            } else {
                $arrIntegracao = Ead1_TO_Dinamico::encapsularTo($arrIntegracao, new AvaliacaoIntegracaoTO());
                $avaliacaoRO = new AvaliacaoRO();
                $resultService = $avaliacaoRO->retornarModeloAvaliacaoSA();
                switch ($resultService->getTipo()) {
                    case Ead1_IMensageiro::SUCESSO:
                        $arrResposta = array();
                        $arrService = $resultService->getMensagem();
                        foreach ($arrIntegracao as $integracao) {
                            foreach ($arrService as $sv) {
                                if ($integracao->getSt_codsistema() == $sv->id_modeloavaliacao) {
                                    $arrResposta[] = $sv;
                                }
                            }
                        }
                        if (empty($arrResposta)) {
// 							THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
                        }
                        $this->mensageiro->setMensageiro($arrResposta, Ead1_IMensageiro::SUCESSO);
                        break;
                    case Ead1_IMensageiro::AVISO:
                        THROW new Zend_Validate_Exception($resultService->mensagem[0]);
                        break;
                    case Ead1_IMensageiro::ERRO:
                        THROW new Zend_Exception($resultService->getCodigo());
                        break;
                }
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Integra√ß√µes de Avaliação!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as quest√µes, os itens e as respostas de uma determinada Avaliação
     * @param string $guidAvaliacao
     * @return Ead1_Mensageiro
     */
    public function retornarQuestoesItensRespostasSA($guidAvaliacao)
    {
        $service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
        $dadosServico = $service->retornarAvaliacaoQuestaoItensRespostas($guidAvaliacao);
        if ($dadosServico->getTipo() == Ead1_IMensageiro::SUCESSO) {
            foreach ($dadosServico->getMensagem() as $chave => $xml) {
                if ($xml->numquestao) {
                    if (!isset($arDados['aluno'])) {
                        $tbUsuarioIntegracaoTO = new UsuarioIntegracaoTO();
                        $tbUsuarioIntegracaoTO->setSt_codusuario($xml->codpessoaavaliada);
                        $tbUsuarioIntegracaoTO->setId_sistema(SistemaTO::SISTEMA_AVALIACAO);
                        $tbUsuarioIntegracaoTO->fetch(null, true, true);

                        $usuarioTO = new UsuarioTO();
                        $usuarioTO->setId_usuario($tbUsuarioIntegracaoTO->getId_usuario());
                        $usuarioTO->fetch(true, true, true);
                        $arDados['aluno'] = $usuarioTO->getSt_nomecompleto();
                    }
                    $arDados['avaliacao'] = $guidAvaliacao;
                    $arDados['questoes'][(string)$xml->codquestao]['id'] = (string)$xml->codquestao;
                    $arDados['questoes'][(string)$xml->codquestao]['referencia'] = ((string)$xml->numquestao ? (string)$xml->numquestao : 0);
                    $conversor = new Ead1_ConversorAlfaNumerico();
                    $arDados['questoes'][(string)$xml->codquestao]['itens'][(string)$xml->coditem]['id'] = (string)$xml->coditem;
                    $arDados['questoes'][(string)$xml->codquestao]['itens'][(string)$xml->coditem]['referencia'] = strtoupper($conversor->converteNumero((int)((string)$xml->numitem ? (string)$xml->numitem : 0))->getLetra());
                    $arDados['questoes'][(string)$xml->codquestao]['itens'][(string)$xml->coditem]['marcado'] = ((string)$xml->coditemrespondido == (string)$xml->coditem);
                    $arDados['questoes'][(string)$xml->codquestao]['itens'] = array_values($arDados['questoes'][(string)$xml->codquestao]['itens']);
                }
            }
            if (isset($arDados['questoes'])) {
                $arDados['questoes'] = array_values($arDados['questoes']);
                return new Ead1_Mensageiro($arDados);
            } else {
                return new Ead1_Mensageiro("Avaliação não encontrada, ou não impressa!", Ead1_IMensageiro::AVISO);
            }
        } else {
            return $dadosServico;
        }
    }

    /**
     * Método que cadastra via WebService uma resposta por vez no Sistema de Avaliação
     *    Ap√≥s o cadastro ele já busca o cálculo da nota percentual da Avaliação no Sistema de Avalia√ß√µes e gera a nota atual no Gestor2
     * @param string $guidAvaliacao
     * @param int $idQuestao
     * @param int $idItemRespondido
     * @return Ead1_Mensageiro
     */
    public function cadastrarRespostasSA($guidAvaliacao, $idQuestao, $idItemRespondido)
    {
        $service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
        $mensageiroRespostas = $service->cadastrarResposta($guidAvaliacao, $idQuestao, $idItemRespondido);
        if ($mensageiroRespostas->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiroNota = $service->retornarPercentualAprovacaoAvaliacao($guidAvaliacao);
            if ($mensageiroNota->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $avaliacaoDAO = new AvaliacaoDAO();
                $avaliacaoAlunoDados = $avaliacaoDAO->retornarAvaliacaoAgendamentoIntegracao($guidAvaliacao);
                $aaTO = new AvaliacaoAlunoTO();
                $aaTO->setId_matricula($avaliacaoAlunoDados['id_matricula']);
                $aaTO->setId_avaliacao($avaliacaoAlunoDados['id_avaliacao']);
                $aaTO->setId_avaliacaoagendamento($avaliacaoAlunoDados['id_avaliacaoagendamento']);
                $aaTO->setId_avaliacaoconjuntoreferencia($avaliacaoAlunoDados['id_avaliacaoconjuntoreferencia']);
                $aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
                $aaTO->setSt_nota((string)$mensageiroNota->getCurrent()->nota);

                return $this->salvarAvaliacaoAluno($aaTO);
            } else {
                return $mensageiroNota;
            }
        } else {
            return $mensageiroRespostas;
        }
    }

    /**
     * Método que cadastra via WebService as respostas no Sistema de Avaliação e depois realiza o cálculo da nota em valor percentual até o momento
     * @param string $guidAvaliacao
     * @param array $arQuestoes
     * @return Ead1_Mensageiro
     */
    public function cadastrarBlocoRespostasSA($guidAvaliacao, array $arQuestoes)
    {
        $service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
        $mensageiroRespostas = $service->cadastrarBlocoRespostas($guidAvaliacao, $arQuestoes);
        if ($mensageiroRespostas->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensageiroNota = $service->retornarPercentualAprovacaoAvaliacao($guidAvaliacao);
            if ($mensageiroNota->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $avaliacaoDAO = new AvaliacaoDAO();
                $avaliacaoAlunoDados = $avaliacaoDAO->retornarAvaliacaoAgendamentoIntegracao($guidAvaliacao);

                $aaTO = new AvaliacaoAgendamentoTO();
                $aaTO->setId_avaliacaoagendamento($avaliacaoAlunoDados[0]['id_avaliacaoagendamento']);
                $avaliacaoAgendamento = $avaliacaoDAO->retornarAvaliacaoAgendamento($aaTO)->toArray();

                $avaliacaoTO = new AvaliacaoTO();
                $avaliacaoTO->setId_avaliacao($avaliacaoAlunoDados[0]['id_avaliacao']);
                $avaliacaoTO->fetch(true, true, true);

                $aaTO = new AvaliacaoAlunoTO();
                $aaTO->setId_matricula($avaliacaoAlunoDados[0]['id_matricula']);
                $aaTO->setId_avaliacao($avaliacaoAlunoDados[0]['id_avaliacao']);
                $aaTO->setId_avaliacaoagendamento($avaliacaoAlunoDados[0]['id_avaliacaoagendamento']);
                $aaTO->setId_avaliacaoconjuntoreferencia($avaliacaoAlunoDados[0]['id_avaliacaoconjuntoreferencia']);
                $aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
                $aaTO->setSt_nota((((string)$mensageiroNota->getCurrent()->nota) / 100 * $avaliacaoTO->getNu_valor()));
                $aaTO->setDt_avaliacao($avaliacaoAgendamento[0]['dt_agendamento']);
                $this->salvarAvaliacaoAluno($aaTO);
                $arrayNota['nota'] = $aaTO->getSt_nota();
                $arrayNota['notaPercentual'] = $avaliacaoTO->getNu_valor();
                return $this->mensageiro->setMensageiro($arrayNota, Ead1_IMensageiro::SUCESSO);
            } else {
                return $mensageiroNota;
            }
        } else {
            return $mensageiroRespostas;
        }
    }

    /**
     * Método que verifica se o aluno está apto a prova final ou não
     * Essa marcacao é feita na tb_matricula
     * @see AvaliacaoBO::salvarAvaliacaoAluno
     * @param AvaliacaoAlunoTO $aaTO
     * @return bool
     * @moved \G2\Negocio\Avaliacao.php::verificaProvaFinal
     */
    public function verificaProvaFinal(AvaliacaoAlunoTO $aaTO)
    {
        $ng_avaliacao = new \G2\Negocio\Avaliacao();
        $matricula = $ng_avaliacao->find('\G2\Entity\Matricula', $aaTO->getId_matricula());
        return $ng_avaliacao->verificaProvaFinal($matricula);
    }

    /**
     * Método responsavel por alterar a situacao de uma disciplina de TCC
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     * @param VwAlunoGradeIntegracaoTO $vwAlunoGradeIntegracaoTO
     * @return booleam
     */
    public function alteraSituacaoTCC(AlocacaoTO $alocacaoTO)
    {

        $mensageiro = new Ead1_Mensageiro();

        try {
            $salaDeAulaDAO = new SalaDeAulaDAO();
            $where = "id_alocacao = " . $alocacaoTO->getId_alocacao();
            $salaDeAulaDAO->editarAlocacao($alocacaoTO, $where);

            $boT = new \G2\Negocio\Tramite();
            $st_tramite = 'TCC Enviado.';
            $retorno = $boT->salvarTramiteTCC($st_tramite);
//                    Zend_Debug::dump($retorno);

            if ($retorno->getId()) {
                $boT->salvarTramiteLiberarTCC($retorno->getId(), (int)$alocacaoTO->getId_alocacao(), AlocacaoTO::SITUACAOTCC_NAOAPTO);
            }

            $mensageiro->addMensagem('TCC enviado com sucesso');
            $this->mensageiro = $mensageiro;
        } catch (Zend_Exception $e) {
            $mensageiro->addMensagem('Erro ao tentar salvar a situacao do TCC do aluno!');
            $this->mensageiro = $mensageiro;
        }

        return $this->mensageiro;
    }

    /**
     * Método responsavel por fazer o envio do email para o aluno com mensagem
     * de TCC enviado.
     * @param VwAlunosSalaTO $to
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function enviarEmailEnvioTcc(VwAlunosSalaTO $to)
    {
        try {

            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::RECIBO_TCC_ALUNO, $to, TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }
            $id_professor = $to->getId_professor();
            if (!empty($id_professor)) {
                $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::RECIBO_TCC, $to, TipoEnvioTO::EMAIL, true);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiro->getFirstMensagem());
                }
            }

            $this->mensageiro->setMensageiro('Email Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $mensageiro->getFirstMensagem());
        }

        return $this->mensageiro;
    }

    /**
     * Método para sincronizar notas vindas do moodle por url.
     * (atenção para erro de "String data, right truncation"
     * provavelmente será por causa do campo id_avaliacaoconjuntoreferencia
     * por ter o nome muito extenso (não acontece no servidor)
     *
     * Débora Castro - debora.castro@unyleya.com.br
     * AC-570 - [21-11-2013]
     * @param $parametros
     * @return Ead1_Mensageiro
     */
    public function sincronizaNotaDoMoodle($parametros)
    {
        $retorno = \NULL;
        $array_to = array();

        try {
            if (empty($parametros['idSala']) || empty($parametros['idUsuario'])
//                || empty($parametros['idEntidade'])
            ) {
                throw new InvalidArgumentException('Faltam parametros obrigatórios para a sincronização de nota Moodle', 500);
            }

            $negocio = new G2\Negocio\Negocio();
            $result = $negocio->findOneBy('\G2\Entity\VwAlunoGradeIntegracao', array(
                'st_codsistemacurso' => $parametros['idSala'],
                'st_codusuario' => $parametros['idUsuario'],
//                'id_entidade' => $parametros['idEntidade'],
                'id_aproparcial' => false
            ));


            // Fim alteracoes AC-27088
            if ($result instanceof \G2\Entity\VwAlunoGradeIntegracao) {

                /** @var \G2\Entity\SalaDeAula $salaDeAula */
                $salaDeAula = $negocio->find(\G2\Entity\SalaDeAula::class, $result->getId_saladeaula());

                if (!$salaDeAula) {
                    throw  new Zend_Exception("Sala de aula não encontrada.");
                }

//                seta o id_entidade da sala
                $parametros['idEntidade'] = $salaDeAula->getId_entidade();

                $ws_moodle = new MoodleAlocacaoWebServices($parametros['idEntidade']);

                $portalBo = new PortalSalaDeAulaBO();
                $notasMoodle = $portalBo->retornaAlunosNotaSincronizacao(
                    $parametros['idSala'],
                    $result->getId_saladeaula(),
                    $parametros['idEntidade'],
                    $parametros['idUsuario']
                );


                if ($notasMoodle->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                    return $notasMoodle;
                }

                $notas = $notasMoodle->getMensagem();
                $array_to = array();
                foreach ($notas as $nota) {
                    if ($result->getId_matricula() == $nota->id_matricula && !empty($nota->st_nota)) {
                        $vw = new VwAvaliacaoAlunoTO();
                        $vw->setId_matricula($nota->id_matricula);
                        $vw->setId_avaliacao($nota->id_avaliacao);
                        $vw->setId_avaliacaoconjuntoreferencia($nota->id_avaliacaoconjuntoreferencia);
                        $vw->setSt_notaava($nota->st_notaava);

                        $array_to[] = $vw;
                    }
                }


                if (!empty($array_to)) {
                    $sincroniza = $ws_moodle->sincronizarNotas($array_to,
                        $result->getId_usuario() ? $result->getId_usuario() : $parametros['idUsuario']
                    );
                    $retorno = $sincroniza;
                } else {
                    $retorno = new Ead1_Mensageiro("Sem notas para sincronizar.", Ead1_IMensageiro::SUCESSO);
                }
            }

        } catch (Zend_Exception $e) {
            throw $e;
        } catch (InvalidArgumentException $e) {
            $retorno = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $retorno;
    }

}

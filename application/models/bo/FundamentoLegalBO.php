<?php 
/**
 * Classe com regras de negocio para interacoes de Fundamento Legal
 * @author Eduardo Romao - ejushiro@gmail.com
 * @since 11/11/2010
 * 
 * @package models
 * @subpackage bo
 */
class FundamentoLegalBO extends SecretariaBO{
	
	public $mensageiro;
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new FundamentoLegalDAO();
	}
	
	/**
	 * Metodo que cadastra Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFundamentoLegal(FundamentoLegalTO $flTO){
		$flTO->setId_usuariocadastro($flTO->getSessao()->id_usuario);
		if(!$flTO->getId_entidade()){
			$flTO->setId_entidade($flTO->getSessao()->id_entidade);
		}
		if(!$this->comparaData($flTO->getDt_publicacao(),$flTO->getDt_vigencia())){
			return $this->mensageiro->setMensageiro('A Data de Publicação não Pode ser Maior que a Data de Vigência!',Ead1_IMensageiro::AVISO);
		}
		$newFlTO = new VwFundamentoLegalTO();
		$newFlTO->setId_entidade($flTO->getId_entidade());
		$newFlTO->setBl_ativo(1);
		$newFlTO->setId_tipofundamentolegal($flTO->getId_tipofundamentolegal());
		$verificador = $this->retornarFundamentoLegal($newFlTO);
		if(!is_object($verificador)){
			return $this->mensageiro->setMensageiro('Erro ao Verificar Tipo do Fundamento Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		if($verificador->getTipo() == Ead1_IMensageiro::SUCESSO){
			return $this->mensageiro->setMensageiro('Só pode Haver 1 Tipo de Portaria e Fundamentação Legal!',Ead1_IMensageiro::AVISO);
		}
		$dadosFundamentolegal = $this->dao->cadastrarFundamentoLegal($flTO);
		if(!$dadosFundamentolegal){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Fundamento Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$flTO->setId_fundamentolegal($dadosFundamentolegal->id_fundamentolegal);
		$dadosFL[0] = $dadosFundamentolegal->toArray();
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dadosFL, new VwFundamentoLegalTO(),true),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Edita Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFundamentoLegal(FundamentoLegalTO $flTO){
		if(!$this->comparaData($flTO->getDt_publicacao(),$flTO->getDt_vigencia())){
			return $this->mensageiro->setMensageiro('A Data de Publica√ß√£o n√£o Pode Ser Maior que a Data de Vig√™ncia!',Ead1_IMensageiro::AVISO);
		}
		if(Ead1_TO_Dinamico::is_boolean($flTO->getBl_ativo())){
			$newFlTO = new VwFundamentoLegalTO();
			$newFlTO->setId_entidade($flTO->getId_entidade());
			$newFlTO->setBl_ativo(1);
			$newFlTO->setId_tipofundamentolegal($flTO->getId_tipofundamentolegal());
			$verificador = $this->retornarFundamentoLegal($newFlTO);
			if(!is_object($verificador)){
				return $this->mensageiro->setMensageiro('Erro ao Verificar Tipo do Fundamento Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
			}
			$verificador = $verificador->toArray();
			if(!empty($verificador) && $verificador[0]->getId_fundamentolegal() != $flTO->getId_fundamentolegal()){
				return $this->mensageiro->setMensageiro('S√≥ pode Haver 1 Tipo de Portaria e Fundamenta√ß√£o Legal!',Ead1_IMensageiro::AVISO);
			}
		}
		if(!$this->dao->editarFundamentoLegal($flTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar o Fundamento Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Fundamento Legal Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo Que Exclui o Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFundamentoLegal(FundamentoLegalTO $flTO){
		if(!$this->dao->deletarFundamentoLegal($flTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Fundamento Legal!',Ead1_IMensageiro::ERRO,$this->excecao);
		}
		return $this->mensageiro->setMensageiro('Fundamento Legal Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFundamentoLegal(VwFundamentoLegalTO $flTO){
		if(!$flTO->getId_entidade()){
			$flTO->setId_entidade($flTO->getSessao()->id_entidade);
		}
		$fundamento = $this->dao->retornarFundamentoLegal($flTO);
		if(is_object($fundamento)){
			$fundamento = $fundamento->toArray();
		}else{
			unset($fundamento);
		}
		if(!$fundamento){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($fundamento, new VwFundamentoLegalTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Tipo do Fundamento Legal
	 * @param TipoFundamentoLegalTO $tflTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoFundamentoLegal(TipoFundamentoLegalTO $tflTO){
		$tipo = $this->dao->retornarTipoFundamentoLegal($tflTO);
		if(is_object($tipo)){
			$tipo = $tipo->toArray();
		}else{
			unset($tipo);
		}
		if(!$tipo){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo, new TipoFundamentoLegalTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Método que retorna o fundamento legal da matricula
	 * @param VwFundamentoMatriculaTO $to
	 * @return Ead1_Mensageiro 
	 */
	public function retornarVwFundamentoMatricula(VwFundamentoMatriculaTO $to){
		try{
			$dados = $this->dao->retornarVwFundamentoMatricula($to)->toArray();
			if(empty($dados)){
				THROW new Zend_Validate_Exception("Nenhum Registro Encontrado.");
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwFundamentoMatriculaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar Fundamentação Legal.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
}
<?php

/**
 * Classe com regras de negócio para interações de Matricula e Contrato
 * @author Eder Lamar Mariano edermariano@gmail.com
 * @since 26/07/2011
 *
 * @package models
 * @subpackage bo
 */
class ContratoBO extends Ead1_BO
{

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * @var MatriculaDAO
     */
    public $dao;

    /**
     * Método Construtor
     */
    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new MatriculaDAO();
    }

    /**
     * Gera o contrato de um aluno com seus responsáveis financeiros, o reponsável pedagógico e suas matrículas
     * Após a geração do contrato é gerado uma venda para aquele contrato inserido
     * @param ContratoTO $contratoTO
     * @param array $arContratoResponsavelFinanceiroTO
     * @param ContratoResponsavelTO $contratoResponsavelPedagogico
     * @param array $arMatriculaTO
     * @param array $arLancamentoTO
     * @param ContratoRegraTO $contratoRegraTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoMatricula(ContratoTO $contratoTO, $arContratoResponsavelFinanceiroTO, ContratoResponsavelTO $contratoResponsavelPedagogico, $arMatriculaTO, $arSeries, $arLancamentoTO, ContratoRegraTO $contratoRegraTO, $arVwTurmasDisponiveis = null)
    {
        $editando = false;
        $matriculaBO = new MatriculaBO();
        $labelContrato = Ead1_LabelFuncionalidade::getLabel(187);
        $this->dao->beginTransaction();
        try {
            if (!$contratoTO->getId_contrato()) {
                $contratoTO = $this->cadastrarContrato($contratoTO);
                $contratoTO->setId_venda($matriculaBO->cadastrarVendaContrato($contratoTO, $this->somatorioValorLiquidoVendaProduto($arMatriculaTO, $contratoRegraTO)));
                if ($contratoTO->getId_venda() == false) {
                    throw new DomainException('Não foi possível gerar uma venda para ' . $labelContrato);
                }
            } else {
                if ($contratoTO->getId_situacao() == ContratoTO::SITUACAO_PENDENTE) {
                    $this->deletarVinculosContrato($contratoTO);
                }
                $this->editarContrato($contratoTO);
                $editando = true;
                if (!$matriculaBO->editarVendaContrato($contratoTO, $this->somatorioValorLiquidoVendaProduto($arMatriculaTO, $contratoRegraTO))) {
                    throw new DomainException('Não foi possível editar a venda de ' . $labelContrato);
                }
                $this->deletarVendaProduto($contratoTO->getId_venda());
            }
            $contratoResponsavelPedagogico->setId_contrato($contratoTO->getId_contrato());
            $contratoResponsavelPedagogico->setId_contratoresponsavel(null);
            $contratoResponsavelPedagogico->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_PEDAGOGICO);
            if (!$matriculaBO->salvarContratoResponsavel($contratoResponsavelPedagogico)) {
                throw new DomainException('Não foi possível vincular o responsável pedagógico ao ' . $labelContrato);
            }

            $this->cadastrarArResponsaveisFinanceiros($arContratoResponsavelFinanceiroTO, $contratoTO);

            if ($arMatriculaTO) {
                $this->cadastrarArMatriculas($arMatriculaTO, $arSeries, $contratoTO, $contratoRegraTO, $arVwTurmasDisponiveis);
            }

            $this->dao->commit();
            if ($editando) {
                return new Ead1_Mensageiro("$labelContrato editado com sucesso.", Ead1_IMensageiro::SUCESSO);
            }
            return new Ead1_Mensageiro($contratoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            $editandoCadastrando = ($editando ? "editar" : "cadastrar");
            return new Ead1_Mensageiro("Erro ao $editandoCadastrando $labelContrato. ERRO: {$e->getMessage()}", Ead1_IMensageiro::ERRO, $e);
        }
    }

    private function deletarVendaProduto($id_venda)
    {
        $vpTO = new VendaProdutoTO();
        $vpTO->setId_venda($id_venda);
        $produtoBO = new ProdutoBO();
        $mensageiro = $produtoBO->retornarVendaProduto($vpTO);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $mensagem = $mensageiro->getMensagem();
            foreach ($mensagem as $vendaProdutoTO) {
                $matriculaDAO = new MatriculaDAO();
                $matriculaDAO->deletarVendaProdutoNivelSerie(new VendaProdutoNivelSerieTO(array('id_vendaproduto' => $vendaProdutoTO->getId_vendaproduto())));
            }
            $produtoDAO = new produtoDAO();
            $produtoDAO->deletarVendaProduto($vpTO);
        } elseif ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
            throw new Zend_Exception($mensageiro->getMensagem());
        }
    }

    /**
     * Método para cadastrar o contrato
     * @param ContratoTO $contratoTO
     */
    public function cadastrarContrato(ContratoTO $contratoTO)
    {
        $matriculaBO = new MatriculaBO();
        $contratoTO->setBl_ativo(1);
        $contratoTO->setId_entidade(($contratoTO->getId_entidade() ? $contratoTO->getId_entidade() : $contratoTO->getSessao()->id_entidade));
        $contratoTO->setId_contrato((int)$matriculaBO->cadastrarContrato($contratoTO));

        if ($contratoTO->getId_contrato() == false) {
            throw new Zend_Exception('Não foi possível gerar o contrato');
        }

        return $contratoTO;
    }

    /**
     * Método que cadastra os responsáveis financeiros através de um array e analisa os dados
     * @param unknown_type $arContratoResponsavelFinanceiroTO
     */
    public function cadastrarArResponsaveisFinanceiros($arContratoResponsavelFinanceiroTO, ContratoTO $contratoTO)
    {
        if (count($arContratoResponsavelFinanceiroTO) === 0) {
            throw new InvalidArgumentException('Deve haver ao menos um reponsável financeiro para geração do contrato');
        }
        $matriculaBO = new MatriculaBO();
        $somaPorcentagemFinanceiro = 0;

        foreach ($arContratoResponsavelFinanceiroTO as $contratoResponsavel) {
            if (!($contratoResponsavel instanceof ContratoResponsavelTO)) {
                throw new InvalidArgumentException('O reponsável financeiro informado na lista não é do tipo ContratoResponsavelTO ');
            }

            if ($contratoResponsavel->getId_tipocontratoresponsavel() != TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO) {
                throw new InvalidArgumentException('O tipo do responsável financeiro informado está incorreto ');
            }

            $contratoResponsavel->setId_contrato($contratoTO->getId_contrato());

            if (!$contratoResponsavel->getId_contratoresponsavel()) {
                $where = 'id_contrato = ' . $contratoTO->getId_contrato()
                    . ' AND id_tipocontratoresponsavel = ' . TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO
                    . ' AND id_usuario = ' . $contratoResponsavel->getId_usuario();

                if ($matriculaBO->retornarContratoResponsavel($contratoResponsavel, $where)->getTipo() === Ead1_IMensageiro::SUCESSO) {
                    throw new DomainException('Não é possível adicionar responsáveis financeiros duplicados ');
                }
            }

            $contratoResponsavel->setId_contratoresponsavel(null);

            if ($matriculaBO->salvarContratoResponsavel($contratoResponsavel) === false) {
                throw new DomainException('Não foi possível vincular o responsável financeiro ao contrato');
            }

            $somaPorcentagemFinanceiro += $contratoResponsavel->nu_porcentagem;

        }

        if ($somaPorcentagemFinanceiro < 100.00 || $somaPorcentagemFinanceiro > 100.00) {
            throw new DomainException('A soma das porcentagens dos responsáveis financeiros não corresponde a 100%');
        }
    }

    /**
     * Método que soma os valores das matrículas para gerar o valor da venda
     * @param array $arMatriculaTO
     * @param ContratoRegraTO $contratoRegraTO
     * @return mixed
     */
    private function somatorioValorLiquidoVendaProduto($arMatriculaTO, ContratoRegraTO $contratoRegraTO)
    {
        $valorLiquido = 0;
        $matriculaBO = new MatriculaBO();
        if (!empty($arMatriculaTO)) {
            foreach ($arMatriculaTO as $matriculaTO) {
                if (!($matriculaTO instanceof MatriculaTO)) {
                    throw new InvalidArgumentException('O item da lista de matrículas não é do tipo MatriculaTO');
                }
                $produtoProjetoPedagogicoROW = $matriculaBO->retornarProdutoProjetoPedagogico($matriculaTO->id_projetopedagogico, $matriculaTO->id_entidadematricula);
                if ($produtoProjetoPedagogicoROW === null) {
                    throw new DomainException('Para o projeto pedagógico selecionado não existe produto vinculado');
                }
                $valorLiquido += ($produtoProjetoPedagogicoROW->nu_valor + ($produtoProjetoPedagogicoROW->nu_valormensal * $contratoRegraTO->nu_mesesmensalidade));

                $vwProdutoTaxaProjetoTO = new VwProdutoTaxaProjetoTO();
                $vwProdutoTaxaProjetoTO->setId_taxa(TaxaTO::MATRICULA);
                $vwProdutoTaxaProjetoTO->setId_projetopedagogico($matriculaTO->id_projetopedagogico);

                $vendaBO = new VendaBO();
                $mensageiroProdutoTaxa = $vendaBO->retornarVwProdutoTaxaProjeto($vwProdutoTaxaProjetoTO);
                if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::ERRO) {
                    throw new DomainExcetion($mensageiroProdutoTaxa->getMensagem());
                } else if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $mensagem = $mensageiroProdutoTaxa->getMensagem();
                    $produtoTaxaProjetoROW = $mensagem[0];
                    $valorLiquido += $produtoTaxaProjetoROW->nu_valor;
                }
            }
        }

        return $valorLiquido;
    }

    /**
     * Método que pega o Array de matrículas e realiza as validações para inserir
     * @param array $arMatriculaTO
     * @param array $arSeries
     * @param ContratoTO $contratoTO
     * @param ContratoRegraTO $contratoRegraTO
     * @param array $arVwTurmasDisponiveis
     * @param mixed $editando
     */
    public function cadastrarArMatriculas($arMatriculaTO, $arSeries, ContratoTO $contratoTO, ContratoRegraTO $contratoRegraTO, $arVwTurmasDisponiveis = null)
    {
        $matriculaBO = new MatriculaBO();
        foreach ($arMatriculaTO as $matriculaTO) {
            if (!($matriculaTO instanceof MatriculaTO)) {
                throw new InvalidArgumentException('O item da lista de matrículas não é do tipo MatriculaTO');
            }
            $produtoProjetoPedagogicoROW = $matriculaBO->retornarProdutoProjetoPedagogico($matriculaTO->id_projetopedagogico, $matriculaTO->id_entidadematricula);

            if ($produtoProjetoPedagogicoROW === null) {
                throw new DomainException('Para o projeto pedagógico selecionado não existe produto vinculado');
            }
            $vwTurmasDisponiveisTO = new VwTurmasDisponiveisTO();
            if (!empty($arVwTurmasDisponiveis)) {
                foreach ($arVwTurmasDisponiveis as $turmaDisponivel) {
                    if ($turmaDisponivel instanceof VwTurmasDisponiveisTO) {
                        if ($turmaDisponivel->id_projetopedagogico == $matriculaTO->id_projetopedagogico) {
                            $vwTurmasDisponiveisTO = $turmaDisponivel;
                        }
                    }
                }
            }

            $this->cadastrarProdutoNaVenda($matriculaTO, $arSeries, $contratoTO, $contratoRegraTO, $produtoProjetoPedagogicoROW, $vwTurmasDisponiveisTO);

            $vwProdutoTaxaProjetoTO = new VwProdutoTaxaProjetoTO();
            $vwProdutoTaxaProjetoTO->setId_taxa(TaxaTO::MATRICULA);
            $vwProdutoTaxaProjetoTO->setId_projetopedagogico($matriculaTO->id_projetopedagogico);
            $vwProdutoTaxaProjetoTO->setId_entidade($matriculaTO->getSessao()->id_entidade);

            $vendaBO = new VendaBO();
            $mensageiroProdutoTaxa = $vendaBO->retornarVwProdutoTaxaProjeto($vwProdutoTaxaProjetoTO);
            if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new DomainExcetion($mensageiroProdutoTaxa->getMensagem());
            } else if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensagem = $mensageiroProdutoTaxa->getMensagem();
                $produtoTaxaProjetoROW = $mensagem[0];
                $vwTurmasDisponiveisTO->setId_turma(null);
                $this->cadastrarProdutoNaVenda($matriculaTO, $arSeries, $contratoTO, $contratoRegraTO, $produtoTaxaProjetoROW, $vwTurmasDisponiveisTO);
            }
        }
    }

    public function cadastrarProdutoNaVenda($matriculaTO, $arSeries, ContratoTO $contratoTO, ContratoRegraTO $contratoRegraTO, $produtoProjetoPedagogicoROW, $vwTurmasDisponiveisTO)
    {
        $matriculaBO = new MatriculaBO();
//			[Verificando se o produto da venda j's não foi cadastrado]
        $ConsultaVendaProdutoORM = new VendaProdutoORM();
        $vendaProdutoTO = $ConsultaVendaProdutoORM->consulta(new VendaProdutoTO(array('id_venda' => $contratoTO->getId_venda(), 'id_produto' => $produtoProjetoPedagogicoROW->id_produto)), false, true);
        if ($vendaProdutoTO) {
            $this->dao->deletarVendaProdutoNivelSerie(new VendaProdutoNivelSerieTO(array('id_vendaproduto' => $vendaProdutoTO->getId_vendaproduto())));
            if ($arSeries) {
                $this->cadastrarVendaProdutoSerieNivel($arSeries, $matriculaTO->getId_projetopedagogico(), $vendaProdutoTO->getId_vendaproduto());
            }
// 				continue;
        }
        $vendaProduto = new VendaProdutoTO();
        $vendaProduto->id_produto = $produtoProjetoPedagogicoROW->id_produto;
        $vendaProduto->id_venda = $contratoTO->getId_venda();
        $vendaProduto->nu_valorbruto = ($produtoProjetoPedagogicoROW->nu_valor + ($produtoProjetoPedagogicoROW->nu_valormensal * $contratoRegraTO->nu_mesesmensalidade));
        $vendaProduto->nu_valorliquido = $vendaProduto->getNu_valorbruto();
        $vendaProduto->nu_desconto = 0.00;
        $vendaProduto->id_turma = $vwTurmasDisponiveisTO->id_turma;

        $vendaProduto->setId_matricula($matriculaTO->getId_matricula());

        $idVendaProdutoProjetoPedagogico = $matriculaBO->cadastrarVendaProdutoProjetoPedagogico($vendaProduto);


        if ($arSeries) {
            $this->cadastrarVendaProdutoSerieNivel($arSeries, $matriculaTO->getId_projetopedagogico(), $idVendaProdutoProjetoPedagogico);
        }


        if ($idVendaProdutoProjetoPedagogico == false) {
            throw new DomainException('Não foi possível vincular o produto do projeto pedagógico com a venda');
        }
    }

    /**
     * Método que realiza o cadastramento na tb_vendaprodutonivelserie
     * @param array $arSeries
     * @param int $idProjetoPedagogico
     * @param int $idVendaProduto
     */
    public function cadastrarVendaProdutoSerieNivel($arSeries, $idProjetoPedagogico, $idVendaProduto)
    {
        if (isset($arSeries[$idProjetoPedagogico])) {
            $arSeriesAtual = $arSeries[$idProjetoPedagogico];
            foreach ($arSeriesAtual as $serie) {
                if ($serie) {
                    if ($idProjetoPedagogico == $serie->id_projetopedagogico) {
                        $vendaProdutoNivelSerieTO = new VendaProdutoNivelSerieTO();
                        $vendaProdutoNivelSerieTO->setId_nivelensino($serie->id_nivelensino);
                        $vendaProdutoNivelSerieTO->setId_projetopedagogico($serie->id_projetopedagogico);
                        $vendaProdutoNivelSerieTO->setId_serie($serie->id_serie);
                        $vendaProdutoNivelSerieTO->setId_vendaproduto($idVendaProduto);

                        $matriculaBO = new MatriculaBO();
                        if ($matriculaBO->cadastrarVendaProdutoNivelSerie($vendaProdutoNivelSerieTO) == false) {
                            throw new DomainException('Não foi possível vincular a venda do produto com a série.');
                        }
                    }
                }
            }
        }
    }

    /**
     * Metodo que Edita Contrato
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function editarContrato(ContratoTO $contratoTO)
    {
        try {
            $contratoTO->setId_entidade(($contratoTO->getId_entidade() ? $contratoTO->getId_entidade() : $contratoTO->getSessao()->id_entidade));
            $this->dao->editarContrato($contratoTO);
            return $this->mensageiro->setMensageiro('Contrato Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método para Extender o Prazo de um Contrato
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     * @see ContratoRO::solicitarExtensaoContrato()
     */
    public function solicitarExtensaoContrato(ContratoTO $contratoTO)
    {

        $this->dao->beginTransaction();
        try {

            if ($contratoTO->getId_contrato() == '') {
                throw new Zend_Exception('ID do Contrato não informado.');
            }

            $taxaTO = new TaxaTO();
            $taxaTO->setId_taxa(1);

            $vendaBO = new VendaBO();
            $vendaBO->salvarVendaContratoProdutoTaxa($contratoTO, $taxaTO);


            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Solicitação de extensão de prazo salva com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao Solicitar uma extensão de prazo de Contrato: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }


    /**
     * Extende um Contrato
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function extenderContrato(ContratoTO $contratoTO)
    {

        try {
            $this->dao->beginTransaction();

            $contratoRegraTO = new ContratoRegraTO();
            $contratoRegraTO->setId_contratoregra($contratoTO->getId_contratoregra());
            $contratoRegraTO->fetch(true, true, true);

            if (!$contratoRegraTO->getNu_tempoextensao()) {
                throw new Zend_Exception("A quantidade de dias para extensão de um Contrato nào foi definida!");
            }

            $contratoTO->setDt_termino($contratoTO->getDt_termino()->addDay($contratoRegraTO->getNu_tempoextensao()));
            $contratoBO = new ContratoBO();
            $contratoBO->editarContrato($contratoTO);
            $contratoTO->fetch(true, true, true);

            $this->dao->commit();

            return new Ead1_Mensageiro("Contrato Estendido com Sucesso!", Ead1_IMensageiro::SUCESSO, $contratoTO);

        } catch (Zend_Exception $e) {

            $this->dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro("Erro ao Estender um Contrato " . $e->getMessage(), Ead1_IMensageiro::ERRO);

        }


    }

    /**
     * Método que Solicita a Alteração de um contrato baseando-se em uma taxa
     * @param ContratoTO $contratoTO
     * @param TaxaTO $taxaTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     * @see ContratoRO::solicitarAlteracaoContratoTaxa()
     */
    public function solicitarAlteracaoContratoTaxa(ContratoTO $contratoTO, TaxaTO $taxaTO)
    {

        $this->dao->beginTransaction();
        try {

            if ($contratoTO->getId_contrato() == '') {
                throw new Zend_Exception('ID do Contrato não informado.');
            }

            if ($taxaTO->getId_taxa() == '') {
                throw new Zend_Exception('ID da Taxa não informado.');
            }

            $vendaBO = new VendaBO();
            $vendaBO->salvarVendaContratoProdutoTaxa($contratoTO, $taxaTO);


            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Solicitação salva com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao Solicitar uma alteração de Contrato por taxa: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }


    }


    /**
     * Método que executa os processos de Atualização do Contrato Antigo no caso de uma Renovação
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function processoVerificaAlteraRenovacaoContrato(ContratoTO $contratoTO)
    {
        $this->dao->beginTransaction();

        try {
            $vendaProduto = new VendaProdutoTO();
            $vendaProduto->setId_venda($contratoTO->getId_venda());
            $vendaProdutoORM = new VendaProdutoORM();

            $vendaProdutoTO = $vendaProdutoORM->consulta($vendaProduto, false, true);

            $contratoMatriculaORM = new ContratoMatriculaORM();

            $rsContratoMatricula = $contratoMatriculaORM->fetchRow(array('id_matricula = ' . $vendaProdutoTO->getid_matricula(), 'id_contrato != ' . $contratoTO->getid_contrato()));

            if (!empty($rsContratoMatricula)) {
                $contratoMatriculaTO = new ContratoMatriculaTO($rsContratoMatricula->toArray());
                $oldContratoTO = new ContratoTO();
                $oldContratoTO->setId_contrato($contratoMatriculaTO->getId_contrato());
                $oldContratoTO->setId_situacao(ContratoTO::SITUACAO_RENOVADO);

                $oldContratoBO = new ContratoBO();
                $oldContratoBO->editarContrato($oldContratoTO);

                $contratoMatriculaTO->setBl_ativo(false);
                $matriculaBO = new MatriculaBO();
                $matriculaBO->editarContratoMatricula($contratoMatriculaTO);

                $novoContratoMatriculaTO = new ContratoMatriculaTO();
                $novoContratoMatriculaTO->setId_contrato($contratoTO->getid_contrato());
                $novoContratoMatriculaTO->setId_matricula($vendaProdutoTO->getid_matricula());
                $novoContratoMatriculaTO->setBl_ativo(true);
                $matriculaBO->cadastrarContratoMatricula($novoContratoMatriculaTO);

                $this->dao->commit();
                return $this->mensageiro->setMensageiro('Contrato Antigo marcado como RENOVAD0!', Ead1_IMensageiro::SUCESSO);

            } else {
                $this->dao->rollBack();
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Contrato antigo encontrado!', Ead1_IMensageiro::AVISO);
            }

        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao tentar Inativar o Contrato antigo!', Ead1_IMensageiro::ERRO);
        }


    }


    /**
     * Método que elimina os vínculos relacionados ao contrato para realizar uma edição
     * @param ContratoTO $contratoTO
     */
    private function deletarVinculosContrato(ContratoTO $contratoTO)
    {
        $this->deletarContratoMatricula($contratoTO);
        $this->deletarContratoResponsavel($contratoTO);
        $this->deletarLancamentos($contratoTO);
    }

    /**
     * Método que deleta os Responsáveis do contrato
     * @param ContratoTO $contratoTO
     */
    public function deletarContratoResponsavel(ContratoTO $contratoTO)
    {
        $matriculaBO = new MatriculaBO();
        $contratoResponsavelDelete = new ContratoResponsavelTO();
        $contratoResponsavelDelete->setId_contrato($contratoTO->getId_contrato());

        $respostaDelete = $matriculaBO->deletarContratoResponsavel($contratoResponsavelDelete);
        if ($respostaDelete->getTipo() != Ead1_IMensageiro::SUCESSO) {
            throw new DomainException('Erro ao excluir os Responsáveis Financeiros do Contrato. ' . $respostaDelete->getCodigo()->getMessage());
        }
    }

    /**
     * Método para excluir Matrículas do Contrato
     * @param ContratoTO $contratoTO
     */
    public function deletarContratoMatricula(ContratoTO $contratoTO)
    {
        $matriculaBO = new MatriculaBO();
        $contratoMatriculaTODelete = new ContratoMatriculaTO();
        $contratoMatriculaTODelete->setId_contrato($contratoTO->getId_contrato());

        $contratoMatriculaORM = new ContratoMatriculaORM();
        $contratoMatriculaTOs = $contratoMatriculaORM->consulta($contratoMatriculaTODelete);

        if ($matriculaBO->deletarContratoMatricula($contratoMatriculaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
            throw new DomainException('Erro ao excluir vínculos de matrícula ao Contrato.');
        }

        if ($contratoMatriculaTOs) {
            foreach ($contratoMatriculaTOs as $contratoMatriculaTO) {
                $matriculaTODelete = new MatriculaTO();
                $matriculaTODelete->setId_matricula($contratoMatriculaTO->getId_matricula());

                if ($matriculaBO->deletarMatricula($matriculaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new DomainException('Erro ao excluir matrícula do Contrato.');
                }
            }
        }
    }

    /**
     * Método que deleta os lançamentos da venda do contrato
     * @param ContratoTO $contratoTO
     */
    public function deletarLancamentos(ContratoTO $contratoTO)
    {
        $matriculaBO = new MatriculaBO();
        $vendaTODelete = new VendaTO();
        $vendaTODelete->setId_venda($contratoTO->getId_venda());
        $matriculaBO->deletarLancamento($vendaTODelete);
    }

    /**
     * Método que deleta a venda do contrato
     * @param ContratoTO $contratoTO
     */
    public function deletarVenda(ContratoTO $contratoTO)
    {
        $matriculaBO = new MatriculaBO();
        $vendaTODelete = new VendaTO();
        $vendaTODelete->setId_venda($contratoTO->getId_venda());

        if ($matriculaBO->deletarVenda($vendaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
            throw new DomainException('Erro ao excluir a Venda do Contrato.');
        }
    }
}
<?php

/**
 * Classe com regras de negócio para TipoAvaliacao
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-09-06
 * 
 * @package models
 * @subpackage bo
 */
class TipoAvaliacaoBO extends Ead1_BO
{

    public $mensageiro;

    /**
     *
     * @var Entity TipoAvaliacao 
     */
    protected $repositoryName = 'G2\Entity\TipoAvaliacao';

    public function __construct ()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * 
     * @param array $where
     * @param array $order
     * @param integer $limit
     * @param integer $offset
     * @return object
     */
    public function findBy ($where = array(), $order = array(), $limit = 100, $offset = 0)
    {
        return $this->em->getRepository($this->repositoryName)->findBy($where, $order, $limit, $offset);
    }

}
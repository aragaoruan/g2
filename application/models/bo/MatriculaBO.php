<?php

/**
 * Classe com regras de negócio para interações de Matricula
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 21/02/2011
 *
 * @package models
 * @subpackage bo
 */
class MatriculaBO extends Ead1_BO
{

    const SITUACAO_CONTRATO_ATIVO = 44;
    const SITUACAO_MATRICULA_ATIVA = 50;

    /*
     * Constantes relacionadas as situações possíveis de matrícula
     */
    /**
     * Favor usar a VendaTO::SITUACAO_VENDA_ATIVA
     * @var int
     */
    const SITUACAO_VENDA_ATIVA = 39;
    const EVOLUCAO_CONTRATO_CONFIRMADA = 4;
    const EVOLUCAO_MATRICULA_CURSANDO = 6;

    /*
     * Constantes relacionadas as evoluções  possíveis de matrícula
     */
    const EVOLUCAO_VENDA_CONFIRMADA = 10;
    const EVOLUCAO_MATRICULA_CERTIFICADA = 16;
    public $mensageiro;
    /**
     * @var MatriculaDAO
     */
    public $dao;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new MatriculaDAO();
        $this->negocio = new \G2\Negocio\Negocio();
    }

    /**
     * Metodo que verifica e confirma o recebimento do contrato do aluno.
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function confirmarRecebimento(ContratoTO $cTO)
    {
        try {
            $contratoORM = new ContratoORM();
            $contratos = $contratoORM->consulta($cTO);
            if ($contratos) {
                $cTO = $contratos[0];
            } else {
                throw new Zend_Exception(Ead1_LabelFuncionalidade::getLabel(159) . " não encontrado.", Ead1_IMensageiro::ERRO);
            }


            if (empty($cTO->getId_venda())) {
                throw new Zend_Exception("Contrato sem uma venda.", Ead1_IMensageiro::ERRO);
            }


            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($cTO->getId_venda());

            $vendaBO = new VendaBO();
            $this->mensageiro = $vendaBO->confirmaRecebimento($vendaTO);

            if ($this->mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($this->mensageiro->getMensagem(), Ead1_IMensageiro::ERRO);
            }

            return new Ead1_Mensageiro("Recebimento confirmado com sucesso!");
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), $e->getCode(), $this->dao->excecao);
        }
    }

    /**
     * Metodo que verifica se a matricula na entidade é automatica
     * Caso seja automatica valida e ativa a matricula
     * @param LancamentoTO $lancamentoTO
     * @throws Zend_Exception
     */
    public function verificaMatriculaAutomatica(LancamentoTO $lancamentoTO)
    {
//        $entidadeFinanceiroORM = new EntidadeFinanceiroORM();
//        $entidadefinanceiroTO = new EntidadeFinanceiroTO();
//        $entidadefinanceiroTO->setId_entidade($lancamentoTO->getId_entidade());
//        $entidadefinanceiroTO = $entidadeFinanceiroORM->consulta($entidadefinanceiroTO, false, true);
//        if ($entidadefinanceiroTO === false) {
//			THROW new Zend_Exception('A Entidade não tem Configuração Financeira! (tb_entidadefinanceiro)');
//            return false;
//        }

//        if (!$entidadefinanceiroTO->getBl_automatricula()) {
//            return;
//        }

        $esquemaNegocio = new \G2\Negocio\EsquemaConfiguracao();
        //busca os dados da configuração de acordo com os itens
        $resultConfig = $esquemaNegocio->retornarEsquemaValores(array(
            'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::CONFIRMAR_RECEBIMENTO_ATUOMATICO,
            'id_entidade' => $lancamentoTO->getId_entidade()
        ));
        $ativarAutomatico = true;
        if ($resultConfig->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
            $ativacao = $resultConfig->getFirstMensagem();
            $ativarAutomatico = boolval($ativacao['st_valor']);
        }

        if ($ativarAutomatico) {
            $this->ativarMatriculaAutomatica($lancamentoTO);
        }
    }

    /**
     * Metodo que verifica se Todos os Lançamentos foram pagos para ativar a matricula
     * @param LancamentoTO $lancamentoTO
     * @return bool
     */
    protected function ativarMatriculaAutomatica(LancamentoTO $lancamentoTO)
    {
        $vendaDAO = new VendaDAO();
        $lancamentoVendaORM = new LancamentoVendaORM();
        $lancamentoVendaTO = new LancamentoVendaTO();
        $lancamentoVendaTO->setId_lancamento($lancamentoTO->getId_lancamento());
        $lancamentoVendaTO = $lancamentoVendaORM->consulta($lancamentoVendaTO, false, true);
        if ($lancamentoVendaTO === false) {
//			THROW new Zend_Exception('Erro ao Verificar Lançamento da Venda!');
            return false;
        }
        $where = 'id_venda = ' . $lancamentoVendaTO->getId_venda();
        try {
            $lancamentos = $vendaDAO->retornarVendaLancamento(new VwVendaLancamentoTO(), $where)->toArray();
        } catch (Zend_Exception $e) {
            return false;
        }
        if (empty($lancamentos)) {
//			THROW new Zend_Exception('Nenhum Lançamento Encontrado para a venda!');
            return false;
        }
        $lancamentos = Ead1_TO_Dinamico::encapsularTo($lancamentos, new VwVendaLancamentoTO());
        $quitados = true;
        foreach ($lancamentos as $lancamento) {


            switch ($lancamento->getId_meiopagamento()) {
                case MeioPagamentoTO::CARTAO:
                    if (!$lancamento->getDt_prevquitado()) {
                        $quitados = false;
                    };
                    break;
                case MeioPagamentoTO::BOLETO:
                    if ($lancamento->getbl_quitado() == false && $lancamento->getBl_entrada() == true) {
                        $quitados = false;
                    }
                    break;
                default:
                    if (!$lancamento->getbl_quitado()) {
                        $quitados = false;
                    }
                    break;
            }
        }


        if ($quitados) {
            $contratoORM = new ContratoORM();
            $contratoTO = new ContratoTO();
            $contratoTO->setId_venda($lancamentoVendaTO->getId_venda());
            $contratoTO->setBl_ativo(true);
            $contratoTO = $contratoORM->consulta($contratoTO, false, true);
            if ($contratoTO === false) {
//				THROW new Zend_Exception('Não foi encontrado o Contrato da Venda!');
                return false;
            }
            try {
                $mensageiro = $this->procedimentoAtivarMatricula($contratoTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
//					THROW new Zend_Exception($mensageiro->mensagem[0]);
                    return false;
                }
            } catch (Zend_Exception $e) {
                return false;
            } catch (DomainException $e) {
                return false;
            } catch (Exception $e) {
                return false;
            }
        }
    }

    /**
     * Procedimento que ativa a Matricula do Aluno
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     * @throws DomainException
     * @return Ead1_Mensageiro
     */
    public function procedimentoAtivarMatricula(ContratoTO $contratoTO, $matricula = null, VendaTO $vendaTO = null)
    {
        try {
            $this->dao->beginTransaction();
            $this->_subProcessoAtivarMatriculaEditarContrato($contratoTO);
            $this->_subProcessoAtivarMatriculaEditarVenda($contratoTO);

            try {
                //verifica se a venda foi passada
                if (empty($vendaTO) || !$vendaTO->getId_venda()) {
                    $vendaTO = new VendaTO();
                    $vendaTO->setId_venda($contratoTO->getId_venda());
                    $vendaTO->fetch(false, true, true);
                }

                //se for do tipo renovação, gera o aditivo, senão cria uma nova matricula como de costume.
                if ($vendaTO->getBl_renovacao()) {
                    $this->gerarAditivo($contratoTO, $matricula, \G2\Constante\TipoAditivo::RENOVACAO);
                    $arrMatricula = array();
                    array_push($arrMatricula, new MatriculaTO($matricula));
                } else {
                    $arrMatricula = $this->_subProcessoAtivarMatriculaCadastrarMatricula($contratoTO);

                    /**
                     * Método utilizado para reabrir as ocorrências que estão fechadas, onde vai ser verificado se uma venda que tem um
                     * id_ocorrenciaaproveitamento.
                     * @author helder.silva <helder.silva@unyleya.com.br>
                     * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
                     */
                    $negocio = new \G2\Negocio\Aproveitamento();
                    $negocio->reabrirOcorrenciaParaAproveitamento($vendaTO);

                }


                /**
                 * Criar o ZendJob para o Alocar Automatico
                 */
                $job = new \ZendJobQueue();
                $job->createHttpJob('/default/robo/auto-alocar-matriculas', array(
                    'id_venda' => $contratoTO->getId_venda(),
                    'matriculas' => $arrMatricula
                ), array(
                        'name' => 'Auto Alocar.',
                        'schedule_time' => date('Y-m-d H:i:s', time() + 10)
                    )
                );

            } catch (Exception $e) {
                THROW new Zend_Exception($e->getMessage());
            }

            $this->dao->commit();
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao ativar matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
            return $this->mensageiro;
        } catch
        (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao ativar matrícula.', Ead1_IMensageiro::ERRO, $e->getMessage());
            return $this->mensageiro;
        }
        $this->mensageiro->setMensageiro('Matricula Ativada com Sucesso!');

        //Enviado Emails
        $msgEmail = '';
        try {
            $mensagemBO = new MensagemBO();
            $blEnviarEmail = false;
            foreach ($arrMatricula as $mTO) {

                /*
                 * Verifica se é projetopedagogico e se o bl_enviaremail do projeto é true
                 * */
                if ($mTO->getId_projetopedagogico()) {
                    $projetoPedagogico = $this->negocio->find('G2\Entity\ProjetoPedagogico', $mTO->getId_projetopedagogico());
                    if ($projetoPedagogico->getBl_enviaremail()) {
                        //caso seja renovação vai enviar uma mensagem de confirmação da renovação
                        if ($vendaTO->getBl_renovacao()) {
                            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_EMAIL_CONFIRMACAO_RENOVACAO, $mTO, TipoEnvioTO::EMAIL, false)->subtractMensageiro();
                        } else {
                            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::MATRICULA, $mTO, TipoEnvioTO::EMAIL, false)->subtractMensageiro();
                        }
                        $blEnviarEmail = true;
                    }
                } else {
                    $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::MATRICULA, $mTO, TipoEnvioTO::EMAIL, false)->subtractMensageiro();
                }

                //envia sms caso não seja renovação
                if (!$vendaTO->getBl_renovacao()) {
                    /**
                     * SMS
                     */
                    $entidadeIntegracao = new EntidadeIntegracaoTO();
                    $entidadeIntegracao->setId_sistema(SistemaTO::TILTSMS);
                    $entidadeIntegracao->setId_entidade($mTO->getId_entidadematricula());

                    $enti = new EntidadeBO();
                    $mei = $enti->retornaEntidadeIntegracao($entidadeIntegracao);
                    if ($mei->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $ei = $mei->getFirstMensagem();
                        if ($ei->getId_entidadeintegracao()) {

                            $vw = new PessoaTO();
                            $vw->setId_entidade($mTO->getId_entidadematricula());
                            $vw->setId_usuario($mTO->getid_usuario());
                            $vw->fetch(false, true, true);

                            /*
                             * Verifica se é projetopedagogico e se o bl_enviaremail é true
                             * */
                            if ($blEnviarEmail && $vw->getId_usuariocadastro()) {
                                $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO_SMS, $vw, TipoEnvioTO::SMS, false)->subtractMensageiro();
                            } elseif ($vw->getId_usuariocadastro()) {
                                $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO_SMS, $vw, TipoEnvioTO::SMS, false)->subtractMensageiro();
                            }
                        }
                    }
                    /**
                     * fim SMS
                     */
                }
            }
        } catch (Zend_Exception $e) {
            $msgEmail = $e->getMessage();
        }
        $this->mensageiro->setMensageiro('Matricula Ativada Com Sucesso! Envio de Email: ' . $msgEmail, Ead1_IMensageiro::SUCESSO);

        //Auto Alocar - MOVIDO PARA O ZEND JOB MAIS ACIMA
        /*
        try {
            foreach ($arrMatricula as $matriculaTO) {
                $ng_matricula = new \G2\Negocio\Matricula();
                $ng_matricula->alocarAutomatico($matriculaTO, true);
            }
        } catch (Zend_Exception $e) {
            throw new Exception($e->getMessage());
        }
        */

        return $this->mensageiro;
    }


    /**
     * SubProcesso do processo de ativar matricula que edita o contrato
     * @param ContratoTO $contratoTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     */
    private function _subProcessoAtivarMatriculaEditarContrato(ContratoTO $contratoTO)
    {
        //Setando nova situação e evolução do contrato
        $contratoTO->setId_situacao(MatriculaBO::SITUACAO_CONTRATO_ATIVO);
        $contratoTO->setId_evolucao(MatriculaBO::EVOLUCAO_CONTRATO_CONFIRMADA);
        //Setando data de ativação
        $contratoTO->setDt_ativacao(new Zend_Date(null, Zend_Date::ISO_8601));

        //Retornando regra do contrato
        $contratoRegraTO = new ContratoRegraTO();
        $contratoRegraTO->setId_contratoregra($contratoTO->getId_contratoregra());
        //$contratoRegraTO->setId_entidade($contratoTO->getId_entidade());
        $contratoRegra = $this->dao->retornarContratoRegra($contratoRegraTO)->toArray();

        if (empty($contratoRegra)) {
            THROW new Zend_Validate_Exception('O Contrato não Possui uma Regra Cadastrada!');
        }

        $contratoRegraTO = Ead1_TO_Dinamico::encapsularTo($contratoRegra, new ContratoRegraTO(), true);
        $date = new Zend_Date(null, Zend_Date::ISO_8601);
        //Verificando o tipo de duração do contrato
        $contratoTO->setDt_termino((
        $contratoRegraTO->getId_projetocontratoduracaotipo() == ProjetoContratoDuracaoTipoTO::DURACAO_DIAS ?
            $date->addDay($contratoRegraTO->getNu_contratoduracao()) :
            $date->addMonth($contratoRegraTO->getNu_contratoduracao())));

        $this->dao->editarContrato($contratoTO);
    }

    /**
     * SubProcesso de ativar matricula que edita a venda
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     */
    private function _subProcessoAtivarMatriculaEditarVenda(ContratoTO $contratoTO)
    {
        $produtoDAO = new ProdutoDAO();
        $vTO = new VendaTO();
        $vTO->setId_venda($contratoTO->getId_venda());
        $vTO->setId_situacao(MatriculaBO::SITUACAO_VENDA_ATIVA);
        $vTO->setId_evolucao(MatriculaBO::EVOLUCAO_VENDA_CONFIRMADA);
        $vTO->setDt_confirmacao(new Zend_Date());
        $produtoDAO->editarVenda($vTO);
    }

    /**
     * SubProcesso de Ativar Matricula que cadastra e vincula as matriculas e suas disciplinas
     * @param ContratoTO $contratoTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     * @return $arrMatricula | retorna um array de MatriculaTO
     */
    private function _subProcessoAtivarMatriculaCadastrarMatricula(ContratoTO $contratoTO)
    {
        $produtoDAO = new ProdutoDAO();
        $vendaDAO = new VendaDAO();
        //Verificando se existem produtos do tipo projeto para a venda
        $vendaProdutoORM = new VendaProdutoORM();

        $where = $vendaProdutoORM->select()
            ->distinct(true)
            ->from(array('vp' => 'tb_vendaproduto'), array('vp.id_venda', 'vp.id_vendaproduto', 'vp.id_turma'))
            ->joinInner(array('ppp' => 'tb_produtoprojetopedagogico'), 'ppp.id_produto = vp.id_produto', array('ppp.id_projetopedagogico', 'ppp.id_entidade', 'ppp.nu_tempoacesso'))
            ->joinInner(array('pp' => 'tb_projetopedagogico'), 'pp.id_projetopedagogico = ppp.id_projetopedagogico', array('pp.id_entidadecadastro'))
            ->setIntegrityCheck(false)
            ->where('vp.id_matricula is null and vp.id_venda = ' . $contratoTO->getId_venda())
            ->where('vp.bl_ativo = 1');
        $arrVendaProduto = $produtoDAO->retornarVendaProduto(new VendaProdutoTO(), $where)->toArray();
        //Zend_Debug::dump($arrVendaProduto);exit;

        if (empty($arrVendaProduto)) {
            THROW new Zend_Validate_Exception('O Contrato Não Possui Produtos do Tipo Projeto Pedagógico!');
        }

        try {
            //cadastrando Matriculas
            $arrMatricula = array();

            foreach ($arrVendaProduto as $produto) {
                $matriculaTO = new MatriculaTO();
                $matriculaTO->setId_usuario($contratoTO->getId_usuario());
                $matriculaTO->setId_projetopedagogico($produto['id_projetopedagogico']);
                $matriculaTO->setId_entidadematriz($produto['id_entidade']);
                $matriculaTO->setId_entidadeatendimento($contratoTO->getId_entidade());
                $matriculaTO->setId_entidadematricula($contratoTO->getId_entidade());
                $matriculaTO->setId_situacao(MatriculaBO::SITUACAO_MATRICULA_ATIVA);
                $matriculaTO->setId_evolucao(MatriculaBO::EVOLUCAO_MATRICULA_CURSANDO);
                $matriculaTO->setBl_ativo(true);
                $matriculaTO->setId_vendaproduto($produto['id_vendaproduto']);
                $matriculaTO->setId_usuariocadastro((empty($matriculaTO->getSessao()->id_usuario) ? $contratoTO->getId_usuario() : $matriculaTO->getSessao()->id_usuario));
                $matriculaTO->setDt_inicio(new Zend_Date(null, Zend_Date::ISO_8601));
                $matriculaTO->setId_turma($produto['id_turma']);
                $matriculaTO->setNu_semestreatual(1);
                //calcula a data futura
                if ($produto['nu_tempoacesso'] > 0 && $produto['nu_tempoacesso'] != NULL) {
                    $dt_termino = $this->getDataFutura($produto['nu_tempoacesso']);
                    $matriculaTO->setDt_termino($dt_termino);
                } else {
                    $matriculaTO->setDt_termino(null);
                }
                //Cadastrando Matricula
                $id_matricula = $this->dao->cadastrarMatricula($matriculaTO);


                /**
                 * Copia o Termo de Responsabilidade se houver
                 */
                $traDAO = new TramiteDAO();
                $traDAO->copiaTermoVendaMatricula($contratoTO->getId_venda(), $id_matricula);

                //Atualizando o VendaProduto
                $vendaProdutoORM->update(array('id_matricula' => $id_matricula), array('id_vendaproduto = ' . $produto['id_vendaproduto']));

                $matriculaTO->setId_matricula($id_matricula);
                $contratoMatriculaTO = new ContratoMatriculaTO();
                $contratoMatriculaTO->setId_contrato($contratoTO->getId_contrato());
                $contratoMatriculaTO->setId_matricula($matriculaTO->getId_matricula());

                //Vinculando Matricula oa Contrato
                $this->dao->cadastrarContratoMatricula($contratoMatriculaTO);

                //Cadastrando o Nivel e Serie da Matricula de Acordo com a Venda
                $arrVendaProdutoNivelSerie = $vendaDAO->retornarVendaProdutoNivelSerie(new VendaProdutoNivelSerieTO(
                        array(
                            'id_vendaproduto' => $matriculaTO->getId_vendaproduto(),
                            'id_projetopedagogico' => $matriculaTO->getId_projetopedagogico()
                        )
                    )
                )->toArray();


                //Vinculando Nivel e Seria a Matricula
                if (!empty($arrVendaProdutoNivelSerie)) {
                    foreach ($arrVendaProdutoNivelSerie as $vendaProdutoNivelSerie) {
                        $this->dao->cadastrarMatriculaNivelSerie(new MatriculaNivelSerieTO(
                                array('id_matricula' => $matriculaTO->getId_matricula(),
                                    'id_projetopedagogico' => $matriculaTO->getId_projetopedagogico(),
                                    'id_serie' => $vendaProdutoNivelSerie['id_serie'],
                                    'id_nivelensino' => $vendaProdutoNivelSerie['id_nivelensino']
                                )
                            )
                        );
                    }
                }
                $arrMatricula[] = $matriculaTO;
            }

            $this->dao->cadastrarMatriculaDisciplinaSP($contratoTO->getId_contrato());
            if (isset($matriculaTO))
                $this->vincularPerfilAluno($matriculaTO);
            return $arrMatricula;
        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
//            return $this->mensageiro->setMensagem("Erro ao executar a Matrícula")->setTipo(Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que vincula o perfil aluno ao usuário que tiver a matrícula ativada
     * @param MatriculaTO $matriculaTO
     * @param Boolean $matricula_institucional - Verifica se é matrícula institucional ou não
     * @return bool
     * @throws Zend_Exception
     */
    public function vincularPerfilAluno(MatriculaTO $mTO, $matricula_institucional = false)
    {
        //$this->dao->beginTransaction();
        try {
            $matriculaTONovo = new MatriculaTO();
            $matriculaTONovo->setId_matricula($mTO->getId_matricula());
            $matriculaTONovo->fetch(true, true, true);

            if (!$matriculaTONovo->getId_usuariocadastro()) {
                throw new Zend_Exception('A consulta da matrícula do aluno não retornou a matrícula ' . $mTO->getId_matricula());
            }

            $perfilTOPesquisa = new PerfilTO();
            $perfilTOPesquisa->setId_entidade($matriculaTONovo->getId_entidadematricula());
            if ($matricula_institucional)
                $perfilTOPesquisa->setId_perfilpedagogico(PerfilPedagogicoTO::ALUNO_INSTITUCIONAL);
            else
                $perfilTOPesquisa->setId_perfilpedagogico(PerfilPedagogicoTO::ALUNO); //Aluno-5
            $perfilTOPesquisa->setId_sistema(SistemaTO::PORTAL_ALUNO); // Portal do Aluno

            $perfilORM = new PerfilORM();
            $perfilTO = $perfilORM->consulta($perfilTOPesquisa, false, true);

            if (!$perfilTO) {
                throw new Zend_Exception("Não foi encontrado perfil de aluno para o sistema Portal do Aluno na entidade {$matriculaTONovo->getId_entidadematricula()}", 0);
            }

            $usuarioPerfilEntidadeTO = new UsuarioPerfilEntidadeTO();
            $usuarioPerfilEntidadeTO->setId_perfil($perfilTO->getId_perfil());
            $usuarioPerfilEntidadeTO->setId_usuario($matriculaTONovo->getId_usuario());
            $usuarioPerfilEntidadeTO->setId_entidade($matriculaTONovo->getId_entidadematricula());
            $usuarioPerfilEntidadeTO->setId_situacao(\G2\Constante\Situacao::TB_USUARIOPERFILENTIDADE_ATIVO); // Ativo

            $usuarioPerfilEntidadeORM = new UsuarioPerfilEntidadeORM();
            $existe = $usuarioPerfilEntidadeORM->consulta($usuarioPerfilEntidadeTO, false, true);


            if (!$existe) {
                $usuarioPerfilEntidadeTO->setDt_inicio(new Zend_Date());
                $usuarioPerfilEntidadeTO->setBl_ativo(true);
                $usuarioPerfilEntidadeTO->setStToken(md5($usuarioPerfilEntidadeTO->getId_usuario() . $usuarioPerfilEntidadeTO->getId_perfil() . $usuarioPerfilEntidadeTO->getId_entidade() . $usuarioPerfilEntidadeTO->getId_entidadeidentificacao() . date("Y-m-d H:i:s") . rand(1, 3000)));

                $usuarioPerfilEntidadeORM->insert($usuarioPerfilEntidadeTO->toArrayInsert());
            }
            //$this->dao->commit();
            return true;
        } catch (Zend_Exception $e) {
            //$this->dao->rollBack();
//            throw $e;
            return false;
        }
    }

    /**
     * Metodo que inicia o processo de Ativação de Matricula
     * @param ContratoTO $contratoTO
     * @throws Zend_Exception
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function ativarMatricula(ContratoTO $contratoTO)
    {

        try {
            if (!$contratoTO->getId_contrato() && !$contratoTO->getId_venda()) {
                throw new Zend_Exception('O id_venda ou id_contrato tem que vir setado!');
            }

            //verifica se a flag bl_ativo do contrato que foi passado por parametro é diferente de true
            if (!$contratoTO->getBl_ativo()) {
                //se o bl ativo não for true ele seta o parametro para true e consulta de novo o contrato no banco
                $contratoTO->setBl_ativo(true);
                $contrato = $this->dao->retornarContrato($contratoTO)->toArray();

                if (empty($contrato)) {
                    throw new Zend_Validate_Exception('Não Foi Encontrado Nenhum Contrato Ativo.');
                }
                $contratoTO = Ead1_TO_Dinamico::encapsularTo($contrato, new ContratoTO(), true);
            }

            $contratoMatriculaTO = new ContratoMatriculaTO();
            $contratoMatriculaTO->setId_contrato($contratoTO->getId_contrato());
            $contratoMatriculaTO->fetch(false, true, true);

            if ($contratoMatriculaTO->getId_matricula()) {
                $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
                return $this->mensageiro->setMensagem('Matricula Já Ativada!');
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($contratoTO->getId_venda());
            $vendaTO = $this->_subProcessoAtivarMatriculaVerificarVenda($vendaTO);

            /**
             * Novo Bloco para conversão de Zend_Exception para Zend_Validate_Exception
             */
            try {
                $this->procedimentoAtivarMatricula($contratoTO, null, $vendaTO);
            } catch (Exception $e) {
                throw new Zend_Validate_Exception($e->getMessage());
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensagem($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensagem('Erro ao Ativar Matricula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * SubProcesso de Ativar Matricula que valida se a venda esta confirmada
     * @param VendaTO $vendaTO
     * @return array|VendaTO
     * @throws Zend_Validate_Exception
     */
    private function _subProcessoAtivarMatriculaVerificarVenda(VendaTO $vendaTO)
    {
        $produtoDAO = new ProdutoDAO();
        $vendaTO->setBl_ativo(true);
        $arrVenda = $produtoDAO->retornarVenda($vendaTO)->toArray();
        if (empty($arrVenda)) {
            THROW new Zend_Validate_Exception("Erro ao Verificar Estatus da Venda!");
        }
        $vendaTO = Ead1_TO_Dinamico::encapsularTo($arrVenda, new VendaTO(), true);
        if (!($vendaTO->getId_evolucao() == MatriculaBO::EVOLUCAO_VENDA_CONFIRMADA)) {
            THROW new Zend_Validate_Exception("Não foi possível ativar a matrícula pois a venda não está Confirmada!");
        }
        return $vendaTO;
    }

    /**
     * Metodo que decide se cadastra ou edita o contrato do Envolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoEnvolvido(ContratoEnvolvidoTO $ceTO)
    {
        $orm = new ContratoEnvolvidoORM();
        if ($orm->consulta($ceTO, true)) {
            return $this->editarContratoEnvolvido($ceTO);
        }
        return $this->cadastrarContratoEnvolvido($ceTO);
    }

    /**
     * Metodo que Edita ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function editarContratoEnvolvido(ContratoEnvolvidoTO $ceTO)
    {
        try {
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $this->dao->editarContratoEnvolvido($ceTO);
            return $this->mensageiro->setMensageiro('Envolvido no Contrato Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Envolvido no Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContratoEnvolvido(ContratoEnvolvidoTO $ceTO)
    {
        try {
            $this->dao->cadastrarContratoEnvolvido($ceTO);
            return $this->mensageiro->setMensageiro('Envolvido no Contrato Cadastrado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Envolvido no Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que decide se cadastra ou edita uma matricula, venda e vendaproduto.
     * OBS: Este metodo cadastra/edita a venda e sobrescreve(edita/cadastra) a vendaproduto e matricula
     * @param MatriculaTO $mTO
     * @param VendaTO $vTO
     * @param VendaProdutoTO $vpTO
     * @return Ead1_Mensageiro
     */
    public function salvarMatriculaVendaProduto(MatriculaTO $mTO, VendaTO $vTO, VendaProdutoTO $vpTO)
    {
        $this->dao->beginTransaction();
        try {
            $produtoRO = new ProdutoRO();
            $respostaVenda = $produtoRO->salvarVenda($vTO);
            if ($respostaVenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro = $respostaVenda;
                THROW new Zend_Exception('Erro ao Salvar Venda!');
            }
            $msgVenda = $respostaVenda->getMensagem();
            if ($msgVenda[0] instanceof VendaTO) {
                $vTO = $msgVenda[0];
            }
            $vpTO->setId_venda($vTO->getId_venda());
            $respostaVendaProduto = $produtoRO->salvarVendaProduto($vpTO);
            if ($respostaVendaProduto->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro = $respostaVendaProduto;
                THROW new Zend_Exception('Erro ao Salvar VendaProduto!');
            }
            $msgVendaProduto = $respostaVendaProduto->getMensagem();
            if ($msgVendaProduto[0] instanceof VendaProdutoTO) {
                $vpTO = $msgVendaProduto[0];
            }
            $mTO->setId_vendaproduto($vpTO->getId_vendaproduto());
            if ($mTO->getId_matricula()) {
                $respostaMatricula = $this->editarMatricula($mTO);
                if ($respostaMatricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->mensageiro = $respostaMatricula;
                    THROW new Zend_Exception('Erro ao Editar Matricula!');
                }
            } else {
                $respostaMatricula = $this->cadastrarMatricula($mTO);
                if ($respostaMatricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->mensageiro = $respostaMatricula;
                    THROW new Zend_Exception('Erro ao Cadastrar Matricula!');
                }
                $msgMatricula = $respostaMatricula->getMensagem();
                $mTO = $msgMatricula[0];
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro(array($mTO, $vTO, $vpTO), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Edita Matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function editarMatricula(MatriculaTO $mTO)
    {
        try {
            if (!is_null($mTO->getId_entidadeatendimento()) && $mTO->getId_entidadeatendimento()) {
                $orm = new MatriculaORM();
                $matriculaSalva = $orm->consulta($mTO, true, true);
                if ($matriculaSalva) {
                    if ($matriculaSalva->getId_entidadeatendimento() != $mTO->getId_entidadeatendimento()) {
                        $pessoaBO = new PessoaBO();
                        $pessoaBO->importarUsuarioEntidade($matriculaSalva->getId_usuario(), $matriculaSalva->getId_entidadeatendimento(), $mTO->getId_entidadeatendimento(), $mTO->getSessao()->id_usuario);
                    }
                }
            }
            $this->dao->editarMatricula($mTO);
            return $this->mensageiro->setMensageiro('Matricula Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarMatricula(MatriculaTO $mTO)
    {
        try {
            if (!$mTO->getId_usuariocadastro()) {
                $mTO->setId_usuariocadastro($mTO->getSessao()->id_usuario);
            }

            $id_matricula = $this->dao->cadastrarMatricula($mTO);
            $mTO->setId_matricula($id_matricula);
            return $this->mensageiro->setMensageiro($mTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra a venda e vendaproduto e seta no contrato e na matricula
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoMatriculaVendaProduto(ContratoTO $cTO)
    {
        $this->dao->beginTransaction();
        try {
            if (!$cTO->getId_contrato()) {
                $this->mensageiro->setMensageiro('Erro ao Retornar Contrato!', Ead1_IMensageiro::AVISO, 'O id_contrato não veio setado!');
                THROW new Zend_Exception('O id_contrato não veio setado!');
            }
            $produtoRO = new ProdutoRO();
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $cmTOconsulta = new ContratoMatriculaTO();
            $cmTOconsulta->setId_contrato($cTO->getId_contrato());
            $objContratoMatricula = $this->retornarContratoMatricula($cmTOconsulta);
            if ($objContratoMatricula->getTipo() == Ead1_IMensageiro::ERRO) {
                $this->mensageiro = $objContratoMatricula;
                THROW new Zend_Exception('Erro ao Retornar Matriculas do Contrato!');
            } elseif ($objContratoMatricula->getTipo() == Ead1_IMensageiro::AVISO) {
                $objContratoMatricula->setMensagem('Este Contrato não tem Nenhuma Matricula Vinculada!');
                $this->mensageiro = $objContratoMatricula;
                THROW new Zend_Exception('Contrato Sem Nenhuma Matricula Vinculada!');
            }
            $vTO = new VendaTO();
            $vTO->setBl_ativo(true);
            $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            $vTO->setId_situacao(28);
            $objVenda = $produtoRO->cadastrarVenda($vTO);
            if ($objVenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro = $objVenda;
                THROW new Zend_Exception('Erro ao Cadastrar Venda!');
            }
            $msgVenda = $objVenda->getMensagem();
            $vTO = $msgVenda[0];
            $cTO->setId_venda($vTO->getId_venda());
            $objContrato = $this->editarContrato($cTO);
            if ($objContrato->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro = $objContrato;
                THROW new Zend_Exception('Erro ao Setar Venda no Contrato!');
            }
            foreach ($objContratoMatricula->getMensagem() as $cmTO) {
                $mTOConsulta = new MatriculaTO();
                $mTOConsulta->setId_matricula($cmTO->getId_matricula());
                $mTOConsulta->setBl_ativo(true);
                $objMatricula = $this->retornarMatricula($mTOConsulta);
                if ($objMatricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->mensageiro = $objMatricula;
                    THROW new Zend_Exception('Erro ao Retornar Matricula!');
                }
                foreach ($objMatricula->getMensagem() as $mTO) {
                    $pppTOConsulta = new ProdutoProjetoPedagogicoTO();
                    $pppTOConsulta->setId_projetopedagogico($mTO->getId_projetopedagogico());
                    $objProdutoProjetoPedagogico = $produtoRO->retornarProdutoProjetoPedagogico($pppTOConsulta);
                    if ($objProdutoProjetoPedagogico->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $this->mensageiro = $objProdutoProjetoPedagogico;
                        THROW new Zend_Exception('Erro ao Retornar Produto do Projeto Pedagógico!');
                    }
                    $msgProdutoProjetoPedagogico = $objProdutoProjetoPedagogico->getMensagem();
                    $pppTOConsulta = $msgProdutoProjetoPedagogico[0];
                    $vpTO = new VendaProdutoTO();
                    $vpTO->setId_venda($vTO->getId_venda());
                    $vpTO->setId_produto($pppTOConsulta->getId_produto());
                    $objVendaProduto = $produtoRO->salvarVendaProduto($vpTO);
                    if ($objVendaProduto->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $this->mensageiro = $objVendaProduto;
                        THROW new Zend_Exception('Erro ao Salvar VendaProduto!');
                    }
                    $msgVendaProduto = $objVendaProduto->getMensagem();
                    $vpTO = $msgVendaProduto[0];
                    $mTO->setId_vendaproduto($vpTO->getId_vendaproduto());
                    $objMatricula = $this->editarMatricula($mTO);
                    if ($objMatricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $this->mensageiro = $objMatricula;
                        THROW new Zend_Exception('Erro ao Setar a VendaProduto na Matricula!');
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($cTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que retorna o ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @return Ead1_Mensageiro
     */
    public function retornarContratoMatricula(ContratoMatriculaTO $cmTO)
    {
        try {
            $retorno = $this->dao->retornarContratoMatricula($cmTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Contrato Matrícula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoMatriculaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Contrato da Matricula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Contrato
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function editarContrato(ContratoTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $this->dao->editarContrato($cTO);
            return $this->mensageiro->setMensageiro('Contrato Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarMatricula(MatriculaTO $mTO)
    {
        try {
            $retorno = $this->dao->retornarMatricula($mTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Matrícula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new MatriculaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Matricula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salva os responsaveis do contrato
     * Salva varios Responsaveis do tipo Financeiro e um unico do tipo Pedagogico
     * Parametro 1: Array de ContratoResponsavelTO do tipo Financeiro
     * Parametro 2: ContratoResponsavel do tipo Pedagogico
     * @param array $arrTO
     * @param ContratoResponsavelTO $crTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayContratoResponsavel($arrTO, ContratoResponsavelTO $crTO)
    {
        $this->dao->beginTransaction();
        try {
            if ($crTO->getId_tipocontratoresponsavel() != 2) {
                $this->mensageiro->setMensagem('Erro ao Salvar Responsáveis!');
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                THROW new Zend_Exception('O TO id_tipocontratoresponsavel do segundo parametro não é do tipo Pedagogico!');
            }
            //Seta todo mundo para bl_ativo = 0 do id_contrato
            $newCrTO = new ContratoResponsavelTO();
            $newCrTO->setBl_ativo(0);
            $newCrTO->setId_contrato($crTO->getId_contrato());
            $where = ' id_contrato = ' . $newCrTO->getId_contrato();
            $mensageiro = $this->editarContratoResponsavel($newCrTO, $where);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro = $mensageiro;
                THROW new Zend_Exception($mensageiro->getCodigo());
            }
            $where .= ' AND id_usuario = ' . $crTO->getId_usuario() . ' AND id_tipocontratoresponsavel = 2';
            $mensageiro = $this->retornarContratoResponsavel($crTO, $where);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                $this->mensageiro = $mensageiro;
                THROW new Zend_Exception($mensageiro->getCodigo());
            } elseif ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                $mensageiro = $this->cadastrarContratoResposanvel($crTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->mensageiro = $mensageiro;
                    THROW new Zend_Exception($mensageiro->getCodigo());
                }
            } else {
                $where = 'id_contrato = ' . $crTO->getId_contrato() . ' AND id_usuario = ' . $crTO->getId_usuario() . ' AND id_tipocontratoresponsavel = 2';
                $mensageiro = $this->editarContratoResponsavel($crTO, $where);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->mensageiro = $mensageiro;
                    THROW new Zend_Exception($mensageiro->getCodigo());
                }
            }
            if (!is_array($arrTO)) {
                $this->mensageiro->setMensagem('Erro ao Salvar Responsáveis!');
                $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                THROW new Zend_Exception('O parametro recebido não é um array!');
            }
            foreach ($arrTO as $index => $contResponsavelTO) {
                if (!($contResponsavelTO instanceof ContratoResponsavelTO)) {
                    $this->mensageiro->setMensagem('Erro ao Editar Responsáveis Financeiros!');
                    $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
                    THROW new Zend_Exception('O Objeto não é uma Instancia da Classe ContratoResponsavelTO!');
                }
                $where = 'id_contrato = ' . $crTO->getId_contrato() . ' AND id_tipocontratoresponsavel = 1 AND id_usuario = ' . $contResponsavelTO->getId_usuario();
                $mensageiro = $this->retornarContratoResponsavel($contResponsavelTO, $where);
                if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $this->mensageiro = $mensageiro;
                    THROW new Zend_Exception($mensageiro->getCodigo());
                } elseif ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                    $mensageiro = $this->cadastrarContratoResposanvel($contResponsavelTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $this->mensageiro = $mensageiro;
                        THROW new Zend_Exception($mensageiro->getCodigo());
                    }
                } else {
                    $where = 'id_contrato = ' . $crTO->getId_contrato() . ' AND id_usuario = ' . $contResponsavelTO->getId_usuario() . ' AND id_tipocontratoresponsavel = 1';
                    $mensageiro = $this->editarContratoResponsavel($contResponsavelTO, $where);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $this->mensageiro = $mensageiro;
                        THROW new Zend_Exception($mensageiro->getCodigo());
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Responsáveis Salvos com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setCodigo($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Edita Contrato Responsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @return Ead1_Mensageiro
     */
    public function editarContratoResponsavel(ContratoResponsavelTO $crTO, $where = null)
    {
        try {
            $this->dao->editarContratoResponsavel($crTO, $where);
            return $this->mensageiro->setMensageiro('Responsavel Pelo Contrato Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Responsavel Pelo Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o ContratoResponsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @return Ead1_Mensageiro
     */
    public function retornarContratoResponsavel(ContratoResponsavelTO $crTO, $where = null)
    {
        try {
            $retorno = $this->dao->retornarContratoResponsavel($crTO, $where)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Responsável do COntrato Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoResponsavelTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Contrato do Responsável!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Contrato responsavel
     * @param ContratoResponsavelTO $crTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContratoResposanvel(ContratoResponsavelTO $crTO)
    {
        try {
            $id_contratoresponsavel = $this->dao->cadastrarContratoResponsavel($crTO);
            $crTO->setId_contratoresponsavel($id_contratoresponsavel);
            return $this->mensageiro->setMensageiro($crTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Responsável pelo Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra o vinculo do contrato com o conveio
     * @param ContratoConvenioTO $to
     * @param bool $soExcluir Exlui apenas todos os convenios do contrato
     * @return Ead1_Mensageiro
     */
    public function salvarContratoConvenio(ContratoConvenioTO $to, $soExcluir = false)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_contrato()) {
                THROW new Zend_Exception('O id_contrato não veio setado!');
            }
            $this->dao->deletarContratoConvenio(new ContratoConvenioTO(array('id_contrato' => $to->getId_contrato())));
            if (!$soExcluir) {
                $this->dao->cadastrarContratoConvenio($to);
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular o Convenio com o Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra Registro de matrícula
     * @param LivroRegistroTO $liTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarRegistro(LivroRegistroTO $liTO)
    {
        try {
            $jaRegistrou = $this->dao->retornarRegistro($liTO)->toArray();
            if (!empty($jaRegistrou)) {
                return $this->mensageiro->setMensageiro('Matrícula já possui registro!', Ead1_IMensageiro::ERRO);
            }
            $matriculaORM = new MatriculaORM();
            $matriculaTO = $matriculaORM->consulta(new MatriculaTO(array('id_matricula' => $liTO->getId_matricula())), false, true);
            if (!$matriculaTO) {
                THROW new Zend_Exception('Erro ao Verificar Matricula');
            }
            $projetoPedagogicoORM = new ProjetoPedagogicoORM();
            $projetoPedagogicoTO = $projetoPedagogicoORM->consulta(new ProjetoPedagogicoTO(array('id_projetopedagogico' => $matriculaTO->getId_projetopedagogico(), 'bl_ativo' => 1)), false, true);
            if (!$projetoPedagogicoTO) {
                THROW new Zend_Exception('Erro ao Verificar Projeto Pedagógico!');
            }
            $liTO->setId_livroregistroentidade($projetoPedagogicoTO->getId_livroregistroentidade());
            $lreTO = new LivroRegistroEntidadeTO();
            $lreTO->setId_livroregistroentidade($projetoPedagogicoTO->getId_livroregistroentidade());
            $livroRegistroDAO = new LivroRegistroDAO();
            $livro = $livroRegistroDAO->retornarLivroRegistroEntidade($lreTO);

            if (!empty($livro)) {
                $livro = $livro->toArray();
            }

            $lrTOaux = new LivroRegistroTO();
            $lrTOaux->setId_livroregistroentidade($projetoPedagogicoTO->getId_livroregistroentidade());
            $response = $this->dao->retornarRegistro($lrTOaux, null, 'dt_cadastro DESC');
            if ($response) {
                $ultimoRegistro = $response->toArray();
            } else {
                $ultimoRegistro = $response;
            }
            if (!$ultimoRegistro) {
                $liTO->setNu_folha(1);
                $liTO->setNu_livro(1);
                $liTO->setNu_registro($livro[0]['nu_registroinicial']);
            } else {
                $liTO->setNu_registro($ultimoRegistro['nu_registro'] + 1);
                $lrTOaux2 = new LivroRegistroTO();
                $lrTOaux2->setNu_folha($ultimoRegistro['nu_folha']);

                $folhaRegistro = $this->dao->retornarRegistro($lrTOaux2)->toArray();
                if (count($folhaRegistro) == $livro[0]['nu_registros']) {
                    if ($lrTOaux->getNu_folha() == $livro[0]['nu_folhas']) {
                        $liTO->setNu_folha($lrTOaux2->getNu_folha() + 1);
                        $liTO->setNu_livro($ultimoRegistro['nu_livro'] + 1);
                    } else {
                        $liTO->setNu_folha($lrTOaux2->getNu_folha() + 1);
                        $liTO->setNu_livro($ultimoRegistro['nu_livro']);
                    }
                } else {
                    $liTO->setNu_folha($lrTOaux2->getNu_folha());
                    $liTO->setNu_livro($ultimoRegistro['nu_livro']);
                }
            }

            $liTO->setId_usuariocadastro($liTO->getSessao()->id_usuario);
            if (!$this->dao->cadastrarRegistro($liTO)) {
                throw new Zend_Exception('Erro ao cadastrar registro');
            }

            return $this->mensageiro->setMensageiro($liTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Registrar Matrícula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Gera o contrato de um aluno com seus responsáveis financeiros, o reponsável pedagógico e suas matrículas
     * Após a geração do contrato é gerado uma venda para aquele contrato inserido
     * @param ContratoTO $contratoTO
     * @param array $arContratoResponsavelFinanceiroTO
     * @param ContratoResponsavelTO $contratoResponsavelPedagogico
     * @param array $arMatriculaTO
     * @param array $arLancamentoTO
     * @param ContratoRegraTO $contratoRegraTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoMatricula($contratoTO, $arContratoResponsavelFinanceiroTO, $contratoResponsavelPedagogico, $arMatriculaTO, $arLancamentoTO, $contratoRegraTO)
    {
        $this->dao->beginTransaction();
        $editando = false;
        try {
            $contratoTO->id_entidade = $contratoTO->getSessao()->id_entidade;
            if ($contratoTO->id_entidade && !$contratoTO->id_contrato) {
                $contratoTO->bl_ativo = 1;
                $idContrato = $this->cadastrarContrato($contratoTO);
                $contratoTO->id_contrato = $idContrato;
            } else {
                if ($contratoTO->getId_situacao() == ContratoTO::SITUACAO_PENDENTE) {

                    $contratoResponsavelDelete = new ContratoResponsavelTO();
                    $contratoResponsavelDelete->setId_contrato($contratoTO->getId_contrato());

                    if ($this->deletarContratoResponsavel($contratoResponsavelDelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new DomainException('Erro ao excluir os Responsáveis Financeiros do Contrato.');
                    }

                    $contratoMatriculaTODelete = new ContratoMatriculaTO();
                    $contratoMatriculaTODelete->setId_contrato($contratoTO->getId_contrato());

                    $contratoMatriculaDAO = new MatriculaDAO();
                    $contratoMatriculaTOs = $contratoMatriculaDAO->retornarContratoMatricula($contratoMatriculaTODelete);

                    if ($this->deletarContratoMatricula($contratoMatriculaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new DomainException('Erro ao excluir vínculos de matrícula ao Contrato.');
                    }

                    if ($contratoMatriculaTOs) {
                        foreach ($contratoMatriculaTOs as $contratoMatriculaTO) {
                            $matriculaTODelete = new MatriculaTO();
                            $matriculaTODelete->setId_matricula($contratoMatriculaTO->getId_matricula());

                            if ($this->deletarMatricula($matriculaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                                throw new DomainException('Erro ao excluir matrícula do Contrato.');
                            }
                        }
                    }

                    $vendaTODelete = new VendaTO();
                    $vendaTODelete->setId_venda($contratoTO->getId_venda());
                    $this->deletarLancamento($vendaTODelete);

                    $vendaTODelete = new VendaTO();
                    $vendaTODelete->setId_venda($contratoTO->id_venda);

                    if ($this->deletarVenda($vendaTODelete)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new DomainException('Erro ao excluir a Venda do Contrato.');
                    }
                }
                $this->editarContrato($contratoTO);
                $editando = true;
            }

            if ($contratoTO->id_contrato === false) {
                throw new DomainException('Não foi possível gerar o contrato');
            }

            $contratoResponsavelPedagogico->id_contrato = $contratoTO->id_contrato;
            if ($contratoResponsavelPedagogico->id_tipocontratoresponsavel !== TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_PEDAGOGICO) {
                throw new InvalidArgumentException('O tipo do responsável pedagógico informado está incorreto ');
            }

            if ($this->salvarContratoResponsavel($contratoResponsavelPedagogico) === false) {
                throw new DomainException('Não foi possível vincular o responsável pedagógico ao contrato');
            }

            if (count($arContratoResponsavelFinanceiroTO) === 0) {
                throw new InvalidArgumentException('Deve haver ao menos um reponsável financeiro para geração do contrato');
            }

            $somaPorcentagemFinanceiro = 0;

            foreach ($arContratoResponsavelFinanceiroTO as $contratoResponsavel) {
                if (!($contratoResponsavel instanceof ContratoResponsavelTO)) {
                    throw new InvalidArgumentException('O reponsável financeiro informado na lista não é do tipo ContratoResponsavelTO ');
                }

                if ($contratoResponsavel->id_tipocontratoresponsavel !== TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO) {
                    throw new InvalidArgumentException('O tipo do responsável financeiro informado está incorreto ');
                }

                $contratoResponsavel->id_contrato = $contratoTO->id_contrato;
                if (!$contratoResponsavel->id_contratoresponsavel) {
                    if ($this->retornarContratoResponsavel($contratoResponsavel, 'id_contrato = ' . $contratoTO->id_contrato . ' AND id_tipocontratoresponsavel = ' . TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO . ' AND id_usuario = ' . $contratoResponsavel->id_usuario)->getTipo() === Ead1_IMensageiro::SUCESSO) {
                        throw new DomainException('Não é possível adicionar responsáveis financeiros duplicados ');
                    }
                }
                if ($this->salvarContratoResponsavel($contratoResponsavel) === false) {
                    throw new DomainException('Não foi possível vincular o responsável financeiro ao contrato');
                }

                $somaPorcentagemFinanceiro += $contratoResponsavel->nu_porcentagem;
            }

            if ($somaPorcentagemFinanceiro < 100.00 || $somaPorcentagemFinanceiro > 100.00) {
                throw new DomainException('A soma das porcentagens dos responsáveis financeiros não corresponde a 100%');
            }

            $idVenda = $this->cadastrarVendaContrato($contratoTO); //pode ter alguma coisa haver
            if ($idVenda == false) {
                throw new DomainException('Não foi possível gerar uma venda para o contrato');
            }

            $contratoTO->setId_venda($idVenda);

            foreach ($arMatriculaTO as $matriculaTO) {
                if (!($matriculaTO instanceof MatriculaTO)) {
                    throw new InvalidArgumentException('O item da lista de matrículas não é do tipo MatriculaTO');
                }
                //$matriculaTO->dt_inicio =  $matriculaTO->dt_cadastro;
                if (!$editando) {
                    $produtoProjetoPedagogicoROW = $this->retornarProdutoProjetoPedagogico($matriculaTO->id_projetopedagogico, $matriculaTO->id_entidadematricula);

                    if ($produtoProjetoPedagogicoROW === null) {
                        throw new DomainException('Para o projeto pedagógico selecionado não existe produto vinculado');
                    }
                    $vendaProduto = new VendaProdutoTO();
                    $vendaProduto->setId_produto($produtoProjetoPedagogicoROW->id_produto);
                    $vendaProduto->setId_venda($idVenda);
                    $vendaProduto->setNu_valorbruto(($produtoProjetoPedagogicoROW->nu_valor + ($produtoProjetoPedagogicoROW->nu_valormensal * $contratoRegraTO->getNu_mesesmensalidade())));
                    $vendaProduto->setNu_valorliquido($vendaProduto->nu_valorbruto);
                    $vendaProduto->setNu_desconto(0.00);

                    $idVendaProdutoProjetoPedagogico = $this->cadastrarVendaProdutoProjetoPedagogico($vendaProduto);

                    $matriculaTO->setId_vendaproduto($idVendaProdutoProjetoPedagogico);
                    $matriculaTO->setId_usuario($contratoTO->getId_usuario());
                    $matriculaTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULA_AGUARDANDOCONFIRMACAO);
                    $matriculaTO->setId_situacao(\G2\Constante\Situacao::TB_MATRICULA_INATIVA);
                    $matriculaTO->setBl_ativo(false);

                    if ($idVendaProdutoProjetoPedagogico == false) {
                        throw new DomainException('Não foi possível vincular o produto do projeto pedagógico com a venda');
                    }
                }

                if (!$matriculaTO->id_matricula) {
                    $mensageiro = $this->cadastrarMatricula($matriculaTO);
                } else {
                    $mensageiro = $this->editarMatricula($matriculaTO);
                }
                if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO || $mensageiro->getTipo() == Ead1_IMensageiro::AVISO) {
                    throw new DomainException($mensageiro->getCodigo());
                }
                if (!$editando) {
                    $arMensagem = $mensageiro->getMensagem();
                    $matriculaTO = $arMensagem[0];
                    $idMatricula = $matriculaTO->id_matricula;

                    if ($this->cadastrarContratoMatricula(new ContratoMatriculaTO(array('id_matricula' => $idMatricula, 'id_contrato' => $contratoTO->id_contrato)))->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                        throw new DomainException('Não foi possível vincular a matrícula ao contrato');
                    }
                }
            }
            if (!$editando) {
                if (!empty($arLancamentoTO)) {
                    if (!$this->cadastrarLancamentos($arLancamentoTO, $idVenda, true, $contratoTO->id_usuario)) {
                        throw new DomainException('Não foi possível vincular os lancamentos de venda!');
                    }
                }
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro($contratoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro durante geração do contrato: ' . $exc->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Cadastra um contrato
     * Seta o id_evolucao = 1 ( Aguardando Confirmação )
     * Seta o id_situacao = 47 ( Contrato Pendente )
     * @param ContratoTO $contratoTO
     * @throws Exception
     * @see VendaBO::_processoPreVendaCadastrarContrato()
     * @return int  - código do contrato inserido ( chave primária )
     */
    public function cadastrarContrato(ContratoTO $contratoTO)
    {


        if (empty($contratoTO->id_usuario)) {
            throw new Exception('O contrato informado deve possuir um aluno vinculado');
        }

        if (empty($contratoTO->id_entidade)) {
            throw new Exception('Selecione a entidade que será gerado o contrato');
        }

        $contratoTO->setId_usuariocadastro($contratoTO->getId_usuariocadastro() ? $contratoTO->getId_usuariocadastro() : $contratoTO->getSessao()->id_usuario);
        $contratoTO->bl_ativo = true;
        $contratoTO->id_evolucao = 1;
        $contratoTO->id_situacao = 47;

        $matriculaDAO = new MatriculaDAO();
        $pkContrato = $matriculaDAO->cadastrarContrato($contratoTO);

        return $pkContrato;
    }

    /**
     * Metodo que Exclui Contrato Responsavel
     * @param ContratoResponsavelTO $crTO
     * @param string $where
     * @return Ead1_Mensageiro
     */
    public function deletarContratoResponsavel(ContratoResponsavelTO $crTO, $where = null)
    {
        try {
            $this->dao->deletarContratoResponsavel($crTO, $where);
            return $this->mensageiro->setMensageiro('Responsável pelo Contrato Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Responsável pelo Contrato!', Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Metodo que Exclui ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @return Ead1_Mensageiro
     */
    public function deletarContratoMatricula(ContratoMatriculaTO $cmTO)
    {
        try {
            $this->dao->deletarContratoMatricula($cmTO);
            return $this->mensageiro->setMensageiro('Vinculo do Contrato com a Matricula Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo do Contrato com a Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui a Matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function deletarMatricula(MatriculaTO $mTO)
    {
        try {
            $this->dao->deletarMatricula($mTO);
            return $this->mensageiro->setMensageiro('Matricula Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Matricula! ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Deletar Lancamento e LancamentoVenda
     * @param $vendaProdutoTO
     * @return bool
     */
    public function deletarLancamento(VendaTO $vendaTO)
    {
        try {

            if (!$vendaTO->id_venda) {
                throw new Exception('Erro ao excluir lançamentos!');
            } else {
                $lancamentoVendaTO = new LancamentoVendaTO();
                $lancamentoVendaTO->setId_venda($vendaTO->id_venda);
                $lancamentoVendaORM = new LancamentoVendaORM();
                $lancamentos = $lancamentoVendaORM->consulta($lancamentoVendaTO);

                $lancamentoORM = new LancamentoORM();
                if ($lancamentos) {
                    foreach ($lancamentos as $lancamentoVendaAtual) {
                        $lancamentoTO = new LancamentoTO();
                        $lancamentoTO->setId_lancamento($lancamentoVendaAtual->id_lancamento);

                        $lancamentoVendaORM->delete("id_lancamento = {$lancamentoTO->getId_lancamento()}");
                        $lancamentoORM->delete("id_lancamento = {$lancamentoTO->getId_lancamento()}");
                    }
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo que Exclui a Venda
     * @param VendaTO $vendaTO
     * @param string $where
     * @return Ead1_Mensageiro
     */
    public function deletarVenda(VendaTO $vendaTO)
    {
        try {
            $this->dao->deletarVenda($vendaTO);
            return $this->mensageiro->setMensageiro('Venda Excluída com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Vincula o responsável há um contrato existente
     * @param ContratoResponsavelTO $contratoResponsavel
     * @throws DomainException
     * @return int
     */
    public function salvarContratoResponsavel(ContratoResponsavelTO $contratoResponsavel)
    {


        if (empty($contratoResponsavel->id_contrato)) {
            throw new DomainException('Informe o código do contrato para gerar o vínculo com o reponsável');
        }

        if (empty($contratoResponsavel->id_usuario) && empty($contratoResponsavel->id_entidaderesponsavel)) {
            throw new DomainException('Informe o usuário que será vinculado como reponsável para o contrato');
        }

        if ($contratoResponsavel->getId_usuario()) {
            $usuario = new UsuarioTO();
            $usuario->setId_usuario($contratoResponsavel->getId_usuario());
            $usuario->fetch(true, true, true);
            if (!$usuario->getSt_nomecompleto()) {
                throw new DomainException('O Usuário informado como responsável para este contrato não existe');
            }
        }

        if (empty($contratoResponsavel->id_tipocontratoresponsavel)) {
            throw new DomainException('Informe o tipo do responsável para vínculo com o contrato');
        }

        if ($contratoResponsavel->id_tipocontratoresponsavel === TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO) {
            if (empty($contratoResponsavel->nu_porcentagem) || is_nan($contratoResponsavel->nu_porcentagem)) {
                throw new DomainException('Informe a porcentagem de pagamento para o responsável do tipo FINANCEIRO');
            }
        }

        $matriculaDAO = new MatriculaDAO();
        if (!$contratoResponsavel->getId_contratoresponsavel()) {
            return $matriculaDAO->cadastrarContratoResponsavel($contratoResponsavel);
        } else {
            return $matriculaDAO->editarContratoResponsavel($contratoResponsavel);
        }
    }

    /**
     * Invocado após o cadastro de contrato.
     * Deve ser utilizado somente para vendas de contrato ( Éder )
     * Gera uma venda vinculado ao contrato
     * Registra uma venda inicial com todos os valores zerados
     * Seta a venda com o bl_contrato = true informando que a venda de um contrato
     * Seta a venda com o id_evolucao = 7 (Aguardando Negociação)
     * Seta a venda com o id_situacao = 46 (Venda Pendente)
     * Realiza o vínculo de venda para o contrato
     * @throws DomainException
     * @param ContratoTO $cTO
     * @param mixed $valorBruto
     * @return int - id da venda gerada
     */
    public function cadastrarVendaContrato(ContratoTO $contratoTO, $valorBruto = 0.00)
    {

        if (empty($contratoTO->id_entidade)) {
            throw new DomainException('O contrato informado para a venda não possui entidade vinculada');
        }

        if (empty($contratoTO->id_usuario)) {
            throw new DomainException('O contrato informado para a venda não possui aluno vinculado');
        }

        if (!$contratoTO->getId_venda()) {

            $dao = new WebServiceDAO();
            $formaPagamento = $dao->retornarFormaPagamento($contratoTO->id_entidade);
            if (!$formaPagamento) {
                throw new Zend_Exception("Forma de Pagamento não encontrada para a categoria SITE!");
            }

            $vendaTO = new VendaTO();
            $matriculaDAO = new MatriculaDAO();
            $bolsaPorcentagem = $contratoTO->getNu_bolsa();
            $bolsaValor = (($valorBruto * $bolsaPorcentagem) / 100);
            $vendaTO->bl_ativo = 1;
            $vendaTO->bl_contrato = true;
            $vendaTO->id_usuariocadastro = $vendaTO->getSessao()->id_usuario;
            $vendaTO->nu_descontoporcentagem = $bolsaPorcentagem;
            $vendaTO->nu_descontovalor = $bolsaValor;
            $vendaTO->nu_juros = 0.00;
            $vendaTO->id_evolucao = 7;
            $vendaTO->id_situacao = 46;
            $vendaTO->nu_valorbruto = $valorBruto;
            $vendaTO->nu_valorliquido = ($valorBruto - $bolsaValor);
            $vendaTO->id_entidade = $contratoTO->id_entidade;
            $vendaTO->id_usuario = $contratoTO->id_usuario;
            $vendaTO->id_formapagamento = $formaPagamento->id_formapagamento;


            $idVenda = $matriculaDAO->cadastrarVendaContrato($vendaTO);

            if ($idVenda === false) {
                throw new DomainException('Não foi possível gerar a venda para o contrato');
            }


            $contratoTO->id_venda = $idVenda;
        } else {
            $idVenda = $contratoTO->getId_venda();
        }
        if ($this->editarContrato($contratoTO)->getTipo() == Ead1_IMensageiro::AVISO || $this->editarContrato($contratoTO)->getTipo() == Ead1_IMensageiro::ERRO) {
            throw new DomainException('Não foi possível vincular a venda ao contrato');
        }
        return $idVenda;
    }

    /**
     * Verifica se determinado projeto pedagógico de uma entidade já possui um produto vinculado a ele
     * @param int $idProjetoPedagogico
     * @param int $idEntidade
     * @return bool
     */
    public function retornarProdutoProjetoPedagogico($idProjetoPedagogico, $idEntidade)
    {
        $matriculaDAO = new MatriculaDAO();
        $produtoProjetoPedagogicoROW = $matriculaDAO->retornarProdutoProjetoPedagogico($idProjetoPedagogico, $idEntidade);
        return $produtoProjetoPedagogicoROW;
    }

    /**
     * Gera o vínculo entre uma venda e seus produtos
     * @param $vendaProdutoTO
     * @return bool
     */
    public function cadastrarVendaProdutoProjetoPedagogico(VendaProdutoTO $vendaProdutoTO)
    {

        if (empty($vendaProdutoTO->id_produto)) {
            throw new InvalidArgumentException('Para vincular o produto a uma venda é necessário informar o produto');
        }

        if (empty($vendaProdutoTO->id_venda)) {
            throw new InvalidArgumentException('Para vincular o produto a uma venda é necessário informar a venda');
        }

        $matriculaDAO = new MatriculaDAO();

        $idProdutoVenda = $matriculaDAO->cadastrarVendaProdutoProjetoPedagogico($vendaProdutoTO);

        return $idProdutoVenda;
    }

    /**
     * Metodo que cadastra ContratoMatricula
     *    1 - Após cadastrada a matrícula fazer um vínculo desta matrícula com a (tb_vendaproduto)
     *  1.1 - Para achar o vínculo deve-se seguir o seguinte fluxo de tabelas: [tb_contratomatricula.id_matricula] > [tb_matricula.id_matricula | tb_matricula]
     *
     * @TODO implementar a vendaproduto para cada matrícula realizada
     * @param ContratoMatriculaTO $cmTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContratoMatricula(ContratoMatriculaTO $cmTO)
    {
        try {
            $this->dao->cadastrarContratoMatricula($cmTO);
            return $this->mensageiro->setMensageiro('Contrato Vinculado com a Matricula com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Contrato com Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Cadastra os lancamentos de uma venda/contrato.
     * @param array $arrLancamentoTO
     * @param int $idVenda
     * @param boolean $matricula
     * @param int $idUsuarioLancamento
     * @throws InvalidArgumentException
     * @throws DomainException
     * @return boolean
     */
    public function cadastrarLancamentos(array $arrLancamentoTO, $idVenda, $matricula = false, $idUsuarioLancamento = null)
    {
        try {
            $nu_ordem = 1;

            foreach ($arrLancamentoTO as $lancamentoTO) {
                if (!$lancamentoTO instanceof LancamentoTO && is_array($lancamentoTO)) {
                    $this->cadastrarLancamentos($lancamentoTO, $idVenda, $matricula, $idUsuarioLancamento);
                } else {
                    if ($matricula)
                        $lancamentoTO = $this->completaLancamentoTO($lancamentoTO);

                    if ($idUsuarioLancamento)
                        $lancamentoTO->id_usuariolancamento = $idUsuarioLancamento;

                    $lancamentoVendaTO = new LancamentoVendaTO();
                    $lancamentoVendaTO->id_venda = $idVenda;
                    $lancamentoVendaTO->bl_entrada = isset($lancamentoTO->bl_entrada) ? $lancamentoTO->bl_entrada : false;
                    $lancamentoVendaTO->nu_ordem = $nu_ordem++;

                    unset($lancamentoTO->bl_ativo);
                    unset($lancamentoTO->bl_entrada);

                    if (!$this->cadastrarLancamentoDB($lancamentoTO, $lancamentoVendaTO)) {
                        throw new DomainException('Não foi possível vincular os lancamentos de venda (entrada) !');
                    }
                }
            }

            return true;
        } catch (Exception $e) {
            throw new Zend_Exception("Erro. " . $e->getMessage());
        } catch (Zend_Exception $e) {
            throw new Zend_Exception("Erro argumento. " . $e->getMessage());
        }

    }

    /**
     * Completa o TO de lancamento
     * @param LancamentoTO $lancamentoTO
     * @return $lancamentoTO
     */
    private function completaLancamentoTO(LancamentoTO $lancamentoTO)
    {
        $lancamentoTO->id_tipolancamento = 2;
//		$lancamentoTO->id_tipolancamento;
        //$lancamentoTO->dt_cadastro = new Zend_Date();
        $lancamentoTO->id_meiopagamento = 3;
        //$lancamentoTO->id_meiopagamento;
        //$lancamentoTO->id_usuariolancamento = $lancamentoTO->id_usuariolancamento;
        $lancamentoTO->id_entidade = $lancamentoTO->getSessao()->id_entidade;
        $lancamentoTO->id_usuariocadastro = $lancamentoTO->getSessao()->id_usuario;
        $lancamentoTO->bl_quitado = false;
        return $lancamentoTO;
    }

    /**
     * Gera lancamentos nas tabelas
     * @param LancamentoTO $lancamentoTO
     * @param LancamentoVendaTO $lancamentoVendaTO
     * @throws InvalidArgumentException
     * @return boolean
     */
    public function cadastrarLancamentoDB(LancamentoTO $lancamentoTO, LancamentoVendaTO $lancamentoVendaTO)
    {
        $vendaDAO = new VendaDAO();
        $vendaDAO->beginTransaction();
        try {
            if (empty($lancamentoTO->nu_valor)) {
                throw new InvalidArgumentException('Para vincular o produto de uma venda é necessário informar o valor do produto');
            }


            //Decide se é para criar um novo ou editar
            if ($lancamentoTO->getId_lancamento()) {
                $idLancamento = $lancamentoTO->getId_lancamento();
                $vendaDAO->editarLancamento($lancamentoTO);
            } else {
                $idLancamento = $vendaDAO->cadastrarLancamento($lancamentoTO);
                $lancamentoTO->setId_lancamento($idLancamento);
            }

            //passa o id_lancamento para a to de lancamento venda
            $lancamentoVendaTO->setId_lancamento($idLancamento);

            //verifica se o id do lancamento não existe
            if (!$idLancamento) {
                $vendaDAO->rollBack();
                throw new InvalidArgumentException('Para vincular o produto a um lançamento de venda é necessário informar o id lancamento!');
            }

            $lancamentoVendaBuscaTO = new LancamentoVendaTO();
            $lancamentoVendaBuscaTO->setId_lancamento($lancamentoVendaTO->getId_lancamento());
            $lancamentoVendaBuscaTO->setId_venda($lancamentoVendaTO->getId_venda());

            //Faz o fetch do lancamento venda
            $busca = $lancamentoVendaBuscaTO->fetch(false, true, true);

            //se existir o lancamento venda edita o registro
            if ($busca) {
                $vendaDAO->editarLancamentoVenda($lancamentoVendaTO);
                $idLancamentoVenda = $lancamentoVendaTO->getId_venda();
            } else { //senão cadastra um novo
                $idLancamentoVenda = $vendaDAO->cadastrarLancamentoVenda($lancamentoVendaTO);
            }


            if (!$idLancamentoVenda) {
                $vendaDAO->rollBack();
                throw new InvalidArgumentException('Para vincular o produto a um lançamento de venda é necessário informar o id de venda!');
            }

            $vendaDAO->commit();
            return $idLancamentoVenda;
        } catch (Exception $e) {
            $vendaDAO->rollBack();
            throw new Zend_Exception("Erro ao tentar salvar lançamento. " . $e->getMessage());
        }

    }

    /**
     * Faz todo o processo de transferência de matrícula de aluno.
     * @param MatriculaTO $matriculaTO
     * @param VwUsuarioMatriculaProjetoTO $matriculaOrigemTO
     * @param TramiteTO $tramiteTO
     * @return Ead1_Mensageiro
     */
    public function transferirAluno(MatriculaTO $matriculaTO, VwUsuarioMatriculaProjetoTO $matriculaOrigemTO, TramiteTO $tramiteTO)
    {
        try {
            $this->dao->beginTransaction();
            $contratoMatriculaTO = new ContratoMatriculaTO();
            $contratoMatriculaTO->setId_matricula($matriculaOrigemTO->getId_matricula());

            $contratoMatriculaTO = $this->dao->retornarContratoMatricula($contratoMatriculaTO)->toArray();
            $contratoMatriculaTO = Ead1_TO_Dinamico::encapsularTo($contratoMatriculaTO, new ContratoMatriculaTO());
            if (!empty($contratoMatriculaTO)) {

                $matriculaTOrigem = new MatriculaTO();
                $matriculaTOrigem->setId_matricula($matriculaOrigemTO->getId_matricula());
                $matriculaTOrigem->fetch(false, true, true);

                if ($matriculaTOrigem->getId_evolucao() == MatriculaTO::EVOLUCAO_TRANSFERIDO) {
                    $this->dao->rollBack();
                    return $this->mensageiro->setMensageiro('Matrícula já foi transferida!', Ead1_IMensageiro::ERRO);
                }

                $contratoMatriculaTO = $contratoMatriculaTO[0];
                $contratoMatriculaTO->setBl_ativo(0);

                $mensageiro = $this->editarContratoMatricula($contratoMatriculaTO);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->dao->rollBack();
                    return $mensageiro;
                }

                $matriculaTO->setId_matriculaorigem($matriculaOrigemTO->getId_matricula());
                $mensageiro = $this->matricularDireto($matriculaTO, true, false);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->dao->rollBack();
                    return $mensageiro;
                }
//                Zend_Debug::dump($this->dao->getQtdTransacao());die;
//                $this->dao->commit();
                $matriculaTO = $mensageiro->getFirstMensagem();
                $contratoMatriculaTO->setId_matricula($matriculaTO->getId_matricula());
                $contratoMatriculaTO->setBl_ativo(1);

                $mensageiroContrato = $this->cadastrarContratoMatricula($contratoMatriculaTO);
                if ($mensageiroContrato->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $this->dao->rollBack();
                    return $mensageiroContrato;
                }
            } else {
                $this->dao->rollBack();
                return $this->mensageiro->setMensageiro('Matrícula de Origem não tem contrato!', Ead1_IMensageiro::ERRO);
            }

            $matriculaTOrigem = new MatriculaTO();
            $matriculaTOrigem->setId_matricula($matriculaOrigemTO->getId_matricula());
            $matriculaTOrigem->setId_situacao(MatriculaTO::SITUACAO_NAO_ATIVADA);
            $matriculaTOrigem->setId_evolucao(MatriculaTO::EVOLUCAO_TRANSFERIDO);
            $mensageiro = $this->editarMatricula($matriculaTOrigem);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->dao->rollBack();
                return $mensageiro;
            }
            $salaBO = new SalaDeAulaBO();
            $mensageiro = $salaBO->desalocarSalasMatricula($matriculaTOrigem->getId_matricula());
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->dao->rollBack();
                return $mensageiro;
            }

            $tramiteBO = new TramiteBO();
            $tramiteTO->setId_tipotramite(TramiteMatriculaTO::TRANSFERENCIA);
            $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteTO);

            unset($tramiteTO->id_tramite);
            $tramiteBO->cadastrarMatriculaTramite($matriculaTOrigem, $tramiteTO);

            $this->dao->commit();

            return $this->mensageiro->setMensageiro($matriculaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao transferir alunos.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita ContratoMatricula
     * @param ContratoMatriculaTO $cmTO
     * @return Ead1_Mensageiro
     */
    public function editarContratoMatricula(ContratoMatriculaTO $cmTO)
    {
        try {
            $this->dao->editarContratoMatricula($cmTO);
            return $this->mensageiro->setMensageiro('Vinculo do Contrato com a Matricula Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Vinculo do Contrato com a Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que matrícula diretamente
     * @param MatriculaTO $matTO
     * @param boolean $bl_transferencia
     * @param boolean $bl_envioimediato
     * @param boolean $ids_disciplinas
     * @return Ead1_Mensageiro
     */
    public function matricularDireto(MatriculaTO $matTO, $bl_transferencia = false, $bl_envioimediato = true, $ids_disciplinas = false, $bl_enviaemail = false)
    {
        $dao = new MatriculaDAO();
        $dao->beginTransaction();
        try {
            $tbProjetoPedagogicoORM = new ProjetoPedagogicoORM();

            $mTO = new MatriculaTO();
            $mTO->setBl_institucional(!$bl_transferencia);
            $mTO->setId_usuario($matTO->getId_usuario());
            $mTO->setId_usuariocadastro($mTO->getSessao()->id_usuario);
            $mTO->setId_entidadeatendimento($matTO->getId_entidadeatendimento() ? $matTO->getId_entidadeatendimento() : $mTO->getSessao()->id_entidade);
            $mTO->setId_entidadematricula($matTO->getId_entidadematricula() ? $matTO->getId_entidadematricula() : $mTO->getSessao()->id_entidade);
            $mTO->setId_evolucao(MatriculaTO::EVOLUCAO_MATRICULA_CURSANDO); // Cursando
            $mTO->setId_situacao(MatriculaTO::SITUACAO_MATRICULA_ATIVA); // Ativa
            $mTO->setDt_inicio(new Zend_Date());
            $mTO->setId_entidadematriz($tbProjetoPedagogicoORM->fetchRow("id_projetopedagogico = " . $matTO->getId_projetopedagogico())->id_entidadecadastro);
            $mTO->setId_projetopedagogico($matTO->getId_projetopedagogico());
            $mTO->setId_matriculaorigem($matTO->getId_matriculaorigem());
            $mTO->setBl_ativo(true);
            $mTO->setId_turma($matTO->getId_turma());

            ($bl_transferencia == true) ? $mTO->setId_vendaproduto($matTO->getId_vendaproduto()) : null;

            $mensageiroMatricula = $this->cadastrarMatricula($mTO);
            if ($mensageiroMatricula->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception($mensageiroMatricula->getFirstMensagem());
            } else {
                $matriculaTO = $mensageiroMatricula->getFirstMensagem();
            }

            if (!$dao->cadastrarMatriculaDisciplinaDiretaSP($matriculaTO->getId_matricula())) {
                throw new Zend_Exception("ERRO ao cadastrar disciplinas para a matrícula!");
            }

            $this->vincularPerfilAluno($matriculaTO, $bl_transferencia ? false : true);
            $dao->commit();

            $msgEmail = '';
            $salaDeAulaBO = new SalaDeAulaBO();
            $ng_matricula = new \G2\Negocio\Matricula();

            /*if ($bl_transferencia) {
                if ($ids_disciplinas) {
                    $salaDeAulaBO->alocarTransferencia($mTO, $bl_transferencia, $ids_disciplinas);
                }
                $ng_matricula->alocarAutomatico($matriculaTO, $bl_transferencia);
            }*/
            $ng_matricula->alocarAutomatico($matriculaTO, $bl_transferencia);

            if ($bl_transferencia && !$bl_enviaemail) {
                return new Ead1_Mensageiro($matriculaTO, Ead1_IMensageiro::SUCESSO);
            } else {
                //Verifica se o usuario tem e-mail padrão cadastrado
                $vwEmail = new VwPessoaEmailTO();
                $vwEmail->setId_usuario($matTO->getId_usuario());
                $vwEmail->setBl_padrao(true);
                $vwEmail->setId_entidade($matTO->getSessao()->id_entidade);
                $pessoaBO = new PessoaBO();
                $mensageiroVerificaEmail = $pessoaBO->retornarVwPessoaEmail($vwEmail);

                if ($mensageiroVerificaEmail->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($mensageiroVerificaEmail->getFirstMensagem());
                }

                $mensagemBO = new MensagemBO();
                $retornoEmail = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::MATRICULA, $mTO, TipoEnvioTO::EMAIL, $bl_envioimediato);
                $msgEmail = $retornoEmail->getFirstMensagem();

                /**
                 * SMS
                 */
//                $entidadeIntegracao = new EntidadeIntegracaoTO();
//                $entidadeIntegracao->setId_sistema(SistemaTO::TILTSMS);
//                $entidadeIntegracao->setId_entidade($mTO->getId_entidadematricula());
//
//                $enti = new EntidadeBO();
//                $mei = $enti->retornaEntidadeIntegracao($entidadeIntegracao);
//                if ($mei->getTipo() == Ead1_IMensageiro::SUCESSO) {
//                    $ei = $mei->getFirstMensagem();
//                    if ($ei->getId_entidadeintegracao()) {
//
//                        $vw = new PessoaTO();
//                        $vw->setId_entidade($mTO->getId_entidadematricula());
//                        $vw->setId_usuario($mTO->getid_usuario());
//                        $vw->fetch(false, true, true);
//                        if ($vw->getId_usuariocadastro()) {
//                            $mensageiroSMS = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO_SMS, $vw, TipoEnvioTO::SMS, false)->subtractMensageiro();
//                        }
//                    }
//                }
                /**
                 * fim SMS
                 */

            }

            return new Ead1_Mensageiro("Matrícula realizada com Sucesso! Integração: Envio de Email: " . $msgEmail);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que cadastra o aproveitamento das disciplinas já cursadas em outra matricula do aluno
     * @param $arrVwAproveitamentoDisciplinaInternoTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAproveitamentoDisciplinasInterno($arrVwAproveitamentoDisciplinaInternoTO)
    {
        try {
            if (!is_array($arrVwAproveitamentoDisciplinaInternoTO)) {
                THROW new Zend_Exception('O Parametro recebido não é array!');
            }
            $this->dao->beginTransaction();
            foreach ($arrVwAproveitamentoDisciplinaInternoTO as $vwAproveitamentoDisciplinaInternoTO) {
                if (!($vwAproveitamentoDisciplinaInternoTO instanceof VwAproveitamentoDisciplinaInternoTO)) {
                    THROW new Zend_Exception('Um dos objetos do array não é uma instancia de VwAproveitamentoDisciplinaInternoTO!');
                }
                //Editando Matricula Disciplina
                $matriculaDisciplinaTO = new MatriculaDisciplinaTO();
                $matriculaDisciplinaTO->setDt_conclusao(new Zend_Date(null, Zend_Date::ISO_8601));
                $matriculaDisciplinaTO->setId_disciplina($vwAproveitamentoDisciplinaInternoTO->getId_disciplina());
                $matriculaDisciplinaTO->setId_matricula($vwAproveitamentoDisciplinaInternoTO->getId_matricula());
                $matriculaDisciplinaTO->setNu_aprovafinal($vwAproveitamentoDisciplinaInternoTO->getNu_aprovafinal());
                $matriculaDisciplinaTO->setId_situacao(65);
                $matriculaDisciplinaTO->setId_evolucao(12);
                $where = 'id_matricula = ' . $matriculaDisciplinaTO->getId_matricula() . ' AND id_disciplina = ' . $matriculaDisciplinaTO->getId_disciplina();
                $this->dao->editarMatriculaDisciplina($matriculaDisciplinaTO, $where);
                //Cadastrando Aproveitamento
                $aproveitamentoDisciplinaTO = new AproveitamentoDisciplinaTO();
                $aproveitamentoDisciplinaTO->setSt_cargahoraria($vwAproveitamentoDisciplinaInternoTO->getNu_cargahoraria());
                $aproveitamentoDisciplinaTO->setSt_curso($vwAproveitamentoDisciplinaInternoTO->getSt_projetopedagogico());
                $aproveitamentoDisciplinaTO->setSt_disciplina($vwAproveitamentoDisciplinaInternoTO->getSt_disciplina());
                $aproveitamentoDisciplinaTO->setSt_instituicao($vwAproveitamentoDisciplinaInternoTO->getSt_nomeentidadematriculaorigem());
//				[Variavel movida para AproveitamentoMatriculaDisciplinaTO]
//				$aproveitamentoDisciplinaTO->setSt_notaoriginal($vwAproveitamentoDisciplinaInternoTO->getSt_notaoriginal());
                $aproveitamentoDisciplinaTO->setDt_conclusao($vwAproveitamentoDisciplinaInternoTO->getDt_conclusao());
                $aproveitamentoDisciplinaTO->setBl_ativo(true);
                $aproveitamentoDisciplinaTO->setId_municipio($vwAproveitamentoDisciplinaInternoTO->getId_municipio());
                $aproveitamentoDisciplinaTO->setId_usuariocadastro($vwAproveitamentoDisciplinaInternoTO->getSessao()->id_usuario);
                $aproveitamentoDisciplinaTO->setSg_uf($vwAproveitamentoDisciplinaInternoTO->getSg_uf());
                $id_aproveitamentodisciplina = $this->dao->cadastrarAproveitamentoDisciplina($aproveitamentoDisciplinaTO);
                $aproveitamentoDisciplinaTO->setId_aproveitamentodisciplina($id_aproveitamentodisciplina);
                //Cadastrando Vinculo do aproveitamento com a disciplina da matricula
                $aproveitamentoMatriculaDisciplinaTO = new AproveitamentoMatriculaDisciplinaTO();
//				[Adicionando variavel que estava no AproveitamentoDisciplinaTO]
                $aproveitamentoMatriculaDisciplinaTO->setSt_notaoriginal($vwAproveitamentoDisciplinaInternoTO->getSt_notaoriginal());
                $aproveitamentoMatriculaDisciplinaTO->setId_aproveitamentodisciplina($aproveitamentoDisciplinaTO->getId_aproveitamentodisciplina());
                $aproveitamentoMatriculaDisciplinaTO->setId_disciplina($vwAproveitamentoDisciplinaInternoTO->getId_disciplina());
                $aproveitamentoMatriculaDisciplinaTO->setId_matricula($vwAproveitamentoDisciplinaInternoTO->getId_matricula());
                $this->dao->cadastrarAproveitamentoMatriculaDisciplina($aproveitamentoMatriculaDisciplinaTO);
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Aproveitamento Realizado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Aproveitar Disciplinas!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Gera o vínculo entre uma vendaproduto e as séries e níveis
     * @param $vendaProdutoTO
     * @return bool
     */
    public function cadastrarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $vendaProdutoNivelSerieTO)
    {
        $matriculaDAO = new MatriculaDAO();
        return $matriculaDAO->cadastrarVendaProdutoNivelSerie($vendaProdutoNivelSerieTO);
    }

    /**
     * Meotodo que Cadastra Regra de Contrato
     * @param ContratoRegraTO $crTO
     * @see MatriculaRO::cadastrarContratoRegra
     * @return Ead1_Mensageiro
     */
    public function cadastrarContratoRegra(ContratoRegraTO $crTO)
    {
        $crTO->setId_entidade($crTO->getSessao()->id_entidade);
        $id_contratoregra = $this->dao->cadastrarContratoRegra($crTO);
        if (!$id_contratoregra) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Regra de contrato!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $crTO->setId_contratoregra($id_contratoregra);
        return $this->mensageiro->setMensageiro($crTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra Aproveitamento
     * @param AproveitamentoDisciplinaTO $adTO
     * @param array $arrMatriculaDisciplinaTO
     * @param array $arrAproveitamentoMatriculaDisciplinaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAproveitamento(AproveitamentoDisciplinaTO $adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplinaTO)
    {
        try {

            $this->dao->beginTransaction();
            $aproveitamentoMatriculaDisciplinaORM = new AproveitamentoMatriculaDisciplinaORM();
            $adTO->setId_usuariocadastro($adTO->getSessao()->id_usuario);
            $id_aproveitamentodisciplina = $this->dao->cadastrarAproveitamentoDisciplina($adTO);
            $adTO->setId_aproveitamentodisciplina($id_aproveitamentodisciplina);
            if (!empty($arrAproveitamentoMatriculaDisciplinaTO)) {
                foreach ($arrAproveitamentoMatriculaDisciplinaTO as $index => $aproveitamentoMatriculaDisciplinaTO) {
                    $consultaTO = new AproveitamentoMatriculaDisciplinaTO();
                    $consultaTO->setId_disciplina($aproveitamentoMatriculaDisciplinaTO->getId_disciplina());
                    $consultaTO->setId_matricula($aproveitamentoMatriculaDisciplinaTO->getId_matricula());
                    if ($consultaTO->getId_disciplina() && $aproveitamentoMatriculaDisciplinaORM->consulta($consultaTO)) {
                        $this->dao->rollBack();
                        return $this->mensageiro->setMensageiro('Pelo menos uma das Disciplinas selecionadas já foi aproveitada com essa matrícula!', Ead1_IMensageiro::AVISO);
                    }
                    $aproveitamentoMatriculaDisciplinaTO->setId_aproveitamentodisciplina($adTO->getId_aproveitamentodisciplina());
                    $this->dao->cadastrarAproveitamentoMatriculaDisciplina($aproveitamentoMatriculaDisciplinaTO);
                    $mdTOEdicao = $arrMatriculaDisciplinaTO[$index];
                    if ($mdTOEdicao->getId_disciplina()) {
                        $mdTOEdicao->setId_situacao(MatriculaDisciplinaTO::SITUACAO_CONCEDIDA);
                        $mdTOEdicao->setId_evolucao(MatriculaDisciplinaTO::EVOLUCAO_CONCLUIDA);
                        $mdTOEdicao->setDt_conclusao(new Zend_Date(null, Zend_Date::ISO_8601));
                        $where = 'id_matricula = ' . $mdTOEdicao->getId_matricula() . ' AND id_disciplina = ' . $mdTOEdicao->getId_disciplina();
                        $this->dao->editarMatriculaDisciplina($mdTOEdicao, $where);
                    }
                }
            }
            $adTO->setId_aproveitamentodisciplina($id_aproveitamentodisciplina);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($adTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro durante o cadastro de Aproveitamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que cadastra Aproveitamento
     * @param AproveitamentoDisciplinaTO $adTO
     * @param array $arrMatriculaDisciplinaTO
     * @param array $arrAproveitamentoMatriculaDisciplinaTO
     * @return Ead1_Mensageiro
     */
    public function editarAproveitamento(AproveitamentoDisciplinaTO $adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplinaTO)
    {
        try {
            $this->dao->beginTransaction();
            $this->dao->editarAproveitamento($adTO);
            $amdTO = new AproveitamentoMatriculaDisciplinaTO();
            $amdTO->setId_aproveitamentodisciplina($adTO->getId_aproveitamentodisciplina());
            $this->dao->deletarAproveitamentoMatriculaDisciplina($amdTO);
            if (!empty($arrAproveitamentoMatriculaDisciplinaTO)) {
                foreach ($arrAproveitamentoMatriculaDisciplinaTO as $index => $aproveitamentoMatriculaDisciplinaTO) {
                    $aproveitamentoMatriculaDisciplinaTO->setId_aproveitamentodisciplina($adTO->getId_aproveitamentodisciplina());
                    $this->dao->cadastrarAproveitamentoMatriculaDisciplina($aproveitamentoMatriculaDisciplinaTO);
                    $mdTOEdicao = $arrMatriculaDisciplinaTO[$index];
                    if ($mdTOEdicao->getId_disciplina()) {
                        $mdTOEdicao->setId_situacao(MatriculaDisciplinaTO::SITUACAO_CONCEDIDA);
                        $mdTOEdicao->setId_evolucao(MatriculaDisciplinaTO::EVOLUCAO_CONCLUIDA);
                        $mdTOEdicao->setDt_conclusao(new Zend_Date(null, Zend_Date::ISO_8601));
                        $where = 'id_matricula = ' . $mdTOEdicao->getId_matricula() . ' AND id_disciplina = ' . $mdTOEdicao->getId_disciplina();
                        $this->dao->editarMatriculaDisciplina($mdTOEdicao, $where);
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Aproveitamento Realizado com Sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro Durante o Cadastro de Aproveitamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Invocado após o cadastro de contrato.
     * Deve ser utilizado somente para vendas de contrato ( Éder )
     * Gera uma venda vinculado ao contrato
     * Registra uma venda inicial com todos os valores zerados
     * Seta a venda com o bl_contrato = true informando que a venda de um contrato
     * Seta a venda com o id_evolucao = 7 (Aguardando Negociação)
     * Seta a venda com o id_situacao = 46 (Venda Pendente)
     * Realiza o vínculo de venda para o contrato
     * @throws DomainException
     * @param ContratoTO $cTO
     * @param mixed $valorBruto
     * @return boolean
     */
    public function editarVendaContrato(ContratoTO $contratoTO, $valorBruto = 0.00)
    {

        if (empty($contratoTO->id_entidade)) {
            throw new DomainException('O contrato informado para a venda não possui entidade vinculada');
        }

        if (empty($contratoTO->id_usuario)) {
            throw new DomainException('O contrato informado para a venda não possui aluno vinculado');
        }

        $vendaTO = new VendaTO();
        $matriculaDAO = new MatriculaDAO();

        $bolsaPorcentagem = $contratoTO->getNu_bolsa();
        $bolsaValor = (($valorBruto * $bolsaPorcentagem) / 100);
        $vendaTO->id_venda = $contratoTO->id_venda;
        $vendaTO->bl_ativo = 1;
        $vendaTO->bl_contrato = true;
        $vendaTO->id_usuariocadastro = $vendaTO->getSessao()->id_usuario;
        $vendaTO->nu_descontoporcentagem = $bolsaPorcentagem;
        $vendaTO->nu_descontovalor = $bolsaValor;
        $vendaTO->nu_juros = 0.00;
        /**
         * @todo removendo o id_evolucao fixo
         * O id_evolucao fixo interfere na sequencia de processos que setam evoluções dos processos
         * na venda e prevenda.
         */
//		$vendaTO->id_evolucao				= 7;
        $vendaTO->id_situacao = 46;
        $vendaTO->nu_valorbruto = $valorBruto;
        $vendaTO->nu_valorliquido = ($valorBruto - $bolsaValor);
        $vendaTO->id_entidade = $contratoTO->id_entidade;
        $vendaTO->id_usuario = $contratoTO->id_usuario;


        return $matriculaDAO->editarVenda($vendaTO);
    }

    /**
     * Metodo que cadastra ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        try {
            $this->dao->cadastrarContratoConvenio($ccTO);
            return $this->mensageiro->setMensageiro('Contrato Vinculado com o Convenio com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Contrato com o Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Convenio
     * @param ConvenioTO $cTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarConvenio(ConvenioTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $id_convenio = $this->dao->cadastrarConvenio($cTO);
            $cTO->setId_convenio($id_convenio);
            return $this->mensageiro->setMensageiro($cTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Regra Contrato
     * @param ContratoRegraTO $crTO
     * @see MatriculaBO::editarContratoRegra
     * @return Ead1_Mensageiro
     */
    public function editarContratoRegra(ContratoRegraTO $crTO)
    {
        if (!$this->dao->editarContratoRegra($crTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Regra de Contrato!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro($crTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @return Ead1_Mensageiro
     */
    public function editarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        try {
            $this->dao->editarContratoConvenio($ccTO);
            return $this->mensageiro->setMensageiro('Vinculo do Contrato com o Convenio Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Vinculo do Contrato com o Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Convenio
     * @param ConvenioTO $cTO
     * @return Ead1_Mensageiro
     */
    public function editarConvenio(ConvenioTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $this->dao->editarConvenio($cTO);
            return $this->mensageiro->setMensageiro('Convenio Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui Contrato
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function deletarContrato(ContratoTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $this->dao->deletarContrato($cTO);
            return $this->mensageiro->setMensageiro('Contrato Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui ContratoConvenio
     * @param ContratoConvenioTO $ccTO
     * @return Ead1_Mensageiro
     */
    public function deletarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        try {
            $this->dao->deletarContratoConvenio($ccTO);
            return $this->mensageiro->setMensageiro('Vinculo do Contrato com o Convenio Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo do Contrato com o Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui Convenio
     * @param ConvenioTO $cTO
     * @return Ead1_Mensageiro
     */
    public function deletarConvenio(ConvenioTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $this->dao->deletarConvenio($cTO);
            return $this->mensageiro->setMensageiro('Convenio Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Convenio!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui o vínculo da Venda com o produto
     * @param VendaProdutoTO $vendaProdutoTO
     * @return Ead1_Mensageiro
     */
    public function deletarVendaProduto(VendaProdutoTO $vendaProdutoTO)
    {
        try {
            $this->dao->deletarVendaProduto($vendaProdutoTO);
            return $this->mensageiro->setMensageiro('Vínculo de Venda com Produto Excluído com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Vínculo de Venda com Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que verifica se existem vinculos com o contrato e exclui
     * @param ContratoTO $to
     * @return Ead1_Mensageiro
     */
    public function verificaExcluiContrato(ContratoTO $to)
    {
        try {
            $this->dao->beginTransaction();
            $contratoORM = new ContratoORM();
            $arrParams = array('id_contrato' => $to->getId_contrato());
            //,'id_situacao' => 47, 'id_evolucao' => 1);
            $contrato = $contratoORM->consulta(new ContratoTO($arrParams), false, true);
            if ($contrato) {
                $contrato = $contrato->toArray();
            }
            if ($contrato['id_situacao'] != 47 && $contrato['id_evolucao'] != 1) {
                THROW new Zend_Validate_Exception('O Contrato não pode ser Excluido porque o Contrato esta Ativo e/ou Confirmado!');
            }
            $vwMatriculaORM = new VwMatriculaORM(array('id_contrato' => $to->getId_contrato()));
            if ($vwMatriculaORM->consulta(new VwMatriculaTO(array('id_contrato' => $to->getId_contrato())), false, true)) {
                THROW new Zend_Validate_Exception('O Contrato não pode ser Excluido porque Existem Matrículas Vinculadas a Ele!');
            }
            $to->setBl_ativo(false);
            $this->dao->editarContrato($to);

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($contrato['id_venda']);
            $vendaTO->setBl_ativo(false);
            $produtoDAO = new ProdutoDAO();
            $produtoDAO->editarVenda($vendaTO);
            $this->dao->commit();
            $this->mensageiro->setMensageiro("Contrato Excluido com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro("Erro ao Excluir Contrato!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Exclui ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function deletarContratoEnvolvido(ContratoEnvolvidoTO $ceTO)
    {
        try {
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $this->dao->deletarContratoEnvolvido($ceTO);
            return $this->mensageiro->setMensageiro('Envolvido no Contrato Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluido Envolvido no Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que exclui a disciplina aproveitada
     * @param VwAproveitamentoTO $vwAproveitamentoTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaAproveitada(VwAproveitamentoTO $vwAproveitamentoTO)
    {
        try {
            return new Ead1_Mensageiro($this->dao->deletarDisciplinaAproveitada($vwAproveitamentoTO));
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao excluir a disciplina aproveitada!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as matriculas que tenham relação com a entidade da sessão
     * @param VwMatriculaTO $vmTO
     * @param int $id_entidade
     * @return Ead1_Mensageiro
     */
    public function retornarMatriculaRelacaoEntidadeSessao(VwMatriculaTO $vmTO, $id_entidade = null)
    {
        try {
            $id_entidade = $id_entidade ?: $vmTO->getSessao()->id_entidade;

            $where = "(id_entidadeatendimento = " . $id_entidade
                . " OR id_entidadematricula = " . $id_entidade
                . " OR id_entidadematriz = " . $id_entidade . ")";

            if ($vmTO->getId_entidadematricula()) {
                $where .= " AND id_entidadematricula = {$vmTO->getId_entidadematricula()}";
            }

            $where .= $vmTO->getId_matricula() ? " AND id_matricula = " . $vmTO->getId_matricula() : "";
            $where .= $vmTO->getId_usuario() ? " AND id_usuario = " . $vmTO->getId_usuario() : "";
            $where .= $vmTO->getId_evolucao() ? " AND id_evolucao = " . $vmTO->getId_evolucao() : "";

            $retorno = $this->dao->retornarVwMatricula($vmTO, $where);
            if (!empty($retorno)) {
                $retorno = $retorno->toArray();
            } else {
                return $this->mensageiro->setMensageiro('Nenhum Registro de vw Matrícula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwMatriculaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Matricula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function retornaResponsavelLegal($id_usuario)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            return $negocio->findOneBy('\G2\Entity\ResponsavelLegal', array('id_usuario' => $id_usuario));
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Responsável Legal', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de grade e sala do aluno.
     * @param VwMatriculaGradeSalaTO $vmgsTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMatriculaGradeSala(VwMatriculaGradeSalaTO $vmgsTO)
    {
        try {
            $dados = array();
            $dadosModulos = $this->dao->retornarVwMatriculaGradeSalaModulo($vmgsTO)->toArray();
            if (empty($dadosModulos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Grade de Sala Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            $dadosModulos = Ead1_TO_Dinamico::encapsularTo($dadosModulos, new VwMatriculaGradeSalaTO());
            foreach ($dadosModulos as $indexModulo => $modulo) {
                $vmgsTO->setId_modulo($modulo->getId_modulo());
                $dados[$indexModulo]['modulos'] = $modulo;
                $dadosDisciplinas = $this->dao->retornarVwMatriculaGradeSala($vmgsTO);
                $dadosDisciplinas = Ead1_TO_Dinamico::encapsularTo($dadosDisciplinas, new VwMatriculaGradeSalaTO());
                foreach ($dadosDisciplinas as $indexDisciplina => $disciplina) {
                    $dados[$indexModulo]['disciplinas'][$disciplina->getId_disciplina()] = $disciplina;
                }
            }
            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Grade da Matrícula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de grade do aluno por tipo.
     * @param VwGradeTipoTO $vwGradeTipoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwGradeTipo(VwGradeTipoTO $vwGradeTipoTO)
    {
        try {
            $dados = array();
            $arrTipoAvaliacao = array();
            $dadosModulos = $this->dao->retornarVwGradeTipoModulo($vwGradeTipoTO)->toArray();
            if (empty($dadosModulos)) {
                return $this->mensageiro->setMensageiro(null, Ead1_IMensageiro::SUCESSO);
            }
            $dadosModulos = Ead1_TO_Dinamico::encapsularTo($dadosModulos, new VwGradeTipoTO());
            foreach ($dadosModulos as $indexModulo => $modulo) {
                $vwGradeTipoTO->setId_modulo($modulo->getId_modulo());
                $dados[$indexModulo]['modulos'] = $modulo;
                $dadosDisciplinas = $this->dao->retornarVwGradeTipo($vwGradeTipoTO);
                $dadosDisciplinas = Ead1_TO_Dinamico::encapsularTo($dadosDisciplinas, new VwGradeTipoTO());
                foreach ($dadosDisciplinas as $indexDisciplina => $disciplina) {
                    if (empty($arrTipoAvaliacao[$disciplina->getId_tipoavaliacao()])) {
                        $arrTipoAvaliacao[$disciplina->getId_tipoavaliacao()] = $disciplina->getSt_labeltipo();
                    }
                    $dados[$indexModulo]['disciplinas'][$disciplina->getId_disciplina()][$disciplina->getId_tipoavaliacao()][] = $disciplina;
                }
                $dados[$indexModulo]['tiposAvaliacao'] = $arrTipoAvaliacao;
            }
            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Grade da Matrícula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os dados da Matrícula do Projeto e do Usuário
     *
     * @see MatriculaRO::retornarMatriculaUsuarioProjeto();
     *
     * @param VwUsuarioMatriculaProjetoTO $vumpTO
     * @return Ead1_Mensageiro
     */
    public function retornarMatriculaUsuarioProjeto(VwUsuarioMatriculaProjetoTO $vumpTO)
    {
        try {
            $retorno = $this->dao->retornarMatriculaUsuarioProjeto($vumpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::SUCESSO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwUsuarioMatriculaProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Matricula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Contrato
     * @param ContratoTO $cTO
     * @return Ead1_Mensageiro
     */
    public function retornarContrato(ContratoTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarContrato($cTO);

            if (!count($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Contrato Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }

            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno->toArray(), new ContratoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Contrato', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna aproveitamento de disciplina
     * @param AproveitamentoDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function retornarAproveitamento(AproveitamentoDisciplinaTO $adTO)
    {
        try {
            $vwaTO = new VwAproveitamentoTO();
            $vwaTO->setId_aproveitamentodisciplina($adTO->getId_aproveitamentodisciplina());
            $vwAproveitamentoDisciplinaORM = new VwAproveitamentoORM();
            $retorno = $vwAproveitamentoDisciplinaORM->consulta($vwaTO);
            if (!$retorno) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado Registro Aproveitamento Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro($retorno);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Aproveitamento de disciplina', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna disciplinas aproveitadas
     * @param AproveitamentoMatriculaDisciplinaTO $amdTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinaAproveitada(AproveitamentoMatriculaDisciplinaTO $amdTO)
    {
        try {
            $retorno = array();
            $aproveitamentoMatriculaDisciplinaORM = new AproveitamentoMatriculaDisciplinaORM();
            $dados = $aproveitamentoMatriculaDisciplinaORM->consulta($amdTO);
            if (!$dados) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Disciplina Aproveitada Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            foreach ($dados as $index => $aproveitamentoDisciplina) {
                $disciplinaTO = new DisciplinaTO();
                $disciplinaTO->setId_disciplina($aproveitamentoDisciplina->getId_disciplina());
                $disciplinaDAO = new DisciplinaDAO();
                $disciplina = $disciplinaDAO->retornaDisciplina($disciplinaTO)->toArray();
                if (empty($disciplina)) {
                    THROW new Zend_Exception('Não retornou disciplina do aproveitamento');
                }
                $disciplina = Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO(), true);
                $retorno[$index] = $disciplina;
            }
            return $this->mensageiro->setMensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Aproveitamento de disciplina', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Contrato Regra
     * @param ContratoRegraTO $crTO
     * @see MatriculaRO::retornarContratoRegra
     * @return Ead1_Mensageiro
     */
    public function retornarContratoRegra(ContratoRegraTO $crTO)
    {

        //Setando dados que são passados como false pelo flex indevidamente.
        $crTO->setBl_proporcaomes(null);
        $crTO->setBl_renovarcontrato(null);
        $crTO->setBl_ativo(null);

        try {

            $retorno = $this->dao->retornarContratoRegra($crTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Regra de Contrato Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoRegraTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Contrato Regra', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o ContratoConvenio
     * @param ContratoMatriculaTO $cmTO
     * @return Ead1_Mensageiro
     */
    public function retornarContratoConvenio(ContratoConvenioTO $ccTO)
    {
        try {
            $retorno = $this->dao->retornarContratoConvenio($ccTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Convênio Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoConvenioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Contrato do Convenio', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Convenio
     * @param ConvenioTO $cTO
     * @return Ead1_Mensageiro
     */
    public function retornarConvenio(ConvenioTO $cTO)
    {
        try {
            if (!$cTO->getId_entidade()) {
                $cTO->setId_entidade($cTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarConvenio($cTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Convênio Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ConvenioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Convenio', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o ContratoEnvolvido
     * @param ContratoEnvolvidoTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function retornarContratoEnvolvido(ContratoEnvolvidoTO $ceTO)
    {
        try {
            if (!$ceTO->getId_entidade()) {
                $ceTO->setId_entidade($ceTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarContratoEnvolvido($ceTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoEnvolvidoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Contrato do Envolvido!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo de Contrato do Responsavel
     * @param TipoContratoResponsavelTO $tcrTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoContratoResponsavel(TipoContratoResponsavelTO $tcrTO)
    {
        try {
            $retorno = $this->dao->retornarTipoContratoResponsavel($tcrTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoContratoResponsavelTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo de Contrato do Resposável!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo de Envolvido
     * @param TipoEnvolvidoTO $teTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoEnvolvido(TipoEnvolvidoTO $teTO)
    {
        try {
            $retorno = $this->dao->retornarTipoEnvolvido($teTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoEnvolvidoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo de Envolvido!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Evolucao
     * @param EvolucaoTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarEvolucao(EvolucaoTO $eTO)
    {
        try {
            $retorno = $this->dao->retornarEvolucao($eTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new EvolucaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Evolução!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de usuario com contrato responsavel
     * @param VwUsuarioContratoResponsavelTO $vwUcrTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwUsuarioContratoResponsavel(VwUsuarioContratoResponsavelTO $vwUcrTO)
    {
        try {
            $retorno = $this->dao->retornarVwUsuarioContratoResponsavel($vwUcrTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Usuário Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwUsuarioContratoResponsavelTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Responsáveis pelo Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de usuario com contrato responsavel
     * @param VwEntidadeContratoResponsavelTO $vwTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeContratoResponsavel(VwEntidadeContratoResponsavelTO $vwTO)
    {
        try {
            $retorno = $this->dao->retornarVwEntidadeContratoResponsavel($vwTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Pessoa Jurídica Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwEntidadeContratoResponsavelTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Responsáveis pelo Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de disciplinas para aproveitamento
     * @param VwMatriculaDisciplinaTO $vwmdTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMatriculaDisciplina(VwMatriculaDisciplinaTO $vwmdTO)
    {
        $label = new Ead1_LabelFuncionalidade();
        try {
            $retorno = $this->dao->retornarVwMatriculaDisciplina($vwmdTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado de ' . $label->getLabel(24) . ' para Aproveitamento', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwMatriculaDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar ' . $label->getLabel(24) . 's para aproveitamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de disciplinas para aproveitamento por area
     * @param VwMatriculaDisciplinaTO $vwmdTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMatriculaDisciplinaArea(VwMatriculaDisciplinaTO $vwmdTO)
    {
        $label = new Ead1_LabelFuncionalidade();
        try {
            $orm = new VwMatriculaDisciplinaORM();
            $whereSelect = '';
            if ($vwmdTO->getId_matricula()) {
                $whereSelect = 'id_matricula = ' . $vwmdTO->getId_matricula();
            }
            $where = $orm->select()->from('vw_matriculadisciplina', array('id_areaconhecimento', 'st_areaconhecimento'))->distinct(true)->where($whereSelect);
            $retorno = $this->dao->retornarVwMatriculaDisciplina($vwmdTO, $where)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado de ' . $label->getLabel(24) . ' para Aproveitamento', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwMatriculaDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar ' . $label->getLabel(24) . 's para aproveitamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de disciplinas para aproveitamento por serie
     * @param VwMatriculaDisciplinaTO $vwmdTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMatriculaDisciplinaSerie(VwMatriculaDisciplinaTO $vwmdTO)
    {
        $label = new Ead1_LabelFuncionalidade();
        try {
            $orm = new VwMatriculaDisciplinaORM();
            $whereSelect = '';
            if ($vwmdTO->getId_matricula()) {
                $whereSelect = 'id_matricula = ' . $vwmdTO->getId_matricula();
            }
            $where = $orm->select()->from('vw_matriculadisciplina', array('id_serie', 'st_serie'))->distinct(true)->where($whereSelect);
            $retorno = $this->dao->retornarVwMatriculaDisciplina($vwmdTO, $where)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado de ' . $label->getLabel(24) . ' para Aproveitamento', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwMatriculaDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar ' . $label->getLabel(24) . 's para aproveitamento');
        }
    }

    /**
     *
     * Retorna os dados da view de produto do contrato
     * @param VwProdutoContratoTO $produtoContratoTO
     * @throws InvalidArgumentException
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoContrato(VwProdutoContratoTO $produtoContratoTO)
    {
        try {
            if (!$produtoContratoTO->id_contrato) {
                THROW new Zend_Exception("Id do contrato não informado!");
            }

            $dados = $this->dao->retornarVwProdutoContrato($produtoContratoTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Não foi Encontrado Nenhum Produto para o Contrato');
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwProdutoContratoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Produtos do Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Pesquisa dos Alunos Concluintes
     * @param PesquisarAlunoTO $paTO
     * @return Ead1_Mensageiro
     */
    public function retornarAlunosConluintes(PesquisarAlunoTO $paTO)
    {
        try {
            //ID's Fixos
            $ID_EVOLUCAO_CONCLUINTE = 15;
            $ID_EVOLUCAO_CERTIFICADO = 16;
            //----------------------------
            $paTO->setBl_ativo(1);
            if (!$paTO->getId_entidadematricula()) {
                $paTO->setId_entidadematricula($paTO->getSessao()->id_entidade);
            }
            $arrWhere = array();
            $order = array('st_nomecompleto');

            if ($paTO->getSt_nomecompleto()) {
                $arrWhere[] = " st_nomecompleto like '%" . $paTO->getSt_nomecompleto() . "%'";
            }
            if ($paTO->getDt_inicio() && $paTO->getDt_termino()) {
                $arrWhere[] = "( dt_concluinte >= '" . $this->converteDataBanco($paTO->getDt_inicio()) . "' AND dt_concluinte <= '" . $this->converteDataBanco($paTO->getDt_termino()) . "' )";
            } elseif ($paTO->getDt_inicio() && !$paTO->getDt_termino()) {
                $arrWhere[] = " dt_concluinte >= '" . $this->converteDataBanco($paTO->getDt_inicio()) . "' ";
            } elseif (!$paTO->getDt_inicio() && $paTO->getDt_termino()) {
                $arrWhere[] = " dt_concluinte <= '" . $this->converteDataBanco($paTO->getDt_termino()) . "' ";
            }

            //dt_cadastrocertificacao
            if ($paTO->getDt_iniciocertificadogerado() && $paTO->getDt_terminocertificadogerado()) {
                $arrWhere[] = "( dt_cadastrocertificacao >= '" . $this->converteDataBanco($paTO->getDt_iniciocertificadogerado()) . ' 00:00:00' . "' AND dt_cadastrocertificacao <= '" . $this->converteDataBanco($paTO->getDt_terminocertificadogerado()) . ' 23:59:59' . "' )";
            } elseif ($paTO->getDt_iniciocertificadogerado() && !$paTO->getDt_termino()) {
                $arrWhere[] = " dt_cadastrocertificacao >= '" . $this->converteDataBanco($paTO->getDt_terminocertificadogerado()) . ' 00:00:00' . "' ";
            } elseif (!$paTO->getDt_iniciocertificadogerado() && $paTO->getDt_terminocertificadogerado()) {
                $arrWhere[] = " dt_cadastrocertificacao <= '" . $this->converteDataBanco($paTO->getDt_terminocertificadogerado()) . ' 23:59:59' . "' ";
            }

            //dt_enviocertificado
            if ($paTO->getDt_inicioenviadocertificadora() && $paTO->getDt_terminoenviadocertificadora()) {
                $arrWhere[] = "( dt_enviocertificado >= '" . $this->converteDataBanco($paTO->getDt_inicioenviadocertificadora()) . ' 00:00:00' . "' AND dt_enviocertificado <= '" . $this->converteDataBanco($paTO->getDt_terminoenviadocertificadora()) . ' 23:59:59' . "' )";
            } elseif ($paTO->getDt_inicioenviadocertificadora() && !$paTO->getDt_terminoenviadocertificadora()) {
                $arrWhere[] = " dt_enviocertificado >= '" . $this->converteDataBanco($paTO->getDt_inicioenviadocertificadora()) . ' 00:00:00' . "' ";
            } elseif (!$paTO->getDt_inicioenviadocertificadora() && $paTO->getDt_terminoenviadocertificadora()) {
                $arrWhere[] = " dt_enviocertificado <= '" . $this->converteDataBanco($paTO->getDt_terminoenviadocertificadora()) . ' 23:59:59' . "' ";
            }


            //dt_retornocertificadora
            if ($paTO->getDt_inicioretornadocertificadora() && $paTO->getDt_terminoretornadocertificadora()) {
                $arrWhere[] = "( dt_retornocertificadora >= '" . $this->converteDataBanco($paTO->getDt_inicioretornadocertificadora()) . ' 00:00:00' . "' AND dt_retornocertificadora <= '" . $this->converteDataBanco($paTO->getDt_terminoretornadocertificadora()) . ' 23:59:59' . "' )";
            } elseif ($paTO->getDt_inicioretornadocertificadora() && !$paTO->getDt_terminoretornadocertificadora()) {
                $arrWhere[] = " dt_retornocertificadora >= '" . $this->converteDataBanco($paTO->getDt_inicioretornadocertificadora()) . ' 00:00:00' . "' ";
            } elseif (!$paTO->getDt_inicioretornadocertificadora() && $paTO->getDt_terminoretornadocertificadora()) {
                $arrWhere[] = " dt_retornocertificadora <= '" . $this->converteDataBanco($paTO->getDt_terminoretornadocertificadora()) . ' 23:59:59' . "' ";
            }

            //dt_envioaluno
            if ($paTO->getDt_inicioenviadoaluno() && $paTO->getDt_terminoenviadoaluno()) {
                $arrWhere[] = "( dt_envioaluno >= '" . $this->converteDataBanco($paTO->getDt_inicioenviadoaluno()) . ' 00:00:00' . "' AND dt_envioaluno <= '" . $this->converteDataBanco($paTO->getDt_terminoenviadoaluno()) . ' 23:59:59' . "' )";
            } elseif ($paTO->getDt_inicioenviadoaluno() && !$paTO->getDt_terminoenviadoaluno()) {
                $arrWhere[] = " dt_envioaluno >= '" . $this->converteDataBanco($paTO->getDt_inicioenviadoaluno()) . ' 00:00:00' . "' ";
            } elseif (!$paTO->getDt_inicioenviadoaluno() && $paTO->getDt_terminoenviadoaluno()) {
                $arrWhere[] = " dt_envioaluno <= '" . $this->converteDataBanco($paTO->getDt_terminoenviadoaluno()) . ' 23:59:59' . "' ";
            }

//Zend_Debug::dump($paTO->getBl_documentacao());
            if ($paTO->getBl_documentacao() != '') {
                $arrWhere[] = " bl_documentacao=" . $paTO->getBl_documentacao();
            }

            if ($paTO->getId_evolucao() != '') {
                $arrWhere[] = " id_evolucao = " . $paTO->getId_evolucao();
            } else {
                $arrWhere[] = " id_evolucao in ({$ID_EVOLUCAO_CONCLUINTE},{$ID_EVOLUCAO_CERTIFICADO}) ";
            }

            if ($paTO->getId_evolucaocertificacao()) {
                $arrWhere[] = " id_evolucaocertificacao = " . $paTO->getId_evolucaocertificacao();
            }

            if (is_bool($paTO->getBl_disciplinacomplementar())) {
                $arrWhere[] = " bl_disciplinacomplementar = " . ($paTO->getBl_disciplinacomplementar() ? 1 : 0);
            }
            if ($paTO->getBl_academico() != '' && $paTO->getBl_academico() != 'selecione') {
                $arrWhere[] = " bl_academico =" . $paTO->getBl_academico();
            }

            if (!empty($arrWhere)) {
                $where = " ( " . implode(" AND ", $arrWhere) . " )";
                if ($paTO->getId_entidadematricula()) {
                    $where .= " AND id_entidadematricula = " . $paTO->getId_entidadematricula();
                } else {
                    $where .= " AND id_entidadematricula = " . $paTO->getSessao()->id_entidade;
                }
            } else {
                $where = " id_entidadematricula = " . $paTO->getId_entidadematricula();
            }
            if ($paTO->getId_projetopedagogico()) {
                $where .= " AND id_projetopedagogico = " . $paTO->getId_projetopedagogico();
            }
            $dados = $this->dao->retornarVwMatricula($paTO, $where, $order)->toArray();

            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Matrículas Encontrado!', Ead1_IMensageiro::AVISO);
            }

            $arrTO = Ead1_TO_Dinamico::encapsularTo($dados, new PesquisarAlunoTO());

            foreach ($arrTO as $index => $dado) {
                if ($dado->getDt_concluinte()) {
                    $dado->setSt_dataconcluinte($dado->getDt_concluinte()->toString('dd/MM/Y'));
                }
                $arrTO[$index] = $dado;
            }

            return $this->mensageiro->setMensageiro($arrTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Alunos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna os registros de livro registro existentes
     * Retorna um mensageiro com um array de LivroRegistroTO
     *
     * @param LivroRegistroTO $livroRegistroTO
     * @return Ead1_Mensageiro
     */
    public function retornarLivroRegistro(LivroRegistroTO $livroRegistroTO, $where = null)
    {

        try {
            $livroRegistroRS = $this->dao->retornarLivroRegistro($livroRegistroTO, $where);

            if ($livroRegistroRS->count() < 1) {
                throw new DomainException('Nenhum registro de livro registro encontrado');
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($livroRegistroRS, $livroRegistroTO), Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainExc) {
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro('Erro ao retornar Registro de livro', Ead1_IMensageiro::ERRO, $exc);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna as disciplinas que podem ser aproveitadas pelo aluno
     * @param VwAproveitamentoDisciplinaInternoTO $vwAdiTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitamentoDisciplinaInterno(VwAproveitamentoDisciplinaInternoTO $vwAdiTO)
    {
        try {
            $dados = $this->dao->retornarVwAproveitamentoDisciplinaInterno($vwAdiTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Disciplina a Aproveitar!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAproveitamentoDisciplinaInternoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas a Aproveitar!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as disciplinas já aproveitadas pelo aluno
     * @param VwAproveitamentoTO $vwaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitamento(VwAproveitamentoTO $vwaTO)
    {
        try {
            $dados = $this->dao->retornarVwAproveitamento($vwaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Disciplina a Aproveitar!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAproveitamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas a Aproveitar!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as avaliações para transferências
     * @param VwAproveitarAvaliacaoTO $vwaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitarAvaliacao(VwAproveitarAvaliacaoTO $vwaTO)
    {
        try {
            $dados = $this->dao->retornarVwAproveitarAvaliacao($vwaTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Nota a ser aproveitada!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAproveitarAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Notas a Aproveitar!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as disciplinas extras da matricula
     * @param VwDisciplinasExtrasTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwDisciplinasExtras(VwDisciplinasExtrasTO $to)
    {
        try {
            $dados = $this->dao->retornarVwDisciplinasExtras($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwDisciplinasExtrasTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas Extras!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as series e niveis do produto da venda
     * @param VendaProdutoNivelSerieTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $to)
    {
        try {
            $dados = $this->dao->retornarVendaProdutoNivelSerie($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro("Nenhum Registro de Séries Encontrado!", Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VendaProdutoNivelSerieTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Nivel e Serie!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Evolui a situação de uma matrícula para CERTIFICADA
     * Gera o trâmite correspodente
     * Caso não seja informado o id_entidadeatendimento da matrícula
     * é utilizado o id_entidade da sessão
     *
     * @param MatriculaTO
     * @return Ead1_Mensageiro
     */
    public function certificarMatricula(MatriculaTO $matriculaTO)
    {
        $this->dao->beginTransaction();
        try {

            if (empty($matriculaTO->id_matricula) || $matriculaTO->id_matricula == false) {
                throw new DomainException('Informe o id_matricula para certificar a matrícula');
            }

            if ($matriculaTO->getId_evolucaocertificacao() == MatriculaTO::EVOLUCAO_CERT_APTO_GERAR_CERTIFICADO) {
                $matriculaTO->setId_evolucaocertificacao(MatriculaTO::EVOLUCAO_CERT_CERTIFICADO_GERADO);

                //Registrando evolucao da certificacao
                $tramite = new \G2\Negocio\Tramite();
                $evolucao = \G2\Constante\Evolucao::getArrayCertificado();
                $stTramite = "A Evolução do certificado foi alterada para " . $evolucao[MatriculaTO::EVOLUCAO_CERT_CERTIFICADO_GERADO];
                $tramite->salvarTramiteMatricula($stTramite, $matriculaTO->getId_matricula());
            }

            if (!$this->dao->editarMatricula($matriculaTO)) {
                throw new DomainException('Não foi possível evoluir a matrícula para certificada');
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro('Matrícula Certificada com Sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainExc) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao cerificar a matrícula', Ead1_IMensageiro::ERRO, $exc->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Envia email com o link de boleto.
     * @param VwResumoFinanceiroTO $vwResumoFinanceiroTO
     * @return Ead1_Mensageiro
     */
    public function enviarEmailBoleto(VwResumoFinanceiroTO $vwResumoFinanceiroTO)
    {
        try {
            $vendaTo = new VendaTO();
            $vendaTo->setId_venda($vwResumoFinanceiroTO->getId_venda());
            $vendaTo->fetch(false, true, true);
            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_BOLETO, $vendaTo, TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->mensageiro->setMensageiro('Email Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao enviar Email', Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Envia o link para pagamento via cartão de crédito
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function enviarEmailCartao(VendaTO $to)
    {
        try {

            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_CARTAO_ACORDO, $to, TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->mensageiro->setMensageiro('Email Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao enviar Email', Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Envia o link para pagamento via cartão de crédito do acordo
     * @author Rafael Leite - rafael.leite@unyleya.com.br
     * @param VwResumoFinanceiroTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function enviarEmailCartaoAcordo(VwResumoFinanceiroTO $to)
    {
        try {

            $mensagemBO = new MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_CARTAO_ACORDO, $to, TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->mensageiro->setMensageiro('Email Enviado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao enviar Email. Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Envia o e-mail de Matrícula
     * @param VwMatriculaTO $vmTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function enviarArrayMensagemAtivacaoMatricula(VwMatriculaTO $vmTO)
    {
        try {

            if (!$vmTO->getId_turma()) {
                throw new Zend_Exception("Favor informar a Turma!");
            }

            $mensageiro = $this->retornarVwMatricula($vmTO);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {

                foreach ($mensageiro->getMensagem() as $vwmatricula) {
                    $to = new MatriculaTO();
                    $to->setId_matricula($vwmatricula->getid_matricula());
                    $to->fetch(true, true, true);
                    $arrTO[] = $to;
                }
                $mensagemBO = new MensagemBO();
                $retorno = $mensagemBO->enviarArrayMensagemAtivacaoMatricula($arrTO);

                $this->mensageiro = $retorno;
            } else {
                $this->mensageiro = $mensageiro;
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao enviar Email', Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a view de Matricula
     * @param VwMatriculaTO $vmTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMatricula(VwMatriculaTO $vmTO)
    {
        try {
            $retorno = $this->dao->retornarVwMatricula($vmTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de vws Matricula Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwMatriculaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Matricula', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que verifica se a matrícula possui pendências de Secretaria ou Financeiro
     * @param MatriculaTO $matriculaTO
     * @param boolean $secretaria
     * @param boolean $financeiro
     * @param boolean $pedagogico
     * @return boolean
     */
    public function verificaPendenciasMatricula($matriculaTO, $secretaria = false, $financeiro = false, $pedagogico = false)
    {
        $secretariaRO = new SecretariaRO();
        $financeiroRO = new FinanceiroRO();
        $pedagogicoRO = new ProjetoPedagogicoRO();

        if ($secretaria) {
            $pendencia = ($secretariaRO->retornarStatusSecretaria($matriculaTO)->getCurrent() == 'Pendente' ? true : false);
        }

        if ($financeiro && !$pendencia) {
            $pendencia = ($financeiroRO->retornarStatusFinanceiro($matriculaTO)->getCurrent() == 'Pendente' ? true : false);
        }

        if ($pedagogico && !$pendencia) {
            $pendencia = ($pedagogicoRO->retornarStatusPedagogico($matriculaTO)->getCurrent() == 'Pendente' ? true : false);
        }

        return $pendencia;
    }

    public function trataMedia($valor)
    {
        if (empty($valor)) {
            return '--';
        }
        if ($valor / 2) {
            if (substr($valor, -2, -1) == ',' || substr($valor, -3, -2) == ',') {
                $valor = str_replace(',', '.', $valor);
                if (substr($valor, -2, -1) == '.') {
                    $valor .= '0';
                    return $valor;
                }
            }
            if (strlen($valor) == 1) {
                $valor .= '.00';
                return $valor;
            }
            return Ead1_BO::toFloat($valor, 2);
        } else {
            return $valor;
        }
    }

    /**
     * Método que une os responsáveis pessoa fisica e jurida em apenas um array
     * @param unknown_type $id_venda
     * @param unknown_type $id_tipocontratoresponsavel
     * @param unknown_type $id_contrato
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarContratoResponsaveisArrayUnico($id_venda, $id_tipocontratoresponsavel, $id_contrato = 0)
    {
        try {
            $mesageiro = $this->retornarContratoResponsaveis($id_venda, $id_tipocontratoresponsavel, $id_contrato = 0);
            $array = $mesageiro->getMensagem();
            $dados = array_merge($array[0], $array[1]);

            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar dados do responsáveis: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    public function retornarContratoResponsaveis($id_venda, $id_tipocontratoresponsavel, $id_contrato = 0)
    {
        try {
            $vwUcrTO = new VwUsuarioContratoResponsavelTO();
            $vwUcrTO->setId_contrato($id_contrato);
            $vwUcrTO->setId_venda($id_venda);
            $vwUcrTO->setId_tipocontratoresponsavel($id_tipocontratoresponsavel);
            $retornoFisicas = $this->dao->retornarVwUsuarioContratoResponsavel($vwUcrTO)->toArray();

            $dados[] = Ead1_TO_Dinamico::encapsularTo($retornoFisicas, new VwUsuarioContratoResponsavelTO());

            $vwTO = new VwEntidadeContratoResponsavelTO();
            $vwTO->setId_contrato($id_contrato);
            $vwTO->setId_venda($id_venda);
            $vwTO->setId_tipocontratoresponsavel($id_tipocontratoresponsavel);
            $retornoJuridicas = $this->dao->retornarVwEntidadeContratoResponsavel($vwTO)->toArray();

            $dados[] = Ead1_TO_Dinamico::encapsularTo($retornoJuridicas, new VwEntidadeContratoResponsavelTO());

            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Responsáveis pelo Contrato!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function retornarResposavelFinanceiro($id_matricula)
    {

        $responsaveis = $this->dao->retornarResponsavelFinanceiro($id_matricula);

        $mensageiro = new Ead1_Mensageiro();
        if ($responsaveis) {
            $mensageiro->setMensageiro($responsaveis, Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro->setMensageiro(null, Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function retornarIdVendaMatricula($id_matricula)
    {

        if (\Ead1_Sessao::getSessaoGeral()->id_linhadenegocio === \G2\Constante\LinhaDeNegocio::GRADUACAO) {
            $id_venda = $this->dao->retornarIdUltimaVendaGraduacao($id_matricula);
        } else {
            $id_venda = $this->dao->retornarIdVendaMatricula($id_matricula);
        }


        $mensageiro = new Ead1_Mensageiro();
        if ($id_venda) {
            $mensageiro->setMensageiro($id_venda, Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro->setMensageiro(null, Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * SubProcesso de Ativar Matricula que valida os lançamentos de entrada.
     * @param ContratoTO $contratoTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Exception
     */
    private function _subProcessoAtivarMatriculaValidaLancamentosEntrada(ContratoTO $contratoTO)
    {
        $produtoDAO = new ProdutoDAO();
        $lvTO = new LancamentoVendaTO();
        $lvTO->setId_venda($contratoTO->getId_venda());
        $lvTO->setBl_entrada(true);
        $where = 'id_venda = ' . $lvTO->getId_venda() . ' AND bl_entrada = ' . $lvTO->getBl_entrada();
        //Retornando os Lançamentos de Entrada
        $lancamentoVenda = $produtoDAO->retornarLancamentoVenda($lvTO, $where)->toArray();
        if (!empty($lancamentoVenda)) {
            $arrWhere = array();
            //Este Foreach montara o where para retornar os lançamentos da venda
            foreach ($lancamentoVenda as $index => $lanVenda) {
                $arrWhere[$lanVenda['id_lancamento']] = $lanVenda['id_lancamento'];
            }
            $where = 'id_lancamento in (' . implode(', ', $arrWhere) . ')';
            //Retorna os Lancamentos de entrada
            $lancamentos = $produtoDAO->retornarLancamento(new LancamentoTO(), $where)->toArray();
            //verificar os lancamentos
            foreach ($lancamentos as $lancamento) {
                switch ($lancamento['id_meiopagamento']) {
                    case MeioPagamentoTO::CARTAO:
                        if (is_null($lancamento['dt_prevquitado'])) {
                            THROW new Zend_Validate_Exception('Não foi Possivel Ativar a Matricula, um dos Lançamentos de Entrada não tem a Data de Quitado.');
                        }
                        break;
                    case MeioPagamentoTO::CHEQUE:
                        if (is_null($lancamento['dt_prevquitado'])) {
                            THROW new Zend_Validate_Exception('Não foi Possivel Ativar a Matricula, um dos Lançamentos de Entrada não tem a Data de Quitado.');
                        }
                        break;
                    case MeioPagamentoTO::BOLETO:
                        if ($lancamento['bl_quitado'] != 1) {
                            THROW new Zend_Validate_Exception('Não foi Possivel Ativar a Matricula, um dos Lançamentos de Entrada não Esta Quitado.');
                        }
                        break;
                    case MeioPagamentoTO::DINHEIRO:
                        if ($lancamento['bl_quitado'] != 1) {
                            THROW new Zend_Validate_Exception('Não foi Possivel Ativar a Matricula, um dos Lançamentos de Entrada não Esta Quitado.');
                        }
                        break;
                    default:
                        THROW new Zend_Exception('O id_lancamento(' . $lancamento['id_lancamento'] . ' não esta entre os meios de pagamento previstos.)');
                        break;
                }
            }
        }
    }

    public function gerarAditivo($contratoTo, $matricula, $aditivo = null, $matriculaorigem = null)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $objContrato = $negocio->find('\G2\Entity\ContratoRegra', $contratoTo->getId_contratoregra());
            $to = new TextoSistemaTO();

            //verifica se de fato encontrou o contrato regra
            if (!$objContrato instanceof \G2\Entity\ContratoRegra) {
                throw  new \Exception("Contrato Regra id " . $contratoTo->getId_contratoregra() . " não encontrado na base de dados.");
            }

            $idVenda = $contratoTo->getId_venda();

            if ($aditivo == \G2\Constante\TipoAditivo::RENOVACAO) {
                $to->setId_textosistema($objContrato->getId_extensaomodelo());
                $nome_arquivo = 'AditivoRenovacao' . $contratoTo->getId_venda() . '.pdf';
                $mat = $matricula['id_matricula'];
            } elseif ($aditivo == \G2\Constante\TipoAditivo::TRANSFERENCIA) {

                //verifica se encontrou o aditivo transferencia
                if (!$objContrato->getId_aditivotransferencia() instanceof \G2\Entity\TextoSistema) {
                    throw new Exception("Id aditivo transferência não encontrado.");
                }

                $to->setId_textosistema($objContrato->getId_aditivotransferencia()->getId_textosistema());
                $nome_arquivo = 'AditivoTransferencia' . $contratoTo->getId_venda() . '_' . $matricula['id_matricula'] . '.pdf';
                $mat = $matriculaorigem;
            } else {
                return array('title' => 'Erro', 'text' => 'Tipo de aditivo não definido. ', 'type' => 'error');
            }

            $objVwMatricula = $negocio->find('\G2\Entity\VwMatricula', $mat);
            $objVendaProduto = $negocio->find('\G2\Entity\VendaProduto', $objVwMatricula->getId_vendaproduto());

            if ($objVendaProduto instanceof \G2\Entity\VendaProduto) {
                $idVenda = $idVenda ?: $objVendaProduto->getId_venda()->getId_venda();
            } else {
                $objVendaAditivo = $negocio->findOneBy('\G2\Entity\VendaAditivo', array('id_matricula' => $matricula['id_matricula']));
                if ($objVendaAditivo instanceof \G2\Entity\VendaAditivo) {
                    $idVenda = $objVendaAditivo->getId_venda()->getId_venda();
                }
            }

            $path = realpath(APPLICATION_PATH . '/../public/upload/minhapasta/') . '/' . $idVenda . '/';
            $bo = new TextoSistemaBO();

            $mensageiro = $bo->gerarHTML($to, array('id_contrato' => $contratoTo->getId_contrato()));

            if ($mensageiro->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception($mensageiro->getFirstMensagem());
            }

            $html = mb_detect_encoding($mensageiro->getFirstMensagem()->getSt_texto(), 'UTF-8', true) === TRUE
                ? $mensageiro->getFirstMensagem()->getSt_texto()
                : utf8_encode($mensageiro->getFirstMensagem()->getSt_texto());

            $ead1PDF = new Ead1_Pdf();
            $ead1PDF->carregarHTML($html)->configuraPapel();

            $minhaPastaNegocio = new \G2\Negocio\MinhaPasta();

            //Se já existir uma pasta criada...
            if (is_dir($path)) {
                //...dá permissao total a pasta para garantir que vai salvar o contrato na mesma
                chmod($path, 0777);
                //...salvar o contrato no na tabela "tb_minhapasta"
                $minhaPastaNegocio->salvarContratoMinhaPasta(
                    $nome_arquivo,
                    $contratoTo->getId_venda(),
                    $matricula['id_matricula'],
                    null,
                    $nome_arquivo
                );
                //...gera e salva o contrato em pdf na pasta
                $ead1PDF->gerarPdf($nome_arquivo, $path);
                //... retorna uma mensagem de sucesso
                return array('title' => 'Sucesso', 'text' => 'Termo aditivo gerado.', 'type' => 'success');
            } else {//...se não...
                //...cria a pasta e da permissão total
                $oldmask = umask(0);
                mkdir($path, 0777);
                umask($oldmask);
                //se por algum probela não conseguir criar a pasta...
                if (!is_dir($path)) {
                    //... retorna uma mensagem de erro
                    return array('title' => 'Erro', 'text' => 'Houve um erro ao gerar diretório para o termo aditivo. ', 'type' => 'error');
                } else {//...se criar a pasta com sucesso...
                    //...salvar o contrato no na tabela "tb_minhapasta"
                    $minhaPastaNegocio->salvarContratoMinhaPasta(
                        $nome_arquivo,
                        $contratoTo->getId_venda(),
                        $matricula['id_matricula'],
                        null,
                        $nome_arquivo
                    );
                    //...gera e salva o contrato em pdf na pasta
                    $ead1PDF->gerarPdf($nome_arquivo, $path);
                    //... retorna uma mensagem de sucesso
                    return array('title' => 'Sucesso', 'text' => 'Termo aditivo gerado.', 'type' => 'success');
                }
            }
        } catch (\Exception $e) {
            return array('title' => 'Erro', 'text' => 'Houve um erro ao gerar o termo aditivo. ' . $e->getMessage(), 'type' => 'error');
        }

    }

    public function retornaGenero($id_matricula)
    {
        $negocio = new \G2\Negocio\Negocio();
        $result = $negocio->getRepository('G2\Entity\Matricula')->retornaGenero($id_matricula);
        return $result;
    }

}

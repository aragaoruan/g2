<?php 
/**
 * Classe com regras de negócio para interações de Horarios de Aula
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 11/11/2010
 * 
 * @package models
 * @subpackage bo
 */
class HorarioAulaBO extends Ead1_BO{
	
	public $mensageiro;
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new HorarioAulaDAO();
	}
		
	/**
	 * Metodo que Decide se Cadastra ou Exclui o HorarioDiaSemana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		$orm = new HorarioDiaSemanaORM();
		if($orm->consulta($hdsTO,true)){
			return $this->deletarHorarioDiaSemana($hdsTO);
		}
		return $this->cadastrarHorarioDiaSemana($hdsTO);
	}
		
	/**
	 * Metodo que Cadastra e Exclui os Vinculos do Horario com o Dias da Semanas
	 * @param HorarioAulaTO $haTO
	 * @param array (HorarioDiaSemanaTO $arrhdsTO)
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayHorarioAulaDiaSemana(HorarioAulaTO $haTO, $arrhdsTO){
		
		if(!$haTO->getId_entidade()){
			$haTO->setId_entidade($haTO->getSessao()->id_entidade);
		}
		
		if(!is_array($arrhdsTO)){
			return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!',Ead1_IMensageiro::AVISO);
		}
		
		if($this->comparaHora($haTO->getHr_inicio(),$haTO->getHr_fim()) === -1){
			return $this->mensageiro->setMensageiro('A Hora de Início não Pode Ser Maior que a Hora de Término!',Ead1_IMensageiro::AVISO);
		}

		if($this->comparaHora($haTO->getHr_inicio(),$haTO->getHr_fim()) === -2){
			return $this->mensageiro->setMensageiro('O período não pode ter mais do que 4 horas de duração!',Ead1_IMensageiro::AVISO);
		}
		
		$this->dao->beginTransaction();
		
		if(!$haTO->getId_horarioaula()){
			$msg = $haTO;
			$mensageiro = $this->cadastrarHorarioAula($haTO);
			if($mensageiro->tipo != 1){
				$this->dao->rollBack();
				return $mensageiro;
			}
			$haTO = $mensageiro->mensagem[0];
		}else{
			$msg = 'Horário Aula Editado com Sucesso!';
			$mensageiro = $this->editarHorarioAula($haTO);
			if($mensageiro->tipo != 1){
				$this->dao->rollBack();
				return $mensageiro;
			}
		}
		$delhdsTO = new HorarioDiaSemanaTO();
		$delhdsTO->setId_horarioaula($haTO->getId_horarioaula());
		try{
			$mensageiro = $this->deletarHorarioDiaSemana($delhdsTO);
			if($mensageiro->tipo != 1){
				THROW new Zend_Exception('Erro ao Excluir Vinculos do Horario da Aula com os Dias da Semana!');
			}
			foreach($arrhdsTO as $hdsTO){
				if(!($hdsTO instanceof HorarioDiaSemanaTO)){
					Throw new Zend_Exception('O Objeto não é uma Instancia da Classe HorarioDiaSemanaTO!');
				}
				$hdsTO->setId_horarioaula($haTO->getId_horarioaula());
				if(!$this->dao->cadastrarHorarioDiaSemana($hdsTO)){
					Throw new Zend_Exception('Erro ao Vincular Horario da Aula com os Dias da Semana!');
				}
			}
			
			if(!$this->dao->executaCriaperiodoh7( $haTO )){
				Throw new Zend_Exception('Erro ao Vincular Horario da Aula com o Sistema da Catraca');
			}
			
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($msg,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
			$this->mensageiro->setCodigo($this->dao->excecao);
			$this->mensageiro->setMensagem($e->getMessage());
			return $this->mensageiro;
		}
		
	}
	
	/**
	 * Metodo que Cadastra Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarHorarioAula(HorarioAulaTO $haTO){
		if(!$haTO->getId_entidade()){
			$haTO->setId_entidade($haTO->getSessao()->id_entidade);
		}
		if(!$this->comparaHora($haTO->getHr_inicio(),$haTO->getHr_fim())){
			return $this->mensageiro->setMensageiro('A Hora de Início não Pode ser Maior que a Hora de Fim!',Ead1_IMensageiro::AVISO);
		}
		$id_horarioAula = $this->dao->cadastrarHorarioAula($haTO);
		if(!$id_horarioAula){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Horário da Aula!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$haTO->setId_horarioaula($id_horarioAula);
		return $this->mensageiro->setMensageiro($haTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Cadastra o Horario em Determinado Dia da Semana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		if(!$this->dao->cadastrarHorarioDiaSemana($hdsTO)){
			return $this->mensageiro->setMensageiro('Erro ao Vincular Horario ao Dia da Semana!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro($hdsTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Edita o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function editarHorarioAula(HorarioAulaTO $haTO){
		if(!$this->dao->editarHorarioAula($haTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar o Horário da Aula!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Horário da Aula Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Exclui o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioAula(HorarioAulaTO $haTO){
		if(!$this->dao->deletarHorarioAula($haTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Horário da Aula!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Horário da Aula Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Exclui o Horario da Aula e os seus vínculos de dia de semana
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioAulaDiaSemana(HorarioAulaTO $haTO){
		if(!$this->dao->deletarHorarioAulaDiaSemana($haTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Horário da Aula!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Horário da Aula Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Exclui Vinculo do Horario da Aula com o Dia da Semana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		if(!$this->dao->deletarHorarioDiaSemana($hdsTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo do Dia da Semana!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo do Dia da Semana Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarHorarioAula(HorarioAulaTO $haTO){
		if(!$haTO->getId_entidade()){
			$haTO->setId_entidade($haTO->getSessao()->id_entidade);
		}
		$horarioAula = $this->dao->retornarHorarioAula($haTO);
		if(is_object($horarioAula)){
			$horarioAula = $horarioAula->toArray();
		}else{
			unset($horarioAula);
		}
		if(!$horarioAula){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($horarioAula, new HorarioAulaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Dia da Semana do Horario da Aula
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		$dia = $this->dao->retornarHorarioDiaSemana($hdsTO);
		if(is_object($dia)){
			$dia = $dia->toArray();
		}else{
			unset($dia);
		}
		if(!$dia){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dia, new HorarioDiaSemanaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Turno
	 * @param TurnoTO $tTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurno(TurnoTO $tTO){
		$turno = $this->dao->retornarTurno($tTO);
		if(is_object($turno)){
			$turno = $turno->toArray();
		}else{
			unset($turno);
		}
		if(!$turno){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($turno, new TurnoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna DIa da Semana
	 * @param DiaSemanaTO $dsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDiaSemana(DiaSemanaTO $dsTO){
		$dia = $this->dao->retornarDiaSemana($dsTO);
		if(is_object($dia)){
			$dia = $dia->toArray();
		}else{
			unset($dia);
		}
		if(!$dia){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dia, new DiaSemanaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Turno e o Horario da Aula
	 * @param VwTurnoHorarioAulaTO $vwthaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurnoHorarioAula(VwTurnoHorarioAulaTO $vwthaTO){
		$turnoHorario = $this->dao->retornarTurnoHorarioAula($vwthaTO);
		if(is_object($turnoHorario)){
			$turnoHorario = $turnoHorario->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar o Turno e o Horário da Aula!',Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		if(empty($turnoHorario)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($turnoHorario, new VwTurnoHorarioAulaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Retorna o Dia da Semana por horario aula
	 * @param VwHorarioAulaDiaSemanaTO $vwhadsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarHorarioAulaDiaSemana(VwHorarioAulaDiaSemanaTO $vwhadsTO){
		$horarioDia = $this->dao->retornarHorarioAulaDiaSemana($vwhadsTO);
		if(is_object($horarioDia)){
			$horarioDia = $horarioDia->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar o Turno e o Horário da Aula!',Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		if(empty($horarioDia)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($horarioDia, new VwHorarioAulaDiaSemanaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Turno, horario da aula e os Dias da Semana
	 * @param VwTurnoHorarioAulaTO $vwthaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurnoHorarioAulaArrDiaSemana(VwTurnoHorarioAulaTO $vwthaTO){
		if(!$vwthaTO->getId_entidade()){
			$vwthaTO->setId_entidade($vwthaTO->getSessao()->id_entidade);
		}
		$turnoHorarioAula = $this->dao->retornarTurnoHorarioAula($vwthaTO);
		$dados = array();
		if(is_object($turnoHorarioAula)){
			$turnoHorarioAula = $turnoHorarioAula->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar o Turno e o Horário da Aula!',Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		if(empty($turnoHorarioAula)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		foreach($turnoHorarioAula as $chave => $horarioaula){
			$retorno[0] = $horarioaula;
			$dados[$horarioaula['id_horarioaula']] = Ead1_TO_Dinamico::encapsularTo($retorno, new VwTurnoHorarioAulaTO, true);
			$hdsTO = new VwHorarioAulaDiaSemanaTO();
			$hdsTO->setId_horarioaula($horarioaula['id_horarioaula']);
			$mensageiro = $this->retornarHorarioAulaDiaSemana($hdsTO);
			if($mensageiro->tipo == 3){
				return $mensageiro;
			}elseif($mensageiro->tipo == 2){
				$dados[$horarioaula['id_horarioaula']]->arr_diasemana = null; 
			}else{
				$dados[$horarioaula['id_horarioaula']]->arr_diasemana = $mensageiro->mensagem;
			}
		}
		return $this->mensageiro->setMensageiro($dados,Ead1_IMensageiro::SUCESSO);
	}
	
}
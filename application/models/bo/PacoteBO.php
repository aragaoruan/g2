<?php

/**
 * Classe com regras de negócio para o Pacote
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 12/06/2012
 * @package models
 * @subpackage bo
 */
class PacoteBO extends Ead1_BO {
	
	
	/**
	 * @var PacoteDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new PacoteDAO();
	}
	
	
	/**
	 * Método de Matrículas disponíveis para o Pacote
	 * @param VwPacoteMatriculaTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarPacoteMatricula(VwPacoteMatriculaTO $to){
		
		try {
			
			$orm = new VwPacoteMatriculaORM();
			$resultado = $orm->consulta($to);
			return new Ead1_Mensageiro($resultado, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Matrículas disponíveis para o Pacote: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * Método de listagem de Pacote
	 * @param PacoteTO $pacoteTO
	 * @return Ambigous <boolean, multitype:>
	 * @see
	 */
	public function listarPacote(PacoteTO $pacoteTO){
	
		try {
			return new Ead1_Mensageiro($this->dao->listarPacote($pacoteTO), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar os Pacotes: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}
	
	
	/**
	 * Metodo que decide se cadastra ou edita o Pacote
	 * @param PacoteTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarPacote(PacoteTO $to){
	
		//$mensageiro = false;
		$this->dao->beginTransaction();
		try {
			
			if(!$to->getId_remessa()){
				throw new Zend_Exception("Favor informar uma Remessa.");
			}			
			
			$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			
			if($to->getId_pacote()){
				$mensageiro = $this->editarPacote($to);
			} else {
				$mensageiro =  $this->cadastrarPacote($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o Pacote: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}

	
	/**
	 * Método que cadastra umo Pacote
	 * @param PacoteTO $pacoteTO
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see PacoteBO::salvarPacote();
	 */
	public function cadastrarPacote(PacoteTO $pacoteTO){
		$this->dao->beginTransaction();
		try {
			
			
			//$this->verificaPacoteAtiva($pacoteTO);
			
			$pacoteTO->setId_pacote($this->dao->cadastrarPacote($pacoteTO));
			$pacoteTO->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($pacoteTO, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o Pacote, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	/**
	 * Método que edita umo Pacote
	 * @param PacoteTO $pacoteTO
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see PacoteBO::salvarPacote();
	 */
	public function editarPacote(PacoteTO $pacoteTO){
		$this->dao->beginTransaction();
		try {
			
			//$this->verificaPacoteAtiva($pacoteTO);
			
			$this->dao->editarPacote($pacoteTO);
			$pacoteTO->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($pacoteTO, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o Pacote, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	
	
	/**
	 * Cadastra a entrega de um material para o processo de entrega
	 * @param array EntregaMaterialTO $em ou EntregaMaterialTO $em
	 * @param PacoteTO $paTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaMaterial($arem, PacoteTO $paTO = null){
		
		if($arem instanceof EntregaMaterialTO){
			$arem[] = $arem;
		}
		
		$this->dao->beginTransaction();
		try {
		
			foreach ($arem as $em){
				$em->setid_entregamaterial($this->dao->cadastrarEntregaMaterial($em));
				$em->fetch(true,true,true);

				if($paTO){
					$peTO = new PacoteEntregaTO();
					$peTO->setId_pacote($paTO->getId_pacote());
					$peTO->setId_entregamaterial($em->getid_entregamaterial());
					$peORM = new PacoteEntregaORM();
					$peORM->insert($peTO->toArrayInsert());
				}
			}
		
			$this->dao->commit();
			return new Ead1_Mensageiro($arem, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar a entrega do material, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	
	/**
	 * Edita a entrega de um material para o processo de entrega
	 * @param EntregaMaterialTO $em
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $em){
		$this->dao->beginTransaction();
		try {
	
			$this->dao->editarEntregaMaterial($em);
			$this->dao->commit();
			return new Ead1_Mensageiro($em, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar a Entrega de Material, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
}

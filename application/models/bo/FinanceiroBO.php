<?php
/**
 * Classe com regras de negócio para interações de Financeiro
 * @author Eder Lamar - eder.mariano@ead1.com.br
 * 
 * [Ead1 Componentes Utilizados]
 * @see Ead1_BO
 * @see Ead1_Mensageiro
 * 
 * [DAO Utilizadas]
 * @see FinanceiroDAO
 * 
 * [TO Utilizados]
 * @see VendaTO
 * @see VwProdutoVendaTO
 * @see VwLancamentoTO
 * @see LancamentoTO
 * @see VendaProdutoTO
 * @see LancamentoVendaTO
 * 
 * @since 05/11/2010
 * 
 * @package models
 * @subpackage bo
 */
class FinanceiroBO extends Ead1_BO{
	
	
	public function __construct ()
	{
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		
	}
	
	/**
	 * Método que possui as regras de negócio para o retorno de vendas e retorna um mensageiro
	 * 	Usar FinanceiroDAO::retornarVenda();
	 *
	 * Método já implementado em outras classes
	 * 
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que possui as regras de negócio para o retorno de vendas produtos e os dados necessários para a tela 
	 * 	Usar FinanceiroDAO::retornarVendaProduto();
	 *
	 * @TODO implementar
	 * 
	 * @param VwProdutoVendaTO $vpvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaProduto(VwProdutoVendaTO $vpvTO) {
	
	}
	
	/**
	 * Método que possui as regras de negócio para retornar lancamentos, retorna um mensageiro
	 * 	Usar FinanceiroDAO::retornarLancamento();
	 *
	 * @TODO implementar
	 * 
	 * @param VwLancamentoTO $vlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLancamento(VwLancamentoTO $vlTO) {
	
	}
	
	/**
	 * Método que possui as regras de negócio para registro de uma venda e retorna um mensageiro
	 * 	O cadastro de venda poderá ser proveniente de um contrato ou de uma venda de produto comum.
	 * 	Caso seja proveniente de um contrato deverá ser preenchido como true o campo bl_contrato do VendaTO
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que realiza registro na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
	
	/**
	 * Método que realiza atualização na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function atualizarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que realiza atualização na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function atualizarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
	
	/**
	 * Método que realiza exclusão [LÓGICA] na tabela de vendas (VendaORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVenda(VendaTO $vTO) {
	
	}
	
	/**
	 * Método que realiza exclusão na tabela de vendas produtos (VendaProdutoORM) e retorna um mensageiro
	 *
	 * @TODO implementar
	 * 
	 * @param VendaProdutoTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVendaProduto(VendaProdutoTO $vpTO) {
	
	}
	
	
	/**
	 * Retorna os dados de configuração do boleto para a entidade
	 * @param int $idEntidade
	 * @todo implementar LOGGER DE ERRO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosConfBoleto( $idEntidade ) {
		
		try {
			if( is_nan( $idEntidade ) || $idEntidade == 0 ) {
				throw new InvalidArgumentException( 'Informe o id da entidade para listar os dados de configuração do boleto' );
			}
			
			$financeiroDAO		= new FinanceiroDAO();
			$dadosConfBoletoROW	= $financeiroDAO->retornarDadosConfBoleto( $idEntidade );
			
			if( $dadosConfBoletoROW === null ) {
				throw new ErrorException( 'Não foi possível retornar os dados de configuração do boleto' );
			}
			
			$this->mensageiro->setMensageiro( $dadosConfBoletoROW, Ead1_IMensageiro::SUCESSO );
			
		} catch ( Zend_Db_Exception  $dbException ) {
			$this->mensageiro->setMensageiro( 'Um erro ocorreu durante o processamento da aplicação. A nossa equipe de suporte já foi avisada. Erro: '. $dbException->getMessage() , Ead1_IMensageiro::ERRO  );
		} catch ( Exception $exc ) {
			$this->mensageiro->setMensageiro( $exc->getMessage(), Ead1_IMensageiro::ERRO );
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Realiza o pagamento dos lançamentos de uma venda que sejam para cartão de crédito
	 * @todo implementar logger de erro de confirmação de pgaamento
	 * @return Ead1_Mensageiro
	 */
	public function pagamentoCartaoLancamentoVenda( PagamentoCartaoTO $pagamentoCartaoTO ) {
		
		$financeiroDAO	= new FinanceiroDAO();
		$financeiroDAO->beginTransaction();
		
		try {
			
			if( empty( $pagamentoCartaoTO->id_cartaobandeira ) || is_nan( $pagamentoCartaoTO->id_cartaobandeira ) || $pagamentoCartaoTO->id_cartaobandeira == 0 ) {
				throw new ErrorException( 'Informe a bandeira que será utilizada no pagamento em cartão' );
			}
			
			if( empty( $pagamentoCartaoTO->id_venda ) || is_nan( $pagamentoCartaoTO->id_venda ) ) {
				throw new ErrorException( 'Informe a venda que será feito o pagamento em cartão' );
			}
			
			$vwLancamentoTO						= new VwLancamentoTO();
			$vwLancamentoTO->id_venda			= $pagamentoCartaoTO->id_venda;
			$vwLancamentoTO->id_meiopagamento	= MeioPagamentoTO::CARTAO;
			
			$vwLancamentoROWSET	= $financeiroDAO->retornarLancamento( $vwLancamentoTO );
			
			if( $vwLancamentoROWSET->count() == 0 ) {
				throw new DomainException( 'Não foi possível encontrar lançamentos de cartão de crédito para a venda informada. Código da venda: '. $pagamentoCartaoTO->id_venda );
			}
			
			$vwLancamentoROW		= $vwLancamentoROWSET->current(); 
			
			$dadosConfCartaoROWSET	= $financeiroDAO->retornarDadosConfCartao( $pagamentoCartaoTO->getSessao()->id_entidade, $pagamentoCartaoTO->id_cartaobandeira );
			
			if( $dadosConfCartaoROWSET->count() < 1 ) {
				throw new DomainException( 'Não foi possível encontrar os dados de configuração da entidade código: '. $pagamentoCartaoTO->getSessao()->id_entidade );
			}
			
			
			$totalParcelas			= $vwLancamentoROWSET->count();
			$valorTotalPagamento	= 0;
									
			$valorLancamento		= $vwLancamentoROW->nu_valor ;
			
			foreach ( $vwLancamentoROWSET as $vwLancamentoROW ) {
				if( $vwLancamentoROW->nu_valor !== $valorLancamento ) {
					throw new DomainException( 'Os valores de lançamento com o meio de pagamento cartão de crédito devem possuir valores iguais.' );
				}
				$valorTotalPagamento	+=	 $vwLancamentoROW->nu_valor;
			}
			
			$pagamentoCartaoTO->valor_total			= $valorTotalPagamento;
			$pagamentoCartaoTO->quantidade_parcela	= $totalParcelas;
			$pagamentoCartaoTO->num_afiliacao		= $dadosConfCartaoROWSET->current()->st_contratooperadora;
			$pagamentoCartaoTO->num_gateway			= $dadosConfCartaoROWSET->current()->st_gateway;
			$pagamentoCartaoTO->id_cartaooperadora	= $dadosConfCartaoROWSET->current()->id_cartaooperadora;
			
			$serviceMeioPagamento	= new Ead1_Service_PagamentoCartao();
			$serviceMeioPagamento->realizarPagamento( $pagamentoCartaoTO );
			
			$lancamentoTO	= '';
			$produtoBO		= new ProdutoBO();
			
			foreach ( $vwLancamentoROWSET as $lancamentoROW ) {
				$lancamentoTO	= new LancamentoTO();
				$lancamentoTO->id_lancamento	= $lancamentoROW->id_lancamento;
				$lancamentoTO->bl_quitado		= true;
				$lancamentoTO->dt_quitado		= date( 'Y-m-d H:i:s' );
				$lancamentoTO->dt_prevquitado	= date( 'Y-m-d H:i:s' );
				$lancamentoTO->st_coddocumento	= $pagamentoCartaoTO->tid;
				
				$mensageiro	 = $produtoBO->editarLancamento( $lancamentoTO );
					
				if( $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO ) {
					throw new Ead1_Exception_ConfirmacaoPagamento( 'Não foi possível confirmar o pagamento do lançamento: '. $lancamentoTO->id_lancamento );
				}
			}
			
			$financeiroDAO->commit();
			$this->mensageiro->setMensageiro( $pagamentoCartaoTO, Ead1_IMensageiro::SUCESSO );
		} catch ( Ead1_Exception_ConfirmacaoPagamento $exc ) {
			$financeiroDAO->commit();
			$this->mensageiro->setMensageiro( 'Pagamento realizado com sucesso. Houve um problema durante a confirmação do pagamento em nosso sistema. ', Ead1_IMensageiro::AVISO );
			$this->mensageiro->addMensagem( $pagamentoCartaoTO );
		} catch ( Exception $exc ) {
			$financeiroDAO->rollBack();
			$this->mensageiro->setMensageiro( $exc->getMessage(), Ead1_IMensageiro::ERRO );
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Retorna as bandeiras de cartão da entidade da sessão
	 * @todo gerar log de erro
	 * @param CartaoBandeiraTO $cartaoBandeiraTO
	 * @param int $id_sistema
	 * @return Ead1_Mensageiro
	 */
	public function retornarCartaoBandeira( CartaoBandeiraTO $cartaoBandeiraTO, $id_sistema = null ){
		
		try {
			
			$idEntidade				= $cartaoBandeiraTO->getSessao()->id_entidade;

			if( empty( $idEntidade ) ) {
				throw new DomainException( 'Não foi possível identificar a entidade através da sessão para busca das bandeiras' );
			}
			$financeiroDAO			= new FinanceiroDAO();
			$cartaoBandeiraROWSET	= $financeiroDAO->retornarCartaoBandeira( $cartaoBandeiraTO, $idEntidade, $id_sistema );


			if( $cartaoBandeiraROWSET->count() < 1 ){
				throw new DomainException( 'Não foi encontrado nenhuma bandeira configurada para entidade '. $idEntidade );
			}
			
			$this->mensageiro->setMensageiro( Ead1_TO_Dinamico::encapsularTo( $cartaoBandeiraROWSET, new CartaoBandeiraTO() ), Ead1_IMensageiro::SUCESSO  );
		}catch ( DomainException $domainExc ) {
			$this->mensageiro->setMensageiro( $domainExc->getMessage(), Ead1_IMensageiro::AVISO );
		}catch ( Exception $e ) {
			$this->mensageiro->setMensageiro( 'Não foi possível retornar as bandeiras de cartão. Erro: '. $e->getMessage(), Ead1_IMensageiro::ERRO );
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Método que analisa o status financeiro da matrícula (Em Atraso, Em Dia ou Pago)
	 * @param MatriculaTO $matriculaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarStatusFinanceiro(MatriculaTO $matriculaTO, $detalhado = false){
		try{
			$statusLancamento = $this->verificarStatusLancamento($matriculaTO, $detalhado);
			$this->mensageiro->setMensagem($statusLancamento);
			$this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
		}catch(Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	
	/**
	 * Método que verifica o status financeiro da matrícula de forma simples
	 * 	Em Atraso, Em Dia, Pago. Com ou sem detalhamento, por padrão sem detalhamento!
	 * 
	 * @param MatriculaTO $matriculaTO
	 * @param Boolean $detalhado
	 * @return Ead1_Mensageiro
	 */
	private function verificarStatusLancamento(MatriculaTO $matriculaTO, $detalhado = false){
		$matriculaLancamentoPesquisaTO = new VwMatriculaLancamentoTO();
		$matriculaLancamentoPesquisaTO->setId_matricula($matriculaTO->getId_matricula());

		$vwMatriculaLancamentoORM = new VwMatriculaLancamentoORM();
		$vwMatriculaLancamentosTO = $vwMatriculaLancamentoORM->consulta($matriculaLancamentoPesquisaTO);

		$detalhamento = array();	
		if(!$vwMatriculaLancamentosTO){
			$detalhamento = true;	
			$status = "Confirmado";
			return ($detalhado ? $detalhamento : $status);
		}else{
			
			$status = "Confirmado";		
			$detalhamento["label"] = "Lançamentos";
			$detalhamento["dados"] = array();
			foreach($vwMatriculaLancamentosTO as $vwMatriculaLancamento){
				if($vwMatriculaLancamento->getBl_quitado() == false){
					$status = "Pendente";
					if($this->comparaData($vwMatriculaLancamento->getDt_vencimento(), new Zend_Date())){
						$status = "Pendente";
						if(!$detalhado){
							break;
						}
						$detalhamento["dados"]["Em Atraso"]["label"] = "Em Atraso";
						$detalhamento["dados"]["Em Atraso"]["dados"][] = $vwMatriculaLancamento;
					}else{
						$detalhamento["dados"]["Em Dia"]["label"] = "Em Dia";					
						$detalhamento["dados"]["Em Dia"]["dados"][] = $vwMatriculaLancamento;					
					}
				}else{
					$detalhamento["dados"]["Pago"]["label"] = "Pagos";
					$detalhamento["dados"]["Pago"]["dados"][] = $vwMatriculaLancamento;
				}
			}
			if(!empty($detalhamento["dados"])){
				$detalhamento["dados"] =  array_values($detalhamento["dados"]);
			}
		}
		
		return ($detalhado ? $detalhamento : $status);
	}


    /**
     * Retorna as vendas do aluno
     * @param VwClienteVendaTO $vwCVTO
     * @return Ead1_Mensageiro
     */
    public function retornaVendasCliente(VwClienteVendaTO $vwCVTO){

        try{
            $vendas = new VendaDAO();
            $arrVendas = $vendas->retornarClienteVenda($vwCVTO)->toArray();
            if(!empty($arrVendas)){
                $x=0;
                foreach ($arrVendas as $venda){
                    $produto = new VwProdutoVendaTO();
                    $produto->setId_venda($venda['id_venda']);
                    $produto->setId_entidade($vwCVTO->getId_entidade());

                    $vendaLancamento = new VwVendaLancamentoTO();
                    $vendaLancamento->setId_venda($venda['id_venda']);

                    $vwVendaLancamentoQuitado = new VwVendaLancamentoQuitadosTO();
                    $vwVendaLancamentoQuitado->setId_venda($produto->getId_venda());
                    $vwVendaLancamentoQuitado->fetch(false, true, true);
                    $arrVendas[$x]['nu_quitado'] = $vwVendaLancamentoQuitado->getNu_quitado();

                    $arProdutos = null;
                    $produtos   = $vendas->retornarVwProdutoVenda($produto)->toArray();
                    if($produtos){
//                        \Zend_Debug::dump($produtos, __CLASS__ . '(' . __LINE__ . ')');
//                        exit;
                        $ppp = new \G2\Negocio\ProdutoProjetoPedagogico();
                        $arrP = Ead1_TO_Dinamico::encapsularTo($produtos, new VwProdutoVendaTO());
                        foreach($arrP as $prod){
                            $vp = $ppp->find('G2\Entity\VendaProduto', $prod->getid_vendaproduto());
                            $dt_inicio = null;
                            if($vp->getId_turma()){
                                $art = $vp->getId_turma()->toBackBoneArray();
                                $dt_inicio = $art['dt_inicio'];
                            }

                            $epro = $ppp->retornaProdutoProjetoPedagogico(array('id_produto'=>$prod->getid_produto()));
                            if($epro){
                                $arProdutos[] = array_merge($prod->toArray(), array(
                                    'st_projetopedagogico'=>$epro[0]->getId_projetopedagogico()->getSt_projetopedagogico(),
                                    'id_projetopedagogico'=>$epro[0]->getId_projetopedagogico()->getId_projetopedagogico(),
                                    'st_turma'=>($vp->getid_turma() ? $vp->getid_turma()->getst_turma() : null ),
                                    'dt_inicio'=>$dt_inicio
                                ));
                            }

                        }
                    }
                    $arrVendas[$x]['produtos']      = $arProdutos;

                    $arrlancamentos = null;
                    $lancamentos = $vendas->retornarVendaLancamento($vendaLancamento)->toArray();
                    if($lancamentos){
                        $arrL = Ead1_TO_Dinamico::encapsularTo($lancamentos, new VwVendaLancamentoTO());
                        foreach($arrL as $lanc){
                            $arrlancamentos[] = $lanc->toArray();
                        }
                    }
                    $arrVendas[$x]['lancamentos']   = $arrlancamentos;
                    $x++;
                }
            }

            $this->mensageiro->setMensageiro($arrVendas,Ead1_IMensageiro::SUCESSO);
        }catch (Zend_Validate_Exception $e){
            $this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
        }catch (Zend_Exception $e){
            $this->mensageiro->setMensageiro("Erro ao Retornar Vendas Alunos.",Ead1_IMensageiro::ERRO,$e->getMessage());
        }

        return $this->mensageiro;


    }


}






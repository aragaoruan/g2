<?php
/**
 * Classe com regras de negócio para interações de Telemarketing
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 12/09/2011
 * 
 * @package models
 * @subpackage bo
 */
class TelemarketingBO extends Ead1_BO{

	private $dao;
	
	public function __construct(){
		$this->dao = new TelemarketingDAO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método que conclui o atendimento 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoConcluirAtendimento(VendaTO $vTO){
		try{
			$vTO->setId_entidade(($vTO->getId_entidade() ? $vTO->getId_entidade() : $vTO->getSessao()->id_entidade));
			if(!$vTO->getId_venda()){
				THROW new Zend_Exception('A VendaTO veio sem o id_venda setado!');
			}
			$vendaORM = new VendaORM();
			$vTO = $vendaORM->consulta(new VendaTO(array('id_venda' => $vTO->getId_venda())),true,true);
			if(!$vTO){
				THROW new Zend_Validate_Exception('Venda não encontrada!');
			}
			//Verificando dados de pessoa
			$usuarioORM = new UsuarioORM();
			$usuarioTO = $usuarioORM->consulta(new UsuarioTO((array('id_usuario' => $vTO->getId_usuario()))),true,true);
			if(!$usuarioTO){
				THROW new Zend_Validate_Exception('Pessoa da Venda Não Encontrado!');
			}
			if($usuarioTO->getId_registropessoa() == RegistroPessoaTO::BRASILEIRO_MAIOR_DE_IDADE && !$this->validaCPF($usuarioTO->getSt_cpf()) || !$usuarioTO->getId_registropessoa()){
				THROW new Zend_Validate_Exception('Dados de Pessoa Inválidos!');
			}
			//Verificando dados de Venda e Lançamentos
			$vendaDAO = new VendaDAO();
			$nu_valorliquidovenda = $vTO->getNu_valorliquido();
			$vendaLancamentos = $vendaDAO->retornarVendaLancamento(new VwVendaLancamentoTO(
																					 array(	'id_venda' => $vTO->getId_venda(),
																					 		'id_entidade' => $vTO->getId_entidade())
																					 ))->toArray();
			if(empty($vendaLancamentos)){
				THROW new Zend_Validate_Exception('A Venda não possui Lançamentos!');
			}
			$nu_valorlancamentos = 0;
			foreach ($vendaLancamentos as $lancamento){
				$nu_valorlancamentos += $lancamento['nu_valor'];
			}
			if($nu_valorlancamentos < $nu_valorliquidovenda){
				THROW new Zend_Validate_Exception('O Valor da Soma dos Lançamentos é Menor que o Valor Liquido da Venda!');
			}
			$entidadeFinanceiroORM = new EntidadeFinanceiroORM();
			$entidadeFinanceiroTO = $entidadeFinanceiroORM->consulta(new EntidadeFinanceiroTO(array('id_entidade' => $vTO->getId_entidade())),false,true);
			if(!$entidadeFinanceiroTO){
				THROW new Zend_Validate_Exception('Configurações Financeiras da Instituição não Encontradas!');
			}
			$vendaTO = new VendaTO(array( 	'id_venda' => $vTO->getId_venda()
											,'id_entidade' => $vTO->getId_entidade()
											,'id_evolucao' => ($entidadeFinanceiroTO->getBl_protocolo() ? 
																											VendaTO::EVOLUCAO_NEGOCIADA 
																										: 
																											VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO)
										));
			$vendaDAO->editarVenda($vTO);
			$this->mensageiro->setMensageiro('Atendimento Concluido com Sucesso!');
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Concluir Atendimento!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Procedimento que marca a evolução da venda como em andamento quando o atendimento for do proprio atendente
	 * @param PesquisarVendaAtendimentoTO $pesquisarVendaAtendimentoTO
	 * @param int $id_usuariosessao
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoAtenderVenda(PesquisarVendaAtendimentoTO $pesquisarVendaAtendimentoTO, $id_usuariosessao = null){
		try{
			if(!$pesquisarVendaAtendimentoTO->getId_venda()){
				THROW new Zend_Exception('O id_venda não veio setado!');
			}
			if(!$pesquisarVendaAtendimentoTO->getId_usuarioatendente()){
				THROW new Zend_Exception('O id_usuarioatendente não veio setado!');
			}
			if(!$id_usuariosessao){
				$id_usuariosessao = $pesquisarVendaAtendimentoTO->getSessao()->id_usuario;
			}
			if($id_usuariosessao == $pesquisarVendaAtendimentoTO->getId_usuarioatendente()){
				$vendaORM = new VendaORM();
				$vendaTO = $vendaORM->consulta(new VendaTO(array(
																	'id_venda' => $pesquisarVendaAtendimentoTO->getId_venda())
																)
															,true,true);
				if(!$vendaTO){
					THROW new Zend_Validate_Exception('Venda Não Encontrada!');
				}
				if($vendaTO->getId_evolucao() != VendaTO::EVOLUCAO_EM_ANDAMENTO){
					$vendaDAO = new VendaDAO();
					$vendaDAO->editarVenda(new VendaTO(array(
																'id_venda' => $pesquisarVendaAtendimentoTO->getId_venda()
																,'id_evolucao' => VendaTO::EVOLUCAO_EM_ANDAMENTO
															)));
				}
					
			}
			$this->mensageiro->setMensageiro("Atendimento Efetuado com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro no Processo de Atendimento!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que cadastra um atendente para a venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAtendenteVenda(AtendenteVendaTO $to){
		try{
			$this->dao->beginTransaction();
			if(!$to->getId_venda()){
				THROW new Zend_Exception('O id_venda não veio setado!');
			}
			$this->dao->editarAtendenteVenda(	new AtendenteVendaTO(
																		array(
																				'id_venda' => $to->getId_venda()
																				,'bl_ativo' => 0))
												,"id_venda = ".$to->getId_venda());
												
			$to->setId_atendentevenda(0);
			$id_atendentevenda = $this->dao->cadastrarAtendenteVenda($to);
			$to->setId_atendentevenda($id_atendentevenda);
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro ao Vincular um Atendente a Venda!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que edita um vinculo de atendente e venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarAtendenteVenda(AtendenteVendaTO $to){
		try{
			$this->dao->editarAtendenteVenda($to);
			$this->mensageiro->setMensageiro('Venda do Atendente Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Editar Venda do Atendente!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna o vinculo de atendente e venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarAtendenteVenda(AtendenteVendaTO $to){
		try{
			$dados = $this->dao->retornarAtendenteVenda($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AtendenteVendaTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Editar Venda do Atendente!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
}
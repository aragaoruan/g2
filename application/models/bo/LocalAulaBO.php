<?php
/**
 * Classe com regras de negócio para interações de Locais de Aula
 * @author Paulo Silva - paulo.silva@unyleya.com.br
 * @since 15/04/2013
 * 
 * @package models
 * @subpackage bo
 */
class LocalAulaBO extends Ead1_BO {
	public $mensageiro;
	private $dao;
	public function __construct() {
		$this->mensageiro = new Ead1_Mensageiro ();
		$this->dao = new LocalAulaDAO ();
	}
	
	/**
	 * Metodo que Cadastra Local de Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLocalAula(LocalAulaTO $laTO) {
		$id_localAula = $this->dao->cadastrarLocalAula ( $laTO );
		
		if (! $id_localAula) {
			return $this->mensageiro->setMensageiro ( 'Erro ao Cadastrar Local da Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao );
		}
		$laTO->setid_localaula ( $id_localAula );
		return $this->mensageiro->setMensageiro ( $laTO, Ead1_IMensageiro::SUCESSO );
	}
	
	/**
	 * Metodo que Edita o Local da Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * 
	 * @return Ead1_Mensageiro
	 */
	public function editarLocalAula(LocalAulaTO $laTO) {
		if (! $this->dao->editarLocalAula ( $laTO )) {
			return $this->mensageiro->setMensageiro ( 'Erro ao Editar o Local da Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao );
		}
		return $this->mensageiro->setMensageiro ( 'Local da Aula Editado com Sucesso!', Ead1_IMensageiro::SUCESSO );
	}
	
	/**
	 * Metodo que Exclui o Local da Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * @return Ead1_Mensageiro
	 */
	public function deletarLocalAula(LocalAulaTO $laTO) {
		if (! $this->dao->deletarLocalAula ( $laTO )) {
			return $this->mensageiro->setMensageiro ( 'Erro ao Excluir Local da Aula!', Ead1_IMensageiro::ERRO, $this->dao->excecao );
		}
		return $this->mensageiro->setMensageiro ( 'Local da Aula Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO );
	}
	
	/**
	 * Metodo que Retorna o Local da Aula
	 * 
	 * @param LocalAulaTO $laTO        	
	 * @return Ead1_Mensageiro
	 */
	public function retornarLocalAula(LocalAulaTO $laTO) {
		$localAula = $this->dao->retornarLocalAula ( $laTO );
		if (is_object ( $localAula )) {
			$localAula = $localAula->toArray ();
		} else {
			unset ( $localAula );
		}
		if (! $localAula) {
			return $this->mensageiro->setMensageiro ( 'Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao );
		}
		return $this->mensageiro->setMensageiro ( Ead1_TO_Dinamico::encapsularTo ( $localAula, new LocalAulaTO () ), Ead1_IMensageiro::SUCESSO );
	}
}
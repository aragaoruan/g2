<?php

/**
 * @author Denise - denisexavier@ead1.com.br
 * Classe para as regras de negócio para o financeiro/vendas
 *
 */
class PortalFinanceiroBO extends Ead1_BO
{

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Retorna as vendas do aluno
     * @param VwClienteVendaTO $vwCVTO
     * @return Ead1_Mensageiro
     */
    public function retornaVendasCliente(VwClienteVendaTO $vwCVTO, $evolucaoMaiorOuIgual = false)
    {

        try {
            $vendas = new VendaDAO();
            $arrVendas = array();
            $resultVendas = $vendas->retornarClienteVenda($vwCVTO, $evolucaoMaiorOuIgual);
            if ($resultVendas) {
                foreach ($resultVendas->toArray() as $x => $venda) {
                    $produto = new VwProdutoVendaTO();
                    $produto->setId_venda($venda['id_venda']);

                    $vendaLancamento = new VwVendaLancamentoTO();
                    $vendaLancamento->setId_venda($venda['id_venda']);

                    $vwVendaLancamentoQuitado = new VwVendaLancamentoQuitadosTO();
                    $vwVendaLancamentoQuitado->setId_venda($produto->getId_venda());
                    $vwVendaLancamentoQuitado->fetch(false, true, true);

                    $arrVendas[$x] = $venda;
                    $arrVendas[$x]['nu_quitado'] = $vwVendaLancamentoQuitado->getNu_quitado();
                    $arrVendas[$x]['produtos'] = $vendas->retornarVwProdutoVenda($produto)->toArray();
                    $arrVendas[$x]['lancamentos'] = $vendas->retornarVendaLancamento($vendaLancamento)->toArray();
                }
            }

            $this->mensageiro->setMensageiro($arrVendas, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Vendas Alunos.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;


    }


}


?>
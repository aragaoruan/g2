<?php
/**
 * Classe com regras de negócio para interações de Email
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 29/07/2011
 * 
 * @package models
 * @subpackage bo
 */
class EmailBO extends Ead1_BO{
	
	public $mensageiro;
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new EmailDAO();
	}
	
	/**
	 * Metodo que salva uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEmailConfig(EmailConfigTO $ecTO){
		if($ecTO->getId_emailconfig()){
			return $this->editarEmailConfig($ecTO);
		}
		return $this->cadastrarEmailConfig($ecTO);
	}
	
	public function cadastrarMensagem( MensagemTO $mensagemTO ){
		if( empty( $mensagemTO->id_entidade ) || $mensagemTO->id_entidade == false ) {}
		
		$this->dao->salvarMensagem( $mensagemTO );
	}
	
	/**
	 * Metodo que cadastra uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEmailConfig(EmailConfigTO $ecTO){
		try{
			if(!$ecTO->getId_entidade()){
				$ecTO->setId_entidade($ecTO->getSessao()->id_entidade);
			}
			$ecTO->setId_usuariocadastro($ecTO->getSessao()->id_usuario);
			$id_emailconfig = $this->dao->cadastrarEmailConfig($ecTO);
			$ecTO->setId_emailconfig($id_emailconfig);
			
			$this->mensageiro->setMensageiro($ecTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Cadastrar Configuração do Email',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que edita uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEmailConfig(EmailConfigTO $ecTO){
		try{
			if(!$ecTO->getId_entidade()){
				$ecTO->setId_entidade($ecTO->getSessao()->id_entidade);
			}
			$this->dao->editarEmailConfig($ecTO);
			$entidadeEmailTO = new EntidadeEmailTO();
			$entidadeEmailTO->setId_emailconfig($ecTO->getId_emailconfig());
			$entidadeEmailTO->setId_entidade($ecTO->getSessao()->id_entidade);
			$entidadeEmailTO->fetch(false,true, true);
			
			$entidadeEmailTO->setSt_email($ecTO->getSt_conta());
			
			$this->dao->editarEntidadeEmail($entidadeEmailTO);
			$this->mensageiro->setMensageiro('Configuracao do Email Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Editar Configuração do Email',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEmailConfig(EmailConfigTO $ecTO){
		try{
			if(!$ecTO->getId_entidade()){
				$ecTO->setId_entidade($ecTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarEmailConfig($ecTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EmailConfigTO()),Ead1_IMensageiro::SUCESSO);
			
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Configuração do Email',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna o tipo de conexao do email
	 * @param TipoConexaoEmailTO $tceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoConexaoEmail(TipoConexaoEmailTO $tceTO){
		try{
			$dados = $this->dao->retornarTipoConexaoEmail($tceTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoConexaoEmailTO()),Ead1_IMensageiro::SUCESSO);
			
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Conexão do Email',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Retorna as mensagens padrões de cada entidade
	 * Retorna as configurações de e-mail vinculadas a mensagem padrão
	 * O tipo de retorno é um Ead1_Mensageiro contendo um objeto Zend_Db_Table_Row
	 * Os seguintes atributos estão disponíves: 
	 * 'id_mensagempadrao', 'id_tipoenvio', 'st_mensagempadrao', 'bl_autenticacaosegura', 'id_tipoconexaoemailentrada'
	 * , 'id_tipoconexaoemailsaida', 'nu_portaentrada', 'nu_portasaida', 'st_tipoconexaoemail' , 'tipoconexaoemailentrada', 'tipoconexaoemailsaida'
	 * 
	 * @param int_type $idEntidade
	 * @param int $idMensagemPadrao
	 * @return Ead1_Mensageiro
	 */
	public function retornarConfEmailMensagemPadrao( $idEntidade, $idMensagemPadrao ) {
		
		try {
			$mensagemEmailROW	= $this->dao->retornarConfEmailMensagemPadrao( $idEntidade, $idMensagemPadrao );
			
			if( is_null( $mensagemEmailROW ) ) {
				throw new DomainException( 'Nenhum registro de mensagem padrão vinculado a e-mail encontrada' );
			}
			
			$this->mensageiro->setMensageiro( $mensagemEmailROW, Ead1_IMensageiro::SUCESSO );
		} catch( DomainException $domainExc ) {
			$this->mensageiro->setMensageiro( $domainExc->getMessage(), Ead1_IMensageiro::AVISO );
		} catch( Exception $exception ) {
			$this->mensageiro->setMensageiro( 'Erro ao retornar a configuração de e-mail para a mensagem padrão', Ead1_IMensageiro::ERRO, $exception->getMessage() );
		}
		return $this->mensageiro;
	}
	
}
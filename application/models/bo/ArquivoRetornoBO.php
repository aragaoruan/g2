<?php

/**
 * Classe com regras de negócio para o ArquivoRetorno
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 16/10/2012
 * @package models
 * @subpackage bo
 */
class ArquivoRetornoBO extends Ead1_BO {



	/**
	 * @var ArquivoRetornoDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new ArquivoRetornoDAO();
	}
    
    
    
	/**
	 * Método de listagem de ArquivoRetorno
	 * @param ArquivoRetornoTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarArquivoRetorno(ArquivoRetornoTO $to){
		
		try {
			
			$to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
			
			return new Ead1_Mensageiro($this->dao->listarArquivoRetorno($to), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar Arquivo Retorno: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
    
    
	/**
	 * Metodo que decide se cadastra ou edita o ArquivoRetorno
	 * @param ArquivoRetornoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarArquivoRetorno(ArquivoRetornoTO $to){
	
		$this->dao->beginTransaction();
		try {
			
			$to->setId_entidade($to->getSessao()->id_entidade);
			$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			
			if($to->getId_arquivoretorno()){
				$mensageiro = $this->editarArquivoRetorno($to);
			} else {
				$mensageiro =  $this->cadastrarArquivoRetorno($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o Arquivo Retorno: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}


	
	/**
	 * Método que cadastra umo ArquivoRetorno
	 * @param ArquivoRetornoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see ArquivoRetornoBO::salvarArquivoRetorno();
	 */
	public function cadastrarArquivoRetorno(ArquivoRetornoTO $to){
		$this->dao->beginTransaction();
		try {
			
			$to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
			$to->setId_usuariocadastro(($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario));
			$to->setId_arquivoretorno($this->dao->cadastrarArquivoRetorno($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o ArquivoRetorno, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
    
	/**
	 * Método que edita umo ArquivoRetorno
	 * @param ArquivoRetornoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see ArquivoRetornoBO::salvarArquivoRetorno();
	 */
	public function editarArquivoRetorno(ArquivoRetornoTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarArquivoRetorno($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o ArquivoRetorno, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
   
    
	/**
	 * Método de listagem de ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $to){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarArquivoRetornoLancamento($to), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar ArquivoRetornoLancamento: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
    
    
	/**
	 * Metodo que decide se cadastra ou edita o ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $to){
	
		$this->dao->beginTransaction();
		try {
			
			
			
			if($to->getId_ArquivoRetornoLancamento()){
				$mensageiro = $this->editarArquivoRetornoLancamento($to);
			} else {
				$mensageiro =  $this->cadastrarArquivoRetornoLancamento($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o ArquivoRetornoLancamento: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}


	
	/**
	 * Método que cadastra umo ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see ArquivoRetornoLancamentoBO::salvarArquivoRetornoLancamento();
	 */
	public function cadastrarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $to){
		$this->dao->beginTransaction();
		try {

			$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			
			$to->setId_ArquivoRetornoLancamento($this->dao->cadastrarArquivoRetornoLancamento($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o ArquivoRetornoLancamento, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
    
	/**
	 * Método que edita umo ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see ArquivoRetornoLancamentoBO::salvarArquivoRetornoLancamento();
	 */
	public function editarArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarArquivoRetornoLancamento($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o ArquivoRetornoLancamento, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
	
	
	
	
	/**
	 * Quita os boletos encontrados no arquivo de retorno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param array $arrTo
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function quitarLancamentosArquivoRetorno(array $arrTo){
		$arquivoRetornoNegocio = new \G2\Negocio\ArquivoRetorno();
		return $arquivoRetornoNegocio->quitarLancamentosArquivoRetorno($arrTo);
	}
	
	
	/**
	 * Método que processa o Arquito Retorno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param ArquitoTO $arquitoTO
	 */
	public function processarArquivoRetorno(ArquivoTO $arquivoTO){

		$arquivoRetornoTO 	= false;
		$registroLinha 		= false;
		$this->dao->beginTransaction();
		try {
			$cnab240 =  Ead1_Financeiro_Retorno_Factory::getRetorno($arquivoTO->getSt_caminhodiretorio());

			$retorno = new  Ead1_Financeiro_Retorno_Banco($cnab240);
			$arqretorno = $retorno->retornaConteudoArquivoProcessado();
			if($arqretorno){
				$x = 0;
				foreach ($arqretorno as $to){
					
					if($x==10000000000000){ /* este pedaço é para facilitar os testes locais, como a conexão demora, da timeout */
						continue;
					}
					
					if($to instanceof Ead1_Financeiro_Retorno_TO_HeaderTO){
						$mensageiro = $this->_processarHeaderArquivoRetorno($to);
						if($mensageiro->getFirstMensagem() instanceof ArquivoRetornoTO){
							$arquivoRetornoTO = $mensageiro->getFirstMensagem();
						}
					} elseif($to instanceof Ead1_Financeiro_Retorno_TO_DetalheTO) {
						
						if(!$arquivoRetornoTO){
							throw new Zend_Exception('Arquivo inválido, não foi informada a linha Header');
						}

						$arqretlanTO = new ArquivoRetornoLancamentoTO();
                        $arqretlanTO->setDt_ocorrencia($to->getDt_ocorrencia());
						$arqretlanTO->setId_arquivoretorno($arquivoRetornoTO->getid_arquivoretorno());
						$arqretlanTO->setNu_valor($to->getNu_valor());
						$arqretlanTO->setSt_nossonumero($to->getSt_nossonumero());
						$arqretlanTO->setSt_carteira($to->getSt_carteira());
						$arqretlanTO->setSt_documento($to->getSt_documento());
                        $arqretlanTO->setNu_desconto($to->getNu_descontos());
                        $arqretlanTO->setNu_juros($to->getNu_juros());
                        $arqretlanTO->setNu_tarifa($to->getNu_tarifa());
                        $arqretlanTO->setNu_valornominal($to->getNu_valornominal());
                        $arqretlanTO->setDt_quitado($to->getDt_quitado());

                        $mensageiro = $this->cadastrarArquivoRetornoLancamento($arqretlanTO);

						if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
							throw new Zend_Exception($mensageiro->getFirstMensagem());
						}
						
						$registroLinha = true;
					}
					
					$x++;
					
				}
			} else {
				throw new Zend_Exception('Arquivo vazio!');
			}
			
			if(!$registroLinha){
				throw new Zend_Exception('Arquivo sem lançamentos a salvar!');
			}
			
			$this->dao->commit();
            $mensageiro = new Ead1_Mensageiro('Arquivo processado com sucesso!', Ead1_IMensageiro::SUCESSO);
            $mensageiro->addMensagem($arquivoRetornoTO);
			return $mensageiro;

		} catch (Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		

		
	}
	
	
	
	/**
	 * Método que salva o ArquivoRetorno baseado em uma linha do arquivoRetorno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param Ead1_Financeiro_Retorno_TO_HeaderTO $to
	 * @throws Zend_Exception
	 * @throws Exception
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>|Ead1_Mensageiro
	 */
	protected function _processarHeaderArquivoRetorno(Ead1_Financeiro_Retorno_TO_HeaderTO $to){
		
		
		$this->dao->beginTransaction();
		try {
			
			$arquivo = explode(DIRECTORY_SEPARATOR, $to->getSt_nomearquivo());
			
			$arqTO = new ArquivoRetornoTO();
			$arqTO->setBl_ativo(true);
			$arqTO->setSt_arquivoretorno($to->getSt_nomearquivo());
			$arqTO->setSt_banco($to->getSt_banco());
			
			$mensageiro = $this->cadastrarArquivoRetorno($arqTO);
			if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($mensageiro->getFirstMensagem());
			}
			
			$this->dao->commit();
			return $mensageiro;
		} catch (Exception $e) {
			$this->dao->rollBack();
			throw $e;
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
	
	
	/**
	 * Retorna os dados da Vw_retornolancamento
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VwRetornoLancamentoTO $to
	 * @param unknown_type $where
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwRetornoLancamento(VwRetornoLancamentoTO $to, $where = null){
		$arquivoRetornoNegocio = new \G2\Negocio\ArquivoRetorno();
		return $arquivoRetornoNegocio->retornarVwRetornoLancamento($to, $where);
	}

	public function retornoLancamentos(array $params){

		try {
			/** @var $doctrineContainer Bisna\Doctrine\Container */
			$doctrineContainer = Zend_Registry::get('doctrine');
			/** @var $em Doctrine\ORM\EntityManager */
			$em = $doctrineContainer->getEntityManager();
			$sel = '';
			$sel1 = '';
			if(isset($params['id_lancamento']) && !empty($params['id_lancamento'])){
				$sel .= ' AND lc.id_lancamento '.($params['id_lancamento'] == 'is null' || $params['id_lancamento'] == 'is not null' ? $params['id_lancamento'] : ' = '.(int) $params['id_lancamento'] );
			}

			if(isset($params['st_carteira']) && !empty($params['st_carteira'])){
				$sel .= " AND st_carteira = '{$params['st_carteira']}' ";
			}

			if(isset($params['bl_quitado']) &&  !empty($params['bl_quitado'])){
				$sel .= " AND arl.bl_quitado = ".$params['bl_quitado'];
			}

			if(isset($params['bl_ativo']) && (!empty($params['bl_ativo']) || $params['bl_ativo'] === '0')){
				$sel .= " AND lc.bl_ativo = ".$params['bl_ativo'];
			}

			if(isset($params['nu_maior']) && !empty($params['nu_maior']) ){
				$sel1 .= " AND  ( ( lc.nu_valor - (lc.nu_valor*isnull(cc.nu_valordesconto,0))/100)  - 2.70 - ISNULL(lc.nu_desconto,0) ) > arl.nu_valor";
			}elseif(isset($params['nu_maior']) && (!empty($params['nu_maior']) || $params['nu_maior'] === '0')){
				$sel1 .= " AND  ( ( lc.nu_valor - (lc.nu_valor*isnull(cc.nu_valordesconto,0))/100)  - 2.70 - ISNULL(lc.nu_desconto,0) ) < arl.nu_valor";
			}

			$sql = "SELECT  ar.id_arquivoretorno ,
							st_arquivoretorno ,
							ar.id_entidade ,
							ar.bl_ativo AS bl_ativoretorno ,
							ar.st_banco ,
							arl.id_arquivoretornolancamento ,
							arl.st_nossonumero ,
							arl.nu_valor AS nu_valorretorno ,
							arl.nu_desconto AS nu_descontoretorno ,
							arl.nu_juros AS nu_jurosretorno ,
							arl.dt_ocorrencia AS dt_ocorrenciaretorno ,
							arl.bl_quitado AS bl_quitadoretorno ,
							( lc.nu_valor - (lc.nu_valor*isnull(lc.nu_valordesconto,0))/100)  - 2.70 - ISNULL(lc.nu_desconto,0) AS nu_valor ,
							lc.id_meiopagamento ,
							lc.bl_quitado ,
							lc.dt_vencimento ,
							lc.dt_quitado ,
							lc.dt_emissao ,
							lc.dt_prevquitado ,
							id_entidadelancamento ,
							lc.bl_ativo ,
							lc.id_lancamento ,
							lc.st_coddocumento,
							arl.st_carteira,
							arl.dt_quitado AS dt_quitadoretorno,
							arl.nu_valornominal,
							arl.nu_tarifa,
							NULL AS nu_valortotal,
							CASE WHEN lc.id_venda IS NOT NULL THEN CAST(lc.id_venda AS VARCHAR(8000))+'G2U' END AS id_venda
					FROM    dbo.tb_arquivoretorno AS ar
							JOIN dbo.tb_arquivoretornolancamento AS arl ON arl.id_arquivoretorno = ar.id_arquivoretorno
							OUTER APPLY(
								SELECT lc.* , lv.id_venda, cc.nu_valordesconto
									FROM  tb_lancamento AS lc
										JOIN dbo.tb_lancamentovenda lv ON lc.id_lancamento = lv.id_lancamento
										JOIN dbo.tb_venda v ON lv.id_venda = v.id_venda
										LEFT JOIN dbo.tb_campanhacomercial cc ON v.id_campanhapontualidade = cc.id_campanhacomercial
														AND lc.dt_vencimento >= arl.dt_ocorrencia
										WHERE lc.id_meiopagamento = 2
										AND CAST(arl.st_nossonumero AS INT) = lc.id_lancamento
										{$sel1}
						   ) AS lc

						   WHERE ar.id_arquivoretorno = {$params['id']} {$sel}";
			//var_dump($sql);
			$stmt = $em->getConnection()->prepare($sql);

			$stmt->execute();
			$results = $stmt->fetchAll();

			//$to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
			return new Ead1_Mensageiro($results, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar Vw_retornolancamento: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
}

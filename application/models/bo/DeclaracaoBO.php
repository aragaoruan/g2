<?php
/**
 * Classe que encapsula regra de negocio de Declaração
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class DeclaracaoBO extends Ead1_BO{
	
	private $dao;
	
	/**
	 * Método construtor
	 */
	public function __construct(){
		$this->dao = new DeclaracaoDAO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método que salva uma Entrega de Declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		if($to->getId_entregadeclaracao()){
			return $this->editarEntregaDeclaracao($to);
		}
		return $this->cadastrarEntregaDeclaracao($to);
	}
	
	/**
	 * Método que cadastra uma Entrega de Declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		try{
			$to->setDt_solicitacao(new Zend_Date(null,Zend_Date::ISO_8601));
			$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			$id_entregadeclaracao = $this->dao->cadastrarEntregaDeclaracao($to);
			$to->setId_entregadeclaracao($id_entregadeclaracao);
			$this->mensageiro->setMensageiro($to,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Cadastrar a Entrega Declaração!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que verifica e cadastra a entrega de declaração para o aluno e verifica os creditos de gratuidade do produto
	 * @param array $arrEntregaDeclaracaoTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAlunoEntregaDeclaracao($arrEntregaDeclaracaoTO){
		try{
			$this->dao->beginTransaction();
			$arrMensagens = array();
			$arrMensagensErro = array();
			foreach ($arrEntregaDeclaracaoTO as $to){
				if(!$to->getId_textosistema()){
					THROW new Zend_Exception("Declaração não Encontrada!");
				}
				$produtoRO = new ProdutoRO();
				$vwProdutoTextoSistemaTO = new VwProdutoTextoSistemaTO();
				$vwProdutoTextoSistemaTO->setId_entidade($to->getSessao()->id_entidade);
				$vwProdutoTextoSistemaTO->setId_tipoproduto(TipoProdutoTO::DECLARACOES);
				$vwProdutoTextoSistemaTO->setId_textosistema($to->getId_textosistema());
				$msgProduto = $produtoRO->retornarVwProdutoTextoSistema($vwProdutoTextoSistemaTO);
				switch ($msgProduto->getTipo()){
					case Ead1_IMensageiro::AVISO:
						THROW new Zend_Validate_Exception($msgProduto->mensagem[0]);
						break;
					case Ead1_IMensageiro::ERRO:
						THROW new Zend_Exception($msgProduto->getCodigo());
						break;
				}
				$vwProdutoTextoSistemaTO = $msgProduto->getCurrent();
				$vwEntregaDeclaracaoTO = new VwEntregaDeclaracaoTO();
				$vwEntregaDeclaracaoTO->setId_matricula($to->getId_matricula());
				$vwEntregaDeclaracaoTO->setId_textosistema($to->getId_textosistema());
				$msgDeclaracao = $this->retornarVwEntregaDeclaracao($vwEntregaDeclaracaoTO);
				switch ($msgDeclaracao->getTipo()){
					case Ead1_IMensageiro::ERRO:
						THROW new Zend_Exception($msgProduto->getCodigo());
						break;
				}
				if(count($msgDeclaracao->getMensagem()) >= $vwProdutoTextoSistemaTO->getNu_gratuito()){
					$arrMensagensErro[] = "A declaração ".$vwProdutoTextoSistemaTO->getSt_produto()." não pode ser solicitada por já ter excedido o limite de solicitações, procure a sua secretária.";
				}else{
					$this->cadastrarEntregaDeclaracao($to);
					$arrMensagens[] = "Declaração ".$vwProdutoTextoSistemaTO->getSt_produto()." solicitada com Sucesso!";
				}
				
			}
			if(!empty($arrMensagensErro)){
				THROW new Zend_Validate_Exception(implode(" \n", array_merge($arrMensagensErro, $arrMensagens)));
			}
			$mensagem = implode(" \n", $arrMensagens);
			$this->mensageiro->setMensageiro($mensagem,Ead1_IMensageiro::SUCESSO);
			$this->dao->commit();
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
			$this->dao->rollBack();
		}catch(Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Solicitar Declaração",Ead1_IMensageiro::ERRO,$e->getMessage());
			$this->dao->rollBack();
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que edita uma entrega de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		try{
			if(!$to->getId_entregadeclaracao()){
				THROW new Zend_Exception("O id_entregadeclaracao não foi setado!");
			}
			$to->setDt_entrega($to->getDt_entrega() ? $to->getDt_entrega() : new Zend_Date(null,Zend_Date::ISO_8601));
			$this->dao->editarEntregaDeclaracao($to);
			$this->mensageiro->setMensageiro("Entrega de Declaração Editada com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Editar a Entrega da Declaração",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que deleta uma entrega de declaração
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirEntregaDeclaracao(EntregaDeclaracaoTO $to){
		try{
			$this->dao->excluirEntregaDeclaracao($to);
			$this->mensageiro->setMensageiro("Entrega da Declaração Excluida com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Excluir a Entrega da Declaração!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Método que retorna as entregas de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		try{
			$dados = $this->dao->retornarEntregaDeclaracao($to)->toArray();
			if(empty($dados)){
				THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EntregaDeclaracaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar as Entregas da Declaração",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna os dados da view de entregas de declaracao
	 * @param VwEntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntregaDeclaracao(VwEntregaDeclaracaoTO $to){
		try{
			$dados = $this->dao->retornarVwEntregaDeclaracao($to)->toArray();
			if(empty($dados)){
				THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEntregaDeclaracaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar as Entregas da Declaração",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
}
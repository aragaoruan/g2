<?php
/**
 * Classe com regras de negócio para sincronização
 * @author edermariano
 *
 */
class SincronizadorBO extends Ead1_BO{
	
	/**
	 * Método que atualiza os lançamentos pendentes da entidade
	 * @return Ead1_Mensageiro
	 */
	public function lancamentosPendentesFluxus(){
		$wsTotvs = new TotvsLancamentosWebServices();
		return $wsTotvs->sincronizarLancamentosNaoEnviados();
	}
	
	/**
	 * Método que sincroniza áreas
	 * @param $idEntidade
	 * @param $idSistema
	 * @return Ead1_Mensageiro
	 */
	public function areas($idEntidade, $idSistema){
		Zend_Debug::dump($this->atualizaAreasSincronizadas($idEntidade, $idSistema),"AREAS ATUALIZADAS: ");
		Zend_Debug::dump($this->areasNaoSincronizadas($idEntidade, $idSistema),"AREAS SINCRONIZADAS: ");
	}
	
	/**
	 * Método que edita as areas já sincronizadas
	 * @param $idEntidade
	 * @param $idSistema
	 * @return Ead1_Mensageiro
	 */
	private function atualizaAreasSincronizadas($idEntidade, $idSistema){
		$sincronizadorDAO = new SincronizadorDAO();
		$dados = Ead1_TO_Dinamico::encapsularTo($sincronizadorDAO->areasSincronizadas($idEntidade, $idSistema), new AreaConhecimentoTO());
		if(empty($dados)){
			return new Ead1_Mensageiro("Nenhum Registro para Sincronização!", Ead1_IMensageiro::AVISO);
		}
		foreach ($dados as $areaConhecimentoTO){
			$wsBO = new WsAreaBO(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
			$sincronizacao = $wsBO->editarArea($areaConhecimentoTO);
			if($sincronizacao->getTipo() == Ead1_IMensageiro::ERRO){
				$arRetorno['ERROS'][$areaConhecimentoTO->getId_areaconhecimento()] = $areaConhecimentoTO->getSt_areaconhecimento() . " - " . $sincronizacao->getCurrent();	
			}else{
				$arRetorno['SUCESSOS'][$areaConhecimentoTO->getId_areaconhecimento()] = $areaConhecimentoTO->getSt_areaconhecimento();	
			}
		}
		return new Ead1_Mensageiro($arRetorno,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Método que sincroniza as areas não sincronizadas
	 * @param $idEntidade
	 * @param $idSistema
	 * @return Ead1_Mensageiro
	 */
	private function areasNaoSincronizadas($idEntidade, $idSistema){
		$sincronizadorDAO = new SincronizadorDAO();
		$dados = Ead1_TO_Dinamico::encapsularTo($sincronizadorDAO->areasNaoSincronizadas($idEntidade, $idSistema), new AreaConhecimentoTO());
		if(!empty($dados)){
			foreach($dados as $areaConhecimentoTO){
				$wsBO = new WsAreaBO(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
				$sincronizacao = $wsBO->cadastrarArea($areaConhecimentoTO);
				if($sincronizacao->getTipo() == Ead1_IMensageiro::ERRO){
					$arRetorno['ERROS'][$areaConhecimentoTO->getId_areaconhecimento()] = $areaConhecimentoTO->getSt_areaconhecimento() . " - " . $sincronizacao->getCurrent();	
				}else{
					$arRetorno['SUCESSOS'][$areaConhecimentoTO->getId_areaconhecimento()] = $areaConhecimentoTO->getSt_areaconhecimento();	
				}
			}
			return new Ead1_Mensageiro($arRetorno,Ead1_IMensageiro::SUCESSO);
		}
		return new Ead1_Mensageiro("Nenhum Registro para Sincronização!", Ead1_IMensageiro::AVISO);
	}
}
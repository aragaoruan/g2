<?php

/**
 * Classe com regras de negócio para interações de Venda
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/05/2011
 *
 *
 * @package models
 * @subpackage bo
 */
class VendaBO extends Ead1_BO
{

    private $dao;

    /**
     * Variavel que contem um array com as informações de juros do lançamento
     * @var array
     */
    private $_arrInfoLancamentos = array();

    /**
     * @return array
     */
    public function getArrInfoLancamentos()
    {
        return $this->_arrInfoLancamentos;
    }

    /**
     * @param array $_arrInfoLancamentos
     */
    public function setArrInfoLancamentos($_arrInfoLancamentos)
    {
        $this->_arrInfoLancamentos = $_arrInfoLancamentos;
    }

    public function __construct()
    {
        parent::__construct();
        $this->dao = new VendaDAO();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que Confirma o Recebimento de uma Venda
     * @param VendaTO $vendaTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     * @see MatriculaBO::confirmaRecebimento()
     */
    public function confirmaRecebimento(VendaTO $vendaTO)
    {
        $this->dao->beginTransaction();
        try {
            $vendaNegocio = new \G2\Negocio\Venda();
            $vendaORM = new VendaORM();
            $vendas = $vendaORM->consulta($vendaTO);
            if ($vendas) {
                $vendaTO = $vendas[0];
            } else {
                throw new Zend_Exception("Venda não encontrada.", Ead1_IMensageiro::ERRO);
            }

            $vendaTO->fetch(true, false, false);
            $pulafluxus = false;

            if ($vendaTO->getNu_valorliquido() != 0) {

                $vendaLancamentoTO = new VwVendaLancamentoTO();
                $vendaLancamentoTO->setId_venda($vendaTO->getId_venda());

                $vorm = new VwVendaLancamentoORM();
                $lancamentos = $vorm->consulta($vendaLancamentoTO, false, false, array('nu_ordem'));
                // 			$lancamentos = $this->dao->retornarVendaLancamento($vendaLancamentoTO);
                $dataHoje = new Zend_Date(null, Zend_Date::ISO_8601);
                if (!empty($lancamentos)) {
                    /** @var VwVendaLancamentoTO $lancamento */
                    foreach ($lancamentos as $lancamento) {

                        //salva o lançamento como bl_original
                        $lancamentoArr = $lancamento->toArray();
                        $lancamentoArr['bl_original'] = true;


                        /*
                         * FIN-17169 - Adicionando essa regra para quando o lançamento for do tipo cartão
                         * id's: 1, 7 ou 11
                         * e for entrada ele deverá ser marcado como quitado ao confirmar a venda.
                         */
                        if ($lancamento->getBl_entrada()
                            && in_array($lancamento->getId_meiopagamento(), \G2\Constante\MeioPagamento::getMeioPagamentosCartao())
                        ) {
                            $lancamentoArr['bl_quitado'] = true;
                            $lancamentoArr['dt_quitado'] = $lancamento->getDt_quitado() ? $lancamento->getDt_quitado()->get('dd/MM/YYYY') : date('d/m/Y');
                        }

                        if (!$vendaNegocio->salvarLancamento($lancamentoArr)) {
                            throw new Zend_Exception("Erro ao tentar setar lançamentos como bl_original");
                        }

                        if ($lancamento->bl_quitado || ($lancamento->id_meiopagamento == MeioPagamentoTO::CARTAO && $lancamento->dt_prevquitado != null)) {
                            continue;
                        }


                        /*   if(($lancamento->getbl_entrada()==true && $lancamento->getid_meiopagamento()==MeioPagamentoTO::RECORRENTE && $lancamento->getid_campanhacomercial())==false){
                          if ($lancamento->bl_entrada && !$lancamento->bl_quitado && $lancamento->id_meiopagamento != MeioPagamentoTO::CARTAO) {
                          throw new Zend_Exception("Não é possível confirmar a venda pois existem lançamentos de entrada não quitados!", Ead1_IMensageiro::AVISO);
                          }
                          if ($lancamento->id_meiopagamento == MeioPagamentoTO::CARTAO && $lancamento->dt_prevquitado == null) {
                          throw new Zend_Exception("Não é possível confirmar a venda pois existem lançamentos de cartão não quitados!" . $this->dao->getQtdTransacao(), Ead1_IMensageiro::AVISO);
                          }
                          } else {
                          $pulafluxus = true;
                          } */


                        // 					$dataVencimento = new Zend_Date($lancamento->getdt_vencimento(),Zend_Date::ISO_8601);
                        // 					$st_meiopagamento = '';
                        // 					switch ($lancamento->id_meiopagamento){
                        // 						case MeioPagamentoTO::DINHEIRO:
                        // 							$st_meiopagamento = 'dinheiro';
                        // 							break;
                        // 						case MeioPagamentoTO::CARTAO:
                        // 							$st_meiopagamento = 'cartão';
                        // 							break;
                        // 						case MeioPagamentoTO::BOLETO:
                        // 							$st_meiopagamento = 'boleto';
                        // 							break;
                        // 						case MeioPagamentoTO::CHEQUE:
                        // 							$st_meiopagamento = 'cheque';
                        // 							if(!$dataVencimento->isToday()){
                        // 								if($dataHoje->isLater($dataVencimento)){
                        // 									if(!$lancamento->dt_emissao || !$lancamento->st_emissor || !$lancamento->st_coddocumento){
                        // 										throw new Zend_Exception("Não é possível confirmar " . Ead1_LabelFuncionalidade::getLabel(201) . " pois existem lançamentos em cheque não registrados! (Lançamento {$lancamento->id_lancamento})", Ead1_IMensageiro::AVISO);
                        // 									}
                        // 								}
                        // 							}
                        // 							break;
                        // 					}
                        // 					if(!$dataVencimento->isToday()){
                        // 						if($dataHoje->isLater($dataVencimento)){
                        // 							THROW new Zend_Exception("Não foi possivel confirmar  a venda pois existem lançamentos em ".$st_meiopagamento." vencidos!", Ead1_IMensageiro::AVISO);
                        // 						}
                        // 					}
                    }
                }
            }

            $vTO = new VendaTO();
            $vTO->setId_venda($vendaTO->getId_venda());
            $vTO->setId_entidade($vendaTO->getId_entidade());
            $vTO->setId_situacao(VendaTO::SITUACAO_VENDA_ATIVA);
            $vTO->setId_evolucao(VendaTO::EVOLUCAO_CONFIRMADA);
            $vTO->setDt_cadastro($vendaTO->getDt_cadastro());
            $vTO->setId_usuario($vendaTO->getId_usuario());
            $vTO->setId_operador($vendaTO->getId_operador());
            $vTO->setDt_confirmacao(new Zend_Date());
            $vTO->setid_ocorrencia($vendaTO->getid_ocorrencia());

            $produtoDAO = new ProdutoDAO();
            $produtoDAO->editarVenda($vTO);


            // SALVAR AS DATAS DE VENCIMENTO PROGRAMADAS
            $mensageiro = $this->salvarDataVencimentoProgramado($vTO);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getMensagem());
            }

            // EXECUTAR PROCEDIMENTOS AUTOMATICOS PARTINDO DO TIPO DE PRODUTO
            $this->_procedimentoPosConfirmacaoVenda($vTO);

            //Efetua a baixa no estoque do produto
            $this->baixarProdutoEstoque($vTO);

            $this->dao->commit();
            $mensagem = "Recebimento confirmado com sucesso!";
            if ($pulafluxus) {
                return new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::SUCESSO);
            }

            return new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            //echo "confirmaRecebimento rollBack";
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva os dias de vencimento programados
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vendaTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarDataVencimentoProgramado(VendaTO $vendaTO)
    {

        try {

            $this->dao->beginTransaction();

            if (!$vendaTO->getId_venda()) {
                throw new Zend_Exception('Venda não informada');
            }
            $vendaTO->fetch(true, true, true);
            if (!$vendaTO->getDt_confirmacao()) {
                $vendaTO->setDt_confirmacao(new Zend_Date());
                $this->dao->editarVenda($vendaTO);
                $vendaTO->fetch(true, true, true);
            }

            if (!$vendaTO->getDt_confirmacao()) {
                throw new Zend_Exception('Venda sem data de confirmação!');
            }

            $vendaLancamentoTO = new VwVendaLancamentoTO();
            $vendaLancamentoTO->setId_venda($vendaTO->getId_venda());
            $vendaLancamentoTO->setBl_entrada(false);

            $vorm = new VwVendaLancamentoORM();
            $lancamentos = $vorm->consulta($vendaLancamentoTO, false, false, array('nu_ordem'));

            $x = 0;
            if (!empty($lancamentos)) {
                foreach ($lancamentos as $lTO) {
                    if ($lTO->getnu_diavencimento() && $lTO->getnu_ordem()) {

                        $dt_vencimento = clone $vendaTO->getDt_confirmacao();
                        $dt_vencimento->add(($lTO->getnu_ordem() - 1), Zend_Date::MONTH);
                        $dt_vencimento->set($lTO->getnu_diavencimento(), Zend_Date::DAY);

                        $lanTO = new LancamentoTO();
                        $lanTO->setId_lancamento($lTO->getid_lancamento());
                        $lanTO->setDt_vencimento($dt_vencimento);
                        $this->dao->editarLancamento($lanTO);
                        $x++;
                    }
                }
            }
            $this->dao->commit();
            return new Ead1_Mensageiro($x . ' data(s) de vencimento atualizadas com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro('Erro ao salvar as datas de vencimento.', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Executa os Procedimentos pós confirmação de venda, como Alterar data do contrato, emitir certificado, enviar e-mail
     * @param VendaTO $vendaTO
     * @throws Zend_Exception
     * @return void
     */
    public function _procedimentoPosConfirmacaoVenda(VendaTO $vendaTO)
    {

        $posConfirmacaoVendaBO = new PosConfirmacaoVendaBO();
        $posConfirmacaoVendaBO->procedimentoPosConfirmacaoVenda($vendaTO);
    }

    /**
     * Método que Cria ou Altera a Venda adicionando Produto baseando-se em uma taxa
     * @see ContratoBO::solicitarExtensaoContrato()
     * @see ContratoBO::solicitarAlteracaoContratoTaxa()
     * @param ContratoTO $contratoTO
     * @param TaxaTO $taxaTO
     * @param VendaTO|null $vendaTO
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function salvarVendaContratoProdutoTaxa(ContratoTO $contratoTO, TaxaTO $taxaTO, VendaTO $vendaTO = null)
    {

        if ($vendaTO == null) {

            $vendaTO = new VendaTO();
            $vendaTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            $vendaTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
            $vendaTO->setBl_contrato(0);
            $vendaTO->setBl_ativo(1);
            $vendaTO->setId_entidade($contratoTO->getSessao()->id_entidade);
            $vendaTO->setId_usuario($contratoTO->getId_usuario());
            $vendaTO->setId_usuariocadastro($contratoTO->getSessao()->id_usuario);
            $vendaTO->setNu_descontoporcentagem(0);
            $vendaTO->setNu_descontovalor(0);
            $vendaTO->setNu_diamensalidade(0);
            $vendaTO->setNu_juros(0);
            $vendaTO->setNu_parcelas(0);
            $vendaTO->setNu_valorbruto(0);
            $vendaTO->setNu_valorliquido(0);
        }
        $this->dao->beginTransaction();
        try {

            if (!$vendaTO->getBl_ativo()) {
                throw new Zend_Exception('Essa venda não é de um Contrato');
            }

            $vwProdutoTaxaContratoTO = new VwProdutoTaxaContratoTO();
            $vwProdutoTaxaContratoTO->setId_contrato($contratoTO->getId_contrato());
            $vwProdutoTaxaContratoTO->setId_taxa($taxaTO->getId_taxa());
            $vwProdutoTaxaContratoTO->setId_entidade($contratoTO->getSessao()->id_entidade);

            $vwProdutoTaxaContratoORM = new VwProdutoTaxaContratoORM();
            $produto = $vwProdutoTaxaContratoORM->consulta($vwProdutoTaxaContratoTO);

            if (!$produto) {
                throw new Zend_Exception('Nenhum Produto encontrado com a Taxa informada!');
            }

            foreach ($produto as $produtoTaxaContratoTO) {
                $vendaTO->setNu_valorliquido($vendaTO->getNu_valorliquido() + $produtoTaxaContratoTO->getNu_valor());
            }

            $vendaTO->setNu_valorbruto($vendaTO->getNu_valorliquido());

            if ($vendaTO->getId_venda()) {
                $this->editarVenda($vendaTO)->getCurrent();
            } else {
                $vendaTO = $this->cadastrarVenda($vendaTO)->getCurrent();
            }


            $arVendaProdutoTO = false;
            foreach ($produto as $produtoTaxaContratoTO) {

                $vendaProdutoTO = new VendaProdutoTO();
                $vendaProdutoTO->setId_matricula($produtoTaxaContratoTO->getid_matricula());
                $vendaProdutoTO->setId_produto($produtoTaxaContratoTO->getid_produto());
                $vendaProdutoTO->setId_venda($vendaTO->getId_venda());
                $vendaProdutoTO->setNu_valorliquido($produtoTaxaContratoTO->getNu_valor());
                $vendaProdutoTO->setNu_valorbruto($produtoTaxaContratoTO->getNu_valor());
                $arVendaProdutoTO[] = $vendaProdutoTO;
            }

            $this->salvarArrVendaProduto($arVendaProdutoTO);

            $this->dao->commit();

            // 			Zend_Debug::dump($transacao);exit;
            return $this->mensageiro->setMensageiro('Solicitação de extensão de prazo do Contrato realizada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao realizar a extensão de prazo do Contrato: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que salva o array de VendaProdutoTO
     * @param array $arrVendaProdutoTO
     * @return Ead1_Mensageiro
     * @see VendaBO::salvarVendaContratoProdutoTaxa()
     */
    public function salvarArrVendaProduto($arrVendaProdutoTO)
    {
        try {
            $this->dao->beginTransaction();
            $pDAO = new ProdutoDAO();
            if (empty($arrVendaProdutoTO)) {
                THROW new Zend_Exception('O Array de VendaProdutoTO veio Vazio!');
            }
            foreach ($arrVendaProdutoTO as $vendaProdutoTO) {
                if (!($vendaProdutoTO instanceof VendaProdutoTO)) {
                    THROW new Zend_Exception('Um dos items do array não é instancia de VendaProdutoTO');
                }
                if ($vendaProdutoTO->getId_vendaproduto()) {
                    $pDAO->editarVendaProduto($vendaProdutoTO);
                } else {
                    $verificar = $this->verificarProdutoVendaUnica($vendaProdutoTO->getId_venda(), $vendaProdutoTO->getId_produto());
                    if ($verificar->getTipo() == Ead1_IMensageiro::ERRO) {
                        THROW new Zend_Exception($verificar->getFirstMensagem());
                    }

                    $pDAO->cadastrarVendaProduto($vendaProdutoTO);
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Produtos da Venda Salvos com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            //throw $e;
            return $this->mensageiro->setMensageiro('Erro ao Salvar os Produtos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Verifica se o produto pode ser vendido para o Usuário mais de uma vez
     * @param int $id_venda
     * @param int $id_produto
     * @throws Zend_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function verificarProdutoVendaUnica($id_venda, $id_produto)
    {

        try {

            $produto = new ProdutoTO();
            $produto->setId_produto($id_produto);
            $produto->fetch(true, true, true);

            if (!$produto->getBl_unico()) {
                return new Ead1_Mensageiro("Venda permitida", Ead1_IMensageiro::SUCESSO);
            }

            $negocio = new \G2\Negocio\Negocio();
            $venda = $negocio->findOneBy('\G2\Entity\Venda', array('id_venda' => $id_venda));

            if (!$venda->getId_entidade()) {
                throw new Zend_Exception("Venda não encontrada!");
            }

            $vwPessoaTO = new VwPessoaTO();
            $vwPessoaTO->setId_usuario($venda->getId_usuario()->getId_usuario());
            $vwPessoaTO->setId_entidade($venda->getId_entidade());
            $vwPessoaTO->fetch(false, true, true);

            if (!$vwPessoaTO->getSt_email()) {
                throw new Zend_Exception("E-mail do cliente não encontrado!");
            }

            $vwVendaunica = new VwProdutoVendaUnicaTO();
            $vwVendaunica->setSt_email($vwPessoaTO->getSt_email());
            $vwVendaunica->setId_produto($produto->getId_produto());

            $vwVendaunica->fetch(false, true, true);

            if ($vwVendaunica->getId_vendaproduto()) {
                throw new Zend_Exception("O e-mail " . $vwPessoaTO->getSt_email() . " já foi utilizado para adquirir o produto (" . $vwVendaunica->getId_produto() . ") - " . $produto->getSt_produto());
            }
            return new Ead1_Mensageiro("Venda permitida", Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva o array de lancamento de venda
     * @param array $arrVwVendaLancamentoTO com VwVendaLancamentoTO
     * @throws InvalidArgumentException
     * @throws DomainException
     * @return Ead1_Mensageiro
     */
    public function salvarArrLancamentoVenda(array $arrVwVendaLancamentoTO)
    {
        try {
            $negocioVenda = new \G2\Negocio\Venda();

            $this->dao->beginTransaction();
            $arrIdLancamentos = array();
            $arrIdLacamentoTO = array();


            if (isset($arrVwVendaLancamentoTO[0])) {
                $toVeVendaLancamento = $arrVwVendaLancamentoTO[0];
                $toLancamentoVenda = new LancamentoVendaTO();
                $toLancamentoVenda->setId_venda($toVeVendaLancamento->getId_venda());

                //Busca os lancamentos vinculados as vendas
                $arrIdLancamentos = $negocioVenda->retornarArrayIdLancamentosDaVenda($toVeVendaLancamento->getId_venda());
            }

            //Instancia BO de Matricula
            $matriculaBO = new MatriculaBO();

            //Verifica se o array de venda lancamento esta preenchido
            if (!empty($arrVwVendaLancamentoTO)) {

                //array de dias vencimento
                $arrDiasVencimento = array();


                //Percorre o array de TO
                foreach ($arrVwVendaLancamentoTO as $key => $vwVendaLancamentoTO) {

                    //se o id_lancamento foi passado armazena o valor em um array temporario
                    if ($vwVendaLancamentoTO->getId_lancamento()) {
                        $arrIdLacamentoTO[] = $vwVendaLancamentoTO->getId_lancamento();
                    }

                    //Verifica se é uma instancia de VwVendaLancamentoTO
                    if (!$vwVendaLancamentoTO instanceof VwVendaLancamentoTO) {
                        throw new InvalidArgumentException("O argumento passado para o array não é do tipo correto!");
                    }

                    $arrLancamentoTO[$key] = $this->completaArrLancamento($vwVendaLancamentoTO);

                    //Verifica é uma entrada ou parcela
                    if ($vwVendaLancamentoTO->getBl_entrada()) {
                        $arrLancamentoTO[$key]->bl_entrada = true;
                    } else {
                        $arrLancamentoTO[$key]->bl_entrada = false;
                        //verifica se nu_diavencimento foi passado
                        if ($vwVendaLancamentoTO->nu_diavencimento) {
                            //atribui o valor no novo array para dias vencimentos
                            $arrDiasVencimento[$vwVendaLancamentoTO->id_meiopagamento] = $vwVendaLancamentoTO->nu_diavencimento;
                        }
                    }
                }
                //Diferencia os valores nos arrays, os que foram atribuido a variavel serão inativados no banco
                $arrDelete = array_diff_assoc($arrIdLancamentos, $arrIdLacamentoTO);

                //verifica se o array contei posições
                if ($arrDelete) {
                    //chama o método da negocio de venda para inativar
                    $negocioVenda->inativaArrayLancamentos($arrDelete);
                    if (!$negocioVenda) {
                        throw new Exception("Erro ao tentar inativar lancamentos da venda.");
                    }
                }

                $vendaMeioVencimentoTO = new VendaMeioVencimentoTO(); //Instancia a to VendaMeioVencimentoTO
                $vendaMeioVencimentoTO->setId_venda($vwVendaLancamentoTO->getId_venda()); //seta o id da venda


                if (!$this->dao->deletarVendaMeioVencimento($vendaMeioVencimentoTO)) {
                    throw new Zend_Exception('Erro na hora de deletar dia de vencimento antigo.');
                }

                //Cadastra o lancamento
                if (!$matriculaBO->cadastrarLancamentos($arrLancamentoTO, $vwVendaLancamentoTO->getId_venda())) {
                    throw new DomainException('Não foi possível vincular os lancamentos de venda!');
                }

                //Cadastra os meios de vencimento
                if (!empty($arrDiasVencimento)) {
                    $this->cadastrarArrVendaMeioVencimento($arrDiasVencimento, $vwVendaLancamentoTO->getId_venda());
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro("Lançamento de Venda cadastrado com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao salvar o(s) dado(s) de lancamento! ERRO: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $arrDiasVencimento
     * @param int $id_venda
     * @throws Zend_Exception
     */
    public function cadastrarArrVendaMeioVencimento(array $arrDiasVencimento, $id_venda)
    {
        $vendaMeioVencimentoTO = new VendaMeioVencimentoTO();
        $vendaMeioVencimentoTO->setId_venda($id_venda);
        foreach ($arrDiasVencimento as $id_meiopagamento => $nu_diavencimento) {
            $vendaMeioVencimentoTO->setId_meiopagamento($id_meiopagamento);
            $vendaMeioVencimentoTO->setNu_diavencimento($nu_diavencimento);
            if (!$this->dao->cadastrarVendaMeioVencimento($vendaMeioVencimentoTO)) {
                throw new Zend_Exception('Erro ao tentar cadastrar dia de vencimento do meio de pagamento.');
            }
        }
    }

    /**
     * Metodo que exclui os lançamentos de uma venda e cadastra um array de Lançamentos e seus vinculos com a venda
     * @param array $arrLancamentoTO
     * @param VendaTO $vendaTO
     * @param $gerarDataPrimeiroLancamento | indica que sera gerada a data do primeiro lançamento
     * @param Boolean $gerarDatasVencimento | Indica se sera gerado automaticamente os vencimentos dos lançamentos pós o primeiro lançamento
     * @return Ead1_Mensageiro
     * @throws Exception $e
     */
    public function salvarArrayLancamentos($arrLancamentoTO, VendaTO $vendaTO, $gerarDataPrimeiroLancamento = false, $gerarDatasVencimento = true, $arrOrdens = null, $id_modelovenda = 1)
    {

        try {

            $arrayLancamentoRetorno = array();

            $this->dao->beginTransaction();
            //Variavel que indica se já voram excluidos os vinculos
            $delected = false;
            //Variavel que indica se é o primeiro lançamento para cadastrar como entrada
            $first = true;
            $setaRecorrente = true;
            $data_entrada_recorrente = new Zend_Date(null, Zend_Date::ISO_8601);
            $data_entrada = new Zend_Date(null, Zend_Date::ISO_8601);

            /*$efTO = new EntidadeFinanceiroTO();
            $efTO->setId_entidade($vendaTO->getId_entidade());
            $efTO->fetch(false, true, true);*/

            $efTO = $this->em->getRepository('\G2\Entity\EntidadeFinanceiro')->findOneBy(array('id_entidade' => $vendaTO->getId_entidade()));

            $lancamento = $arrLancamentoTO[0];
            if ($id_modelovenda == \G2\Entity\ModeloVenda::ASSINATURA && $lancamento instanceof LancamentoTO) {
                $data_entrada_recorrente = $lancamento->getDt_vencimento();
                $data_entrada = $lancamento->getDt_vencimento();
            } else {
                if ($efTO->getNu_diaspagamentoentrada()) {
                    $data_entrada->addDay($efTO->getNu_diaspagamentoentrada());
                }
            }

            $dia_vencimento = $vendaTO->getNu_diamensalidade();

            //Apagando os dados da TransacaoLancamento
            //$where = 'id_lancamento in (select id_lancamento from tb_lancamentovenda where id_venda = ' . $vendaTO->getid_venda() . ') ';
            $this->em->getRepository('\G2\Entity\Venda')->deletarTransacaoLancamento($vendaTO->getid_venda());

            $this->em->getRepository('\G2\Entity\LancamentoVenda')->deletarLancamentoVenda($vendaTO->getId_venda());

            $arrLancamentos = $this->em->getRepository('\G2\Entity\VwVendaLancamento')->findBy(array('id_venda' => $vendaTO->getId_venda()));

            //Excluindo Lançamentos
            if ($arrLancamentos) {
                $arrWhere = [];
                foreach ($arrLancamentos as $vwLancamento) {
                    $arrWhere[] = $vwLancamento->getIdLancamento();
                }
                $where = "id_lancamento in (" . implode(" ,", $arrWhere) . ")";
                $this->em->getRepository('\G2\Entity\Lancamento')->deletarLancamento($where);
            }

            foreach ($arrLancamentoTO as $index => $lancamentoTO) {
                if (!($lancamentoTO instanceof LancamentoTO)) {
                    THROW new Zend_Validate_Exception("Um dos Items do Array Não é uma Instancia de LancamentoTO!");
                }

                $lancamentoTO->setId_usuariocadastro($lancamentoTO->getId_usuariocadastro() ? $lancamentoTO->getId_usuariocadastro() : $lancamentoTO->getSessao()->id_usuario);

                $lancamentoVendaTO = new LancamentoVendaTO();
                //validar lançamentos de entrada e vencimento
                if ($first) {
                    $first = false;
                    $lancamentoVendaTO->setBl_entrada(true);
                    //Verificando se sera gerada a data do primeiro lançamento
                    if ($gerarDataPrimeiroLancamento) {
                        $lancamentoTO->setDt_vencimento($data_entrada);
                        $dia_vencimento = $data_entrada->toString('dd');
                    } else {
                        $data_entrada = new Zend_Date($lancamentoTO->getDt_vencimento(), Zend_Date::ISO_8601);
                    }
                } else {
                    if ($lancamentoTO->getnu_cartao() > 1 && $data_entrada->toString('YYYY-MM-dd') == $lancamentoTO->getDt_vencimento()->toString('YYYY-MM-dd') && $arrOrdens[$index] == 1) {
                        $lancamentoVendaTO->setBl_entrada(true);
                    } else {
                        $lancamentoVendaTO->setBl_entrada(false);
                    }


                    // Coloca o vencimento do Recorrente para o dia da compra
                    if ($lancamentoTO->getId_meiopagamento() == MeioPagamentoTO::RECORRENTE && $setaRecorrente) {
                        $dia_vencimento = $data_entrada_recorrente->toString('dd');
                        $data_entrada = $data_entrada_recorrente;
                        $setaRecorrente = false;
                    }

                    //Verificando se serão geradas as datas do restante dos lançamentos
                    if ($gerarDatasVencimento) {
                        $data_entrada = $data_entrada->addMonth(1);
                        $mes_lancamento_atual = (int)$data_entrada->toString('MM');
                        $data_entrada = $data_entrada->setDay($dia_vencimento);
                        //Se o mes for diferente do definido quer dizer que o dia do mes anterior é maior que o do mes atual
                        //Colocando o vencimento para o ultimo dia do mes atual
                        if ((int)$data_entrada->toString('MM') != $mes_lancamento_atual) {
                            $data = clone $data_entrada;
                            $data = $data->setDay(1);
                            $data = $data->setMonth($mes_lancamento_atual);
                            $data = $data->addMonth(1);
                            $data = $data->subDay(1);
                            $ultimo_dia_mes = (int)$data->toString('dd');
                            $data_entrada = $data_entrada->setDay($ultimo_dia_mes);
                            $data_entrada = $data_entrada->setMonth($mes_lancamento_atual);
                        }
                        $lancamentoTO->setDt_vencimento($data_entrada);
                    }
                }
                $lancamentoTO->setBl_quitado($lancamentoTO->getBl_quitado() ? $lancamentoTO->getBl_quitado() : false);
                $lancamentoTO->setId_usuariocadastro($lancamentoTO->getId_usuariocadastro() ? $lancamentoTO->getId_usuariocadastro() : $lancamentoTO->getSessao()->id_usuario);
                $lancamentoTO->setId_entidade($lancamentoTO->getId_entidade() ? $lancamentoTO->getId_entidade() : $vendaTO->getId_entidade());


                //Cadastra Lançamento
                $id_lancamento = $this->dao->cadastrarLancamento($lancamentoTO);


                //Monta o TO  de lançamento venda
                $lancamentoVendaTO->setId_lancamento($id_lancamento);
                $lancamentoVendaTO->setId_venda($vendaTO->getId_venda());
                $lancamentoVendaTO->setNu_ordem((!empty($arrOrdens) ? $arrOrdens[$index] : $index));

                //Cadastro relacionamento entre lançamentos da venda
                $id_lancamentovenda = $this->dao->cadastrarLancamentoVenda($lancamentoVendaTO);
                $lancamentoTO->setId_lancamento($id_lancamento);

                $arrayLancamentoRetorno[] = array(
                    'id_lancamento' => $lancamentoTO->getId_lancamento(),
                    'nu_valor' => $lancamentoTO->getNu_valor(),
                    'id_meiopagamento' => $lancamentoTO->getId_meiopagamento(),
                    'id_usuariolancamento' => $lancamentoTO->getId_usuariolancamento(),
                    'id_entidadelancamento' => $lancamentoTO->getId_entidadelancamento(),
                    'id_entidade' => $lancamentoTO->getId_entidade(),
                    'bl_quitado' => $lancamentoTO->getBl_quitado(),
                    'dt_vencimento' => $lancamentoTO->getDt_vencimento() ? $lancamentoTO->getDt_vencimento()->toString("yyyy-MM-dd HH:mm:ss") : '',
                    'nu_ordem' => $lancamentoVendaTO->getNu_ordem()
                );
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro('Lançamentos Cadastrados Com Sucesso!', Ead1_IMensageiro::SUCESSO, $arrayLancamentoRetorno);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Lançamentos', Ead1_IMensageiro::ERRO, $e);
            THROW $e;
        } catch (Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Lançamentos', Ead1_IMensageiro::ERRO, $e);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que salva o envolvido da venda
     * @param VendaEnvolvidoTO $to
     * @param $soExcluir Indica se ira somente deixar todos os envolvidos como bl_ativo = 0
     * @return Ead1_Mensageiro
     */
    public function salvarVendaEnvolvido(VendaEnvolvidoTO $to, $soExcluir = false)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_venda()) {
                THROW new Zend_Exception('O id_venda não veio setado!');
            }
            $orm = new VendaEnvolvidoORM();
            $this->dao->editarVendaEnvolvido(new VendaEnvolvidoTO(array('bl_ativo' => 0)), 'id_venda = ' . $to->getId_venda());
            if (!$soExcluir) {
                if ($orm->consulta(new VendaEnvolvidoTO(array('id_venda' => $to->getId_venda(), 'id_usuario' => $to->getId_usuario())), false, true)
                ) {
                    $to->setId_vendaenvolvido(null);
                    $this->dao->editarVendaEnvolvido($to, 'id_venda = ' . $to->getId_venda() . ' AND id_usuario = ' . $to->getId_usuario());
                } else {
                    $to->setId_vendaenvolvido(null);
                    $id_vendaenvolvido = $this->dao->cadastrarVendaEnvolvido($to);
                    $to->setId_vendaenvolvido($id_vendaenvolvido);
                }
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Salvar o Envolvido da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que salva a integração da venda
     * @param VendaIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarVendaIntegracaoTO(VendaIntegracaoTO $to, $soExcluir = false)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_venda()) {
                THROW new Zend_Exception('O id_venda não veio setado!');
            }

            $orm = new VendaIntegracaoORM();
            if ($orm->consulta(new VendaIntegracaoTO(array('id_venda' => $to->getId_venda())), false, true)) {
                $this->dao->editarVendaIntegracao($to, 'id_venda = ' . $to->getId_venda());
            } else {
                $to->setId_VendaIntegracao(null);
                $id_vendaintegracao = $this->dao->cadastrarVendaIntegracao($to);
                $to->setId_VendaIntegracao($id_vendaintegracao);
            }

            $to->fetch(true, true, true);

            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Salvar a Integração da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra uma Venda
     * @param VendaTO $vTO
     * @throws Zend_Validate_Exception
     * @throws Zend_Db_Exception
     * @return Ead1_Mensageiro
     * @see VendaBO::salvarVendaContratoProdutoTaxa()
     */
    public function cadastrarVenda(VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_usuario()) {
                THROW new Zend_Validate_Exception("Não foi o Encontrado o Usuário da Venda!");
            }

            if (!$vTO->getId_entidade()) {
                $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            }

            $vTO->setBl_ativo(1);
            $vTO->setId_usuariocadastro(($vTO->getId_usuariocadastro() ? $vTO->getId_usuariocadastro() : $vTO->getSessao()->id_usuario));

            if (!$vTO->getId_evolucao()) {
                $vTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_NEGOCIACAO);
            }

            if (!$vTO->getId_situacao()) {
                $vTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
            }

            $id_venda = $this->dao->cadastrarVenda($vTO);
            $vTO->setId_venda($id_venda);
            $this->mensageiro->setMensageiro($vTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro($vTO, Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Cadastra um lançamento
     * @author desconhecido
     * @param LancamentoTO $lancamentoTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function cadastrarLancamento(LancamentoTO $lancamentoTO)
    {
        try {
            $this->dao->beginTransaction();
            $id_lancamento = $this->dao->cadastrarLancamento($lancamentoTO);
            $lancamentoTO->setId_lancamento($id_lancamento);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($lancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao cadastrar lançamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que altera um Lançamento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param LancamentoTO $lancamentoTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function editarLancamento(LancamentoTO $lancamentoTO, $where = null, $aceitaNull = false)
    {
        $this->dao->beginTransaction();
        try {
            $lancamentoTO->setDt_atualizado(new Zend_Date(null));

            if ($this->dao->editarLancamento($lancamentoTO, $where, $aceitaNull)) {
// 				$fluxusBO = new FluxusBO();
// 				if($fluxusBO->atualizarLancamentoFluxus($lancamentoTO)->getTipo() === Ead1_IMensageiro::ERRO){
// 					throw new Zend_Exception("Erro ao atualizar registro no Fluxus!");
// 				}
            }

            $this->dao->commit();
            return $this->mensageiro->setMensageiro("Lancamento atualizado com sucesso!", Ead1_IMensageiro::SUCESSO, $lancamentoTO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao editar o lançamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Cadastra um lancamento venda
     * @author desconhecido
     * @param LancamentoVendaTO $lancamentoVendaTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function cadastrarLancamentoVenda(LancamentoVendaTO $lancamentoVendaTO)
    {
        try {
            $this->dao->beginTransaction();
            $this->dao->cadastrarLancamentoVenda($lancamentoVendaTO);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($lancamentoVendaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao vincular lançamento a venda ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Cadastra todos os lançamentos da renegociação do lançamento original
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     * @param mixed $arrLancamentoTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarLancamentoVendaRenegociacao(array $arrLancamentoTO)
    {

        try {

            if (!empty($arrLancamentoTO)) {
                $this->dao->beginTransaction();

                // cria um lancamentoTO e busca os dados com base no
                // id_lancamentooriginal, para fazer as comparações
                $lancamentoTOOriginal = new LancamentoTO();
                $lancamentoTOOriginal->setId_lancamento($arrLancamentoTO[0]->getId_lancamentooriginal());
                $lancamentoTOOriginal->fetch(true, true, true);

                if ($lancamentoTOOriginal->getBl_ativo()) {
                    $lancamentoTOOriginal->setBl_ativo(false);
                    $lancamentoTOOriginal->setDt_atualizado(new Zend_Date(null));

                    if (!$this->dao->editarLancamento($lancamentoTOOriginal)) {
                        throw new Zend_Db_Exception('Erro ao atualizar lancamento original da renegociação.');
                    }
                }

                $lancamentoVendaTO = new LancamentoVendaTO();
                $lancamentoVendaTO->setId_lancamento($lancamentoTOOriginal->getId_lancamento());
                $lancamentoVendaTO->fetch(false, true, true);

                $nu_ordem = 1;

                foreach ($arrLancamentoTO as &$lancamentoTO) {

                    if ($lancamentoTO instanceof LancamentoTO) {

                        if (!$lancamentoTO->getId_lancamentooriginal()) {
                            throw new Zend_Validate_Exception('Necessário informar id_lancamentooriginal');
                        }

                        if (!$lancamentoTO->getNu_valor()) {
                            throw new Zend_Validate_Exception('Necessário informar Valor');
                        }

                        if (!Zend_Date::isDate($lancamentoTO->getDt_vencimento())) {
                            throw new Zend_Validate_Exception('Necessário informar Data de Vencimento');
                        }

                        if (!$lancamentoTO->getId_meiopagamento()) {
                            throw new Zend_Validate_Exception('Necessário informar Meio de Pagamento');
                        }

                        if (!$lancamentoTO->getId_usuariolancamento()) {
                            throw new Zend_Validate_Exception('Necessário informar Responsavel');
                        }

                        if (!$lancamentoTO->getSt_renegociacao()) {
                            throw new Zend_Validate_Exception('Necessário informar Motivo da Renegociação');
                        }
                    } else {
                        throw new Zend_Exception('Objeto passado para o método não é uma instância de LancamentoTO');
                    }

                    $lancamentoTO->setid_entidade($lancamentoTOOriginal->getid_entidade());
                    $lancamentoTO->setId_usuariocadastro($lancamentoTO->getSessao()->id_usuario);
                    $lancamentoTO->setId_usuariolancamento($lancamentoTOOriginal->getId_usuariolancamento());
                    $lancamentoTO->setId_entidadelancamento($lancamentoTOOriginal->getId_entidadelancamento());
                    $lancamentoTO->setNu_cobranca($lancamentoTOOriginal->getNu_cobranca() ? $lancamentoTOOriginal->getNu_cobranca() : $nu_ordem);
                    $lancamentoTO->setNu_assinatura($lancamentoTOOriginal->getNu_assinatura());
                    $lancamentoTO->setId_tipolancamento(TipoLancamentoTO::CREDITO);

                    $mensageiro = $this->cadastrarLancamento($lancamentoTO);

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }

                    $lancamentoTO = $mensageiro->getFirstMensagem();
                    $lancamentoVendaTOReneg = new LancamentoVendaTO();
                    $lancamentoVendaTOReneg->setId_venda($lancamentoVendaTO->getId_venda());
                    $lancamentoVendaTOReneg->setId_lancamento($lancamentoTO->getId_lancamento());
                    $lancamentoVendaTOReneg->setBl_entrada(false);
                    $lancamentoVendaTOReneg->setNu_ordem($nu_ordem);

                    if (!$this->cadastrarLancamentoVenda($lancamentoVendaTOReneg)->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception('Erro ao salvar LancamentoVendaTO para o lancamento de renegociação!');
                    }

                    $nu_ordem++;
                }

                //reset($arrLancamentoTO);

                $this->dao->commit();
                //return $this->mensageiro->setMensageiro($arrLancamentoTOResultado, Ead1_IMensageiro::SUCESSO);
                return $this->mensageiro->setMensageiro("Lançamentos da renegociação salvos com sucesso!", Ead1_IMensageiro::SUCESSO, $arrLancamentoTO);
            }
        } catch (Zend_Validate_Exception $ve) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($ve->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao cadastrar lançamentos da renegociação - ' . $e->getLine(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que altera um Lançamento quando é Renegociação
     * @author Rafael Rocha - rafael.rocha.mg@gmail.com
     * @param LancamentoTO $lancamentoTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function editarLancamentoVendaRenegociacao(LancamentoTO $lancamentoTO)
    {

        try {
            $this->dao->beginTransaction();

            $id_lancamento = $lancamentoTO->getId_lancamento();
            $nu_quitado = $lancamentoTO->getNu_quitado();
            $dt_quitado = $lancamentoTO->getDt_quitado();

            if (!empty($id_lancamento)) {

                $lancamentoTO->setDt_atualizado(new Zend_Date(null)); //Atualiza data
                //Se tiver valor a ser quitado, a data e esse valor for maior ou igual ao valor do lancamento.
                if (!empty($nu_quitado) && !empty($dt_quitado)) {

                    $lanctoTOAux = new LancamentoTO();
                    $lanctoTOAux->setId_lancamento($lancamentoTO->getId_lancamento());
                    $lanctoTOAux->fetch(true, true, true);
                    $lanctoTOAux = $this->retornarLancamentoValorAtualizado($lanctoTOAux)->getFirstMensagem();

                    if ($lancamentoTO->getNu_quitado() >= $lanctoTOAux->getNu_valor()) {
                        $lancamentoTO->setBl_quitado(true);
                    } else {
                        $lancamentoTO->setBl_quitado(false);
                    }
                }


                if ($this->dao->editarLancamento($lancamentoTO)) {

                    $lancamentoTO->fetch(true, true, true);
                    $fluxusBO = new FluxusBO();
                    $fluxusBO->atualizarLancamentoFluxus($lancamentoTO);

                    $this->dao->commit();
                }
            }

            return $this->mensageiro->setMensageiro($lancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao editar o lançamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que seta protocolo pra vendas
     * @param ProtocoloTO $pTO
     * @param array $arrVendas
     * @throws Zend_Db_Exception
     * @return Ead1_Mensageiro
     */
    public function editaVendaProtocolo(ProtocoloTO $pTO, $arrVendas)
    {
        try {
            foreach ($arrVendas as $venda) {
                $vTO = new VendaTO();
                $vTO->setId_venda($venda->getId_venda());
                $vTO->setId_evolucao($venda->getId_evolucao());
                $vTO->setId_protocolo($pTO->getId_protocolo());
                if (!$this->dao->editarVenda($vTO)) {
                    throw new Zend_Exception('Erro ao fazer update');
                }
            }
            return $this->mensageiro->setMensageiro('Protocolo vinculado a venda(s) com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Víncular Protocolo a Venda(s)!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo edita venda
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     * @see VendaBO::salvarVendaContratoProdutoTaxa()
     */
    public function editarVenda(VendaTO $vTO, $aceitanull = false)
    {
        try {
            if (!$this->dao->editarVenda($vTO, $aceitanull)) {
                throw new Zend_Exception('Erro ao fazer update');
            }
            return $this->mensageiro->setMensageiro('Venda editada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao Editar Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que faz o cancelamento do recebimento de uma Venda.
     * Este método pode executar algum webservice
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     */
    public function cancelarRecebimentoVenda(VendaTO $vTO)
    {

        $this->dao->beginTransaction();
        try {

            if (!$vTO->getId_venda()) {
                throw new Zend_Exception('Id da venda não informado!');
            }

            $vendaClone = new VendaTO();
            $vendaClone->setId_venda($vTO->getId_venda());
            $vendaClone->fetch(true, true, true);

            $vTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            $vTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
            $mensageiro = $this->editarVenda($vTO);
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $transacao = new TransacaoFinanceiraTO();
            $transacao->setId_venda($vTO->getId_venda());
            $transacao->setNu_status(TransacaoFinanceiraTO::AUTORIZADA);
            $transacao->fetch(false, true, true);

            if ($transacao->getId_transacaofinanceira()) {

                $me = $this->dao->cancelarRecebimentoLancamentoTransacaoFinanceira($transacao);
                if ($me->getTipo() == Ead1_IMensageiro::ERRO) {
                    throw new Zend_Exception($me->getFirstMensagem());
                }

                $tranBO = new TransacaoFinanceiraBO();
                $transacao->setNu_status(TransacaoFinanceiraTO::CANCELADA);
                $me = $tranBO->editarTransacaoFinanceira($transacao);
                if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($me->getFirstMensagem());
                }

                /**
                 * TODO Verificar como saber se a transação foi recorrente ou normal, por enquanto passando apenas normal
                 */
                if ($transacao->getId_cartaobandeira()) {
                    $cartao = new PagamentoCartaoBO($vTO->getId_entidade(), $transacao->getId_cartaobandeira(), MeioPagamentoTO::CARTAO);
                    $mensageiro = $cartao->cancelarCartao($transacao, $vendaClone);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }
                }
            }

            $this->dao->commit();
            return new Ead1_Mensageiro('Venda cancelada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->beginTransaction();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que altera o usuario da Venda, Contrato, Responsável Financeiro e Lançamentos com o Id_usuario Anterior
     * @param UsuarioTO $uTO
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoAlterarUsuarioVendaContratoResponsavelLancamento(UsuarioTO $uTO, VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_usuario()) {
                THROW new Zend_Exception('O id_usuario da venda não veio setado!');
            }
            if (!$uTO->getId_usuario()) {
                THROW new Zend_Exception('O id_usuario do novo usuario não veio setado!');
            }
            if ($uTO->getId_usuario() == $vTO->getId_usuario()) {
                THROW new Zend_Exception('O id_usuario do novo usuario é o mesmo da venda!');
            }
            $this->dao->beginTransaction();
            $matriculaDAO = new MatriculaDAO();
            $this->dao->editarVenda(new VendaTO(array('id_venda' => $vTO->getId_venda(), 'id_usuario' => $uTO->getId_usuario())));
            $contrato = $matriculaDAO->retornarContrato(new ContratoTO(array('id_venda' => $vTO->getId_venda())))->toArray();
            $contrato = Ead1_TO_Dinamico::encapsularTo($contrato, new ContratoTO(), true);
            if (!$contrato) {
                THROW new Zend_Validate_Exception("Nenhum Contrato Encontrado!");
            }
            $matriculaDAO->editarContrato(new ContratoTO(array(
                'id_contrato' => $contrato->getId_contrato(),
                'id_venda' => $vTO->getId_venda(),
                'id_usuario' => $uTO->getId_usuario()
            )));
            $matriculaDAO->editarContratoResponsavel(new ContratoResponsavelTO(array(
                'id_contrato' => $contrato->getId_contrato(),
                'bl_ativo' => 0)), "id_contrato = " . $contrato->getId_contrato() . " AND id_usuario = " . $vTO->getId_usuario());
            $arrVendaLancamento = $this->dao->retornarVendaLancamento(new VwVendaLancamentoTO(array('id_venda' => $vTO->getId_venda())))->toArray();
            if (!empty($arrVendaLancamento)) {
                $arrId_lancamento = array();
                foreach ($arrVendaLancamento as $vendaLancamento) {
                    $arrId_lancamento[$vendaLancamento['id_lancamento']] = $vendaLancamento['id_lancamento'];
                }
                $where = 'id_lancamento in (' . implode(', ', $arrId_lancamento) . ')';
                $this->dao->deletarLancamento(new LancamentoTO(), $where);
                $this->dao->deletarLancamentoVenda(new LancamentoVendaTO(), $where);
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro('Usuário da Venda Alterado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Alterar Usuário da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que transforma uma pre-venda em venda
     * @param PreVendaTO $pvTO
     * @param AtendenteVendaTO $avTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoGerarVenda(PreVendaTO $pvTO, AtendenteVendaTO $avTO)
    {
        try {
            //Referencias
            $usuarioTO = new UsuarioTO();
            $pessoaTO = new PessoaTO();
            $vendaTO = new VendaTO();
            $contratoTO = new ContratoTO();
            //--
            try {
                $this->dao->beginTransaction();

                $this->_processoPreVendaCadastrarUsuario($pvTO, $usuarioTO);
                $pessoaCadastrada = $this->_processoPreVendaCadastrarPessoa($pvTO, $pessoaTO, $usuarioTO);

                $this->_processoPreVendaEditarPreVenda($pvTO);
                $this->_processoPreVendaCadastrarVenda($pvTO, $vendaTO);
                $this->_processoPreVendaCadastrarContrato($pvTO, $vendaTO, $contratoTO);
                $this->_processoPreVendaCadastrarLancamentos($pvTO, $vendaTO);
                $this->_processoPreVendaCadastrarAtendenteVenda($avTO, $vendaTO);
                $this->dao->commit();
            } catch (Zend_Exception $e) {
                $this->dao->rollBack();
                THROW $e;
            }
            try {
                if ($this->validaEmail($pvTO->getSt_email()) && !$pessoaCadastrada) {
                    $this->_processoPreVendaCadastrarEmailPessoa($pvTO, $usuarioTO);
                }
                if ($pvTO->getSt_endereco() && !$pessoaCadastrada) {
                    $this->_processoPreVendaCadastrarEnderecoPessoa($pvTO, $usuarioTO);
                }
                if (!$pessoaCadastrada) {
                    $this->_processoPreVendaCadastrarTelefones($pvTO, $usuarioTO);
                }
                if (!$pessoaCadastrada && $pvTO->getSt_rg()) {
                    //Método responsável por cadastrar os documentos do usuário que vem de pré venda.
                    $this->processoPreVendaCadastrarDocumentos($pvTO, $usuarioTO);
                }
            } catch (Zend_Exception $e) {
                //Continua a Execução
            }
        } catch (Zend_Exception $e) {
            //Para a Execução
        }
        return $this->mensageiro;
    }

    public function processoPreVendaCadastrarDocumentos(PreVendaTO $pvTO, UsuarioTO $usuarioTO)
    {

        $documentoIdentidadeTO = new DocumentoIdentidadeTO();
        $pessoaBO = new PessoaBO();

        $documentoIdentidadeTO->setId_entidade($pvTO->getId_entidade());
        $documentoIdentidadeTO->setId_usuario($usuarioTO->getId_usuario());
        $documentoIdentidadeTO->setSt_rg($pvTO->getSt_rg());
        $documentoIdentidadeTO->setSt_orgaoexpeditor($pvTO->getSt_orgaoexpeditor());

        $pessoaBO->cadastrarDocumentoIdentidade($documentoIdentidadeTO);
    }

    /**
     * Subprocesso de pre-venda que verifica e cadastra usuario caso não haja
     * @param PreVendaTO $pvTO
     * @param UsuarioTO $usuarioTO
     */
    private function _processoPreVendaCadastrarUsuario(PreVendaTO $pvTO, UsuarioTO $usuarioTO)
    {
        try {
            $pessoaBO = new PessoaBO();
            $usuarioTO->setBl_ativo(true);
            $usuarioTO->setSt_cpf($pvTO->getSt_cpf());
            $usuarioTO->setSt_nomecompleto($pvTO->getSt_nomecompleto());

            if (!$pvTO->getId_usuario()) {
                //Cadastrando Usuario
                $validaCPF = ($pvTO->getSt_cpf() ? true : false);
                $pessoaBO->cadastrarUsuario($usuarioTO, $pvTO->getId_entidade(), $validaCPF);
            } else {
                $usuarioTO->setId_usuario($pvTO->getId_usuario());
            }

            $pvTO->setId_usuario($usuarioTO->getId_usuario());
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Usuário!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    /**
     * Subprocesso de pre-venda que verifica e pessoa na entidade caso não haja
     * @param PreVendaTO $pvTO
     * @param PessoaTO $pessoaTO
     * @param UsuarioTO $usuarioTO
     * @return Booelan | a resposta deste subprocesso indica se a pessoa já existia ou não
     */
    private function _processoPreVendaCadastrarPessoa(PreVendaTO $pvTO, PessoaTO $pessoaTO, UsuarioTO $usuarioTO)
    {
        try {
            $pessoaBO = new PessoaBO();
            $orm = new PessoaORM();

            $pessoaTO->setBl_ativo(true);
            $pessoaTO->setId_usuario($pvTO->getId_usuario());
            $pessoaTO->setId_entidade(($pvTO->getId_entidade() ? $pvTO->getId_entidade() : $pvTO->getSessao()->id_entidade));

            $existe = $orm->consulta($pessoaTO, true, true);

            //Verificando se a pessoa existe na entidade
            if (!$existe) {
                //Cadastrando Pessoa Se ela não existir na entidade
                $pessoaTO->setId_municipio($pvTO->getId_municipionascimento());
                $pessoaTO->setId_pais($pvTO->getId_paisnascimento());
                $pessoaTO->setSg_uf($pvTO->getSg_ufnascimento());
                $pessoaTO->setSt_sexo($pvTO->getSt_sexo());
                $pessoaTO->setDt_nascimento($pvTO->getDt_nascimento());
                //@todo estado civil fixo!
                $pessoaTO->setId_estadocivil(1);
                $pessoaBO->cadastrarPessoa($usuarioTO, $pessoaTO);
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Pessoa!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
        return ($existe ? true : false);
    }

    /**
     * Subprocesso da pre-venda que edita a pre-venda com o id do usuario
     * @param PreVendaTO $pvTO
     * @throws Zend_Exception
     */
    private function _processoPreVendaEditarPreVenda(PreVendaTO $pvTO)
    {
        try {
            $preVendaDAO = new PreVendaDAO();
            $prevendaTO = new PreVendaTO();
            $prevendaTO->setId_prevenda($pvTO->getId_prevenda());
            $prevendaTO->setId_usuario($pvTO->getId_usuario());
            //Adicionando o id usuario a prevenda
            $preVendaDAO->editarPreVenda($prevendaTO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Pré-Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    /**
     * Subprocesso da pre-venda que Cadastra uma venda
     * @param PreVendaTO $pvTO
     * @param VendaTO $vendaTO
     * @return array $produtos | para cadastro de Matriculas
     */
    private function _processoPreVendaCadastrarVenda(PreVendaTO $pvTO, VendaTO $vendaTO)
    {
        try {
            $vendaTO->setId_entidade($pvTO->getId_entidade());
            $vendaTO->setId_prevenda($pvTO->getId_prevenda());
            $vendaTO->setId_formapagamento($pvTO->getId_formapagamento());
            $vendaTO->setId_usuario($pvTO->getId_usuario());
            $vendaTO->setBl_ativo(true);
            $dt_mensalidade = new Zend_Date(null, Zend_Date::ISO_8601);
            $vendaTO->setNu_diamensalidade($dt_mensalidade->getDay()->toString('dd'));
            $vendaTO->setNu_descontoporcentagem(0);
            $vendaTO->setNu_descontovalor(0);
            $vendaTO->setNu_juros($pvTO->getNu_juros());
            $vendaTO->setNu_parcelas($pvTO->getNu_lancamentos());
            $vendaTO->setNu_valorbruto($pvTO->getNu_valorliquido());
            $vendaTO->setNu_valorliquido($pvTO->getNu_valorliquido());
            $vendaTO->setId_prevenda($pvTO->getId_prevenda());
            $produtos['projetos'] = array();
            $produtos['produtos'] = array();
            $orm = new ProdutoValorORM();
            //Select que retorna os produtos do projeto pedagogico
            $select = $orm->select()->from(array("pvp" => "tb_prevendaproduto"))
                ->setIntegrityCheck(false)
                ->joinInner(array("pv" => "tb_produtovalor"), "pvp.id_produto = pv.id_produto", array("pv.id_produtovalor"))
                ->joinInner(array("ppp" => "tb_produtoprojetopedagogico"), "ppp.id_produto = pv.id_produto", array("ppp.id_produto", "ppp.id_projetopedagogico"))
                ->joinInner(array("pp" => "tb_projetopedagogico"), "pp.id_projetopedagogico = ppp.id_projetopedagogico", array("pp.id_entidadecadastro"))
                ->where("pvp.id_prevenda = " . $pvTO->getId_prevenda());
            $produtos['projetos'] = $orm->fetchAll($select)->toArray();
            if (empty($produtos['projetos'])) {
                $label = new Ead1_LabelFuncionalidade();
                THROW new Zend_Validate_Exception('Os Produtos não possuem vínculo com ' . $label->getLabel(19) . '!');
            }
            $vendaTO->setBl_contrato((empty($produtos) ? false : true));
            $this->cadastrarVenda($vendaTO);
            //Select que retorna todos os produtos
            $select = $orm->select()->from(array("pvp" => "tb_prevendaproduto"))
                ->setIntegrityCheck(false)
                ->joinInner(array("pv" => "tb_produtovalor"), "pvp.id_produto = pv.id_produto", array("pv.id_produtovalor"))
                ->where("pvp.id_prevenda = " . $pvTO->getId_prevenda());
            $produtos['produtos'] = $orm->fetchAll($select)->toArray();
            if (!empty($produtos['projetos']) && count($produtos['projetos']) != count($produtos['produtos'])) {
                THROW new Zend_Validate_Exception('Os Produtos da Pré-Venda não são do Mesmo Tipo!');
            }
            //Cadastrando Venda Produto
            foreach ($produtos['projetos'] as $indice => $produto) {
                $this->_preVendaCadastrarVendaProduto($vendaTO, $pvTO, $produto['id_produto']);

                $vwProdutoTaxaProjetoTO = new VwProdutoTaxaProjetoTO();
                $vwProdutoTaxaProjetoTO->setId_taxa(TaxaTO::MATRICULA);
                $vwProdutoTaxaProjetoTO->setId_projetopedagogico($produto['id_projetopedagogico']);
                $mensageiroProdutoTaxa = $this->retornarVwProdutoTaxaProjeto($vwProdutoTaxaProjetoTO);
                if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::ERRO) {
                    throw new Zend_Exception($mensageiroProdutoTaxa->getMensagem());
                } else if ($mensageiroProdutoTaxa->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $mensagem = $mensageiroProdutoTaxa->getMensagem();
                    $produtoTaxaProjetoROW = $mensagem[0];
                    $vendaTO->nu_valorliquido += $produtoTaxaProjetoROW->nu_valor;
                    $vendaTO->nu_valorbruto += $produtoTaxaProjetoROW->nu_valor;
                    if (!$this->dao->editarVenda($vendaTO)) {
                        throw new Zend_Exception('Erro ao setar valor das taxas na venda.');
                    }
                    $this->_preVendaCadastrarVendaProduto($vendaTO, $pvTO, $produtoTaxaProjetoROW->id_produto, $produtoTaxaProjetoROW->nu_valor);
                }
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    private function _preVendaCadastrarVendaProduto(VendaTO $vendaTO, PreVendaTO $pvTO, $id_produto, $nu_valor = null)
    {
        $valor = (!is_null($nu_valor) ? $nu_valor : $pvTO->getNu_valorliquido());
        $produtoDAO = new ProdutoDAO();
        $vendaProdutoTO = new VendaProdutoTO();
        $vendaProdutoTO->setId_venda($vendaTO->getId_venda());
        $vendaProdutoTO->setId_produto($id_produto);
        $vendaProdutoTO->setNu_valorbruto($valor);
        $vendaProdutoTO->setNu_valorliquido($valor);
        $vendaProdutoTO->setNu_desconto(0);
        $id_vendaproduto = $produtoDAO->cadastrarVendaProduto($vendaProdutoTO);
    }

    /**
     * Subprocesso da pre-venda que cadastra um contrato
     * @param PreVendaTO $pvTO
     * @param VendaTO $vendaTO
     */
    private function _processoPreVendaCadastrarContrato(PreVendaTO $pvTO, VendaTO $vendaTO, ContratoTO $contratoTO)
    {
        try {
            $matriculaBO = new MatriculaBO();
            $matriculaDAO = new MatriculaDAO();
            $contratoTO->setBl_ativo(true);
            $contratoTO->setId_entidade($pvTO->getId_entidade());
            $contratoTO->setId_usuario($pvTO->getId_usuario());
            $contratoTO->setId_venda($vendaTO->getId_venda());
            $id_contrato = $matriculaBO->cadastrarContrato($contratoTO);
            $contratoTO->setId_contrato($id_contrato);
            $contratoResponsavelTO = new ContratoResponsavelTO();
            $contratoResponsavelTO->setBl_ativo(1);
            $contratoResponsavelTO->setId_contrato($id_contrato);
            $contratoResponsavelTO->setId_usuario($pvTO->getId_usuario());
            $contratoResponsavelTO->setNu_porcentagem(100);
            $contratoResponsavelTO->setId_tipocontratoresponsavel(TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO);
            $matriculaDAO->cadastrarContratoResponsavel($contratoResponsavelTO);
        } catch (DomainException $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW new Zend_Exception($e->getMessage());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar Contrato!", Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    /**
     * Subprocesso da pre-venda que cadastra os lançamentos
     * @param PreVendaTO $pvTO
     * @param VendaTO $vendaTO
     */
    private function _processoPreVendaCadastrarLancamentos(PreVendaTO $pvTO, VendaTO $vendaTO)
    {
        try {
            $arrLancamentoTO = array();
            $nu_lancamentos = $pvTO->getNu_lancamentos();
            for ($nu_lancamento = 0; $nu_lancamento < $nu_lancamentos; $nu_lancamento++) {

                $lancamentoTO = new LancamentoTO();
                $lancamentoTO->setId_entidade($pvTO->getId_entidade());
                $lancamentoTO->setId_meiopagamento($pvTO->getId_meiopagamento());
                $lancamentoTO->setId_tipolancamento(TipoLancamentoTO::LANCAMENTO);
                $lancamentoTO->setId_usuariolancamento($pvTO->getId_usuario());
                $lancamentoTO->setNu_valor(round(($pvTO->getNu_valorliquido() / $nu_lancamentos), 2));
                $arrLancamentoTO[] = $lancamentoTO;
            }
            $this->salvarArrayLancamentos($arrLancamentoTO, $vendaTO, true, true);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Lançamentos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    /**
     * Subprocesso da pre-venda que cadastra a venda do atendente
     * @param AtendenteVendaTO $avTO
     * @param VendaTO $vendaTO
     */
    private function _processoPreVendaCadastrarAtendenteVenda(AtendenteVendaTO $avTO, VendaTO $vendaTO)
    {
        try {
            $telemarketingDAO = new TelemarketingDAO();
            $atendenteVenda = clone $avTO;
            $atendenteVenda->setBl_ativo(false);
            $telemarketingDAO->editarAtendenteVenda($atendenteVenda, "id_venda = " . $vendaTO->getId_venda());
            $avTO->setBl_ativo(true);
            $avTO->setId_venda($vendaTO->getId_venda());
            $telemarketingDAO->cadastrarAtendenteVenda($avTO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar a Venda do Atendente!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
    }

    /**
     * Subprocesso da pre-venda que cadastra um email para a pessoa
     * @param PreVendaTO $pvTO
     * @param UsuarioTO $usuarioTO
     */
    private function _processoPreVendaCadastrarEmailPessoa(PreVendaTO $pvTO, UsuarioTO $usuarioTO)
    {
        try {
            $pessoaBO = new PessoaBO();
            $contatosEmailTO = new ContatosEmailTO();
            $contatosEmailPessoaPerfilTO = new ContatosEmailPessoaPerfilTO();
            $contatosEmailPessoaPerfilTO->setId_entidade($pvTO->getId_entidade());
            $contatosEmailPessoaPerfilTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosEmailTO->setSt_email($pvTO->getSt_email());
            $contatosEmailPessoaPerfilTO->setBl_padrao(true);
            $pessoaBO->cadastrarEmail($usuarioTO, $contatosEmailTO, $contatosEmailPessoaPerfilTO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao cadastrar email", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Subprocesso da pre-venda que cadastra um endereço para a pessoa
     * @param PreVendaTO $pvTO
     * @param UsuarioTO $usuarioTO
     */
    private function _processoPreVendaCadastrarEnderecoPessoa(PreVendaTO $pvTO, UsuarioTO $usuarioTO)
    {
        try {
            $pessoaBO = new PessoaBO();
            $enderecoTO = new EnderecoTO();

            $pessoaEnderecoTO = new PessoaEnderecoTO();

            $enderecoTO->setId_municipio($pvTO->getId_municipio());
            $enderecoTO->setId_pais($pvTO->getId_pais());
            $enderecoTO->setBl_ativo(true);
            $enderecoTO->setNu_numero($pvTO->getNu_numero());
            $enderecoTO->setSg_uf($pvTO->getSg_uf());
            $enderecoTO->setSt_bairro($pvTO->getSt_bairro());
            $enderecoTO->setSt_cep($pvTO->getSt_cep());
            $enderecoTO->setSt_complemento($pvTO->getSt_complemento());
            $enderecoTO->setSt_endereco($pvTO->getSt_endereco());
            $enderecoTO->setId_tipoendereco($pvTO->getId_tipoendereco());
            $pessoaEnderecoTO->setId_entidade($pvTO->getId_entidade());
            $pessoaEnderecoTO->setId_usuario($usuarioTO->getId_usuario());

            $pessoaBO->cadastrarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO);

            //@todo Não quero que mostre uma mensagem de sucesso de cadastro de Endereço, verificar como fazer isso.
            $this->mensageiro->setMensageiro('Endereço cadastrado com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro na hora de cadastrar endereço', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Subprocesso da pre-venda que cadastra os telefones para a pessoa
     * @param PreVendaTO $pvTO
     * @param UsuarioTO $usuarioTO
     */
    private function _processoPreVendaCadastrarTelefones(PreVendaTO $pvTO, UsuarioTO $usuarioTO)
    {
        $pessoaBO = new PessoaBO();

        try {
            //Celular
            $contatosTelefoneTO = new ContatosTelefoneTO();
            $contatosTelefonePessoaTO = new ContatosTelefonePessoaTO();
            $contatosTelefonePessoaTO->setId_entidade($pvTO->getId_entidade());
            $contatosTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosTelefonePessoaTO->setBl_padrao(true);
            $contatosTelefoneTO->setId_tipotelefone(TipoTelefoneTO::CELULAR);
            $contatosTelefoneTO->setNu_ddd($pvTO->getNu_dddcelular());
            $contatosTelefoneTO->setNu_ddi($pvTO->getNu_ddicelular());
            $contatosTelefoneTO->setNu_telefone($pvTO->getNu_telefonecelular());
            $pessoaBO->cadastrarTelefone($usuarioTO, $contatosTelefoneTO, $contatosTelefonePessoaTO);

            //Comercial
            $contatosTelefoneTO = new ContatosTelefoneTO();
            $contatosTelefonePessoaTO = new ContatosTelefonePessoaTO();
            $contatosTelefonePessoaTO->setId_entidade($pvTO->getId_entidade());
            $contatosTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosTelefonePessoaTO->setBl_padrao(true);
            $contatosTelefoneTO->setId_tipotelefone(TipoTelefoneTO::COMERCIAL);
            $contatosTelefoneTO->setNu_ddd($pvTO->getNu_dddcomercial());
            $contatosTelefoneTO->setNu_ddi($pvTO->getNu_ddicomercial());
            $contatosTelefoneTO->setNu_telefone($pvTO->getNu_telefonecomercial());
            $pessoaBO->cadastrarTelefone($usuarioTO, $contatosTelefoneTO, $contatosTelefonePessoaTO);

            //Residencial
            $contatosTelefoneTO = new ContatosTelefoneTO();
            $contatosTelefonePessoaTO = new ContatosTelefonePessoaTO();
            $contatosTelefonePessoaTO->setId_entidade($pvTO->getId_entidade());
            $contatosTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosTelefonePessoaTO->setBl_padrao(true);
            $contatosTelefoneTO->setId_tipotelefone(TipoTelefoneTO::RESIDENCIAL);
            $contatosTelefoneTO->setNu_ddd($pvTO->getNu_dddresidencial());
            $contatosTelefoneTO->setNu_ddi($pvTO->getNu_ddiresidencial());
            $contatosTelefoneTO->setNu_telefone($pvTO->getNu_telefoneresidencial());
            $pessoaBO->cadastrarTelefone($usuarioTO, $contatosTelefoneTO, $contatosTelefonePessoaTO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao cadastrar telefone.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Preenche os arrays
     * @param VwVendaLancamentoTO $vwVendaLancamentoTO
     * @return  LancamentoTO
     */
    private function completaArrLancamento($vwVendaLancamentoTO)
    {
        $lancamentoTO = new LancamentoTO();
        $lancamentoTO->setId_lancamento($vwVendaLancamentoTO->getId_lancamento());
        $lancamentoTO->setId_tipolancamento($vwVendaLancamentoTO->getId_tipolancamento());
        $lancamentoTO->setId_meiopagamento($vwVendaLancamentoTO->getId_meiopagamento());
        $lancamentoTO->setNu_valor($vwVendaLancamentoTO->getNu_valor());
        $lancamentoTO->setId_usuariocadastro($vwVendaLancamentoTO->getSessao()->id_usuario);
        $lancamentoTO->setId_usuariolancamento($vwVendaLancamentoTO->getId_usuariolancamento());
        $lancamentoTO->setId_entidadelancamento($vwVendaLancamentoTO->getId_entidadelancamento());
        $lancamentoTO->setId_entidade($vwVendaLancamentoTO->getSessao()->id_entidade);
        $lancamentoTO->setDt_vencimento($vwVendaLancamentoTO->getDt_vencimento());
        $date = new Zend_Date();
        $lancamentoTO->setDt_cadastro($date);
        $lancamentoTO->setNu_cartao($vwVendaLancamentoTO->getNu_cartao());
        $lancamentoTO->setBl_ativo($vwVendaLancamentoTO->getBl_ativo());

        return $lancamentoTO;
    }

    /**
     * Metodo que marca(edita) o lançamento (Dinheiro) como recebido
     * @param LancamentoTO $lTO
     * @return Ead1_Mensageiro
     */
    public function receberDinheiroLancamento(LancamentoTO $lTO)
    {
        try {
            $mensageiro = $this->quitarLancamento($lTO);
            $mensageiro->subtractMensageiro();
            $this->mensageiro->setMensageiro($lTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Salvar Recebimento do Dinheiro.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que quita o Boleto
     * @param LancamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function quitarBoleto(LancamentoTO $to)
    {
        try {
            $mensageiro = $this->quitarLancamento($to);
            $this->mensageiro->setMensageiro("Boleto Quitado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Quitar Boleto - ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que quita o Cheque
     * @param LancamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function quitarCheque(LancamentoTO $to)
    {
        try {
            $mensageiro = $this->quitarLancamento($to);
            $mensageiro->subtractMensageiro();
            $this->mensageiro->setMensageiro("Cheque Quitado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Quitar Cheque.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que quita o Débito
     * @param LancamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function quitarDebito(LancamentoTO $to)
    {
        try {
            $mensageiro = $this->quitarLancamento($to);
            $mensageiro->subtractMensageiro();
            $this->mensageiro->setMensageiro("Débito Quitado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Quitar Débito.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Quinta o Crédito de uma Venda
     * @param VendaTO $venda
     * @param integer $nu_cartao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     * @see PagamentoCartaoBO::verificarPagamento
     * @see PagamentoCartaoBO::verificarPagamento
     */
    public function quitarCreditoVenda(VendaTO $venda, $nu_cartao = 1)
    {

        $this->dao->beginTransaction();
        try {

            if ($venda->getId_evolucao() == VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO || $venda->getId_evolucao() == VendaTO::EVOLUCAO_CONFIRMADA) {

                $vlTO = new VwVendaLancamentoTO();
                $vlTO->setId_venda($venda->getId_venda());
                $vlTO->setnu_cartao($nu_cartao);

                $me = $this->retornarVendaLancamento($vlTO);

                if ($me->getTipo() == Ead1_IMensageiro::SUCESSO) {

                    $arrLancamentoTO = false;

                    foreach ($me->getMensagem() as $vela) {
                        if ($vela instanceof VwVendaLancamentoTO) {
                            if ($vela->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_RECORRENTE
                                || $vela->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_CREDITO
                            ) {
                                $lanTO = new LancamentoTO();
                                $lanTO->setId_lancamento($vela->getid_lancamento());
                                $lanTO->fetch(true, true, true);
//                                if ($vela->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_RECORRENTE && $vela->getBl_entrada()) {
//                                    $lanTO->setId_meiopagamento(\G2\Constante\MeioPagamento::CARTAO_CREDITO);
//                                    $this->editarLancamento($lanTO);
//                                }
                                $arrLancamentoTO[] = $lanTO;
                            }
                        }
                    }

                    if ($arrLancamentoTO) {
                        $mevenda = $this->quitarCredito($arrLancamentoTO);
                        if ($mevenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                            throw new Zend_Exception($mevenda->getFirstMensagem());
                        }
                    } else {
                        throw new Zend_Exception('Não foi possível encontrar os lançamentos!');
                    }
                } else {
                    throw new Zend_Exception('Não foi possível encontrar os lançamentos!');
                }
            }

            $this->dao->commit();
            return new Ead1_Mensageiro("Venda quitada com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que quita o Crédito
     * @param array $arrayLancamentos
     * @return Ead1_Mensageiro
     */
    public function quitarCredito($arrayLancamentos)
    {
        try {

            if ($arrayLancamentos[0]->getDt_prevquitado() instanceof Zend_Date) {
                $dataAux = $arrayLancamentos[0]->getDt_prevquitado();
            } elseif (!$arrayLancamentos[0]->getDt_prevquitado()) {
                $dataAux = new Zend_Date();
            }

            foreach ($arrayLancamentos as $lancamentoTO) {

                $dataAux = $dataAux->addDay(30);
                $lancamentoTO->setDt_prevquitado($dataAux);
                $lancamentoTO->setBl_quitado(false);
                $mensageiro = $this->quitarLancamento($lancamentoTO);
            }

            $mensageiro->subtractMensageiro();
            $this->mensageiro->setMensageiro("Crédito Quitado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Quitar Débito.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que quita o lancamento
     * Alteração realizada por Kayo Silva <kayo.silva@unyleya.com.br> adicionando st_numcheque
     * @param LancamentoTO $to
     * @return Ead1_Mensageiro
     */
    protected function quitarLancamento(LancamentoTO $to)
    {

        /**
         * Problema com valores. Para resolver, clonamos o TO
         * Na sequencia, buscamos o mesmo do banco para que possamos ai sim,
         * avaliarmos o objeto completo e depois atualizarmos com os novos dados.
         *
         */
        $toQuitado = clone $to;

        $this->dao->beginTransaction();
        try {
            if (!$to->getId_lancamento()) {
                THROW new Zend_Validate_Exception('Lançamento Não Identificado!');
            }

            $to->fetch(true, true, true);
            $to->setNu_quitado($toQuitado->getNu_quitado() ? $toQuitado->getNu_quitado() : $to->getNu_valor());
            $to->setNu_juros($toQuitado->getNu_juros() ? $toQuitado->getNu_juros() : $to->getNu_juros());
            $to->setNu_desconto($toQuitado->getNu_desconto() ? $toQuitado->getNu_desconto() : $to->getNu_desconto());
            $to->setBl_baixadofluxus($toQuitado->getBl_baixadofluxus());


            switch ($to->getId_meiopagamento()) {
                case MeioPagamentoTO::BOLETO:
                    $to->setBl_quitado(true);
                    $to->setSt_statuslan($toQuitado->getSt_statuslan() ? $toQuitado->getSt_statuslan() : 1);
                    $to->setDt_quitado($toQuitado->getDt_quitado() ? $toQuitado->getDt_quitado() : new Zend_Date(null));
                    $to->setNu_jurosboleto($toQuitado->getNu_jurosboleto());
                    $to->setNu_descontoboleto($toQuitado->getNu_descontoboleto());
                    $to->setId_usuarioquitacao($toQuitado->getId_usuarioquitacao());
                    $to->setBl_quitacaoretorno($toQuitado->getBl_quitacaoretorno());
                    break;
                case MeioPagamentoTO::CARTAO:
                case MeioPagamentoTO::RECORRENTE:
                    $to->setDt_prevquitado($toQuitado->getDt_prevquitado());
                    $to->setBl_quitado($toQuitado->getBl_quitado());
                    $to->setSt_numcheque($toQuitado->getSt_numcheque());
                    $to->setId_cartaoconfig($toQuitado->getId_cartaoconfig());
                    $to->setSt_coddocumento($toQuitado->getSt_coddocumento());
                    $to->setSt_autorizacao($toQuitado->getSt_autorizacao());
                    $to->setSt_ultimosdigitos($toQuitado->getSt_ultimosdigitos());
                    break;
                default:
                    $to->setBl_quitado(true);
                    $to->setDt_quitado($toQuitado->getDt_quitado() ? $toQuitado->getDt_quitado() : new Zend_Date(null));
                    $to->setSt_numcheque($toQuitado->getSt_numcheque()); //Adicionado código do cheque
            }

            $vwVendaLancamentoTO = new VwVendaLancamentoTO();
            $vwVendaLancamentoTO->setId_lancamento($to->getId_lancamento());
            $vwVendaLancamentoTO->fetch(false, true, true);

            /** tentando não atualizar o vencimento no método de quitar
             * Criei outro cone para não atrapalhar possíveis métodos que usem o mesmo objeto
             */
            $toParaAtaualiar = clone $to;
            $toParaAtaualiar->setDt_vencimento(null);
            $this->mensageiro = $this->editarLancamento($toParaAtaualiar); //->setMensageiro('Lançamento Quitado com Sucesso!',Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();

            if ($vwVendaLancamentoTO->getBl_entrada() && $vwVendaLancamentoTO->getId_entidade() && $vwVendaLancamentoTO->getId_venda()) {
                $ei = new \G2\Entity\EntidadeIntegracao();
                $ei->findOneBy(array('id_sistema' => 21, 'id_entidade' => $vwVendaLancamentoTO->getId_entidade()));
                if ($ei->getId_entidadeintegracao()) {
                    $crm = new \G2\Negocio\IntegracaoCRM($vwVendaLancamentoTO->getId_entidade());
                    $crm->atualizarPagamentoCRM($vwVendaLancamentoTO->getId_venda());
                }
            }

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            $this->dao->rollBack();
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Quitar Lançamento!", Ead1_IMensageiro::ERRO, $e->getMessage());
            $this->dao->rollBack();
        }
        return $this->mensageiro;
    }

    /**
     * Método que insere a data de previsão de quitação
     * @param array $arLTO - array de LancamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function salvaPrevisaoPagamentoLancamento(array $arrTO)
    {
        try {


            foreach ($arrTO as $to) {

                if (!($to instanceof LancamentoTO)) {
                    THROW new Zend_Validate_Exception('É necessário informar um array de lancamentoTO!');
                }

                if (!$to->getId_lancamento()) {
                    THROW new Zend_Validate_Exception('Lançamento Não Identificado!');
                }

                $to->fetch(true, true, true);
                $lancamentoTO = new LancamentoTO();
                $lancamentoTO->setId_lancamento($to->getId_lancamento());
                $lancamentoTO->setId_entidade($to->getId_entidade());
                $lancamentoTO->setId_meiopagamento($to->getId_meiopagamento());
                $dataOpcao = $to->getDt_vencimento() ? $to->getDt_vencimento() : new Zend_Date(null);

                if ($lancamentoTO->getId_meiopagamento() == MeioPagamentoTO::CARTAO) {
                    $dt_prevquitado = ($to->getDt_prevquitado() ? $to->getDt_prevquitado() : $dataOpcao);
                    $lancamentoTO->setDt_prevquitado($dt_prevquitado ? $dt_prevquitado : new Zend_Date());
                }

                $mensageiro = $this->dao->editarLancamento($lancamentoTO);
            }

            $this->mensageiro->setMensageiro('Lançamento(s) Quitado(s) com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Quitar Lançamento(s)!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que edita os Lançamentos com os dados do Cheque
     * O parametro recebido pode ser um array de LançamentoTO ou LançamentoTO
     * @param array $arrLTO
     * @return Ead1_Mensageiro
     */
    public function salvarDadosChequeLancamento(array $arrLTO)
    {
        try {
            $this->dao->beginTransaction();
            if (is_array($arrLTO)) {
                foreach ($arrLTO as $index => $lTO) {
                    if (!($lTO instanceof LancamentoTO)) {
                        THROW new Zend_Exception('Um dos parametros recebidos no array não é uma instancia de LancamentoTO');
                    }
                    if (!$lTO->getId_lancamento()) {
                        THROW new Zend_Exception('Um dos Lançamentos não veio com o id_lançamento setado.');
                    }
                    if (!$this->comparaData(new Zend_Date(), $lTO->getDt_prevquitado())) {
                        THROW new Zend_Exception('A data de vencimento do cheque do lançamento é menor que a data atual.');
                    }
                    $lTO->setDt_emissao(date('Y-m-d'));
                    $this->validaDadosChequeLancamento($lTO);
                    $this->dao->editarLancamento($lTO);
                }
            } else {
                THROW new Zend_Exception('O parametro recebido não é um array de LancamentoTO');
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro('Cheques Salvos com Sucesso!');
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Dados do Cheque.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que valida os dados do cheque do lancamento
     * @param unknown_type $lTO
     * @throws Zend_Exception
     * @return Boolean
     */
    private function validaDadosChequeLancamento(LancamentoTO $lTO)
    {
        $erro = false;
        if (!$lTO->getDt_prevquitado()) {
            THROW new Zend_Exception('É Necessario Informar a Data de Vencimento.');
            $erro = true;
        }
        if (!$lTO->getSt_emissor()) {
            THROW new Zend_Exception('É Necessario Informar o Responsável.');
            $erro = true;
        }
        if (!$lTO->getSt_coddocumento()) {
            THROW new Zend_Exception('É Necessario Informar Número do Cheque.');
            $erro = true;
        }
        return $erro ? false : true;
    }

    /**
     * Metodo que retorna o Usuário e Venda
     * @param VwVendaUsuarioTO $vwVendaUsuarioTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwVendaUsuario(VwVendaUsuarioTO $vwVendaUsuarioTO)
    {
        try {
            $vendaProduto = $this->dao->retornarVwVendaUsuario($vwVendaUsuarioTO)->toArray();
            if (empty($vendaProduto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($vendaProduto, new VwVendaUsuarioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Usuário e Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna a Venda
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $to
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVenda(VendaTO $to)
    {
        try {
            $venda = $this->dao->retornarVenda($to);
            if (empty($venda)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro($venda, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Usuário e Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Forma de Pagamento com a Venda
     * @param VwFormaMeioPagamentoVendaTO $vwFormaMeioPagamentoVendaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwFormaMeioDivisaoVenda(VwFormaMeioDivisaoVendaTO $vwFormaMeioDivisaoVendaTO)
    {
        try {
            $meioPagamento = $this->dao->retornarVwFormaMeioDivisaoVenda($vwFormaMeioDivisaoVendaTO)->toArray();
            if (empty($meioPagamento)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($meioPagamento, new VwFormaMeioDivisaoVendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Formas de pagamentos de uma Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna produto taxa em projeto pedagogico
     * @param VwProdutoTaxaProjetoTO $vwProdutoTaxaProjetoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoTaxaProjeto(VwProdutoTaxaProjetoTO $vwProdutoTaxaProjetoTO)
    {
        try {
            $produtotaxa = $this->dao->retornarVwProdutoTaxaProjeto($vwProdutoTaxaProjetoTO)->toArray();
            if (empty($produtotaxa)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($produtotaxa, new VwProdutoTaxaProjetoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Produto Taxa de matrícula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os Produtos da Venda
     * @param VwProdutoVendaTO $vwProdutoVendaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoVenda(VwProdutoVendaTO $vwProdutoVendaTO)
    {
        try {
            $vendaProduto = $this->dao->retornarVwProdutoVenda($vwProdutoVendaTO)->toArray();
            if (empty($vendaProduto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($vendaProduto, new VwProdutoVendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Produtos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os Produtos da Venda de um Livro
     * @param VwProdutoVendaLivroTO $vwProdutoVendaLivroTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoVendaLivro(VwProdutoVendaLivroTO $vwProdutoVendaLivroTO)
    {
        try {

            $vendaProduto = $this->dao->retornarVwProdutoVendaLivro($vwProdutoVendaLivroTO)->toArray();
            if (empty($vendaProduto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Vínculo de Livro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($vendaProduto, new VwProdutoVendaLivroTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Produtos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Retorna os campos para montar o combo de campanhas
     * @param CampanhaVendaProdutoTO $cvpTO
     * @return Ead1_Mensageiro
     */
    public function retornaSpCampanhaVendaProduto(SpCampanhaVendaProdutoTO $cvpTO)
    {
        try {
            if (!$cvpTO->getId_entidade()) {
                $cvpTO->setId_entidade($cvpTO->getSessao()->id_entidade);
            }
            $retornoVendaProduto = $this->dao->retornaSpCampanhaVendaProduto($cvpTO);
            if (empty($retornoVendaProduto)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::SUCESSO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retornoVendaProduto, new SpCampanhaVendaProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Produtos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Retorna as vendas de um cliente
     * @param VwClienteVendaTO $vwCVTO
     * @return Ead1_Mensageiro
     */
    public function retornarClienteVenda(VwClienteVendaTO $vwCVTO)
    {
        try {
            $retorno = $this->dao->retornarClienteVenda($vwCVTO);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwClienteVendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) cliente(s) da venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Retorna os lancamentos de uma venda
     * @param VwVendaLancamentoTO $vlTO
     * @return Ead1_Mensageiro
     */
    public function retornarVendaLancamento(VwVendaLancamentoTO $vlTO, $where = null)
    {
        try {
            if (!$vlTO->getId_entidade()) {
                $vlTO->setId_entidade($vlTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVendaLancamento($vlTO, $where);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendaLancamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) lancamento(s) da venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Retorna os lancamentos de uma venda
     * @param VwVendaLancamentoTO $vlTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwVendaLancamentoQuitados(VwVendaLancamentoQuitadosTO $vlTO, $where = null)
    {
        try {
            if (!$vlTO->getId_entidade()) {
                $vlTO->setId_entidade($vlTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVwVendaLancamentoQuitados($vlTO, $where);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::SUCESSO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendaLancamentoQuitadosTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) lancamento(s) da venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *
     * Retorna os lancamentos de uma venda que está sendo renegociada
     * @param VwVendaLancamentoTO $vlTO , array $arrayDatas
     * @return Ead1_Mensageiro
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function retornarVendaLancamentoRenegociacao(VwVendaLancamentoTO $vlTO, array $datasPesquisa = null)
    {
        try {

            $where = null;

            $id_venda = $vlTO->getId_venda();
            $st_nomecompleto = $vlTO->getSt_nomecompleto();
            $bl_temFiltroData = false;

            foreach ($datasPesquisa as $data) {
                if (isset($data) && $data != null && !empty($data)) {
                    $bl_temFiltroData = true;
                    break;
                }
            }

            if (!empty($id_venda) || !empty($st_nomecompleto) || $bl_temFiltroData) {

                if ($datasPesquisa) {

                    $where = array();

                    foreach ($datasPesquisa as $index => $dataFiltro) {
                        if ($index == 'dt_inicioCadastro') {
                            if ($dataFiltro != null) {
                                $dtInicioCadastro = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtInicioCadastro)) {
                                    $where [] = 'dt_cadastro >= \'' . $dtInicioCadastro->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_inicioCadastro não é uma data válida!');
                                }
                            }
                        }

                        if ($index == 'dt_fimCadastro') {
                            if ($dataFiltro != null) {
                                $dtFimCadastro = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtFimCadastro)) {
                                    $where [] = 'dt_cadastro >= \'' . $dtFimCadastro->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_fimCadastro não é uma data válida!');
                                }
                            }
                        }

                        if ($index == 'dt_inicioVencimento') {
                            if ($dataFiltro != null) {
                                $dtInicioVencimento = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtInicioVencimento)) {
                                    $where [] = 'dt_vencimento >= \'' . $dtInicioVencimento->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_inicioVencimento não é uma data válida!');
                                }
                            }
                        }

                        if ($index == 'dt_fimVencimento') {
                            if ($dataFiltro != null) {
                                $dtFimVencimento = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtFimVencimento)) {
                                    $where [] = 'dt_vencimento <= \'' . $dtFimVencimento->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_fimVencimento não é uma data válida!');
                                }
                            }
                        }
                    }
                }

                if (isset($dtInicioCadastro) && isset($dtFimCadastro) && $dtInicioCadastro->isLater($dtFimCadastro)) {
                    throw new Zend_Date_Exception('Data de Inicio do Cadastro não pode ser maior que a data Fim');
                }

                if (isset($dtInicioVencimento) && isset($dtFimVencimento) && $dtInicioVencimento->isLater($dtFimVencimento)) {
                    throw new Zend_Date_Exception('Data de Inicio do Vencimento não pode ser maior que a data Fim');
                }
            } else {
                throw new Zend_Exception("Necessário informar pelo menos um dos parâmetros: Usuário, Venda ou Data de Vencimento.");
            }

            if (!$vlTO->getId_entidade()) {
                $vlTO->setId_entidade($vlTO->getSessao()->id_entidade);
            }

            $where[] = 'bl_ativo = 1';
            $retorno = $this->dao->retornarVendaLancamentoRenegociacao($vlTO, $where);
            $retorno = $retorno->toArray();

            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro("Nenhum registro encontrado!", Ead1_IMensageiro::AVISO);
            }

            $arrVwVendaLancamentoTO = Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendaLancamentoTO());

            foreach ($arrVwVendaLancamentoTO as $vwVendaLancamentoTO) {
                $id_lancamento = $vwVendaLancamentoTO->getId_lancamento();
                $lancamentoTO = new LancamentoTO();
                $lancamentoTO->setId_lancamento($id_lancamento);
                $nu_valoratualizado = $this->retornarLancamentoValorAtualizado($lancamentoTO)->getFirstMensagem()->getNu_valor();
                $vwVendaLancamentoTO->setNu_valoratualizado($nu_valoratualizado);
            }

            //Zend_Debug::dump($this->mensageiro->setMensageiro ( Ead1_TO_Dinamico::encapsularTo ( $retorno, new VwVendaLancamentoTO () ), Ead1_IMensageiro::SUCESSO ),__CLASS__.'('.__LINE__.')'); exit;
            return $this->mensageiro->setMensageiro($arrVwVendaLancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Date_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Erro ao validar datas!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Erro ao retornar o(s) lancamento(s) da venda!');
        }
    }

    /**
     *
     * Retorna os vendas para processo de ativação automática.
     * @param VwVendasRecebimentoTO $vwVendasRecebimentoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwVendasRecebimento(VwVendasRecebimentoTO $vwVendasRecebimentoTO)
    {
        try {

            $retorno = $this->dao->retornarVwVendasRecebimento($vwVendasRecebimentoTO);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::SUCESSO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendasRecebimentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Vendas de recebimento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *  Retorna a venda com o produto de um determinado usuário
     * @param VwUsuarioVendaProdutoTO $vlTO
     * @return Ead1_Mensageiro
     */
    public function retornarUsuarioVendaProduto(VwUsuarioVendaProdutoTO $uvpTO)
    {
        try {
            if (!$uvpTO->getId_entidade()) {
                $uvpTO->setId_entidade($uvpTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarUsuarioVendaProduto($uvpTO);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwUsuarioVendaProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) produto(s) do usuário da venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     *  Retorna o resumo financeiro da venda
     * @param VwResumoFinanceiroTO $rfTO
     * @return Ead1_Mensageiro
     */
    public function retornarResumoFinanceiroVenda(VwResumoFinanceiroTO $rfTO)
    {
        try {
            if (!$rfTO->getId_entidade()) {
                $rfTO->setId_entidade($rfTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarResumoFinanceiroVenda($rfTO);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            foreach ($retorno as $index => $resumo) {
                $lancamentoTO = new LancamentoTO();
                $lancamentoTO->setId_lancamento($resumo['id_lancamento']);
                $this->retornarLancamentoValorAtualizado($lancamentoTO);
                if ($this->mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return $this->mensageiro;
                }
                $retorno[$index]['nu_valoratualizado'] = $this->mensageiro->mensagem[0]->getNu_valor();
                $retorno[$index]['dt_vencimento'] = \DateTime::createFromFormat('Y-m-d', $resumo['dt_vencimento']);
            }

            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwResumoFinanceiroTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Resumo de Lançamento da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o lançamento com o valor atualizado (com valor da multa e aplicação de juros)
     * @param LancamentoTO $lancamentoTO
     * @return Ead1_Mensageiro
     */
    public function retornarLancamentoValorAtualizado(LancamentoTO $lancamentoTO)
    {
        try {
            if (!$lancamentoTO->getId_lancamento()) {
                THROW new Zend_Exception('O id_lancamento não veio setado!');
            }
            $vwVendaLancamentoTO = new VwVendaLancamentoTO();
            $vwVendaLancamentoTO->setId_lancamento($lancamentoTO->getId_lancamento());
            $lancamento = $this->dao->retornarVendaLancamento($vwVendaLancamentoTO)->toArray();
            if (empty($lancamento)) {
                THROW new Zend_Validate_Exception('Nenhum Lançamento Encontrado!');
            }

            /**
             * ->format('w')
             * w    Numeric representation of the day of the week    0 (for Sunday) through 6 (for Saturday)
             *
             * 0 - Sunday
             * 1 - Monday
             * 2 - Tuesday
             * 3 - Wednesday
             * 4 - Thursday
             * 5 - Friday
             * 6 - Saturday
             */
            foreach ($lancamento as &$lanc) {
                $data_vencimentoreal = \DateTime::createFromFormat('Y-m-d', $lanc['dt_vencimento']);

                if ($data_vencimentoreal->format('w') == 0) {
                    $data_vencimentoreal = $data_vencimentoreal->modify('+1 day');
                } elseif ($data_vencimentoreal->format('w') == 6) {
                    $data_vencimentoreal = $data_vencimentoreal->modify('+2 day');
                }
                $aux = new \Zend_Date($data_vencimentoreal->format('Y-m-d H:i:s'), \Zend_Date::ISO_8601);
                $today = new Zend_Date(Zend_Date::DATE_SHORT);
                //a segunda mais proxima é uma data maior ou igual a de hoje?
                if (!$this->comparaData($aux, $today)) {
                    $lanc['dt_vencimento'] = $data_vencimentoreal->format('Y-m-d');
                    $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['dt_novo_vencimento'] = $aux;
                }
            }
            /** @var VwVendaLancamentoTO $vwVendaLancamentoTO */
            $vwVendaLancamentoTO = Ead1_TO_Dinamico::encapsularTo($lancamento, new VwVendaLancamentoTO(), true);
            $data_vencimento = new Zend_Date($vwVendaLancamentoTO->getDt_vencimento(), Zend_Date::DATE_SHORT);
            $data_novo_vencimento = new Zend_Date(Zend_Date::DATE_SHORT);
            $lancamentoTO->setNu_valor($vwVendaLancamentoTO->getNu_valor());
            $lancamentoTO->setDt_vencimento($vwVendaLancamentoTO->getDt_vencimento());
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['porcentagem_juros'] = $vwVendaLancamentoTO->getNu_juros();
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['dias_atraso'] = 0;
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['total_juros'] = 0;
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_multa'] = $vwVendaLancamentoTO->getnu_multaatraso();
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['total_multa'] = (($vwVendaLancamentoTO->getNu_valor() * $vwVendaLancamentoTO->getnu_multaatraso()) / 100);
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['juros_dia'] = ($vwVendaLancamentoTO->getnu_jurosatraso() * $vwVendaLancamentoTO->getNu_valor() / 100);
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_pontualidade'] = 0;
            $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['bl_original'] = $vwVendaLancamentoTO->getBl_original();

            if (!$vwVendaLancamentoTO->getBl_quitado()) {
                if ($this->comparaData($data_vencimento, $data_novo_vencimento)) {
                    $this->aplicarJurosMultaAtraso($vwVendaLancamentoTO, $lancamentoTO);
                } else {
                    //busca os dados do desconto pela campanha de pontualidade
                    $campanhaNegocio = new \G2\Negocio\CampanhaComercial();
                    $pontualiadeCampanha = $campanhaNegocio->retornarCampanhaPontualidadeByVenda($vwVendaLancamentoTO->getId_venda());
                    $valorDesconto = 0;//seta o valor do desconto como zero
                    //verifica se retornou algo
                    if ($pontualiadeCampanha->getType() == Ead1_IMensageiro::TYPE_SUCESSO) {
                        $desconto = $pontualiadeCampanha->getFirstMensagem();//separa os dados do mensageiro

                        //verifica se o desconto é valor ou porcentagem
                        if ($desconto['tipo_desconto']['id'] == \G2\Constante\TipoDesconto::VALOR) {
                            $valorDesconto = $vwVendaLancamentoTO->getNu_valor() - floatval($desconto['valor_desconto']);//subtrai o desconto do valor
                        } elseif ($desconto['tipo_desconto']['id'] == \G2\Constante\TipoDesconto::PORCENTAGEM) {
                            //calcula a porcetagem de desconto no valor
                            $valorDesconto = $vwVendaLancamentoTO->getNu_valor() * (floatval($desconto['valor_desconto']) / 100);
                        }

                    }
                    //seta a posição no array com o desconto
                    $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_pontualidade'] = $valorDesconto;
//                  $this->aplicarCampanhaPontualidade($vwVendaLancamentoTO, $lancamentoTO);
                }
            }
            $lancamentoTO->setBl_quitado($vwVendaLancamentoTO->getBl_quitado());
            $lancamentoTO->setDt_emissao($vwVendaLancamentoTO->getDt_emissao());
            $lancamentoTO->setDt_prevquitado($vwVendaLancamentoTO->getDt_prevquitado());
            $lancamentoTO->setDt_quitado($vwVendaLancamentoTO->getDt_quitado());
            $lancamentoTO->setId_entidade($vwVendaLancamentoTO->getId_entidade());
            $lancamentoTO->setId_meiopagamento($vwVendaLancamentoTO->getId_meiopagamento());
            $lancamentoTO->setId_tipolancamento($vwVendaLancamentoTO->getId_tipolancamento());
            $lancamentoTO->setId_usuariolancamento($vwVendaLancamentoTO->getId_usuariolancamento());
            $lancamentoTO->setNu_vencimento($vwVendaLancamentoTO->getNu_vencimento());
            $lancamentoTO->setSt_banco($vwVendaLancamentoTO->getSt_banco());
            $lancamentoTO->setSt_coddocumento($vwVendaLancamentoTO->getSt_coddocumento());
            $lancamentoTO->setSt_emissor($vwVendaLancamentoTO->getSt_emissor());
            $this->mensageiro->setMensageiro($lancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Aplica os Juros para uma previsualização - não tem retorno
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwVendaLancamentoTO $vwVendaLancamentoTO
     * @return void
     * @see $this->retornarLancamentoValorAtualizado()
     */
    private function aplicarJurosMultaAtraso(VwVendaLancamentoTO $vwVendaLancamentoTO, LancamentoTO $lancamentoTO)
    {


        $data_vencimento = new Zend_Date($vwVendaLancamentoTO->getDt_vencimento(), Zend_Date::DATE_SHORT);
        $data_novo_vencimento = new Zend_Date(Zend_Date::DATE_SHORT);
        $dias_atraso = $this->diferencaDias($data_vencimento, $data_novo_vencimento);
        $valor_lancamento = $vwVendaLancamentoTO->getNu_valor();
        $valor_multa = (($valor_lancamento * $vwVendaLancamentoTO->getnu_multaatraso()) / 100);
        $juros_dia = ($vwVendaLancamentoTO->getnu_jurosatraso() * $valor_lancamento / 100);
        $valor_juros = ($juros_dia * $dias_atraso);
        $valor_lancamento_atualizado = ($valor_lancamento + $valor_juros + $valor_multa);

        // Valida o dia corrente
        $aux_zdate = new Zend_Date(Zend_Date::now(), Zend_Date::DATE_SHORT);
        $data_agora = trim(substr($aux_zdate->toString(), 0, 10));
        $aux_data_vencimento = trim(substr($data_vencimento->toString(), 0, 10));
        if ($data_agora == $aux_data_vencimento)
            $valor_lancamento_atualizado = $valor_lancamento;

        $lancamentoTO->setNu_quitado($valor_lancamento_atualizado);
        $lancamentoTO->setDt_vencimento($data_vencimento);

        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['juros_dia'] = $juros_dia;
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['porcentagem_juros'] = $vwVendaLancamentoTO->getNu_juros();
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['dias_atraso'] = $dias_atraso;
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['total_juros'] = $valor_juros;
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_multa'] = $vwVendaLancamentoTO->getnu_multaatraso();
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['total_multa'] = $valor_multa;
        $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['dt_novo_vencimento'] = $data_novo_vencimento;
        $lancamentoTO->setNu_valor($valor_lancamento_atualizado);
    }

    /**
     * Aplica a campanha de Pontualidade "TipoCampanhaTO::PONTUALIDADE"
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwVendaLancamentoTO $lanTO
     * @see $this->retornarLancamentoValorAtualizado()
     */
    private function aplicarCampanhaPontualidade(VwVendaLancamentoTO $vwVendaLancamentoTO, LancamentoTO $lancamentoTO)
    {
        $cvpTO = new SpCampanhaVendaProdutoTO();
        $cvpTO->setId_entidade($vwVendaLancamentoTO->getId_entidade());
        $cvpTO->setId_tipocampanha(TipoCampanhaTO::PONTUALIDADE);
        $cvpTO->setId_formapagamento($vwVendaLancamentoTO->getId_formapagamento());
        $cvpTO->setId_venda($vwVendaLancamentoTO->getid_venda());
        $mensPontualidade = $this->retornaSpCampanhaVendaProduto($cvpTO);

        if ($mensPontualidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $arrPontualidade = $mensPontualidade->getMensagem();
            foreach ($arrPontualidade as $pontualidade) {
                $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_pontualidade'] += $pontualidade->getnu_valormin();
            }
            $lancamentoTO->setNu_valor($vwVendaLancamentoTO->getNu_valor() - $this->_arrInfoLancamentos[$vwVendaLancamentoTO->getId_lancamento()]['valor_pontualidade']);
        }
    }

    /**
     * Metodo que retorna os meios de pagamento do resumo da venda
     * @param VwResumoFinanceiroTO $vwRfTO
     * @return Ead1_Mensageiro
     */
    public function retornarMeioPagamentoResumoVenda(VwResumoFinanceiroTO $vwRfTO)
    {
        try {
            if (!$vwRfTO->getId_venda()) {
                THROW new Zend_Exception('É necessario enviar o id da venda no TO');
            }
            $resumo = $this->dao->retornarResumoFinanceiroVenda($vwRfTO)->toArray();
            if (empty($resumo)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrMeioPagamento = array();
            foreach ($resumo as $meioPagamento) {
                $arrMeioPagamento[$meioPagamento['id_meiopagamento']]['id_meiopagamento'] = $meioPagamento['id_meiopagamento'];
                $arrMeioPagamento[$meioPagamento['id_meiopagamento']]['st_meiopagamento'] = $meioPagamento['st_meiopagamento'];
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrMeioPagamento, new MeioPagamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Meios de Pagamento do Resumo da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o envolvido da venda
     * @param VendaEnvolvidoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVendaEnvolvido(VendaEnvolvidoTO $to)
    {
        try {
            $dados = $this->dao->retornarVendaEnvolvido($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VendaEnvolvidoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Envolvidos da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna as Vendas Pendentes de uma Pessoa
     * @param VwVendaUsuarioTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVendaPendentesPessoa(VwVendaUsuarioTO $to)
    {
        try {
            $produtoDAO = new ProdutoDAO();
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $EVOLUCAO_CONFIRMADA = 10;
            $where = 'id_entidade = ' . $to->getId_entidade() .
                ' AND id_evolucao != ' . $EVOLUCAO_CONFIRMADA .
                ($to->getId_venda() ? ' AND id_venda = ' . $to->getId_venda() : '') .
                ($to->getId_usuario() ? ' AND id_usuario = ' . $to->getId_usuario() : '');
            $dados = $produtoDAO->retornarVendaUsuario($to, $where)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwVendaUsuarioTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Vendas Pendentes!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    public function processoAtivacaoAutomatica(VendaTO $vendaTO)
    {
        /**
         * Setando novo atributo criado para evitar criacao duplicada de mensagens
         */
        $orm = new \VendaORM();
        $vendaTO->setId_evolucao(\VendaTO::EVOLUCAO_PROCESSAMENTO);
        $orm->update($vendaTO->toArrayUpdate(FALSE, array('id_venda')), array('id_venda = ' . $vendaTO->getId_venda()));

        $matriculaBO = new MatriculaBO();
        $mensageiro = $this->confirmaRecebimento($vendaTO);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
            return $mensageiro;
        }


        $contratoTO = new ContratoTO();
        $contratoTO->setId_venda($vendaTO->getId_venda());
        $contratoTO->setId_entidade($vendaTO->getId_entidade());
        $contratoTO = $matriculaBO->retornarContrato($contratoTO)->getFirstMensagem();

        //verifica se retornou o contrato
        if ($contratoTO instanceof ContratoTO) {
            $mensageiro = $matriculaBO->ativarMatricula($contratoTO);

            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                $vTO = new VendaTO();
                $vTO->setId_venda($vendaTO->getId_venda());
                $vTO->setId_situacao(VendaTO::SITUACAO_VENDA_PENDENTE);
                $vTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
                $vTO->setDt_confirmacao(null);
                $produtoDAO = new ProdutoDAO();
                $produtoDAO->editarVenda($vTO);

                //metodo criado para substituir o registro de tramite utilizado na ProdutoDAO.php

                $negocioVenda = new \G2\Negocio\Venda();
                $parametrosEv['evolucao'] = array(
                    'id_venda' => $vendaTO->getId_venda(),
                    'id_evolucao' => VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO
                );

                $parametrosSit['situacao'] = array(
                    'id_venda' => $vendaTO->getId_venda(),
                    'id_situacao' => VendaTO::SITUACAO_VENDA_PENDENTE
                );

                $negocioVenda->registraTramite($parametrosEv);
                $negocioVenda->registraTramite($parametrosSit);

            } else {
                //metodo criado para substituir o registro de tramite utilizado na ProdutoDAO.php
                $negocioVenda = new \G2\Negocio\Venda();
                $parametrosEv['evolucao'] = array(
                    'id_venda' => $vendaTO->getId_venda(),
                    'id_evolucao' => VendaTO::EVOLUCAO_CONFIRMADA
                );

                $parametrosSit['situacao'] = array(
                    'id_venda' => $vendaTO->getId_venda(),
                    'id_situacao' => VendaTO::SITUACAO_VENDA_ATIVA
                );

                $ev = $negocioVenda->registraTramite($parametrosEv);
                $sit = $negocioVenda->registraTramite($parametrosSit);


                return $this->mensageiro->setMensageiro('Matrícula ativada e recebimento confirmado com sucesso!', Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return $this->mensageiro->setMensageiro('Recebimento confirmado com sucesso!', Ead1_IMensageiro::SUCESSO);
        }
        return $mensageiro;
    }

    /**
     * Retorna os objetos da view vw_vendalancamentoatrasado
     * @param VwVendaLancamentoAtrasadoTO $to
     * @param unknown_type $where
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwVendaLancamentoAtrasado(VwVendaLancamentoAtrasadoTO $to, array $datas = null, $where = null)
    {
        try {

            $where = " ( st_cpf like '%{$to->getSt_cpf()}%' OR st_nomecompleto like '%{$to->getSt_nomecompleto()}%' OR st_email like '%{$to->getSt_email()}%' ) ";

            if ($datas) {

                if ($datas['dt_inicio']) {
                    $data = new Zend_Date($datas['dt_inicio'], 'd/m/Y');
                    $where .= " AND dt_vencimento >= '{$data->toString('Y-m-d')}' ";
                }

                if ($datas['dt_termino']) {
                    $data = new Zend_Date($datas['dt_termino'], 'd/m/Y');
                    $where .= " AND dt_vencimento <= '{$data->toString('Y-m-d')}' ";
                }
            }

            $where .= " AND id_entidade = {$to->getSessao()->id_entidade} ";
            //$where .=  " AND id_usuario = {$to->getSessao()->id_usuario} ";
            $where .= ($to->getId_venda() ? " AND id_venda = {$to->getId_venda()} " : "");
            $where .= ($to->getNu_diasatraso() ? " AND nu_diasatraso >= {$to->getNu_diasatraso()} " : "");
            $where .= ($to->getId_situacaolancamento() ? " AND id_situacaolancamento = {$to->getId_situacaolancamento()} " : "");
            //Zend_Debug::dump($where,__CLASS__.'('.__LINE__.')'); exit;
            $retorno = $this->dao->retornarVwVendaLancamentoAtrasado($to, $where);
            $retorno = $retorno->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro(array(), Ead1_IMensageiro::SUCESSO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendaLancamentoAtrasadoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar o(s) lancamento(s) em atraso da venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna o proximo número da ordem dos lancamentos na venda
     * @param unknown_type $id_venda
     * @return Ead1_Mensageiro
     */
    public function retornarOrdemVendaLancamento($id_venda)
    {

        try {
            $retorno = $this->dao->retornarOrdemVendaLancamento($id_venda);
            $retorno = $retorno->toArray();
            return $this->mensageiro->setMensageiro($retorno['nu_ordem'], Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar a ordem do lancamento na venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna true se o objeto foi atualizado ou não
     * @param unknown_type $id_venda
     * @return Ead1_Mensageiro
     */
    public function retornarAfiliadoAtualizado(VendaTO $to)
    {
        try {
            $this->dao->editarObjetoAfiliado($to, 'id_venda = ' . $to->getId_venda());
            return true;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Salvar a Integração Afiliado da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
            return false;
        }
    }

    public function pesquisarCombo($id_combo)
    {
        try {
            return $this->dao->itensDoCombo($id_combo);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao pesquisar os itens do combo!', Ead1_IMensageiro::ERRO, $e->getMessage());
            return false;
        }
    }

    /**
     * @param $id_campanhacomercial
     * @return bool
     * Retorna o premio referente a campanha comercial
     */

    public function retornaPremio($id_campanhacomercial, $id_venda)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $campanhacomercial = $negocio->findBy('\G2\Entity\CampanhaComercial', $id_campanhacomercial);
            $premio = $negocio->findBy('\G2\Entity\Premio', $campanhacomercial->getId_premio());
            if ($premio->getId_tipopremio() == 2) {

            }
        } catch (\Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar prêmio!', Ead1_IMensageiro::ERRO, $e->getMessage());
            return false;
        }
    }

    /**
     * Retorna o id_vendaaditivo depois de ser salvo na tabela. Caso contrario retorna false.
     *
     * @params $objContratoMatricula,
     * @params $matriculaorigem,
     * @params $matriculadestino,
     * @params $vendaAditivo,
     * @params $id_venda
     * @params $bl_gerarArquivo - Marca se deve gerar o arquivo no momento ou posteriormente
     * @return int $id_vendaaditivo
     */
    public function salvarAditivoTransferencia($objContratoMatricula, $matriculaorigem, $matriculadestino, $vendaAditivo, $id_venda, $bl_gerarArquivo = false)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $contratoTo = new \ContratoTO();
            $contratoTo->setId_contrato($objContratoMatricula->getId_contrato());
            $contratoTo->fetch(false, true, true);

            $vendaAditivoTO = new \VendaAditivoTO();
            $vendaAditivoTO->setId_venda($id_venda ? $id_venda : $contratoTo->id_venda);
            $vendaAditivoTO->setId_matricula($matriculadestino);
            $vendaAditivoTO->setId_usuario($contratoTo->id_usuario);
            $vendaAditivoTO->setDt_cadastro(new \DateTime());
            if (array_key_exists("nu_valorproporcional", $vendaAditivo)) {
                $vendaAditivoTO->setNu_valorproporcional((float)$vendaAditivo['nu_valorproporcional']);
            }


            if (is_array($vendaAditivo) && !empty($vendaAditivo)) {
                //atribui valores disponiveis tanto para restituição, quanto para negociação financeira
                $vendaAditivoTO->setNu_valorapagar((float)$vendaAditivo['nu_valorapagar']);
                $vendaAditivoTO->setSt_observacao($vendaAditivo['st_observacao']);
                $vendaAditivoTO->setNu_valorcursoorigem((!empty($vendaAditivo['nu_valorliquido']) ? (float)$vendaAditivo['nu_valorliquido'] : null));
                $vendaAditivoTO->setNu_valortransferencia(!empty($vendaAditivo['nu_valortransferencia'])?
                    (float)$vendaAditivo['nu_valortransferencia']:0);

                if (array_key_exists('bl_cancelamento', $vendaAditivo)) {
                    $vendaAditivoTO->setBl_cancelamento(true);
                }

                //Valores setados somente se o valor foi positivo (Negociação Financeira)
                if ((float)$vendaAditivo['nu_valorapagar'] > 0) {
                    $vendaAditivoTO->setId_meiopagamento((int)$vendaAditivo['id_meiopagamento']);

                    if (array_key_exists('id_campanhacomercial', $vendaAditivo)) {
                        $vendaAditivoTO->setId_campanhacomercial((int)$vendaAditivo['id_campanhacomercial']);
                    }

                    $vendaAditivoTO->setNu_quantidadeparcelas((int)$vendaAditivo['nu_quantidadeparcelas']);
                    $vendaAditivoTO->setDt_primeiraparcela($negocio->converterData($vendaAditivo['dt_primeiraparcela'], 'datetime'));
                    $vendaAditivoTO->setId_atendente((int)$vendaAditivo['id_atendente']);
                }
            }

            //Cadastra Venda Aditivo
            $id_vendaaditivo = $this->dao->cadastrarVendaAditivo($vendaAditivoTO);

            if (!$id_vendaaditivo) {
                return false;
            } elseif ($bl_gerarArquivo) {
                //gera o Aditivo e salva na pasta do aluno se for criada a venda aditivo
                $matriculaBO = new \MatriculaBO();
                $matriculaBO->gerarAditivo($contratoTo, array('id_matricula' => $matriculadestino), \G2\Constante\TipoAditivo::TRANSFERENCIA, $matriculaorigem);

                return $id_vendaaditivo;
            } else {
                return $id_vendaaditivo;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function baixarProdutoEstoque($vTO)
    {
        try {
            //pesquisa o produto da venda
            $vwProdutoVendaTO = new VwProdutoVendaTO();
            $vwProdutoVendaTO->setId_venda($vTO->getId_venda());
            $vwProdutoVendaTO->fetch(false, true, true);

            //pesquisa o produto para pegar a quantidade do estoque
            $produtoTO = new ProdutoTO();
            $produtoTO->setId_produto($vwProdutoVendaTO->getId_produto());
            $produtoTO->fetch(true, true, true);

            //decrementa só se for do tipo livro e quando for maior que 0
            if ($produtoTO->getId_tipoproduto() == \G2\Constante\TipoProduto::LIVRO
                && $produtoTO->getNu_estoque() > 0
            )
                $produtoTO->setNu_estoque($produtoTO->getNu_estoque() - 1);

            //edita o produto decrementando uma unidade no estoque
            $produtoDAO = new ProdutoDAO();
            return $produtoDAO->editarProduto($produtoTO);

        } catch (\Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao baixar produto no estoque!', Ead1_IMensageiro::ERRO, $e->getMessage());
            return false;
        }
    }
}

<?php

/**
 * Classe com regras de negócio para WebService, acessível em qualquer camada...
 * Controller, BO, DAO, ORM, TO
 * @author edermariano
 *
 */
class WsBO extends Ead1_BO
{

    /**
     * @var Zend_Rest_Client
     */
    protected $clientRest;
    /** @var  EntidadeIntegracaoTO */
    protected $entidadeIntegracaoTO;

    /**
     * Método construtor
     * @param mixed $wsLink Link do Client Rest
     */
    public function __construct($wsLink = null)
    {
        parent::__construct();
        if ($wsLink) {
            $this->setClientRest($wsLink);
        }
    }

    /**
     * Método que seta o client Rest
     * @param String $wsLink
     */
    public function setClientRest($wsLink)
    {

        $this->clientRest = new Zend_Rest_Client($wsLink);
        $httpClient = $this->clientRest->getHttpClient();
        $httpClient->setConfig(array("timeout" => 720));

    }

    /**
     * Método que pega um Objeto e converte em Array
     * @param object
     */
    public static function object_to_array($object)
    {
        $new = NULL;
        if (is_object($object)) {
            $object = (array)$object;
        }

        if (is_array($object)) {
            $new = array();
            foreach ($object as $key => $val) {
                $key = preg_replace("/^\\0(.*)\\0/", "", $key);
                $key = str_replace("key_", "", $key);
                $new[$key] = self::object_to_array($val);
            }
        } else {
            $new = $object;
        }
        return $new;
    }

    /**
     * Return the Rest Status Result Success
     */
    final public function REST_STATUS_SUCCESS()
    {
        return 'success';
    }

    /**
     * Método para cadastrar matrícula no WebService com o sistema Actor
     * @param AlocacaoTO $alocacaoTO
     */
    public function cadastrarMatricula(AlocacaoTO $alocacaoTO)
    {

        $vwIntegracaoAlocacaoTO = $this->pesquisarVwAlocacaoIntegracao($alocacaoTO);

        if (!$vwIntegracaoAlocacaoTO) {
            return new Ead1_Mensageiro("Erro ao buscar dados de Alocação Integração.", Ead1_IMensageiro::ERRO);
        }

        $usuarioTO = new UsuarioTO();
        $usuarioTO->setId_usuario($vwIntegracaoAlocacaoTO->getId_usuario());

        if (!$vwIntegracaoAlocacaoTO->getSt_codusuario()) {

            $wsBO = new WsBO(Ead1_Ambiente::geral()->wsURL . 'services/pessoa.php');
            $respostaWsPessoa = $wsBO->cadastrarPessoa($usuarioTO);

            if ($respostaWsPessoa instanceof Ead1_Mensageiro) {
                return $respostaWsPessoa;
            } else {
                return $this->cadastrarMatricula($alocacaoTO);
            }
        }

        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
            if ($entidadeIntegracaoTO) {
                $this->clientRest->inserirmatricula()
                    ->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
                    ->webservicechave($entidadeIntegracaoTO->getSt_codchave())
                    ->dados(array(
                        'codpessoa' => $vwIntegracaoAlocacaoTO->getSt_codusuario()
                    , 'codcurso' => $vwIntegracaoAlocacaoTO->getSt_codsistemacurso()
                    , 'codcursoreferencia' => $vwIntegracaoAlocacaoTO->getSt_codsistemareferencia()
                    ));
                $dados = $this->clientRest->post()->inserirmatricula;
                if (empty($dados->retorno)) {

                    $retorno = 'Matricula Cadastrada, mas erro na chamada do Serviço!';
                    if (!empty($dados->key_0->mensagem)) {
                        $retorno .= $dados->key_0->mensagem;
                    }
                    throw new Exception($retorno, Ead1_IMensageiro::ERRO);

                } else {
                    if ($dados->retorno == "erro") {
                        throw new Exception("WS: {$dados->mensagem}", Ead1_IMensageiro::AVISO);
                    } else {
                        $this->cadastrarDadosIntegracaoMatricula($dados, $vwIntegracaoAlocacaoTO);
                    }
                }
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro na chamada do Serviço!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $dados->matricula->codmatricula;
    }

    /**
     * Método que pesquisa e retorna dados da VwAlocacaoIntegracao
     * @param AlocacaoTO $alocacaoTO
     * @return VwAlocacaoIntegracaoTO
     */
    private function pesquisarVwAlocacaoIntegracao(AlocacaoTO $alocacaoTO)
    {
        $vwAlocacaoIntegracaoTO = new VwAlocacaoIntegracaoTO();
        $vwAlocacaoIntegracaoTO->setId_alocacao($alocacaoTO->getId_alocacao());

        $vwAlocacaoIntegracaoORM = new VwAlocacaoIntegracaoORM();
        return $vwAlocacaoIntegracaoORM->consulta($vwAlocacaoIntegracaoTO, false, true);
    }

    /**
     * Método para cadastrar pessoa no WebService com o sistema Actor
     * @param UsuarioTO $usuarioTO
     */
    public function cadastrarPessoa(UsuarioTO $usuarioTO)
    {
        $usuarioTO = $this->pesquisarUsuarioTO($usuarioTO);
        $usuarioIntegracaoTO = $this->pesquisarUsuarioIntegracaoTO($usuarioTO);
        if ($usuarioIntegracaoTO) {
            return $this->editarPessoa($usuarioIntegracaoTO);
        }

        $pessoaDao = new PessoaDAO();
        $emails = $pessoaDao->retornaEmailPerfil($usuarioTO->getId_usuario(), $usuarioTO->getSessao()->id_entidade);

        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);

            if (!$entidadeIntegracaoTO) {
                throw new Exception("Não existe interface de integração para a entidade em questão!", 0);
            }
            $this->clientRest->inserirpessoa()
                ->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
                ->webservicechave($entidadeIntegracaoTO->getSt_codchave())
                ->dados(
                    array('nompessoa' => $usuarioTO->getSt_nomecompleto()
                    , 'numcpf' => $usuarioTO->getSt_cpf()
                    , 'desemail' => $emails->st_email
                    )
                );
            $dados = $this->clientRest->post()->inserirpessoa;

            if (empty($dados->retorno)) {

                $retorno = 'Pessoa Cadastrada, mas erro na chamada do Serviço!';
                if (!empty($dados->key_0->mensagem)) {
                    $retorno .= $dados->key_0->mensagem;
                }
                return new Ead1_Mensageiro($retorno, Ead1_IMensageiro::ERRO, $dados);

            } else {
                $this->cadastrarDadosIntegracaoPessoa($dados, $usuarioTO);
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro na chamada do Serviço!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $dados->pessoa->codpessoa;
    }

    /**
     * Método que pesquisa e retorna dados da UsuarioORM
     * @param UsuarioTO $usuarioTO
     * @return UsuarioTO
     */
    private function pesquisarUsuarioTO(UsuarioTO $usuarioTO)
    {
        $usuarioORM = new UsuarioORM();
        return $usuarioORM->consulta($usuarioTO, true, true);
    }

    /**
     * Método que pesquisa e retorna dados da UsuarioIntegracaoORM
     * @param UsuarioTO $usuarioTO
     * @return UsuarioIntegracaoTO
     */
    public function pesquisarUsuarioIntegracaoTO(UsuarioTO $usuarioTO)
    {
        $usuarioIntegacaoTO = new UsuarioIntegracaoTO();
        $usuarioIntegacaoTO->setId_usuario($usuarioTO->getId_usuario());

        $entidadeIntegracao = $this->getEntidadeIntegracaoTO();

        if ($entidadeIntegracao->getId_sistema() == \G2\Constante\Sistema::MOODLE) {
            $usuarioIntegacaoTO->setid_entidadeintegracao($entidadeIntegracao->getId_entidadeintegracao());
            $usuarioIntegacaoTO->setId_sistema(\G2\Constante\Sistema::MOODLE);
        }

        $usuarioIntegacaoTO->setId_entidade($entidadeIntegracao->getId_entidade());

        $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
        return $usuarioIntegracaoORM->consulta($usuarioIntegacaoTO, false, true);
    }

    /**
     * WebService para editar pessoa
     * @param UsuarioTO $usuarioTO
     */
    public function editarPessoa(UsuarioTO $usuarioTO)
    {
        $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
        $usuarioIntegracaoTO = $this->pesquisarUsuarioIntegracaoTO($usuarioTO);

        if (!$entidadeIntegracaoTO) {
            return true;
        }

        if (!$usuarioIntegracaoTO) {
            $this->cadastrarPessoa($usuarioTO);
        }

        if (!$disciplinaIntegracaoTO) {
            $retornoCadastrarWs = $this->cadastrarDisciplina($dTO);
            if ($retornoCadastrarWs instanceof Ead1_Mensageiro) {
                return $retornoCadastrarWs;
            } else {
                $this->editarDisciplina($dTO);
            }
        }

        try {
            $this->clientRest->atualizardisciplina()
                ->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
                ->webservicechave($entidadeIntegracaoTO->getSt_codchave())
                ->dados(
                    array(
                        'codmodulo' => $disciplinaIntegracaoTO->getSt_codsistema()
                    , 'nommodulo' => $dTO->getSt_disciplina()
                    , 'nommodulosite' => $dTO->getSt_tituloexibicao()
                    , 'codarea' => $this->getEntidadeIntegracaoTO()->getSt_codarea()
                    , 'desmodulo' => $dTO->getSt_descricao()
                    , 'desurl' => 'http://www.ead1.com.br'
                    , 'numhoras' => $dTO->getNu_cargahoraria()
                    )
                );
            $dados = $this->clientRest->post()->atualizardisciplina;
            if (empty($dados->retorno)) {
                return new Ead1_Mensageiro('Disciplina editada, mas erro na chamada do serviço.', Ead1_IMensageiro::ERRO, $dados);
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro('Disciplina editada, mas erro na chamada do serviço.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return true;
    }

    /**
     * Método que retorna a EntidadeIntegracaoTO
     *
     * 22.11.2013 -> Incluído parametro id_entidade
     * Débora Castro - debora.castro@unyleya.com.br
     * @param null $sistema
     * @param null $id_entidade
     * @param null $idEntidadeIntegracao
     * @return Ead1_TO_Dinamico|EntidadeIntegracaoTO|false
     * @throws Zend_Exception
     */
    public function getEntidadeIntegracaoTO($sistema = null, $id_entidade = null, $idEntidadeIntegracao = null)
    {
        if (!$this->entidadeIntegracaoTO) {
            if (!$sistema) {
                $sistema = SistemaTO::MOODLE;
            }
            $entidadeIntegracaoTO = new EntidadeIntegracaoTO();

            if ($idEntidadeIntegracao) {
                //verifica se é uma instancia da classe entidade ingração e pega o valor
                if ($idEntidadeIntegracao instanceof \G2\Entity\EntidadeIntegracao) {
                    $entidadeIntegracaoTO->setId_entidadeintegracao($idEntidadeIntegracao->getId_entidadeintegracao());
                } else {
                    $entidadeIntegracaoTO->setId_entidadeintegracao($idEntidadeIntegracao);
                }
            } else {
                if ($id_entidade !== null) {
                    $entidadeIntegracaoTO->setId_entidade((int)$id_entidade);
                } else {
                    $entidadeIntegracaoTO->setId_entidade((int)$entidadeIntegracaoTO->getSessao()->id_entidade);
                }
                $entidadeIntegracaoTO->setId_sistema($sistema);
            }
            $entidadeIntegracaoTO->setbl_ativo(true)
                ->setid_situacao(\G2\Constante\Situacao::TB_ENTIDADEINTEGRACAO_ATIVA);
            $this->entidadeIntegracaoTO = $entidadeIntegracaoTO->fetch(false, true, true);

            if (!$this->entidadeIntegracaoTO) {
                throw new Exception("Dados de integração não encontrado. Verifique se o sistema ({$sistema}) ativo.");
            }

            $this->setClientRest($this->entidadeIntegracaoTO->getSt_codsistema());
        }

        return $this->entidadeIntegracaoTO;
    }


    /**
     * @param $idEntidadeIntegracao
     * @return $this
     * @throws Zend_Exception
     */
    public function setEntidadeIntegracao($idEntidadeIntegracao)
    {
        $this->entidadeIntegracaoTO = null;
        $this->getEntidadeIntegracaoTO(\G2\Constante\Sistema::MOODLE, null, $idEntidadeIntegracao);
        $this->setClientRest($this->entidadeIntegracaoTO->getSt_codsistema());
        return $this;
    }

    /**
     * Método para cadastar Disciplina no WebService com o sistema Actor
     * @param DisciplinaTO $dTO
     */
    public function cadastrarDisciplina(DisciplinaTO $dTO)
    {
        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
            if ($entidadeIntegracaoTO) {
                $orm = new DisciplinaIntegracaoORM();
                $disciplinaIntegracaoTO = $orm->consulta(new DisciplinaIntegracaoTO(array('id_disciplina' => $dTO->getId_disciplina(), 'id_sistema' => SistemaTO::ACTOR)), false, true);
                if ($disciplinaIntegracaoTO) {
                    return $this->editarDisciplina($dTO);
                }
                $this->clientRest->inserirdisciplina()
                    ->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
                    ->webservicechave($entidadeIntegracaoTO->getSt_codchave())
                    ->dados(
                        array(
                            'nommodulo' => $dTO->getSt_disciplina()
                        , 'nommodulosite' => $dTO->getSt_tituloexibicao()
                        , 'codarea' => (int)$entidadeIntegracaoTO->getSt_codarea()
                        , 'desmodulo' => $dTO->getSt_descricao()
                        , 'desurl' => 'http://www.ead1.com.br'
                        , 'numhoras' => (int)$dTO->getNu_cargahoraria()
                        )
                    );
                $dados = $this->clientRest->post()->inserirdisciplina;
                $response = $this->_SimpleXMLElementToArray($dados);
                if (empty($response['retorno']) || $response['retorno'] == "erro") {
                    $mensagens = $this->trataMensagensWs($dados);
                    if ($response['mensagem']) {
                        return new Ead1_Mensageiro("Erro no WebService! " . $response['mensagem'], Ead1_IMensageiro::ERRO, $dados);
                    }
                    return new Ead1_Mensageiro("Erro no retorno do WebService!", Ead1_IMensageiro::ERRO, $dados);
                } else {
                    $this->cadastrarDadosIntegracaoDisciplina($dados, $dTO);
                    return new Ead1_Mensageiro($dados->disciplina->codmodulo, Ead1_IMensageiro::SUCESSO);
                }
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro na chamada do Serviço!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * WebService para editar disciplina
     * @param DisciplinaTO $dTO
     */
    public function editarDisciplina(DisciplinaTO $dTO)
    {
        $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO(SistemaTO::ACTOR);
        $disciplinaIntegracaoTO = $this->pesquisarDisciplinaIntegracao($dTO);

        if (!$entidadeIntegracaoTO) {
            return true;
        }

        if (!$disciplinaIntegracaoTO) {
            $retornoCadastrarWs = $this->cadastrarDisciplina($dTO);
            if ($retornoCadastrarWs instanceof Ead1_Mensageiro) {
                return $retornoCadastrarWs;
            } else {
                $this->editarDisciplina($dTO);
            }
        }

        try {
            $this->clientRest->atualizardisciplina()
                ->webservicecodentidade($entidadeIntegracaoTO->getSt_codsistema())
                ->webservicechave($entidadeIntegracaoTO->getSt_codchave())
                ->dados(
                    array(
                        'codmodulo' => $disciplinaIntegracaoTO->getSt_codsistema()
                    , 'nommodulo' => $dTO->getSt_disciplina()
                    , 'nommodulosite' => $dTO->getSt_tituloexibicao()
                    , 'codarea' => $this->getEntidadeIntegracaoTO()->getSt_codarea()
                    , 'desmodulo' => $dTO->getSt_descricao()
                        //,'desurl'		=> 'http://www.ead1.com.br'
                    , 'numhoras' => $dTO->getNu_cargahoraria()
                    )
                );
            $dados = $this->clientRest->post()->atualizardisciplina;
            if (empty($dados->retorno)) {
                $mensagens = $this->trataMensagensWs($dados);
                if ($mensagens) {
                    return new Ead1_Mensageiro("Erro no WebService (editarDisciplina)! " . $mensagens, Ead1_IMensageiro::ERRO, $dados);
                }
                return new Ead1_Mensageiro("Erro no retorno do WebService (editarDisciplina)!", Ead1_IMensageiro::ERRO, $dados);
            }

        } catch (Exception $e) {
            return new Ead1_Mensageiro('Disciplina editada, mas erro na chamada do serviço.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return true;
    }

    /**
     * Método que pesquisa e retorna dados da Disciplina Integração
     * @param $disciplinaTO
     * @return DisciplinaIntegracaoTO
     */
    private function pesquisarDisciplinaIntegracao(DisciplinaTO $disciplinaTO)
    {
        $disciplinaIntegracaoTO = new DisciplinaIntegracaoTO();
        $disciplinaIntegracaoTO->setId_disciplina($disciplinaTO->getId_disciplina());

        $entidadeIntegracao = $this->getEntidadeIntegracaoTO();
        if ($entidadeIntegracao->getId_sistema() == \G2\Constante\Sistema::MOODLE) {
            $disciplinaIntegracaoTO->setid_entidadeintegracao($entidadeIntegracao->getId_entidadeintegracao());
        }

        $disciplinaIntegracaoORM = new DisciplinaIntegracaoORM();
        return $disciplinaIntegracaoORM->consulta($disciplinaIntegracaoTO, false, true);
    }

    /**
     * Método que retorna o texto das mensagens do WS em caso de erro
     * @param unknown_type $dados
     */
    private function trataMensagensWs($dados)
    {
        if (is_array($dados)) {
            $resposta = false;
            $mensagem = true;
            $i = 0;
            while ($mensagem) {
                $chave = "key_$i";
                if (isset($dados->$chave)) {
                    $arMensagens[] = $dados->$chave->mensagem;
                } else {
                    $mensagem = false;
                }
                $i++;
            }

            if ($arMensagens) {
                $resposta = implode(", ", $arMensagens) . ".";
            }
        } else {
            if (isset($dados->mensagem)) {
                $resposta = $dados->mensagem;
            } else {
                $resposta = $dados;
            }
        }

        return $resposta;
    }

    /**
     * Método que transforma um SimpleXMLElement em array
     * @param object $obj - Response
     * @param Boolean $bl_paginator - variável que indica a remoção de um paginator
     * @param Boolean $bl_recurssive - transforma todos os SimpleXMLElement em array recurssivamente
     * @return array $newArray
     */
    protected function _SimpleXMLElementToArray($obj, $bl_paginator = false, $bl_recurssive = false)
    {
        $newArray = array();
        $objArr = get_object_vars($obj);
        if (!empty($obj)) {
            foreach ($objArr as $key => $value) {
                if (strtolower($key) == 'paginator' && !$bl_paginator) {
                    continue;
                }
                if (is_object($value)) {
                    if (get_class($value) == 'SimpleXMLElement') {
                        if ($bl_recurssive) {
                            $temp = $this->_SimpleXMLElementRecurssive($value, $bl_paginator, $bl_recurssive);
                            if (!empty($temp)) {
                                $newArray[$key] = $temp;
                            }
                        } else {
                            $temp = get_object_vars($value);
                            if (!empty($temp)) {
                                $newArray[$key] = $temp;
                            }
                        }
                    }
                } else if (!is_array($value)) {
                    $newArray[$key] = $value;
                }
            }
        }
        return $newArray;
    }

    /**
     * Método que transforma um SimpleXMLElement em array recurssivamente
     * @param object $obj - Response
     * @param Boolean $bl_paginator - variável que indica a remoção de um paginator
     * @param Boolean $bl_recurssive - transforma todos os SimpleXMLElement em array recurssivamente
     * @return array $newArray
     */
    protected function _SimpleXMLElementRecurssive($obj, $bl_paginator = false, $bl_recurssive = false)
    {
        $newArray = array();
        $objArr = get_object_vars($obj);
        if (!empty($obj)) {
            foreach ($objArr as $key => $value) {
                if (strtolower($key) == 'paginator' && !$bl_paginator) {
                    continue;
                }
                if (is_object($value)) {
                    if (get_class($value) == 'SimpleXMLElement') {
                        if ($bl_recurssive) {
                            $temp = $this->_SimpleXMLElementRecurssive($value);
                            if (!empty($temp)) {
                                $newArray[$key] = $temp;
                            }
                        } else {
                            $temp = get_object_vars($value);
                            if (!empty($temp)) {
                                foreach ($temp as $k => $validate) {
                                    if (is_object($validate) && get_class($validate) == 'SimpleXMLElement') {
                                        $temp2 = $this->_SimpleXMLElementRecurssive($validate, $bl_paginator, $bl_recurssive);
                                        if (!empty($temp2)) {
                                            $temp[$k] = $temp2;
                                        } else {
                                            unset($temp[$k]);
                                        }
                                    }
                                }
                                $newArray[] = $temp;
                            }
                        }
                    }
                } else if (!is_array($value)) {
                    $newArray[$key] = $value;
                }
            }
        }
        return $newArray;
    }

    /**
     * Método que cadastra a referência dos dados de integração
     * @param mixed $dados
     * @param DisciplinaTO $dTO
     */
    private function cadastrarDadosIntegracaoDisciplina($dados, DisciplinaTO $dTO)
    {
        $dao = new DisciplinaDAO();
        $dao->beginTransaction();
        try {
            $disciplinaIntegracaoTO = new DisciplinaIntegracaoTO();
            $disciplinaIntegracaoTO->setId_disciplina($dTO->getId_disciplina());
            $disciplinaIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
            $disciplinaIntegracaoTO->setSt_codsistema("" . $dados->disciplina->codmodulo);
            $disciplinaIntegracaoTO->setId_usuariocadastro($dTO->getSessao()->id_usuario);

            $disciplinaIntegracaoORM = new DisciplinaIntegracaoORM();
            $disciplinaIntegracaoORM->insert($disciplinaIntegracaoTO->toArrayInsert());
            $dao->commit();
        } catch (Exception $e) {
            throw $e;
            $dao->rollBack();
        }
    }

    /**
     * Método que cadastra a referência dos dados de integração de pessoa
     * @param mixed $dados
     * @param UsuarioTO $dTO
     */
    protected function cadastrarDadosIntegracaoPessoa($dados, UsuarioTO $usuarioTO)
    {
        $dao = new PessoaDAO();
        $dao->beginTransaction();
        try {
            $usuarioIntegracaoTO = new UsuarioIntegracaoTO();
            $usuarioIntegracaoTO->setId_usuario($usuarioTO->getId_usuario());
            $usuarioIntegracaoTO->setId_usuariocadastro($usuarioIntegracaoTO->getSessao()->id_usuario);
            $usuarioIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
            $usuarioIntegracaoTO->setSt_codusuario("" . $dados->pessoa->codpessoa);
            $usuarioIntegracaoTO->setSt_loginintegrado($dados->pessoa->nomloginsugestao);
            $usuarioIntegracaoTO->setSt_senhaintegrada($dados->pessoa->dessenhasugestao);

            $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
            $usuarioIntegracaoORM->insert($usuarioIntegracaoTO->toArrayInsert());

            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Método que cadastra a referência dos dados de integração de matrícula
     * @param $dados
     * @param VwAlocacaoIntegracaoTO $vwIntegracaoAlocacaoTO
     * @throws Exception
     */
    private function cadastrarDadosIntegracaoMatricula($dados, VwAlocacaoIntegracaoTO $vwIntegracaoAlocacaoTO)
    {
        $dao = new PessoaDAO();
        $dao->beginTransaction();
        try {
            $alocacaoIntegracaoTO = new AlocacaoIntegracaoTO();
            $alocacaoIntegracaoTO->setId_alocacao($vwIntegracaoAlocacaoTO->getId_alocacao());
            $alocacaoIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
            $alocacaoIntegracaoTO->setId_usuariocadastro($vwIntegracaoAlocacaoTO->getSessao()->id_usuario);
            $alocacaoIntegracaoTO->setid_entidadeintegracao($this->getEntidadeIntegracaoTO()->getId_entidadeintegracao());
            $alocacaoIntegracaoTO->setSt_codalocacao("" . $dados->matricula->codmatricula);

            $alocacaoIntegracaoORM = new AlocacaoIntegracaoORM();
            $alocacaoIntegracaoORM->insert($alocacaoIntegracaoTO->toArrayInsert());

            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Edição de Sala de Aula via WebService
     * @param SalaDeAulaTO $salaDeAulaTO
     * @return Ead1_Mensageiro
     */
    public function editarSalaDeAula(SalaDeAulaTO $salaDeAulaTO)
    {
        try {
            $entidadeIntegracaoTO = $this->getEntidadeIntegracaoTO();
            if (!$entidadeIntegracaoTO) {
                return new Ead1_Mensageiro("Instituição não possui configuração para utilizar WebServices!", Ead1_IMensageiro::ERRO);
            }
            $salaDeAulaIntegracaoTO = $this->pesquisarSalaDeAulaIntegracao($salaDeAulaTO);
            $disciplinaSalaDeAulaTO = $this->pesquisarDisciplinaSalaDeAula($salaDeAulaTO);

            if (!$salaDeAulaIntegracaoTO || !$salaDeAulaIntegracaoTO->getSt_codsistemacurso()) {
                return $this->cadastrarSalaDeAula($salaDeAulaTO, $disciplinaSalaDeAulaTO);
            }

            $disciplinaTO = $this->pesquisarDisciplina($disciplinaSalaDeAulaTO);
            $disciplinaIntegracaoTO = $this->pesquisarDisciplinaIntegracao($disciplinaTO);
            $this->clientRest->atualizarsaladeaula()
                ->webservicecodentidade($this->getEntidadeIntegracaoTO()->getSt_codsistema())
                ->webservicechave($this->getEntidadeIntegracaoTO()->getSt_codchave())
                ->dados(
                    array(
                        'codcurso' => $salaDeAulaIntegracaoTO->getSt_codsistemacurso()
                    , 'destitulo' => $salaDeAulaTO->getSt_saladeaula()
                    , 'descurso' => $disciplinaTO->getSt_descricao()
                    , 'datiniciocurso' => "01/01/2000"
                    , 'datterminocurso' => $this->trataDataIntegracao(($salaDeAulaTO->getDt_encerramento() ? $salaDeAulaTO->getDt_encerramento() : "01/01/2100"))  // FUI FORÇADO PELO TOFFOLÃ�STICO ass.: Eder
                    , 'codarea' => $this->getEntidadeIntegracaoTO()->getSt_codarea()
                    , 'codnivel' => '2'
                    , 'desurl' => 'http://www.ead1.com.br'
                    , 'numhoras' => $disciplinaTO->getNu_cargahoraria()
                    , 'flasituacao' => 'A'
                    , 'desemail' => 'gestor2@ead1.com.br'
                    , 'codturma' => $salaDeAulaIntegracaoTO->getSt_codsistemasala() // Informando o codturma você altera a disciplina.
                    , 'nomturma' => $salaDeAulaTO->getSt_saladeaula()
                    , 'datinicioturma' => "01/01/2000"
                    , 'datterminoturma' => $this->trataDataIntegracao(($salaDeAulaTO->getDt_encerramento() ? $salaDeAulaTO->getDt_encerramento() : "01/01/2100")) // FUI FORÇADO PELO TOFFOLÃ�STICO ass.: Eder
                    )
                );
            $dados = $this->clientRest->post()->atualizarsaladeaula;
            if (empty($dados->retorno)) {
                $mensagens = $this->trataMensagensWs($dados);
                if ($mensagens) {
                    return new Ead1_Mensageiro("Erro no WebService (editarSalaDeAula)! " . $mensagens, Ead1_IMensageiro::ERRO, $dados);
                }
                return new Ead1_Mensageiro("Erro no retorno do WebService (editarSalaDeAula)!", Ead1_IMensageiro::ERRO, $dados);
            }
            return new Ead1_Mensageiro("Edição de Sala de Aula Integrada via WebService!", Ead1_IMensageiro::SUCESSO, $dados);
        } catch (Exception $e) {
            return new Ead1_Mensageiro("Erro no WebService (editarSalaDeAula)! " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que pesquisa e retorna dados da Sala de Aula Integração
     * @param SalaDeAulaTO $salaDeAulaTO
     * @param null $idCourse
     * @param null $idSistema
     * @return array|bool
     */
    public function pesquisarSalaDeAulaIntegracao(SalaDeAulaTO $salaDeAulaTO, $idCourse = null, $idSistema = null)
    {
        $salaDeAulaIntegracaoTOPesquisa = new SalaDeAulaIntegracaoTO();
        $salaDeAulaIntegracaoTOPesquisa->setId_saladeaula($salaDeAulaTO->getId_saladeaula());

        if ($idCourse) {
            $salaDeAulaIntegracaoTOPesquisa->setSt_codsistemacurso($idCourse);
        }

        if ($idSistema) {
            $salaDeAulaIntegracaoTOPesquisa->setid_sistema($idSistema);
        }

        $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
        return $salaDeAulaIntegracaoORM->consulta($salaDeAulaIntegracaoTOPesquisa, false, true);
    }

    /**
     * Método que pesquisa e retorna dados da Disciplina Sala de Aula
     * @param $salaDeAulaTO
     * @return DisciplinaSalaDeAulaTO
     */
    private function pesquisarDisciplinaSalaDeAula(SalaDeAulaTO $salaDeAulaTO)
    {
        $salaDeAulaDisciplinaTOPesquisa = new DisciplinaSalaDeAulaTO();
        $salaDeAulaDisciplinaTOPesquisa->setId_saladeaula($salaDeAulaTO->getId_saladeaula());

        $salaDeAulaDisciplinaORM = new DisciplinaSalaDeAulaORM();
        return $salaDeAulaDisciplinaORM->consulta($salaDeAulaDisciplinaTOPesquisa, false, true);
    }

    /**
     * Inserção para Sala de Aula no Sistema Actor
     *
     */
    public function cadastrarSalaDeAula(SalaDeAulaTO $salaDeAulaTO, DisciplinaSalaDeAulaTO $disciplinaSalaDeAulaTO)
    {

        try {
            $disciplinaTO = $this->pesquisarDisciplina($disciplinaSalaDeAulaTO);
            $disciplinaIntegracaoTO = $this->pesquisarDisciplinaIntegracao($disciplinaTO);
            if (!$disciplinaTO) {
                throw new Exception ("Erro no disciplinaTO", 0);
            }

            if (!$disciplinaIntegracaoTO) {
                $wsBo = new WsBO(Ead1_Ambiente::geral()->wsURL . 'services/disciplina.php');
                $respostaWsDisciplina = $wsBo->cadastrarDisciplina($disciplinaTO);
                if ($respostaWsDisciplina instanceof Ead1_Mensageiro) {
                    if ($respostaWsDisciplina->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        return $this->cadastrarSalaDeAula($salaDeAulaTO, $disciplinaSalaDeAulaTO);
                    } else {
                        return $respostaWsDisciplina;
                    }
                } else {
                    return $this->cadastrarSalaDeAula($salaDeAulaTO, $disciplinaSalaDeAulaTO);
                }
            }

            $this->clientRest->inserirsaladeaula()
                ->webservicecodentidade($this->getEntidadeIntegracaoTO()->getSt_codsistema())
                ->webservicechave($this->getEntidadeIntegracaoTO()->getSt_codchave())
                ->dados(
                    array(
                        'codmodulosaladeaula' => $disciplinaIntegracaoTO->getSt_codsistema() // ObrigatÃ³rio, ele indica qual mÃ³dulo serÃ¡ usado para a saladeaula.
                    , 'destitulo' => $salaDeAulaTO->getSt_saladeaula()
                    , 'datiniciocurso' => "01/01/2000"
                    , 'datterminocurso' => $this->trataDataIntegracao(($salaDeAulaTO->getDt_encerramento() ? $salaDeAulaTO->getDt_encerramento() : "01/01/2100")) // FUI FORÃ‡ADO PELO TOFFOLÃ�STICO ass.: Eder
                    , 'datinicioturma' => "01/01/2000"
                    , 'datterminoturma' => $this->trataDataIntegracao(($salaDeAulaTO->getDt_encerramento() ? $salaDeAulaTO->getDt_encerramento() : "01/01/2100")) // FUI FORÃ‡ADO PELO TOFFOLÃ�STICO ass.: Eder
                    , 'nomturma' => $salaDeAulaTO->getSt_saladeaula() // OPCIONAL
                        //,'codcurso'				=>'7042' // INFORMANDO O CODCURSO NESSA PARTE, O SISTEMA VAI INSERIR APENAS A TURMA, NÃO INFORMANDO, ELE INSERE TUDO
                    , 'codarea' => $this->getEntidadeIntegracaoTO()->getSt_codarea()
                    , 'descurso' => $disciplinaTO->getSt_descricao()
                    , 'desurl' => 'http://www.ead1.com.br'
                    , 'numhoras' => $disciplinaTO->getNu_cargahoraria()
                    , 'codnivel' => '2'
                    , 'desemail' => 'gestor2@ead1.com.br'
                    )
                );
            $dados = $this->clientRest->post()->inserirsaladeaula;
            $response = $this->_SimpleXMLElementToArray($dados);
            if (empty($response['retorno'])) {
                if ($response['mensagem']) {
                    return new Ead1_Mensageiro("Erro no WebService! " . $response['mensagem'], Ead1_IMensageiro::ERRO, $dados);
                }
                return new Ead1_Mensageiro("Erro no retorno do WebService!", Ead1_IMensageiro::ERRO, $dados);
            } else {
                if ($response['retorno'] == 'erro') {
                    return new Ead1_Mensageiro("Erro no WebService! " . $response['mensagem'], Ead1_IMensageiro::ERRO, $dados);
                }
                $this->cadastrarDadosIntegracaoSalaDeAula($dados, $salaDeAulaTO);
            }
            return new Ead1_Mensageiro("Sala de Aula Integrada via WebService!", Ead1_IMensageiro::SUCESSO, $dados);
        } catch (Exception $exception) {
            return new Ead1_Mensageiro($exception->getMessage(), Ead1_IMensageiro::ERRO, $exception);
        }
    }

    /**
     * Método que pesquisa e retorna dados da Disciplina
     * @param $disciplinaSalaDeAulaTO
     * @return DisciplinaTO
     */
    private function pesquisarDisciplina(DisciplinaSalaDeAulaTO $disciplinaSalaDeAulaTO)
    {
        $disciplinaTOPesquisa = new DisciplinaTO();
        $disciplinaTOPesquisa->setId_disciplina($disciplinaSalaDeAulaTO->getId_disciplina());

        $disciplinaORM = new DisciplinaORM();
        return $disciplinaORM->consulta($disciplinaTOPesquisa, false, true);
    }

    /**
     * Método que trata data para a integração
     * @param mixed $data Data a ser tratada
     * @param mixed $formato formato a ser atribuÃ­do
     * @param Boolean $default valor default
     */
    private function trataDataIntegracao($data = null, $formato = "d/m/Y", $default = true)
    {
        if ($data) {
            $dataHorario1 = explode("T", $data);
            $dataHorario = explode(" ", $dataHorario1[0]);

            $dataTratamento = explode("-", $dataHorario[0]);
            if (count($dataTratamento) == 3) {
                $dataTratada = "$dataTratamento[2]/$dataTratamento[1]/$dataTratamento[0]";
            } else {
                $dataTratada = $dataTratamento[0];
            }
        } else {
            if ($default) {
                $dataTratada = date($formato);
            }
        }
        return $dataTratada;
    }

    /**
     * Método que cadastra os dados da integração de sala de aula
     * @param mixed $dados XML dos dados
     * @param SalaDeAulaTO $salaDeAulaTO
     */
    private function cadastrarDadosIntegracaoSalaDeAula($dados, SalaDeAulaTO $salaDeAulaTO)
    {
        $dao = new PessoaDAO();
        $dao->beginTransaction();
        try {

            $salaDeAulaIntegracaoTO = new SalaDeAulaIntegracaoTO();
            $salaDeAulaIntegracaoTO->setId_saladeaula($salaDeAulaTO->getId_saladeaula());
            $salaDeAulaIntegracaoTO->setId_sistema($this->getEntidadeIntegracaoTO()->getId_sistema());
            $salaDeAulaIntegracaoTO->setSt_codsistemacurso("" . $dados->saladeaula->curso->codcurso);
            $salaDeAulaIntegracaoTO->setSt_codsistemasala("" . $dados->saladeaula->turma->codturma);
            $salaDeAulaIntegracaoTO->setSt_codsistemareferencia("" . $dados->saladeaula->cursoreferencia->codcursoreferencia);
            $salaDeAulaIntegracaoTO->setId_usuariocadastro($salaDeAulaTO->getSessao()->id_usuario);
            $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
            $salaDeAulaIntegracaoORM->insert($salaDeAulaIntegracaoTO->toArrayInsert());
            $dao->commit();
        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Método que pesquisa e retorna dados do usuário
     * @param AlocacaoTO $alocacaoTO
     * @return AlocacaoTO
     */
    private function pesquisarUsuarioAlocacao(AlocacaoTO $alocacaoTO)
    {
        $matriculaDisciplinaTOConsulta = new MatriculaDisciplinaTO();
        $matriculaDisciplinaTOConsulta->setId_matriculadisciplina($alocacaoTO->getId_matriculadisciplina());

        $matriculaDisciplinaORM = new MatriculaDisciplinaORM();
        $matriculaDisciplinaTO = $matriculaDisciplinaORM->consulta($matriculaDisciplinaTOConsulta, true, true);

        $matriculaTOConsulta = new MatriculaTO();
        $matriculaTOConsulta->setId_matricula($matriculaDisciplinaTO->getId_matricula());

        $matriculaORM = new MatriculaORM();
        $matriculaTO = $matriculaORM->consulta($matriculaTOConsulta, true, true);

        $usuarioTO = new UsuarioTO();
        $usuarioTO->setId_usuario($matriculaTO->getId_usuario());

        $salaDeAulaIntegracaoTOPesquisa = new SalaDeAulaIntegracaoTO();
        $salaDeAulaIntegracaoTOPesquisa->setId_saladeaula($salaDeAulaTO->getId_saladeaula());

        $salaDeAulaIntegracaoORM = new SalaDeAulaIntegracaoORM();
        return $salaDeAulaIntegracaoORM->consulta($salaDeAulaIntegracaoTOPesquisa, false, true);
    }

}

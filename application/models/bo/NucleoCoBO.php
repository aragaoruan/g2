<?php

/**
 * Class NucleoCoBO
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 */
class NucleoCoBO extends Ead1_BO
{

    /**
     * Método que salva o Núcleo verificando se é para edição ou inserção
     * @param NucleoCoTO $nucleo
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarNucleo(NucleoCoTO $nucleo)
    {
        $dao = new NucleoCoDAO();
        try {
            $dao->beginTransaction();


            /** @noinspection PhpUndefinedFieldInspection */
            $nucleo->setId_usuariocadastro($nucleo->getSessao()->id_usuario);
            /** @noinspection PhpUndefinedFieldInspection */
            $nucleo->setId_entidade($nucleo->getSessao()->id_entidade);

            if ($nucleo->getId_nucleoco()) {
                $dao->editarNucleoCo($nucleo);
            } else {
                $nucleo->setId_nucleoco($dao->cadastrarNucleoCo($nucleo));
            }

            $dao->commit();
            return new Ead1_Mensageiro($nucleo);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }

    }

    /**
     * @param VwNucleoCoTO $nucleo
     * @return Ead1_Mensageiro
     */
    public function retornaVwNucleo(VwNucleoCoTO $nucleo)
    {
        if (!$nucleo->getId_entidade()) {
            /** @noinspection PhpUndefinedFieldInspection */
            $nucleo->setId_entidade($nucleo->getSessao()->id_entidade);
        }
        $nucleo->setBl_ativo(1);
        $dao = new NucleoCoDAO();
        return new Ead1_Mensageiro($dao->listarVwNucleo($nucleo));
    }


    /**
     * @param VwNucleoAssuntoCoTO $nucleoassunto
     * @return Ead1_Mensageiro
     */
    public function retornaVwNucleoAssunto(VwNucleoAssuntoCoTO $nucleoassunto)
    {

        if (!$nucleoassunto->getId_nucleoco()) {
            /** @noinspection PhpUndefinedFieldInspection */
            $nucleoassunto->setId_entidade($nucleoassunto->getSessao()->id_entidade);
            return new Ead1_Mensageiro("O Id do Núcleo é obrigatório!", Ead1_IMensageiro::ERRO);
        }
        $dao = new NucleoAssuntoCoDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
            $dao->listarAssuntosNucleo($nucleoassunto), $nucleoassunto
        ));
    }

    /**
     * Método que retorna NucleoCoTO
     * @param NucleoCoTO $nucleo
     * @return Ead1_Mensageiro
     */
    public function listarNucleoCo(NucleoCoTO $nucleo)
    {
        if (!$nucleo->getId_entidade()) {
            /** @noinspection PhpUndefinedFieldInspection */
            $nucleo->setId_entidade($nucleo->getSessao()->id_entidade);
        }
        $nucleo->setBl_ativo(1);
        $dao = new NucleoCoDAO();
        return new Ead1_Mensageiro($dao->listarNucleoCo($nucleo));
    }


    /**
     * Método que deleta de forma lógica o núcleo
     * @param NucleoCoTO $nucleo
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function deletarNucleo(NucleoCoTO $nucleo)
    {
        $dao = new NucleoCoDAO();
        try {

            if (!$nucleo->getId_nucleoco()) {
                throw new Zend_Exception("Id não informado");
            }

            $dao->deletarNucleo($nucleo);

            return new Ead1_Mensageiro("Núcleo inativado com sucesso");
        } catch (Zend_Exception $e) {
            throw $e;
        }
    }


    /**
     * Método que vincula um Assunto a um Núcleo
     * @param $nucleoAssunto
     * @return Ead1_Mensageiro
     * @throws Exception
     * @deprecated
     */
    public function salvarNucleoAssunto($nucleoAssunto)
    {

        if ($nucleoAssunto instanceof NucleoAssuntoCoTO) {
            $arnucleoassunto[] = $nucleoAssunto;
        }

        $dao = new NucleoAssuntoCoDAO();
        $dao->beginTransaction();
        try {
            foreach ($arnucleoassunto as $nucleoAssuntoTO) {

                //TODO criar nova instancia só com parametros de busca

                $nucleoAssuntoTO->fetch(false, true, true);

                if (!$nucleoAssuntoTO->getId_nucleoassuntoco()) {
                    //TODO insere o vinculo
                } else {
                    //TODO atualiza o vinculo para ativo!
                }

            }

            $dao->commit();
            return new Ead1_Mensageiro("Vinculo salvo com Sucesso!");
        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Método que retorna os Assuntos que podem ser adicionados a um núcleo
     * @param VwNucleoAssuntoCoTO $vwNucleoAssunto
     * @return Ead1_Mensageiro
     */
    public function listarAssuntosNucleoAdicionar(VwNucleoAssuntoCoTO $vwNucleoAssunto)
    {
        if (!$vwNucleoAssunto->getId_nucleoco()) {
            return new Ead1_Mensageiro("O Id do Núcleo é obrigatório!", Ead1_IMensageiro::ERRO);
        }
        $dao = new NucleoAssuntoCoDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao->listarAssuntosNucleo($vwNucleoAssunto, true), $vwNucleoAssunto));
    }

    /**
     * Método que cadastra os assuntos para o núcleo
     * @param array $arAssuntos
     * @param NucleoCoTO $nucleoCoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     * @see NucleoCoRO::salvarAssuntosNucleo
     */
    public function salvarAssuntosNucleo(array $arAssuntos, NucleoCoTO $nucleoCoTO)
    {
        $dao = new NucleoAssuntoCoDAO();
        $assuntoNucleoCoTo = new NucleoAssuntoCoTO();
        $assuntoNucleoCoTo->setId_nucleoco($nucleoCoTO->getId_nucleoco());

        try {
            $dao->beginTransaction();
            if (!$nucleoCoTO->getId_nucleoco()) {
                throw new Zend_Exception("A chave do núcleo não foi encontrada.");
            }
            foreach ($arAssuntos as $arrayTO) {
                $assuntoNucleoCoTo->setId_assuntoco($arrayTO);
                $assuntoNucleoCoTo->fetch(false, true, true);

                $assuntoNucleoCoTo->setBl_ativo(1);
                /** @noinspection PhpUndefinedFieldInspection */
                $assuntoNucleoCoTo->setId_usuariocadastro($nucleoCoTO->getSessao()->id_usuario);

                if (!$assuntoNucleoCoTo->getId_nucleoassuntoco()) {
                    $dao->cadastrarAssuntoNucleoCo($assuntoNucleoCoTo);
                } else {
                    $dao->editarNucleoAssuntoCo($assuntoNucleoCoTo);
                }
            }

            $dao->commit();
            return new Ead1_Mensageiro("Asssuntos do núcleo cadastrados com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }

    }

    /**
     * Método que exclui o vínculo de um Assunto com o Núcleo
     * @param VwNucleoAssuntoCoTO $nucleoAssunto
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function excluirNucleoAssuntoCo(VwNucleoAssuntoCoTO $nucleoAssunto)
    {
        $dao = new NucleoAssuntoCoDAO();
        try {
            $dao->beginTransaction();
            if (!$nucleoAssunto->getId_nucleoassuntoco())
                throw new Zend_Exception("A chave do NucleoAssunto não foi encontrada");

            $nucleoAssunto->fetch(true, true, true);
            $negocio = new \G2\Negocio\Ocorrencia();
            $nu_ocorrencia = $negocio->verificaOcorrenciaAssunto($nucleoAssunto);
            if ($nu_ocorrencia[0]['nu_ocorrencias'] > 0) {
                throw new Zend_Exception("Existem " . $nu_ocorrencia[0]['nu_ocorrencias'] . " ocorrências não encerradas vinculadas a esse assunto nesse núcleo. Não é possível excluí-lo do núcleo.");
            }
            //Zend_Debug::dump($nu_ocorrencia[0]['nu_ocorrencias']);die;
            $dao->excluirNucleoAssuntoCo($nucleoAssunto->getId_nucleoassuntoco());
            $dao->commit();
            return new Ead1_Mensageiro("Assunto excluído do núcleo com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
//			throw $e;
            return new Ead1_Mensageiro("Erro ao excluir o vínculo do assunto com o núcleo - " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Método que Lista Responsáveis pelo núcleo
     * @param VwNucleoPessoaCoTO $vwNucleoPessoa
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function retornaResponsaveisAssunto(VwNucleoPessoaCoTO $vwNucleoPessoa)
    {
        try {
            if (!$vwNucleoPessoa->getId_nucleoco() || !$vwNucleoPessoa->getId_assuntoco()) {
                throw new Zend_Exception("O Id do núcleo e/ou Id do Assunto são obrigatórios");
            }
            unset($vwNucleoPessoa->bl_prioritario);
            $dao = new NucleoAssuntoCoDAO();
            return new Ead1_Mensageiro($dao->listaResponsaveisAssunto($vwNucleoPessoa));
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Salva Pessoa como responsavel ao atendente no Nucleo
     * @param NucleoPessoaCoTO $nucleoPessoaCo
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarNucleoPessoaCo(NucleoPessoaCoTO $nucleoPessoaCo)
    {
        $dao = new NucleoPessoaCoDAO();
        try {
            $dao->beginTransaction();

            /** @noinspection PhpUndefinedFieldInspection */
            $nucleoPessoaCo->setId_usuariocadastro($nucleoPessoaCo->getSessao()->id_usuario);
            if ($nucleoPessoaCo->getId_nucleopessoaco()) {
                $dao->editarNucleoPessoaCo($nucleoPessoaCo);
            } else {
                $nucleoPessoaCo->setId_nucleopessoaco($dao->cadastrarNucleoPessoaCo($nucleoPessoaCo));
            }

            $dao->commit();
            return new Ead1_Mensageiro($nucleoPessoaCo);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Método que exclui vincula da Pessoa com o núcleo
     * @param NucleoPessoaCoTO $nucleoPessoaCo
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     * @see NucleoCoRO::excluirPessoaNucleo
     * @deprecated
     */
    public function excluirPessoaNucleo(NucleoPessoaCoTO $nucleoPessoaCo)
    {
        $dao = new NucleoPessoaCoDAO();
        try {
            $dao->beginTransaction();
            if (!$nucleoPessoaCo->getId_nucleopessoaco())
                throw new Zend_Exception("A chave do NucleoPessoa não foi encontrada");
            $dao->excluirNucleoPessoaCo($nucleoPessoaCo->getId_nucleopessoaco());
            $dao->commit();
            return new Ead1_Mensageiro("Pessoa excluída do assunto com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Lista Emails do NucleoCo
     * @param VwNucleoCoEmailTO $nucleoCoEmail
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function retornaEmail(VwNucleoCoEmailTO $nucleoCoEmail)
    {
        $dao = new NucleoCoEmailDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
            $dao->listarEmail($nucleoCoEmail, true), $nucleoCoEmail
        ));
    }

    /**
     * Vincula e-mails ao núcleo
     * @param array $arEmail
     * @param NucleoCoEmailTO $nucleoEmail
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarEmails(array $arEmail, NucleoCoEmailTO $nucleoEmail)
    {
        $dao = new NucleoCoEmailDAO();
        try {
            $dao->beginTransaction();
            if (!$nucleoEmail->getId_nucleoco()) {
                throw new Zend_Exception("A chave do núcleo não foi encontrada.");
            }
            foreach ($arEmail as $arrayTO) {
                $email = new NucleoCoEmailTO();
                $email->setId_emailconfig($arrayTO);
                $email->setId_nucleoco($nucleoEmail->getId_nucleoco());
                $dao->cadastrarNucleoCoEmail($email);
            }

            $dao->commit();
            return new Ead1_Mensageiro("E-mails do núcleo cadastrados com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Exclui Vinculo do e-mail com o núcleo
     * @param NucleoCoEmailTO $nucleoEmail
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function excluirEmail(NucleoCoEmailTO $nucleoEmail)
    {
        $dao = new NucleoCoEmailDAO();
        try {
            $dao->beginTransaction();
            if (!$nucleoEmail->getId_emailconfig() || !$nucleoEmail->getId_nucleoco())
                throw new Zend_Exception("A chave do NucleoEmail e/ou NucleoCO não foi encontrada");
            $dao->excluirNucleoCoEmail($nucleoEmail);
            $dao->commit();
            return new Ead1_Mensageiro("Email excluído do núcleo com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }


    /**
     * Retorna VwCategoriaOcorrencia
     * @param VwCategoriaOcorrenciaTO $categoriaOcorrenciaTO
     * @return Ead1_Mensageiro
     */
    public function retornaCategoriaOcorrencia(VwCategoriaOcorrenciaTO $categoriaOcorrenciaTO)
    {
        $dao = new CategoriaOcorrenciaDAO();
        $categoriaOcorrenciaTO->setBl_ativo(true);
        return new Ead1_Mensageiro($dao->listarVwCategoriaOcorrencia($categoriaOcorrenciaTO));
    }

    /**
     * Método que decide se edita/altera/reativa CategoriaOCorrencia
     * @param CategoriaOcorrenciaTO $categoria
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarCategoriaOcorrencia(CategoriaOcorrenciaTO $categoria)
    {
        $dao = new CategoriaOcorrenciaDAO();
        try {
            $dao->beginTransaction();
            /** @noinspection PhpUndefinedFieldInspection */
            $categoria->setId_entidadecadastro($categoria->getSessao()->id_entidade);

            /** @var CategoriaOcorrenciaTO $categoriaOcorrencia */
            $categoriaOcorrencia = $categoria;
            $categoriaOcorrencia = Ead1_TO_Dinamico::encapsularTo($dao->verificaTipoOcorrencia($categoriaOcorrencia), new CategoriaOcorrenciaTO(), true);

            if (!empty($categoriaOcorrencia))
                $categoria->setId_categoriaocorrencia($categoriaOcorrencia->getId_categoriaocorrencia());

            if ($categoria->getId_categoriaocorrencia()) {
                $dao->editarCategoriaOcorrencia($categoria);
            } else {
                $dao->salvarCategoriaOcorrencia($categoria);
            }
            $dao->commit();
            return new Ead1_Mensageiro($categoria);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Método que exclui de forma lógica uma CategoriaOCorrencia
     * @param CategoriaOcorrenciaTO $categoria
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function excluirCategoriaOcorrencia(CategoriaOcorrenciaTO $categoria)
    {
        $dao = new CategoriaOcorrenciaDAO();
        try {
            $dao->beginTransaction();
            if (!$categoria->getId_categoriaocorrencia())
                throw new Zend_Exception("A chave do Tipo Ocorrência é obrigatória!");
            $categoria->setBl_ativo(false);
            $dao->editarCategoriaOcorrencia($categoria);

            $dao->commit();
            return new Ead1_Mensageiro("Tipo de ocorrência excluída com sucesso!");

        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Lista Evoluções relacionadas ao núcleo
     * @param NucleoEvolucaoCoTO $nucleoEvolucao
     * @return Ead1_Mensageiro
     */
    public function retornaNucleoEvolucaoCo(NucleoEvolucaoCoTO $nucleoEvolucao)
    {
        if (!$nucleoEvolucao->getId_nucleoco()) {
            return new Ead1_Mensageiro("O Id do Núcleo é obrigatório!", Ead1_IMensageiro::ERRO);
        }
        $dao = new NucleoEvolucaoCoDAO();
        return new Ead1_Mensageiro($dao->listarNucleoEvolucaoCo($nucleoEvolucao));

    }

    /**
     * Método que deleta as evoluções relacionadas ao núcleo e salva novamente
     * @param array $nucleosEvolucao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarNucleoEvolucaoCo(array $nucleosEvolucao)
    {
        $dao = new NucleoEvolucaoCoDAO();
        try {
            $dao->beginTransaction();
            if (!empty($nucleosEvolucao[0])) {
                $to = $nucleosEvolucao[0][0];
                $dao->deletarEvolucoesNucleo($to->getId_nucleoco());
                foreach ($nucleosEvolucao as $neTO) {
                    if (!empty($neTO)) {
                        foreach ($neTO as $to) {
                            $dao->salvarNucleoEvolucaoco($to);
                        }
                    }
                }
            }
            $dao->commit();
            return new Ead1_Mensageiro("Evoluções vinculadas com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * @param NucleoFinalidadeTO $nucleoFinalidade
     * @param null $where
     * @return Ead1_Mensageiro
     */
    public function retornarNucleoFinalidadeTO(NucleoFinalidadeTO $nucleoFinalidade, $where = null)
    {
        $dao = new NucleoCoDAO();
        $mensageiro = new Ead1_Mensageiro;
        return $mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornarNucleoFinalidadeTO($nucleoFinalidade, $where), new NucleoFinalidadeTO ()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorna objetos VwNucleoOcorrenciaTO de acordo com os registros encontrados na vw_nucleoocorrencia
     * @param VwNucleoOcorrenciaTO $vw
     * @param null $where
     * @return Ead1_Mensageiro
     */
    public function retornarVwNucleoOcorrenciaTO(VwNucleoOcorrenciaTO $vw, $where = null)
    {
        $dao = new NucleoCoDAO();
        $mensageiro = new Ead1_Mensageiro;
        return $mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo(
            $dao->retornarVwNucleoOcorrenciaTO($vw, $where), new VwNucleoOcorrenciaTO()
        ), Ead1_IMensageiro::SUCESSO);
    }


    /**
     * Retorna as pesoas vinculadas a um núcleo
     * @param VwPessoasNucleoAssuntoTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornaPessoasNucleoAssunto(VwPessoasNucleoAssuntoTO $vw)
    {
        $dao = new NucleoCoDAO();
        $retorno = $dao->retornaPessoasNucleoAssunto($vw)->toArray();
        $mensageiro = new Ead1_Mensageiro();
        if (empty($retorno)) {
            $mensageiro->setMensageiro('Nenhuma pessoa encontrada!', Ead1_IMensageiro::AVISO);
        } else
            $mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwPessoasNucleoAssuntoTO()), Ead1_IMensageiro::SUCESSO);
        return $mensageiro;
    }


    /**
     * Método que Vw_assuntosnucleopessoa
     * @param VwAssuntoNucleoPessoaTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornaFuncoesPessoaNucleo(VwAssuntoNucleoPessoaTO $vw)
    {

        $dao = new NucleoCoDAO();
        $vw->setId_funcao(FuncaoTO::ATENDENTE_CA);

        $atendenteCA = Ead1_TO_Dinamico::encapsularTo($dao->retornaVwAssuntoNucleoPessoa($vw)->toArray(), new VwAssuntoNucleoPessoaTO());
        $vw->setId_funcao(FuncaoTO::ATENDENTE_SETOR_CA);
        $atendenteSetorCA = Ead1_TO_Dinamico::encapsularTo($dao->retornaVwAssuntoNucleoPessoa($vw)->toArray(), new VwAssuntoNucleoPessoaTO());

        $vw->setId_funcao(FuncaoTO::RESPONSAVEL_CA);
        $responsavelCA = Ead1_TO_Dinamico::encapsularTo($dao->retornaVwAssuntoNucleoPessoa($vw)->toArray(), new VwAssuntoNucleoPessoaTO());

        $retorno = array('atendente_ca' => $atendenteCA
        , 'atendente_setor' => $atendenteSetorCA
        , 'responsavel_ca' => $responsavelCA);
//        Zend_Debug::dump($retorno,__CLASS__.'('.__LINE__.')');die;
        $mensageiro = new Ead1_Mensageiro($retorno, Ead1_IMensageiro::SUCESSO);
        if (empty($retorno)) {
            $mensageiro->setMensageiro('Nenhuma função encontrada!', Ead1_IMensageiro::AVISO);
        }

        return $mensageiro;
    }


    /**
     * Método que salva um array de funcoes para uma pessoa no nucleo
     * @param array $arrTO
     * @return Ead1_Mensageiro
     * @deprecated
     */
    public function salvarArrayFuncoes(array $arrTO)
    {
        try {
            //o array vem do flex tratado: Caso ele tenha id_nucleopessoco, é para desativar a funcao da pessoa no nucleo
            foreach ($arrTO as $to) {
                if ($to->getId_nucleopessoaco()) {
                    $this->excluirPessoaNucleo($to);
                } else {
                    $this->salvarNucleoPessoaCo($to);
                }
            }
            $this->mensageiro->setMensageiro('Alterações realizadas com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Ocorreu um erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Verifica se o usuario existe na entidade, se não existir faz a importacao
     * @param VwPessoaTO $vwPessoa
     * @param VwEntidadeCompartilhaDadosTO $compartilhaTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuarioEntidade(VwPessoaTO $vwPessoa, VwEntidadeCompartilhaDadosTO $compartilhaTO)
    {
        try {
            $newPessoa = new VwPessoaTO();
            $newPessoa->setId_usuario($vwPessoa->getId_usuario());
            $newPessoa->setId_entidade($compartilhaTO->getId_entidadeorigem());
            $newPessoa->fetch(false, true, true, false, true);
            if (!$newPessoa->getSt_nomecompleto()) {
//                  'Nao tem a pessoa na entidade, deve importar';
                $daoPessoa = new PessoaDAO();
                /** @noinspection PhpUndefinedFieldInspection */
                $importacao = $daoPessoa->importarUsuarioEntidade($vwPessoa->getId_usuario(), $compartilhaTO->getId_entidadedestino(), $compartilhaTO->getId_entidadeorigem(), $vwPessoa->getSessao()->id_usuario);
                if (!$importacao) {
                    throw new Zend_Exception('Ocorreu um erro na importação da pessoa!');
                } else
                    $this->mensageiro->setMensageiro('Usuário importado com sucesso!', Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Usuário importado com sucesso!');
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Ocorreu um erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Retorna pesquisa por pessoas, independente da entidade
     * @param VwPessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoas(VwPessoaTO $pTO)
    {
        try {
            if (!$pTO->getSt_nomecompleto() && !$pTO->getSt_cpf() && !$pTO->getSt_email()) {
                throw new Zend_Exception('É necessário algum paramentro para pesquisa!');
            }
            $daoPessoa = new PessoaDAO();
            $dados = $daoPessoa->pesquisaUsuarioSemEntidade($pTO);
            if (!empty($dados))
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaTO()), Ead1_IMensageiro::SUCESSO);
            else
                $this->mensageiro->setMensageiro('Nenhum usuário encontrado', Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }
}

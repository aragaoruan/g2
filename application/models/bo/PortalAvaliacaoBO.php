<?php
/**
 * Classe com regras de negócio para interações de Avaliação
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class PortalAvaliacaoBO extends Ead1_BO{

	private $_dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->_dao = new AvaliacaoDAO();
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a avaliacao
	 * @param AvaliacaoTO $aTO
	 * @param array $arrDisciplinas
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacao(AvaliacaoTO $aTO, array $arrDisciplinas){
		if($aTO->getId_avaliacao()){
			$adTO = new AvaliacaoDisciplinaTO();
			$adTO->setId_avaliacao($aTO->getId_avaliacao());
			$this->dao->excluirAvaliacaoDisciplina($adTO);
		}
		
		if($aTO->getId_avaliacao()){
			return $this->editarAvaliacao($aTO, $arrDisciplinas);
		}
		return $this->cadastrarAvaliacao($aTO, $arrDisciplinas);
	}
	
	/**
	 * Metodo que cadastra avaliações para os alunos
	 * @param AvaliacaoAlunoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO){
		$this->dao->beginTransaction();
		try{
			$aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
			$id_avaliacaoaluno = $this->dao->cadastrarAvaliacaoAluno($aaTO);
			$aaTO->setId_avaliacaoaluno($id_avaliacaoaluno);
			$this->processaNotaDisciplina($aaTO);
			$this->dao->commit();
			return $this->mensageiro->setMensageiro("Nota aplicada com sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Nota para esta Avaliação!',Ead1_IMensageiro::ERRO,$e->getTraceAsString());
		}
	}
	
	/**
	 * Metodo que verifica avaliações dos alunos para inserir notas nas disciplinas
	 * @param AvaliacaoAlunoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function processaNotaDisciplina(AvaliacaoAlunoTO $aaTO){
		try{
			$vaaTO = new VwAvaliacaoAlunoTO();
			$vaaTO->setId_matricula($aaTO->getId_matricula());
			$vaaTO->setId_avaliacao($aaTO->getId_avaliacao());
			$disciplinas = $this->dao->retornarVwAvaliacaoAluno($vaaTO);
			
			$arDisciplinasModulo = false;
			if($disciplinas){
				foreach($disciplinas as $disciplina){
					$matriculaDisciplinaTO = new MatriculaDisciplinaTO();
					if($disciplina->id_tipoprova == 1){ // ProjetoPedagógico
						
						if($disciplina->st_nota && $disciplina->nu_notamaxima){
							$percentualNota = (($disciplina->st_nota / $disciplina->nu_notamaxima) * 100);
							if($percentualNota >= $disciplina->nu_percentualaprovacao){
								$matriculaDisciplinaTO->setId_evolucao(12); // concluinte
								$matriculaDisciplinaTO->setDt_conclusao(new Zend_Date(null,Zend_Date::ISO_8601));
							}else{
								$matriculaDisciplinaTO->setId_evolucao(13); // cursando								
							}
							$matriculaDisciplinaTO->setNu_aprovafinal($percentualNota);
							$where = "id_matricula = {$aaTO->getId_matricula()} AND id_disciplina = {$disciplina->id_disciplina}";
							$matriculaDAO = new MatriculaDAO();
							$matriculaDAO->editarMatriculaDisciplina($matriculaDisciplinaTO, $where);
							$matriculaDisciplinaTO->setId_matricula($aaTO->getId_matricula());
							
							$matriculaTO = new MatriculaTO();
							$matriculaTO->setId_matricula($matriculaDisciplinaTO->getId_matricula());

							$ng_matricula = new \G2\Negocio\Matricula();
							$ng_matricula->alocarAutomatico($matriculaTO);
							
							$this->concluirMatricula($matriculaDisciplinaTO);
						}else{
							throw new Exception("Não existe valor de nota máximo ou nota definida", 0);
						}
					}else{
						if(!$arDisciplinasModulo){
							$vaa2TO = new VwAvaliacaoAlunoTO();
							$vaa2TO->setId_matricula($aaTO->getId_matricula());
							$vaa2TO->setId_avaliacao($aaTO->getId_avaliacao());
							//$vaa2TO->setId_modulo($disciplina->id_modulo);
							$vaa2TO->setId_tipoprova(2); // Sala de Aula
							$arDisciplinasModulo = $this->dao->retornarVwAvaliacaoAluno($vaa2TO);
						}
						//Zend_Debug::dump( $arDisciplinasModulo );
												
						if($arDisciplinasModulo){
							$notasQueNaoDevemSerSomadas = array();
							foreach($arDisciplinasModulo as $avaliacao){
								if($avaliacao->st_nota == null && $avaliacao->id_avaliacaorecupera == null){
									//return false;
									continue;
								}else{
									if(!isset($arSomaSimples[$avaliacao->id_disciplina])){
										$arSomaSimples[$avaliacao->id_disciplina]['nota'] = 0;
									}
									if(!in_array($avaliacao->id_avaliacao, $notasQueNaoDevemSerSomadas)){
										
										$arSomaSimples[$avaliacao->id_disciplina]['nota'] = $arSomaSimples[$avaliacao->id_disciplina]['nota'] + $avaliacao->st_nota;
										$arSomaSimples[$avaliacao->id_disciplina]['notamax'] = $avaliacao->nu_notamaxima;
										$arSomaSimples[$avaliacao->id_disciplina]['percentualaprovacao'] = $avaliacao->nu_percentualaprovacao;
										if($avaliacao->id_avaliacaorecupera && $avaliacao->st_nota){
											if(isset($arSomaSimples[$avaliacao->id_disciplina]['avaliacao'][$avaliacao->id_avaliacaorecupera])){
												if($avaliacao->st_nota != null){
													$notaAntiga = $arSomaSimples[$avaliacao->id_disciplina]['avaliacao'][$avaliacao->id_avaliacaorecupera]['nota'];
													$arSomaSimples[$avaliacao->id_disciplina]['nota'] = $arSomaSimples[$avaliacao->id_disciplina]['nota'] - $notaAntiga;
												}
											}
											
											$notasQueNaoDevemSerSomadas[] = $avaliacao->id_avaliacaorecupera;
											
										}
										$arSomaSimples[$avaliacao->id_disciplina]['avaliacao'][$avaliacao->id_avaliacao]['nota'] = $avaliacao->st_nota;
										$arSomaSimples[$avaliacao->id_disciplina]['avaliacao'][$avaliacao->id_avaliacao]['recupera'] = $avaliacao->id_avaliacaorecupera;
									}
								}
							}
							
							if($arSomaSimples){
								foreach($arSomaSimples as $idDisciplina => $disciplinaNota){
									$percentualNota = (($disciplinaNota['nota'] / $disciplinaNota['notamax']) * 100);
									$matriculaDisciplinaTO->setNu_aprovafinal($percentualNota);
									$where = "id_matricula = {$aaTO->getId_matricula()} AND id_disciplina = {$idDisciplina}";
									
									if($percentualNota >= $disciplinaNota['percentualaprovacao']){
										$matriculaDisciplinaTO->setId_evolucao(12); // concluinte
									}else{
										$matriculaDisciplinaTO->setId_evolucao(13); // cursando								
									}
									
									$matriculaDAO = new MatriculaDAO();
									$matriculaDAO->editarMatriculaDisciplina($matriculaDisciplinaTO, $where);
									
								}
								$matriculaDisciplinaConclusaoTO = new MatriculaDisciplinaTO();
								$matriculaDisciplinaConclusaoTO->setId_matricula($aaTO->getId_matricula());
								
								$matriculaTO = new MatriculaTO();
								$matriculaTO->setId_matricula($aaTO->getId_matricula());
								$ng_matricula = new \G2\Negocio\Matricula();
								$ng_matricula->alocarAutomatico($matriculaTO);
								
								$this->concluirMatricula($matriculaDisciplinaConclusaoTO);
								return true;
							}
						
						}
					}
				}
			}
			
		}catch (Zend_Exception $e){
			throw $e;
			return $this->mensageiro->setMensageiro('Erro ao Processar Nota para esta Avaliação!',Ead1_IMensageiro::ERRO,$e->getTraceAsString());
		}
	}
	
	/**
	 * Método que verifica se todas as matrículas disciplinas obrigatórias estão com evolução concluída para a 
	 * 	matrícula em questão, se estiverem marca a matrícula como concluinte na evolução.
	 * @param MatriculaDisciplinaTO $matriculaDisciplinaTO
	 */
	private function concluirMatricula(MatriculaDisciplinaTO $matriculaDisciplinaTO){
		$matriculaDiscplinaORM = new MatriculaDisciplinaORM();
		$matriculaDisciplinaTO->setBl_obrigatorio(true);
		$matriculasDisciplinas = $matriculaDiscplinaORM->consulta($matriculaDisciplinaTO);
		if($matriculasDisciplinas){
			$concluir = true;
			foreach($matriculasDisciplinas as $matriculaDisciplinaAtual){
				if($matriculaDisciplinaAtual->getId_evolucao() != 12){
					$concluir = false;
				}
			}
			
			if($concluir){
				$matriculaTO = new MatriculaTO();
				$matriculaTO->setId_matricula($matriculaDisciplinaTO->getId_matricula());
				$matriculaTO->setDt_concluinte(new Zend_Date(null,Zend_Date::ISO_8601));
				$matriculaTO->setId_evolucao(15); //concluinte
				
				$matriculaDAO = new MatriculaDAO();
				if(!$matriculaDAO->editarMatricula($matriculaTO)){
					throw new Exception("ERRO ao alterar a evolução da Matrícula!",0);
				}
			}
		}else{
			throw new Exception("Sem Matrícula Disciplina Cadastrada!",0);
		}
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO){
		if($acTO->getId_avaliacaoconjunto()){
			return $this->editarAvaliacaoConjunto($acTO);
		}
		return $this->cadastrarAvaliacaoConjunto($acTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a relacao do conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO){
		if($acrTO->getId_avaliacaoconjuntorelacao()){
			return $this->editarAvaliacaoConjuntoRelacao($acrTO);
		}
		return $this->cadastrarAvaliacaoConjuntoRelacao($acrTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita um agendamento de avaliacao
	 * @param AvaliacaoAgendamentoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO){
		if($aaTO->getId_avaliacaoagendamento()){
			return $this->editarAvaliacaoAgendamento($aaTO);
		}
		return $this->cadastrarAvaliacaoAgendamento($aaTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita um conjunto de avaliacao do projeto pedagogico
	 * @param AvaliacaoConjuntoProjetoTO $acpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO){
		if($acpTO->getId_avaliacaoconjuntoprojeto()){
			return $this->editarAvaliacaoConjuntoProjeto($acpTO);
		}
		return $this->cadastrarAvaliacaoConjuntoProjeto($acpTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita um conjunto de avaliacao de sala de aula
	 * @param AvaliacaoConjuntoSalaTO $acsTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO){
		if($acsTO->getId_avaliacaoconjuntosala()){
			return $this->editarAvaliacaoConjuntoSala($acsTO);
		}
		return $this->cadastrarAvaliacaoConjuntoSala($acsTO);
	}
	
	/**
	 * Método que salva a integração da avaliacao
	 * @param array $arrAvaliacaoIntegracaoTO
	 * @param AvaliacaoTO $avaliacaoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoIntegracao($arrAvaliacaoIntegracaoTO,AvaliacaoTO $avaliacaoTO){
		try{
			if(!$avaliacaoTO->getId_avaliacao()){
				THROW new Zend_Exception('O id_avaliacao Não veio setado!');
			}
			if(!is_array($arrAvaliacaoIntegracaoTO)){
				THROW new Zend_Exception('O Parametro Recebido não é um array');
			}
			$this->dao->beginTransaction();
			if(empty($arrAvaliacaoIntegracaoTO)){
				$this->dao->deletarAvaliacaoIntegracao(new AvaliacaoIntegracaoTO(),"id_avaliacao = ".$avaliacaoTO->getId_avaliacao()." AND id_sistema = ".SistemaTO::SISTEMA_AVALIACAO);
			}else{
				$delected = false;
				foreach ($arrAvaliacaoIntegracaoTO as $avaliacaoIntegracaoTO){
					if(!($avaliacaoIntegracaoTO instanceof AvaliacaoIntegracaoTO)){
						THROW new Zend_Exception('Um dos Itens do Array não é uma instancia de AvaliacaoIntegracaoTO');
					}
					if(!$delected){
						$delected = true;
						$this->dao->deletarAvaliacaoIntegracao(new AvaliacaoIntegracaoTO(),"id_avaliacao = ".$avaliacaoIntegracaoTO->getId_avaliacao()." AND id_sistema = ".$avaliacaoIntegracaoTO->getId_sistema());
					}
					$avaliacaoIntegracaoTO->setId_usuariocadastro($avaliacaoIntegracaoTO->getSessao()->id_usuario);
					$this->dao->cadastrarAvaliacaoIntegracao($avaliacaoIntegracaoTO);
				}
			}
			$this->dao->commit();
			$this->mensageiro->setMensageiro('Integração Salva com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro ao Salvar Integração das Avaliações!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a aplicacao de uma avaliacao
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		if($apTO->getId_avaliacaoaplicacao()){
			return $this->editarAvaliacaoAplicacao($apTO);
		}
		return $this->cadastrarAvaliacaoAplicacao($apTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a referencia do conjunto de avaliacao
	 * @param AvaliacaoConjuntoReferenciaTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO){
		if($acrTO->getId_avaliacaoconjuntoreferencia()){
			return $this->editarAvaliacaoConjuntoReferencia($acrTO);
		}
		return $this->cadastrarAvaliacaoConjuntoReferencia($acrTO);
	}
	
	/**
	 * Metodo que cadastra/edita a avaliacao aplicacao e recadastra a avaliacao aplicacao relacao
	 * @param AvaliacaoAplicacaoTO $aaTO
	 * @param array $arrAvaliacaoAplicacaoRelacaoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO, array $arrAvaliacaoAplicacaoRelacaoTO){
		try{
			$this->dao->beginTransaction();
			$mensageiro = $this->salvarAvaliacaoAplicacao($aaTO);
			if($mensageiro->getTipo() == Ead1_IMensageiro::ERRO){
				THROW new Zend_Exception($mensageiro->getCodigo());
			}else if($mensageiro->getTipo() == Ead1_IMensageiro::AVISO){
				THROW new Zend_Exception($mensageiro->getMensagem());
			}
			$delected = false;
			foreach ($arrAvaliacaoAplicacaoRelacaoTO as $aarTO){
				if(!$delected){
					$this->dao->deletarAvaliacaoAplicacaoRelacao(new AvaliacaoAplicacaoRelacaoTO(),'id_avaliacaoaplicacao = '.$aaTO->getId_avaliacaoaplicacao());
					$delected = true;
				}
				if(!($aarTO instanceof AvaliacaoAplicacaoRelacaoTO)){
					THROW new Zend_Exception('Um dos items do array não é uma instancia de AvaliacaoAplicacaoRelacaoTO');
				}
				$aarTO->setId_avaliacaoaplicacao($aaTO->getId_avaliacaoaplicacao());
				$this->dao->cadastrarAvaliacaoAplicacaoRelacao($aarTO);
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($aaTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Salvar a Aplicação da Avaliação e suas Relações',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que salva a avaliacaoConjunto e o array de relações
	 * @param AvaliacaoConjuntoTO $acTO
	 * @param array $arrAvaliacaoConjuntoRelacaoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrAvaliacaoConjuntoRelacao(AvaliacaoConjuntoTO $acTO, array $arrAvaliacaoConjuntoRelacaoTO){
		try{
			$this->dao->beginTransaction();
			$mensageiro = $this->salvarAvaliacaoConjunto($acTO);
			if($mensageiro->getTipo() == Ead1_IMensageiro::ERRO){
				THROW new Zend_Exception($mensageiro->getCodigo());
			}
			foreach ($arrAvaliacaoConjuntoRelacaoTO as $acrTO){
				if(!($acrTO instanceof AvaliacaoConjuntoRelacaoTO)){
					THROW new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoRelacaoTO');
				}
				$acrTO->setId_avaliacaoconjunto($acTO->getId_avaliacaoconjunto());
				//o
				$mensageiro = $this->salvarAvaliacaoConjuntoRelacao($acrTO);
				if($mensageiro->getTipo() == Ead1_IMensageiro::ERRO){
					THROW new Zend_Exception($mensageiro->getCodigo());
				}
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($acTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack(); 
			return $this->mensageiro->setMensageiro('Erro ao Salvar o Conjunto de Avaliações e suas Relações',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	public function editarArrAvaliacaoConjuntoRelacao(AvaliacaoConjuntoTO $acTO, array $arrAvaliacaoConjuntoRelacaoTO){
		try{
			$this->dao->beginTransaction();
			$mensageiro = $this->editarAvaliacaoConjunto( $acTO );
			if( $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO ){
				throw new Zend_Exception($mensageiro->getCodigo());
			}
			
			$this->dao->deletarAvaliacaoConjuntoRelacao( new AvaliacaoConjuntoRelacaoTO(), 'id_avaliacaoconjunto = '.  $acTO->id_avaliacaoconjunto );
			
			foreach ( $arrAvaliacaoConjuntoRelacaoTO as $acrTO ){
				if( !( $acrTO instanceof AvaliacaoConjuntoRelacaoTO ) ){
					throw new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoRelacaoTO');
				}
				$acrTO->setId_avaliacaoconjuntorelacao(NULL);
				$acrTO->setId_avaliacaoconjunto($acTO->getId_avaliacaoconjunto());
				$mensageiro = $this->salvarAvaliacaoConjuntoRelacao( $acrTO );
				if( $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO ){
					THROW new Zend_Exception($mensageiro->getCodigo());
				}
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro($acTO,Ead1_IMensageiro::SUCESSO);
		}catch (Exception $e){
			$this->dao->rollBack(); 
			return $this->mensageiro->setMensageiro('Erro ao Salvar o Conjunto de Avaliações e suas Relações',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que salva as referencias do conjunto de avaliacao
	 * @param $arrAvaliacaoConjuntoRefenciaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrAvaliacaoConjuntoReferencia($arrAvaliacaoConjuntoRefenciaTO){
		try{
			$this->dao->beginTransaction();
			$dt_hoje = new Zend_Date(null,Zend_Date::ISO_8601);
			foreach ($arrAvaliacaoConjuntoRefenciaTO as $acrTO){
				if(!($acrTO instanceof AvaliacaoConjuntoReferenciaTO)){
					THROW new Zend_Exception('Um dos items do array não é instancia de AvaliacaoConjuntoReferenciaTO');
				}
				if(!$this->comparaData($dt_hoje, $acrTO->getDt_inicio())){
					THROW new Zend_Validate_Exception("Uma das Datas de Início é Inferior a Data de Hoje.");
				}
				if($acrTO->getDt_fim()){
					if(!$this->comparaData($acrTO->getDt_inicio(), $acrTO->getDt_fim())){
						THROW new Zend_Validate_Exception("A Data de Término Não pode Ser Menor que a Data de Início.");
					}
				}
				$mensageiro = $this->salvarAvaliacaoConjuntoReferencia($acrTO);
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					THROW new Zend_Exception($mensageiro->getCodigo());
				}
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Referencias do Conjunto de Avaliação Salvas com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->dao->rollBack(); 
			return $this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack(); 
			return $this->mensageiro->setMensageiro('Erro ao Salvar Referencias do Conjunto de Avaliação',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a avaliacao disciplina
	 * @param AvaliacaoDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO){
		try{
			$id_avaliacaodisciplina = $this->dao->cadastrarAvaliacaoDisciplina($adTO);
			$adTO->setId_avaliacao($id_avaliacaodisciplina);
			return $this->mensageiro->setMensageiro($adTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar vínculo de Avaliação com Disciplina!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a avaliacao
	 * @param AvaliacaoTO $aTO
	 * @param array $arrDisciplinas
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAvaliacao(AvaliacaoTO $aTO, array $arrDisciplinas){
		try{
			$aTO->setId_usuariocadastro($aTO->getSessao()->id_usuario);
			if(!$aTO->getId_entidade()){
				$aTO->setId_entidade($aTO->getSessao()->id_entidade);
			}
			$id_avaliacao = $this->dao->cadastrarAvaliacao($aTO);
			$aTO->setId_avaliacao($id_avaliacao);
			
			foreach ($arrDisciplinas as $avaliacaoDisciplina){
				$adTO = new AvaliacaoDisciplinaTO();
				$adTO->setId_avaliacao($id_avaliacao);
				$adTO->setId_disciplina($avaliacaoDisciplina->id_disciplina);
				$this->cadastrarAvaliacaoDisciplina($adTO);
			}
			
			return $this->mensageiro->setMensageiro($aTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra um Conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO){
		try{
			$acTO->setId_usuariocadastro($acTO->getSessao()->id_usuario);
			if(!$acTO->getId_entidade()){
				$acTO->setId_entidade($acTO->getSessao()->id_entidade);
			}
			$id_avaliacaoconjunto = $this->dao->cadastrarAvaliacaoConjunto($acTO);
			$acTO->setId_avaliacaoconjunto($id_avaliacaoconjunto);
			return $this->mensageiro->setMensageiro($acTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a relacao de um Conjunto de avaliacao
	 * @param AvaliacaoConjuntoRelacaoTO $acrTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO){
		try{
			$id_avaliacaoconjuntorelacao = $this->dao->cadastrarAvaliacaoConjuntoRelacao($acrTO);
			$acrTO->setId_avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao);
			return $this->mensageiro->setMensageiro($acrTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Relação do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a aplicacao da avaliacao
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		try{
			if(!$apTO->getId_entidade()){
				$apTO->setId_entidade($apTO->getSessao()->id_entidade);
			}
			$apTO->setId_usuariocadastro($apTO->getSessao()->id_usuario);
			$id_avaliacaoaplicacao = $this->dao->cadastrarAvaliacaoAplicacao($apTO);
			$apTO->setId_avaliacaoaplicacao($id_avaliacaoaplicacao);
			return $this->mensageiro->setMensageiro($apTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Aplicação da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra um agendamento de avaliacao
	 * @param AvaliacaoAgendamentoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoAgendamento( AvaliacaoAgendamentoTO $aaTO ){
		try{
			$vwAvaliacaoAplicacaoAgendamentoTO	= Ead1_TO_Dinamico::mergeTO( new VwAvaliacaoAplicacaoAgendamentoTO(), clone $aaTO );
			$vwAvaliacaoAplicacaoAgendamentoTO->dt_agendamento = null;
			$vwAvaliacaoAplicacaoAgendamentoTO->id_situacao  = null;
			$vwAvaliacaoAplicacaoAgendamentoTO->bl_ativo = null;
			$mensageiro							= $this->retornarVwAvaliacaoAplicacaoAgendamento( $vwAvaliacaoAplicacaoAgendamentoTO );
			if( $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO ) {
				throw new DomainException( 'Não foi encontrado nenhuma avaliação disponível para agendamento' );
			}
			
			$vwAvaliacaoAplicacaoAgendamentoTO	= $mensageiro->mensagem[0];
			
			if( $vwAvaliacaoAplicacaoAgendamentoTO->bl_unica != false ) {
				
				$diasSemana	= $this->retornaDiasSemanaAplicacaoAvaliacao( $vwAvaliacaoAplicacaoAgendamentoTO->id_horarioaula );
				if( !array_key_exists( $aaTO->dt_agendamento->getWeekday()->toString( Zend_Date::WEEKDAY_DIGIT ), $diasSemana  ) ) {
					throw new DomainException( 'A data de agendamento deve ser entre os dias da semana: '. implode( ', ', $diasSemana ) );
				}
			}
			
			$aaTO->setId_usuariocadastro( $aaTO->getSessao()->id_usuario );
			$aaTO->setDt_cadastro( date('Y-m-d') );
			$aaTO->setId_situacao(AvaliacaoAgendamentoTO::SITUACAO_AGENDADO);
			$id_avaliacaoagendamento = $this->dao->cadastrarAvaliacaoAgendamento($aaTO);
			$aaTO->setId_avaliacaoagendamento($id_avaliacaoagendamento);
			
			$wsBO = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
			$mensageiroWS = null;
			if($wsBO->getEntidadeIntegracaoTO(SistemaTO::SISTEMA_AVALIACAO)){
				$mensageiroWS = $wsBO->cadastrarAvaliacaoAluno($aaTO);
				if($mensageiroWS->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($mensageiroWS->getCurrent());
				}
			}
			
			return $this->mensageiro->setMensageiro($aaTO,Ead1_IMensageiro::SUCESSO, $mensageiroWS);
			
		}catch ( DomainException $domainExc ){
			return $this->mensageiro->setMensageiro( $domainExc->getMessage(), Ead1_IMensageiro::AVISO ) ;
		}catch ( Zend_Exception $e ){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Agendamento da Avaliação! ' . $e->getMessage(),Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra um conjunto de avaliacao de projeto pedagogico
	 * @param AvaliacaoConjuntoProjetoTO $acpTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO){
		try{
			$id_avaliacaoconjuntoprojeto = $this->dao->cadastrarAvaliacaoConjuntoProjeto($acpTO);
			$acpTO->setId_avaliacaoconjuntoprojeto($id_avaliacaoconjuntoprojeto);
			return $this->mensageiro->setMensageiro($acpTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Agendamento da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra um conjunto de avaliacao de sala de aula
	 * @param AvaliacaoConjuntoSalaTO $acsTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO){
		try{
			$id_avaliacaoconjuntosala = $this->dao->cadastrarAvaliacaoConjuntoSala($acsTO);
			$acsTO->setId_avaliacaoconjuntosala($id_avaliacaoconjuntosala);
			return $this->mensageiro->setMensageiro($acsTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de Avaliação da Sala de Aula!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a referencia de um conjunto de avaliacao
	 * @param AvaliacaoConjuntoReferenciaTO $acrTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO){
		try{
			$id_avaliacaoconjuntoreferencia = $this->dao->cadastrarAvaliacaoConjuntoReferencia($acrTO);
			$acrTO->setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia);
			return $this->mensageiro->setMensageiro($acrTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Referencia do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra a relação de aplicacao da avaliacao
	 * @param AvaliacaoAplicacaoRelacaoTO $aprTO
	 * @return Ead1_Mensageiro
	 */
		public function cadastrarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO){
		try{
			$this->dao->cadastrarAvaliacaoAplicacaoRelacao($aprTO);
			return $this->mensageiro->setMensageiro('Relação da Aplicação da Avaliação Cadastrada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de Avaliação do Projeto Pedagógico!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita a avaliacao
	 * @param AvaliacaoTO $aTO
	 * @param array $arrDisciplinas
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacao(AvaliacaoTO $aTO, array $arrDisciplinas){
		
		$this->dao->beginTransaction();
		
		try {
			
			$this->dao->editarAvaliacao( $aTO );
			$this->dao->deletarAvaliacaoDisciplina( new AvaliacaoDisciplinaTO(), ' id_avaliacao = '. $aTO->id_avaliacao );
			foreach ( $arrDisciplinas as $avaliacaoDisciplina ){
				$adTO = new AvaliacaoDisciplinaTO();
				$adTO->setId_avaliacao($aTO->getId_avaliacao());
				$adTO->setId_disciplina($avaliacaoDisciplina->id_disciplina);
				$mensageiro	= $this->cadastrarAvaliacaoDisciplina($adTO); 
				if( $mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO ) {
					throw new ErrorException( $mensageiro->mensagem[0] );
				}
			}
			
			$this->mensageiro->setMensageiro( 'Avaliação editada com sucesso!', Ead1_IMensageiro::SUCESSO );
			$this->dao->commit();
			
		} catch ( Exception $e) {
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro ao Cadastrar Avaliação!', Ead1_IMensageiro::ERRO,$e->getMessage() );
		}
		
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que edita um Conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO){
		try{
			$this->dao->editarAvaliacaoConjunto( $acTO );
			return $this->mensageiro->setMensageiro('Conjunto de Avaliação Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita uma relacao de Conjunto de avaliacao
	 * @param AvaliacaoConjuntoRelacaoTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO){
		try{
			$this->dao->editarAvaliacaoConjuntoRelacao($acrTO);
			return $this->mensageiro->setMensageiro('Relação do Conjunto de Avaliação Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Relação do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita uma aplicacao de uma avaliacao
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		try{
			$this->dao->editarAvaliacaoAplicacao($apTO);
			return $this->mensageiro->setMensageiro('Aplicação da Avaliação Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar a Aplicação de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita um agendamento de uma avaliacao
	 * @param AvaliacaoAgendamentoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO){
		try{
			$this->dao->editarAvaliacaoAgendamento($aaTO);
			return $this->mensageiro->setMensageiro('Agendamento da Avaliação Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Agendamento de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita um conjunto de avaliacao de sala de aula
	 * @param AvaliacaoConjuntoSalaTO $acsTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO){
		try{
			$this->dao->editarAvaliacaoConjuntoSala($acsTO);
			return $this->mensageiro->setMensageiro('Conjunto de Avaliação da Sala de Aula Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Conjunto de Avaliação da Sala de Aula!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita um conjunto de avaliacao de projeto pedagogico
	 * @param AvaliacaoConjuntoProjetoTO $acpTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO){
		try{
			$this->dao->editarAvaliacaoConjuntoProjeto($acpTO);
			return $this->mensageiro->setMensageiro('Conjunto de Avaliação de Projeto Pedagógico Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Conjunto de Avaliação Projeto Pedagógico!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita uma referencia de  conjunto de avaliacao
	 * @param AvaliacaoConjuntoReferenciaTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO){
		try{
			$this->dao->editarAvaliacaoConjuntoReferencia($acrTO);
			return $this->mensageiro->setMensageiro('Referencia do Conjunto de Avaliação Editada com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Referencia do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui a avaliacao
	 * @param AvaliacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacao(AvaliacaoTO $aTO){
		try{
			$this->dao->deletarAvaliacao($aTO);
			return $this->mensageiro->setMensageiro('Avaliação Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui o conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO){
		try{
			$this->dao->deletarAvaliacaoConjunto($acTO);
			return $this->mensageiro->setMensageiro('Conjunto de Avaliação Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui a relação do conjunto de avaliacao
	 * @param AvaliacaoConjuntoRelacaoTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO){
		try{
			$this->dao->deletarAvaliacaoConjuntoRelacao($acrTO);
			return $this->mensageiro->setMensageiro('Relação do Conjunto de Avaliação Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Relação do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que exclui uma aplicacao de uma avaliacao
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
		public function deletarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		try{
			$this->dao->deletarAvaliacaoAplicacao($apTO);
			return $this->mensageiro->setMensageiro('Aplicação da Avaliação Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Aplicação da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que edita uma aplicacao de uma avaliacao colocando apenas o bl_ativo para 0
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
		public function desativarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		try{
			$this->dao->beginTransaction();
			if(!$apTO->getId_avaliacaoaplicacao()){
				THROW new Zend_Exception('O id_avaliacaoaplicacao não pode vir vazio!');
			}
			$orm = new AvaliacaoAplicacaoORM();
			$orm->update(array('bl_ativo' => 0), 'id_avaliacaoaplicacao = '.$apTO->getId_avaliacaoaplicacao());
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Aplicação da Avaliação Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Excluir Aplicação da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui a relação de aplicacao da avaliacao
	 * @param AvaliacaoAplicacaoRelacaoTO $aprTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO){
		try{
			$this->dao->deletarAvaliacaoAplicacaoRelacao($aprTO);
			return $this->mensageiro->setMensageiro('Relação da Aplicação da Avaliação Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Relação da Aplicação da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui um agendamento de avaliacao
	 * @param AvaliacaoAgendamentoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO){
		try{
			$this->dao->deletarAvaliacaoAgendamento($aaTO);
			return $this->mensageiro->setMensageiro('Agendamento da Avaliação Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Agendamento da Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui um conjunto de avaliacao de projeto pedagogico
	 * @param AvaliacaoConjuntoProjetoTO $acpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO){
		try{
			$this->dao->deletarAvaliacaoConjuntoProjeto($acpTO);
			return $this->mensageiro->setMensageiro('Conjunto da Avaliação de Sala de Aula Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto da Avaliação de Sala de Aula!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui um conjunto de avaliacao de sala de aula
	 * @param AvaliacaoConjuntoSalaTO $acsTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO){
		try{
			$this->dao->deletarAvaliacaoConjuntoSala($acsTO);
			return $this->mensageiro->setMensageiro('Conjunto da Avaliação de Projeto Pedagógico Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Conjunto da Avaliação de Projeto Pedagógico!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}

	/**
	 * Metodo que exclui a referencia de  conjunto de avaliacao
	 * @param AvaliacaoConjuntoReferenciaTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO){
		try{
			$this->dao->deletarAvaliacaoConjuntoReferencia($acrTO);
			return $this->mensageiro->setMensageiro('Referencia do Conjunto da Avaliação Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Referencia do Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Deleta os vínculos entre avaliação e disciplina
	 * @param AvaliacaoDisciplinaTO $avaliacaoDisciplinaTO
	 */
	public function deletarAvaliacaoDisciplina( AvaliacaoDisciplinaTO $avaliacaoDisciplinaTO ) {}

	/**
	 * Metodo que verifica se existem vinculos com a avaliacao e exclui
	 * @param AvaliacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function verificaExcluiAvaliacao(AvaliacaoTO $to){
		try{
			$this->dao->beginTransaction();
			$arrParams = array('id_avaliacao' => $to->getId_avaliacao());
			$this->dao->deletarAvaliacaoDisciplina(new AvaliacaoDisciplinaTO(), "id_avaliacao = ".$to->getId_avaliacao());
			$this->dao->deletarAvaliacao($to);
			$this->dao->commit();
			$this->mensageiro->setMensageiro("Avaliação Excluida com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("A Avaliação não pode ser Excluida!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}

	/**
	 * Metodo que verifica se existem vinculos com a avaliacao conjunto e exclui
	 * @param AvaliacaoConjuntoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function verificaExcluiAvaliacaoConjunto(AvaliacaoConjuntoTO $to){
		try{
			$this->dao->beginTransaction();
			$arrParams = array('id_avaliacaoconjunto' => $to->getId_avaliacaoconjunto());
			$conjuntoReferenciaORM = new AvaliacaoConjuntoReferenciaORM();
			if($conjuntoReferenciaORM->consulta(new AvaliacaoConjuntoReferenciaTO($arrParams),false,true)){
				THROW new Zend_Validate_Exception("Não é Possivel Excluir o Conjunto de Avaliação pois Existem Referencias Vinculadas a Ele!");
			}
			$this->dao->deletarAvaliacaoConjuntoRelacao(new AvaliacaoConjuntoRelacaoTO(),"id_avaliacaoconjunto = ".$to->getId_avaliacaoconjunto());
			$this->dao->deletarAvaliacaoConjunto($to);
			$this->dao->commit();
			$this->mensageiro->setMensageiro("Conjunto de Avaliação Excluido com Sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::ERRO,$e->getMessage());
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("O Conjunto de Avaliação não pode ser Excluido!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna avaliação
	 * @param AvaliacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacao(AvaliacaoTO $aTO){
		try{
			if(!$aTO->getId_entidade()){
				$aTO->setId_entidade($aTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarAvaliacao($aTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna avaliação disciplina
	 * @param AvaliacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoDisciplina(AvaliacaoTO $aTO){
		try{
			$vwAvaliacaoDisciplinaTO = new VwAvaliacaoDisciplinaTO();
			$vwAvaliacaoDisciplinaTO->setId_avaliacao($aTO->getId_avaliacao());
			$dados = $this->dao->retornarAvaliacaoDisciplina($vwAvaliacaoDisciplinaTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoDisciplinaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar vínculos de Disciplina com Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de avaliação
	 * @param AvaliacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoAvaliacao(TipoAvaliacaoTO $taTO){
		try{
			$dados = $this->dao->retornarTipoAvaliacao($taTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoAvaliacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a relacao do conjunto de avaliacoes
	 * @param AvaliacaoConjuntoRelacaoTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO){
		try{
			$dados = $this->dao->retornarAvaliacaoConjuntoRelacao($acrTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoRelacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna tipo de calculo de avaliacao
	 * @param TipoCalculoAvaliacaoTO $tcaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCalculoAvaliacao(TipoCalculoAvaliacaoTO $tcaTO){
		try{
			$dados = $this->dao->retornarTipoCalculoAvaliacao($tcaTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoCalculoAvaliacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao
	 * @param AvaliacaoConjuntoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO){
		try{
			if(!$acTO->getId_entidade()){
				$acTO->setId_entidade($acTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarAvaliacaoConjunto($acTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a aplicacao da avaliacao
	 * @param AvaliacaoAplicacaoTO $apTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO){
		try{
			if(!$apTO->getId_entidade()){
				$apTO->setId_entidade($apTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarAvaliacaoAplicacao($apTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAplicacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Aplicação da Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a relação de aplicacao da avaliacao
	 * @param AvaliacaoAplicacaoRelacaoTO $aprTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO){
		try{
			$dados = $this->dao->retornarAvaliacaoAplicacaoRelacao($aprTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAplicacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Relação da Aplicação da Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um agendamento de avaliacao
	 * @param AvaliacaoAgendamentoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO){
		try{
			$dados = $this->dao->retornarAvaliacaoAgendamento($aaTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoAgendamentoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Agendamento da Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao de sala de aula
	 * @param AvaliacaoConjuntoSalaTO $acsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO){
		try{
			$dados = $this->dao->retornarAvaliacaoConjuntoSala($acsTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoSalaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalicação da Sala de Aula!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao de projeto pedagogico
	 * @param AvaliacaoConjuntoProjetoTO $acpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO){
		try{
			$dados = $this->dao->retornarAvaliacaoConjuntoProjeto($acpTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoProjetoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avalicação de Projeto Pedagógico!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a referencia conjunto de avaliacao
	 * @param AvaliacaoConjuntoReferenciaTO $acrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO){
		try{
			$dados = $this->dao->retornarAvaliacaoConjuntoReferencia($acrTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new AvaliacaoConjuntoReferenciaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Referencia do Conjunto de Avalicação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao, agendamento, aluno, projeto pedagógico, entidade
	 * @param VwAvaliacaoAgendamentoTO $vaaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoAgendamento(VwAvaliacaoAgendamentoTO $vaaTO){
		return new Ead1_Mensageiro($vaaTO->fetch(false, false, false, true, true));
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao, aplicacao e agendamento
	 * @param VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO
	 * @see AvaliacaoRO::retornarVwAvaliacaoAplicacaoAgendamento()
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoAplicacaoAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO){
		try{
			$dados = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento($vaaaTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAplicacaoAgendamentoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna as avaliacos disponiveis para agendamento
	 * @param VwAvaliacaoAplicacaoAgendamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornaAvaliacaoAplicacaoDisponiveisAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $to){
		try{
			$dados = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento($to)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			//Removendo os já agendados
			foreach ($dados as $index => $dado){
				if($dado['id_situacao'] == AvaliacaoAgendamentoTO::SITUACAO_AGENDADO){
					unset($dados[$index]);
				}
			}
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$arrDadosVerifica = $this->dao->retornarVwAvaliacaoAplicacaoAgendamento(
																						new VwAvaliacaoAplicacaoAgendamentoTO(
																							array('id_matricula' => $to->getId_matricula(),
																							'id_situacao' => AvaliacaoAgendamentoTO::SITUACAO_AGENDADO)
																						)
																					)->toArray();
			//Verificando se existe já algum agendamento para o Dia a agendar
			if(!empty($arrDadosVerifica)){
				foreach ($dados as $index => $dado){
					foreach ($arrDadosVerifica as $verifica){
						if(!empty($verifica['dt_agendamento'])){
							if(	$dado['id_avaliacao'] == $verifica['id_avaliacao']){
								unset($dados[$index]);
							}
						}
					}
				}
			}
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$arrDadosAvaliacao = $this->dao->retornarVwAvaliacaoAluno(
																		new VwAvaliacaoAlunoTO(
																			array('id_matricula' => $to->getId_matricula(),
																					'id_tipoavaliacao' => TipoAvaliacaoTO::RECUPERACAO)
																		)
																	)->toArray();
			if(!empty($arrDadosAvaliacao)){
				foreach ($dados as $index => $dado){
					foreach ($arrDadosAvaliacao as $avaliacao){
						if($dado['id_avaliacao'] == $avaliacao['id_avaliacao'] && $avaliacao['id_tipoavaliacao'] == TipoAvaliacaoTO::RECUPERACAO){
							foreach ($dados as $confere){
								if($confere['id_avaliacao'] == $avaliacao['id_avaliacaorecupera'] && empty($avaliacao['st_nota'])){
									unset($dados[$index]);
								}
							}
						}
					}
				}
			}
			
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAplicacaoAgendamentoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de avaliacao, matricula, notas e modulo
	 * @param VwAvaliacaoAlunoTO $vaaTO
	 * @see AvaliacaoRO::retornarVwAvaliacaoAluno()
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoAluno(VwAvaliacaoAlunoTO $vaaTO){
		try{
			$dados = $this->dao->retornarVwAvaliacaoAluno($vaaTO)->toArray();
			//ID_FIXOS
			$TIPO_AVALIACAO_RECUPERACAO = 2;
			//
			foreach ($dados as $index => $dado){
				if($dado['id_tipoavaliacao'] == $TIPO_AVALIACAO_RECUPERACAO){
					foreach ($dados as $valida){
						if($dado['id_avaliacaorecupera'] == $valida['id_avaliacao']){
							if($valida['st_nota']){
								$dado['bl_editavel'] = 1;
							}else{
								$dado['bl_editavel'] = 0;
							}
						}
					}
				}else{
					$dado['bl_editavel'] = 1;
				}
				$dados[$index] = $dado;
			}
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoAlunoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna um conjunto de disciplina, matricula, notas e modulo
	 * @param VwAvaliacaoAlunoTO $vaaTO
	 * @see AvaliacaoRO::retornarVwAvaliacaoAluno()
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoAlunoDisciplina(VwAvaliacaoAlunoTO $vaaTO){
		try{
			$dados = array();
			$dadosModulo = $this->dao->retornarVwAvaliacaoAlunoModulo($vaaTO)->toArray();
			if(empty($dadosModulo)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Avaliação Encontrado!',Ead1_IMensageiro::AVISO);
			}
			$dadosModulo = Ead1_TO_Dinamico::encapsularTo($dadosModulo, new VwAvaliacaoAlunoTO());
			foreach ($dadosModulo as $index1 => $dadoModulo){
				$vaaTO->setId_modulo($dadoModulo->getId_modulo());
				$dadosDisciplina = $this->dao->retornarVwAvaliacaoAlunoDisciplina($vaaTO)->toArray();
				$dados[$index1]['modulo'] = $dadoModulo;
				$dadosDisciplina = Ead1_TO_Dinamico::encapsularTo($dadosDisciplina, new VwAvaliacaoAlunoTO());
				foreach ($dadosDisciplina as $index2 => $dadoDisciplina){
					$dadoDisciplina->setSt_nota(0);
					$vaaTO->setId_disciplina($dadoDisciplina->getId_disciplina());
					$dadosAvaliacao = $this->dao->retornarVwAvaliacaoAluno($vaaTO)->toArray();
					foreach ($dadosAvaliacao as $dadoAvaliacao){
						if($dadoAvaliacao['st_nota'] != null){
							$dadoDisciplina->setSt_nota($dadoDisciplina->getSt_nota() + $dadoAvaliacao['st_nota']);
						}else{
							$dadoDisciplina->setSt_nota('');
						}
					}
					$dados[$index1]['disciplinas'][$index2] = $dadoDisciplina;
				}
			}
			return $this->mensageiro->setMensageiro($dados,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Dados!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a view de avaliacao
	 * @param VwPesquisaAvaliacaoTO $vwpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwPesquisaAvaliacao(VwPesquisaAvaliacaoTO $vwpaTO){
		try{
			if(!$vwpaTO->getId_entidade()){
				$vwpaTO->setId_entidade($vwpaTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarVwPesquisaAvaliacao($vwpaTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPesquisaAvaliacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna as Relações das Avaliações da aplicação no formato de PesquisaAvaliacaoTO
	 * @param AvaliacaoAplicacaoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAvaliacoesAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO){
		try{
			$this->dao->beginTransaction();
			$pesquisarDAO = new PesquisarDAO();
			$relacoes = $this->dao->retornarAvaliacaoAplicacaoRelacao(new AvaliacaoAplicacaoRelacaoTO(),'id_avaliacaoaplicacao = '.$aaTO->getId_avaliacaoaplicacao())->toArray();
			if(empty($relacoes)){
				$this->dao->commit();
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$where = '';
			$arrWhere = array();
			foreach ($relacoes as $relacao){
				$arrWhere[$relacao['id_avaliacao']] = $relacao['id_avaliacao'];
			}
			$implode = implode(' ,', $arrWhere);
			$where = 'id_avaliacao in ('.$implode.')';
			$avaliacoes = $pesquisarDAO->retornarPesquisaAvaliacao(new PesquisaAvaliacaoTO(),$where)->toArray();
			if(empty($avaliacoes)){
				THROW new Zend_Exception('Os ids das Avaliações na tb_avaliacaoaplicacaorelacao estão diferentes ');
			}
			$this->dao->commit();
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($avaliacoes, new PesquisaAvaliacaoTO()));
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			return $this->mensageiro->setMensageiro('Erro ao Retornar a Relação da Aplicação das Avaliações');
		}
	}
	
	/**
	 * Metodo que retorna a view de avaliacaoconjunto
	 * @param VwAvaliacaoConjuntoTO $vwacTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoConjunto(VwAvaliacaoConjuntoTO $vwacTO){
		try{
			if(!$vwacTO->getId_entidade()){
				$vwacTO->setId_entidade($vwacTO->getSessao()->id_entidade);
			}
			$dados = $this->dao->retornarVwAvaliacaoConjunto($vwacTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a view de avaliacaoconjuntorelacao
	 * @param VwAvaliacaoConjuntoRelacaoTO $vwacrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoConjuntoRelacao(VwAvaliacaoConjuntoRelacaoTO $vwacrTO,$where = null){
		try{
			$dados = $this->dao->retornarVwAvaliacaoConjuntoRelacao($vwacrTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoRelacaoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação Relação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a view de avaliacaoconjuntoreferencia
	 * @param VwAvaliacaoConjuntoReferenciaTO $vwacrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoConjuntoReferencia(VwAvaliacaoConjuntoReferenciaTO $vwacrTO,$where = null){
		try{
			$dados = $this->dao->retornarVwAvaliacaoConjuntoReferencia($vwacrTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwAvaliacaoConjuntoReferenciaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação Referencia!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que exclui logicamente o registro da tb_avaliacaoaluno
	 * @param AvaliacaoAlunoTO $aaTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirNotaAvaliacao(AvaliacaoAlunoTO $aaTO){
		try{
			$dados = $this->dao->excluirNotaAvaliacao($aaTO);
			if($dados){
				return new Ead1_Mensageiro('Nota excluída com sucesso!',Ead1_IMensageiro::SUCESSO);
			}else{
				throw new Zend_Exception('Erro ao excluir Nota!');
			}
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Conjunto de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que exclui disciplina de avaliação.
	 * @param AvaliacaoDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO){
		try{
			$dados = $this->dao->excluirAvaliacaoDisciplina($adTO);
			if($dados){
				return new Ead1_Mensageiro('Disciplina excluída com sucesso!',Ead1_IMensageiro::SUCESSO);
			}else{
				throw new Zend_Exception('Erro ao excluir Disciplina!');
			}
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao excluir Disciplina!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Retorna os módulos em que o aluno
	 * possuis disciplinas na situação de cursando
	 * 
	 * @param int idMatricula 
	 * @return Ead1_Mensageiro
	 */
	public function retornarModulosDisciplinaCursandoMatricula( $idMatricula ) {
			
		try {
			
			$dadosRS	= $this->dao->retornarModulosDisciplinaCursandoMatricula( $idMatricula );
			
			if( $dadosRS->count() < 1 ) {
				throw new DomainException( 'Para a matrícula informada não foram encontrado módulos vinculados a disciplina cursando' );
			}
			
			$this->mensageiro->setMensageiro( $dadosRS, Ead1_IMensageiro::SUCESSO );
			
		} catch( DomainException $domainExc ) {
			
			$this->mensageiro->setMensageiro( $domainExc->getMessage(), Ead1_IMensageiro::AVISO );
			
		} catch( Exception $e ) {
			$this->mensageiro->setMensageiro( 'Erro ao listar os módulos', Ead1_IMensageiro::ERRO, $e->getMessage() );
			
		}
		return $this->mensageiro;
	}
	
	
	public function retornaDiasSemanaAplicacaoAvaliacao( $idHorarioAula ) {
		
		$horarioAulaBO				= new HorarioAulaBO();
		$vwHorarioAulaDiaSemanaTO	= new VwHorarioAulaDiaSemanaTO( array( 'id_horarioaula' => $idHorarioAula ) );
		$mensageiro					= $horarioAulaBO->retornarHorarioAulaDiaSemana( $vwHorarioAulaDiaSemanaTO );
		$diasSemana					= array();
		$textoSemana				= array(); 
		
		if( $mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO ) {
			$diasSemana				= $mensageiro->mensagem;
		}
		
		foreach( $diasSemana as $vwHorarioAulaDiaSemanaTO ) {
			$textoSemana[$vwHorarioAulaDiaSemanaTO->id_diasemana]	= $vwHorarioAulaDiaSemanaTO->st_diasemana;
		}
		
		return  $textoSemana;
	}
	
	/**
	 * Método que retorna o vinculo de integração da Avaliação com o Modelo de Avaliação do Sistema de Avaliacao
	 * @param AvaliacaoTO $avaliacaoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVinculoAvaliacaoModeloAvaliacaoSA(AvaliacaoTO $avaliacaoTO){
		try{
			if(!$avaliacaoTO->getId_avaliacao()){
				THROW new Zend_Exception('O id_avaliacao não veio setado!');
			}
			$arrIntegracao = $this->dao->retornarAvaliacaoIntegracaoTO(new AvaliacaoIntegracaoTO(
																			array(	'id_avaliacao' => $avaliacaoTO->getId_avaliacao(),
																					'id_sistema' => SistemaTO::SISTEMA_AVALIACAO)
																			))->toArray();
			if(empty($arrIntegracao)){
				THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
			}else{
				$arrIntegracao = Ead1_TO_Dinamico::encapsularTo($arrIntegracao, new AvaliacaoIntegracaoTO());
				$avaliacaoRO = new AvaliacaoRO();
				$resultService = $avaliacaoRO->retornarModeloAvaliacaoSA();
				switch ($resultService->getTipo()){
					case Ead1_IMensageiro::SUCESSO:
						$arrResposta = array();
						$arrService = $resultService->getMensagem();
						foreach ($arrIntegracao as $integracao){
							foreach ($arrService as $sv){
								if($integracao->getSt_codsistema() == $sv->id_modeloavaliacao){
									$arrResposta[] = $sv;
								}
							}
						}
						if(empty($arrResposta)){
							THROW new Zend_Validate_Exception('Nenhum Registro Encontrado!');
						}
						$this->mensageiro->setMensageiro($arrResposta,Ead1_IMensageiro::SUCESSO);
						break;
					case Ead1_IMensageiro::AVISO:
						THROW new Zend_Validate_Exception($resultService->mensagem[0]);
						break;
					case Ead1_IMensageiro::ERRO:
						THROW new Zend_Exception($resultService->getCodigo());
						break;
				}
			}
		}catch (Zend_Validate_Exception $e){
				$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Integrações de Avaliação!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna as questões, os itens e as respostas de uma determinada avaliação 
	 * @param string $guidAvaliacao
	 * @return Ead1_Mensageiro
	 */
	public function retornarQuestoesItensRespostasSA($guidAvaliacao){
		$service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
		$dadosServico = $service->retornarAvaliacaoQuestaoItensRespostas($guidAvaliacao);
		if($dadosServico->getTipo() == Ead1_IMensageiro::SUCESSO){
			foreach($dadosServico->getMensagem() as $chave => $xml){
				if($xml->numquestao){
					if(!isset($arDados['aluno'])){
						$tbUsuarioIntegracaoTO = new UsuarioIntegracaoTO();
						$tbUsuarioIntegracaoTO->setSt_codusuario($xml->codpessoaavaliada);
						$tbUsuarioIntegracaoTO->setId_sistema(SistemaTO::SISTEMA_AVALIACAO);
						$tbUsuarioIntegracaoTO->fetch(null, true, true);
						
						$usuarioTO = new UsuarioTO();
						$usuarioTO->setId_usuario($tbUsuarioIntegracaoTO->getId_usuario());
						$usuarioTO->fetch(true, true, true);
						$arDados['aluno'] = $usuarioTO->getSt_nomecompleto();
					}
					$arDados['avaliacao'] = $guidAvaliacao;
					$arDados['questoes'][(string) $xml->codquestao]['id'] 		= (string) $xml->codquestao;
					$arDados['questoes'][(string) $xml->codquestao]['referencia'] 	= ((string) $xml->numquestao ? (string) $xml->numquestao : 0);
					$conversor = new Ead1_ConversorAlfaNumerico();
					$arDados['questoes'][(string) $xml->codquestao]['itens'][(string) $xml->coditem]['id'] 	= (string) $xml->coditem;
					$arDados['questoes'][(string) $xml->codquestao]['itens'][(string) $xml->coditem]['referencia'] 	= strtoupper($conversor->converteNumero((int) ((string)$xml->numitem ? (string)$xml->numitem : 0))->getLetra());
					$arDados['questoes'][(string) $xml->codquestao]['itens'][(string) $xml->coditem]['marcado'] 	= ((string)$xml->coditemrespondido == (string) $xml->coditem);
					$arDados['questoes'][(string) $xml->codquestao]['itens'] = array_values($arDados['questoes'][(string) $xml->codquestao]['itens']);
					
				}
			}
			if(isset($arDados['questoes'])){
				$arDados['questoes'] = array_values($arDados['questoes']);
				return new Ead1_Mensageiro($arDados);
			}else{
				return new Ead1_Mensageiro("Avaliação não encontrada, ou não impressa!", Ead1_IMensageiro::AVISO);
			}
		}else{
			return $dadosServico;
		}
	}
	
	/**
	 * Método que cadastra via WebService uma resposta por vez no Sistema de Avaliação
	 * 	Após o cadastro ele já busca o cálculo da nota percentual da avaliação no Sistema de Avaliações e gera a nota atual no Gestor2
	 * @param string $guidAvaliacao
	 * @param int $idQuestao
	 * @param int $idItemRespondido
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarRespostasSA($guidAvaliacao, $idQuestao, $idItemRespondido){
		$service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
		$mensageiroRespostas = $service->cadastrarResposta($guidAvaliacao, $idQuestao, $idItemRespondido);
		if($mensageiroRespostas->getTipo() == Ead1_IMensageiro::SUCESSO){
			$mensageiroNota = $service->retornarPercentualAprovacaoAvaliacao($guidAvaliacao);
			if($mensageiroNota->getTipo() == Ead1_IMensageiro::SUCESSO){
				$avaliacaoDAO = new AvaliacaoDAO();
				$avaliacaoAlunoDados = $avaliacaoDAO->retornarAvaliacaoAgendamentoIntegracao($guidAvaliacao);
				$aaTO = new AvaliacaoAlunoTO();
				$aaTO->setId_matricula($avaliacaoAlunoDados['id_matricula']);
				$aaTO->setId_avaliacao($avaliacaoAlunoDados['id_avaliacao']);
				$aaTO->setId_avaliacaoagendamento($avaliacaoAlunoDados['id_avaliacaoagendamento']);
				$aaTO->setId_avaliacaoconjuntoreferencia($avaliacaoAlunoDados['id_avaliacaoconjuntoreferencia']);
				$aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
				$aaTO->setSt_nota((string) $mensageiroNota->getCurrent()->nota);
				
				return $this->salvarAvaliacaoAluno($aaTO);
			}else{
				return $mensageiroNota;
			}
		}else{
			return $mensageiroRespostas;
		}
	}
	
	/**
	 * Método que cadastra via WebService as respostas no Sistema de Avaliação e depois realiza o cálculo da nota em valor percentual até o momento
	 * @param string $guidAvaliacao
	 * @param array $arQuestoes
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarBlocoRespostasSA($guidAvaliacao, array $arQuestoes){
		$service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
		$mensageiroRespostas = $service->cadastrarBlocoRespostas($guidAvaliacao, $arQuestoes);
		if($mensageiroRespostas->getTipo() == Ead1_IMensageiro::SUCESSO){
			$mensageiroNota = $service->retornarPercentualAprovacaoAvaliacao($guidAvaliacao);
			if($mensageiroNota->getTipo() == Ead1_IMensageiro::SUCESSO){
				$avaliacaoDAO = new AvaliacaoDAO();
				$avaliacaoAlunoDados = $avaliacaoDAO->retornarAvaliacaoAgendamentoIntegracao($guidAvaliacao);
				
				$avaliacaoTO = new AvaliacaoTO();
				$avaliacaoTO->setId_avaliacao($avaliacaoAlunoDados[0]['id_avaliacao']);
				$avaliacaoTO->fetch(true, true, true);
				
				$aaTO = new AvaliacaoAlunoTO();
				$aaTO->setId_matricula($avaliacaoAlunoDados[0]['id_matricula']);
				$aaTO->setId_avaliacao($avaliacaoAlunoDados[0]['id_avaliacao']);
				$aaTO->setId_avaliacaoagendamento($avaliacaoAlunoDados[0]['id_avaliacaoagendamento']);
				$aaTO->setId_avaliacaoconjuntoreferencia($avaliacaoAlunoDados[0]['id_avaliacaoconjuntoreferencia']);
				$aaTO->setId_usuariocadastro($aaTO->getSessao()->id_usuario);
				$aaTO->setSt_nota((((string) $mensageiroNota->getCurrent()->nota) / 100 * $avaliacaoTO->getNu_valor()));
				
				return $this->salvarAvaliacaoAluno($aaTO);
			}else{
				return $mensageiroNota;
			}
		}else{
			return $mensageiroRespostas;
		}
	}
}
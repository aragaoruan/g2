<?php

/**
 * Classe com regras de negócio para interações de Robos
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 04/10/2011
 *
 * @package models
 * @subpackage bo
 */
class RoboBO extends Ead1_BO
{

    /**
     * Metodo Construtor
     */
    public function __construct()
    {
        $this->dao = new RoboDAO();
        $this->mensageiro = new Ead1_Mensageiro();
        parent::__construct();
    }

    /**
     * Variavel que contem a instancia do Mensageiro
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * Variavel que contem a Instancia do Robo DAO
     * @var RoboDAO
     */
    private $dao;

    /**
     * Metodo que gera os Lançamentos dos Alunos
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function geraLancamentos()
    {
        try {
            $dateInicio = new Zend_Date(null, Zend_Date::ISO_8601);
            $dateInicio = $dateInicio->setDay(1);
            $dateTermino = new Zend_Date(null, Zend_Date::ISO_8601);
            $dateTermino = $dateTermino->setDay($this->getLastDay($dateTermino));
            $arrMensagem = array();
            $arrMensagem['dados'] = array();
            $arrMensagem['erros'] = array();
            $arrMensagem['info']['inicio'] = $dateInicio->toString('dd/MM/Y');
            $arrMensagem['info']['termino'] = $dateTermino->toString('dd/MM/Y');
            //Não retorna mais os já cadastrados, se necessario implementar no futuro
            $arrMensagem['jacadastrados'] = array();
            $lancamentos = $this->dao->retornarVwRoboMensalidade(new VwRoboMensalidadeTO())->toArray();
            if (empty($lancamentos)) {
                $this->mensageiro->setMensageiro("Já Foram Gerados Todos os Lançamentos.", Ead1_IMensageiro::AVISO);
            } else {
                $lancamentos = Ead1_TO_Dinamico::encapsularTo($lancamentos, new VwRoboMensalidadeTO());
                $lancamentoTO = null;
                $lancamentoVendaTO = null;
                $produtoDAO = new ProdutoDAO();
                $dataLancamento = new Zend_Date(null, Zend_Date::ISO_8601);
                //Numero de dias do Mes
                $maxDiasMes = $dataLancamento->get(Zend_Date::MONTH_DAYS);
                foreach ($lancamentos as $vwRoboMensalidadeTO) {
                    $lancamentoTO = new LancamentoTO();
                    $lancamentoVendaTO = new LancamentoVendaTO();
                    $lancamentoTO->setId_entidade($vwRoboMensalidadeTO->getId_entidade());
                    $lancamentoTO->setId_tipolancamento(TipoLancamentoTO::LANCAMENTO);
                    $lancamentoTO->setId_meiopagamento(MeioPagamentoTO::BOLETO);
                    $lancamentoTO->setId_usuariolancamento($vwRoboMensalidadeTO->getId_usuariolancamento());
                    $lancamentoTO->setId_usuariocadastro($vwRoboMensalidadeTO->getId_usuario());
                    $lancamentoTO->setBl_quitado(false);
                    //Verificando se o Dia de Vencimento esta dentro do Maximo de Dias do Mes
                    if ($vwRoboMensalidadeTO->getNu_diamensalidade() >= $maxDiasMes) {
                        $diaMensalidade = $maxDiasMes;
                    } else {
                        $diaMensalidade = ($vwRoboMensalidadeTO->getNu_diamensalidade() != 0 ? $vwRoboMensalidadeTO->getNu_diamensalidade() : 1);
                    }
                    $vwRoboMensalidadeTO->setNu_diamensalidade($diaMensalidade);
                    $dataLancamento = $dataLancamento->setDay($diaMensalidade);
                    $lancamentoTO->setDt_vencimento($dataLancamento);
                    $lancamentoTO->setDt_emissao(new Zend_Date(null, Zend_Date::ISO_8601));
                    $valorMensal = $vwRoboMensalidadeTO->getNu_valormensal();
                    $porcentagemBolsa = ($vwRoboMensalidadeTO->getNu_bolsa() ? $vwRoboMensalidadeTO->getNu_bolsa() : 0);
                    $porcentagemResponsavel = $vwRoboMensalidadeTO->getNu_porcentagem();
                    //Descontando Porcentagem da Bolsa
                    $valorFinal = (($valorMensal * $porcentagemBolsa) / 100);
                    //Descontando Porcentagem Correspondente ao Responsavel
                    $valorFinal = (($valorFinal * $porcentagemResponsavel) / 100);
                    $lancamentoTO->setNu_valor($valorFinal);
                    $lancamentoVendaTO->setId_venda($vwRoboMensalidadeTO->getId_venda());
                    $lancamentoVendaTO->setBl_entrada(false);
                    //Cadastrando e Vinculando Lancamentos
                    try {
                        $this->dao->beginTransaction();
                        $id_lancamento = $produtoDAO->cadastrarLancamento($lancamentoTO);
                        $lancamentoVendaTO->setId_lancamento($id_lancamento);
                        $produtoDAO->cadastrarLancamentoVenda($lancamentoVendaTO);
                        $arrMensagem['dados']['sucesso'][$vwRoboMensalidadeTO->getId_entidade()]['entidade'] = $vwRoboMensalidadeTO->getSt_nomeentidade();
                        $arrMensagem['dados']['sucesso'][$vwRoboMensalidadeTO->getId_entidade()]['lancamentos'][$vwRoboMensalidadeTO->getId_matricula()] = $vwRoboMensalidadeTO;
                        $this->dao->commit();
                    } catch (Zend_Exception $e) {
                        $this->dao->rollBack();
                        $vwRoboMensalidadeTO->erro = $e->getMessage();
                        $arrMensagem['dados']['erro'][$vwRoboMensalidadeTO->getId_entidade()]['entidade'] = $vwRoboMensalidadeTO->getSt_nomeentidade();
                        $arrMensagem['dados']['erro'][$vwRoboMensalidadeTO->getId_entidade()]['lancamentos'][$vwRoboMensalidadeTO->getId_matricula()] = $vwRoboMensalidadeTO;
                        continue;
                    }
                }
                $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Método que verifica os alunos com a data de termino menor que a data de hoje e os marca como Retornados
     */
    public function verificaRenovacaoAluno()
    {
        try {
            $matriculaDAO = new MatriculaDAO();
            $where = "dt_termino < getdate() and id_evolucao = " . MatriculaTO::EVOLUCAO_MATRICULA_CURSANDO;
            $dados = $matriculaDAO->retornarVwMatricula(new VwMatriculaTO(), $where)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Aluno Cursando com a Renovação Vencida.");
            }
            $matriculas = array();
            $rel = array();
            $dados = Ead1_TO_Dinamico::encapsularTo($dados, new VwMatriculaTO());
            foreach ($dados as $dado) {
                $matriculas[$dado->getId_matricula()] = $dado->getId_matricula();
                $rel[$dado->getId_matricula()] = $dado;
            }
            $matriculaTO = new MatriculaTO();
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_SEM_RENOVACAO);
            $where = "id_matricula in (" . implode(", ", $matriculas) . ")";
            $matriculaDAO->editarMatricula($matriculaTO, $where);
            $this->mensageiro->setMensageiro($rel, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Verificar as Matriculas de Renovação.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Processo ativação automatica
     * Alterado por Kayo Silva <kayo.silva@unyleya.com.br>
     * adicionando parametro para vw_vendasrecebimento id_formapagamentoaplicacao = 2, solicitado por Felipe Toffolo
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function processoAtivacaoAutomatica()
    {
        try {
            $dateInicio = new Zend_Date(null, Zend_Date::ISO_8601);
            $dateInicio = $dateInicio->setDay(1);
            $dateTermino = new Zend_Date(null, Zend_Date::ISO_8601);
            $dateTermino = $dateTermino->setDay($this->getLastDay($dateTermino));

            $arrMensagem = array();
            $arrMensagem['dados'] = array();
            $arrMensagem['erros'] = array();
            $arrMensagem['info']['inicio'] = $dateInicio->toString('dd/MM/Y');
            $arrMensagem['info']['termino'] = $dateTermino->toString('dd/MM/Y');

            $vendaBO = new VendaBO();

            //executando a consulta para trazer as vendas que devem ser ativadas basendo no id da forma de pagamento da aplicação
            $vwVendasrecebimento = new \G2\Negocio\Venda();
            $constantes = new \G2\Constante\FormaPagamentoAplicacao;
            $result = $vwVendasrecebimento->retornaDadosVwVendasRecebimento(
                array('id_formapagamentoaplicacao' => array(
                    $constantes::VENDA_SITE,
                    $constantes::VENDA_CICLICA,
                    $constantes::VENDA_AUTOMATICA)
                ), array('id_venda' => 'DESC'), 100);

            //É feito o processo de ativação automática utilizando os resultandos obtidos
            if (!empty($result)) {
                foreach ($result as $key => $row) {
                    $arrDados = $this->toArrayEntity($row);
                    $vendaTO = new VendaTO();
                    $vendaTO->setId_venda($arrDados['id_venda']);
                    $vendaTO->setId_entidade($arrDados['id_entidade']);
                    $mensageiro = $vendaBO->processoAtivacaoAutomatica($vendaTO);

                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $arrMensagem['dados']['sucesso'][$vendaTO->getId_venda()]['view'] = $arrDados;
                        $arrMensagem['dados']['sucesso'][$vendaTO->getId_venda()]['mensagem'] = $mensageiro->getFirstMensagem();
                    } else {
                        $arrMensagem['dados']['erro'][$vendaTO->getId_venda()]['view'] = $arrDados;
                        $arrMensagem['dados']['erro'][$vendaTO->getId_venda()]['mensagem'] = $mensageiro->getFirstMensagem() . ' Erro: ' . $mensageiro->getCodigo();
                    }
                    unset($vendaTO, $mensageiro);
                }
                $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);

            } else {
                $this->mensageiro->setMensageiro('Nenhuma matrícula a ser ativada!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro;
        } catch (Exception $e) {
            throw new Exception("Erro ao executar método para ativação automática. " . $e->getMessage(), $e->getCode());
        }
    }


    /**
     * Verifica as Transacoes Financeiras
     *
     */
    public function verificaPagamentoCartao()
    {

        try {
// 			exit;
            $tran = new TransacaoFinanceiraBO();
            $mensageiro = $tran->listarTransacaoFinanceiraPendente();
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {

                $pag = null;

                foreach ($mensageiro->getMensagem() as $traTO) {
                    try {
                        $venda = new VendaTO();
                        $venda->setId_venda($traTO->getId_venda());
                        $venda->fetch(true, true, true);

                        $pagBO = new PagamentoCartaoBO($venda->getId_entidade(), $traTO->getId_cartaobandeira(), MeioPagamentoTO::CARTAO);
                        $pag[] = $pagBO->verificarPagamento($traTO);
                    } catch (\Exception $e) {
                        $pag[] = new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
                    }

                }
                return new Ead1_Mensageiro($pag, Ead1_IMensageiro::SUCESSO);
            }
            return new Ead1_Mensageiro("Nenhuma transação encontrada!", Ead1_IMensageiro::AVISO);

        } catch (\Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }


    }


    /**
     * MÉTODO REESCRITO PELO GABRIEL USANDO DOCTRINE
     * Verifica as Transações do Cartao Recorrente que estão pendentes e faz a quitação
     * @return Ead1_Mensageiro
     */
// 	public function verificaPagamentocartaoRecorrente(){

// 		$dao 			= new VendaDAO();
// 		$resultado 		= false;
// 		$entidades 		= array();
// 		$temrecorrente 	= array();

// 		try {

// 			$lancamentos = $dao->listarlancamentosCartaoRecorrentePendente();
// 			if($lancamentos){
// 				foreach($lancamentos as $vw){

// 					if(in_array($vw->getId_entidade(), $entidades)==false){

// 						$entidades[] = $vw->getId_entidade();

// 						$integracao = new EntidadeIntegracaoTO();
// 						$integracao->setId_entidade($vw->getId_entidade());
// 						$integracao->setId_sistema(SistemaTO::BRASPAG_RECORRENTE);
// 						$integracao->fetch(false,true, true);

// 						if(!$integracao->getSt_codchave()){
// 							$temrecorrente[$vw->getId_entidade()] = false;
// 						} else {
// 							$temrecorrente[$vw->getId_entidade()] = true;
// 						}

// 					}

// 					if($temrecorrente[$vw->getId_entidade()]){
// 						$recorrente = new BraspagRecorrenteBO($vw->getId_entidade());
// 						//$resultado[]  = $recorrente->verificar($vw);
// 					}

//                 }
// 			return new Ead1_Mensageiro($resultado, Ead1_IMensageiro::SUCESSO);
// 			}
// 			return new Ead1_Mensageiro("Nenhum lançamento encontrado!", Ead1_IMensageiro::AVISO);
// 		} catch (Exception $e) {
// 			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
// 		}

// 	}

    /**
     *
     * Método que gera os lançamentos de uma assinatura
     * ele precisa também atualizar o prazo de matrícula.
     *
     * @throws Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function gerarLancamentoAssinatura()
    {

        $retorno = array();
        try {

            $lancamentos = $this->negocio->getRepository('G2\Entity\VwLancamentoAssinatura')->listarLancamentosAssinaturaPendentes();
            if ($lancamentos) {
                $bo = new VendaBO();
                $dao = new VendaDAO();
                $bom = new MatriculaBO();

                foreach ($lancamentos as $entity) {
                    if ($entity instanceof G2\Entity\VwLancamentoAssinatura) {

                        if (!$entity->getId_matricula()) {
                            continue;
                        }


                        $dao->beginTransaction();
                        try {
                            $lancamentoTO = new LancamentoTO();

                            $lancamentoTO->setId_cartaoconfig($entity->getId_cartaoconfig());
                            $lancamentoTO->setId_entidade($entity->getId_entidade());
                            $lancamentoTO->setId_entidadelancamento($entity->getId_entidadelancamento());
                            $lancamentoTO->setId_meiopagamento($entity->getId_meiopagamento());
                            $lancamentoTO->setId_tipolancamento($entity->getId_tipolancamento());
                            $lancamentoTO->setId_usuariocadastro($entity->getId_usuariocadastro());
                            $lancamentoTO->setId_usuariolancamento($entity->getId_usuariolancamento());

                            $lancamentoTO->setNu_desconto($entity->getNu_valordesconto());
                            $lancamentoTO->setNu_valor($entity->getNu_valorlancamento());

                            $lancamentoTO->setDt_emissao(new Zend_Date());
                            $lancamentoTO->setDt_vencimento($entity->getDt_vencimento());

                            $lancamentoTO->setNu_cobranca($entity->getNu_ordem());
                            $lancamentoTO->setNu_cobranca($entity->getNu_ordem());

                            $lancamentoTO->setBl_ativo(true);
                            $lancamentoTO->setBl_quitado(false);

                            $mensageirocadastro = $bo->cadastrarLancamento($lancamentoTO);
                            if (!$mensageirocadastro->getTipo()) {
                                throw new Zend_Exception("Não foi possível cadastrar o novo lançamento para a venda " . $entity->getId_venda());
                            }

                            $lancamentoVendaTO = new LancamentoVendaTO();
                            $lancamentoVendaTO->setId_venda($entity->getId_venda());
                            $lancamentoVendaTO->setId_lancamento($lancamentoTO->getId_lancamento());
                            $lancamentoVendaTO->setBl_entrada(false);
                            $lancamentoVendaTO->setNu_ordem($entity->getNu_ordem());

                            $mensageirovl = $bo->cadastrarLancamentoVenda($lancamentoVendaTO);
                            if (!$mensageirovl->getTipo()) {
                                throw new Zend_Exception("Não foi possível cadastrar o novo lançamento venda para a venda " . $entity->getId_venda());
                            }

                            /**
                             * Adicionando 1 mês na matrícula do usuário
                             */
                            if ($entity->getId_matricula()) {
                                $mTO = new MatriculaTO();
                                $mTO->setId_matricula($entity->getId_matricula());
                                $mTO->fetch(true, true, true);

                                if ($mTO->getId_matricula()) {
                                    $dt_termino = $lancamentoTO->getDt_vencimento()->addMonth(1);
                                    $mTO->setDt_termino($dt_termino);
                                    $mensageirom = $bom->editarMatricula($mTO);
                                    if ($mensageirom->getTipo() != Ead1_IMensageiro::SUCESSO) {
                                        throw new Zend_Exception("Não foi possível atualizar a Matrícula " . $entity->getId_matricula() . " para a venda " . $entity->getId_venda());
                                    }
                                }
                            }
                            $dao->commit();
                            $retorno[] = "Lançamento ID " . $lancamentoTO->getId_lancamento() . ", ordem " . $entity->getNu_ordem() . " cadastrado para a Venda " . $entity->getId_venda();

                        } catch (Exception $e) {
                            $dao->rollBack();
                            $retorno[] = $e->getMessage();
                        }

                    }//if($entity instanceof G2\Entity\VwLancamentoAssinatura){
                }
                if ($retorno == false) {
                    $retorno = "Nenhum lançamento encontrado.";
                }

                return new \Ead1_Mensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
            }

            return new \Ead1_Mensageiro("Nenhum lançamento encontrado!", \Ead1_IMensageiro::AVISO);
        } catch (Exception $exception) {
            return new \Ead1_Mensageiro($exception->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Confere e tenta corrigir os Alunos que não receberam seus grupos no BB
     * @param int $id_entidade
     * @return Ead1_Mensageiro
     */
    public function corrigirAlunosGrupo($id_entidade = null)
    {

        try {

            $retornoMsg = array();

            $dao = new SalaDeAulaDAO();
            $retornoA = $dao->retornaAlunosSemGrupoBB($id_entidade);
            if ($retornoA) {
                $x = -1;
                foreach ($retornoA as $aluno) {
                    $x++;
                    $curso = json_decode($aluno['st_retornows']);
                    if ($curso) {
                        $grupocerto = false;

                        $retornoMsg[$x] = '<br><br>Atualizando o Grupo do Aluno ' . $aluno['st_nomecompleto'] . ' Sala (' . $aluno['st_curso'] . ') Grupo-PP (' . $aluno['st_grupo'] . ')<br>
						-- Sala G2 ' . $aluno['id_saladeaula'] . ' PP ' . $aluno['id_projetopedagogico'] . ' id_alocacao ' . $aluno['id_alocacao'] . ' id_usuario ' . $aluno['id_usuario'] . '<br>
						-- Sala BB ' . $aluno['st_codsistemacurso'] . ' Grupo-PP ' . $aluno['st_groupid'] . ' CourseMemberId ' . $aluno['st_codalocacao'] . '. UserId ' . $aluno['st_codusuario'] . ' Login ' . $aluno['st_loginintegrado'] . '<br>';

                        try {
                            $bb = new BlackBoardBO($aluno['id_entidade']);
                            $bb->debug = null;
                            $retornoBB = $bb->getGroupMembership($curso->id, $aluno['st_groupid']);

                            if ($retornoBB->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                                $retorno = $retornoBB->getMensagem();


                                if (isset($retorno['GroupMembership']['groupMembershipId'])) {
                                    if ($retorno['GroupMembership']['courseMembershipId'] == $aluno["st_codalocacao"]) {
                                        $retornoMsg[$x] .= "-- Existe vinculo";
                                        $grupocerto = $retorno['GroupMembership'];
                                    };
                                } else if (isset($retorno['GroupMembership'][0]['courseMembershipId'])) {
                                    foreach ($retorno['GroupMembership'] as $groupmember) {
                                        if ($groupmember['courseMembershipId'] == $aluno["st_codalocacao"]) {
                                            $retornoMsg[$x] .= "-- Existe vinculo";
                                            $grupocerto = $groupmember;
                                        };
                                    }
                                }

                            } else { //if($retorno->getTipo()==BlackBoard_Mensageiro::SUCESSO){
                                $retornoMsg[$x] .= " -- Grupo nao encontrado<br>";
                                $retornoMsg[$x] .= " -- " . $retornoBB->getMensagem() . "<br>";
                            }


                            if (!$grupocerto) { // Inserir o Usuário no Grupo
                                $retornoMsg[$x] .= "-- Não Existe vinculo, tentando criar o vínculo com o Grupo: " . $aluno['st_grupo'] . "<br>";
                                $gm = new BlackBoard_VO_GroupMembershipVO();
                                $gm->setCourseMembershipId($aluno["st_codalocacao"]);
                                $gm->setGroupId($aluno['st_groupid']);

                                $matricula = $bb->incluirUsuarioGrupo($curso->id, $gm);
                                if ($matricula->getTipo() == Ead1_IMensageiro::SUCESSO) {
                                    $retornoMsg[$x] .= ' -- Grupo do Aluno ' . $aluno['st_nomecompleto'] . ' Encontrado. Aluno incluído no Grupo com sucesso!<br>';
                                } else {
                                    $retornoMsg[$x] .= ' -- ERRO ao vincular o Aluno ' . $aluno['st_nomecompleto'] . ' ao Grupo: ' . $matricula->getMensagem() . '<br>';
                                    continue;
                                }
                                if ($matricula->getMensagem('GroupMembershipId'))
                                    $grupocerto = array('groupMembershipId' => $matricula->getMensagem('GroupMembershipId'));

                            }//if(!$grupocerto){

                            if ($grupocerto) {
                                $dao->beginTransaction();
                                try {
                                    $orm = new AlocacaoIntegracaoORM();
                                    $orm->update(array('st_codalocacaogrupo' => $grupocerto['groupMembershipId']), array('id_alocacaointegracao = ' . $aluno['id_alocacaointegracao']));
                                    $dao->commit();
                                    $retornoMsg[$x] .= ' -- Grupo do Aluno ' . $aluno['st_nomecompleto'] . ' Atualizado com sucesso!<br>';
                                } catch (Exception $e) {
                                    $dao->rollBack();
                                    $retornoMsg[$x] .= ' -- Grupo do Aluno ' . $aluno['st_nomecompleto'] . ' com erros na Atualização ' . $e->getMessage() . ' <br>';
                                }
                            } else {
                                $retornoMsg[$x] .= " -- Vinculo com o Grupo nao encontrado.";
                            }

                        } catch (Exception $e) {
                            $retornoMsg[$x] .= " -- Erro: " . $e->getMessage() . '<br>';
                            $retornoMsg[$x] .= " -- Arquivo: " . $e->getFile() . '(' . $e->getLine() . ')<br> -->';
                        }

                    }//if($curso){
                }//foreach($retorno as $aluno){
            } else {
                $retorno[] = "Nenhum Aluno encontrado";
            } // if($retorno){
            return new Ead1_Mensageiro($retornoMsg, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            return new Ead1_Mensageiro("Erro ao tentar corrigir os Grupos dos Alunos: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    public function gerarMensagemVencimentoCartao()
    {
        try {
            $mensagemBO = new MensagemBO();

            $vw = new VwRecorrenteVencendoTO();
            $vws = $vw->fetch();

            foreach ($vws as $vw) {
                $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::AVISO_VENCIMENTO_CARTAO_RECORRENTE, $vw, \G2\Entity\TipoEnvio::EMAIL, true);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Exception($mensageiro->getFirstMensagem());
                }
                $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::AVISO_VENCIMENTO_CARTAO_RECORRENTE, $vw, \G2\Entity\TipoEnvio::HTML, true);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Exception($mensageiro->getFirstMensagem());
                }
            }
            die('Mensagens geradas com sucesso!');
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}

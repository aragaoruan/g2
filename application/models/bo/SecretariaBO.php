<?php

/**
 * Classe com regras de negócio para interações de Secretaria
 * @author Arthur Cláudio de Almeida Pereira
 * @since 20/01/2010
 *
 * @package models
 * @subpackage bo
 */
class SecretariaBO extends Ead1_BO
{

    public $mensageiro;
    private $dao;
    private $ng;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new SecretariaDAO();
        $this->ng = new G2\Negocio\Negocio();
    }

    /**
     * Metodo que verificase o aluno possui alguma pendencia na secretaria
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarStatusSecretaria(MatriculaTO $mTO)
    {
        try {
            $status = true;
            if (!$this->verificaDocumentosAluno($mTO)) {
                $status = false;
            }
            if (!$this->verificaIdadeAlunoProjetoConclusao($mTO)) {
                $status = false;
            }
            return $this->mensageiro->setMensageiro(($status ? 'Confirmado' : 'Pendente'), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao Retornar Status do Aluno!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o o detalhamento do status da secretaria
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarDetalhamentoStatusSecretaria(MatriculaTO $mTO)
    {
        try {
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            $documentos = $this->retornaDocumentosPendentesAluno($mTO);
            if ($documentos) {
                $this->mensageiro->addMensagem($documentos);
            }
            $idade = $this->verificaIdadeAlunoProjetoConclusao($mTO, true);
            if ($idade) {
                $this->mensageiro->addMensagem($idade);
            }
            if (empty($this->mensageiro->mensagem)) {
                $this->mensageiro->setMensagem('Este Aluno não Possui Nenhuma Pendência.');
            }
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao Retornar Detalhamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método para salvar a situação do documento na entrega
     * @param EntregaDocumentosTO $entregaDocumentosTO
     * @return bool|Ead1_Mensageiro
     */
    public function salvarSituacaoEntregaDocumento(EntregaDocumentosTO $entregaDocumentosTO)
    {
        $secretariaDAO = new SecretariaDAO();
        $secretariaDAO->beginTransaction();
        try {
            if ($entregaDocumentosTO->getId_entregadocumentos()) {
                $operacao = $secretariaDAO->salvarSituacaoEntregaDocumento($entregaDocumentosTO);
            } else {
                $entregaDocumentosTO->setId_usuariocadastro($entregaDocumentosTO->getSessao()->id_usuario);
                $operacao = $secretariaDAO->cadastrarEntregaDocumento($entregaDocumentosTO);
            }
            $secretariaDAO->commit();
            return $operacao;
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro(
                'Erro de aplicação: ' . $exc->getMessage(),
                Ead1_IMensageiro::ERRO, $exc->getMessage()
            );
        }
    }

    /**
     * Metodo que verifica se o Aluno Entregou todos os Documentos
     * @param MatriculaTO $mTO
     * @return bool
     * @throws Zend_Exception
     */
    private function verificaDocumentosAluno(MatriculaTO $mTO)
    {
        try {
            $to = new MatriculaTO();
            $to->setId_matricula($mTO->getId_matricula());
            $to->fetch(true, true, true);

            return ($to->getBl_documentacao() == 1 ? true : false);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que verifica a pendencia do aluno de acordo com a idade e a idade minima para conclusão
     * do projeto pedagogico
     * @param MatriculaTO $mTO
     * @param bool $detalhado
     * @return array|bool|null
     * @throws Exception
     * @throws Zend_Exception
     */
    private function verificaIdadeAlunoProjetoConclusao(MatriculaTO $mTO, $detalhado = false)
    {
        if (!$mTO->getId_matricula()) {
            THROW new Zend_Exception('O id_matricula não veio setado!');
        }
        $matriculaORM = new MatriculaORM();
        $projetoORM = new ProjetoPedagogicoORM();
        $pessoaORM = new PessoaORM();
        $mTO = $matriculaORM->consulta($mTO, true, true);
        $label = new Ead1_LabelFuncionalidade();
        if (!$mTO) {
            THROW new Zend_Exception('Erro ao Retornar Matricula!');
        }
        $projetoTO = $projetoORM
            ->consulta(new ProjetoPedagogicoTO(array(
                'id_projetopedagogico' => $mTO->getId_projetopedagogico()
            )), true, true);
        if (!$projetoTO) {
            THROW new Zend_Exception('Erro ao Retornar Projeto Pedagógico!');
        }
        if (is_null($projetoTO->getNu_idademinimaconcluir()) || $projetoTO->getNu_idademinimaconcluir() == 0) {
            return ($detalhado ? null : true);
        }
        $pessoaTO = $pessoaORM->consulta(new PessoaTO(array(
            'id_usuario' => $mTO->getId_usuario(),
            'id_entidade' => $mTO->getId_entidadeatendimento()
        )), true, true);
        if (!$pessoaTO) {
            THROW new Zend_Exception('Erro ao Retornar Pessoa!');
        }
        if (!$pessoaTO->getDt_nascimento()) {
            return ($detalhado ? array(
                'label' => 'Requisitos Pessoais',
                'dados' => array(
                    array(
                        'label' => 'O Aluno não Atingiu a Idade Minima para Concluir o ' . $label->getLabel(19) . '.',
                        'dados' => array()
                    )
                )
            ) : false);
        }
        $idade = $this->retornaIdade($pessoaTO->getDt_nascimento());
        if ($idade < $projetoTO->getNu_idademinimaconcluir()) {
            return ($detalhado ? array(
                'label' => 'Requisitos Pessoais',
                'dados' => array(
                    array(
                        'label' => 'O Aluno não Atingiu a Idade Minima para Concluir o ' . $label->getLabel(19) . '.',
                        'dados' => array()
                    )
                )
            ) : false);
        } else {
            return ($detalhado ? null : true);
        }
    }

    /**
     * Metodo que retorna os documentos pendentes do aluno e de seus responsaveis
     * @param MatriculaTO $mTO
     * @throws Zend_Exception
     * @return array $documentos
     */
    private function retornaDocumentosPendentesAluno(MatriculaTO $mTO)
    {
        try {
            //ID's Fixos
            $ID_SITUACAO_DOCUMENTO_ENTREGUE = 59;
            $ID_DOCUMENTO_UTILIZACAO_ALUNO = 1;
            $ID_DOCUMENTO_UTILIZACAO_RESPONSAVEL = 2;
            //-----------------------------------
            $where = 'id_matricula = ' . $mTO->getId_matricula()
                . ' AND (id_situacao != ' . $ID_SITUACAO_DOCUMENTO_ENTREGUE . ' OR id_situacao is null) ';
            $documentosMatricula = $this->dao
                ->retornarDocumentoMatriculaEntrega(new VwMatriculaEntregaDocumentoTO(), $where)->toArray();
            if (empty($documentosMatricula)) {
                return null;
            }
            $documentosMatricula = Ead1_TO_Dinamico::encapsularTo(
                $documentosMatricula, new VwMatriculaEntregaDocumentoTO()
            );
            $arrDocumentosPendentes = array();
            foreach ($documentosMatricula as $vwMatriculaEntregaDocumentoTO) {
                $arrDocumentosPendentes[$vwMatriculaEntregaDocumentoTO->getId_usuario()]['label']
                    = $vwMatriculaEntregaDocumentoTO->getSt_nomecompleto();
                $arrDocumentosPendentes[$vwMatriculaEntregaDocumentoTO
                    ->getId_usuario()]['dados'][$vwMatriculaEntregaDocumentoTO->getId_documentos()]
                    = $vwMatriculaEntregaDocumentoTO;
            }
            $documentos = array();
            $documentos['label'] = 'Documentos';
            $documentos['dados'] = $arrDocumentosPendentes;
            return $documentos;
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Retorna as categorias ativas do documento
     * @return Ead1_Mensageiro
     */
    public function retornaDocumentoCategoria()
    {

        try {

            $secretariaDAO = new SecretariaDAO();
            $arrDocumentoCategoria = $secretariaDAO->retornarDocumentoCategoria()->toArray();
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arrDocumentoCategoria, new DocumentosCategoriaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
     * @param $bl_recusa
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoCoordenador_Coordenador(
        VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO,
        $bl_recusa
    )
    {
        if (!$vwEncerramentoCoordenadorTO->getId_coordenador()) {
            $vwEncerramentoCoordenadorTO->setId_coordenador($vwEncerramentoCoordenadorTO->getSessao()->id_usuario);
        }

        if (!$vwEncerramentoCoordenadorTO->getId_entidade()) {
            $vwEncerramentoCoordenadorTO->setId_entidade($vwEncerramentoCoordenadorTO->getSessao()->id_entidade);
        }
        $where = ' dt_encerramentoprofessor IS NOT NULL AND dt_encerramentocoordenador IS NULL AND id_entidade = '
            . $vwEncerramentoCoordenadorTO->getId_entidade() . ' AND id_coordenador = '
            . $vwEncerramentoCoordenadorTO->getId_coordenador().' AND nu_alunos <> 0';

        if ($vwEncerramentoCoordenadorTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoCoordenadorTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoCoordenadorTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoCoordenadorTO->getId_matricula()) {
            $where .= ' and id_matricula = ' . $vwEncerramentoCoordenadorTO->getId_matricula();
        }
        if ($vwEncerramentoCoordenadorTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoCoordenadorTO->getId_categoriasala();
        }
        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null) {
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        }

        return $this->retornarVwEncerramentoCoordenador($vwEncerramentoCoordenadorTO, $where);
    }

    /**
     * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
     * @param null $bl_recusa
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoCoordenador_Financeiro(
        VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO, $bl_recusa = null
    )
    {
        if (!$vwEncerramentoCoordenadorTO->getId_coordenador()) {
            $vwEncerramentoCoordenadorTO->setId_coordenador($vwEncerramentoCoordenadorTO->getSessao()->id_usuario);
        }
        if (!$vwEncerramentoCoordenadorTO->getId_entidade()) {
            $vwEncerramentoCoordenadorTO->setId_entidade($vwEncerramentoCoordenadorTO->getSessao()->id_entidade);
        }

        $where = ' dt_encerramentopedagogico is not null and dt_encerramentofinanceiro is null and id_entidade ='
            . $vwEncerramentoCoordenadorTO->getId_entidade()
            . ' and id_coordenador = '
            . $vwEncerramentoCoordenadorTO->getId_coordenador();

        if ($vwEncerramentoCoordenadorTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoCoordenadorTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoCoordenadorTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoCoordenadorTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoCoordenadorTO->getId_categoriasala();
        }

        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;
        return $this->retornarVwEncerramentoCoordenador($vwEncerramentoCoordenadorTO, $where);
    }

    /**
     * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
     * @param $bl_recusa
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoCoordenador_Pedagogico(
        VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO, $bl_recusa
    )
    {
        if (!$vwEncerramentoCoordenadorTO->getId_coordenador()) {
            $vwEncerramentoCoordenadorTO->setId_coordenador($vwEncerramentoCoordenadorTO->getSessao()->id_usuario);
        }
        if (!$vwEncerramentoCoordenadorTO->getId_entidade()) {
            $vwEncerramentoCoordenadorTO->setId_entidade($vwEncerramentoCoordenadorTO->getSessao()->id_entidade);
        }

        $where = ' dt_encerramentocoordenador is not null and dt_encerramentopedagogico is null and id_entidade ='
            . $vwEncerramentoCoordenadorTO->getId_entidade()
            . ' and id_coordenador = ' . $vwEncerramentoCoordenadorTO->getId_coordenador();

        if ($vwEncerramentoCoordenadorTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoCoordenadorTO->getId_usuarioprofessor();
        }

        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoCoordenadorTO->getId_tipodisciplina();
        }

        if ($vwEncerramentoCoordenadorTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoCoordenadorTO->getId_categoriasala();
        }
        if ($vwEncerramentoCoordenadorTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;

        return $this->retornarVwEncerramentoCoordenador($vwEncerramentoCoordenadorTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento
     * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoCoordenador(
        VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO,
        $where = null)
    {
        try {
            $dados = $this->dao->retornarVwEncerramentoCoordenador($vwEncerramentoCoordenadorTO, $where)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro(
                    'Nenhum Registro de dados de encerramento encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(
                    Ead1_TO_Dinamico::encapsularTo($dados, new VwEncerramentoCoordenadorTO()), Ead1_IMensageiro::SUCESSO
                );
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro(
                'Erro ao Retornar Dados de Encerramento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula para quem tem o perfil pedagógico.
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalas_Coordenador(
        VwEncerramentoSalasTO $vwEncerramentoSalasTO,
        $bl_recusa = null
    )
    {
        if (!$vwEncerramentoSalasTO->getId_entidade()) {
            $vwEncerramentoSalasTO->setId_entidade($vwEncerramentoSalasTO->getSessao()->id_entidade);
        }
        $where = 'dt_encerramentocoordenador is null and id_entidade = ' . $vwEncerramentoSalasTO->getId_entidade();
        if ($vwEncerramentoSalasTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoSalasTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoSalasTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;
        return $this->retornarVwEncerramentoSalas($vwEncerramentoSalasTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula para quem tem o perfil pedagógico.
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalas_Pedagogico(VwEncerramentoSalasTO $vwEncerramentoSalasTO,
                                                           $bl_recusa = false)
    {
        if (!$vwEncerramentoSalasTO->getId_entidade()) {
            $vwEncerramentoSalasTO->setId_entidade($vwEncerramentoSalasTO->getSessao()->id_entidade);
        }
        $where = 'dt_encerramentocoordenador is not null and dt_encerramentopedagogico is null and id_entidade = '
            . $vwEncerramentoSalasTO->getId_entidade();
        if ($vwEncerramentoSalasTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoSalasTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoSalasTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoSalasTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoSalasTO->getId_categoriasala();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;
        return $this->retornarVwEncerramentoSalas($vwEncerramentoSalasTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula para quem tem o perfil financeiro.
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalas_Financeiro(VwEncerramentoSalasTO $vwEncerramentoSalasTO,
                                                           $bl_recusa = null)
    {
        if (!$vwEncerramentoSalasTO->getId_entidade()) {
            $vwEncerramentoSalasTO->setId_entidade($vwEncerramentoSalasTO->getSessao()->id_entidade);
        }
        $where = 'dt_encerramentocoordenador is not null and dt_encerramentopedagogico is 
        not null and dt_encerramentofinanceiro is null and id_entidade = ' . $vwEncerramentoSalasTO->getId_entidade();
        if ($vwEncerramentoSalasTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoSalasTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoSalasTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoSalasTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoSalasTO->getId_categoriasala();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;
        return $this->retornarVwEncerramentoSalas($vwEncerramentoSalasTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula para quem tem o perfil financeiro e que já foi pago.
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalas_Pagos(VwEncerramentoSalasTO $vwEncerramentoSalasTO, $bl_recusa = null)
    {
        if (!$vwEncerramentoSalasTO->getId_entidade()) {
            $vwEncerramentoSalasTO->setId_entidade($vwEncerramentoSalasTO->getSessao()->id_entidade);
        }
        $where = 'dt_encerramentofinanceiro is not null and id_entidade = ' . $vwEncerramentoSalasTO->getId_entidade();
        if ($vwEncerramentoSalasTO->getId_usuarioprofessor()) {
            $where .= ' and id_usuarioprofessor = ' . $vwEncerramentoSalasTO->getId_usuarioprofessor();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina()) {
            $where .= ' and id_tipodisciplina = ' . $vwEncerramentoSalasTO->getId_tipodisciplina();
        }
        if ($vwEncerramentoSalasTO->getId_categoriasala()) {
            $where .= ' and id_categoriasala = ' . $vwEncerramentoSalasTO->getId_categoriasala();
        }
        if ($vwEncerramentoSalasTO->getId_tipodisciplina() == TipoDisciplinaTO::TCC && $bl_recusa !== null):
            if ($bl_recusa) {
                $where .= ' and dt_recusa is not null ';
            } else {
                $where .= ' and dt_recusa is null ';
            }
        endif;

        return $this->retornarVwEncerramentoSalas($vwEncerramentoSalasTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula.
     * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalas(VwEncerramentoSalasTO $vwEncerramentoSalasTO, $where)
    {
        try {
            $dados = $this->dao->retornarVwEncerramentoSalas($vwEncerramentoSalasTO, $where)->toArray();
            if (empty($dados)) {
                return $this->mensageiro
                    ->setMensageiro('Nenhum Registro de dados de encerramento encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(
                    Ead1_TO_Dinamico::encapsularTo($dados, new VwEncerramentoSalasTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao Retornar Dados de Encerramento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna os tipos de documentos
     * Realiza um filtro de acordo com os tipos de documentos
     * @return Ead1_Mensageiro
     */
    public function retornarTipoDocumento()
    {
        try {
            $secretariaDAO = new SecretariaDAO();
            $arrTipoDoc = $secretariaDAO->retornarTipoDocumento()->toArray();
            return new Ead1_Mensageiro(
                Ead1_TO_Dinamico::encapsularTo($arrTipoDoc, new TipoDocumentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Retorna os projetos do documento
     * Realiza um filtro de acordo com os documentos
     * @param DocumentosProjetoPedagogicoTO $dppTO
     * @see SecretariaRO::retornarDocumentosProjetoPedagogico
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentosProjetoPedagogico(DocumentosProjetoPedagogicoTO $dppTO)
    {
        $dao = new SecretariaDAO();
        $vdppTO = new VwDocumentosProjetoPedagogicoTO();
        $vdppTO->setId_documentos($dppTO->getId_documentos());
        $vdppTO->setId_projetopedagogico($dppTO->getId_projetopedagogico());
        $documentoProjeto = $dao->retornarDocumentosProjetoPedagogico($vdppTO);
        if (is_object($documentoProjeto)) {
            $documentoProjeto = $documentoProjeto->toArray();
        } else {
            unset($documentoProjeto);
        }
        if (!$documentoProjeto) {
            return $this->mensageiro
                ->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this
            ->mensageiro
            ->setMensageiro(
                Ead1_TO_Dinamico::encapsularTo($documentoProjeto, new VwDocumentosProjetoPedagogicoTO()),
                Ead1_IMensageiro::SUCESSO
            );
    }

    /**
     * Retorna as formas de utilização para os documentos
     * Realiza um filtro de acordo com os atributos da TO
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentoUtilizacao()
    {
        try {
            $secretariaDAO = new SecretariaDAO();
            $arrDocUtilizacao = $secretariaDAO->retornarDocumentoUtilizacao()->toArray();
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arrDocUtilizacao,
                new DocumentosUtilizacaoTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Retorna as formas de utilização de um documento
     * Realiza um filtro de acordo com os atributos da TO
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentoUtilizacaoRelacao(DocumentosUtilizacaoRelacaoTO $durTO)
    {
        try {
            $secretariaDAO = new SecretariaDAO();
            $arrDocumentos = $secretariaDAO->retornarDocumentoUtilizacaoRelacao($durTO)->toArray();
            if (!$arrDocumentos) {
                return $this
                    ->mensageiro
                    ->setMensageiro('Nenhum Registro de utilização de documento Encontrado!',
                        Ead1_IMensageiro::AVISO, $secretariaDAO->excecao);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arrDocumentos,
                new DocumentosUtilizacaoRelacaoTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Retorna as 'fases' de obrigatoriedade do documento
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentoObrigatoriedade()
    {

        try {
            $secretariaDAO = new SecretariaDAO();
            $arrObrigatoriedade = $secretariaDAO->retornarDocumentoObrigatoriedade()->toArray();
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arrObrigatoriedade,
                new Material_DocumentosObrigatoriedadeTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os documentos existentes
     * Realiza um filtro de acordo com os atributos de TO
     * @param DocumentosTO $docTO
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentos(DocumentosTO $docTO)
    {

        try {
            if (!$docTO->getId_entidade()) {
                $docTO->setId_entidade($docTO->getSessao()->id_entidade);
            }
            $secretariaDAO = new SecretariaDAO();
            $arrDocumentos = $secretariaDAO->retornarDocumento($docTO)->toArray();
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arrDocumentos,
                new Material_VwDocumentosSecretariaTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO, $exc);
        }
    }

    /**
     * Metodo que Retorna Documento
     * @param DocumentosTO $docTO
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornarDocumento(DocumentosTO $docTO)
    {
        $dao = new SecretariaDAO();
        $documento = $dao->retornarDocumentos($docTO);
        if (is_object($documento)) {
            $documento = $documento->toArray();
        } else {
            unset($documento);
        }
        if (!$documento) {
            return $this->mensageiro
                ->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo(
            $documento, new DocumentosTO()
        ), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna Documento da matrícula, seus responsáveis e seus dados de entrega
     *
     * @see SecretariaBO::retornarDocumentoMatriculaEntrega();
     * @param VwMatriculaEntregaDocumentoTO $vmedTO
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornarDocumentoMatriculaEntrega(VwMatriculaEntregaDocumentoTO $vmedTO)
    {
        $dao = new SecretariaDAO();
        $documento = $dao->retornarDocumentoMatriculaEntrega($vmedTO);
        if (is_object($documento)) {
            $documento = $documento->toArray();
        } else {
            unset($documento);
        }
        if (!$documento) {
            return $this
                ->mensageiro
                ->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo(
            $documento, new VwMatriculaEntregaDocumentoTO()
        ), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorna os projetos pedagógicos vinculados a um documento
     * @param $idDocumento
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogicoDocumento($idDocumento)
    {
        try {
            if (empty($idDocumento)) {
                throw new DomainException('Informe o id do documento para buscar os projetos pedagógicos vinculados');
            }

            $secretariaDAO = new SecretariaDAO();
            $arProjPedagogico = $secretariaDAO->retornarProjetoPedagogicoDocumento($idDocumento)->toArray();

            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arProjPedagogico, new ProjetoPedagogicoTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os documentos marcados como substituto de um documento
     * @param $idDocumento
     * @return Ead1_Mensageiro
     */
    public function retornarDocumentoSubstituto($idDocumento)
    {
        try {
            if (empty($idDocumento)) {
                throw new DomainException('Informe o id do documento para buscar os documentos substitutos');
            }

            $secretariaDAO = new SecretariaDAO();
            $arDocumentoSubstituto = $secretariaDAO->retornarDocumentoSubstituto($idDocumento)->toArray();

            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo(
                $arDocumentoSubstituto,
                new DocumentosTO()
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que retorna a view de entrega de documentos
     * @param VwEntregaDocumentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntregaDocumento(VwEntregaDocumentoTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarVwEntregaDocumento($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEntregaDocumentoTO()));
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro
                ->setMensageiro('Erro ao Retornar Lista de Documentos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Cadastra um novo documento
     * @param DocumentosTO $docTO -
     * @param array $collectionProjetoPedagogico - lista com os projetos pedagógicos para vincular ao documento
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumento(DocumentosTO $docTO, $collectionUtilizacao, $collectionProjetoPedagogico = null)
    {
        $secretariaDAO = new SecretariaDAO();
        $secretariaDAO->beginTransaction();
        try {

            $codUsuarioCadastro = $docTO->getSessao()->id_usuario;
            $codEntidade = $docTO->getSessao()->id_entidade;
            $idDocumento = $secretariaDAO->cadastrarDocumento($docTO, $codUsuarioCadastro, $codEntidade);

            if ($idDocumento === false) {
                return new Ead1_Mensageiro('Não foi possível inserir um novo documento.', Ead1_IMensageiro::ERRO);
            }

            //TODO verificar se a coleção de projetos pedagógicos é obrigatória
            if (!empty($collectionProjetoPedagogico)) {
                foreach ($collectionProjetoPedagogico as $projetoPedagogico) {
                    $secretariaDAO
                        ->vincularProjetoPedagogicoDocumento($projetoPedagogico->id_projetopedagogico, $idDocumento);
                }
            }
            if (!empty($collectionUtilizacao)) {
                foreach ($collectionUtilizacao as $utilizacao) {
                    $secretariaDAO->vincularUtlizacaoDocumento($utilizacao->id_documentosutilizacao, $idDocumento);
                }
            }
            $docTO->setId_documentos($idDocumento);
            $secretariaDAO->commit();
            return new Ead1_Mensageiro($docTO, Ead1_IMensageiro::SUCESSO);
        } catch (Ead1_BO_Exception $excBO) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro($excBO->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro de aplicação: ' . $exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Edita um documento existente
     * @param DocumentosTO $docTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumento(DocumentosTO $docTO, $collectionUtilizacao, $collectionProjetoPedagogico = null)
    {

        $secretariaDAO = new SecretariaDAO();
        $secretariaDAO->beginTransaction();
        try {
            if (empty($docTO->id_documentos)) {
                throw new DomainException('Informe o identificador de documentos para edição');
            }

            if ($secretariaDAO->editarDocumento($docTO) != 1) {
                throw new DomainException('Não foi possível atualizar o documento');
            }

            //TODO verificar se a coleção de projetos pedagógicos é obrigatória
            if (!empty($collectionProjetoPedagogico)) {
                $documentosProjetoPedagogicoTO = new DocumentosProjetoPedagogicoTO();
                $documentosProjetoPedagogicoTO->id_documentos = $docTO->id_documentos;

                $secretariaDAO->excluirDocumentoProjetoPedagogico($documentosProjetoPedagogicoTO);

                foreach ($collectionProjetoPedagogico as $projetoPedagogico) {
                    if ($projetoPedagogico->id_projetopedagogico) {
                        $secretariaDAO
                            ->vincularProjetoPedagogicoDocumento(
                                $projetoPedagogico->id_projetopedagogico,
                                $docTO->id_documentos
                            );
                    }
                }
            }
            if (!empty($collectionUtilizacao)) {
                $documentosUtilizacaoRelacaoTO = new DocumentosUtilizacaoRelacaoTO();
                $documentosUtilizacaoRelacaoTO->id_documentos = $docTO->id_documentos;
                $secretariaDAO->excluirUtilizacoesDocumento($documentosUtilizacaoRelacaoTO);
                foreach ($collectionUtilizacao as $utilizacao) {
                    if ($utilizacao->id_documentosutilizacao) {
                        $secretariaDAO->vincularUtlizacaoDocumento(
                            $utilizacao->id_documentosutilizacao,
                            $docTO->id_documentos
                        );
                    }
                }
            }
            $secretariaDAO->commit();
            return new Ead1_Mensageiro($docTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Cadastra documento como substituto para outro documento
     * @param int $idDocumentoOriginal - id do documento originnal
     * @param int $idDocumentoSubstituto - id do documento usado como substituto
     * @return Ead1_Mensageiro
     */
    public function vincularDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto)
    {

        try {
            $secretariaDAO = new SecretariaDAO();
            $secretariaDAO->beginTransaction();


            if ((empty($idDocumentoOriginal) || empty($idDocumentoSubstituto)) ||
                (!is_numeric($idDocumentoOriginal) || !is_numeric($idDocumentoSubstituto))
            ) {
                throw new DomainException('Informe o código do documento original/substituto ');
            }


            $documentosVinculados = $this->retornarDocumentoSubstituto($idDocumentoOriginal);
            if (!empty($documentosVinculados)) {
                foreach ($documentosVinculados->getMensagem(0) as $documentos) {
                    if ($documentos->getid_documentos() == $idDocumentoSubstituto) {
                        throw new DomainException(
                            'O Documento '
                            . $documentos->getst_documentos()
                            . ' já faz parte da lista de Documentos Substitutos.'
                        );
                    }
                }
            }

            if ($secretariaDAO->vincularDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto) === false) {
                throw new DomainException('Não foi possível salvar o documento substituto');
            }
            $secretariaDAO->commit();
            return new Ead1_Mensageiro('Documento substituto cadastrado com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainEx) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro ao salvar: ' . $domainEx->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro de aplicação: ' . $exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Vincula uma lista de projetos pedagógicos ao documento
     * @param array $collectionProjetoPedagogico
     * @param int $idDocumento
     * @return Ead1_Mensageiro
     */
    public function vincularProjetoPedagogicoDocumento($collectionProjetoPedagogico, $idDocumento)
    {

        try {

            $secretariaDAO = new SecretariaDAO();
            $secretariaDAO->beginTransaction();

            foreach ($collectionProjetoPedagogico as $projetoPedagogico) {
                if (!$secretariaDAO
                    ->vincularProjetoPedagogicoDocumento($projetoPedagogico->id_projetopedagogico, $idDocumento)
                ) {
                    throw new DomainException('Não foi possível vincular os projetos pedagóficos ao documento');
                }
            }

            $secretariaDAO->commit();
            return new Ead1_Mensageiro('Projetos Pedagógicos vinculados com sucesso. ', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro de aplicação: ' . $exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Exclui um documento marcado como substituto de outro documento
     * @param $idDocumentoOriginal
     * @param $idDocumentoSubstituto
     * @return Ead1_Mensageiro
     */
    public function excluirDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto)
    {
        $secretariaDAO = new SecretariaDAO();
        $secretariaDAO->beginTransaction();
        try {
            if ((empty($idDocumentoOriginal) || empty($idDocumentoSubstituto)) ||
                (!is_numeric($idDocumentoOriginal) || !is_numeric($idDocumentoSubstituto))
            ) {
                throw new DomainException('Informe o código do documento original/substituto ');
            }

            if ($secretariaDAO->excluirDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto) == 0) {
                throw new DomainException('Não foi possível excluir o documento substituto');
            }
            $secretariaDAO->commit();
            return new Ead1_Mensageiro('Documento substituto excluído com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (DomainException $domainEx) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro ao excluir: ' . $domainEx->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $secretariaDAO->rollBack();
            return new Ead1_Mensageiro('Erro de aplicação: ' . $exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $arrId_encerramentosala
     * @return Ead1_Mensageiro
     */
    public function salvarArrEncerramentoSala_Coordenador(array $arrId_encerramentosala)
    {
        try {
            foreach ($arrId_encerramentosala as $id_encerramentosala) {
                $encerramentoSalaTO = new EncerramentoSalaTO();
                $encerramentoSalaTO->setId_encerramentosala($id_encerramentosala['id_encerramentosala']);
                $encerramentoSalaTO->setId_usuariocoordenador($encerramentoSalaTO->getSessao()->id_usuario);
                $encerramentoSalaTO->setDt_encerramentocoordenador(new \DateTime());
                $this->dao->editarEncerramentoSala($encerramentoSalaTO);
            }
            return $this->mensageiro
                ->setMensageiro('Registro(s) liberado(s) para o Pedagógico com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro(
                'Não foi possível liberar o(s) registro(s) para o Pedagógico.',
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * @param array $arrId_encerramentosala
     * @return Ead1_Mensageiro
     */
    public function salvarArrEncerramentoSala_Pedagogico(array $arrId_encerramentosala)
    {
        try {
            foreach ($arrId_encerramentosala as $id_encerramentosala) {
                $encerramentoSalaTO = new EncerramentoSalaTO();
                $encerramentoSalaTO->setId_encerramentosala($id_encerramentosala['id_encerramentosala']);
                $encerramentoSalaTO->setId_usuariopedagogico(1);
                $encerramentoSalaTO->setDt_encerramentopedagogico(new \DateTime());
                $this->dao->editarEncerramentoSala($encerramentoSalaTO);
            }
            return $this->mensageiro
                ->setMensageiro('Registro(s) liberado(s) para o Financeiro com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro(
                'Não foi possível liberar o(s) registro(s) para o Financeiro.', Ead1_IMensageiro::ERRO, $e->getMessage()
            );
        }
    }

    /**
     * @param array $arrayIdsDatas
     * @return Ead1_Mensageiro
     */
    public function salvarArrEncerramentoSala_Financeiro(array $arrayIdsDatas)
    {
        try {
            foreach ($arrayIdsDatas as $dados) {
                $data = $this->ng->converteDataBanco($dados['dt_encerramentofinanceiro']);

                $encerramentoSalaTO = new EncerramentoSalaTO();
                $encerramentoSalaTO->setId_encerramentosala($dados['id_encerramentosala']);
                $encerramentoSalaTO->setId_usuariofinanceiro($encerramentoSalaTO->getSessao()->id_usuario);
                $encerramentoSalaTO->setDt_encerramentofinanceiro($data);
                $encerramentoSalaTO->setNu_valorpago($dados['nu_valor']);
                $this->dao->editarEncerramentoSala($encerramentoSalaTO);
            }
            return $this->mensageiro->setMensageiro('Registro(s) encerrado(s) com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro(
                'Não foi possível encerrar o(s) registro(s).', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Cadastra e Exclui as Aplicações de Documento;
     * @param array ( AreaProjetoSalaTO $apsTO )
     * @return Ead1_Mensageiro
     */
    public function salvarArrayProjeto($arrTO)
    {
        $dao = new SecretariaDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $delected = false;
        $proximo = false;
        $arr['area'] = null;
        $arr['area'] = array();
        $arr['projeto'] = null;
        $arr['projeto'] = array();
        $arr['nivel'] = null;
        $arr['nivel'] = array();
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $chave => $dppTO) {

                if (!$delected) {
                    $delected = true;
                    $dpTO = new DocumentosProjetoPedagogicoTO();
                    $dpTO->setId_documentos($dppTO->getId_documentos());
                    if (!$dao->excluirDocumentoProjetoPedagogico($dpTO)) {
                        Throw new Zend_Exception('Erro ao Excluir Aplicações de Documentos!');
                    }
                }
                if (!($dppTO instanceof DocumentosProjetoPedagogicoTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe DocumentosProjetoPedagogicoTO!');
                }
                if (!$dppTO->getId_projetopedagogico()) {
                    THROW new Zend_Exception('O id_projetopedagogico não veio Setado!');
                }

                $contador = 0;
                if ($dppTO->getId_projetopedagogico()) {
                    if (!in_array($dppTO->getId_projetopedagogico(), $arr['projeto'])) {
                        $arr['projeto'][] = $dppTO->getId_projetopedagogico();
                    } else {
                        $proximo = true;
                    }
                    $contador++;
                }
                if ($contador > 1) {
                    THROW new Zend_Exception('Erro na Tipagem do DocumentosProjetoPedagogicoTO!');
                }
                if ($contador == 0) {
                    THROW new Zend_Exception('Não Veio Nenhum Parametro Setado!');
                }
                if ($proximo) {
                    $proximo = false;
                    continue;
                }
                if (!$dao->cadastrarDocumentosProjetoPedagogico($dppTO)) {
                    THROW new Zend_Exception('Erro ao Cadastrar Aplicações!');
                }
            }
            $dao->commit();
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     *
     * Retorna os registros da VwResgateAlunos de acordo com a pesquisa na tela
     * @param VwResgateAlunos $resgateAlunosTO , array $arrayDatas
     * @return Ead1_Mensageiro
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function retornarVwResgateAlunos(VwResgateAlunosTO $vwResgateAlunosTO, array $datasPesquisa = null)
    {
        try {

            $where = null;

            $nu_diassemacesso = ($vwResgateAlunosTO->getNu_diassemacesso() == 0
                ? null : $vwResgateAlunosTO->getNu_diassemacesso());
            $st_nomecompleto = $vwResgateAlunosTO->getSt_nomecompleto();
            $nu_aproveitamento = ($vwResgateAlunosTO->getNu_aproveitamento() == 0
                ? null : $vwResgateAlunosTO->getNu_aproveitamento());

            $bl_temFiltroData = false;

            //$cpf = trim(str_replace(str_replace($vwResgateAlunosTO->getSt_cpf(), '-', ''), '.', ''));
            $cpf = trim(str_replace('-', '', str_replace('.', '', $vwResgateAlunosTO->getSt_cpf())));
            $vwResgateAlunosTO->setSt_cpf(($cpf === '%%' ? null : $cpf));
            //$cpf = $vwResgateAlunosTO->getSt_cpf();

            $vwResgateAlunosTO->setSt_nomecompleto(($st_nomecompleto == '%%' ? null : $st_nomecompleto));
            $st_nomecompleto = $vwResgateAlunosTO->getSt_nomecompleto();

            foreach ($datasPesquisa as $data) {
                if (isset($data) && $data != null && !empty($data)) {
                    $bl_temFiltroData = true;
                    break;
                }
            }

            if (!is_null($vwResgateAlunosTO->getId_ocorrencia())) {

                if ($datasPesquisa) {

                    $where = array();

                    foreach ($datasPesquisa as $index => $dataFiltro) {
                        if ($index == 'dt_inicioMatricula') {
                            if ($dataFiltro != null) {
                                $dtInicioMatricula = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtInicioMatricula)) {
                                    $where [] = 'dt_cadastro >= \'' . $dtInicioMatricula->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_inicioMatricula não é uma data válida!');
                                }
                            }
                        }

                        if ($index == 'dt_fimMatricula') {
                            if ($dataFiltro != null) {
                                $dtFimMatricula = new Zend_Date($dataFiltro);
                                if (Zend_Date::isDate($dtFimMatricula)) {
                                    $where [] = 'dt_cadastro <= \'' . $dtFimMatricula->toString('yyyy-MM-dd') . '\'';
                                } else {
                                    throw new Zend_Date_Exception('dt_fimMatricula não é uma data válida!');
                                }
                            }
                        }
                    }
                }

                if (isset($dtInicioMatricula)
                    && isset($dtFimCadastro)
                    && $dtInicioMatricula->isLater($dtFimMatricula)
                ) {
                    throw new Zend_Date_Exception('Data de Inicio do Cadastro não pode ser maior que a data Fim');
                }
            } else {
                throw new Zend_Exception(
                    "Necessário informar um dos campos: Ocorrencia e/ou área e/ou Data início e fim de matrícula."
                );
            }

            if (!$vwResgateAlunosTO->getId_entidade()) {
                $vwResgateAlunosTO->setId_entidade($vwResgateAlunosTO->getSessao()->id_entidade);
            }

            if ($vwResgateAlunosTO->getSt_nomecompleto() === '%%') {
                $vwResgateAlunosTO->setSt_nomecompleto(null);
            }

            if ($vwResgateAlunosTO->getSt_cpf() === '%%') {
                $vwResgateAlunosTO->setSt_cpf(null);
            }

            if ($vwResgateAlunosTO->getNu_aproveitamento()) {
                $where[] = 'nu_aproveitamento <= ' . $vwResgateAlunosTO->getNu_aproveitamento();
                $vwResgateAlunosTO->setNu_aproveitamento(null);
            }

            if ($vwResgateAlunosTO->getNu_diassemacesso()) {
                $where[] = 'nu_diassemacesso >= ' . $vwResgateAlunosTO->getNu_diassemacesso();
                $vwResgateAlunosTO->setNu_diassemacesso(null);
            }

            //$where[] = 'bl_ativo = 1';
            $retorno = $this->dao->retornarVwResgateAlunos($vwResgateAlunosTO, $where);
            $retorno = $retorno->toArray();

            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro("Nenhum registro encontrado!", Ead1_IMensageiro::AVISO);
            }

            return $this->mensageiro
                ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwResgateAlunosTO()),
                    Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Date_Exception $e) {
            return $this->mensageiro
                ->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, 'Erro ao validar datas!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro(
                    $e->getMessage(), Ead1_IMensageiro::ERRO, 'Erro ao retornar o(s) lancamento(s) da venda!');
        }
    }

    /**
     *
     * Gera ocorrencia
     * @param VwResgateAlunos $resgateAlunosTO , UsuarioTO $atendente
     * @return Ead1_Mensageiro
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function gerarOcorrenciaResgate(VwResgateAlunosTO $vwResgateAlunosTO, UsuarioTO $atendente)
    {

        try {

            // Criando obj configuraçãoEntidade
            $configTO = new ConfiguracaoEntidadeTO ();
            $configTO->setId_entidade($configTO->getSessao()->id_entidade);
            $configTO->fetch(true, true, true);

            // Buscando o texto padrão com base no id_textoresgate do ConfiguracaoEntidade
            $texto = new TextoSistemaTO();
            $texto->setId_textosistema($configTO->getId_textoresgate());
            $texto->fetch(true, true, true);

            //Criando a ocorrencia a ser salva
            $ocorrenciaTO = new OcorrenciaTO();
            $ocorrenciaTO->setId_matricula($vwResgateAlunosTO->getId_matricula());
            $ocorrenciaTO->setId_evolucao(OcorrenciaTO::EVOLUCAO_AGUARDANDO_ATENDENTE);
            $ocorrenciaTO->setId_situacao(OcorrenciaTO::SITUACAO_PENDENTE);
            $ocorrenciaTO->setId_usuariointeressado($vwResgateAlunosTO->getId_usuario());
            $ocorrenciaTO->setId_categoriaocorrencia(OcorrenciaTO::CATEGORIA_TRAMITE);
            $ocorrenciaTO->setId_assuntoco($configTO->getId_assuntoresgate());
            $ocorrenciaTO->setId_saladeaula($vwResgateAlunosTO->getId_saladeaula());
            $ocorrenciaTO->setSt_titulo("Ocorrência de Resgate Aluno - " . $vwResgateAlunosTO->getSt_nomecompleto());
            $ocorrenciaTO->setSt_ocorrencia($texto->getSt_textosistema());
            $ocorrenciaTO->setId_entidade($configTO->getId_entidade());
            $ocorrenciaTO->setId_usuariocadastro($atendente->getId_usuario());
            $ocorrenciaTO->setId_motivoocorrencia(MotivoOcorrenciaTO::MOTIVO_RESGATE);

            //Criando uma instancia de CAtencaoBO para salvar a ocorrencia
            $catencaoBO = new CAtencaoBO();
            $mensageiro = $catencaoBO->salvarOcorrencias($ocorrenciaTO);

            //Registrando a vincula��o com atendente
            $novaOcorrencia = $mensageiro->getMensagem();
            $ocorrenciaResponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaResponsavel->setId_usuario($atendente->getId_usuario());
            $ocorrenciaResponsavel->setId_ocorrencia($novaOcorrencia[0]->getId_ocorrencia());
            $ocorrenciaResponsavel->setBl_ativo(true);
            $dao = new CAtencaoDAO();
            $dao->vincularResponsavelOcorrencia($ocorrenciaResponsavel);

            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro->setMensageiro("Ocorrência gerada com sucesso!", Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getCode());
        }

        return $this->mensageiro;
    }

    /**
     * Rotina que decide qual alteração fará em uma matrícula a partir de uma ocorrência ou de uma matrícula
     * @param MatriculaTO $matriculaTO
     * @param $st_acao
     * @param $st_motivo
     * @param OcorrenciaTO|null $ocorrenciaTO
     * @param array|null $arrdatas
     * @return Ambigous|Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function alterarMatricula(MatriculaTO $matriculaTO,
                                     $st_acao,
                                     $st_motivo,
                                     OcorrenciaTO $ocorrenciaTO = null,
                                     array $arrdatas = null)
    {
        switch ($st_acao) {
            case 'bloquear':
                return $this->bloquearMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            case 'cancelar':
                return $this->cancelarMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            case 'transferir':
                return $this->transferirMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            case 'trancar':
                return $this->trancarMatricula($matriculaTO, $st_motivo, $ocorrenciaTO, $arrdatas);
                break;
            case 'liberar';
                return $this->liberarMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            case 'anular';
                return $this->anularMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            case 'prazo':
                return $this->decursoPrazoMatricula($matriculaTO, $st_motivo, $ocorrenciaTO);
                break;
            default:
                break;
        }
    }

    //QUANDO VEM DA MATRICULA< ELE TA GRAVANDO A OCORRENCIA E NAO DEVE
    //VERIFICAR O PORQUE ELE TA FAZENDO ISSO

    /**
     * Método que libera uma matrícula
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function liberarMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $matriculaDAO = new MatriculaDAO();
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_MATRICULA_CURSANDO);
            $matriculaTO->setId_situacao(50);

            if ($matriculaDAO->editarMatricula($matriculaTO)) {

                //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                if ($ocorrencia->getId_ocorrencia()):
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Liberar Acesso Matrícula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                endif;

                //salva trâmite para a matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Liberar Acesso Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);
            }
            return $this->mensageiro->setMensageiro('Matrícula liberada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao liberada matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que bloqueia uma matrícula
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function bloquearMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $matriculaDAO = new MatriculaDAO();
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_BLOQUEADO);
            if ($matriculaDAO->editarMatricula($matriculaTO)) {
                //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                if ($ocorrencia->getId_ocorrencia()) {
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Bloqueio de Matrícula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                }

                //salva trâmite para a matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Bloqueio de Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);
            }
            return $this->mensageiro->setMensageiro('Matrícula bloqueada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao bloquear matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que anula uma matrícula
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function anularMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $matriculaDAO = new MatriculaDAO();
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_ANULADO);

            if ($matriculaDAO->editarMatricula($matriculaTO)) {

                //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                if ($ocorrencia->getId_ocorrencia()) {
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Anulação de Matrícula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                }

                //salva trâmite para a matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Anulação de Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);

                //Ele será desalocado das salas de aula cujas a data de termino seja menor que a data atual
                $mensageiro = $this->retornaSalasDeAulaDesalocacao($matriculaTO, array('dt_atual' => date('d-m-Y')));
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $salaBO = new SalaDeAulaBO();

                    foreach ($mensageiro->getMensagem() as $vw) {
                        if ($vw instanceof VwSalaDisciplinaAlocacaoTO) {

                            $alocacaoTO = new AlocacaoTO();
                            $alocacaoTO->setId_saladeaula($vw->getId_saladeaula());
                            $alocacaoTO->setId_matriculadisciplina($vw->getId_matriculadisciplina());
                            $mensageiroAlocacao = $salaBO->inativarAlocacao($alocacaoTO);
                            if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO) {
                                throw new Zend_Exception($mensageiroAlocacao->getMensagem());
                            }

                        }

                    }
                } else if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $this->mensageiro->setMensageiro($mensageiro->getMensagem(), $mensageiro->getTipo());
                }
            }
            return $this->mensageiro->setMensageiro('Matrícula anulada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao anular matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function decursoPrazoMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        $matriculaDAO = new MatriculaDAO();
        $matriculaDAO->beginTransaction();
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULA_DECURSO_DE_PRAZO);

            if ($matriculaDAO->editarMatricula($matriculaTO)) {

                //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                if ($ocorrencia->getId_ocorrencia()) {
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Cancelamento de Matrícula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                }

                //salva trâmite para a matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Decurso de Prazo da Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);

                //Ele será desalocado das salas de aula cujas a data de termino seja menor que a data atual
                $mensageiro = $this->retornaSalasDeAulaDesalocacao($matriculaTO, array('dt_atual' => date('d-m-Y')));
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $salaBO = new SalaDeAulaBO();

                    foreach ($mensageiro->getMensagem() as $vw) {
                        if ($vw instanceof VwSalaDisciplinaAlocacaoTO) {

                            $alocacaoTO = new AlocacaoTO();
                            $alocacaoTO->setId_saladeaula($vw->getId_saladeaula());
                            $alocacaoTO->setId_matriculadisciplina($vw->getId_matriculadisciplina());
                            $mensageiroAlocacao = $salaBO->inativarAlocacao($alocacaoTO);
                            if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO) {
                                throw new Zend_Exception($mensageiroAlocacao->getFirstMensagem());
                            }

                        }

                    }
                } else if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $this->mensageiro->setMensageiro($mensageiro->getMensagem(), $mensageiro->getTipo());
                }

                $this->mensageiro
                    ->setMensageiro('A evolução da matrícula foi alterada para Decurso de Prazo.',
                        Ead1_IMensageiro::SUCESSO
                    );
            }
            $matriculaDAO->commit();
        } catch (Zend_Exception $e) {
            $matriculaDAO->rollBack();
            $this->mensageiro->setMensageiro('Erro ao cancelar matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que cancela uma matricula e desaloca as salas de aula que a data de termino seja menor que a data atual
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function cancelarMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        $negocio = new \G2\Negocio\Matricula();

        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }

            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULA_CANCELADO);

            /** @var \G2\Entity\Matricula $en_matricula */
            $en_matricula = $negocio->find('\G2\Entity\Matricula', $matriculaTO->getId_matricula());

            if ($en_matricula instanceof G2\Entity\Matricula) {
                $negocio->beginTransaction();
                $en_matricula->setId_evolucao($negocio
                    ->getReference('G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO));
                $returnSave = $negocio->save($en_matricula);
                $negocio->commit();

                if ($returnSave instanceof G2\Entity\Matricula) {

                    //Ele será desalocado das salas de aula cujas a data de termino seja menor que a data atual
                    $mensageiro = $this->retornaSalasDeAulaDesalocacao($matriculaTO, array('dt_atual' => date('d-m-Y')));

                    if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $salaBO = new SalaDeAulaBO();

                        foreach ($mensageiro->getMensagem() as $vw) {
                            if ($vw instanceof VwSalaDisciplinaAlocacaoTO) {

                                $alocacaoTO = new AlocacaoTO();
                                $alocacaoTO->setId_saladeaula($vw->getId_saladeaula());
                                $alocacaoTO->setId_matriculadisciplina($vw->getId_matriculadisciplina());
                                $mensageiroAlocacao = $salaBO->inativarAlocacao($alocacaoTO);
                                if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO) {
                                    throw new Zend_Exception($mensageiroAlocacao->getFirstMensagem());
                                }
                            }
                        }
                    } else if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                        $this->mensageiro->setMensageiro($mensageiro->getMensagem(), $mensageiro->getTipo());
                    }

                    //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                    if ($ocorrencia->getId_ocorrencia()) {
                        $tramite = new TramiteTO();
                        $tramite->setBl_visivel(false);
                        $tramite->setSt_tramite('Cancelamento de Matrícula: ' . $st_motivo);
                        $tramite->setId_tipotramite(\G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO);
                        $tramite->setId_entidade($negocio->sessao->id_entidade);
                        $tramite->setId_usuario($negocio->sessao->id_usuario);
                        $cAtencaoBO = new CAtencaoBO();
                        $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                    }

                    //salva trâmite para a matrícula
                    $tramiteBO = new TramiteBO();
                    $tramiteMatricula = new TramiteTO();
                    $tramiteMatricula->setBl_visivel(true);
                    $tramiteMatricula->setId_tipotramite(\G2\Constante\TipoTramite::MATRICULA_EVOLUCAO);
                    $tramiteMatricula->setSt_tramite('Cancelamento de Matrícula: ' . $st_motivo);
                    $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);

                    // Suspender aluno de TODAS as salas que ele está alocado
                    // desde que a sala não esteja compartilhada com outra matrícula
                    $ws_moodle = new \MoodleAlocacaoWebServices($en_matricula->getId_entidadematricula());
                    $ws_moodle->suspenderAlunoSalas($en_matricula);

                    $this->mensageiro->setMensageiro('Matrícula cancelada com sucesso!', Ead1_IMensageiro::SUCESSO);
                } else {
                    $negocio->rollback();
                }
            }
        } catch (Zend_Exception $e) {
            $negocio->rollback();
            $this->mensageiro->setMensageiro('Erro ao cancelar matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Matrícula cancelada com sucesso! <br>'. $e->getMessage(),Ead1_IMensageiro::SUCESSO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que trasnfere ( na evolução para trasnferido de entidade) e cancela uma matricula,
     * desaloca as salas de aula que a data de termino seja menor que a data atual
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function transferirMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia)
    {
        $matriculaDAO = new MatriculaDAO();
        $matriculaDAO->beginTransaction();
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e foi passado!');
            }
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_TRANSFERIR_ENTIDADE);

            if ($matriculaDAO->editarMatricula($matriculaTO)) {

                //caso seja da central de ocorrencias, grava tramite não visivel na ocorrencia
                if ($ocorrencia->getId_ocorrencia()):
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Cancelamento de MatrÃ­cula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                endif;

                //salva tramite para a Matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Cancelamento de Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);

                //Ele sera desalocado das salas de aula cujas a data de termino seja menor que a data atual
                $mensageiro = $this->retornaSalasDeAulaDesalocacao($matriculaTO, array('dt_atual' => date('d-m-Y')));
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $salaBO = new SalaDeAulaBO();

                    foreach ($mensageiro->getMensagem() as $vw) {
                        if ($vw instanceof VwSalaDisciplinaAlocacaoTO) {

                            $alocacaoTO = new AlocacaoTO();
                            $alocacaoTO->setId_saladeaula($vw->getId_saladeaula());
                            $alocacaoTO->setId_matriculadisciplina($vw->getId_matriculadisciplina());
                            $mensageiroAlocacao = $salaBO->inativarAlocacao($alocacaoTO);
                            if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO) {
                                throw new Zend_Exception($mensageiroAlocacao->getMensagem());
                            }

                        }

                    }
                } else if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $this->mensageiro->setMensageiro($mensageiro->getMensagem(), $mensageiro->getTipo());
                }


                $this->mensageiro
                    ->setMensageiro(
                        'Evolução alterada para Transferido de Entidade, com sucesso!', Ead1_IMensageiro::SUCESSO);
            }
            $matriculaDAO->commit();
        } catch (Zend_Exception $e) {
            $matriculaDAO->rollBack();
            $this->mensageiro
                ->setMensageiro('Erro ao alterar evolução da matrícuula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que tranca uma matricula e chama a desalocacao das salas cuja as datas de inicio e
     * termino toquem as datas de inicio e termino da trancamento.
     * @param MatriculaTO $matriculaTO
     * @param $st_motivo
     * @param OcorrenciaTO $ocorrencia
     * @param array $arrDatas
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function trancarMatricula(MatriculaTO $matriculaTO, $st_motivo, OcorrenciaTO $ocorrencia, array $arrDatas)
    {
        $matriculaDAO = new MatriculaDAO();
        $matriculaDAO->beginTransaction();
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $matriculaTO->fetch(true, true, true);
            $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_TRANCADO);
            if ($matriculaDAO->editarMatricula($matriculaTO)) {

                //caso seja da central de ocorrências, grava trâmite não-visível na ocorrência
                if ($ocorrencia->getId_ocorrencia()):
                    $tramite = new TramiteTO();
                    $tramite->setBl_visivel(false);
                    $tramite->setSt_tramite(('Trancamento de Matrícula: ' . $st_motivo));
                    $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                    $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                    $tramite->setId_usuario($matriculaTO->getSessao()->id_usuario);
                    $cAtencaoBO = new CAtencaoBO();
                    $cAtencaoBO->salvaTramite($ocorrencia, $tramite);
                endif;

                //salva trâmite para a matrícula
                $tramiteBO = new TramiteBO();
                $tramiteMatricula = new TramiteTO();
                $tramiteMatricula->setBl_visivel(true);
                $tramiteMatricula->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                $tramiteMatricula->setSt_tramite(('Trancamento de Matrícula: ' . $st_motivo));
                $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteMatricula);

                //Ele será desalocado das salas de aula cuja as datas de
                // inicio e termino toquem as datas de inicio e termino da trancamento.
                $mensageiro = $this->retornaSalasDeAulaDesalocacao($matriculaTO, $arrDatas);
                if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $salaBO = new SalaDeAulaBO();

                    foreach ($mensageiro->getMensagem() as $vw) {
                        $alocacaoTO = new AlocacaoTO();
                        $alocacaoTO->setId_saladeaula($vw->getId_saladeaula());
                        $alocacaoTO->setId_matriculadisciplina($vw->getId_matriculadisciplina());
                        $mensageiroAlocacao = $salaBO->inativarAlocacao($alocacaoTO);
                        if ($mensageiroAlocacao->getTipo() == Ead1_IMensageiro::ERRO) {
                            throw new Zend_Exception($mensageiroAlocacao->getMensagem());
                        }
                    }
                } else if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $this->mensageiro->setMensageiro($mensageiro->getMensagem(), $mensageiro->getTipo());
                }

                $this->mensageiro->setMensageiro('Matrícula trancada com sucesso!', Ead1_IMensageiro::SUCESSO);
            }
            $matriculaDAO->commit();
        } catch (Zend_Exception $e) {
            $matriculaDAO->rollBack();
            $this->mensageiro->setMensageiro('Erro ao trancada matrícula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Retorna as salas de aula para desalocação do Cancelamento E Trancamento da Matrícula
     * @param MatriculaTO $matriculaTO
     * @param array $arrDatas
     * @throws Zend_Exception
     * @throws Zend_Date_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaSalasDeAulaDesalocacao(MatriculaTO $matriculaTO, array $arrDatas = null)
    {
        try {
            if (!$matriculaTO->getId_matricula()) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado!');
            }
            $where = '';
            $vwSalaDisciplinaAlocacaoTO = new VwSalaDisciplinaAlocacaoTO();

            if ($arrDatas) {
                //Trancamento - dt_encerramento >= datadeiniciotrancamento OR dt_abertura <= datadeterminotrancamento
                if (!empty($arrDatas['dt_encerramento']) && !empty($arrDatas['dt_abertura'])) {
                    $dtEncerramento = new Zend_Date($arrDatas['dt_encerramento'], null, 'pt-BR');
                    $dtAbertura = new Zend_Date($arrDatas['dt_abertura'], null, 'pt-BR');

                    $where = "dt_abertura <= '" . $dtAbertura->toString('yyyy-MM-dd') .
                        "' AND (dt_encerramento >= '"
                        . $dtEncerramento->toString('yyyy-MM-dd') . "' or dt_encerramento is null)";
                }

                //Cancelamento - Ele será desalocado das salas de aula cujas a data de termino seja menor que a data atual
                if (array_key_exists('dt_atual', $arrDatas)) {
                    $dtAtual = new Zend_Date($arrDatas['dt_atual'], null, 'pt-BR');
                    if (Zend_Date::isDate($dtAtual)) {
                        $where = 'dt_encerramento >= \''
                            . $dtAtual->toString('yyyy-MM-dd')
                            . '\' or dt_encerramento is null';
                    }
                }

                if (isset($dtInicio) && isset($dtTermino) && $dtInicio->isLater($dtTermino)) {
                    throw new Zend_Date_Exception('Data de Abertura não pode ser maior que a Data Fim');
                }
            }

            $vwSalaDisciplinaAlocacaoTO->setId_matricula($matriculaTO->getId_matricula());
            $retorno = $this->dao->retornaSalasDeAulaDesalocar($vwSalaDisciplinaAlocacaoTO, $where);
            $retorno = $retorno->toArray();

            if (empty($retorno)) {
                $this->mensageiro->setMensageiro('Nenhuma sala de aula encontrada!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro
                    ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwSalaDisciplinaAlocacaoTO()),
                        Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Ocorreu um erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula por curso para quem tem o perfil financeiro.
     * @param VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO
     * @param array $filters array com parametros de pesquisa
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalasCurso_Financeiro(
        VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO,
        array $filters = NULL
    )
    {
        if (!$vwEncerramentoSalasCursoTO->getId_entidade()) {
            $vwEncerramentoSalasCursoTO->setId_entidade($vwEncerramentoSalasCursoTO->getSessao()->id_entidade);
        }

        switch ($filters['tipoEncerramento']) {
            case 'coordenador':
                $where = 'dt_encerramentocoordenador is null 
                          and  id_entidade = ' . $vwEncerramentoSalasCursoTO->getId_entidade();
                break;
            case 'pedagogico':
                $where = 'dt_encerramentocoordenador is not null
                         and dt_encerramentopedagogico is null
                         and id_entidade = ' . $vwEncerramentoSalasCursoTO->getId_entidade();
                break;
            case 'financeiro':
                $where = 'dt_encerramentocoordenador is not null
                         and dt_encerramentopedagogico is not null
                         and dt_encerramentofinanceiro is null
                         and id_entidade = ' . $vwEncerramentoSalasCursoTO->getId_entidade();
                break;
            case 'pagos':
                $where = 'dt_encerramentocoordenador is not null
                         and dt_encerramentopedagogico is not null
                         and dt_encerramentofinanceiro is not null
                         and id_entidade = ' . $vwEncerramentoSalasCursoTO->getId_entidade();
                break;
            default :
                $where = 'dt_encerramentocoordenador is not null 
                          and dt_encerramentopedagogico is not null
                          and dt_encerramentofinanceiro is not null
                          and id_entidade = ' . $vwEncerramentoSalasCursoTO->getId_entidade();
                break;
        }

        if (isset($filters['idUsuarioProfessor']) && $filters['idUsuarioProfessor'] > 0) {
            $where .= ' and  id_usuarioprofessor = ' . $filters['idUsuarioProfessor'];
        }
        if (isset($filters['idTipoDisciplina']) && $filters['idTipoDisciplina'] == 0) {
            $where .= ' and id_tipodisciplina = 1';
        } else {
            $where .= ' and id_tipodisciplina = ' . $filters['idTipoDisciplina'];
        }

        return $this->retornarVwEncerramentoSalasCurso($vwEncerramentoSalasCursoTO, $where);
    }

    /**
     * Metodo que retorna dados de encerramento de sala de aula por curso.
     * @param VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEncerramentoSalasCurso(VwEncerramentoSalasCursoTO $vwEncerramentoSalasCursoTO, $where)
    {
        try {
            $dados = $this->dao->retornarVwEncerramentoSalasCurso($vwEncerramentoSalasCursoTO, $where)->toArray();
            if (!$dados) {
                return $this->mensageiro
                    ->setMensageiro('Nenhum Registro de dados de encerramento encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEncerramentoSalasCursoTO()),
                    Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro ao Retornar Dados de Encerramento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Método que gera o link para relatorio de resumo de interacoes de tcc no moodle
     * @param Ead1_TO_Dinamico $vw
     * @return Ead1_Mensageiro
     */
    public function gerarLinkResumoInteracoes(Ead1_TO_Dinamico $vw)
    {
        try {

            if (!($vw instanceof VwEncerramentoCoordenadorTO) && !($vw instanceof VwEncerramentoSalasTO)) {
                throw new Zend_Exception('A vw passada não é uma instância do objeto esperado!');
            }
            $salaBO = new SalaDeAulaBO();
            $sala = new VwSalaDeAulaPerfilProjetoIntegracaoTO();
            $sala->setId_entidade($vw->getSessao()->id_entidade);
            $sala->setId_usuario($vw->getSessao()->id_usuario);
            $sala->setId_disciplina($vw->getId_disciplina());
            $sala->setId_saladeaula($vw->getId_saladeaula());
            $mensageiro = $salaBO->retornarVwSalaDeAulaPerfilProjetoIntegracao($sala);
            $mensageiro->subtractMensageiro();
            $sala = $mensageiro->getFirstMensagem();
            if (!$sala->getSt_loginintegrado()) {
                throw new Zend_Exception('Usuário sem integração com o sistema para acesso ao relatório!');
            }
            $USUARIO_MOODLE = $sala->getSt_loginintegrado();
            $SENHA_MOODLE = $sala->getSt_senhaintegrada();
            $CURSO_MOODLE = $sala->getSt_codsistemacurso();
            $COD_SISTEMA_ENT = $sala->getSt_codsistema();
            $usuarioIntegracao = new UsuarioIntegracaoTO();
            $usuarioIntegracao->setId_usuario($vw->getId_aluno());
            $usuarioIntegracao->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracao->setId_entidade($vw->getSessao()->id_entidade);
            $usuarioIntegracao->fetch(false, true, true);
            if (!$usuarioIntegracao->getSt_codusuario()) {
                throw new Zend_Exception('Aluno não integrado com o sistema!');
            }

            $ID_USER = $usuarioIntegracao->getSt_codusuario();
            $st_caminho = $sala->getst_caminho();
//		    	$INTEGRACAO = $sala->getSt_integracao();
//		    	$COD_USUARIO = $sala->getSt_codusuario();

            switch ($sala->getId_sistema()) {

                case SistemaTO::MOODLE:
                    $sessaoMoodle = new Zend_Session_Namespace('moodle');
                    $sessaoMoodle->usuario = $USUARIO_MOODLE;
                    $sessaoMoodle->senha = $SENHA_MOODLE;
                    $sessaoMoodle->curso = $CURSO_MOODLE;
                    $URL = substr($COD_SISTEMA_ENT, 0, -26)
                        . "login/indexg2.php?u=" . $USUARIO_MOODLE
                        . "&p=" . $SENHA_MOODLE
                        . "&rel=true&id_sala=" . $CURSO_MOODLE
                        . "&id_user=" . $ID_USER;
                    break;

                default:
                    THROW new Zend_Exception("Sistema não implementado!");
                    break;
            }
            return $this->mensageiro->setMensageiro($URL, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function retornarExtratoPagamento(array $params)
    {
        $vw = new VwEncerramentoSalasTO($params);
        $id_usuarioprofessor = $params['id_usuarioprofessor']
            ? $params['id_usuarioprofessor'] : $vw->getSessao()->id_usuario;
        $where = 'id_usuarioprofessor = ' . $id_usuarioprofessor . ' AND dt_encerramentofinanceiro IS NOT NULL ';
        if ($params['mes']) {
            $where .= ' AND MONTH(dt_encerramentofinanceiro) = ' . $params['mes'];
        }

        if ($params['ano']) {
            $where .= ' AND YEAR(dt_encerramentofinanceiro) = ' . $params['ano'];
        }

        return $this->retornarVwEncerramentoSalas($vw, $where);
    }



    /**
     * Método que gera o link para relatorio de encerramento do coordenador
     * @param Ead1_TO_Dinamico $vw
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function gerarLinkRelatorioMoodleEncerramento(Ead1_TO_Dinamico $vw)
    {
        try {
            if (!($vw instanceof VwEncerramentoCoordenadorTO) && !($vw instanceof VwEncerramentoSalasTO)) {
                throw new Zend_Exception('A vw passada não é uma instância do objeto esperado!');
            }

            /** @var \G2\Entity\SalaDeAula $salaDeAula */
            $salaDeAula = (new \G2\Negocio\Negocio())
                ->find(\G2\Entity\SalaDeAula::class, $vw->getId_saladeaula());

            if (!$salaDeAula) {
                throw  new Zend_Exception("Sala de aula não encontrada.");
            }

            // Verificar se o usuario logado possui login integrado ao moodle para
            // acessar o relatorio em tal entidade e saladeaula

            $salaBO = new SalaDeAulaBO();
            $salaDeAulaPerfilProjetoIntegracao = new VwSalaDeAulaPerfilProjetoIntegracaoTO();

            //pegando o id da entidade da sala de aula
            $salaDeAulaPerfilProjetoIntegracao->setId_entidade($salaDeAula->getId_entidade());
            $salaDeAulaPerfilProjetoIntegracao->setId_usuario($vw->getSessao()->id_usuario);
            $salaDeAulaPerfilProjetoIntegracao->setId_saladeaula($vw->getId_saladeaula());
            $mensageiro = $salaBO->retornarVwSalaDeAulaPerfilProjetoIntegracao($salaDeAulaPerfilProjetoIntegracao, null, 1);
            $mensageiro->subtractMensageiro();
            $salaDeAulaPerfilProjetoIntegracao = $mensageiro->getFirstMensagem();

            if (!$salaDeAulaPerfilProjetoIntegracao->getSt_loginintegrado()) {
                throw new Zend_Exception(
                    'Usuário logado não possui integração com o sistema para acesso ao relatório!'
                );
            }

            $moodleUsuario = new MoodleUsuariosWebServices($salaDeAulaPerfilProjetoIntegracao->getId_entidade(),
                $salaDeAula->getid_entidadeintegracao()->getId_entidadeintegracao());

            $userExisteNoMoodle = $moodleUsuario->retornarUsuarioByUserName($salaDeAulaPerfilProjetoIntegracao->getSt_loginintegrado());

            if (!$userExisteNoMoodle) {
                throw new Zend_Exception('Usuário atualmente logado no Portal não existe no Moodle.');
            }

            $USUARIO_MOODLE = $salaDeAulaPerfilProjetoIntegracao->getSt_loginintegrado();
            $SENHA_MOODLE = $salaDeAulaPerfilProjetoIntegracao->getSt_senhaintegrada();
            $CURSO_MOODLE = $salaDeAulaPerfilProjetoIntegracao->getSt_codsistemacurso();
            $COD_SISTEMA_ENT = $salaDeAulaPerfilProjetoIntegracao->getSt_codsistema();

            if ($vw->getId_usuarioprofessor()) {
                $idUsuarioRelatorio = $vw->getId_usuarioprofessor();
                $stUsuario = 'professor';
            } else if ($vw->getId_aluno()) {
                $idUsuarioRelatorio = $vw->getId_aluno();
                $stUsuario = 'aluno';
            } else {
                $idUsuarioRelatorio = $vw->getSessao()->id_usuario;
                $stUsuario = 'usuário';
            }

            $usuarioIntegracao = new UsuarioIntegracaoTO();
            $usuarioIntegracao->setId_usuario($idUsuarioRelatorio);
            $usuarioIntegracao->setId_sistema(SistemaTO::MOODLE);
            $usuarioIntegracao->setId_entidade($vw->getSessao()->id_entidade);
            $usuarioIntegracao->setId_entidadeintegracao($salaDeAula->getid_entidadeintegracao()->getId_entidadeintegracao());
            $usuarioIntegracao->fetch(false, true, true);

            if (!$usuarioIntegracao->getSt_codusuario()) {
                throw new Zend_Exception("Este {$stUsuario} não está integrado com o moodle!");
            }

            $ID_USER = $usuarioIntegracao->getSt_codusuario();

            $st_caminho = $salaDeAulaPerfilProjetoIntegracao->getst_caminho();

            switch ($salaDeAulaPerfilProjetoIntegracao->getId_sistema()) {

                case SistemaTO::MOODLE:
                    $sessaoMoodle = new Zend_Session_Namespace('moodle');
                    $sessaoMoodle->usuario = $USUARIO_MOODLE;
                    $sessaoMoodle->senha = $SENHA_MOODLE;
                    $sessaoMoodle->curso = $CURSO_MOODLE;
                    $URL = substr($COD_SISTEMA_ENT, 0, -26)
                        . "login/indexg2.php?u=" . $USUARIO_MOODLE
                        . "&p=" . $SENHA_MOODLE
                        . "&rel=coordenador&id_sala=" . $CURSO_MOODLE
                        . "&id_user=" . $ID_USER;
                    break;

                default:
                    THROW new Zend_Exception("Sistema não implementado!");
                    break;
            }

            return $this->mensageiro->setMensageiro($URL, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro
                ->setMensageiro('Erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

}

<?php
/**
 * Classe com regras de negócio para o AcessoAfiliado
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage bo
 */
class AcessoAfiliadoBO extends Ead1_BO {



	/**
	 * @var AcessoAfiliadoDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new AcessoAfiliadoDAO();
	}
    
    
    
    
	/**
	 * Método de listagem de AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarAcessoAfiliado(AcessoAfiliadoTO $to){
		
		try {
		
			$dados = $this->dao->listarAcessoAfiliado($to);
			if($dados){
				return new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
			}
			return new Ead1_Mensageiro('Nenhum AcessoAfiliado encontrado!', Ead1_IMensageiro::AVISO);
		
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar AcessoAfiliado: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
    
    
	/**
	 * Metodo que decide se cadastra ou edita o AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarAcessoAfiliado(AcessoAfiliadoTO $to){
	
		$this->dao->beginTransaction();
		try {
			
			
			
			if($to->getId_acessoafiliado()){
				$mensageiro = $this->editarAcessoAfiliado($to);
			} else {
				$mensageiro =  $this->cadastrarAcessoAfiliado($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o AcessoAfiliado: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}


	
	/**
	 * Método que cadastra umo AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see AcessoAfiliadoBO::salvarAcessoAfiliado();
	 */
	public function cadastrarAcessoAfiliado(AcessoAfiliadoTO $to){
		$this->dao->beginTransaction();
		try {
			
			if(!$to->getId_contratoafiliado()){
				throw new Zend_Exception('O Contrato não foi informado!');
			}
			
			$to->setId_acessoafiliado($this->dao->cadastrarAcessoAfiliado($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o AcessoAfiliado, ".$e->getMessage(), Ead1_IMensageiro::ERRO);
		}
	}
    
    
    
	/**
	 * Método que edita umo AcessoAfiliado
	 * @param AcessoAfiliadoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see AcessoAfiliadoBO::salvarAcessoAfiliado();
	 */
	public function editarAcessoAfiliado(AcessoAfiliadoTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarAcessoAfiliado($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o AcessoAfiliado, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    

}
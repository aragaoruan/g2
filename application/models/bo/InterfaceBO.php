<?php
/**
 * Classe de comunicação com Flex para interações com objeto Interface
 * @author Eder Lamar - edermariano@gmail.com
 * @package application
 * @subpackage ro
 */
class InterfaceBO extends Ead1_BO{
	
	/**
	 * Método que retorna os dados de Interface Pessoa
	 * @param VwInterfacePessoaTO $vwInterfacePessoaTO
	 */
	public function retornarVwInterfacePessoa(VwInterfacePessoaTO $vwInterfacePessoaTO){
		return new Ead1_Mensageiro($vwInterfacePessoaTO->fetch());
	}
	
	/**
	 * Método que cadastra a configuração de interface
	 * @param array $arInterfacePessoaTO array de InterfacePessoaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArInterfacePessoa(array $arInterfacePessoaTO){
		$dao = new InterfaceDAO();
		$dao->beginTransaction();
		try{
			$interfacePessoaTO = $arInterfacePessoaTO[0];
			if(!$interfacePessoaTO->getId_usuario() || !$interfacePessoaTO->getId_entidade()){
				throw new Zend_Exception("Usuário e/ou entidade não informados!");
			}
			$dao->deletarInterfacePessoa($interfacePessoaTO);
			foreach($arInterfacePessoaTO as $chave => $interfacePessoaTO){
				if(!$interfacePessoaTO->getId_tipointerfacepessoa() || $interfacePessoaTO->getSt_color() === NULL){
					throw new Zend_Exception("Cor e/ou propriedade não informados!");
				}
				$mensageiro = ($interfacePessoaTO->getId_interfacepessoa() ? $this->editarInterfacePessoa($interfacePessoaTO) : $this->inserirInterfacePessoa($interfacePessoaTO));
				if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO){
					$arInterfacePessoaTONovo[$chave] = $mensageiro->getCurrent();
				}else{
					throw new Zend_Exception($mensageiro->getCurrent());
				}
			}
			$dao->commit();
			return new Ead1_Mensageiro($arInterfacePessoaTONovo);
		}catch(Zend_Exception $e){
			$dao->rollBack();
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	/**
	 * Método que cadastra a configuração de interface
	 * @param InterfacePessoaTO $interfacePessoaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		try{
			return ($interfacePessoaTO->getId_interfacepessoa() ? $this->editarInterfacePessoa($interfacePessoaTO) : $this->inserirInterfacePessoa($interfacePessoaTO));
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	/**
	 * Método que edita uma configuração de interface da pessoa 
	 * @param InterfacePessoaTO $interfacePessoaTO
	 */
	public function editarInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		try{
			$dao = new InterfaceDAO();
			return new Ead1_Mensageiro($dao->editarInterfacePessoa($interfacePessoaTO), Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	/**
	 * Método que insere uma configuração de interface da pessoa 
	 * @param InterfacePessoaTO $interfacePessoaTO
	 */
	public function inserirInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
	try{
			$dao = new InterfaceDAO();
			return new Ead1_Mensageiro($dao->inserirInterfacePessoa($interfacePessoaTO), Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
}
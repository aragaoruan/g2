<?php

/**
 * Classe que faz o direcionamento de cada tratamento referente a tabela tb_taxa
 *
 * @author Elcio Mauro Guimarães
 * @package models
 * @subpackage bo
 *
 */
class PosConfirmacaoVendaBO extends Ead1_BO
{

    /**
     * Executa os Procedimentos pós confirmação de venda, como Alterar data do contrato, emitir certificado, enviar e-mail
     *
     * @param VendaTO $vendaTO
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoPosConfirmacaoVenda(VendaTO $vendaTO)
    {
        try {
            if ($vendaTO->getId_evolucao() != VendaTO::EVOLUCAO_CONFIRMADA) {
                throw new Zend_Exception("Essa venda nào foi confirmada");
            }

            if ($vendaTO->getid_ocorrencia()) {
                $this->alteraEvolucaoOcorrencia($vendaTO->getid_ocorrencia());
            }

            $produtoVendaTO = new VwProdutoVendaTO();
            $produtoVendaTO->setId_venda($vendaTO->getId_venda());

            $mensagemBO = new MensagemBO();
            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::CONFIRMACAO_VENDA, $vendaTO, TipoEnvioTO::EMAIL, false);

            /**
             * Se houver uma campanha prêmio, envia um email para o usúario.
             */
            if ($vendaTO->getId_campanhacomercialpremio()) {
                $negocio = new \G2\Negocio\Negocio();
                /**
                 * \G2\Entity\CampanhaComercial $campanhaComercial
                 */
                $campanhaComercial = $negocio->find('\G2\Entity\CampanhaComercial', $vendaTO->getId_campanhacomercialpremio());

                if ($campanhaComercial->getId_categoriacampanha() &&
                    $campanhaComercial->getId_categoriacampanha()->getId_categoriacampanha() == \G2\Constante\CategoriaCampanha::PREMIO
                ) {
                    $premio = $negocio->find('\G2\Entity\Premio', $campanhaComercial->getId_premio());

                    if ($premio->getId_tipopremio() == 2) {
                        $negocioCupom = new \G2\Negocio\Cupom();
                        $cupom = $negocioCupom->salvarCupom(
                            array(
                                'st_prefixo' => $premio->getSt_prefixocupompremio(),
                                'id_campanhacomercial' => $premio->getId_cupomcampanha()->getId_campanhacomercial(),
                                'id_tipodesconto' => $premio->getId_tipodescontopremio(),
                                'id_usuariocadastro' => $vendaTO->getId_usuario(),
                                'bl_ativacao' => true,
                                'bl_unico' => true,
                                'dt_inicio' => (new \DateTime())->format('d/m/Y'),
                                'dt_fim' => (new \DateTime())->add(new DateInterval('P1Y'))->format('d/m/Y'),
                                'nu_desconto' => $premio->getNu_descontopremio()
                            )
                        );
                        if ($cupom) {
                            $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_CUPOM_PREMIO, $vendaTO, TipoEnvioTO::EMAIL, false, 0, array('variaveis' => array('#venda_cupom#' => array(str_replace('#', '', $cupom->getSt_codigocupom()))), 'id_venda' => $vendaTO->getId_venda()));
                        }
                    }
                }
            }

            $produtoVendaORM = new VwProdutoVendaORM();
            $produtos = $produtoVendaORM->consulta($produtoVendaTO);

            if ($produtos) {
                foreach ($produtos as $produto) {
                    $this->_executaProcedimentosTipoProduto($produto);
                }
            }
            return new Ead1_Mensageiro("Procedimentos pós venda finalizados!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            //echo "procedimentoPosConfirmacaoVenda > Zend_Exception";
//            throw $e;
            return new Ead1_Mensageiro("Erro ao executar os procedimentos pós venda: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Baseado em cada produto, executa o que lhe for devido, se for devido
     *
     * @param VwProdutoVendaTO $produtoVendaTO
     * @return bool|Ead1_Mensageiro
     * @throws Zend_Exception
     */
    private function _executaProcedimentosTipoProduto(VwProdutoVendaTO $produtoVendaTO)
    {
        switch ($produtoVendaTO->getId_tipoproduto()) {
            case TipoProdutoTO::LIVRO:
                try {

                    $vw = new VwProdutoVendaLivroTO();
                    $vw->setId_produto($produtoVendaTO->getId_produto());
                    $vw->setId_venda($produtoVendaTO->getId_venda());
                    $vw->setId_tipolivro(TipoLivroTO::E_BOOK);
                    $vw->fetch(false, true, true);

                    if (!$vw->getId_livro()) {
                        return true;
                        break;
                    }

                    $vendaTO = new VendaTO();
                    $vendaTO->setId_venda($produtoVendaTO->getId_venda());
                    $vendaTO->fetch(true, true, true);

                    $mensagemBO = new MensagemBO();
                    $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::VENDA_EBOOK, $vendaTO, TipoEnvioTO::EMAIL, false);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }
                    return $mensageiro;
                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao executar os procedimentos para envio dos links de boleto: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }

                break;
            case TipoProdutoTO::TAXA:
                /**
                 * Tipo de Produto TAXA retorna um Exception
                 * Qualquer execução aqui cancela a confirmação da Venda
                 */
                try {

                    $produtoTaxaTO = new VwProdutoTaxaTO();
                    $produtoTaxaTO->setId_produto($produtoVendaTO->getId_produto());
                    $produtoTaxaTO->setId_venda($produtoVendaTO->getId_venda());
                    $produtoTaxaTO->fetch(true, true, true);

                    return $this->executaProcedimentosTaxa($produtoTaxaTO);
                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao executar os procedimentos pós venda por tipo de produto: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }
                break;
            case TipoProdutoTO::SIMULADO:
                try {
                    $negocio = new \G2\Negocio\Negocio();
                    $habilitarAluno = new \AmaisAvaliacao_HabilitarAluno();
                    $habilitarAlunoTO = new \AmaisAvaliacao_HabilitaCadastroTO();

                    $produto = $negocio->find('\G2\Entity\Produto', $produtoVendaTO->getId_produto());
                    $produtoValor = $negocio->findOneBy('\G2\Entity\ProdutoValor', array('id_produto' => $produtoVendaTO->getId_produto()));
                    $aluno = $negocio->findOneBy('\G2\Entity\VwPessoa', array('id_usuario' => $produtoVendaTO->getId_usuario()));

                    if (isset($produto) && isset($produtoValor) && isset($aluno)) {
                        //monta os dados para enviar o aluno
                        $habilitarAlunoTO->setEmail($aluno->getSt_email());
                        $habilitarAlunoTO->setIdentificadorAplicadorProva($produto->getSt_codigoavaliacao());
                        $habilitarAlunoTO->setNome($aluno->getSt_nomecompleto());
                        $habilitarAlunoTO->setCpf($aluno->getSt_cpf());
                        $habilitarAlunoTO->setCodigoPlanoSistemaOrigem($produto->getId_produto());
                        $habilitarAlunoTO->setCodigoAlunoSistemaOrigem($aluno->getId_usuario());
                        $habilitarAlunoTO->setCidade($aluno->getSt_cidade());
                        $habilitarAlunoTO->setUf($aluno->getSg_uf());
                        $habilitarAlunoTO->setIdentificadorAplicacaoProva($produto->getSt_codigoavaliacao());
                        $habilitarAlunoTO->setIsAtivo(1);
                        $habilitarAlunoTO->setSenha($aluno->getSt_senha());

                        $cadastroAmais = $habilitarAluno->cadastroAluno($habilitarAlunoTO);

                        //se o retorno for true salva o usuario na tb_usuariointegracao
                        if ($cadastroAmais) {
                            $usuaruiIntegracao = new \G2\Negocio\UsuarioIntegracao();

                            //monta os paramentros e salvar a integraçao no G2
                            $usuaruiIntegracao->salvarIntegracao(
                                array(
                                    'id_usuariocadastro' => $aluno->getId_usuario(),
                                    'id_usuario' => $aluno->getId_usuario(),
                                    'st_codusuario' => $aluno->getId_usuario(),
                                    'bl_encerrado' => false,
                                    'id_sistema' => \G2\Constante\Sistema::AMAIS,
                                    'dt_cadastro' => new Zend_Date(),
                                    'id_entidade' => $produtoVendaTO->getSessao()->id_entidade,
                                )
                            );
                        }
                    } else {
                        throw new Zend_Exception('Erro ao fazer a busca do produto ou aluno.');
                    }
                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao executar agendamento pós venda para uma avaliação: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }
                break;
            case TipoProdutoTO::PRR:
                try {
                    $produtoPrr = new ProdutoPrrTO();
                    $produtoPrr->setId_produto($produtoVendaTO->getId_produto());
                    $produtoPrr->setId_entidade($produtoVendaTO->getSessao()->id_entidade);
                    $produtoPrr->fetch(false, true, true);

                    $vendaProduto = new VendaProdutoTO();
                    $vendaProduto->setId_produto($produtoVendaTO->getId_produto());
                    $vendaProduto->fetch(false, true, true);

                    $disciplinaSala = new DisciplinaSalaDeAulaTO();
                    $disciplinaSala->setId_saladeaula($produtoPrr->getId_saladeaula());
                    $disciplinaSala->fetch(false, true, true);

                    $matriculaDisciplina = new MatriculaDisciplinaTO();
                    $matriculaDisciplina->setId_matricula($vendaProduto->getId_matricula());
                    $matriculaDisciplina->setId_disciplina($disciplinaSala->getId_disciplina());
                    $matriculaDisciplina->fetch(false, true, true);

                    $alocacaoTO = new AlocacaoTO();
                    $alocacaoTO->setId_categoriasala(SalaDeAulaTO::SALA_PRR);
                    $alocacaoTO->setId_matriculadisciplina($matriculaDisciplina->getId_matriculadisciplina());
                    $alocacaoTO->setId_saladeaula($produtoPrr->getId_saladeaula());

                    $salaBO = new SalaDeAulaBO();

                    $mensageiro = $salaBO->salvarAlocacaoAluno($alocacaoTO);

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getFirstMensagem());
                    }
                    return $mensageiro;
                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao executar agendamento pós venda para uma avaliação: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }
                break;
            case TipoProdutoTO::DECLARACOES:
                try {

                    /*
                     * Gera uma ocorrencia de o produto for do tipo 3 : Declaracão
                     */
                    $vwProduto = new \G2\Negocio\Produto();
                    $configuiracaoEntidade = new \G2\Negocio\ConfiguracaoEntidade();
                    $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
                    $result = $vwProduto->retornarProduto(array('id_produto' => $produtoVendaTO->getId_produto()));
                    $dadosConfiguracaoEntidade = $configuiracaoEntidade->findConfiguracaoEntidade(array('id_entidade' => $produtoVendaTO->getId_entidade()), true);
                    $ocorrencia = $negocioOcorrencia->retornaOcorrencia(array('id_venda' => $produtoVendaTO->getId_venda()), $return_array = true);

                    if (!$ocorrencia || !isset($ocorrencia[0]['id_venda'])) {

                        /*
                         * Verifica o tipo de produto se for do tipo emissão online vai criar a ocorrência fechada
                         *
                         */
                        //dados da ocorrência Com situação fechada
                        $ocorrencia = array('ocorrencia' =>
                            array(
                                'id_usuariointeressado' => $produtoVendaTO->getId_usuario(),
                                'id_usuariocadastro' => $produtoVendaTO->getId_usuario(),
                                'id_matricula' => $produtoVendaTO->getId_matricula(),
                                'id_assuntoco' => $dadosConfiguracaoEntidade['id_assuntorequerimento'],
                                'id_categoriaocorrencia' => $dadosConfiguracaoEntidade['id_categoriaocorrencia'],
                                'id_entidade' => $produtoVendaTO->getId_entidade(),
                                'id_evolucao' => ((isset($result[0]) && \G2\Constante\FormaDisponibilizacao::EmissaoOnline == $result[0]->getId_formadisponibilizacao() ?
                                    \G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO : \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO)),
                                'id_situacao' => ((isset($result[0]) && \G2\Constante\FormaDisponibilizacao::EmissaoOnline == $result[0]->getId_formadisponibilizacao() ?
                                    \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA : \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE)),
                                'id_venda' => $produtoVendaTO->getId_venda(),
                                'st_titulo' => 'Requerimento de Declaração',
                                'st_ocorrencia' => "Solicitação de declaração:<br>" .
                                    "Declaração :  <br>" . (isset($result[0]) ? $result[0]->getSt_produto() : "") . '<br>' .
                                    "Forma de disponibilização :" . (isset($result[0]) ? $result[0]->getSt_formadisponibilizacao() : "")
                            )
                        );
                        try {
                            $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
                            $salvarOcorrencia = $negocioOcorrencia->salvarOcorrencia($ocorrencia);
                        } catch (Zend_Exception $e) {
                            return new Ead1_Mensageiro("Erro ao executar os procedimentos pós venda GERAR OCORRÊNCIA: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                        }

                    }

                    /**
                     * Cadastrando uma entrega de declaração para cada produto.
                     */
                    $situacaoNegocio = new \G2\Negocio\Situacao();
                    $objetoEntregaDeclaracao = new \G2\Entity\EntregaDeclaracao();
                    $objetoEntregaDeclaracao->setId_usuariocadastro($negocioOcorrencia->getReference('\G2\Entity\Usuario', $produtoVendaTO->getId_usuario()));
                    $objetoEntregaDeclaracao->setId_textosistema($negocioOcorrencia->getReference('\G2\Entity\TextoSistema', $result[0]->getId_textosistema()));
                    $objetoEntregaDeclaracao->setDt_cadastro(new \DateTime());
                    $objetoEntregaDeclaracao->setId_situacao((\G2\Constante\FormaDisponibilizacao::EmissaoOnline == $result[0]->getId_formadisponibilizacao() ?
                        $situacaoNegocio->find('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_ENTREGADECLARACAO_ENTREGUE) : $situacaoNegocio->find('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_ENTREGADECLARACAO_PENDENTE)));
                    $objetoEntregaDeclaracao->setId_matricula($negocioOcorrencia->getReference('\G2\Entity\Matricula', $produtoVendaTO->getId_matricula()));
                    $objetoEntregaDeclaracao->setId_vendaproduto($negocioOcorrencia->getReference('\G2\Entity\VendaProduto', $produtoVendaTO->getId_vendaproduto()));

                    $negocioOcorrencia->save($objetoEntregaDeclaracao);

                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao criar a ocorrência automatica" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }
                break;
            default:
                return true;
                break;
        }
    }

    /**
     * Executa os procedimentos pós venda relacionados a Taxas
     *
     * @param VwProdutoTaxaTO $vwProdutoTaxa
     * @throws Zend_Exception
     * @return Ead1_Mensageiro|boolean
     */
    public function executaProcedimentosTaxa(VwProdutoTaxaTO $vwProdutoTaxa)
    {

        switch ($vwProdutoTaxa->getId_taxa()) {
            case TaxaTO::EXTENSAO_CONTRATO:

                try {

                    $vwContratoMatriculaProjetoTO = new VwContratoMatriculaProjetoTO();
                    $vwContratoMatriculaProjetoTO->setId_matricula($vwProdutoTaxa->getId_matricula());
                    $vwContratoMatriculaProjetoTO->fetch(false, true, true);

                    if (!$vwContratoMatriculaProjetoTO->getId_contrato()) {
                        throw new Zend_Exception("Erro ao Recuperar o Contrato da Matrícula");
                    }


                    $matriculaTO = new MatriculaTO();
                    $matriculaTO->setId_matricula($vwProdutoTaxa->getId_matricula());
                    $matriculaTO->setId_evolucao(MatriculaTO::EVOLUCAO_MATRICULA_CURSANDO);
                    $matriculaTO->setId_situacao(MatriculaTO::SITUACAO_MATRICULA_ATIVA);

                    $matriculaBO = new MatriculaBO();
                    $mensageiro = $matriculaBO->editarMatricula($matriculaTO);

                    if ($mensageiro->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getMensagem());
                    }

                    $contratoTO = new ContratoTO();
                    $contratoTO->setId_contrato($vwContratoMatriculaProjetoTO->getId_contrato());
                    $contratoTO->fetch(false, true, true);

                    $contratoBO = new ContratoBO();

                    return $contratoBO->extenderContrato($contratoTO);
                } catch (Zend_Exception $e) {
                    return new Ead1_Mensageiro("Erro ao extender um contrato: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
                }

                break;

            default:
                return true;
                break;
        }
    }

    /**
     * Função responsável por alterar a evolução de uma ocorrência
     * Ligada a uma venda
     *
     * @param $idOcorrencia
     * @throws
     */
    public function alteraEvolucaoOcorrencia($idOcorrencia)
    {
        $this->negocio->beginTransaction();

        try {

            /**
             * Alterando Evolução da Ocorrência para pagamento realizado
             */
            $idEvolucao = $this->em->getReference(
                'G2\Entity\Evolucao',
                \G2\Constante\Evolucao::PAGAMENTO_REALIZADO);
            $ocEntity = $this->em->find('\G2\Entity\Ocorrencia', $idOcorrencia);
            $ocEntity->setId_evolucao($idEvolucao);

            $ocorrencia = $this->negocio->save($ocEntity);

            /**
             * Criando um Novo Tramite
             */
            $tramiteEntity = new \G2\Entity\Tramite();

            $idTipoTramite = $this->em->getReference(
                'G2\Entity\TipoTramite',
                \G2\Constante\TipoTramite::EVOLUCAO_VENDA);
            $idUsuario = $this->em->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
            $idEntidade = $this->em->getReference('G2\Entity\Entidade', $this->sessao->id_entidade);

            $tramiteEntity->setId_tipotramite($idTipoTramite);
            $tramiteEntity->setId_usuario($idUsuario);
            $tramiteEntity->setId_entidade($idEntidade);
            $tramiteEntity->setSt_tramite('Alterando Evolução da Venda: ' . $idEvolucao->getSt_evolucao());
            $tramiteEntity->setDt_cadastro(new DateTime());
            $tramiteEntity->setBl_visivel(true);

            /**
             * Salva os tramites.
             */
            $cAtencao = new \G2\Negocio\CAtencao();
            $cAtencao->salvaTramite($ocorrencia, $tramiteEntity);

            $this->negocio->commit();

        } catch (Zend_Exception $e) {
            $this->negocio->rollback();
//            throw new Zend_Exception("Erro ao fazer registro no tramite.");
            return new Ead1_Mensageiro("Erro ao fazer registro no tramite." . $e->getMessage(), Ead1_IMensageiro::ERRO,
                $e);
        }
    }

}

?>

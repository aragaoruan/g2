<?php
/**
 * Classe com regras de negocio para interaÃƒÆ’Ã‚Â§ÃƒÆ’Ã‚Âµes de importação
 * @author Eduardo RomÃƒÆ’Ã‚Â£o - ejushiro@gmail.com
 * @since 15/08/2011
 *
 * @package models
 * @subpackage bo
 */
class ImportacaoBO extends Ead1_BO {

	private $dao;
	public $mensageiro;
	protected $importacaoTO;

	public function __construct(){
		$this->dao 			= new ImportacaoDAO();
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->importacaoTO = new ImportacaoTO();
	}

	/**
	 * Metodo que faz o factory para importar os dados
	 * @param ImportacaoTO $iTO
	 * @return Ead1_Mensageiro
	 */
	public function importarDados( ImportacaoTO $iTO ){

		$tempFile			= $this->saveTempFile( $iTO );
		$this->importacaoTO = $iTO;

		if( strtolower($iTO->getSt_extensao()) == 'csv' ){
// 			$readerStrategy	= new Ead1_Reader_CSVStrategy2( $tempFile );
			$reader				= new Ead1_Reader_CSVToArray($tempFile);
			$readerMensageiro	= $reader->getData();
			if($readerMensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
				$readerStrategy	= $readerMensageiro->getMensagem();			
			} else {
				$this->mensageiro->setMensageiro($readerMensageiro->getFirstMensagem(),Ead1_IMensageiro::AVISO);
				return $this->mensageiro;
			}			
		}else{
			$readerStrategy	= new Ead1_Reader_XLSStrategy( $tempFile );
		}
		set_time_limit(0);
		switch ($iTO->getSt_tabela()){
			case ImportacaoTO::TB_PESSOAIMPORTACAO:
				$this->importarPessoas( $readerStrategy );
				break;
			case ImportacaoTO::TB_MATRICULAIMPORTACAO:
				$this->importarMatriculas( $readerStrategy );
				break;
			case ImportacaoTO::TB_APROVEITAMENTOIMPORTACAO:
				$this->importarAproveitamentos( $readerStrategy );
				break;
			case ImportacaoTO::TB_AVALICAOIMPORTACAO:
				$this->importarAvaliacoes( $readerStrategy );
				break;
			case ImportacaoTO::TB_DOCUMENTOENTREGUEIMPORTACAO:
				$this->importarDocumentosEntregues( $readerStrategy );
				break;
			case ImportacaoTO::TB_LANCAMENTOIMPORTACAO:
				$this->importarLancamentos( $readerStrategy );
				break;
			case ImportacaoTO::TB_RESPONSAVELIMPORTACAO:
				$this->importarResponsaveis( $readerStrategy );
				break;
			case ImportacaoTO::TB_HISTORICOSITUACAOIMPORTACAO:
				$this->importarHistoricoMatricula( $readerStrategy );
				break;
			case ImportacaoTO::TB_TIPOLANCAMENTOIMPORTACAO:
				$this->importarTipoLancamento( $readerStrategy );
				break;
			case ImportacaoTO::TB_PROJETODISCIPLINAIMPORTACAO:
				$this->importarProjetoDisciplina( $readerStrategy );
				break;
			default:
				$this->mensageiro->setMensageiro('Não foi Encontrada a Tabela de importação!',Ead1_IMensageiro::AVISO);
				break;
		}

		@unlink( $tempFile );
		return $this->mensageiro;
	}

	/**
	 * Metodo que cria e le um xls temporario
	 * @param ImportacaoTO $iTO
	 */
	protected function saveTempFile( ImportacaoTO $importacaoTO ){
		$path 	= realpath( Ead1_Ambiente::geral()->uploadFolder );
		$file 	= $this->geradorHash(6,false,true);
		$file 	.= '.'.$importacaoTO->getSt_extensao();
		$filepath = $path . DIRECTORY_SEPARATOR . $file;
		//Abre o arquivo para leitura
		$handle = fopen( $filepath, 'w+' );
		//Escreve binario do flex no arquivo xls
		fwrite($handle, $importacaoTO->getBi_data() );
		//Fecha arquivo
		fclose($handle);
		chmod( $filepath, 0777 );
		return $filepath;
	}

	/**
	 * Metodo que salva um array de PessoaImportacaoTO
	 * @param $arrPessoaImportacaoTO
	 * @return Ead1_Mensageiro
	 */
	private function cadastrarPessoaImportacao( PessoaImportacaoTO $pessoaImportacaoTO ){
		if(!$pessoaImportacaoTO->getId_entidadeimportado()){
			$pessoaImportacaoTO->setId_entidadeimportado($pessoaImportacaoTO->getSessao()->id_entidade);
		}
		return $this->dao->cadastrarPessoaImportacao( $pessoaImportacaoTO );
	}

	/**
	 * Metodo que le o XLS de importação de pessoas e cadastra no tabela tb_pessoaimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarPessoas( $dados ){

		$arrImportados = array();
		$this->dao->beginTransaction();
		try {
				
			if( empty( $dados ) ){
				throw new Zend_Validate_Exception( 'O arquivo está aparentemente vazio' );
			}
			
			foreach ( $dados as $pessoaImportacao ){

				$pessoaImportacao['nu_cpf']					= str_pad($pessoaImportacao['nu_cpf'], 11, '0', STR_PAD_LEFT);
				$pessoaImportacao['id_sistemaimportacao']	= $this->importacaoTO->getId_sistemaimportacao();
				$pessoaImportacao['id_entidadeimportado']	= $this->importacaoTO->getSessao()->id_entidade;
				$pessoaImportacao['nu_telefone']			= str_replace ( ' ', '', $pessoaImportacao['nu_telefone'] );
				if( isset( $pessoaImportacao['st_rg'] ) ) {
					$pessoaImportacao['st_rg']					= preg_replace('/(\W)/', '', $pessoaImportacao['st_rg'] );
				}
				if( isset( $pessoaImportacao['st_numeroeleitor'] )){
					$pessoaImportacao['st_numeroeleitor']		= preg_replace('/(\W)/', '', $pessoaImportacao['st_numeroeleitor'] );
				}
				$pessoaImportacao							= array_map('utf8_encode', $pessoaImportacao);
				
				$imTO = new PessoaImportacaoTO( $pessoaImportacao );
				$arrImportados[] = $this->cadastrarPessoaImportacao( $imTO );

			}
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de usuários realizado com sucesso', Ead1_IMensageiro::SUCESSO,$arrImportados);
		}catch (Zend_Validate_Exception $validateException){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $validateException->getMessage(), Ead1_IMensageiro::AVISO );
		}catch ( Exception $e ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação da pessoa '.$pessoaImportacao['st_nomecompleto'].': '. $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		return $this->mensageiro;
	}

	/**
	 * Cadastra o histórico de situação de matrícula em sistemas externos a partir de um arquivo XLS
	 * Gera os trâmites de acordo com os históricos informados
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarHistoricoMatricula( $dados ) {

		try{
			$this->dao->beginTransaction();
				
			foreach ( $dados as $historicoSituacaoImportacao ){
				$historicoSituacaoImportacao['id_sistemaimportacao']	=  $this->importacaoTO->getId_sistemaimportacao() ;
				$historicoSituacaoImportacao['id_entidadeimportado']	=  $this->importacaoTO->getSessao()->id_entidade;
				$this->dao->cadastrarHistoricoSituacaoImportacao( new HistoricoSituacaoImportacaoTO($historicoSituacaoImportacao) );
				unset( $historicoSituacaoImportacao );
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'importação de histórico de situação de matrícula realizada com sucesso.', Ead1_IMensageiro::SUCESSO );
		} catch( Exception $exc ) {
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação de histórico de situação: '. $exc->getMessage(), Ead1_IMensageiro::ERRO, $exc->getMessage() );
			$this->dao->rollBack();
		}

		return $this->mensageiro;
	}

	/**
	 * Cadastra os tipos de lançamentos existentes em sistemas externos a partir de um arquivo XLS
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarTipoLancamento( $dados ) {

		try{
			$this->dao->beginTransaction();
				
			foreach ( $dados as $tipoLancamentoImportacao ){
				$tipoLancamentoImportacao['st_tipolancamentoorigem'] = utf8_encode( $tipoLancamentoImportacao['st_tipolancamentoorigem'] );
				$this->dao->cadastrarTipoLancamento( new TipoLancamentoImportacaoTO( $tipoLancamentoImportacao ) );
				unset( $tipoLancamentoImportacao );
			}
				
			$this->mensageiro->setMensageiro( 'importação de tipo de lançamento realizada com sucesso.', Ead1_IMensageiro::SUCESSO );
			$this->dao->commit();
		} catch( Exception $exc ) {
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação de tipo de lançamento. Erro: '. $exc->getMessage() , Ead1_IMensageiro::ERRO, $exc );
			$this->dao->rollBack();
		}

		return $this->mensageiro;
	}

	/**
	 * Cadastra um novo projeto com sua disciplina vinculada a partir de um arquivo XLS
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarProjetoDisciplina( $dados ) {

		try{
			$this->dao->beginTransaction();
				
			foreach ( $dados as $projetoDisciplinaImportacao ){
				$projetoDisciplinaImportacao['id_sistemaimportacao']	= $this->importacaoTO->getId_sistemaimportacao();
				$projetoDisciplinaImportacao['st_projetoorigem'] 		= utf8_encode( $projetoDisciplinaImportacao['st_projetoorigem'] );
				$projetoDisciplinaImportacao['st_disciplinaorigem'] 	= utf8_encode( $projetoDisciplinaImportacao['st_disciplinaorigem'] );
				
				if(!empty($projetoDisciplinaImportacao['st_areaoriginal']))
					$projetoDisciplinaImportacao['st_areaoriginal'] 	= utf8_encode( $projetoDisciplinaImportacao['st_areaoriginal'] );
				
				
				$this->dao->cadastrarProjetoDisciplina( new ProjetoDisciplinaImportacaoTO( $projetoDisciplinaImportacao )   );
			}

			$this->mensageiro->setMensageiro( 'importação de projetos e disciplinas realizada com sucesso.', Ead1_IMensageiro::SUCESSO );
			$this->dao->commit();
		} catch( Exception $exc ) {
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação de projetos e disciplinas .', Ead1_IMensageiro::ERRO, $exc->getMessage() );
			$this->dao->rollBack();
		}
		return $this->mensageiro;
	}

	/**
	 * importação de aproveitamentos e cadastra no tabela tb_aproveitamentoimportacao
	 * @param array $dados - lista de registros
	 * @return Ead1_Mensageiro
	 */
	public function importarAproveitamentos( $dados ){

		try{
			$this->dao->beginTransaction();
				
			if( empty( $dados ) ){
				throw new Zend_Validate_Exception('O arquivo está aparentemente vazio!');
			}
			foreach ( $dados as $aproveitamentoImportacao ){
				$aproveitamentoImportacao['id_sistemaimportacao'] 				= $this->importacaoTO->getId_sistemaimportacao();
				$aproveitamentoImportacao['st_instituicaoaproveitamento'] 		= utf8_encode( $aproveitamentoImportacao['st_instituicaoaproveitamento'] );
				$aproveitamentoImportacao['st_cidadeinstituicaoaproveitamento'] = utf8_encode( $aproveitamentoImportacao['st_cidadeinstituicaoaproveitamento'] );
				$aproveitamentoImportacao['nu_aproveitamento']			 		= (float) str_replace( ',', '.', $aproveitamentoImportacao['nu_aproveitamento'] ) ;
				$this->dao->cadastrarAproveitamentoImportacao( new AproveitamentoImportacaoTO( $aproveitamentoImportacao ) );
				unset( $aproveitamentoImportacao );
			}
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de aproveitamentos realizado com sucesso', Ead1_IMensageiro::SUCESSO );
		}catch ( Zend_Validate_Exception $validateExc ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $validateExc->getMessage(), Ead1_IMensageiro::AVISO );
		}catch ( Exception $e ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro no Processo de importação de Aproveitamentos: '. $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage() );
		}
		return $this->mensageiro;
	}

	/**
	 * importação de avaliaçÃµes e cadastra no tabela tb_avaliacaoimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarAvaliacoes( $dados ){
		try{
			$this->dao->beginTransaction();
				
			if( empty( $dados ) ){
				$this->mensageiro->setMensagem("O arquivo não possui nenhum dado.!");
				throw new Zend_Validate_Exception('O arquivo está aparentemente vazio;');
			}
				
			foreach ( $dados as $linha ){
				$linha['id_sistemaimportacao'] 	= $this->importacaoTO->getId_sistemaimportacao();
				$linha['nu_nota']			 	= (float)str_replace( ',', '.', $linha['nu_nota']) ;
				$linha['nu_codmatriculaorigem'] = (int)$linha['nu_codmatriculaorigem'];
				$this->dao->cadastrarAvaliacaoImportacao( new AvaliacaoImportacaoTO( $linha )  );
				unset($linha);
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de avaliações efetuado com sucesso!',Ead1_IMensageiro::SUCESSO );
		}catch ( Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação de avaliações.'.$e->getMessage(), Ead1_IMensageiro::AVISO, $e->getMessage() );
		}
		return $this->mensageiro;
	}

	/**
	 * importação de documentos e cadastra no tabela tb_documentoentregueimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarDocumentosEntregues( $dados ){
		try{
			$this->dao->beginTransaction();
				
			if(empty($dados)){
				$this->mensageiro->setMensagem("O XLS Não possui nenhum dado!");
				throw new Zend_Validate_Exception('O XLS Esta aparentemente vazio!');
			}
				
			foreach ( $dados as $documentoEntregueImportacao ){
				$documentoEntregueImportacao['id_sistemaimportacao'] = $this->importacaoTO->getId_sistemaimportacao();
				$this->dao->cadastrarDocumentoEntregueImportacao( new DocumentoEntregueImportacaoTO($documentoEntregueImportacao) );
				unset( $documentoEntregueImportacao );
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de documentos efetuado com sucesso!',Ead1_IMensageiro::SUCESSO );
		}catch (Zend_Validate_Exception $validateException){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro durante o processamentos dos dados de documentações entregues. '. $e->getMessage(),Ead1_IMensageiro::ERRO, $e->getMessage());
		}

		return $this->mensageiro;
	}

	/**
	 * importação de lançamentos e cadastra no tabela tb_lancamentoimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarLancamentos( $dados ){
		try{
			$this->dao->beginTransaction();
				
			if(empty($dados)){
				throw new Zend_Validate_Exception('O XLS Esta aparentemente vazio!');
			}
				
			foreach ( $dados as $lancamentoImportacao ){
				$lancamentoImportacao['id_sistemaimportacao'] 	= $this->importacaoTO->getId_sistemaimportacao();
				$lancamentoImportacao['nu_valor']	 			= (float) str_replace( ',', '.', $lancamentoImportacao['nu_valor'] );
				$lancamentoImportacao['nu_desconto']	 		= (float) str_replace( ',', '.', $lancamentoImportacao['nu_desconto'] );
				$lancamentoImportacao['nu_juros']	 			= (float) str_replace( ',', '.', $lancamentoImportacao['nu_juros'] );
				$lancamentoImportacao['nu_multa']	 			= (float) str_replace( ',', '.', $lancamentoImportacao['nu_multa'] );
				$lancamentoImportacao['nu_correcaomonetaria']	= (float) str_replace( ',', '.', $lancamentoImportacao['nu_correcaomonetaria'] );
				$this->dao->cadastrarLancamentoImportacao( new LancamentoImportacaoTO( $lancamentoImportacao ) );
				unset($lancamentoImportacao);
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de lançamentos efetuado com sucesso!',Ead1_IMensageiro::SUCESSO );
		}catch (Zend_Validate_Exception $validateException){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $e->getMessage(), Ead1_IMensageiro::AVISO );
		}catch ( Exception $e ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro durante o processo de importação de lançamentos. Erro: '. $e->getMessage() ,Ead1_IMensageiro::AVISO, $e->getMessage());
		}
		
		return $this->mensageiro;
	}

	/**
	 * Metodo que le o XLS de importação de Matriculas e cadastra no tabela tb_matriculaimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarMatriculas( $dados ){
		
		
		$this->dao->beginTransaction();
		try{
				
			if(empty($dados)){
				$this->mensageiro->setMensagem("O XLS não possui nenhum dado!");
				throw new Zend_Validate_Exception('O XLS está aparentemente vazio!');
			}
				
			
			foreach ( $dados as $matriculaImportacao ){
				
				$matriculaImportacao['id_sistemaimportacao']	= $this->importacaoTO->getId_sistemaimportacao();
				$matriculaImportacao['id_entidade'] 			= $this->importacaoTO->getSessao()->id_entidade;
				$matriculaImportacao['nu_codmatriculaorigem'] 	= (int)$matriculaImportacao['nu_codmatriculaorigem'];
				
				$to =  new MatriculaImportacaoTO($matriculaImportacao);
				$this->dao->cadastrarMatriculaImportacao($to);
				unset( $matriculaImportacao );
			}
				
			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo de importação de matrículas efetuado com sucesso!',Ead1_IMensageiro::SUCESSO );
		}catch (Zend_Validate_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $e->getMessage(),Ead1_IMensageiro::AVISO );
		}catch ( Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro durante o processo de importação de matrículas. '. $e->getMessage(),Ead1_IMensageiro::AVISO, $e->getMessage());
		}catch ( Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro durante o processo de importação de matrículas. '. $e->getMessage(),Ead1_IMensageiro::AVISO, $e->getMessage());
		}
		
		return $this->mensageiro;
	}

	/**
	 * Metodo que le o XLS de importação de Responsaveis e cadastra no tabela tb_responsavelimportacao
	 * @param array $dados
	 * @return Ead1_Mensageiro
	 */
	public function importarResponsaveis( $dados ){
		try{
			$this->dao->beginTransaction();
				
			if( empty( $dados ) ){
				$this->mensageiro->setMensagem("O XLS não possui nenhum dado!");
				throw new Zend_Validate_Exception('O XLS está aparentemente vazio!');
			}
				
			foreach ( $dados as $reponsavelImportacao ){
				$reponsavelImportacao['st_rg']					= preg_replace('/(\W)/', '', $reponsavelImportacao['st_rg'] );
				$reponsavelImportacao['st_nomecompleto']		= utf8_encode( utf8_decode( $reponsavelImportacao['st_nomecompleto'] ) ) ;
				$reponsavelImportacao['st_endereco']			= utf8_encode( utf8_decode( $reponsavelImportacao['st_endereco'] ) ) ;
				$reponsavelImportacao['st_complementoendereco']	= utf8_encode( utf8_decode( $reponsavelImportacao['st_complementoendereco'] ) ) ;
				$reponsavelImportacao['st_municipioendereco']	= utf8_encode( utf8_decode( $reponsavelImportacao['st_municipioendereco'] ) ) ;
				$reponsavelImportacao['id_entidadeimportado']	= $this->importacaoTO->getSessao()->id_entidade;
				$this->dao->cadastrarResponsavelImportacao( new ResponsavelImportacaoTO( $reponsavelImportacao ) );
				unset($reponsavelImportacao);
			}
				
			$this->mensageiro->setMensageiro( 'Processo de importação de responsáveis efetuado com sucesso.',Ead1_IMensageiro::SUCESSO );
			$this->dao->commit();
		}catch (Zend_Validate_Exception $validateException){
			var_dump( $validateException );
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $e->getMessage(),Ead1_IMensageiro::AVISO );
		}catch (Exception $e){
			
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro durante o processamento dos dados de responsáveis: ' . $e->getMessage() ,Ead1_IMensageiro::ERRO, $e->getMessage() );
		}
		
		return $this->mensageiro;
	}

	/**
	 * Retorna os estados civis de importação
	 * @param EstadocivilImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEstadoCivilImportacao(EstadocivilImportacaoTO $to){
		try{
			$dados = $this->dao->retornarEstadoCivilImportacao($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EstadocivilImportacaoTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Estado Civil da importação',Ead1_IMensageiro::ERRO,$e->getMessage());
			throw $e;
		}
		return $this->mensageiro;
	}

	/**
	 * Retorna as etnias de importação
	 * @param EtniaImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEtniaImportacao(EtniaImportacaoTO $to){
		try{
			$dados = $this->dao->retornarEtniaImportacao($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EstadocivilImportacaoTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Etnia da importação',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}

	/**
	 * Retorna os sistemas da importacao
	 * @param SistemaImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarSistemaImportacao(SistemaImportacaoTO $to){
		try{
			$dados = $this->dao->retornarSistemaImportacao($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new SistemaImportacaoTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Db_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar os Sistemas da importação',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}



	/**
	 * Realiza o processo completo de importação de dados externos de usuário, matrícula, avaliações e aproveitamentos
	 * O processo de importação de usuário é executa obrigatoriamente
	 * A partir dele são executados os outros processos de importação opcionais
	 * Para execução do processo de importação de avaliação e aproveitamento é obrigatório
	 * a execução do processo de importação de matrícula.
	 *
	 * @param boolean $blImportarMatricula - Informa se deseja realizar o processo de importação de matrícula
	 * @param boolean $blImportarAvalicao - Informa se deseja realizar o processo de importação de matrícula
	 * @param boolean $blImportarAproveitamento - Informa se deseja realizar o processo de importação de matrícula
	 * @return Ead1_Mensageiro
	 */
	public function processarImportacao( $blImportarMatricula = false, $blImportarAvaliacao = false, $blImportarAproveitamento = false  ) {

		set_time_limit(0);

		try {
			$this->dao->beginTransaction();
				
			$arUsuarios 			= $this->dao->processarDadosUsuarioMatriculaImportacao((bool)$blImportarMatricula);
			$totalUsuariosGerados	= count( $arUsuarios );
				
			if( $blImportarAproveitamento == true ){
				$this->dao->execSPImportarAproveitamentoExterno();
			}
			if( $blImportarAvaliacao == true ){
				$this->dao->execSPImportarAvaliacaoExterna();
				$avaliacaoBO	= new AvaliacaoBO();
				if( $totalUsuariosGerados > 0 ){
					foreach( $arUsuarios as $usuario ){
						if( empty($usuario['id_matricula']) ){
							throw new Zend_Validate_Exception( 'Não foi encontrada matrícula para o usuário: '. $usuario['id_usuario'] );
						}
						$avaliacaoBO->processaNotaDisciplina( new AvaliacaoAlunoTO( array( 'id_matricula' => $usuario['id_matricula']) ) );
					}
				}
			}

			$this->dao->commit();
			$this->mensageiro->setMensageiro( 'Processo concluído com sucesso. Total de usuários importados: '. $totalUsuariosGerados, Ead1_IMensageiro::SUCESSO ) ;
		} catch( Zend_Validate_Exception $e ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage() ) ;
		} catch( Exception $e ){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro( 'Erro durante a importação. ', Ead1_IMensageiro::ERRO, $e->getMessage() ) ;
		}
		return $this->mensageiro;
	}
}
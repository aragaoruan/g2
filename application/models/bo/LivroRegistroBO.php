<?php
/**
 * Classe com regras de negócio para interações de Livro de registro
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class LivroRegistroBO extends Ead1_BO {

	public $mensageiro;
	
	/**
	 * @var LivroRegistroDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new LivroRegistroDAO();
	}
	
	/**
	 * Metodo cadastra a Livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		try{
			if(!$lreTO->getId_entidade()){
				$lreTO->setId_entidade($lreTO->getSessao()->id_entidade);
			}
			if(!$lreTO->getId_usuariocadastro()){
				$lreTO->setId_usuariocadastro($lreTO->getSessao()->id_usuario);
			}
			$id_livroregistro = $this->dao->cadastrarLivroRegistro($lreTO);
			if(!$id_livroregistro){
				throw new Zend_Exception('Erro ao fazer insert');
			}
			$lreTO->setId_livroregistroentidade($id_livroregistro);
			return $this->mensageiro->setMensageiro($lreTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Livro de Registro!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo edita livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		try{
			if(!$this->dao->editarLivroRegistro($lreTO)){
				throw new Zend_Exception('Erro ao fazer update');
			}
			return $this->mensageiro->setMensageiro('Livro editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Livro de Registro!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo exclui livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		try{
			if(!$this->dao->excluirLivroRegistro($lreTO)){
				throw new Zend_Exception('Erro ao fazer delete');
			}
			return $this->mensageiro->setMensageiro("Livro Deletado com Sucesso!", Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Livro de Registro!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar a livro registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroRegistroEntidade(LivroRegistroEntidadeTO $lreTO){
		try{
			if(!$lreTO->getId_entidade()) {
				$lreTO->id_entidade	= $lreTO->getSessao()->id_entidade;
			}
			
			$dados = $this->dao->retornarLivroRegistroEntidade($lreTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Livro de Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new LivroRegistroEntidadeTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Livro de Registro!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna o livro registro
	 * @param LivroRegistroTO $lrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroRegistro(LivroRegistroTO $lrTO){
		try{
			$dados = $this->dao->retornarLivroRegistro($lrTO)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new LivroRegistroTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar o Livro de Registro!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
}


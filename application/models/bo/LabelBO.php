<?php
/**
 * Classe com regras de negócio para interações de Label
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 28/03/2011
 * 
 * @package models
 * @subpackage bo
 */
class LabelBO extends Ead1_BO{
	
	private $dao;
	
	public $mensageiro;
	
	public function __construct(){
		$this->dao = new LabelDAO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Metodo que retorna as funcionalidades da Entidade com o Label
	 * @param VwLabelFuncionalidadeTO $vwLfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaLabelFuncionalidade(VwLabelFuncionalidadeTO $vwLfTO){
		try{
			if(!$vwLfTO->getId_entidade()){
				$vwLfTO->setId_entidade($vwLfTO->getSessao()->id_entidade);
			}
			$labelFuncionalidade = $this->dao->retornaLabelFuncionalidade($vwLfTO)->toArray();
			if(empty($labelFuncionalidade)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			$arrTOs = Ead1_TO_Dinamico::encapsularTo($labelFuncionalidade, new VwLabelFuncionalidadeTO());
			return $this->mensageiro->setMensageiro($arrTOs,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar o Label das Funcionalidades!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	
}
<?php

/**
 * Classe com regras de negocio para interacoes de Pessoa
 * @author Eduardo Romao - ejushiro@gmail.com
 * @since 27/08/2010
 *
 * @package models
 * @subpackage bo
 */
class PessoaBO extends Ead1_BO
{

    public $mensageiro;
    private $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new PessoaDAO();
    }

    /**
     * Metodo que verifica e retorna dados de usuario.
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuario(UsuarioTO $uTO)
    {
        try {
            $uTO->setBl_ativo(true);
            $uTO = $this->retornaUsuario($uTO);
            if ($uTO === false) {
                $this->mensageiro->setMensageiro('Nenhum Usuário Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $pessoaTO = $this->verificaPessoa($uTO, $uTO->getSessao()->id_entidade);
                if ($pessoaTO === false) {
                    $mensageiro = $this->verificaUsuarioEntidadeRecursivo($uTO, $uTO->getSessao()->id_entidade);
                    switch ($mensageiro->getTipo()) {
                        case Ead1_IMensageiro::AVISO:
                            $this->mensageiro->setMensageiro($uTO, Ead1_IMensageiro::SUCESSO);
                            break;
                        case Ead1_IMensageiro::ERRO:
                            THROW new Zend_Exception($mensageiro->getCodigo());
                            break;
                        case Ead1_IMensageiro::SUCESSO:
                            $this->mensageiro->setMensageiro($mensageiro->getMensagem(), Ead1_IMensageiro::SUCESSO);
                            break;
                    }
                } else {
                    $this->mensageiro->setMensageiro($pessoaTO, Ead1_IMensageiro::SUCESSO);
                }
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Verificar Pessoa!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Retorna Usuario
     * @param UsuarioTO $uTO
     * @return TO
     */
    public function retornaUsuario(UsuarioTO $to)
    {
        $dao = new PessoaDAO();

        $to->setBl_redefinicaosenha(null);
        $to->setSt_cpf($to->getSt_cpf() ? $this->somenteNumero($to->getSt_cpf()) : null);
        $usuario = $dao->retornaUsuario($to);
        if (!$usuario) {
            return false;
        } else {
            return $usuario;
        }
    }

    /**
     * Metodo que verifica se existe Pessoa
     * Caso não seja informado o $idEntidade é utilizado o da sessão atual
     * @param $uTO
     * @param $idEntidade - OPCIONAL
     * @return PessoaTO|false
     */
    public function verificaPessoa(UsuarioTO $uTO, $idEntidade = null)
    {
        $pessoa = new PessoaTO();

        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }

        $dados = $this->retornaPessoa($uTO, $idEntidade);
        if ($dados) {
            return $pessoa->setAtributos($dados);
        } else {
            return false;
        }
    }

    /**
     * Metodo que retorna Pessoa
     * CAso não seja informado o $idEntidade será utilizado o da sessão atual
     * @param $to
     * @param $idEntidade - OPCIONAL
     * @return mixed array|false
     */
    public function retornaPessoa(UsuarioTO $to, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        $pessoa = $dao->retornaPessoa($to, $idEntidade);
        if (!$pessoa) {
            return false;
        } else {
            return $pessoa->toArray();
        }
    }

    /**
     * Retorna os vínculos do usuário  na entidade atual e nas entidades filhas a ela
     * Utiliza o to como filtro para a pesquisa
     * @param $usuarioTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuarioEntidadeRecursivo(UsuarioTO $usuarioTO, $idEntidade)
    {

        try {

            $entidadeDAO = new EntidadeDAO();
            $pessoaDAO = new PessoaDAO();
            $arEntidade = $entidadeDAO->retornaEntidadesFilhas($idEntidade);
            $arIdEntidade = array($idEntidade => $idEntidade);
            $compartilhaDadosBO = new CompartilhaDadosBO();
            $compartilhaDadosTO = new CompartilhaDadosTO();
            $compartilhaDadosTO->setId_tipodados(TipoDadosTO::PESSOAS);
            $compartilhaDadosTO->setId_entidadedestino($idEntidade);
            $mensageiro = $compartilhaDadosBO
                ->retornarCompartilhaDados($compartilhaDadosTO)
                ->subtractMensageiro(Ead1_IMensageiro::ERRO);

            $arCompartilhadas = $mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO ? $mensageiro->getMensagem() : array();
            foreach ($arEntidade as $entidade) {
                $arIdEntidade[$entidade['id_entidade']] = $entidade['id_entidade'];
            }

            foreach ($arCompartilhadas as $compartilhada) {
                $arIdEntidade[$compartilhada->getId_entidadeorigem()] = $compartilhada->getId_entidadeorigem();
            }

            $negocioHolding = new \G2\Negocio\Holding();
            $arrHolding = $negocioHolding->retornaUsuarioHoldingImportacao();
            foreach ($arrHolding as $entidade) {
                $arIdEntidade[$entidade['id_entidade']] = $entidade['id_entidade'];
            }


            $usuarioROWSET = $pessoaDAO->retornarUsuarioListaEntidade($usuarioTO, $arIdEntidade);


            if ($usuarioROWSET->count() < 1) {
                throw new ErrorException('Não Foi Encontrado Nenhum Usuário nas Instituições Filhas.');
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($usuarioROWSET, new UsuarioEntidadeTO()),
                Ead1_IMensageiro::SUCESSO);
        } catch (ErrorException $errorExc) {
            $this->mensageiro->setMensageiro($errorExc->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro('Erro a Listar os Usuários das Entidades Filhas ' . $exc->getMessage(),
                Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que verifica e retorna dados de usuario.
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuario2(UsuarioTO $uTO)
    {

        $dao = new PessoaDAO();
        $usuario = $this->retornaUsuario($uTO);
        if ($usuario) {

            $idEntidadeSessao = $uTO->getSessao()->id_entidade;

            //caso já existe o usuário na entidade da sessão só retorno os dados para utilização
            if ($this->verificaPessoa($usuario, $idEntidadeSessao) !== false) {
                unset($uTO->bl_ativo);
                $rowset = $dao->retornarUsuarioListaEntidade($uTO, array($idEntidadeSessao));

                if ($rowset->count() < 1) {
                    return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
                }

                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($rowset, new UsuarioEntidadeTO()),
                    Ead1_IMensageiro::SUCESSO);
            } else { //caso contrário busco o usuário nas entidade filhas da entidade da sessão
                $mensageiro = $this->verificaUsuarioEntidadeRecursivo($uTO, $idEntidadeSessao);
                $this->mensageiro = $mensageiro;
            }
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
        }

        return $this->mensageiro;
    }

    public function retornarDadosUsuarioEntidade(UsuarioEntidadeTO $usuarioEntidadeTO)
    {
        $usuarioEntidadeTO->setBl_redefinicaosenha(null);
//        var_dump($usuarioEntidadeTO);
        try {
            $usuarioEntidadeTO->id_entidade = ($usuarioEntidadeTO->id_entidade ? $usuarioEntidadeTO->id_entidade : $usuarioEntidadeTO->getSessao()->id_entidade);
            $idEntidadeSessao = $usuarioEntidadeTO->getSessao()->id_entidade;
            $idUsuarioSessao = $usuarioEntidadeTO->getSessao()->id_usuario;

            if ($usuarioEntidadeTO->id_entidade == $idEntidadeSessao) {

                $usuarioTO = new UsuarioTO();
                $usuarioTO->setId_usuario($usuarioEntidadeTO->getId_usuario());
                $mensageiro = $this->retornarDadosUsuario($usuarioTO, $usuarioEntidadeTO->id_entidade);
                $this->mensageiro = $mensageiro;
            } else {
                $resposta = $this->importarUsuarioEntidade($usuarioEntidadeTO->id_usuario,
                    $usuarioEntidadeTO->id_entidade, $idEntidadeSessao, $idUsuarioSessao);
                $mensageiro = $this->retornarDadosUsuario(new UsuarioTO($resposta), $resposta['id_entidade']);
                $this->mensageiro = $mensageiro;
            }
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados do usuário', Ead1_IMensageiro::SUCESSO,
                $exc->getMessage());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar os dados do usuário', Ead1_IMensageiro::SUCESSO,
                $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Retorna os dados básicos de um usuário
     * @param UsuarioTO $usuarioTO
     * @return Ead1_Mensageiro
     */
    protected function retornarDadosUsuario(UsuarioTO $usuarioTO, $idEntidade)
    {
        unset($usuarioTO->st_senha);
        $dadosUsuario = $this->retornaUsuario($usuarioTO);
        $pessoa = $this->verificaPessoa($usuarioTO, $idEntidade);
        if ($dadosUsuario) {
            $usuarioTO = $dadosUsuario;
        }
        $this->mensageiro->setMensageiro($usuarioTO);
        if ($pessoa) {


            $this->mensageiro->addMensagem($pessoa->tipaTOFlex());
            $dadosIdentidade = $this->retornaIdentidade($usuarioTO, $idEntidade);

            if ($dadosIdentidade) {
                $this->mensageiro->addMensagem($dadosIdentidade);
            } else {
                $this->mensageiro->addMensagem("Sem Dados Identidade!");
            }

            $dadosCertidao = $this->retornaCertidaoNascimento($usuarioTO, $idEntidade);

            if ($dadosCertidao) {
                $this->mensageiro->addMensagem($dadosCertidao);
            } else {
                $this->mensageiro->addMensagem("Sem Dados Certidão de Nascimento!");
            }

            $dadosReservista = $this->retornaReservista($usuarioTO, $idEntidade);

            if ($dadosReservista) {
                $this->mensageiro->addMensagem($dadosReservista);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Reservista!");
            }

            $dadosTitulo = $this->retornaTituloEleitor($usuarioTO, $idEntidade);
            if ($dadosTitulo) {
                $this->mensageiro->addMensagem($dadosTitulo);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Titulo de Eleitor!");
            }

            $dadosBiomedicos = $this->retornaDadosBiomedicos($usuarioTO, $idEntidade);
            if ($dadosBiomedicos) {
                $this->mensageiro->addMensagem($dadosBiomedicos);
            } else {
                $this->mensageiro->addMensagem("Sem Dados Biomédicos!");
            }

            $dadosEndereco = $this->retornaEnderecos($usuarioTO, false, $idEntidade);
            if ($dadosEndereco) {
                $this->mensageiro->addMensagem($dadosEndereco);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Endereço!");
            }

            $dadosEmail = $this->retornaEmails($usuarioTO, $idEntidade);
            if ($dadosEmail) {
                $this->mensageiro->addMensagem($dadosEmail);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Email!");
            }

            $dadosRedesSociais = $this->retornaRedesSociais($usuarioTO, $idEntidade);
            if ($dadosRedesSociais) {
                $this->mensageiro->addMensagem($dadosRedesSociais);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Rede Social!");
            }

            $dadosFone = $this->retornaTelefones($usuarioTO, $idEntidade);
            if ($dadosFone) {
                $this->mensageiro->addMensagem($dadosFone);
            } else {
                $this->mensageiro->addMensagem("Sem Dados de Telefone!");
            }

            $dadosEnderecoView = $this->retornaEnderecos($usuarioTO, true, $idEntidade);

            if ($dadosEnderecoView) {
                $this->mensageiro->addMensagem($dadosEnderecoView);
            } else {
                $this->mensageiro->addMensagem("Sem Dados na View de Endereço!");
            }

            //Busca os Dados Acadêmicos
            $dadosAcademico = $this->retornaInformacaoAcademica($usuarioTO, $idEntidade);
            if ($dadosAcademico) {
                $this->mensageiro->addMensagem($dadosAcademico);
            } else {
                $this->mensageiro->addMensagem("Sem Dados Acadêmicos!");
            }
            return $this->mensageiro;
        } else {
            return $this->mensageiro->setMensageiro($usuarioTO);
        }
    }

    /**
     * Metodo que retorna Documento de Identidade
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return DocumentoIdentidadeTO
     */
    public function retornaIdentidade(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();

        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $identidades = $dao->retornaIdentidade($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($identidades, new DocumentoIdentidadeTO(), true);
    }

    /**
     * Metodo que retorna o documento de certidao de nascimento
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return DocumentoCertidaoDeNascimentoTO
     */
    public function retornaCertidaoNascimento(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $certidaoNascimentos = $dao->retornaCertidaoNascimento($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($certidaoNascimentos, new DocumentoCertidaoDeNascimentoTO(), true);
    }

    /**
     * Metodo que retorna o Reservista
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return DocumentoDeReservistaTO
     */
    public function retornaReservista(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $reservistas = $dao->retornaReservista($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($reservistas, new DocumentoDeReservistaTO(), true);
    }

    /**
     * Metodo que retorna o documento de titulo eleitor
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @param DocumentoTituloDeEleitorTO
     */
    public function retornaTituloEleitor(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }

        $titulosEleitor = $dao->retornaTituloEleitor($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($titulosEleitor, new DocumentoTituloDeEleitorTO(), true);
    }

    /**
     * Metodo que retorna informações de dados biomédicos
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return DadosBiomedicosTO
     */
    public function retornaDadosBiomedicos(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();

        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }

        $dadosBiomedicos = $dao->retornaDadosBiomedicos($uTO, $idEntidade);

        return Ead1_TO_Dinamico::encapsularTo($dadosBiomedicos, new DadosBiomedicosTO(), true);
    }

    /**
     * Método que retorna Endereços da Pessoa
     * Caso o parâmetro view seja passado como true é retornado uma coleção de VwPessoaEnderecoTO
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param boolean $view
     * @param int $idEntidade - OPCIONAL
     * @return array Lista de mixed EnderecoTO|VwPessoaEnderecoTO
     */

    public function retornaEnderecos(UsuarioTO $uTO, $view = false, $idEntidade = null)
    {
        $dao = new PessoaDAO();

        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $enderecos = $dao->retornaEnderecos($uTO, $idEntidade);

        if ($view) {
            return Ead1_TO_Dinamico::encapsularTo($enderecos, new VwPessoaEnderecoTO(), false, false, false, false);
        } else {
            return Ead1_TO_Dinamico::encapsularTo($enderecos, new EnderecoTO(), false, false, false, false);
        }
    }

    /**
     * Metodo que retorna Emails da Pessoa
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return array - Lista de VwPessoaEmailTO
     */
    public function retornaEmails(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $emails = $dao->retornaEmails($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($emails, new VwPessoaEmailTO());
    }

    /**
     * Metodo que retorna Redes Sociais da Pessoa
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return array - Lista de VwPessoaRedeSocialTO
     */
    public function retornaRedesSociais(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $redesSociais = $dao->retornaRedesSociais($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($redesSociais, new VwPessoaRedeSocialTO());
    }

    /**
     * Metodo que retorna Telefones da Pessoa
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade
     * @return array - Lista de VwPessoaTelefoneTO
     */
    public function retornaTelefones(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $telefones = $dao->retornaTelefones($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($telefones, new VwPessoaTelefoneTO());
    }

    /**
     * Metodo que retorna Informações Academicas da Pessoa
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade
     * @return array - Lista de VwPessoaInformacaoAcademicaTO
     */
    public function retornaInformacaoAcademica(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $infoAcademicas = $dao->retornaInformacaoAcademica($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($infoAcademicas, new VwPessoaInformacaoAcademicaTO());
    }

    /**
     * Importa os dados básicos do usuário vinculado a uma determinada entidade
     * para outra entidade
     * Retorna um array com os dados de usuário
     * @param $idUsuario
     * @param $idEntidadeOrigem
     * @param $idEntidadeDestino
     * @param $idUsuarioCadastro
     * return mixed array|false
     */
    public function importarUsuarioEntidade($idUsuario, $idEntidadeOrigem, $idEntidadeDestino, $idUsuarioCadastro)
    {
        $pessoaDAO = new PessoaDAO();
        $resposta = $pessoaDAO->importarUsuarioEntidade($idUsuario, $idEntidadeOrigem, $idEntidadeDestino,
            $idUsuarioCadastro);
        return $resposta;
    }

    /**
     * Metodo que atualiza e define a conta bancaria padrao da pessoa
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function setaContaBancariaPadrao(ContaPessoaTO $cpTO)
    {
        try {
            $dao = new PessoaDAO();
            $dao->beginTransaction();
            if (!$cpTO->getId_entidade()) {
                $cpTO->setId_entidade($cpTO->getSessao()->id_entidade);
            }
            if (!$cpTO->getId_usuario()) {
                THROW new Exception('Usuário não identificado!');
            }
            $newCpTO = new ContaPessoaTO();
            $newCpTO->setBl_padrao(false);
            $newCpTO->setId_usuario($cpTO->getId_usuario());
            $newCpTO->setId_entidade($cpTO->getId_entidade());
            $dao->editarContaPessoa($newCpTO);
            $cpTO->setBl_padrao(true);
            $dao->editarContaPessoa($cpTO);
            $dao->commit();
            return $this->mensageiro->setMensageiro('Conta Bancaria Definida como Padrão com Sucesso!',
                Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Conta Padrão!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os endereços da pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwPessoaEnderecoTO(UsuarioTO $uTO)
    {
        try {
            $dao = new PessoaDAO();
            $dados = $dao->retornaEnderecos($uTO, $uTO->getSessao()->id_entidade);
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaEnderecoTO()),
                Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Endereço!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Método que retorna a view de emails da pessoa
     * @param VwPessoaEmailTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwPessoaEmail(VwPessoaEmailTO $to)
    {
        try {
            $dados = $this->dao->retornarVwPessoaEmail($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Email Encontrado");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPessoaEmailTO()),
                Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Emails da Pessoa.", Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna Informação Profissional da Pessoa
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuariaTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @return array - Lista de VwPessoaInformacaoProfissionalTO
     */
    public function retornaInformacaoProfissional(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $infoProfissionais = $dao->retornaInformacaoProfissional($uTO, $idEntidade);
        return Ead1_TO_Dinamico::encapsularTo($infoProfissionais, new VwPessoaInformacaoProfissionalTO());
    }

    /**
     * Metodo que retorna Contas Bancarias do usuário da entidade atual da sessão
     * Caso não seja fornecido  idEntidade é buscado os valores da entidade da sessão atual
     * @param UsuarioTO $uTO
     * @param int $idEntidade
     * @return array - Lista de VwPessoaContaTO
     */
    public function retornaContaBancaria(UsuarioTO $uTO, $idEntidade = null)
    {
        try {
            $dao = new PessoaDAO();
            if (!$idEntidade) {
                $idEntidade = $uTO->getSessao()->id_entidade;
            }
            $contaBancarias = $dao->retornaContaBancaria($uTO, $idEntidade)->toArray();
            if (empty($contaBancarias)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($contaBancarias,
                new VwPessoaContaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Conta!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Retorna atalho de pessoa pra funcionalidade.
     * @param AtalhoPessoaTO $aTO
     */
    public function retornarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        try {
            if (!$aTO->getId_entidade()) {
                $aTO->setId_entidade($aTO->getSessao()->id_entidade);
            }
            $atalhos = $this->dao->retornarAtalhoPessoa($aTO);
            if (empty($atalhos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Atalho Encontrado!',
                    Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($atalhos, new AtalhoPessoaTO()),
                Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Atalhos de funcionalidade.',
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Cadastra Informações Profissional da Pessoa
     * @param UsuarioTO $uTO
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarInformacaoProfissional(UsuarioTO $uTO, InformacaoProfissionalPessoaTO $ippTO)
    {
        $dao = new PessoaDAO();
        if ($ippTO->getDt_inicio()) {
            if (!$this->comparaData($ippTO->getDt_inicio(), new Zend_Date())) {
                return $this->mensageiro->setMensageiro('A Data de Início não pode ser Maior que a Data de Atual!',
                    Ead1_IMensageiro::ERRO);
            }
            if ($ippTO->getDt_fim()) {
                if (!$this->comparaData($ippTO->getDt_inicio(), $ippTO->getDt_fim())) {
                    return $this->mensageiro->setMensageiro('A Data de Início não pode ser Maior que a Data de Término!',
                        Ead1_IMensageiro::ERRO);
                }
            }
        }
        if (!$pk = $dao->cadastrarInformacaoProfissional($uTO, $ippTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Informação Profissional!',
                Ead1_IMensageiro::ERRO, $dao->excecao);
        } else {
            $ippTO->setId_informacaoprofissionalpessoa($pk);
            return $this->mensageiro->setMensageiro($ippTO, Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Metodo que Cadastra Rede Social
     * @param UsuarioTO $uTO
     * @param ContatosRedeSocialTO $crsTO
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarRedeSocial(UsuarioTO $uTO, ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $dao = new PessoaDAO();
        if (!$dao->cadastrarRedeSocial($uTO, $crsTO, $crspTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Rede Social!', Ead1_IMensageiro::ERRO);
        } else {
            $this->mensageiro->addMensagem($crsTO);
            $this->mensageiro->addMensagem($crspTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Cadastra Conta Bancaria
     * @param UsuarioTO $uTO
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContaPessoa(UsuarioTO $uTO, ContaPessoaTO $cpTO)
    {
        unset($cpTO->bl_padrao);
        $dao = new PessoaDAO();
        $orm = new ContaPessoaORM();
        $newCpTO = new ContaPessoaTO();
        $newCpTO->setId_usuario($cpTO->getid_usuario());
        $newCpTO->setId_entidade($cpTO->getid_entidade());
        $newCpTO->setst_banco($cpTO->getSt_banco());
        $newCpTO->setId_tipodeconta($cpTO->getid_tipodeconta());
        $consulta = $orm->consulta($newCpTO);
        if ($consulta) {
            return $this->mensageiro->setMensageiro('A Pessoa não Pode Ter o Mesmo Tipo de Conta no Mesmo Banco!',
                Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        if (!$pk = $dao->cadastrarContaPessoa($uTO, $cpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Conta Bancaria!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            $cpTO->setId_contapessoa($pk);
            return $this->mensageiro->setMensageiro($cpTO, Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Cadastra atalho de pessoa pra funcionalidade
     * @param $aTO
     */
    public function cadastrarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        try {
            if (!$aTO->getId_entidade()) {
                $aTO->setId_entidade($aTO->getSessao()->id_entidade);
            }
            $id_atalhoentidade = $this->dao->cadastrarAtalhoPessoa($aTO);
            $aTO->setId_atalhopessoa($id_atalhoentidade);
            return $this->mensageiro->setMensageiro($aTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao cadatrar atalho de funcionalidade.',
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Salva todos atalhos de pessoa pra entidade
     * @param array $arrayAtalhoPessoaTO
     */
    public function salvarArrayAtalhoPessoa($arrayAtalhoPessoaTO)
    {
        try {
            $aTO = new AtalhoPessoaTO();
            $aTO->setId_entidade($arrayAtalhoPessoaTO[0]->getId_entidade() ? $arrayAtalhoPessoaTO[0]->getId_entidade() : $aTO->getSessao()->id_entidade);
            $aTO->setId_usuario($arrayAtalhoPessoaTO[0]->getId_usuario());
            $this->dao->deletarAtalhoPessoa($aTO);
            foreach ($arrayAtalhoPessoaTO as $atalhoPessoaTO) {
                $atalhoPessoaTO->setId_entidade($atalhoPessoaTO->getSessao()->id_entidade);
                if (!$this->dao->cadastrarAtalhoPessoa($atalhoPessoaTO)) {
                    throw new Zend_Exception('Erro no insert.');
                }
            }
            return $this->mensageiro->setMensageiro('Atalhos salvos com sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao salvar atalho.', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Cadastra Dados Biomedicos
     * @param UsuarioTO $uTO
     * @param DadosBiomedicosTO $dbTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDadosBiomedicos(UsuarioTO $uTO, DadosBiomedicosTO $dbTO)
    {
        try {
            if (!$dbTO->getId_entidade()) {
                $dbTO->setId_entidade($dbTO->getSessao()->id_entidade);
            }
            $id_dadosbiomedicos = $this->dao->cadastrarDadosBiomedicos($uTO, $dbTO);
            $dbTO->setId_dadosbiomedicos($id_dadosbiomedicos);
            $this->mensageiro->setMensageiro($dbTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Dados Biomédicos!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Titulo de Eleitor
     * @param UsuarioTO $uTO
     * @param DocumentoTituloDeEleitorTO $dtiTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoTituloEleitor(UsuarioTO $uTO, DocumentoTituloDeEleitorTO $dtiTO)
    {
        try {
            if (!$dtiTO->getId_entidade()) {
                $dtiTO->setId_entidade($uTO->getSessao()->id_entidade);
            }
            $dtiTO->setId_usuario($uTO->getId_usuario());
            $dtiTO->setSt_numero($this->somenteNumero($dtiTO->getSt_numero()));
//			if(!$this->validaTituloEleitor($dtiTO->getSt_numero())){
//				return $this->mensageiro->addMensagem('Titulo de Eleitor InvÃ¡lido!',Ead1_IMensageiro::AVISO);
//			}
//			if(!$this->validaData($dtiTO->getDt_dataexpedicao())){
//				return $this->mensageiro->setMensageiro('Data de ExpediÃ§Ã£o InvÃ¡lida!',Ead1_IMensageiro::AVISO);
//			}
//			$dtiTO->setDt_dataexpedicao($this->converteDataBanco($dtiTO->getDt_dataexpedicao()));
            $this->dao->cadastrarDocumentoTituloEleitor($dtiTO);
            return $this->mensageiro->setMensageiro($dtiTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Titulo de Eleitor!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Certidão de Nascimento
     * @param UsuarioTO $uTO
     * @param DocumentoCertidaoDeNascimentoTO $dcnTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoCertidaoNascimento(UsuarioTO $uTO, DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        try {
            if (!$dcnTO->getId_entidade()) {
                $dcnTO->setId_entidade($dcnTO->getSessao()->id_entidade);
            }

            if ($dcnTO->getDt_dataexpedicao()) {
                if ($this->comparaData(new Zend_Date(), $dcnTO->getDt_dataexpedicao())) {
                    THROW new Zend_Validate_Exception('A data de expedição da certidão não pode ser uma data futura!');
                }
            }

            $this->dao->cadastrarDocumentoCertidaoNascimento($uTO, $dcnTO);
            $this->mensageiro->setMensageiro($dcnTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Certidão de Nascimento!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Reservista
     * @param UsuarioTO $uTO
     * @param DocumentoDeReservistaTO $drTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoReservista(UsuarioTO $uTO, DocumentoDeReservistaTO $drTO)
    {
        try {
            if (!$drTO->getId_entidade()) {
                $drTO->setId_entidade($drTO->getSessao()->id_entidade);
            }

            if ($drTO->getDt_datareservista()) {
                if ($this->comparaData(new Zend_Date(), $drTO->getDt_datareservista())) {
                    THROW new Zend_Validate_Exception('A data de expedição da carteira de reservista não pode ser uma data futura!');
                }
            }

            $this->dao->cadastrarDocumentoReservista($uTO, $drTO);
            $this->mensageiro->setMensageiro($drTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Reservista!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Altera a Senha
     * @param UsuarioTO $uTO
     * @param array $novosUsuarioSenha
     * @throws Zend_Exception
     * @throws Zend_Validate_Exception
     * @throws Zend_Db_Exception
     * @return Ead1_Mensageiro
     */
    public function alterarUsuarioSenha(UsuarioTO $uTO, $novosUsuarioSenha = false, $id_entidade)
    {


        $this->dao->beginTransaction();
        try {

            if ($uTO->getId_usuario() == null) {
                throw new Zend_Exception('Usuário inválido ou vazio!');
            }

            if ($novosUsuarioSenha === false) {
                throw new Zend_Exception('Nova senha não informada!');
            }

            if (($novosUsuarioSenha->st_senha_nova != $novosUsuarioSenha->st_senha_nova_confirma) || (!$novosUsuarioSenha->st_senha_nova)) {
                throw new Zend_Exception('As novas senhas são diferentes!');
            }

            $validacao = new Zend_Validate_Regex(array('pattern' => '(^[a-zA-Z0-9\.]*$)'));
            if (!$validacao->isValid($novosUsuarioSenha->st_senha_nova)) {
                throw new Zend_Validate_Exception("Nova senha inválida!");
            }

            $dTO = new DadosAcessoTO();
            $dTO->setId_entidade($id_entidade);
            $dTO->setId_usuario($uTO->getId_usuario());
            $dTO->setSt_login($uTO->getSt_login());
            $dTO->setSt_senha($novosUsuarioSenha->st_senha_nova);
            $dTO->setBl_ativo(1);
            $me = $this->cadastrarDadosAcesso($dTO);
            if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception('Erro ao Salvar os dados de acesso!');
            }


            $uTO->setSt_senha($novosUsuarioSenha->st_senha_nova);

            $this->dao->editarUsuario($uTO);
            $this->mensageiro->setMensageiro('Senha alterada com sucesso!', Ead1_IMensageiro::SUCESSO);
            $this->dao->commit();
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Db_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao alterar a senha!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Edita Informações Profissional da Pessoa
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function editarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $dao = new PessoaDAO();
        if ($ippTO->getDt_inicio() && $ippTO->getDt_fim()) {
            if (!$this->comparaData($ippTO->getDt_inicio(), $ippTO->getDt_fim())) {
                return $this->mensageiro->setMensageiro('A Data de Início não pode ser Maior que a Data de Término!',
                    Ead1_IMensageiro::ERRO);
            }
        }
        if (!$dao->editarInformacaoProfissional($ippTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Informação Profissional!', Ead1_IMensageiro::ERRO);
        } else {
            return $this->mensageiro->setMensageiro('Informação Profissional Editada com Sucesso!',
                Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * etodo que Edita Rede Social
     * @param ContatosRedeSocialTO $crsTO
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function editarRedeSocial(ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $dao = new PessoaDAO();
        if (!$dao->editarRedeSocial($crsTO, $crspTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Rede Social!', Ead1_IMensageiro::ERRO);
        } else {
            return $this->mensageiro->setMensageiro('Rede Social Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Metodo que Edita Conta Bancaria
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function editarContaPessoa(ContaPessoaTO $cpTO)
    {
        unset($cpTO->bl_padrao);
        $dao = new PessoaDAO();
        $orm = new ContaPessoaORM();
        $newCpTO = new ContaPessoaTO();
        $newCpTO->setId_usuario($cpTO->getid_usuario());
        $newCpTO->setId_entidade($cpTO->getid_entidade());
        $newCpTO->setId_banco($cpTO->getid_banco());
        $newCpTO->setId_tipodeconta($cpTO->getid_tipodeconta());
        $consulta = $orm->consulta($newCpTO);
        if ($consulta[0]->getId_contapessoa() != $cpTO->getId_contapessoa()) {
            return $this->mensageiro->setMensageiro('A Pessoa não Pode Ter o Mesmo Tipo de Conta no Mesmo Banco!',
                Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        if (!$dao->editarContaPessoa($cpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Conta Bancaria!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            return $this->mensageiro->setMensageiro('Conta Bancaria Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Edita atalho de pessoa pra funcionalidade.
     * @param AtalhoPessoaTO $aTO
     */
    public function editarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        try {
            if ($this->dao->editarAtalhoPessoa($aTO)) {
                return $this->mensageiro->setMensageiro('Atalho salvo com sucesso.', Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao editar atalho', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Edita Dados Biomedicos
     * @param DadosBiomedicosTO $dbTO
     * @return Ead1_Mensageiro
     */
    public function editarDadosBiomedicos(DadosBiomedicosTO $dbTO)
    {
        try {
            if (!$dbTO->getId_entidade()) {
                $dbTO->setId_entidade($dbTO->getSessao()->id_entidade);
            }
            $this->dao->editarDadosBiomedicos($dbTO);
            $this->mensageiro->setMensageiro('Dados Biomédicos Editados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Dados Biomédicos!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Edita Documento Titulo de Eleitor
     * @param DocumentoTituloDeEleitorTO $dtiTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoTituloEleitor(DocumentoTituloDeEleitorTO $dtiTO)
    {
        try {
            if (!$dtiTO->getId_entidade()) {
                $dtiTO->setId_entidade($dtiTO->getSessao()->id_entidade);
            }
            $this->dao->editarDocumentoTituloEleitor($dtiTO);
            $this->mensageiro->setMensageiro('Titulo de Eleitor Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Titulo de Eleitor!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Edita Documento Certidão de Nascimento
     * @param DocumentoCertidaoDeNascimentoTO $dcnTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoCertidaoNascimento(DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        try {
            if (!$dcnTO->getId_entidade()) {
                $dcnTO->setId_entidade($dcnTO->getSessao()->id_entidade);
            }

            if ($dcnTO->getDt_dataexpedicao()) {
                if ($this->comparaData(new Zend_Date(), $dcnTO->getDt_dataexpedicao())) {
                    THROW new Zend_Validate_Exception('A data de expedição da certidão não pode ser uma data futura!');
                }
            }

            $this->dao->editarDocumentoCertidaoNascimento($dcnTO);
            $this->mensageiro->setMensageiro('Certidão de Nascimento Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Certidão de Nascimento!', Ead1_IMensageiro::ERRO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Edita Documento Reservista
     * @param DocumentoDeReservistaTO $drTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoReservista(DocumentoDeReservistaTO $drTO)
    {
        try {
            if ($drTO->getId_entidade()) {
                $drTO->setId_entidade($drTO->getSessao()->id_entidade);
            }

            if ($drTO->getDt_datareservista()) {
                if ($this->comparaData(new Zend_Date(), $drTO->getDt_datareservista())) {
                    THROW new Zend_Validate_Exception('A data de expedição da carteira de reservista não pode ser uma data futura!');
                }
            }

            $this->dao->editarDocumentoReservista($drTO);
            $this->mensageiro->setMensageiro('Reservista Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Reservista!', Ead1_IMensageiro::ERRO, $dao->excecao);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que deleta o telefone de uma pesssoa
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function deletarTelefone(ContatosTelefonePessoaTO $ctpTO)
    {
        $dao = new PessoaDAO();
        try {
            $dao->beginTransaction();
            $result = $dao->deletarContatosTelefonePessoa($ctpTO);
            if ($result) {
                $dao->deletarContatosTelefone($ctpTO);
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Telefone excluído com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao deletar telefone!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Metodo que deleta a rede social de uma pesssoa
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function deletarRedeSocial(ContatosRedeSocialPessoaTO $crspTO)
    {
        $dao = new PessoaDAO();
        try {
            $dao->beginTransaction();
            $dao->deletarContatosRedeSocialPessoa($crspTO);
            $dao->deletarContatosRedeSocial($crspTO);
            $dao->commit();
            return $this->mensageiro->setMensageiro('Rede social excluída com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao deletar rede social!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Metodo que exclui Informações Academicas da Pessoa
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function deletarInformacaoAcademica(InformacaoAcademicaPessoaTO $iapTO)
    {
        $dao = new PessoaDAO();
        if (!$dao->deletarInformacaoAcademica($iapTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Informação Academica!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            return $this->mensageiro->setMensageiro('Informação Academica Excluida com Sucesso!',
                Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Metodo que exclui Informações Profissionais da Pessoa
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function deletarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $dao = new PessoaDAO();
        if (!$dao->deletarInformacaoProfissional($ippTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Informação Profissional!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            return $this->mensageiro->setMensageiro('Informação Profissional Excluida com Sucesso!',
                Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Metodo que exclui Conta da Pessoa
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function deletarContaPessoa(ContaPessoaTO $cpTO)
    {
        $dao = new PessoaDAO();
        if (!$cpTO->getId_entidade()) {
            $cpTO->setId_entidade($cpTO->getSessao()->id_entidade);
        }
        if (!$dao->deletarContaPessoa($cpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Conta Bancaria!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            return $this->mensageiro->setMensageiro('Conta Bancaria Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Deleta atalho de pessoa pra funcionalidade
     * @param $aTO
     */
    public function deletarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        try {
            if ($this->dao->deletarAtalhoPessoa($aTO)) {
                return $this->mensageiro->setMensageiro('Atalho deletado com sucesso.', Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao deletar Atalho.', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /**
     * Procedimento de salvar dados de usuario e pessoa para o cadastro de pessoa resumido
     * @param UsuarioTO $usuarioTO
     * @param PessoaTO $pessoaTO
     * @param DocumentoIdentidadeTO $documentoIdentidadeTO
     * @param ContatosTelefoneTO $contatosTelefoneTO
     * @param ContatosTelefonePessoaTO $contatosTelefonePessoaTO
     * @param ContatosEmailTO $contatosEmailTO
     * @param ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @param EnderecoTO $enderecoTO
     * @param InformacaoAcademicaPessoaTO $informacaoAcademicaPessoaTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoSalvarPessoaCadastroResumido(
        UsuarioTO $usuarioTO,
        PessoaTO $pessoaTO,
        DocumentoIdentidadeTO $documentoIdentidadeTO,
        ContatosTelefoneTO $contatosTelefoneTO,
        ContatosTelefonePessoaTO $contatosTelefonePessoaTO,
        ContatosEmailTO $contatosEmailTO,
        ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO,
        PessoaEnderecoTO $pessoaEnderecoTO = null,
        EnderecoTO $enderecoTO = null,
        InformacaoAcademicaPessoaTO $informacaoAcademicaPessoaTO = null,
        PessoaEnderecoTO $pendCorresp,
        EnderecoTO $endCorresp = null
    )
    {
        $this->dao->beginTransaction();
        try {
            $arrRetorno = array();
            if (!$pessoaTO->getId_entidade()) {
                $pessoaTO->setId_entidade($pessoaTO->getSessao()->id_entidade);
            }
            //veirificando atualização no moodle
            if (!$usuarioTO->getId_usuario()) {
                $bl_atualizamoodle = false;
            } else {
                $vwPessoaAux = new VwPessoaTO();
                $vwPessoaAux->setId_usuario($usuarioTO->getId_usuario());
                $vwPessoaAux->fetch(false, true, true);

                if (($vwPessoaAux->getSt_email() != $contatosEmailTO->getSt_email())
                    || ($vwPessoaAux->getSt_nomecompleto() != $usuarioTO->getSt_nomecompleto())) {
                    $bl_atualizamoodle = true;
                } else {
                    $bl_atualizamoodle = false;
                }
            }

            //Salvando Usuario
            $validaCPF = ($usuarioTO->getSt_cpf() ? true : false);
            $this->_procedimentoSalvarUsuario($usuarioTO, $validaCPF, $pessoaTO->getSessao()->id_entidade);
            $documentoIdentidadeTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $contatosEmailPessoaPerfilTO->setId_usuario($usuarioTO->getId_usuario());
            //Setando ID entidade
            $documentoIdentidadeTO->setId_entidade($pessoaTO->getId_entidade());
            $contatosTelefonePessoaTO->setId_entidade($pessoaTO->getId_entidade());
            $contatosEmailPessoaPerfilTO->setId_entidade($pessoaTO->getId_entidade());
            //Setando Telefone e Email como Padrao
            $contatosTelefonePessoaTO->setBl_padrao(true);
            $contatosEmailPessoaPerfilTO->setBl_padrao(true);
            //Salvando Pessoa
            $this->_procedimentoSalvarPessoa($usuarioTO, $pessoaTO);
            //Salvando Documento Identidade
            if ($documentoIdentidadeTO->getSt_rg()) {
                $this->_procedimentoSalvarDocumentoIdentidade($usuarioTO, $documentoIdentidadeTO);
            }
            //Salvando Telefone
            $this->_procedimentoSalvarContatosTelefonePessoa($usuarioTO, $contatosTelefoneTO,
                $contatosTelefonePessoaTO);
            //Salvando Email
            if ($contatosEmailTO->st_email != null && $contatosEmailTO->st_email != '') {
                $this->_procedimentoSalvarContatosEmailPessoaPerfil($usuarioTO, $contatosEmailTO,
                    $contatosEmailPessoaPerfilTO);
            }
            if (!is_null($pessoaEnderecoTO) && !is_null($enderecoTO)) {
                //Salvando Endereço da Pessoa
                $pessoaEnderecoTO->setId_usuario($usuarioTO->getId_usuario());
                $pessoaEnderecoTO->setId_entidade($pessoaTO->getId_entidade());
                $pessoaEnderecoTO->setBl_padrao(true);
                $this->_procedimentoSalvarPessoaEndereco($usuarioTO, $pessoaEnderecoTO, $enderecoTO);

                if ($pendCorresp->getId_endereco() && $endCorresp->getId_endereco()) {
                    $this->_procedimentoSalvarPessoaEndereco($usuarioTO, $pendCorresp, $endCorresp);
                }
            }

            //Salva dados academicos
            if (!is_null($pessoaEnderecoTO) && $informacaoAcademicaPessoaTO->getId_nivelensino()) {
                $this->cadastrarInformacaoAcademica($usuarioTO, $informacaoAcademicaPessoaTO);
            }

            if ($bl_atualizamoodle && $usuarioTO->getId_usuario()) {
                $this->atualizarDadosAlunoMoodle($usuarioTO, $usuarioTO->getSessao()->id_entidade,
                    'procedimentoSalvarPessoaCadastroResumido');
            }

            $this->dao->commit();
            return $this->mensageiro->setMensageiro(array(
                $usuarioTO,
                $pessoaTO,
                $documentoIdentidadeTO,
                $contatosTelefoneTO,
                $contatosTelefonePessoaTO,
                $contatosEmailTO,
                $contatosEmailPessoaPerfilTO,
                $pessoaEnderecoTO,
                $enderecoTO,
                $informacaoAcademicaPessoaTO
            ), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Procedimento que salva o usuario
     * @param UsuarioTO $usuarioTO
     * @param Boolean $validaCPF
     * @throws Zend_Exception
     */
    public function _procedimentoSalvarUsuario(UsuarioTO $usuarioTO, $validaCPF, $id_entidade)
    {
        if (!$usuarioTO->getId_usuario()) {
            $this->cadastrarUsuario($usuarioTO, $id_entidade, $validaCPF);
        } else {
            $this->editarUsuario($usuarioTO, $validaCPF);
//            $this->atualizarDadosAlunoMoodle($usuarioTO);
        }
        return $usuarioTO;
    }

    /**
     * Metodo que Cadastra Usuario
     * @param UsuarioTO $uTO
     * @param Boolean $validaCPF
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuario(UsuarioTO $uTO, $id_entidade, $validaCPF = true)
    {

        $this->dao->beginTransaction();
        try {
            $loginBO = new LoginBO();
            if ($validaCPF && $uTO->getSt_cpf()) {
                if ($uTO->getId_registropessoa() == RegistroPessoaTO::BRASILEIRO_MAIOR_DE_IDADE) {
                    if (!$this->validaCPF($uTO->getSt_cpf())) {
                        THROW new Zend_Validate_Exception('CPF Inválido!');
                    }
                    $usChecknew = $this->verificaUsuarioCPF($uTO->getSt_cpf());
                    if ($usChecknew) {
                        $uTO->setId_usuario($usChecknew->getId_usuario());
                        $uTO->fetch(true, true, true);
                        $this->dao->commit();
                        return $this->mensageiro->setMensageiro($uTO, Ead1_IMensageiro::SUCESSO);
                        //  THROW new Zend_Validate_Exception('Não é possível adicionar o usuário. O CPF informado já é existente.');
                    }
                }
            }
//            $novaSenha = $loginBO->geradorDeSenha(); //antigo
            //AC-1202 -> Como acadêmico quero mudar o padrão de geração de nova senha
            //6 primeiro dígitos do nome (sem espaço) + 4 números
            $nome = $this->substituirCaracteresEspeciaisAtualizado(str_replace(' ', '', $uTO->getSt_nomecompleto()));
            $senhaInicio = substr($nome, 0, 6);
            $senhaFim = $loginBO->geradorHash(4, true, false);
            $novaSenha = $senhaInicio . $senhaFim;

            /**
             * salvamos aqui a senha pura sem criptografia
             */
            $dTO = new DadosAcessoTO();
            $dTO->setSt_senha($uTO->getSt_senha() ? $uTO->getSt_senha() : $novaSenha);


            $uTO->setSt_senha($uTO->getSt_senha() ? $uTO->getSt_senha() : $novaSenha);
            $login = $uTO->getSt_login();

            if (empty($login)) {
                $newUTO = $loginBO->geradorDeLogin($uTO);
                $uTO->setSt_login($newUTO->getSt_login());
            } else {
                if (!$this->validaLoginUsuario($uTO->getSt_login()) && $this->validaLogin($uTO->getSt_login())) {
                    THROW new Zend_Validate_Exception('Login Inválido!');
                }
            }

            if (!$this->verificaLogin($uTO)) {
                THROW new Zend_Validate_Exception('Login não Disponivel!');
            }

            if (!$this->validaNomeCompleto($uTO->getSt_nomecompleto())) {
                THROW new Zend_Validate_Exception('Nome Inválido!');
            }


            //Método para salvar usuário refatorado para utilização do log padrão.

            $negocio = new \G2\Negocio\Negocio();
            $objUsuario = new \G2\Entity\Usuario();

            $objUsuario->montarEntity($uTO->toArray());
            $objUsuario->setBl_ativo(true);
            $objUsuario->setBl_redefinicaosenha($uTO->getBl_redefinicaosenha() != null ? $uTO->getBl_redefinicaosenha() : false);

            $usuario = $negocio->save($objUsuario);
            $id_usuario = $usuario->getId_usuario();

            $dTO->setId_entidade($id_entidade ? $id_entidade : $dTO->getSessao()->id_entidade);
            $dTO->setId_usuario($id_usuario);
            $dTO->setSt_login($uTO->getSt_login());
            $dTO->setBl_ativo(1);
            $me = $this->cadastrarDadosAcesso($dTO);
            if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($me->getFirstMensagem());
            }

            //$caminhoFoto = Ead1_Ambiente::geral()->uploadUrl . '/usuario/usuario_foto_generica.jpg';
            //$uTO->caminhofoto = $caminhoFoto;
            $uTO->setId_usuario($id_usuario);
            $uTO->fetch(true, true, true);


            $this->dao->commit();
            return $this->mensageiro->setMensageiro($uTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
            return $this->mensageiro;
        } catch (Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Usuário!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
            return $this->mensageiro;
        }
    }

    /**
     * Verifica se o cpf já existe na base
     * @param $numCPF - número do cpf sem pontos ou traços
     * @return UsuarioTO
     */
    public function verificaUsuarioCPF($numCPF)
    {
        $pessoaDAO = new PessoaDAO();
        $usuarioTO = new UsuarioTO();
        $usuarioTO->setSt_cpf($numCPF);
        $usuarioTO->setBl_ativo(true);
        return $pessoaDAO->retornaUsuario($usuarioTO);
    }

    /**
     * Metood que Verifica se o Login Esta disponivel!
     * @param UsuarioTO $uTO
     * @return boolean
     */
    public function verificaLogin(UsuarioTO $uTO)
    {
        $dao = new PessoaDAO();
        $usuario = $dao->verificaLogin($uTO);
        if (is_object($usuario)) {
            $usuario = $usuario->toArray();
        } else {
            return false;
        }
        if (empty($usuario)) {
            return true;
        }
        $to = Ead1_TO_Dinamico::encapsularTo($usuario, new UsuarioTO(), true);
        if ($to->getId_usuario() != $uTO->getId_usuario()) {
            return false;
        }
        return true;
    }

    /**
     * Cadastra os Dados de Acesso
     * @param DadosAcessoTO $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarDadosAcesso(DadosAcessoTO $to)
    {
        $this->dao->beginTransaction();
        try {
            $to->setId_usuariocadastro($to->getId_usuario());
            $to->setId_dadosacesso($this->dao->cadastrarDadosAcesso($to));
            $to->fetch(true, true, true);

            $integracao = new IntegracaoBO();
            $integracao->salvarUsuarioSenha($to);

            $this->dao->commit();
            return new Ead1_Mensageiro("Dados salvos com sucesso!", Ead1_IMensageiro::SUCESSO, $to);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Edita Rede Social
     * @param $to
     * @return Ead1_Mensageiro
     */

    /**
     * Metodo que Edita Usuario
     * @param UsuarioTO $uTO
     * @param Boolean $validaCPF
     * @return Ead1_Mensageiro
     */
    public function editarUsuario(UsuarioTO $uTO, $validaCPF = true, $enviarSenha = false, $id_entidade = null)
    {
        try {
            $usuarioTO = false;
            if ($validaCPF) {
                if ($uTO->getId_registropessoa() == RegistroPessoaTO::BRASILEIRO_MAIOR_DE_IDADE) {
                    if (!$this->validaCPF($uTO->getSt_cpf())) {
                        THROW new Zend_Validate_Exception('CPF Inválido!');
                    }
                    $usuarioTO = $this->verificaUsuarioCPF($uTO->getSt_cpf());
                    if ($usuarioTO != false) {
                        if ($usuarioTO->getId_usuario() != $uTO->getId_usuario()) {
                            THROW new Zend_Validate_Exception('Não é possível atualizar os dados do usuário. CPF já utilizado em outro cadastro.');
                        }
                    }
                }
            }

            if (!$usuarioTO) {
                $usuarioTO = new UsuarioTO();
                $usuarioTO->setId_usuario($uTO->getId_usuario());
                $usuarioTO->fetch(false, true, true);
            }
            if (!$this->verificaLogin($uTO)) {
                THROW new Zend_Validate_Exception('Usuário não Disponivel!');
            }
            if (!$this->validaLogin($uTO->getSt_login())) {
                THROW new Zend_Validate_Exception('Login Inválido!');
            }
            if (!$this->validaNomeCompleto($uTO->getSt_nomecompleto())) {
                THROW new Zend_Validate_Exception('Nome Inválido!');
            }
            $senhaUsuario = trim($uTO->getSt_senha());

            if (empty($senhaUsuario)) {
                $uTO->setSt_senha(null);
            } else {
//                //AC-1201 -> gravando senha atualizada nos dados de acesso
                $dTO = new DadosAcessoTO();

                //Força o id_entidade caso não tenha sido passado pelo parametro
                if (!$id_entidade) {
                    $id_entidade = $dTO->getSessao()->id_entidade;
                }


                $dTO->setId_entidade($id_entidade);
                $dTO->setId_usuario($uTO->getId_usuario());
                $dTO->setSt_login($uTO->getSt_login());
                $dTO->setSt_senha($senhaUsuario);
                $dTO->setBl_ativo(1);

                $me = $this->cadastrarDadosAcesso($dTO);
                $dTO->fetch(false, true, true);
                $uTO->setSt_senha($dTO->getSt_senha());

                if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($me->getFirstMensagem());
                }


                if ($enviarSenha) {
                    $msgRO = new MensagemRO();
                    $resposta = $msgRO->enviarEmailDadosAcesso($uTO)->subtractMensageiro();
//                    Zend_Debug::dump($msgRO->enviarEmailDadosAcesso($uTO)->getFirstMensagem());die;
                }
            }

            //Método para salvar usuário refatorado para utilização do log padrão.

            $negocio = new \G2\Negocio\Negocio();
            $objUsuario = $negocio->find('\G2\Entity\Usuario', $uTO->getId_usuario());
            if ($objUsuario) {

                $objUsuario->montarEntity($uTO->toArray());

                if ($uTO->getBl_ativo() != null) {
                    $objUsuario->setBl_ativo($uTO->getBl_ativo());
                } else {
                    $objUsuario->setBl_ativo(true);
                }

                if ($uTO->getBl_redefinicaosenha() != null) {
                    $objUsuario->setBl_redefinicaosenha($uTO->getBl_redefinicaosenha());
                } else {
                    $objUsuario->setBl_redefinicaosenha(true);
                }

                $objUsuario->setId_usuariopai($uTO->getId_usuariopai() ? $negocio->getReference('\G2\Entity\Usuario',
                    $uTO->getId_usuariopai()) : null);
                $objUsuario->setId_titulacao($uTO->getId_titulacao() ? $negocio->getReference('\G2\Entity\Titulacao',
                    $uTO->getId_titulacao()) : null);
                $objUsuario->setId_registropessoa($uTO->getId_registropessoa() ? $negocio->getReference('\G2\Entity\RegistroPessoa',
                    $uTO->getId_registropessoa()) : null);
            }
            $negocio->save($objUsuario);

            if ($uTO->getSessao()->id_entidade && ($usuarioTO->getSt_nomecompleto() != $uTO->getSt_nomecompleto())) {
                $this->atualizarDadosAlunoMoodle($uTO,null,'editarUsuario');
            }
            $uTO->setId_usuario($uTO->getId_usuario());
            $this->mensageiro->setMensageiro('Usuário atualizado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Usuário!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Procedimento que salva a Pessoa
     * @param UsuarioTO $usuarioTO
     * @param PessoaTO $pessoaTO
     * @throws Zend_Exception
     */
    public function _procedimentoSalvarPessoa(UsuarioTO $usuarioTO, PessoaTO $pessoaTO)
    {

        $orm = new PessoaORM();
        $negocio = new G2\Negocio\Pessoa();
        $edicao = $negocio->findBy('\G2\Entity\Pessoa',
            array("id_entidade" => $pessoaTO->getId_entidade(), "id_usuario" => $pessoaTO->getId_usuario()));
        //Caso haja id_usuario, chama método de edição.
        if ($pessoaTO->getId_usuario() && $edicao) {
            $mensageiro = $this->editarPessoa($usuarioTO, $pessoaTO);
//            $this->atualizarDadosAlunoMoodle($usuarioTO);
        } else {
            //Caso não haja id_usuário, seta o novo id e chama o método de cadastro.

            //Setando ID Usuario
            $pessoaTO->setId_usuario($usuarioTO->getId_usuario());
            $mensageiro = $this->cadastrarPessoa($usuarioTO, $pessoaTO);
        }
        return $pessoaTO;
    }

    /**
     * Metodo que Edita Pessoa
     * @param UsuarioTO $uTO
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function editarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        try {

            if ($pTO->getDt_nascimento()) {
                if ($this->comparaData(new Zend_Date(), $pTO->getDt_nascimento())) {
                    THROW new Zend_Validate_Exception('A data de nascimento não pode ser uma data futura!');
                }
            }

            $negocio = new \G2\Negocio\Pessoa();
            $negocio->savePessoa($pTO);

            $this->atualizarDadosAlunoMoodle($uTO, null,'editarPessoa');
            $this->mensageiro->setMensageiro('Pessoa Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Pessoa', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Pessoa
     * @param UsuarioTO $uTO
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        try {
            if (!$pTO->getId_entidade()) {
                $pTO->setId_entidade($pTO->getSessao()->id_entidade);
            }

            $idEntidade = ($pTO->getId_entidade() ? $pTO->getId_entidade() : null);

            if ($this->verificaPessoa($uTO, $idEntidade)) {
                $this->mensageiro->setMensageiro('Este usuário já possui uma pessoa cadastrada nesta instituição!',
                    Ead1_IMensageiro::AVISO);
                THROW new Zend_Validate_Exception('Este usuário já possui uma pessoa cadastrada nesta instituição!');
            }

            if ($pTO->getDt_nascimento()) {
                if ($this->comparaData(new Zend_Date(), $pTO->getDt_nascimento())) {
                    THROW new Zend_Validate_Exception('A data de nascimento não pode ser uma data futura!');
                }
            }

            $pTO->setId_usuariocadastro($pTO->getId_usuariocadastro() ? $pTO->getId_usuariocadastro() : $pTO->getSessao()->id_usuario);
            $pTO->setId_usuario($uTO->getId_usuario());
            $pTO->setBl_ativo(true);
            $pTO->setId_situacao(1);

            $this->dao->cadastrarPessoa($uTO, $pTO);
            $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Pessoa', Ead1_IMensageiro::ERRO, $e);
            THROW $e;
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Pessoa.: ' . $e->getMessage(), Ead1_IMensageiro::ERRO,
                $e);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Procedimento que salva o Documento de Identidade da Pessoa
     * @param UsuarioTO $uTO
     * @param DocumentoIdentidadeTO $diTO
     * @throws Zend_Exception
     * @return DocumentoIdentidadeTO
     */
    public function _procedimentoSalvarDocumentoIdentidade(UsuarioTO $uTO, DocumentoIdentidadeTO $diTO)
    {
        $negocio = new \G2\Negocio\Pessoa();
        $aux = $negocio->findOneBy('\G2\Entity\DocumentoIdentidade', array(
            'id_usuario' => (int)$uTO->getId_usuario()
        ,
            'id_entidade' => (int)$diTO->getId_entidade()
        ));
        if ($aux instanceof \G2\Entity\DocumentoIdentidade && $aux->getSt_rg()) {
            $mensageiro = $this->editarDocumentoIdentidade($diTO);
        } else {
            $mensageiro = $this->cadastrarDocumentoIdentidade($diTO);
        }
        return $diTO;
    }

    /**
     * Metodo que Edita Documento de Identidade
     * @param DocumentoIdentidadeTO $diTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoIdentidade(DocumentoIdentidadeTO $diTO)
    {
        try {
            if (!$diTO->getId_entidade()) {
                $diTO->setId_entidade($diTO->getSessao()->id_entidade);
            }
            if ($diTO->getDt_dataexpedicao()) {
                if ($this->comparaData(new Zend_Date(), $diTO->getDt_dataexpedicao())) {
                    THROW new Zend_Validate_Exception('A data de expedição da identidade não pode ser uma data futura!');
                }
            }
            $this->dao->editarDocumentoIdentidade($diTO);
            $this->mensageiro->setMensageiro('Documento de Identidade Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Documento de Identidade!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento de Identidade
     * @param UsuarioTO $uTO
     * @param DocumentoIdentidadeTO $diTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoIdentidade(DocumentoIdentidadeTO $diTO)
    {
        try {
            if (!$diTO->getId_entidade()) {
                $diTO->setId_entidade($diTO->getSessao()->id_entidade);
            }

            if ($diTO->getDt_dataexpedicao()) {
                if ($this->comparaData(new Zend_Date(), $diTO->getDt_dataexpedicao())) {
                    THROW new Zend_Validate_Exception('A data de expedição da identidade não pode ser uma data futura!');
                }
            }

            $this->dao->cadastrarDocumentoIdentidade($diTO);
            $this->mensageiro->setMensageiro($diTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Documento de Identidade!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Procedimento que salva o Telefone da Pessoa
     * @throws Zend_Exception
     * @param UsuarioTO $uTO
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return array
     */
    public function _procedimentoSalvarContatosTelefonePessoa(
        UsuarioTO $uTO,
        ContatosTelefoneTO $ctTO,
        ContatosTelefonePessoaTO $ctpTO
    )
    {
        if ($ctpTO->getId_telefone()) {
            $mensageiro = $this->editarTelefone($ctTO, $ctpTO);
        } else {
            $mensageiro = $this->cadastrarTelefone($uTO, $ctTO, $ctpTO);
        }
        return array($ctTO, $ctpTO);
    }

    /**
     * Metodo que Edita Telefone
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefone(ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        try {
            if (!$ctpTO->getId_entidade()) {
                $ctpTO->setId_entidade($ctpTO->getSessao()->id_entidade);
            }

            $ctTO->setNu_telefone($this->somenteNumero($ctTO->getNu_telefone()));
            if (!$this->validaNumero($ctTO->getNu_telefone(), 15)) {
                THROW new Zend_Validate_Exception('Número de Telefone (' . $ctTO->getNu_telefone() . ') Inválido!');
            }

            $ctTO->setNu_ddd($this->somenteNumero($ctTO->getNu_ddd()));
            if (!$this->validaNumero($ctTO->getNu_ddd(), 3)) {
                THROW new Zend_Validate_Exception('Número de DDD Inválido!');
            }

            if ($ctpTO->getBl_padrao() === true) {
                $uTO = new UsuarioTO();
                $uTO->setId_usuario($ctpTO->getId_usuario());
                $this->editarTelefonePadrao($uTO);
            }
            $this->dao->editarTelefone($ctTO, $ctpTO);
            $this->mensageiro->setMensageiro('Telefone Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Telefone!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Telefone
     * @param UsuarioTO $uTO
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTelefone(UsuarioTO $uTO, ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        try {
            if (!$ctpTO->getId_entidade()) {
                $ctpTO->setId_entidade($ctpTO->getSessao()->id_entidade);
            }

            $ctTO->setNu_telefone($this->somenteNumero($ctTO->getNu_telefone()));
            if (!$this->validaNumero($ctTO->getNu_telefone(), 15)) {
                THROW new Zend_Validate_Exception('Número de Telefone (' . $ctTO->getNu_telefone() . ') Inválido!');
            }

            $ctTO->setNu_ddd($this->somenteNumero($ctTO->getNu_ddd()));
            if (!$this->validaNumero($ctTO->getNu_ddd(), 3)) {
                THROW new Zend_Validate_Exception('Número de DDD Inválido!');
            }

            if ($ctpTO->getBl_padrao() === true) {
                $uTO = new UsuarioTO();
                $uTO->setId_usuario($ctpTO->getId_usuario());
                $this->editarTelefonePadrao($uTO);
            }
            $this->dao->cadastrarTelefone($uTO, $ctTO, $ctpTO);
            $this->mensageiro->addMensagem($ctTO);
            $this->mensageiro->addMensagem($ctpTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Telefone!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Editar telefone padrão, passo para false os bl_padrao dos outros telefones
     * @param UsuarioTO $uTO
     * @return Boolean
     */
    private function editarTelefonePadrao(UsuarioTO $uTO)
    {
        try {
            $dao = new PessoaDAO();
            $arrTelefones = $this->retornaTelefones($uTO, $uTO->getSessao()->id_entidade);
            $ctTO = new ContatosTelefoneTO();
            $ctpTO = new ContatosTelefonePessoaTO();
            foreach ($arrTelefones as $fone => $value) {
                $ctTO->setId_telefone($value->getId_telefone());
                $ctTO->setId_tipotelefone($value->getId_tipotelefone());
                $ctTO->setNu_ddd($value->getNu_ddd());
                $ctTO->setNu_ddi($value->getNu_ddi());

                $ctpTO->setBl_padrao(false);
                $ctpTO->setId_entidade($value->getId_entidade());
                $ctpTO->setId_telefone($value->getId_telefone());
                $ctpTO->setId_usuario($value->getId_usuario());
                $result = $dao->editarTelefone($ctTO, $ctpTO);
            }
        } catch (Zend_Exception $e) {
            throw new Zend_Exception('Erro ao editar o telefone padrão! ' . $dao->excecao);
        }
    }

    /**
     * Procedimento que salva o Email da Pessoa
     * @throws Zend_Exception
     * @return array
     */
    public function _procedimentoSalvarContatosEmailPessoaPerfil(
        UsuarioTO $uTO,
        ContatosEmailTO $ceTO,
        ContatosEmailPessoaPerfilTO $ceppTO
    )
    {
        if ($ceppTO->getId_email()) {
            $mensageiro = $this->editarEmail($ceTO, $ceppTO);
        } else {
            $mensageiro = $this->cadastrarEmail($uTO, $ceTO, $ceppTO);
        }
        return array($ceTO, $ceppTO);
    }

    /**
     * Metodo que Edita Email
     * @param ContatosEmailTO $ceTO
     * @param ContatosEmailPessoaPerfilTO $ceppTO
     * @return Ead1_Mensageiro
     */
    public function editarEmail(ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        try {

            $uTO = new UsuarioTO();
            $uTO->setId_usuario($ceppTO->getId_usuario());
            $uTO->fetch(true, true, true);
//                Zend_Debug::dump($uTO);die;
            $this->verificaEmailDuplicidade($uTO, $ceTO);

            if (!$ceppTO->getId_entidade()) {
                $ceppTO->setId_entidade($ceppTO->getSessao()->id_entidade);
            }
            if (!$this->validaEmail($ceTO->getSt_email())) {
                THROW new Zend_Validate_Exception('Email Inválido!');
            }
            if ($ceppTO->getBl_padrao() === true) {
                $this->editarEmailPadrao($uTO);
            }
            $this->dao->editarEmail($ceTO, $ceppTO);

            $this->mensageiro->setMensageiro('Email Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Email!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Email
     * @param UsuarioTO $uTO
     * @param ContatosEmailTO $ceTO
     * @param ContatosEmailPessoaPerfilTO $ceppTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEmail(UsuarioTO $uTO, ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        try {

            $this->verificaEmailDuplicidade($uTO, $ceTO);

            if (!$ceppTO->getId_entidade()) {
                $ceppTO->setId_entidade($ceppTO->getSessao()->id_entidade);
            }
            if (!$this->validaEmail($ceTO->getSt_email())) {
                THROW new Zend_Validate_Exception('Email Inválido!');
            }
            if ($ceppTO->getBl_padrao() === true) {
                $uTOEdit = new UsuarioTO();
                $uTOEdit->setId_usuario($ceppTO->getId_usuario());
                $this->editarEmailPadrao($uTOEdit, $ceppTO->getId_entidade());
            }
            $uTO->setId_usuario($ceppTO->getId_usuario());
            $this->dao->cadastrarEmail($uTO, $ceTO, $ceppTO);
            $this->mensageiro->addMensagem($ceTO);
            $this->mensageiro->addMensagem($ceppTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Email!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Verifica se a entidade trabalha com e-mail único, se trabalhar controla o cadastro
     * @param UsuarioTO $uTO
     * @param ContatosEmailTO $cemailTO
     * @return bool true para email duplicado, false para email não duplicado
     * @throws Exception
     */
    public function verificaEmailDuplicidade(UsuarioTO $uTO, ContatosEmailTO $cemailTO)
    {
        try {

            //instancia a negocio do esquema de configuração
            $negocio = new \G2\Negocio\EsquemaConfiguracao();
            $validarEmail = $negocio->retornarConfiguracaoParaEmailDuplicado();

            $duplicado = false;
            //verifica a variavel de controle
            if ($validarEmail) {
                //if true, vai buscar no banco o e-mail passado
                $emails = $this->retornaEmailDuplicidade($uTO, $cemailTO->getSt_email(),
                    $uTO->getSessao()->id_entidade);
                //se o e-mail já existir no banco vai gerar um exception
                if ($emails) {
                    throw new Zend_Exception('Este e-mail já está cadastrado em nosso sistema!');
                }

                //se passou do if acima, significa que o e-mail não esta no banco então podemos continuar
                $duplicado = true;
            }

            return $duplicado;

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo que retorna Emails em duplicidade, pega o ID usuario e o E-mail e verifica se o mesmo existe para outro usuário que não aquele
     * @param UsuarioTO $uTO
     * @param int $idEntidade - OPCIONAL
     * @param string $st_email
     * @return array - Lista de VwPessoaEmailTO
     */
    public function retornaEmailDuplicidade(UsuarioTO $uTO, $st_email, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        if (empty($idEntidade)) {
            $idEntidade = $uTO->getSessao()->id_entidade;
        }
        $emails = $dao->retornaEmailDuplicidade($uTO, $idEntidade, $st_email);
        return Ead1_TO_Dinamico::encapsularTo($emails, new VwPessoaEmailTO());
    }

    /**
     * Editar emails padrão
     * @param UsuarioTO $uTO
     * @return Boolean
     */
    private function editarEmailPadrao(UsuarioTO $uTO, $idEntidade = null)
    {
        $dao = new PessoaDAO();
        $arrEmails = $this->retornaEmails($uTO, $idEntidade);
        $ceTO = new ContatosEmailTO();
        $ceppTO = new ContatosEmailPessoaPerfilTO();
        foreach ($arrEmails as $email => $value) {
            $ceTO->setId_email($value->getId_email());
            $ceTO->setSt_email($value->getSt_email());

            //$ceppTO->setBl_ativo(true);
            $ceppTO->setBl_ativo(false);
            $ceppTO->setBl_padrao(false);
            $ceppTO->setId_perfil($value->getId_perfil());
            $ceppTO->setId_entidade($value->getId_entidade());
            $ceppTO->setId_email($value->getId_email());
            $ceppTO->setId_usuario($value->getId_usuario());
            $result = $dao->editarEmail($ceTO, $ceppTO);
            $this->atualizarDadosAlunoMoodle($uTO,null,'editarEmailPadrao');

            if (!$result) {
                return $this->mensageiro->setMensageiro('Erro ao editar o email padrão! ' . $dao->excecao,
                    Ead1_IMensageiro::ERRO, $dao->excecao);
            }
        }
    }

    /**
     * @param $TO
     * @param null $id_entidade
     * @param string $nameFunction
     * @throws Zend_Exception
     */
    public function atualizarDadosAlunoMoodle($TO, $id_entidade = null, $nameFunction = '')
    {
        try {
            $id_entidade = ($id_entidade ? $id_entidade : $TO->getSessao()->id_entidade);
            $uiTO = new UsuarioIntegracaoTO();
            $uiTO->setId_usuario($TO->getId_usuario());
            $uiTO->setId_entidade($id_entidade);
            $uiTO->setId_sistema(\G2\Constante\Sistema::MOODLE);//Define o id sistema do Moodle
            $uiTO->fetch(false, true, true);

            if ($uiTO->getId_usuariointegracao() && $uiTO->getId_entidadeintegracao()) {
                if (!is_null($TO->getId_usuario())) {
                    $vwPessoaTO = new VwPessoaTO();
                    $vwPessoaTO->setId_usuario($TO->getId_usuario());
                    $vwPessoaTO->setId_entidade($id_entidade);
                    $dadosPessoa = $this->retornarVwPessoa($vwPessoaTO);
                    $dadosPessoa = Ead1_TO_Dinamico::encapsularTo($dadosPessoa, new VwPessoaTO());

                    $moodleUsuario = new MoodleUsuariosWebServices($id_entidade, $uiTO->getId_entidadeintegracao());
                    $mensageiro = $moodleUsuario->atualizarUsuario($dadosPessoa, $nameFunction);

                    $this->mensageiro->setMensageiro($mensageiro, Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro("Nenhum usuário foi encontrado", Ead1_IMensageiro::ERRO);
                }
            }
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao atualizar dados do aluno no moodle. " . $e->getMessage());
        }

    }

    /**
     *
     * @param VwPessoaTO $vwPessoaTO
     * @return boolean|multitype:
     */
    public function retornarVwPessoa(VwPessoaTO $vwPessoaTO, $where = null)
    {
        $dao = new PessoaDAO();
        $pessoa = $dao->retornarVwPessoa($vwPessoaTO, $where);
        if (!$pessoa) {
            return false;
        } else {
            return $pessoa->toArray();
        }

    }

    /**
     * Procedimento que salva o Endereço da Pessoa
     * @throws Zend_Exception
     * @param UsuarioTO $uTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @param EnderecoTO $enderecoTO
     * @return array
     */
    public function _procedimentoSalvarPessoaEndereco(
        UsuarioTO $usuarioTO,
        PessoaEnderecoTO $pessoaEnderecoTO,
        EnderecoTO $enderecoTO
    )
    {
        if ($pessoaEnderecoTO->getId_endereco()) {
            $mensageiro = $this->editarEndereco($enderecoTO, $pessoaEnderecoTO);
        } else {
            $mensageiro = $this->cadastrarEndereco($usuarioTO, $enderecoTO, $pessoaEnderecoTO);
        }
        return array($enderecoTO, $pessoaEnderecoTO);
    }

    /**
     * Metodo que Edita Endereço
     * @param EnderecoTO $eTO
     * @param PessoaEnderecoTO $peTO
     * @return Ead1_Mensageiro
     */
    public function editarEndereco(EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {

        try {
            if ($eTO->getNu_numero()) {
                $numeroLimpo = $this->somenteNumero($eTO->getNu_numero());
                if (empty($numeroLimpo)) {
                    throw new Zend_Validate_Exception('É necessário haver ao menos 1 caracter numérico no número!');
                }
            }
            if (!$peTO->getId_entidade()) {
                $peTO->setId_entidade($peTO->getSessao()->id_entidade);
            }
//			if(!$this->validaCep($eTO->getSt_cep())){
//				THROW new Zend_Validate_Exception('Cep Invalido!');
//			}
            if ($peTO->getBl_padrao() === true) {
                $uTO = new UsuarioTO();
                $uTO->setId_usuario($peTO->getId_usuario());
                $this->editarEnderecoPadrao($uTO, $eTO);
            }
            $this->dao->editarEndereco($eTO, $peTO);
            $this->mensageiro->setMensageiro('Endereço Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Endereço!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Endereço
     * @param UsuarioTO $uTO
     * @param EnderecoTO $eTO
     * @param PessoaEnderecoTO $peTO
     * @return Ead1_Mensageiro
     * @throws Zend_Db_Exception
     * @throws Zend_Validate_Exception
     */
    public function cadastrarEndereco(UsuarioTO $uTO, EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        try {
// 			if(!$this->somenteNumero($eTO->getNu_numero())){
// 				throw new Zend_Validate_Exception('É necessário haver ao menos 1 caracter numérico no número!');
// 			}
            if (!$peTO->getId_entidade()) {
                $peTO->setId_entidade($peTO->getSessao()->id_entidade);
            }
            $peTO->setId_usuario($uTO->getId_usuario());
//			if(!$this->validaCep($eTO->getSt_cep())){
//				THROW new Zend_Validate_Exception('Cep Invalido!');
//			}

            if ($peTO->getBl_padrao() == null) {
                $peTO->setBl_padrao(false);
            } elseif ($peTO->getBl_padrao() === true) {
                $uTOEdit = new UsuarioTO();
                $uTOEdit->setId_usuario($peTO->getId_usuario());
                $this->editarEnderecoPadrao($uTOEdit, $eTO);
            }
            $eTO->setBl_ativo(true);
            $eTO->setSt_cep($this->limpaCep($eTO->getSt_cep()));

            $id_endereco = $this->dao->cadastrarEndereco($uTO, $eTO, $peTO);

            $eTO->setId_endereco($id_endereco);
            $peTO->setId_endereco($id_endereco);

            $this->mensageiro->addMensagem($eTO);
            $this->mensageiro->addMensagem($peTO);
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Endereço!', Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            THROW $e;
        }

        return $this->mensageiro;
    }

    /**
     * Editar endereço padrão
     * @param UsuarioTO $uTO
     * @param EnderecoTo $endTO
     * @return Boolean
     */
    private function editarEnderecoPadrao(UsuarioTO $uTO, EnderecoTO $endTO)
    {
        $dao = new PessoaDAO();
        $arrEnderecos = $this->retornaEnderecos($uTO, true);
        $eTO = new EnderecoTO();
        $peTO = new PessoaEnderecoTO();
        foreach ($arrEnderecos as $endereco => $valor) {
            $eTO->setId_endereco($valor->getId_endereco());
            $eTO->setId_municipio($valor->getId_municipio());
            $eTO->setId_pais($valor->getId_pais());
            $eTO->setId_tipoendereco($valor->getId_tipoendereco());
            $eTO->setSt_bairro($valor->getSt_bairro());
            $eTO->setSt_cep($valor->getSt_cep());
            $eTO->setSt_cidade($valor->getSt_cidade());
            $eTO->setSt_complemento($valor->getSt_complemento());
            $eTO->setSt_endereco($valor->getSt_endereco());
            $eTO->setSt_estadoprovincia($valor->getSt_estadoprovincia());
            $eTO->setSg_uf($valor->getSg_uf());
            $eTO->setBl_ativo($valor->getBl_ativo());
            //$eTO->setBl_ativo(false);
            $peTO->setId_entidade($valor->getId_entidade());
            $peTO->setId_usuario($valor->getId_usuario());
            $peTO->setId_endereco($valor->getId_endereco());
            $peTO->setBl_padrao(false);
            if ($endTO->getId_tipoendereco() === $eTO->getId_tipoendereco()) {
                $result = $dao->editarEndereco($eTO, $peTO);
                if (!$result) {
                    return $this->mensageiro->setMensageiro('Erro ao editar o endereço padrão! ' . $dao->excecao,
                        Ead1_IMensageiro::ERRO, $dao->excecao);
                }
            }
        }
    }

    /**
     * Metodo que Cadastra Informações Academicas da Pessoa
     * @param UsuarioTO $uTO
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarInformacaoAcademica(UsuarioTO $uTO, InformacaoAcademicaPessoaTO $iapTO)
    {

        $dao = new PessoaDAO();
        if (!$iapTO->getId_informacaoacademicapessoa()) {
            if (!$pk = $dao->cadastrarInformacaoAcademica($uTO, $iapTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Cadastrar Informações Academicas!',
                    Ead1_IMensageiro::ERRO, $dao->excecao);
            } else {
                $iapTO->setId_informacaoacademicapessoa($pk);
                return $this->mensageiro->setMensageiro($iapTO, Ead1_IMensageiro::SUCESSO);
            }
        } else {
            if (!$this->editarInformacaoAcademica($iapTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Editar Informações Academicas!',
                    Ead1_IMensageiro::ERRO, $dao->excecao);
            } else {
                $iapTO->fetch(true, true, true);
                return $this->mensageiro->setMensageiro($iapTO, Ead1_IMensageiro::SUCESSO);
            }
        }


    }

    /**
     * Metodo que Edita Informações Academicas da Pessoa
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function editarInformacaoAcademica(InformacaoAcademicaPessoaTO $iapTO)
    {
        $dao = new PessoaDAO();
        if (!$dao->editarInformacaoAcademica($iapTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Informações Academicas!', Ead1_IMensageiro::ERRO,
                $dao->excecao);
        } else {
            return $this->mensageiro->setMensageiro('Informações Academicas Editadas com Sucesso!',
                Ead1_IMensageiro::SUCESSO);
        }
    }

    /**
     * Retorna as pessoas pra autocomplete
     * @throws Zend_Exception
     * @param PesquisarPessoaTO $to
     * @return array
     */
    public function pesquisarPessoaAutoComplete(PesquisarPessoaTO $to)
    {
        try {
            if (!$to->getId_entidade()) {
                $to->setId_entidade($to->getSessao()->id_entidade);
            }

            $pesquisarDAO = new PesquisarDAO();
            $usuarios = $pesquisarDAO->retornaPesquisaPessoa($to)->toArray();

            if (empty($usuarios)) {
                return $this->mensageiro->setMensageiro('Nenhum registro de Usuário encontrado!',
                    Ead1_IMensageiro::AVISO);
            }

            //Adicionando validação de Meioridade
            $usuarios = Ead1_TO_Dinamico::encapsularTo($usuarios, new PesquisarPessoaTO());

            foreach ($usuarios as $usuario) {
                $data_hoje = new Zend_Date(null, Zend_Date::ISO_8601);

                //Converte DateTime para String
                $data_usuario = $usuario->getDt_nascimento();
                //$string_data = $data_usuario->format('Y-m-d');

                $dt_nascimento = new Zend_Date($data_usuario, Zend_Date::ISO_8601);
                $diff = $data_hoje->sub($dt_nascimento, Zend_Date::YEAR);

                //Verifica se é menor de idade
                if ($diff->get(Zend_Date::YEAR) >= 18) {
                    $usuario->setBl_menoridade(false);
                } else {
                    $usuario->setBl_menoridade(true);
                }

                $vwMatriculaTO = new VwMatriculaTO();
                $vwMatriculaTO->setId_usuario($usuario->getId_usuario());
                $vwMatriculaTO->setId_entidadematricula($usuario->getId_entidade());
                $matriculaDAO = new MatriculaDAO();
                $matriculas = $matriculaDAO->retornarVwMatriculaPesquisaPessoa($vwMatriculaTO)->toArray();
                $matriculas = Ead1_TO_Dinamico::encapsularTo($matriculas, new VwMatriculaTO());

                if (!empty($matriculas)) {
                    foreach ($matriculas as $index => $matricula) {
                        $usuario->projetos[$index] = $matricula;
                    }
                } else {
                    $usuario->projetos = null;
                }
            }
            return $this->mensageiro->setMensageiro($usuarios, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Pessoa!', Ead1_IMensageiro::ERRO,
                $e->getMessage());
        }
    }

    /*
     * Método criado para atualizar dados do usuário no moodle de acordo com as atualizações do g2
     * Débora Castro 03.12.2013 - debora.castro@unyleya.com.br
     * AC-128
     */

    /**
     * Metodo que retorna dados da VwFuncionarioTO
     * @param VwFuncionarioTO $vwTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwFuncionario(VwFuncionarioTO $vwTO)
    {
        try {

            if (!$vwTO->getId_entidade()) {
                $vwTO->setId_entidade($vwTO->getSessao()->id_entidade);
            }

            $funcionario = $this->dao->retornarVwFuncionario($vwTO);

            if (!$funcionario) {
                $this->mensageiro->setMensageiro("Nenhum dado encontrado", Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcionario, new VwFuncionarioTO()),
                    Ead1_IMensageiro::SUCESSO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao retornar VwFuncionarioTO", Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

}

<?php

/**
 * Classe com regras de negócio para o TCCBO
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 24/11/2012
 * @package models
 * @subpackage bo
 */
class TCCBO extends Ead1_BO
{


    /**
     * @var TCCDAO
     */
    protected $dao;

    protected $pesolimite = 100;

    public function __construct()
    {
        $this->dao = new TCCDAO();
    }

    /**
     * Método de listagem de ParametroTCC
     * @param ParametroTCCTO $to
     * @return Ambigous <boolean, multitype:>
     * @see
     */
    public function retornarParametroTCC(ParametroTCCTO $to)
    {

        try {

            $dados = $this->dao->retornarParametroTCC($to);
            if ($dados) {
                return new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
            }
            return new Ead1_Mensageiro('Nenhum Parâmetro encontrado!', Ead1_IMensageiro::AVISO);

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro("Erro ao retornar ParametroTCC: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Calcula o Peso Limite
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param unknown_type $id_projetopedagogico
     * @param ParametroTCCTO $to
     * @throws Zend_Exception
     * @return boolean
     */
    public function verificaTotalPesoParametroTCC(ParametroTCCTO $to)
    {

        try {

            if (!$to->getId_projetopedagogico()) {
                throw new Zend_Exception('O Projeto Pedagógico é obrigatório!');
            }

            $peso = $this->dao->retornaTotalPesoParametroTCC($to);
            if ($peso) {
                $pesofinal = $peso['total'] + $to->getNu_peso();
            } else {
                $pesofinal = $to->getNu_peso();
            }

            if ($pesofinal > $this->pesolimite) {
                throw new Zend_Exception('O Peso total dos Parâmetros do TCC do Projeto Pedagógico não pode ser superior a ' . $this->pesolimite . '!');
            }

            return true;
        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
            return false;
        }


    }


    /**
     * Exclui a nota anterior
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param AvaliacaoParametrosTO $apTO
     * @return Ead1_Mensageiro
     */
    public function excluirAvaliacaoParametroTCC(AvaliacaoParametrosTO $apTO)
    {

        $orm = new AvaliacaoParametrosORM();
        $this->dao->beginTransaction();
        $excluidos = 0;
        try {

            if ($apTO->getId_avaliacaoaluno() && $apTO->getId_parametrotcc()) {
                $excluidos = $orm->delete('id_avaliacaoaluno = ' . $apTO->getId_avaliacaoaluno() . ' AND  id_parametrotcc = ' . $apTO->getId_parametrotcc());
            }
            $this->dao->commit();
            return new Ead1_Mensageiro($excluidos . ' Excluído(s) com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível excluir a nota anterior, " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);

        }

    }


    /**
     * Metodo que decide se cadastra ou edita o ParametroTCC
     * @param ParametroTCCTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarParametroTCC(ParametroTCCTO $to)
    {

        $this->dao->beginTransaction();
        try {


            if (!$to->getId_projetopedagogico()) {
                throw new Zend_Exception('O Projeto Pedagógico é obrigatório!');
            }


            if ($to->getNu_peso() < 0) {
                throw new Zend_Exception('Zero não é um valor válido!');
            }

            $this->verificaTotalPesoParametroTCC($to);

            if ($to->getId_parametrotcc()) {
                $mensageiro = $this->editarParametroTCC($to);
            } else {
                $mensageiro = $this->cadastrarParametroTCC($to);
            }

            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception($mensageiro->getCurrent());
            }

            $this->dao->commit();
            return $mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Erro ao salvar o Parametro: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Método que cadastra umo ParametroTCC
     * @param ParametroTCCTO $to
     * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
     * @see ParametroTCCBO::salvarParametroTCC();
     */
    public function cadastrarParametroTCC(ParametroTCCTO $to)
    {
        $this->dao->beginTransaction();
        try {

            /*$to->setId_usuariocadastro($to->getSessao()->id_usuario);
            $to->setId_entidade($to->getSessao()->id_entidade);*/

            $to->setId_parametrotcc($this->dao->cadastrarParametroTCC($to));
            $to->fetch(true, true, true);

            $this->dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível cadastrar o ParametroTCC, " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que edita umo ParametroTCC
     * @param ParametroTCCTO $to
     * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
     * @see ParametroTCCBO::salvarParametroTCC();
     */
    public function editarParametroTCC(ParametroTCCTO $to)
    {
        $this->dao->beginTransaction();
        try {

            $this->dao->editarParametroTCC($to);
            $to->fetch(true, true, true);

            $this->dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível editar o ParametroTCC, " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Método que faz o UPLOAD do arquivo de TCC
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param ArquivoTO | array $arquivo
     * @param array de  $arApTO AvaliacaoParametrosTO
     * @param AvaliacaoAlunoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function salvarNotaTCC(AvaliacaoAlunoTO $aaTO, array $arApTO, $arquivo = null)
    {
        $this->dao->beginTransaction();
        try {

            if (empty($arApTO)) {
                throw new Zend_Exception("O array de Avaliação Parâmetros é obrigatório!");
            }

            $avBO = new AvaliacaoBO();
            if (!$aaTO->getid_avaliacaoaluno()) {
                $meav = $avBO->salvarAvaliacaoAluno($aaTO);
                if ($meav->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meav->getFirstMensagem());
                }
            }

            foreach ($arApTO as $apTO) {
                $apTO->setId_avaliacaoaluno($aaTO->getid_avaliacaoaluno());
                if ($this->salvarAvaliacaoParametros($apTO)->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception('Não foi possível salvar os Parâmetros de Avaliação!');
                }
            }

            if ($arquivo) {
                if ($arquivo instanceof ArquivoTO) {
                    $arquivoTO = $arquivo;
                } else {
                    $name = (explode('.', $arquivo['name']));

                    $arquivoTO = new ArquivoTO();
                    $arquivoTO->setAr_arquivo($arquivo);
                    $arquivoTO->setSt_extensaoarquivo(end($name));
                }

                $arquivoTO->setSt_caminhodiretorio('avaliacao');
                $arquivoTO->setSt_nomearquivo($aaTO->getid_avaliacaoaluno());


                $meupload = $this->uploadBasico($arquivoTO, false);
                if ($meupload->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meupload->getFirstMensagem());
                }

                $upTO = new UploadTO();
                $upTO->setSt_upload($arquivoTO->getSt_nomearquivo() . '.' . $arquivoTO->getSt_extensaoarquivo());

                $dao = new UploadDAO();
                $upTO->setId_upload($dao->cadastrarUpload($upTO));


                $aaTO->setId_upload($upTO->getId_upload());
                $meav = $avBO->editarAvaliacaoAluno($aaTO);
                if ($meav->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($meav->getFirstMensagem());
                }

                $mensagem = "Nota e Upload da versão final do TCC salvos com sucesso!";
            } else {
                $mensagem = "Nota do TCC salva com sucesso!";
            }

            $this->dao->commit();
            return new Ead1_Mensageiro($mensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível salvar o TCC. " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);

        }

    }


    /**
     * Método de listagem de AvaliacaoParametros
     * @param AvaliacaoParametrosTO $to
     * @return Ambigous <boolean, multitype:>
     * @see
     */
    public function listarAvaliacaoParametros(AvaliacaoParametrosTO $to)
    {

        try {

            $dados = $this->dao->listarAvaliacaoParametros($to);
            if ($dados) {
                return new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
            }
            return new Ead1_Mensageiro('Nenhum AvaliacaoParametros encontrado!', Ead1_IMensageiro::AVISO);

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro("Erro ao listar AvaliacaoParametros: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Retorna os dados da Vw_AvaliacaoParametroTCC
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VwAvaliacaoParametroTCCTO $to
     * @return Ead1_Mensageiro
     */
    public function retornaVwAvaliacaoParametroTCC(VwAvaliacaoParametroTCCTO $to)
    {

        try {

            return $this->dao->retornaVwAvaliacaoParametroTCC($to);

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro("Erro ao listar AvaliacaoParametros: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Metodo que decide se cadastra ou edita o AvaliacaoParametros
     * @param AvaliacaoParametrosTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoParametros(AvaliacaoParametrosTO $to)
    {

        $this->dao->beginTransaction();
        try {


            if ($to->getId_avaliacaoparametros()) {
                $mensageiro = $this->editarAvaliacaoParametros($to);
            } else {
                $mensageiro = $this->cadastrarAvaliacaoParametros($to);
            }

            if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                throw new Zend_Exception($mensageiro->getCurrent());
            }

            $this->dao->commit();
            return $mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Erro ao salvar o AvaliacaoParametros: " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Método que cadastra umo AvaliacaoParametros
     * @param AvaliacaoParametrosTO $to
     * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
     * @see AvaliacaoParametrosBO::salvarAvaliacaoParametros();
     */
    public function cadastrarAvaliacaoParametros(AvaliacaoParametrosTO $to)
    {
        $this->dao->beginTransaction();
        try {

            $to->setId_usuariocadastro($to->getSessao()->id_usuario);
            $to->setId_entidade($to->getSessao()->id_entidade);

            $to->setgetId_avaliacaoparametros($this->dao->cadastrarAvaliacaoParametros($to));
            $to->fetch(true, true, true);

            $this->dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível cadastrar o AvaliacaoParametros, " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Método que edita umo AvaliacaoParametros
     * @param AvaliacaoParametrosTO $to
     * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
     * @see AvaliacaoParametrosBO::salvarAvaliacaoParametros();
     */
    public function editarAvaliacaoParametros(AvaliacaoParametrosTO $to)
    {
        $this->dao->beginTransaction();
        try {

            $this->dao->editarAvaliacaoParametros($to);
            $to->fetch(true, true, true);

            $this->dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro("Não foi possível editar o AvaliacaoParametros, " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }



    public function enviarTcc($id_matricula,$id_usuario,$st_tituloavaliacao,$arquivo){

        $this->dao->beginTransaction();
        try {

            $vwenvio = new \G2\Entity\VwEnvioTcc();
            $vwenvio->findOneBy(array('id_matricula'=>$id_matricula,'id_usuario'=>$id_usuario ));
            if(!$vwenvio->getid_matricula())
                throw new \Exception("Não encontramos nenhum TCC liberado para envio");


            $aaTO = new AvaliacaoAlunoTO();
            $aaTO->setId_matricula($vwenvio->getid_matricula());
            $aaTO->setId_avaliacaoconjuntoreferencia($vwenvio->getid_avaliacaoconjuntoreferencia());
            $aaTO->setId_avaliacao($vwenvio->getid_avaliacao());
            $aaTO->setSt_tituloavaliacao($st_tituloavaliacao);
            $aaTO->setSt_nota(null, false);

            $avParamTO = new AvaliacaoParametrosTO();
            $avParamTO->setId_avaliacaoaluno($vwenvio->getid_avaliacao());

            $mensageiro = $this->salvarNotaTCC($aaTO, array($avParamTO), $arquivo);
            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $alocacaoTO = new AlocacaoTO();
                $alocacaoTO->setId_alocacao($vwenvio->getid_alocacao());
                $alocacaoTO->setId_saladeaula($vwenvio->getid_saladeaula());
                $alocacaoTO->setId_situacaotcc(AlocacaoTO::SITUACAOTCC_EM_ANALISE);

                $avaliacaoBO = new AvaliacaoBO();
                $mensageiro = $avaliacaoBO->alteraSituacaoTCC($alocacaoTO);

                $toEmail = new VwAlunosSalaTO();
                $toEmail->setId_saladeaula($vwenvio->getid_saladeaula());
                $toEmail->setId_alocacao($vwenvio->getid_alocacao());
                $toEmail->setId_entidade($vwenvio->getid_entidadesala());
                $toEmail->setId_matricula($vwenvio->getid_matricula());
                $toEmail->fetch(false, true, true);

                //Enviado email para o aluno com a confirmação de envio de TCC
                if ($toEmail->getSt_email()) {
                    $mensageiro = $avaliacaoBO->enviarEmailEnvioTcc($toEmail);
                }
            } else {
                throw new \Exception($mensageiro->getFirstMensagem());
            }
            $this->dao->commit();
            return $mensageiro;
        } catch (\Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


}

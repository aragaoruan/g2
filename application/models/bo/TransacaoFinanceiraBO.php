<?php

/**
 * Classe com regras de negócio para o TransacaoFinanceira
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage bo
 */
class TransacaoFinanceiraBO extends Ead1_BO {



	/**
	 * @var TransacaoFinanceiraDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new TransacaoFinanceiraDAO();
	}
    
    
    
    
	/**
	 * Método de listagem de TransacaoFinanceira
	 * @param TransacaoFinanceiraTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarTransacaoFinanceira(TransacaoFinanceiraTO $to){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarTransacaoFinanceira($to), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar TransacaoFinanceira: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
    
	/**
	 * Retorna as Transações financeiras pendentes
	 * @return Ead1_Mensageiro
	 */
	public function listarTransacaoFinanceiraPendente(){
		
		try {
			
			$transacoes = $this->dao->listarTransacaoFinanceiraPendente();
			if($transacoes){
				return new Ead1_Mensageiro($transacoes, Ead1_IMensageiro::SUCESSO);
			} else {
				return new Ead1_Mensageiro("Nenhuma transação encontrada!", Ead1_IMensageiro::AVISO);
			}
			
		} catch (Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar TransacaoFinanceira: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
	/**
	 * Metodo que decide se cadastra ou edita o TransacaoFinanceira
	 * @param TransacaoFinanceiraTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarTransacaoFinanceira(TransacaoFinanceiraTO $to){
	
		$this->dao->beginTransaction();
		try {
			
			
			
			if($to->getid_transacaofinanceira()){
				$mensageiro = $this->editarTransacaoFinanceira($to);
			} else {
				$mensageiro =  $this->cadastrarTransacaoFinanceira($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o TransacaoFinanceira: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}


	
	/**
	 * Método que cadastra umo TransacaoFinanceira
	 * @param TransacaoFinanceiraTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see TransacaoFinanceiraBO::salvarTransacaoFinanceira();
	 */
	public function cadastrarTransacaoFinanceira(TransacaoFinanceiraTO $to){
		$this->dao->beginTransaction();
		try {

			$to->setId_usuariocadastro(($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario));
			$to->setid_transacaofinanceira($this->dao->cadastrarTransacaoFinanceira($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o TransacaoFinanceira, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
	/**
	 * Método que edita umo TransacaoFinanceira
	 * @param TransacaoFinanceiraTO $to
	 * @return Ead1_Mensageiro
	 * @see TransacaoFinanceiraBO::salvarTransacaoFinanceira();
	 */
	public function editarTransacaoFinanceira(TransacaoFinanceiraTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarTransacaoFinanceira($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o TransacaoFinanceira, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
	/**
	 * Salva a Transação Financeira no Banco de Dados
	 * @param array $arrLanTO - array de LancamentoTO
	 * @param TransacaoFinanceiraTO $tranTO
	 */
	public function salvarLancamentoTransacaoFinanceira(array $arrLanTO, TransacaoFinanceiraTO $tranTO){
	
		$dao = new VendaDAO();
		$dao->beginTransaction();
	
		try {
	
	
			if(!$tranTO->getId_transacaofinanceira()){
				$transacao = $this->cadastrarTransacaoFinanceira($tranTO);
				if($transacao->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($transacao->getFirstMensagem());
				}
				$tranTO = $transacao->getFirstMensagem();
			}
			
			foreach($arrLanTO as $lanTO){
				$tranLanTO = new TransacaoLancamentoTO();
				$tranLanTO->setId_lancamento($lanTO->getid_lancamento());
				$tranLanTO->setId_transacaofinanceira($tranTO->getId_transacaofinanceira());
				$mensageiro = $this->cadastrarTransacaoLancamento($tranLanTO);
				if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception($mensageiro->getFirstMensagem());
				}
			}
	
			$dao->commit();
			return new Ead1_Mensageiro('Transação salva com sucesso.', Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$dao->rollBack();
			return new Ead1_Mensageiro('Erro ao salvar a Transação. '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}

   
    
	/**
	 * Método de listagem de TransacaoLancamento
	 * @param TransacaoLancamentoTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarTransacaoLancamento(TransacaoLancamentoTO $to){
		
		try {
			return new Ead1_Mensageiro($this->dao->listarTransacaoLancamento($to), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar TransacaoLancamento: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
 	
	/**
	 * Método que cadastra umo TransacaoLancamento
	 * @param TransacaoLancamentoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see TransacaoLancamentoBO::salvarTransacaoLancamento();
	 */
	public function cadastrarTransacaoLancamento(TransacaoLancamentoTO $to){
		$this->dao->beginTransaction();
		try {

			$to->setid_transacaolancamento($this->dao->cadastrarTransacaoLancamento($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o TransacaoLancamento, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
    
	/**
	 * Método que edita umo TransacaoLancamento
	 * @param TransacaoLancamentoTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see TransacaoLancamentoBO::salvarTransacaoLancamento();
	 */
	public function editarTransacaoLancamento(TransacaoLancamentoTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarTransacaoLancamento($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o TransacaoLancamento, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}

}
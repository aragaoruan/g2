<?php
/**
 * Classe com regras de negócio para interações de Campanha Comercial
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 17/11/2010
 * 
 * @package models
 * @subpackage bo
 */
class CampanhaComercialBO extends FinanceiroBO{
	
	public $mensageiro;
	
	/**
	 * @var CampanhaComercialDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new CampanhaComercialDAO();
	}
				
	/**
	 * Metodo que Decide se Cadastra ou Exclui o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		$orm = new CampanhaConvenioORM();
		if($orm->consulta($ccTO,true)){
			return $this->deletarCampanhaConvenio($ccTO);
		}
			return $this->cadastrarCampanhaConvenio($ccTO);
	}
				
	/**
	 * Metodo que Cadastra ou Exclui o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		$orm = new CampanhaPerfilORM();
		if($orm->consulta($cpTO,true)){
			return $this->deletarCampanhaPerfil($cpTO);
		}
			return $this->cadastrarCampanhaPerfil($cpTO);
	}
					
	/**
	 * Metodo que Cadastra ou Exclui o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		$orm = new CampanhaProjetoAreaORM();
		if($orm->consulta($cpaTO,true)){
			return $this->deletarCampanhaProjetoArea($cpaTO);
		}
			return $this->cadastrarCampanhaProjetoArea($cpaTO);
	}
		
	/**
	 * Metodo que Cadastra a Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaComercial(CampanhaComercialTO $ccTO){
		
		$ccTO->setId_usuariocadastro($ccTO->getSessao()->id_usuario);
		
		if(!$ccTO->getId_entidade()){
			$ccTO->setId_entidade($ccTO->getSessao()->id_entidade);
		}

		if(!$this->comparaData($ccTO->getDt_inicio(), $ccTO->getDt_fim())){
			$this->mensageiro->setMensageiro('A Data de Término Não Pode ser Menor que a Data de Inicio!', Ead1_IMensageiro::AVISO);
		}
		
		$id_campanhagravada = $this->dao->cadastrarCampanhaComercial($ccTO);
		
		if(!$id_campanhagravada) {
			$this->mensageiro->setMensageiro('Erro ao Cadastrar Campanha Comercial!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
		}else {
			$ccTO->setId_campanhacomercial($id_campanhagravada);
			$this->mensageiro->setMensageiro($ccTO, Ead1_IMensageiro::SUCESSO);
		}
		
		return $this->mensageiro;
		
	}
	
	/**
	 * Metodo que Cadastra o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$id_campanhaDesconto = $this->dao->cadastrarCampanhaDesconto($cdTO);
		if(!$id_campanhaDesconto){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Desconto da Campanha Comercial!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$cdTO->setId_campanhadesconto($id_campanhaDesconto);
		$cdTO->fetch(true,true,true);
		
		return $this->mensageiro->setMensageiro($cdTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		if(!$this->dao->cadastrarCampanhaConvenio($ccTO)){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Vinculo da Campanha com o Convenio!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Convenio Cadastrado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		if(!$this->dao->cadastrarCampanhaPerfil($cpTO)){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Vinculo da Campanha com o Perfil!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Perfil Cadastrado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Lançamento
	 * @param LancamentoCampanhaTO $lcTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLancamentoCampanha(LancamentoCampanhaTO $lcTO){
		if(!$this->dao->cadastrarLancamentoCampanha($lcTO)){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Vinculo da Campanha com o Lançamento!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Lançamento Cadastrado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	
	
	/**
	 * Vincula um lançamento a Todas as campanhas comerciais
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VwVendaLancamentoTO $vwVendaLancamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function vincularLancamentoCampanhaPontualidade(VwVendaLancamentoTO $vwVendaLancamentoTO){
		
		
		$vendaBO 	= new VendaBO();
		$hoje 		= new Zend_Date(Zend_Date::DATE_SHORT);
		$vencimento = $vwVendaLancamentoTO->getDt_vencimento();
		$executa 	= $vendaBO->comparaData($vencimento, $hoje);
		
		
		if((!$vwVendaLancamentoTO->getBl_quitado()) && (!$executa)){
		
			$this->dao->beginTransaction();
			try {
			
				$cvpTO = new SpCampanhaVendaProdutoTO();
				$cvpTO->setId_entidade($vwVendaLancamentoTO->getId_entidade());
				$cvpTO->setId_tipocampanha(TipoCampanhaTO::PONTUALIDADE);
				$cvpTO->setId_formapagamento($vwVendaLancamentoTO->getId_formapagamento());
				$cvpTO->setId_venda($vwVendaLancamentoTO->getid_venda());
				$mensPontualidade = $vendaBO->retornaSpCampanhaVendaProduto($cvpTO);
			
				if($mensPontualidade->getTipo()==Ead1_IMensageiro::SUCESSO){
					$arrPontualidade = $mensPontualidade->getMensagem();
					if(!empty($arrPontualidade)){
			
						$orm = new LancamentoCampanhaORM();
						$orm->delete("id_lancamento = ".$vwVendaLancamentoTO->getId_lancamento());					
					
						foreach($arrPontualidade as $pontualidade){
							$lcTO = new LancamentoCampanhaTO();
							$lcTO->setId_lancamento($vwVendaLancamentoTO->getId_lancamento());
							$lcTO->setId_campanhacomercial($pontualidade->getId_campanhacomercial());
							$this->cadastrarLancamentoCampanha($lcTO);
						}
					}
				}
				$this->dao->commit();
				return new Ead1_Mensageiro("Campanha(s) de Pontualidade vinculada(s) com sucesso!", Ead1_IMensageiro::SUCESSO);
			} catch (Zend_Exception $e) {
				$this->dao->rollBack();
				return new Ead1_Mensageiro("Erro ao Vincular o Lançamento às Campanhas de Pontualidade", Ead1_IMensageiro::ERRO);
			}
		} else {
			//retorna sucesso também pois não é um erro o lançamento não estar apto e nem é tido como aviso
			return new Ead1_Mensageiro("Este Lançamento não está apto a receber os vínculos com as campanhas.", Ead1_IMensageiro::SUCESSO);
		}
	}
	
	
	
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		if(!$this->dao->cadastrarCampanhaProjetoArea($cpaTO)){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Vinculo da Campanha com o Projeto Pedagógico e a Area!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Projeto Pedagógico e a Area Cadastrado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Edita Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCampanhaComercial(CampanhaComercialTO $ccTO){
		if(!$ccTO->getId_entidade()){
			$ccTO->setId_entidade($ccTO->getSessao()->id_entidade);
		}
		if(!$this->comparaData($ccTO->getDt_inicio(),$ccTO->getDt_fim())){
			return $this->mensageiro->setMensageiro('A Data de Término Não Pode ser Menor que a Data de Inicio!',Ead1_IMensageiro::AVISO);
		}
		if(!$this->dao->editarCampanhaComercial($ccTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Campanha Comercial!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro($ccTO,Ead1_IMensageiro::SUCESSO);
	}
		
	/**
	 * Metodo que Edita o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		if(!$this->dao->editarCampanhaDesconto($cdTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar o Desconto da Campanha Comercial!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Desconto da Campanha Comercial Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Exclui Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaComercial(CampanhaComercialTO $ccTO){
		
		$dados = $this->dao->retornarCampanhaComercial($ccTO)->toArray();
		$retorno = Ead1_TO_Dinamico::encapsularTo($dados, new CampanhaComercialTO());
		$paraEditarTO = $retorno[0];
		$paraEditarTO->setBl_ativo(0);
		
		$this->dao->editarCampanhaComercial($paraEditarTO);

		return $this->mensageiro->setMensageiro("Campanha deletada com sucesso!", Ead1_IMensageiro::SUCESSO);
	}
			
	/**
	 * Metodo que Exclui o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		if(!$this->dao->deletarCampanhaDesconto($cdTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir o Desconto da Campanha Comercial!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Desconto da Campanha Comercial Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
			
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		if(!$this->dao->deletarCampanhaConvenio($ccTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo da Campanha com o Convenio!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Convenio Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
			
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		if(!$this->dao->deletarCampanhaPerfil($cpTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo da Campanha com o Perfil!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Perfil Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
			
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		if(!$this->dao->deletarCampanhaProjetoArea($cpaTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir o Vinculo da Campanha com o Projeto Pedagógico e a Area!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Vinculo da Campanha com o Projeto Pedagógico e a Area Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que Exclui o vinculo da campanha com o produto
	 * @param CampanhaProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO){
		try{
			if(!$this->dao->deletarCampanhaProduto($cpTO)){
				throw new DomainException("Erro ao deletar dados na tabela.");
			}
			return $this->mensageiro->setMensageiro('Vinculo Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Vinculo do Produto com a Campanha Comercial!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a Forma de Pagamento
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaComercial(CampanhaComercialTO $ccTO){
		if(!$ccTO->getId_entidade()){
			$ccTO->setId_entidade($ccTO->getSessao()->id_entidade);
		}
		
		$campanha = $this->dao->retornarCampanhaComercial($ccTO);
		if(is_object($campanha)){
			$campanha = $campanha->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar Campanha Comercial!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		if(empty($campanha)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaComercialTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna a view de campanha, forma de pagamento e produto
	 * @param VwCampanhaFormaPagamentoProduto $vwcfppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaFormaPagamentoProduto(VwCampanhaFormaPagamentoProdutoTO $vwcfppTO){
		$view = $this->dao->retornarVwCampanhaFormaPagamentoProduto($vwcfppTO)->toArray();
		if(empty($view)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($view, new VwCampanhaFormaPagamentoProdutoTO()),Ead1_IMensageiro::SUCESSO); 
	}
	

	
	/**
	 * Metodo que retorna a view de campanha, forma de pagamento e produto
	 * @param VwCampanhaFormaPagamentoProduto $vwcfppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaFormaPagamento(VwCampanhaFormaPagamentoTO $vwcfpTO){
		if(!$vwcfpTO->getId_entidade()){
			$vwcfpTO->setId_entidade($vwcfpTO->getSessao()->id_entidade);
		}
		$view = $this->dao->retornarVwCampanhaFormaPagamento($vwcfpTO)->toArray();
		if(empty($view)){
			return $this->mensageiro->setMensageiro('Nenhum Registro de Forma Pagamento Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($view, new VwCampanhaFormaPagamentoTO()),Ead1_IMensageiro::SUCESSO); 
	}
	
	/**
	 * Metodo que retorna a view de campanha e produto
	 * @param VwCampanhaProdutoTO $vwcpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaProduto(VwCampanhaProdutoTO $vwcpTO){
		$view = $this->dao->retornarVwCampanhaProduto($vwcpTO)->toArray();
		if(empty($view)){
			return $this->mensageiro->setMensageiro('Nenhum Registro de Produto Encontrado!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($view, new VwCampanhaProdutoTO()),Ead1_IMensageiro::SUCESSO); 
	}
	
	/**
	 * Metodo que retorna o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		$campanha = $this->dao->retornarCampanhaDesconto($cdTO);
		if(is_object($campanha)){
			$campanha = $campanha->toArray();
		}else{
			return $this->mensageiro->setMensageiro('Erro ao Retornar Desconto da Campanha!',Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		if(empty($campanha)){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaDescontoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		$campanha = $this->dao->retornarCampanhaConvenio($ccTO);
		if(is_object($campanha)){
			$campanha = $campanha->toArray();
		}else{
			unset($campanha);
		}
		if(!$campanha){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaConvenioTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		$campanha = $this->dao->retornarCampanhaPerfil($cpTO);
		if(is_object($campanha)){
			$campanha = $campanha->toArray();
		}else{
			unset($campanha);
		}
		if(!$campanha){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaPerfilTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Desconto
	 * @param TipoDescontoTO $tdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDesconto(TipoDescontoTO $tdTO){
		$desconto = $this->dao->retornarTipoDesconto($tdTO);
		if(is_object($desconto)){
			$desconto = $desconto->toArray();
		}else{
			unset($desconto);
		}
		if(!$desconto){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($desconto, new TipoDescontoTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		$campanha = $this->dao->retornarCampanhaProjetoArea($cpaTO);
		if(is_object($campanha)){
			$campanha = $campanha->toArray();
		}else{
			unset($campanha);
		}
		if(!$campanha){
			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO,$dao->excecao);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaProjetoAreaTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o tipo da campanha
	 * @param TipoCampanhaTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCampanha(TipoCampanhaTO $tcTO){
		try{
			$tipocampanha = $this->dao->retornarTipoCampanha($tcTO)->toArray();
			if(empty($tipocampanha)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipocampanha, new TipoCampanhaTO()));
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo da Campanha!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		
	}
}
<?php
/**
 * Classe com regras de negócio para interações de Função
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 16/06/2011
 * 
 * @package models
 * @subpackage bo
 */
class FuncaoBO extends Ead1_BO {

	/**
	 * @var FuncaoDAO
	 */
	private $dao;
	
	public $mensageiro;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new FuncaoDAO();
	}
	
	/**
	 * Metodo que retorna função para perfil
	 * @param FuncaoFuncionalidadeTO $ffTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoFuncionalidade(FuncaoFuncionalidadeTO $ffTO) {
		try{
			$dados = $this->dao->retornarFuncao($ffTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Função Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro($dados,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Função para Perfil!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Método que retorna a função 
	 * @param FuncaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncao(FuncaoTO $to){
		try{
			$dados = $this->dao->retornarFuncao($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro("Nenhum Registro Encontrado!",Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FuncaoTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
				$this->mensageiro->setMensageiro("Erro ao Retornar Função!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna perfis para função.
	 * @param FuncaoPerfilTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoPerfil(FuncaoPerfilTO $fpTO){
		try{
			$perfis = $this->dao->retornarFuncaoPerfil($fpTO)->toArray();	
			if(empty($perfis)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil de Função Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfis, new FuncaoPerfilTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Perfil de Função!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna função com dados adicionais.
	 * @param FuncaoPerfilTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoCompleto(FuncaoTO $fTO){
		try{
			$funcoes = $this->dao->retornarFuncaoCompleto($fTO)->toArray();	
			if(empty($funcoes)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil de Função Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcoes, new FuncaoCompletoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Perfil de Função!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que cadastra perfis para função.
	 * @param $arrayDePerfis
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFuncaoPerfil($arrayDePerfis){
		try{
			$fpTO = new FuncaoPerfilTO();
			$fpTO->setId_funcao($arrayDePerfis[0]->id_funcao);
			if(!$this->dao->excluirFuncaoPerfil($fpTO)){
				throw new Zend_Exception('Erro ao deletar perfis antigos');
			}
			foreach ($arrayDePerfis as $perfil){
				$perfil->id_usuariocadastro = $perfil->getSessao()->id_usuario;
				if(!$this->dao->cadastrarFuncaoPerfil($perfil)){
					throw new Zend_Exception('Erro no insert');
				}
			}
			return $this->mensageiro->setMensageiro("Perfil(s) cadastrado(s) com sucesso!",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Perfis para função!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna usuários com função específica.
	 * @param VwFuncaoUsuarioTO $vwfuTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwFuncaoUsuario(VwFuncaoUsuarioTO $vwfuTO){
		try{
			if(!$vwfuTO->getId_entidade()){
				$vwfuTO->setId_entidade($vwfuTO->getSessao()->id_entidade);
			}
			$funcoes = $this->dao->retornarVwFuncaoUsuario($vwfuTO)->toArray();	
			if(empty($funcoes)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Usuário com essa Função Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcoes, new VwFuncaoUsuarioTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Usuários!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
}

?>
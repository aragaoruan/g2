<?php

/**
 * Classe com regras de negócio para interações de Progeto Pedagogico
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 18/10/2010
 *
 * @package models
 * @subpackage bo
 */
class ProjetoPedagogicoBO extends Ead1_BO
{

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    public $arDisciplinasIndisponiveis;

    /**
     * Metodo que retorna o o detalhamento do status pedagogico
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarDetalhamentoStatusPedagogico(MatriculaTO $mTO)
    {
        try {
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            $disciplinas = $this->retornaDisciplinasPendentesAluno($mTO);
            if ($disciplinas) {
                $this->mensageiro->addMensagem($disciplinas);
            }

            if (empty($this->mensageiro->mensagem)) {
                $this->mensageiro->setMensagem('Este Aluno não Possui Pendências Pedagógicas.');
            }
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Detalhamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as séries do projeto pedagógico
     * @param int $idProjetoPedagogico
     */
    public function retornarSerieProjetoPedagogico($idProjetoPedagogico)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarSerieProjetoPedagogico($idProjetoPedagogico);
            if ($dados) {
                $dadosTO = $dados->toArray();
            } else {
                return $this->mensageiro->setMensageiro('Erro ao Retornar Séries!', Ead1_IMensageiro::ERRO, "DAO");
            }
            return $this->mensageiro->setMensageiro($dadosTO, Ead1_IMensageiro::SUCESSO, $idProjetoPedagogico);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Séries!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que verifica se o aluno possui alguma pendencia pedagogica
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro;
     */
    public function retornarStatusPedagogico(MatriculaTO $mTO)
    {
        try {
            $status = true;
            if (!$this->verificaStatusDisciplinasAluno($mTO)) {
                $status = false;
            }
            return $this->mensageiro->setMensageiro(($status ? 'Confirmado' : 'Pendente'), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Status do Aluno!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que verifica se o aluno concluiu todas as disciplinas da matricula
     * @param MatriculaTO $mTO
     * @throws Zend_Exception
     * @return Boolean
     */
    private function verificaStatusDisciplinasAluno(MatriculaTO $mTO)
    {
        try {
            //ID's Fixos
            $ID_EVOLUCAO_MATRICULA_DISCIPLINA_CONCLUIDA = 12;
            //----------------------------
            $matriculaDAO = new MatriculaDAO();
            if (!$mTO->getId_matricula()) {
                THROW new Zend_Exception('O Id_matricula não veio setado!');
            }
            $where = 'id_matricula = ' . $mTO->getId_matricula() . ' AND id_evolucao != ' . $ID_EVOLUCAO_MATRICULA_DISCIPLINA_CONCLUIDA . ' AND bl_obrigatorio=1';
            $arrMatriculaDisciplina = $matriculaDAO->retornarVwMatriculaDisciplina(new VwMatriculaDisciplinaTO(), $where)->toArray();
            return (!empty($arrMatriculaDisciplina) ? false : true);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que retorna as disciplinas pendentes da matricula do aluno
     * @param MatriculaTO $mTO
     * @return array $disciplinas
     */
    private function retornaDisciplinasPendentesAluno(MatriculaTO $mTO)
    {
        try {
            //ID's Fixos
            $ID_EVOLUCAO_MATRICULA_DISCIPLINA_CONCLUIDA = 12;
            //----------------------------
            if (!$mTO->getId_matricula()) {
                THROW new Zend_Exception('O id_matricula não veio setado!');
            }
            $matriculaDAO = new MatriculaDAO();
            $where = 'id_matricula = ' . $mTO->getId_matricula() . ' AND id_evolucao != ' . $ID_EVOLUCAO_MATRICULA_DISCIPLINA_CONCLUIDA . ' AND bl_obrigatorio=1';
            $dados = $matriculaDAO->retornarVwMatriculaDisciplina(new VwMatriculaDisciplinaTO(), $where)->toArray();
            if (empty($dados)) {
                return null;
            }
            $disciplinas = array();
            $disciplinas['label'] = 'Disciplinas';
            $disciplinas['dados'] = Ead1_TO_Dinamico::encapsularTo($dados, new VwMatriculaDisciplinaTO());
            return $disciplinas;
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que salva Informações Sobre o Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function salvarDadosProjetoPedagogico(ProjetoPedagogicoTO $ppTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dao->editarProjetoPedagogico($ppTO);
            $this->mensageiro->setMensageiro('Informações do Projeto Pedagógico Salvas com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Salvar Informações do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que salvar os Meios de Avaliação do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @param array $arrTO (ProjetoTipoProvaTO $ptpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoProjetoPedagogico(ProjetoPedagogicoTO $ppTO, $arrTO = null)
    {
        $dao = new ProjetoPedagogicoDAO();

        /* $regra = $this->regrasAvaliacaoProjetoPedagogicoBasico($ppTO);
          if(is_object($regra)){
          return $regra;
          } */
        if ($arrTO) {
            $this->salvarArrayProjetoTipoProva($arrTO);
        }
        if (!$dao->editarProjetoPedagogico($ppTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Salvar Avaliação!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Avaliação Salva com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que salvar o Contrato e Valores do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoProjetoPedagogico(ProjetoPedagogicoTO $ppTO)
    {
        $dao = new ProjetoPedagogicoDAO();
//		$regra = $this->regrasContratoProjetoPedagogicoBasico($ppTO);
//		if(is_object($regra)){
//			return $regra;
//		}
        $this->salvarArrayProjetoTipoProva();
        if (!$dao->editarProjetoPedagogico($ppTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Salvar Contrato do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Contrato do Projeto Pedagógico Salvo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Edita os dados de contrato de um projeto pedagógico
     * @param ProjetoPedagogicoTO $projPedagogicoTO
     * @return Ead1_Mensageiro
     */
    public function editarContratoProjetoPedagogico(ProjetoPedagogicoTO $projPedagogicoTO)
    {
        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
        $projetoPedagogicoDAO->beginTransaction();
        try {
            if ($projPedagogicoTO->getId_projetopedagogico() == 0) {
                throw new DomainException('É necessário informar o projeto pedagógico para atualizar os dados de contrato');
            }
            if ($projPedagogicoTO->getId_projetocontratoduracaotipo() == ProjetoContratoDuracaoTipoTO::DURACAO_MESES) {
                if (!$projPedagogicoTO->getNu_contratoduracao()) {
                    throw new DomainException('É necessário informar a quantidade de meses de duração do contrato');
                }
            }
            if (!$projPedagogicoTO->getNu_mesesmulta()) {
                throw new DomainException('É necessario informar a quantidade de meses de multa');
            }
            if (!$projPedagogicoTO->getNu_contratomultavalor()) {
                throw new DomainException('É necessario informar o valor da multa');
            }
            $projPedagogicoContratoTO = new ProjetoPedagogicoTO();
            $projPedagogicoContratoTO->setId_projetopedagogico($projPedagogicoTO->getId_projetopedagogico());
            $projPedagogicoContratoTO->setId_projetocontratoduracaotipo($projPedagogicoTO->getId_projetocontratoduracaotipo());
            $projPedagogicoContratoTO->setId_projetocontratomultatipo($projPedagogicoTO->getId_projetocontratomultatipo());
            $projPedagogicoContratoTO->setNu_contratoduracao($projPedagogicoTO->getNu_contratoduracao());
            $projPedagogicoContratoTO->setBl_renovarcontrato($projPedagogicoTO->getBl_renovarcontrato());
            $projPedagogicoContratoTO->setNu_mesesmulta($projPedagogicoTO->getNu_mesesmulta());
            $projPedagogicoContratoTO->setBl_proporcaopormeses($projPedagogicoTO->getBl_proporcaopormeses());
            $projPedagogicoContratoTO->setNu_contratomultavalor($projPedagogicoTO->getNu_contratomultavalor());
            //Grupo Valores
            $projPedagogicoContratoTO->setNu_mensalidade($projPedagogicoTO->getNu_mensalidade());
            $projPedagogicoContratoTO->setBl_mensalidadecredito($projPedagogicoTO->getBl_mensalidadecredito());
            $projPedagogicoContratoTO->setNu_mensalidadecredito($projPedagogicoTO->getNu_mensalidadecredito());
            $projPedagogicoContratoTO->setNu_valorprojetopedagogico($projPedagogicoTO->getNu_valorprojetopedagogico());
            $projPedagogicoContratoTO->setId_projetopedagogicovalortipo($projPedagogicoTO->getId_projetopedagogicovalortipo());

            if ($projetoPedagogicoDAO->editarProjetoPedagogico($projPedagogicoContratoTO) === false) {
                throw new DomainException('Erro ao atualizar o projeto pedagógico com os dados de contrato. ' . $projetoPedagogicoDAO->excecao);
            }

            $projetoPedagogicoDAO->commit();
            return new Ead1_Mensageiro('Dados do contrato salvo com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {

            $projetoPedagogicoDAO->rollBack();
            return new Ead1_Mensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Serie e Nivel de Ensino do Porjeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $orm = new ProjetoPedagogicoSerieNivelEnsinoORM();
        if ($orm->consulta($ppsneTO, true)) {
            return $this->deletarProjetoPedagogicoSerieNivelEnsino($ppsneTO);
        }
        return $this->cadastrarProjetoPedagogicoSerieNivelEnsino($ppsneTO);
    }

    /**
     * Metodo que Cadastra e Exclui os vinculos do ProjetoPedagogico com a Serie e Nivel de Ensino
     * @param array ( ProjetoPedagogicoSerieNivelEnsinoTO $arrTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayProjetoPedagogicoSerieNivelEnsino($arrTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $delected = false;
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $ppsneTO) {
                if (!$delected) {
                    $delected = true;
                    $delppsneTO = new ProjetoPedagogicoSerieNivelEnsinoTO();
                    $delppsneTO->setId_projetopedagogico($ppsneTO->getId_projetopedagogico());
                    if (!$dao->deletarProjetoPedagogicoSerieNivelEnsino($delppsneTO)) {
                        Throw new Zend_Exception('Erro ao Excluir Vinculos da Série Nivel Ensino com o Projeto Pedagógico!');
                    }
                }
                if (!($ppsneTO instanceof ProjetoPedagogicoSerieNivelEnsinoTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe ProjetoPedagogicoSerieNivelEnsinoTO!');
                }
                if (!$dao->cadastrarProjetoPedagogicoSerieNivelEnsino($ppsneTO)) {
                    Throw new Zend_Exception('Erro ao Vincular Projeto Pedagógico com a Série e Nivel de Ensino!');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Area de Conhecimento do Porjeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function salvarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $orm = new AreaProjetoPedagogicoORM();
//		if($orm->consulta($appTO,true)){
//			return $this->deletarAreaProjetoPedagogico($appTO);
//		}
        return $this->cadastrarAreaProjetoPedagogico($appTO);
    }

    /**
     * Metodo que decide se Cadastra ou Edita o Projeto Extracurricular
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $orm = new ProjetoExtracurricularORM();
        if ($orm->consulta($peTO, true)) {
            return $this->editarProjetoExtracurricular($peTO);
        }
        return $this->cadastrarProjetoExtracurricular($peTO);
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Serie e Nivel de Ensino do Modulo do Porjeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloSerieNivelEnsinoTO $msneTO)
    {
        $orm = new ModuloSerieNivelEnsinoORM();
        if ($orm->consulta($msneTO)) {
            return $this->deletarModuloSerieNivelEnsino($msneTO);
        }
        return $this->cadastrarModuloSerieNivelEnsino($ppsneTO, $msneTO);
    }

    /**
     * Metodo que cadastra e exclui o vinculo do modulo com a serie e nivel de ensino
     * Itera o array de TO
     *
     * @param array ModuloSerieNivelEnsinoTO $arrTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayModuloSerieNivelEnsino($arrTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $moduloSerieNivelEnsinoORM = new ModuloSerieNivelEnsinoORM();
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $msneTO) {
                if (!($msneTO instanceof ModuloSerieNivelEnsinoTO)) {
                    throw new Zend_Exception('Objeto não é uma Instancia da Classe ModuloSerieNivelEnsinoTO!');
                }
                if ($moduloSerieNivelEnsinoORM->consulta($msneTO) === false) {
                    if (!$dao->cadastrarModuloSerieNivelEnsino($msneTO)) {
                        throw new Zend_Exception('Erro ao Vincular Módulo com a Série e Nivel de Ensino!');
                    }
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Disciplina do Modulo
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloDisciplina(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloDisciplinaTO $mdTO)
    {
        $orm = new ModuloDisciplinaORM();
        if ($orm->consulta($mdTO)) {
            return $this->deletarDisciplinaModulo($mdTO);
        }
        return $this->cadastrarModuloDisciplina($ppsneTO, $mdTO);
    }

    /**
     * Metodo que Cadastra e Exclui os vinculos do Módulo com a Disciplina
     * @param array ( ModuloDisciplinaTO $arrTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayModuloDisciplina($arrTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $dao->beginTransaction();
        try {
            $delected = false;
            foreach ($arrTO as $mdTO) {
                if (!$delected) {
                    $delected = true;
                    $delTO = new ModuloDisciplinaTO();
                    $delTO->setId_modulo($mdTO->getId_modulo());
                    $delTO->setBl_ativo(0);
                    if (!$dao->deletarDisciplinaModulo($delTO)) {
                        THROW new Zend_Exception('Erro ao Excluir Vinculos do Módulo com as Disciplinas!');
                    }
                }
                if (!($mdTO instanceof ModuloDisciplinaTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe ModuloDisciplinaTO!');
                }
                $consultaTO = null;
                $arrConsulta = null;
                $resposta = null;
                $arrConsulta[0] = $mdTO->toArray();
                $consultaTO = Ead1_TO_Dinamico::encapsularTo($arrConsulta, new ModuloDisciplinaTO(), true);
                $consultaTO->setNu_ordem(null);
                $resposta = $dao->retornarModuloDisciplina($consultaTO);
                if (is_object($resposta)) {
                    $resposta = $resposta->toArray();
                    if (empty($resposta)) {
                        if (!$dao->cadastrarModuloDisciplina($mdTO)) {
                            THROW new Zend_Exception('Erro ao Vincular Disciplinas com o Módulo!');
                        }
                    } else {
                        if (!$dao->deletarDisciplinaModulo($mdTO)) {
                            THROW new Zend_Exception('Erro ao Excluir Vinculos das Disciplinas com o Módulo!');
                        }
                    }
                } else {
                    THROW new Zend_Exception('Erro ao Verificar Módulos e Disciplinas');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que decide se cadastra ou exclui o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProva $ptpTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $orm = new ProjetoTipoProvaORM();
        if ($orm->consulta($ptpTO, true)) {
            return $this->editarProjetoTipoProva($ptpTO);
        }
        return $this->cadastrarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $orm = new ProjetoRecuperacaoORM();
        if ($orm->consulta($prTO, true)) {
            return $this->editarProjetoRecuperacao($prTO);
        }
        return $this->cadastrarProjetoRecuperacao($prTO);
    }

    /**
     * Metodo que salva as recuperações do projeto pedagogico e os tipos de prova
     * @param ProjetoRecuperacaoTO $prTO
     * @param Array (RecuperacaoTipoProvaTO $rtpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoRecuperacaoTipoProva(ProjetoRecuperacaoTO $prTO, $arrTO = null)
    {
        $orm = new ProjetoRecuperacaoORM();
        $dao = new ProjetoPedagogicoDAO();

        if ($orm->consulta($prTO, true)) {
            if (!$dao->editarProjetoRecuperacao($prTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Editar Recuperações!', Ead1_IMensageiro::ERRO, $dao->excecao);
            }
        } else {
            if (!$dao->cadastrarProjetoRecuperacao($prTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Cadastrar Recuperações!', Ead1_IMensageiro::ERRO, $dao->excecao);
            }
        }
        if (!$arrTO) {
            return $this->mensageiro->setMensageiro('Recuperação Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } else {
            return $this->salvarArrayRecuperacaoTipoProva($arrTO);
        }
    }

    /**
     * Metodo que cadastra o tipo de prova de recuperação do projeto pedagógico
     * @param Array (RecuperacaoTipoProvaTO $rtpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayRecuperacaoTipoProva($arrTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $delected = false;
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $rtpTO) {
                if (!($rtpTO instanceof RecuperacaoTipoProvaTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe RecuperacaoTipoProvaTO!');
                }
                if (!$delected) {
                    $delected = true;
                    $delrtpTO = new RecuperacaoTipoProvaTO();
                    $delrtpTO->setId_tiporecuperacao($rtpTO->getId_tiporecuperacao());
                    $delrtpTO->setId_projetopedagogico($rtpTO->getId_projetopedagogico());
                    if (!$dao->deletarRecuperacaoTipoProva($delrtpTO)) {
                        Throw new Zend_Exception('Erro ao Excluir Vinculos do Projeto Pedagógico com o Tipo de Prova!');
                    }
                }
                if (!$dao->cadastrarProjetoRecuperacaoTipoProva($rtpTO)) {
                    Throw new Zend_Exception('Erro ao Vincular Projeto Pedagógico com o Tipo de Prova!');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que cadastra e exclui os vinculos do projeto com os tipos de prova
     * @param array ( ProjetoTipoProvaTO $ptpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayProjetoTipoProva($arrTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $delected = false;
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $ptpTO) {
                $newptpTO = new ProjetoTipoProvaTO();

                $newptpTO->setId_tipoprova($ptpTO->getId_tipoprova());
                $newptpTO->setId_projetopedagogico($ptpTO->getId_projetopedagogico());
                $newptpTO->setNu_taxa($ptpTO->getNu_taxa());
                if (!($newptpTO instanceof ProjetoTipoProvaTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe ProjetoTipoProvaTO!');
                }
                if (!$delected) {
                    $delected = true;
                    $delptpTO = new ProjetoTipoProvaTO();
                    $delptpTO->setId_projetopedagogico($newptpTO->getId_projetopedagogico());
                    if (!$dao->deletarProjetoTipoProva($delptpTO)) {
                        Throw new Zend_Exception('Erro ao Excluir Vinculos do Projeto Pedagógico com o Tipo de Prova!');
                    }
                }
                if (!$dao->cadastrarProjetoTipoProva($newptpTO)) {
                    Throw new Zend_Exception('Erro ao Vincular Projeto Pedagógico com o Tipo de Prova!');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Cadastra e Exclui o vinculo da Area com as Disciplinas
     * @param array (AreaConhecimentoTO $acTO)
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayAreaProjetoPedagogico($arrTO, AreaProjetoPedagogicoTO $appTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $dao->beginTransaction();
        try {
            if (!$dao->deletarAreaProjetoPedagogico($appTO)) {
                Throw new Zend_Exception('Erro ao Excluir Vinculos do Projeto Pedagógico com a Area de Conhecimento!');
            }
            foreach ($arrTO as $acTO) {
                if (!($acTO instanceof AreaConhecimentoTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe AreaConhecimentoTO!');
                }
                $appTO->setId_areaconhecimento($acTO->getId_areaconhecimento());
                if (!$dao->cadastrarAreaProjetoPedagogico($appTO)) {
                    Throw new Zend_Exception('Erro ao Vincular Area de Conhecimento com o Projeto Pedagógico!');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Salva / edita um vínculo entre módulo e área de conhecimento
     * @param ModuloAreaTO $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloArea(ModuloAreaTO $moduloAreaTO)
    {

        if ($moduloAreaTO->getId_moduloarea() != 0) {
            return $this->editarModuloArea($moduloAreaTO);
        }

        return $this->cadastrarModuloArea($moduloAreaTO);
    }

    /**
     * Cadastra um vínculo entre módulo e área de conhecimento
     * @param ModuloAreaTO $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModuloArea(ModuloAreaTO $moduloAreaTO)
    {

        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
        $projetoPedagogicoDAO->beginTransaction();

        try {

            $idAreaConhecimento = $moduloAreaTO->getId_areaconhecimento();
            $idModulo = $moduloAreaTO->getId_modulo();

            if ($projetoPedagogicoDAO->retornarModuloArea($moduloAreaTO)->count() > 0) {
                throw new DomainException('Vínculo já existente entre o módulo e a área de conhecimento');
            }

            $idModuloArea = $projetoPedagogicoDAO->cadastrarModuloArea($moduloAreaTO);

            if ($idModuloArea === false) {
                throw new DomainException('Não foi possível salvar o vínculo entre área de conhecimento e o módulo');
            }

            $moduloAreaTO->setId_moduloarea($idModuloArea);
            $projetoPedagogicoDAO->commit();

            return $this->mensageiro->setMensageiro($moduloAreaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {

            $projetoPedagogicoDAO->rollBack();
            return $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Cadastra Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, TrilhaTO $tTO = null)
    {
        //ID's Fixos
        $SITUACAO_PROJETO_ENTIDADE_ATIVO = 51;
        //--------------
        //Objeto temporário para pegar somente o nome do TO passado como parâmetro.
        $tempPpTO = new ProjetoPedagogicoTO();
        $tempPpTO->setSt_projetopedagogico($ppTO->getSt_projetopedagogico());
        $tempPpTO->setId_entidadecadastro($ppTO->getSessao()->id_entidade);
        $tempPpTO->setBl_ativo(true);

        $dao = new ProjetoPedagogicoDAO();


        //Verifica se o projeto pedagógico que está sendo criado já possui duplicidade de nomes.
//        $projetoCadastrado = $dao->retornarProjetoPedagogico($tempPpTO)->toArray();

        //   $projetoCadastrado = new \G2\Negocio\ProjetoPedagogico();
        //   $result = $projetoCadastrado->verificaNomeProjetoPedagogico($tempPpTO->toArray());
        //
//
//        if ($result) {
//            return $this->mensageiro->setMensageiro('Não é possível adicionar dois projetos com o mesmo nome.', Ead1_IMensageiro::AVISO);
//        }

        //Fluxo principal do método
        $ppTO->setId_entidadecadastro($ppTO->getSessao()->id_entidade);
        $ppTO->setId_usuariocadastro($ppTO->getSessao()->id_usuario);
        $regra = $this->regrasProjetoPedagogicoBasico($ppTO);
        if (is_object($regra)) {
            return $regra;
        }
        if ($tTO && !$tTO->getId_tipotrilha()) {
            return $this->mensageiro->setMensageiro('É Necessario Escolher o Tipo da Trilha!', Ead1_IMensageiro::AVISO);
        }
        $dao->beginTransaction();
        try {

            if ($tTO) {
                $id_trilha = $dao->cadastrarTrilha($tTO);
                if (!$id_trilha) {
                    throw new Zend_Exception('Erro ao Cadastrar Trilha');
                }
                $ppTO->setId_trilha($id_trilha);
            }
            $id_projetoPedagogico = $dao->cadastrarProjetoPedagogico($ppTO);
            if (!$id_projetoPedagogico) {
                throw new Zend_Exception('Erro ao Cadastrar Projeto Pedagógico!');
            }
            $ppTO->setId_projetopedagogico($id_projetoPedagogico);
            $ppTO->setId_projetopedagogicoorigem($id_projetoPedagogico);
            if (!$dao->editarProjetoPedagogico($ppTO)) {
                throw new Zend_Exception('Erro ao Cadastrar Origem do Projeto Pedagógico!');
            }
            $peTO = new ProjetoEntidadeTO();
            $peTO->setId_projetopedagogico($ppTO->getId_projetopedagogico());
            $peTO->setBl_ativo(true);
            $peTO->setId_entidade($peTO->getSessao()->id_entidade);
            $peTO->setId_usuariocadastro($peTO->getSessao()->id_usuario);
            $peTO->setDt_inicio(date('Y-m-d'));
            $peTO->setDt_cadastro(date('Y-m-d'));
            $peTO->setId_situacao($SITUACAO_PROJETO_ENTIDADE_ATIVO);
            if (!$dao->cadastrarProjetoEntidade($peTO)) {
                throw new Zend_Exception('Erro ao Cadastrar Projeto Pedagógico da Instituição!');
            }
            $dao->commit();
            $this->saveLog($ppTO, 'tb_projetopedagogico', 1683537081, 'nu_semestre');
            return $this->mensageiro->setMensageiro($ppTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     * Metodo que Cadastra Serie e Nivel de Ensino do Projeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarProjetoPedagogicoSerieNivelEnsino($ppsneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Serie e Nivel de Ensino do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Serie e Nivel de Ensino Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que vincula Area ao Projeto Pedagogico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarAreaProjetoPedagogico($appTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Area de Conhecimento ao Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Area de Conhecimento Vinculada ao Projeto Pedagógico com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que vincula Disciplina Extracurricular ao Projeto Pedagógico
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarProjetoExtracurricular($peTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Disciplina Extracurricular ao Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Disciplina Extracurricular Vinculada ao Projeto Pedagógico com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra Trilha
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTrilha(TrilhaTO $tTO, $idProjetoPedagogico)
    {
        $dao = new ProjetoPedagogicoDAO();
        $dao->beginTransaction();

        try {
            $idTrilha = $dao->cadastrarTrilha($tTO);
            $projetoPedagogicoTO = new ProjetoPedagogicoTO();
            $projetoPedagogicoTO->setId_projetopedagogico($idProjetoPedagogico);
            $projetoPedagogicoTO->setId_trilha($idTrilha);

            if ($dao->editarProjetoPedagogico($projetoPedagogicoTO) === false) {
                throw new DomainException($dao->excecao);
            }

            $tTO->setId_trilha($idTrilha);
            $dao->commit();
            return $this->mensageiro->setMensageiro($tTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro($tTO, Ead1_IMensageiro::ERRO);
        }


        return $this->mensageiro->setMensageiro($tTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra Modulo do Projeto Pedagógico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModulo(ModuloTO $mTO)
    {
        if ($mTO->getId_situacao() == false) {
            $sit = new SituacaoTO();
            $sit->setSt_tabela('tb_modulo');
            $sit->setSt_situacao('Ativo');
            $situacoes = $sit->fetch(false, true);
            $mTO->setId_situacao($situacoes->getid_situacao());
        }

        $dao = new ProjetoPedagogicoDAO();
        $id_modulo = $dao->cadastrarModulo($mTO);
        if (!$id_modulo) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Módulo', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        $mTO->setId_modulo($id_modulo);
        return $this->mensageiro->setMensageiro($mTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra Serie e Nivel de Ensino do Modulo
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModuloSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloSerieNivelEnsinoTO $msneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $msneTO->setId_nivelensino($ppsneTO->getId_nivelensino());
        if (!$dao->cadastrarModuloSerieNivelEnsino($msneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Serie e Nivel de Ensino do Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro($msneTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que vincula o Modulo as Disciplinas
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModuloDisciplina(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloDisciplinaTO $mdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $mdTO->setId_nivelensino($ppsneTO->getId_nivelensino());
        if (!$dao->cadastrarModuloDisciplina($mdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Disciplina ao Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Disciplina Vinculada ao Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Faz o mesmo que o médoto cadastrarModuloDisciplina, mas sem a necessidade do objeto ProjetoPedagogicoSerieNivelEnsinoTO
     * @param ModuloDisciplinaTO $mdTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function inserirModuloDisciplina(ModuloDisciplinaTO $mdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarModuloDisciplina($mdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Disciplina ao Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        $depois = '{"nu_obrigatorioalocacao": "' . $mdTO->getNu_obrigatorioalocacao() . '", "nu_disponivelapartirdo":"' . $mdTO->getNu_disponivelapartirdo() . '", "nu_percentualsemestre" : "' . $mdTO->getNu_percentualsemestre() . '"}';
        $this->saveLog($mdTO, 'tb_modulodisciplina', 1716917188, null, null, $depois);

        return $this->mensageiro->setMensageiro('Disciplina Vinculada ao Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Editar o Vínculo da Disciplina ao Módulo
     * @param ModuloDisciplinaTO $mdTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function editarModuloDisciplina(ModuloDisciplinaTO $mdTO)
    {
        $aux = new ModuloDisciplinaTO();
        $aux->setId_modulodisciplina($mdTO->getId_modulodisciplina());
        $aux->fetch(true, true, true);
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarModuloDisciplina($mdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Vínculo da Disciplina ao Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }

        $antes = '{"nu_obrigatorioalocacao": "' . $aux->getNu_obrigatorioalocacao() . '", "nu_disponivelapartirdo":"' . $aux->getNu_disponivelapartirdo() . '", "nu_percentualsemestre" : "' . $aux->getNu_percentualsemestre() . '"}';
        $depois = '{"nu_obrigatorioalocacao": "' . $mdTO->getNu_obrigatorioalocacao() . '", "nu_disponivelapartirdo":"' . $mdTO->getNu_disponivelapartirdo() . '", "nu_percentualsemestre" : "' . $mdTO->getNu_percentualsemestre() . '"}';
        $this->saveLog($mdTO, 'tb_modulodisciplina', 1716917188, null, $antes, $depois);
        return $this->mensageiro->setMensageiro('Disciplina Vinculada ao Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra as trilhas na disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {

        try {
            $dao = new ProjetoPedagogicoDAO();

            if ($tdTO->getId_disciplina() == $tdTO->getId_disciplinaprerequisito()) {
                throw new Zend_Exception('A disciplina não pode ser pré - requisito da própria disciplina.');
            }

            if (!$dao->cadastrarTrilhaDisciplina($tdTO)) {
                throw new Zend_Exception('Erro ao salvar disciplina da trilha fixa.');
            }

            return $this->mensageiro->setMensageiro($tdTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Cadastrar Pré-Requisito da Disciplina', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Cadastra (vincula) as disciplinas na trilha de um projeto pedagógico
     * @param array $arTrilhaDisciplinaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDisciplinasTrilhaProjetoPedagogico($arTrilhaDisciplinaTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $dao->beginTransaction();

        try {

            if (count($arTrilhaDisciplinaTO) == 0) {
                throw new DomainException('Informe ao menos uma disciplina para vincular a trilha');
            }

            $trilhaDisciplinaTO = new TrilhaDisciplinaTO();
            $trilhaDisciplinaTO->setId_trilha($arTrilhaDisciplinaTO[0]->getId_trilha());

            $dao->deletarTrilhaDisciplina($trilhaDisciplinaTO);

            foreach ($arTrilhaDisciplinaTO as $trilhaDisciplinaTO) {
                $mensageiro = $this->cadastrarTrilhaDisciplina($trilhaDisciplinaTO);

                if ($mensageiro->getTipo() == Ead1_IMensageiro::ERRO) {
                    $mensagem = $mensageiro->getMensagem();
                    throw new DomainException($mensagem[0]);
                }
            }

            $dao->commit();
            return $this->mensageiro->setMensageiro('Disciplinas vinculadas a trilha com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que cadastra tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProva $ptpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarProjetoTipoProva($ptpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Tipo da Prova de Recuperação!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Tipo da Prova de Recuperação Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->cadastrarProjetoRecuperacao($prTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Recuperação do Projeto Pedagogico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Recuperação do Projeto Pedagogico Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita os Dados da Trilha do Projeto Pedagógico
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function editarDadosTrilha(TrilhaTO $tTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarDadosTrilha($tTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Trilha!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Trilha Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita o Tipo da Trilha do Projeto Pedagogico
     * @todo Este metodo exclui todos os vinculos da TrilhaDisciplina ao editar!!!
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function editarTipoTrilha(TrilhaTO $tTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if ($this->verificarTrilha($tTO)) {
            if (!$dao->deletarVinculosTrilhaDisciplina($tTO)) {
                return $this->mensageiro->setMensageiro('Erro ao Excluir Trilha', Ead1_IMensageiro::ERRO, $dao->excecao);
            }
        }
        if (!$dao->editarDadosTrilha($tTO, true)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Trilha!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Trilha Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProva $ptpTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarProjetoTipoProva($ptpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Tipo da Prova do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Tipo da Prova do Projeto Pedagógico Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarProjetoRecuperacao($prTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Recuperação do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Recuperação do Projeto Pedagógico Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita Projeto Pedagógicp
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoPedagogico(ProjetoPedagogicoTO $ppTO)
    {
        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
        $projetoPedagogicoDAO->beginTransaction();

        try {

            //Objeto temporário para pegar somente o nome do TO passado como parâmetro.
            $tempPpTO = new ProjetoPedagogicoTO();
            $tempPpTO->setId_projetopedagogico($ppTO->getId_projetopedagogico());
            $tempPpTO->setSt_projetopedagogico($ppTO->getSt_projetopedagogico());
            $tempPpTO->setId_entidadecadastro($ppTO->getSessao()->id_entidade);

            $dao = new ProjetoPedagogicoDAO();

            //verifica se o projeto cadastrado já tem grade montada
            $bl_criadisciplinas = false;
            $auxilarTO = new ProjetoPedagogicoTO();
            $auxilarTO->setId_projetopedagogico($ppTO->getId_projetopedagogico());
            $auxilarTO->fetch(true, true, true);

            $antes = '{"nu_semestre":"' . $auxilarTO->getNu_semestre() . '"}';
            if (!$auxilarTO->getBl_gradepronta()) {

                $bl_criadisciplinas = true;
                $ppTO->setId_usuariograde($ppTO->getSessao()->id_usuario);
                $ppTO->setDt_criagrade(new Zend_Date());
            }

            $this->regrasProjetoPedagogicoBasico($ppTO);

            if (!$projetoPedagogicoDAO->editarProjetoPedagogico($ppTO)) {
                throw new Exception('Não foi possível editar o projeto pedagógico');
            }

            $ppTO->fetch(true, true, true);
            $this->mensageiro->setMensageiro('Projeto pedagógico editado com sucesso', Ead1_IMensageiro::SUCESSO);
            $projetoPedagogicoDAO->commit();

            $this->saveLog($ppTO, 'tb_projetopedagogico', 1683537081, 'nu_semestre', $antes);

            if ($bl_criadisciplinas) {
                $this->cadastraGradeCurricular($ppTO);
            }

        } catch (DomainException $domainExc) {
            $this->mensageiro->setMensageiro($domainExc->getMessage(), Ead1_IMensageiro::AVISO);
            $projetoPedagogicoDAO->rollBack();
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao editar o  projeto pedagógico', Ead1_IMensageiro::ERRO, $e->getMessage());
            $projetoPedagogicoDAO->rollBack();
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra Modulo do Projeto Pedagógico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function editarModulo(ModuloTO $mTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarModulo($mTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Módulo Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita a Trilha da Disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function editarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $mdTO = new ModuloDisciplinaTO();
        if ($tdTO->getId_disciplina() == $tdTO->getId_disciplinaprerequisito()) {
            return $this->mensageiro->setMensageiro('A Disciplina não pode ser Requisito da Mesma!', Ead1_IMensageiro::AVISO);
        }
        if ($tdTO->getId_disciplinaprerequisito()) {
            $resposta = $this->verificarDisciplinaRequisito($mdTO, $tdTO);
            if (!$resposta) {
                return $this->mensageiro->setMensageiro('Não é Possivel Criar este Vinculo!', Ead1_IMensageiro::AVISO);
            }
            if (is_object($resposta)) {
                return $resposta;
            }
        }
        if (!$dao->editarTrilhaDisciplina($tdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Trilha da Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Trilha da Disciplina Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    public function editarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->editarProjetoExtracurricular($peTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Atualizar Disciplina Extracurricular!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Disciplina Extracurricular Atualizada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Edita um vínculo entre o módulo e a área de conhecimento
     * @param ModuloAreaTO $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function editarModuloArea(ModuloAreaTO $moduloAreaTO)
    {

        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
        $projetoPedagogicoDAO->beginTransaction();

        try {

            if ($moduloAreaTO->getId_moduloarea() == 0) {
                throw new DomainException('Informe o id_moduloarea para apagar o vínculo entre módulo e área de conhecimento');
            }

            if ($projetoPedagogicoDAO->editarModuloArea($moduloAreaTO) === false) {
                throw new DomainException('Não foi possível atualizar o vínculo entre módulo e área de conhecimento');
            }

            $projetoPedagogicoDAO->commit();
            return $this->mensageiro->setMensageiro($moduloAreaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $projetoPedagogicoDAO->rollBack();
            return $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que Exclui logicamente o Projeto Pedagógico
     * @todo verificar regras de negócio ao remover o projeto pedagógico
     * @param EntidadeTO $eTO
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoPedagogico(ProjetoPedagogicoTO $projetoPedagogicoTO)
    {
        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();

        $projetoPedagogicoDAO->beginTransaction();
        try {

            if (empty($projetoPedagogicoTO->id_projetopedagogico)) {
                throw new DomainException('Para remover um projeto pedagógico, informe o seu código');
            }

            $projPedagogicoTO = new ProjetoPedagogicoTO();
            $projPedagogicoTO->id_projetopedagogico = $projetoPedagogicoTO->id_projetopedagogico;
            $projPedagogicoTO->bl_ativo = 0;

            if ($projetoPedagogicoDAO->editarProjetoPedagogico($projPedagogicoTO) === false) {
                throw new DomainException('Erro ao excluir um projeto pedagógico. Erro: ' . $projetoPedagogicoDAO->excecao);
            }

            $projetoPedagogicoDAO->commit();
            $this->mensageiro->setMensageiro('Projeto pedagógico removido com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $projetoPedagogicoDAO->rollBack();
            $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;

        $ppTO->setId_entidadecadastro($eTO->getId_entidade());
        if (!$dao->editarProjetoPedagogico($ppTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Projeto Pedagógico Deletado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui Area do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function deletarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $eTO = new EntidadeTO();
        if ($this->verificarAreaProjetoPedagogico($appTO)) {
            return $this->mensageiro->setMensageiro('É Necessario Excluir as Diciplinas Desta Area nos Módulos', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->deletarAreaProjetoPedagogico($appTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Area do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Serie de Ensino Deletado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui Serie e Nivel de Ensino do Projeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if ($this->verificarSerieDisciplinaModulo($ppsneTO)) {
            return $this->mensageiro->setMensageiro('É Necessario Excluir as Disciplinas que são Desta Serie nos Módulos!', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->deletarProjetoPedagogicoSerieNivelEnsino($ppsneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Serie de Ensino do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Serie de Ensino Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui a Disciplina do Modulo
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaModulo(ModuloDisciplinaTO $mdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if ($this->verificarDependenciaDisciplinaModulo($mdTO)) {
            return $this->mensageiro->setMensageiro('É Necessario Excluir as Dependencias da Disciplina!', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->deletarDisciplinaModulo($mdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Disciplina do Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Disciplina Excluida do Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProva $ptpTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->deletarProjetoTipoProva($ptpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Exluir o Tipo da Prova do Projeto Pedagogico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Tipo da Prova do Projeto Pedagogico Excluida do Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que exclui Serie do Modulo
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function deletarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if ($this->verificarModuloSerieDisciplina($msneTO)) {
            return $this->mensageiro->setMensageiro('É Necessario Excluir as Disciplinas desta Serie no Módulo!', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->deletarModuloSerieNivelEnsino($msneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Serie do Módulo!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Serie Excluida do Módulo com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui um projeto Extracurricular
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->deletarProjetoExtracurricular($peTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir o Projeto Extracurricular!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Projeto Extracurricular deletado com sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui Disciplina pré-requisito da Trilha
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function deletarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->deletarTrilhaDisciplina($tdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Disciplina pré-requisito da Trilha!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Disciplina pré-requisito da Trilha Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (!$dao->deletarProjetoRecuperacao($prTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Recuperação do Projeto Pedagogico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Recuperação do Projeto Pedagogico Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Deleta um vínculo entre um módulo e área de conhecimento
     * @param ModuloAreaTO $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function deletarModuloArea(ModuloAreaTO $moduloAreaTO)
    {

        $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
        $projetoPedagogicoDAO->beginTransaction();

        try {

            if ($moduloAreaTO->getId_moduloarea() == 0) {
                throw new DomainException('Informe o id_moduloarea para apagar o vínculo entre módulo e área de conhecimento');
            }

            if ($projetoPedagogicoDAO->deletarModuloArea($moduloAreaTO) === false) {
                throw new DomainException('Não foi possível deletar o vínculo entre o módulo e a área de conhecimento');
            }

            $projetoPedagogicoDAO->commit();
            return $this->mensageiro->setMensageiro('Vínculo entre módulo e área de conhecimento apagado com sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $projetoPedagogicoDAO->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que contem regras basicas do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro | boolean false
     */
    public function regrasProjetoPedagogicoBasico(ProjetoPedagogicoTO $ppTO)
    {
        if (!is_null($ppTO->getId_projetopedagogico()) && $ppTO->getSt_projetopedagogico() == '') {
            throw new DomainException('O título do projeto pedagógico é obrigatório.');
        }
        if (!is_null($ppTO->getId_projetopedagogico()) && $ppTO->getNu_cargahoraria() == '') {
            throw new DomainException('A carga horária do projeto pedagógico é obrigatório.');
        }

        if (!is_null($ppTO->getId_projetopedagogico()) && (is_null($ppTO->getId_situacao()) || $ppTO->getId_situacao() == 0)) {
            throw new DomainException('A situação do projeto pedagógico é obrigatório');
        }

        return true;
    }

    /**
     * Metodo que contem as regras de Cadastro de Contrato e Valores do Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro | boolean false
     */
    public function regrasContratoProjetoPedagogicoBasico(ProjetoPedagogicoTO $ppTO)
    {
        $erro = '';
        if ($ppTO->getId_projetocontratoduracaotipo() == 1) { //simulando que o Id_projetocontratoduracaotipo é o de Duraçao Meses
            if (!$ppTO->getNu_contratoduracao()) {
                $erro = 1;
                $this->mensageiro->addMensagem('É necessario informar a Quantidade de Meses de Duração do Contrato!');
            }
        }
        if (!$ppTO->getNu_mesesmulta()) {
            $erro = 1;
            $this->mensageiro->addMensagem('É necessario Informar a Quantidade de Meses de Multa!');
        }
        if (!$ppTO->getNu_contratomultavalor()) {
            $erro = 1;
            $this->mensageiro->addMensagem('É necessario Informar Valor da Multa!');
        }
        if (!$ppTO->getNu_valorprojetopedagogico()) {
            $erro = 1;
            $this->mensageiro->addMensagem('É necessario Informar Valor da Multa!');
        }
        if (!$ppTO->getNu_valoravaliacaoadicional()) {
            $erro = 1;
            $this->mensageiro->addMensagem('É necessario Informar Valor da Avaliação Adicional!');
        }
        if (!$ppTO->getNu_valorrecuperacao()) {
            $erro = 1;
            $this->mensageiro->addMensagem('É necessario Informar Valor da Recuperação!');
        }
        if ($erro) {
            $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
            return $this->mensageiro;
        }
        return false;
    }

    /**
     * Metodo que contem regras de Avaliação do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro | boolean false
     */
    public function regrasAvaliacaoProjetoPedagogicoBasico(ProjetoPedagogicoTO $ppTO)
    {
        $errogeral = '';
        if ($ppTO->getBl_recuperacaosomatorio()) {
            $erro2 = '';
            if (!$ppTO->getNu_recuperacaosomatoriopercentual()) {
                $this->mensageiro->addMensagem('É necessario Informar o Valor Percentual para Recuperação!');
                $erro2 = 1;
            }
            if (!$ppTO->getNu_valorrecuperacao()) {
                $this->mensageiro->addMensagem('É necessario Informar o Valor da Recuperação!');
                $erro2 = 1;
            }
            if ($erro2) {
                $errogeral = 1;
            }
        }
        if ($ppTO->getBl_recuperacaosubparcial()) {
            $erro3 = '';
            if (!$ppTO->getNu_recuperacaosubparcialmaxaplicacoes()) {
                $this->mensageiro->addMensagem('É necessario Informar o Máximo de Aplicações!');
                $erro3 = 1;
            }
            if (!$ppTO->getNu_recuperacaosubparcialmaxaprov()) {
                $this->mensageiro->addMensagem('É necessario Informar o Máximo de Aproveitamento da Nota!');
                $erro3 = 1;
            }
            if (!$ppTO->getNu_recuperacaosubparcialpercentual()) {
                $this->mensageiro->addMensagem('É necessario Informar o Percentual para Recuperação!');
                $erro3 = 1;
            }
            if (!$ppTO->getBl_recuperacaosubparcialdisciplina() && !$ppTO->getBl_recuperacaosubparcialfinal()) {
                $this->mensageiro->addMensagem('É necessario Informar Para Qual Avaliação!');
                $erro3 = 1;
            }
            if ($erro3) {
                $errogeral = 1;
            }
        }
        if ($ppTO->getBl_recuperacaosubtotal()) {
            $erro4 = '';
            if (!$ppTO->getNu_recuperacaosubtotalmaxaplicacoes()) {
                $this->mensageiro->addMensagem('É necessario Informar o Máximo de Aplicações!');
                $erro3 = 1;
            }
            if (!$ppTO->getNu_recuperacaosubtotalmaxaprov()) {
                $this->mensageiro->addMensagem('É necessario Informar o Máximo de Aproveitamento da Nota!');
                $erro3 = 1;
            }
            if (!$ppTO->getNu_recuperacaosubtotalpercentual()) {
                $this->mensageiro->addMensagem('É necessario Informar o Percentual para Recuperação!');
                $erro3 = 1;
            }
            if ($erro4) {
                $errogeral = 1;
            }
        }
        if ($errogeral) {
            $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
            return $this->mensageiro;
        }
        return false;
    }

    /**
     * Metodo que verifica se Existe alguma Disciplina em algum modulo na Area do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return boolean
     */
    public function verificarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $resposta = $dao->retornarVwAreaProjetoPedagogico($appTO);
        if (is_object($resposta)) {
            $resposta = $resposta->toArray();
            if (!$resposta) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que verifica se a Disciplina Tem Alguma Dependencia na Trilha
     * @param ModuloDisciplinaTO $mdTO
     * @return boolean
     */
    public function verificarDependenciaDisciplinaModulo(ModuloDisciplinaTO $mdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $resposta = $dao->retornarDependenciaDisciplinaModulo($mdTO);
        if (is_object($resposta)) {
            $resposta = $resposta->toArray();
            if (!$resposta) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que verifica se existe alguma Disciplina com Determinada Serie nos Modulos
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return boolean
     */
    public function verificarSerieDisciplinaModulo(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $resposta = $dao->retornarSerieModulo($ppsneTO);
        if (is_object($resposta)) {
            $resposta = $resposta->toArray();
            if (!$resposta) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que verifica se existe alguma Disciplina com Determinada Serie no Modulo
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return boolean
     */
    public function verificarModuloSerieDisciplina(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $resposta = $dao->retornarModuloSerieDisciplina($msneTO);
        if (is_object($resposta)) {
            $resposta = $resposta->toArray();
            if (!$resposta) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que Verifica se Existem Disciplinas vinculadas na TrilhaDisciplina
     * @param TrilhaTO $tTO
     * @return boolean
     */
    public function verificarTrilha(TrilhaTO $tTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $resposta = $dao->verificarTrilha($tTO);
        if (is_object($resposta)) {
            $resposta = $resposta->toArray();
            if (!$resposta) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que verifica se a Disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @return boolean
     */
    public function verificarDisciplinaRequisito(TrilhaDisciplinaTO $tdTO)
    {
        if ($tdTO->getId_disciplina() == $tdTO->getId_disciplinaprerequisito()) {
            return false;
        }
        $dao = new ProjetoPedagogicoDAO();
        $disciplinaPai = $tdTO->getId_disciplina();
        $requisito[] = $tdTO->getId_disciplinaprerequisito();
        while ($requisito) {
            $newTdTO = new TrilhaDisciplinaTO();
            foreach ($requisito as $disciplinarequisito) {
                $newTdTO->setId_disciplina($disciplinarequisito);
                $newTdTO->setId_trilha($tdTO->getId_trilha());
//				$resposta = $dao->retornarTrilhaDisciplina($mdTO, $newTdTO);
                $resposta = $dao->retornarTrilhaDisciplina($newTdTO);
                if (is_object($resposta)) {
                    $resposta = $resposta->toArray();
                } else {
                    return $this->mensageiro->setMensageiro('Erro ao Retornar Trilha das Disciplinas!', Ead1_IMensageiro::ERRO);
                }
                if (!$resposta) {
//					return $this->mensageiro->setMensageiro('É Necessario que Exista a Disciplina na Trilha para Poder Vincular-la!',Ead1_IMensageiro::AVISO);
                    $requisito = '';
                    continue;
                }
                foreach ($resposta as $disciplina) {
                    if ($disciplina['id_disciplinaprerequisito'] && ($disciplina['id_disciplinaprerequisito'] != $disciplinaPai)) {
                        $requisito[] = $disciplina['id_disciplinaprerequisito'];
                    } elseif ($disciplina['id_disciplinaprerequisito'] == $disciplinaPai) {
                        return false;
                    } elseif (!$disciplina['id_disciplinaprerequisito']) {
                        unset($requisito);
                        $requisito = '';
                    }
                }
            }
        }
        return true;
    }

    /**
     * Metodo que retorna Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, EntidadeTO $eTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            if (!$ppTO->getId_entidadecadastro()) {
                if (!$eTO->getId_entidade()) {
                    $eTO->setId_entidade($eTO->getSessao()->id_entidade);
                    $eTO->setBl_ativo(1);
                }
                $ppTO->setId_entidadecadastro($eTO->getId_entidade());
                $ppTO->setBl_ativo($eTO->getBl_ativo());
            }
            $projetoPedagogico = $dao->retornarProjetoPedagogico($ppTO)->toArray();

            if (empty($projetoPedagogico)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Projeto Pedagógico Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($projetoPedagogico, new ProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoPedagogico(ProjetoPedagogicoTO $ppTO, EntidadeTO $eTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            if (!$ppTO->getId_entidadecadastro()) {
                if (!$eTO->getId_entidade()) {
                    $eTO->setId_entidade($eTO->getSessao()->id_entidade);
                }
                $ppTO->setId_entidadecadastro($eTO->getId_entidade());
            }
            $projetoPedagogico = $dao->retornarVwProjetoPedagogico($ppTO)->toArray();
            if (empty($projetoPedagogico)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($projetoPedagogico, new VwProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo Generico que retorna os tipos das Disciplinas Extra Curricular
     * @param TipoExtracurricularTO $teTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoExtracurricular(TipoExtracurricularTO $teTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoExtra = $dao->retornarTipoExtracurricular($teTO);
        if (is_object($tipoExtra)) {
            $tipoExtra = $tipoExtra->toArray();
        } else {
            unset($tipoExtra);
        }
        if (!$tipoExtra) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoExtra, new TipoExtracurricularTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna lista de areas relacionadas a projeto pedagogico
     * sem repetir as areas
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function retornarVwProjetoAreaUnico(VwProjetoAreaTO $vwProjetoAreaTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $areaProjeto = $dao->retornarVwProjetoAreaUnico($vwProjetoAreaTO)->toArray();
            if (empty($areaProjeto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($areaProjeto, new VwProjetoAreaTO), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Areas do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwProjetoAreaCadastro(VwProjetoAreaTO $vwProjetoAreaTO)
    {
        try {


            $vwProjetoAreaTO->setId_entidade($vwProjetoAreaTO->getId_entidade() ? $vwProjetoAreaTO->getId_entidade() : $vwProjetoAreaTO->getSessao()->id_entidade);


            $dao = new ProjetoPedagogicoDAO();
            $areaProjeto = $dao->retornarVwProjetoAreaCadastro($vwProjetoAreaTO->getId_entidade(), $vwProjetoAreaTO->getId_projetopedagogico());
            if (empty($areaProjeto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
            }
            return $this->mensageiro->setMensageiro($areaProjeto, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Areas do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     * Metodo que retorna Areas de Conhecimento do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function retornarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO)
    {

        try {
            $dao = new ProjetoPedagogicoDAO();
            $areaProjeto = $dao->retornarAreaProjetoPedagogico($appTO)->toArray();
            if (empty($areaProjeto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($areaProjeto, new AreaProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Areas do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     * Metodo que retorna as Series e Nivel de Ensino do Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoTO $ppTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $projetoSerieNivel = $dao->retornarProjetoPedagogicoSerieNivelEnsino($ppTO);
        if (is_object($projetoSerieNivel)) {
            $projetoSerieNivel = $projetoSerieNivel->toArray();
        } else {
            unset($projetoSerieNivel);
        }
        if (!$projetoSerieNivel) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($projetoSerieNivel, new ProjetoPedagogicoSerieNivelEnsinoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as Areas de Conhecimento do Projeto Pedagogico
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoExtracurricular(ProjetoExtracurricularTO $peTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $projetoExtra = $dao->retornarProjetoExtracurricular($peTO);
        if (is_object($projetoExtra)) {
            $projetoExtra = $projetoExtra->toArray();
        } else {
            unset($projetoExtra);
        }
        if (!$projetoExtra) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($projetoExtra, new ProjetoExtracurricularTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna os Modulos do Projeto Pedagogico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloProjetoPedagogico(ModuloTO $mTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $modulo = $dao->retornarModuloProjetoPedagogico($mTO);
        if (is_object($modulo)) {
            $modulo = $modulo->toArray();
        } else {
            unset($modulo);
        }
        if (!$modulo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($modulo, new ModuloTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Serie e Nivel de Ensino do Modulo do Projeto Pedagogico
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $moduloSNE = $dao->retornarModuloSerieNivelEnsino($msneTO);
        if (is_object($moduloSNE)) {
            $moduloSNE = $moduloSNE->toArray();
        } else {
            unset($moduloSNE);
        }
        if (!$moduloSNE) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($moduloSNE, new ModuloSerieNivelEnsinoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as Disciplinas do Modulo do Projeto Pedagogico
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloDisciplina(ModuloDisciplinaTO $mdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $moduloDisciplina = $dao->retornarModuloDisciplina($mdTO);
        if (is_object($moduloDisciplina)) {
            $moduloDisciplina = $moduloDisciplina->toArray();
        } else {
            unset($moduloDisciplina);
        }
        if (!isset($moduloDisciplina)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($moduloDisciplina, new ModuloDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as Disciplinas da Trilha e seus Requisitos
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function retornarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $trilhaDisciplina = $dao->retornarTrilhaDisciplina($tdTO);
        if (is_object($trilhaDisciplina)) {
            $trilhaDisciplina = $trilhaDisciplina->toArray();
        } else {
            unset($trilhaDisciplina);
        }
        if (!$trilhaDisciplina) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($trilhaDisciplina, new TrilhaDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o tipo da Trilha
     * @param TipoTrilhaTO $ttTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoTrilha(TipoTrilhaTO $ttTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoTrilha = $dao->retornarTipoTrilha($ttTO);
        if (is_object($tipoTrilha)) {
            $tipoTrilha = $tipoTrilha->toArray();
        } else {
            unset($tipoTrilha);
        }
        if (!$tipoTrilha) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoTrilha, new TipoTrilhaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o tipo de Trilha Fixa
     * @param TipoTrilhaFixaTO $ttfTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoTrilhaFixa(TipoTrilhaFixaTO $ttfTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $ttfTO->setId_tipotrilha(TipoTrilhaTO::TIPO_TRILHA_FIXA);
        $tipoTrilhaFixa = $dao->retornarTipoTrilhaFixa($ttfTO);
        if (is_object($tipoTrilhaFixa)) {
            $tipoTrilhaFixa = $tipoTrilhaFixa->toArray();
        } else {
            unset($tipoTrilhaFixa);
        }
        if (!$tipoTrilhaFixa) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoTrilhaFixa, new TipoTrilhaFixaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Trilha do Projeto Pedagogico
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function retornarTrilha(TrilhaTO $tTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $trilha = $dao->retornarTrilha($tTO);
        if (is_object($trilha)) {
            $trilha = $trilha->toArray();
        } else {
            unset($trilha);
        }
        if (!$trilha) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($trilha, new TrilhaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo de Aplicação da Avaliação
     * @param TipoAplicacaoAvaliacaoTO $taaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAplicacaoAvaliacao(TipoAplicacaoAvaliacaoTO $taaTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoAplicacaoAvaliacao = $dao->retornarTipoAplicacaoAvaliacao($taaTO);
        if (is_object($tipoAplicacaoAvaliacao)) {
            $tipoAplicacaoAvaliacao = $tipoAplicacaoAvaliacao->toArray();
        } else {
            unset($tipoAplicacaoAvaliacao);
        }
        if (!$tipoAplicacaoAvaliacao) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoAplicacaoAvaliacao, new TipoAplicacaoAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o tipo de Duração do Contrato
     * @param ProjetoContratoDuracaoTipoTO $pcdtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoContratoDuracaoTipo(ProjetoContratoDuracaoTipoTO $pcdtTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoContratoDuracao = $dao->retornarProjetoContratoDuracaoTipo($pcdtTO);
        if (is_object($tipoContratoDuracao)) {
            $tipoContratoDuracao = $tipoContratoDuracao->toArray();
        } else {
            unset($tipoContratoDuracao);
        }
        if (!$tipoContratoDuracao) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoContratoDuracao, new ProjetoContratoDuracaoTipoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo da Multa do Contrato
     * @param ProjetoContratoMultaTipoTO $pcmtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoContratoMultaTipo(ProjetoContratoMultaTipoTO $pcmtTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoMultaContrato = $dao->retornarProjetoContratoMultaTipo($pcmtTO);
        if (is_object($tipoMultaContrato)) {
            $tipoMultaContrato = $tipoMultaContrato->toArray();
        } else {
            unset($tipoMultaContrato);
        }
        if (!$tipoMultaContrato) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoMultaContrato, new ProjetoContratoMultaTipoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o tipo de Valor do projeto Pedagogico
     * @param ProjetoPedagogicoValorTipoTO $ppvtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogicoValorTipo(ProjetoPedagogicoValorTipoTO $ppvtTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoValor = $dao->retornarProjetoPedagogicoValorTipo($ppvtTO);
        if (is_object($tipoValor)) {
            $tipoValor = $tipoValor->toArray();
        } else {
            unset($tipoValor);
        }
        if (!$tipoValor) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoValor, new ProjetoPedagogicoValorTipoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo da Prova Final
     * @param TipoProvaTO $tpTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoProva(TipoProvaTO $tpTO, $pesquisa = false)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoProva = $dao->retornarTipoProva($tpTO);
        if (is_object($tipoProva)) {
            $tipoProva = $tipoProva->toArray();
        } else {
            unset($tipoProva);
        }
        if (!$tipoProva) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($tipoProva, 'st_tipoprova', 'id_tipoprova');
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoProva, new TipoProvaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo de Recuperação
     * @param TipoRecuperacaoTO $trTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoRecuperacao(TipoRecuperacaoTO $trTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tipoRecuperacao = $dao->retornarTipoRecuperacao($trTO);
        if (is_object($tipoRecuperacao)) {
            $tipoRecuperacao = $tipoRecuperacao->toArray();
        } else {
            unset($tipoRecuperacao);
        }
        if (!$tipoRecuperacao) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoRecuperacao, new TipoRecuperacaoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo de Prova do Projeto Pedagogico
     * @param VwProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoTipoProva(VwProjetoTipoProvaTO $ptpTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $tpTO = new TipoProvaTO();
        $tipoProva = $dao->retornarTipoProva($tpTO);
        $resultTipoProva = $tipoProva->toArray();
        $result = array();

        foreach ($resultTipoProva as $indiceTipoProva => $valorTipoProva) {
            $ptpTO->setId_tipoprova($valorTipoProva['id_tipoprova']);
            $result[$indiceTipoProva] = $valorTipoProva;
            $result[$indiceTipoProva]['id_tipoprovaresult'] = null;
            $result[$indiceTipoProva]['id_projetopedagogico'] = null;
            $result[$indiceTipoProva]['nu_taxa'] = null;
            if ($ptpTO->getId_projetopedagogico()) {
                $resultProjTipoProva = $this->retornaArrayProjetoTipoProva($ptpTO);
                if (!empty($resultProjTipoProva)) {
                    $result[$indiceTipoProva]['id_tipoprovaresult'] = $resultProjTipoProva[0]['id_tipoprova'];
                    $result[$indiceTipoProva]['id_tipoprova'] = $valorTipoProva['id_tipoprova'];
                    $result[$indiceTipoProva]['st_tipoprova'] = $valorTipoProva['st_tipoprova'];
                    $result[$indiceTipoProva]['st_descricao'] = $valorTipoProva['st_descricao'];
                    $result[$indiceTipoProva]['id_projetopedagogico'] = $resultProjTipoProva[0]['id_projetopedagogico'];
                    $result[$indiceTipoProva]['nu_taxa'] = $resultProjTipoProva[0]['nu_taxa'];
                }
            }
        }
        if (!$result) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($result, new VwProjetoTipoProvaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     *
     * Método que retorna um array de projeto tipo prova
     * @param VwProjetoTipoProvaTO $ptpTO
     * @return Array
     */
    private function retornaArrayProjetoTipoProva(VwProjetoTipoProvaTO $ptpTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        if (is_nan($ptpTO->getNu_taxa())) {
            $ptpTO->setNu_taxa(null);
        }
        $projTipoProva = $dao->retornarProjetoTipoProva($ptpTO);

        return $resultProjTipoProva = $projTipoProva->toArray();
    }

    /**
     * Metodo que retorna a Recuperação do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $recuperacao = $dao->retornarProjetoRecuperacao($prTO)->toArray();
            if (empty($recuperacao)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($recuperacao, new ProjetoRecuperacaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Recuperação.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna um array de ModuloTO com um array dentro de DisciplinaTO
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplina(ModuloTO $mTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $disciplinaDAO = new DisciplinaDAO();
        try {
            $modulo = $dao->retornarModuloProjetoPedagogico($mTO);
            if (!is_object($modulo)) {
                THROW new Zend_Exception('Erro ao Retornar Módulo!');
            }
            $modulo = $modulo->toArray();
            if (empty($modulo)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrModuloTO = Ead1_TO_Dinamico::encapsularTo($modulo, new ModuloTO());
            foreach ($arrModuloTO as $chave => $newMTO) {
                $mdTO = new ModuloDisciplinaTO();
                $mdTO->setId_modulo($newMTO->getId_modulo());
                $moduloDisciplina = $dao->retornarModuloDisciplina($mdTO);
                if (!is_object($moduloDisciplina)) {
                    THROW new Zend_Exception('Erro ao Retornar Disciplinas do Modulo!');
                }
                $moduloDisciplina = $moduloDisciplina->toArray();
                if (empty($moduloDisciplina)) {
                    $this->mensageiro->addMensagem($newMTO->arrDisciplinas = null);
                    $this->mensageiro->addMensagem($newMTO);
                    continue;
                } else {
                    $arrModuloDisciplina = Ead1_TO_Dinamico::encapsularTo($moduloDisciplina, new ModuloDisciplinaTO());
                    $arrDisciplinas = array();
                    foreach ($arrModuloDisciplina as $chave2 => $newMdTO) {
                        $dTO = new DisciplinaTO();
                        $dTO->setId_disciplina($newMdTO->getId_disciplina());
                        $disciplina = $disciplinaDAO->retornaDisciplina($dTO);
                        if (!is_object($disciplina)) {
                            THROW new Zend_Exception('Erro ao Retornar Disciplinas!');
                        }
                        $disciplina = $disciplina->toArray();
                        $newDTO = Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO(), true);
                        $arrDisciplinas[$chave2] = $newDTO;
                    }
                    $newMTO->arrDisciplinas = $arrDisciplinas;
                    unset($arrDisciplinas);
                }
                $this->mensageiro->addMensagem($newMTO);
            }
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensagem('Erro ao Retornar Arvore de Módulos com Disciplinas!');
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setCodigo($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que retorna um array de DisciplinaTO com um array dentro de DisciplinaTO (prerequisitos)
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreTrilhaDisciplinaPrerequisitos(TrilhaDisciplinaTO $tdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $disciplinaDAO = new DisciplinaDAO();
        try {
            $trilhaDisciplinas = $dao->retornarTrilhaDisciplina($tdTO);
            if (is_object($trilhaDisciplinas)) {
                $trilhaDisciplinas = $trilhaDisciplinas->toArray();
            } else {
                THROW new Zend_Exception('Erro ao Retornas as Disciplinas da Trilha!');
            }
            if (empty($trilhaDisciplinas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrDisciplinas = array();
            foreach ($trilhaDisciplinas as $chave => $disciplinaPai) {
                $arrDisciplinas[$disciplinaPai['id_disciplina']]['prerequisitos'][$disciplinaPai['id_disciplinaprerequisito']] = $disciplinaPai['id_disciplinaprerequisito'];
            }
            $arrFinal = array();
            foreach ($arrDisciplinas as $disciplinaPai => $prerequisitos) {
                $dTO = new DisciplinaTO();
                $dTO->setId_disciplina($disciplinaPai);
                $nomDisciplina = $disciplinaDAO->retornaDisciplina($dTO);
                if (is_object($nomDisciplina)) {
                    $nomDisciplina = $nomDisciplina->toArray();
                } else {
                    THROW new Zend_Exception('Erro ao Retornar Disciplina!');
                }
                if (empty($nomDisciplina)) {
                    THROW new Zend_Exception('Erro ao Nomear Disciplina!');
                }
                $dTO = Ead1_TO_Dinamico::encapsularTo($nomDisciplina, new DisciplinaTO(), true);
                $arrTempPre = array();
                foreach ($prerequisitos as $prerequisito => $arrprerequisito) {
                    foreach ($arrprerequisito as $id_prerequisito => $valor) {
                        $predTO = new DisciplinaTO();
                        $predTO->setId_disciplina($id_prerequisito);
                        $nomDisciplinaPre = $disciplinaDAO->retornaDisciplina($predTO);
                        if (is_object($nomDisciplinaPre)) {
                            $nomDisciplinaPre = $nomDisciplinaPre->toArray();
                        } else {
                            THROW new Zend_Exception('Erro ao Retornar Disciplina Pré-requisito!');
                        }
                        if (empty($nomDisciplinaPre)) {
                            THROW new Zend_Exception('Erro ao Nomear Disciplina Pré-requisito!');
                        }
                        $predTO = Ead1_TO_Dinamico::encapsularTo($nomDisciplinaPre, new DisciplinaTO(), true);
                        $arrTempPre[$id_prerequisito] = $predTO;
                    }
                }
                $dTO->arrDados = $arrTempPre;
                unset($arrTempPre);
                $arrFinal[$disciplinaPai] = $dTO;
            }
            return $this->mensageiro->setMensageiro($arrFinal, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setCodigo($dao->excecao . ' ' . $disciplinaDAO->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
        }
    }

    /**
     * Metodo que retorna um array de ModuloTO com um array dentro de DisciplinaTO e que contem um array dentro de DisciplinaTO (prerequisitos)
     * @param ModuloTO $mTO
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplinaTrilhaDisciplina(ModuloTO $mTO, TrilhaTO $tTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $disciplinaDAO = new DisciplinaDAO();
        $mensageiro = new Ead1_Mensageiro();
        try {
            $modulo = $dao->retornarModuloProjetoPedagogico($mTO);
            if (!is_object($modulo)) {
                THROW new Zend_Exception('Erro ao Retornar Módulo!');
            }
            $modulo = $modulo->toArray();
            if (empty($modulo)) {
                return $mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrModuloTO = Ead1_TO_Dinamico::encapsularTo($modulo, new ModuloTO());
            foreach ($arrModuloTO as $chave => $newMTO) {
                $mdTO = new ModuloDisciplinaTO();
                $mdTO->setId_modulo($newMTO->getId_modulo());
                $moduloDisciplina = $dao->retornarModuloDisciplina($mdTO);
                if (!is_object($moduloDisciplina)) {
                    THROW new Zend_Exception('Erro ao Retornar Disciplinas do Modulo!');
                }
                $moduloDisciplina = $moduloDisciplina->toArray();
                if (empty($moduloDisciplina)) {
                    $mensageiro->addMensagem($newMTO->arrDisciplinas = null);
                    $mensageiro->addMensagem($newMTO);
                    continue;
                } else {
                    $arrModuloDisciplina = Ead1_TO_Dinamico::encapsularTo($moduloDisciplina, new ModuloDisciplinaTO());
                    $arrDisciplinas = array();
                    foreach ($arrModuloDisciplina as $chave2 => $newMdTO) {
                        $dTO = new DisciplinaTO();
                        $dTO->setId_disciplina($newMdTO->getId_disciplina());
                        $disciplina = $disciplinaDAO->retornaDisciplina($dTO);
                        if (!is_object($disciplina)) {
                            THROW new Zend_Exception('Erro ao Retornar Disciplinas!');
                        }
                        $disciplina = $disciplina->toArray();
                        $newDTO = Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO(), true);
                        $tdTO = new TrilhaDisciplinaTO();
                        $tdTO->setId_trilha($tTO->getId_trilha());
                        $tdTO->setId_disciplina($newDTO->getId_disciplina());
                        $trilhaDisciplina = $this->retornarArvoreTrilhaDisciplinaPrerequisitos($tdTO);
                        if ($trilhaDisciplina->getTipo() == Ead1_IMensageiro::ERRO) {
                            return $trilhaDisciplina;
                        } elseif ($trilhaDisciplina->getTipo() == Ead1_IMensageiro::AVISO) {
                            $newDTO->arrDados = array();
                        } elseif ($trilhaDisciplina->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            $trilhaDisciplina = $trilhaDisciplina->getMensagem();
                            foreach ($trilhaDisciplina as $newTdTO) {
                                $newDTO->arrDados = $newTdTO->arrDados;
                            }
                            $arrDisciplinas[$chave2] = $newDTO;
                        }
                    }
                    $newMTO->arrDisciplinas = $arrDisciplinas;
                    unset($arrDisciplinas);
                }
                $mensageiro->addMensagem($newMTO);
            }
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $mensageiro->setMensagem('Erro ao Retornar Arvore de Módulos com Disciplinas!');
            $mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $mensageiro->setCodigo($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Retorna as áreas de conhecimento de um projeto pedagógico
     * Retorna os dados da view vw_areaprojetopedagogico
     * @param $vwAreaProjetoPedagogico
     * @return Ead1_Mensageiro
     */
    public function retornarVwAreaProjetoPedagogico(VwAreaProjetoPedagogicoTO $vwAreaProjetoPedagogico)
    {

        try {
            $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
            $arVwAreaProjetoPedagogico = $projetoPedagogicoDAO->retornarVwAreaProjetoPedagogico($vwAreaProjetoPedagogico);
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arVwAreaProjetoPedagogico->toArray(), new VwAreaProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que verifica e retorna as Disciplinas que não podem ser exibidas para vinculo na trilha disciplina
     * @param TrilhaDisciplinaTO $tdTO
     * @return array
     */
    public function filtrarDisciplinaRequisito(TrilhaDisciplinaTO $tdTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $trilhaDisciplina = $dao->retornarTrilhaDisciplina($tdTO);
        if (is_object($trilhaDisciplina)) {
            $trilhaDisciplina = $trilhaDisciplina->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplina da Trilha');
        }
        if (empty($trilhaDisciplina) || empty($trilhaDisciplina[0]['id_disciplinaprerequisito'])) {
            return array();
        }
        $disciplinasPercorridas[$tdTO->getId_disciplinaprerequisito()] = $tdTO->getId_disciplinaprerequisito();
        $requisito[$tdTO->getId_disciplinaprerequisito()] = $tdTO->getId_disciplinaprerequisito();
        while ($requisito) {
            foreach ($requisito as $disciplinaPrerequisito => $id) {
                $newTdTO = new TrilhaDisciplinaTO();
                $newTdTO->setId_disciplina($id);
                $newTdTO->setId_trilha($tdTO->getId_trilha());
                $prerequisitos = $dao->retornarTrilhaDisciplina($newTdTO);
                if (is_object($prerequisitos)) {
                    $prerequisitos = $prerequisitos->toArray();
                } else {
                    return $this->mensageiro->setMensageiro('Erro ao Retornar Pré-requisitos da Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
                }
                if (!empty($prerequisitos)) {
                    foreach ($prerequisitos as $chave => $prerequisito) {
                        if (!in_array($prerequisito['id_disciplinaprerequisito'], $disciplinasPercorridas)) {
                            $disciplinasPercorridas[$prerequisito['id_disciplinaprerequisito']] = $prerequisito['id_disciplinaprerequisito'];
                            $requisito[$prerequisito['id_disciplinaprerequisito']] = $prerequisito['id_disciplinaprerequisito'];
                        }
                    }
                }
                unset($requisito[$id]);
            }
        }
        return $disciplinasPercorridas;
    }

    /**
     * Metodo que Retorna as Disciplinas Disponivei como Pre-Requisito Para a Disciplina da Trilha
     * @param TrilhaDisciplinaTO $tdTO
     * @param array ( ModuloDisciplinaTO $arrTO )
     */
    public function retornarModuloDisciplinaTrilha(TrilhaDisciplinaTO $tdTO, $arrTO)
    {
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('Erro nos Parametros Recebidos!', Ead1_IMensageiro::AVISO, 'O Segundo Parametro Recebido Não é um Array!');
        }
        $arrRemoverDisciplinas = $this->filtrarDisciplinaRequisito($tdTO);
        if (is_object($arrRemoverDisciplinas)) {
            return $arrRemoverDisciplinas;
        }
        if (empty($arrRemoverDisciplinas)) {
            foreach ($arrTO as $chave => $mdTO) {
                if (!($mdTO instanceof ModuloDisciplinaTO)) {
                    return $this->mensageiro->setMensageiro('Erro nos Parametros Recebidos!', Ead1_IMensageiro::AVISO, 'O Objeto Recebido no Array Não é uma Instancia do Objeto ModuloDisciplinaTO!');
                }
                if ($mdTO->getId_disciplina() == $tdTO->getId_disciplina()) {
                    unset($arrTO[$chave]);
                }
            }
        } else {
            foreach ($arrTO as $chave => $mdTO) {
                if (!($mdTO instanceof ModuloDisciplinaTO)) {
                    return $this->mensageiro->setMensageiro('Erro nos Parametros Recebidos!', Ead1_IMensageiro::AVISO, 'O Objeto Recebido no Array Não é uma Instancia do Objeto ModuloDisciplinaTO!');
                }
                foreach ($arrRemoverDisciplinas as $id_disciplina) {
                    if ($mdTO->getId_disciplina() == $id_disciplina || $mdTO->getId_disciplina() == $tdTO->getId_disciplina()) {
                        unset($arrTO[$chave]);
                    }
                }
            }
        }
        return $this->mensageiro->setMensageiro($arrTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna uma arvode de modulos com disciplinas para o flex
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplinaFlex(ModuloTO $mTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $disciplinaDAO = new DisciplinaDAO();
        try {
            $modulo = $dao->retornarModuloProjetoPedagogico($mTO);
            if (!is_object($modulo)) {
                THROW new Zend_Exception('Erro ao Retornar Módulo!');
            }
            $modulo = $modulo->toArray();
            if (empty($modulo)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $arrModuloTO = Ead1_TO_Dinamico::encapsularTo($modulo, new ModuloTO());
            $arrPrincipal = array();
            foreach ($arrModuloTO as $chave => $newMTO) {
                $arrPrincipal['modulo'][$chave]['to'] = $newMTO;
                $arrPrincipal['modulo'][$chave]['label'] = $newMTO->getSt_tituloexibicao();
                $mdTO = new ModuloDisciplinaTO();
                $mdTO->setId_modulo($newMTO->getId_modulo());
                $mdTO->setBl_ativo(true);
                $moduloDisciplina = $dao->retornarModuloDisciplina($mdTO);
                if (!is_object($moduloDisciplina)) {
                    THROW new Zend_Exception('Erro ao Retornar Disciplinas do Modulo!');
                }
                $moduloDisciplina = $moduloDisciplina->toArray();
                if (empty($moduloDisciplina)) {
                    $arrPrincipal['modulo'][$chave]['children'] = null;
                    continue;
                } else {
                    $arrModuloDisciplina = Ead1_TO_Dinamico::encapsularTo($moduloDisciplina, new ModuloDisciplinaTO());
                    foreach ($arrModuloDisciplina as $chave2 => $newMdTO) {
                        $dTO = new DisciplinaTO();
                        $dTO->setId_disciplina($newMdTO->getId_disciplina());
                        $disciplina = $disciplinaDAO->retornaDisciplina($dTO);

                        $sTO = new SerieTO();
                        $sTO->setId_serie($newMdTO->getId_serie());
                        $serie = $disciplinaDAO->retornaSerie($sTO)->toArray();
                        $serieUnica = $serie[0];
                        if (!is_object($disciplina)) {
                            THROW new Zend_Exception('Erro ao Retornar Disciplinas!');
                        }
                        $disciplina = $disciplina->toArray();
                        $newDTO = Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO(), true);
                        $arrPrincipal['modulo'][$chave]['children'][$chave2]['to'] = $newDTO;
                        $arrPrincipal['modulo'][$chave]['children'][$chave2]['label'] = $newDTO->getSt_disciplina();
                        $arrPrincipal['modulo'][$chave]['children'][$chave2]['children'] = null;

                        $vwDsnTO = new VwDisciplinaSerieNivelTO();
                        $vwDsnTO->setId_disciplina($newMdTO->getId_disciplina());
                        $vwDsnTO->setId_serie($newMdTO->getId_serie());
                        $vwDsnTO->setBl_ativa(true);
                        $dsnORM = new VwDisciplinaSerieNivelORM();
                        $dsnDados = $dsnORM->fetchAll($dsnORM->montarWhereView($vwDsnTO, false, true))->toArray();
                        if (!empty($dsnDados)) {
                            $dadosVw = Ead1_TO_Dinamico::encapsularTo($dsnDados, new VwDisciplinaSerieNivelTO(), true);
                        }

                        $arrPrincipal['modulo'][$chave]['children'][$chave2]['toVw'] = $dadosVw;

                        $arrPrincipal['serie'][$newMdTO->getId_serie()]['to'] = $serieUnica;
                        $arrPrincipal['serie'][$newMdTO->getId_serie()]['label'] = $serieUnica['st_serie'];
                        $arrPrincipal['serie'][$newMdTO->getId_serie()]['children'][] = array('to' => $newDTO, 'label' => $newDTO->getSt_disciplina(), 'children' => null);
                    }
                }
                $ppTO = new ProjetoPedagogicoTO();
                $ppTO->setBl_ativo(true);
                $ppTO->setId_projetopedagogico($mTO->getId_projetopedagogico());
                $projeto = $dao->retornarProjetoPedagogico($ppTO)->toArray();
                $projeto = $projeto[0];
                if (empty($projeto)) {
                    $arrPrincipal['manual'] = $arrPrincipal['modulo'];
                } else {
                    $tdTO = new TrilhaDisciplinaTO();
                    $tdTO->setId_trilha($projeto['id_trilha']);
                    $where = 'id_trilha = ' . $projeto['id_trilha'];
                    $trilha = $dao->retornarTrilhaDisciplina($tdTO, $where)->toArray();
//					Zend_Debug::dump($projeto);exit;
                    if (empty($trilha)) {
                        $arrPrincipal['manual'] = $arrPrincipal['modulo'];
                    } else {
                        foreach ($trilha as $index => $trilhaDisciplinaTO) {
                            $dTO = new DisciplinaTO();
                            $dTO->setId_disciplina($trilhaDisciplinaTO['id_disciplina']);
                            $disciplina = $disciplinaDAO->retornaDisciplina($dTO)->toArray();
                            $arrPrincipal['manual'][$index]['to'] = Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO(), true);
                            $arrPrincipal['manual'][$index]['label'] = $disciplina[0]['st_disciplina'];
                        }
                    }
                }
            }
            return $this->mensageiro->setMensageiro($arrPrincipal, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensagem('Erro ao Retornar Arvore de Módulos com Disciplinas!');
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setCodigo($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Método que retorna os módulos, série, disciplinas e disciplinas pré-requisito
     * @param ProjetoPedagogicoTO $ppTO
     */
    public function retornarModuloSerieDisciplinaPreRequisito(VwModuloDisciplinaPreRequisitoTO $ppTO)
    {
        $dao = new ProjetoPedagogicoDAO();
        $dados = $dao->retornarModuloSerieDisciplinaPreRequisito($ppTO);
        try {
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dados->toArray(), new VwModuloDisciplinaPreRequisitoTO()));
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage());
        }
    }

    /**
     * Método que retorna as disciplinas pré-requisito disponíveis para determinado projeto pedagógico e disciplina
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasPreRequisito(VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO)
    {
        try {
            if (!$vwMdptTO->getId_disciplina()) {
                return $this->mensageiro->setMensageiro("Erro ao Retornar Pré-Requisitos", Ead1_IMensageiro::ERRO, "É necessário haver uma disciplina selecionada para retornar as disciplinas pré-requisto disponíveis.");
            }
            if (!$vwMdptTO->getId_projetopedagogico()) {
                return $this->mensageiro->setMensageiro("Erro ao Retornar Pré-Requisitos", Ead1_IMensageiro::ERRO, "É necessário haver um projeto pedagógico selecionado para retornar as disciplinas pré-requisto disponíveis.");
            }

            $dao = new ProjetoPedagogicoDAO();
            $toPesquisa = new VwModuloDisciplinaProjetoTrilhaTO();
            $toPesquisa->setId_trilha($vwMdptTO->getId_trilha());
            $toPesquisa->setId_projetopedagogico($vwMdptTO->getId_projetopedagogico());
            $dadosGerais = $dao->retornarVwModuloDisciplinaProjetoTrilha($toPesquisa);
            $tdTO = new TrilhaDisciplinaTO();
            $tdTO->setId_trilha($vwMdptTO->getId_trilha());
            $tdTO->setId_disciplina($vwMdptTO->getId_disciplina());
            $trilhaDisciplina = $dao->retornarTrilhaDisciplina($tdTO)->toArray();
            $jaCadastradas = array();
            $jaCadastradas[$vwMdptTO->getId_disciplina()] = $vwMdptTO->getId_disciplina();
            if (!empty($trilhaDisciplina)) {
                foreach ($trilhaDisciplina as $trilha) {
                    $jaCadastradas[$trilha['id_disciplinaprerequisito']] = $trilha['id_disciplinaprerequisito'];
                }
            }

            if (!empty($dadosGerais)) {
                $arDisciplinasDisponiveis = array();
                foreach ($dadosGerais->toArray() as $index => $disciplina) {
                    $tdTO = new TrilhaDisciplinaTO();
                    $tdTO->setId_disciplina($vwMdptTO->getId_disciplina());
                    $tdTO->setId_disciplinaprerequisito($disciplina['id_disciplina']);
                    $tdTO->setId_trilha($vwMdptTO->getId_trilha());
                    if ($this->verificarDisciplinaRequisito($tdTO) && !in_array($disciplina['id_disciplina'], $jaCadastradas)) {
                        $arDisciplinasDisponiveis[$disciplina['id_disciplina']] = $disciplina;
                    }
                }
                if (empty($arDisciplinasDisponiveis)) {
                    return $this->mensageiro->setMensageiro("Nenhuma disciplina disponível!", Ead1_IMensageiro::AVISO);
                } else {
                    return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arDisciplinasDisponiveis, new VwModuloDisciplinaProjetoTrilhaTO()), Ead1_IMensageiro::SUCESSO);
                }
            } else {
                return $this->mensageiro->setMensageiro("Nenhuma disciplina disponível!", Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Pré-Requisitos', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna a área do projeto nível ensino para o componente de pesquisa do projeto pedagógico (Contrato - Matrícula).
     * @param VwAreaProjetoNivelEnsinoTO $apneTO
     * @return Ead1_Mensageiro
     */
    public function retonarAreaProjetoNivelEnsino(VwAreaProjetoNivelEnsinoTO $apneTO)
    {
        try {
            if (!$apneTO->getId_entidade()) {
                $apneTO->setId_entidade($apneTO->getSessao()->id_entidade);
            }
            $dao = new ProjetoPedagogicoDAO();
            $orm = new VwAreaProjetoNivelEnsinoORM();
            $where = $orm->select()->from('vw_areaprojetonivelensino', array('id_entidade', 'id_projetopedagogico', 'st_tituloexibicao',
                'st_projetopedagogico', 'bl_ativo', 'id_nivelensino',
                'st_nivelensino', 'st_nomeentidade', 'st_situacao'))
                ->distinct(true)->where($orm->montarWhereView($apneTO, false, true));
            $dados = $dao->retonarAreaProjetoNivelEnsino($apneTO, $where);
            if ($dados) {
                return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dados->toArray(), new VwAreaProjetoNivelEnsinoTO()));
            } else {
                return new Ead1_Mensageiro("Nenhum Registro Encontrado!", Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar os Projetos', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna as áreas de conhecimento vinculadas ao projeto pedagógico
     * Busca os dados na view vw_projetoarea
     * @param $vwProjetoAreaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoArea(VwProjetoAreaTO $vwProjetoAreaTO)
    {
        $dao = new ProjetoPedagogicoDAO();

        try {

            //$dao->
        } catch (Exception $exc) {

        }
    }

    /**
     * Lista os projetos pedagógicos vinculados ao produto
     *
     * @see ProjetoPedagogicoRO::retornarVwProdutoProjetoTipoValor();
     *
     * @param VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoProjetoTipoValor(VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO)
    {

        try {
            $projetoPedagogicoDAO = new ProjetoPedagogicoDAO();
            $listaPP = $projetoPedagogicoDAO->retornarVwProdutoProjetoTipoValor($vwProdutoProjetoTipoValorTO);
            if ($listaPP->toArray()) {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($listaPP->toArray(), $vwProdutoProjetoTipoValorTO), Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro("Nenhum Registro de Projeto Encontrado!", Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que analisa de forma recursiva as disciplinas que estão como pré-requisito dentro da árvore de disciplinas
     * @param int $idProjetoPedagogico
     * @param int $idDisciplina
     */
    private function verificaDisciplinaPreRequisito($idProjetoPedagogico, $idDisciplina)
    {
        $to = new VwModuloDisciplinaPreRequisitoTO();
        $to->setId_projetopedagogico($idProjetoPedagogico);
        $to->setId_disciplinaprerequisito($idDisciplina);

        $dao = new ProjetoPedagogicoDAO();
        $dado = $dao->retornarModuloSerieDisciplinaPreRequisito($to);

        if ($dado) {
            $arDados = $dado->toArray();
            if (!empty($arDados)) {
                foreach ($arDados as $disciplina) {
                    $this->arDisciplinasIndisponiveis[] = $disciplina['id_disciplina'];
                    if ($disciplina['id_disciplina'] != NULL) {
                        $this->verificaDisciplinaPreRequisito($idProjetoPedagogico, $disciplina['id_disciplina']);
                    }
                }
            }
        }
    }

    /**
     * Retorna os modulos do projeto pedagogico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModulo(ModuloTO $mTO)
    {
        try {

            $dao = new ProjetoPedagogicoDAO();
            $modulos = $dao->retornarModulo($mTO);

            if ($modulos->count() < 1) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado.', Ead1_IMensageiro::AVISO);
            }

            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($modulos, new ModuloTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {

            return $this->mensageiro->setMensageiro('Erro ao Retornar Módulos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna as disciplinas do modulo
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwModuloDisciplinaProjetoTrilha(VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $vwMdptTO->setBl_ativo(1);
            $modulos = $dao->retornarVwModuloDisciplinaProjetoTrilha($vwMdptTO)->toArray();
            if (empty($modulos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de ' . Ead1_LabelFuncionalidade::getLabel(24) . ' Encontrado.', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($modulos, new VwModuloDisciplinaProjetoTrilhaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar ' . Ead1_LabelFuncionalidade::getLabel(62) . '(s) e ' . Ead1_LabelFuncionalidade::getLabel(24) . '(s)!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna disciplinas do projeto
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasProjeto(VwModuloDisciplinaProjetoTrilhaTO $vwTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $modulos = $dao->retornarVwModuloDisciplinaProjetoTrilhaSelect($vwTO)->toArray();
            if (empty($modulos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de ' . Ead1_LabelFuncionalidade::getLabel(24) . ' Encontrado.', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($modulos, new VwModuloDisciplinaProjetoTrilhaTO()));
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar ' . Ead1_LabelFuncionalidade::getLabel(24) . 's do Projeto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna as disciplinas do modulo e seu status para de acordo com a validação dos pre requisitos
     * Atualizado por Elcio - Adicionada uma condição na linha 2711 para evitar o erro de quando o array está vazio.
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloDisciplinaStatus(VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO, MatriculaTO $mTO)
    {


        //Status Fixos:
        $APTO = 'Apto';
        $INAPTO = 'Inapto';
        $CURSANDO = 'Cursando';
        $CONCLUIDO = 'Conclu�do';
        //-----------------------
        //Situações e Evoluções Fixas
        $SITUACAO_PENDENTE = 53;
        $SITUACAO_ATIVA = 54;
        $SITUACAO_CONCEBIDA = 65;
        $EVOLUCAO_NAO_INICIADO = 11;
        $EVOLUCAO_CONCLUIDA = 12;
        $EVOLUCAO_CURSANDO = 13;
        //----------------------------
        try {
            $dao = new ProjetoPedagogicoDAO();
            $matriculaDAO = new MatriculaDAO();
            $arrFinal = array();
            $vwMdptTOConsulta = new VwModuloDisciplinaProjetoTrilhaTO;
            $vwMdptTOConsulta->setId_trilha($vwMdptTO->getId_trilha());
            $vwMdptTOConsulta->setId_disciplina($vwMdptTO->getId_disciplina());
            $vwMdptTOConsulta->setId_projetopedagogico($vwMdptTO->getId_projetopedagogico());
            $disciplinas = $dao->retornarVwModuloDisciplinaProjetoTrilha($vwMdptTOConsulta)->toArray();
            if (empty($disciplinas)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado.', Ead1_IMensageiro::AVISO);
            }
            $arrDisciplinas = Ead1_TO_Dinamico::encapsularTo($disciplinas, new VwModuloDisciplinaProjetoTrilhaTO());
            $tdTO = new TrilhaDisciplinaTO();
            $tdTO->setId_trilha($vwMdptTO->getId_trilha());
            foreach ($arrDisciplinas as $index => $disciplina) {
                $tdTO->setId_disciplina($disciplina->getId_disciplina());
                $mdTO = new MatriculaDisciplinaTO();
                $mdTO->setId_matricula($mTO->getId_matricula());
                $mdTO->setId_disciplina($disciplina->getId_disciplina());
                $validaDisciplina = $matriculaDAO->retornarMatriculaDisciplina($mdTO)->toArray();
                if (empty($validaDisciplina)) {
                    $disciplina->setSt_status($APTO);
                    $arrFinal[$index] = $disciplina;
                    continue;
                } else {

                    $validaDisciplina = $validaDisciplina[0];

                    if ($validaDisciplina['id_situacao'] == $SITUACAO_ATIVA && $validaDisciplina['id_evolucao'] == $EVOLUCAO_CURSANDO) {
                        $disciplina->setSt_status($CURSANDO);
                        $arrFinal[$index] = $disciplina;
                        continue;
                    } else if ($validaDisciplina['id_situacao'] == $SITUACAO_ATIVA && $validaDisciplina['id_evolucao'] == $EVOLUCAO_CONCLUIDA) {
                        $disciplina->setSt_status($CONCLUIDO);
                        $arrFinal[$index] = $disciplina;
                        continue;
                    } else if ($validaDisciplina['id_evolucao'] == $EVOLUCAO_NAO_INICIADO && $validaDisciplina['id_situacao'] == $SITUACAO_PENDENTE) {
                        $disciplinasTrilha = $dao->retornarTrilhaDisciplina($tdTO)->toArray();
                        if (empty($disciplinasTrilha)) {
                            $disciplina->setSt_status($APTO);
                            $arrFinal[$index] = $disciplina;
                            continue;
                        } else {

                            $statusPrerequisitosDisciplina = false;
                            foreach ($disciplinasTrilha as $disciplinaTrilha) {
                                $mdTO->setId_disciplina($disciplinaTrilha['id_disciplinaprerequisito']);
                                $arrMatriculaDisciplina = $matriculaDAO->retornarMatriculaDisciplina($mdTO)->toArray();
                                if (empty($arrMatriculaDisciplina)) {
                                    continue;
                                }
                                $matriculaDisciplinaTO = Ead1_TO_Dinamico::encapsularTo($arrMatriculaDisciplina, new MatriculaDisciplinaTO(), true);
                                //Adicionando OU e mudando de negação pra confirmação da condicional abaixo
                                if (($matriculaDisciplinaTO->getId_situacao() == $SITUACAO_ATIVA && $matriculaDisciplinaTO->getId_evolucao() == $EVOLUCAO_CONCLUIDA) || ($matriculaDisciplinaTO->getId_situacao() == $SITUACAO_CONCEBIDA)) {
                                    $statusPrerequisitosDisciplina = true;
                                }
                            }
                            if ($statusPrerequisitosDisciplina) {
                                $disciplina->setSt_status($APTO);
                                $arrFinal[$index] = $disciplina;
                                continue;
                            } else {
                                $disciplina->setSt_status($INAPTO);
                                $arrFinal[$index] = $disciplina;
                                continue;
                            }
                        }
                    } else {
                        $disciplina->setSt_status($INAPTO);
                        $arrFinal[$index] = $disciplina;
                        continue;
                    }
                    $arrFinal[$index] = $disciplina;
                }
            }
            return $this->mensageiro->setMensageiro($arrFinal, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Modulos e Disciplinas!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Retorna os pre requisitos da disciplina
     * @param VwDisciplinaRequisitoTO $vwDrTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinaRequisito(VwDisciplinaRequisitoTO $vwDrTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $requisitos = $dao->retornarDisciplinaRequisito($vwDrTO)->toArray();
            if (empty($requisitos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado.', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($requisitos, new VwDisciplinaRequisitoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Modulos e Disciplinas!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os modulos do projeto pedagogico da matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloMatricula(MatriculaTO $mTO)
    {
        try {
            if (!$mTO->getId_matricula()) {
                THROW new Zend_Exception('O id da matricula não veio setado!');
            }
            $matriculaDAO = new MatriculaDAO();
            $dao = new ProjetoPedagogicoDAO();
            $arMatricula = $matriculaDAO->retornarMatricula($mTO)->toArray();
            if (empty($arMatricula)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $mTO = Ead1_TO_Dinamico::encapsularTo($arMatricula, new MatriculaTO(), true);
            if (!$mTO->getId_projetopedagogico()) {
                THROW new Zend_Exception('Esta Matricula não possui um projeto pedagogico vinculado!');
            }
            $moduloTO = new ModuloTO();
            $moduloTO->setId_projetopedagogico($mTO->getId_projetopedagogico());
            $moduloTO->setBl_ativo(true);
            $arrModulos = $dao->retornarModulo($moduloTO)->toArray();
            if (empty($arrModulos)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, 'Não foi encontrado nenhum modulo vinculado ao projeto pedagogico da matricula');
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrModulos, new ModuloTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Módulos da Matricula!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as disciplinas do modulo
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasModulo(ModuloTO $mTO)
    {
        try {
            if (!$mTO->getId_modulo()) {
                THROW new Zend_Exception('O id do modulo não veio setado!');
            }
            $dao = new ProjetoPedagogicoDAO();
            $disciplinaDAO = new DisciplinaDAO();
            $mdTO = new ModuloDisciplinaTO();
            $mdTO->setId_modulo($mTO->getId_modulo());
            $mdTO->setBl_ativo(true);
            $arrModuloDisciplina = $dao->retornarModuloDisciplina($mdTO)->toArray();
            if (empty($arrModuloDisciplina)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, 'Não foi encontrado nenhuma disciplina em este modulo');
            }
            $arrWhere = array();
            foreach ($arrModuloDisciplina as $moduloDisciplina) {
                $arrWhere[$moduloDisciplina['id_disciplina']] = $moduloDisciplina['id_disciplina'];
            }
            $implode = implode(' ,', $arrWhere);
            $where = 'id_disciplina in (' . $implode . ')';
            $arrDisciplinas = $disciplinaDAO->retornaDisciplina(new DisciplinaTO(), $where)->toArray();
            if (empty($arrDisciplinas)) {
                THROW new Zend_Exception('As disciplinas do modulo são diferentes das disciplinas da tabela de disciplina');
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrDisciplinas, new DisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas do Modulo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view das disciplinas do modulo
     * @param VwModuloDisciplinaTO $vwMdTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwModuloDisciplina(VwModuloDisciplinaTO $vwMdTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarVwModuloDisciplina($vwMdTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwModuloDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Disciplinas do Módulo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a entidade da sessão e as entidades que compartilham os projetos pedagogicos com a entidade da sessão
     * @param EntidadeTO $entidadeTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadesProjetos(EntidadeTO $entidadeTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $entidadeTO->setId_entidade(($entidadeTO->getId_entidade() ? $entidadeTO->getId_entidade() : $entidadeTO->getSessao()->id_entidade));
            $entidades = $dao->retornarEntidadesProjetos($entidadeTO)->toArray();
            if (empty($entidades)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($entidades, new EntidadeTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Instituições!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna as disciplinas de uma entidade de acordo com os projetos pedagógicos
     * @param VwEntidadeProjetoDisciplinaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeProjetoDisciplina(VwEntidadeProjetoDisciplinaTO $to)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
//			$to->setId_entidadecadastro($to->getId_entidade());
            $to->setId_entidade($to->getSessao()->id_entidade);
            $disciplinas = $dao->retornarVwEntidadeProjetoDisciplina($to)->toArray();
            if (empty($disciplinas)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplinas, new VwEntidadeProjetoDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Disciplinas!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os projetos pedagógicos da entidade
     * @param VwProjetoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoEntidade(VwProjetoEntidadeTO $to)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $dados = $dao->retornarVwProjetoEntidade($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwProjetoEntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Projetos Pedagógicos da Instituição!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as turmas disponiveis pra matricula
     * @param VwTurmasDisponiveisTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwTurmasDisponiveis(VwTurmasDisponiveisTO $to)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $to->setId_entidadecadastro($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $to->getSessao()->id_entidade);
            $dados = $dao->retornarVwTurmasDisponiveis($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwTurmasDisponiveisTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar " . Ead1_LabelFuncionalidade::getLabel(409) . "!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os projetos pedagógicos da entidade
     * @param VwProjetoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoEntidadeProjeto(VwProjetoEntidadeTO $to)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarVwProjetoEntidade($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwProjetoEntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Projetos Pedagógicos da Instituição!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna usuários que podem ser cadastrados como coordenadores
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwUsuarioPerfilPedagogico(VwUsuarioPerfilPedagogicoTO $to)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarVwUsuarioPerfilPedagogico($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwUsuarioPerfilPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Perfis Pedagogicos!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param VwUsuarioPerfilEntidadeReferenciaTO $to
     */
    public function retornarVwUsuarioPerfilEntidadeReferencia(VwUsuarioPerfilEntidadeReferenciaTO $to, $where = null)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarVwUsuarioPerfilEntidadeReferencia($to, $where)->toArray();
            //Zend_Debug::dump($dados,__CLASS__.'('.__LINE__.')');exit;
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro de Usuário com esse perfil Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwUsuarioPerfilEntidadeReferenciaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Coordenadores!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna pessoas Coordenador de projeto pedagogico ou de disciplinas ou professor - integração moodle
     * @param VwUsuarioPerfilEntidadeTO $to
     */
    public function retornarVwUsuarioPerfilEntidade(VwUsuarioPerfilEntidadeTO $to)
    {
        try {

            $dao = new ProjetoPedagogicoDAO();
            $dados = $dao->retornarVwUsuarioPerfilEntidade($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro de Usuário com esse perfil Encontrado!");
            }

            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwUsuarioPerfilEntidadeTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Usuário!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Salva um coordenador verificando se o mesmo já tem cadastro ativo
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarCoordenadorUnico(UsuarioPerfilEntidadeReferenciaTO $to)
    {
        try {

            $vto = new UsuarioPerfilEntidadeReferenciaTO();
            $vto->setId_perfil($to->getId_perfil());
            $vto->setId_usuario($to->getId_usuario());
            $vto->setId_entidade($to->getId_entidade());
            $vto->setId_projetopedagogico($to->getId_projetopedagogico());
            $vto->setId_saladeaula($to->getId_saladeaula());
            $vto->setBl_ativo(true);
            $vto->setId_disciplina($to->getId_disciplina());
            $vto->setId_livro($to->getId_livro());

            $verificacao = $vto->fetch(false, false, false);
            if ($verificacao) {
                foreach ($verificacao as $vto) {
                    if ($vto->getId_perfilReferencia() != $to->getId_perfilReferencia()) {
                        throw new Zend_Exception("Este usuário já possui este perfil!");
                    }
                }
            }

            $this->mensageiro = $this->salvarCoordenador($to);
        } catch (Exception $e) {

            $this->mensageiro->setMensageiro("Erro ao salvar/editar Coordenadores - " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que decide se edita ou salva Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarCoordenador(UsuarioPerfilEntidadeReferenciaTO $to)
    {
        $dao = new ProjetoPedagogicoDAO();
        try {
            $vto = new UsuarioPerfilEntidadeReferenciaTO();
            $vto->montaToDinamico($to->toArray());
            $vto->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $vto->setBl_ativo(false);
            $vto->fetch(false, true, true, true, true);
            $to->setId_entidade($vto->getId_entidade());
            if ($vto->getId_perfilReferencia()) {
                $to->setId_perfilReferencia($vto->getId_perfilReferencia());
                $to->setBl_ativo(true);
                $dao->editarCoordenador($to);
            } else {
                $to->setBl_ativo(true);
                $to->setId_perfilReferencia($dao->salvarCoordenador($to));

                //Se salvou mas não retornou o id_perfilreferencia, pesquisa novamente
                if(empty($to->getId_perfilReferencia())){
                    $to->fetch(false,true,true);
                }
            }
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao salvar/editar Coordenadores - " . $e->getMessage(),
                Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /** Método que seta como titular o de projeto pedagogico ou de disciplinas - integração moodle
     * Apenas 1 pode ser coordenador titular de cada perfil
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function setarCoordenador(UsuarioPerfilEntidadeReferenciaTO $to)
    {

        $dao = new ProjetoPedagogicoDAO();
        $dao->beginTransaction();
        try {
            //pesquisa se tem outros titulares
            $titular = $dao->retornaCoordenadorTitular($to)->toArray();
            if (!empty($titular)) {
                foreach ($titular as $key => $tiTO) {
                    if ($tiTO['id_perfilreferencia'] != $to->getId_perfilreferencia()) {
                        $titular[$key]['bl_titular'] = false;
                        $dao->editarCoordenador($to->encapsularTo($titular, new UsuarioPerfilEntidadeReferenciaTO(), true));
                    }
                }
            }
            $dao->editarCoordenador($to);
            $dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro("Erro ao setar Coordenadore titular - " . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Exclui de forma lógica coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function excluirCoordenador(UsuarioPerfilEntidadeReferenciaTO $to)
    {
        $dao = new ProjetoPedagogicoDAO();
        try {
            $dao->beginTransaction();
            if (!$to->getId_perfilReferencia()) {
                throw new Zend_Exception("Id PerfilReferencia não foi setado");
            }

            $to->setBl_ativo(false);

            $perfilReferenciaInt = new PerfilReferenciaIntegracaoTO();
            $perfilReferenciaInt->setId_perfilreferencia($to->getId_perfilReferencia());
            $perfilReferenciaInt->fetch(false, true, true);

            if ($perfilReferenciaInt->getId_sistema() == SistemaTO::MOODLE) {
                $to->setBl_desativarmoodle(true);
            }

            $dao->editarCoordenador($to);
            $dao->commit();
            $this->mensageiro->setMensageiro('Excluído com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao excluir coordenador ! -  ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna Areas de Conhecimento do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function retornarAreasProjetoPedagogicoUnico(AreaProjetoPedagogicoTO $appTO)
    {
        try {
            $dao = new ProjetoPedagogicoDAO();
            $areaProjeto = $dao->retornarAreasProjetoPedagogicoUnico($appTO)->toArray();
            if (empty($areaProjeto)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($areaProjeto, new AreaProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Areas do Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    /**
     * Método que cadastra a grade curricular de determinado projeto pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function cadastraGradeCurricular(ProjetoPedagogicoTO $ppTO)
    {
        try {
            $dao = new MatriculaDAO();
            $contratoOrm = new ContratoORM();
            $where = $contratoOrm->select()
                ->from(array('c' => 'tb_contrato'), array('c.id_contrato', 'c.id_venda'))
                ->joinInner(array('vp' => 'tb_vendaproduto'), 'c.id_venda =  vp.id_venda')
                ->joinInner(array('m' => 'tb_matricula'), 'm.id_matricula = vp.id_matricula')
                ->joinInner(array('ppp' => 'tb_produtoprojetopedagogico'), 'ppp.id_produto = vp.id_produto')
                ->joinInner(array('pp' => 'tb_projetopedagogico'), 'pp.id_projetopedagogico = ppp.id_projetopedagogico AND m.id_projetopedagogico = pp.id_projetopedagogico')
                ->setIntegrityCheck(false)
                ->where('pp.id_projetopedagogico = ' . $ppTO->getId_projetopedagogico());

            $arrContratoCriarGrade = $dao->retornarContrato(new ContratoTO(), $where)->toArray();
            if (empty($arrContratoCriarGrade)) {
                return $this->mensageiro->setMensageiro('Nenhuma grade para ser criada!', Ead1_IMensageiro::SUCESSO);
            } else {
                $contratos = Ead1_TO_Dinamico::encapsularTo($arrContratoCriarGrade, new ContratoTO());
                $arrayRetorno = array();
                if (count($contratos)) {
                    foreach ($contratos as $contrato) {
                        $arrayRetorno[] = $dao->cadastrarMatriculaDisciplinaSP($contrato->getId_contrato());
                    }
                }
            }
            return $this->mensageiro->setMensageiro('Cadastro de grades curriculares feitos com sucesso!', Ead1_IMensageiro::SUCESSO, $arrayRetorno);
        } catch (Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao cadastrar grade curricular!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
    }

    private function saveLog(Ead1_TO_Dinamico $peTo, $st_tabela, $id_tabela, $st_coluna, $antes = NULL, $depois = false)
    {
        try {
            $negocio = new \G2\Negocio\Log();
            $params = array('id_entidade' => $peTo->getSessao()->id_entidade
            , 'id_usuario' => $peTo->getSessao()->id_usuario
            , 'st_tabela' => $st_tabela
            , 'st_coluna' => $st_coluna
            , 'st_antes' => $antes
            , 'st_depois' => ($depois ? $depois : '{"' . $st_coluna . '":"' . $peTo->{$st_coluna} . '"}')
            , 'id_tabela' => $id_tabela
            , 'id_operacao' => (is_null($antes) ? $negocio->getReference('\G2\Entity\OperacaoLog', \G2\Constante\OperacaoLog::INSERT) :
                    $negocio->getReference('\G2\Entity\OperacaoLog', \G2\Constante\OperacaoLog::UPDATE))
            , 'st_motivo' => null
            , 'dt_cadastro' => new DateTime()
            , 'id_funcionalidade' => null
            , 'id_perfil' => null
            );
            if (is_null($antes) || ($params['st_antes'] != $params['st_depois']))
                $negocio->salvarLogIndividual($params);
        } catch (Exception $e) {
            return false;
        }
    }
}

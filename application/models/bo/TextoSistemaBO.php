<?php

class TextoSistemaBO extends Ead1_BO
{

    const HTML = 'html';
    const PDF = 'pdf';

    /**
     * Substitui todas as variaveis iguais por um unico valor
     * @var NULL
     */
    const MATCH_ALL = 0;

    /**
     * Substitui as variaveis iguais uma a uma com um valor correspondente
     * @var int
     */
    const MATCH_ONE_PER_ONE = 1;

    // usado para controle de gêneros, a chave o termo masculino e o valor o termo feminino
    private static $termosGenero = array(
        'nascido' => 'nascida',
        'Diplomado' => 'Diplomada',
        'TECNÓLOGO' => 'TECNÓLOGA',
        'BACHAREL' => 'BACHARELA',
        'LICENCIADO' => 'LICENCIADA',
        'ESPECIALISTA' => 'ESPECIALISTA',
        'MESTRE' => 'MESTRE',
        'DOUTOR' => 'DOUTORA'
    );

    /**
     * Método construtor
     */
    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->_bo = new TextosBO();
        $this->_tipoSaida = TextoSistemaBO::HTML;
        $this->_view = Zend_Registry::get('view');
        $this->_params = array();
        $this->_negocio = new \G2\Negocio\TextoSistema();
    }

    /**
     * Tipo da Saida
     * @var String
     */
    private $_tipoSaida;

    /**
     * Contem os parametros recebidos
     * - Evite Usar!
     * @var array
     */
    private $_params;

    /**
     * Contem a instancia do helper da view
     * @var Zend_View
     */
    private $_view;

    /**
     * Contem a Instancia do BO de Textos
     * @var TextosBO
     */
    private $_bo;

    /**
     * Contém a instancia do TextoSistema
     * @var \G2\Negocio\TextoSistema
     */
    private $_negocio;

    /**
     * Método que gera o Texto Sistema em HTML
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarHTML(TextoSistemaTO $textoSistemaTO, array $params, $textoComEntidade = false)
    {
        try {

            //POR-22: Movendo alteração do path para antes do código que gera o texto.

            /**
             * PHTML encontrado em três lugares
             * \application\apps\loja\views\scripts\textos
             * \application\apps\gestor2\views\scripts\textos -------- UNICO LUGAR USADO, OS OUTROS PODEM SER IGNORADOS
             * \application\apps\unico\views\scripts\textos
             * Feito assim para funcionar tambem no endereço http://loja.[dominio]
             */

            $view = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer')->view;
            $view->addScriptPath(APPLICATION_PATH . '/apps/default/views/scripts');

            $this->_tipoSaida = TextoSistemaBO::HTML;
            $this->gerarTexto($textoSistemaTO, $params, $textoComEntidade)->subtractMensageiro();

            $this->_view->corpo = $this->mensageiro->getFirstMensagem()->getSt_texto();
            if ($this->mensageiro->getFirstMensagem()->getId_textocategoria() == \G2\Constante\TextoCategoria::DECLARACOES) {
                $this->_view->id_textocategoria = $this->mensageiro->getFirstMensagem()->getId_textocategoria();
                $this->_view->cabecalho = $this->adicionaCabecalhoRodape($textoSistemaTO, \G2\Constante\TextoCategoria::CABECALHO);
                $this->_view->rodape = $this->adicionaCabecalhoRodape($textoSistemaTO, \G2\Constante\TextoCategoria::RODAPE);
            }
            $html = $this->_view->render('textos/geracao.phtml');
            $this->mensageiro->getFirstMensagem()->setSt_texto($html);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO, $this->mensageiro->getCodigo());
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Método que gera o Texto Sistema em PDF
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @param String $filename
     * @param string $caminhoArquivo Caminho do PDF a ser
     * @return Ead1_Mensageiro
     */
    public function gerarPDF(TextoSistemaTO $textoSistemaTO, array $params, $filename, $caminhoArquivo = null,
                             $isRecibo = false)
    {
        try {
            $this->_tipoSaida = TextoSistemaBO::PDF;
            $this->mensageiro = $this->gerarTexto($textoSistemaTO, $params)->subtractMensageiro();
            $this->mensageiro->filename = $filename . '_' . date('d_m_Y_H_i_s') . '.pdf';

            if ($this->mensageiro->getFirstMensagem()->getId_textocategoria() == \G2\Constante\TextoCategoria::DECLARACOES) {
                $this->_view->corpo = $this->limpaHtmlCabecalhoRodape($this->mensageiro->getFirstMensagem()->getSt_texto());
            } else {
                $this->_view->corpo = $this->mensageiro->getFirstMensagem()->getSt_texto();
                $orientacao = 'p';
            }

            if ($textoSistemaTO->id_orientacaotexto == OrientacaoTextoTO::PAISAGEM) {
                $orientacao = 'l';
            }

            if ($this->mensageiro->getFirstMensagem()->getId_textocategoria()
                == \G2\Constante\TextoCategoria::DECLARACOES) {

                $this->_view->id_textocategoria = $this->mensageiro->getFirstMensagem()->getId_textocategoria();
                $this->_view->cabecalho =
                    $this->limpaHtmlCabecalhoRodape($this->adicionaCabecalhoRodape($textoSistemaTO,
                        \G2\Constante\TextoCategoria::CABECALHO));
                $this->_view->rodape =
                    $this->limpaHtmlCabecalhoRodape($this->adicionaCabecalhoRodape($textoSistemaTO,
                        \G2\Constante\TextoCategoria::RODAPE));
            }

            $ead1_IMPdf = new Ead1_IMPdf();
            $ead1_IMPdf->configuraPapel('A4', $orientacao);

            if ($this->mensageiro->getFirstMensagem()->getId_textocategoria() == \G2\Constante\TextoCategoria::DECLARACOES) {
                $ead1_IMPdf->carregarHTML($this->_view->render('textos/geracao-declaracao-pdf.phtml'));
            } else {
                $template = 'textos/geracao-pdf.phtml';
                if ($isRecibo) {
                    $template = 'textos/geracao-recibo-pdf.phtml';
                }
                $ead1_IMPdf->carregarHTML($this->_view->render($template));
            }

            $ead1_IMPdf->gerarPdf($filename . '_' . date('d_m_Y_H_i_s') . '.pdf', $caminhoArquivo, 'd');

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO, $this->mensageiro->getCodigo());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->mensageiro->getCodigo());
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao gerar PDF.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que gera o Texto
     * @param TextoSistemaTO $textoSistemaTO
     * @see gerarHTML | gerarPDF
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarTexto(TextoSistemaTO $textoSistemaTO, array $params, $textoComEntidade = false)
    {
        try {

            $this->_params = $params;

            if ($textoComEntidade) {
                $textoSistemaTO = $this->_bo->retornarTextoSistema($textoSistemaTO, true)->subtractMensageiro()->getFirstMensagem();
            } else {
                $textoSistemaTO = $this->_retornarTextoSistemaSemEntidade($textoSistemaTO)->subtractMensageiro()->getFirstMensagem();
            }

            $textoCategoriaTO = new TextoCategoriaTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria()));
            $textoCategoriaTO = $this->_retornarTextoCategoria($textoCategoriaTO)->subtractMensageiro()->getFirstMensagem();
            $metodo = $textoCategoriaTO->getSt_metodo();

            if (!in_array($metodo, get_class_methods(__CLASS__))) {
                throw new Zend_Validate_Exception("O Método de geração do Texto da Categoria não Existe.");
            }

            $textoSistemaTO = $this->$metodo($textoSistemaTO, $params)->subtractMensageiro()->getFirstMensagem();
            if ($textoSistemaTO->getid_textoexibicao() == TextoExibicaoTO::SMS) {
                $textoSistemaTO->setSt_texto($textoSistemaTO->getSt_texto());
            } else {
                $htmlConversor = new Ead1_ConversorFlexHTML();
                $textoSistemaTO->setSt_texto($htmlConversor->toHTML($textoSistemaTO->getSt_texto()));
            }

            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->mensageiro->getCodigo());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo da categoria de Agendamento que gera textos de mail
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarTextoAgendamento(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            $vwGerarTextoAgendamentoTO = new VwGerarTextoAgendamentoTO($params);

            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $vwGerarTextoAgendamentoTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao gerar agendamento! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }
//
//    public function gerarTextoCupomPremio(TextoSistemaTO $textoSistemaTO, array $params){
//        $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
//
//        $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), Ead1_TO , $arrTextoVariaveisTO, $params);
//        $textoSistemaTO->setSt_texto($textoSubstituido);
//        $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
//    }

    /**
     * Método que substitui as variaveis do Texto Pelos valores das variaveis do array
     * @param array $variaveis - Variaveis a serem substituidas
     * @param array $substituir - Valores a Substituirem as variaveis
     * @param String $st_texto
     * @param int $match - Indica se ira substituir as variaveis por um unico valor (ALL) ou por varios valores (ONE_PER_ONE)
     * @return String
     */
    public function substituiTextoVariaveis(array $variaveis, array $substituir, $st_texto, $match, $genero = null)
    {
        if (count($variaveis) != count($substituir)) {
            throw new Zend_Exception("O Número de Variaveis a Substituir é Diferente do Número de Valores a Serem Substituidos");
        }
        $text = '';
        //Monta a expressÃ£o
        foreach ($variaveis as $index => $variavel) {
            $variaveis[$index] = '/' . $variavel . '/';
        }
        switch ($match) {
            case self::MATCH_ALL:
                $text = preg_replace($variaveis, $substituir, $st_texto);
                break;
            case self::MATCH_ONE_PER_ONE:
                $text = preg_replace($variaveis, $substituir, $st_texto, 1);
                break;
        }

        if ($genero) {
            // Ajustar concordância
            $text = str_replace(array(
                'curso de Superior',
                'CURSO DE Superior',
                'curso de SUPERIOR',
                'CURSO DE SUPERIOR'
            ), array(
                'curso Superior',
                'CURSO Superior',
                'curso SUPERIOR',
                'CURSO SUPERIOR'
            ), $text);

            if ($genero == 'F') {
                // subistituir termos por gênero
                foreach (self::$termosGenero as $k => $value) {
                    $text = preg_replace("/\b{$k}\b/u", $value, $text);
                }
            }
        }

        return $text;
    }

    /**
     * Método que retorna as variaveis que estÃ£o no texto
     * @param String $st_texto
     * @param array $variaveisEncontrar - Variaveis a serem encontradas no texto
     * @param Boolean $bl_unica - Indica se retorna a variavel 1 vez independente de quantas vezes
     *          tenha aparecido no texto (true), ou se retorna a variavel referente ao numero de vezes
     *          que aparece no texto
     * @return array $variaveisEncontradas
     */
    public function retornarVariaveisTexto($st_texto, array $variaveisEncontrar, $bl_unica = true)
    {
        $variaveisEncontradas = array();
        if ($bl_unica) {
            foreach ($variaveisEncontrar as $index => $variavel) {
                preg_match('/' . $variavel . '/', $st_texto, $encontrado, PREG_OFFSET_CAPTURE);
                if (!empty($encontrado)) {
                    $variaveisEncontradas[$index] = $variavel;
                }
            }
        } else {
            foreach ($variaveisEncontrar as $variavel) {
                preg_match_all('/' . $variavel . '/', $st_texto, $encontrado, PREG_PATTERN_ORDER);
                if (!empty($encontrado[0])) {
                    foreach ($encontrado[0] as $location => $val) {
                        $variaveisEncontradas[] = $variavel;
                    }
                }
            }
        }
        return $variaveisEncontradas;
    }

    /**
     * Metodo que retorna o link para pagemento da venda
     * @param Int $id_venda
     * @return Number
     */
    public function gerarLinkCartaoVenda($id_venda)
    {

        if (!$id_venda) {
            return '#link_cartao#';
        }
        return "<a href='" . Ead1_Ambiente::geral()->st_url_loja . "/loja/pagamento/?id_venda=" . $id_venda . "' target='_blank'>Efetuar pagamento via CartÃ£o de Crédito</a><br>";
    }

    /**
     * Metodo que retorna o link para pagemento de cartão de credito do acordo
     * @param Int $id_venda
     * @return Number
     */
    public function gerarLinkCartaoVendaAcordo($id_venda, $arr)
    {
        if (!$id_venda && !$arr['id_acordo']) {
            return '#link_cartao_acordo#';
        }
        //array com os parametros
        $arrParamsLinkPagamento = array(
            'id_venda' => $arr['id_venda'],
            'id_acordo' => $arr['id_acordo'],
            'id_lancamento' => $arr['id_lancamento']);
        $paramsLinkPagamento = json_encode($arrParamsLinkPagamento);

        $securityKey = Zend_Registry::get('config')->security->key;
        $paramsCifrado = \util\Cipher::encrypt($paramsLinkPagamento, $securityKey);

        return "<a href='" . Ead1_Ambiente::geral()->st_url_loja . "/loja/recebimento/negociacao/?key=" . $paramsCifrado . "' target='_blank'>Efetuar pagamento via Cartão de Crédito</a><br>";
    }

    /**
     * Metodo que retorna os links de Boleto
     * @param Int $id_venda
     * @return Number
     */
    public function gerarLinkBoletoLancamento($id_venda)
    {

        if (!$id_venda) {
            return '#link_boleto#';
        }

        $lancamentoTO = new VwLancamentoTO();
        $lancamentoTO->setId_venda($id_venda);
        $lancamentoTO->setId_meiopagamento(MeioPagamentoTO::BOLETO);

        $lancamentoORM = new VwLancamentoORM();
        $arrLancamento = $lancamentoORM->consulta($lancamentoTO, false, false, array('dt_vencimento'));

        if (empty($arrLancamento)) {
            return '#link_boleto#';
        }

        $linkBoleto = false;
        foreach ($arrLancamento as $lanTO) {
            $linkBoleto .= "<a href='" . Ead1_Ambiente::geral()->st_url_loja . "/loja/pagamento/boleto?id_lancamento=" .
                $lanTO->getId_lancamento() . "' target='_blank'>Emitir Boleto - Vencimento " .
                ($lanTO->getDt_vencimento() ? $lanTO->getDt_vencimento()->toString('d/M/Y') : "") . "</a><br>";
        }
        if (!$linkBoleto) {
            return '#link_boleto#';
        }

        return $linkBoleto;
    }

    /**
     * Metodo que retorna os links de Boleto de uma unica parcela
     * @param Int $id_lancamento
     * @return string
     */
    public function gerarLinkBoletoParcela($id_lancamento, array $param = null)
    {
        $id_lancamento = array_key_exists('id_lancamento', $param) && $param['id_lancamento'] ? $param['id_lancamento'] : $id_lancamento;
        if (!$id_lancamento) {
            return '#link_boleto_parcela#';
        }

        $lancamentoTO = new LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);
        $lancamentoTO->fetch(true, true, true);

        if (!($lancamentoTO->getId_lancamento())) {
            return '#link_boleto_parcela#';
        }

        $linkBoleto = "<a href='" . Ead1_Ambiente::geral()->st_url_loja . "/loja/pagamento/boleto?id_lancamento=" .
            $lancamentoTO->getId_lancamento() . "' target='_blank'>Emitir Boleto - Vencimento " .
            ($lancamentoTO->getDt_vencimento() ? $lancamentoTO->getDt_vencimento()->toString('d/M/Y') : "") . "</a><br>";

        return $linkBoleto;
    }

    /**
     * Método que substitui as variaveis de acordo com as variaveis encontradas e os atributos do Método
     * @param string $st_texto
     * @param Ead1_TO $to
     * @param array $arrTextoVariaveisTO
     * @param array $params
     * @return string
     * @throws Zend_Exception
     */
    public function substituirVariaveisTO($st_texto, Ead1_TO $to, array $arrTextoVariaveisTO, array $params, $genero = null)
    {
        //Zend_Debug::dump($params,__CLASS__.'('.__LINE__.')'); exit;
        $variaveisEncontrar = array();
        foreach ($arrTextoVariaveisTO as $textoVariaveisTO) {
            if (!($textoVariaveisTO instanceof TextoVariaveisTO)) {
                throw new Zend_Exception("Um dos Itens do Array de Variaveis não é uma instancia de TextoVariaveisTO.");
            }
            $variaveisEncontrar[] = $textoVariaveisTO->getSt_textovariaveis();
        }
        $arrVariaveisEncontradas = $this->retornarVariaveisTexto($st_texto, $variaveisEncontrar);
        $variaveis = array();
        $substituir = array();
        $variaveisDinamicas = array();
        $substituirDinamicas = array();
        $variaveisTO = get_object_vars($to);
        foreach ($arrTextoVariaveisTO as $index => $textoVariaveisTO) {
            $mascara = $textoVariaveisTO->getSt_mascara();
            $variavelSubstituicao = $textoVariaveisTO->getSt_textovariaveis();

            switch ($textoVariaveisTO->getId_tipotextovariavel()) {
                case TipoTextoVariavelTO::SIMPLES:
                case TipoTextoVariavelTO::COMPLEXA:
                    $campo = $textoVariaveisTO->getSt_camposubstituir();
                    if (array_key_exists($campo, $variaveisTO)) {
                        $valor = $variaveisTO[$campo];

                        if (in_array($variavelSubstituicao, $arrVariaveisEncontradas)) {
                            if ($mascara) {
                                $valor = $this->$mascara($variaveisTO[$campo]);
                            }

                            // Verificar se a variável possui subvariáveis
                            if (trim($valor) != trim($variavelSubstituicao) && substr_count($valor, '#') >= 2) {
                                $valor = $this->substituirVariaveisTO($valor, $to, $arrTextoVariaveisTO, $params);
                            }

                            //echo $variavelSubstituicao." - ".$campo."[".$valor."]<br>";
                            $substituir[$index] = $valor;
                            $variaveis[$index] = $variavelSubstituicao;
                        }
                    }
                    break;
                case TipoTextoVariavelTO::SELECAO:
                case TipoTextoVariavelTO::INSERCAO:
                    if (array_key_exists('variaveis', $params)) {
                        //A Variavel não pode vir com ## ou o indice não aparece
                        if (array_key_exists($textoVariaveisTO->getSt_textovariaveis(), $params['variaveis'])) {
                            if (array_search($variavelSubstituicao, $arrVariaveisEncontradas)) {
                                ksort($params['variaveis'][$textoVariaveisTO->getSt_textovariaveis()]);
                                foreach ($params['variaveis'][$textoVariaveisTO->getSt_textovariaveis()] as $val) {
                                    $substituirDinamicas[] = $val;
                                    $variaveisDinamicas[] = $variavelSubstituicao;
                                }
                            }
                        }
                    }
                    break;
                case TipoTextoVariavelTO::MULTIPLOS_PARAMETROS:
                    $campo = $textoVariaveisTO->getSt_camposubstituir();
                    if (array_key_exists($campo, $variaveisTO)) {
                        $valor = $variaveisTO[$campo];
                        if (in_array($variavelSubstituicao, $arrVariaveisEncontradas)) {
                            if ($mascara) {
                                $valor = $this->$mascara($variaveisTO[$campo], $params);
                            }
                            $substituir[$index] = $valor;
                            $variaveis[$index] = $variavelSubstituicao;
                        }
                    }
                    break;


                default:
                    throw new Zend_Exception("Tipo de Variavel não Implementada!");
                    break;
            }

        }

        $st_texto = $this->substituiTextoVariaveis($variaveis, $substituir, $st_texto, self::MATCH_ALL, $genero);
        return $this->substituiTextoVariaveis($variaveisDinamicas, $substituirDinamicas, $st_texto, self::MATCH_ONE_PER_ONE);
    }

    /**
     * Método das Categorias Matricula, Declaracoes e Certificado que gera Declaracoes
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */

    public function gerarDeclaracao(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {

            if (!array_key_exists('id_matricula', $params)) {
                throw new Zend_Exception('Para o método gerarDeclaracao funcionar é necessario enviar o parametro id_matricula no array de parametros!');

            }

            $vwGerarDeclaracaoTO = new VwGerarDeclaracaoTO();
            $vwGerarDeclaracaoTO->setId_matricula((int)$params['id_matricula']);

            if (!empty($params['id_disciplinaagendamento'])) {
                $vwGerarDeclaracaoTO->setId_disciplinaagendamento($params['id_disciplinaagendamento']);
            }

            /** @var vwGerarDeclaracaoTO $vwGerarDeclaracaoTO */
            $vwGerarDeclaracaoTO = $this->_bo
                ->retornarVwGerarDeclaracao($vwGerarDeclaracaoTO)
                ->subtractMensageiro()
                ->getFirstMensagem();

            if (!empty($params["id_saladeaula"])) {
                $alocarNegocio = new \G2\Negocio\Alocacao();

                //Função responsável por buscar os dados da sala e da disciplina, para fazer a substituição das variaveis de texto
                $stDisciplina = $alocarNegocio->retornaDisciplinaSala($params["idMatDisc"]);

                $vwGerarDeclaracaoTO->st_disciplina = $stDisciplina["st_disciplina"];
                $vwGerarDeclaracaoTO->dt_abertura = $stDisciplina["dt_abertura"];
                $vwGerarDeclaracaoTO->dt_encerramento = $stDisciplina["dt_encerramento"];
            }

            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $vwGerarDeclaracaoTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Declaração! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Metodo da categoria de OcorrÃªncia que gera textos de ocorrÃªncias
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarOcorrencia(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            if (!array_key_exists('id_ocorrencia', $params)) {
                throw new Zend_Exception('Para o Método gerarOcorrencia funcionar é necessario enviar o parametro id_ocorrencia no array de parÃ¢metros!');
            }
            $vwGerarOcorrenciaTO = new VwGerarOcorrenciaTO(array('id_ocorrencia' => $params['id_ocorrencia']));
            $vwGerarOcorrenciaTO = $this->_bo->retornarVwGerarOcorrencia($vwGerarOcorrenciaTO)->subtractMensageiro()->getFirstMensagem();
            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $vwGerarOcorrenciaTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Contrato! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo da categoria ClasseAfiliados que gera textos das Classes de Afiliados
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarClasseAfiliado(TextoSistemaTO $textoSistemaTO, array $params)
    { //id_textosistemacontrato
        try {
            if (!array_key_exists('id_classeafiliado', $params)) {
                throw new Zend_Exception('Para o Método gerarClasseAfiliado funcionar é necessario enviar o parametro id_classeafiliado no array de parÃ¢metros!');
            }
            $classeAfiliadoTO = new ClasseAfiliadoTO(array('id_classeafiliado'));
            $classeAfiliadoTO = $this->_bo->retornarClasseAfiliado($classeAfiliadoTO)->subtractMensageiro()->getFirstMensagem();
            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $classeAfiliadoTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Texto Classe Afiliado! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo da categoria de Contrato que gera textos de contratos
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarContrato(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            if (!array_key_exists('id_contrato', $params)) {
                throw new Zend_Exception(
                    'Para o Método gerarContrato funcionar é necessario enviar o parametro id_contrato no array de parâmetros!'
                );
            }

            $vwGerarContratoTO = new VwGerarContratoTO(array(
                'id_contrato' => $params['id_contrato'],
                'id_tipocontratoresponsavel_financeiro' => TipoContratoResponsavelTO::TIPO_CONTRATO_RESPONSAVEL_FINANCEIRO
            ));

            $vwGerarContratoTO = $this->_bo->retornarVwGerarContrato($vwGerarContratoTO)
                ->subtractMensageiro()
                ->getFirstMensagem();

            $txVariavel = new TextoVariaveisTO(array(
                'id_textocategoria' => $textoSistemaTO->getId_textocategoria()
            ));
            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis($txVariavel)
                ->subtractMensageiro()
                ->getMensagem();

            $textoSubstituido = $this->substituirVariaveisTO(
                $textoSistemaTO->getSt_texto(),
                $vwGerarContratoTO,
                $arrTextoVariaveisTO,
                $params
            );
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Contrato! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Método da Categoria Dados de Acesso que gera textos de dados de acesso da pessoa
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarDadosPessoais(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            if (!array_key_exists('id_usuario', $params) || !array_key_exists('id_entidade', $params)) {
                throw new Zend_Exception('Para o Método gerarDadosPessoais funcionar é necessario enviar o parametro id_usuario e id_entidade no array de parametros!');
            }
            $vwDadosPessoaisTO = $this->_bo->retornarVwDadosPessoais(new VwDadosPessoaisTO(array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade'])))
                ->subtractMensageiro()
                ->getFirstMensagem();
            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();
            if (array_key_exists('st_senha', $params)):
                foreach ($arrTextoVariaveisTO as $key => $vars) {
                    if ($vars->getSt_textovariaveis() == '#nova_senha#')
                        unset($arrTextoVariaveisTO[$key]);
                }
                $textoSistemaTO->setSt_texto(str_replace('#nova_senha#', $params['st_senha'], $textoSistemaTO->getSt_texto()));
            endif;
            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $vwDadosPessoaisTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Contrato! ', Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo da Categoria de Venda que gera textos de Venda
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarVenda(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {

            //Zend_Debug::dump($params,__CLASS__.'('.__LINE__.')'); exit;
            if (!array_key_exists('id_venda', $params)) {
                throw new Zend_Exception(
                    'Para o método gerarVenda funcionar e necessário enviar o parâmetro id_venda 
                        no array de parâmetros!'
                );
            }

            $pesquisa = array('id_venda' => $params['id_venda']);
            $where = ' id_venda = ' . $params['id_venda'];

            if (array_key_exists('id_lancamento', $params)) {
                $pesquisa['id_lancamento'] = $params['id_lancamento'];
                $where .= ' AND id_lancamento = ' . $params['id_lancamento'];
            }

            if (array_key_exists('id_acordo', $params)) {
                $pesquisa['id_acordo'] = $params['id_acordo'];
                $where .= ' AND id_acordo = ' . $params['id_acordo'];
            }

            if (array_key_exists('dt_inicio', $params)) {
                $pesquisa['dt_inicio'] = $params['dt_inicio'];
                $data_inicio = new Zend_Date($params['dt_inicio'], 'd/m/Y');
                $where .= " AND dt_lancamentodataquitado >= '" . $data_inicio->toString('Y-m-d') . "'";
            }

            if (array_key_exists('dt_termino', $params)) {
                $pesquisa['dt_termino'] = $params['dt_termino'];
                $data_termino = new Zend_Date($params['dt_termino'], 'd/m/Y');
                $where .= " AND dt_lancamentodataquitado <= '" . $data_termino->toString('Y-m-d') . "'";
            }

            $dadosTO =
                $this->_bo->retornarVwGerarVendaTexto(
                    new VwGerarVendaTextoTO($pesquisa),
                    $where
                )->subtractMensageiro()
                    ->getFirstMensagem();

            $arrTextoVariaveisTO =
                $this->_bo->retornarTextoVariaveis(
                    new TextoVariaveisTO(
                        array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())
                    )
                )->subtractMensageiro()
                    ->getMensagem();

            $textoSubstituido =
                $this->substituirVariaveisTO(
                    $textoSistemaTO->getSt_texto(),
                    $dadosTO,
                    $arrTextoVariaveisTO,
                    $params
                );

            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro(
                'Erro ao Gerar Contrato! ',
                Ead1_IMensageiro::ERRO,
                $e->getMessage()
            );
        }
        return $this->mensageiro;
    }

    /**
     * Método que verifica as Variaveis Dinamicas do Texto para Retornar o Factory de Filtros
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function retornaFactoryVariaveisDinamicas(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            $this->_params = $params;
            $textoSistemaTO = $this->_retornarTextoSistemaSemEntidade($textoSistemaTO)->subtractMensageiro()->getFirstMensagem();
            try {
                $arrVariaveisSelecao = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria(), 'id_tipotextovariavel' => TipoTextoVariavelTO::SELECAO)))
                    ->subtractMensageiro()
                    ->getMensagem();
            } catch (Zend_Validate_Exception $e) {
                $arrVariaveisSelecao = array();
            }
            try {
                $arrVariaveisInsercao = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria(), 'id_tipotextovariavel' => TipoTextoVariavelTO::INSERCAO)))
                    ->subtractMensageiro()
                    ->getMensagem();
            } catch (Zend_Validate_Exception $e) {
                $arrVariaveisInsercao = array();
            }
            $variaveis = array_merge($arrVariaveisInsercao, $arrVariaveisSelecao);
            $arrFiltros = array();
            $a = true;
            foreach ($variaveis as $textoVariaveisTO) {
                $encontrar = array($textoVariaveisTO->getSt_textovariaveis());
                $encontrou = $this->retornarVariaveisTexto($textoSistemaTO->getSt_texto(), $encontrar, false);
                if (!empty($encontrou)) {
                    $quantidade = count($encontrou);
                    $mascara = $textoVariaveisTO->getSt_mascara();
                    $filtro = $this->$mascara($params, $textoVariaveisTO->getSt_textovariaveis());
                    for ($i = 0; $i < $quantidade; $i++) {
                        $cloneFiltro = clone $filtro;
                        $cloneFiltro->setSt_name($filtro->getSt_variavel() . '_' . $i);
                        $arrFiltros[] = $cloneFiltro;
                    }
                }
            }
            $this->mensageiro->setMensageiro($arrFiltros, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Verificar Variaveis do Texto.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna o filtro de seleção das leis
     * @param array $params
     * @param String $variavel - de Substituicao
     * @throws Zend_Exception
     * @return FiltroTextoTO $filtroTextoTO
     */
    protected function retornaSelecaoLei(array $params, $variavel)
    {
        if (!array_key_exists('id_matricula', $params)) {
            throw new Zend_Exception("Para A Variavel de seleção de Lei Funcionar e necessario Informar a Matricula.");
        }
        $filtroTextoTO = new FiltroTextoTO();
        $selecao = array();
        $fundamentoLegalBO = new FundamentoLegalBO();
        $fundamentoTO = new VwFundamentoMatriculaTO();
        $fundamentoTO->setId_matricula($params['id_matricula']);
        $fundamentoTO->setId_tipofundamentolegal(TipoFundamentoLegalTO::LEI);
        $mensageiro = $fundamentoLegalBO->retornarVwFundamentoMatricula($fundamentoTO);
        switch ($mensageiro->getTipo()) {
            case Ead1_IMensageiro::SUCESSO:
                foreach ($mensageiro->getMensagem() as $index => $mensagem) {
                    $selecao[$index]['label'] = $mensagem->getNu_numero();
                    $selecao[$index]['valor'] = $mensagem->getNu_numero();
                }
                break;
            case Ead1_IMensageiro::AVISO:
                $selecao[0]['label'] = 'Nenhum Registro Encontrado.';
                $selecao[0]['valor'] = 0;
                break;
            case Ead1_IMensageiro::ERRO:
                $selecao[0]['label'] = 'Erro ao Retornar Registro.';
                $selecao[0]['valor'] = 0;

                break;
        }
        $lastIndex = count($selecao);
        $selecao[$lastIndex]['label'] = '---';
        $selecao[$lastIndex]['valor'] = '---';
        $filtroTextoTO->setFiltro('Lei:', 'nu_numero', Ead1_IFlex::DropDownList, $variavel, $selecao);
        return $filtroTextoTO;
    }

    /**
     * Método que retorna o filtro de seleção das ResoluÃ§Ãµes
     * @param array $params
     * @param String $variavel - de substituicao
     * @throws Zend_Exception
     * @return FiltroTextoTO $filtroTextoTO
     */
    protected function retornaSelecaoResolucao(array $params, $variavel)
    {
        if (!array_key_exists('id_matricula', $params)) {
            throw new Zend_Exception("Para A Variavel de seleção de Resolucao Funcionar é necessário Informar a Matricula.");
        }
        $filtroTextoTO = new FiltroTextoTO();
        $selecao = array();
        $fundamentoLegalBO = new FundamentoLegalBO();
        $fundamentoTO = new VwFundamentoMatriculaTO();
        $fundamentoTO->setId_matricula($params['id_matricula']);
        $fundamentoTO->setId_tipofundamentolegal(TipoFundamentoLegalTO::RESOLUCAO);
        $mensageiro = $fundamentoLegalBO->retornarVwFundamentoMatricula($fundamentoTO);
        switch ($mensageiro->getTipo()) {
            case Ead1_IMensageiro::SUCESSO:
                foreach ($mensageiro->getMensagem() as $index => $mensagem) {
                    $selecao[$index]['label'] = $mensagem->getNu_numero();
                    $selecao[$index]['valor'] = $mensagem->getNu_numero();
                }
                break;
            case Ead1_IMensageiro::AVISO:
                $selecao[0]['label'] = 'Nenhum Registro Encontrado.';
                $selecao[0]['valor'] = 0;
                break;
            case Ead1_IMensageiro::ERRO:
                $selecao[0]['label'] = 'Erro ao Retornar Registro.';
                $selecao[0]['valor'] = 0;

                break;
        }
        $lastIndex = count($selecao);
        $selecao[$lastIndex]['label'] = '---';
        $selecao[$lastIndex]['valor'] = '---';
        $filtroTextoTO->setFiltro('ResoluÃ§Ã£o:', 'nu_numero', Ead1_IFlex::DropDownList, $variavel, $selecao);
        return $filtroTextoTO;
    }

    /**
     * Método que retorna o filtro de seleção dos Pareceres
     * @param array $params
     * @param $variavel - de substituicao
     * @throws Zend_Exception
     * @return FiltroTextoTO $filtroTextoTO
     */
    protected function retornaSelecaoParecer(array $params, $variavel)
    {
        if (!array_key_exists('id_matricula', $params)) {
            throw new Zend_Exception("Para A Variavel de seleção de Parecer Funcionar é necessário Informar a Matricula.");
        }
        $filtroTextoTO = new FiltroTextoTO();
        $selecao = array();
        $fundamentoLegalBO = new FundamentoLegalBO();
        $fundamentoTO = new VwFundamentoMatriculaTO();
        $fundamentoTO->setId_matricula($params['id_matricula']);
        $fundamentoTO->setId_tipofundamentolegal(TipoFundamentoLegalTO::PARECER);
        $mensageiro = $fundamentoLegalBO->retornarVwFundamentoMatricula($fundamentoTO);
        switch ($mensageiro->getTipo()) {
            case Ead1_IMensageiro::SUCESSO:
                foreach ($mensageiro->getMensagem() as $index => $mensagem) {
                    $selecao[$index]['label'] = $mensagem->getNu_numero();
                    $selecao[$index]['valor'] = $mensagem->getNu_numero();
                }
                break;
            case Ead1_IMensageiro::AVISO:
                $selecao[0]['label'] = 'Nenhum Registro Encontrado.';
                $selecao[0]['valor'] = 0;
                break;
            case Ead1_IMensageiro::ERRO:
                $selecao[0]['label'] = 'Erro ao Retornar Registro.';
                $selecao[0]['valor'] = 0;

                break;
        }
        $lastIndex = count($selecao);
        $selecao[$lastIndex]['label'] = '---';
        $selecao[$lastIndex]['valor'] = '---';
        $filtroTextoTO->setFiltro('Parecer:', 'nu_numero', Ead1_IFlex::DropDownList, $variavel, $selecao);
        return $filtroTextoTO;
    }

    /**
     * Método que retorna o filtro de seleção das portarias
     * @param array $params
     * @param String $variavel - de substituicao
     * @throws Zend_Exception
     * @return FiltroTextoTO $filtroTextoTO
     */
    protected function retornaSelecaoPortaria(array $params, $variavel)
    {
        if (!array_key_exists('id_matricula', $params)) {
            throw new Zend_Exception("Para A Variavel de seleção de Portaria Funcionar é necessário Informar a Matricula.");
        }
        $filtroTextoTO = new FiltroTextoTO();
        $selecao = array();
        $fundamentoLegalBO = new FundamentoLegalBO();
        $fundamentoTO = new VwFundamentoMatriculaTO();
        $fundamentoTO->setId_matricula($params['id_matricula']);
        $fundamentoTO->setId_tipofundamentolegal(TipoFundamentoLegalTO::PORTARIA);
        $mensageiro = $fundamentoLegalBO->retornarVwFundamentoMatricula($fundamentoTO);
        switch ($mensageiro->getTipo()) {
            case Ead1_IMensageiro::SUCESSO:
                foreach ($mensageiro->getMensagem() as $index => $mensagem) {
                    $selecao[$index]['label'] = $mensagem->getNu_numero();
                    $selecao[$index]['valor'] = $mensagem->getNu_numero();
                }
                break;
            case Ead1_IMensageiro::AVISO:
                $selecao[0]['label'] = 'Nenhum Registro Encontrado.';
                $selecao[0]['valor'] = 0;
                break;
            case Ead1_IMensageiro::ERRO:
                $selecao[0]['label'] = 'Erro ao Retornar Registro.';
                $selecao[0]['valor'] = 0;

                break;
        }
        $lastIndex = count($selecao);
        $selecao[$lastIndex]['label'] = '---';
        $selecao[$lastIndex]['valor'] = '---';
        $filtroTextoTO->setFiltro('Portaria:', 'nu_numero', Ead1_IFlex::DropDownList, $variavel, $selecao);
        return $filtroTextoTO;
    }

    /**
     * Método que retorna o filtro de inserÃ§Ã£o de observaÃ§Ã£o
     * @param array $params
     * @param String $variavel - de substituicao
     * @return FiltroTextoTO
     */
    protected function retornaInsercaoObservacao(array $params, $variavel)
    {
        $filtroTextoTO = new FiltroTextoTO();
        $filtroTextoTO->setFiltro('ObservaÃ§Ã£o:', 'st_observacao', Ead1_IFlex::TextInput, $variavel);
        return $filtroTextoTO;
    }

    /**
     *
     * Métodos de Mascara de Variaveis
     *
     */

    /**
     * Metodo que retorna e gera o historico do aluno
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridHistorico($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistorico é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->_retornaArrGridHistoricoGeral($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridhistorico.phtml');
    }

    /**
     * Metodo que retorna e gera o historico de disciplinas do aluno
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridHistoricoDisciplinas($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistoricoDisciplinas é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->_retornaArrGridHistoricoGeral($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridhistoricodisciplinas.phtml');
    }

    /**
     * Metodo que retorna e gera o historico de disciplinas do aluno
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridHistoricoEscolarDisciplinas($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistoricoEscolarDisciplinas é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->retornarArrGridHistoricoEscolar($id_matricula, true);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridhistoricoescolardisciplinas.phtml');
    }

    /**
     * Metodo que retorna e gera grid de notas das disciplinas para o certificado
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridNotasDisciplinaCertificado($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridNotasDisciplinas é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->retornarArrVwGridCertificado($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridnotasdisciplinascertificado.phtml');
    }

    /**
     * Metodo que retorna e gera grid de notas das disciplinas
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridNotasDisciplina($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridNotasDisciplinas é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->retornarArrVwGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridnotasdisciplinas.phtml');
    }

    /**
     * Método que gera CabeÃ§alho do texto
     * @param int $id_entidade
     * @return String
     */
    public function gerarCabecalho($id_entidade)
    {
        if ($this->_tipoSaida == TextoSistemaBO::HTML) {
            $caminho = 'textos/cabecalho-html.phtml';
        } else {
            $caminho = 'textos/cabecalho-pdf.phtml';
        }
        $this->_view->vwGerarCabecalhoTO = $this->_bo->retornarVwGerarCabecalho(new VwGerarCabecalhoTO(array('id_entidade' => $id_entidade)))
            ->subtractMensageiro()
            ->getFirstMensagem();
        return $this->_view->render($caminho);
    }

    /**
     * Metodo que gera a grid de media das disciplinas
     * @param int $id_matricula
     * @throws Zend_Exception
     */
    public function gerarGridMediaDisciplinas($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridMediaDisciplinas é necessário informar  Matricula');
        }
        $this->_view->arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        return $this->_view->render('textos/gridmediadisciplinas.phtml');
    }

    /**
     * Método que gera o Link de RecuperaÃ§Ã£o de Senha
     * @param int $id_usuario
     * @return String
     */
    public function gerarLinkRecuperacaoSenha($id_usuario)
    {
        if (!array_key_exists('id_entidade', $this->_params)) {
            throw new Zend_Exception('Para usar a máscara gerarLinkRecuperacaoSenha é necessário informar o id_entidade no array de parÃ¢metros');
        }
        $dataHoje = date('Y.m.d');
        $id_entidade = $this->_params['id_entidade'];
        $url = Ead1_Ambiente::geral()->applicationUrl . '/Index/recuperar-senha/q/' . base64_encode($id_usuario) . '/e/' . base64_encode($id_entidade) . '/d/' . base64_encode($dataHoje);
        return '<a href="' . $url . '" target="blank"> Recuperar senha </a>';
    }

    /**
     * Método que gera o Link de um e-Book
     * @param int $id_venda
     * @return String
     * @TODO terminar o método para envio do link
     */
    public function gerarLinkEBook($id_venda)
    {

        try {

            if (!$id_venda) {
                throw new Zend_Exception("O identificador da venda não foi informado!");
            }

            $vwProdutoVendaLivroTO = new VwProdutoVendaLivroTO();
            $vwProdutoVendaLivroTO->setId_venda($id_venda);
            $vwProdutoVendaLivroTO->setBl_ativo(1);
            $vwProdutoVendaLivroTO->setId_tipoproduto(TipoProdutoTO::LIVRO);
            $vwProdutoVendaLivroTO->setId_tipolivro(TipoLivroTO::E_BOOK);

            $bo = new VendaBO();
            $menesageiro = $bo->retornarVwProdutoVendaLivro($vwProdutoVendaLivroTO);
            if ($menesageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($menesageiro->getFirstMensagem());
            }

            $intBO = new EntidadeIntegracaoTO();
            $intBO->setId_entidade($menesageiro->getFirstMensagem()->getId_entidade());
            $intBO->setId_sistema(SistemaTO::LEYA_BOOKSTORE);
            $intBO->fetch(false, true, true);

            if ($intBO->getSt_codchave() == false) {
                throw new Zend_Exception("A Entidade não tem vínculo com o sistema Leya Bookstore!");
            }

            $links = array();

            foreach ($menesageiro->getMensagem() as $to) {
                $link = Leya_Service_Bookstore::getLink($to->getSt_isbn(), $to->getid_venda(), $intBO->getSt_codchave());
                if ($link) {
                    $links[] = '<a href="' . $link->getLink() . '" target="blank">' . $to->getSt_produto() . '</a>';
                } else {
                    throw new Zend_Exception("link não encontrado!");
                }
            }

            if ($links) {
                $url = implode("<br />", $links);
            } else {
                throw new Zend_Exception("Nenhum link encontrado!");
            }

            return $url;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Método que gera o Link para redefiniÃ§Ã£o de senha
     * @param int $id_usuario
     * @return String
     */
    public function gerarLinkRedefinicaoSenha($id_usuario)
    {
        if (!array_key_exists('id_entidade', $this->_params)) {
            throw new Zend_Exception('Para usar a mÃ¡scara gerarLinkRedefinicaoSenha é necessário informar o id_entidade no array de parÃ¢metros');
        }

        $us = new UsuarioTO();
        $us->setId_usuario($id_usuario);
        $us->fetch(true, true, true);
        if ($us->getBl_redefinicaosenha()) {
            return '';
        }
        $url = Ead1_Ambiente::geral()->st_url_portal . '/portal/usuario/redefinir-senha?q=' . base64_encode($id_usuario) . '&e=' . base64_encode($this->_params['id_entidade']);
        return '<a href="' . $url . '" target="blank">Para redefinir sua senha, clique aqui</a>.';
    }

    /**
     * Método que gera a grid dos estabelecimentos de conclusÃ£o das disciplinas do aluno
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridEstabelecimento($id_matricula)
    {
        if (empty($id_matricula)) {
            throw new Zend_Exception('Para utilizar o gerarGridEstabelecimento é necessário informar a Matricula.');
        }
        $to = new VwGridEstabelecimentoTO(array('id_matricula' => $id_matricula));
        $dados = $this->_bo->retornarVwGridEstabelecimento($to)->subtractMensageiro()->getMensagem();
        $arrOrganizador = array();
        foreach ($dados as $dado) {
            $arrOrganizador['dados'][$dado->getId_serie()]['tos'][] = $dado;
            $arrOrganizador['series'][$dado->getId_serie()]['st_serie'] = $dado->getSt_serie();
            $arrOrganizador['series'][$dado->getId_serie()]['id_serie'] = $dado->getId_serie();
        }
        foreach ($arrOrganizador['dados'] as $id_serie => $serie) {
            $serieConcluida = true;
            foreach ($serie['tos'] as $dadosSerie) {
                if ($dadosSerie->getId_evolucao() != MatriculaDisciplinaTO::EVOLUCAO_CONCLUIDA) {
                    $serieConcluida = false;
                }
            }
            $arrOrganizador['series'][$id_serie]['bl_concluida'] = $serieConcluida;
            if ($serieConcluida) {
                foreach ($serie['tos'] as $dadosSerie) {
                    if (!array_key_exists('data', $arrOrganizador['series'][$id_serie])) {
                        $arrOrganizador['series'][$id_serie]['to'] = $dadosSerie;
                        $arrOrganizador['series'][$id_serie]['data'] = new Zend_Date('01/01/1901');
                    }
                    if (!$dadosSerie->getNu_anoconclusao()) {
                        $dataConclusao = new Zend_Date('01/01/1901');
                    } else {
                        $dataConclusao = new Zend_Date('01/01/' . $dadosSerie->getNu_anoconclusao());
                    }
                    if (!$dadosSerie->getDt_avaliacao()) {
                        $dataAvaliacao = new Zend_Date('01/01/1901');
                    } else {
                        $dataAvaliacao = $dadosSerie->getDt_avaliacao();
                    }
                    if ($dataConclusao->isLater($arrOrganizador['series'][$id_serie]['data'])) {
                        $arrOrganizador['series'][$id_serie]['data'] = $dataConclusao;
                        $arrOrganizador['series'][$id_serie]['to'] = $dadosSerie;
                    }
                    if ($dataAvaliacao->isLater($arrOrganizador['series'][$id_serie]['data'])) {
                        $arrOrganizador['series'][$id_serie]['data'] = $dataConclusao;
                        $arrOrganizador['series'][$id_serie]['to'] = $dadosSerie;
                    }
                }
            }
        }
        unset($arrOrganizador['dados']);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridestabelecimento.phtml');
    }

    /**
     * Metodo que gera a grid das disciplinas de transferencia
     * @param int $id_matricula
     * @return String $html
     */
    public function gerarGridTransferencia($id_matricula)
    {
        if (empty($id_matricula)) {
            throw new Zend_Exception('Para utilizar o gerarGridTransferencia é necessário informar a Matricula.');
        }
        $this->_view->basicStyle = 'border:solid #000; border-collapse:collapse; border-width:thin; text-align:center;';
        $this->_view->rightStyle = 'border:solid #000; border-collapse:collapse; border-width:thin; text-align:right;';
        $this->_view->areasStyle = 'border:solid #000; border-collapse:collapse; border-width:thin;';
        $this->_view->grayColor = 'background-color: #cccccc;';
        $this->_view->arrOrganizador = $this->_retornaArrGridHistoricoGeral($id_matricula);
        return $this->_view->render('textos/gridtransferencia.phtml');
    }

    /**
     * Método que gera uma nova senha para o usuÃ¡rio e o edita
     * @param int $id_usuario
     * @return String
     */
    public function gerarNovaSenhaUsuario($id_usuario, array $parametros)
    {
        $loginBO = new LoginBO();
        $dao = new Ead1_DAO();
        $dao->beginTransaction();
//        Zend_Debug::dump($parametros);die;
        try {

            if (!isset($parametros['id_entidade']) || !$parametros['id_entidade']) {
                throw new Zend_Exception("Entidade Obrigatória!!");
            }

            $pessoaBO = new PessoaBO();
            $usuarioTO = $pessoaBO->retornaUsuario(new UsuarioTO(array('id_usuario' => $id_usuario)));

            $dTO = new DadosAcessoTO();
            $dTO->setId_entidade($parametros['id_entidade']);
            $dTO->setId_usuario($usuarioTO->getId_usuario());
            $dTO->setBl_ativo(1);
            $dTO->fetch(false, true, true);

            if ($dTO->getSt_senha()) {
                $novaSenha = $dTO->getSt_senha();
                $usuarioTO->setSt_senha($novaSenha); /* Atualiza a senha atual */
            } else {
                //                $novaSenha = $loginBO->geradorDeSenha();
                //
                //AC-1202 -> Como acadêmico quero mudar o padrão de geração de nova senha
                //6 primeiro dígitos do nome (sem espaço) + 4 números
                $nome = str_replace(' ', '', $usuarioTO->getSt_nomecompleto());
                $senhaInicio = substr(strtolower($nome), 0, 6);
                $senhaFim = $loginBO->geradorHash(4, true, false);
                $novaSenha = $senhaInicio . $senhaFim;
                $usuarioTO->setSt_senha($novaSenha);

                $retorno = $pessoaBO->editarUsuario($usuarioTO, true, false, $parametros['id_entidade'])->subtractMensageiro();
            }

            $dao->commit();
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw new Zend_Exception("Não foi Possivel Gerar uma Nova Senha para o Usuário. " . $e->getMessage());
        }
        return $novaSenha;
    }

    /**
     * Metodo que gera a grid das disciplinas de aproveitamento
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridAproveitamento($id_matricula)
    {
        if (empty($id_matricula)) {
            throw new Zend_Exception('Para utilizar o gerarGridTransferencia é necessário informar o idMatricula');
        }
        $vwHistoricoGeralTO = new VwHistoricoGeralTO();
        $vwHistoricoGeralTO->setId_matricula($id_matricula);
        $arrHistoricoGeral = $this->_bo->retornarVwHistoricoGeral($vwHistoricoGeralTO)->subtractMensageiro()->getMensagem();
        $arrOrganizador = array();
        $arrOrganizador['areaconhecimento'] = array();
        $arrOrganizador['dados'] = array();
        foreach ($arrHistoricoGeral as $organizar) {
            $arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['st_areaconhecimento'] = $organizar->getSt_areaconhecimento();
            $arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['disciplinas'][$organizar->Nu_ordem()]['bl_destadisciplina'] = true;
            if ($organizar->getSt_cargahorariaaproveitamento()) {
                $arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['disciplinas'][$organizar->Nu_ordem()]['st_nota'] = (($organizar->getNu_notamaxima() * $organizar->getNu_aprovafinal()) / 100);
            } else {
                $arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['disciplinas'][$organizar->Nu_ordem()]['st_nota'] = 'X';
            }
            $arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['disciplinas'][$organizar->Nu_ordem()]['id_disciplina'] = $organizar->getId_disciplina();
            if (!array_key_exists('maxdisciplinas', $arrOrganizador['dados'])) {
                $arrOrganizador['dados']['maxdisciplinas'] = 0;
            }
            $arrOrganizador['ordem'][$organizar->Nu_ordem()] = $organizar->Nu_ordem();
            $arrOrganizador['dados']['maxdisciplinas'] = count($arrOrganizador['ordem']);
            ksort($arrOrganizador['areaconhecimento'][$organizar->getId_areaconhecimento()]['disciplinas']);
            ksort($arrOrganizador['ordem']);
            asort($arrOrganizador['areaconhecimento']);
        }

        foreach ($arrOrganizador['areaconhecimento'] as $id_areaconhecimento => $dadosArea) {
            foreach ($arrOrganizador['ordem'] as $nu_ordem) {
                if (!array_key_exists($nu_ordem, $dadosArea['disciplinas'])) {
                    $arrOrganizador['areaconhecimento'][$id_areaconhecimento]['disciplinas'][$nu_ordem]['bl_destadisciplina'] = false;
                    ksort($arrOrganizador['areaconhecimento'][$id_areaconhecimento]['disciplinas']);
                }
            }
        }
        $arrStyles['tableStyle'] = 'width:100%; border-collapse:collapse; border: 1px solid black;border-width:thin;';
        $arrStyles['basicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['leftBasicStyle'] = 'text-left; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['grayBgColorStyle'] = 'background-color:#cccccc;';
        $this->_view->styles = $arrStyles;
        $this->_view->dados = $arrOrganizador;
        return $this->_view->render('textos/grid-aproveitamento.phtml');
    }

    /**
     * Metodo que gera uma grid de produto e valor
     * @param int $id_venda
     * @return String
     */
    public function geraGridProdutoValor($id_venda)
    {
        if (!$id_venda) {
            return '#venda_produtos#';
        }
        $vendaBO = new VendaBO();
        $dados = $vendaBO->retornarVwProdutoVenda(new VwProdutoVendaTO(array('id_venda' => $id_venda)))->subtractMensageiro()->getMensagem();
        $arrProdutos = array();
        foreach ($dados as $dado) {
            $dado->setNu_valorliquido($this->formataValor($dado->getNu_valorliquido()));
            $arrProdutos[$dado->getId_produto()] = $dado;
        }
        $this->_view->produtos = $arrProdutos;
        $arrStyles['tableStyle'] = 'width:100%; border-collapse:collapse; border: 1px solid black;border-width:thin;';
        $arrStyles['basicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['leftBasicStyle'] = 'text-left; border:solid #000; border-collapse:collapse; border-width:thin;';
        $this->_view->styles = $arrStyles;
        return $this->_view->render('textos/grid-produtovalor.phtml');
    }

    /**
     * Metodo que gera uma grid de produto e valor que já foram quitados
     * @param int $id_venda
     * @return String
     */
    public function geraGridVendaValorQuitado($id_venda, $variaveis)
    {

        if (!$id_venda) {
            return '#grid_produtos_venda_quitados#';
        }
        $vendaBO = new VendaBO();

        $to = new VwVendaLancamentoTO();
        $to->setId_venda($id_venda);
        $to->setBl_quitado(1);

        if (key_exists('dt_inicio', $variaveis)) {
            $dt_inicio = new Zend_Date($variaveis['dt_inicio'], 'd/m/Y');
        }

        if (key_exists('dt_termino', $variaveis)) {
            $dt_termino = new zend_date($variaveis['dt_termino'], 'd/m/Y');
        }

        $pesquisa = array(
            'id_venda = ' . $id_venda,
            'bl_quitado = 1'
        );

        if (isset($dt_inicio)) {
            $pesquisa[] = "dt_quitado >= '{$dt_inicio->toString('Y-m-d')}'";
        }
        if (isset($dt_termino)) {
            $pesquisa[] = "dt_quitado <= '{$dt_termino->toString('Y-m-d')}'";
        }

        //Zend_Debug::dump($pesquisa,__CLASS__.'('.__LINE__.')'); exit;
        $dados = $vendaBO->retornarVendaLancamento($to, $pesquisa)->subtractMensageiro()->getMensagem();
        $arrProdutos = array();
        foreach ($dados as $dado) {
            $dado->setnu_quitado($this->formataValor($dado->getnu_quitado()));

            $lanctoVenda = new LancamentoVendaTO();
            $lanctoVenda->setId_lancamento($dado->getId_lancamento());
            $lanctoVenda->fetch(true, true, true);
            $dado->nu_ordem = $lanctoVenda->getNu_ordem();

            $arrProdutos[] = $dado;
        }
        $this->_view->dados = $arrProdutos;
        $colunas = array();
        $colunas[] = array('st_coluna' => 'Nº Parcela', 'st_campoto' => 'nu_ordem');
        $colunas[] = array('st_coluna' => 'Meio de Pagamento', 'st_campoto' => 'st_meiopagamento');
        //$colunas[] = array('st_coluna'=>'Data de Vencimento', 'st_campoto'=>'dt_vencimento');
        $colunas[] = array('st_coluna' => 'Data de Pagamento', 'st_campoto' => 'dt_quitado');
        $colunas[] = array('st_coluna' => 'Valor Pago', 'st_campoto' => 'nu_quitado');
        $this->_view->colunas = $colunas;

        $arrStyles['tableStyle'] = 'width:100%; border-collapse:collapse; border: 1px solid black;border-width:thin;';
        $arrStyles['basicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['leftBasicStyle'] = 'text-left; border:solid #000; border-collapse:collapse; border-width:thin;';
        $this->_view->styles = $arrStyles;
        return $this->_view->render('textos/gridgenerica.phtml');
    }

    public function gerarTextoPeriodoReciboConsolidado($id_venda, $variaveis)
    {

        if (!$id_venda || !$variaveis) {
            return "#venda_periodo_quitado#";
        }


        $vendaBO = new VendaBO();

        $to = new VwVendaLancamentoTO();
        $to->setId_venda($id_venda);
        $to->setBl_quitado(1);

        if (key_exists('dt_inicio', $variaveis)) {
            $dt_inicio = new Zend_Date($variaveis['dt_inicio'], 'd/m/Y');
        }

        if (key_exists('dt_termino', $variaveis)) {
            $dt_termino = new zend_date($variaveis['dt_termino'], 'd/m/Y');
        }

        $pesquisa = array(
            'id_venda = ' . $id_venda,
            'bl_quitado = 1'
        );

        if (isset($dt_inicio)) {
            $pesquisa[] = "dt_quitado >= '{$dt_inicio->toString('Y-m-d')}'";
        }
        if (isset($dt_termino)) {
            $pesquisa[] = "dt_quitado <= '{$dt_termino->toString('Y-m-d')}'";
        }

        //Zend_Debug::dump($pesquisa,__CLASS__.'('.__LINE__.')'); exit;
        $dados = $vendaBO->retornarVendaLancamento($to, $pesquisa)->subtractMensageiro()->getMensagem();

        $datasQuitado = array();
        foreach ($dados as $dado) {
            $datasQuitado[] = $dado->getDt_quitado()->toString('YYYY-MM-d');
        }

        asort($datasQuitado);

        $mesInicio = new Zend_Date($datasQuitado[0], 'YYYY-MM-d');
        $mesTermino = new Zend_Date($datasQuitado[count($datasQuitado) - 1], 'YYYY-MM-d');
        return $mesInicio->toString('MMMM/Y') . ' à ' . $mesTermino->toString('MMMM/Y');
    }

    public function gerarTotalParcelas($id_venda, $variaveis)
    {

        if (!$id_venda || !$variaveis) {
            return "#venda_periodo_quitado#";
        }


        $vendaBO = new VendaBO();

        $to = new VwVendaLancamentoTO();
        $to->setId_venda($id_venda);
        $to->setBl_quitado(1);

        if (key_exists('dt_inicio', $variaveis)) {
            $dt_inicio = new Zend_Date($variaveis['dt_inicio'], 'd/m/Y');
        }

        if (key_exists('dt_termino', $variaveis)) {
            $dt_termino = new zend_date($variaveis['dt_termino'], 'd/m/Y');
        }

        $pesquisa = array(
            'id_venda = ' . $id_venda,
            'bl_quitado = 1'
        );

        if (isset($dt_inicio)) {
            $pesquisa[] = "dt_quitado >= '{$dt_inicio->toString('Y-m-d')}'";
        }
        if (isset($dt_termino)) {
            $pesquisa[] = "dt_quitado <= '{$dt_termino->toString('Y-m-d')}'";
        }

        //Zend_Debug::dump($pesquisa,__CLASS__.'('.__LINE__.')'); exit;
        $dados = $vendaBO->retornarVendaLancamento($to, $pesquisa)->subtractMensageiro()->getMensagem();

        $total = 0;
        foreach ($dados as $dado) {
            $total += $dado->getNu_quitado();
        }

        return $this->formataValor($total);
    }

    public function gerarListaChequesDevolvidos($id_venda, $variaveis)
    {
        if (!$id_venda || !$variaveis) {
            return "#lista_numerocheque#";
        }

        $retorno = '';

        if (array_key_exists('lancamentos', $variaveis)) {
            foreach ($variaveis['lancamentos'] as $id) {
                $lTO = new LancamentoTO();
                $lTO->setId_lancamento($id);
                $lTO->fetch(true, true, true);
                $retorno .= $lTO->getSt_numcheque() . '<br/>';
            }
        }

        return $retorno;
    }

    public function gerarTotalParcelasExtenso($id_venda, $variaveis)
    {

        if (!$id_venda || !$variaveis) {
            return "#venda_periodo_quitado#";
        }


        $vendaBO = new VendaBO();

        $to = new VwVendaLancamentoTO();
        $to->setId_venda($id_venda);
        $to->setBl_quitado(1);

        if (key_exists('dt_inicio', $variaveis)) {
            $dt_inicio = new Zend_Date($variaveis['dt_inicio'], 'd/m/Y');
        }

        if (key_exists('dt_termino', $variaveis)) {
            $dt_termino = new zend_date($variaveis['dt_termino'], 'd/m/Y');
        }

        $pesquisa = array(
            'id_venda = ' . $id_venda,
            'bl_quitado = 1'
        );

        if (isset($dt_inicio)) {
            $pesquisa[] = "dt_quitado >= '{$dt_inicio->toString('Y-m-d')}'";
        }
        if (isset($dt_termino)) {
            $pesquisa[] = "dt_quitado <= '{$dt_termino->toString('Y-m-d')}'";
        }

        //Zend_Debug::dump($pesquisa,__CLASS__.'('.__LINE__.')'); exit;
        $dados = $vendaBO->retornarVendaLancamento($to, $pesquisa)->subtractMensageiro()->getMensagem();

        $total = 0;
        foreach ($dados as $dado) {
            $total += $dado->getNu_quitado();
        }

        return $this->formataValorExtenso($total);
    }

    /**
     * Este método estátratando a informação errada. Ele nào recebe um id_lancamento e sim um valor.
     *
     * Metodo que retorna o valor de apenas um lançamento
     * @param Int $id_lancamento
     * @return Number
     */
    public function retornaLancamentoUnico($nu_valor)
    {
        if (!$nu_valor) {
            return '#lancamento_valor#';
        }
        return $this->formataValor($nu_valor);
    }

    public function retornaMotivoCancelamento($id_matricula)
    {
        if (!$id_matricula) {
            return '#st_motivo#';
        }
        $this->negocio = new G2\Negocio\Negocio();
        $matricula = $this->negocio->find('\G2\Entity\Matricula', $id_matricula);
        $motivos = $this->negocio->findBy('\G2\Entity\MotivoCancelamento', array('id_cancelamento' => $matricula->getId_cancelamento(), 'bl_ativo' => true));
        $retorno = null;
        foreach ($motivos as $array) {
            $retorno = $retorno . $array->getId_motivo()->getSt_motivo() . '</br> ';
        }
        return $retorno;

    }

    public function retornaValorCredito($id_matricula)
    {
        if (!$id_matricula) {
            return '#st_valorcredito#';
        }
        $this->negocio = new G2\Negocio\Negocio();
        $matricula = $this->negocio->find('\G2\Entity\Matricula', $id_matricula);
        if (is_null($matricula->getId_cancelamento()))
            return '#st_valorcredito#';

        $cancelamento = $this->negocio->find('\G2\Entity\Cancelamento', $matricula->getId_cancelamento());
        return number_format($cancelamento->getNu_valorcarta(), 2, ',', '.');

    }

    /**
     * Metodo que converte a data de vencimento de um lancamento especifico para o formato dd/MM/Y
     * @param int $id_lancamento
     * @return String
     */
    public function converteDataVencimentoLancamentoUnico($id_lancamento)
    {
        if (!$id_lancamento) {
            return '#lancamento_vencimento#';
        }
        $orm = new VwGerarVendaTextoORM();
        $vwGerarVendaTextoTO = new VwGerarVendaTextoTO(array('id_lancamento' => $id_lancamento));
        $vwGerarVendaTextoTO = $orm->consulta($vwGerarVendaTextoTO, false, true);
        if (!$vwGerarVendaTextoTO) {
            return '#lancamento_vencimento#';
        }
        if (is_string($vwGerarVendaTextoTO->getDt_lancamentovencimento())) {
            $data = new Zend_Date($vwGerarVendaTextoTO->getDt_lancamentovencimento(), Zend_Date::ISO_8601);
            return $data->toString('dd/MM/Y');
        } elseif ($vwGerarVendaTextoTO->getDt_lancamentovencimento() instanceof Zend_Date) {
            $data = $vwGerarVendaTextoTO->getDt_lancamentovencimento();
            return $data->toString('dd/MM/Y');
        } else {
            return '#lancamento_vencimento#';
        }
    }

    /**
     * Metodo que converte a data que foi quitado um lancamento especifico para o formato dd/MM/Y
     * @param int $id_lancamento
     * @return String
     */
    public function converteDataQuitadoLancamentoUnico($id_lancamento)
    {
        if (!$id_lancamento) {
            return '#lancamento_vencimento#';
        }
        $orm = new VwGerarVendaTextoORM();
        $vwGerarVendaTextoTO = new VwGerarVendaTextoTO(array('id_lancamento' => $id_lancamento));
        $vwGerarVendaTextoTO = $orm->consulta($vwGerarVendaTextoTO, false, true);
        if (!$vwGerarVendaTextoTO) {
            return '#lancamento_vencimento#';
        }
        if (is_string($vwGerarVendaTextoTO->getDt_lancamentodataquitado())) {
            $data = new Zend_Date($vwGerarVendaTextoTO->getDt_lancamentodataquitado(), Zend_Date::ISO_8601);
            return $data->toString('dd/MM/Y');
        } elseif ($vwGerarVendaTextoTO->getDt_lancamentodataquitado() instanceof Zend_Date) {
            $data = $vwGerarVendaTextoTO->getDt_lancamentodataquitado();
            return $data->toString('dd/MM/Y');
        } else {
            return 'não Quitado';
        }
    }

    /**
     * Metodo que retorna e gera o historico do aluno
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String $gridHistorico
     */
    public function gerarGridAtaIndividual($id_matricula)
    {
        if (empty($id_matricula)) {
            throw new Zend_Exception('Para utilizar o gerarGridHistorico é necessário informar a Matrícula.');
        }
        $arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridataindividual.phtml');
    }

    /**
     * Metodo que retorna e gera o historico do aluno
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridCertificadoAprovacao($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistorico é necessário informar o idMatricula');
        }
        $arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridcertificadoaprovacao.phtml');
    }

    /**
     * Metodo que gera a grid de ata individual de conclusao
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridAtaIndividualConclusao($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistorico é necessário informar o idMatricula');
        }
        $arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridataindividualconclusao.phtml');
    }

    /**
     * Metodo que gera a grid de declaracao de escolaridade
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridDeclaracaoEscolaridade($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridHistorico é necessário informar a Matrícula.');
        }
        $arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/griddeclaracaoescolaridade.phtml');
    }

    /**
     * Metodo que gera a grid de declaracao de escolaridade
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridCargaHorariaDisciplina($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridCargaHorariaDisciplina é necessário informar a Matrícula.');
        }
        $arrOrganizador = $this->_retornaArrGridHistorico($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridcargahorariadisciplina.phtml');
    }

    /**
     * Metodo que gera a grid gerarGridCurricular
     * @param int $id_matricula
     * @return String
     */
    public function gerarGridCurricular($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridCurricular é necessário informar a Matrícula.');
        }
        $arrHistoricoGeral = $this->_retornaArrGridHistoricoGeral($id_matricula, true);
        $arrOrganizador = array();
        foreach ($arrHistoricoGeral['agregadora'] as $id_areaagregadora => $agregadora) {
            foreach ($agregadora['area'] as $id_area => $area) {
                $arrOrganizador['area'][$id_area]['st_areaconhecimento'] = $area['st_areaconhecimento'];
                foreach ($area['serie'] as $id_serie => $serie) {
                    $arrOrganizador['series'][$id_serie]['st_serie'] = $serie['st_serie'];
                    if (!array_key_exists('serie', $arrOrganizador['area'][$id_area])) {
                        $arrOrganizador['area'][$id_area]['serie'] = array();
                    }
                    foreach ($serie['disciplinas'] as $id_disciplina => $disciplina) {
                        $arrOrganizador['area'][$id_area]['serie'][$id_serie]['nu_cargaarea'] = isset($arrOrganizador['area'][$id_area]['serie'][$id_serie]['nu_cargaarea']) ? $arrOrganizador['area'][$id_area]['serie'][$id_serie]['nu_cargaarea'] : 0;
                        $arrOrganizador['area'][$id_area]['serie'][$id_serie]['nu_cargaarea'] += $disciplina['st_cargahoraria'];
                        $arrOrganizador['area'][$id_area]['serie'][$id_serie]['modulos'][] = $disciplina['nu_ordem'];
                        $arrOrganizador['area'][$id_area]['serie'][$id_serie]['st_serie'] = $serie['st_serie'];
                    }
                }
            }
        }
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridcurricular.phtml');;
    }

    /**
     *
     * METODOS ORGANIZADORES
     *
     */

    /**
     * Metodo que retorna o Array ja organizado para montar as grids de Historico
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return $arrOrganizador
     */
    public function _retornaArrGridHistorico($id_matricula)
    {
        $vwGridTO = new VwGridTO();
        $vwGridTO->setId_matricula($id_matricula);
        $dados = $this->_bo->retornarVwGrid($vwGridTO)->subtractMensageiro()->getMensagem();
        $arrOrganizador = array();
        $jaAdicionouSerie = false;
        foreach ($dados as $indexModulo => $dado) {
            //Organizador
            $arrOrganizador['st_nomeentidade'] = $dado->getSt_nomeentidade();
            $arrOrganizador['st_cidade'] = $dado->getSt_cidade();
            $arrOrganizador['sg_uf'] = $dado->getSg_uf();
            $arrOrganizador['dt_concluinte'] = $dado->getDt_concluinte();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['id_modulo'] = $dado->getId_modulo();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['st_modulo'] = $dado->getSt_modulo();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['st_nomeentidade'] = $dado->getSt_nomeentidade();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['st_cidade'] = $dado->getSt_cidade();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['dt_concluinte'] = $dado->getSg_uf();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['st_serie'] = $dado->getSt_serie();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina'][$dado->getId_disciplina()]['st_disciplina'] = $dado->getSt_disciplina();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina'][$dado->getId_disciplina()]['nu_aprovafinal'] = $dado->getNu_aprovafinal();
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina'][$dado->getId_disciplina()]['nu_cargahoraria'] = $dado->getNu_cargahoraria();
            //Tratamentos Carga Horaria
            if (!array_key_exists('nu_cargahorariatotal', $arrOrganizador)) {
                $arrOrganizador['nu_cargahorariatotal'] = 0;
            }
            $arrOrganizador['nu_cargahorariatotal'] += $dado->getNu_cargahoraria();
            if (!array_key_exists('nu_cargahoraria', $arrOrganizador['modulo'][$dado->getId_modulo()])) {
                $arrOrganizador['modulo'][$dado->getId_modulo()]['nu_cargahoraria'] = 0;
            }
            $arrOrganizador['modulo'][$dado->getId_modulo()]['nu_cargahoraria'] += $dado->getNu_cargahoraria();
            //Tratamento Quantidade de areas, series e disciplinas
            $arrOrganizador['nu_modulos'] = count($arrOrganizador['modulo']);
            $arrOrganizador['modulo'][$dado->getId_modulo()]['nu_series'] = count($arrOrganizador['modulo'][$dado->getId_modulo()]['serie']);
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['nu_disciplinas'] = count($arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina']) - 1;
            //Calculando Total de Notas das disciplinas da Serie
            if (!array_key_exists('nu_totalnotas', $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina'])) {
                $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina']['nu_totalnotas'] = 0;
            }
            $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'][$dado->getId_serie()]['disciplina']['nu_totalnotas'] += $dado->getNu_aprovafinal();
            if (!$jaAdicionouSerie) {
                $arrOrganizador['serie'] = $arrOrganizador['modulo'][$dado->getId_modulo()]['serie'];
                $jaAdicionouSerie = true;
            }
        }
        //Calculos de Peso de Notas/Medias
        foreach ($arrOrganizador['modulo'] as $id_modulo => $dadosModulo) {
            foreach ($dadosModulo['serie'] as $id_serie => $dadosSerie) {
                $arrOrganizador['modulo'][$id_modulo]['serie'][$id_serie]['nu_media'] = ($dadosSerie['disciplina']['nu_totalnotas'] / ($dadosSerie['nu_disciplinas'] ? $dadosSerie['nu_disciplinas'] : 1));
                if (!array_key_exists('nu_totalnotas', $arrOrganizador['modulo'][$id_modulo])) {
                    $arrOrganizador['modulo'][$id_modulo]['nu_totalnotas'] = 0;
                }
                $arrOrganizador['modulo'][$id_modulo]['nu_totalnotas'] += $arrOrganizador['modulo'][$id_modulo]['serie'][$id_serie]['nu_media'];
            }
            $arrOrganizador['modulo'][$id_modulo]['nu_media'] = ($arrOrganizador['modulo'][$id_modulo]['nu_totalnotas'] / $dadosModulo['nu_series']);
        }
        return $arrOrganizador;
    }

    /**
     * Metodo que organiza os dados em uma matriz para geração de grid de disciplinas extras, baseada na grid de historico geral
     * @param int $idMatricula
     * @return array $arrOrganizador
     */
    private function _retornarArrDisciplinasExtras($idMatricula)
    {
        $arrOrganizador = array();
        $dados = array();
        $matriculaBO = new MatriculaBO();
        $mensageiro = $matriculaBO->retornarVwDisciplinasExtras(new VwDisciplinasExtrasTO(array('id_matricula' => $idMatricula)))->subtractMensageiro(Ead1_IMensageiro::ERRO);
        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
            $dados = $mensageiro->getMensagem();
        }
        if (!empty($dados)) {
            foreach ($dados as $dado) {
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['st_areaagregadora'] = $dado->getSt_areaagregadora();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['id_areaagregadora'] = $dado->getId_areaagregadora();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['st_areaconhecimento'] = $dado->getSt_areaconhecimento();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['nu_cargahoraria'] = (isset($arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['nu_cargahoraria']) ?
                    $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['nu_cargahoraria'] : 0);
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['nu_cargahoraria'] += $dado->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['nu_cargahoraria'] = (isset($arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['nu_cargahoraria']) ?
                    $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['nu_cargahoraria'] : 0);
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['nu_cargahoraria'] += $dado->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['st_serie'] = $dado->getSt_serie();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['nu_cargahoraria'] = (isset($arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['nu_cargahoraria']) ?
                    $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['st_serie'] : 0);
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['nu_cargahoraria'] += $dado->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['disciplinas'][$dado->getSt_disciplina()]['st_disciplina'] = $dado->getSt_disciplina();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['disciplinas'][$dado->getSt_disciplina()]['st_nota'] = $dado->getNu_aprovafinal();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['disciplinas'][$dado->getSt_disciplina()]['nu_cargahoraria'] = $dado->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['disciplinas'][$dado->getSt_disciplina()]['nu_ordem'] = $dado->getNu_ordem();
                $arrOrganizador['series'][$dado->getId_serie()]['st_serie'] = $dado->getSt_serie();
                $arrOrganizador['series'][$dado->getId_serie()]['nu_cargahoraria'] = (isset($arrOrganizador['series'][$dado->getId_serie()]['nu_cargahoraria']) ? $arrOrganizador['series'][$dado->getId_serie()]['nu_cargahoraria'] : 0);
                $arrOrganizador['series'][$dado->getId_serie()]['nu_cargahoraria'] += $dado->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['maxdisciplinas'] = count($arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['disciplinas']);
                $arrOrganizador['series'][$dado->getId_serie()]['maxdisciplinas'] = (isset($arrOrganizador['series'][$dado->getId_serie()]['maxdisciplinas']) ?
                    $arrOrganizador['series'][$dado->getId_serie()]['maxdisciplinas'] : 0);
                if ($arrOrganizador['series'][$dado->getId_serie()]['maxdisciplinas'] < $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['maxdisciplinas']) {
                    $arrOrganizador['series'][$dado->getId_serie()]['maxdisciplinas'] = $arrOrganizador['agregadora'][$dado->getId_areaagregadora()]['area'][$dado->getSt_areaconhecimento()]['serie'][$dado->getId_serie()]['maxdisciplinas'];
                }
            }
        }
        return $arrOrganizador;
    }

    /**
     * Metodo que retorna array notas das disciplinas que constituem o certificado de uma matricula
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $idMatricula
     * @return array
     */
    private function retornarArrVwGridCertificado($idMatricula)
    {
        try {

            $negocio = new \G2\Negocio\Negocio();
            $dados = $negocio->findBy('\G2\Entity\VwGridCertificado', array('id_matricula' => $idMatricula), array(
                'id_tipodisciplina' => 'ASC',
                'dt_encerramentoextensao' => 'ASC'
            ));

            $arrDisciplinas = array();
            foreach ($dados as $index => $VwGridCertificado) {
                $arrDisciplinas[$index]['st_disciplina'] = $VwGridCertificado->getSt_disciplina();
                $arrDisciplinas[$index]['st_conceito'] = $VwGridCertificado->getSt_conceito();
                $arrDisciplinas[$index]['st_cargahoraria'] = $VwGridCertificado->getNu_cargahoraria();
                $arrDisciplinas[$index]['st_titularcertificacao'] = $VwGridCertificado->getSt_titularcertificacao();
                $arrDisciplinas[$index]['st_titulacao'] = $VwGridCertificado->getSt_titulacao();
            }
            return $arrDisciplinas;
        } catch (Zend_Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que retorna array notas das disciplinas
     * modified by Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $idMatricula
     * @return array
     */
    private function retornarArrVwGridHistorico($idMatricula)
    {
        try {
            $vwGridHistoricoTO = new VwGridHistoricoTO();
            $vwGridHistoricoTO->setId_matricula($idMatricula);
            $dados = $this->_bo->retornarVwGridHistorico($vwGridHistoricoTO)->subtractMensageiro()->getMensagem();
            $arrDisciplinas = array();
            foreach ($dados as $index => $vwGridHistoricoTO) {
                $arrDisciplinas[$index]['st_disciplina'] = $vwGridHistoricoTO->st_disciplina;
                $arrDisciplinas[$index]['st_evolucao'] = $vwGridHistoricoTO->st_evolucao;
                $arrDisciplinas[$index]['st_cargahoraria'] = (empty($vwGridHistoricoTO->st_cargahorariaaproveitamento) ?
                    $vwGridHistoricoTO->nu_cargahoraria : $vwGridHistoricoTO->st_cargahorariaaproveitamento);
                $arrDisciplinas[$index]['st_nota'] = (empty($vwGridHistoricoTO->st_notaoriginalaproveitamento) ?
                    (($vwGridHistoricoTO->getNu_notamaxima() * $vwGridHistoricoTO->getNu_aprovafinal()) / 100) : $vwGridHistoricoTO->st_notaoriginalaproveitamento);
            }
            return $arrDisciplinas;
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Metodo que retorna array com os dados das disciplinas para o histórico escolar
     * modified by Rafael Leite<rafael.leite@unyleya.com.br>
     * @param $idMatricula
     * @return array
     */
    private function retornarArrGridHistoricoEscolar($idMatricula)
    {
        try {
            $vwGridHistoricoTO = new VwGridHistoricoTO();
            $vwGridHistoricoTO->setId_matricula($idMatricula);
            $dados = $this->_bo->retornarVwGridHistorico($vwGridHistoricoTO)->subtractMensageiro()->getMensagem();
            $arrDisciplinas = array();
            foreach ($dados as $index => $vwGridHistoricoTO) {
                $arrDisciplinas[$index]['st_disciplina'] = $vwGridHistoricoTO->st_disciplina;
                $arrDisciplinas[$index]['id_evolucao'] = $vwGridHistoricoTO->id_evolucao;
                $arrDisciplinas[$index]['st_evolucao'] = ($vwGridHistoricoTO->id_evolucao == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO) ?
                    'A cursar' : $vwGridHistoricoTO->st_evolucao;
                $arrDisciplinas[$index]['id_evolucao'] = $vwGridHistoricoTO->id_evolucao;
                $arrDisciplinas[$index]['st_semestre'] = $vwGridHistoricoTO->st_semestre;
                $arrDisciplinas[$index]['st_cargahoraria'] = (empty($vwGridHistoricoTO->st_cargahorariaaproveitamento) ?
                    $vwGridHistoricoTO->nu_cargahoraria : $vwGridHistoricoTO->st_cargahorariaaproveitamento);
                $arrDisciplinas[$index]['st_nota'] = !empty($vwGridHistoricoTO->nu_aprovafinal) ?
                    $vwGridHistoricoTO->nu_aprovafinal : '-';
                $arrDisciplinas[$index]['st_statusdisciplina'] = $vwGridHistoricoTO->st_statusdisciplina;
            }
            return $arrDisciplinas;
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Metodo que organiza os dados em uma matriz para geração de grid de relatorios, declarações e historicos
     * @param int $idMatricula
     * @return array $arrOrganizador
     */
    private function _retornaArrGridHistoricoGeral($idMatricula, $curricular = false)
    {
        //ID's Fixos
        $ID_EVOLUCAO_CONCLUIDA = 12;
        $ID_SITUACAO_DISCIPLINA_CONCEBIDA = 65;
        //---
        if (!$curricular) {
            $vwHistoricoGeralTO = new VwHistoricoGeralTO();
            $vwHistoricoGeralTO->setId_matricula($idMatricula);
            $arrHistoricoGeral = $this->_bo->retornarVwHistoricoGeral($vwHistoricoGeralTO)->subtractMensageiro()->getMensagem();
        } else {
            $vwGridHistoricoTO = new VwGridHistoricoTO();
            $vwGridHistoricoTO->setId_matricula($idMatricula);
            $arrHistoricoGeral = $this->_bo->retornarVwGridHistorico($vwGridHistoricoTO)->subtractMensageiro()->getMensagem();
        }
        $arrOrganizador = array();
        $arrOrganizador['st_totalcargahorariaegral'] = 0;
        //Organizando dados em niveis
        foreach ($arrHistoricoGeral as $organizador) {
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['st_areaagregadora'] = $organizador->getSt_areaagregadora();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['st_areaconhecimento'] = $organizador->getSt_areaconhecimento();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['st_serie'] = $organizador->getSt_serie();
            $arrOrganizador['series'][$organizador->getId_serie()]['st_serie'] = $organizador->getSt_serie();
            $arrOrganizador['series'][$organizador->getId_serie()]['bl_aprovado'] = true;
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['id_evolucao'] = $organizador->getId_evolucao();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_evolucao'] = $organizador->getSt_evolucao();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['id_situacao'] = $organizador->getId_situacao();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_disciplina'] = $organizador->getSt_disciplina();
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['nu_ordem'] = $organizador->getNu_ordem();
            if (!array_key_exists('st_totalcargahoraria', $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()])) {
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['st_totalcargahoraria'] = 0;
            }
            if (!array_key_exists('st_totalcargahoraria', $arrOrganizador['series'][$organizador->getId_serie()])) {
                $arrOrganizador['series'][$organizador->getId_serie()]['st_totalcargahoraria'] = 0;
            }
            if (!array_key_exists('dt_conclusao', $arrOrganizador['series'][$organizador->getId_serie()])) {
                $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = null;
            }
            if (!array_key_exists('dt_conclusao', $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()])) {
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'] = null;
            }
            $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['id_disciplina'] = $organizador->getId_disciplina();
            if ($organizador->getSt_cargahorariaaproveitamento()) {
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['bl_aproveitamento'] = true;
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_nota'] = $organizador->getSt_notaoriginalaproveitamento();
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_cargahoraria'] = $organizador->getSt_cargahorariaaproveitamento();
                if (!$arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao']) {
                    $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = $organizador->getDt_conclusaoaproveitamento();
                } else {
                    $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = (
                    $this->comparaData($arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'], $organizador->getDt_conclusaoaproveitamento()) ?
                        $organizador->getDt_conclusaoaproveitamento() :
                        $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao']
                    );
                }
                if (!$arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]) {
                    $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'] = $organizador->getDt_conclusaoaproveitamento();
                } else {
                    $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'] = (
                    $this->comparaData($arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'], $organizador->getDt_conclusaoaproveitamento()) ?
                        $organizador->getDt_conclusaoaproveitamento() :
                        $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao']
                    );
                }
                $arrOrganizador['series'][$organizador->getId_serie()]['st_nomeentidadeconclusao'] = $organizador->getSt_instituicaoaproveitamento();
                $arrOrganizador['series'][$organizador->getId_serie()]['st_municipioentidadeconclusao'] = $organizador->getSt_nomemunicipioaproveitamento();
                $arrOrganizador['series'][$organizador->getId_serie()]['sg_ufentidadeconclusao'] = $organizador->getSg_ufaproveitamento();
                $arrOrganizador['series'][$organizador->getId_serie()]['st_totalcargahoraria'] = $organizador->getSt_cargahorariaaproveitamento();
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['st_totalcargahoraria'] = $organizador->getSt_cargahorariaaproveitamento();
            } else {
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_cargahoraria'] = $organizador->getNu_cargahoraria();
                if (!$arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao']) {
                    $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = $organizador->getDt_conclusaodisciplina();
                } else {
                    $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = (
                    $this->comparaData($arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'], $organizador->getDt_conclusaodisciplina()) ?
                        $organizador->getDt_conclusaodisciplina() :
                        $arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao']
                    );
                }
                if (!$arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao']) {
                    $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'] = $organizador->getDt_conclusaodisciplina();
                } else {
                    $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'] = (
                    $this->comparaData($arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao'], $organizador->getDt_conclusaodisciplina()) ?
                        $organizador->getDt_conclusaodisciplina() :
                        $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['dt_conclusao']
                    );
                }
//				$arrOrganizador['series'][$organizador->getId_serie()]['dt_conclusao'] = $organizador->getDt_conclusaodisciplina();
                $arrOrganizador['series'][$organizador->getId_serie()]['st_nomeentidadeconclusao'] = $organizador->getSt_nomeentidade();
                $arrOrganizador['series'][$organizador->getId_serie()]['st_municipioentidadeconclusao'] = $organizador->getSt_nomemunicipio();
                $arrOrganizador['series'][$organizador->getId_serie()]['sg_ufentidadeconclusao'] = $organizador->getSg_uf();
                $arrOrganizador['series'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()] = $organizador->getId_disciplina();
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['st_totalcargahoraria'] += $organizador->getNu_cargahoraria();
                $arrOrganizador['series'][$organizador->getId_serie()]['st_totalcargahoraria'] += $organizador->getNu_cargahoraria();
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['bl_aproveitamento'] = false;
                $arrOrganizador['agregadora'][$organizador->getId_areaagregadora()]['area'][$organizador->getId_areaconhecimento()]['serie'][$organizador->getId_serie()]['disciplinas'][$organizador->getId_disciplina()]['st_nota'] = (($organizador->getNu_notamaxima() * $organizador->getNu_aprovafinal()) / 100);
                $arrOrganizador['st_totalcargahorariageral'] += $organizador->getNu_cargahoraria();
            }
        }
        //Calculando medias, carga horaria e afins
        foreach ($arrOrganizador['agregadora'] as $id_areaagregadora => $areaagregadora) {
            if (!array_key_exists('nu_totaldisciplinas', $arrOrganizador['agregadora'][$id_areaagregadora])) {
                $arrOrganizador['agregadora'][$id_areaagregadora]['nu_totaldisciplinas'] = 1;
            }
            foreach ($areaagregadora['area'] as $id_areaconhecimento => $dadosArea) {
                if (!array_key_exists('bl_aprovado', $areaagregadora['area'][$id_areaconhecimento])) {
                    $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['bl_aprovado'] = true;
                }
                foreach ($arrOrganizador['series'] as $id_seriev => $sv) {
                    if (!array_key_exists($id_seriev, $dadosArea['serie'])) {
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['bl_aprovado'] = false;
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['st_serie'] = $sv['st_serie'];
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['bl_aproveitamento'] = false;
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['bl_todasnotas'] = false;
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['st_media'] = null;
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['st_totalcargahoraria'] = 0;
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_seriev]['disciplinas'] = array();
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['bl_aprovado'] = false;
                    }
                }
                foreach ($dadosArea['serie'] as $id_serie => $dadosSerie) {
                    //Validando quantidade de series
                    //Calculando Numero Maximo de Disciplinas por Serie para montar celulas da grid
                    if (!array_key_exists('bl_aprovado', $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie])) {
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_aprovado'] = true;
                    }
                    if (!array_key_exists('maxdisciplinas', $arrOrganizador['series'][$id_serie])) {
                        $arrOrganizador['series'][$id_serie]['maxdisciplinas'] = 0;
                    }
                    $arrOrganizador['series'][$id_serie]['maxdisciplinas'] = (
                    $arrOrganizador['series'][$id_serie]['maxdisciplinas'] >= count($arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['disciplinas']) ?
                        $arrOrganizador['series'][$id_serie]['maxdisciplinas'] :
                        count($arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['disciplinas'])
                    );
                    //Calculando Medias e verificando se o aluno esta aprovado
                    if (!array_key_exists('bl_todasnotas', $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie])) {
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_todasnotas'] = true;
                    }
                    $arrOrganizador['agregadora'][$id_areaagregadora]['nu_totaldisciplinas'] += count($dadosSerie['disciplinas']);
                    foreach ($dadosSerie['disciplinas'] as $id_disciplina => $dadosDisciplina) {
                        if ($arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_aproveitamento']) {
                            $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['st_media'] = $dadosDisciplina['st_nota'];
                        } else {
                            if (!$arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['disciplinas'][$id_disciplina]['st_nota']) {
                                $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_todasnotas'] = false;
                            }
                            if (!array_key_exists('st_somanotas', $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie])) {
                                $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['st_somanotas'] = 0;
                            }
                            $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['st_somanotas'] += $dadosDisciplina['st_nota'];
                        }
                        if ($dadosDisciplina['id_evolucao'] != $ID_EVOLUCAO_CONCLUIDA && $dadosDisciplina['id_situacao'] != $ID_SITUACAO_DISCIPLINA_CONCEBIDA) {
                            $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_aprovado'] = false;
                            $arrOrganizador['series'][$id_serie]['bl_aprovado'] = false;
                            $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['bl_aprovado'] = false;
                        }
                    }
                    if (!$arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['bl_aproveitamento']) {
                        $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['st_media'] = (
                            $arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['st_somanotas'] /
                            count($arrOrganizador['agregadora'][$id_areaagregadora]['area'][$id_areaconhecimento]['serie'][$id_serie]['disciplinas'])
                        );
                    }
                }
            }
        }
        return $arrOrganizador;
    }

    /**
     *
     * METODOS DE RETORNO TRATADOS
     *
     */

    /**
     * Método que retorna o texto sistema e trata o retorno
     * @param TextoSistemaTO $textoSistemaTO
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    private function _retornarTextoSistemaSemEntidade(TextoSistemaTO $textoSistemaTO)
    {
        try {

            if (!$textoSistemaTO->getId_textosistema()) {
                throw new Zend_Validate_Exception("Texto não Indentificado.");
            }
            $textoSistemaBuscaTO = new TextoSistemaTO();
            $textoSistemaBuscaTO->setId_textosistema($textoSistemaTO->getId_textosistema());
            $textoSistemaTO = $this->_bo->retornarTextoSistema($textoSistemaBuscaTO)->subtractMensageiro()->getFirstMensagem();
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro('Nenhum Texto Encontrado.', Ead1_IMensageiro::AVISO, $e->getMessage());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Verificar o Texto.', Ead1_IMensageiro::AVISO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna e trata a categoria do texto
     * @param TextoCategoriaTO $textoCategoriaTO
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    private function _retornarTextoCategoria(TextoCategoriaTO $textoCategoriaTO)
    {
        try {
            if (!$textoCategoriaTO->getId_textocategoria()) {
                throw new Zend_Validate_Exception("Categoria do Texto não Indentificado.");
            }
            $textoCategoriaTO = $this->_bo->retornarTextoCategoria($textoCategoriaTO)->subtractMensageiro()->getFirstMensagem();
            $this->mensageiro->setMensageiro($textoCategoriaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro('Nenhuma Categoria de Texto Encontrada.', Ead1_IMensageiro::AVISO, $e->getMessage());
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Verificar a Categoria do Texto.', Ead1_IMensageiro::AVISO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Retorna uma lista HTML com os Produtos - inicialmente desenvolvido para o Recibo
     * @param int $id_venda
     * @return string|boolean
     */
    public function listaProdutosVenda($id_venda)
    {
        try {
            $vwpTO = new VwProdutoVendaTO();
            $vwpTO->setId_venda($id_venda);

            $bo = new ProdutoBO();
            $retorno = $bo->retornarVwProdutoVenda($vwpTO);

            if ($retorno->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return '';
            } else {
                $produtos = $retorno->getMensagem();
                $list = '<ul>';
                foreach ($produtos as $produto) {
                    // 					$produto = new VwProdutoVendaTO();
                    $list .= '<li>' . $produto->getSt_produto() . '</li>';
                }
                $list .= '</ul>';
                return $list;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Retorna uma lista HTML com os Produtos - inicialmente desenvolvido para o Recibo
     * @param int $id_venda
     * @return string|boolean
     */
    public function listaProdutosVendaSimples($id_venda)
    {
        try {
            $vwpTO = new VwProdutoVendaTO();
            $vwpTO->setId_venda($id_venda);

            $bo = new ProdutoBO();
            $retorno = $bo->retornarVwProdutoVenda($vwpTO);

            if ($retorno->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return '';
            } else {
                $produtos = $retorno->getMensagem();
                $list = '';
                foreach ($produtos as $produto) {
                    $list .= !empty($list) ? ', ' . $produto->getSt_produto() : $produto->getSt_produto();
                }

                return $list;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Metodo que muda o formato da data para o padrão brasileiro
     * @param String $dt
     * @return String $dt
     */
    public function setDataFormatoBrasileiro($dt)
    {
        $data = new Zend_Date($dt);
        return $data->get("dd 'de' MMMM 'de' YYYY");
    }

    /**
     * Metodo que gera uma grid de produto e valor do lançamento selecionado
     * @param int $id_lancamento
     * @return String
     */
    public function geraGridVendaReciboMensal($id_lancamento)
    {
        if (!$id_lancamento) {
            return '#grid_produtos_venda_quitados#';
        }
        $vendaBO = new VendaBO();

        $lanctoVenda = new LancamentoVendaTO();
        $lanctoVenda->setId_lancamento($id_lancamento);
        $lanctoVenda->fetch(false, true, true);

        $to = new VwVendaLancamentoTO();
        $to->setId_venda($lanctoVenda->getId_venda());
        $to->setBl_quitado(1);

        $where = array('id_lancamento = ' . $lanctoVenda->getId_lancamento(), 'bl_quitado = 1');

        $dados = $vendaBO->retornarVendaLancamento($to, $where)->subtractMensageiro()->getMensagem();

        $arrProdutos = array();
        foreach ($dados as $dado) {
            $dado->setnu_quitado($this->formataValor($dado->getnu_quitado()));

            $lanctoVenda = new LancamentoVendaTO();
            $lanctoVenda->setId_lancamento($id_lancamento);
            $lanctoVenda->fetch(false, true, true);
            $dado->nu_ordem = $lanctoVenda->getNu_ordem();
            $arrProdutos[] = $dado;
        }
        $this->_view->dados = $arrProdutos;
        $colunas = array();
        $colunas[] = array('st_coluna' => 'Nº Parcela', 'st_campoto' => 'nu_ordem');
        $colunas[] = array('st_coluna' => 'Meio de Pagamento', 'st_campoto' => 'st_meiopagamento');
        //$colunas[] = array('st_coluna'=>'Data de Vencimento', 'st_campoto'=>'dt_vencimento');
        $colunas[] = array('st_coluna' => 'Data de Pagamento', 'st_campoto' => 'dt_quitado');
        $colunas[] = array('st_coluna' => 'Valor Pago', 'st_campoto' => 'nu_quitado');
        $this->_view->colunas = $colunas;

        $arrStyles['tableStyle'] = 'width:100%; border-collapse:collapse; border: 1px solid black;border-width:thin;';
        $arrStyles['basicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['leftBasicStyle'] = 'text-left; border:solid #000; border-collapse:collapse; border-width:thin;';
        $this->_view->styles = $arrStyles;
        return $this->_view->render('textos/gridgenerica.phtml');
    }

    /**
     * Metodo que gera uma grid de valor total dos pagamentos feitos por usuario selecionado
     * @param int $id_usuario
     * @return String
     * @author Yannick NR
     */
    public function geraGridValorExtensoImposto($id_venda)
    {

        if (!$id_venda) {
            return '#grid_valor_extenso_imposto#';
        }

        $negocioVendaProduto = new \G2\Negocio\Venda();

        $negocio = new G2\Negocio\Negocio();
        $vendaProduto = $negocio->findOneBy('\G2\Entity\VendaProduto', array('id_venda' => $id_venda));

        $vendaProdutoMatricula =
            $negocioVendaProduto->retornarVendaProduto(
                array('id_matricula' => $vendaProduto->getId_matricula()->getId_matricula())
            );

        $vendaProdutoMatricula = $negocioVendaProduto->toArrayEntity($vendaProdutoMatricula);

        $valor_total = 0;
        foreach ($vendaProdutoMatricula as $value) {

            $id_venda = $value['id_venda']['id_venda'];
            $vwVendaLancamentoTO = new VwVendaLancamentoTO();
            $vwVendaLancamentoTO->setId_venda($id_venda);

            $where = array('bl_quitado=1 and nu_valor!=0 and nu_valor IS NOT NULL and id_venda =' . $id_venda);
            $vendaBO = new VendaBO();
            $dados = $vendaBO->retornarVendaLancamento($vwVendaLancamentoTO, $where)->getMensagem();

            if (empty($dados)) {
                continue;
            }

            $dt_hoje = new Zend_Date();
            $year = $dt_hoje->get(Zend_Date::YEAR) - 1;
            $dt_inicio = new Zend_Date($year . "-01-01");
            $dt_fim = new Zend_Date($year . "-12-31");

            foreach ($dados as $dado) {
                $data_quitado = new Zend_Date($dado->getdt_quitado());
                if ($data_quitado > $dt_inicio && $data_quitado < $dt_fim) {
                    $valor_total += $dado->getNu_quitado();
                }
            }
        }

        $valor = $this->formataValor($valor_total);
        $extenso = $this->formataValorExtenso($valor_total);
        $arrProdutos = array(array('valor' => $valor, 'extenso' => $extenso));

        $this->_view->dados = $arrProdutos;
        $colunas = array();
        $colunas[] = array('st_coluna' => 'Valor(*)', 'st_campoto' => $valor);
        $colunas[] = array('st_coluna' => 'Extenso', 'st_campoto' => $extenso);
        $this->_view->colunas = $colunas;

        $arrStyles['tableStyle'] = 'margin:0 auto; width:75%; border-collapse:collapse; border: 1px solid black; border-width:thin;';
        $arrStyles['basicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $arrStyles['centerBasicStyle'] = 'text-align:center; border:solid #000; border-collapse:collapse; border-width:thin;';
        $this->_view->styles = $arrStyles;

        return $this->_view->render('textos/gridimpostorenda.phtml');
    }

    /**
     * Metodo responsavel por gerar o link que é enviado ao email do usuario
     * no momento que este faz uma ocorrencia.
     *
     * @param VwOcorrenciaTO
     * @return UrlResumidaTO
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function gerarLinkOcorrencia($id_ocorrencia)
    {
        $urlResumidaTO = new UrlResumidaTO();
        $vwOcorrenciaTO = new VwOcorrenciaTO();
        $vwOcorrenciaTO->setId_ocorrencia($id_ocorrencia);
        $vwOcorrenciaTO->fetch(false, true, true);

        //Gerando url resumida utilizando base64_encode
        $urlResumidaTO->setSt_urlresumida(base64_encode($vwOcorrenciaTO->getId_ocorrencia()));

        $vwUsuarioPerfilPedagogicoTO = new VwUsuarioPerfilPedagogicoTO();
        $vwUsuarioPerfilPedagogicoTO->setId_usuario($vwOcorrenciaTO->getId_usuariointeressado());
        $vwUsuarioPerfilPedagogicoTO->setId_matricula($vwOcorrenciaTO->getId_matricula());
        $vwUsuarioPerfilPedagogicoTO->fetch(false, true, true);

        /*
         * Verificando se o usuario tem perfil pedagogico, caso tenha
         * será gerado o link para ocorrencia, caso não, nada será feito.
         */
        if ($vwUsuarioPerfilPedagogicoTO->getId_perfilpedagogico()) {

            //Gerando url completa utilizando dados do usuario interessado
            $urlCompleta = Ead1_Ambiente::geral()->st_url_portal .
                '/portal/index/index' .
                '/id_entidade/' . $vwOcorrenciaTO->getId_entidade() .
                '/id_perfil/' . $vwUsuarioPerfilPedagogicoTO->getId_perfil() .
                '/id_perfilpedagogico/' . $vwUsuarioPerfilPedagogicoTO->getId_perfilpedagogico() .
                '/id_projetopedagogico/' . $vwUsuarioPerfilPedagogicoTO->getId_projetopedagogico() .
                '/id_matricula/' . $vwOcorrenciaTO->getId_matricula() .
                '/?url-direta=c-atencao/ocorrencia/id_ocorrencia/' . $vwOcorrenciaTO->getId_ocorrencia();

            $urlResumidaTO->setSt_urlcompleta($urlCompleta);

            /*
             * Verificando se ja existe um link para a ocorrencia caso tenha entao
             * somente retorna o link ja existente.
             */
            $urlResumidaBO = new UrlResumidaBO();
            if (!$urlResumidaBO->buscaPorUrlResumida($urlResumidaTO)) {
                $urlResumidaBO->insert($urlResumidaTO);
            }

            return "<a href='" . Ead1_Ambiente::geral()->st_url_portal . '/portal/index/url/' . $urlResumidaTO->getSt_urlresumida() . "'>link</a>";
        } else {
            return null;
        }
    }

    /**
     * Metodo da categoria do Primeiro Acesso que gera textos do Primeiro Acesso
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function gerarPrimeiroAcesso(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            if (!isset($params['id_venda']) && empty($params['id_venda'])) {
                throw new Zend_Exception(
                    'Para o Método gerarPrimeiroAcesso funcionar é necessario enviar o parametro id_venda no array de parâmetros!'
                );
            }

            if (!isset($params['id_entidade']) && empty($params['id_entidade'])) {
                $params['id_entidade'] = $textoSistemaTO->getSessao()->id_entidade;
            }

            $vwPrimeiroAcessoTO = new VwPrimeiroAcessoTO(array(
                    'id_venda' => $params['id_venda']
                , 'id_entidade' => $params['id_entidade']
                )
            );
            $vwPrimeiroAcessoTO->fetch(false, true, true);

            $txVariavel = new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria()));
            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis($txVariavel)
                ->subtractMensageiro()
                ->getMensagem();

            $textoSubstituido = $this->substituirVariaveisTO(
                $textoSistemaTO->getSt_texto()
                , $vwPrimeiroAcessoTO
                , $arrTextoVariaveisTO
                , $params
            );
            $textoSistemaTO->setSt_texto($textoSubstituido);
            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Gerar Texto Primeiro Acesso! ', Ead1_IMensageiro::ERRO, $e->getMessage());
        } catch (\Exception $e) {
        }

        return $this->mensageiro;
    }


    /**
     * Gera o texto apresentado na tela de venda
     * @param TextoSistemaTO $textoSistemaTO
     * @param array $params
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function gerarTextoLoja(TextoSistemaTO $textoSistemaTO, array $params)
    {
        try {
            if (!isset($params['id_venda']) && empty($params['id_venda'])) {
                throw new Zend_Exception('Para o Método gerarTextoVenda funcionar é necessario enviar o parametro id_venda no array de parâmetros!');
            }

            $vwTO = new VwVendaUsuarioTO();
            $vwTO->setId_venda($params['id_venda']);
            $vwTO->fetch(false, true, true);

            if ($vwTO->getId_entidade() == false) {
                throw new Zend_Exception('Venda não encontrada no momento de usar o método gerarTextoVenda!');
            }

            $arrTextoVariaveisTO = $this->_bo->retornarTextoVariaveis(new TextoVariaveisTO(array('id_textocategoria' => $textoSistemaTO->getId_textocategoria())))->subtractMensageiro()->getMensagem();

            $textoSubstituido = $this->substituirVariaveisTO($textoSistemaTO->getSt_texto(), $vwTO, $arrTextoVariaveisTO, $params);
            $textoSistemaTO->setSt_texto($textoSubstituido);

            $this->mensageiro->setMensageiro($textoSistemaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro gerarTextoVenda: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna e gera grid das parcelas do contrato
     * @param int $id_venda
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridParcelasContrato($id_venda)
    {
//        $vwLancamentoTO = new VwLancamentoTO();
//        $vwLancamentoTO->setId_venda($id_venda);
//        $vwLancamentoTO = $vwLancamentoTO->fetch();
//        $this->_view->vwLancamentosTO = $vwLancamentoTO;
        $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $em = $bootstrap->getResource('doctrine')->getEntityManager();
        $repository = $em->getRepository('G2\Entity\VwLancamento');
        $this->_view->vwLancamentosTO = $repository->findBy(array('id_venda' => $id_venda));

        return $this->_view->render('/textos/grid-parcelas-contrato.phtml');
    }

    /**
     * Metodo que retorna e gera grid do plano de pagamento da graduação
     * @param int $id_venda
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridPlanoPagamentoGraduacao($id_venda)
    {
        $negocio = new \G2\Negocio\Negocio();

        $this->_view->vwVendaLancamento = $negocio->findBy('G2\Entity\VwVendaLancamento', array('id_venda' => $id_venda));

        $venda = $negocio->find('G2\Entity\Venda', $id_venda);
        $this->_view->venda = $venda;

        //verifica se existe campanha pontualidade, se não passa null
        if ($venda->getIdCampanhaPontualidade() != NULL) {
            //verifica se a campanha pontualidade será em valor (id=1) ou porcentagem (id=2)
            if ($venda->getIdCampanhaPontualidade()->getId_tipodesconto()->getId_tipodesconto() == \G2\Constante\TipoDesconto::VALOR) {
                $this->_view->valorcampanhapontualidade = $venda->getIdCampanhaPontualidade()->getNu_valordesconto();
            } else {
                $this->_view->valorcampanhapontualidade = ($venda->getNu_valorliquido() / 6) * ($venda->getIdCampanhaPontualidade()->getNu_valordesconto() / 100);
            }
        } else {
            $this->_view->valorcampanhapontualidade = null;
        }
        return $this->_view->render('/textos/grid-plano-pagamento-graduacao.phtml');
    }

    /**
     * @history AC-27138
     */
    public function getVwGradeNotaByMatricula($id_matricula, $where = array())
    {
        $negocio = new \G2\Negocio\Negocio();
        $query = $negocio
            //->getRepository('\G2\Entity\VwGridCertificado')
            ->getRepository('\G2\Entity\VwGradeNota')
            ->createQueryBuilder('vw')
            ->select('vw')
            ->where('vw.id_matricula = :id_matricula')
            ->setParameter('id_matricula', $id_matricula)
            ->orderBy('vw.dt_encerramentoextensao', 'DESC');

        if ($where && is_array($where)) {
            foreach ($where as $filter) {
                $query->andWhere("vw.{$filter}");
            }
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @history AC-27138
     * Metodo que retorna e gera grid de disciplinas do Projeto Pedagogico com carga horaria para as Declaracoes
     * @param int $id_matricula
     * @param array $campos
     * @param array $where
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridDisciplinasProjeto($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridDisciplinasProjeto é necessário informar a Matrícula');
        }

        $this->_view->campos = array(
            'st_tituloexibicaodisciplina' => 'Disciplina',
            'nu_cargahoraria' => 'Carga Horária'
        );

        $results = $this->getVwGradeNotaByMatricula($id_matricula, array(
            'id_tipodisciplina != ' . \TipoDisciplinaTO::AMBIENTACAO
        ));

        if ($results && is_array($results)) {
            $this->_view->arrOrganizador = $results;
            return $this->_view->render('textos/gridvwgradenota.phtml');
        }

        return '';
    }

    /**
     * @history AC-27138
     * Metodo que retorna e gera grid de disciplinas do Projeto Pedagogico com carga horaria para as Declaracoes
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridDisciplinasEncerradasComNota($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridDisciplinasProjeto é necessário informar a Matrícula');
        }

        $this->_view->campos = array(
            'st_tituloexibicaodisciplina' => 'Disciplina',
            'nu_cargahoraria' => 'Carga Horária',
            'nu_notatotal' => 'Nota'
        );

        $results = $this->getVwGradeNotaByMatricula($id_matricula, array(
            'id_tipodisciplina != ' . \TipoDisciplinaTO::AMBIENTACAO,
            'nu_notatotal > 0',
            "dt_encerramentoextensao < '" . date('Y-m-d') . "'"
        ));

        if ($results && is_array($results)) {

            foreach ($results as &$result) {
                $result->getNu_notatotal();

                //Adiciona texto de nota parcial.
                if ($result->getNu_notafinal() == 0 || $result->getNu_notafinal() == null) {
                    $result->setNu_notatotal($result->getNu_notatotal() . ' (nota parcial)');
                }
            }

            $this->_view->arrOrganizador = $results;
            return $this->_view->render('textos/gridvwgradenota.phtml');
        }

        return '';
    }

    /**
     * @history AC-27139
     * Metodo que retorna e gera grid de disciplinas de sala PRR do aluno
     * @param int $id_matricula
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridDisciplinasSalaPRR($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o gerarGridDisciplinasSalaPRR é necessário informar a Matrícula');
        }

        $this->_view->campos = array(
            'st_tituloexibicaodisciplina' => 'Disciplina',
            'dt_inicio' => 'Data Início'
        );

        $results = $this->getVwGradeNotaByMatricula($id_matricula, array(
            'id_categoriasala = ' . \G2\Constante\CategoriaSala::PRR
        ));

        if ($results && is_array($results)) {
            $this->_view->arrOrganizador = $results;
            return $this->_view->render('textos/gridvwgradenota.phtml');
        }

        return '';
    }

    public function retornarCursosContrato($id_contrato)
    {
        $primeiroAcessoBO = new PrimeiroAcessoBO();

        if (!$id_contrato) {
            $contratoMatriculaTO = new ContratoMatriculaTO();
            $contratoMatriculaTO->setId_matricula($contratoMatriculaTO->getSessao()->id_matricula);
            $contratoMatriculaTO->fetch(true, true, true);
            $id_contrato = $contratoMatriculaTO->getId_contrato();
        }

        $contrato = new ContratoTO();
        $contrato->setId_contrato($id_contrato);
        $contrato->fetch(true, true, true);

        $vwMatricula = new VwMatriculaTO();
        $vwMatricula->setId_contrato($contrato->getId_contrato());
        $vwMatricula = $vwMatricula->fetch(false, false);

        $cursos = '';
        if (count($vwMatricula) > 0) {
            foreach ($vwMatricula as $vw) {
                $cursos = $cursos ? ', ' : '';
                $cursos .= $vw->getSt_projetopedagogico();
            }
            return $cursos;
        } else {
            return $vwMatricula->getSt_projetopedagogico();
        }
    }


    public function retornaEnderecoNucleoUltimoAgendamento($id_matricula)
    {
        $negocio = new G2\Negocio\GerenciaProva();
        $result = $negocio->findOneByPresencaAgendamento(array(
            'id_matricula' => $id_matricula
        ));

        $string = '';
        if ($result) {
            $string .= 'Endereço: ' . $result['st_endereco'] . ', ' . $result['nu_numero'] . ', ' . $result['st_cidade'] . ' - ' . $result['sg_uf'];
        }

        return $string;
    }

    public function retornaDataUltimoAgendamento($id_matricula)
    {
        $negocio = new G2\Negocio\GerenciaProva();
        $result = $negocio->findOneByPresencaAgendamento(array(
            'id_matricula' => $id_matricula
        ));

        $string = '';
        if ($result) {
            $dt_agendamento = new \DateTime($result['dt_aplicacao']['date']);
            $string .= $dt_agendamento->format('d') . ' de ' . $this->retornaStringMes($dt_agendamento->format('m')) . ' de ' . $dt_agendamento->format('Y');
        }

        return $string;
    }

    public function retornaHorarioUltimoAgendamento($id_matricula)
    {
        $negocio = new G2\Negocio\GerenciaProva();
        $result = $negocio->findOneByPresencaAgendamento(array(
            'id_matricula' => $id_matricula
        ));

        $string = '';
        if ($result) {
            $hr_inicio = new \DateTime($result['hr_inicio']);
            $hr_fim = new \DateTime($result['hr_fim']);
            $string .= $hr_inicio->format('H:i') . ' às ' . $hr_fim->format('H:i');
        }

        return $string;
    }

    /**
     * Função para retornar o professor titular de um TCC
     * @param int $id_matricula
     * @return string
     */
    public function retornaProfessorTitularTCC($id_matricula)
    {
        $negocio = new G2\Negocio\Negocio();
        $sala_tcc = $negocio->findOneBy('\G2\Entity\VwSalaDisciplinaTurma', array(
            'id_matricula' => $id_matricula,
            'id_tipodisciplina' => \TipoDisciplinaTO::TCC
        ), null, 1);

        if ($sala_tcc && $sala_tcc->getId_saladeaula()) {
            $id_saladeaula = $sala_tcc->getId_saladeaula();

            $titular = $negocio->findOneBy('\G2\Entity\UsuarioPerfilEntidadeReferencia', array(
                'id_saladeaula' => $id_saladeaula,
                'bl_titular' => true
            ), null, 1);

            if ($titular && $titular->getId_usuario()) {
                return $titular->getId_usuario()->getSt_nomecompleto();
            }
        }

        return '';
    }

    /**
     * @param int $id_matricula
     * @return string
     */
    public function retornaProfessorCertificacaoTCC($id_matricula)
    {
        $negocio = new G2\Negocio\Negocio();
        $certificado = $negocio->findOneBy('\G2\Entity\VwGridCertificado', array(
            'id_matricula' => $id_matricula,
            'id_tipodisciplina' => \TipoDisciplinaTO::TCC
        ), null, 1);

        if ($certificado) {
            return $certificado->getSt_titularcertificacao();
        }

        return '';
    }

    /**
     * @param int|null $nu_mes
     * @param bool|true $uc_first
     * @return string
     */
    private function retornaStringMes($nu_mes = null, $uc_first = false)
    {
        if (!$nu_mes)
            $nu_mes = date('m');

        $meses = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        );

        $str = $meses[$nu_mes];
        return $uc_first ? ucfirst($str) : $str;
    }

    function salvarPDF($filename, $html_content, $paper_type = "P", $path)
    {
        $pdf = new Ead1_Pdf();
        $pdf->carregarHTML($html_content);

        if ($pdf->gerarPdf($filename, $path)) { //Tenta salvar o pdf gerado
            $mensageiro = new Ead1_Mensageiro(true, Ead1_IMensageiro::SUCESSO); // Salvo com sucesso.
        } else {
            $mensageiro = new Ead1_Mensageiro(false, Ead1_IMensageiro::ERRO, 'Erro ao salvar arquivo no servidor.'); // Erro ao salvar o arquivo
        }
        unset($pdf);
        Zend_Loader_Autoloader::getInstance()->removeAutoloader('DOMPDF_autoload');
        return $mensageiro;
        //$dompdf = new DOMPDF();
//        if ($paper_type == "L") {
//            $dompdf->set_paper("legal", "landscape"); // Altera o papel para modo paisagem.
//        }
//        $dompdf->load_html($html_content); // Carrega o HTML para a classe.
//        $dompdf->render();
//        $pdf = $dompdf->output(); // Cria o pdf
//        $arquivo = $path . $filename; // Caminho onde será salvo o arquivo.
//        if (file_put_contents($arquivo, $pdf)) { //Tenta salvar o pdf gerado
//            return new Ead1_Mensageiro(true, Ead1_IMensageiro::SUCESSO); // Salvo com sucesso.
//        } else {
//            return new Ead1_Mensageiro(false, Ead1_IMensageiro::ERRO, 'Erro ao salvar arquivo no servidor.'); // Erro ao salvar o arquivo
//        }
    }

    public function gerarPdfBoletos($idVenda)
    {
        try {
            $pagamento = new \G2\Negocio\BoletoPHP();
            return $pagamento->gerarPdfBoletosVenda($idVenda);
        } catch (\Exception $message) {
            throw $message;
        }
    }

    /**
     * @param mixed [String|Zend_Date|DateTime] $date
     * @return string
     */
    public function retornaDataPorExtenso($date)
    {
        if ($date) {
            $date = $this->retornaDateTime($date);
            return $date->format('d') . ' de ' . $this->retornaStringMes($date->format('m')) . ' de ' . $date->format('Y');
        }
        return '';
    }

    /**
     * @param mixed [String|Zend_Date|DateTime] $date
     * @return string
     */
    public function retornaMesAnoPorExtenso($date)
    {
        if ($date) {
            $date = $this->retornaDateTime($date);
            return $this->retornaStringMes($date->format('m')) . ' de ' . $date->format('Y');
        }
        return '';
    }

    /**
     * @param mixed [String|Zend_Date|DateTime] $date
     * @return \DateTime
     */
    public function retornaDateTime($date)
    {
        if ($date instanceof \Zend_Date) {
            return new DateTime($date->toString("yyyy-MM-dd HH:mm:ss"));
        }

        if (is_string($date)) {
            $dt = substr($date, 0, 10);
            $dt = implode('-', array_reverse(explode('/', trim($dt))));
            $hr = trim(substr($date, 11));

            $date = new DateTime(trim($dt . ' ' . $hr));
        }

        return $date;
    }

    /**
     * Metodo usado como mascara para datas em variaveis de textos de sistema.
     *
     * @param string $data
     * @return string
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function converteDataCliente($data)
    {
        return $this->formataDataExtensoPtBr($data);
    }

    /**
     * monta tabela para declaração de isenção de matrícula
     *
     * @param $data
     * @return string
     */
    public function montaTabelaIsencao($data)
    {
        $negocio = new \G2\Negocio\Negocio();
        $disciplinas = $negocio->getRepository('\G2\Entity\Matricula')->retornaDisciplinasIsencao($data);

        $html = '<table style="width:100%; border-collapse: collapse; border: 1px solid black" >
                   <tr>
                     <th style="border: 1px solid black; text-align: left">Disciplinas Isentas nesta IES</th>
                     <th style="border: 1px solid black; text-align: left">Carga Horária nesta IES</th>
                     <th style="border: 1px solid black; text-align: left">Disciplinas da IES de Origem</th> 
                     <th style="border: 1px solid black; text-align: left">Carga Horária na IES de Origem</th> 
                     <th style="border: 1px solid black; text-align: left">IES de origem</th>
                   </tr>';

        $countData = 0;

        foreach ($disciplinas as $disciplina) {
            $count = $negocio->getRepository('\G2\Entity\MatriculaDisciplina')->countDisciplinasDependentes($disciplina['id_matriculadisciplina'], 'id_matriculadisciplina');

            if ($count != 0) {
                $html = $this->montaTabela($html, $disciplina, $count);
                $countData++;
            }
        }

        $html .= '</table>';

        // evita montagem de tabela vazia
        if ($countData == 0) {
            $html = 'sem dados para isenção';
        }

        return $html;
    }

    /**
     * monta parta da tabela com as duas primeiras colunas com rowspan (http://confluence.unyleya.com.br/pages/viewpage.action?pageId=10159331)
     *
     * @param $html
     * @return string
     */
    public function montaTabela($html, $disciplina, $count)
    {
        $negocio = new \G2\Negocio\Negocio();
        $discOrigem = $negocio->getRepository('\G2\Entity\MatriculaDisciplina')->getDadosDiscOrigem($disciplina['id_matriculadisciplina']);

        $countDiscRelacao = $negocio->getRepository('\G2\Entity\MatriculaDisciplina')->countDisciplinasDependentes($discOrigem[0]['id_disciplinaorigem'], 'id_disciplinaorigem');

        if ($count > 0) {
            $html .= '<tr>
                <td rowspan="' . $count . '" style="border: 1px solid black">' . $disciplina['st_disciplina'] . '</td>
                <td rowspan="' . $count . '" style="border: 1px solid black">' . $disciplina['nu_cargahoraria'] . '</td>
                ';

            $i = 0;

            // evita que sejam montadas mais linhas do que o rowspan
            while ($i < $count) {
                $html .= '<td style="border: 1px solid black">
                    <table style="width:100%" >
                      <tr>
                        <td>' . $discOrigem[$i]['st_disciplina'] . '</td>
                      </tr>
                    </table>
                  </td>';

                $discOrigem[$i]['nu_cargahoraria'] = $discOrigem[$i]['nu_cargahoraria'] == null ? '-' : $discOrigem[$i]['nu_cargahoraria'];

                $html .= '<td style="border: 1px solid black">
                     <table style="width:100%" >
                       <tr>
                         <td>' . $discOrigem[$i]['nu_cargahoraria'] . '</td>
                       </tr>
                     </table>
                   </td>';

                $html .= '<td style="border: 1px solid black">
                     <table style="width:100%" >
                       <tr>
                         <td>' . $discOrigem[$i]['st_instituicao'] . '</td>
                       </tr>
                     </table>
                   </td>';
                $html .= '</tr>';

                $i++;
            }
        }

        return $html;
    }

    /**
     * Converte data da variavel do texto sistema e retorna a data convertida concatenada com a string
     * @param $data
     * @return string
     */
    private function converteDataAceite($data)
    {
        if ($data) {
            $data = $this->converteDataCliente($data);
            return "Data de Aceite do Contrato: " . $data . ".";
        }
        return '';

    }


    /**
     * Gera grid de disciplinas para e-mail de renovacao do aluno
     * @param $id_venda
     * @return string
     * @throws Exception
     */
    public function gerarGridDisciplinasRenovacao($id_matricula)
    {
        if (!$id_matricula) {
            throw new Exception('Para utilizar o gerarGridDisciplinasRenovacao é necessário informar a matricula.');
        }
        $negocio = new \G2\Negocio\Matricula();
        $arrDisciplinas = $negocio->getRepository('\G2\Entity\Matricula')->retornarGridDisciplinasByMatriculaRenovacao($id_matricula);
        $arrOrganizador = array();
        foreach ($arrDisciplinas as $prematricula) {
            $arrOrganizador[] = $prematricula['st_tituloexibicao'];
        }
        $this->_view->arrDisciplinas = $arrOrganizador;
        return $this->_view->render('textos/griddisciplinasrenovacao.phtml');
    }


    /**
     * Retorna String da grade disciplina
     * @return string
     */
    public function gerarUrlAcessoGradeDisciplina()
    {
        return Ead1_Ambiente::geral()->st_url_portal . '/grade-disciplina';
    }

    public function retornaValorSemestre($id_matricula)
    {
        return $this->formataValor($this->_negocio->getRepository('\G2\Entity\VwMatricula')->retornaVariaveisVendaConfirmada($id_matricula, 'nu_valorliquido'));
    }

    public function retornaValorEntrada($id_matricula)
    {
        return $this->formataValor($this->_negocio->getRepository('\G2\Entity\VwMatricula')->retornaVariaveisVendaConfirmada($id_matricula, 'nu_valorentrada'));
    }

    public function retornaValorParcela($id_matricula)
    {
        return $this->formataValor($this->_negocio->getRepository('\G2\Entity\VwMatricula')->retornaVariaveisVendaConfirmada($id_matricula, 'nu_valormensalidade'));
    }

    public function retornaNumeroParcelas($id_matricula)
    {
        return $this->_negocio->getRepository('\G2\Entity\VwMatricula')->retornaVariaveisVendaConfirmada($id_matricula, 'nu_parcelas');
    }

    public function retornarCargaHorariaIntegralizada($id_matricula)
    {
        $matricula = $this->_negocio
                          ->getRepository('\G2\Entity\VwMatriculaCargaHoraria')
                          ->find($id_matricula);

        return null != $matricula ? $matricula->getNu_cargahorariaintegralizada() : '-';
    }

    public function retornaCoeficienteRendimento($id_matricula)
    {
        try {
            $matricula = new \G2\Negocio\Matricula();
            $cf_acumulado = $matricula->retornarCoeficienteRendimento($id_matricula);

            return (is_null($cf_acumulado) ? '--' : number_format($cf_acumulado, 1, ',', ''));
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    public function retornaCoeficienteRendimentoSemDecimais($id_matricula)
    {
        try {
            $matricula = new \G2\Negocio\Matricula();
            $cf_acumulado = $matricula->retornarCoeficienteRendimento($id_matricula);

            return (is_null($cf_acumulado) ? '--' : number_format($cf_acumulado, 0, ',', ''));
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    public function gerarGridHistoricoEscolar($id_matricula)
    {
        try {
            if (!$id_matricula) {
                throw new Exception('Para utilizar o gerarGridHistoricoEscolar é necessário informar a matricula.');
            }
            $negocio = new \G2\Negocio\Matricula();
            $arrGrid = $negocio->getRepository('\G2\Entity\Matricula')->retornarGridHistoricoEscolar($id_matricula);
            $this->_view->arrGrid = $arrGrid;
            return $this->_view->render('textos/gridhistoricoescolar.phtml');
        } catch (Exception $e) {
            echo 'Erro: ' . $e->getMessage();
            return false;
        }
    }


    /**
     * Retorna as variaveis maiusculas
     * @param $variavel
     * @return string
     */
    public function retornaVariaveisUpper($variavel)
    {
        $encoding = mb_internal_encoding();
        return mb_strtoupper($variavel, $encoding);
    }

    /**
     * Retorna a data americana convertida em PT
     * @param $data
     * @return bool|string
     */
    public function converterDataUsaBr($data)
    {
        if ($data) {
            $negocio = new \G2\Negocio\Negocio();
            return $negocio->converterData($data, 'd/m/Y');
        }

        return '#data_expedicao_certificado#';
    }

    /**
     * Retorna a nacionalidade
     * @param $id_pais
     * @return string
     */
    public function retornaNacionalidade($id_pais)
    {
        if ($id_pais) {
            if ($id_pais == 22) {
                return 'BRASILEIRA';
            }
            return 'ESTRANGEIRA';
        }
        return '#nacionalidade_aluno#';
    }

    /**
     * Retorna string com o municipio e UF do último agendamento do aluno.
     */

    public function gerarStringMunicipioUfAgendamento()
    {
        $negocio = new G2\Negocio\Negocio();
        $stringFormatada = null;
        $result = $negocio->findOneBy('\G2\Entity\VwAvaliacaoAgendamento', array('id_matricula' => $this->_params['id_matricula']), array('id_avaliacaoagendamento' => 'DESC'));
        if ($result) {
            $stringFormatada = $result->getSt_cidade() . ' - ' . $result->getSg_uf();
        }
        return $stringFormatada ? $stringFormatada : '-';
    }


    /**
     * Adiciona os textos de cabeçalho e rodapé em um texo de declaração caso estejam cadastrados
     * @param TextoSistemaTO $to
     * @param $id_textocategoria
     * @return string
     */
    public function adicionaCabecalhoRodape(TextoSistemaTO $to, $id_textocategoria)
    {
        try {
            $negocio = new G2\Negocio\Negocio();
            $entity = $negocio->find('\G2\Entity\TextoSistema', $to->getId_textosistema());
            if ($entity instanceof \G2\Entity\TextoSistema && $entity->getId_textosistema()) {
                if ($id_textocategoria == \G2\Constante\TextoCategoria::CABECALHO && $entity->getId_cabecalho() instanceof \G2\Entity\TextoSistema && $entity->getId_cabecalho()->getSt_textosistema()) {
                    return $entity->getId_cabecalho()->getSt_texto();
                } elseif ($id_textocategoria == \G2\Constante\TextoCategoria::RODAPE && $entity->getId_rodape() instanceof \G2\Entity\TextoSistema && $entity->getId_rodape()->getSt_textosistema()) {
                    return $entity->getId_rodape()->getSt_texto();
                }
            }
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Gera parcelas adicionadas em uma transferencia
     * @param $id_vendaaditivo
     * @return bool|string
     */
    public function gerarGridParcelasTransferencia($id_vendaaditivo)
    {
        try {
            if (!$id_vendaaditivo) {
                return '#planopagamento_transferencia#';
            }

            $negocio = new G2\Negocio\Negocio();
            $to = new VwVendaLancamentoTO();
            $to->setId_vendaaditivo($id_vendaaditivo);

            $orm = new VwVendaLancamentoORM();
            $arrEn_ = $orm->consulta($to);
            $arrVendaLanacamento = null;
            if(!empty($arrEn_)){
                foreach ($arrEn_ as $key => $ent) {
                    $arrVendaLanacamento[$key]['dt_vencimento'] = $negocio->converterData($ent->dt_vencimento, 'd/m/Y');
                    $arrVendaLanacamento[$key]['nu_valor'] = number_format($ent->nu_valor, 2, '.', ',');
                    $arrVendaLanacamento[$key]['st_meiopagamento'] = utf8_decode($ent->st_meiopagamento);
                }
            }

            $this->_view->arrGrid = $arrVendaLanacamento;
            $html = $this->_view->render('textos/gridparcelastransferencia.phtml');
            return mb_detect_encoding($html, 'UTF-8', true) === TRUE ? $html : utf8_encode($html);

        } catch (Exception $e) {
            echo 'Erro: ' . $e->getMessage();
            return false;
        }
    }

    public function retornarDia($dt)
    {
        return ($dt) ? \G2\Utils\Helper::converterData($dt, 'd') : $dt;
    }

    public function retornarMes($dt)
    {
        return ($dt) ? \G2\Utils\Helper::converterData($dt, 'm') : $dt;
    }

    public function retornarAno($dt)
    {
        return ($dt) ? \G2\Utils\Helper::converterData($dt, 'Y') : $dt;
    }

    public function retornarSemestre($dt)
    {
        if ($dt) {
            return \G2\Utils\Helper::converterData($dt, 'm') >= 7 ? 2 : 1;
        }

        return $dt;
    }

    /**
     * Metodo usado como mascara para datas em variaveis de textos de sistema.
     * @param string $data
     * @return string
     * @author Denise Xavier <denise.xavier@unyleya.com.br>
     */
    public function convertDataToBr($data)
    {
        return ($data) ? \G2\Utils\Helper::converterData($data, 'd/m/Y') : '--';
    }

    /**
     * retorna a titulação de acordo com o projeto acadêmico do aluno
     *
     * @param $id_matricula
     * @return mixed
     */
    public function gerarTitulacao($id_matricula)
    {
        $negocio = new \G2\Negocio\Matricula();
        return $negocio->retornarTitulacao($id_matricula);
    }

    /**
     * retorna a titulação do projeto de acordo com o projeto acadêmico do aluno
     *
     * @param $id_matricula
     * @return mixed
     */
    public function gerarTitulacaoProjeto($id_matricula)
    {
        $negocio = new \G2\Negocio\Matricula();
        return $negocio->retornarTitulacao($id_matricula, true);
    }

    /**
     * Metodo que retorna e gera grid do plano de pagamento da graduação para
     * ajustar a venda na renovação.
     * @param int $id_venda
     * @throws Zend_Exception
     * @return String
     */
    public function gerarGridPlanoPagamentoGraduacaoPrimeiraVenda($id_venda)
    {
        $negocio = new \G2\Negocio\Negocio();

        $this->_view->vwVendaLancamento = $negocio->findBy('G2\Entity\VwVendaLancamento', array('id_venda' => $id_venda));

        $venda = $negocio->find('G2\Entity\Venda', $id_venda);
        $this->_view->venda = $venda;

        //verifica se existe campanha pontualidade, se não passa null
        if ($venda->getIdCampanhaPontualidade() != NULL) {
            //verifica se a campanha pontualidade será em valor (id=1) ou porcentagem (id=2)
            if ($venda->getIdCampanhaPontualidade()->getId_tipodesconto()->getId_tipodesconto() == \G2\Constante\TipoDesconto::VALOR) {
                $this->_view->valorcampanhapontualidade = $venda->getIdCampanhaPontualidade()->getNu_valordesconto();
            } else {
                $this->_view->valorcampanhapontualidade = ($venda->getNu_valorliquido() / 6) * ($venda->getIdCampanhaPontualidade()->getNu_valordesconto() / 100);
            }
        } else {
            $this->_view->valorcampanhapontualidade = null;
        }
        return $this->_view->render('/textos/grid-plano-pagamento-graduacao-primeira-venda.phtml');    }

    /**
     * Função responsável por calcular até qual dia o aluno pode entrar com recurso para revisao de prova,
     * Para então retornar esta informação no e-mail. Através das variáveis de texto.
     *
     * @return string
     * @throws Zend_Date_Exception
     */
    public function calcularDataRecurso()
    {
        $date = new Zend_Date();
        $date->setDate(Zend_Date::now());
        $date->addDay('15', Zend_Date::DAY);
        return '' . $date->get('dd/MM/YYYY');
    }

    /**
     * Retorna HTML com a grid disciplina Certificado Ucam
     * @param $id_matricula
     * @return string
     * @throws Zend_Exception
     */
    public function retornarDisciplinaCertificadoUcam($id_matricula)
    {
        if (!$id_matricula) {
            throw new Zend_Exception('Para utilizar o retornarDisciplinaCertificadoUcam é necessário informar a Matrícula');
        }
        $arrOrganizador = $this->retornarArrVwGridCertificadoUcam($id_matricula);
        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/gridnotasdisciplinascertificadoUcam.phtml');
    }

    /**
     * Retorna Array da grid certificado Ucam
     * @param $idMatricula
     * @return array
     * @throws Zend_Exception
     */
    public function retornarArrVwGridCertificadoUcam($idMatricula)
    {
        try {

            $negocio = new \G2\Negocio\Negocio();
            $dados = $negocio->findBy('\G2\Entity\VwGridCertificado',
                array('id_matricula' => $idMatricula),
                array(
                'id_tipodisciplina' => 'ASC',
                'dt_encerramentoextensao' => 'ASC'
                )
            );


            $arrDisciplinas = array();
            foreach ($dados as $index => $VwGridCertificado) {

                //Convertendo nota ex: 74.00 para 7,40
                $nota = (int)$VwGridCertificado->getNu_aprovafinal();
                $nota = preg_replace("/^([0-9]+)*?([0-9]{1})$/", "$1.$2", $nota);
                $nota = number_format($nota, 2, ',', '');

                $arrDisciplinas[$index]['st_disciplina'] = $VwGridCertificado->getSt_disciplina();
                $arrDisciplinas[$index]['nu_aprovafinal'] = $nota;
                $arrDisciplinas[$index]['st_cargahoraria'] = $VwGridCertificado->getNu_cargahoraria();
                $arrDisciplinas[$index]['st_titularcertificacao'] = $VwGridCertificado->getSt_titularcertificacao();
                $arrDisciplinas[$index]['st_titulacao'] = $VwGridCertificado->getSt_titulacao();
            }
            return $arrDisciplinas;

        } catch (Zend_Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Função responsável por buscar a grade de disciplinas para o certificado parcial
     *
     * @param $id_matricula
     * @param $arrParams
     * @return string
     * @throws Zend_Exception
     */
    public function gerarGridDisciplinasCertificadoParcial($idMatricula, $arrParams)
    {
        $negocio = new \G2\Negocio\CertificadoParcial();
        $arrOrganizador = $negocio->retornaDisciplinasCertificadoParcial($idMatricula, $arrParams);

        $this->_view->arrOrganizador = $arrOrganizador;
        return $this->_view->render('textos/griddisciplinascertificadoparcial.phtml');
    }

    /**
     * Função responsável por buscar o registro do certificado parcial
     *
     * @param $idMatricula
     * @param $arrParams
     * @return string
     * @throws Exception
     */
    public function gerarRegistroCertificadoParcial($idMatricula, $arrParams)
    {
        $negocio = new \G2\Negocio\CertificadoParcial();
        $registro = $negocio->gerarRegistroCertificadoParcial($arrParams["id_certificadoparcial"], $idMatricula, $arrParams["id_entidade"]);

        if($registro instanceof  Ead1_Mensageiro){
            return "Por favor preencha o campo Sigla para Certificado.";
        }

        return $registro;
    }

    public function gerarPeriodoRealizacaoCertificadoParcial($idMatricula, $arrParams)
    {
        $negocio = new \G2\Negocio\CertificadoParcial();
        $periodo = $negocio->gerarPeriodoRealizacaoCertificadoParcial($idMatricula, $arrParams);
        return $periodo;
    }

    /**
     * @param $id_matricula
     */
    public function gerarTituloMonografia($id_matricula)
    {
        $negocioMatricula = new \G2\Negocio\Matricula();
        $matricula = $negocioMatricula->buscarTcc(array('id_matricula' => $id_matricula));

        if (array_key_exists('st_tituloavaliacao', $matricula)) {
            return $matricula['st_tituloavaliacao'];
        }
        return false;
    }

    /**
     * Retorna o número de uma parcela com base no lançamento
     * @param $id_lancamento
     * @return int|string
     */
    public function retornaLancamentoParcela($id_lancamento)
    {

        if(!$id_lancamento) return '0';

        $lanP = new \G2\Entity\LancamentoVenda();
        $lanP->findOneBy(array('id_lancamento'=>$id_lancamento));

        return $lanP->getNuOrdem() ? $lanP->getNuOrdem() : '0';

    }
}

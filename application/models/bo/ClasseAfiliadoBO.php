<?php

/**
 * Classe com regras de negócio para interações de ClasseAfiliado
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage bo
 */
class ClasseAfiliadoBO extends Ead1_BO
{

    public $mensageiro;

    /**
     * @var ClasseAfiliadoDAO
     */
    public $dao;

    public function __construct ()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new ClasseAfiliadoDAO();
    }

    /**
     * Método que decide se cadastra ou edita um ClasseAfiliadoTO
     * @param ClasseAfiliadoTO $classeAfiliadoTO
     * @return Ead1_Mensageiro
     */
    public function salvarClasseAfiliado (ClasseAfiliadoTO $classeAfiliadoTO)
    {
        try {
            $this->dao->beginTransaction();

            if ($classeAfiliadoTO->getId_classeafiliado()) {
                $this->mensageiro = $this->editarClasseAfiliado($classeAfiliadoTO);
            } else {
                $this->mensageiro = $this->cadastrarClasseAfiliado($classeAfiliadoTO);
            }

            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro("Erro ao salvar ou editar Classe de Afiliados!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra uma Classe de Afiliados
     * @param ClasseAfiliadoTO $classeAfiliadoTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarClasseAfiliado (ClasseAfiliadoTO $classeAfiliadoTO)
    {
        try {

            $this->dao->beginTransaction();
            $classeAfiliadoTO->setId_entidade($classeAfiliadoTO->getSessao()->id_entidade);
            $classeAfiliadoTO->setId_usuariocadastro($classeAfiliadoTO->getSessao()->id_usuario);
            $classeAfiliadoTO->setDt_cadastro(new Zend_Date());

            $classeAfiliadoTO->setId_classeafiliado($this->dao->cadastrarClasseAfiliado($classeAfiliadoTO));

            $this->dao->commit();
            $this->mensageiro->setMensageiro($classeAfiliadoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao salvar classe de afiliado: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Método que edita uma Classe de Afiliado
     * @param ClasseAfiliadoTO $classeAfiliadoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function editarClasseAfiliado (ClasseAfiliadoTO $classeAfiliadoTO)
    {
        try {
            $this->dao->beginTransaction();

            if (!$this->dao->editarClasseAfiliado($classeAfiliadoTO)) {
                throw new Zend_Exception("Erro ao editar Classe de Afiliado - {$classeAfiliadoTO->getId_classeafiliado()} - {$classeAfiliadoTO->getSt_classeafiliado()}!");
            }

// 			//Exclui todos os vinculos para depois serem refeitos ao salvar o comissionamento
// 			$vinculo = new ClasseAfiliadoComissionamentoTO();
// 			$vinculo->setId_classeafiliado($classeAfiliadoTO->getId_classeafiliado());
// 			$this->desvincularComissionamentosClasseAfiliado($vinculo);

            $this->dao->commit();
            $this->mensageiro->setMensageiro($classeAfiliadoTO, Ead1_IMensageiro::SUCESSO, 'Classe de Afiliado Editada com Sucesso!');
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna Classes de Afiliados
     * @param ClasseAfiliadoTO $to
     * @param unknown_type $where
     * @return Ead1_Mensageiro
     */
    public function retornarClasseAfiliado (ClasseAfiliadoTO $to, $where = null)
    {
        try {
            $retorno = $this->dao->retornarClasseAfiliado($to, $where);
            if (empty($retorno)) {
                $this->mensageiro->setMensageiro('Nenhuma Classe de Afiliado Encontrada!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ClasseAfiliadoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Classe de Afiliado', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que decide se cadastra ou edita um ComissionamentoTO
     * @param ComissionamentoTO $comissionamento
     */
    public function salvarComissionamento (ClasseAfiliadoTO $classeAfiliado, ComissionamentoTO $comissionamento, array $meiosPagamento = null)
    {
        if ($comissionamento->getId_comissionamento()) {
            return $this->editarComissionamento($classeAfiliado, $comissionamento, $meiosPagamento);
        } else {
            return $this->cadastrarComissionamento($classeAfiliado, $comissionamento, $meiosPagamento);
        }
    }

    /**
     * Método que cadastra um array de ComissionamentoTO
     * @param ClasseAfiliadoTO $classeAfiliado
     * @param array $comissionamentos
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarArrayComissionamentos (ClasseAfiliadoTO $classeAfiliado, array $comissionamentos)
    {
        try {
            if ($comissionamentos) {
                $this->dao->beginTransaction();
                foreach ($comissionamentos as $comissionamento) {
                    if ($this->salvarComissionamento($classeAfiliado, $comissionamento)->getTipo() !== Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception('Erro ao salvar comissionamentos da Classe de Afiliado!');
                    }
                }
                $this->dao->commit();
                $this->mensageiro->setMensageiro('Comissionamentos Salvos com sucesso!', Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Não existem comissionamentos para salvar.', Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao salvar comissionamentos!', Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra um Comissionamento
     * @param ComissionamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarComissionamento (ClasseAfiliadoTO $classeAfiliado, ComissionamentoTO $to, array $meiosPagamento = null)
    {
        try {

            $this->dao->beginTransaction();
            $to->setDt_cadastro(new Zend_Date());
            $to->setId_usuariocadastro($to->getSessao()->id_usuario);
            $to->setBl_ativo(true);

            $to->setId_comissionamento($this->dao->cadastrarComissionamento($to));

            if ($to->getId_comissionamento()) {
                $vinculo = new ClasseAfiliadoComissionamentoTO();
                $vinculo->setId_classeafiliado($classeAfiliado->getId_classeafiliado());
                $vinculo->setId_comissionamento($to->getId_comissionamento());
                $this->vincularClasseAfiliadoComissionamento($vinculo);
            }

            if ($to->getId_comissionamento() && $meiosPagamento) {
                $this->desvincularMeiosPagamentoComissionamento($to);
                $this->cadastrarMeiosPagamentoComissionamento($to, $meiosPagamento);
            }

            $this->dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao salvar comissionamento: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que edita um ComissionamentoTO
     * @param ComissionamentoTO $comissionamento
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function editarComissionamento (ClasseAfiliadoTO $classeAfiliado, ComissionamentoTO $comissionamento, array $meiosPagamento = null)
    {
        try {
            $this->dao->beginTransaction();

            try {
                $this->dao->editarComissionamento($comissionamento);
            } catch (Exception $e) {
                throw new Zend_Exception("Erro ao editar Comissionamento - {$comissionamento->getId_comissionamento()}! - {$e->getMessage()}");
            }

            if ($comissionamento->getId_comissionamento() && !$comissionamento->bl_ativo) {
                $vinculo = new ClasseAfiliadoComissionamentoTO();
                $vinculo->setId_classeafiliado($classeAfiliado->getId_classeafiliado());
                $vinculo->setId_comissionamento($comissionamento->getId_comissionamento());
                //desvincula para vincular novamente
                $this->desvincularComissionamentosClasseAfiliado($vinculo);
                //$this->vincularClasseAfiliadoComissionamento($vinculo);
            }

            if ($comissionamento->getId_comissionamento() && $meiosPagamento) {
                $this->desvincularMeiosPagamentoComissionamento($comissionamento);
                $this->cadastrarMeiosPagamentoComissionamento($comissionamento, $meiosPagamento);
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro($comissionamento, Ead1_IMensageiro::SUCESSO, 'Comissionamento Editado com Sucesso!');
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
        }
        return $this->mensageiro;
    }

    /**
     * Método que exclui logicamento um comissionamento
     * @param ComissionamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function deletarComissionamento (ComissionamentoTO $comissionamento)
    {

        try {
            $this->dao->beginTransaction();
            try {
                $comissionamento->setBl_ativo(false);
                $this->dao->editarComissionamento($comissionamento);
            } catch (Exception $e) {
                throw new Zend_Exception("Erro ao excluir Comissionamento - {$comissionamento->getId_comissionamento()}! - {$e->getMessage()}");
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro($comissionamento, Ead1_IMensageiro::SUCESSO, 'Comissionamento Excluido com Sucesso!');
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
        }
        return $this->mensageiro;
    }

    /**
     * Método que vincula um comissionamento em uma Classe de Afiliados
     * @param ClasseAfiliadoTO $classeAfiliado
     * @param ComissionamentoTO $comissionamento
     */
    private function vincularClasseAfiliadoComissionamento (ClasseAfiliadoComissionamentoTO $vinculo)
    {
        try {
            $this->dao->beginTransaction();

            try {
                $this->dao->vincularClasseAfiliadoComissionamento($vinculo);
            } catch (Exception $e) {
                throw new Zend_Exception("Erro ao vincular Comissionamento - {$vinculo->getId_comissionamento()}! - {$e->getMessage()}");
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro($vinculo, Ead1_IMensageiro::SUCESSO, 'Comissionamento vinculado à Classe de Afiliado com Sucesso!');
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
        }
        return $this->mensageiro;
    }

    /**
     * Método que exclui todos os vinculos entre comissionamento e classe afiliado de uma determinada classe de afiliados
     * @param ClasseAfiliadoTO $to
     * @return Ead1_Mensageiro
     */
    public function desvincularComissionamentosClasseAfiliado (ClasseAfiliadoComissionamentoTO $to)
    {
        try {

            $this->dao->beginTransaction();

            if (!$to && !$to->getId_classeafiliado() && !$to->getId_comissionamento()) {
                $this->mensageiro->setMensageiro('Não foi possível excluir os vínculos de comissionamentos, id_classeafiliado e id_comissionamento estão null');
            } else {
                $this->dao->desvincularComissionamentosClasseAfiliado($to);
                $this->mensageiro->setMensageiro("Comissionamentos desvinculados da Classe de Afiliados com Sucesso!", Ead1_IMensageiro::SUCESSO);
                $this->dao->commit();
            }
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao desvincular Comissionamentos da Classe de Afiliados', Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Retorna um objeto ComissionamentoTO
     * @param ComissionamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarComissionamento (ComissionamentoTO $to, $where = null)
    {
        try {
            $retorno = $this->dao->retornarComissionamento($to, $where);
            if (empty($retorno)) {
                $this->mensageiro->setMensageiro('Nenhum Comissionamento Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ComissionamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Comissionamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna Comissionamentos de uma Classe de Afiliados
     * @param ClasseAfiliadoTO $to
     * @param mixed $where
     * @return Ead1_Mensageiro
     */
    public function retornarComissionamentosClasseAfiliados (ClasseAfiliadoTO $to, $where = null)
    {
        $comissionamentos = array();
        try {
            $classeAfiliadoComissionamento = new ClasseAfiliadoComissionamentoTO();
            $classeAfiliadoComissionamento->setId_classeafiliado($to->getId_classeafiliado());
            $retorno = $this->dao->retornarComissionamentosClasseAfiliados($classeAfiliadoComissionamento, $where);

            if (!$retorno) {
                $this->mensageiro->setMensageiro('Nenhum Comissionamento Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            } else {
                foreach ($retorno as $dados) {
                    $comissionamento = new ComissionamentoTO();
                    $comissionamento->setId_comissionamento($dados['id_comissionamento']);
                    $comissionamento->fetch(true, true, true);
                    if ($comissionamento->getBl_ativo()) {
                        $comissionamentos[] = $comissionamento;
                    }
                }
                usort($comissionamentos, "ClasseAfiliadoBO::ordenarArrayComissionamentos");
            }

            $this->mensageiro->setMensageiro($comissionamentos, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Comissionamentos da Classe de Afiliados', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    static function ordenarArrayComissionamentos ($obj1, $obj2)
    {
        if ($obj1->getNu_minmeta() < $obj2->getNu_minmeta()) {
            return -1;
        } elseif ($obj1->getNu_minmeta() > $obj2->getNu_minmeta()) {
            return +1;
        }
        return 0;
    }

    /**
     * Metodo que salva os vinculos de Meios de Pagamento com o Comissionamento
     * @param ComissionamentoTO $comissionamento
     * @param array $meiosPagamento
     * @return Ead1_Mensageiro
     */
    public function cadastrarMeiosPagamentoComissionamento (ComissionamentoTO $comissionamento, array $meiosPagamento)
    {
        try {

            $this->dao->beginTransaction();

            if (!$meiosPagamento) {
                $this->mensageiro->setMensageiro('Nenhum meio de pagamento a ser vinculado com o comissionamento da classe de afiliado.');
            } else {

                $arrayMeioPagamentoComissionamento = array();

                foreach ($meiosPagamento as $meio) {

                    $cm = new ComissionamentoMeioTO();
                    $cm->setId_comissionamento($comissionamento->getId_comissionamento());
                    $cm->setId_meiopagamento($meio->getId_meiopagamento());
                    $arrayMeioPagamentoComissionamento[] = $cm;

                    $this->dao->cadastrarMeioPagamentoComissionamento($cm);
                }

                $this->dao->commit();
                $this->mensageiro->setMensageiro("Meios de Pagamento do Comissionamento salvos com sucesso!", Ead1_IMensageiro::SUCESSO);
            }
        } catch (Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao salvar meios de pagamento do comissionamento: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Método que exclui todos os vinculos de Meio de Pagamento com Comissionamento
     * @param ComissionamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function desvincularMeiosPagamentoComissionamento (ComissionamentoTO $to)
    {
        try {

            $this->dao->beginTransaction();

            if (!$to || !$to->getId_comissionamento()) {
                $this->mensageiro->setMensageiro('Não foi possível excluir os meios de pagamento, Id_comissionamento está null');
            } else {
                $this->dao->desvincularMeiosPagamentoComissionamento($to);
                $this->mensageiro->setMensageiro("Meios de Pagamento do Comissionamento excluidos com Sucesso!", Ead1_IMensageiro::SUCESSO);
                $this->dao->commit();
            }
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao excluir Meio de Pagamento do Comissionamento', Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Método que retorna os meios de pagamento relacionados com o comissionamento
     * @param ComissionamentoTO $comissionamento
     * @param unknown_type $where
     * @return Ead1_Mensageiro
     */
    public function retornarMeiosPagamentoComissionamento (ComissionamentoTO $comissionamento, $where = null)
    {
        $meiosPagamento = array();
        try {
            $comissionamentoMeio = new ComissionamentoMeioTO();
            $comissionamentoMeio->setId_comissionamento($comissionamento->getId_comissionamento());
            $retorno = $this->dao->retornarMeiosPagamentoComissionamentos($comissionamentoMeio, $where);

            if (!$retorno) {
                $this->mensageiro->setMensageiro('Nenhum Meio de Pagamento Encontrado para o Comissionamento!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            } else {
                foreach ($retorno as $dados) {
                    $meioPag = new MeioPagamentoTO();
                    $meioPag->setId_meiopagamento($dados['id_meiopagamento']);
                    $meioPag->fetch(true, true, true);
                    $meiosPagamento[] = $meioPag;
                }
            }

            $this->mensageiro->setMensageiro($meiosPagamento, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Meios de Pagamento do Comissionamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna TipoPessoaTO
     * @param TipoPessoaTO $tipoPessoaTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaTipoPessoa (TipoPessoaTO $tipoPessoaTO)
    {
        try {
            $retorno = $this->dao->retornaTipoPessoa($tipoPessoaTO);
            if (!$retorno) {
                throw new Zend_Exception('Nenhum tipo pessoa encontrado!');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoPessoaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retorna tipo pessoa: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Cadastra Afiliado
     * @param ContratoAfiliadoTO $to
     */
    public function cadastrarContratoAfiliado (ContratoAfiliadoTO $to)
    {
        try {
            if (!$to->getId_usuariocadastro()) {
                $to->setId_usuariocadastro($to->getSessao()->id_usuario);
            }
            if (!$to->getId_entidadecadastro()) {
                $to->setId_entidadecadastro($to->getSessao()->id_entidade);
            }
            $id_contratoafiliado = $this->dao->cadastrarContratoAfiliado($to);
            if (!$id_contratoafiliado) {
                throw new Zend_Exception($this->dao->excecao['SQL']);
            }
            $to->setId_contratoafiliado($id_contratoafiliado);
            return $this->mensageiro->setMensageiro($to);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao cadastrar Afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Cadastra uma série de produtos, categorias e comissões para um afiliado. 
     * Como parâmetro tem o array de ComissionamentoTO e array de AfiliadoProdutoTO.
     * @param array $arrAfiliadoProduto
     * @param array $arrComissionamento
     * @return Ead1_Mensageiro
     */
    public function cadastrarArrayAfiliadoProdutoComissionamento ($arrAfiliadoProduto, $arrComissionamento)
    {
        try {
            $arrDados = array();
            $this->dao->beginTransaction();
            if (!empty($arrAfiliadoProduto)) {
                $afiliadoProdutoDeleta = new AfiliadoProdutoTO();
                $afiliadoProdutoDeleta->setBl_ativo(false);
                $this->editarAfiliadoProduto($afiliadoProdutoDeleta, 'id_contratoafiliado = ' . $arrAfiliadoProduto[0]->id_contratoafiliado)->subtractMensageiro();

                foreach ($arrAfiliadoProduto as $key => $afiliadoProdutoCompletoTO) {
                    $comissionamentoTO = new ComissionamentoTO();
                    $comissionamentoTO->setNu_comissao($arrComissionamento[$key]->nu_comissao);
                    $comissionamentoTO->setId_usuariocadastro($comissionamentoTO->getSessao()->id_usuario);
                    $comissionamentoTO->setNu_minmeta(0);

                    $afiliadoProdutoTO = new AfiliadoProdutoTO();
                    $afiliadoProdutoTO->setId_afiliadoproduto($afiliadoProdutoCompletoTO->getId_afiliadoproduto());
                    $afiliadoProdutoTO->setId_categoria($afiliadoProdutoCompletoTO->getId_categoria());
                    $afiliadoProdutoTO->setId_comissionamento($afiliadoProdutoCompletoTO->getId_comissionamento());
                    $afiliadoProdutoTO->setId_contratoafiliado($afiliadoProdutoCompletoTO->getId_contratoafiliado());
                    $afiliadoProdutoTO->setId_produto($afiliadoProdutoCompletoTO->getId_produto());
                    $afiliadoProdutoTO->setBl_ativo(true);

                    if ($afiliadoProdutoTO->getId_afiliadoproduto()) {
                        $comissionamentoTO->setId_comissionamento($afiliadoProdutoTO->getId_comissionamento());
                        $this->dao->editarComissionamento($comissionamentoTO);

                        $this->dao->editarAfiliadoProduto($afiliadoProdutoTO);
                    } else {
                        $id_comissionamento = $this->dao->cadastrarComissionamento($comissionamentoTO);
                        $afiliadoProdutoTO->setId_comissionamento($id_comissionamento);

                        $id_afiliadoproduto = $this->dao->cadastrarAfiliadoProduto($afiliadoProdutoTO);
                        $afiliadoProdutoCompletoTO->setId_afiliadoproduto($id_afiliadoproduto);
                        $afiliadoProdutoCompletoTO->setId_comissionamento($id_comissionamento);

                        $arrDados[] = $afiliadoProdutoCompletoTO;
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($arrDados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao cadastrar Produtos de Afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Edita Afiliado
     * @param ContratoAfiliadoTO $to
     * @return Ead1_Mensageiro 
     */
    public function editarContratoAfiliado (ContratoAfiliadoTO $to)
    {
        try {
            if (!$this->dao->editarContratoAfiliado($to)) {
                throw new Zend_Exception($this->dao->excecao['SQL']);
            }
            return $this->mensageiro->setMensageiro('Afiliado editado com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao editar Afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Edita Vínculo de Afiliado com categoria, produto e comissão.
     * @param AfiliadoProdutoTO $to
     * @return Ead1_Mensageiro 
     */
    public function editarAfiliadoProduto (AfiliadoProdutoTO $to, $where = null)
    {
        try {
            if (!$this->dao->editarAfiliadoProduto($to, $where)) {
                throw new Zend_Exception($this->dao->excecao['SQL']);
            }
            return $this->mensageiro->setMensageiro('Vínculo de produto, categoria e comissão com afiliado editado com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao editar vínculo de produto, categoria e comissão com afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Deleta Afiliado
     * @param ContratoAfiliadoTO $to
     * @return Ead1_Mensageiro 
     */
    public function deletarContratoAfiliado (ContratoAfiliadoTO $to)
    {
        try {
            if (!$this->dao->deletarContratoAfiliado($to)) {
                throw new Zend_Exception($this->dao->excecao['SQL']);
            }
            return $this->mensageiro->setMensageiro('Afiliado excluído com sucesso!');
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao excluir Afiliado!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna Afiliados.
     * @param ContratoAfiliadoTO $to
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarContratoAfiliado (ContratoAfiliadoTO $to)
    {
        try {
            if (!$to->getId_entidadecadastro()) {
                $to->setId_entidadecadastro($to->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarContratoAfiliado($to)->toArray();
            if (empty($retorno)) {
                throw new Zend_Validate_Exception('Nenhum Registro de Afiliado Encontrado!');
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ContratoAfiliadoTO()));
        } catch (Zend_Validate_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Afiliado', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna Vínculo de Produto, Categoria e Comissão com Afiliado.
     * @param AfiliadoProdutoTO $to
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarAfiliadoProduto (AfiliadoProdutoTO $to)
    {
        try {
            $retorno = $this->dao->retornarAfiliadoProduto($to)->toArray();
            if (empty($retorno)) {
                throw new Zend_Validate_Exception('Nenhum Registro de Vínculo de Produto, Categoria e Comissão com Afiliado Encontrado!');
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new AfiliadoProdutoTO()));
        } catch (Zend_Validate_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Vínculo de Produto, Categoria e Comissão com Afiliado', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna Categorias com Produtos e Comissões de um Afiliado
     * @param int $id_contratoafiliado
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarAfiliadoCategoriaProdutoComissao ($id_contratoafiliado)
    {
        try {
            $to = new AfiliadoProdutoTO();
            $to->setId_contratoafiliado($id_contratoafiliado);
            $to->setBl_ativo(true);
            $retorno = $this->dao->retornarAfiliadoCategoriaProdutoComissionamento($to)->toArray();
            if (empty($retorno)) {
                throw new Zend_Validate_Exception('Nenhum Registro de Vínculo de Produto, Categoria e Comissão com Afiliado Encontrado!');
            }

            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new AfiliadoProdutoCompletoTO()));
        } catch (Zend_Validate_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Vínculo de Produto, Categoria e Comissão com Afiliado', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

}

?>
<?php
/**
 * Classe com regras de negócio para interações do Primeiro Acesso
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @since 30/08/2013
 *
 * @package models
 * @subpackage bo
 */
use \G2\Constante\Pais;

class PrimeiroAcessoBO extends Ead1_BO
{
    const BOAS_VINDAS = 'boasvindas';
    const DADOS_CONTATO = 'dadoscontato';
    const DADOS_CADASTRAIS = 'dadoscadastrais';
    const ACEITACAO_CONTRATO = 'aceitacaocontrato';
    const MENSAGEM_INFORMATIVA = 'mensageminformativa';

    const WITH_LAST_SEPARATOR = true;
    const WITHOUT_LAST_SEPARATOR = false;

    protected $mensageiro;
    protected $dao;
    private $textoSistemaBO;
    private $pessoaBO;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new PrimeiroAcessoDAO();
        $this->textoSistemaBO = new TextoSistemaBO();
        $this->pessoaBO = new PessoaBO();
    }

    /**
     * Metodo que decide se Cadastra ou Edita a etapa de Boas Vindas
     * @param PABoasVindasTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarEtapa(Ead1_TO_Dinamico $to, $etapa = null)
    {
        if (! $this->validaClassesPrimeiroAcesso($to)) {
            return $this->mensageiro->setMensageiro(
                'Classe passada como parâmetro não corresponde a nenhuma classe relativa ao Primeiro Acesso'
                , Ead1_IMensageiro::ERRO
            );
        }

        $getId = 'getId_pa_' . $etapa;

        $this->dao->beginTransaction();
        $method = $to->$getId()
                ? 'editarEtapa'
                : 'cadastrarEtapa';

        try {
            $this->mensageiro = $this->mensageiro->setMensageiro(
                $this->$method($to, $etapa)
            );
            $this->dao->commit();

        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro(
                'Erro ao salvar categoria - ' . $e->getMessage()
                , Ead1_IMensageiro::ERRO
            );
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que Edita uma Etapa
     *
     * @param Ead1_TO_Dinamico $to
     * @param type $etapa
     * @return type
     * @throws Zend_Exception
     */
    public function editarEtapa(Ead1_TO_Dinamico $to, $etapa)
    {
        $this->dao->beginTransaction();

        try {
            $getId = 'getId_pa_' . $etapa;

            if (! $this->dao->editarEtapa($to, $etapa)) {
                throw new Zend_Exception(
                    sprintf(
                        'Erro ao editar Etapa %s id(%s).'
                        , $etapa
                        , $to->$getId()
                    )
                );
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro(
                $to
                , Ead1_IMensageiro::SUCESSO
                , 'Etapa editada com sucesso!'
            );
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro(
                $e->getMessage()
                , Ead1_IMensageiro::ERRO
                , $e->getTraceAsString()
            );
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que Cadastra uma etapa
     *
     * @param Ead1_TO_Dinamico $to
     * @return Ead1_Mensageiro
     */
    public function cadastrarEtapa(Ead1_TO_Dinamico $to, $etapa = null)
    {
        $this->dao->beginTransaction();

        try {
            if (! $to->getId_entidade()) {
                $to->setId_entidade($to->getSessao()->id_entidade);
            }

            if (! $to->getId_usuariocadastro()) {
                $to->setId_usuariocadastro($to->getSessao()->id_usuario);
            }

            $to->setDt_cadastro(new Zend_Date(NULL));

            $setId = 'setId_pa_' . $etapa;
            $getId = 'getId_pa_' . $etapa;

            $id_etapa = $this->dao->cadastrarEtapa($to, $etapa);

            if (! $id_etapa) {
                throw new Zend_Exception(
                    sprintf('Erro ao cadastrar etapa %s id(%s).'
                        , $etapa
                        , $to->$getId()
                    )
                );
            } else {
                $to->$setId($id_etapa);
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro(
                $to
                , Ead1_IMensageiro::SUCESSO
                , sprintf('Etapa %s salva com sucesso!', $etapa)
            );

        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro(
                $e->getMessage()
                , Ead1_IMensageiro::ERRO
                , $e->getTraceAsString()
            );
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que avança as etapas do primeiro acesso
     *
     * @param type $etapa
     * @return Ead1_Mensageiro
     */
    public function prosseguirEtapa($etapa)
    {
        $etapa = str_replace('-', '', $etapa);
        $setEtapa = 'setBl_' . $etapa;

        $marcacaoEtapa = $this->retornarProgresso();
        $marcacaoEtapa->$setEtapa(1);

        $this->criarMinhaPasta($marcacaoEtapa->getId_venda());

        if (PrimeiroAcessoBO::ACEITACAO_CONTRATO == $etapa) {
            $marcacaoEtapa->setDt_aceita(new Zend_Date());

            $mensageiro = $this->gerarPDFContrato(
                $marcacaoEtapa->getId_venda()
            );

            if (Ead1_IMensageiro::SUCESSO != $mensageiro->getTipo()) {
                return $mensageiro;
            }
        } elseif (PrimeiroAcessoBO::MENSAGEM_INFORMATIVA == $etapa) {
            $this->gerarEmailMensagemInformativa();
        }

        return $this->salvarProgresso($marcacaoEtapa);
    }

    /**
     * Método que retorna o progresso do aluno da sessão
     * @param PAMarcacaoEtapaTO $to
     * @return \PAMarcacaoEtapaTO
     */
    public function retornarProgresso($id_venda = null)
    {
        $marcacaoEtapaTO = new PAMarcacaoEtapaTO();

        if ($id_venda) {
            $marcacaoEtapaTO->setId_venda($id_venda);
        } else {

            $contratoMatriculaTO = new ContratoMatriculaTO();
            if (!Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula) {
                throw new Zend_Exception(
                    "Não foram encontrados dados de sessão. Por favor faça novo login no portal."
                );
            }
            $contratoMatriculaTO->setId_matricula(
                Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula
            );
            $contratoMatriculaTO->fetch(false, true, true);

            //Se não houver contrato, ele pula o primeiro acesso,
            //por esse motivo o retorno do bl_mensageminformativa como true
            if (! $contratoMatriculaTO->getId_contrato()) {
                $marcacaoEtapaTO->setBl_mensageminformativa(true);
                return $marcacaoEtapaTO;
            }

            $contratoTO = new ContratoTO();
            $contratoTO->setId_contrato($contratoMatriculaTO->getId_contrato());
            $contratoTO->fetch(true, true, true);

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($contratoTO->getId_venda());
            $vendaTO->fetch(true, true, true);

            $marcacaoEtapaTO->setId_venda($vendaTO->getId_venda());
        }

        $marcacaoEtapaTO->fetch(false, true, true);

        return $marcacaoEtapaTO;
    }

    public function criarMinhaPasta($id_venda)
    {
        $fullPath = self::myFolderHome(
            $id_venda,
            self::WITHOUT_LAST_SEPARATOR
        );

        if (! is_dir($fullPath)) {
            mkdir($fullPath, 0777, true);
        }
    }

    public function gerarPDFContrato($idVenda = null)
    {
        try {
            $mpdf = new \Ead1_IMPdf();
            $mpdf->carregarHTML(
                $this->retornarTextoContrato($idVenda)->getSt_texto()
            );

            $fileName = sprintf('ContratoVenda%s.pdf', $idVenda);
            $folderPath = self::myFolderHome($idVenda, self::WITH_LAST_SEPARATOR);
            $mpdf->gerarPdf($fileName, $folderPath, 'F');

            $minhaPastaNegocio = new \G2\Negocio\MinhaPasta();
            $minhaPastaNegocio->salvarContratoMinhaPasta($fileName, $idVenda, null, null, $fileName);

            return new Ead1_Mensageiro(true, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            return $mensageiro = new Ead1_Mensageiro(
                null,
                Ead1_IMensageiro::ERRO,
                $e->getMessage()
            );
        }
    }

    /**
     * Retorna o texto com as variáveis substituidas do contrato do aluno
     * @return Ead1_Mensageiro
     */
    public function retornarTextoContrato($id_venda = null)
    {
        $textoSistemaTO = $this->retornarTextoSistemaContrato($id_venda);
        $contratoTO = $this->retornarContrato($id_venda);

        $param = array();
        $param['id_contrato'] = $contratoTO->getId_contrato();

        $mensageiro = $this->textoSistemaBO->gerarContrato($textoSistemaTO, $param);

        if (Ead1_IMensageiro::SUCESSO == $mensageiro->getTipo()) {
            return $mensageiro->getFirstMensagem();
        } else {
            $textoSistemaTO->setSt_texto("Erro ao retornar o texto do contrato");
        }

        return $textoSistemaTO;
    }

    public function retornarTextoSistemaContrato($id_venda = null)
    {
        $contratoTO = $this->retornarContrato($id_venda);
        $contratoRegraTO = new ContratoRegraTO();
        $contratoRegraTO->setId_contratoregra($contratoTO->getId_contratoregra());
        $contratoRegraTO->fetch(true, true, true);

        $textoSistemaTO = new TextoSistemaTO();
        $textoSistemaTO->setId_textosistema($contratoRegraTO->getId_contratomodelo());
        return $textoSistemaTO->fetch(true, true, true);
    }

    /**
     * Método que retorna o contrato do aluno
     * @return \ContratoTO
     */
    public function retornarContrato($id_venda = null)
    {
        $contratoTO = new ContratoTO();
        if ($id_venda != null) {
            $contratoTO->setId_venda($id_venda);
            $contratoTO->fetch(false, true, true);
        } else {
            $contratoMatriculaTO = new ContratoMatriculaTO();
            $contratoMatriculaTO->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);
            $contratoMatriculaTO->fetch(false, true, true);

            $contratoTO->setId_contrato($contratoMatriculaTO->getId_contrato());
            $contratoTO->fetch(true, true, true);
        }

        return $contratoTO;
    }

    public function gerarEmailMensagemInformativa()
    {
        $mensagemBO = new MensagemBO();

        $paMensagemInformativa = new PAMensagemInformativaTO();
        $paMensagemInformativa->setId_entidade($paMensagemInformativa->getSessao()->id_entidade);
        $paMensagemInformativa->fetch(false, true, true);

        $textoSistemaTO = new TextoSistemaTO();
        $textoSistemaTO->setId_textosistema($paMensagemInformativa->getId_textoemail());

        $progresso = $this->retornarProgresso();

        $params = array('id_venda' => $progresso->getId_venda());

        $emailConfig = new EmailConfigTO();
        $emailConfig->setId_entidade($emailConfig->getSessao()->id_entidade);

        return $mensagemBO->enviarNotificacaoTextoSistema(
            $textoSistemaTO
            , $params, $emailConfig->getId_emailconfig()
            , $emailConfig->getSessao()->id_usuario
            , $emailConfig->getSessao()->id_entidade
        );
    }

    public function salvarProgresso(PAMarcacaoEtapaTO $to)
    {
        if ($this->retornarProgresso()->getId_pa_marcacaoetapa()) {
            return $this->editarMarcacaoEtapa($to);
        }

        return $this->cadastrarMarcacaoEtapa($to);
    }

    public function editarMarcacaoEtapa(PAMarcacaoEtapaTO $to)
    {
        $this->dao->beginTransaction();

        try {
            if (! $this->dao->editarMarcacaoEtapa($to)) {
                throw new Zend_Exception(
                    "Erro ao editar progresso do Primeiro Acesso!"
                );
            }

            $this->dao->commit();

            $this->mensageiro->setMensageiro(
                $to
                , Ead1_IMensageiro::SUCESSO
                , 'Etapa editada com sucesso!'
            );
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();

            $this->mensageiro->setMensageiro(
                $e->getMessage()
                , Ead1_IMensageiro::ERRO
                , $e->getTraceAsString()
            );
        }

        return $this->mensageiro;
    }

    public function cadastrarMarcacaoEtapa(PAMarcacaoEtapaTO $to)
    {
        $this->dao->beginTransaction();

        try {
            if (! $to->getId_venda()) {
                $to->setId_venda($this->retornarProgresso()->getId_venda());
            }

            $to->setDt_checkin(new Zend_Date());

            $id = $this->dao->cadastrarMarcacaoEtapa($to);

            if (! $id) {
                throw new Zend_Exception(
                    "Erro ao cadastrar progresso do aluno no Primeiro Acesso!"
                );
            }

            $to->setId_pa_marcacaoetapa($id);

            $this->dao->commit();

            $this->mensageiro->setMensageiro(
                $to
                , Ead1_IMensageiro::SUCESSO
                , 'Progresso do Primeiro Acesso criado com sucesso!'
            );

        } catch (Zend_Exception $e) {
            $this->dao->rollBack();

            $this->mensageiro->setMensageiro(
                $e->getMessage()
                , Ead1_IMensageiro::ERRO
                , $e->getTraceAsString()
            );
        }
        return $this->mensageiro;
    }

    public function salvarAlteracoesDadosContato(VwPessoaTO $vw, $alteracoes = null)
    {
        $mensageiro = new Ead1_Mensageiro('', Ead1_IMensageiro::SUCESSO);
        $pessoaBO = new PessoaBO();

        if (isset($alteracoes['alterar']['endereco_correspondencia'])) {

            $vwEndereco = new VwPessoaEnderecoTO();
            $vwEndereco->setId_usuario($vw->getSessao()->id_usuario);
            $vwEndereco->setId_entidade($vw->getSessao()->id_entidade);
            $vwEndereco->setId_tipoendereco(TipoEnderecoTO::CORRESPONDENCIA);
            $vwEndereco->setBl_padrao(1);
            $vwEndereco->setBl_ativo(1);
            $vwEndereco->fetch(false, true, true);

            $enderecoCorresnpondeciaTO = new EnderecoTO();
            $enderecoCorresnpondeciaTO->setSt_endereco($vw->st_enderecocorrespondencia);
            $enderecoCorresnpondeciaTO->setNu_numero($vw->nu_numerocorrespondencia);
            $enderecoCorresnpondeciaTO->setSt_complemento($vw->st_complementocorrespondencia);
            $enderecoCorresnpondeciaTO->setSt_bairro($vw->st_bairrocorrespondencia);
            $enderecoCorresnpondeciaTO->setSg_uf($vw->sg_ufcorrespondencia);
            $enderecoCorresnpondeciaTO->setId_municipio($vw->id_municipiocorrespondencia);
            $enderecoCorresnpondeciaTO->setSt_cep($vw->st_cepcorrespondencia);
            $enderecoCorresnpondeciaTO->setId_pais($vw->getId_pais() ? $vw->getId_pais() : 22);
            $enderecoCorresnpondeciaTO->setId_tipoendereco(TipoEnderecoTO::CORRESPONDENCIA);

            $municipioCorrespTO = new MunicipioTO();
            $municipioCorrespTO->setId_municipio($vw->id_municipiocorrespondencia);
            $municipioCorrespTO->fetch(false, true, true);
            $enderecoCorresnpondeciaTO->setSt_cidade($municipioCorrespTO->getSt_nomemunicipio());

            if ($vwEndereco->getId_endereco()) {
                $enderecoCorresnpondeciaTO->setId_endereco($vwEndereco->getId_endereco());

                if (! $this->dao->salvarEndereco($enderecoCorresnpondeciaTO)) {
                    $mensageiro->setMensageiro('Erro ao salvar o endereço.\n', Ead1_IMensageiro::ERRO);
                }
            } else {
                //Dados Endereço Correspondência
                $pessoaEnderecoCorrenspondenciaTO = new PessoaEnderecoTO();
                $pessoaEnderecoCorrenspondenciaTO->setBl_padrao(true);
                $pessoaEnderecoCorrenspondenciaTO->setId_entidade($vw->getSessao()->id_entidade);
                $pessoaEnderecoCorrenspondenciaTO->setId_usuario($vw->getSessao()->id_usuario);

                $usuarioTO = new UsuarioTO();
                $usuarioTO->setId_usuario($vw->getSessao()->id_usuario);
                $usuarioTO->fetch(false, true, true);

                $ro = new PessoaRO();
                $mensag = $ro->salvarEndereco(
                    $usuarioTO
                    , $enderecoCorresnpondeciaTO
                    , $pessoaEnderecoCorrenspondenciaTO
                );

                if ($mensag->getType() == Ead1_IMensageiro::ERRO) {
                    $mensageiro->setMensageiro(
                        'Erro ao salvar o endereço de correspondência.\n'
                        , Ead1_IMensageiro::ERRO
                    );
                }
            }

            $vwEndereco = new VwPessoaEnderecoTO();
            $vwEndereco->setId_usuario($vw->getSessao()->id_usuario);
            $vwEndereco->setId_entidade($vw->getSessao()->id_entidade);
            $vwEndereco->setId_tipoendereco(TipoEnderecoTO::CORRESPONDENCIA);
            $vwEndereco->setBl_padrao(1);
            $vwEndereco->setBl_ativo(1);
            $vwEndereco->fetch(false, true, true);
        }

        if (isset($alteracoes['alterar']['endereco'])) {
            $endereco = new EnderecoTO();
            $endereco->setId_endereco($vw->getId_endereco());
            $endereco->setSt_endereco($vw->getSt_endereco());
            $endereco->setNu_numero($vw->getNu_numero());
            $endereco->setSt_complemento($vw->getSt_complemento());
            $endereco->setSt_bairro($vw->getSt_bairro());
            $endereco->setSg_uf($vw->getSg_uf());
            $endereco->setId_municipio($vw->getId_municipio());
            $endereco->setSt_cep($vw->getSt_cep());
            $endereco->setId_pais(
                $vw->getId_pais()
                ? $vw->getId_pais()
                : Pais::BRASIL
            );

            $endereco->setId_tipoendereco(TipoEnderecoTO::RESIDENCIAL);

            if (! $this->dao->salvarEndereco($endereco)) {
                $mensageiro->setMensageiro(
                    'Erro ao salvar o endereço.\n'
                    , Ead1_IMensageiro::ERRO
                );
            }
        }

        if (isset($alteracoes['alterar']['email'])) {
            $email = new \G2\Entity\ContatosEmail();
            $email->setId_email($vw->getId_email());
            $email->setSt_email($vw->getSt_email());

            $contatosEmailNegocio = new \G2\Negocio\ContatosEmail();

            if (! $contatosEmailNegocio->merge($email)) {
                $mensageiro->setMensageiro($mensageiro->getMensagem()
                . 'Erro ao salvar o email.\n', Ead1_IMensageiro::ERRO);
            }
        }

        $usuarioTO = new UsuarioTO();
        $usuarioTO->setId_usuario($usuarioTO->getSessao()->id_usuario);
        $usuarioTO->fetch(true, true, true);

        if (isset($alteracoes['alterar']['telefone'])) {

            $telefone = new ContatosTelefoneTO();
            $telefone->setId_telefone($vw->getId_telefone());
            $telefone->fetch(true, true, true);
            $telefone->setNu_ddd(str_replace('(', '', str_replace(')', '', $vw->getNu_ddd())));
            $telefone->setNu_telefone(str_replace('-', '', $vw->getNu_telefone()));

            $contatosTelefonePessoa = new ContatosTelefonePessoaTO();
            $contatosTelefonePessoa->setBl_padrao(1);
            $contatosTelefonePessoa->setId_entidade($vw->getSessao()->id_entidade);
            $contatosTelefonePessoa->setId_telefone($telefone->getId_telefone());
            $contatosTelefonePessoa->setId_usuario($vw->getSessao()->id_usuario);

            $isContatoTelefonePessoaSave = $this->pessoaBO
                                                ->_procedimentoSalvarContatosTelefonePessoa(
                                                    $usuarioTO
                                                    , $telefone
                                                    , $contatosTelefonePessoa
                                                );
            if (! $isContatoTelefonePessoaSave) {
                $mensageiro->setMensageiro($mensageiro->getMensagem() .
                    'Erro ao salvar o telefone.\n', Ead1_IMensageiro::ERRO);
            }
        }

        if (isset($alteracoes['alterar']['telefone_alt'])) {
            $telefoneAlt = new ContatosTelefoneTO();
            $telefoneAlt->setId_telefone($vw->getId_telefonealternativo());
            $telefoneAlt->fetch(true, true, true);
            $telefoneAlt->setNu_ddd(str_replace('(', '', str_replace(')', '', $vw->getNu_dddalternativo())));
            $telefoneAlt->setNu_telefone(str_replace('-', '', $vw->getNu_telefonealternativo()));

            $contatosTelefonePessoaAlt = new ContatosTelefonePessoaTO();
            $contatosTelefonePessoaAlt->setBl_padrao(0);
            $contatosTelefonePessoaAlt->setId_entidade($vw->getSessao()->id_entidade);
            $contatosTelefonePessoaAlt->setId_telefone($telefoneAlt->getId_telefone());
            $contatosTelefonePessoaAlt->setId_usuario($vw->getSessao()->id_usuario);

            $isContatoTelefonePessoaSave = $this->pessoaBO
                                                ->_procedimentoSalvarContatosTelefonePessoa(
                                                    $usuarioTO
                                                    , $telefoneAlt
                                                    , $contatosTelefonePessoaAlt
                                                );
            if (! $isContatoTelefonePessoaSave) {
                $mensageiro->setMensageiro($mensageiro->getMensagem()
                . 'Erro ao salvar o telefone alternativo.\n', Ead1_IMensageiro::ERRO
                );
            }
        }

        if (isset($alteracoes['Cidade']) && $alteracoes['Cidade']) {
            $municipioTO = new MunicipioTO();
            $municipioTO->setId_municipio($alteracoes['Cidade']);
            $municipioTO->fetch(true, true, true);
            $alteracoes['Cidade'] = $municipioTO->getSt_nomemunicipio();
        }

        if ($alteracoes) {
            $mensageiro = $this->gerarOcorrencia(
                'Primeiro Acesso - Alterações de dados de contato'
                , $alteracoes
            );

            $cAtencaoBO = new CAtencaoBO();

            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $tramite = new TramiteTO();
                $tramite->setBl_visivel(true);
                $tramite->setId_entidade($tramite->getSessao()->id_entidade);
                $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                $tramite->setId_usuario($tramite->getSessao()->id_usuario);
                $tramite->setSt_tramite(
                    'Prezado aluno, aguardamos o comprovante do seu novo
                    endereço (via e-mail ou correspondência) para finalização
                    dessa ocorrência'
                );

                $mensageiro = $cAtencaoBO->novaInteracao(
                    $mensageiro->getFirstMensagem()
                    , $tramite
                );
            } else {
                $mensageiro->setMensageiro("Os dados foram atualizados.");
            }
        }

        return $mensageiro;
    }

    /**
     * Metodo que retorna etapas
     *
     * @param Ead1_TO_Dinamico $to
     * @return Ead1_Mensageiro
     */
    public function retornarEtapa(Ead1_TO_Dinamico $to)
    {
        if (! $this->validaClassesPrimeiroAcesso($to)) {
            return $this->mensageiro->setMensageiro(
                'Classe passada como parâmetro não corresponde a nenhuma classe relativa ao Primeiro Acesso'
                , Ead1_IMensageiro::ERRO
            );
        }

        return new Ead1_Mensageiro($to->fetch(false, true, true));
    }

    public function salvarAlteracoesDadosCadastrais($alteracoes = null)
    {
        if ($alteracoes) {
            return $this->gerarOcorrencia(
                'Primeiro Acesso - Alterações de dados cadastrais'
                , $alteracoes
            );
        }

        return new Ead1_Mensageiro(
            $alteracoes
            , Ead1_IMensageiro::SUCESSO
            , 'Informações de cadastro salvas com sucesso.'
        );
    }

    /**
     * Retorna se é para exibir o tutorial
     * @return bool
     * @throws Exception
     */
    public function passarPeloPrimeiroAcesso()
    {
        $esquemaNegocio = new \G2\Negocio\EsquemaConfiguracao();
        $configuracaoTutorial = $esquemaNegocio->retornarConfiguracaoPrimeiroAcesso();

        return $configuracaoTutorial;
    }

    public function retornarOcorrenciasAbertasDadosCadastrais()
    {
        $etapa = new PADadosCadastraisTO();
        $etapa->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
        $etapa = $this->retornarEtapa($etapa)->getFirstMensagem();

        $ocorrencia = new OcorrenciaTO();
        $ocorrencia->setId_assuntoco($etapa ? $etapa->getId_assuntoco() : null);
        $ocorrencia->setId_categoriaocorrencia($etapa ? $etapa->getId_categoriaocorrencia() : null);
        $ocorrencia->setId_matricula(Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula);

        return $this->dao->retornarOcorrenciasAbertasDadosCadastrais($ocorrencia);
    }

    public function gerarContratosInexistentes($proximo_id_marcacao = 0)
    {
        $progressos = $this->dao->retornarPAMarcacaoSemContrato($proximo_id_marcacao);
        $resultado = array();

        foreach ($progressos as $progresso) {
            $venda = new VendaTO();
            $venda->setId_venda($progresso->getId_venda());
            $venda->fetch(true, true, true);

            if (! $venda->getId_venda() || !$progresso->getId_venda()) {
                die('Id venda não encontrato para o progresso ' . $progresso->getId_pa_marcacaoetapa());
            }

            $idUsuario = $venda->getId_usuario();
            $idVenda = $progresso->getId_venda();
            $fullpath = self::myFolderHome($idVenda, self::WITH_LAST_SEPARATOR)
                      . sprintf('ContratoVenda%s.pdf', $idVenda);

            if (file_exists($fullpath)) {
                $log = sprintf(
                    "Contrato da venda %s do aluno %s já existe em PDF.\r\n"
                    , $idVenda
                    . $idUsuario
                );

                $this->gravarLog($log);

                echo $log;
            } else {
                $this->gerarPDFContrato($idVenda);

                $log = sprintf(
                    "Contrato da venda %s do aluno %s gerado com sucesso.\r\n"
                    , $idVenda
                    , $idUsuario
                );

                $this->gravarLog($log);

                echo $log;

                $resultado[] = $progresso->getId_venda();
            }

            $this->gravarId($progresso->getId_pa_marcacaoetapa());
        }

        return $resultado;
    }

    public function gravarLog($log)
    {
        $nome_arquivo = $_SERVER['DOCUMENT_ROOT'] . '/upload/minhapasta/log.txt';
        $fp = fopen($nome_arquivo, "a+");
        fwrite($fp, $log);
        fclose($fp);
    }

    public function gravarId($id)
    {
        $nome_arquivo = $_SERVER['DOCUMENT_ROOT'] . '/upload/minhapasta/id.txt';
        unlink($nome_arquivo);
        $fp = fopen($nome_arquivo, "a+");
        fwrite($fp, $id);
        fclose($fp);
    }

    public function lerId()
    {
        $nome_arquivo = $_SERVER['DOCUMENT_ROOT'] . '/upload/minhapasta/id.txt';
        $fp = fopen($nome_arquivo, "a+");
        $id = fread($fp, 1024);
        fclose($fp);
        return $id;
    }

    private function gerarOcorrencia($titulo, $dadosAlterados)
    {
        if ($dadosAlterados['etapa'] == 'dados-contato') {
            $etapa = new PADadosContatoTO();
        } elseif ($dadosAlterados['etapa'] == 'dados-cadastrais') {
            $etapa = new PADadosCadastraisTO();
        } else {
            throw new Zend_Exception('Etapa não informada');
        }

        $etapa->setId_entidade($etapa->getSessao()->id_entidade);
        $etapa = $this->retornarEtapa($etapa)->getFirstMensagem();

        if (! $etapa) {
            throw new Zend_Exception('Etapa não encontrada');
        }

        $contratoMatricula = new ContratoMatriculaTO();
        $contratoMatricula->setId_contrato($this->retornarContrato()->getId_contrato());
        $contratoMatricula->fetch(false, true, true);

        $st_ocorrencia = 'Alterações de dados no Primeiro Acesso<br/>';
        unset($dadosAlterados['alterar']);
        unset($dadosAlterados['etapa']);
        foreach ($dadosAlterados as $key => $value) {
            $st_ocorrencia .= $key . ': ' . $value . '<br/>';
        }

        $ocorrencia = new OcorrenciaTO();
        $ocorrencia->setDt_cadastro(new DateTime());
        $ocorrencia->setId_assuntoco($etapa->getId_assuntoco());
        $ocorrencia->setId_categoriaocorrencia($etapa->getId_categoriaocorrencia());
        $ocorrencia->setId_entidade($etapa->getId_entidade());
        $ocorrencia->setId_evolucao(\G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO);
        $ocorrencia->setId_matricula($contratoMatricula->getId_matricula());
        $ocorrencia->setId_situacao(\G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE);
        $ocorrencia->setId_usuariocadastro($contratoMatricula->getSessao()->id_usuario);
        $ocorrencia->setId_usuariointeressado($contratoMatricula->getSessao()->id_usuario);
        $ocorrencia->setSt_ocorrencia($st_ocorrencia);
        $ocorrencia->setSt_titulo($titulo);

        $cAtencaoBO = new CAtencaoBO();

        // antes de gerar a ocorrencia, verifica se ela ja nao foi gerada antes para nao duplicar
        $ng_catencao = new \G2\Negocio\CAtencao();

        $et_ocorrencia = new \G2\Entity\Ocorrencia();
        $et_ocorrencia->setId_usuariointeressado($ng_catencao->find(
            '\G2\Entity\Usuario'
            , $ocorrencia->getId_usuariointeressado())
        );
        $et_ocorrencia->setId_assuntoco($ng_catencao->find('\G2\Entity\AssuntoCo', $ocorrencia->getId_assuntoco()));
        $et_ocorrencia->setId_matricula($ng_catencao->find('\G2\Entity\Matricula', $ocorrencia->getId_matricula()));

        if ($ng_catencao->possuiOcorrenciaCadastradaRecentemente($et_ocorrencia)) {
            $oc_temp = new OcorrenciaTO();
            $oc_temp->setId_usuariointeressado($ocorrencia->getId_usuariointeressado());
            $oc_temp->setId_assuntoco($ocorrencia->getId_assuntoco());
            $oc_temp->setId_matricula($ocorrencia->getId_matricula());
            $oc_temp = $oc_temp->fetch(false, true, true);

            return new \Ead1_Mensageiro($oc_temp, \Ead1_IMensageiro::ERRO);
        }

        return $cAtencaoBO->salvarOcorrencia($ocorrencia);
    }

    private function validaClassesPrimeiroAcesso(Ead1_TO_Dinamico $to)
    {
        return (
            $to instanceof PABoasVindasTO ||
            $to instanceof PADadosContatoTO ||
            $to instanceof PADadosCadastraisTO ||
            $to instanceof PAMensagemInformativaTO
        );
    }

    /**
     * monta o caminha da minha pasta da venda informada.
     * lança exception se um id de venda não for informado
     *
     * @param  integer $homeDir
     * @return string
     * @throws Exception
     */
    private static function myFolderHome ($homeDir, $lastSeparator = false)
    {
        if (! $homeDir) {
            throw new \Exception(
                'O número da venda é requerido para criar "MinhaPasta"'
            );
        }

        return self::myFolderRootPath(self::WITH_LAST_SEPARATOR)
               . $homeDir
               . ($lastSeparator ? DIRECTORY_SEPARATOR : null)
               ;
    }

    private static function myFolderRootPath($lastSeparator = false)
    {
        return $_SERVER['DOCUMENT_ROOT']
               . '/upload/minhapasta'
               . ($lastSeparator ? DIRECTORY_SEPARATOR : null)
              ;
    }
}

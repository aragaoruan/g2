<?php
/**
 * Classe de testes de regras
 * 
 * @author edermariano
 *
 * @package models
 * @subpackage bo
 */
class TesteBO extends Ead1_BO{
	
	public function testar($pars){
		try{
			if(!empty($pars['to'])){
				
		    	$arrayTO = explode(',',$pars['to']);
		    	foreach($arrayTO as $tos){
		    		$tos = trim($tos);
			    	$to = new $tos();
			    	$pars['setto'] = implode(',',$pars['setsTO'.$tos]);
			    	$arraySetTO = null;
			    	$arrayParsTO = null;
			    	$arraySetParsTO = null;
			    	if(trim($pars['setto']) != '' || trim($pars['parametrosto'.$tos]) != ''){
				    	$arraySetTO = $pars['setsTO'.$tos];//explode(',',$pars['setto']);
				    	$arrayParsTO = explode(',',$pars['parametrosto'.$tos]);
				    	if(count($arraySetTO) != count($arrayParsTO)){
				    		throw new Zend_Exception('Quantidade de SETS diferentes da quantidade de parametros!');
				    	}
				    	foreach($arraySetTO as $index => $value){
				    		$arraySetParsTO[trim($value)] = trim($arrayParsTO[$index]);
				    	}
				    	foreach($arraySetParsTO as $setsTO => $valTO){
					    	if(method_exists($to, $setsTO)){   
					    		$to->$setsTO($valTO);
							}
				    	}
			    	}
			    	$arrTOSintanciados[] = $to;
		    	}
			}
	    	$sessao = new Zend_Session_Namespace('geral');
			$sessao->id_usuario = $pars['id_usuario'];
			$sessao->id_entidade = $pars['id_entidade'];
			$sessao->id_perfil = $pars['id_perfil'];
			
			if($pars['ro'] !== 'ro'){
	    		$roN = $pars['ro'];
			}else{
				$roN = 'Ead1_'.strtoupper($pars['ro']);
			}
	    	$ro = new $roN();
	    	$metodoRO = $pars['metodoRO'];
			if(count($arrTOSintanciados) == 1){
				Zend_Debug::dump($ro->$metodoRO($to));
			}elseif(count($arrTOSintanciados) == 0){
				Zend_Debug::dump($ro->$metodoRO());
			}else{
				Zend_Debug::dump(call_user_func_array(array($ro, $metodoRO), $arrTOSintanciados));
			}
	    	 
		}catch(Exception $e){
			echo 'Mensagem de Erro: '.$e->getMessage().' <br />';
			echo 'Linha: '.$e->getLine().' <br />';
			echo 'Arquivo: '.$e->getFile().' <br/>';
			echo 'Rota do Erro: '.$e->getTraceAsString();
		}
	}
	
	public function mostrarSets($pars){
		$tos = explode(',',$pars['to']);
		foreach($tos as $index => $to){
			$to = trim($to);
			if(isset($arSets)){
				unset($arSets);
			}
			$atributos = get_object_vars(new $to());
			foreach($atributos as $atributo => $valor){
				$metodoSet = 'set' . ucfirst($atributo);
				$arSets[] = $metodoSet;
			}
			$arrTO[$index] = $arSets;
		}
		return $arrTO;
	}
	
	public function mostrarRo(){
		$dir = '../application/ro/';
		if(is_dir($dir)){
		    if($dh = opendir($dir)){
		        while(($file = readdir($dh)) !== false){
		        	if($file !== '.' && $file !== '..' && $file !== '.svn'){
		            	$nameFile[] = substr($file,0,-4);
		        	}
		        }
		        return $nameFile;
		        closedir($dh);
		    }
		}
	}
	
	public function mostrarMetodosRo($ro){
		$ro = $ro['ro'];
		$roEad1 = new Ead1_RO();
		if($ro !== 'ro'){
			$arMetodos = get_class_methods(get_class(new $ro()));
		}
		$arMetodosEad1 = get_class_methods(get_class(new $roEad1()));
		foreach($arMetodos as $index => $value){
			if($value !== '__construct'){
				$metodosRO[$index] = trim($value);
			}
		}
		foreach($arMetodosEad1 as $indexEad => $valueEad){
			if($valueEad !== '__construct'){
				$metodosEad1[$indexEad] = trim($valueEad);
			}
		}
		$metodosGeral = array_diff($metodosRO,$metodosEad1);
		if($ro !== 'ro'){
			return array_values($metodosGeral);
		}else{
			return array_values($metodosEad1);
		}
	}
	
	public function procurarTosMetodos($pars){
		$arquivo = $pars['ro'].'.php';
		$metodo = $pars['metodoRO'];
		
		$arquivo = file('../application/ro/'.$arquivo);
		$arrayTOs = $this->lerLinhaArquivosMetodos($arquivo, $metodo);
		if(!empty($arrayTOs)){
			return $arrayTOs;
		}else{
			$arquivo  = file('../library/Ead1/RO.php');
			$arrayTOs = $this->lerLinhaArquivosMetodos($arquivo, $metodo);
			return $arrayTOs;
		}
		
	}
	/**
	 * Método que le as linhas dos arquivos procurando metodos e retorna um array de metodos
	 * @param String $arquivo
	 * @param String $metodo
	 * @return Array
	 */
	public function lerLinhaArquivosMetodos($arquivo, $metodo){
		foreach ($arquivo as $linha_num => $linha) {
			$linha_sp = substr(trim($linha),0,15);
			if($linha_sp === "public function" && trim($linha)!== 'public function __construct(){'){
				$nome = explode("(", $linha);
				$nomeMetodo = trim($nome[0]);
				$nomeTO = explode(")",$nome[1]);
				if($nomeMetodo == 'public function '.$metodo){
					$nome = explode("(", $linha);
					$nomeMetodo = trim($nome[0]);
					$soTO = explode(")",$nome[1]);
					$nomeTOsemVar = explode(", ",$soTO[0]);
					foreach($nomeTOsemVar as $tos){
						$tos = explode(" $",$tos);
						$arrayTOs[] = $tos[0];
					}
				}
			}
		}
    	return $arrayTOs;
	}
}
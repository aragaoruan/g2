<?php
/**
 * Classe que encapsula regra de negócio de Compartilha Dados
 * @author eduardoromao
 */
class CompartilhaDadosBO extends Ead1_BO{
	
	private $_dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->_dao = new CompartilhaDadosDAO();
	}
	
	/**
	 * Método que salva um array de dados compartilhados
	 * @param array $arrCompartilhaDadosTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrCompartilhaDados(array $arrCompartilhaDadosTO){
		try{
			$this->_dao->beginTransaction();
			$deletado = false;
			foreach ($arrCompartilhaDadosTO as $compartilhaDadosTO){
				if(!$deletado){
					$deletado = true;
					$clone = clone $compartilhaDadosTO;
					$clone->setId_entidadedestino(null);
					$this->deletarCompartilhaDados($clone)->subtractMensageiro();
				}
				$this->cadastrarCompartilhaDados($compartilhaDadosTO)->subtractMensageiro();
			}
			$this->_dao->commit();
			$this->mensageiro->setMensageiro("Compartilhamento Salvo Com Sucesso.",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->_dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->_dao->rollBack();
			$this->mensageiro->setMensageiro("Erro ao Cadastrar Dados Compartilhados.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método cadastra dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCompartilhaDados(CompartilhaDadosTO $to){
		try{
			$to->setId_entidadeorigem($to->getId_entidadeorigem() ? $to->getId_entidadeorigem() : $to->getSessao()->id_entidade);
			$orm = new CompartilhadadosORM();
			if(!$orm->consulta($to,false,true)){
				$this->_dao->cadastrarCompartilhaDados($to);
			}
			$this->mensageiro->setMensageiro($to,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Cadastrar Dados Compartilhados.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método edita dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarCompartilhaDados(CompartilhaDadosTO $to){
		try{
			$to->setId_entidadeorigem($to->getId_entidadeorigem() ? $to->getId_entidadeorigem() : $to->getSessao()->id_entidade);
			$this->_dao->editarCompartilhaDados($to);
			$this->mensageiro->setMensageiro("Dados Compartilhados Editados com Sucesso.",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Cadastrar Dados Compartilhados.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método excluir dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return Ead1_Mensageiro
	 */
	public function deletarCompartilhaDados(CompartilhaDadosTO $to){
		try{
			$to->setId_entidadeorigem($to->getId_entidadeorigem() ? $to->getId_entidadeorigem() : $to->getSessao()->id_entidade);
			$this->_dao->deletarCompartilhaDados($to);
			$this->mensageiro->setMensageiro("Dado Compartilhado Excluido com Sucesso.",Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Cadastrar Dados Compartilhados.",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que retorna os dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarCompartilhaDados(CompartilhaDadosTO $to){
		try{
			if(!$to->getId_entidadedestino() && !$to->getId_entidadeorigem()){
				$to->setId_entidadeorigem($to->getSessao()->id_entidade);
			}
			$dados = $this->_dao->retornarCompartilhaDados($to)->toArray();
			if(empty($dados)){
				THROW new Zend_Validate_Exception("Nenhum Dado Compartilhado Encontrado");
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new CompartilhaDadosTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$this->mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao Retornar Dados Compartilhados",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
}
<?php
/**
 * Classe que define exporta áreas do Gestor2 para outros sistemas
 * 	Sistema de Avaliação...
 * @author edermariano
 *
 */
class WsAreaBO extends WsBO{
	
	/**
	 * Método que cadastra as áreas do gestor2 em outros sistemas
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArea(AreaConhecimentoTO $areaConhecimentoTO, $idSistema = SistemaTO::SISTEMA_AVALIACAO){
		$wsDAO = new WebServiceDAO();
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO($idSistema);
			$orm = new AreaIntegracaoORM();
			if(!$interfaceIntegracao){
				throw new Exception("Não há interface de integração para o sistema (" . SistemaTO::getSistema($idSistema) . "), ou você não está logado no sistema.");
			}
			$areaIntegracaoTOPai = null;
			if($areaConhecimentoTO->getId_areaconhecimentopai()){
				$areaIntegracaoTOPai = $orm->consulta(new AreaIntegracaoTO(array(
											'id_areaconhecimento' => $areaConhecimentoTO->getId_areaconhecimentopai(),
											'id_sistema' => $idSistema)),false,true);
				if(!$areaIntegracaoTOPai){
					throw new Exception("Área pai não integrada!");
				}
			}
			$areaIntegracaoTO = null;
			if($areaConhecimentoTO->getId_areaconhecimento()){
				$areaIntegracaoTO = new AreaIntegracaoTO();
				$areaIntegracaoTO->setId_areaconhecimento($areaConhecimentoTO->getId_areaconhecimento());
				$areaIntegracaoTO = $orm->consulta(new AreaIntegracaoTO(array(
											'id_areaconhecimento' => $areaConhecimentoTO->getId_areaconhecimento(),
											'id_sistema' => $idSistema)),false,true);
				if($areaIntegracaoTO){
					THROW new Exception("Esta area já foi Integrada!");
				}
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->salvarTipoArea()
								->entidade($interfaceIntegracao->getSt_codchave())
								->descricao($areaConhecimentoTO->getSt_areaconhecimento())
								->tipoAreaPai(($areaIntegracaoTOPai ? $areaIntegracaoTOPai->getSt_codsistema() : null))
								->ativo($areaConhecimentoTO->getBl_ativo())
								->id(null)
								->post()->salvarTipoArea);
								
			if($mensageiroWS->getTipo() == Ead1_IMensageiro::SUCESSO){
				$wsDAO->beginTransaction();
				$this->cadastrarAreaIntegracao($areaConhecimentoTO, $mensageiroWS->getCurrent(), $idSistema);
				$wsDAO->commit();
			}else{
				throw new Zend_Exception(implode(', ', $mensageiroWS->getMensagem()));
			}
			return new Ead1_Mensageiro('Cadastrado com Sucesso!');
		}catch(Zend_Exception $e){
			$wsDAO->rollBack();
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}catch(Exception $e){
			$wsDAO->rollBack();
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
	
	
	public function editarArea(AreaConhecimentoTO $areaConhecimentoTO, $idSistema = SistemaTO::SISTEMA_AVALIACAO){
		try{
			$interfaceIntegracao = $this->getEntidadeIntegracaoTO($idSistema);
			$orm = new AreaIntegracaoORM();
			if(!$interfaceIntegracao){
				throw new Exception("Não há interface de integração para o sistema (" . SistemaTO::getSistema($idSistema) . "), ou você não está logado no sistema.");
			}
			$areaIntegracaoTOPai = null;
			if($areaConhecimentoTO->getId_areaconhecimentopai()){
				$areaIntegracaoTOPai = $orm->consulta(new AreaIntegracaoTO(array(
											'id_areaconhecimento' => $areaConhecimentoTO->getId_areaconhecimentopai(),
											'id_sistema' => $idSistema)),false,true);
				if(!$areaIntegracaoTOPai){
					throw new Exception("Área pai não integrada!");
				}
			}
			$areaIntegracaoTO = null;
			if($areaConhecimentoTO->getId_areaconhecimento()){
				$areaIntegracaoTO = new AreaIntegracaoTO();
				$areaIntegracaoTO->setId_areaconhecimento($areaConhecimentoTO->getId_areaconhecimento());
				$areaIntegracaoTO = $orm->consulta(new AreaIntegracaoTO(array(
											'id_areaconhecimento' => $areaConhecimentoTO->getId_areaconhecimento(),
											'id_sistema' => $idSistema)),false,true);
				if(!$areaIntegracaoTO){
					THROW new Exception("Area Não Integrada por isso não pode ser Editada!");
				}
			}
			$mensageiroWS = new Ead1_Mensageiro_Service($this->clientRest->salvarTipoArea()
								->entidade($interfaceIntegracao->getSt_codchave())
								->descricao($areaConhecimentoTO->getSt_areaconhecimento())
								->tipoAreaPai($areaIntegracaoTOPai ? $areaIntegracaoTOPai->getSt_codsistema() : null)
								->ativo($areaConhecimentoTO->getBl_ativo())
								->id($areaIntegracaoTO->getSt_codsistema())
								->post()->salvarTipoArea);
			if($mensageiroWS->getTipo() != Ead1_IMensageiro::SUCESSO){
				return new Ead1_Mensageiro($mensageiroWS->getMensagem(),$mensageiroWS->getTipo(),$mensageiroWS->getCodigo());
			}else{
				return new Ead1_Mensageiro('Atualizado com Sucesso!');
			}
		}catch (Zend_Exception $e){
			return new Ead1_Mensageiro("Erro ao Editar Area!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Método que cadastra a AreaIntegracao
	 * @param mixed $stCodSistema
	 */
	private function cadastrarAreaIntegracao(AreaConhecimentoTO $areaConhecimentoTO, $stCodSistema, $idSistema){
		$areaIntegracaoTO = new AreaIntegracaoTO();
		$areaIntegracaoTO->setSt_codsistema($stCodSistema);
		$areaIntegracaoTO->fetch(null, true, true);
		
		if(!$areaIntegracaoTO->getId_areaintegracao()){
			$areaIntegracaoTO->setId_areaconhecimento($areaConhecimentoTO->getId_areaconhecimento());
			$areaIntegracaoTO->setId_usuariocadastro($areaConhecimentoTO->getSessao()->id_usuario);
			$areaIntegracaoTO->setId_sistema($idSistema);
			
			$areaIntegracaoORM = new AreaIntegracaoORM();
			$areaIntegracaoORM->insert($areaIntegracaoTO->toArrayInsert());
		}
	}
}
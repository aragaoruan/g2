<?php
/**
 * Classe com regras de negócio para interações de Produto
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 21/02/2011
 *
 * @package models
 * @subpackage bo
 */
class ProdutoBO extends Ead1_BO
{

    public $mensageiro;

    /**
     * @var ProdutoDAO
     */
    public $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new ProdutoDAO();
    }

    /**
     * Metodo que decide se salva ou edita o Lançamento da Venda
     * @param LancamentoVendaTO $lvTO
     * @return Ead1_Mensageiro
     */
    public function salvarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        $orm = new LancamentoVendaORM();
        if ($orm->consulta($lvTO, true)) {
            return $this->editarLancamentoVenda($lvTO);
        }
        return $this->cadastrarLancamentoVenda($lvTO);
    }

    /**
     * Metodo que salva o produto projeto pedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        try {
            $pppTO->setId_entidade(($pppTO->getId_entidade() ? $pppTO->getId_entidade() : $pppTO->getSessao()->id_entidade));
            $pppTOConsulta = clone $pppTO;
            $pppTOConsulta->setId_produto(null);
            $orm = new ProdutoProjetoPedagogicoORM();
            $consulta = $orm->consulta($pppTOConsulta, true, true);
            if (!empty($consulta)) {
                $produtoTO = new ProdutoTO();
                $produtoTO->setId_produto($pppTOConsulta->getId_produto());
                $produtoTO->fetch(true, true, true);
                if ($produtoTO->getBl_ativo()) {
                    if ($consulta->getId_produto() != $pppTO->getId_produto()) {
                        THROW new Zend_Validate_Exception('Já Existe um Produto Cadastrado com o ' . Ead1_LabelFuncionalidade::getLabel(19) . ' Selecionado!');
                    } else {
                        $this->mensageiro->setMensageiro($pppTO);
                    }
                }
            } else {
                $this->cadastrarProdutoProjetoPedagogico($pppTO);
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que exclui os vinculos do produto com o texto sistema e cadastra um novo
     * @param ProdutoTextoSistemaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoTextoSistema(ProdutoTextoSistemaTO $to)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_produto() || !$to->getId_textosistema()) {
                THROW new Zend_Exception(!$to->getId_produto() ? 'O Id_produto não veio setado!' : 'O id_textossistema não veio setado!');
            }
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $orm = new ProdutoTextoSistemaORM();
            $lastProdutoTextoSistemaTO = $orm->consulta(new ProdutoTextoSistemaTO(
                array('id_produto' => $to->getId_produto(), 'id_entidade' => $to->getId_entidade())
            ), false, true);
            if ($lastProdutoTextoSistemaTO) {
                if ($lastProdutoTextoSistemaTO->getId_textosistema() != $to->getId_textosistema()) {
                    $this->dao->deletarProdutoTextoSistema($lastProdutoTextoSistemaTO);
                }
            }
            $this->dao->cadastrarProdutoTextoSistema($to);
            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular Produto ao Texto Sistema!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que exclui os vinculos do produto com a taxa e cadastra um novo
     * @param ProdutoTaxaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoTaxa(ProdutoTaxaTO $to)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_produto() || !$to->getId_taxa()) {
                THROW new Zend_Exception(!$to->getId_produto() ? 'O Id_produto não veio setado!' : 'O id_taxa não veio setado!');
            }
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $orm = new ProdutoTaxaORM();
            $lastProdutoTaxaTO = $orm->consulta(new ProdutoTaxaTO(
                array('id_produto' => $to->getId_produto(), 'id_entidade' => $to->getId_entidade())
            ), false, true);
            if ($lastProdutoTaxaTO) {
                $this->dao->deletarProdutoTaxaProjeto($lastProdutoTaxaTO);
                $this->dao->deletarProdutoTaxa($lastProdutoTaxaTO);
            }
            $this->dao->cadastrarProdutoTaxa($to);
            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular Produto a Taxa!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Metodo que exclui os vinculos do produto com o livro e cadastra um novo
     * @param ProdutoLivroTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoLivro(ProdutoLivroTO $to)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_produto() || !$to->getId_livro()) {
                THROW new Zend_Exception(!$to->getId_produto() ? 'O Id_produto não veio setado!' : 'O id_livro não veio setado!');
            }
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $orm = new ProdutoLivroORM();
            $lastProdutoLivroTO = $orm->consulta(new ProdutoLivroTO(
                array('id_produto' => $to->getId_produto(), 'id_entidade' => $to->getId_entidade(), 'id_livro' => $to->getId_livro())
            ), false, true);
            if ($lastProdutoLivroTO) {
                $this->dao->deletarProdutoLivro($lastProdutoLivroTO);
            }
            $this->dao->cadastrarProdutoLivro($to);
            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular Produto ao Livro!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que salva ou atualiza os vinculos do produto com o combo de produtos e cadastra um novo
     * @param ProdutoComboTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoCombo($pTO, $arrProdutoCombo)
    {

        try {

        	if($pTO->id_produto) {
        		$retorno = $this->editarProduto($pTO);
        	} else {
        		$retorno = $this->cadastrarProduto($pTO);
        	}

        	if($retorno->tipo == Ead1_IMensageiro::SUCESSO) {
        		if($retorno->mensagem[0] instanceOf ProdutoTO) {
        			$id_produto = $retorno->mensagem[0]->id_produto;
        		} else {
        			$id_produto = $pTO->id_produto;
        		}
        	}


        	if(!$id_produto) {
        		throw( new Zend_Exception('Erro ao cadastrar o produto. Dados insuficientes'));
        	}

        	$this->dao->beginTransaction();
            if (!empty($arrProdutoCombo)) {

                //Criando o objeto para pesquisar e excluir todos os produtos da tabela relacionados com o combo
                $prodCTO = new ProdutoComboTO();

                //Setando o valor do id do combo no objeto
                $prodCTO->setId_produto($id_produto);

                //Exclui todos os produtos relacionados
                $this->dao->excluirProdutoCombo($prodCTO);

                //Rodando o array para o cadastro dos itens com o combo
                foreach ($arrProdutoCombo as $pc) {
                    $produtoComboTO = new ProdutoComboTO();
                    $produtoComboTO->setId_produto($id_produto);
                    $produtoComboTO->setId_usuario($produtoComboTO->getId_usuario() ? $produtoComboTO->getId_usuario() : $produtoComboTO->getSessao()->id_usuario);
                    $produtoComboTO->setId_produtoitem($pc->id_produtoitem);
                    $produtoComboTO->setNu_descontoporcentagem($pc->nu_descontoporcentagem);

                    //Cadastrando os produtos no combo
                    $this->dao->cadastrarProdutoCombo($produtoComboTO);
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
            //return $this->mensageiro->setMensageiro("Produtos cadastrados no combo com sucesso!");
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao cadastrar produtos no combo", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que Exclui um produto do Combo de Produtos
     * @param ProdutoComboTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function excluirProdutoCombo(ProdutoComboTO $to)
    {
        try {
            $this->dao->beginTransaction();

            if ((int)$to->getId_produtocombo() == false) {
                throw new Zend_Exception("Informa o ID do Combo para Exclusão");
            }

            $vp = new VwProdutoVendaTO();
            $vp->setId_produto($to->getId_produto());
            $arr = $vp->fetch();
            if ($arr) {
                throw new Zend_Exception("Não é possível excluir este Produto, ele já foi envolvido em uma venda!");
            }

            if ($to->getId_produtocombo()) {
                $this->dao->excluirProdutoCombo($to);
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Excluir o Produto do Combo de Produtos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Metodo que exclui os vinculos do produto com a taxa e o projeto e cadastra os novos
     * @param array $arProdutoTaxaProjetoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArProdutoTaxaProjeto(array $arProdutoTaxaProjetoTO)
    {
        $this->dao->beginTransaction();
        try {
            if (is_array($arProdutoTaxaProjetoTO)) {
                //$this->dao->deletarProdutoTaxaProjetoPorProduto($arProdutoTaxaProjetoTO[0]->getId_produto());
                foreach ($arProdutoTaxaProjetoTO as $produtoTaxaProjetoTO) {
                    $this->cadastrarProdutoTaxaProjeto($produtoTaxaProjetoTO);
                }
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro("Vínculo Realizado com Sucesso", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Vincular Produto a Taxa!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Exclui os planos de pagamento velhos e cadastra novos.
     * @param array $arrPlanoPagamentoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrPlanoPagamento($arrPlanoPagamentoTO)
    {
        $this->dao->beginTransaction();
        try {
            if (!empty($arrPlanoPagamentoTO)) {
                $planoPagamentoTOdeletar = new PlanoPagamentoTO();
                $planoPagamentoTOdeletar->setId_produto($arrPlanoPagamentoTO[0]->getId_produto());
                $this->dao->deletarPlanoPagamento($planoPagamentoTOdeletar);
                foreach ($arrPlanoPagamentoTO as $planoPagamentoTOsalvar) {
                    if (!$this->dao->cadastrarPlanoPagamento($planoPagamentoTOsalvar)) {
                        $this->mensageiro->setMensageiro('Erro no Insert!', Ead1_IMensageiro::ERRO);
                    }
                }
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro('Plano(s) de Pagamento salvo(s) com Sucesso.', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollback();
            $this->mensageiro->setMensageiro('Erro ao cadastrar Plano(s) de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que salva o produto e cria o vinculo entre o produto e avaliacao
     * @param ProdutoTO $produtoTO
     * @param AvaliacaoTO $avaliacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoProdutoAvaliacao(ProdutoTO $produtoTO, AvaliacaoTO $avaliacaoTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$avaliacaoTO->getId_avaliacao()) {
                THROW new Zend_Exception('O id_avaliacao não veio setado!');
            }
            if (!$produtoTO->getId_produto()) {
                $mensageiroProduto = $this->cadastrarProduto($produtoTO);
            } else {
                $mensageiroProduto = $this->editarProduto($produtoTO);
            }
            if (!is_string($mensageiroProduto->mensagem[0])) {
                $produtoTO = $mensageiroProduto->mensagem[0];
            }
            switch ($mensageiroProduto->tipo) {
                case Ead1_IMensageiro::AVISO:
                    THROW new Zend_Validate_Exception($mensageiroProduto->mensagem[0]);
                    break;
                case Ead1_IMensageiro::ERRO:
                    THROW new Zend_Exception($mensageiroProduto->codigo);
                    break;
            }
            $produtoAvaliacaoORM = new ProdutoAvaliacaoORM();
            $produtoAvaliacaoTO = new ProdutoAvaliacaoTO(array('id_produto' => $produtoTO->getId_produto(),
                'id_avaliacao' => $avaliacaoTO->getId_avaliacao(),
                'id_entidade' => $produtoTO->getId_entidade()));
            if (!$produtoAvaliacaoORM->consulta($produtoAvaliacaoTO, true, true)) {
                $mensageiroAvaliacao = $this->cadastrarProdutoAvaliacao($produtoAvaliacaoTO);
                switch ($mensageiroAvaliacao->tipo) {
                    case Ead1_IMensageiro::AVISO:
                        THROW new Zend_Validate_Exception($mensageiroAvaliacao->mensagem[0]);
                        break;
                    case Ead1_IMensageiro::ERRO:
                        THROW new Zend_Exception($mensageiroAvaliacao->codigo);
                        break;
                }
            }
            $this->dao->commit();
            $this->mensageiro->setMensageiro($produtoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setMensageiro("Erro ao Salvar o Produto!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que cadastra um vinculo de produto com avaliacao
     * @param ProdutoAvaliacaoTO $paTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoAvaliacao(ProdutoAvaliacaoTO $paTO)
    {
        try {
            $paTO->setId_entidade($paTO->getId_entidade() ? $paTO->getId_entidade() : $paTO->getSessao()->id_entidade);
            $this->dao->cadastrarProdutoAvaliacao($paTO);
            $this->mensageiro->setMensageiro("Vinculo de Avaliação com Produto Criado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Cadastrar o Vinculo da Avaliação com o Produto!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra CampanhaProduto
     * @param CampanhaProdutoTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        try {
            $this->dao->cadastrarCampanhaProduto($cpTO);
            return $this->mensageiro->setMensageiro('Campanha do Produto Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Campanha do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Tag
     * @param TagTO $tagTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTag(TagTO $tagTO)
    {
        try {
            if (!$tagTO->getId_entidade()) {
                $tagTO->setId_entidade($tagTO->getSessao()->id_entidade);
            }
            if (!$tagTO->getId_usuariocadastro()) {
                $tagTO->setId_usuariocadastro($tagTO->getSessao()->id_usuario);
            }
            $this->dao->cadastrarTag($tagTO);
            return $this->mensageiro->setMensageiro('Tag Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Tag!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Cadastra tags para um produto.
     * @param int $id_produto
     * @param array $arrTags
     * @return Ead1_Mensageiro
     */
    public function cadastrarArrTagProduto($id_produto, $arrTags)
    {
        try {
            $this->dao->beginTransaction();
            $tagProdutoTO = new TagProdutoTO();
            $tagProdutoTO->setId_produto($id_produto);
            $this->dao->deletarTagProduto($tagProdutoTO);
            if (!empty($arrTags)) {
                foreach ($arrTags as $st_tag) {
                    $tagTO = new TagTO();
                    $st_tag = strtolower($st_tag);
                    $tagTO->setSt_tag($st_tag);
                    $tagTO->setId_entidade($tagTO->getSessao()->id_entidade);
                    $retorno = $this->dao->retornarTag($tagTO)->toArray();
                    if (empty($retorno)) {
                        $tagTO->setId_usuariocadastro($tagTO->getSessao()->id_usuario);
                        $id_tag = $this->dao->cadastrarTag($tagTO);
                        $tagTO->setId_tag($id_tag);
                    } else {
                        $tagTO = Ead1_TO_Dinamico::encapsularTo($retorno, new TagTO(), true);
                    }
                    $tagProdutoTO->setId_tag($tagTO->getId_tag());
                    $tagProdutoTO->setId_usuariocadastro($tagProdutoTO->getSessao()->id_usuario);
                    $this->dao->cadastrarTagProduto($tagProdutoTO);
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro("Tags cadastradas com sucesso!");
        } catch (Exception $e) {
        	Zend_Debug::dump($e,__CLASS__.'('.__LINE__.')');exit;
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro("Erro ao cadastrar Tags", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Tag de Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        try {
            $retorno = $this->dao->retornarTagProduto($tagProdutoTO)->toArray();
            if (!empty($retorno)) {
                return $this->mensageiro->setMensageiro('Essa Tag já está cadastrada nesse Produto!', Ead1_IMensageiro::AVISO);
            }
            if (!$tagProdutoTO->getId_usuariocadastro()) {
                $tagProdutoTO->setId_usuariocadastro($tagProdutoTO->getSessao()->id_usuario);
            }
            $this->dao->cadastrarTagProduto($tagProdutoTO);
            return $this->mensageiro->setMensageiro('Tag Cadastrada no Produto com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Tag no Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra array de CampanhaProduto
     * @param $arrayCampanhaProduto
     * @return Ead1_Mensageiro
     */
    public function cadastrarArrayCampanhaProduto($arrayCampanhaProduto)
    {
        try {
            $cpTO = new CampanhaProdutoTO();
            $cpTO->setId_campanhacomercial($arrayCampanhaProduto[0]->id_campanhacomercial);
            if (!$this->dao->deletarCampanhaProduto($cpTO)) {
                throw new Zend_Exception('Erro ao excluir produtos passados.');
            }
            foreach ($arrayCampanhaProduto as $campanhaProdutoTO) {
                if (!$this->dao->cadastrarCampanhaProduto($campanhaProdutoTO)) {
                    throw new Zend_Exception('Erro ao cadastrar Produto da Campanha!');
                }
            }
            return $this->mensageiro->setMensageiro('Produto(s) da Campanha cadastrado(s) com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Produtos da Campanha!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Salva todas os produtos para uma campanha
     * @param CampanhaProdutoTO $cTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function vincularTodasCampanhaProduto(CampanhaProdutoTO $cTO)
    {

        try {
            $this->dao->beginTransaction();

            if (!$cTO->getId_campanhacomercial()) {
                throw new Zend_Exception('A campanha comercial é obrigatória!');
            }

            if (!$cTO->getSessao()->id_entidade) {
                throw new Zend_Exception('Você precisa estar logado para vincular as Formas de Pagamento!');
            }


            $proTO = new ProdutoTO();
            $proTO->setId_entidade($proTO->getSessao()->id_entidade);

            $proORM = new ProdutoORM();
            $arrProduto = $proORM->consulta($proTO);

            $arrayCampanhaProduto = array();
            foreach ($arrProduto as $proTO) {
                $cpTO = new CampanhaProdutoTO();
                $cpTO->setId_campanhacomercial($cTO->getId_campanhacomercial());
                $cpTO->setId_produto($proTO->getid_produto());
                $arrayCampanhaProduto[] = $cpTO;
            }

            $mensageiro = $this->cadastrarArrayCampanhaProduto($arrayCampanhaProduto);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Todos Produtos foram vinculados à Campanha!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Vincular os Produtos da Campanha!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }


    /**
     * Metodo que cadastra Produto
     *
     * [Quem utiliza]
     * @see ProdutoRO::cadastrarProduto();
     *
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProduto(ProdutoTO $pTO, array $arcategoriasProduto = null)
    {
    	$this->dao->beginTransaction();
    	try {

        	if(!$pTO->getId_usuariocadastro())
            	$pTO->setId_usuariocadastro(   $pTO->getSessao()->id_usuario);


            if (!$pTO->getId_entidade()) {
                $pTO->setId_entidade($pTO->getSessao()->id_entidade);
            }
            $id_produto = $this->dao->cadastrarProduto($pTO);
            $pTO->setId_produto($id_produto);
            //salvando as categorias dos produtos
            if (is_array($arcategoriasProduto)) {
                $this->salvarArCategoriasProduto($arcategoriasProduto, $pTO);
            }
            //salvando os produtos do projeto
            if (is_array($arcategoriasProduto)) {
                $this->salvarArCategoriasProduto($arcategoriasProduto, $pTO);
            }

            $ba = new \G2\Negocio\Baiao();
            $ba->atualizaProduto($pTO->getId_produto());
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    public function cadastrarProdutoPrr(ProdutoTO $pTO, ProdutoPrrTO $prrTO, ProdutoValorTO $pvTO)
    {
            	$this->dao->beginTransaction();

        try {
            $pTO->setId_usuariocadastro($pTO->getSessao()->id_usuario);
            if (!$pTO->getId_entidade()) {
                $pTO->setId_entidade($pTO->getSessao()->id_entidade);
            }

            $id_produto = $this->dao->cadastrarProduto($pTO);
            $pTO->setId_produto($id_produto);
//        	Zend_Debug::dump($id_produto,__CLASS__.'('.__LINE__.')');

             $prrTO->setId_produto($id_produto);
             $cadPrdoutoPrr = $this->dao->cadastrarProdutoPrr($prrTO);
//        	Zend_Debug::dump($cadPrdoutoPrr,__CLASS__.'('.__LINE__.')');

            $pvTO->setId_produto($id_produto);
            $cadValor = $this->cadastrarProdutoValor($pvTO);
//        	Zend_Debug::dump($cadValor,__CLASS__.'('.__LINE__.')');exit;
            $this->dao->commit();

            return $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que cadastra um Projeto pedagógico a taxa em um produto
     *
     * @see ProdutoRO::cadastrarProdutoTaxaProjeto();
     *
     * @param ProdutoTaxaProjetoTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoTaxaProjeto(ProdutoTaxaProjetoTO $ptpTO)
    {
        try {
            if (!$ptpTO->getId_entidade()) {
                $ptpTO->setId_entidade($ptpTO->getSessao()->id_entidade);
            }
            $this->dao->cadastrarProdutoTaxaProjeto($ptpTO);
            return $this->mensageiro->setMensageiro($ptpTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            throw $e;
            return $this->mensageiro->setMensageiro('Erro ao vincular o Projeto Pedagógico à taxa, no Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        try {
            $pppTO->setId_entidade(($pppTO->getId_entidade() ? $pppTO->getId_entidade() : $pppTO->getSessao()->id_entidade));
            $this->dao->cadastrarProdutoProjetoPedagogico($pppTO);
            $this->mensageiro->setMensageiro($pppTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Produto Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra ProdutoValor
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoValor(ProdutoValorTO $pvTO)
    {
    	$this->dao->beginTransaction();
    	try {
            if (!$pvTO->getId_usuariocadastro()) {
                $pvTO->setId_usuariocadastro($pvTO->getSessao()->id_usuario);
            }
            $id_produtovalor = $this->dao->cadastrarProdutoValor($pvTO);
            $pvTO->setId_produtovalor($id_produtovalor);

            $ba = new \G2\Negocio\Baiao();
            $ba->atualizaProduto($pvTO->getId_produto());
            $this->dao->commit();

            return $this->mensageiro->setMensageiro($pvTO, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        try {
            $this->dao->cadastrarLancamentoVenda($lvTO);
            return $this->mensageiro->setMensageiro('Lançamento da Venda Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Lançamento da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra VendaProduto
     * @param VendaProdutoTO $vpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarVendaProduto(VendaProdutoTO $vpTO)
    {
        try {

            $vendaBO = new VendaBO();
        	$verificar = $vendaBO->verificarProdutoVendaUnica($vpTO->getId_venda(), $vpTO->getId_produto());
        	if($verificar->getTipo()==Ead1_IMensageiro::ERRO){
        		THROW new Zend_Exception($verificar->getFirstMensagem());
        	}

            $id_vendaproduto = $this->dao->cadastrarVendaProduto($vpTO);
            $vpTO->setId_vendaproduto($id_vendaproduto);
            return $this->mensageiro->setMensageiro($vpTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Venda do Produto: '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Metodo que cadastra o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoTextoSistema(ProdutoTextoSistemaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $this->dao->cadastrarProdutoTextoSistema($to);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar o Produto do Texto Sistema.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra o vinculo do produto com a Taxa
     * @param ProdutoTaxaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoProdutoTaxa(ProdutoTaxaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $this->dao->cadastrarProdutoProdutoTaxa($to);
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar a Taxa do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra Venda
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarVenda(VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_usuariocadastro()) {
                $vTO->setId_usuariocadastro($vTO->getSessao()->id_usuario);
            }
            if (!$vTO->getId_entidade()) {
                $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            }
            $id_venda = $this->dao->cadastrarVenda($vTO);
            $vTO->setId_venda($id_venda);
            return $this->mensageiro->setMensageiro($vTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        } catch (Zend_Db_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra Lancamento
     * @param LancamentoTO $lTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarLancamento(LancamentoTO $lTO)
    {
        try {
            $id_lancamento = $this->dao->cadastrarLancamento($lTO);
            $lTO->setId_lancamento($id_lancamento);
            return $this->mensageiro->setMensageiro($lTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Produto
     *
     * [Quem utiliza]
     * @see ProdutoRO::editarProduto();
     *
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     */
    public function editarProduto(ProdutoTO $pTO, array $arcategoriasProduto = null)
    {

        $this->dao->beginTransaction();
    	try {
            //Verificando se existe um produto na vendaproduto
            if (!$pTO->getBl_ativo()) {
                $vendaProdutoORM = new VendaProdutoORM();
                if ($vendaProdutoORM->consulta(new VendaProdutoTO(array('id_produto' => $pTO->getId_produto())), false, true)) {
                    THROW new Zend_Validate_Exception('Não é Possivel Excluir o Produto! O Produto Possui um Vinculo com uma Venda!');
                }
            }
            //editando as categorias dos produtos
            if (is_array($arcategoriasProduto)) {
                $this->salvarArCategoriasProduto($arcategoriasProduto, $pTO);
            }
            $this->dao->editarProduto($pTO);

            $ba = new \G2\Negocio\Baiao();
            $ba->atualizaProduto($pTO->getId_produto());
            $this->dao->commit();

            return $this->mensageiro->setMensageiro('Produto Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Editar Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Produto Prr
     *
     * [Quem utiliza]
     * @see ProdutoRO::editarProdutoPrr();
     *
     * @param ProdutoPrrTO $pTO
     * @return Ead1_Mensageiro
     */
    public function editarProdutoPrr(ProdutoTO $pTO, ProdutoValorTO $pvTO)
    {
        try {

            $this->dao->editarProduto($pTO);

            $pvConsulta = new ProdutoValorTO();
            $pvConsulta->setId_produto($pvTO->id_produto);
            $pvConsulta->fetch(false, true, true);

            $pvTO->setId_produtovalor($pvConsulta->getId_produtovalor());
            $this->dao->editarProdutoValor($pvTO);

            return $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Ead1_Mensageiro
     */
    public function editarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        try {
            $pppTO->setId_entidade(($pppTO->getId_entidade() ? $pppTO->getId_entidade() : $pppTO->getSessao()->id_entidade));
            $this->dao->editarProdutoProjetoPedagogico($pppTO);
            return $this->mensageiro->setMensageiro($pppTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Produto Projeto Pedagógico!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita ProdutoValor
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function editarProdutoValor(ProdutoValorTO $pvTO)
    {
    	$this->dao->beginTransaction();
    	try {
            $this->dao->editarProdutoValor($pvTO);

            $ba = new \G2\Negocio\Baiao();
            $ba->atualizaProduto($pvTO->getId_produto());
            $this->dao->commit();

            return $this->mensageiro->setMensageiro('Valor do Produto Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
        	$this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Editar Valor do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @return Ead1_Mensageiro
     */
    public function editarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        try {
            $this->dao->editarLancamentoVenda($lvTO);
            return $this->mensageiro->setMensageiro('Lançamento da Venda Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Lançamento da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita VendaProduto
     * @param VendaProdutoTO $vpTO
     * @return Ead1_Mensageiro
     */
    public function editarVendaProduto(VendaProdutoTO $vpTO)
    {
        try {
            $this->dao->editarVendaProduto($vpTO);
            return $this->mensageiro->setMensageiro('Venda do Produto Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Venda do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Venda
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     */
    public function editarVenda(VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_entidade()) {
                $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            }
            $this->dao->editarVenda($vTO);
            return $this->mensageiro->setMensageiro('Venda Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Lancamento
     * @param LancamentoTO $lTO
     * @return Ead1_Mensageiro
     */
    public function editarLancamento(LancamentoTO $lTO)
    {
        try {
            $this->dao->editarLancamento($lTO);
            return $this->mensageiro->setMensageiro('Lançamento Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Edita Tag
     * @param TagTO $tagTO
     * @return Ead1_Mensageiro
     */
    public function editarTag(TagTO $tagTO)
    {
        try {
            $tagTempTO = new TagTO();
            $tagTempTO->setSt_tag($tagTO->getSt_tag());
            $tagTempTO->setId_entidade($tagTO->getId_entidade());
            $retorno = $this->dao->retornarTag($tagTempTO)->toArray();
            if (!empty($retorno)) {
                return $this->mensageiro->setMensageiro('Já existe Tag com esse nome nessa ' . Ead1_LabelFuncionalidade::getLabel(3), Ead1_IMensageiro::AVISO);
            }

            $this->dao->editarTag($tagTO);
            return $this->mensageiro->setMensageiro('Tag Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Tag!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui CampanhaProduto
     * @param CampanhaProdutoTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        try {
            $this->dao->deletarCampanhaProduto($cpTO);
            return $this->mensageiro->setMensageiro('Campanha do Produto Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Campanha do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui Tag
     * @param TagTO $tagTO
     * @return Ead1_Mensageiro
     */
    public function deletarTag(TagTO $tagTO)
    {
        try {
            $this->dao->deletarTag($tagTO);
            return $this->mensageiro->setMensageiro('Tag Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Tag!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui relação de Tag com Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return Ead1_Mensageiro
     */
    public function deletarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        try {
            $this->dao->deletarTagProduto($tagProdutoTO);
            return $this->mensageiro->setMensageiro('Tag do Produto Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Tag do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui Produto
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     */
    public function deletarProduto(ProdutoTO $pTO)
    {
        try {
            $this->dao->deletarProduto($pTO);
            return $this->mensageiro->setMensageiro('Produto Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui/Inativa ProdutoPrr
     * @param ProdutoPrrTO $pTO
     * @return Ead1_Mensageiro
     */
    public function excluirProdutoPrr(ProdutoTO $pTO)
    {
        try {
            $pTO->setBl_ativo(false);
            $this->dao->editarProduto($pTO);
            return $this->mensageiro->setMensageiro('Produto Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Ead1_Mensageiro
     */
    public function deletarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        try {
            $this->dao->deletarProdutoProjetoPedagogico($pppTO);
            return $this->mensageiro->setMensageiro(Ead1_LabelFuncionalidade::getLabel(19) . ' Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Produto do ' . Ead1_LabelFuncionalidade::getLabel(19) . '!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui ProdutoValor
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function deletarProdutoValor(ProdutoValorTO $pvTO)
    {
        try {
            $this->dao->deletarProdutoValor($pvTO);
            return $this->mensageiro->setMensageiro('Valor do Produto Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Valor do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @return Ead1_Mensageiro
     */
    public function deletarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        try {
            $this->dao->deletarLancamentoVenda($lvTO);
            return $this->mensageiro->setMensageiro('Lançamento da Venda Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Lançamento da Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui VendaProduto
     * @param VendaProdutoTO $vpTO
     * @return Ead1_Mensageiro
     */
    public function deletarVendaProduto(VendaProdutoTO $vpTO)
    {
        try {
            $this->dao->beginTransaction();
            $matriculaDAO = new MatriculaDAO();
            $matriculaDAO->deletarVendaProdutoNivelSerie(new VendaProdutoNivelSerieTO(array('id_vendaproduto' => $vpTO->getId_vendaproduto())));
            $this->dao->deletarVendaProduto($vpTO);
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Venda do Produto Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Excluir Venda do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function deletarProdutoTextoSistema(ProdutoTextoSistemaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $this->dao->deletarProdutoTextoSistema($to);
            $this->mensageiro->setMensageiro('Texto Sistema Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Excluir o Produto do Texto Sistema.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que exclui o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTaxaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function deletarProdutoTaxa(ProdutoTaxaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $this->dao->deletarProdutoTaxa($to);
            $this->mensageiro->setMensageiro('Taxa do Produto Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Excluir Taxa do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Exclui Venda
     * @param VendaTO $vTO
     * @return Ead1_Mensageiro
     */
    public function deletarVenda(VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_entidade()) {
                $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            }
            $this->dao->deletarVenda($vTO);
            return $this->mensageiro->setMensageiro('Venda Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui Lancamento
     * @param LancamentoTO $lTO
     * @return Ead1_Mensageiro
     */
    public function deletarLancamento(LancamentoTO $lTO)
    {
        try {
            $this->dao->deletarLancamento($lTO);
            return $this->mensageiro->setMensageiro('Lançamento Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Campanha Produto
     * @param CampanhaProdutoTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function retornarCampanhaProduto(CampanhaProdutoTO $cpTO)
    {
        try {
            $retorno = $this->dao->retornarCampanhaProduto($cpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Campanha Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new CampanhaProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a Campanha do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Tag
     * @param TagTO $tagTO
     * @return Ead1_Mensageiro
     */
    public function retornarTag(TagTO $tagTO)
    {
        try {
            if (!$tagTO->getId_entidade()) {
                $tagTO->setId_entidade($tagTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarTag($tagTO)->toArray();
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TagTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Tag', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna Tag de Produto
     * @param TagProdutoTO $tagProdutoTO
     * @return Ead1_Mensageiro
     */
    public function retornarTagProduto(TagProdutoTO $tagProdutoTO)
    {
        try {
            $retorno = $this->dao->retornarTagProduto($tagProdutoTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Tag de Produto Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TagProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Tag de Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna tags de um produto
     * @param int $id_produto
     * @return Ead1_Mensageiro
     */
    public function retornarTagsDeProduto($id_produto)
    {
        try {
            $tagProdutoTO = new TagProdutoTO();
            $tagProdutoTO->setId_produto($id_produto);
            $retorno = $this->dao->retornarTagsDeProduto($tagProdutoTO)->toArray();
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TagTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Tag de Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retornar os planos de pagamento do produto
     * @param PlanoPagamentoTO $planoPagamentoTO
     */
    public function retornarPlanoPagamento(PlanoPagamentoTO $planoPagamentoTO)
    {
        try {
            $retorno = $this->dao->retornarPlanoPagamento($planoPagamentoTO);
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Plano de Pagamento Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new PlanoPagamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Plano(s) de Pagamento.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Retorna um Array de VwProtudosTO
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array $arrVwProdutoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro|Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarArrVwProduto(array $arrVwProdutoTO)
    {
        try {

            $produtos = array();

            if (empty($arrVwProdutoTO)) {
                throw new Zend_Exception('Array de Produtos vazio.');
            }

            foreach ($arrVwProdutoTO as $vwpTO) {
                if (!($vwpTO instanceof VwProdutoTO)) {
                    throw new Zend_Exception('O Array precisa ser composto de VwProdutoTO.');
                }
                $tmpmen = $this->retornarVwProduto($vwpTO);
                if ($tmpmen->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $produtos[] = $tmpmen->getFirstMensagem();
                }
            }
            if (empty($produtos)) {
                return new Ead1_Mensageiro('Nenhum Produto encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                return new Ead1_Mensageiro($produtos, Ead1_IMensageiro::SUCESSO);
            }

        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar ao retornar os produtos!', Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Metodo que retorna a view de produto
     * @param VwProdutoTO $vwpTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProduto(VwProdutoTO $vwpTO, $isCombo = false)
    {
        try {
            if ($vwpTO->getSt_produto() == '') {
                $vwpTO->setSt_produto(null);
            }

            if (!$vwpTO->getId_entidade()) {
                $vwpTO->setId_entidade($vwpTO->getSessao()->id_entidade);
            }

            $retorno = $this->dao->retornarVwProduto($vwpTO, $isCombo)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a view de produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna a Vw_produto verificando o compartilhamento entre entidades
     * @param VwProdutoTO $vwpTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwProdutoCompartilhado(VwProdutoTO $vwpTO)
    {
        try {
            if (!$vwpTO->getId_produto()) {
            	throw new Zend_Exception("Favor informar o ID do Produto!");
            }

            if (!$vwpTO->getId_entidade()) {
                throw new Zend_Exception("Favor informar o ID da Entidade!");
            }

            $retorno = $this->dao->retornarVwProdutoCompartilhado($vwpTO->getId_produto(), $vwpTO->getId_entidade());
            if (!$retorno) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro: '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /*
     * Método que retorna a VW de Produto do tipo PRR
     * criada em 29/08/2013 para AC-433
     * Débora Castro - deboracastro.pm@gmail.com
     */
    public function retornarVwProdutoPrr(VwProdutoPrrTO $vwProdPrr)
    {
        try {

            if ($vwProdPrr->getSt_produto() == '') {
                $vwProdPrr->setSt_produto(null);
            }

            if (!$vwProdPrr->getId_entidade()) {
                $vwProdPrr->setId_entidade($vwProdPrr->getSessao()->id_entidade);
            }

            $vwProdPrr->setBl_ativo(true);

            $retorno = $this->dao->retornarVwProdutoPrr($vwProdPrr)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwProdutoPrrTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a view de produto prr', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Metodo que retorna a view de produto venda
     * @param VwProdutoVendaTO $vwpTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoVenda(VwProdutoVendaTO $vwpTO)
    {
        try {
            if (!$vwpTO->getId_entidade()) {
                $vwpTO->setId_entidade($vwpTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVwProdutoVenda($vwpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwProdutoVendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a view de produto venda', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a view de produto combo
     * @param VwProdutoComboTO $vwpTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoCombo(VwProdutoComboTO $vwpTO)
    {
        try {
            if (!$vwpTO->getId_entidade()) {
                $vwpTO->setId_entidade($vwpTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVwProdutoCombo($vwpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwProdutoComboTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a view de produto combo', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo do Produto
     * @param TipoProdutoTO $tpTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoProduto(TipoProdutoTO $tpTO)
    {
        try {
            $retorno = $this->dao->retornarTipoProduto($tpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo do Produto em caso de Combo
     * @param TipoProdutoTO $tpTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoProdutoCombo(TipoProdutoTO $tpTO)
    {
    	try {
    		$retorno = $this->dao->retornarTipoProdutoCombo($tpTO)->toArray();
    		if (empty($retorno)) {
    			return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
    		}
    		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoProdutoTO()), Ead1_IMensageiro::SUCESSO);
    	} catch (Zend_Exception $e) {
    		return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
    	}
    }

    /**
     * Metodo que retorna o Produto
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornarProduto(ProdutoTO $pTO)
    {
        try {
            $retorno = $this->dao->retornarProduto($pTO)->toArray();
            if (empty($retorno)) {
                //return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado de Produto Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o ProdutoProjetoPedagogico
     * @param ProdutoProjetoPedagogicoTO $pppTO
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO)
    {
        try {
            $pppTO->setId_entidade(!$pppTO->getId_entidade() ? $pppTO->getSessao()->id_entidade : $pppTO->getId_entidade());
            $retorno = $this->dao->retornarProdutoProjetoPedagogico($pppTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ProjetoPedagogicoProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Produto do Projeto Pedagógico', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o ProdutoValor
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoValor(ProdutoValorTO $pvTO)
    {
        try {
            $retorno = $this->dao->retornarProdutoValor($pvTO)->toArray();
            $x = Ead1_TO_Dinamico::encapsularTo($retorno, new ProdutoValorTO(), true);
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new ProdutoValorTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Valor do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Retorna o Produto valor Ativo
     * @param ProdutoValorTO $pvTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarProdutoValorAtivo(ProdutoValorTO $pvTO)
    {
        try {
            $retorno = $this->dao->retornarProdutoValorAtivo($pvTO)->toArray();
            if (empty($retorno)) {
                return new Ead1_Mensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            $to = new ProdutoValorTO();
            $to->montaToDinamico($retorno[0]);

            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar o Valor do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Metodo que retorna o LancamentoVenda
     * @param LancamentoVendaTO $lvTO
     * @return Ead1_Mensageiro
     */
    public function retornarLancamentoVenda(LancamentoVendaTO $lvTO)
    {
        try {
            $retorno = $this->dao->retornarLancamentoVenda($lvTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new LancamentoVendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Lançamento da Venda', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o VendaProduto
     * @param VendaProdutoTO $vpTO
     * @return Ead1_Mensageiro
     */
    public function retornarVendaProduto(VendaProdutoTO $vpTO)
    {
        try {
            $retorno = $this->dao->retornarVendaProduto($vpTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VendaProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a Venda do Produto', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Venda
     * @param VendaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornarVenda(VendaTO $vTO)
    {
        try {
            if (!$vTO->getId_entidade()) {
                $vTO->setId_entidade($vTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVenda($vTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VendaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Venda Usuário
     * @param VwVendaUsuarioTO $vTO
     * @return Ead1_Mensageiro
     */
    public function retornarVendaUsuario(VwVendaUsuarioTO $vTO)
    {
        try {
            $retorno = $this->dao->retornarVendaUsuario($vTO)->toArray();

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($vTO->getId_venda());
            $retornoVenda = $this->dao->retornarVenda($vendaTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwVendaUsuarioTO()), Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro->addMensagem(Ead1_TO_Dinamico::encapsularTo($retornoVenda, new VendaTO(), true));
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar a Venda!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Lançamento
     * @param LancamentoTO $lTO
     * @return Ead1_Mensageiro
     */
    public function retornarLancamento(LancamentoTO $lTO)
    {
        try {
            $retorno = $this->dao->retornarLancamento($lTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new LancamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo de Lançamento
     * @param TipoLancamentoTO $tlTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoLancamento(TipoLancamentoTO $tlTO)
    {
        try {
            $retorno = $this->dao->retornarTipoLancamento($tlTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoLancamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Tipo de Lançamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna os dados da view de lancamento
     * @param VwLancamentoTO $vwLTO
     * @return Zend_Db_Table_Rowset_Abstract|false
     */
    public function retornarVwLancamento(VwLancamentoTO $vwLTO)
    {
        try {
            if (!$vwLTO->getId_entidade()) {
                $vwLTO->setId_entidade($vwLTO->getSessao()->id_entidade);
            }
            $retorno = $this->dao->retornarVwLancamento($vwLTO)->toArray();
            if (empty($retorno)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwLancamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar os Lançamentos!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o tipo de valor do produto
     * @param TipoProdutoValorTO $tpvTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoProdutoValor(TipoProdutoValorTO $tpvTO)
    {
        try {
            $tipo = $this->dao->retornarTipoProdutoValor($tpvTO)->toArray();
            if (empty($tipo)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!',  Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo, new TipoProdutoValorTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Valor do Produto.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o vinculo do produto com a declaracao (textosistema)
     * @param ProdutoTextoSistemaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoTextoSistema(ProdutoTextoSistemaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarProdutoTextoSistema($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProdutoTextoSistemaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar o Produto do Texto Sistema.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna o vinculo do produto com a taxa
     * @param ProdutoTaxaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoTaxa(ProdutoTaxaTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarProdutoTaxa($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProdutoTaxaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar a Taxa do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna o vinculo do produto com o livro
     * @param ProdutoLivroTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoLivro(ProdutoLivroTO $to)
    {
        try {
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $dados = $this->dao->retornarProdutoLivro($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProdutoLivroTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar o Vínculo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna o vinculo do produto com o combo de produtos
     * @param ProdutoComboTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoCombo(ProdutoComboTO $to)
    {
        try {
            $dados = $this->dao->retornarProdutoCombo($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProdutoComboTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar o Vínculo!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a Taxa
     * @param TaxaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarTaxa(TaxaTO $to)
    {
        try {
            $dados = $this->dao->retornarTaxa($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TaxaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar a Taxa!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna os projetos em uma view para recarregar os projetos da taxa
     * @param ProdutoTaxaProjetoTO $produtoTaxaProjetoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoTaxaProjetoVw(ProdutoTaxaProjetoTO $produtoTaxaProjetoTO)
    {
        try {
            $arProjetos = false;
            if ($produtoTaxaProjetoTO->getId_produto() && $produtoTaxaProjetoTO->getId_taxa()) {
                $produtoTaxaProjetoTO->setId_entidade($produtoTaxaProjetoTO->getSessao()->id_entidade);
                foreach ($produtoTaxaProjetoTO->fetch() as $produtoTaxaProjetoTO) {
                    $vwAreaProjetoNivelEnsinoTO = new VwAreaProjetoNivelEnsinoTO();
                    $vwAreaProjetoNivelEnsinoTO->setId_projetopedagogico($produtoTaxaProjetoTO->getId_projetopedagogico());
                    $vwAreaProjetoNivelEnsinoTO->fetch(false, true, true);
                    $arProjetos[] = $vwAreaProjetoNivelEnsinoTO;
                }
            }
            $this->mensageiro->setMensageiro($arProjetos);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna as taxas disponiveis para o produto da entidade
     * @param ProdutoTaxaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTaxasDisponiveisProdutoEntidade(ProdutoTaxaTO $to)
    {
        try {
            $orm = new TaxaORM();
            $to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
            $where = "id_taxa NOT IN(SELECT id_taxa FROM tb_produtotaxa WHERE id_entidade = " . $to->getId_entidade() . ($to->getId_produto() ? "and id_produto != " . $to->getId_produto() : "") . ")";
            $dados = $this->dao->retornarTaxa(new TaxaTO(), $where)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhuma Taxa Disponivel!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TaxaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar a Taxa!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna o Produto Pelo id da avaliacao e da entidade
     * @param ProdutoAvaliacaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarProdutoAvaliacaoEntidade(ProdutoAvaliacaoTO $to)
    {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            if (!$to->getId_avaliacao()) {
                THROW new Zend_Exception("O id_avaliacao não foi setado!");
            }
            $dados = $this->dao->retornarProdutoAvaliacaoEntidade($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro("Nenhum Registro Encontrado!", Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new ProdutoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Produto da Avaliação!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dados da view dos Produtos do textosistema
     * @param VwProdutoTextoSistemaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoTextoSistema(VwProdutoTextoSistemaTO $to)
    {
        try {
            $to->setId_entidade($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            $dados = $this->dao->retornarVwProdutoTextoSistema($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwProdutoTextoSistemaTO()));
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar os Produtos!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Método que retorna as imagens vinculadas a um produto
     * @param ProdutoImagemTO $to
     * @throws Zend_Validate
     * @throws Zend_Validate_Exception
     * @return Ead1_Mensageiro (UploadTO)
     */
    public function retornarProdutoImagem(ProdutoImagemTO $to)
    {
        try {
            if (!$to->getId_produto()) {
                throw new Zend_Validate_Exception("O id_produto é obrigatório e não foi passado!");
            }
            $to->setBl_ativo(true);
            $imagens = $this->dao->retornaProdutoImagem($to)->toArray();
            if (empty($imagens)) {
                throw new Zend_Validate_Exception("Nenhum registro encontrado!");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($imagens, new UploadTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Upload de imagem para produto
     * @param ProdutoImagemTO $produtoTo
     * @param ArquivoTO $arquivoImagem
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function uploadImagemProduto(ProdutoImagemTO $produtoTo, ArquivoTO $arquivoImagem)
    {
        try {
            if (!$produtoTo->getId_produto() || (!$arquivoImagem instanceof ArquivoTO)) {
                throw new Zend_Exception('O id_produto é obrigatório e/ou o arquivo para upload!');
            }

            $arquivoImagem->setSt_nomearquivo($produtoTo->getId_produto() . '_' . mt_rand(1000, 9999));
            $arquivoImagem->setSt_caminhodiretorio('produto' . DIRECTORY_SEPARATOR);
            $meupload = $this->uploadBasico($arquivoImagem);

            if ($meupload->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($meupload->getFirstMensagem());
            }
            $upTO = new UploadTO();
            $upTO->setSt_upload($arquivoImagem->getSt_nomearquivo() . '.' . $arquivoImagem->getSt_extensaoarquivo());

            $uploadDao = new UploadDAO();
            $upTO->setId_upload($uploadDao->cadastrarUpload($upTO));

            $produtoTo->setId_upload($upTO->getId_upload());
            $produtoTo->setBl_ativo(true);
            $produtoTo->setId_produtoimagem($this->dao->cadastrarProdutoImagem($produtoTo));

            $this->mensageiro->setMensageiro($upTO, Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Deleta de forma lógica uma imagem
     * @param ProdutoImagemTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function deletarProdutoImagem(ProdutoImagemTO $to)
    {
        try {

            if (!$to->getId_upload()) {
                throw new Zend_Exception("o id_upload é obrigatorio e não foi passado!");
            }
            $to->setBl_ativo(true);
            $to->fetch(false, true, true);

            $to->setBl_ativo(false);
            if ($this->dao->editarProdutoImagem($to)) {
                $this->mensageiro->setMensageiro('Imagem excluída com sucesso!', Ead1_IMensageiro::SUCESSO);
            }

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao excluir imagem: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Salva imagem padrão de um produto
     * @param ProdutoImagemTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoImagemPadrao(ProdutoImagemTO $to)
    {
        try {
            if (!$to->getId_produto() || !$to->getId_upload()) {
                throw new Zend_Exception('O id_produto e/ou o id_upload são obrigatorios e não foram passados');
            }
            $to->setBl_ativo(true);
            $to->fetch(false, true, true);

            $produto = new ProdutoTO();
            $produto->setId_produto($to->id_produto);
            $produto->setId_produtoimagempadrao($to->getId_upload());

            if ($this->dao->editarProduto($produto)) {
                $this->mensageiro->setMensageiro("Imagem padrão cadastrada com sucesso!", Ead1_IMensageiro::SUCESSO);
            }

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao cadastrar imagem padrão: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Retorna o array de categorias do produto
     * @param ProdutoTO $produtoTo
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaCategoriasProduto(ProdutoTO $produtoTo)
    {
        try {
            if (!$produtoTo->getId_produto()) {
                throw new Zend_Exception('O id_produto é obrigatório e não foi passado!');
            }
            $categorias = $this->dao->retornaCategoriasProduto($produtoTo);
            if (!$categorias) {
                $this->mensageiro->setMensageiro('Nenhuma categoria vinculada a esse produto!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($categorias, new CategoriaProdutoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar categorias do produto: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Retorna as categorias de um produto
     * @param ProdutoTO $produtoTo
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornaCategoriasDoProduto(ProdutoTO $produtoTo)
    {
        try {
            if (!$produtoTo->getId_produto()) {
                throw new Zend_Exception('O id_produto é obrigatório e não foi passado!');
            }
            $categorias = $this->dao->retornarCategoriasDoProduto($produtoTo);
            if (!$categorias) {
                $this->mensageiro->setMensageiro('Nenhuma categoria vinculada a esse produto!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($categorias, new CategoriaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar categorias do produto: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Retorna o array de produtos de categoria
     * @param $id_categoria
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarCategoriaProduto($id_categoria)
    {
        try {
            $categoriaProdutoTO = new CategoriaProdutoTO();
            $categoriaProdutoTO->setId_categoria($id_categoria);
            $categorias = $this->dao->retornarCategoriaProduto($categoriaProdutoTO);
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($categorias, new ProdutoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar produtos de categoria: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }


    /**
     * Método que cadastra os vinculos da categoria com o produto
     * @see ProdutoBO::cadastrarProduto
     * @see ProdutoBO::editarProduto
     * @param array $arcategoriasProduto
     * @param ProdutoTO $pTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarArCategoriasProduto(array $arcategoriasProduto, ProdutoTO $pTO)
    {
        $this->dao->beginTransaction();
        try {
            if (!$pTO->getId_produto()) {
                throw new Zend_Exception("A chave do produto não foi encontrada.");
            }
            $this->dao->deletarCategoriaProdutoPorProduto($pTO->getId_produto());
			$arrayCatPro = array();
            foreach ($arcategoriasProduto as $categoriaTO) {
                $categoriaProdutoTO = new CategoriaProdutoTO();
                $categoriaProdutoTO->setId_categoria($categoriaTO->getId_categoria());
                $categoriaProdutoTO->setId_produto($pTO->getId_produto());
                $categoriaProdutoTO->setId_usuariocadastro($pTO->getSessao()->id_usuario);
                $this->dao->cadastrarArCategoriaProdutos($categoriaProdutoTO);
                $arrayCatPro[] = $categoriaProdutoTO;
            }

            $this->dao->commit();
            return new Ead1_Mensageiro("Vínculo de categorias ao produto realizado com sucesso!", Ead1_IMensageiro::SUCESSO, $arrayCatPro);
        } catch (Exception $e) {
            $this->dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro("Erro ao realizar Vínculo de categorias com o produto!", Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * Salva o vínculo de uma categoria com um produto
     * @param CategoriaProdutoTO $categoriaProdutoTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function salvarCateroriaProduto(CategoriaProdutoTO $categoriaProdutoTO){

        $this->dao->beginTransaction();
    	try {

            if (!$categoriaProdutoTO->getId_produto()) {
    			throw new Zend_Exception("A chave do produto não foi encontrada.");
    		}
    		if (!$categoriaProdutoTO->getId_categoria()) {
    			throw new Zend_Exception("A categoria do produto não foi encontrada.");
    		}

            $categoriaProdutoTO->fetch(false, true, true);
    		if($categoriaProdutoTO->getId_usuariocadastro()==false){
	    		$categoriaProdutoTO->setId_usuariocadastro($categoriaProdutoTO->getSessao()->id_usuario);
	    		$this->dao->cadastrarArCategoriaProdutos($categoriaProdutoTO);
    		}

            $this->dao->commit();
    		return new Ead1_Mensageiro($categoriaProdutoTO, Ead1_IMensageiro::SUCESSO);
    	} catch (Exception $e) {
    		$this->dao->rollBack();
    		return new Ead1_Mensageiro("Erro ao realizar Vínculo de categoria com o produto!", Ead1_IMensageiro::ERRO, $e);
    	}

    }

}

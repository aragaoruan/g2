<?php
/**
 * Classe com regras de negócio para interações de Categoria de Produtos
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @since 12/12/2012
 * 
 * @package models
 * @subpackage bo
 */
class CategoriaBO extends Ead1_BO{
	
	public $mensageiro;
	
	public $dao;
	
	public function __construct(){
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new CategoriaDAO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita Categoria
	 * @param CategoriaTO $to
	 * @param array $arEntidadeTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCategoria(CategoriaTO $to, array $arEntidadeTO = null, ArquivoTO $arquivo = null){
		$this->dao->beginTransaction();
		try{
			if($to->getId_categoria()){
				$mensageiro = $this->mensageiro->setMensageiro($this->editarCategoria($to, $arEntidadeTO));
				if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO && $arquivo){
					$this->salvarImagemCategoria($to, $arquivo);
				}
			} else {
				$mensageiro = $this->mensageiro->setMensageiro($this->cadastrarCategoria($to, $arEntidadeTO));
				if($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO && $arquivo){
					$this->salvarImagemCategoria($this->mensageiro->getFirstMensagem(), $arquivo);
				}
			}
			$this->mensageiro = $mensageiro;
			$this->dao->commit();
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro("Erro ao salvar categoria - {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Meotodo que Cadastra Categoria
	 * Metodo atribui o id_entidadecadastro, id_usuariocadastro e dt_cadastro 
	 * não necessitando fazer isso no Flex ou em outros metodos
	 * 
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCategoria(CategoriaTO $to, array $arEntidadeTO = null){
		$this->dao->beginTransaction();
		try{
			if(!$to->getId_entidadecadastro()){
				$to->setId_entidadecadastro($to->getSessao()->id_entidade);
			}
			
			if(!$to->getId_usuariocadastro()){
				$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			}
			
			$to->setDt_cadastro(new Zend_Date());
			$to->setBl_ativo(true);
			
			$id_categoria = $this->dao->cadastrarCategoria($to);
			
			if(!$id_categoria){
				throw new Zend_Exception("Erro ao cadastrar Categoria {$to->getId_categoria()} - {$to->getSt_categoria()}!");
			}else{
				$to->setId_categoria($id_categoria);
				if($arEntidadeTO) {
					foreach ($arEntidadeTO as $entidade){
						$this->salvarArCategoriaEntidade($entidade, $to);
					}
				}
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Categoria salva com sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();			
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Meotodo que Edita Categoria de Produto
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function editarCategoria(CategoriaTO $to, array $arEntidadeTO = null){
		$this->dao->beginTransaction();
		try{
			if(!$this->dao->editarCategoria($to)){
				throw new Zend_Exception("Erro ao editar Categoria {$to->getId_categoria()} - {$to->getSt_categoria()}!");
			} else if($arEntidadeTO) {
				foreach ($arEntidadeTO as $entidade){
					$this->salvarArCategoriaEntidade($arEntidadeTO, $to);
				}
			}
			
			$this->dao->commit();
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO, 'Categoria Editada com Sucesso!');
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getTraceAsString());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que Desvincula a Categoria, fazendo a exclusão lógica, passando o bl_ativo para false
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function desvincularCategoria(CategoriaTO $to){
		$this->dao->beginTransaction();
		try{
			$to->setBl_ativo(false);
			if(!$this->dao->editarCategoria($to)){
				throw new Zend_Exception("Erro ao desvincular a Categoria {$to->getId_categoria()} - {$to->getSt_categoria()}!");
			}
			 
			$this->dao->commit();
			return $this->mensageiro->setMensageiro('Categoria Excluida com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $dao->excecao);
		}
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna categorias
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarCategoria(CategoriaTO $to){
		try{
			$retorno = $this->dao->retornarCategoria($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhuma Categoria Encontrada!',Ead1_IMensageiro::AVISO,$this->dao->excecao);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new CategoriaTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar a Categoria',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
		
	}
	
	/**
	 * Metodo que retorna uma árvore de categorias na forma de objetos a partir de uma categoria
	 * Se for passado null como parametro, ele retornará uma arvore de TO com todas as categorias cadastradas
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarArvoreCategoria(CategoriaTO $to = null){
		if($to === null){
			$to = new CategoriaTO();
			$to->setId_categoria(null);
			$to->setBl_ativo(true);
		}
		
		$arCategoriasFilhas = array();
		$arCategoriasFilhas = $this->retornarFilhosCategoria($to);
		$to->setAr_categoriasfilha($arCategoriasFilhas);
			
		if(empty($arCategoriasFilhas)){
			$this->mensageiro->setMensageiro("Nenhuma categoria encontrado!", Ead1_IMensageiro::AVISO);
		}else {
			$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
		}
// 		Zend_Debug::dump($this->mensageiro,__CLASS__.'('.__LINE__.')');exit;
		
		return $this->mensageiro;
	}
	
	/**
	 * Metodo que retorna um array de categorias pai para preencher o cbCategoriaPai
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarArrayCategoriasPai(){
		try{
			
			//Retorno na forma de árvore
					
			//$to = $this->retornarArvoreCategoria()->getFirstMensagem();
			//$array = array();
					
			//if($to){
			//	$this->formatarArray($to, '', $array);
			//}
					
			//return $this->mensageiro->setMensageiro($array, Ead1_IMensageiro::SUCESSO);
			
			//Fim do retorno na forma de árvore
		
			$to = new CategoriaEntidadeTO();
			$to->setId_entidade($to->getSessao()->id_entidade);
			
			$array = array();
			
			foreach ($this->dao->retornarCategoriaEntidade($to) as $categoriaEntidade){
				$categ = new CategoriaTO();
				$categ->setId_categoria($categoriaEntidade['id_categoria']);
				$array[] = $categ->fetch(true, true, true);
			}
			
			if(!empty($array)){
				$this->mensageiro->setMensageiro($array, Ead1_IMensageiro::SUCESSO);
			}else {
				$this->mensageiro->setMensageiro("Nenhuma categoria foi encontrada!", Ead1_IMensageiro::AVISO);
			}
						
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro retornar pesquisa de categorias {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		
		return $this->mensageiro;
		
	}

	/**
	 * Método recursivo que formata o nome das categorias simulando uma árvore e retorna no array passado como parâmetro $retorno
	 * 
	 * @see CategoriaBO::retornarArrayCategoriasPai()
	 * 
	 * @param CategoriaTO $to
	 * @param mixed $marca
	 * @param array $retorno
	 */
	private function formatarArray(CategoriaTO $to, $marca, array &$retorno){
		if(!$to->getId_categoriapai()){
			$marca = '';
		}
		
		if($to->getId_categoria()){
			$to->setSt_categoria($marca . $to->getSt_categoria());
			$retorno[] = $to;
			
		}
		
		if($to->getAr_categoriasfilha()){
			foreach($to->getAr_categoriasfilha() as $toFilho){
				$this->formatarArray($toFilho, $marca . '    ', $retorno);
			}
		}else{
			return;
		}
	
	}
	
	/**
	 * Metodo que retorna uma árvore de categorias na forma de objetos a partir de uma categoria
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro $mensageiro
	 */
	public function retornarFilhosCategoria(CategoriaTO $to){
		
		$to->setBl_ativo(true);
		
		$categoriasFilha = new CategoriaTO();
		$categoriasFilha->setId_categoriapai($to->getId_categoria());
		
	    $where = $to->getId_categoria() ? '' :  "bl_ativo = 1 AND id_categoriapai is null AND id_entidade = ".$to->getSessao()->id_entidade;
		
		$categoriasFilha = $this->dao->retornarCategoriaFilha($categoriasFilha, $where)->toArray();
		
		$retorno = array();
		
		if($categoriasFilha){
			$categoriasFilha = Ead1_TO_Dinamico::encapsularTo($categoriasFilha, new CategoriaTO());
			foreach ($categoriasFilha as $cat){
				$cat->setAr_categoriasfilha($this->retornarFilhosCategoria($cat));
				$retorno[] = $cat;
			}
			return $retorno;
		}else {
			return;
		}
	}
	
	/**
	 * Metodo que retorna um array de entidades que estão relacionadas com determinada categoria
	 * @param CategoriaTO $categoriaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArCategoriaEntidade(CategoriaTO $categoriaTO){
		//return $this->dao->retornarArCategoriaEntidade($categoriaTO);
		
		try{

			$to = new CategoriaEntidadeTO();
			$to->setId_categoria($categoriaTO->getId_categoria());
			//$to->setId_entidade($to->getSessao()->id_entidade);
				
			$array = array();
				
			foreach ($this->dao->retornarCategoriaEntidade($to) as $categoriaEntidade){
				$entidadeTO = new EntidadeTO();
				$entidadeTO->setId_entidade($categoriaEntidade['id_entidade']);
				$array[] = $entidadeTO->fetch(true, true, true);
			}
				
			if(!empty($array)){
				$this->mensageiro->setMensageiro($array, Ead1_IMensageiro::SUCESSO);
			}else {
				$this->mensageiro->setMensageiro("Nenhum vínculo categoria/entidade foi encontrado!", Ead1_IMensageiro::AVISO);
			}
		
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro retornar pesquisa de categorias {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		
		return $this->mensageiro;
		
	}
	
	/**
	 * Método que cadastra os vínculos de categoria com entidade
	 * @param mixed $entidadesTO array de Tos do tipo EntidadeTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArCategoriaEntidade($entidadesTO, CategoriaTO $categoriaTO){
		
		try {
			$this->dao->beginTransaction();
			if(!$categoriaTO->getId_categoria()){
				throw new Zend_Exception("A chave da categoria não foi encontrada.");
			}
			$this->dao->deletarCategoriaEntidade($categoriaTO->getId_categoria());
			if(is_array($entidadesTO)){
				foreach($entidadesTO as $entidadeTO){
					$categoriaEntidadeTO = new CategoriaEntidadeTO();
					$categoriaEntidadeTO->setId_entidade($entidadeTO->getId_entidade());
					$categoriaEntidadeTO->setId_categoria($categoriaTO->getId_categoria());
					$categoriaEntidadeTO->setId_usuariocadastro($categoriaEntidadeTO->getSessao()->id_usuario);
					$categoriaEntidadeTO->setDt_cadastro(new Zend_Date(null));
					$this->dao->cadastrarCategoriaEntidade($categoriaEntidadeTO);
				}
			} else if ($entidadesTO instanceof EntidadeTO){
				$categoriaEntidadeTO = new CategoriaEntidadeTO();
				$categoriaEntidadeTO->setId_entidade($entidadesTO->getId_entidade());
				$categoriaEntidadeTO->setId_categoria($categoriaTO->getId_categoria());
				$categoriaEntidadeTO->setId_usuariocadastro($categoriaEntidadeTO->getSessao()->id_usuario);
				$categoriaEntidadeTO->setDt_cadastro(new Zend_Date(null));
				$this->dao->cadastrarCategoriaEntidade($categoriaEntidadeTO);
			} else {
				throw new Zend_Exception('Parâmetro $entidadesTO deve ser um array ou um objeto do tipo EntidadeTO!');
			}
				
			$this->dao->commit();
			return new Ead1_Mensageiro("Vínculo de Categoria com Entidade realizado com sucesso!");
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			throw $e;
			return new Ead1_Mensageiro("Erro ao realizar Vínculo de categoria com entidade!", Ead1_IMensageiro::ERRO, $e);
		}
	
	}
	
	/**
	 * Método que salva a imagem da categoria
	 * @param CategoriaTO $categoria
	 * @param ArquivoTO $arquivo
	 * @return Ead1_Mensageiro
	 */
	public function salvarImagemCategoria(CategoriaTO $categoria, ArquivoTO $arquivo){
		$this->dao->beginTransaction();
		try {

			if($arquivo instanceof ArquivoTO){
				$arquivo->setSt_nomearquivo($categoria->getId_categoria());
				$retorno = $this->uploadBasico($arquivo);
				
			} elseif(!empty($arquivo['name'])) {
				$renomear = explode('.', $arquivo['name']);
				$arquivo['name'] = $categoria->getId_categoria().'.'.$renomear[1];
	
				$adapter = new Zend_File_Transfer_Adapter_Http();
				$adapter->addFilter('Rename', $arquivo['name']);
				$adapter->setDestination ('upload'.DIRECTORY_SEPARATOR.'tramite'.DIRECTORY_SEPARATOR) ;

				if (!$adapter->receive()) {
					$messages = $adapter->getMessages();
					echo implode("\n", $messages);
				}
	
				$upload = new Zend_File_Transfer();
				$upload->receive();
	
			};
	
			$upload = new UploadTO();
			$upload->setSt_upload($arquivo instanceof ArquivoTO ? $arquivo->getSt_nomearquivo() . '.' . $arquivo->getSt_extensaoarquivo() : $arquivo['name']);
			$id_upload = $this->dao->salvarImagemUpload($upload);//aqui que tenho q tratar se tinha arquivo ou nao
			//$categoria->setId_categoria($categoria->getId_categoria());
			$categoria->setId_uploadimagem($id_upload);
			$this->salvarCategoria($categoria);
			$this->dao->commit();
			$this->mensageiro->setMensageiro('Arquivo salvo com sucesso!', Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			$this->dao->rollBack();
			$this->mensageiro->setMensageiro('Erro no Upload da Imagem da Categoria: '.$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
		return $this->mensageiro;
		
	}
	
	/**
	 * Método que retorna um UploadTO da imagem da Categoria
	 * @param CategoriaTO $categoria
	 * @return Ead1_Mensageiro
	 */
	public function retornarUploadCategoria(CategoriaTO $categoria){
		try{
		
			$to = new UploadTO();
			$to->setId_upload($categoria->getId_uploadimagem());
			$to->fetch(true, true, true);
			
			if($to->getSt_upload()){
				$this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
			}else {
				$this->mensageiro->setMensageiro("Nenhuma imagem foi encontrada para a categoria!", Ead1_IMensageiro::AVISO);
			}
		
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro retornar imagem da categoria - {$e->getMessage()}", Ead1_IMensageiro::ERRO);
		}
		
		return $this->mensageiro;
	}

}
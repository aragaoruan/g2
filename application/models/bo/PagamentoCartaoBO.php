<?php

/**
 * Classe com regras de negócio para o Pagamento com cartão de Crédito Cartao
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 12/06/2012
 * @package models
 * @subpackage bo
 */
class PagamentoCartaoBO extends Ead1_BO
{

    var $entidadecartao;
    var $service;


    protected $id_meiopagamento;


    /**
     * PagamentoCartaoBO constructor.
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param $id_entidade
     * @param int $id_cartaobandeira
     * @param int $id_meiopagamento
     * @throws Zend_Exception
     */
    public function __construct($id_entidade, $id_cartaobandeira = CartaoBandeiraTO::BANDEIRA_VISA, $id_meiopagamento = MeioPagamentoTO::CARTAO)
    {

        $to = new VwEntidadeCartaoTO();

        $to->setId_entidade($id_entidade);
        $to->setId_cartaobandeira($id_cartaobandeira);
        $to->fetch(false, true, true);

        if (!$to->getId_cartaoconfig()) {
            throw new Zend_Exception('Nenhuma bandeira encontrada!');
        }

        $this->entidadecartao = $to;
        $this->setId_meiopagamento($id_meiopagamento);

//		switch ($to->getSt_gateway()) {
//			case Ead1_IPagamentoCartao::BRASPAG:
////				switch ($this->getId_meiopagamento()) {
////					case MeioPagamentoTO::RECORRENTE:
////					    /** @var \BraspagRecorrenteBO service */
////						$this->service = new BraspagRecorrenteBO($to->getId_entidade());
////					    break;
////					case MeioPagamentoTO::CARTAO:
////					default:
////					    /** @var BraspagPagadorBO service */
////						$this->service = new BraspagPagadorBO($to->getId_entidade());
////					    break;
////				}
//			    break;
//            case Ead1_IPagamentoCartao::PAGAR_ME:
//                /** @var \G2\Negocio\Pagarme service */
//                $this->service = new \G2\Negocio\Pagarme();
//                break;
//			default:
//			    throw new Zend_Exception('Nenhum gateway configurado com o comando salvo!');
//                break;
//		}
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param field_type $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * Retorna uma Venda
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param unknown_type $id_venda
     * @throws Zend_Exception
     * @return VendaTO
     */
    protected function _retornaVenda($id_venda)
    {
        try {
            $venda = new VendaTO();
            $venda->setId_venda($id_venda);
            $venda->setId_entidade($this->entidadecartao->getId_entidade());
            $venda->fetch(true, true, true);

            if (!$venda->getId_entidade()) {
                throw new Zend_Exception('Venda não encontrada!');
            }

            return $venda;
        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }


    /**
     * Cancela um pagamento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param Ead1_TO_Dinamico $transacao
     * @return Ead1_Mensageiro com a transação cancelada em caso de sucesso
     */
    public function cancelarCartao(TransacaoFinanceiraTO $transacao, VendaTO $venda)
    {

        try {

            if (!$transacao->getid_venda()) {
                throw new Zend_Exception('Obrigatório informar o ID da venda!');
            }

            return $this->service->cancelar($transacao, $venda);

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Verifica junto a Operadora do Cartão se a compra foi aprovada
     * Deve obedecer o TO correspondente ao Cartao
     * @param array $arDadoscartao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function autorizarCartao(array $arDadoscartao, $nu_cartao = 1)
    {

        $dao = new Ead1_DAO();

        try {
            $arrLancamentoTO = false;
            if (empty($arDadoscartao['id_venda'])) {
                throw new Zend_Exception('Venda não informada!');
            }
            $venda = $this->_retornaVenda($arDadoscartao['id_venda']);
            $pagBO = new PagamentoBO();
            $pagBO->verificaPossibilidadePagamento($venda->getId_venda());
            $arDadoscartao['bl_entrada'] = isset($arDadoscartao['bl_entrada']) ? $arDadoscartao['bl_entrada'] : true;

            if (isset($arDadoscartao['id_modelovenda']) && $arDadoscartao['id_modelovenda'] == \G2\Entity\ModeloVenda::ASSINATURA) {
                $dt_vencimento = $this->getDiaVencimentoAssinatura($venda->getId_venda());
                $arDadoscartao['dt_iniciorecorrencia'] = $dt_vencimento->toString("dd/MM/yyyy");
            } else {
                $arDadoscartao['dt_iniciorecorrencia'] = null;
                $dt_vencimento = null;
            }
            if (isset($arDadoscartao['id_modelovenda']) == false) {
                $arDadoscartao['id_modelovenda'] = \G2\Entity\ModeloVenda::PADRAO;
            }
            $mensageiro = $this->service->autorizar($arDadoscartao);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {

                if (is_string($mensageiro->getFirstMensagem())) {
                    $reset = $this->resetaVendaTransacao($venda->getId_venda());

                    if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                        throw new Zend_Exception('Não foi possível autorizar o pagamento junto a operadora. ');

                    throw new Zend_Exception($reset->getFirstMensagem());
                } else {
                    throw new Zend_Exception('Não foi possível autorizar o pagamento junto a operadora, pagamento negado.');
                }
            }

            $this->_procedimentoPosAutorizarCartao($arDadoscartao, $nu_cartao, true);

            if ($mensageiro->getFirstMensagem() instanceof TransacaoFinanceiraTO && $arrLancamentoTO) {

                $mensageiro->getFirstMensagem()->setid_usuariocadastro($venda->getId_usuario());
                $mensageiro->getFirstMensagem()->setid_venda($venda->getId_venda());
                $mentran = $this->salvarLancamentoTransacaoFinanceira($arrLancamentoTO, $mensageiro->getFirstMensagem());

                if ($mentran->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $reset = $this->resetaVendaTransacao($venda->getId_venda());

                    if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                        throw new Zend_Exception('Não foi possível salvar as Transações, mas o pagamento foi Aprovado, entre em contato com a Instituição!');

                    throw new Zend_Exception($reset->getFirstMensagem());
                }
            }


            if ($mensageiro->getTipo() == Ead1_IMensageiro::AVISO && $this->getId_meiopagamento() == MeioPagamentoTO::RECORRENTE) {
                $mensageiro->setMensagem('Pagamento Autorizado na primeira parcela. Entraremos em contato para verificação das demais.');
                $mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            } elseif ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $mensageiro->setMensagem('Pagamento Autorizado.');
            }


            return $mensageiro;
        } catch (Exception $e) {
            $dao->rollBack();
            if (isset($mensageiro) && $mensageiro instanceof Ead1_Mensageiro) {
                try {
                    if ($mensageiro->getCodigo() instanceof TransacaoFinanceiraTO) {
                        $mensageiro->getCodigo()->setid_usuariocadastro($venda->getId_usuario());
                        $mensageiro->getCodigo()->setid_venda($venda->getId_venda());

                        $mensageiro->getCodigo()->setid_cartaooperadora($arDadoscartao['id_cartaooperadora']);
                        $mensageiro->getCodigo()->setid_cartaobandeira($arDadoscartao['id_cartaobandeira']);

                        $me = $this->inserirTransacaoFinanceira($mensageiro->getCodigo());
                    }
                } catch (Exception $e2) {
                }
            }
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, (isset($mensageiro) ? isset($mensageiro) : 'Nenhum Mensageiro setado.'));
        }


    }

    /**
     * Efetua os procedimentos pos autorização do cartão
     * @param array $arDadoscartao
     * @param int $nu_cartao
     * @param bool $bl_braspag
     * @throws Exception
     */
    public function _procedimentoPosAutorizarCartao(array $arDadoscartao, $nu_cartao = 1, $bl_braspag = true)
    {

        $dao = new \Ead1_DAO();
        $pagBO = new \PagamentoBO();
        $vendaBO = new \VendaBO();

        $dao->beginTransaction();
        try {

            $venda = $this->_retornaVenda($arDadoscartao['id_venda']);
            $venda->setBl_ativo(true);

            if ($bl_braspag && $this->service instanceof BraspagRecorrenteBO) {
                $venda->setRecorrenteOrderid($venda->getId_venda() . 'G2U');
            }

            $dt_vencimento = null;
            if (isset($arDadoscartao['id_modelovenda']) && $arDadoscartao['id_modelovenda'] == \G2\Entity\ModeloVenda::ASSINATURA) {
                $dt_vencimento = $this->getDiaVencimentoAssinatura($venda->getId_venda());
            }

            if (isset($arDadoscartao['id_modelovenda']) == false) {
                $arDadoscartao['id_modelovenda'] = \G2\Entity\ModeloVenda::PADRAO;
            }

            if ($venda->getId_evolucao() == VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO || $venda->getId_evolucao() == VendaTO::EVOLUCAO_CONFIRMADA) {
                if (isset($arDadoscartao['id_lancamento']) && $arDadoscartao['id_lancamento']) {

                    $lanTO = new LancamentoTO();
                    $lanTO->setId_lancamento($arDadoscartao['id_lancamento']);
                    $lanTO->fetch(true, true, true);

                    $mevenda = $vendaBO->quitarCredito(array($lanTO));
                    if ($mevenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mevenda->getFirstMensagem());
                    }
                } else {
                    $mecredito = $vendaBO->quitarCreditoVenda($venda, $nu_cartao);
                    if ($mecredito->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mecredito->getFirstMensagem());
                    }
                }

            } else {
                if ($arDadoscartao['id_modelovenda'] == \G2\Entity\ModeloVenda::ASSINATURA) {
                    $venda->setNu_diamensalidade($dt_vencimento->toString('dd'));
                    $lancamento = $pagBO->gerarLancamentosAssinatura($venda->getId_venda(), $arDadoscartao['nu_parcelas'], ($arDadoscartao['nu_valortotal'] / 100), $this->entidadecartao->getId_cartaoconfig(), $dt_vencimento);
                    if ($lancamento->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $reset = $this->resetaVendaTransacao($venda->getId_venda());
                        if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                            throw new Zend_Exception(\G2\Constante\MensagemSistema::ERRO_CRIAR_LANCAMENTO . $lancamento->getFirstMensagem());
                        throw new Zend_Exception($reset->getFirstMensagem());
                    }
                } else {
                    $lancamento = $pagBO->gerarLancamentos($venda->getId_venda(), $arDadoscartao['nu_parcelas'], $this->getId_meiopagamento(), isset($arDadoscartao['id_cartaoconfig']) && $arDadoscartao['id_cartaoconfig'] ? $arDadoscartao['id_cartaoconfig'] : $this->entidadecartao->getId_cartaoconfig());
                    if ($lancamento->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $reset = $this->resetaVendaTransacao($venda->getId_venda());
                        if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                            throw new Zend_Exception(\G2\Constante\MensagemSistema::ERRO_CRIAR_LANCAMENTO . $lancamento->getFirstMensagem());
                        throw new Zend_Exception($reset->getFirstMensagem());
                    }
                }

                $arrLancamentoTO = $lancamento->getMensagem();
//                if ($this->getId_meiopagamento() == MeioPagamentoTO::RECORRENTE && $arDadoscartao['id_modelovenda'] != \G2\Entity\ModeloVenda::ASSINATURA) {
//                    $arrLancamentoTO[0]->setId_meiopagamento(MeioPagamentoTO::CARTAO); // a primeira é Cartão normal
//                }

                $mevenda = $vendaBO->salvarArrayLancamentos($arrLancamentoTO, $venda, true, true, null, $arDadoscartao['id_modelovenda']);

                if ($mevenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $reset = $this->resetaVendaTransacao($venda->getId_venda());
                    if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                        throw new Zend_Exception(\G2\Constante\MensagemSistema::ERRO_CRIAR_LANCAMENTO . $mevenda->getFirstMensagem());
                    throw new Zend_Exception($reset->getFirstMensagem());

                } else {
                    $meprev = $vendaBO->salvaPrevisaoPagamentoLancamento($arrLancamentoTO);
                    if ($meprev->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $reset = $this->resetaVendaTransacao($venda->getId_venda());
                        if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO)
                            throw new Zend_Exception(\G2\Constante\MensagemSistema::ERRO_CRIAR_LANCAMENTO . $meprev->getFirstMensagem());
                        throw new Zend_Exception($reset->getFirstMensagem());

                    }
                }

                $venda->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            }

            $vendaBO->editarVenda($venda);


//            try {
//
//                $viTO = new VendaIntegracaoTO();
//                $viTO->setId_venda($venda->getId_venda());
//                $viTO->fetch(false, true, true);
//                if ($viTO->getId_vendaintegracao()) {
//                    $result = AtualizaFonteLojaWebServices::atualizaStatusFonte($viTO, AtualizaFonteLojaWebServices::STATUS_APROVADA);
//                    if ($result->getTipo() != Ead1_IMensageiro::SUCESSO) {
//                        throw new Exception($result->getText());
//                    }
//                }
//
//            } catch (Exception $e) {
//                throw $e;
//            }
            /** Comitamos tudo para garantir o salvamento **/
            $dao->commit();

        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
        }
    }

    /**
     * Edita a venda para aguardando recebimento ou para qualquer evolução que for passada e a transacao para status null,
     * caso haja falha na geração dos lançamentos, para que o robo que verifica os pagamentos do cartão
     * possa tentar novamente criar os lançamentos.
     * @param integer $id_venda
     * @param null $evolucao
     * @return Ead1_Mensageiro
     */
    public function resetaVendaTransacao($id_venda, $evolucao = null)
    {

        try {

            $negocio = new \G2\Negocio\Negocio();

            $venda = $negocio->find('\G2\Entity\Venda', $id_venda);
            if ($venda) {
                $venda->setId_evolucao($negocio->getReference('\G2\Entity\Evolucao', $evolucao ? $evolucao : \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO));
                $negocio->save($venda);
            }

            $transacao = $negocio->findOneBy('\G2\Entity\TransacaoFinanceira', array('id_venda' => $id_venda));
            if ($transacao) {
                $transacao->setNuStatus(null);
                $negocio->save($transacao);
            }

            return new Ead1_Mensageiro(\G2\Constante\MensagemSistema::SUCESSO_RESETAR_TRANSACAO_FINANCEIRA, Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new Ead1_Mensageiro(\G2\Constante\MensagemSistema::ERRO_RESETAR_TRANSACAO_FINANCEIRA, Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Verifica um pagamento e faz a quintação do débito da venda
     * @param TransacaoFinanceiraTO $to
     * @return Ead1_Mensageiro
     */
    public function verificarPagamento(TransacaoFinanceiraTO $to)
    {

        try {

            $vendaBO = new VendaBO();
            $pagBO = new PagamentoBO();
            if ($this->service instanceof BraspagRecorrenteBO) {
                throw new Zend_Exception("Para verificar o pagamendo recorrente, use diretamente o BraspagRecorrenteBO::verificar()");
            }
            $tra = new TransacaoFinanceiraBO();

            $mensageiro = $this->service->verificar($to);
            $order = $mensageiro->getFirstMensagem();
            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {

                if (isset($order['Status']))
                    $to->setNu_status($order['Status']);


                $to->setNu_verificacao($to->getNu_verificacao() + 1);
                $metran = $tra->editarTransacaoFinanceira($to);
                if ($metran->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception($metran->getFirstMensagem());
                }

                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            if ($order['Status'] == 0) {
                $venda = new VendaTO();
                $venda->setId_venda($to->getId_venda());
                $venda->fetch(true, true, true);
                $mecredito = $vendaBO->quitarCreditoVenda($venda);
                if ($mecredito->getTipo() != Ead1_IMensageiro::SUCESSO) {

                    if ($mecredito->getCodigo() == 'SEMLANCAMENTO') {

                        if (!$venda->getId_formapagamento()) {

                            $vw = new VwFormaMeioPagamentoTO();
                            $vw->setId_meiopagamento(MeioPagamentoTO::CARTAO);
                            $vw->setId_entidade($venda->getId_entidade());
                            $vw->setId_formapagamentoaplicacao(2);
                            $vw->fetch(false, true, true);

                            if ($vw->getId_formapagamento()) {
                                $venda->setId_formapagamento($vw->getId_formapagamento());
                                $me = $vendaBO->editarVenda($venda);
                                if ($me->getTipo() != Ead1_IMensageiro::SUCESSO) {
                                    throw new Zend_Exception("Não foi possível salvar a Forma de Pagamento.");
                                }
                            } else {
                                throw new Zend_Exception("Não foi possível encontrar a Forma de Pagamento.");
                            }

                        }


                        $lancamento = $pagBO->gerarLancamentos($venda->getId_venda(), $order['NumberOfPayments'], $this->getId_meiopagamento(), $this->entidadecartao->getId_cartaoconfig());
                        if ($lancamento->getTipo() != Ead1_IMensageiro::SUCESSO) {
                            $reset = $this->resetaVendaTransacao($venda->getId_venda());
                            if ($reset->getTipo() == Ead1_IMensageiro::SUCESSO) {
                                throw new Zend_Exception('Não foi possível criar os lançamentos! ' . $lancamento->getFirstMensagem());
                            } else {
                                throw new Zend_Exception($reset->getFirstMensagem());
                            }
                        }
                        $arrLancamentoTO = $lancamento->getMensagem();
                        $mevenda = $vendaBO->salvarArrayLancamentos($arrLancamentoTO, $venda, true, true, null);
                        if ($mevenda->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            $mecredito = $vendaBO->quitarCreditoVenda($venda);
                            if ($mecredito->getTipo() != Ead1_IMensageiro::SUCESSO) {
                                throw new Zend_Exception("NAO CRIOU OS LANCAMENTOS PELO ROBO - " . $mecredito->getFirstMensagem());
                            }
                        } else {
                            throw new Zend_Exception($mevenda->getFirstMensagem());
                        }

                    } else {
                        throw new Zend_Exception($mecredito->getFirstMensagem());
                    }

                }
            }

            if (isset($order['Status']))
                $to->setNu_status($order['Status']);

            $to->setNu_verificacao($to->getNu_verificacao() + 1);
            $metran = $tra->editarTransacaoFinanceira($to);
            if ($metran->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($metran->getFirstMensagem());
            }

            return new Ead1_Mensageiro("Venda {$to->getId_venda()} Pagamento verificado com sucesso - " . $mensageiro->getCodigo(), Ead1_IMensageiro::SUCESSO, $mensageiro);


        } catch (Exception $e) {
            return new Ead1_Mensageiro("Venda {$to->getId_venda()} Erro ao verificar o pagamento: " . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }


    /**
     * Salva a transação financeira
     * Este método foi passado apra TransacaoFinanceiraBO, sendo aqui apenas chamado de lá.
     * @param TransacaoFinanceiraTO $tranTO
     * @return Ead1_Mensageiro
     */
    public function inserirTransacaoFinanceira(TransacaoFinanceiraTO $tranTO)
    {
        $tra = new TransacaoFinanceiraBO();

        $mensageiro = $tra->salvarTransacaoFinanceira($tranTO);
        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            return new Ead1_Mensageiro('Não foi possível salvar a Transação Financeira.', Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }


    /**
     * Salva a Transação Financeira no Banco de Dados
     * Este método foi passado apra TransacaoFinanceiraBO, sendo aqui apenas chamado de lá.
     * @param array $arrLanTO - array de LancamentoTO
     * @param TransacaoFinanceiraTO $tranTO
     */
    public function salvarLancamentoTransacaoFinanceira(array $arrLanTO, TransacaoFinanceiraTO $tranTO)
    {


        $tra = new TransacaoFinanceiraBO();
        $mensageiro = $tra->salvarLancamentoTransacaoFinanceira($arrLanTO, $tranTO);
        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
            return new Ead1_Mensageiro('Não foi possível salvar a Transação Financeira.', Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Método que retorna o modelo de venda, só precisamos do primeiro produto da venda para isso.
     * @param unknown_type $id_venda
     * @return Ambigous <the, field_type>
     */
    public function retornaModeloVenda($id_venda)
    {
        $pv = new VwProdutoVendaTO();
        $pv->setId_venda($id_venda);
        $pv->fetch(false, true, true);
        return $pv->getId_modelovenda();
    }

    /**
     * Retorna a data de vencimento das parcelas de uma assintatura
     * @param int $id_venda
     * @return Zend_Date
     */
    public function getDiaVencimentoAssinatura($id_venda)
    {

        $venda = new VendaTO();
        $venda->setId_venda($id_venda);
        $venda->fetch(true, true, true);

        $dt_vencimento = new Zend_Date();
        if ($venda->getId_campanhacomercial()) {
            $cm = new CampanhaComercialTO();
            $cm->setId_campanhacomercial($venda->getId_campanhacomercial());
            $cm->fetch(true, true, true);
            $dt_vencimento->addDay($cm->getNu_diasprazo());
        }

        if ($dt_vencimento->toString('dd') >= 28) {
            $dt_vencimento->addMonth(1);
            $dt_vencimento->setDay(1);
        }

        return $dt_vencimento;
    }

}

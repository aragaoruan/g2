<?php
/**
 * Classe que encapsula regra de negócio de Mensagem Motivacional
 * @author Domício de A. Medeiros - domicio.medeiros@gmail.com
 */
class MensagemMotivacionalBO extends Ead1_BO{
	
	private $dao;
	
	/**
	 * Método construtor
	 */
	public function __construct(){
		$this->dao = new MensagemMotivacionalDAO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método responsável por verificar se a mensagem motivacional está em branco (null).
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return boolean
	 */
	public function validaMensagemMotivacional(MensagemMotivacionalTO $msgMotTO){
		return $msgMotTO->getSt_mensagemmotivacional() ? true : false;
	}
	
	/**
	 * Método responsável por cadastrar Mensagem Motivacional.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastraMensagemMotivacional(MensagemMotivacionalTO $msgMotTO){
		try {
			if(!$this->validaMensagemMotivacional($msgMotTO)) {
				$this->mensageiro->setMensageiro("Mensagem motivacinal obrigatória.", Ead1_IMensageiro::AVISO);
			}else {
				$id_mensagemgravada = $this->dao->cadastraMensagemMotivacional($msgMotTO);
				$msgMotTO->setId_mensagemmotivacional($id_mensagemgravada);
				$this->mensageiro->setMensageiro($msgMotTO, Ead1_IMensageiro::SUCESSO);
			}
		} catch (Zend_Exception $e) {
			$this->mensageiro->setMensageiro("Erro ao cadastrar mensagem motivacional", Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método responsável por retornar Mensagem Motivacional.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @throws Zend_Validate_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornaMensagemMotivacional(MensagemMotivacionalTO $msgMotTO){
		try {
			$dados = $this->dao->retornaMensagemMotivacional($msgMotTO)->toArray();
			if(empty($dados)){
				THROW new Zend_Validate_Exception("Nenhum Registro Encontrado!");
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new MensagemMotivacionalTO()), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Validate_Exception $e) {
			$this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
		} catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro("Erro ao retornar mensagem motivacional.", Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método para retornar mensagens aleatoriamente
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaMensagemMotivacionalAleatoria(MensagemMotivacionalTO $msgMotTO) {
		try{
			$dados = $this->dao->retornaMensagemMotivacionalAleatoria($msgMotTO)->toArray();
			if(empty($dados)) {
				$msgMotTO->setSt_mensagemmotivacional("");
				return $this->mensageiro->setMensageiro($msgMotTO, Ead1_IMensageiro::AVISO);
			}
			
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new MensagemMotivacionalTO()), Ead1_IMensageiro::SUCESSO);	
		} catch (Zend_Exception $e) {
			return $this->mensageiro->setMensageiro("Erro ao retornar mensagem motivacional", Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		
		
	}
	
	/**
	 * Método responsável editar Mensagem Motivacional.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @throws Zend_Validate_Exception
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function editarMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		try {
			if(!$this->validaMensagemMotivacional($msgMotTO)) {
				THROW new Zend_Validate_Exception("Mensagem motivacional obrigatória.");
			}
			if(!$msgMotTO->getId_mensagemmotivacional()) {
				THROW new Zend_Exception("O id_mensagemmotivacional não foi setado!");
			}
			$this->dao->editarMensagemMotivacional($msgMotTO);
			$this->mensageiro->setMensageiro("Mensagem editada com sucesso!", Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->mensageiro->setMensageiro("Erro ao editar mensagem motivacional", Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método responsável por excluir Mensagem Motivacional.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		try {
			$this->dao->excluirMensagemMotivacional($msgMotTO);
			$this->mensageiro->setMensageiro("Mensagem Motivacional Excluída com Sucesso!", Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->mensageiro->setMensageiro("Erro ao Excluir Mensagem Motivacional!", Ead1_IMensageiro::ERRO, $e->getMessage());
		}
		return $this->mensageiro;
	}
	
	/**
	 * Método que verifica se mensagem salvar deve ser cadastrada ou editada.
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return @return Ead1_Mensageiro
	 */
	public function salvarMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		if($msgMotTO->getId_mensagemmotivacional()){
			return $this->editarMensagemMotivacional($msgMotTO);
		}
		return $this->cadastraMensagemMotivacional($msgMotTO);
	}
}
?>
<?php

/**
 * Classe com regras de negocio para interacoes de Pessoa em DadosAcesso
 * @author Felipe Pastor
 * @since 13/03/2013
 *
 * @package models
 * @subpackage bo
 */
class DadosAcessoBO extends Ead1_BO
{

    public $mensageiro;
    private $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new PessoaDAO();
    }


    public function verificarDadosDeAcesso(DadosAcessoTO $to, array $entidades_pessoa = null, array $entidades_dados_acesso = null)
    {
        try {
            $entidades_ids = array();
            $entidades_ids_dados = array();

            if ($entidades_pessoa != null) {
                foreach ($entidades_pessoa as $e) {
                    $entidades_ids[] = $e['id_entidade'];
                }
            }

            if ($entidades_dados_acesso != null) {
                foreach ($entidades_dados_acesso as $e) {
                    $entidades_ids_dados[] = $e['id_entidade'];
                }
            }

            $dif_entidades = array_diff(array_unique($entidades_ids), array_unique($entidades_ids_dados));
            if (sizeof($dif_entidades) > 0) {
                $this->dao->atualizarDadosAcesso($to, $dif_entidades);
                return true;
            }

            return false;

        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * @param DadosAcessoTO $dTO
     * @return boolean:
     */
    public function retornarDadosAcesso(DadosAcessoTO $dTO)
    {
        $this->dao = new PessoaDAO();
        $pessoa = $this->dao->retornarDadosAcesso($dTO);
        if (!$pessoa) {
            return false;
        } else {
            return $pessoa->toArray();
        }
    }
}
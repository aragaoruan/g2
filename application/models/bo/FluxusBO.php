<?php

/**
 * Classe para integrações com o sistema Fluxus
 * @author edermariano
 *
 */
class FluxusBO extends Ead1_BO {

    /**
     * @var String $linkedServer Servidor que contém a integração
     */
    private $linkedServer;

    /**
     * @var String $linkedDb Database a ser utilizada
     */
    private $linkedDb;

    /**
     * @var mixed Array de respostas da integração
     */
    private $dadosIntegracao;

    /**
     * @var mixed Tipo da resposta da integração
     */
    private $respostaIntegracao;

    /**
     * @var FluxusDAO
     */
    private $dao;

    /**
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /*
     * @var VendaBO
     */
    private $vendaBO;

    /**
     * Método Construtor da classe
     */
    public function __construct() {
        parent::__construct();
        $this->linkedServer = Ead1_Ambiente::geral()->fluxusIP;
        $this->linkedDb = Ead1_Ambiente::geral()->fluxusDB;
        $this->dao = new FluxusDAO();
        $this->mensageiro = new Ead1_Mensageiro();
        $this->vendaBO = new VendaBO();
    }

    /**
     * Método que integra automaticamente lançamentos não integrados de acordo com a data pré-estabelecida
     * @param Zend_Date $dataInicio
     * @param Zend_Date $dataFim
     */
    public function integrarAutomaticamente(Zend_Date $dataInicio, Zend_Date $dataFim = null) {
        // TODO Buscar lançamentos não integrados a partir da $dataInicio até $dataFim se houver e integrar.
    }

    /**
     * Método que busca os lançamentos de acordo com a venda
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    public function integrar(VendaTO $vendaTO) {
        try {
            $lancamentos = new LancamentoTO();
            $lancamentos = $this->buscarLancamentos($vendaTO);
            if ($lancamentos->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($lancamentos->getMensagem() as $vwIntegraGestorFluxusTO) {
                    $this->spIntegrar($vwIntegraGestorFluxusTO);
                }
            } else {
                throw new Zend_Exception('Lançamentos não encontrados, não foi possível integrar com o Fluxus.');
            }
        } catch (Exception $e) {
            throw $e;
        }

        return new Ead1_Mensageiro($this->dadosIntegracao, $this->respostaIntegracao);
    }

    /**
     * Método que busca os lançamentos de acordo com a venda
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    private function buscarLancamentos(VendaTO $vendaTO) {
        $dao = new FluxusDAO();
        return $dao->dadosLancamentosIntegracao($vendaTO);
    }

    public function cadastrarLancamentoIntegracao(LancamentoIntegracaoTO $lancamentoIntegracaoTO) {
        try {
            $dao = new FluxusDAO();
            $id_lancamentointegracao = $dao->cadastrarLancamentoIntegracao($lancamentoIntegracaoTO);
            $lancamentoIntegracaoTO->setId_lancamentointegracao($id_lancamentointegracao);
            return $this->mensageiro->setMensageiro($lancamentoIntegracaoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao integrar lançamento', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que invoca a SP da integração via Linked Server
     * @param LancamentoTO $lancamento
     * @return Ead1_Mensageiro
     */
    private function spIntegrar(Totvs_VwIntegraGestorFluxusTO $vwIntegraGestorFluxusTO) {
        $dao = new FluxusDAO();
        return $dao->spIntegracao($vwIntegraGestorFluxusTO, $vwIntegraGestorFluxusTO->getSt_linkedserver(), $vwIntegraGestorFluxusTO->getSt_caminho());
    }

    /**
     * Método que verifica os dados retornados da integração com o fluxus
     * @param Ead1_Mensageiro $dados
     */
    private function verificarIntegracao(Ead1_Mensageiro $dados) {
        if ($dados->getTipo() == Ead1_IMensageiro::SUCESSO) {
            return $dados;
        } else {
            $this->dadosIntegracao[] = $dados->getMensagem();
            $this->respostaIntegracao = $dados->getTipo();
        }
    }

    /**
     * No processo de integração com o sistema RM - Fluxus da TOTVS
     * 	é necessário informar o código da coligada e o centro de custo
     * 	que definem a entidade no Gestor2. Esses códigos serão encontrados
     * 	na tabela (tb_entidadeintegracao) onde st_codsistema representa a COLIGADA e 
     * 	st_codchave representa o centro de custo da entidade.
     * 
     * @return Ead1_Mensageiro
     */
    private function trataColigadaCentroCustoContaCaixa(LancamentoTO $lancamentoTO) {
        $entidadeIntegracaoTO = new EntidadeIntegracaoTO();
        $entidadeIntegracaoTO->setId_sistema(SistemaTO::FLUXUS);
        $entidadeIntegracaoTO->setId_entidade($lancamentoTO->getId_entidade());

        $entidadeIntegracaoORM = new EntidadeIntegracaoORM();
        $dadosFluxus = $entidadeIntegracaoORM->consulta($entidadeIntegracaoTO);
        if (!$dadosFluxus) {
            $entidadeLabel = Ead1_LabelFuncionalidade::getLabel(3);
            return new Ead1_Mensageiro("Não foram encontrados dados de integração com o Fluxus para a $entidadeLabel em questão!", Ead1_IMensageiro::ERRO);
        } else {
            // TODO Validações para Conta Caixa, Centro de Custo e Conta Caixa
        }
    }

    /**
     * Método que atualiza um Lançamento integrado no Fluxus.
     * A partir de um LancamentoTO, o método busca uma integração salva na tb_lancamentointegracao,
     * caso exista ele prossegue com a atualização.
     *
     * @author Rafael Costa Rocha rafael.rocha.mg@gmail.com
     * @param LancamentoTO $lancamentoTO
     * @return Ead1_Mensageiro
     * @since 07/12/2012
     */
    public function atualizarLancamentoFluxus(LancamentoTO $lancamentoTO) {

        $this->dao->beginTransaction();
        try {
            $lancamentoIntegracao = new LancamentoIntegracaoTO();
            $lancamentoIntegracao->setId_lancamento($lancamentoTO->getId_lancamento());
            $lancamentoIntegracao->setId_sistema(SistemaTO::FLUXUS);
            $lancamentoIntegracao->fetch(true, true, true);

            if ($lancamentoIntegracao->getSt_codlancamento()) {

                $lancamentoVenda = new LancamentoVendaTO();
                $lancamentoVenda->setId_lancamento($lancamentoIntegracao->getId_lancamento());
                $lancamentoVenda->fetch(true, true, true);

                $entidadeIntegracao = new EntidadeIntegracaoTO();
                $entidadeIntegracao->setId_entidade($lancamentoTO->getSessao()->id_entidade);
                $entidadeIntegracao->setId_sistema(SistemaTO::FLUXUS);
                $entidadeIntegracao->fetch(false, true, true);

                $vwAtualizarFluxusGestorTO = new Totvs_VwAtualizarFluxusGestorTO();
                $vwAtualizarFluxusGestorTO->setIDLAN($lancamentoIntegracao->getSt_codlancamento());
                $vwAtualizarFluxusGestorTO->setId_venda($lancamentoVenda->getId_venda());
                $vwAtualizarFluxusGestorTO->setCODCOLIGADA($entidadeIntegracao->getSt_codsistema());
                $vwAtualizarFluxusGestorTO->setSTATUSLAN(1);
                $vwAtualizarFluxusGestorTO->setDATA_BAIXA($lancamentoTO->getDt_quitado());
                $vwAtualizarFluxusGestorTO->setVALOR_BAIXADO($lancamentoTO->getNu_quitado());

                $vwAtualizarFluxusGestorTO->setId_lancamento($lancamentoTO->getId_lancamento());

                $mensageiro = $this->atualizar($vwAtualizarFluxusGestorTO);
                if ($mensageiro->getTipo() === Ead1_IMensageiro::SUCESSO) {
                    $lancamentoIntegracao->setDt_sincronizado(new Zend_Date(null));
                    $this->editarLancamentoIntegracao($lancamentoIntegracao);
                    $this->dao->commit();
                    return $mensageiro;
                } else {
                    throw new Zend_Exception('Erro ao atualizar registro no Fluxus - IDLAN(' . $lancamentoIntegracao->getSt_codlancamento() . ')');
                }
            } else {
                $this->dao->commit();
                return $this->mensageiro->setMensageiro("O Lançamento não está integrado!", Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            $this->dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que atualiza um lançamento integrado ao Fluxus
     * @param Totvs_VwAtualizarFluxusGestorTO $vwAtualizarFluxusGestorTO
     * @return Ead1_Mensageiro
     * @author Rafael Costa Rocha rafael.rocha.mg@gmail.com
     * @see VendaBO::
     */
    public function atualizar(Totvs_VwAtualizarFluxusGestorTO $vwAtualizarFluxusGestorTO) {
        try {

// 			$lancamentoIntegracao = new LancamentoIntegracaoTO();
// 			$lancamentoIntegracao->setId_lancamento($vwAtualizarFluxusGestorTO->getId_lancamento());
// 			$lancamentoIntegracao->setId_sistema(SistemaTO::FLUXUS);
// 			$lancamentoIntegracao->fetch(true,true,true);

            if ($vwAtualizarFluxusGestorTO->getIDLAN()) {
                return $this->spAtualizarFluxus($vwAtualizarFluxusGestorTO);
            } else {
                return new Ead1_Mensageiro('Integração não encontrada, não foi possível atualizar lançamento no Fluxus.', Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            return new Ead1_Mensageiro("Erro ao Atualizar Lançamento!", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return new Ead1_Mensageiro("Atualizado com Sucesso!", Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que invoca a SP da atualização via Linked Server
     * @param Totvs_VwAtualizarFluxusGestorTO $vwAtualizarFluxusGestorTO
     * @return Ead1_Mensageiro
     * @author Rafael Costa Rocha rafael.rocha.mg@gmail.com
     */
    private function spAtualizarFluxus(Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO) {

        if ($vwAtualizarGestorFluxusTO->getIDLAN() && $vwAtualizarGestorFluxusTO->getId_venda() &&
                $vwAtualizarGestorFluxusTO->getCODCOLIGADA() && $vwAtualizarGestorFluxusTO->getSTATUSLAN() &&
                $vwAtualizarGestorFluxusTO->getDATA_BAIXA() && $vwAtualizarGestorFluxusTO->getVALOR_BAIXADO()) {
            //passar como 3º parametro o nome do db
            return $this->dao->spAtualizaFluxus($vwAtualizarGestorFluxusTO, "[189.114.56.180]", "CorporeRM");
        } else {
            return new Ead1_Mensageiro("Erro: Os Parâmetros IDLAN, id_venda, COLIGADA, STATUSLAN, DATA_BAIXA e VALOR_BAIXADO são obrigatórios.", Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que chama o método de edição de um LacamentoIntegracaoTO do FluxusDao
     * @param LancamentoIntegracaoTO $to
     * @param Mixed $where
     * @return Ead1_Mensageiro $mensageiro
     * @author Rafael Rocha rafael.rocha.mg@gmail.com
     */
    public function editarLancamentoIntegracao(LancamentoIntegracaoTO $to, $where = null) {
        $dao = new FluxusDAO();
        try {
            $dao->beginTransaction();
            $dao->editarLancamentoIntegracao($to);
            $dao->commit();
            return new Ead1_Mensageiro("Lancamento Integração editado com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {

            $dao->rollBack();
            return new Ead1_Mensageiro("Erro ao editar Lancamento Integraçao!", Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que atualiza/sincroniza lançamentos entre o G2 e o Fluxus que ainda não foram atualizados
     * @return Ead1_Mensageiro $mensageiro
     * @author Rafael Rocha - rafael.rocha.mg@gmail.com
     */
    public function sincronizarLancamentosIntegracao() {
        $dao = new FluxusDAO();
        try {
            $dao->beginTransaction();
            $vwLancamentoAATualizar = new Totvs_VwLancamentosAAtualizarTO();
            $vwLancamentoAATualizar->fetch(false, false, true);

            foreach ($vwLancamentoAATualizar as $lancamentoAAtualizar) {
                $lancamentoTO = new LancamentoTO();
                $lancamentoTO->setId_lancamento($lancamentoAAtualizar->getId_lancamento());
                $lancamentoTO->fetch(true, true, true);

                if ($lancamentoAAtualizar->getSt_codlancamento()) {
                    $this->atualizarLancamentoFluxus($lancamentoTO);
                }
            }
            $dao->commit();
            return new Ead1_Mensageiro("Lancamentos da Integração sincronizados com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro("Erro ao sincronizar lancamentos da integração!", Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Método que invoca a SP da atualização via Linked Server
     * @param Totvs_VwAtualizarFluxusGestorTO $vwAtualizarFluxusGestorTO
     * @return Ead1_Mensageiro
     * @author Gabriel Resende gabriel.resende@unyleya.com.br
     */
    public function spAtualizaFlagFluxus(Totvs_VwAtualizarFluxusGestorTO $vwAtualizarGestorFluxusTO) {

        if ($vwAtualizarGestorFluxusTO->getIDLAN() && $vwAtualizarGestorFluxusTO->getCONTRATO()) {
            //passar como 3º parametro o nome do db
            return $this->dao->spAtualizaFlagFluxus($vwAtualizarGestorFluxusTO, "[189.114.56.180]", "CorporeRM_Int_ACt");
        } else {
            return new Ead1_Mensageiro("Erro: Os Parâmetros IDLAN, CONTRATO são obrigatórios.", Ead1_IMensageiro::ERRO);
        }
    }

    public function sincronizarDadosFluxusGestor($contrato = null) {
        //ini_set('display_errors', 1);
        ini_set('max_execution_time', 240);
        $vwSincronizaFluxusG2TO = new Totvs_VwSincronizaFluxusG2TO();
        $vwSincronizaFluxusG2TO->setSTATUSATUALIZACAO(1);
        $vwSincronizaFluxusG2TO->setCONTRATO($contrato);
        $lancamentosSicronizar = $this->dao->retornarVwSincronizaFluxusG2($vwSincronizaFluxusG2TO);
        //$lancamentosSicronizar = $vwSincronizaFluxusG2TO->fetch(false, false, false, false, false, 'IDLAN');

        $arrayMensageiros = array();
        $arrayVendas = array();

        foreach ($lancamentosSicronizar as $lancamentoSincronizar) {
            $this->dao->beginTransaction();

            $vwRetornarLancamento = new Totvs_VwSincronizaListaLancamentoFluxusG2TO();
            $vwRetornarLancamento->setId_venda(str_replace('G2U', '', $lancamentoSincronizar->getCONTRATO()));
            $vwRetornarLancamento->setSt_codlancamento($lancamentoSincronizar->getIDLAN());
            $vwRetornarLancamento->fetch(false, true, true);
            $vwSincronizaAcordoFluxusG2TO = new Totvs_VwSincronizaAcordoFluxusG2TO();
            $vwSincronizaAcordoFluxusG2TO->setCodcoligada($lancamentoSincronizar->getCODCOLIGADA());
            $vwSincronizaAcordoFluxusG2TO->setIdlan($lancamentoSincronizar->getIDLAN());
            $vwSincronizaAcordoFluxusG2TO->fetch(false, true, true);

            $venda = new VendaTO();
            $venda->setId_venda(str_replace('G2U', '', $lancamentoSincronizar->getCONTRATO()));
            $venda->fetch(true, true, true);

            //Atualizacao do Lancamento
            if ($vwRetornarLancamento->getId_lancamento()) {

                $lancamentoIntegracao = new LancamentoIntegracaoTO();
                $lancamentoIntegracao->setId_lancamento($vwRetornarLancamento->getId_lancamento());
                $lancamentoIntegracao->setSt_codlancamento($lancamentoSincronizar->getIDLAN());
                $lancamentoIntegracao->fetch(false, true, true);

                if ($lancamentoIntegracao->getId_lancamentointegracao()) {

                    $lancamentoIntegracao->setSt_codresponsavel($lancamentoSincronizar->getCODCFO());
                    $lancamentoIntegracao->setDt_cadastro($lancamentoSincronizar->getDATA_EMISSAO());
                    $lancamentoIntegracao->setDt_sincronizado($lancamentoSincronizar->getDATA_ALTERACAO());

                    //$mensageiro = $this->dao->editarLancamentoIntegracao($lancamentoIntegracao);
                    Zend_Debug::dump($lancamentoIntegracao, __CLASS__ . '(' . __LINE__ . ') LancamentoIntegracaoTO INSERÇÃO');
                    $mensageiro = $this->dao->editarLancamentoIntegracao($lancamentoIntegracao);
                    Zend_Debug::dump($mensageiro, __CLASS__ . '(' . __LINE__ . ') Mensageiro após salvar LancamentoIntegracaoTO INSERÇÃO');

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $arrayMensageiros[] = $mensageiro;
                        $this->dao->rollBack();
                        continue;
                    }
                } else {
                    $lancamentoIntegracao->setSt_codresponsavel($lancamentoSincronizar->getCODCFO());
                    $lancamentoIntegracao->setDt_cadastro($lancamentoSincronizar->getDATA_EMISSAO());
                    $lancamentoIntegracao->setDt_sincronizado($lancamentoSincronizar->getDATA_ALTERACAO());
                    $lancamentoIntegracao->setId_entidade($venda->getId_entidade());
                    $lancamentoIntegracao->setId_lancamento($vwRetornarLancamento->getId_lancamento());
                    $lancamentoIntegracao->setId_sistema(SistemaTO::FLUXUS);
                    $lancamentoIntegracao->setId_usuariocadastro($vwRetornarLancamento->getId_usuariocadastro());
                    $lancamentoIntegracao->setSt_codlancamento($lancamentoSincronizar->getIDLAN());

                    $mensageiro = $this->dao->cadastrarLancamentoIntegracao($lancamentoIntegracao);

                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        $arrayMensageiros[] = $mensageiro;
                        $this->dao->rollBack();
                        continue;
                    }
                }

                $lancamento = new LancamentoTO();
                $lancamento->setId_lancamento($vwRetornarLancamento->getId_lancamento());
                $lancamento->fetch(true, true, true);

                //Informacoes Tratadas no PHP                
                $lancamento->setId_tipolancamento($lancamentoSincronizar->getTIPO_LANCAMENTO());
                $lancamento->setId_usuariolancamento($lancamentoSincronizar->getId_usuariolancamento());
                $lancamento->setId_usuariocadastro($lancamentoSincronizar->getId_usuariocadastro());

                if (in_array($lancamentoSincronizar->getSTATUSLAN(), array(1, 3))) {
                    $lancamento->setBl_quitado(1);
                } else {
                    $lancamento->setBl_quitado(0);
                }

                if (in_array($lancamentoSincronizar->getSTATUSLAN(), array(0, 1))) {
                    $lancamento->setBl_ativo(1);
                } else {
                    $lancamento->setBl_ativo(0);
                }

                //Informacoes Vindas da Vw
                $lancamento->setNu_valor($lancamentoSincronizar->getVALOR_ORIGINAL());
                $lancamento->setDt_cadastro($lancamentoSincronizar->getDATA_EMISSAO());
                $lancamento->setId_meiopagamento($lancamentoSincronizar->getId_meiopagamento());
                $lancamento->setId_entidade($venda->getId_entidade());
                $lancamento->setSt_banco($lancamentoSincronizar->getCODIGO_BANCO_EXTRATO());
                $lancamento->setDt_vencimento($lancamentoSincronizar->getDATA_VENCIMENTO());
                $lancamento->setDt_emissao($lancamentoSincronizar->getDATA_EMISSAO());
                $lancamento->setNu_quitado($lancamentoSincronizar->getVALOR_BAIXADO());
                $lancamento->setDt_atualizado($lancamentoSincronizar->getDATA_ALTERACAO());
                $lancamento->setId_acordo($vwSincronizaAcordoFluxusG2TO->getIdacordo());
                $lancamento->setSt_agencia($lancamentoSincronizar->getCODIGO_AGENCIA_EXTRATO());
                $lancamento->setSt_numcheque($lancamentoSincronizar->getNUMERO_CHEQUE_EXTRATO());
                $lancamento->setSt_nossonumero($lancamentoSincronizar->getNOSSO_NUMERO());
                $lancamento->setSt_codconta($lancamentoSincronizar->getCONTA_CAIXA());
                $lancamento->setId_codcoligada($lancamentoSincronizar->getCODCOLIGADA());
                $lancamento->setSt_statuslan($lancamentoSincronizar->getSTATUSLAN() . "");
                $lancamento->setDt_vencimentocheque($lancamentoSincronizar->getDATA_VENCIMENTO_CHEQUE());

                $lancamento->setDt_quitado($lancamentoSincronizar->getDATA_BAIXA());

                $mensageiro = $this->vendaBO->editarLancamento($lancamento, null, true);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $arrayMensageiros[] = $mensageiro;
                    $this->dao->rollBack();
                    continue;
                }
            } else {
                //Insert
                $lancamento = new LancamentoTO();

                //Informacoes Tratadas no PHP                
                $lancamento->setId_tipolancamento(1);
                $lancamento->setId_usuariolancamento($venda->getId_usuario());
                $lancamento->setId_usuariocadastro($venda->getId_usuariocadastro());
                $lancamento->setId_entidade($venda->getId_entidade());

                if (in_array($lancamentoSincronizar->getSTATUSLAN(), array(1, 3))) {
                    $lancamento->setBl_quitado(1);
                } else {
                    $lancamento->setBl_quitado(0);
                }

                if (in_array($lancamentoSincronizar->getSTATUSLAN(), array(0, 1))) {
                    $lancamento->setBl_ativo(1);
                } else {
                    $lancamento->setBl_ativo(0);
                }

                //Informacoes Vindas da Vw
                $lancamento->setNu_valor($lancamentoSincronizar->getVALOR_ORIGINAL());
                $lancamento->setDt_cadastro($lancamentoSincronizar->getDATA_EMISSAO());
                $lancamento->setId_meiopagamento($lancamentoSincronizar->getId_meiopagamento());
                $lancamento->setSt_banco($lancamentoSincronizar->getCODIGO_BANCO_EXTRATO());
                $lancamento->setDt_vencimento($lancamentoSincronizar->getDATA_VENCIMENTO());
                $lancamento->setDt_emissao($lancamentoSincronizar->getDATA_EMISSAO());
                $lancamento->setNu_quitado($lancamentoSincronizar->getVALOR_BAIXADO());
                $lancamento->setDt_atualizado($lancamentoSincronizar->getDATA_ALTERACAO());
                $lancamento->setId_acordo($vwSincronizaAcordoFluxusG2TO->getIdacordo());
                $lancamento->setSt_agencia($lancamentoSincronizar->getCODIGO_AGENCIA_EXTRATO());
                $lancamento->setSt_numcheque($lancamentoSincronizar->getNUMERO_CHEQUE_EXTRATO());
                $lancamento->setSt_nossonumero($lancamentoSincronizar->getNOSSO_NUMERO());
                $lancamento->setSt_codconta($lancamentoSincronizar->getCONTA_CAIXA());
                $lancamento->setId_codcoligada($lancamentoSincronizar->getCODCOLIGADA());
                $lancamento->setSt_statuslan($lancamentoSincronizar->getSTATUSLAN() . "");
                $lancamento->setDt_vencimentocheque($lancamentoSincronizar->getDATA_VENCIMENTO_CHEQUE());

                $lancamento->setDt_quitado($lancamentoSincronizar->getDATA_BAIXA());

                $mensageiro = $this->vendaBO->cadastrarLancamento($lancamento);
                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $arrayMensageiros[] = $mensageiro;
                    $this->dao->rollBack();
                    continue;
                }

                $lancamento->setId_lancamento($mensageiro->getFirstMensagem()->getId_lancamento());

                $lancamentoVenda = new LancamentoVendaTO();
                $lancamentoVenda->setId_venda($venda->getId_venda());
                $nu_ordem = $lancamentoVenda->fetch(false, true, false, false, false, 'nu_ordem desc');
                $nu_ordem = is_object($nu_ordem) && !is_null($nu_ordem->getNu_ordem()) ? $nu_ordem->getNu_ordem() : 0;

                $lancamentoVenda->setId_lancamento($lancamento->getId_lancamento());
                $lancamentoVenda->setBl_entrada(false);
                $lancamentoVenda->setNu_ordem($nu_ordem + 1);

                $mensageiro = $this->vendaBO->cadastrarLancamentoVenda($lancamentoVenda);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $arrayMensageiros[] = $mensageiro;
                    $this->dao->rollBack();
                    continue;
                }
                $lancamentoIntegracao = new LancamentoIntegracaoTO();
                $lancamentoIntegracao->setDt_cadastro(new Zend_Date(Zend_Date::ISO_8601));
                $lancamentoIntegracao->setDt_sincronizado(new Zend_Date(Zend_Date::ISO_8601));
                $lancamentoIntegracao->setId_entidade($venda->getId_entidade());
                $lancamentoIntegracao->setId_lancamento($lancamento->getId_lancamento());
                $lancamentoIntegracao->setId_sistema(SistemaTO::FLUXUS);
                $lancamentoIntegracao->setId_usuariocadastro($venda->getId_usuariocadastro());
                $lancamentoIntegracao->setSt_codlancamento($lancamentoSincronizar->getIDLAN());
                $lancamentoIntegracao->setSt_codresponsavel($lancamentoSincronizar->getCODCFO());

                Zend_Debug::dump($lancamentoIntegracao, __CLASS__ . '(' . __LINE__ . ') LancamentoIntegracaoTO INSERÇÃO');
                $mensageiro = $this->cadastrarLancamentoIntegracao($lancamentoIntegracao);
                Zend_Debug::dump($mensageiro, __CLASS__ . '(' . __LINE__ . ') Mensageiro após salvar LancamentoIntegracaoTO INSERÇÃO');

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $arrayMensageiros[] = $mensageiro;
                    $this->dao->rollBack();
                    continue;
                }
            }

            $acordoRel = new AcordoRelTO();
            $acordoRel->setIDACORDO($vwSincronizaAcordoFluxusG2TO->getIdacordo());
            $acordoRel->setIDLAN($vwSincronizaAcordoFluxusG2TO->getIdlan());
            $acordoRel->fetch(true, true, true);

            if (!$acordoRel->getCODCOLIGADA() && $acordoRel->getIDACORDO()) {

                $acordoRel->setCODCOLIGADA($vwSincronizaAcordoFluxusG2TO->getCodcoligada());
                $acordoRel->setCLASSIFICACAO($vwSincronizaAcordoFluxusG2TO->getClassificacao());

                $mensageiro = $this->dao->cadastrarAcordoRel($acordoRel);

                if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    $arrayMensageiros[] = $mensageiro;
                    $this->dao->rollBack();
                    continue;
                }
            }

            if (!in_array(str_replace('G2U', '', $lancamentoSincronizar->getCONTRATO()), $arrayVendas)) {
                $arrayVendas[] = str_replace('G2U', '', $lancamentoSincronizar->getCONTRATO());
            }

            $this->dao->commit();

            $entidadeIntegracao = new EntidadeIntegracaoTO();
            $entidadeIntegracao->setId_entidade($venda->getId_entidade());
            $entidadeIntegracao->setId_sistema(SistemaTO::FLUXUS);
            $entidadeIntegracao->fetch(false, true, true);

            $vwAtualizarGestorFluxusTO = new Totvs_VwAtualizarFluxusGestorTO();
            $vwAtualizarGestorFluxusTO->setIDLAN($lancamentoSincronizar->getIDLAN());
            $vwAtualizarGestorFluxusTO->setCONTRATO($lancamentoSincronizar->getCONTRATO());

            $this->dao->spAtualizaFlagFluxus($vwAtualizarGestorFluxusTO, $entidadeIntegracao->getSt_linkedserver(), $entidadeIntegracao->getSt_caminho());

            $arrayMensageiros[] = new Ead1_Mensageiro($lancamento, Ead1_IMensageiro::SUCESSO);
        }

        $tipoMensageiro = array('ERRO', 'SUCESSO', 'AVISO');

        echo "<h1>Resultados:</h1>";
        //Atualiza todos os nu_valorliquido e nu_valorbruto das vendas que tiveram os lançamentos editados
        foreach ($arrayVendas as $id_venda) {

            $vwSincronizaValorVenda = new Totvs_VwSincronizaValorVendaFluxusG2TO();
            $vwSincronizaValorVenda->setId_venda($id_venda);
            $valorVenda = $vwSincronizaValorVenda->fetch(true, true, false, 'where id_venda = ' . $id_venda);
            $valor = $valorVenda instanceof Totvs_VwSincronizaValorVendaFluxusG2TO ? $valorVenda->getNu_valor() : 0;

            if ($valor) {
                $venda = new VendaTO();
                $venda->setId_venda($id_venda);
                $venda->setNu_valoratualizado($valor);
                $mensageiro = $this->vendaBO->editarVenda($venda);
                echo '<hr/>';
            }

            echo $tipoMensageiro[$mensageiro->getTipo()] . '  /  Venda: ' . $id_venda . ' Atualizada com ' . $tipoMensageiro[$mensageiro->getTipo()] . '<br/>';
        }

        echo '<hr/>';

        foreach ($arrayMensageiros as $mensageiro) {
            if ($mensageiro->getTipo() == Ead1_iMensageiro::SUCESSO) {
                echo $tipoMensageiro[$mensageiro->getTipo()] . '  /  Lançamento: ' . $mensageiro->getFirstMensagem()->getId_lancamento() . '<br/>';
                Zend_Debug::dump($mensageiro, __FILE__ . "(" . __LINE__ . ")<br/>");
            } else {
                echo $tipoMensageiro[$mensageiro->getTipo()] . '  /  ' . $mensageiro->getFirstMensagem() . '<br/>';
                Zend_Debug::dump($mensageiro, __FILE__ . "(" . __LINE__ . ")<br/>");
            }
        }
        //Zend_Debug::dump($arrayMensageiros, __FILE__ . "(" . __LINE__ . ")<br/>");
        exit;
    }

}
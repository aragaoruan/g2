<?php

/**
 * Classe com regras de negócio para o Pagamento
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 05/07/2012
 * @package models
 * @subpackage bo
 */
class PagamentoBO extends Ead1_BO
{

    /**
     * Verifica se uma Venda pode ser paga
     * @param int $id_venda
     * @throws Zend_Exception
     */
    public function verificaPossibilidadePagamento($id_venda, $renegociacao = false, $id_lancamento = null)
    {

        $vendaTO = new VendaTO();
        $vendaTO->setId_venda($id_venda);
        $vendaTO->fetch(true, true, true);

        if (!$vendaTO->getDt_cadastro()) {
            throw new Zend_Exception('Não encontramos a Venda informada.');
        }

        $eTO = new EntidadeTO();
        $eTO->setId_entidade($vendaTO->getId_entidade());
        $eTO->fetch(true, true, true);

        if (!$eTO->getDt_cadastro()) {
            throw new Zend_Exception('Não encontramos a Entidade da Venda.');
        }

        //verifica se a venda esta em processamento.
        if ($vendaTO->getBl_processamento()) {
            throw new Zend_Exception('Essa venda encontrasse em processamento, por favor entre em contato com a ' . $eTO->getSt_nomeentidade() . '.');
        }

        switch ($vendaTO->getId_evolucao()) {
            case VendaTO::EVOLUCAO_CONFIRMADA:
            case VendaTO::EVOLUCAO_NEGOCIADA:
            case VendaTO::EVOLUCAO_EM_ANDAMENTO:

                # se estiver tratando de renegociacao e a evolução for confirmada,
                # então não será lancada exception. isto vai adequar ao pagamento
                # de renegociacao sem alteração em outros pontos relevantes
                if ($renegociacao && ($vendaTO->getId_evolucao() != VendaTO::EVOLUCAO_CONFIRMADA)) {
                    throw new Zend_Exception('Essa venda já foi finalizada, por favor entre em contato com a ' . $eTO->getSt_nomeentidade() . '.');
                }
                break;
        }

        switch ($vendaTO->getId_situacao()) {
            case VendaTO::SITUACAO_VENDA_INATIVA:
                throw new Zend_Exception('Venda inativa, por favor, verifique o Status de pagamento junto a ' . $eTO->getSt_nomeentidade() . '.');
                break;
        }

        $negocio = new \G2\Negocio\Negocio();

        $tf = $negocio->findBy('G2\Entity\TransacaoFinanceira',
            array('id_venda' => $vendaTO->getId_venda(), 'id_statustransacao' => array(187, 188, 189))
        );

        if ($id_lancamento) {
            /** @var G2\Entity\Lancamento $lan */
            $lan = $negocio->findOneBy('G2\Entity\Lancamento', array('id_lancamento' => $id_lancamento));
        }

        if ($tf && (isset($lan) &&
                $lan->getId_meiopagamento()->getId_meiopagamento() != \G2\Constante\MeioPagamento::BOLETO)
        ) {
            throw new Zend_Exception('Foi identificado um pagamento para essa venda, 
            por favor entre em contato com a ' . $eTO->getSt_nomeentidade() . '.');
        }

    }

    /**
     * Verifica o pagamentode uma parcela
     * @param $id_lancamento
     * @throws Zend_Exception
     */
    public function verificaPossibilidadePagamentoLancamento($id_lancamento)
    {

        $to = new LancamentoTO();
        $to->setId_lancamento($id_lancamento);
        $to->fetch(true, true, true);

        if (!$to->getDt_cadastro()) {
            throw new Zend_Exception('Não encontramos o Lancamento.');
        }

        if ($to->getDt_prevquitado()) {
            throw new Zend_Exception('Esta venda tem previsão de pagamento informada para ' . $to->getDt_prevquitado() . ', caso não reconheça, entre em contato com o suporte.');
        }

    }

    /**
     * @param int $id_venda
     * @param int $nu_parcelas
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function gerarLancamentoBoleto($id_venda, $nu_parcelas)
    {
        $dao = new Ead1_DAO;
        try {
            $dao->beginTransaction();

            $this->verificaPossibilidadePagamento($id_venda);

            $mensageiro = $this->gerarLancamentos($id_venda, $nu_parcelas, MeioPagamentoTO::BOLETO, null);

            if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {

                try {
                    $negocio = new \G2\Negocio\Negocio();

                    $venda = $negocio->find('\G2\Entity\Venda', $id_venda);
                    if ($venda) {
                        $venda->setId_evolucao($negocio->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO));
                        $negocio->save($venda);
                    }

                    $transacao = $negocio->findOneBy('\G2\Entity\TransacaoFinanceira', array('id_venda' => $id_venda));
                    if ($transacao) {
                        $transacao->setNuStatus(null);
                        $negocio->save($transacao);
                    }

                } catch (\Exception $e) {
                    throw new Zend_Exception('Não foi possível resetar a venda e a transação financeira');
                }

                throw new Zend_Exception($mensageiro->getFirstMensagem());
            }

            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);

            $vendaBO = new VendaBO();
            $mevenda = $vendaBO->salvarArrayLancamentos($mensageiro->getMensagem(), $vendaTO, true, true);
            if ($mevenda->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception($mevenda->getFirstMensagem());
            }

            $mpTO = $this->retornaFormaMeioDivisaoVenda($id_venda, FormaPagamentoAplicacaoTO::VENDA_SITE, MeioPagamentoTO::BOLETO, $nu_parcelas);
            if (!$mpTO) {
                throw new Zend_Exception('Dados da forma de pagamento não encontrados');
            }

            $vendaTO->setId_evolucao(VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO);
            $vendaTO->setId_situacao(VendaTO::SITUACAO_VENDA_ATIVA);
            $vendaTO->setId_formapagamento($mpTO->getId_formapagamento());
            $vendaBO->editarVenda($vendaTO);


            $dao->commit();

            try {
                $mensagemBO = new MensagemBO();
                $emnsageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::ENVIO_BOLETO, $vendaTO, TipoEnvioTO::EMAIL, true);
            } catch (Exception $e) {
                throw $e;
            }

            return new Ead1_Mensageiro('Boletos Gerados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Gera os lançamentos para Efetuar um Pagamento
     * @param int $id_venda
     * @param int $nu_parcelas
     * @param int $id_meiopagamento
     * @param int $id_cartaoconfig
     */
    public function gerarLancamentos($id_venda, $nu_parcelas, $id_meiopagamento = MeioPagamentoTO::BOLETO, $id_cartaoconfig = null)
    {

        try {
            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);

            $fiBO = new FinanceiroBO();

            switch ($id_meiopagamento) {
                case MeioPagamentoTO::BOLETO:

                    $meboleto = $fiBO->retornarDadosConfBoleto($vendaTO->getId_entidade());
                    if ($meboleto->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception('Erro ao buscar os dados para o Boleto.');
                    }

                    $boleto = $meboleto->getFirstMensagem()->toArray();
                    if (!$boleto) {
                        throw new Zend_Exception('Erro ao buscar os dados para o Boleto.');
                    }
                    $id_cartaoconfig = null;
                    $id_boletoconfig = $boleto['id_boletoconfig'];
                    break;
                case MeioPagamentoTO::RECORRENTE:
                case MeioPagamentoTO::CARTAO:
                    $id_boletoconfig = null;
                    break;
            }


            $lancamentos = $this->calcularParcelas($id_meiopagamento, $vendaTO->getId_venda(), $nu_parcelas);
            if ($lancamentos->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new Zend_Exception('Erro ao buscar as parcelas.'.$lancamentos->getFirstMensagem().$id_meiopagamento);
            }
            $arrLancamentoTO = $lancamentos->getMensagem();

            foreach ($arrLancamentoTO as $lancamentoTO) {
                $lancamentoTO->setId_boletoconfig($id_boletoconfig);
                $lancamentoTO->setId_cartaoconfig($id_cartaoconfig);
                $lancamentoTO->setid_usuariolancamento($vendaTO->getId_usuario());
                $lancamentoTO->setdt_emissao(new Zend_Date());
                $lancamentoTO->setNu_valor($lancamentoTO->getNu_valor());
            }


            return new Ead1_Mensageiro($arrLancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao criar o array de Lançamentos. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * Gera os lançamentos de uma campanha de assinatura
     * @param unknown_type $id_venda
     * @param unknown_type $nu_parcelas
     * @param unknown_type $nu_valor
     * @param unknown_type $id_cartaoconfig
     * @return Ead1_Mensageiro
     */
    public function gerarLancamentosAssinatura($id_venda, $nu_parcelas, $nu_valor, $id_cartaoconfig, Zend_Date $dt_vencimento)
    {

        try {
            $vendaTO = new VendaTO();
            $vendaTO->setId_venda($id_venda);
            $vendaTO->fetch(true, true, true);

            $arrLancamentoTO = array();

            for ($x = 0; $x < $nu_parcelas; $x++) {

                $dt_recorrencia = clone $dt_vencimento;

                $lancamento = new LancamentoTO();
                $lancamento->setNu_valor($nu_valor);
                $lancamento->setId_meiopagamento(MeioPagamentoTO::RECORRENTE);
                $lancamento->setId_tipolancamento(TipoLancamentoTO::CREDITO);
                $lancamento->setId_entidade($lancamento->getSessao()->id_entidade);
                $lancamento->setid_usuariolancamento($vendaTO->getId_usuario());
                $lancamento->setdt_emissao(new Zend_Date());
                $lancamento->setDt_vencimento($dt_recorrencia);
                $lancamento->setId_cartaoconfig($id_cartaoconfig);
                $lancamento->setIdSistemacobranca(\G2\Constante\Sistema::PAGARME);
                $lancamento->setNu_cobranca($x + 1);

                if ($x == 0)
                    $lancamento->setBl_quitado(true);

                $dt_vencimento->addMonth(1);

                $arrLancamentoTO[] = $lancamento;
            }

            return new Ead1_Mensageiro($arrLancamentoTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao criar o array de Lançamentos. ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna um array de Produtos
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array|VwProdutoTO $produtos
     * @return Ead1_Mensageiro
     */
    public function retornaProdutosPorID($produtos, $st_codigocupom = null)
    {

        $arrProdutoTO = false;
        $sessao = Ead1_Sessao::getSessaoCarrinho();
        if ($st_codigocupom) {
            $negocio = new \G2\Negocio\Cupom();
        }
        $bo = new ProdutoBO();
        foreach ($produtos as $produto) {

            if ($produto instanceof VwProdutoTO) {
                $pTO = $produto;
                $pTO->nu_valorbruto = $pTO->getNu_valor();
            } else {
                $quantidade = (isset($produto['nu_quantidade']) ? $produto['nu_quantidade'] : 1);

                $vw = new VwProdutoTO();
                $idEntidade = $sessao->getId_entidade() != $vw->getSessao()->id_entidade ? $sessao->getId_entidade() : $vw->getSessao()->id_entidade;
                $vw->setId_entidade($idEntidade);
                $vw->setId_produto($produto['id_produto']);
                $retorno = $bo->retornarVwProdutoCompartilhado($vw);
                if ($retorno->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new Zend_Exception("O Produto {$produto['id_produto']} não foi encontrado!");
                }

                $pTO = $retorno->getFirstMensagem();
                if ($pTO instanceof VwProdutoTO) {

                    $campanha = false;
                    if($sessao->getId_campanhacomercial()){
                        $cc = new \G2\Negocio\CampanhaComercial();
                        $campanha = $cc->retornaVwCampanhaProdutoCategoria(
                            array('id_produto' => $pTO->getid_produto(), 'id_campanhacomercial' => $sessao->getId_campanhacomercial()
                            , 'bl_ativo' => 1, 'bl_recente'=>1), $pTO->getId_entidade());
                    }

                    if ($campanha && isset($campanha['nu_valorvenda'])) {
                        $pTO->setNu_valor($campanha['nu_valorvenda'] * $quantidade);
                    } else {
                        $pTO->setNu_valor($pTO->getNu_valorvenda() * $quantidade);
                    }

                    $pTO->setNu_quantidade($quantidade);
                    $pTO->nu_valorbruto = $pTO->getNu_valor();

                }
            }

            if ($st_codigocupom) {

                if ($pTO->getNu_quantidade() == false) {
                    $pTO->setNu_quantidade(1);
                }

                $cupom = $negocio->findCuponsToWs(array('st_codigocupom' => $st_codigocupom, 'produtos' => array($pTO->getId_produto())));
                if ($cupom->getTipo() == Ead1_IMensageiro::SUCESSO) {

                    $valoranterior = $pTO->getNu_valor();
                    $pTO->setNu_valor($cupom->getFirstMensagem()->getNu_valorliquido() * $pTO->getNu_quantidade());

                    if ($pTO->getNu_valor() < 0) {
                        $pTO->setNu_valor(0);
                    }

                    if ($valoranterior > $pTO->getNu_valor()) {
                        $pTO->nu_valorbruto = $valoranterior;
                    } else {
                        $pTO->nu_valorbruto = $pTO->getNu_valor();
                    }

                    Ead1_Sessao::setSessionCarrinho(array('id_cupom' => $cupom->getFirstMensagem()->getId_cupom()));
                }
            }

            if ($pTO->getId_modelovenda()) {
                Ead1_Sessao::setSessionCarrinho(array('id_modelovenda' => $pTO->getId_modelovenda()));
            }
            //verifica se existe o objeto e se tem o atributo
            if (isset($produto) && isset($produto->id_turma)) {
                $pTO->id_turma = ($produto->id_turma);
            } else {
                $pTO->id_turma = (NULL);
            }

            $arrProdutoTO[] = $pTO;

        }
        if (!empty($arrProdutoTO)) {
            return new Ead1_Mensageiro($arrProdutoTO, Ead1_IMensageiro::SUCESSO);
        } else {
            return new Ead1_Mensageiro('Nenhum produto encontrado', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    public function retornaVendaPagamento(VendaTO $vendaTO)
    {
        try {
            $fiBO = new FinanceiroBO();
            $pagamento = new stdClass();

            try {
                $this->verificaPossibilidadePagamento($vendaTO->getId_venda());
            } catch (Exception $e) {
                return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
            }

            $bo = new VendaBO();
            $vendaUsuarioTO = new VwVendaUsuarioTO();
            $vendaUsuarioTO->setId_venda($vendaTO->getId_venda());
            $vu = $bo->retornarVwVendaUsuario($vendaUsuarioTO);

            if ($vu->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro($vu->getFirstMensagem(), Ead1_IMensageiro::AVISO);
            }

            $pagamento->venda = $vu->getFirstMensagem();

            $sessao = new Zend_Session_Namespace('geral');
            $sessao->id_entidade = $pagamento->venda->getId_entidade();
            $sessao->id_usuario = $pagamento->venda->getId_usuario();


            $produtoVendaTO = new VwProdutoVendaTO();
            $produtoVendaTO->setId_venda($vendaTO->getId_venda());

            $bo = new VendaBO();
            $pv = $bo->retornarVwProdutoVenda($produtoVendaTO);

            if ($pv->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro('Dados dos Produtos não encontrados.', Ead1_IMensageiro::AVISO);
            }

            $pagamento->carrinho['produtos'] = $pv->getMensagem();

            $nu_valortotal = null;
            foreach ($pagamento->carrinho['produtos'] as $produto) {
                $nu_valortotal += $produto->nu_valorliquido;
                $produto->bl_desconto = false;
                if ($produto->getNu_valorbruto() > $produto->getNu_valorliquido()) {
                    $produto->bl_desconto = true;
                }

                $produto->nu_valorliquido = $this->formataValor((double)$produto->nu_valorliquido, 'R$ #,##0.00');
                $produto->nu_valorbruto = $this->formataValor((double)$produto->nu_valorbruto, 'R$ #,##0.00');
            }

            if (!array_key_exists('resumo', $pagamento->carrinho))
                $pagamento->carrinho['resumo'] = new stdClass();

            $pagamento->carrinho['resumo']->nu_valortotal = $this->formataValor((double)$nu_valortotal, 'R$ #,##0.00');
            $pagamento->carrinho['resumo']->nu_subtotal = $this->formataValor((double)$nu_valortotal, 'R$ #,##0.00');
            $pagamento->carrinho['resumo']->nu_valorjuros = $this->formataValor(0, 'R$ #,##0.00');
            $pagamento->carrinho['resumo']->nu_juros = '0%';
            $pagamento->carrinho['resumo']->nu_valor_for_pagarme = number_format((($nu_valortotal) * 100), 0, '', '');

            switch ($pagamento->venda->getid_evolucao()) {
                case VendaTO::EVOLUCAO_AGUARDANDO_RECEBIMENTO:
                    /**
                     * Retornando os dados para pagamento via Cartão de crédito
                     */
                    $meformacartao = $this->_retornaDadosPagamentoCartaoAguardandoRecebimento($vendaTO, $pagamento);
                    if ($meformacartao->getTipo() == Ead1_IMensageiro::ERRO) {
                        return new Ead1_Mensageiro($meformacartao->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }

                    /**
                     * Retornando os dados para pagamento via Boleto Bancário
                     */
                    $meformaboleto = $this->_retornaDadosPagamentoBoletoAguardandoRecebimento($vendaTO, $pagamento);
                    if ($meformaboleto->getTipo() == Ead1_IMensageiro::ERRO) {
                        return new Ead1_Mensageiro($meformaboleto->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }

                    break;
                case VendaTO::EVOLUCAO_AGUARDANDO_NEGOCIACAO:

                    /**
                     * Retornando os dados para pagamento via Cartão de crédito
                     */
                    $meformacartao = $this->_retornaDadosPagamentoCartaoAguardandoNegociacao($vendaTO, $pagamento);
                    if ($meformacartao->getTipo() == Ead1_IMensageiro::ERRO) {
                        return new Ead1_Mensageiro($meformacartao->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }


                    /**
                     * Retornando os dados para pagamento via Boleto
                     */
                    $meformaboleto = $this->_retornaDadosPagamentoBoletoAguardandoNegociacao($vendaTO, $pagamento);
                    if ($meformaboleto->getTipo() == Ead1_IMensageiro::ERRO) {
                        return new Ead1_Mensageiro($meformaboleto->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }


                    /**
                     * Retornando os dados para pagamento via Cartão de crédito Recorrente
                     */
                    $meformacartao = $this->_retornaDadosPagamentoCartaoRecorrenteAguardandoNegociacao($vendaTO, $pagamento);
                    if ($meformacartao->getTipo() == Ead1_IMensageiro::ERRO) {
                        return new Ead1_Mensageiro($meformacartao->getFirstMensagem(), Ead1_IMensageiro::AVISO);
                    }


                    if (!isset($pagamento->carrinho['formapagamento']) || $pagamento->carrinho['formapagamento'] == false) {
                        return new Ead1_Mensageiro('Nenhuma forma de pagamento encontrada para esta venda.', Ead1_IMensageiro::AVISO);
                    }


                    break;
                default:
                    return new Ead1_Mensageiro('Dados da Venda não encontrados.', Ead1_IMensageiro::AVISO);
                    break;
            }


            if ($meformaboleto->getTipo() != Ead1_IMensageiro::SUCESSO && $meformacartao->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro('Os dados para pagamento da Venda não encontrados.', Ead1_IMensageiro::AVISO);
            }

// 			$this->retornarCartaoBandeira($pagamento);
// 			Zend_Debug::dump($pagamento->carrinho,__CLASS__.'('.__LINE__.')');exit;
            return new Ead1_Mensageiro($pagamento, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {

            return new Ead1_Mensageiro('Erro ao Recuperar a Venda.', Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados para pagamento via cartão de crédito para uma venda que esteja aguardando negociação
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    protected function _retornaDadosPagamentoCartaoAguardandoNegociacao(VendaTO $vendaTO, stdClass $pagamento)
    {

        try {

            $fiBO = new FinanceiroBO();
            $bandeira = $fiBO->retornarCartaoBandeira(new CartaoBandeiraTO(), SistemaTO::BRASPAG);

            if ($bandeira->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro('Não foi possível encontrar uma Bandeira para pagamento por Cartão de Crétido.', Ead1_IMensageiro::AVISO);
            }
            if (!is_object($pagamento->carrinho)) {
                $pagamento->carrinho['formapagamento'] = new stdClass();
            }

            $pagamento->carrinho['formapagamento']->bandeira = $bandeira->getMensagem();
            $vwFormaMeioDivisaoVendaTO = new VwFormaMeioDivisaoVendaTO();
            $vwFormaMeioDivisaoVendaTO->setId_venda($vendaTO->getId_venda());
            $vwFormaMeioDivisaoVendaTO->setId_formapagamentoaplicacao(FormaPagamentoAplicacaoTO::VENDA_SITE);
            $vwFormaMeioDivisaoVendaTO->setId_meiopagamento(MeioPagamentoTO::CARTAO);

            $bo = new VendaBO();
            $fm = $bo->retornarVwFormaMeioDivisaoVenda($vwFormaMeioDivisaoVendaTO);

            if ($fm->getTipo() == Ead1_IMensageiro::ERRO) {
                return new Ead1_Mensageiro('Dados dos meios de pagamento CARTÃO não encontrados.', Ead1_IMensageiro::AVISO);
            }
            if ($fm->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $parcelas = $this->_retornaParcelas($fm->getMensagem());
                if ($parcelas) {
                    $pagamento->carrinho['formapagamento']->cartao = $parcelas;
                }
            } else {
                return new Ead1_Mensageiro('As parcelas para Cartão de Crédito não foram encontradas!', Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro('Dados retornados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados para pagamento via cartão de crédito recorrente para uma venda que esteja aguardando negociação
     * @param VendaTO $vendaTO
     * @param stdClass $pagamento
     * @return Ead1_Mensageiro
     */
    protected function _retornaDadosPagamentoCartaoRecorrenteAguardandoNegociacao(VendaTO $vendaTO, stdClass $pagamento)
    {

        try {

            $integracao = new EntidadeIntegracaoTO();
            $integracao->setId_entidade($vendaTO->getId_entidade());
            $integracao->setId_sistema(SistemaTO::BRASPAG_RECORRENTE);
            $integracao->fetch(false, true, true);

            if (!$integracao->getSt_codchave()) {
                return new Ead1_Mensageiro('Não existe interface de integração para o sistema BRASPAG nesta entidade!', Ead1_IMensageiro::AVISO);
            }


            $fiBO = new FinanceiroBO();
            $bandeira = $fiBO->retornarCartaoBandeira(new CartaoBandeiraTO(), SistemaTO::BRASPAG_RECORRENTE);

            if ($bandeira->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro('Não foi possível encontrar uma Bandeira para pagamento por Cartão de Crétido Recorrente.', Ead1_IMensageiro::AVISO);
            }

            if (!isset($pagamento->carrinho['formapagamento']) || !$pagamento->carrinho['formapagamento'])
                $pagamento->carrinho['formapagamento'] = new stdClass();

            $pagamento->carrinho['formapagamento']->bandeirarecorrente = $bandeira->getMensagem();


            $vwFormaMeioDivisaoVendaTO = new VwFormaMeioDivisaoVendaTO();
            $vwFormaMeioDivisaoVendaTO->setId_venda($vendaTO->getId_venda());
            $vwFormaMeioDivisaoVendaTO->setId_formapagamentoaplicacao(FormaPagamentoAplicacaoTO::VENDA_SITE);
            $vwFormaMeioDivisaoVendaTO->setId_meiopagamento(MeioPagamentoTO::RECORRENTE);
            $bo = new VendaBO();
            $fm = $bo->retornarVwFormaMeioDivisaoVenda($vwFormaMeioDivisaoVendaTO);
            if ($fm->getTipo() == Ead1_IMensageiro::ERRO) {
                return new Ead1_Mensageiro('Dados dos meios de pagamento CARTÃO RECORRENTE não encontrados.', Ead1_IMensageiro::AVISO);
            }
            if ($fm->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $parcelas = $this->_retornaParcelas($fm->getMensagem());
                if ($parcelas) {
                    $pagamento->carrinho['formapagamento']->recorrente = $parcelas;
                }
            }


            return new Ead1_Mensageiro('Dados retornados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados para pagamento via boleto bancário para uma venda que esteja aguardando negociação
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    protected function _retornaDadosPagamentoBoletoAguardandoNegociacao(VendaTO $vendaTO, stdClass $pagamento)
    {

        try {
            /*
             * recuperando dados pra boleto
             * */
            $vwFormaMeioDivisaoVendaTO = new VwFormaMeioDivisaoVendaTO();
            $vwFormaMeioDivisaoVendaTO->setId_venda($vendaTO->getId_venda());
            $vwFormaMeioDivisaoVendaTO->setId_formapagamentoaplicacao(FormaPagamentoAplicacaoTO::VENDA_SITE);
            $vwFormaMeioDivisaoVendaTO->setId_meiopagamento(MeioPagamentoTO::BOLETO);

            $fm = null;
            $bo = new VendaBO();
            $fm = $bo->retornarVwFormaMeioDivisaoVenda($vwFormaMeioDivisaoVendaTO);
            if ($fm->getTipo() == Ead1_IMensageiro::ERRO) {
                return new Ead1_Mensageiro('Dados dos meios de pagamento BOLETO não encontrados.', Ead1_IMensageiro::AVISO);
            }
            if ($fm->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $parcelas = $this->_retornaParcelas($fm->getMensagem());
                if ($parcelas) {
                    $pagamento->carrinho['formapagamento']->boleto = $parcelas;
                }
            }

            if (!isset($pagamento->carrinho['formapagamento'])) {
                return new Ead1_Mensageiro('Não foram encontradas formas de pagamento para esta Venda', Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro('Dados retornados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados para pagamento via boleto bancário para uma venda que esteja aguardando recebimento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    protected function _retornaDadosPagamentoBoletoAguardandoRecebimento(VendaTO $vendaTO, stdClass $pagamento)
    {

        try {

            $vela = new VwVendaLancamentoTO();
            $vela->setId_venda($vendaTO->getId_venda());
            $vela->setBl_quitado(false);
            $vela->setId_meiopagamento(MeioPagamentoTO::BOLETO);

            $orm = new VwVendaLancamentoORM();
            $arrfm = $orm->consulta($vela, false, false, array('nu_ordem'));
            if (!$arrfm) {
                return new Ead1_Mensageiro('Dados dos meios de pagamento BOLETO BANCÁRIO não encontrados.', Ead1_IMensageiro::AVISO);
            }
            $parcelas = $this->_retornaParcelasAguardandoRecebimento($arrfm);
            if ($parcelas) {
                $pagamento->carrinho['formapagamento']->boleto = $parcelas;
            }

            if (!isset($pagamento->carrinho['formapagamento'])) {
                return new Ead1_Mensageiro('Não foram encontradas formas de pagamento para esta Venda', Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro('Dados retornados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados para pagamento via cartão de crédito para uma venda que esteja aguardando recebimento
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $vendaTO
     * @return Ead1_Mensageiro
     */
    protected function _retornaDadosPagamentoCartaoAguardandoRecebimento(VendaTO $vendaTO, stdClass $pagamento)
    {

        try {

            $vela = new VwPagamentoCartaoTO();
            $vela->setId_venda($vendaTO->getId_venda());

            $fiBO = new FinanceiroBO();
            $bandeira = $fiBO->retornarCartaoBandeira(new CartaoBandeiraTO(), SistemaTO::BRASPAG);

            if ($bandeira->getTipo() != Ead1_IMensageiro::SUCESSO) {
                return new Ead1_Mensageiro('Não foi possível encontrar uma Bandeira para pagamento por Cartão de Crétido.', Ead1_IMensageiro::AVISO);
            }

            if (!is_object($pagamento->carrinho['formapagamento']))
                $pagamento->carrinho['formapagamento'] = new stdClass();
            $pagamento->carrinho['formapagamento']->bandeira = $bandeira->getMensagem();

            $orm = new VwPagamentoCartaoORM();
            $arrfm = $orm->consulta($vela, false, false);
            if (!$arrfm) {
                return new Ead1_Mensageiro('Dados dos meios de pagamento CARTÃO não encontrados.', Ead1_IMensageiro::AVISO);
            }

            $parcelas = $this->_retornaParcelasAguardandoRecebimentoCartao($arrfm);
            if ($parcelas) {
                $pagamento->carrinho['formapagamento']->cartao = $parcelas;
            }


            return new Ead1_Mensageiro('Dados retornados com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Formata as parcelas para pagamento
     * @param array $meiopagamento - contendo um array de VwPagamentoCartaoTO
     * @return Ambigous <boolean, stdClass>
     */
    protected function _retornaParcelasAguardandoRecebimentoCartao(array $meiopagamento)
    {

        $pagamento = false;

        foreach ($meiopagamento as $to) {

            $mpagamento = new stdClass();
            $mpagamento->nu_valorliquido = $this->formataValor($to->getNu_valorcartao());
            $mpagamento->nu_valortotal = $this->formataValor($to->getNu_valorcartao());
            $mpagamento->nu_valorparcela = $this->formataValor(($to->getNu_valorcartao() / $to->getNu_vezes()));
            $mpagamento->st_meiopagamento = $to->getNu_vezes() . 'x de ';
            $mpagamento->nu_parcelas = $to->getNu_vezes();

            $pagamento[] = $mpagamento;
        }


        return $pagamento;
    }

    /**
     * Formata as parcelas para pagamento
     * @param array $meiopagamento - contendo um array de VwVendaLancamentoTO
     * @return Ambigous <boolean, stdClass>
     */
    protected function _retornaParcelasAguardandoRecebimento(array $meiopagamento)
    {

        $pagamento = false;

        foreach ($meiopagamento as $to) {

            $mpagamento = new stdClass();
            $mpagamento->nu_valorliquido = $this->formataValor($to->getNu_valor());
            $mpagamento->nu_valortotal = $this->formataValor($to->getNu_valor());
            $mpagamento->nu_juros = $to->getNu_juros() ? ' (' . $to->getNu_juros() . '% a.m)' : ' (sem juros)';
            $mpagamento->id_formapagamento = $to->getid_formapagamento();
            $mpagamento->id_lancamento = $to->getid_lancamento();

            if ($to->getBl_entrada()) {
                $mpagamento->st_meiopagamento = 'Entrada de ';
            } else {
                $mpagamento->st_meiopagamento = $to->getNu_ordem() . '&#170 parcela de ';
            }

            $pagamento[] = $mpagamento;
        }


        return $pagamento;
    }

    /**
     * Formata as parcelas para aparecerem na tela do cartão
     * @param array $meiopagamento - contendo um array de VwFormaMeioDivisaoVendaTO
     * @return Ambigous <boolean, stdClass>
     */
    protected function _retornaParcelas(array $meiopagamento)
    {

        $pagamento = false;

        foreach ($meiopagamento as $to) {

            if ($to->getId_categoriacampanha() == \G2\Entity\CategoriaCampanha::PRAZO && $to->getId_meiopagamento() == MeioPagamentoTO::RECORRENTE) {

                $nu_valor = $this->calcularJuros($to->getNu_valorliquido(), $to->getNu_juros(), 1, $to->getId_tipocalculojuros());

                $mpagamento = new stdClass();
                $mpagamento->nu_parcelas = 1;
                $mpagamento->nu_valorliquido = $this->formataValor($nu_valor);
                $mpagamento->st_meiopagamento = '';
                $mpagamento->id_formapagamento = $to->getid_formapagamento();
                $mpagamento->nu_valortotal = $this->formataValor($nu_valor);
                $mpagamento->nu_juros = ' Mensais'; //$to->getNu_juros() ?  ' ('.$to->getNu_juros().'% a.m)'  : ' (sem juros)';
                $pagamento[] = $mpagamento;
            } else {

                if ($to->getNu_parcelaquantidademax() == 1) {

                    $nu_valor = $this->calcularJuros($to->getNu_valorliquido(), $to->getNu_juros(), 1, $to->getId_tipocalculojuros());

                    $mpagamento = new stdClass();
                    $mpagamento->nu_parcelas = 1;
                    $mpagamento->nu_valorliquido = $this->formataValor($nu_valor);
                    $mpagamento->st_meiopagamento = '1 X de ';
                    $mpagamento->id_formapagamento = $to->getid_formapagamento();
                    $mpagamento->nu_valortotal = $this->formataValor($nu_valor);
                    $mpagamento->nu_juros = $to->getNu_juros() ? ' (' . $to->getNu_juros() . '% a.m)' : ' (sem juros)';
                    $pagamento[] = $mpagamento;
                } elseif ($to->getNu_parcelaquantidademax() > 1) {

                    $y = $to->getNu_parcelaquantidademin() ? $to->getNu_parcelaquantidademin() : 1;
                    for ($x = $y; $x <= $to->getNu_parcelaquantidademax(); $x++) {

                        $nu_valor = $this->calcularJuros($to->getNu_valorliquido(), $to->getNu_juros(), $x, $to->getId_tipocalculojuros());
                        if ($nu_valor < $to->getNu_valormin() && $x > 1) {
                            continue;
                        }

                        $mpagamento = new stdClass();
                        $mpagamento->nu_parcelas = $x;
                        $mpagamento->nu_valorliquido = $this->formataValor($nu_valor);
                        $mpagamento->st_meiopagamento = $x . ' X de ';
                        $mpagamento->id_formapagamento = $to->getid_formapagamento();
                        $mpagamento->nu_valortotal = $this->formataValor(($nu_valor * $x));
                        $mpagamento->nu_juros = $to->getNu_juros() ? ' (' . $to->getNu_juros() . '% a.m)' : ' (sem juros)';
                        $pagamento[] = $mpagamento;
                    }
                }
            } //f($to->getId_categoriacampanha()==\G2\Entity\CategoriaCampanha::PRAZO){
        }

        return $pagamento;
    }

    /**
     * Retorna o FormaMeioPagamentoTO ou False
     * @param int $id_venda
     * @param int $id_formapagamentoaplicacao
     * @param int $id_meiopagamento
     * @param int $nu_parcelas
     * @return boolean|VwFormaMeioDivisaoVendaTO
     */
    public function retornaFormaMeioDivisaoVenda($id_venda, $id_formapagamentoaplicacao, $id_meiopagamento, $nu_parcelas)
    {

        $where[] = 'id_venda = ' . $id_venda;
        $where[] = 'id_formapagamentoaplicacao = ' . $id_formapagamentoaplicacao;
        $where[] = 'id_meiopagamento = ' . $id_meiopagamento;
        $where[] = $nu_parcelas . ' between nu_parcelaquantidademin and nu_parcelaquantidademax';

        $vmdvORM = new VwFormaMeioDivisaoVendaORM();
        $vmdvrs = $vmdvORM->fetchRow($where);

        if ($vmdvrs) {
            $vmdv = $vmdvrs->toArray();
        } else {
            $vmdv = false;
        }

        if (!$vmdv) {
            return false;
        } else {
            return new VwFormaMeioDivisaoVendaTO($vmdv);
        }
    }

    /**
     * Retorna um Array com vários lançamentos com as parcelas calculadas
     * @param int $id_meiopagamento
     * @param int $id_venda
     * @param int $nu_parcelas
     * @param int $id_formapagamentoaplicacao
     * @throws Zend_Exception
     * @return Ead1_Mensageiro | array de lancamentosTO
     */
    public function calcularParcelas($id_meiopagamento, $id_venda, $nu_parcelas, $id_formapagamentoaplicacao = FormaPagamentoAplicacaoTO::VENDA_SITE)
    {

        try {

            if (empty($id_meiopagamento)) {
                throw new Zend_Exception('Meio de Pagamento não informado');
            }
            if (empty($id_venda)) {
                throw new Zend_Exception('Venda não informada');
            }
            if (empty($nu_parcelas)) {
                throw new Zend_Exception('Quantidade de Parcelas não informada');
            }


            $mpTO = $this->retornaFormaMeioDivisaoVenda($id_venda, $id_formapagamentoaplicacao, $id_meiopagamento, $nu_parcelas);
            if (!$mpTO) {
                throw new Zend_Exception('Dados da forma de pagamento não encontrados');
            }

            $nu_valor = $this->calcularJuros($mpTO->getNu_valorliquido(), $mpTO->getNu_juros(), $nu_parcelas, $mpTO->getId_tipocalculojuros());

            $parcelas = array();

            for ($x = 0; $x < $nu_parcelas; $x++) {

                $lancamento = new LancamentoTO();
                $lancamento->setNu_valor($nu_valor);
                $lancamento->setId_meiopagamento($id_meiopagamento);
                $lancamento->setId_tipolancamento(TipoLancamentoTO::CREDITO);
                $lancamento->setId_entidade($lancamento->getSessao()->id_entidade);
                $lancamento->setNu_juros($mpTO->getNu_juros());
                $lancamento->setNu_cobranca($x + 1);

                $parcelas[] = $lancamento;
            }

            return new Ead1_Mensageiro($parcelas, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Calcula o valor de uma parcela
     * @param double $valor
     * @param int $taxa
     * @param int $parcelas
     * @param int $id_tipocalculojuros
     * @return string
     */
    public function calcularJuros($valor, $taxa, $parcelas, $id_tipocalculojuros = FormaPagamentoTO::JUROS_SIMPLES)
    {

        switch ($id_tipocalculojuros) {

            case FormaPagamentoTO::JUROS_SIMPLES:
                $taxa = $taxa / 100;
                $m = $valor * (1 + $taxa * $parcelas);


                $valParcela = number_format(Ead1_BO::ceiling(($m / $parcelas), 2), 2, ".", "");
                return $valParcela;

                break;
            case FormaPagamentoTO::JUROS_COMPOSTO:
                $taxa = $taxa / 100;
                $valParcela = $valor * pow((1 + $taxa), $parcelas);

                $valParcela = number_format(Ead1_BO::ceiling(($valParcela / $parcelas), 2), 2, ".", "");
                return $valParcela;
                break;
        }
    }

}

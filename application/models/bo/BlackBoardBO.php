<?php

/**
 * Classe com regras de negócio para o Uso das Classes de WebService do BlackBoard
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 18/02/2014
 * @package models
 * @subpackage bo
 */
class BlackBoardBO extends Ead1_BO
{


    public $debug = null;
    public $key = null;
    public $id_entidade = null;
    public $prefix_sala = null;

    public function __construct($id_entidade)
    {

        try {

            if ((int)$id_entidade == false) {
                throw new BlackBoard_Exception("O Código {$id_entidade} não é um id_entidade válido.");
            }

            $this->id_entidade = $id_entidade;
            $this->prefix_sala = 'G2_' . $id_entidade . '_';

            $to = new EntidadeIntegracaoTO();
            $to->setId_entidade($id_entidade);
            $to->setId_sistema(SistemaTO::BLACKBOARD);
            $to->fetch(false, true, true);

            if ($to->getSt_codchave() == false) {
                throw new BlackBoard_Exception("A Entidade {$id_entidade} não possui integração com o BlackBoard.");
            }

            $this->key = json_decode($to->getSt_codchave());
            if ($this->key == false) {
                throw new BlackBoard_Exception("A Entidade {$id_entidade} não possui dados de acesso na integração com o BlackBoard.");
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

    public function setKeys(BlackBoard_Service $blackboard)
    {

        try {

            $blackboard->setUrl($this->key->url);
            $blackboard->setDataSourceID($this->key->datasourceid);
            $blackboard->setClientprogramid($this->key->clientprogramid);
            $blackboard->setClientvendorid($this->key->clientvendorid);
            $blackboard->setRegistrationpassword($this->key->registrationpassword);
            $blackboard->setSnap_pass($this->key->snap_pass);
            $blackboard->setSnap_url($this->key->snap_url);
            $blackboard->setSnap_user($this->key->snap_user);

        } catch (Exception $e) {
            throw $e;
        }

    }

    /**
     * Cria um Curso no BlackBoard
     * @param array $arrVO
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ambigous <Ambigous, Ead1_Mensageiro, string, mixed>
     */
    public function criarCurso($arrVO)
    {

        try {

            foreach ($arrVO as $vo) {
                if (($vo instanceof BlackBoard_VO_CourseFileVO) == false) {
                    throw new BlackBoard_Exception("Você precisa informar um array de BlackBoard_VO_CourseFileVO mas enviou um array de " . get_class($vo));
                }
            }

            $blackboard = new BlackBoard_Service_Course();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
// 			$blackboard->debug = true;
            $save = $blackboard->createCourse(new BlackBoard_VO_CourseFileVO(), $arrVO);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }
        } catch (Exception $e) {
            throw $e;
        }

    }


    /**
     * Registra quais serviços irão ser usados no WS
     * @throws Exception
     */
    public function registerTool()
    {
// 		try {
// 			$blackboard = new BlackBoard_Service_Context();
// 			$this->setKeys($blackboard);
// // 			$blackboard->iniciarServico();
// 			$blackboard->debug = $this->debug;

// 			return $blackboard->registerTool();

// 		} catch (Exception $e) {
// 			throw $e;
// 		}
    }

    /**
     * Verifica um Curso no BlackBoard por ID
     * @param string $id_saladeaula - G2_XXX ou G2_XX_XXXX
     * @throws Exception
     */
    public function verificarCurso($id_saladeaula)
    {

        try {

            $blackboard = new BlackBoard_Service_Course();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;
            $filter = new BlackBoard_VO_CourseFilterVO();
            $filter->setFilterType(BlackBoard_VO_CourseFilterVO::GET_COURSE_BY_COURSE_ID);
            $filter->setCourseIds(array($id_saladeaula));
            return $blackboard->getCourse($filter);

        } catch (Exception $e) {
            throw $e;

        }

    }


    /**
     * Retorna uma Sala de Aula
     * @param string $id_saladeaula - G2_XXX ou G2_XX_XXXX
     * @param unknown_type $xml_return
     * @throws Exception
     * @return BlackBoard_Mensageiro
     */
    public function getCourse($id_saladeaula, $xml_return = false)
    {

        try {

            $blackboard = new BlackBoard_Service_Course();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;

            $filter = new BlackBoard_VO_CourseFilterVO();
            $filter->setFilterType(BlackBoard_VO_CourseFilterVO::GET_COURSE_BY_COURSE_ID);
            $filter->setCourseIds(array($id_saladeaula));
            return $blackboard->getCourse($filter, $xml_return);

        } catch (Exception $e) {
            throw $e;

        }

    }

    /**
     * Cria uma Comunidade/Organização no BlackBoard
     * @param array $arrVO
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ambigous <Ambigous, Ead1_Mensageiro, string, mixed>
     */
    public function criarOrganizacao(array $arrVO)
    {

        try {

            foreach ($arrVO as $vo) {
                if (($vo instanceof BlackBoard_VO_OrganizationFileVO) == false) {
                    throw new BlackBoard_Exception("Você precisa informar um array de BlackBoard_VO_OrganizationFileVO mas enviou um array de " . get_class($vo));
                }
            }

            $blackboard = new BlackBoard_Service_Organization();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
// 			$blackboard->debug = true;
            $blackboard->debug = $this->debug;

            $save = $blackboard->createOrganization(new BlackBoard_VO_OrganizationFileVO(), $arrVO);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

    /**
     * Verifica uma Organização/Comunidade no BlackBoard por ID
     * @param string $id_saladeaula - G2_XXX ou G2_XX_XXXX
     * @throws Exception
     */
    public function verificarOrganizacao($id_saladeaula)
    {

        try {

            $blackboard = new BlackBoard_Service_Course();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;

            $filter = new BlackBoard_VO_CourseFilterVO();
            $filter->setFilterType(BlackBoard_VO_CourseFilterVO::GET_COURSE_BY_COURSE_ID);
            $filter->setCourseIds(array($id_saladeaula));
            return $blackboard->getOrg($filter);

        } catch (Exception $e) {
            throw $e;

        }

    }


    /**
     * Altera um Curso ou uma Comunidade
     * @param SalaDeAulaTO $to
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function salvarCurso(SalaDeAulaTO $to)
    {

        try {

            $blackboard = new BlackBoard_Service_Course();
            $this->setKeys($blackboard);
            $blackboard->debug = $this->debug;
            $blackboard->iniciarServico();


            $si = new SalaDeAulaIntegracaoTO();
            $si->setId_saladeaula($to->getId_saladeaula());
            $si->setId_sistema(SistemaTO::BLACKBOARD);
            $si->fetch(false, true, true);

            if ($si->getId_saladeaulaintegracao() == false || ($si->getSt_codsistemacurso() == 'AGUARDANDOBLACKBOARD' || $si->getSt_codsistemacurso() == 'AGUARDANDOVERIFICACAO')) {
                return new Ead1_Mensageiro("Essa Sala de Aula ainda não foi Integrada ou Verificada no BlackBoard", Ead1_IMensageiro::SUCESSO);
            }

            $jsonCurso = json_decode($si->getSt_retornows());
            if ($jsonCurso->courseServiceLevel == "Organization") {
                $curso = $this->verificarOrganizacao($si->getSt_codsistemasala());
                if ($curso->getTipo() != Ead1_IMensageiro::SUCESSO || $curso->getMensagem('Organizations') == false) {
                    throw new BlackBoard_Exception('Comunidade não encontrado no BlackBoard');
                }

                $vo = new BlackBoard_VO_CourseVO();
                $vo->buildVOfromXml($curso->getMensagem('Organizations'));
            } else {
                $curso = $this->getCourse($si->getSt_codsistemasala());
                if ($curso->getTipo() != Ead1_IMensageiro::SUCESSO || $curso->getMensagem('Courses') == false) {
                    throw new BlackBoard_Exception('Curso não encontrado no BlackBoard');
                }

                $vo = new BlackBoard_VO_CourseVO();
                $vo->buildVOfromXml($curso->getMensagem('Courses'));
            }

            /**
             * Alterando por enquanto apenas Nome e Situação
             */
            $vo->setName($to->getSt_saladeaula());
            $vo->setAvailable($to->getId_situacao() == 8 ? 'true' : 'false');
//			$blackboard->debug = true;
            $save = $blackboard->saveCourse($vo);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {

                if ($vo->getCourseId()) {
                    /**
                     * Salvando os Grupos
                     */
                    $grupos = NULL;
                    $aps = new VwAreaProjetoSalaTO();
                    $aps->setId_saladeaula($to->getId_saladeaula());
                    $grupos = $aps->fetch(false, false, false);
// 					Zend_Debug::dump($grupos,__CLASS__.'('.__LINE__.')');exit;
                    if ($grupos) {
                        foreach ($grupos as $grupo) {
                            if (!$grupo->getSt_referencia()) {

                                $vog = new BlackBoard_VO_GroupVO();
                                $vog->setCourseId($vo->getId());
                                $vog->setTitle($grupo->getSt_projetopedagogico());
                                $vog->setDescription($grupo->getSt_descricao());
                                $vog->setAvailable('true');

                                $megrupo = $this->salvarGrupo($vog, $grupo->getId_areaprojetosala());
                                if ($megrupo->getTipo() != Ead1_IMensageiro::SUCESSO) {
                                    throw new BlackBoard_Exception("Erro ao salvar o Grupo " . $grupo->getSt_projetopedagogico() . "</strong> Erro: " . $megrupo->getFirstMensagem());
                                }

                                $arrretorno[] = "Grupo: <strong>{$grupo->getSt_projetopedagogico()}</strong>  Criado com sucesso!";

                            }//if($grupo->getSt_referencia()==false){
                        }//foreach($grupos as $grupo){
                    }//if($grupos){
                }//if($vo->getCourseId()){

                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }
        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * @param unknown_type $group_id
     * @param unknown_type $id_areaprojetosala
     */
    public function salvarDadosGrupoG2($group_id, $id_areaprojetosala)
    {
        $dao = new SalaDeAulaDAO();
        $dao->beginTransaction();
        try {
            $aps = new AreaProjetoSalaTO();
            $aps->setId_areaprojetosala($id_areaprojetosala);
            $aps->fetch(false, true, true);
            $aps->setSt_referencia($group_id);

            $dap = $dao->editarAreaProjetoSala($aps);
            if (!$dap)
                throw new \Zend_Exception("Erro ao salvar a AreaProjetoSala");

            $apsi = new AreaProjetoSalaIntegracaoTO();
            $apsi->montaToDinamico($aps->toArray());
            $apsi->fetch(false, true, true);

            $apsi->setId_sistema(SistemaTO::BLACKBOARD);
            if ($apsi->getId_areaprojetosalaintegracao()) {
//                die('if');
                $dapi = $dao->editarAreaProjetoSalaIntegracao($apsi);
            } else {
//                die('else');
                $dapi = $dao->cadastrarAreaProjetoSalaIntegracao($apsi);
            }

            if (!$dapi)
                throw new \Zend_Exception("Erro ao salvar a AreaProjetoSalaIntegracao");

            $dao->commit();
        } catch (\Exception $e) {
            $dao->rollBack();
            throw $e;
        }


    }

    /**
     * Cria um Grupo no BlackBoard
     * @param BlackBoard_VO_GroupVO $vo
     * @param int $id_areaprojetosala
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function salvarGrupo(BlackBoard_VO_GroupVO $vo, $id_areaprojetosala)
    {

        try {
            $blackboard = new BlackBoard_Service_Course();
            $blackboard->debug = $this->debug;
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $group = null;


            if ($vo->getId() == false) {
                $fvo = new BlackBoard_VO_GroupFilterVO();
                $fvo->setFilterType(BlackBoard_VO_GroupFilterVO::GET_GROUP_BY_COURSE_ID);
                $blackboard->debug = $this->debug;
                $mgroup = $blackboard->getGroup($vo->getCourseId(), $fvo);
                if ($mgroup->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                    $argroup = $mgroup->getMensagem();
                    $arrayKeyResult = array_search(trim($vo->getTitle()), array_column($argroup, 'title'));
                    //busca no array de grupos a posição do registro que tem o titulo passado por $vo->getTitle()
                    if (is_integer($arrayKeyResult) && array_key_exists($arrayKeyResult, $argroup)) {
                        $this->salvarDadosGrupoG2($argroup[$arrayKeyResult]['id'], $id_areaprojetosala);
                        return new Ead1_Mensageiro(array('GroupID' => $argroup[$arrayKeyResult]['id']), Ead1_IMensageiro::SUCESSO);
                    }
                }
                $vo->setDescription($vo->getDescription() ? $vo->getDescription() : '.');
                $save = $blackboard->saveGroup($vo);
                if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                    $this->salvarDadosGrupoG2($save->getMensagem('GroupID'), $id_areaprojetosala);
                    return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
                } else {
                    throw new BlackBoard_Exception($save->getMensagem());
                }
            }
        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }


    }

    /**
     * Retorna o Usuário do BlackBoard pelo id_usuario
     * @param int $id_entidade
     * @param int $id_usuario
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function getUser($id_entidade, $id_usuario)
    {

        try {

            $blackboard = new BlackBoard_Service_User();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;

            $filter = new BlackBoard_VO_UserFilterVO();
            $filter->setBatchId(array($id_entidade . '_' . $id_usuario));
            $filter->setFilterType(BlackBoard_VO_UserFilterVO::GET_USER_BY_BATCH_ID_WITH_AVAILABILITY);
            $retorno = $blackboard->getUser($filter);
            if ($retorno->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                $vo = new BlackBoard_VO_UserVO();
                $vo->buildVOfromXml($retorno->getMensagem('Users'));
                return new Ead1_Mensageiro($vo, Ead1_IMensageiro::SUCESSO);
            }
            return new Ead1_Mensageiro($retorno->getMensagem(), Ead1_IMensageiro::ERRO);
        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }


    }


    /**
     * Salva o Usuário no BB
     * @param BlackBoard_VO_UserVO $vo
     * @throws Exception
     * @return void|Ead1_Mensageiro|Ambigous <string, Ead1_Mensageiro>
     */
    public function salvarUsuario(BlackBoard_VO_UserVO $vo, $getuser = true)
    {

        try {
            $blackboard = new BlackBoard_Service_User();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;

            if ($getuser) {
                $filter = new BlackBoard_VO_UserFilterVO();
                $filter->setBatchId(array($vo->getUserBatchUid()));
                $filter->setFilterType(BlackBoard_VO_UserFilterVO::GET_USER_BY_BATCH_ID_WITH_AVAILABILITY);
                $resposta = $blackboard->getUser($filter);
                if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $userBB = $resposta->getMensagem();
                    $vo->setId($userBB['Users']['id']);

                    $vo->setEducationLevel($userBB['Users']['educationLevel']);
                    $vo->setExpansionData($userBB['Users']['expansionData']);
                    if (isset($userBB['Users']['insRoles']) && $userBB['Users']['insRoles']) {
                        $role = $vo->getInsRoles();
                        if (is_string($userBB['Users']['insRoles']) && $userBB['Users']['insRoles'] != $role[0]) {
                            $vo->setInsRoles(array($userBB['Users']['insRoles'], $role[0]));
                        }
                        if (is_array($userBB['Users']['insRoles'])) {
                            if (!in_array($role[0], $userBB['Users']['insRoles'])) {
                                $roles = $userBB['Users']['insRoles'];
                                array_push($roles, $role[0]);
                                $vo->setInsRoles($roles);
                            }
                        }
                    }
                    if (isset($userBB['Users']['systemRoles']) && $userBB['Users']['systemRoles']) {
                        $role = $vo->getSystemRoles();
                        if (isset($role[0]) == false) {
                            $role[0] = false;
                        }
                        if (is_string($userBB['Users']['systemRoles']) && $userBB['Users']['systemRoles'] != $role[0]) {
                            if ($role[0] == false) {
                                $vo->setSystemRoles(array($userBB['Users']['systemRoles']));
                            } else {
                                $vo->setSystemRoles(array($userBB['Users']['systemRoles'], $role[0]));
                            }
                        }
                        if (is_array($userBB['Users']['systemRoles'])) {
                            if (!in_array($role[0], $userBB['Users']['systemRoles']) && $role[0] != false) {
                                $roles = $userBB['Users']['systemRoles'];
                                array_push($roles, $role[0]);
                                $vo->setSystemRoles($roles);
                            } else {
                                $vo->setSystemRoles($userBB['Users']['systemRoles']);
                            }
                        }
                    }
                }
            }

            $ar = explode('_', $vo->getUserBatchUid());
            $roles = $vo->getInsRoles();
            if (!in_array($ar[0], $roles)) {
                array_unshift($roles, $ar[0]);
                $vo->insRoles = $roles;
            }
            $result = array_unique($vo->getInsRoles());
            if ($result) {
                $vo->insRoles = null;
                foreach ($result as $role) {
                    $vo->insRoles[] = $role;
                }
            }
            $save = $blackboard->saveUser($vo);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }
        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Matricula um Aluno
     * @param BlackBoard_VO_CourseMembershipVO $vo
     * @throws Exception
     * @return Ambigous <Ambigous, Ead1_Mensageiro>|Ead1_Mensageiro
     */
    public function salvarPessoaCurso(BlackBoard_VO_CourseMembershipVO $vo, $inativando = false)
    {

        try {

            $blackboard = new BlackBoard_Service_CourseMembership();
            $blackboard->debug = $this->debug;
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();


            $mvo = new BlackBoard_VO_MembershipFilterVO();
            $mvo->setFilterType(BlackBoard_VO_MembershipFilterVO::GET_BY_COURSE_AND_USER_ID);
            $mvo->setCourseIds(array($vo->getCourseId()));
            $mvo->setUserIds(array($vo->getUserId()));
            $mensageiromember = $blackboard->getCourseMembership($vo->getCourseId(), $mvo);

            if ($inativando) {
                if ($mensageiromember->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    return new Ead1_Mensageiro($mensageiromember->getMensagem() . " - Usuário ID " . $vo->getUserId(), Ead1_IMensageiro::AVISO);
                }
            }

            $member = $mensageiromember->getMensagem();

            if (isset($member['CourseMembership']['id']) && $member['CourseMembership']['id']) {
                $vo->setId($member['CourseMembership']['id']);
                if (isset($member['CourseMembership']['roleId']) && $member['CourseMembership']['roleId']) {
                    $role = $vo->getRoleId();
                    if (is_string($member['CourseMembership']['roleId']) && $member['CourseMembership']['roleId'] != $role[0]) {
                        $vo->setRoleId(array($member['CourseMembership']['roleId'], $role[0]));
                    }
                    if (is_array($member['CourseMembership']['roleId'])) {
                        if (!in_array($role[0], $member['CourseMembership']['roleId'])) {
                            $roles = $member['CourseMembership']['roleId'];
                            array_push($roles, $role[0]);
                            $vo->setRoleId($roles);
                        }
                    }

                }
            }
            $vo->setDataSourceId($this->key->datasourceid);
            $save = $blackboard->saveCourseMembership($vo);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                return new Ead1_Mensageiro($save->getMensagem() . " - Usuário ID " . $vo->getUserId(), Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage() . " - Usuário ID " . $vo->getUserId(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     *
     * NAO USADO POIS O OBSERVADOR É EM OUTRO LUGAR
     * Salva o Observador
     * @param BlackBoard_VO_ObserverAssociationVO $vo
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function salvarObservador(BlackBoard_VO_ObserverAssociationVO $vo)
    {

        try {

            $blackboard = new BlackBoard_Service_User();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;
// 			$blackboard->debug = true;
// 			Zend_Debug::dump($vo,__CLASS__.'('.__LINE__.')');
// // 			$voFilter = clone $vo;
// // 			$voFilter->setObserverId(null);

// 			$member = $blackboard->getObservee($vo);
// 			Zend_Debug::dump($member,__CLASS__.'('.__LINE__.')');


            $save = $blackboard->saveObserverAssociation($vo);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }

        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     *
     * Insere um usuário a um Grupo
     * @param int $courseId
     * @param BlackBoard_VO_GroupMembershipVO $vo
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function incluirUsuarioGrupo($courseId, BlackBoard_VO_GroupMembershipVO $vo)
    {

        try {

            $blackboard = new BlackBoard_Service_CourseMembership();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;
            $grupocerto = false;
            $verificando = $this->getGroupMembership($courseId, $vo->getGroupId());
            if ($verificando->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                $retorno = $verificando->getMensagem();
                if (isset($retorno['GroupMembership']['groupMembershipId'])) {
                    if ($retorno['GroupMembership']['courseMembershipId'] == $vo->getCourseMembershipId()) {
                        $grupocerto = $retorno['GroupMembership'];
                    };
                } else if (isset($retorno['GroupMembership'][0]['courseMembershipId'])) {
                    foreach ($retorno['GroupMembership'] as $groupmember) {
                        if ($groupmember['courseMembershipId'] == $vo->getCourseMembershipId()) {
                            $grupocerto = $groupmember;
                        };
                    }
                }
                if ($grupocerto) {
                    return new Ead1_Mensageiro(array('GroupMembershipId' => $grupocerto['groupMembershipId']), Ead1_IMensageiro::SUCESSO);
                }
            }

            $save = $blackboard->saveGroupMembership($courseId, $vo);
            if ($save->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                return new Ead1_Mensageiro($save->getMensagem(), Ead1_IMensageiro::SUCESSO);
            } else {
                throw new BlackBoard_Exception($save->getMensagem());
            }
        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     *
     * Cadastra uma Pessoa no BlackBoard
     * @param array $pessoa
     * @param string $tipo -- _aluno || _professor
     * @throws Zend_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuarioBlackBoard(array $pessoa, $tipo = '_aluno')
    {

        $dao = new PessoaDAO();
        $dao->beginTransaction();
        try {

            $ui = new UsuarioIntegracaoTO();
            if (isset($pessoa['id_usuariointegracao']) && $pessoa['id_usuariointegracao']) {
                $ui->montaToDinamico($pessoa);
                $ui->fetch(true, true, true);
            }

            /**
             * Cadastrar Usuário no BlackBoard
             */
            $usuarioTO = new UsuarioTO();
            $usuarioTO->setId_usuario($pessoa['id_usuario']);
            $usuarioTO->fetch(true, true, true);
            $acesso = $dao->retornarDadosAcessoValidos(new UsuarioTO(array('id_usuario' => $pessoa['id_usuario'])), $pessoa['id_entidade']);

            if ($acesso->getSt_login() == false) {
                $usuario = $pessoa['id_entidade'] . '_' . $usuarioTO->getSt_login();
                $senha = 123456;
            } else {
                $usuario = $acesso->getId_entidade() . '_' . $acesso->getSt_login();
                $senha = $acesso->getSt_senha();
            }

            /**
             * @todo
             * Criar um método para pegar estes dados recebendo a entidade
             */
            $userInfo = new BlackBoard_VO_UserExtendedInfoVO();
            $user = new BlackBoard_VO_UserVO();


            if ($tipo == '_observador' || $tipo == '_coordenador') {
                $user->setInsRoles(array($pessoa['id_entidade'], $pessoa['id_entidade'] . '_professor')); // O Corodenador e Observador Institucional recebem estes dois papeis
            } else {
                $user->setInsRoles(array($pessoa['id_entidade'] . $tipo));
            }

            $user->setName($usuario);
            $user->setIsAvailable('true');
            $user->setPassword($senha);
            $user->setUserBatchUid($pessoa['id_entidade'] . '_' . $usuarioTO->getId_usuario());


            $userInfo->setFamilyName('.'); // Sempre "."

            $userInfo->setGivenName($pessoa['st_nomecompleto']);
            $userInfo->setEmailAddress($pessoa['st_email']);
            $user->setExtendedInfo($userInfo);

            if ($ui->getId_usuariointegracao() == false) {

                $resposta = $this->salvarUsuario($user);
                if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $userBB = $resposta->getMensagem();

                    if (!is_string($userBB['UserId'])) {
                        throw new Zend_Exception("Não foi possível salvar o Usuário no BlackBoard, o método saveUser retornou NULL no UserId! FORAM CRIADAS AS FUNÇÕES DE INSTITUIÇÃO NO BLACKBOARD? " . $pessoa['id_entidade'] . ", " . $pessoa['id_entidade'] . "_aluno e " . $pessoa['id_entidade'] . "_professor ?");
                    }

                    /**
                     * Cadastra a Integração do Usuário
                     */
                    $ui = new UsuarioIntegracaoTO();
                    $ui->setId_usuario($usuarioTO->getId_usuario());
                    $ui->setId_sistema(SistemaTO::BLACKBOARD);
                    $ui->setId_entidade($pessoa['id_entidade']);
                    $ui->fetch(false, true, true);
                    if (!$ui->getId_usuariointegracao()) {

                        $ui->setId_usuario($usuarioTO->getId_usuario());
                        $ui->setId_sistema(SistemaTO::BLACKBOARD);
                        $ui->setId_entidade($pessoa['id_entidade']);

                        $ui->setSt_loginintegrado($usuario);
                        $ui->setSt_senhaintegrada($senha);
                        $ui->setId_usuariocadastro(($ui->getSessao()->id_usuario ? $ui->getSessao()->id_usuario : 1));
                        $ui->setSt_codusuario($userBB['UserId']);

                        $usuarioIntegracaoORM = new UsuarioIntegracaoORM();
                        $ui->setId_usuariointegracao($usuarioIntegracaoORM->insert($ui->toArrayInsert()));
                        if (!$ui->getId_usuariointegracao()) {
                            throw new Zend_Exception('Erro ao cadastrar o Usuário Integração.');
                        }

                    }

                } else { //if($resposta->getTipo()==Ead1_IMensageiro::SUCESSO){
                    throw new Zend_Exception("Não foi possível salvar o Usuário no BlackBoard!");
                }

            } else {

                $resposta = $this->salvarUsuario($user);
                if ($resposta->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $userBB = $resposta->getMensagem();
                    if (!is_string($userBB['UserId'])) {
                        throw new Zend_Exception("Não foi possível salvar o Usuário no BlackBoard, o método saveUser retornou NULL no UserId! FORAM CRIADAS AS FUNÇÕES DE INSTITUIÇÃO NO BLACKBOARD? " . $pessoa['id_entidade'] . ", " . $pessoa['id_entidade'] . "_aluno e " . $pessoa['id_entidade'] . "_professor ?");
                    }
                }

            }//if($ui->getId_usuariointegracao()==false){

            $dao->commit();
            return new Ead1_Mensageiro($ui, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Processo de Matrícula de um Aluno
     * @param array $aluno
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function processoMatricularAluno(array $aluno, VwAlocacaoTO $vw, $groupId)
    {

        $dao = new PessoaDAO();
// 		$dao->beginTransaction();
        try {
//            $this->debug = true;
            $si = new SalaDeAulaIntegracaoTO();
            $si->montaToDinamico($aluno);

            $curso = json_decode($si->getSt_retornows());
            if (!$curso) {
                throw new Zend_Exception("Não foi possível pegar os dados do Curso do BlackBoard (BLACKBOARDBO 776) !");
            }

            $ui = $this->cadastrarUsuarioBlackBoard($aluno, '_aluno')->getFirstMensagem();

            $cm = new BlackBoard_VO_CourseMembershipVO();
            $cm->setAvailable('true');
            $cm->setCourseId($curso->id);
            $cm->setRoleId('S');
            $cm->setUserId($ui->getSt_codusuario());

            $matricula = $this->salvarPessoaCurso($cm);
            if (!$matricula->getTipo() == Ead1_IMensageiro::SUCESSO) {
                throw new BlackBoard_Exception("Erro ao adicionar o Aluno " . $aluno['st_nomecompleto'] . " ao curso: " . $matricula->getMensagem());
            }
            $matriculaBB = $matricula->getMensagem();

            $this->_salvarAlocacaoIntegracao($vw->getId_alocacao(), $matriculaBB['CourseMembershipId']);


            $gm = new BlackBoard_VO_GroupMembershipVO();
            $gm->setCourseMembershipId($matriculaBB['CourseMembershipId']);
            $gm->setGroupId($groupId);

            $matricula = $this->incluirUsuarioGrupo($curso->id, $gm);
            if (!$matricula->getTipo() == Ead1_IMensageiro::SUCESSO) {
                throw new BlackBoard_Exception($matricula->getMensagem());
            }
            $matriculaGrupo = $matricula->getMensagem();

            $this->_salvarAlocacaoIntegracao($vw->getId_alocacao(), $matriculaBB['CourseMembershipId'], $matriculaGrupo['GroupMembershipId']);

// 			$dao->commit();
            return new Ead1_Mensageiro("Aluno Matriculado com Sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
// 			$dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     *
     * Salva a AlocacaoIntegracao para estes casos do BB
     * @param $id_alocacao
     * @param $CourseMembershipId
     * @param null $GroupMembershipId
     * @return bool
     * @throws Exception
     */
    private function _salvarAlocacaoIntegracao($id_alocacao, $CourseMembershipId, $GroupMembershipId = null)
    {

        try {

            $alocacaoIntegracaoTO = new AlocacaoIntegracaoTO();
            $alocacaoIntegracaoTO->setId_sistema(SistemaTO::BLACKBOARD);
            $alocacaoIntegracaoTO->setId_alocacao($id_alocacao);
            $alocacaoIntegracaoTO->fetch(null, true, true);
            if (!$alocacaoIntegracaoTO->getId_alocacaointegracao()) {
                $alocacaoIntegracaoTO->setId_usuariocadastro(($alocacaoIntegracaoTO->getSessao()->id_usuario ? $alocacaoIntegracaoTO->getSessao()->id_usuario : 1));
                $alocacaoIntegracaoTO->setSt_codalocacao($CourseMembershipId);
                $alocacaoIntegracaoTO->setSt_codalocacaogrupo($GroupMembershipId);
                $alocacaoIntegracaoORM = new AlocacaoIntegracaoORM();
                $alocacaoIntegracaoTO->setId_alocacaointegracao($alocacaoIntegracaoORM->insert($alocacaoIntegracaoTO->toArrayInsert(), false));
            } else {
                $alocacaoIntegracaoTO->setSt_codalocacao($CourseMembershipId);
                $alocacaoIntegracaoTO->setSt_codalocacaogrupo($GroupMembershipId);
                $alocacaoIntegracaoORM = new AlocacaoIntegracaoORM();
                $alocacaoIntegracaoORM->update($alocacaoIntegracaoTO->toArrayUpdate(null, array('id_alocacaointegracao')), 'id_alocacaointegracao = ' . $alocacaoIntegracaoTO->getId_alocacaointegracao());
            }

            return true;

        } catch (Exception $e) {
            throw $e;
        }

    }


    /**
     * Cadastra um Professor no BlackBoard
     * @param UsuarioPerfilEntidadeReferenciaTO $up
     * @param SalaDeAulaIntegracaoTO $si
     * @throws Zend_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function processoCadastrarProfessor(UsuarioPerfilEntidadeReferenciaTO $up, SalaDeAulaIntegracaoTO $si, $tipo = '_professor')
    {

        $dao = new PessoaDAO();
        $dao->beginTransaction();
        try {

            if ($tipo == '_professor') {
                $cadastro = "Professor";
            } else {
                $cadastro = "Coordenador";
            }

            $curso = json_decode($si->getSt_retornows());
            if (!$curso) {
                throw new Zend_Exception("BlackBoardBO::processoCadastrarProfessor Não foi possível pegar os dados do Curso do BlackBoard!");
            }

            $pessoa = $dao->retornarVwPessoa(new VwPessoaTO(array('id_usuario' => $up->getId_usuario(), 'id_entidade' => $up->getId_entidade())));
            if (!$pessoa) {
                throw new Zend_Exception("BlackBoardBO::processoCadastrarProfessor Não foi possível recuperar os dados da Pessoa!");
            }

            $dados = $pessoa->toArray();

            $peTO = new VwPessoaTO();
            $peTO->montaToDinamico($dados[0]);

            $ui = new UsuarioIntegracaoTO();
            $ui->setId_entidade($peTO->getid_entidade());
            $ui->setId_sistema(SistemaTO::BLACKBOARD);
            $ui->setId_usuario($peTO->getid_usuario());
            $ui->fetch(false, true, true);
            if ($ui->getId_usuariointegracao() == false) {
                $professor = $dados[0];
            } else {
                $arrui = $ui->toArray();
                $professor = $dados[0] + $arrui;
            }

            $ui = $this->cadastrarUsuarioBlackBoard($professor, $tipo)->getFirstMensagem();

            $cm = new BlackBoard_VO_CourseMembershipVO();
            $cm->setAvailable('true');
            $cm->setCourseId($curso->id);
            $cm->setRoleId('P');
            $cm->setUserId($ui->getSt_codusuario());

            $matricula = $this->salvarPessoaCurso($cm);
            if ($matricula->getTipo() != Ead1_IMensageiro::SUCESSO) {
                throw new BlackBoard_Exception("BlackBoardBO::Erro ao adicionar o Professor " . $peTO->getSt_complemento() . " ao curso: " . $matricula->getMensagem());
            }
            $matriculaBB = $matricula->getMensagem();
            if (isset($matriculaBB['CourseMembershipId']) && $matriculaBB['CourseMembershipId']) {

                $moodleDAO = new MoodleDAO();
                $to = new PerfilReferenciaIntegracaoTO();
                $to->setId_perfilreferencia($up->getId_perfilReferencia());
                $to->setId_sistema(SistemaTO::BLACKBOARD);
                $to->setId_saladeaulaintegracao($si->getId_saladeaulaintegracao());
                $to->fetch(false, true, true);

                $to->setId_usuariocadastro(1);
                $to->setSt_codsistema('{"CourseMembershipId":"' . $matriculaBB['CourseMembershipId'] . '"}');
                $salvar = $moodleDAO->salvarPerfilReferencia($to);

                $dao->commit();

                return new Ead1_Mensageiro("$cadastro cadastrado com Sucesso!", Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Zend_Exception("BlackBoardBO::Não foi possível salvar o $cadastro no BlackBoard, este Curso de ID " . $si->getSt_codsistemacurso() . " realmente existe na Plataforma?");
            }

        } catch (Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Faz o cadastro do Observador
     * @param UsuarioPerfilEntidadeReferenciaTO $up
     * @throws Zend_Exception
     * @throws BlackBoard_Exception
     * @throws Exception
     * @return Ead1_Mensageiro
     */
    public function processoCadastrarObservador(UsuarioPerfilEntidadeReferenciaTO $up)
    {

        $dao = new PessoaDAO();
        try {

            $pessoa = $dao->retornarVwPessoa(new VwPessoaTO(array('id_usuario' => $up->getId_usuario(), 'id_entidade' => $up->getId_entidade())));
            if (!$pessoa) {
                throw new Zend_Exception("Não foi possível pegar os dados da Pessoa!");
            }

            $dados = $pessoa->toArray();

            $peTO = new VwPessoaTO();
            $peTO->montaToDinamico($dados[0]);

            $ui = new UsuarioIntegracaoTO();
            $ui->setId_entidade($peTO->getid_entidade());
            $ui->setId_sistema(SistemaTO::BLACKBOARD);
            $ui->setId_usuario($peTO->getid_usuario());
            $ui->fetch(false, true, true);
            $arrui = $ui->toArray();
            $professor = $dados[0] + $arrui;

            $mensageiro = $this->cadastrarUsuarioBlackBoard($professor, '_observador');
            if (!$mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                throw new BlackBoard_Exception($mensageiro->getMensagem());
            }

            $moodleDAO = new MoodleDAO();

            $to = new PerfilReferenciaIntegracaoTO();
            $to->setId_perfilreferencia($up->getId_perfilReferencia());
            $to->setId_sistema(SistemaTO::BLACKBOARD);
            $to->fetch(false, true, true);

            $to->setId_usuariocadastro(1);
            $to->setSt_codsistema('{"UserID":"' . $ui->getSt_codusuario() . '"}');
            $salvar = $moodleDAO->salvarPerfilReferencia($to);

            return new Ead1_Mensageiro("Observador vinculado com sucesso!", Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Faz a integração das Salas como BlackBoard. É necessário usar o método verificarSalas() para verificar se a mesma foi sincronizada.
     * @return Ead1_Mensageiro
     */
    public function integrarSalas()
    {

        try {

            $arrVO = null;
            $arrOrganizationVO = null;
            $arrretorno = null;
            $arrsalas = null;
            $arrcomunidades = null;
            $bo = new SalaDeAulaBO();
            $dao = new SalaDeAulaDAO();
            $salas = $dao->retornaSalasBlackBoard("sa.id_entidade = {$this->id_entidade} and id_sistema = 15 and si.st_codsistemacurso = 'AGUARDANDOBLACKBOARD'");
            if ($salas) {

                foreach ($salas as $sala) {

                    switch ($sala['id_tipodisciplina']) {
                        case TipoDisciplinaTO::AMBIENTACAO:
                            $tipo = "Comunidade";

                            $vo = new BlackBoard_VO_OrganizationFileVO();
                            $vo->setExternal_organization_key($sala['id_saladeaula']);
                            $vo->setOrganization_id($this->prefix_sala . $sala['id_saladeaula']);
                            $vo->setOrganization_name($sala['st_saladeaula']);
// 							$vo->setAvailable_ind('Y');
                            $vo->setAvailable_ind('N');
                            $vo->setRow_status('ENABLED');
                            $vo->setTemplate_organization_key('Template_Comunidade_Turma');
                            if ($sala['id_tiposaladeaula'] == TipoSalaDeAulaTO::SALA_PERMANENTE) {
                                $vo->setDuration('continuous');
                            } else {
                                if ($sala['dt_abertura'] == false) {
                                    $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> não tem data de abertuda.";
                                    continue;
                                }
                                $vo->setDuration('range');
                                $vo->setStart_date(($sala['dt_abertura'] instanceof \DateTime ? $sala['dt_abertura'] : new DateTime($sala['dt_abertura'])));
                                if ($sala['dt_encerramento'])
                                    $vo->setEnd_date(($sala['dt_encerramento'] instanceof \DateTime ? $sala['dt_encerramento'] : new DateTime($sala['dt_encerramento'])));

                            }
                            //     								$vo->setDays_of_use($sala['nu_diasaluno']);
                            $arrOrganizationVO[] = $vo;
                            $arrcomunidades[] = $sala;

                            break;

                        default:
                            $tipo = "Sala de Aula";
                            $vo = new BlackBoard_VO_CourseFileVO();
                            $vo->setExternal_course_key($sala['id_saladeaula']);
                            $vo->setCourse_id($this->prefix_sala . $sala['id_saladeaula']);
                            $vo->setCourse_name($sala['st_saladeaula']);
// 							$vo->setAvailable_ind('Y');
                            $vo->setAvailable_ind('N');
                            $vo->setRow_status('ENABLED');

                            if ($sala['id_tiposaladeaula'] == TipoSalaDeAulaTO::SALA_PERMANENTE) {
                                $vo->setDuration('continuous');
                            } else {
                                if ($sala['dt_abertura'] == false) {
                                    $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> não tem data de abertuda.";
                                    continue;
                                }
                                $vo->setDuration('range');
                                $vo->setStart_date($sala['dt_abertura']);
                                $vo->setEnd_date($sala['dt_encerramento']);
                            }
                            //     								$vo->setDays_of_use($sala['nu_diasaluno']);
                            $arrVO[] = $vo;
                            $arrsalas[] = $sala;

                            break;
                    }//switch
                } //foreach($salas as $sala){

                if ($arrVO) {
                    try {
                        $retorno = $this->criarCurso($arrVO);
                        if ($retorno->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                            foreach ($arrsalas as $sala) { // Salas que foram integradas
                                $to = new SalaDeAulaIntegracaoTO();
                                $to->setId_saladeaulaintegracao($sala['id_saladeaulaintegracao']);
                                $to->setSt_codsistemacurso('AGUARDANDOVERIFICACAO');
                                $to->setSt_codsistemasala($this->prefix_sala . $sala['id_saladeaula']);
                                $to->setSt_retornows(json_encode($retorno->getMensagem()));
                                $mensageiro = $bo->editarSalaDeAulaIntegracao($to);
                                $arrretorno[] = "<strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong>: " . $mensageiro->getFirstMensagem();
                            }
                        }

                    } catch (Exception $e) {
                        $arrretorno[] = " A integração das Salas de Aula retornou o seguinte erro {$e->getMessage()}";
                    }

                } //if($arrVO){
                if ($arrOrganizationVO) {
                    try {

                        $retorno = $this->criarOrganizacao($arrOrganizationVO);
                        if ($retorno->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                            foreach ($arrcomunidades as $sala) { // Salas que foram integradas
                                $to = new SalaDeAulaIntegracaoTO();
                                $to->setId_saladeaulaintegracao($sala['id_saladeaulaintegracao']);
                                $to->setSt_codsistemacurso('AGUARDANDOVERIFICACAO');
                                $to->setSt_codsistemasala($this->prefix_sala . $sala['id_saladeaula']);
                                $to->setSt_retornows(json_encode($retorno->getMensagem()));
                                $mensageiro = $bo->editarSalaDeAulaIntegracao($to);
                                $arrretorno[] = "<strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong>: " . $mensageiro->getFirstMensagem();
                            }
                        }

                    } catch (Exception $e) {
                        $arrretorno[] = " A integração das Comunidades retornou o seguinte erro {$e->getMessage()} <br>";
                    }
                }//if($arrOrganizationVO){

            } else {//$salas
                return new Ead1_Mensageiro("Nenhum registro encontrado para Integração.", Ead1_IMensageiro::AVISO);
            }

        } catch (Exception $e) {
            $arrretorno[] = $e->getMessage();
        }

        return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

    }

    /**
     * Verifica se as salas de aula de uma entidade foram cadastradas com sucesso no BlackBoard
     * @return Ead1_Mensageiro
     */
    public function verificarSalas($id_saladeaula = null)
    {

        try {
            $bo = new SalaDeAulaBO();
            $dao = new SalaDeAulaDAO();
            $salas = $dao->retornaSalasBlackBoard("sa.id_entidade = {$this->id_entidade } and id_sistema = 15 and aps.st_referencia is null and si.st_codsistemacurso != 'AGUARDANDOBLACKBOARD'");
            if ($salas) {
                foreach ($salas as $iSala => $sala) {
                    $grupos = NULL;
                    $aps = new VwAreaProjetoSalaTO();
                    $aps->setId_saladeaula($sala['id_saladeaula']);
                    $grupos = $aps->fetch(false, false, false);
                    if (!$grupos) {
                        $arrretorno[] = "A Sala ou Comunidade <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> não possui nenhum Projeto Pedagógico vinculado. Eles sào necessários para a criação dos Grupos no BlackBoard! <font color=red>Registro IGNORADO</font>!";
                        continue;
                    }

                    $course = null;
                    $tipo = null;
                    switch ($sala['id_tipodisciplina']) {
                        case TipoDisciplinaTO::AMBIENTACAO:
                            $tipo = "Comunidade";
                            $retorno = $this->verificarOrganizacao($sala['st_codsistemasala']);
                            if ($retorno->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                                $mcourse = $retorno->getMensagem();
                                $course = $mcourse['Organizations'];
                            } else {
                                $arrretorno[] = "<strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong>: " . $retorno->getMensagem();
                            }
                            break;
                        default:
                            $tipo = "Sala de Aula";
                            $retorno = $this->verificarCurso($sala['st_codsistemasala']);
                            if ($retorno->getTipo() == BlackBoard_Mensageiro::SUCESSO) {
                                $mcourse = $retorno->getMensagem();
                                $course = $mcourse['Courses'];
                            } else {
                                $arrretorno[] = "<strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong>: " . $retorno->getMensagem();
                            }
                            break;
                    }//switch ($sala['id_tipodisciplina']) {

                    if (isset($course['courseId'])) {

                        $to = new SalaDeAulaIntegracaoTO();
                        $to->setId_saladeaulaintegracao($sala['id_saladeaulaintegracao']);
                        $to->setSt_codsistemacurso($course['courseId']);
                        $to->setSt_codsistemasala($course['courseId']);
                        $to->setSt_retornows(json_encode($course));

                        $mensageiro = $bo->editarSalaDeAulaIntegracao($to);

                        if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> foi atualizada com sucesso!";
                            /**
                             * Criando os Grupos da Sala de Aula
                             */
                            $arrretorno = $this->processaSalvarArrayGrupos($grupos, $course, $tipo, $sala, $arrretorno);

                        } else {
                            $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> não foi atualizada. Erro {$mensageiro->getFirstMensagem()}.";
                        }
                    } else { //if($course){
                        $to = new SalaDeAulaIntegracaoTO();
                        $to->setId_saladeaulaintegracao($sala['id_saladeaulaintegracao']);
                        $to->fetch(true, true, true);

                        $json = json_decode($to->getSt_retornows());

                        $qtd_verificacao = @$json->verificacao;
                        $qtd_verificar = 15;

                        if ($qtd_verificacao >= $qtd_verificar && $to->getId_sistema() == SistemaTO::BLACKBOARD) {


                            $json = '{"verificacao":0}';
                            $to->setSt_retornows($json);
                            $to->setSt_codsistemacurso('AGUARDANDOBLACKBOARD');

                            $mensageiro = $bo->editarSalaDeAulaIntegracao($to);


                            if ($mensageiro->getTipo() == Ead1_IMensageiro::SUCESSO) {
                                $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "(" . $this->prefix_sala . $sala['id_saladeaula'] . ")</strong> foi verificada no BlackBoard $qtd_verificar vezes ou mais. Essa integração atualizada para que possa novamente ser enviada ao BlackBoard. Favor verificar os dados e clicar em Integrar Salas para repetir o processo.";
                            }

// 							$mensageiro 	= $bo->deletarSalaDeAulaIntegracao($to->getId_saladeaulaintegracao());

                        } else {

                            $json = '{"verificacao":' . (++$qtd_verificacao) . '}';
                            $to->setSt_retornows($json);


                            $mensageiro = $bo->editarSalaDeAulaIntegracao($to);


                            $arrretorno[] = "A $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "(" . $this->prefix_sala . $sala['id_saladeaula'] . ")</strong> não foi encontrada no BlackBoard. Verificação " . ($qtd_verificacao) . ".";

                        }

                        if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                            $arrretorno[] = "Erro ao Editar a $tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "(" . $this->prefix_sala . $sala['id_saladeaula'] . ")</strong>." . $mensageiro->getFirstMensagem();

                        }

                    }//if($course){

                }//foreach($salas as $sala){
            } else {//$salas
                return new Ead1_Mensageiro("Nenhum registro encontrado para verificação.", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            $arrretorno[] = $e->getMessage();
        }

        return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

    }

    /**
     * Separando a função de percorrer os grupos e salvar cada um deles
     * @param array $grupos
     * @param $course
     * @param $tipo
     * @param $sala
     * @return array
     * @throws BlackBoard_Exception
     * @throws Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    private function processaSalvarArrayGrupos(array $grupos, $course, $tipo, $sala, array  $arrretorno)
    {
//        $arrReturn = array();
        if ($grupos) {
            foreach ($grupos as $i => $grupo) {
                $vo = new BlackBoard_VO_GroupVO();
                $vo->setCourseId($course['id']);
                $vo->setTitle($grupo->getSt_projetopedagogico());
                $vo->setDescription($grupo->getSt_descricao());
                $vo->setAvailable('true');

                $megrupo = $this->salvarGrupo($vo, $grupo->getId_areaprojetosala());

                if ($megrupo->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    throw new BlackBoard_Exception("$tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> Erro: " . $megrupo->getFirstMensagem());
                }
                $comunidade = $megrupo->getMensagem();

                $arrretorno[] = "$tipo <strong>{$sala['id_saladeaula']} - " . ($sala['st_saladeaula']) . "</strong> Grupo: <strong>{$grupo->getSt_projetopedagogico()}</strong>  Criado com sucesso!";
            }
        }
        return $arrretorno;
    }


    /**
     * Integra os Alunos Pendentes
     * @param int $horas
     * @return Ead1_Mensageiro
     */
    public function integrarAlunos($horas = null)
    {

        $arrretorno = null;
        try {
            $bo = new SalaDeAulaBO();
            $dao = new SalaDeAulaDAO();
            $alocacoes = $dao->retornarAlocacaoPendenteBlackBoard($this->id_entidade, $horas);
            if ($alocacoes) {
                foreach ($alocacoes as $alocacao) {
                    $aTO = new AlocacaoTO();
                    $aTO->montaToDinamico($alocacao);
                    $retorno = $bo->reintegrarAlocacaoAluno($aTO);
                    if ($retorno instanceof Ead1_Mensageiro) {
                        $arrretorno[] = 'Sala  <strong>' . ($alocacao['st_saladeaula']) . '(' . ($alocacao['st_codsistemacurso']) . ')</strong> - Aluno <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno->getFirstMensagem();
                    } else {
                        if (is_string($retorno)) {
                            $arrretorno[] = 'Sala  <strong>' . ($alocacao['st_saladeaula']) . '(' . ($alocacao['st_codsistemacurso']) . ')</strong> - Aluno <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno;
                        } else {
                            $arrretorno[] = $retorno;
                        }
                    }
                }//foreach($alocacoes as $alocacao){
            } else {//$alocacoes
                return new Ead1_Mensageiro("Nenhum registro encontrado para Integração.", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::SUCESSO);
        }

    }

    /**
     * Método que integra os Observadores
     * No caso do BlackBoard é feito apenas o cadastro do usuário, o restante é feito no próprio sistema BlackBoard
     * @return Ead1_Mensageiro
     */
    public function integrarObservador()
    {

        $arrretorno = null;
        try {

            $x = 1;
            $dao = new SalaDeAulaDAO();
            $alocacoes = $dao->retornarObservadoresBlackBoard($this->id_entidade);
            if ($alocacoes) {
                foreach ($alocacoes as $alocacao) {

// 					if($x>5){
// 						continue;
// 					}

                    if ($alocacao['st_codsistema']) {
                        $arrretorno[] = 'Observador <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - Já estava Integrado. Nenhuma ação foi efetuada.';
                        continue;
                    }

                    $aTO = new UsuarioPerfilEntidadeReferenciaTO();
                    $aTO->montaToDinamico($alocacao);
                    $retorno = $this->processoCadastrarObservador($aTO);
                    if ($retorno instanceof Ead1_Mensageiro) {
                        $arrretorno[] = 'Observador <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno->getFirstMensagem();
                    } else {
                        if (is_string($retorno)) {
                            $arrretorno[] = 'Observador <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno;
                        } else {
                            $arrretorno[] = $retorno;
                        }
                    }

                    $x++;
                }//foreach($alocacoes as $alocacao){
            } else {//$alocacoes
                return new Ead1_Mensageiro("Nenhum registro encontrado para Integração.", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
// 			Zend_Debug::dump($e,__CLASS__.'('.__LINE__.')');exit;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::SUCESSO);
        }

    }


    public function integrarProfessores()
    {

        $arrretorno = null;
        try {

            $x = 1;
            $dao = new SalaDeAulaDAO();
            $alocacoes = $dao->retornarProfessoresBlackBoard($this->id_entidade);
            if ($alocacoes) {
                foreach ($alocacoes as $alocacao) {

                    if ($alocacao['id_perfilpedagogico'] == PerfilPedagogicoTO::COORDENADOR_DE_PROJETO_PEDAGOGICO) {
                        $tipo = 'Coordenador';
                    } else {
                        $tipo = 'Professor';
                    }


                    if ($alocacao['st_codsistema']) {
                        $arrretorno[] = ($alocacao['st_saladeaula']) . ' - ' . $tipo . ' <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - Já estava Integrado. Nenhuma ação foi efetuada.';
                        continue;
                    }

                    if ($x > 100) {
                        continue;
                    }

                    $aTO = new UsuarioPerfilEntidadeReferenciaTO();
                    $aTO->montaToDinamico($alocacao);

                    $si = new SalaDeAulaIntegracaoTO();
                    $si->setId_saladeaulaintegracao($alocacao['id_saladeaulaintegracao']);
                    $si->fetch(true, true, true);
                    if ($si->getSt_codsistemacurso() == false) {
                        $arrretorno[] = ($alocacao['st_saladeaula']) . ' - ' . $tipo . ' <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - A Integração da Sala de Aula não foi encontrada.';
                        continue;
                    }

                    $retorno = $this->processoCadastrarProfessor($aTO, $si, ($alocacao['id_perfilpedagogico'] == PerfilPedagogicoTO::COORDENADOR_DE_PROJETO_PEDAGOGICO ? '_coordenador' : '_professor'));
                    if ($retorno instanceof Ead1_Mensageiro) {
                        $arrretorno[] = ($alocacao['st_saladeaula']) . ' - ' . $tipo . ' <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno->getFirstMensagem();
                    } else {
                        if (is_string($retorno)) {
                            $arrretorno[] = ($alocacao['st_saladeaula']) . ' - ' . $tipo . ' <strong>' . ($alocacao['st_nomecompleto']) . '</strong> - ' . $retorno;
                        } else {
                            $arrretorno[] = $retorno;
                        }
                    }

                    $x++;
                }//foreach($alocacoes as $alocacao){
            } else {//$alocacoes
                return new Ead1_Mensageiro("Nenhum registro encontrado para Integração.", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
// 			Zend_Debug::dump($e,__CLASS__.'('.__LINE__.')');exit;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::SUCESSO);
        }

    }


    /**
     * Inativa o Usuário no Curso
     * @return Ead1_Mensageiro
     */
    public function inativarUsuarioCurso($id_alocacao = null)
    {

        $arrretorno = null;
        try {
//			$this->debug = true;
            $x = 1;
            $dao = new SalaDeAulaDAO();
            $alocacoes = $dao->retornarAlunosInativacaoBlackBoard($this->id_entidade, $id_alocacao);
            if ($alocacoes) {
                foreach ($alocacoes as $alocacao) {

                    $curso = json_decode($alocacao['st_retornows']);
                    if (!$curso) {
                        throw new Zend_Exception("Não foi possível pegar os dados do Curso do BlackBoard!");
                    }

                    $cm = new BlackBoard_VO_CourseMembershipVO();
                    $cm->setAvailable('false');
                    $cm->setCourseId($curso->id);
                    $cm->setUserId($alocacao['st_codusuario']);
                    $cm->setRoleId(array('S'));

                    switch ($alocacao['id_perfilpedagogico']) {
                        case PerfilPedagogicoTO::PROFESSOR:
                            $tipo = "Professor";
                            break;
                        case PerfilPedagogicoTO::COORDENADOR_DE_PROJETO_PEDAGOGICO:
                            $tipo = "Coordenador";
                            break;
                        case PerfilPedagogicoTO::COORDENADOR_DE_AREA:
                            $tipo = "Observador";
                            break;
                        default:
                            $tipo = "Aluno";
                            break;
                    }

                    if ($alocacao['id_alocacaointegracao']) {
                        $cm->setId($alocacao['st_codalocacao']);
                    } else {
                        $alocacaojs = json_decode($alocacao['st_codalocacao']);
                        if (!$alocacaojs) {
                            throw new Zend_Exception("Não foi possível pegar os dados do Curso do BlackBoard para o registro do $tipo " . ($alocacao['st_nomecompleto']) . "!");
                        }
                        $cm->setId($alocacaojs->CourseMembershipId);
                    }


                    $dao->beginTransaction();
                    try {

                        $retorno = $this->salvarPessoaCurso($cm, true);
                        if ($retorno->getTipo() == Ead1_IMensageiro::SUCESSO) {
                            $arrretorno[] = $tipo . ' <strong>' . $alocacao['st_nomecompleto'] . '</strong> - Inativado com sucesso na Sala ' . $alocacao['st_saladeaula'] . 'do Curso ID ' . $cm->getCourseId();
                        } else {
                            $arrretorno[] = 'Erro ao Inativar o ' . $tipo . ' <strong>' . $alocacao['st_nomecompleto'] . '</strong> na Sala ' . $alocacao['st_saladeaula'] . '. erro: ' . $retorno->getFirstMensagem() . ' <strong>O Aluno será marcado como Inativado!</strong>';
                        }

                        if ($alocacao['id_alocacaointegracao']) {
                            $orm = new AlocacaoIntegracaoORM();
                            $orm->update(array('bl_encerrado' => 1), array('id_alocacaointegracao = ' . $alocacao['id_alocacaointegracao']));
                        } else {
                            $orm = new PerfilReferenciaIntegracaoORM();
                            $orm->update(array('bl_encerrado' => 1), array('id_perfilreferenciaintegracao = ' . $alocacao['id_perfilreferenciaintegracao']));
                        }
                        $dao->commit();


                    } catch (Exception $e) {
                        $dao->rollBack();
                        $arrretorno[] = 'Erro ao Inativar o ' . $tipo . ' <strong>' . $alocacao['st_nomecompleto'] . '</strong> na Sala ' . $alocacao['st_saladeaula'] . '. erro: ' . $e->getMessage();
                    }

                }//foreach($alocacoes as $alocacao){
            } else {//$alocacoes
                return new Ead1_Mensageiro("Nenhum registro encontrado para Inativação.", Ead1_IMensageiro::AVISO);
            }

            return new Ead1_Mensageiro($arrretorno, Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::SUCESSO);
        }

    }


    /**
     * Retorna os usuários de um Grupo
     * @param $courseID
     * @param $id
     * @param int $filter
     * @return BlackBoard_Mensageiro
     * @throws Exception
     */
    public function getGroupMembership($courseID, $id, $filter = BlackBoard_VO_MembershipFilterVO::GET_BY_GROUP_ID)
    {

        try {
            $blackboard = new BlackBoard_Service_CourseMembership();
            $this->setKeys($blackboard);
            $blackboard->iniciarServico();
            $blackboard->debug = $this->debug;

            $mvo = new BlackBoard_VO_MembershipFilterVO();

            switch ($filter) {

                case BlackBoard_VO_MembershipFilterVO::GET_BY_COURSEMEMBERSHIP_ID:
                    $mvo->setFilterType(BlackBoard_VO_MembershipFilterVO::GET_BY_COURSEMEMBERSHIP_ID);
                    $mvo->setCourseMembershipIds(array($id));
                    break;

                case BlackBoard_VO_MembershipFilterVO::GET_BY_GROUP_ID:
                default:
                    $mvo->setFilterType(BlackBoard_VO_MembershipFilterVO::GET_BY_GROUP_ID);
                    $mvo->setGroupIds(array($id));
                    break;

            }

            return $blackboard->getGroupMembership($courseID, $mvo);

        } catch (Exception $e) {
            throw $e;

        }

// 			Todos funcionando
// 			$mvo = new BlackBoard_VO_MembershipFilterVO();
// 			$mvo->setFilterType(BlackBoard_VO_MembershipFilterVO::GET_BY_COURSE_ID);
// 			$mvo->setCourseIds(array('_218_1'));
// 			$member = $blackboard->getGroupMembership($courseID, $mvo)->getMensagem();
// 			\Zend_Debug::dump($member,__CLASS__.'('.__LINE__.')');


    }

}
<?php


class BraspagRecorrenteV2BO extends Ead1_BO  {


    /**
     * Verifica um pagamento recorrente na nova API da braspag
     * @param \G2\Entity\VwVendaLancamento $to
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function verificar(G2\Entity\VwVendaLancamento $to, $simulando = false)
    {

        $negocio = new \G2\Negocio\Negocio();
        $negocio->beginTransaction();

		try {

		    $ei = new \G2\Entity\EntidadeIntegracao();
		    $ei->findOneBy(array('id_entidade'=>$to->getIdEntidade(), 'id_sistema'=>\G2\Constante\Sistema::BRASPAG_RECORRENTE));

            $venda = new \G2\Negocio\Venda();
            $recorrente = new \BraspagV2_Recorrente($ei->getSt_codsistema(), $ei->getSt_codchave(), $ei->getSt_caminho());
			$parcelaRecorrente 	= false;

            $orderId = $to->getRecorrenteOrderid(); //"eaee8d16-fb1c-4075-91b6-fc80bcae5f40";

            $mensageiroOrder = $recorrente->getOrder($orderId);

            $order = null;
            if($mensageiroOrder->getTipo() == \Ead1_IMensageiro::SUCESSO){
                $order = $mensageiroOrder->getFirstMensagem();
                $pedidoTransacao = $negocio->getRepository('G2\Entity\PedidoIntegracao')->findOneBy(array('id_venda' => $to->getIdVenda()));

                if(empty($pedidoTransacao))
                    $pedidoTransacao = new G2\Entity\PedidoIntegracao();

                $pedidoTransacao->setIdVenda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));

                if ($order->RecurrentPayment->Amount)
                    $pedidoTransacao->setNuValor($order->RecurrentPayment->Amount);

                $pedidoTransacao->setDtEnvio(new \DateTime($order->RecurrentPayment->CreateDate));
                $pedidoTransacao->setDtInicio(new \DateTime($order->RecurrentPayment->StartDate));

                if(isset($order->RecurrentPayment->NextRecurrency) && $order->RecurrentPayment->NextRecurrency)
                    $pedidoTransacao->setDtProximopagamento(new \DateTime($order->RecurrentPayment->NextRecurrency));

                $pedidoTransacao->setDtTermino(new \DateTime($order->RecurrentPayment->EndDate));
                $pedidoTransacao->setIdSistema($negocio->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::BRASPAG_RECORRENTE));

                if ($order->RecurrentPayment->RecurrencyDay)
                    $pedidoTransacao->setNuDiavencimento($order->RecurrentPayment->RecurrencyDay);

                $pedidoTransacao->setNuIntervalo(30);

                if ($order->RecurrentPayment->OrderNumber)
                    $pedidoTransacao->setStOrderid($order->RecurrentPayment->OrderNumber);

                if (!$simulando) $negocio->save($pedidoTransacao);

            } else {
                throw new \Exception("Assinatura não encontrada na Braspag");
            }

            $mensageiro = $recorrente->GetOrderRecurrencies($order);

			if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
				$recorrencias = $mensageiro->getMensagem();

                if ($simulando) {
                    \Zend_Debug::dump($to, __FILE__ . ' - '.__line__ );
                    \Zend_Debug::dump($order, __FILE__ . ' - '.__line__ );
                    \Zend_Debug::dump($recorrencias, __FILE__ . ' - '.__line__ );
                }

				if($recorrencias){
					foreach($recorrencias as $recorrencia){

					    $vtrans = new \G2\Entity\TransacaoFinanceira();
					    $vtrans->findOneBy(array('un_transacaofinanceira'=>$recorrencia->TransactionId, 'nu_status'=>2));
					    if($vtrans && $vtrans instanceof \G2\Entity\TransacaoFinanceira && $vtrans->getNu_valorautorizado()){

                            if ($simulando)
                                \Zend_Debug::dump("Achou pagamento para parcela no valor de ".$vtrans->getNu_valorautorizado(), __FILE__ . ' - '.__line__ );

					        continue;
                        }

                        if ($recorrencia->Transaction->Payment->ReceivedDate) {
                            $venc = new \DateTime($to->getDtVencimento());
                            $pag = new \DateTime(substr($recorrencia->Transaction->Payment->ReceivedDate, 0, 10));
                            $diff = $pag->diff($venc);
                            if (($pag >= $venc && $diff->days < 30) && (!$parcelaRecorrente || $recorrencia->Transaction->Payment->Status == 2)) {
                                $parcelaRecorrente = $recorrencia;
                            }
                        }

					}
				}
			} else {
			    throw new \Exception("Assinatura encontrada mas ainda não tem pagamentos na Braspag ({$mensageiro->getFirstMensagem()})");
            }

 			if($parcelaRecorrente){

                $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));

                $lancamento->setNuVerificacao($lancamento->getNuVerificacao()+1);
                $lancamento->setNuTentativabraspag($parcelaRecorrente->TryNumber);
                $lancamento->setStRetornoverificacao(serialize($parcelaRecorrente));

                if (!$simulando) $negocio->save($lancamento);

                $transacaoFinanceira = new \G2\Entity\TransacaoFinanceira();
                $situacaoIntegracaoLancamento = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('nu_status' => $parcelaRecorrente->Transaction->Payment->Status, 'id_sistema' => \G2\Constante\Sistema::BRASPAG_NOVO_RECORRENTE_CONSULTA));
                if ($situacaoIntegracaoLancamento) {
                    $transacaoFinanceira->setId_Situacaointegracao($situacaoIntegracaoLancamento->getIdSituacaointegracao());
                }

                if($parcelaRecorrente->Transaction->Payment->Status == 2){ // 2 - PaymentConfirmed

                    $lancamento->setNuQuitado($parcelaRecorrente->Transaction->Payment->Amount/100);
                    $lancamento->setDtQuitado(new \DateTime($parcelaRecorrente->Transaction->Payment->ReceivedDate));
                    $lancamento->setBlBaixadofluxus(true);
                    $lancamento->setDt_atualizado(new \DateTime());
                    if (!$simulando) $venda->quitarCredito(array($lancamento));
                    $mensagem = 'quitado com sucesso!';

                    $transacaoFinanceira->setNu_valorautorizado($parcelaRecorrente->Transaction->Payment->Amount/100);

                } else {

                    if($situacaoIntegracaoLancamento instanceof \G2\Entity\SituacaoIntegracao){
                        $mensagem = "Assinatura encontrada, tentativa de pagamento com status {$parcelaRecorrente->Transaction->Payment->Status} - ".$situacaoIntegracaoLancamento->getStSituacao();
                    } else {
                        $mensagem = "Assinatura encontrada, tentativa de pagamento com status {$parcelaRecorrente->Transaction->Payment->Status} - Status não cadastrado em nossa base";
                    }

                }

                $transacaoFinanceira->setDt_Cadastro(new \DateTime() );

                $usuario = $negocio->getRepository('G2\Entity\Usuario')->findOneBy(array('id_usuario' => 1));
                if ($usuario) {
                    $transacaoFinanceira->setId_Usuariocadastro($usuario);
                }

                $transacaoFinanceira->setId_Venda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));
                $transacaoFinanceira->setNu_Status($parcelaRecorrente->Transaction->Payment->Status);
                $transacaoFinanceira->setNu_Verificacao($parcelaRecorrente->TryNumber);
                $transacaoFinanceira->setUn_Transacaofinanceira($parcelaRecorrente->Transaction->Payment->PaymentId);

                if (!$simulando) $negocio->save($transacaoFinanceira);

                $transacaoLancamento = new \G2\Entity\TransacaoLancamento();
                $transacaoLancamento->setIdLancamento($lancamento->getIdLancamento());
                $transacaoLancamento->setIdTransacaofinanceira($transacaoFinanceira->getId_Transacaofinanceira());

                if (!$simulando) $negocio->save($transacaoLancamento);

			} else {
                throw new \Exception("Assinatura encontrada mas ainda não tem pagamentos na Braspag para esta Parcela");
            }
            $negocio->commit();
            return new \Ead1_Mensageiro('Braspag V2 -> ' . $to->getStNomecompleto() . ' Lançamento ' . $to->getIdLancamento() . ' Vencimento ' . $to->getDtVencimento() . '  Parcela ' . $to->getNuOrdem() . ': ' . $mensagem . ' OrderID: ' . $orderId, \Ead1_IMensageiro::SUCESSO);
		} catch (\Exception $e) {
            $negocio->rollback();

            $mensagem = $e->getMessage();

            try {
                $nuverificacao = 0;
                /** @var G2\Entity\Lancamento $lancamento */
                $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));
                $nuverificacao = ($lancamento->getNuVerificacao() + 1);
                $lancamento->setNuVerificacao($nuverificacao);
                $lancamento->setDt_atualizado(new \DateTime());

                if ($parcelaRecorrente) {
                    $lancamento->setNuTentativabraspag($parcelaRecorrente->TryNumber);
                    $lancamento->setStRetornoverificacao(serialize($parcelaRecorrente));
                }
                if (!$simulando) $negocio->save($lancamento);

            } catch (\Exception $e) {
                $mensagem = $mensagem . ' Exception da contagem:' . $e->getMessage();
                \Zend_Debug::dump($mensageiroOrder, __FILE__ . ' - ' . __line__);
            }

            return new \Ead1_Mensageiro('Braspag V2 -> ' . $to->getStNomecompleto() . ': Lancamento ' . $to->getIdLancamento() . ' Vencimento ' . $to->getDtVencimento() . ' Parcela ' . $to->getNuOrdem() . ' Verificaçao ' . $nuverificacao . ':  ' . $mensagem . ' OrderID: ' . $orderId, \Ead1_IMensageiro::ERRO);
        }

    }

    /** @var  \BraspagV2_Recorrente $recurrency_negocio */
    protected $recurrency_negocio;

    protected function _init($idEntidade, $url = null)
    {

        try {

            $ei = new \G2\Entity\EntidadeIntegracao();
            $ei->findOneBy(array(
                'id_entidade' => $idEntidade,
                'id_sistema' => \G2\Constante\Sistema::BRASPAG_RECORRENTE
            ));

            if (!isset($ei) || empty($ei)) {
                throw new Exception('Nenhuma integração encontrada para o Sistema: BRASPAG RECORRENTE');
            }

            return $this->recurrency_negocio = new \BraspagV2_Recorrente(
                $ei->getSt_codsistema(),
                $ei->getSt_codchave(),
                !empty($url) ? $url : $ei->getSt_caminho()
            );

        } catch (Exception $exception) {
            throw $exception;
        }

    }

    public function getRecurrentPayment($data)
    {
        $negocio = new \G2\Negocio\Negocio();
        try {
            /** @var \G2\Entity\VwResumoVendaLancamento $lancamento */
            $lancamento = $negocio->findOneBy('G2\Entity\VwResumoVendaLancamento', array('id_venda' => $data['id_venda']));

            if (!isset($lancamento) || empty($lancamento)) {
                throw new Exception('Nenhuma pagamento recorrente encontrado para a venda informada!');
            }

            $enEntidadeIntegracao = $negocio->findOneBy('G2\Entity\EntidadeIntegracao',
                array('id_entidade' => ($lancamento->getId_entidade()) ? $lancamento->getId_entidade() : $this->sessao->id_entidade,
                    'id_sistema' => \G2\Constante\Sistema::BRASPAG_RECORRENTE));

            if (!$enEntidadeIntegracao instanceof \G2\Entity\EntidadeIntegracao)
                throw new Exception('Nenhuma url de pagamento encontrada!');

            $recorrente = $this->_init($lancamento->getId_entidade(), $enEntidadeIntegracao->getSt_caminho());

            return $recorrente->getRecurrentPayment($lancamento->getRecorrenteOrderid());

        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Braspag V2: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    public function trocarCartaoRecorrente($data)
    {

        $negocio = new \G2\Negocio\Negocio();

        try {
            /** @var \G2\Entity\VwResumoVendaLancamento $lancamento */
            $lancamento = $negocio->findOneBy('G2\Entity\VwResumoVendaLancamento', array('id_venda' => $data['id_venda']));

            if (!isset($lancamento) || empty($lancamento)) {
                throw new Exception('Nenhuma pagamento recorrente encontrado para a venda informada!');
            }

            /** @var \G2\Entity\VwBandeiraCartao $brand */
            $brand = $negocio->findOneBy('G2\Entity\VwBandeiraCartao',
                array('id_cartaobandeira' => $data['id_cartaobandeira'], 'id_entidade' => $lancamento->getId_entidade(), 'id_sistema' => \G2\Constante\Sistema::BRASPAG_RECORRENTE));

            if (!isset($brand) || empty($brand)) {
                throw new Exception('Bandeira não aceita em nosso Sistema!');
            }

            $recorrente = $this->_init($lancamento->getId_entidade());
            $mensageiro = $this->getRecurrentPayment($data);

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return $mensageiro;
            }

            $recurrency = $mensageiro->getFirstMensagem();

            $mensageiroChangeRecurrency = $recorrente->changeRecurrentPayment($lancamento->getRecorrenteOrderid(),
                array(
                    "Provider" => $recurrency->RecurrentPayment->Provider,
                    'Type' => "CreditCard",
                    "Amount" => $recurrency->RecurrentPayment->Amount,
                    "Installments" => $recurrency->RecurrentPayment->Installments,
                    "Country" => $recurrency->RecurrentPayment->Country,
                    "Currency" => $recurrency->RecurrentPayment->Currency,
                    "CreditCard" => array(
                        "Brand" => $brand->getStCartaobandeira(),
                        "Holder" => $data['st_nomeimpresso'],
                        "CardNumber" => str_replace(' ', '', $data['st_cartao']),
                        "ExpirationDate" => str_pad($data['nu_mesvalidade'], 2, "0", STR_PAD_LEFT) . '/' . $data['nu_anovalidade']
                    )
                )
            );

            $pedidoTransacao = $negocio->getRepository('G2\Entity\PedidoIntegracao')->findOneBy(array('id_venda' => $data['id_venda']));

            if (empty($pedidoTransacao))
                $pedidoTransacao = new G2\Entity\PedidoIntegracao();

            $pedidoTransacao->setIdVenda($negocio->getReference('G2\Entity\Venda', $data['id_venda']));
            $pedidoTransacao->setNuValor($recurrency->RecurrentPayment->Amount);
            $pedidoTransacao->setDtEnvio(new \DateTime($recurrency->RecurrentPayment->CreateDate));
            $pedidoTransacao->setDtInicio(new \DateTime($recurrency->RecurrentPayment->StartDate));

            if (isset($recurrency->RecurrentPayment->NextRecurrency) && $recurrency->RecurrentPayment->NextRecurrency)
                $pedidoTransacao->setDtProximopagamento(new \DateTime($recurrency->RecurrentPayment->NextRecurrency));

            $pedidoTransacao->setDtTermino(new \DateTime($recurrency->RecurrentPayment->EndDate));
            $pedidoTransacao->setIdSistema($negocio->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::BRASPAG_RECORRENTE));
            $pedidoTransacao->setNuDiavencimento($recurrency->RecurrentPayment->RecurrencyDay);
            $pedidoTransacao->setNuIntervalo(30);
            $pedidoTransacao->setStOrderid($recurrency->RecurrentPayment->OrderNumber);
            $pedidoTransacao->setNuTentativas($recurrency->RecurrentPayment->CurrentRecurrencyTry);
            $pedidoTransacao->setNuTentativasfeitas($recurrency->RecurrentPayment->CurrentRecurrencyTry);

            $negocio->save($pedidoTransacao);

            return $mensageiroChangeRecurrency;

        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Braspag V2: ' . $lancamento->getSt_nomecompleto() . ': ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }
}

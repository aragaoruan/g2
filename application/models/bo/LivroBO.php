<?php

/**
 * Classe com regras de negócio para interações de Livros
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @since 17/04/2013
 * 
 * @package models
 * @subpackage bo
 */
class LivroBO extends Ead1_BO
{

    /**
     * Metodo que retorna livros
     * 
     * @param LivroTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarLivro (LivroTO $to)
    {

        $dao = new LivroDAO();
        try {

            $to->setBl_ativo(null);

            $retorno = $dao->retornarLivro($to);
            if (empty($retorno)) {
                return new Ead1_Mensageiro('Nenhum Livro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new LivroTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar Livro', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna livros
     * 
     * @param VwPesquisarLivroTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwPesquisarLivro (VwPesquisarLivroTO $to)
    {

        $dao = new LivroDAO();
        try {
            $retorno = $dao->retornarVwPesquisarLivro($to);
            if (empty($retorno)) {
                return new Ead1_Mensageiro('Nenhum Livro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwPesquisarLivroTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar Livro', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna os tipos de livros 
     * @param TipoLivroTO $to
     */
    public function retornarTipoLivro (TipoLivroTO $to)
    {

        $dao = new LivroDAO();
        try {
            $retorno = $dao->retornarTipoLivro($to);
            if (empty($retorno)) {
                return new Ead1_Mensageiro('Nenhum Tipo de Livro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new TipoLivroTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar Tipo de Livro', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que salva um Livro
     * @param LivroTO $to
     * @return LivroTO|Ead1_Mensageiro
     */
    public function salvarLivro (LivroTO $to, array $arEntidadeTO = null)
    {

        $dao = new LivroDAO();
        $dao->beginTransaction();
        try {

            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);
            $to->setId_entidadecadastro($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $to->getSessao()->id_entidade);

            $to = $dao->salvarLivro($to);

            if (is_array($arEntidadeTO)) {
                $this->salvarArLivroEntidade($arEntidadeTO, $to);
            }

            $dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro('Erro ao Salvar o Livro', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que cadastra os vínculos de livro com entidade
     * @see LivroBO::salvarLivro();
     * 
     * @param mixed $arEntidadeTO array de Tos do tipo EntidadeTO ou um único TO deste tipo
     * @return Ead1_Mensageiro
     */
    public function salvarArLivroEntidade (array $arEntidadeTO, LivroTO $livroTO)
    {
        $dao = new LivroDAO();
        try {
            $dao->beginTransaction();
            if (!$livroTO->getId_livro()) {
                throw new Zend_Exception("A chave do livro não foi encontrada.");
            }
            $dao->deletarLivroEntidadePorLivro($livroTO->getId_livro());
            foreach ($arEntidadeTO as $entidadeTO) {
                $livroEntidadeTO = new LivroEntidadeTO();
                $livroEntidadeTO->setId_entidade($entidadeTO->getId_entidade());
                $livroEntidadeTO->setId_livro($livroTO->getId_livro());
                $dao->cadastrarLivroEntidade($livroEntidadeTO);
            }

            $dao->commit();
            return new Ead1_Mensageiro("Vínculo realizado com sucesso!");
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro("Erro ao realizar Vínculo de livro com entidade!", Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna as entidades do livro em questão
     * @see LivroCo::retornarArLivroEntidade()
     * @param LivroTO $livroTO
     * @return Ead1_Mensageiro
     */
    public function retornarArLivroEntidade (LivroTO $livroTO)
    {
        if (!$livroTO->getId_livro()) {
            return new Ead1_Mensageiro("É necessário informar um livro.", Ead1_IMensageiro::ERRO);
        }
        $dao = new LivroDAO();
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornarArLivroEntidade($livroTO), new LivroEntidadeTO()));
    }

    /**
     * Metodo que retorna Coleções
     * 
     * @param LivroColecaoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarLivroColecao (LivroColecaoTO $to)
    {

        $dao = new LivroDAO();
        try {

            $to->setBl_ativo(true);
            $retorno = $dao->retornarLivroColecao($to);
            if (empty($retorno)) {
                return new Ead1_Mensageiro('Nenhuma Coleção Encontrada!', Ead1_IMensageiro::AVISO);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new LivroColecaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao Retornar Coleção.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que salva um LivroColecaoTO
     * @param LivroColecaoTO $to
     * @return LivroColecaoTO|Ead1_Mensageiro
     */
    public function salvarLivroColecao (LivroColecaoTO $to)
    {

        $dao = new LivroDAO();
        $dao->beginTransaction();
        try {

            $to->setId_usuariocadastro($to->getId_usuariocadastro() ? $to->getId_usuariocadastro() : $to->getSessao()->id_usuario);
            $to->setId_entidadecadastro($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $to->getSessao()->id_entidade);

            $to = $dao->salvarLivroColecao($to);
            $dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro('Erro ao Salvar a Coleção', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método para vincular um Produto a um Livro em uma Entidade
     * @param ProdutoLivroTO $to
     * @return ProdutoLivroTO|Ead1_Mensageiro
     */
    public function inserirProdutoLivro (ProdutoLivroTO $to)
    {

        $dao = new LivroDAO();
        $dao->beginTransaction();
        try {
            $to = $dao->inserirProdutoLivro($to);
            $dao->commit();
            return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro('Erro ao Salvar o vínculo entre Produto, Livro e Entidade', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método para excluir um Produto/Livro/Entidade
     * @param ProdutoLivroTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function excluirProdutoLivro (ProdutoLivroTO $to)
    {

        $dao = new LivroDAO();
        $dao->beginTransaction();
        try {

            if (!$to->getId_entidade() || !$to->getId_livro() || !$to->getId_produto()) {
                throw new Zend_Exception("Você precisa informar todos os dados para exclusão, LIVRO, PRODUTO e ENTIDADE!");
            }

            $to = $dao->excluirProdutoLivro($to);
            $dao->commit();
            return new Ead1_Mensageiro("Vínculo excluído com sucesso", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro('Erro ao excluir o vínculo.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que busca a lista de livros no WS da Leya
     * @return Ead1_Mensageiro
     */
    public function retornarLivrosLeya ($st_codchave)
    {

        try {

            $livros = Leya_Service_Bookstore::getBooks4Distribution($st_codchave);
            return new Ead1_Mensageiro($livros, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao buscar os Livros no WS da Leya', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna o Link do Livro para Download
     * @param LivroTO $livro
     * @param VendaTO $venda
     * @return Ead1_Mensageiro
     */
    public function retornaLinkLivroLeya (LivroTO $livro, VendaTO $venda, $st_codchave)
    {
        try {

            $livro = Leya_Service_Bookstore::getLink($livro->getSt_isbn(), $venda->getId_venda(), $st_codchave);
            return new Ead1_Mensageiro($livro, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao buscar o Link do Livro no WS da Leya', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Retorna as categorias disponíveis na Leya Bookstore
     * @return Ead1_Mensageiro
     */
    public function retornaCategoriasLivroLeya ($st_codchave)
    {
        try {

            $categorias = Leya_Service_Bookstore::getCategories($st_codchave);
            return new Ead1_Mensageiro($categorias, Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new Ead1_Mensageiro('Erro ao buscar as Categorias no WS da Leya', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

}
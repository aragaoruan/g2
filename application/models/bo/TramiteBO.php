<?php

/**
 * Classe com regras de negócio para interações de Tramite
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 03/10/2011
 *
 * @package models
 * @subpackage bo
 */
class TramiteBO extends Ead1_BO
{

    /**
     * Contem a Instancia do Ead1_Mensageiro
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * Contem a Instancia do TramiteDAO
     * @var TramiteDAO
     */
    private $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new TramiteDAO();
    }

    /**
     * Metodo que cadastra um tramite
     * @param TramiteTO $to
     * @param bool $idCategoriaTramiteManual
     * @param null $idChave
     * @param null $arquivo
     * @return Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramite(TramiteTO $to, $idCategoriaTramiteManual = false, $idChave = null, $arquivo = null)
    {
        $this->dao->beginTransaction();
        try {
            if (!$to->getId_entidade()) {
                $to->setId_entidade($to->getSessao()->id_entidade);
            }
            if ($idCategoriaTramiteManual) {
                $categoriaTramiteORM = new CategoriaTramiteORM();
                $categoriaTramiteTO = new CategoriaTramiteTO();
                $categoriaTramiteTO->setId_categoriatramite($idCategoriaTramiteManual);
                $categoriaTramiteTO->fetch(true, true, true);

                if (!$to->getId_tipotramite()) {
                    $to->setId_tipotramite($categoriaTramiteTO->getId_tipomanual());
                }
            }

            if (!$to->getId_usuario())
                $to->setId_usuario($to->getSessao()->id_usuario);

            $id_tramite = $this->dao->cadastrarTramite($to);
            $to->setId_tramite($id_tramite);

            if ($idChave) {
                switch ($categoriaTramiteTO->getSt_tabela()) {
                    case "tb_tramitevenda":
                        $ormVinculo = new TramiteVendaORM();
                        $toVinculo = new TramiteVendaTO();
                        break;
                    case "tb_tramitematricula":
                        $ormVinculo = new TramiteMatriculaORM();
                        $toVinculo = new TramiteMatriculaTO();
                        break;
                    case "tb_tramiteentregamaterial":
                        $ormVinculo = new TramiteEntregaMaterialORM();
                        $toVinculo = new TramiteEntregaMaterialTO();
                        break;
                    case "tb_prevenda":
                        $ormVinculo = new TramitePreVendaORM();
                        $toVinculo = new TramitePreVendaTO();
                        break;
                }

                $metodo = "set" . ucfirst($categoriaTramiteTO->getSt_campo());
                $toVinculo->$metodo($idChave);
                $toVinculo->setId_tramite($id_tramite);
                $ormVinculo->insert($toVinculo->toArrayInsert());
            }

            $this->dao->commit();


            if ($arquivo) {
                $salvarArquivo = new G2\Negocio\CAtencao;
                $entidadeTramite = $salvarArquivo->find('G2\Entity\Tramite', $id_tramite);
                $salvarArquivo->salvarArquivoTramite($entidadeTramite, $arquivo['anexo']);
            }

            return $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
    }

    /**
     * Metodo que cadastra um vinculo de tramite com matricula
     * @param TramiteMatriculaTO $to
     * @return Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteMatricula(TramiteMatriculaTO $to)
    {
        try {
            $id_tramitematricula = $this->dao->cadastrarTramiteMatricula($to);
            $to->setId_tramitematricula($id_tramitematricula);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
        return $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra processamento
     * @param ProcessamentoTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     * @see MensagemCobrancaBO->registrarMensagensCobrancaRobo
     */
    public function cadastrarProcessamento(ProcessamentoTO $to)
    {
        try {
            $id_processamento = $this->dao->cadastrarProcessamento($to);
            $to->setId_processamento($id_processamento);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
        return $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que gera tramite de Lançamento
     * @param LancamentoTO $lancamentoTO
     * @return Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteLancamento(LancamentoTO $lancamentoTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!$lancamentoTO->getId_lancamento()) {
                THROW new Zend_Exception('O id_lancamento não veio setado!');
            }
            $vwVendaLancamentoORM = new VwVendaLancamentoORM();
            $vwVendaLancamentoTO = new VwVendaLancamentoTO();
            $vwVendaLancamentoTO->setId_lancamento($lancamentoTO->getId_lancamento());
            $vwVendaLancamentoTO = $vwVendaLancamentoORM->consulta($vwVendaLancamentoTO, false, true);

            if ($vwVendaLancamentoTO) {

                $tramiteVendaTO = new TramiteVendaTO();
                $tramiteTO = new TramiteTO();
                $tramiteTO->setId_tipotramite(TramiteVendaTO::TIPO_TRAMITE_LANCAMENTO);
                $tramiteTO->setId_entidade($tramiteVendaTO->getSessao()->id_entidade);
                $tramiteTO->setId_usuario($tramiteVendaTO->getSessao()->id_usuario);
                $tramiteTO->setSt_tramite(
                    "Recebido " . Zend_Locale_Format::toNumber($lancamentoTO->getNu_quitado(),
                        array(
                            'number_format' => 'R$ #,##0.00'
                        )) . " em " . $vwVendaLancamentoTO->getSt_meiopagamento() . " Pelo Lançamento Número: "
                    . $lancamentoTO->getId_lancamento()
                );
                $id_tramite = $this->dao->cadastrarTramite($tramiteTO);
                $tramiteTO->setId_tramite($id_tramite);
                $tramiteVendaTO->setId_venda($vwVendaLancamentoTO->getId_venda());
                $tramiteVendaTO->setId_tramite($tramiteTO->getId_tramite());
                $id_tramitevenda = $this->dao->cadastrarTramiteVenda($tramiteVendaTO);
                $tramiteVendaTO->setId_tramitevenda($id_tramitevenda);
            }

            $this->dao->commit();
            $this->mensageiro->setMensageiro('Tramite de Lançamento Gerado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra um tramite de venda
     * @param VendaTO $to
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function cadastrarTramiteVenda(VendaTO $to)
    {
        try {
            $this->dao->beginTransaction();
            if (!$to->getId_venda()) {
                THROW new Zend_Exception('O id_venda não veio setado para geração de tramite!');
            }
            $produtoDAO = new ProdutoDAO();
            $vendaTO = new VendaTO();
            $algum_tramite = false;
            $vendaTO->setId_venda($to->getId_venda());
            $venda = $produtoDAO->retornarVenda($vendaTO)->toArray();

            if (empty($venda)) {
                THROW new Zend_Exception('Não foi encontrada nenhuma venda com o id_venda fornecido!');
            }
            $vendaTO = Ead1_TO_Dinamico::encapsularTo($venda, new VendaTO(), true);
            //Valida e cadastra um tramite da venda para a situacao
            if ($to->getId_situacao() && $to->getId_situacao() != $vendaTO->getId_situacao()) {
                $tramiteVendaSituacaoTO = new TramiteVendaTO();
                $tramiteVendaSituacaoTO->setId_venda($vendaTO->getId_venda());
                $situacaoTO = new SituacaoTO();
                $situacaoTO->setId_situacao($to->getId_situacao());
                $situacaoTO->setSt_tabela("tb_venda");
                $this->_cadastrarTramiteVendaSituacao($tramiteVendaSituacaoTO, $situacaoTO);
                $algum_tramite = true;
            }
            //Valida e cadastra um tramite da venda para a Evolucao
            if ($to->getId_evolucao() && $to->getId_evolucao() != $vendaTO->getId_evolucao()) {
                $tramiteVendaEvolucaoTO = new TramiteVendaTO();
                $tramiteVendaEvolucaoTO->setId_venda($vendaTO->getId_venda());
                $evolucaoTO = new EvolucaoTO();
                $evolucaoTO->setId_evolucao($to->getId_evolucao());
                $evolucaoTO->setSt_tabela("tb_venda");
                $this->_cadastrarTramiteVendaEvolucao($tramiteVendaEvolucaoTO, $evolucaoTO);
                $algum_tramite = true;
            }
            if ($algum_tramite) {
                $this->mensageiro
                    ->setMensageiro("Tramites de Venda Cadastrados Com Sucesso!", Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro
                    ->setMensageiro("Nenhum Tramite Por Que Não Houve Alterações na Venda!", Ead1_IMensageiro::AVISO);
            }
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * @param TramiteVendaTO $tramiteVendaTO
     * @param SituacaoTO $situacaoTO
     * @throws Exception
     * @throws Zend_Exception
     */
    private function _cadastrarTramiteVendaSituacao(TramiteVendaTO $tramiteVendaTO, SituacaoTO $situacaoTO)
    {
        try {
            $tramiteTO = new TramiteTO();
            $tramiteTO->setId_tipotramite(TramiteVendaTO::TIPO_TRAMITE_SITUACAO);
            $tramiteTO->setId_entidade($tramiteVendaTO->getSessao()->id_entidade);
            $tramiteTO->setId_usuario($tramiteVendaTO->getSessao()->id_usuario);
            $situacaoORM = new SituacaoORM();
            $situacaoTO = $situacaoORM->consulta($situacaoTO, false, true);
            $tramiteTO->setSt_tramite("Alterando Situação da Venda para: " . $situacaoTO->getSt_situacao());
            $id_tramite = $this->dao->cadastrarTramite($tramiteTO);
            $tramiteTO->setId_tramite($id_tramite);
            $tramiteVendaTO->setId_tramite($tramiteTO->getId_tramite());
            $id_tramitevenda = $this->dao->cadastrarTramiteVenda($tramiteVendaTO);
            $tramiteVendaTO->setId_tramitevenda($id_tramitevenda);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * @param TramiteVendaTO $tramiteVendaTO
     * @param EvolucaoTO $evolucaoTO
     * @throws Exception
     * @throws Zend_Exception
     */
    private function _cadastrarTramiteVendaEvolucao(TramiteVendaTO $tramiteVendaTO, EvolucaoTO $evolucaoTO)
    {
        try {
            $tramiteTO = new TramiteTO();
            $tramiteTO->setId_tipotramite(TramiteVendaTO::TIPO_TRAMITE_EVOLUCAO);
            $tramiteTO->setId_entidade($tramiteVendaTO->getSessao()->id_entidade);
            $tramiteTO->setId_usuario($tramiteVendaTO->getSessao()->id_usuario);
            $evolucaoORM = new EvolucaoORM();
            $evolucaoTO = $evolucaoORM->consulta($evolucaoTO, false, true);
            $tramiteTO->setSt_tramite("Alterando Evolução da Venda para: " . $evolucaoTO->getSt_evolucao());
            $id_tramite = $this->dao->cadastrarTramite($tramiteTO);
            $tramiteTO->setId_tramite($id_tramite);
            $tramiteVendaTO->setId_tramite($tramiteTO->getId_tramite());
            $id_tramitevenda = $this->dao->cadastrarTramiteVenda($tramiteVendaTO);
            $tramiteVendaTO->setId_tramitevenda($id_tramitevenda);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que gera os tramites cada vez que é feito um aproveitamento de disciplina
     * @param $id_matricula
     * @param $st_disciplina
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteAproveitamentoDisciplina($id_matricula, $st_disciplina)
    {
        try {
            if (!$st_disciplina) {
                THROW new Zend_Exception("É necessario informar o nome da disciplina para realizar o tramite");
            }
            $this->dao->beginTransaction();
            //Cadastrando Tramite
            $tramiteTO = new TramiteTO();
            $tramiteTO->setId_tipotramite(2); //Aproveitamento de Disciplina
            $tramiteTO->setSt_tramite($st_disciplina . " foi aproveitada para essa Matrícula.");
            $this->cadastrarTramite($tramiteTO);
            //Vinculando Tramite a Matricula
            $tramiteMatriculaTO = new TramiteMatriculaTO();
            $tramiteMatriculaTO->setId_matricula($id_matricula);
            $tramiteMatriculaTO->setId_tramite($tramiteTO->getId_tramite());
            $this->cadastrarTramiteMatricula($tramiteMatriculaTO);
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
    }

    /**
     * Método que gera o tramite de transferencia de entidade de atendimento da matricula
     * @param MatriculaTO $matriculaTO
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteTransferenciaEntidadeAtendimentoMatricula(MatriculaTO $matriculaTO)
    {
        try {
            if ($matriculaTO->getId_matricula() && $matriculaTO->getId_entidadeatendimento()) {
                $orm = new MatriculaORM();

                $matriculaConsultaTO = new MatriculaTO();
                $matriculaConsultaTO->setId_matricula($matriculaTO->getId_matricula());
                $matriculaConsultaTO->fetch(false, true, true);

                if (!$matriculaConsultaTO) {
                    THROW new Zend_Exception("Matrícula não Encontrada Para Gerar Tramite.");
                }
                $orm = new EntidadeORM();
                $entidadeTO = new EntidadeTO();
                $entidadeTO->setId_entidade($matriculaTO->getId_entidadeatendimento());
                $entidadeTO->fetch(false, true, true);

                if (!$entidadeTO) {
                    THROW new Zend_Exception("Entidade não Encontrada Para Gerar Tramite de Matrícula.");
                }


                if ($matriculaConsultaTO->getId_entidadeatendimento() != $matriculaTO->getId_entidadeatendimento()) {
                    //Cadastrando Tramite
                    $tramiteTO = new TramiteTO();
                    $tramiteTO->setId_tipotramite(TramiteMatriculaTO::TRANSFERENCIA);
                    $tramiteTO->setSt_tramite(
                        "O atendimento dessa matricula foi transferido para a unidade "
                        . $entidadeTO->getSt_nomeentidade()
                    );
                    $this->cadastrarMatriculaTramite($matriculaTO, $tramiteTO);
                }
            }
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
    }

    /**
     * Cadastra tramite e tramite matrícula.
     * @param MatriculaTO $matriculaTO
     * @param TramiteTO $tramiteTO
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarMatriculaTramite(MatriculaTO $matriculaTO, TramiteTO $tramiteTO)
    {
        $this->dao->beginTransaction();
        try {
            //Cadastrando Tramite
            $tramiteTO = $this->cadastrarTramite($tramiteTO)->getFirstMensagem();
            //Vinculando Tramite a Matricula
            $tramiteMatriculaTO = new TramiteMatriculaTO();
            $tramiteMatriculaTO->setId_matricula($matriculaTO->getId_matricula());
            $tramiteMatriculaTO->setId_tramite($tramiteTO->getId_tramite());
            $this->cadastrarTramiteMatricula($tramiteMatriculaTO)->subtractMensageiro();
            $this->dao->commit();
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
    }

    /**
     * Método que gera o tramite de mudança de evolução da Matricula
     * @param MatriculaTO $matriculaTO
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteEvolucaoMatricula(MatriculaTO $matriculaTO)
    {
        try {
            if ($matriculaTO->getId_matricula() && $matriculaTO->getId_evolucao()) {


                $matriculaConsultaTO = new MatriculaTO();
                $matriculaConsultaTO->setId_matricula($matriculaTO->getId_matricula());
                $matriculaConsultaTO->fetch(false, true, true);

                if (!$matriculaConsultaTO) {
                    THROW new Zend_Exception("Matrícula não Encontrada Para Gerar Tramite.");
                }
                $evolucaoTO = new EvolucaoTO();
                $evolucaoTO->setId_evolucao($matriculaTO->getId_evolucao());
                $evolucaoTO->fetch(false, true, true);

                $evolucaoCertificacaoTO = new EvolucaoTO();
                $evolucaoCertificacaoTO->setId_evolucao($matriculaTO->getId_evolucaocertificacao());
                $evolucaoCertificacaoTO->fetch(false, true, true);

                if (!$evolucaoTO) {
                    THROW new Zend_Exception("Evolução não Encontrada Para Gerar Tramite de Matrícula.");
                }

                if ($matriculaConsultaTO->getId_evolucao() != $matriculaTO->getId_evolucao()) {
                    //Cadastrando Tramite
                    $tramiteTO = new TramiteTO();
                    $tramiteTO->setId_tipotramite(TramiteMatriculaTO::EVOLUCAO);
                    $tramiteTO
                        ->setSt_tramite("A Evolução da matricula foi alterada para " . $evolucaoTO->getSt_evolucao());
                    $this->cadastrarMatriculaTramite($matriculaTO, $tramiteTO);
                }

            }
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que cadastra um Tramite para a Entrega de Material (Entrega)
     * @param EntregaMaterialTO $emTO
     * @return Ead1_Mensageiro
     * @throws Exception
     * @throws Zend_Exception
     */
    public function cadastrarTramiteEntregaMaterial(EntregaMaterialTO $emTO)
    {
        try {
            $this->dao->beginTransaction();
            $tramiteTO = new TramiteTO();
            $tramiteTO->setId_tipotramite(4);
            $tramiteTO->setId_entidade($emTO->getSessao()->id_entidade);
            $tramiteTO->setId_usuario($emTO->getSessao()->id_usuario);
            $materialDAO = new MaterialDAO();
            $itemMaterialTO = new ItemDeMaterialTO();
            $itemMaterialTO->setId_itemdematerial($emTO->getId_itemdematerial());
            $material = $materialDAO->retornarItemDeMaterial($itemMaterialTO)->toArray();
            if (empty($material)) {
                THROW new Zend_Exception("Não foi encontrado o Item de Material");
            }
            $situacaoORM = new SituacaoORM();
            $situacaoTO = $situacaoORM->consulta(new SituacaoTO(array(
                'id_situacao' => $emTO->getId_situacao()
            )), true, true);
            if (!$situacaoTO) {
                THROW new Zend_Exception('Erro ao Retornar a Situação do Material!');
            }
            $itemMaterialTO = Ead1_TO_Dinamico::encapsularTo($material, new ItemDeMaterialTO(), true);
            $tramiteTO->setSt_tramite(
                'Material ' . $itemMaterialTO->getSt_itemdematerial() . ' ' . $situacaoTO->getSt_situacao() . '.'
            );
            $id_tramite = $this->dao->cadastrarTramite($tramiteTO);
            $tramiteTO->setId_tramite($id_tramite);
            $tramiteEntregaMaterialTO = new TramiteEntregaMaterialTO();
            $tramiteEntregaMaterialTO->setId_tramite($tramiteTO->getId_tramite());
            $tramiteEntregaMaterialTO->setId_entregamaterial($emTO->getId_entregamaterial());
            $id_tramiteentregamaterial = $this->dao->cadastrarTramiteEntregaMaterial($tramiteEntregaMaterialTO);
            $tramiteEntregaMaterialTO->setId_tramiteentregamaterial($id_tramiteentregamaterial);
            $this->dao->commit();
            $this->mensageiro
                ->setMensageiro('Tramite de Entrega de Material Cadastrado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra um Tramite para a Entrega de Material (Devolução)
     * @param EntregaMaterialTO $emTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function cadastrarTramiteDevolucaoMaterial(EntregaMaterialTO $emTO)
    {
        try {
            $this->dao->beginTransaction();
            $tramiteTO = new TramiteTO();
            $tramiteTO->setId_tipotramite(4);
            $tramiteTO->setId_entidade($emTO->getSessao()->id_entidade);
            $tramiteTO->setId_usuario($emTO->getSessao()->id_usuario);
            $materialDAO = new MaterialDAO();
            $itemMaterialTO = new ItemDeMaterialTO();
            $itemMaterialTO->setId_itemdematerial($emTO->getId_itemdematerial());
            $material = $materialDAO->retornarItemDeMaterial($itemMaterialTO)->toArray();
            if (empty($material)) {
                THROW new Zend_Exception("Não foi encontrado o Item de Material");
            }
            $itemMaterialTO = Ead1_TO_Dinamico::encapsularTo($material, new ItemDeMaterialTO(), true);
            $tramiteTO->setSt_tramite('Material ' . $itemMaterialTO->getSt_itemdematerial() . ' Devolvido.');
            $tramiteBO = new TramiteBO();
            $id_tramite = $this->dao->cadastrarTramite($tramiteTO);
            $tramiteTO->setId_tramite($id_tramite);
            $tramiteEntregaMaterialTO = new TramiteEntregaMaterialTO();
            $tramiteEntregaMaterialTO->setId_tramite($tramiteTO->getId_tramite());
            $tramiteEntregaMaterialTO->setId_entregamaterial($emTO->getId_entregamaterial());
            $id_tramiteentregamaterial = $this->dao->cadastrarTramiteEntregaMaterial($tramiteEntregaMaterialTO);
            $tramiteEntregaMaterialTO->setId_tramiteentregamaterial($id_tramiteentregamaterial);
            $this->dao->commit();
            $this->mensageiro
                ->setMensageiro('Tramite de Entrega de Material Cadastrado com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            THROW $e;
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna os tipos de tramite
     * @param TipoTramiteTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarTipoTramite(TipoTramiteTO $to)
    {
        try {
            $dados = $this->dao->retornarTipoTramite($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new TipoTramiteTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que retorna os tipos de tramite
     * @param CategoriaTramiteTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarCategoriaTramite(CategoriaTramiteTO $to)
    {
        try {
            $dados = $this->dao->retornarCategoriaTramite($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(
                    Ead1_TO_Dinamico::encapsularTo($dados, new CategoriaTramiteTO()),
                    Ead1_IMensageiro::SUCESSO
                );
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que retorna processamentos
     * @param ProcessamentoTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarProcessamento(ProcessamentoTO $to)
    {
        try {
            $dados = $this->dao->retornarProcessamento($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro
                    ->setMensageiro('Nenhum Registro de processamento Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(
                    Ead1_TO_Dinamico::encapsularTo($dados, new ProcessamentoTO()),
                    Ead1_IMensageiro::SUCESSO
                );
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que retorna os vinculos de tramite com matricula
     * @param TramiteMatriculaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarTramiteMatricula(TramiteMatriculaTO $to)
    {
        try {
            $dados = $this->dao->retornarTramiteMatricula($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(
                    Ead1_TO_Dinamico::encapsularTo($dados, new TramiteMatriculaTO()),
                    Ead1_IMensageiro::SUCESSO
                );
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Metodo que retorna os Tramites
     * @param TramiteTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarTramite(TramiteTO $to)
    {
        try {
            if (!$to->getId_entidade()) {
                $to->setId_entidade($to->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarTramite($to)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Método que retorna os tramites pela SP_TRAMITE
     * @param $idCategoriaTramite
     * @param array $idCampo
     * @param null $idTipoTramite
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornarTramiteSp($idCategoriaTramite, array $idCampo, $idTipoTramite = null)
    {
        try {
            $dados = $this->dao->retornarTramiteSp($idCategoriaTramite, $idCampo, $idTipoTramite);
            if (empty($dados)) {
                return $this->mensageiro
                    ->setMensageiro(Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro
                ->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new SpTramiteTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Método que atualiza um trâmite
     * @param TramiteTO $tramiteTo
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function alterarTramite(TramiteTO $tramiteTo)
    {
        try {
            $this->dao->updateTramite($tramiteTo);
            return $this->mensageiro->setMensageiro('Tramite alterado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            THROW $e;
        }
    }

}

<?php

/**
 * Classe com regras de negócio para interações de Disciplina
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 13/10/2010
 * 
 * @package models
 * @subpackage bo
 */
class DisciplinaBO extends Ead1_BO
{

    public $mensageiro;

    CONST EVOLUCAO_DISCIPLINA_CURSANDO = 13;

    public function __construct ()
    {
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo que Cadastra e Exclui o vinculo da Area com as Disciplinas
     * @param array (AreaConhecimentoTO $arrTO)
     * @param AreaDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayAreaDisciplina ($arrTO, AreaDisciplinaTO $adTO)
    {
        $dao = new DisciplinaDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }
        $dao->beginTransaction();
        try {
            if (!$dao->deletarAreaDisciplina($adTO)) {
                Throw new Zend_Exception('Erro ao Excluir Vinculos da Disciplina com a Area!');
            }
            foreach ($arrTO as $acTO) {
                if (!($acTO instanceof AreaConhecimentoTO)) {
                    Throw new Zend_Exception('O Objeto não é uma Instancia da Classe AreaConhecimentoTO!');
                }
                $adTO->setId_areaconhecimento($acTO->getId_areaconhecimento());
                if (!$dao->cadastrarAreaDisciplina($adTO)) {
                    Throw new Zend_Exception('Erro ao Vincular Area com Disciplina!');
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }

    /**
     * Metodo que Cadastra e Exclui vinculos da Disciplina com a Serie e Nivel de Ensino
     * @param array( DisciplinaSerieNivelEnsinoTO $arrTO)
     * @return Ead1_DAO
     */
    public function salvarArrayDisciplinaSerieNivelEnsino (array $arrTO)
    {
        $dao = new DisciplinaDAO();
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro('O Parametro Recebido não é um Array!', Ead1_IMensageiro::AVISO);
        }

        $disciplinaSerieNivelEnsinoORM = new DisciplinaSerieNivelEnsinoORM();
        $dao->beginTransaction();
        try {
            foreach ($arrTO as $dsneTO) {
                if (!($dsneTO instanceof DisciplinaSerieNivelEnsinoTO)) {
                    throw new Zend_Exception('O Objeto não é uma Instancia da Classe DisciplinaSerieNivelEnsinoTO!');
                }
                if ($disciplinaSerieNivelEnsinoORM->consulta($dsneTO) === false) {
                    if (!$dao->cadastrarDisciplinaSerieNivel($dsneTO)) {
                        Throw new Zend_Exception('Erro ao Vincular Disciplina com a Série e Nivel de Ensino!');
                    }
                }
            }
            $dao->commit();
            return $this->mensageiro->setMensageiro('Vinculos Cadastrados com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setCodigo($dao->excecao);
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem($e->getMessage());
            return $this->mensageiro;
        }
    }


    /**
     * Meotodo que Cadastra Disciplina
     * @param DisciplinaTO $dTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDisciplina (DisciplinaTO $dTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dTO->getId_entidade()) {
            $dTO->setId_entidade($dTO->getSessao()->id_entidade);
        }
        $dTO->setId_usuariocadastro($dTO->getSessao()->id_usuario);
        $id_disciplina = $dao->cadastrarDisciplina($dTO);
        if (!$id_disciplina) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro($dTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra a Area da Disciplina
     * @param AreaDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAreaDisciplina (AreaDisciplinaTO $adTO)
    {
        $dao = new DisciplinaDAO();
        if ($this->verificaAreaDisciplina($adTO)) {
            return $this->mensageiro->setMensageiro('Esta Disciplina Já Possue um Área de Conhecimento!', Ead1_IMensageiro::AVISO);
        }
        if (!$dao->cadastrarAreaDisciplina($adTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Area da Disciplina', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro($adTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra Serie, Nivel de Ensino da Area de Conhecimento na Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDisciplinaSerieNivel (DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dao->cadastrarDisciplinaSerieNivel($dsneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Nivel de Ensino da Area de Conhecimento na Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro($dsneTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Meotodo que Edita Disciplina
     * @param DisciplinaTO $dTO
     * @return Ead1_Mensageiro
     */
    public function editarDisciplina (DisciplinaTO $dTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dao->editarDisciplina($dTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
//        } else {
//            $wsBo = new WsBO(Ead1_Ambiente::geral()->wsURL . 'services/disciplina.php');
//            $respostaWs = $wsBo->editarDisciplina($dTO);
        }

        return $this->mensageiro->setMensageiro('Disciplina Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui a Area da Disciplina
     * @param AreaDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function deletarAreaDisciplina (AreaDisciplinaTO $adTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dao->deletarAreaDisciplina($adTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Area da Disciplina', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Area da Disciplina Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui Serie, Nivel de Ensino da Area de Conhecimento da Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaSerieNivel (DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dao->deletarDisciplinaSerieNivel($dsneTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Serie do Nivel de Ensino da Area de Conhecimento na Disciplina!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Serie do Nivel de Ensino da Area de Conhecimento na Disciplina Excluido com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    public function deletarDisciplina (DisciplinaTO $disciplinaTO)
    {
        $disciplinaDAO = new DisciplinaDAO();
        $disciplinaDAO->beginTransaction();

        try {

            if (empty($disciplinaTO->id_disciplina)) {
                throw new DomainException('Para excluir a disciplina é necessário informar o seu código');
            }

            $discTO = new DisciplinaTO();
            $discTO->id_disciplina = $disciplinaTO->id_disciplina;
            $discTO->bl_ativa = 0;

            if ($disciplinaDAO->editarDisciplina($discTO) === false) {
                throw new DomainException('Erro ao excluir a pesquisa. Erro: ' . $disciplinaDAO->excecao);
            }

            $this->mensageiro->setMensageiro('Disciplina removida com sucesso', Ead1_IMensageiro::SUCESSO);
            $disciplinaDAO->commit();
        } catch (Exception $exc) {
            $disciplinaDAO->rollBack();
            $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna Disciplina
     * @param DisciplinaTO $dTO
     * @return Ead1_Mensageiro
     */
    public function retornaDisciplina (DisciplinaTO $dTO)
    {
        $dao = new DisciplinaDAO();
        if (!$dTO->getId_entidade() && !$dTO->getId_disciplina()) {
            $dTO->setId_entidade($dTO->getSessao()->id_entidade);
        }
        $disciplina = $dao->retornaDisciplina($dTO);
        if (is_object($disciplina)) {
            $disciplina = $disciplina->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Disciplina!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        if (empty($disciplina)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplina, new DisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna Disciplina da Area de Conhecimento
     * @param AreaDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function retornaAreaDisciplina (AreaDisciplinaTO $adTO)
    {
        $dao = new DisciplinaDAO();
        $areaDisciplina = $dao->retornaAreaDisciplina($adTO);
        if (is_object($areaDisciplina)) {
            $areaDisciplina = $areaDisciplina->toArray();
        } else {
            unset($areaDisciplina);
        }
        if (!$areaDisciplina) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($areaDisciplina, new VwAreaDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna Serie no Nivel de Ensino na Area de Conhecimento da Disciplina
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @return Ead1_Mensageiro
     */
    public function retornaDisciplinaSerieNivel (DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $dao = new DisciplinaDAO();
        $disciplinaSerieNivel = $dao->retornaDisciplinaSerieNivel($dsneTO);
        if (is_object($disciplinaSerieNivel)) {
            $disciplinaSerieNivel = $disciplinaSerieNivel->toArray();
        } else {
            unset($disciplinaSerieNivel);
        }
        if (!$disciplinaSerieNivel) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplinaSerieNivel, new DisciplinaSerieNivelEnsinoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna Professores de um tipo de disciplina
     * @param VwProfessorDisciplinaTO $vwProfessorDisciplinaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProfessorDisciplina (VwProfessorDisciplinaTO $vwProfessorDisciplinaTO, $order = null)
    {
        $dao = new DisciplinaDAO();
        if (!$vwProfessorDisciplinaTO->getId_entidade()) {
            $vwProfessorDisciplinaTO->setId_entidade($vwProfessorDisciplinaTO->getSessao()->id_entidade);
        }

        $professores = $dao->retornarVwProfessorDisciplina($vwProfessorDisciplinaTO, $order);
        if (is_object($professores)) {
            $professores = $professores->toArray();
        } else {
            unset($professores);
        }
        if (!$professores) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Professor Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($professores, new VwProfessorDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * @param VwProfessorDisciplinaTO $vwProfessorDisciplinaTO
     * @param null $order
     * @return Ead1_Mensageiro
     */
    public function retornarProfessor(VwProfessorDisciplinaTO $vwProfessorDisciplinaTO, $order = null){
        $dao = new DisciplinaDAO();
        if (!$vwProfessorDisciplinaTO->getId_entidade()) {
            $vwProfessorDisciplinaTO->setId_entidade($vwProfessorDisciplinaTO->getSessao()->id_entidade);
        }

        $professores = $dao->retornarProfessor($vwProfessorDisciplinaTO, $order);
        if (is_object($professores)) {
            $professores = $professores->toArray();
        } else {
            unset($professores);
        }
        if (!$professores) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Professor Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($professores, new VwProfessorDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }
    
    
    /**
     * Metodo que Retorna dados da Vw_DisciplinaSerieNivel
     * @param VwDisciplinaSerieNivelTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwDisciplinaSerieNivel(VwDisciplinaSerieNivelTO $to)
    {
        $dao = new DisciplinaDAO();
        if (!$to->getId_entidade()) {
            $to->setId_entidade($to->getSessao()->id_entidade);
        }
        $resultado = $dao->retornarVwDisciplinaSerieNivel($to);
        if (is_object($resultado)) {
            $resultado = $resultado->toArray();
        }
        if (!$resultado) {
            return new Ead1_Mensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($resultado, new VwDisciplinaSerieNivelTO()), Ead1_IMensageiro::SUCESSO);
    }
    /**
     * @todo falta criar a view e o metodo na DAO!
     * Meotod que retorna as Disciplinas por Entidade, Area de Conhecimento e Serie.
     * @param EntidadeTO $eTO
     * @param AreaConhecimentoTO $acTO
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     * @param DisciplinaSerieNivelEnsinoTO $dsneTO
     */
    public function retornarDisciplinaxAreaSerie (EntidadeTO $eTO, AreaConhecimentoTO $acTO, DisciplinaSerieNivelEnsinoTO $dsneTO)
    {
        $dao = new DisciplinaDAO();
        $disciplina = $dao->retornarDisciplinaxAreaSerie($eTO, $acTO, $dsneTO);
        if (is_object($disciplina)) {
            $disciplina = $disciplina->toArray();
        } else {
            unset($disciplina);
        }
        if (!$disciplina) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplina, new VwDisciplinaxAreaSerieTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Meotod que retorna os tipos de disciplina
     * @param TipoDisciplinaTO $tdTO
     * @param boolean $pesquisa
     * @return Ead1_Mensageiro
     */
    public function retornaTipoDisciplina (TipoDisciplinaTO $tdTO, $pesquisa = false)
    {
        $dao = new DisciplinaDAO();
        $tipoDisciplina = $dao->retornaTipoDisciplina($tdTO);
        if (!$tipoDisciplina) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        if ($pesquisa) {
            return $this->retornarCombosPesquisa($tipoDisciplina, 'st_tipodisciplina', 'id_tipodisciplina');
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipoDisciplina->toArray(), new TipoDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Retorna dados de Disciplina por Area de Conhecimento e serie e nivel de ensino
     * @param VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO
     * @return Ead1_Mensageiro
     */
    public function retornaDisciplinaAreaConhecimentoSerieNivel (VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO)
    {
        try {
            $dao = new DisciplinaDAO();
            if (!$vwdacsnTO->getId_entidade()) {
                $vwdacsnTO->setId_entidade($vwdacsnTO->getSessao()->id_entidade);
            }
            $disciplina = $dao->retornaDisciplinaAreaConhecimentoSerieNivel($vwdacsnTO)->toArray();
            if (empty($disciplina)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplina, new VwDisciplinaAreaConhecimentoNivelSerieTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Dados!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que verifica se ja tem uma area na Disciplina
     * @param AreaDisciplinaTO $adTO
     * @return boolean
     */
    public function verificaAreaDisciplina (AreaDisciplinaTO $adTO)
    {
        $dao = new DisciplinaDAO();
        $disciplina = $dao->retornaAreaDisciplina($adTO);
        if (is_object($disciplina)) {
            $disciplina = $disciplina->toArray();
        } else {
            unset($disciplina);
        }
        if (!$disciplina) {
            return false;
        }
        return true;
    }

    /**
     * Método que retorna vw_disciplinaintegracaomoodle
     * @param DisciplinaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarVwDisciplinaIntegracaoMoodle (DisciplinaTO $to)
    {
        try {
            if (!$to->getId_disciplina()) {
                throw new Zend_Exception("O id_disciplina é obrigatório e não foi passado.");
            }
            $dao = new DisciplinaDAO();
            $vw = new VwDisciplinaIntegracaoMoodleTO();
            $vw->setId_disciplina($to->id_disciplina);
            $vw->setBl_ativo(true);
            $retorno = $dao->retornarVwDisciplinaIntegracaoMoodle($vw)->toArray();
            if (!empty($retorno))
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new VwDisciplinaIntegracaoMoodleTO()));
            else
                $this->mensageiro->setMensageiro('Nenhum registro encontrado', Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna apenas as entidades que ainda nao tem vinculo com a disciplina
     * @param DisciplinaTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadesDisciplinaIntegracao (DisciplinaTO $to)
    {
        try {
            if (!$to->getId_disciplina()) {
                throw new Zend_Exception("O id_disciplina é obrigatório e não foi passado.");
            }
            $boEntidade = new EntidadeBO();
            $arrayEntidades = $boEntidade->retornaEntidadeRecursivo()->getMensagem();
            $mensageiroEntidadesExistente = $this->retornarVwDisciplinaIntegracaoMoodle($to);

            if ($mensageiroEntidadesExistente->getTipo() == Ead1_IMensageiro::AVISO) {
                $this->mensageiro->setMensageiro($arrayEntidades, Ead1_IMensageiro::SUCESSO);
            } else {
                foreach ($mensageiroEntidadesExistente->getMensagem() as $entidadeCadastradas) {
                    $array[] = $entidadeCadastradas->getId_entidade();
                }
                foreach ($arrayEntidades as $key => &$entidade) {
                    if (in_array($entidade->getId_entidade(), $array)) {
                        //se ela for igual a entidade sessao ou 
                        unset($arrayEntidades[$key]);
                    }
                }
                $this->mensageiro->setMensageiro($arrayEntidades, Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que salva o st_codsistema para disciplinas integradas ao moodle
     * @param DisciplinaIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarDisciplinaIntegracao (DisciplinaIntegracaoTO $to)
    {
        $dao = new DisciplinaDAO();
        try {
            if ($to->getId_disciplinaintegracao()) {
                if ($this->verificaIntegracao($to))//verificar se ja existe alguma entidade cadastrada
                    $dao->editarDisciplinaIntegracao($to);
                else
                    throw new Zend_Exception('Já existe um registro para essa entidade e disciplina!');
            }else {
                $to->setId_disciplinaintegracao($dao->cadastrarDisciplinaIntegracao($to));
            }
            $this->mensageiro->setMensageiro($to, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna DisciplinaIntegracao para disciplinas integradas ao moodle
     * @param DisciplinaIntegracaoTO $to
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinaIntegracao (DisciplinaIntegracaoTO $to)
    {
        try {
            if (!$to->getId_sistema() || !$to->getId_disciplina()) {
                throw new Zend_Exception("O id_sistema e/ou id_disciplina são obrigatórios e não foram passados.");
            }
            $retorno = $to->fetch(false, true, true);
            if ($retorno instanceof DisciplinaIntegracaoTO)
                $this->mensageiro->setMensageiro($retorno);
            else
                $this->mensageiro->setMensageiro('Nenhum dado encontrado', Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que exclui disciplinas integradas ao moodle
     * @param DisciplinaIntegracaoTO $to
     * @return Ead1_Mensageiro
     */
    public function excluirDisciplinaIntegracao (DisciplinaIntegracaoTO $to)
    {
        $dao = new DisciplinaDAO();
        try {
            if (!$to->getId_disciplinaintegracao()) {
                throw new Zend_Exception('O id_disciplinaintegracao é obrigatorio e não foi passado!');
            }
            $to->setBl_ativo(false);
            $dao->editarDisciplinaIntegracao($to);

            $this->mensageiro->setMensageiro('Vinculo excluído com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Verifica se uma integracao ja existe na entidade e na disciplina 
     * Deve salvar apenas um st_codsistema para cada disciplina na entidade
     * @param DisciplinaIntegracaoTO $to
     * @return boolean
     */
    public function verificaIntegracao (DisciplinaIntegracaoTO $to)
    {
        $toAux = new DisciplinaIntegracaoTO();
        $toAux->setId_disciplina($to->getId_disciplina());
        $toAux->setId_entidade($to->getId_entidade());
        $toAux->setBl_ativo(true);

        $toAux->fetch(false, true, true);
        if ($toAux->getId_disciplinaintegracao() && ($to->getId_disciplinaintegracao() != $toAux->getId_disciplinaintegracao())) {
            return false;
        }
        else
            return true;
    }

}
<?php

/**
 * Classe com regras de negócio para interações de Forma de Pagamento
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 05/11/2010
 *
 * @package models
 * @subpackage bo
 */
class FormaPagamentoBO extends FinanceiroBO
{

    public $mensageiro;
    /**
     * @var FormaPagamentoDAO
     */
    public $dao;

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new FormaPagamentoDAO();
    }


    /**
     * Metodo que cadastra FormaPagamentoProduto
     * @param FormaPagamentoProdutoTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamentoProduto(FormaPagamentoProdutoTO $cpTO)
    {
        try {
            $this->dao->cadastrarFormaPagamentoProduto($cpTO);
            return $this->mensageiro->setMensageiro('Forma de Pagamento do Produto Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Forma de Pagamento do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }


    /**
     * Metodo que cadastra array de CampanhaProduto
     * @param $arrayCampanhaProduto
     * @return Ead1_Mensageiro
     */
    public function cadastrarArrayFormaPagamentoProduto($arrayFormaPagamentoProduto)
    {
        try {

            if (!$arrayFormaPagamentoProduto) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Forma de Pagamento Produto informada!', Ead1_IMensageiro::AVISO);
            }

            $cpTO = new FormaPagamentoProdutoTO();
            $cpTO->setId_formapagamento($arrayFormaPagamentoProduto[0]->id_formapagamento);
            if (!$this->dao->deletarFormaPagamentoProduto($cpTO)) {
                throw new Zend_Exception('Erro ao excluir produtos passados.');
            }


            $arSalvos = array();


            foreach ($arrayFormaPagamentoProduto as $formaPagamentoProduto) {

                if (in_array($formaPagamentoProduto->getId_Produto(), $arSalvos)) {
                    continue;
                }

                $arSalvos[] = $formaPagamentoProduto->getId_Produto();

                if (!$this->dao->cadastrarFormaPagamentoProduto($formaPagamentoProduto)) {
                    throw new Zend_Exception('Erro ao cadastrar Produto da Campanha!');
                }
            }
            return $this->mensageiro->setMensageiro('Forma(s) de Pagamento cadastrada(s) com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar as Forma(s) de Pagamento do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que Exclui CampanhaProduto
     * @param CampanhaProdutoTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamentoProduto(FormaPagamentoProdutoTO $cpTO)
    {
        try {
            $this->dao->deletarFormaPagamentoProduto($cpTO);
            return $this->mensageiro->setMensageiro('Forma de Pagamento do Produto Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Forma de Pagamento do Produto!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * @param VwFormaPagamentoProdutoTO $vwcpTO
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwFormaPagamentoProduto(VwFormaPagamentoProdutoTO $vwcpTO)
    {
        $view = $this->dao->retornarVwFormaPagamentoProduto($vwcpTO)->toArray();
        if (empty($view)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($view, new VwFormaPagamentoProdutoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que decide se Cadastra ou Edita Forma de Pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function salvarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        if ($fpTO->getId_formapagamento()) {
            return $this->editarFormaPagamento($fpTO);
        }
        return $this->cadastrarFormaPagamento($fpTO);
    }

    /**
     * Metodo que decide se salva ou exclui a Aplicação da forma de pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function salvarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        $orm = new FormaPagamentoAplicacoesORM();
        if ($orm->consulta($fpaTO, true)) {
            return $this->deletarFormaPagamentoAplicacoes($fpaTO);
        }
        return $this->cadastrarFormaPagamentoAplicacoes($fpaTO);
    }

    /**
     * @deprecated
     * Metodo que decide se salva ou edita a Distribuição do Meio de Pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @return Ead1_Mensageiro
     */
    public function salvarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $orm = new FormaPagamentoDistribuicaoORM();
        if ($orm->consulta($fpdTO, true)) {
            return $this->editarFormaPagamentoDistribuicao($fpdTO);
        }
        return $this->cadastrarFormaPagamentoDistribuicao($fpdTO);
    }

    /**
     * Metodo que decide se salva ou exclui a Forma do Meio de Pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @return Ead1_Mensageiro
     */
    public function salvarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        $orm = new FormaMeioPagamentoORM();
        if ($orm->consulta($fmpTO, true)) {
            return $this->deletarFormaMeioPagamento($fmpTO);
        }
        return $this->cadastrarFormaMeioPagamento($fmpTO);
    }

    /**
     * Metodo que decide se salva ou exclui a campanha da forma de pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @return Ead1_Mensageiro
     */
    public function salvarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        $orm = new CampanhaFormaPagamentoORM();
        if ($orm->consulta($cfpTO, true)) {
            $mensageiro = new MensageiroTO();
            return $mensageiro->setMensageiro('Esta Campanha Já Esta Cadastrada!', Ead1_IMensageiro::AVISO);
        }
        return $this->cadastrarCampanhaFormaPagamento($cfpTO);
    }


    /**
     * Salva todas as formas de pagamento para uma campanha
     * @param CampanhaFormaPagamentoTO $cTO
     * @throws Zend_Exception
     * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function vincularTodasCampanhasFormaPagamento(CampanhaFormaPagamentoTO $cTO)
    {

        try {
            $this->dao->beginTransaction();

            if (!$cTO->getId_campanhacomercial()) {
                throw new Zend_Exception('A campanha comercial é obrigatória!');
            }

            if (!$cTO->getSessao()->id_entidade) {
                throw new Zend_Exception('Você precisa estar logado para vincular as Formas de Pagamento!');
            }

            $cfpTO = new CampanhaFormaPagamentoTO();
            $cfpTO->setId_campanhacomercial($cTO->getId_campanhacomercial());
            if (!$this->dao->deletarCampanhaFormaPagamento($cfpTO)) {
                throw new Zend_Exception('Erro ao excluir forma de pagamento passadas.');
            }

            $toFP = new FormaPagamentoTO();
            $toFP->setId_entidade($toFP->getSessao()->id_entidade);

            $fORM = new FormaPagamentoORM();
            $arrFP = $fORM->consulta($toFP);

            if (empty($arrFP)) {
                $this->dao->rollBack();
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Forma de Pagamento encontrada!', Ead1_IMensageiro::SUCESSO);
            }

            $arrayCampanhaFormaPagamento = array();
            foreach ($arrFP as $fpTO) {
                $caFPTO = new CampanhaFormaPagamentoTO();
                $caFPTO->setId_campanhacomercial($cTO->getId_campanhacomercial());
                $caFPTO->setId_formapagamento($fpTO->getid_formapagamento());
                $arrayCampanhaFormaPagamento[] = $caFPTO;
            }

            foreach ($arrayCampanhaFormaPagamento as $campanhaFormaPagamentoTO) {
                if (!$this->dao->cadastrarCampanhaFormaPagamento($campanhaFormaPagamentoTO)) {
                    throw new Zend_Exception('Erro ao cadastrar Forma de Pagamento da Campanha!');
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Todas as Formas de Pagamento foram vinculadas à Campanha!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Vincular as Formas de Pagamento da Campanha!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }

    /**
     * Metodo que decide se salva ou exclui a campanha da forma de pagamento
     * @param $arrayCampanhaFormaPagamento
     * @return Ead1_Mensageiro
     */
    public function salvarArrayCampanhaFormaPagamento($arrayCampanhaFormaPagamento)
    {
        try {
            $cfpTO = new CampanhaFormaPagamentoTO();
            $cfpTO->setId_campanhacomercial($arrayCampanhaFormaPagamento[0]->id_campanhacomercial);
            if (!$this->dao->deletarCampanhaFormaPagamento($cfpTO)) {
                throw new Zend_Exception('Erro ao excluir forma de pagamento passadas.');
            }

            foreach ($arrayCampanhaFormaPagamento as $campanhaFormaPagamentoTO) {
                if (!$this->dao->cadastrarCampanhaFormaPagamento($campanhaFormaPagamentoTO)) {
                    throw new Zend_Exception('Erro ao cadastrar Forma de Pagamento da Campanha!');
                }
            }
            return $this->mensageiro->setMensageiro('Forma de Pagamento da Campanha cadastrada com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Forma de Pagamento da Campanha!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo de procedimento que salva e edita tela de Forma de Pagamento no Flex
     *
     * [Quem utiliza]
     * @see Classe::metodo()
     *
     * @param FormaPagamentoTO $fpTO
     * @param array ( FormaPagamentoParcelaTO $arrFormaPagamentoParcelaTO )
     * @param array ( FormaMeioPagamentoTO $arrFormaMeioPagamentoTOParcela )
     * @param array ( FormaMeioPagamentoTO $arrFormaMeioPgamentoTOEntrada )
     * @param array ( FormaPagamentoAplicacoesTO $arrFormaPagamentoAplicacoesTO )
     * @param array ( CampanhaFormaPagamentoTO $arrCampanhaFormaPagamentoTO )
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return Ead1_Mensageiro
     */
    public function salvarProcedimentoFormaPagamento(FormaPagamentoTO $fpTO, $arrFormaPagamentoParcelaTO = null, $arrFormaMeioPagamentoTOParcela = null, $arrFormaMeioPgamentoTOEntrada = null
        , $arrFormaPagamentoAplicacoesTO = null, $arrCampanhaFormaPagamentoTO = null, FormaPagamentoAplicacaoRelacaoTO $fparTO = null)
    {
        $this->dao->beginTransaction();
        try {
            if (!$fpTO->getId_entidade()) {
                $fpTO->setId_entidade($fpTO->getSessao()->id_entidade);
            }
            if ($fpTO->getId_formapagamento()) {
                if (!$this->dao->editarFormaPagamento($fpTO)) {
                    THROW new Zend_Exception('Erro ao Editar Forma de Pagamento!');
                }
            } else {
                $fpTO->setId_usuariocadastro($fpTO->getSessao()->id_usuario);
                $id_formapagamento = $this->dao->cadastrarFormaPagamento($fpTO);
                if (!$id_formapagamento) {
                    THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento!');
                }
                $fpTO->setId_formapagamento($id_formapagamento);
            }
            if (!is_null($arrFormaPagamentoParcelaTO)) {

                foreach ($arrFormaPagamentoParcelaTO as $chave => $fppTO) {
                    if (!($fppTO instanceof FormaPagamentoParcelaTO)) {
                        THROW new Zend_Exception('O Parametro Recebido não é uma instancia de FormaPagamentoParcelaTO!');
                    }
                    $fppTO->setId_formapagamento($fpTO->getId_formapagamento());
                    if ($fppTO->getId_formapagamentoparcela()) {
                        if (!$this->dao->editarFormaPagamentoParcela($fppTO)) {
                            THROW new Zend_Exception('Erro ao Editar Forma de Pagamento da Parcela!');
                        }
                    } else {
                        $id_formapagamentoParcela = $this->dao->cadastrarFormaPagamentoParcela($fppTO);
                        if (!$id_formapagamentoParcela) {
                            THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento da Parcela!');
                        }
                        $fppTO->setId_formapagamentoparcela($id_formapagamentoParcela);
                    }
                }
            }
            if (!is_null($arrFormaMeioPagamentoTOParcela)) {
                $delectedParcela = false;
                foreach ($arrFormaMeioPagamentoTOParcela as $chave => $fmpTO) {
                    if (!($fmpTO instanceof FormaMeioPagamentoTO)) {
                        THROW new Zend_Exception('O Parametro Recebido não é uma instancia de FormaPagamentoParcelaTO!');
                    }
                    if (!$delectedParcela) {
                        $newFmpTO = new FormaMeioPagamentoTO();
                        $newFmpTO->setId_formapagamento($fpTO->getId_formapagamento());
                        //Id do Tipo de Divisao Financeira Parcela
                        $newFmpTO->setId_tipodivisaofinanceira(TipoDivisaoFinanceiraTO::PARCELA);
                        if (!$this->dao->deletarFormaMeioPagamento($newFmpTO)) {
                            THROW new Zend_Exception('Erro ao Excluir Vinculos do Meio de Pagamento das Parcelas');
                        }
                        $delectedParcela = true;
                    }
                    $fmpTO->setId_formapagamento($fpTO->getId_formapagamento());
                    $fmpTO->setId_tipodivisaofinanceira(TipoDivisaoFinanceiraTO::PARCELA);
                    if (!$this->dao->cadastrarFormaMeioPagamento($fmpTO)) {
                        THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento das Parcelas!');
                    }
                }
            }
            if (!is_null($arrFormaMeioPgamentoTOEntrada)) {
                $delectedEntrada = false;
                foreach ($arrFormaMeioPgamentoTOEntrada as $chave => $fmpTO) {
                    if (!($fmpTO instanceof FormaMeioPagamentoTO)) {
                        THROW new Zend_Exception('O Parametro Recebido não é uma instancia de FormaPagamentoParcelaTO!');
                    }
                    if (!$delectedEntrada) {
                        $newFmpTO = new FormaMeioPagamentoTO();
                        $newFmpTO->setId_formapagamento($fpTO->getId_formapagamento());
                        //Id do Tipo de Divisao Financeira Entrada
                        $newFmpTO->setId_tipodivisaofinanceira(TipoDivisaoFinanceiraTO::ENTRADA);
                        if (!$this->dao->deletarFormaMeioPagamento($newFmpTO)) {
                            THROW new Zend_Exception('Erro ao Excluir Vinculos do Meio de Pagamento da Entrada');
                        }
                        $delectedEntrada = true;
                    }
                    $fmpTO->setId_formapagamento($fpTO->getId_formapagamento());
                    $fmpTO->setId_tipodivisaofinanceira(TipoDivisaoFinanceiraTO::ENTRADA);
                    if (!$this->dao->cadastrarFormaMeioPagamento($fmpTO)) {
                        THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento da Entrada!');
                    }
                }
            }
            if (!is_null($arrFormaPagamentoAplicacoesTO)) {
                $delectedAplicacoes = false;
                foreach ($arrFormaPagamentoAplicacoesTO as $chave => $fpaTO) {
                    if (!($fpaTO instanceof FormaPagamentoAplicacoesTO)) {
                        THROW new Zend_Exception('O Parametro Recebido não é uma instancia de FormaPagamentoAplicacoesTO!');
                    }
                    if (!$delectedAplicacoes) {
                        $newFpaTO = new FormaPagamentoAplicacoesTO();
                        $newFpaTO->setId_formapagamento($fpTO->getId_formapagamento());
                        if (!$this->dao->deletarFormaPagamentoAplicacoes($newFpaTO)) {
                            THROW new Zend_Exception('Erro ao Excluir Vinculos do Meio de Pagamento da Entrada');
                        }
                        $delectedAplicacoes = true;
                    }
                    $fpaTO->setId_formapagamento($fpTO->getId_formapagamento());
                    if (!$this->dao->cadastrarFormaPagamentoAplicacoes($fpaTO)) {
                        THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento da Entrada!');
                    }
                }
            }
            if (!is_null($arrCampanhaFormaPagamentoTO)) {
                $delectedCampanha = false;
                foreach ($arrCampanhaFormaPagamentoTO as $chave => $cfpTO) {
                    if (!($fpaTO instanceof CampanhaFormaPagamentoTO)) {
                        THROW new Zend_Exception('O Parametro Recebido não é uma instancia de CampanhaFormaPagamentoTO!');
                    }
                    if (!$delectedCampanha) {
                        $newCfpTO = new CampanhaFormaPagamentoTO();
                        $newCfpTO->setId_formapagamento($fpTO->getId_formapagamento());
                        if (!$this->dao->deletarCampanhaFormaPagamento($newCfpTO)) {
                            THROW new Zend_Exception('Erro ao Excluir Vinculos do Meio de Pagamento da Entrada');
                        }
                        $delectedCampanha = true;
                    }
                    $cfpTO->setId_formapagamento($fpTO->getId_formapagamento());
                    if (!$this->dao->cadastrarCampanhaFormaPagamento($cfpTO)) {
                        THROW new Zend_Exception('Erro ao Cadastrar Forma de Pagamento da Entrada!');
                    }
                }
            }

            if (!is_null($fparTO)) {
                try {
                    $fparTO->setId_formapagamento($fpTO->getId_formapagamento());
                    $this->dao->deletarFormaPagamentoAplicacaoRelacao($fparTO, 'id_formapagamento = ' . $fparTO->getId_formapagamento());
                    $this->dao->cadastrarFormaPagamentoAplicacaoRelacao($fparTO);
                } catch (Zend_Db_Exception $e) {
                    THROW new Zend_Exception('Erro ao Vincular a Aplicação da Forma de Pagamento!');
                }
            }

            $this->dao->commit();
            $this->mensageiro->setMensagem($fpTO);
//			if(!empty($arrEnviar)){
//				$this->mensageiro->addMensagem($arrEnviar);
//			}
            $this->mensageiro->setTipo(Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Método que salva os dias de vencimento da forma de pagamento
     * @param $arrFormaDiaVencimento
     * @return Ead1_Mensageiro
     */
    public function salvarArrFormaDiaVencimento($arrFormaDiaVencimento)
    {
        try {
            $delected = false;
            foreach ($arrFormaDiaVencimento as $to) {
                if (!($to instanceof FormaDiaVencimentoTO)) {
                    THROW new Zend_Exception("Instancia da Classe Inválida.");
                }
                if (!$to->getId_formapagamento()) {
                    THROW new Zend_Exception("O Id da Forma de Pagamento foi não Informado.");
                }
                if (!$delected) {
                    $delected = true;
                    $toDelete = new FormaDiaVencimentoTO(array('id_formapagamento' => $to->getId_formapagamento()));
                    $this->dao->deletarFormaDiaVencimento($toDelete);
                }
                $this->dao->cadastrarFormaDiaVencimento($to);
            }
            $this->mensageiro->setMensageiro('Dias de Vencimento Salvos com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Salvar Dias dos Vencimentos!', Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que salva os dias de vencimento do meio de pagamento
     * @param $arrFormaMeioVencimento
     * @return Ead1_Mensageiro
     */
    public function salvarArrFormaMeioVencimento($arrFormaMeioVencimento)
    {
        try {
            $delected = false;
            foreach ($arrFormaMeioVencimento as $to) {
                if (!($to instanceof FormaMeioVencimentoTO)) {
                    THROW new Zend_Exception("Instancia da Classe Inválida.");
                }
                if (!$to->getId_formapagamento()) {
                    THROW new Zend_Exception("O Id da Forma de Pagamento não foi Informado.");
                }
                if (!$to->getId_meiopagamento()) {
                    THROW new Zend_Exception("O Id do Meio de Pagamento não foi Informado.");
                }

                if (!$delected) {
                    $delected = true;
                    $toDelete = new FormaMeioVencimentoTO(array('id_formapagamento' => $to->getId_formapagamento()
                    , 'id_meiopagamento' => $to->getId_meiopagamento()));
                    $this->dao->deletarFormaMeioVencimento($toDelete);
                }
                $this->dao->cadastrarFormaMeioVencimento($to);
            }
            $this->mensageiro->setMensageiro('Dias de Vencimento Salvos com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Salvar Dias dos Vencimentos!', Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que cadastra a Forma de Pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        if (!$fpTO->getId_entidade()) {
            $fpTO->setId_entidade($fpTO->getSessao()->id_entidade);
        }
        $fpTO->setId_usuariocadastro($fpTO->getSessao()->id_usuario);
        $id_formaPagamento = $this->dao->cadastrarFormaPagamento($fpTO);
        if (!$id_formaPagamento) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Forma de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $fpTO->setId_formapagamento($id_formaPagamento);
        return $this->mensageiro->setMensageiro($fpTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra as Aplicações da Forma de Pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        if (!$this->dao->cadastrarFormaPagamentoAplicacoes($fpaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Aplicação da Forma de Pagamento Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * @deprecated
     * Metodo que cadastra a Distribuição do Meio de Pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        if (!$this->dao->cadastrarFormaPagamentoDistribuicao($fpdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Distribuição do Meio de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Distribuição do Meio de Pagamento Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra a Forma do Meio de Pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        if (!$this->dao->cadastrarFormaMeioPagamento($fmpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar a Forma do Meio de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma do Meio de Pagamento Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO, 1);
    }

    /**
     * Metodo que cadastra Meio de pagamento para integração com o Fluxos.
     * @param MeioPagamentoIntegracaoTO $mpiTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO)
    {
        if (!$mpiTO->getId_entidade()) {
            $mpiTO->setId_entidade($mpiTO->getSessao()->id_entidade);
        }
        $id_meiopagamentointegracao = $this->dao->cadastrarMeioPagamentoIntegracao($mpiTO);
        if (!$id_meiopagamentointegracao) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Meio Pagamento para Integração!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Meio de Pagamento para Integração Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO, 1);
    }

    /**
     * Metodo que salva Meios de pagamento para integração com o Fluxos.
     * @param array $arrMeioPagamentoIntegracao
     * @return Ead1_Mensageiro
     */
    public function salvarArrMeioPagamentoIntegracao($arrMeioPagamentoIntegracao)
    {
        try {
            $this->dao->beginTransaction();
            if (!empty($arrMeioPagamentoIntegracao)) {
                foreach ($arrMeioPagamentoIntegracao as $index => $meioPagamentoIntegracaoTO) {
                    if ($meioPagamentoIntegracaoTO->getId_meiopagamentointegracao()) {
                        if (!$this->dao->editarMeioPagamentoIntegracao($meioPagamentoIntegracaoTO)) {
                            throw new Zend_Exception($this->dao->excecao);
                        }
                    } else {
                        if (!$meioPagamentoIntegracaoTO->getId_usuariocadastro()) {
                            $meioPagamentoIntegracaoTO->setId_usuariocadastro($meioPagamentoIntegracaoTO->getSessao()->id_usuario);
                        }
                        $id_meiopagamentointegracao = $this->dao->cadastrarMeioPagamentoIntegracao($meioPagamentoIntegracaoTO);
                        if (!$id_meiopagamentointegracao) {
                            throw new Zend_Exception($this->dao->excecao);
                        }
                        $meioPagamentoIntegracaoTO->setId_meiopagamentointegracao($id_meiopagamentointegracao);
                        $arrMeioPagamentoIntegracao[$index] = $meioPagamentoIntegracaoTO;
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro($arrMeioPagamentoIntegracao, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao salvar meios de pagamento para integração.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que cadastra as Campanhas da Forma de Pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        if (!$this->dao->cadastrarCampanhaFormaPagamento($cfpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Campanha da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Campanha da Forma de Pagamento Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Cadastra a Forma de Pagamento da Parcela
     * @param FormaPagamentoParcelaTO $fppTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $id_formaPagamentoParcela = $this->dao->cadastrarFormaPagamentoParcela($fppTO);
        if (!$id_formaPagamentoParcela) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Forma de Pagamento da Parcela!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        $fppTO->setId_formapagamentoparcela($id_formaPagamentoParcela);
        return $this->mensageiro->setMensageiro($fppTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO)
    {
        try {
            $this->dao->cadastrarFormaPagamentoAplicacaoRelacao($fparTO);
            $this->mensageiro->setMensageiro('Aplicação da Forma de Pagamento Cadastrada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Edita a Forma de Pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function editarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        if (!$fpTO->getId_entidade()) {
            $fpTO->setId_entidade($fpTO->getSessao()->id_entidade);
        }
        if (!$this->dao->editarFormaPagamento($fpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Forma de Pagamento!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma de Pagamento Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita a Forma de Pagamento
     * @param MeioPagamentoIntegracaoTO $mpiTO
     * @return Ead1_Mensageiro
     */
    public function editarMeioPagamentoIntegracao(FormaPagamentoTO $mpiTO)
    {
        if (!$mpiTO->getId_entidade()) {
            $mpiTO->setId_entidade($mpiTO->getSessao()->id_entidade);
        }
        if (!$this->dao->editarMeioPagamentoIntegracao($mpiTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Meio de Pagamento Integração!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma de Pagamento Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * @deprecated
     * Metodo que Edita a Distribuição do Meio de Pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @return Ead1_Mensageiro
     */
    public function editarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        if (!$this->dao->editarFormaPagamentoDistribuicao($fpdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Distribuição do Meio de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Distribuição do Meio de Pagamento Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita a Distribuição do Meio de Pagamento
     * @param FormaPagamentoParcelaTO $fppTO
     * @return Ead1_Mensageiro
     */
    public function editarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        if (!$this->dao->editarFormaPagamentoParcela($fppTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar a Forma de Pagamento da Parcela!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma de Pagamento da Parcela Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return Ead1_Mensageiro
     */
    public function editarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO)
    {
        try {
            $this->dao->editarFormaPagamentoAplicacaoRelacao($fparTO);
            $this->mensageiro->setMensageiro('Aplicação da Forma de Pagamento Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Editar Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que deleta a relacao da forma de pagamento com a aplicacao
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO)
    {
        try {
            $this->dao->deletarFormaPagamentoAplicacaoRelacao($fparTO);
            $this->mensageiro->setMensageiro('Aplicação da Forma de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Excluir Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que Exclui as Aplicações da Forma de Pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        if (!$this->dao->deletarFormaPagamentoAplicacoes($fpaTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Aplicação da Forma de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui a Forma do Meio de Pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {
        if (!$this->dao->deletarFormaMeioPagamento($fmpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Forma do Meio de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma do Meio de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO, 2);
    }

    /**
     * Metodo que Exclui a Forma de Pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        if (!$this->dao->deletarFormaPagamento($fpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Forma de Pagamento!', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui a Distribuição do Meio de Pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        if (!$this->dao->deletarFormaPagamentoDistribuicao($fpdTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Distribuição do Meio de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Distribuição do Meio de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Exclui as Campanhas da Forma de Pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @return Ead1_Mensageiro
     */
    public function deletarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        if (!$this->dao->deletarCampanhaFormaPagamento($cfpTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Campanha da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Campanha da Forma de Pagamento Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Excluir a Distribuição do Meio de Pagamento
     * @param FormaPagamentoParcelaTO $fppTO
     * @return Ead1_Mensageiro
     */
    public function deletarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        if (!$this->dao->deletarFormaPagamentoParcela($fppTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir a Forma de Pagamento da Parcela!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Forma de Pagamento da Parcela Excluida com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Forma de Pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamento(FormaPagamentoTO $fpTO)
    {
        try {
            if (!$fpTO->getId_entidade()) {
                $fpTO->setId_entidade($fpTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarFormaPagamento($fpTO)->toArray();
            if (empty($dados)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaPagamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Forma de Pagamento por Aplicação
     * @param FormaPagamentoTO $fpTO
     * @param FormaPagamentoAplicacaoTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoPorAplicacao(FormaPagamentoTO $fpTO, FormaPagamentoAplicacaoTO $fpaTO, array $aplicacoes = null)
    {
        try {
            if (!$fpTO->getId_entidade()) {
                $fpTO->setId_entidade($fpTO->getSessao()->id_entidade);
            }
            $dados = $this->dao->retornarFormaPagamentoPorAplicacao($fpTO, $fpaTO, null, $aplicacoes);
            if (empty($dados) || (is_object($dados) && !$dados->count())) {
                return $this->mensageiro->setMensageiro('Nenhuma forma de pagamento encontrada!', Ead1_IMensageiro::AVISO);
            }
            $dados = $dados->toArray();
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaPagamentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a Forma de Pagamento
     * @param CampanhaFormaPagamentoTO $cfpTO
     * @return Ead1_Mensageiro
     */
    public function retornarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO)
    {
        $campanha = $this->dao->retornarCampanhaFormaPagamento($cfpTO);
        if (is_object($campanha)) {
            $campanha = $campanha->toArray();
        } else {
            unset($campanha);
        }
        if (!$campanha) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campanha, new CampanhaFormaPagamentoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Forma de Pagamento
     * @param FormaPagamentoAplicacoesTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO)
    {
        $aplicacoes = $this->dao->retornarFormaPagamentoAplicacoes($fpaTO);
        if (is_object($aplicacoes)) {
            $aplicacoes = $aplicacoes->toArray();
        } else {
            unset($aplicacoes);
        }
        if (!$aplicacoes) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($aplicacoes, new FormaPagamentoAplicacoesTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * @deprecated
     * Metodo que retorna a Forma de Pagamento
     * @param AplicacoesPagamentoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function retornarAplicacoesPagamento(AplicacoesPagamentoTO $apTO)
    {
        $aplicacoes = $this->dao->retornarAplicacoesPagamento($apTO);
        if (is_object($aplicacoes)) {
            $aplicacoes = $aplicacoes->toArray();
        } else {
            unset($aplicacoes);
        }
        if (!$aplicacoes) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($aplicacoes, new AplicacoesPagamentoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Distribuição da Forma de Pagamento
     * @param FormaPagamentoDistribuicaoTO $fpdTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO)
    {
        $distribuicao = $this->dao->retornarFormaPagamentoDistribuicao($fpdTO);
        if (is_object($distribuicao)) {
            $distribuicao = $distribuicao->toArray();
        } else {
            unset($distribuicao);
        }
        if (!$distribuicao) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($distribuicao, new FormaPagamentoDistribuicaoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Forma do Meio de Pagamento
     * @param FormaMeioPagamentoTO $fmpTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO)
    {

        $meioPagamento = $this->dao->retornarFormaMeioPagamento($fmpTO);
        if (is_object($meioPagamento)) {
            $meioPagamento = $meioPagamento->toArray();
        } else {
            unset($meioPagamento);
        }
        if (!$meioPagamento) {
            return new Ead1_Mensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($meioPagamento, new FormaMeioPagamentoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o de Meio de Pagamento
     * @param MeioPagamentoTO $mpTO
     * @return Ead1_Mensageiro
     */
    public function retornarMeioPagamento(MeioPagamentoTO $mpTO)
    {
        $meioPagamento = $this->dao->retornarMeioPagamento($mpTO);
        if (is_object($meioPagamento)) {
            $meioPagamento = $meioPagamento->toArray();
        } else {
            unset($meioPagamento);
        }
        if (!$meioPagamento) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($meioPagamento, new MeioPagamentoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o de Meio de Pagamento Integração
     * @param VwMeioPagamentoIntegracaoTO $vwmpiTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwMeioPagamentoIntegracao(VwMeioPagamentoIntegracaoTO $vwmpiTO)
    {
        try {
            if (!$vwmpiTO->getId_entidade()) {
                $vwmpiTO->setId_entidade($vwmpiTO->getSessao()->id_entidade);
            }
            $meioPagamento = $this->dao->retornarVwMeioPagamentoIntegracao($vwmpiTO);
            if (is_object($meioPagamento)) {
                $meioPagamento = $meioPagamento->toArray();
            } else {
                unset($meioPagamento);
            }
            if (!$meioPagamento) {
                return $this->mensageiro->setMensageiro('Nenhum Registro de Meio Pagamento Integração Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($meioPagamento, new VwMeioPagamentoIntegracaoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Meios de Pagamento para Integração', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna o Tipo de Divisão Financeira
     * @param VwFormaMeioDivisaoTO $fmdfTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoDivisaoFinanceira(VwFormaMeioDivisaoTO $fmdfTO)
    {
        $divisaoFinanceira = $this->dao->retornarTipoDivisaoFinanceira($fmdfTO);
        if (is_object($divisaoFinanceira)) {
            $divisaoFinanceira = $divisaoFinanceira->toArray();
        } else {
            unset($divisaoFinanceira);
        }
        if (!$divisaoFinanceira) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($divisaoFinanceira, new VwFormaMeioDivisaoTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna o Tipo de Forma de Pagamento Parcela
     * @param TipoFormaPagamentoParcelaTO $tfppTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoFormaPagamentoParcela(TipoFormaPagamentoParcelaTO $tfppTO)
    {
        $tipo = $this->dao->retornarTipoFormaPagamentoParcela($tfppTO);
        if (is_object($tipo)) {
            $tipo = $tipo->toArray();
        } else {
            unset($tipo);
        }
        if (!$tipo) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($tipo, new TipoFormaPagamentoParcelaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a Forma de Pagamento da Parcela
     * @param FormaPagamentoParcelaTO $fppTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO)
    {
        $parcela = $this->dao->retornarFormaPagamentoParcela($fppTO);
        if (is_object($parcela)) {
            $parcela = $parcela->toArray();
        } else {
            unset($parcela);
        }
        if (!$parcela) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($parcela, new FormaPagamentoParcelaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     *
     * Retorn as parelas meio pagamento
     * @param FormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function retornarParcelasMeioPagamento(FormaPagamentoTO $fpTO)
    {
        try {
            $formaPagamentoParcelaTO = new FormaPagamentoParcelaTO();
            $formaPagamentoParcelaTO->setId_formapagamento($fpTO->getId_formapagamento());

            $parcelas = $this->dao->retornarFormaPagamentoParcela($formaPagamentoParcelaTO);
            $formaPagamento = $this->dao->retornarFormaPagamento($fpTO);
            $vinculoPagamento = $this->dao->retornarDivisaoFormaMeioPagamento($fpTO);
            $arrAgrupador = array();
            if ($formaPagamento) {
                //$formaPagamento = $formaPagamento->toArray();
                $arrAgrupador['formaPagamento'] = Ead1_TO_Dinamico::encapsularTo($formaPagamento->toArray(), new FormaPagamentoTO());
            }
            if ($parcelas) {
                //$parcelas = $parcelas->toArray();
                $arrAgrupador['parcelas'] = Ead1_TO_Dinamico::encapsularTo($parcelas->toArray(), new FormaPagamentoParcelaTO());
            }
            if ($vinculoPagamento) {
                $arrAgrupador['divisaoMeioPagamento'] = Ead1_TO_Dinamico::encapsularTo($vinculoPagamento->toArray(), new VwDivisaoFormaMeioPagamentoTO());
            }
            return $this->mensageiro->setMensageiro($arrAgrupador, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a relação da aplicação da forma de pagamento
     * @param FormaPagamentoAplicacaoTO $fpaTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoAplicacao(FormaPagamentoAplicacaoTO $fpaTO)
    {
        try {
            $dados = $this->dao->retornarFormaPagamentoAplicacao($fpaTO)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaPagamentoAplicacaoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Aplicação da Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna a relação da aplicação da forma de pagamento
     * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
     * @return Ead1_Mensageiro
     */
    public function retornarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO)
    {
        try {
            $dados = $this->dao->retornarFormaPagamentoAplicacaoRelacao($fparTO)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaPagamentoAplicacaoRelacaoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Db_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar a Relação da Aplicação com a Forma de Pagamento!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dias de vencimento da forma de pagamento
     * @param FormaDiaVencimentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarFormaDiaVencimento(FormaDiaVencimentoTO $to)
    {
        try {
            $dados = $this->dao->retornarFormaDiaVencimento($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhum dia de Vencimento Cadastrado para a Forma de Pagamento.');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaDiaVencimentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar os Dias de Vencimento da Forma de Pagamento.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Método que retorna os dias de vencimento do meio de pagamento
     * @param FormaMeioVencimentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarFormaMeioVencimento(FormaMeioVencimentoTO $to)
    {
        try {
            $dados = $this->dao->retornarFormaMeioVencimento($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhum dia de Vencimento Cadastrado para o Meio de Pagamento.');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaMeioVencimentoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar os Dias de Vencimento do Meio de Pagamento.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna os dias de vencimento do meio de pagamento com mais dados
     * @param FormaMeioVencimentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarFormaMeioVencimentoCompleto(FormaMeioVencimentoTO $to)
    {
        try {
            $dados = $this->dao->retornarFormaMeioVencimentoCompleto($to)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception('Nenhum dia de Vencimento Cadastrado para o Meio de Pagamento.');
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new FormaMeioVencimentoCompletoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar os Dias de Vencimento do Meio de Pagamento.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

}

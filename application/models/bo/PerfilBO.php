<?php

/**
 * Classe com regras de negÃ³cio para interaÃ§Ãµes de Perfil
 * @author Eduardo RomÃ£o - ejushiro@gmail.com
 * @since 21/09/2010
 *
 * @package models
 * @subpackage bo
 */
class PerfilBO extends Ead1_BO
{

    /**
     * Atributo que contÃ©m a instancia do Ead1_Mensageiro
     * @var Ead1_Mensageiro
     */
    public $mensageiro;

    /**
     * Atributo que contÃ©m a instancia da PerfilDAO
     * @var PerfilDAO
     */
    public $dao;

    /**
     * Atributo para recursividade de funcionalidades e geraÃ§Ã£o de BreadCrumb para permissÃµes
     * @var array
     */
    private $arFuncionalidades;

    /**
     * Atributo para zerar recursividade de funcionalidades e geraÃ§Ã£o de BreadCrumb para permissÃµes
     * @var array
     */
    private $arFuncionalidadesReg = array();

    /**
     * Variável responsável por armazenar quais funcionalidades irão ser removidas do perfil
     * @var array
     */
    private $funcRemovidas = array();

    /**
     * MÃ©todo construtor
     */
    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
        $this->dao = new PerfilDAO();
    }

    /**
     * MÃ©todo que contÃ©m as regras de negÃ³cio para duplicar um perfil
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function duplicarPerfil(PerfilTO $pTO)
    {
        $tbPerfil = new PerfilORM();
        $dadosPerfil = $tbPerfil->consulta($pTO, true);
        if ($dadosPerfil) {
            $perfilTO = $dadosPerfil[0];
            $perfilTO->setSt_nomeperfil($pTO->getSt_nomeperfil());
            $perfilTO->setId_perfil(null);
            $perfilTO->setDt_cadastro(null);
            $perfil = $this->cadastrarPerfil($perfilTO);

            if ($perfil->getTipo() == Ead1_IMensageiro::SUCESSO) {

                try {

                    $peTO = new PerfilEntidadeTO();
                    $peTO->setId_entidade($pTO->getSessao()->id_entidade);
                    $infoPerfil = $perfil->getMensagem();
                    $peTO->setId_perfil($infoPerfil[0]->id_perfil);
                    $perfilEntidade = $this->cadastrarPerfilEntidade($peTO);

                    if ($perfilEntidade->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $tbPerfilFuncionalidade = new PerfilFuncionalidadeORM();
                        $pfTO = new PerfilFuncionalidadeTO();
                        $pfTO->setId_perfil($pTO->getId_perfil());
                        $dadosPerfilFuncionalidade = $tbPerfilFuncionalidade->consulta($pfTO);
                        if (is_array($dadosPerfilFuncionalidade) && !empty($dadosPerfilFuncionalidade)) {
                            foreach ($dadosPerfilFuncionalidade as $pfTOAtual) {
                                $this->cadastrarPerfilFuncionalidade($pfTOAtual);
                            }
                        }

                        $tbPerfilPermissaoFuncionalidade = new PerfilPermissaoFuncionalidadeORM();
                        $ppfTO = new PerfilPermissaoFuncionalidadeTO();
                        $ppfTO->setId_perfil($pTO->getId_perfil());
                        $dadosPerfilPermissaoFuncionalidade = $tbPerfilPermissaoFuncionalidade->consulta($ppfTO);
                        if (is_array($dadosPerfilPermissaoFuncionalidade) && !empty($dadosPerfilPermissaoFuncionalidade)) {
                            $this->cadastrarPerfilPermissaoFuncionalidade($dadosPerfilPermissaoFuncionalidade);
                            /* foreach($dadosPerfilPermissaoFuncionalidade as $ppfTOAtual){
                              $this->dao->cadastrarPerfilPermissaoFuncionalidade($ppfTOAtual);
                              } */
                        }

                        $vwPerfisEntidade = new VwPerfisEntidadeORM();
                        $vwTO = new VwPerfisEntidadeTO();
                        $vwTO->setId_entidade($pTO->getSessao()->id_entidade);
                        $vwTO->setId_perfil($infoPerfil[0]->id_perfil);

                        return new Ead1_Mensageiro($vwPerfisEntidade->consulta($vwTO, false, true));
                    } else {
                        return $perfilEntidade;
                    }
                } catch (Exception $exc) {

                    return $this->mensageiro->setMensageiro('Erro ao duplicar o perfil', Ead1_IMensageiro::ERRO, $exc->getMessage());
                }
            } else {
                return $perfil;
            }
        } else {
            return $this->mensageiro->setMensageiro("Perfil não encontrado!", Ead1_IMensageiro::ERRO, $pTO);
        }
    }

    /**
     * Metodo que decide de cadastra ou edita a funcionalidade para um perfil
     * @param $pTO
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function salvarPerfilFuncionalidade(PerfilFuncionalidadeTO $pfTO)
    {
        $orm = new PerfilFuncionalidadeORM();
        if ($orm->consulta($pfTO, true)) {
            return $this->editarPerfilFuncionalidade($pfTO);
        }
        return $this->cadastrarPerfilFuncionalidade($pfTO);
    }

    /**
     * Metodo que cadastra entidades do perfil.
     * @param array $arrayDeEntidades
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilEntidadeArvore($arrayDeEntidades, PerfilTO $pTO)
    {
        try {
            $peTO = new PerfilEntidadeTO();
            $peTO->setId_perfil($pTO->getId_perfil());
            if (!$this->dao->deletarPerfilEntidade($peTO)) {
                throw new Zend_Exception('Erro ao deletar ' . Ead1_LabelFuncionalidade::getLabel(3) . '(s) antigas');
            }
            foreach ($arrayDeEntidades as $entidade) {
                $entidade->setId_perfil($peTO->getId_perfil());
                if (!$this->dao->cadastrarPerfilEntidade($entidade)) {
                    throw new Zend_Exception('Erro no insert');
                }
            }
            return $this->mensageiro->setMensageiro(Ead1_LabelFuncionalidade::getLabel(3) . "(s) cadastrado(s) com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar ' . Ead1_LabelFuncionalidade::getLabel(3) . ' para ' . Ead1_LabelFuncionalidade::getLabel(56) . '!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que decide se edita ou cadastra dados/ Usuario de/a um Perfil
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     */
    public function salvarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO = null)
    {

        $upeTOtemporario = new UsuarioPerfilEntidadeTO();
        $upeTOtemporario->setId_usuario($upeTO->getId_usuario());
        $upeTOtemporario->setId_entidade($upeTO->getId_entidade());
        $upeTOtemporario->setId_perfil($upeTO->getId_perfil());

        if ($upeTOtemporario->fetch(false, true, true)) {
            $retorno = $this->editarUsuarioPerfilEntidade($upeTO);
            return $retorno;
        }

        $retorno = $this->cadastrarUsuarioPerfilEntidade($upeTO);

        //link_alteracao: http://ead1.net/fengoffice/index.php?c=task&a=view_task&id=2856
        $perfilTO = new PerfilTO();
        $perfilTO->setId_perfil($upeTO->getId_perfil());
        $perfilTO->fetch(true, true, true);
        if ($perfilTO->getId_perfilpedagogico() == PerfilTO::PERFIL_PEDAGOGICO_OBSERVADOR || $perfilTO->getId_perfilpedagogico() == PerfilTO::PERFIL_PEDAGOGICO_SUPORTE) {
            $usuarioPerfilEntidade = new UsuarioPerfilEntidadeReferenciaTO();
            $usuarioPerfilEntidade->setId_perfil($upeTO->getId_perfil());
            $usuarioPerfilEntidade->setId_entidade($upeTO->getSessao()->id_entidade);
            $usuarioPerfilEntidade->setId_usuario($upeTO->getId_usuario());
            $usuarioPerfilEntidade->setBl_ativo(true);
            $perfilDAO = new ProjetoPedagogicoDAO();
            $perfilDAO->salvarCoordenador($usuarioPerfilEntidade);
        }

        return $retorno;
    }

    /**
     * Metodo que cadastra/edita as Funcionalidades de um perfil
     * @param array $arrTO
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayPerfilFuncionalidade($arrTO, PerfilTO $pTO)
    {
        if (!is_array($arrTO)) {
            return $this->mensageiro->setMensageiro(\G2\Constante\MensagemSistema::NAO_ARRAY, Ead1_IMensageiro::AVISO);
        }
        $this->dao->beginTransaction();
        try {
            $pfTO = new PerfilFuncionalidadeTO();
            $pfTO->setId_perfil($pTO->getId_perfil());
            $funcPerfil = $this->dao->retornaFuncionalidadePerfil($pfTO);
            if(!empty($funcPerfil)){
                $funcCadastradas = Ead1_TO_Dinamico::encapsularTo($funcPerfil->toArray(), new FuncionalidadeTO());
            }
            $pfTO->setBl_ativo(false);
            $this->dao->editarPerfilFuncionalidadeSemBotoes($pfTO);
            $arFuncionalidades = array();
            $funcPerfilBlAtivo = $this->dao->retornaFuncionalidadePerfil($pfTO);
            if(empty($funcPerfilBlAtivo)){
                $this->dao->rollBack();
                return $this->mensageiro->setMensageiro(\G2\Constante\MensagemSistema::IMPOSSIVEL_RECUPERAR_FUNCIONALIDADES, Ead1_IMensageiro::ERRO);
            }
            $resposta = $funcPerfilBlAtivo->toArray();
            if (!empty($resposta)) {
                $resposta = Ead1_TO_Dinamico::encapsularTo($resposta, new FuncionalidadeTO());
                foreach ($resposta as $funcionalidade) {
                    $arFuncionalidades[$funcionalidade->getId_funcionalidade()] = $funcionalidade;
                }
            }

            // cria o array com as funcionalidades cadastradas no banco que possuem bl ativo.
            $funcCadastradas2 = array();
            if(!empty($funcCadastradas)) {
                foreach ($funcCadastradas as $index => $value) {
                    if ($value->getBl_ativo())
                        $funcCadastradas2[] = $value->getId_funcionalidade();
                }
            }

            // guarda os ids que serão mantidos (aqueles selecionados na página)
            $manter = array();
            if(!empty($arrTO)){
                foreach ($arrTO as $index){
                    $manter[] = $index->getId_funcionalidade();
                }
            }

            foreach ($arrTO as $fTO) {
                if (!($fTO instanceof FuncionalidadeTO)) {
                    Throw new Zend_Exception(\G2\Constante\MensagemSistema::NAO_OBJ_FUNCTO);
                }

                // recupera os dados da funcionalidade visando encontrar a funcionalidade pai
                try {
                    $dadosFuncionalidade = $this->negocio->find('\G2\Entity\Funcionalidade', $fTO->getId_funcionalidade());
                }catch(\Zend_Exception $e){
                    $this->dao->rollBack();
                    return $this->mensageiro->setMensageiro(\G2\Constante\MensagemSistema::IMPOSSIVEL_RECUPERAR_FUNCIONALIDADE_PAI, Ead1_IMensageiro::ERRO);
                }

                // grava ou remove a funcionalidade baseado na seleção ou deseleção na tela
                if($dadosFuncionalidade) {
                    if ($dadosFuncionalidade->getId_funcionalidadepai() instanceof \G2\Entity\Funcionalidade) {
                        if(!empty($dadosFuncionalidade->getId_funcionalidadepai()->getId_funcionalidade())){
                            if(!in_array($dadosFuncionalidade->getId_funcionalidadepai()->getId_funcionalidade(), $funcCadastradas2) &&
                                !in_array($dadosFuncionalidade->getId_funcionalidadepai()->getId_funcionalidade(),$manter)){
                                $this->dao->rollBack();
                                return $this->mensageiro->setMensageiro(\G2\Constante\MensagemSistema::SELECIONE_FUNC_PAI_CAD_FILHO, Ead1_IMensageiro::ERRO);
                            }
                        }
                        try{
                        if($this->removeAdicionaRecursivo($dadosFuncionalidade->getId_funcionalidade(), $manter)){
                            $pfTO->setBl_ativo(true);
                        }
                        else{
                            $this->funcRemovidas[] = $dadosFuncionalidade->getId_funcionalidade();
                            $this->excluirPermissoes($pfTO->getId_perfil());
                            $pfTO->setBl_ativo(false);
                        }
                        }catch (\Zend_Exception $e){
                            $this->dao->rollBack();
                            return $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
                        }
                    } else {
                        $pfTO->setBl_ativo(true);
                    }

                    $pfTO->setId_funcionalidade($fTO->getId_funcionalidade());
                    if (array_key_exists($fTO->getId_funcionalidade(), $arFuncionalidades)) {
                        $this->dao->editarPerfilFuncionalidade($pfTO);
                    } else {
                        $this->dao->cadastrarPerfilFuncionalidade($pfTO);
                    }
                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro(\G2\Constante\MensagemSistema::FUNC_SALVAS, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            $this->mensageiro->setCodigo($e->getMessage());
            $this->mensageiro->setTipo(Ead1_IMensageiro::ERRO);
            $this->mensageiro->setMensagem(\G2\Constante\MensagemSistema::ERRO_SALVAR_FUNC);
            return $this->mensageiro;
        }
    }
    /**
     * Metodo recursivo que percorre a funcionalidade pai em busca de verificar se ela se encontra selecionada
     * @param Int $filho
     * @param Array $manter
     * @return Boolean
     * @throws \Zend_Exception
     */
    private function removeAdicionaRecursivo($filho, $manter){
        try {
            $dadosFuncionalidade = $this->negocio->find('\G2\Entity\Funcionalidade',$filho);
        }catch(\Zend_Exception $e){
            Throw new Zend_Exception(\G2\Constante\MensagemSistema::IMPOSSIVEL_REMOVER_FUNC);
        }

        if(!empty($dadosFuncionalidade)){
            if(empty($dadosFuncionalidade->getId_funcionalidadepai()))
                return true;
            if(in_array($dadosFuncionalidade->getId_funcionalidadepai()->getId_funcionalidade(), $manter)){
                return $this->removeAdicionaRecursivo($dadosFuncionalidade->getId_funcionalidadepai()->getId_funcionalidade(), $manter);
            }
            else
                return false;
        } else
            return true;
    }
    /**
     * Metodo que cadastra Pefil no Sistema
     * @param EntidadeTO $eTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfil(PerfilTO $pTO, $nomeExibicao = NULL)
    {
        $orm = new PerfilORM();

        //Verifica se o perfil que está sendo cadastrado possui duplicidade de nomes, por entidade.
        $dados = $orm->fetchAll(array(
            "st_nomeperfil = '{$pTO->getSt_nomeperfil()}'",
            "bl_ativo = 1",
            "id_entidade = {$pTO->getSessao()->id_entidade}"
        ))->count();

        if ($dados) {
            return $this->mensageiro->setMensageiro('Não é possível adicionar dois perfis com o mesmo nome.', Ead1_IMensageiro::AVISO);
        }

        //Fluxo principal do método
        $pTO->setId_entidade($pTO->getSessao()->id_entidade);
        $pTO->setId_usuariocadastro($pTO->getSessao()->id_usuario);
        $id_perfil = $this->dao->cadastrarPerfil($pTO, $nomeExibicao);
        if (!$id_perfil) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Perfil!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        $pTO->setId_perfil($id_perfil);

        return $this->mensageiro->setMensageiro($pTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que adiciona Permissao ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilFuncionalidade(PerfilFuncionalidadeTO $pfTO)
    {
        if (!$this->dao->cadastrarPerfilFuncionalidade($pfTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar Funcionalidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }

        return $this->mensageiro->setMensageiro($pfTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que cadastra o Perfil da Entidade
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilEntidade(PerfilEntidadeTO $peTO)
    {
        if (!$this->dao->cadastrarPerfilEntidade($peTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Cadastrar o Perfil da Entidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Perfil da Entidade Cadastrado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Adiciona permissões ao perfil para realizar uma determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO $ppfTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilPermissaoFuncionalidade($arPerfilPermissaoFuncionalidadeTO)
    {
        try {
            $this->dao->beginTransaction();
            if (!is_array($arPerfilPermissaoFuncionalidadeTO)) {
                THROW new Zend_Exception('O parametro recebido não é um array');
            }
            if (empty($arPerfilPermissaoFuncionalidadeTO)) {
                THROW new Zend_Exception('Não foi enviada nenhuma permissão para ser salva!');
            }
            $id_perfil = null;
            $arrangeArray = array();
            //Prevendo e Removendo Duplicidade de funcionalidades e permissoes
            foreach ($arPerfilPermissaoFuncionalidadeTO as $to) {
                $id_perfil = $to->getId_perfil();

                if(in_array($to->getId_funcionalidade(), $this->funcRemovidas)){
                    continue;
                }
                if (!($to instanceof PerfilPermissaoFuncionalidadeTO)) {
                    THROW new Zend_Exception('Um dos items do array não é uma instancia de PerfilPermissaoFuncionalidadeTO');
                }
                $arrangeArray[$to->getId_funcionalidade()][$to->getId_permissao()] = $to->getId_permissao();
            }
            if (!$id_perfil) {
                THROW new Zend_Exception('O Id do Perfil não veio setado!');
            }
            $reOrderArray = array();
            foreach ($arrangeArray as $id_funcionalidade => $arrPermissoes) {
                foreach ($arrPermissoes as $id_permissao) {
                    $newTO = new PerfilPermissaoFuncionalidadeTO();
                    $newTO->setId_funcionalidade($id_funcionalidade);
                    $newTO->setId_perfil($id_perfil);
                    $newTO->setId_permissao($id_permissao);
                    $reOrderArray[] = $newTO;
                }
            }
            $arPerfilPermissaoFuncionalidadeTO = $reOrderArray;
            $perfilPermissaoFuncionalidadeORM = new PerfilPermissaoFuncionalidadeORM();
            $perfilFuncionalidadeORM = new PerfilFuncionalidadeORM();
            $funcionalidadeTO = new FuncionalidadeTO();
            $perfilFuncionalidadeTO = new PerfilFuncionalidadeTO();

            $this->dao->deletarPerfilPermissaoFuncionalidade($id_perfil);

            foreach ($arPerfilPermissaoFuncionalidadeTO as $perfilPermissaoFuncionalidadeTO) {
                $this->dao->cadastrarPerfilPermissaoFuncionalidade($perfilPermissaoFuncionalidadeTO);

//código comentado para consertar o problema que ocorria quando cadastrava uma permissão duplicada que possuia bl_ativo 0 e 1.
//manter o código até ter certeza que não impacta o funcionamento do sistema.
//                if ($perfilPermissaoFuncionalidadeTO->getId_permissao() == PermissaoTO::PERMISSAO_ADICIONAR) {
//                    $funcionalidadeTO->setId_funcionalidadepai($perfilPermissaoFuncionalidadeTO->getId_funcionalidade());
//                    $funcionalidadeTO->setId_tipofuncionalidade(TipoFuncionalidadeTO::TIPO_FUNCIONALIDADE_BOTAO_ADICIONAR_MENU);
//
//                    $funcionalidadeBotao = $this->dao->retornaFuncionalidade($funcionalidadeTO);
//                    print_r($perfilPermissaoFuncionalidadeTO);
//
//                    if ($funcionalidadeBotao->count() == 1) {
//                        $botao = $funcionalidadeBotao->current();
//                        $perfilFuncionalidadeTO->setId_funcionalidade($botao->id_funcionalidade);
//                        $perfilFuncionalidadeTO->setId_perfil($perfilPermissaoFuncionalidadeTO->getId_perfil());
//                        if ($perfilFuncionalidadeORM->consulta($perfilFuncionalidadeTO, true)) {
//                            $perfilFuncionalidadeTO->setBl_ativo(1);
//                            $this->dao->editarPerfilFuncionalidade($perfilFuncionalidadeTO);
//                        } else {
//                            $perfilFuncionalidadeTO->setBl_ativo(1);
//                            $this->dao->cadastrarPerfilFuncionalidade($perfilFuncionalidadeTO);
//                        }
//                    }
//                }
            }
            $this->dao->commit();
            return $this->mensageiro->setMensageiro('Permissões Vinculadas com Sucesso', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao Salvar Permissões!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que exclui todas as permissões vinculadas ao perfil
     * @param int $idPerfil
     * @return Ead1_Mensageiro/Boolean
     */

    public function excluirPermissoes($idPerfil){
        $this->dao->beginTransaction();
        try{
            $this->dao->deletarPerfilPermissaoFuncionalidade($idPerfil);
            $this->dao->commit();
            return true;
        }catch (\Zend_Exception $e){
            $this->dao->rollBack();
            return $this->mensageiro->setMensageiro('Erro ao excluir Permissões!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }
    /**
     * Metodo que Adiciona Usuario a um Perfil
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     * @throws Exception
     * @updated Kayo Silva <kayo.silva@unyleya.com.br> 2015-05-15
     */
    public function cadastrarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        try {
            $upeTO->setDt_cadastro(date('Y-m-d'));
            if (!$upeTO->getId_entidade()) {
                $upeTO->setId_entidade($upeTO->getSessao()->id_entidade);
            }
            if (!$upeTO->getId_entidadeidentificacao()) {
                $upeTO->setId_entidadeidentificacao($upeTO->getSessao()->id_entidade);
            }
            $erro = '';
            if (!$this->validaData($upeTO->getDt_inicio())) {
                $this->mensageiro->addMensagem('Data de Início Inválida!');
                $erro = 1;
            }
            if ($upeTO->getDt_termino()) {
                if (!$this->validaData($upeTO->getDt_termino())) {
                    $this->mensageiro->addMensagem('Data de Término Invválida!');
                    $erro = 1;
                }
                if (!$this->comparaData($upeTO->getDt_inicio(), $upeTO->getDt_termino())) {
                    return $this->mensageiro->addMensagem('A data de término não pode ser menor que a data de início!');
                    $erro = 1;
                }
            }
            if ($erro) {
                $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                return $this->mensageiro;
            }
            $salvarTeste = $this->dao->cadastrarUsuarioPerfilEntidade($upeTO);
            if ($salvarTeste) {
                return $this->mensageiro->setMensageiro('Usuário Vinculado ao Perfil com Sucesso!', Ead1_IMensageiro::SUCESSO);
            } else {
                throw new Exception("Erro ao salvar Perfil.");
            }
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Vincular Usuário ao Perfil!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que edita Pefil no Sistema
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function editarPerfil(PerfilTO $pTO, $nomeExibicao = NULL)
    {

        //Objeto PerfilTO temporário para que seja possível a verificação se já existe um cadastro com o mesmo nome, por entidade.
        $temp_pTO = new PerfilTO();
        $temp_pTO->setId_entidade($pTO->getSessao()->id_entidade);
        $temp_pTO->setSt_nomeperfil($pTO->getSt_nomeperfil());

        $perfilDAO = new PerfilDAO();


        //Verifica se o perfil que está sendo editado possui duplicidade de nomes, por entidade.
        $dados = $perfilDAO->retornaPerfil($temp_pTO)->toArray();

        /* Converte o array em TO para que possamos verificar se o nome do projeto
         * que está sendo editado não é o dele mesmo.
         */
        if (!empty($dados)) {
            $verificadorTO = Ead1_TO_Dinamico::encapsularTo($dados, new PerfilTO(), true);
        } else {
            $verificadorTO = new PerfilTO();
        }

        //Verifica se o nome do perfil é igual ao de outro perfil, que não seja ele mesmo.
        if ($verificadorTO->getSt_nomeperfil() == $pTO->getSt_nomeperfil() && $verificadorTO->getId_perfil() != $pTO->getId_perfil()) {
            return $this->mensageiro->setMensageiro('Não é possível adicionar dois perfis com o mesmo nome.', Ead1_IMensageiro::AVISO);
        }

        if (!$pTO->getId_entidade()) {
            $pTO->setId_entidade($pTO->getSessao()->id_entidade);
        }

        if (!$this->dao->editarPerfil($pTO, $nomeExibicao)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Perfil', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }

        return $this->mensageiro->setMensageiro('Perfil Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita Permissao ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO
     * @return boolean
     */
    public function editarPerfilFuncionalidade(PerfilFuncionalidadeTO $pfTO)
    {
        if (!$this->dao->editarPerfilFuncionalidade($pfTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Funcionalidade do Perfil!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Funcionalidade do Perfil Editada com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que Edita o Perfil da Entidade
     * @param PerfilEntidadeTO $peO
     * @return boolean
     */
    public function editarPerfilEntidade(PerfilEntidadeTO $peTO)
    {
        if (!$peTO->getId_entidade()) {
            $peTO->setId_entidade($peTO->getSessao()->id_entidade);
        }
        if (!$this->dao->editarPerfilEntidadeAlias($peTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar o Perfil da Entidade', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Perfil da Entidade Editado com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que "edita" Permissao ao perfil a determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO
     * @return boolean
     */
    public function editarPerfilPermissaoFuncionalidade(PerfilPermissaoFuncionalidadeTO $ppfTO)
    {
        if (!$this->dao->editarPerfilPermissaoFuncionalidade($ppfTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Editar Permissão do Perfil a Funcionalidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro($ppfTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que edita dados do Peril do Usuario
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     */
    public function editarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $upeTO)
    {
        try {
            if (!$upeTO->getId_entidade()) {
                $upeTO->setId_entidade($upeTO->getSessao()->id_entidade);
            }
            $erro = '';
            if (!$this->validaData($upeTO->getDt_inicio())) {
                $this->mensageiro->addMensagem('Data de Início Inválida!');
                $erro = 1;
            }
            if ($upeTO->getDt_termino()) {
                if (!$this->validaData($upeTO->getDt_termino())) {
                    $this->mensageiro->addMensagem('Data de Término Inválida!');
                    $erro = 1;
                }
                if (!$this->comparaData($upeTO->getDt_inicio(), $upeTO->getDt_termino())) {
                    return $this->mensageiro->addMensagem('A data de término não pode ser menor que a data de inÃ­cio!');
                    $erro = 1;
                }
            }
            if ($erro) {
                $this->mensageiro->setTipo(Ead1_IMensageiro::AVISO);
                return $this->mensageiro;
            }
            $this->dao->editarUsuarioPerfilEntidade($upeTO);
            return $this->mensageiro->setMensageiro('Perfil do usuário editado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao editar perfil do usuário!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que deleta Permissao ao perfil a determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO
     * @return boolean
     */
    public function deletarPerfilPermissaoFuncionalidade(PerfilPermissaoFuncionalidadeTO $ppfTO)
    {
        if (!$this->dao->deletarPerfilPermissaoFuncionalidade($ppfTO)) {
            return $this->mensageiro->setMensageiro('Erro ao Excluir Permissão!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro('Permissão da Funcionalidade Excluída com Sucesso!', Ead1_IMensageiro::SUCESSO);
    }

    /*
     * Metodo que retorna todos os Perfis da entidade
     * @param VwPerfisEntidadeTO $vwPeTO
     * @return Ead1_Mensageiro
     */

    public function retornaPerfisEntidade(VwPerfisEntidadeTO $vwPeTO)
    {
        if (!$vwPeTO->getId_entidadecadastro()) {
            $vwPeTO->setId_entidadecadastro($vwPeTO->getSessao()->id_entidade);
        }

        $perfisEntidade = $this->dao->retornaPerfisEntidade($vwPeTO);
        if ($perfisEntidade) {
            if (empty($perfisEntidade)) {
                return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            } else {
                return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfisEntidade, new VwPerfisEntidadeTO()), Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Perfis da Entidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /*
     * Metodo que retorna todos as entidades de forma recursiva com perfil selecionado ou não
     * Caso o perfil esteja selecionado para esta entidade o id_perfil estarão setado
     * 
     * @param $vwPeTO
     * @return Ead1_Mensageiro
     */

    public function retornaEntidadeRecursivaPerfil($vwPeTO)
    {
        if (is_array($vwPeTO)) {
            try {
                $complementoSql = "";
                $complementoSql .= (array_key_exists('id_entidade', $vwPeTO) && $vwPeTO['id_entidade'] ?: "NULL");

                $qry = $this->em->getConnection()
                    ->prepare("EXEC sp_perfilentidaderecursivo {$complementoSql}");
                $qry->execute();
                $perfisEntidade = $qry->fetchAll();
                if ($perfisEntidade) {
                    return $this->mensageiro->setMensageiro($perfisEntidade, Ead1_IMensageiro::SUCESSO);
                } else {
                    return false;
                }
            } catch (Zend_Exception $e) {
                $this->excecao = $e->getMessage();
                Zend_Debug::dump($e);
                exit;
                return false;
            }
        } else {

            if (!$vwPeTO->getId_entidade()) {
                $vwPeTO->setId_entidade($vwPeTO->getSessao()->id_entidade);
            }
            $perfisEntidade = $this->dao->retornaEntidadeRecursivaPerfil($vwPeTO);
            if ($perfisEntidade) {
                if (empty($perfisEntidade)) {
                    return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
                } else {
                    return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfisEntidade, new VwPerfisEntidadeTO()), Ead1_IMensageiro::SUCESSO);
                }
            } else {
                return $this->mensageiro->setMensageiro('Erro ao Retornar as Entidades!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
            }
        }
    }

    /**
     * Metodo que retorna as Pessoas em determinado Perfil
     * @param VwPessoaPerfilTO $vwPpTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoasPerfil(VwPessoaPerfilTO $vwPpTO)
    {
        $perfisPessoa = $this->dao->retornaPessoasPerfil($vwPpTO);
        if (is_object($perfisPessoa)) {
            $perfisPessoa = $perfisPessoa->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar o Perfil!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($perfisPessoa)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfisPessoa, new VwPessoaPerfilTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as Pessoas disponÃ­veis para determinado Perfil
     * @param UsuarioPerfilEntidadeTO $vwPpTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoasDisponiveisPerfil(VwUsuarioPerfilEntidadeTO $vwPpTO)
    {
        try {
            $vwUsuarioPerfisTO = new VwUsuarioPerfisTO($vwPpTO->toArray());
            $vwUsuarioPerfisTO->setBl_ativo(true);
            $dados = $this->dao->retornarVwUsuarioPerfis($vwUsuarioPerfisTO)->toArray();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum Usuário Encontrado.");
            }
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwPesquisarPessoaTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Pessoas.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna o Perfil
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfil(PerfilTO $pTO)
    {
        if (!$pTO->getId_entidade()) {
            $pTO->setId_entidade($pTO->getSessao()->id_entidade);
        }
        $perfil = $this->dao->retornaPerfil($pTO);
        if (is_object($perfil)) {
            $perfil = $perfil->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Perfil!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($perfil)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfil, new PerfilTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna os perfis da entidade(instituiÃ§Ã£o) relacionado com as funcionalidades
     * @param VwPerfilEntidadeFuncionalidadeTO $vwPefTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfilEntidadeFuncionalidade(VwPerfilEntidadeFuncionalidadeTO $vwPefTO)
    {
        if (!$vwPefTO->getId_entidade()) {
            $vwPefTO->setId_entidade($vwPefTO->getSessao()->id_entidade);
        }
        $funcionalidade = $this->dao->retornaPerfilEntidadeFuncionalidade($vwPefTO);
        if (is_object($funcionalidade)) {
            $funcionalidade = $funcionalidade->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao retornar as Funcionalidades da Entidade!!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
        if (empty($funcionalidade)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil Encontrado!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcionalidade, new VwPerfilEntidadeFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as permissoes de uma determinada funcionalidade
     * @param VwPerfilPermissaoFuncionalidadeTO $vwPpfTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfilPermissaoFuncionalidade(VwPerfilPermissaoFuncionalidadeTO $vwPpfTO)
    {
        $permissoes = $this->dao->retornaPerfilPermissaoFuncionalidade($vwPpfTO);
        if (is_object($permissoes)) {
            $permissoes = $permissoes->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Permissões da Funcionalidade da Entidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (!$permissoes) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Permissão da Funcionalidade Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($permissoes, new VwPerfilPermissaoFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as funcionalidades
     * @param FuncionalidadeTO $fTO
     * @return Ead1_Mensageiro
     */
    public function retornaFuncionalidade(FuncionalidadeTO $fTO)
    {
        $funcionalidade = $this->dao->retornaFuncionalidade($fTO);
        if (is_object($funcionalidade)) {
            $funcionalidade = $funcionalidade->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Funcionalidades!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($funcionalidade)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Funcionalidade Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcionalidade, new FuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna as funcionalidades da entidade
     * @param VwEntidadeFuncionalidadeTO $vwefTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeFuncionalidade(VwEntidadeFuncionalidadeTO $vwefTO)
    {
        if (!$vwefTO->getId_entidade()) {
            $vwefTO->setId_entidade($vwefTO->getSessao()->id_entidade);
        }
        $funcionalidades = $this->dao->retornaVwEntidadeFuncionalidade($vwefTO);
        if (is_object($funcionalidades)) {
            $funcionalidades = $funcionalidades->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Funcionalidades da Entidade!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($funcionalidades)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de funcionalidade da Entidade Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcionalidades, new VwEntidadeFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorna o perfil do usuÃ¡rio
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaUsuarioPerfil(UsuarioTO $uTO)
    {
        $usuarioPerfil = $this->dao->retornaUsuarioPerfil($uTO);

        $usuarioPerfil = $usuarioPerfil->toArray();
        if (!$usuarioPerfil) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil pra esse Usuário Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($usuarioPerfil, new VwUsuarioPerfilEntidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo que retorna a arvore de Funcionalidades da Entidade para o Super Usuario
     * @param VwEntidadeFuncionalidadeTO $vwEfTO
     * @return Ead1_Mensageiro
     */
    public function retornaArvoreEntidadeFuncionalidadeSuperUsuario(VwEntidadeFuncionalidadeTO $vwEfTO)
    {
        try {
            if (!$vwEfTO->getId_entidade()) {
                $vwEfTO->setId_entidade($vwEfTO->getSessao()->id_entidade);
            }
            $arvore = array();
            $arvoreGestor = array();
            $arvorePortal = array();
            if (!$vwEfTO->getId_sistema() || $vwEfTO->getId_sistema() == SistemaTO::GESTOR) {
                $where = 'id_sistema = ' . SistemaTO::GESTOR;
                $funcionalidades = $this->dao->retornaFuncionalidade(new FuncionalidadeTO(), $where)->toArray();
                if (!empty($funcionalidades)) {
                    $this->arFuncionalidades = Ead1_TO_Dinamico::encapsularTo($funcionalidades, new FuncionalidadeTO());
                    foreach ($this->arFuncionalidades as $funcionalidadeTO) {
                        if (!$funcionalidadeTO->getId_funcionalidadepai()) {
                            $arvoreGestor[$funcionalidadeTO->getId_funcionalidade()]['to'] = $funcionalidadeTO;
                            $arvoreGestor[$funcionalidadeTO->getId_funcionalidade()]['label'] = $funcionalidadeTO->getSt_funcionalidade();
                            $arvoreGestor[$funcionalidadeTO->getId_funcionalidade()]['tipo'] = $funcionalidadeTO->getId_tipofuncionalidade();
                            $arvoreGestor[$funcionalidadeTO->getId_funcionalidade()]['children'] = $this->funcionalidadeRecursiva($funcionalidadeTO->getId_funcionalidade());
                        }
                    }
                    $arvore[SistemaTO::GESTOR]['to'] = new FuncionalidadeTO();
                    $arvore[SistemaTO::GESTOR]['label'] = 'Gestor 2';
                    $arvore[SistemaTO::GESTOR]['tipo'] = 0;
                    $arvore[SistemaTO::GESTOR]['children'] = $arvoreGestor;
                }
            }

            if (!$vwEfTO->getId_sistema() || $vwEfTO->getId_sistema() == SistemaTO::PORTAL_ALUNO) {
                $where = 'id_sistema = ' . SistemaTO::PORTAL_ALUNO;
                $funcionalidades = $this->dao->retornaFuncionalidade(new FuncionalidadeTO(), $where)->toArray();
                if (!empty($funcionalidades)) {
                    $this->arFuncionalidades = Ead1_TO_Dinamico::encapsularTo($funcionalidades, new FuncionalidadeTO());
                    foreach ($this->arFuncionalidades as $funcionalidadeTO) {
                        if (!$funcionalidadeTO->getId_funcionalidadepai()) {
                            $arvorePortal[$funcionalidadeTO->getId_funcionalidade()]['to'] = $funcionalidadeTO;
                            $arvorePortal[$funcionalidadeTO->getId_funcionalidade()]['label'] = $funcionalidadeTO->getSt_funcionalidade();
                            $arvorePortal[$funcionalidadeTO->getId_funcionalidade()]['tipo'] = $funcionalidadeTO->getId_tipofuncionalidade();
                            $arvorePortal[$funcionalidadeTO->getId_funcionalidade()]['children'] = $this->funcionalidadeRecursiva($funcionalidadeTO->getId_funcionalidade());
                        }
                    }
                    $arvore[SistemaTO::PORTAL_ALUNO]['to'] = new FuncionalidadeTO();
                    $arvore[SistemaTO::PORTAL_ALUNO]['label'] = 'Portal do Aluno';
                    $arvore[SistemaTO::PORTAL_ALUNO]['tipo'] = 0;
                    $arvore[SistemaTO::PORTAL_ALUNO]['children'] = $arvorePortal;
                }
            }

            return $this->mensageiro->setMensageiro($arvore, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Árvore de Funcionalidades.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna a arvore de Funcionalidades da Entidade
     * @param VwEntidadeFuncionalidadeTO $vwEfTO
     * @return Ead1_Mensageiro
     */
    public function retornaArvoreEntidadeFuncionalidade(VwEntidadeFuncionalidadeTO $vwEfTO)
    {
        try {
            if (!$vwEfTO->getId_entidade()) {
                $vwEfTO->setId_entidade($vwEfTO->getSessao()->id_entidade);
            }
            $arvore = array();
            $arvoreGestor = array();
            $arvorePortal = array();
            if (!$vwEfTO->getId_sistema() || $vwEfTO->getId_sistema() == SistemaTO::GESTOR) {

                $where = 'id_entidade = ' . $vwEfTO->getId_entidade() . ' AND bl_funcionalidadeativo = 1 AND bl_entidadefuncionalidadeativo = 1 AND id_sistema = ' . SistemaTO::GESTOR;
                $funcionalidades = $this->dao->retornaVwEntidadeFuncionalidade($vwEfTO, $where)->toArray();
                if (!empty($funcionalidades)) {
                    $this->arFuncionalidades = Ead1_TO_Dinamico::encapsularTo($funcionalidades, new VwEntidadeFuncionalidadeTO());
                    foreach ($this->arFuncionalidades as $funcionalidade) {
                        if (!$funcionalidade->getId_funcionalidadepai()) {
                            $funcionalidadeTO = new FuncionalidadeTO();
                            $funcionalidadeTO->setId_funcionalidade($funcionalidade->getId_funcionalidade());
                            $funcionalidadeTO->setId_funcionalidadepai($funcionalidade->getId_funcionalidadepai());
                            $funcionalidadeTO->setId_situacao($funcionalidade->getId_situacaofuncionalidade());
                            $funcionalidadeTO->setId_tipofuncionalidade($funcionalidade->getId_tipofuncionalidade());
                            $funcionalidadeTO->setBl_ativo($funcionalidade->getBl_funcionalidadeativo());
                            $funcionalidadeTO->setNu_ordem($funcionalidade->getNu_ordemfuncionalidade());
                            $funcionalidadeTO->setSt_classeflex($funcionalidade->getSt_classeflex());
                            $funcionalidadeTO->setSt_funcionalidade($funcionalidade->getSt_funcionalidade());
                            $funcionalidadeTO->setSt_urlicone($funcionalidade->getSt_urlicone());

                            $arvoreGestor[$funcionalidade->getId_funcionalidade()]['to'] = $funcionalidadeTO;
                            $arvoreGestor[$funcionalidade->getId_funcionalidade()]['label'] = $funcionalidade->getSt_funcionalidade();
                            $arvoreGestor[$funcionalidade->getId_funcionalidade()]['tipo'] = $funcionalidade->getId_tipofuncionalidade();
                            $arvoreGestor[$funcionalidade->getId_funcionalidade()]['children'] = $this->funcionalidadeRecursiva($funcionalidade->getId_funcionalidade());
                        }
                    }
                    $arvore[SistemaTO::GESTOR]['to'] = new FuncionalidadeTO();
                    $arvore[SistemaTO::GESTOR]['label'] = 'Gestor 2';
                    $arvore[SistemaTO::GESTOR]['tipo'] = 0;
                    $arvore[SistemaTO::GESTOR]['children'] = $arvoreGestor;
                }
            }

            if (!$vwEfTO->getId_sistema() || $vwEfTO->getId_sistema() == SistemaTO::PORTAL_ALUNO) {
                $where = 'id_entidade = ' . $vwEfTO->getId_entidade() . ' AND bl_funcionalidadeativo = 1 AND bl_entidadefuncionalidadeativo = 1 AND id_sistema = ' . SistemaTO::PORTAL_ALUNO;
                $funcionalidades = $this->dao->retornaVwEntidadeFuncionalidade($vwEfTO, $where)->toArray();
                if (!empty($funcionalidades)) {
                    $this->arFuncionalidades = Ead1_TO_Dinamico::encapsularTo($funcionalidades, new VwEntidadeFuncionalidadeTO());
                    foreach ($this->arFuncionalidades as $funcionalidade) {
                        if (!$funcionalidade->getId_funcionalidadepai()) {
                            $funcionalidadeTO = new FuncionalidadeTO();
                            $funcionalidadeTO->setId_funcionalidade($funcionalidade->getId_funcionalidade());
                            $funcionalidadeTO->setId_funcionalidadepai($funcionalidade->getId_funcionalidadepai());
                            $funcionalidadeTO->setId_situacao($funcionalidade->getId_situacaofuncionalidade());
                            $funcionalidadeTO->setId_tipofuncionalidade($funcionalidade->getId_tipofuncionalidade());
                            $funcionalidadeTO->setBl_ativo($funcionalidade->getBl_funcionalidadeativo());
                            $funcionalidadeTO->setNu_ordem($funcionalidade->getNu_ordemfuncionalidade());
                            $funcionalidadeTO->setSt_classeflex($funcionalidade->getSt_classeflex());
                            $funcionalidadeTO->setSt_funcionalidade($funcionalidade->getSt_funcionalidade());
                            $funcionalidadeTO->setSt_urlicone($funcionalidade->getSt_urlicone());

                            $arvorePortal[$funcionalidade->getId_funcionalidade()]['to'] = $funcionalidadeTO;
                            $arvorePortal[$funcionalidade->getId_funcionalidade()]['label'] = $funcionalidade->getSt_funcionalidade();
                            $arvorePortal[$funcionalidade->getId_funcionalidade()]['tipo'] = $funcionalidade->getId_tipofuncionalidade();
                            $arvorePortal[$funcionalidade->getId_funcionalidade()]['children'] = $this->funcionalidadeRecursiva($funcionalidade->getId_funcionalidade());
                        }
                    }
                    $arvore[SistemaTO::PORTAL_ALUNO]['to'] = new FuncionalidadeTO();
                    $arvore[SistemaTO::PORTAL_ALUNO]['label'] = 'Portal do Aluno';
                    $arvore[SistemaTO::PORTAL_ALUNO]['tipo'] = 0;
                    $arvore[SistemaTO::PORTAL_ALUNO]['children'] = $arvorePortal;
                }
            }

            return $this->mensageiro->setMensageiro($arvore, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Árvore de Funcionalidades.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna as funcionalidades filhas recurssivamente
     * @param int $id_funcionalidade
     * @throws Zend_Exception
     */
    public function funcionalidadeRecursiva($id_funcionalidade)
    {
        try {
            $filha = array();
            $arr = array();
            foreach ($this->arFuncionalidades as $funcionalidade) {
                if ($funcionalidade->getId_funcionalidadepai() == $id_funcionalidade) {
                    $funcionalidadeTO = null;
                    if ($funcionalidade instanceof FuncionalidadeTO) {
                        $funcionalidadeTO = $funcionalidade;
                    } else {
                        $funcionalidadeTO = new FuncionalidadeTO();
                        $funcionalidadeTO->setId_funcionalidade($funcionalidade->getId_funcionalidade());
                        $funcionalidadeTO->setId_funcionalidadepai($funcionalidade->getId_funcionalidadepai());
                        $funcionalidadeTO->setId_situacao($funcionalidade->getId_situacaofuncionalidade());
                        $funcionalidadeTO->setId_tipofuncionalidade($funcionalidade->getId_tipofuncionalidade());
                        $funcionalidadeTO->setBl_ativo($funcionalidade->getBl_funcionalidadeativo());
                        $funcionalidadeTO->setNu_ordem($funcionalidade->getNu_ordemfuncionalidade());
                        $funcionalidadeTO->setSt_classeflex($funcionalidade->getSt_classeflex());
                        $funcionalidadeTO->setSt_funcionalidade($funcionalidade->getSt_funcionalidade());
                        $funcionalidadeTO->setSt_urlicone($funcionalidade->getSt_urlicone());
                    }
                    if ($funcionalidade->getId_tipofuncionalidade() != 6) {
                        $filha[$funcionalidade->getId_funcionalidade()]['to'] = $funcionalidadeTO;
                        $filha[$funcionalidade->getId_funcionalidade()]['label'] = $funcionalidade->getSt_funcionalidade();
                        $filha[$funcionalidade->getId_funcionalidade()]['tipo'] = $funcionalidade->getId_tipofuncionalidade();
                        $filha[$funcionalidade->getId_funcionalidade()]['children'] = $this->funcionalidadeRecursiva($funcionalidade->getId_funcionalidade());
                    } else {
                        $arr = $this->funcionalidadeRecursiva($funcionalidade->getId_funcionalidade());
                        $filha = array_merge($filha, $arr);
                    }
                }
            }
            return (!empty($filha) ? $filha : array());
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que retorna as funcionalidades do perfil
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function retornaFuncionalidadePerfil(PerfilFuncionalidadeTO $pfTO)
    {
        $funcionalidade = $this->dao->retornaFuncionalidadePerfil($pfTO);
        if (is_object($funcionalidade)) {
            $funcionalidade = $funcionalidade->toArray();
        } else {
            return $this->mensageiro->setMensageiro('Erro ao Retornar as Funcionalidades do Perfil!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
        if (empty($funcionalidade)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Funcionalidade do Perfil Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($funcionalidade, new PerfilFuncionalidadeTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * MÃ©todo que retorna as permissÃµes de 1 perfil
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornaPermissoesPerfil(PerfilTO $pTO)
    {
        if (!$pTO->getId_perfil()) {
            return new Ead1_Mensageiro("NÃ£o existe um perfil selecionado para consulta.", Ead1_IMensageiro::ERRO);
        }

        if ($obj = $this->dao->retornaPermissoesPerfil($pTO)) {
            $dados = $obj->toArray();
        } else {
            return new Ead1_Mensageiro("Erro ao consultar as permissões.", Ead1_IMensageiro::ERRO);
        }

        $this->arFuncionalidades = $dados;
        $arFuncionalidadesLocal = array();
        foreach ($this->arFuncionalidades as $valor) {

            $permissoes = null;
            foreach ($this->arFuncionalidades as $permissao) {
                if ($permissao['id_funcionalidade'] == $valor['id_funcionalidade'] && $permissao['id_permissao'] != null) {

                    $permissoes[$permissao['id_permissao']]['id_perfil'] = $valor['id_perfil'];
                    $permissoes[$permissao['id_permissao']]['id_funcionalidade'] = $valor['id_funcionalidade'];
                    $permissoes[$permissao['id_permissao']]['id_permissao'] = $permissao['id_permissao'];

                    $permissaoEncapsulado = Ead1_TO_Dinamico::encapsularTo($permissoes, new PerfilPermissaoFuncionalidadeTO(), true);
                    $permissaoEncapsulado->id_permissao = $permissao['id_permissao'];

                    $permissoesDados[$permissao['id_funcionalidade']][$permissao['id_permissao']]['to'] = $permissaoEncapsulado;
                    $permissoesDados[$permissao['id_funcionalidade']][$permissao['id_permissao']]['nome'] = $permissao['st_nomepermissao'];
                    $permissoesDados[$permissao['id_funcionalidade']][$permissao['id_permissao']]['tip'] = $permissao['st_descricao'];
                    $permissoesDados[$permissao['id_funcionalidade']][$permissao['id_permissao']]['selecionado'] = ($permissao['selecionado'] == 1 ? true : false);
                }
            }
            if (!empty($permissoes)) {
                $breadCrumb = $this->verificaFuncionalidadePai($valor['st_funcionalidade'], $valor['id_funcionalidadepai']);

                $arFuncionalidadesLocal[$breadCrumb]['id_funcionalidade'] = $valor['id_funcionalidade'];
                $arFuncionalidadesLocal[$breadCrumb]['breadcrumb'] = $breadCrumb;
                $arFuncionalidadesLocal[$breadCrumb]['checkbox'] = $permissoesDados[$valor['id_funcionalidade']];
            }
            $this->arFuncionalidadesReg = array();
        }
        ksort($arFuncionalidadesLocal);
        return new Ead1_Mensageiro(array_values($arFuncionalidadesLocal));
    }

    /**
     * MÃ©todo recursivo para montar o BreadCrumb de funcionalidades
     * @param String $nome
     * @param mixed $idPai
     * @return String
     */
    private function verificaFuncionalidadePai($nome, $idPai = null)
    {
        if ($idPai) {
            foreach ($this->arFuncionalidades as $dado) {
                if ($dado['id_funcionalidade'] == $idPai && !array_search($dado['id_funcionalidade'], $this->arFuncionalidadesReg)) {
                    if ($dado['id_tipofuncionalidade'] != 6) {
                        $nome = $dado['st_funcionalidade'] . " > " . $nome;
                    }
                    $this->arFuncionalidadesReg[$idPai] = $dado['id_funcionalidade'];
                    if ($dado['id_funcionalidadepai']) {
                        $nome = $this->verificaFuncionalidadePai($nome, $dado['id_funcionalidadepai']);
                    }
                }
            }
        }
        return $nome;
    }

    /**
     * Retorna os perfis pedagÃ³gicos
     * Utiliza os atributos do TO como filtro
     * @param $perfilPedagogicoTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilPedagogico(PerfilPedagogicoTO $perfilPedagogicoTO)
    {
        try {
            $perfilDAO = new PerfilDAO();
            $rowset = $perfilDAO->retornarPerfilPedagogico($perfilPedagogicoTO);
            if ($rowset->count() == 0) {
                return $this->mensageiro->setMensageiro('Nenhum registro encontrado', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($rowset->toArray(), new PerfilPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Perfil Pedagógico', Ead1_IMensageiro::ERRO, $exc->getMessage());
        }
    }

    /**
     * Metodo que retorna as Instituições do perfil
     * @param PerfilEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilEntidade(PerfilEntidadeTO $to)
    {
        try {
            $dados = $this->dao->retornarPerfilEntidade($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new PerfilEntidadeTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar ' . Ead1_LabelFuncionalidade::getLabel(3) . ' de ' . Ead1_LabelFuncionalidade::getLabel(56), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna as Instituições do perfil com mais dados.
     * @param PerfilEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilEntidadeCompleto(PerfilEntidadeTO $to)
    {
        try {
            $dados = $this->dao->retornarPerfilEntidadeCompleto($to)->toArray();
            if (empty($dados)) {
                $this->mensageiro->setMensageiro('Nenhum Registro de Unidade Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new PerfilEntidadeCompletoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar ' . Ead1_LabelFuncionalidade::getLabel(3) . ' de ' . Ead1_LabelFuncionalidade::getLabel(56), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Retorna os perfis pedagÃ³gicos disponíveis para entidade
     * Utiliza os atributos do TO como filtro
     * @param PerfilTO $perfilTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilPedagogicoDisponivel(PerfilTO $perfilTO)
    {
        try {
            if (!$perfilTO->getId_entidade()) {
                $perfilTO->setId_entidade($perfilTO->getSessao()->id_entidade);
            }
            $perfilDAO = new PerfilDAO();
            $rowset = $perfilDAO->retornarPerfilPedagogicoDisponivel($perfilTO);
            $perfisPedagogicos = $rowset->toArray();
            if (empty($perfisPedagogicos)) {
                return $this->mensageiro->setMensageiro('Nenhum perfil pedagógico disponível para essa entidade.', Ead1_IMensageiro::AVISO);
            }
            return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($perfisPedagogicos, new PerfilPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            return $this->mensageiro->setMensageiro('Erro ao Retornar Perfil Pedagógico', Ead1_IMensageiro::ERRO, $exc->getMessage());
        }
    }

    /**
     * Metodo que exclui(Logicamente) um perfil
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function excluirPerfil(PerfilTO $pTO, $nomeExibicao = null)
    {

        $perfilDAO = new PerfilDAO();

        try {
            //Valida se o bl_ativo está como false:
            if ($pTO->getBl_ativo() != false) {
                $this->mensageiro->setMensageiro("Erro ao excluir perfil.", Ead1_IMensageiro::ERRO, $perfilDAO->excecao);
            }

            if (!$pTO->getId_entidade()) {
                $pTO->setId_entidade($pTO->getSessao()->id_entidade);
            }

            $perfilDAO->excluirPerfil($pTO, $nomeExibicao);
            $this->mensageiro->setMensageiro("Perfil excluído com sucesso!", Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao excluir perfil.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Retorna o perfil do usuÃ¡rio
     * @param UsuarioPerfilEntidadeTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornarUsuarioPerfilEntidade(UsuarioPerfilEntidadeTO $uTO)
    {
        $usuarioPerfil = $this->dao->retornarUsuarioPerfilEntidade($uTO);

        $usuarioPerfil = $usuarioPerfil->toArray();
        if (!$usuarioPerfil) {
            return $this->mensageiro->setMensageiro('Nenhum Registro de Perfil pra esse Usuário Encontrado!', Ead1_IMensageiro::AVISO);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($usuarioPerfil, new UsuarioPerfilEntidadeTO()), Ead1_IMensageiro::SUCESSO);
    }


}

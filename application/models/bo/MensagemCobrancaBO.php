<?php

/**
 * Classe com regras de negócio para o MensagemCobranca
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 
 * @package models
 * @subpackage bo
 */
class MensagemCobrancaBO extends Ead1_BO {



	/**
	 * @var MensagemCobrancaDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->dao = new MensagemCobrancaDAO();
	}
    
    
    
    
	/**
	 * Método de listagem de MensagemCobranca
	 * @param VwMensagemCobrancaTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarMensagemCobranca(VwMensagemCobrancaTO $to){
		
		try {
			
			$to->setId_entidade(($to->getid_entidade() ? $to->getid_entidade() : $to->getSessao()->id_entidade));
			
			return new Ead1_Mensageiro($this->dao->listarMensagemCobranca($to), Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Mensagens de Cobrança: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
    
	/**
	 * Método de listagem de Mensagem de Cobranca para o Robo
	 * @param VwMensagemCobrancaRoboTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarVwMensagemCobrancaRobo(VwMensagemCobrancaRoboTO $to){
		
		try {
			
			$mensagens = $this->dao->listarMensagemCobrancaRobo($to);
			if($mensagens){
				return new Ead1_Mensageiro($mensagens, Ead1_IMensageiro::SUCESSO);
			} else {
				return new Ead1_Mensageiro("Nenhuma Mensagem encontrada.", Ead1_IMensageiro::AVISO);
			}
			
		} catch (Zend_Exception $e) {
			return new Ead1_Mensageiro("Erro ao listar as Mensagens de Cobrança: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
		
	}
    
	
	/**
	 * Salva a Mensagem de Cobrança para Envio
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function registrarMensagensCobrancaRobo(){
		
		$this->dao->beginTransaction();
		try {
			
			$mensagens = $this->listarVwMensagemCobrancaRobo(new VwMensagemCobrancaRoboTO());
			
			if($mensagens->getTipo()==Ead1_IMensageiro::SUCESSO){
				
				
				$mensagem = new MensagemBO();
				foreach($mensagens->getMensagem() as $to){
					$mensageiro = $mensagem->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::COBRANCA, $to, TipoEnvioTO::EMAIL, false);
					if($mensageiro->getTipo()!=Ead1_IMensageiro::SUCESSO){
						throw new Zend_Exception($mensageiro->getFirstMensagem());
					}
				}
				
				$processamento = new ProcessamentoTO();
				$processamento->setDt_processamento(new Zend_Date());
				$processamento->setId_processo(ProcessoTO::MENSAGEM_COBRANCA);
				
				$tramite = new TramiteBO();
				$tramite->cadastrarProcessamento($processamento);
				
			}
			
			$this->dao->commit();
			return new Ead1_Mensageiro('Registro das Mensagens de Cobranças realizado com sucesso!', Ead1_IMensageiro::SUCESSO);
			
		} catch (Exception $e) {
			$this->dao->rollBack();			
			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
		}

		
	}
    
    
	/**
	 * Metodo que decide se cadastra ou edita o MensagemCobranca
	 * @param MensagemCobrancaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarMensagemCobranca(MensagemCobrancaTO $to){
	
		$this->dao->beginTransaction();
		try {
			
			
			
			if($to->getid_mensagemcobranca()){
				
				$to->setId_entidade(($to->getid_entidade() ? $to->getid_entidade() : $to->getSessao()->id_entidade));
				
				$mensageiro = $this->editarMensagemCobranca($to);
			} else {
				$mensageiro =  $this->cadastrarMensagemCobranca($to);
			}

			if($mensageiro->getTipo()==Ead1_IMensageiro::ERRO){
				throw new Zend_Exception($mensageiro->getCurrent());
			}
	
			$this->dao->commit();
			return $mensageiro;
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Erro ao salvar o MensagemCobranca: ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	
	}


	
	/**
	 * Método que cadastra umo MensagemCobranca
	 * @param MensagemCobrancaTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see MensagemCobrancaBO::salvarMensagemCobranca();
	 */
	public function cadastrarMensagemCobranca(MensagemCobrancaTO $to){
		$this->dao->beginTransaction();
		try {

			$to->setId_usuariocadastro($to->getSessao()->id_usuario);
			$to->setId_entidade($to->getSessao()->id_entidade);
			$to->setBl_ativo(true);
			$to->setid_mensagemcobranca($this->dao->cadastrarMensagemCobranca($to));
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível cadastrar o MensagemCobranca, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    
    
	/**
	 * Método que edita umo MensagemCobranca
	 * @param MensagemCobrancaTO $to
	 * @return Ambigous <Ambigous, mixed, multitype:>|Ead1_Mensageiro
	 * @see MensagemCobrancaBO::salvarMensagemCobranca();
	 */
	public function editarMensagemCobranca(MensagemCobrancaTO $to){
		$this->dao->beginTransaction();
		try {
			
			$this->dao->editarMensagemCobranca($to);
			$to->fetch(true,true,true);
			
			$this->dao->commit();
			return new Ead1_Mensageiro($to, Ead1_IMensageiro::SUCESSO);
		} catch (Zend_Exception $e) {
			$this->dao->rollBack();
			return new Ead1_Mensageiro("Não foi possível editar o MensagemCobranca, ".$e->getMessage(), Ead1_IMensageiro::ERRO, $e);
		}
	}
    
    

}
<?php

/**
 * Classe com regras de negócio para interações de Relatórios
 * @author Eder Lamar
 * @since 07/11/2011
 *
 * @package models
 * @subpackage bo
 */
class RelatorioBO extends Ead1_BO
{

    /**
     * @var RelatorioDAO
     */
    public $dao;

    public function __construct()
    {
        parent::__construct();
        $this->dao = new RelatorioDAO();
    }

    /**
     * Método criado inicialmente para gerar o XLS do Enc. Financeiro mas pode ser usado em outros relatórios.
     *
     * @param $dadosCabecalho
     * @param $dadosCorpo
     * @param bool $titulo
     * @param bool $filename
     * @param bool $converterUtf
     * @param bool $converterEnconding
     */
    public function gerarXls($dadosCabecalho, $dadosCorpo, $titulo = false, $filename = false, $converterUtf = false, $converterEnconding = false)
    {

        $xls = new Ead1_GeradorXLS();
        $sessao = new Zend_Session_Namespace('geral');
        $loginBO = new LoginBO();
        $mensageiro = $loginBO->retornaDadosUsuario($sessao->id_usuario, $sessao->id_entidade, $sessao->id_perfil);
        if ($mensageiro->getCodigo() == Ead1_IMensageiro::AVISO) {
            echo 'Erro ao gerar o documento. Tente novamente por favor';
            exit;
        }

        $usuarioTO = $mensageiro->getMensagem();
        $usuarioTO = $usuarioTO[0];


        if ($converterUtf == true) {
            $dadosCabecalho = utf8_encode($dadosCabecalho);
            $dadosCorpo = utf8_encode($dadosCorpo);
        }

        $cabecalho = json_decode($dadosCabecalho, true);
        $dados = json_decode($dadosCorpo, true);

        $arXlsHeaderTO = array();
        foreach ($cabecalho as $index => $colunaCabecalho) {
            $xlsHeaderTO = new XLSHeaderTO();
            $xlsHeaderTO->setSt_header($colunaCabecalho);
            $xlsHeaderTO->setSt_par($index);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }

        $arTO = Ead1_TO_Dinamico::encapsularTo($dados, new ParametroRelatorioTO(), false, false, true);

        $x = new XLSConfigurationTO();
        $x->setTitle((!empty($titulo) ? $titulo : 'Relat&oacute;rio de Pesquisa '));
        $x->setFooter('Gerado por: ' . $usuarioTO->st_nomecompleto . ' - ' . $usuarioTO->st_cpf . ' Data: ' . date('d/m/Y H:i:s'));
        $x->setFilename((!empty($filename) ? $filename : 'relatorio'));

        $xls->geraXLS($arXlsHeaderTO, $arTO, $x, $converterEnconding);
    }


    /**
     * Método que cadastra um relatório com suas propriedades
     * @param RelatorioTO $relatorioTO
     * @param array $dadosCampos
     * @return Ead1_Mensageiro
     */
    public function salvarRelatorio(RelatorioTO $relatorioTO, $dadosCampos)
    {
        $relatorioTO->setId_entidade(($relatorioTO->getId_entidade() ? $relatorioTO->getId_entidade() : $relatorioTO->getSessao()->id_entidade));
        $relatorioTO->setId_usuario((int)($relatorioTO->getId_usuario() ? $relatorioTO->getId_usuario() : $relatorioTO->getSessao()->id_usuario));
        $dao = new RelatorioDAO();
        $dao->beginTransaction();
        try {
            if ($relatorioTO->getId_relatorio()) {
                $idRelatorio = $relatorioTO->getId_relatorio();
                if ($dao->atualizarRelatorio($relatorioTO) === true) {
                    $dao->deletarCamposRelatorio($idRelatorio);
                } else {
                    throw new Zend_Exception("Erro ao atualizar Relatório!", 0);
                }
            } else {
                $idRelatorio = $dao->cadastrarRelatorio($relatorioTO);
                if ($idRelatorio instanceof Ead1_Mensageiro) {
                    throw new Zend_Exception($idRelatorio->getCurrent(), 0);
                }
                if ($idRelatorio == 0) {
                    throw new Zend_Exception("Erro ao cadastrar Relatório! Id 0.", 0);
                }
                $relatorioTO->setId_relatorio($idRelatorio);
            }

            $this->mensageiro->setMensageiro($relatorioTO);
            foreach ($dadosCampos as $campoPropriedades) {
                $this->mensageiro->addMensagem($this->cadastrarCampoPropriedades($campoPropriedades, $relatorioTO));
            }
            $dao->commit();
            return $this->mensageiro;
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que cadastra campo de Relatório e suas propriedades
     * @param array $campoPropriedades
     * @param RelatorioTO $relatorioTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarCampoPropriedades($campoPropriedades, RelatorioTO $relatorioTO)
    {
        $dao = new RelatorioDAO();
        $dao->beginTransaction();
        try {
            if ($campoPropriedades[0] instanceof CampoRelatorioTO) {
                $campoRelatorioTO = $campoPropriedades[0];
                $campoRelatorioTO->setId_relatorio($relatorioTO->getId_relatorio());
                $campoRelatorioTO->setId_camporelatorio(0);
                $campoRelatorioTO->setId_camporelatorio($dao->cadastrarCampoRelatorio($campoRelatorioTO));
            } else {
                throw new Zend_Exception('O índice 0 de um dos campos não representa um CampoRelatorioTO', 0);
            }
            if (isset($campoPropriedades[1]) && $campoRelatorioTO->getBl_filtro()) {
                foreach ($campoPropriedades[1] as $propriedade) {
                    if ($propriedade instanceof PropriedadeCampoRelatorioTO) {
                        if ($propriedade->getSt_valor()) {
                            $propriedade->setId_camporelatorio($campoRelatorioTO->getId_camporelatorio());
                            $dao->cadastrarPropriedadeCampoRelatorio($propriedade);
                        }
                    } else {
                        throw new Zend_Exception('Existem propriedades de configuração que não são PropriedadeCampoRelatorioTO.', 0);
                    }
                }
            }
            $dao->commit();
            return new Ead1_Mensageiro('Campo ' . $campoRelatorioTO->getSt_titulocampo() . ' e Propriedades Cadastrados com Sucesso!');
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            throw $e;
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna as propriedades para o relatório
     * @param TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelTO
     * @return Ead1_Mensageiro
     */
    public function retornarPropriedadesRelatorio(TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelatorioTO = null)
    {
        try {
            $dao = new RelatorioDAO();
            $propriedades = $dao->retornarPropriedadesRelatorio($tipoPropriedadeCampoRelatorioTO);
            $arPropriedades = array();
            if ($propriedades->getTipo() == Ead1_IMensageiro::SUCESSO) {
                foreach ($propriedades->getMensagem() as $chave => $propriedade) {
                    $arPropriedades[$chave]['to'] = $propriedade;
                    $arPropriedades[$chave]['dados'] = $this->trataPropriedades($propriedade);
                }
            }
            return new Ead1_Mensageiro($arPropriedades);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que trata casos específicos para propriedades que precisam de valor para DropDown ou outros componentes
     * @param TipoPropriedadeCampoRelTO $propriedade
     */
    public function trataPropriedades(TipoPropriedadeCampoRelTO $propriedade)
    {
        switch ($propriedade->getId_tipopropriedadecamporel()) {
            case 1: //Operação Relatório
                $dadosTO = Ead1_ORM::instance(new OperacaoRelatorioORM())->consulta(new OperacaoRelatorioTO());
                foreach ($dadosTO as $dado) {
                    $dados[$dado->id_operacaorelatorio]['id'] = $dado->id_operacaorelatorio;
                    $dados[$dado->id_operacaorelatorio]['texto'] = $dado->st_operacaorelatorio;
                }
                break;
            case 2: //Interface
                $dadosTO = Ead1_ORM::instance(new InterfaceORM())->consulta(new InterfaceTO());
                foreach ($dadosTO as $dado) {
                    $dados[$dado->id_interface]['id'] = $dado->id_interface;
                    $dados[$dado->id_interface]['texto'] = $dado->st_interface;
                }
                break;
            case 3: //Tipo Campo
                $dadosTO = Ead1_ORM::instance(new TipoCampoORM())->consulta(new TipoCampoTO());
                foreach ($dadosTO as $dado) {
                    $dados[$dado->id_tipocampo]['id'] = $dado->id_tipocampo;
                    $dados[$dado->id_tipocampo]['texto'] = $dado->st_tipocampo;
                }
                break;
            default:
                $dados = '';
        }

        return $dados;
    }

    /**
     * Método que retorna os tipos de origem para o relatório
     * @param TipoOrigemRelatorioTO $tipoOrigemRelatorioTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoOrigemRelatorio(TipoOrigemRelatorioTO $tipoOrigemRelatorioTO = null)
    {
        try {
            $dao = new RelatorioDAO();
            return $dao->retornarTipoOrigemRelatorio($tipoOrigemRelatorioTO);
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Método que retorna as views que não são do esquema principal DBO
     */
    public function retornarViewsRelatorio()
    {
        try {
            $dao = new RelatorioDAO();
            return new Ead1_Mensageiro($dao->retornarViewsRelatorio());
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Metodo que retorna relatório
     * @param RelatorioTO $relatorioTO
     * @return Ead1_Mensageiro
     */
    public function retornarRelatorio(RelatorioTO $relatorioTO)
    {
        try {
            if (!$relatorioTO->getId_entidade()) {
                $relatorioTO->setId_entidade($relatorioTO->getSessao()->id_entidade);
            }
            $relatorio = $this->dao->retornarRelatorio($relatorioTO);
            if (is_object($relatorio)) {
                $relatorio = $relatorio->toArray();
            } else {
                unset($relatorio);
            }
            if (!$relatorio) {
                return new Ead1_Mensageiro('Nenhum Registro de Relatório Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($relatorio, new RelatorioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar relatório!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna campos do relatório
     * @param CampoRelatorioTO $campoRelatorioTO
     * @return Ead1_Mensageiro
     */
    public function retornarCampoRelatorio(CampoRelatorioTO $campoRelatorioTO)
    {
        try {
            $campos = $this->dao->retornarCampoRelatorio($campoRelatorioTO);
            if (is_object($campos)) {
                $campos = $campos->toArray();
            } else {
                unset($campos);
            }
            if (!$campos) {
                return new Ead1_Mensageiro('Nenhum Registro de Campo do Relatório Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($campos, new CampoRelatorioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar campos do relatório!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Metodo que retorna propriedades de campo do relatório
     * @param PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO
     * @return Ead1_Mensageiro
     */
    public function retornarPropriedadeCampoRelatorio(PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO)
    {
        try {
            $campos = $this->dao->retornarPropriedadeCampoRelatorio($propriedadeCampoRelatorioTO);
            if (is_object($campos)) {
                $campos = $campos->toArray();
            } else {
                unset($campos);
            }
            if (!$campos) {
                return new Ead1_Mensageiro('Nenhum Registro de Propriedade de Campo do Relatório Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
            }
            return new Ead1_Mensageiro(Ead1_TO_Dinamico::encapsularTo($campos, new PropriedadeCampoRelatorioTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar proprieadades de campo do relatório!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Método que retorna as configurações do relatório
     * @param VwDadosRelatorioTO $vwDadosRelatorioTO
     * @param string $labelResposta
     * @return Ead1_Mensageiro
     */
    public function retornarVwDadosRelatorio($params)
    {
        try {

            $arCampos = array();
            $configuracoes = $this->em->getRepository('G2\Entity\VwDadosRelatorio')->findBy($params);
            if ($configuracoes) {
                $relatorio = $this->em->getRepository('G2\Entity\Relatorio')
                    ->find($configuracoes[0]->getId_Relatorio());
                $arCampos['relatorio'] = $this->toArrayEntity($relatorio);
                $arCampos['filtros'] = array();
                $arCampos['exibicao'] = array();
                foreach ($configuracoes as $configuracao) {
                    if ($configuracao->getBl_filtro()) {
                        if (isset($arCampos['filtros'][$configuracao->getId_camporelatorio()])) {
                            $arCampos['filtros'][$configuracao->getId_camporelatorio()] = $this->trataFiltros($configuracao, $arCampos['filtros'][$configuracao->getId_camporelatorio()]);
                            $arCampos['filtros'][$configuracao->getId_camporelatorio()]['st_camporelatorio'] = $configuracao->getSt_camporelatorio();
                        } else {
                            $arCampos['filtros'][$configuracao->getId_camporelatorio()] = $this->trataFiltros($configuracao);
                            $arCampos['filtros'][$configuracao->getId_camporelatorio()]['st_camporelatorio'] = $configuracao->getSt_camporelatorio();
                        }
                    }
                    if ($configuracao->getBl_exibido()) {
                        //$arCampos['exibicao'][$configuracao->getId_camporelatorio()] = $campoTo->encapsularDadosInstanciaTO($configuracao);
                        $arCampos['exibicao'][$configuracao->getId_camporelatorio()] = array(
                            'label' => $configuracao->getSt_titulocampo(),
                            'name' => $configuracao->getSt_camporelatorio(),
                            'visible' => true,
                            'cell' => 'string',
                        );
                    }
                }
                $arCampos['filtros'] = array_values($arCampos['filtros']);
                $arCampos['exibicao'] = array_values($arCampos['exibicao']);
            }
            return $arCampos;
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     *
     */
    private function getColumnsFromQueryRelatorio($prefix, array $arrColumns)
    {
        if ($arrColumns) {
            foreach ($arrColumns as $row) {
                $arrReturn[] = $prefix . '.' . $row['name'];
            }

            $strReturn = implode(", ", $arrReturn);
            return $strReturn;
        }
    }

    /**
     * Retorna a quantidade total dos elementos considerando o distinct.
     * @param $sql
     * @return array
     * @throws Exception
     */
    private function getTotalRegistros($sql)
    {
        try {
            $sqlArr = explode("FROM", $sql);
            if (is_array($sqlArr)) {
                $sqlColunasArr = explode(",", $sqlArr[0]);
                if (is_array($sqlColunasArr)) {
                    $coluna = str_replace("SELECT DISTINCT ", "", $sqlColunasArr[0]);
                    $sqlFinal = "SELECT DISTINCT COUNT({$coluna}) FROM " . $sqlArr[1];
                    $query = $this->dao->db()->fetchOne($sqlFinal);
                    return [
                        "total" => $query,
                        "rows" => $query
                    ];
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Método para retorno dos dados
     * @param array object $relatorio
     * @param array $campoRelatorio
     * @param string $whereSessao
     * @param string $ordenacao
     * @param boolean $isXls
     * @return array
     */
    public function switchFonteDadosRelatorio($relatorio, array $arrColumns, $whereSessao = null, $ordenacao = null, $isXls = false, array $paginacao)
    {
        try {
            $ordenacao = (empty($ordenacao) ? $arrColumns[0]['name'] : $ordenacao);
            switch ($relatorio[0]->getId_tipoorigemrelatorio()->getId_tipoorigemrelatorio()) {
                case 1: // view
                    $configs['name'] = $relatorio[0]->getSt_origem();
                    $configs['schema'] = 'rel';
//                    $configs['colums'] = '*'; //$this->getColumnsFromQueryRelatorio($configs['name'], $arrColumns);
                    $configs['colums'] = $this->getColumnsFromQueryRelatorio($configs['name'], $arrColumns);
                    $whereSessao = (empty($whereSessao) ? NULL : 'AND ' . $whereSessao);
                    $sql = "SELECT DISTINCT {$configs['colums']} FROM {$configs['schema']}.{$configs['name']} WHERE 1=1 {$whereSessao}";
                    break;
                case 2: // query
                    $sql = $this->trataVariaveisSessao($relatorio[0]->getSt_origem());
                    $sql = str_replace('#where#', (empty($whereSessao) ? '' : ' AND ' . $whereSessao), $sql);
                    break;
            }

            $count = $this->getTotalRegistros($sql);

            $limit = (int)$paginacao['qtdRegistrosPorPagina'];
            $offset = (int)($paginacao['pagina'] - 1) * $limit;
            if ($limit) {
                $sql .= ' ORDER BY ' . $ordenacao . ' OFFSET ' . $offset . ' ROWS FETCH NEXT ' . $limit . ' ROWS ONLY';
            }

            $query = $this->em->getConnection()->prepare($sql);

            $query->execute();
            $consulta = $query->fetchAll();

            foreach ($consulta as $index => $dado) {
                foreach ($dado as $chave => $valor) {
                    //converte data
                    if (substr($chave, 0, 3) == 'dt_') {
                        $consulta[$index][$chave] = $this->formatDateToReturn($valor);
                        if ($chave == 'dt_registrolog') {
                            $consulta[$index][$chave] = $this->formatDateToReturn($valor, true);
                        }
                    }

                    // Converter alguns campos para maiúsculo
                    if (in_array($chave, array('st_nomecompleto', 'st_evolucao', 'st_projetopedagogico'))) {
                        $consulta[$index][$chave] = mb_strtoupper($valor, 'UTF-8');
                    }

                    //converte numero
                    if (substr($chave, 0, 3) == 'nu_' && !$isXls) {

                        switch ($chave) {
                            case 'nu_tentativabraspag':
                                $consulta[$index][$chave] = $valor;
                                break;

                            case 'nu_parcela':
                                $consulta[$index][$chave] = $valor;
                                break;

                            case 'nu_telefone':
                                $consulta[$index][$chave] = $valor;
                                break;

                            default:

                                $consulta[$index][$chave] = $this->toFloat($valor);
                                if ($consulta[$index][$chave] == ",00") {
                                    $consulta[$index][$chave] = "0,00";
                                }

                                break;
                        }
                    }
                }
            }


            if ($consulta) {
                $dados['dados'] = $consulta;
            } else {
                $dados['dados'] = false;
            }

            $dados['qtdRegistros'] = (int)$count['total'];
            return $dados;
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao tentar consultar dados do relatório. " . $e->getMessage());
        }
    }

    /**
     * Format Date to return
     * @param Zend_Date $date
     * @return string
     */
    private function formatDateToReturn($valor, $showTime = false)
    {
        if ($valor != null) {
            if ($showTime) {
                $valor = (is_object($valor) ? $valor->format("d-m-Y H:i:s") : $valor);
                $date = new Zend_Date($valor);
                $valor = $date->toString('dd/MM/yyyy H:m:s');
            } else {
                $valor = (is_object($valor) ? $valor->format("d-m-Y") : $valor);
                $date = new Zend_Date($valor);
                $valor = $date->toString('dd/MM/yyyy');
            }
        } else {
            $valor = NULL;
        }
        return $valor;
    }

    /**
     * Trata o array com os campos do relatorio e retorna um array com os filtros e a grid
     * @param array $grid
     * @return array
     */
    private function trataArrayCamposRelatorio(array $arrCampos)
    {
        $arrayReturn = array();
        if ($arrCampos) {
            foreach ($arrCampos as $row) {
                if ($row['bl_filtro']) {
                    $arrayReturn['filtros'][] = $row;
                }
                if ($row['bl_exibido']) {
                    $arrayReturn['grid'][] = array(
                        'label' => $row['st_titulocampo'],
                        'name' => $row['st_camporelatorio'],
                        'cell' => 'string',
                        'editable' => false
                    );
                }
            }
        }
        return $arrayReturn;
    }

    /**
     * Método que busca os dados de acordo com a configuração dos filtros pelo usuário
     * @param int $idRelatorio id do relatório em questão
     * @param mixed $arFiltros array com os filtros setados pelo usuário
     * @param int $pagina número da página a ser exibida9i
     * @param int $qtdRegistros quantidade de registros por página
     * @param boolean $soDados enviar só os dados ou tudo do relatório
     * @param int $idFuncionalidade id da funcionalidade do relatório em questão
     * @return Ead1_Mensageiro
     * @param null $ordenacao
     * @param bool $isXls
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornarDadosRelatorio($idRelatorio = null, $arFiltros = null, $pagina = 1, $qtdRegistros = 30, $soDados = false, $idFuncionalidade = null, $ordenacao = null, $isXls = false)
    {


        try {
            $pagina = ($pagina ? $pagina : 1);
            $dadosPaginacao['pagina'] = $pagina;
            $dadosPaginacao['qtdRegistrosPorPagina'] = ($qtdRegistros ? $qtdRegistros : 30);

            if ($idFuncionalidade) {
                $params['id_funcionalidade'] = $idFuncionalidade;
            } else {
                $params['id_relatorio'] = $idRelatorio;
            }
            $arrRelatorio = array();
            $relatorio = $this->em->getRepository('G2\Entity\Relatorio')->findBy($params);


            if ($relatorio) {
                $dados['relatorio'] = $relatorio[0];
                $arrRelatorio['paginacao'] = $dadosPaginacao;
                $campoExibicao = $this->retornaCamposRelatorios(array(
                    'id_relatorio' => $relatorio[0]->getId_relatorio(),
                ));
                $arrCampos = $this->trataArrayCamposRelatorio($campoExibicao);


                if ($soDados) {

                    $whereSessao = $this->trataFiltroWhereSessao($arrCampos['filtros']);
                    $this->trataFiltrosPesquisa($arrCampos['filtros'], $arFiltros, $whereSessao);
                    $dadosConsulta = $this->switchFonteDadosRelatorio($relatorio, $arrCampos['grid'], $whereSessao, $ordenacao, $isXls, $dadosPaginacao);
                    $dadosPaginacao['qtdRegistrosTotal'] = $dadosConsulta['qtdRegistros'];
                    $dados['grid'] = $arrCampos['grid'];
                    $dados['filtros'] = $arrCampos['filtros'];
                    $dados['paginacao'] = $dadosPaginacao;
                    $dados['dados'] = $dadosConsulta['dados'];
                    $arrRelatorio = $dados;
                }
                return $arrRelatorio;
            } else {
                return $arrRelatorio;
            }
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao consultar dados. " . $e->getMessage());
        }
    }

    /**
     * Find campos relatorio on view_dadosrelatorio
     * @param array $params
     * @return mixed
     * @throws Zend_Exception
     */
    public function findCamposRelatorio(array $params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwDadosRelatorio');
            $query = $repo->createQueryBuilder("vw")
                ->where('1=1');
            //set params
            if ($params) {
                foreach ($params as $key => $value) {
                    if ($value) {
                        $query->andWhere("vw.{$key} = :{$key}")
                            ->setParameter($key, $value);
                    }
                }
            }
            //order
            $query->orderBy('vw.nu_ordem', 'ASC');
            return $query->getQuery()->getResult();
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * Retorna campos dos relatorio
     * @param array $params
     * @return mixed/array
     */
    public function retornaCamposRelatorios(array $params)
    {
        $result = $this->findCamposRelatorio($params);

        $arCampos = array();
        if ($result) {
            foreach ($result as $row) {
                $arCampos[$row->getId_camporelatorio()]['id_camporelatorio'] = $row->getId_camporelatorio();
                $arCampos[$row->getId_camporelatorio()]['st_camporelatorio'] = $row->getSt_camporelatorio();
                $arCampos[$row->getId_camporelatorio()]['st_titulocampo'] = $row->getSt_titulocampo();
                $arCampos[$row->getId_camporelatorio()]['bl_filtro'] = $row->getBl_filtro();
                $arCampos[$row->getId_camporelatorio()]['bl_exibido'] = $row->getBl_exibido();
                $arCampos[$row->getId_camporelatorio()]['bl_obrigatorio'] = $row->getBl_obrigatorio();
                $arCampos[$row->getId_camporelatorio()]['id_operacaorelatorio'] = $row->getId_operacaorelatorio();
                $arCampos[$row->getId_camporelatorio()]['id_tipocampo'] = $row->getId_tipocampo();
                $arCampos[$row->getId_camporelatorio()]['id_interface'] = $row->getId_interface();
                if ($row->getId_tipopropriedadecamporel() == 7) {
                    $arCampos[$row->getId_camporelatorio()]['st_campopai'] = $row->getSt_valor();
                } else {
                    $arCampos[$row->getId_camporelatorio()]['id_tipopropriedadecamporel'] = $row->getId_tipopropriedadecamporel();
                    $arCampos[$row->getId_camporelatorio()]['st_tipopropriedadecamporel'] = $row->getSt_tipopropriedadecamporel();
                    $arCampos[$row->getId_camporelatorio()]['st_valor'] = $row->getSt_valor();
                }
                if (isset($arCampos[$row->getId_camporelatorio()])) {
                    $arCampos[$row->getId_camporelatorio()] = $this->trataFiltros($row, $arCampos[$row->getId_camporelatorio()]);
                } else {
                    $arCampos[$row->getId_camporelatorio()] = $this->trataFiltros($row);
                    $arCampos[$row->getId_camporelatorio()]['id_camporelatorio'] = $row->getId_camporelatorio();
                    $arCampos[$row->getId_camporelatorio()]['st_camporelatorio'] = $row->getSt_camporelatorio();
                }
            }
            return $arCampos;
        }
    }

    /**
     * Percorre o filtro e trata o where
     * @param array $filtros
     * @return string
     * @throws Zend_Exception
     */
    private function trataFiltroWhereSessao(array $filtros)
    {

        $whereSessao = null;

        if (is_array($filtros)) {

            $whereSessaoAr = array();
            foreach ($filtros as $filtro) {

                if (isset($filtro['propriedades']['default'])) {
                    if (substr($filtro['propriedades']['default'], 0, 1) == "$") {
                        $filtroSessao = substr($filtro['propriedades']['default'], 1, strlen($filtro['propriedades']['default']));
                        if (!isset($this->sessao->id_entidade)) {
                            throw new Zend_Exception('Você não possui sessão!');
                        } else {
                            $campoRelatorio = substr($filtro['st_camporelatorio'], 0, strlen($filtro['st_camporelatorio']));
                            $whereSessaoAr[] = "{$campoRelatorio} = " . $this->sessao->$filtroSessao;
                        }
                    }
                }
            }

            $whereSessao = implode(' AND ', $whereSessaoAr);
        }


        return $whereSessao;
    }

    /**
     * Percorre o campo st_origem em busca de variaveis de sessão
     * @param array $st_origem
     * @return string
     *
     */

    public function trataVariaveisSessao($st_origem)
    {

        $sessao = new Zend_Session_Namespace('geral');

        if (empty($st_origem)) {
            return false;
        }

        $buscar_variaveis = preg_match_all('/(\$\w+)/', $st_origem, $retorno);

        if ($buscar_variaveis) {
            $array_variaveis = [];

            foreach ($retorno[1] as $variavel_sessao) {
                if (!in_array($variavel_sessao, $array_variaveis)) {
                    array_push($array_variaveis, $variavel_sessao);
                }
            }
        }
        $novo_st_origem = $st_origem;

        if ($array_variaveis) {

            foreach ($array_variaveis as $ar) {

                $variavel = str_replace('$', '', $ar);

                if (isset($sessao->$variavel)) {

                    $novo_st_origem = preg_replace("/\\" . $ar . "\b/", $sessao->$variavel, $novo_st_origem);
                } else {
                    $novo_st_origem = preg_replace("/\\" . $ar . "\b/", '\'\'', $novo_st_origem);
                }
            }
        }
        return $novo_st_origem;
    }

    /**
     * Método que trata os dados de consulta com base no array de filtros
     * @param array $filtros
     * @param array $arFiltros
     * @param string $whereSessao
     */
    private function trataFiltrosPesquisa($filtros, $arFiltros = null, &$whereSessao = null)
    {
        $arPropriedade = array();
        foreach ($filtros as $dadosFiltros) {
            if (isset($dadosFiltros['propriedades']['tipocampo']))
                $arPropriedade[$dadosFiltros['st_camporelatorio']]['tipo'] = $dadosFiltros['propriedades']['tipocampo'];
        }
        if (is_array($arFiltros)) {
            $arFiltros = array_merge_recursive($arPropriedade, $arFiltros);

            foreach ($arFiltros as $campo => $dados) {
                if (isset($dados['valor1']) && substr($dados['valor1'], 0, 1) == "$") {
                    continue;
                }
                $valores = count($dados) - 2;
                for ($i = 1; $i <= $valores; $i++) {
                    switch ($dados['tipo']) {
                        case 'string':
                            $aspas = true;

                            // Remover mascaras CPF e CNPJ
                            if (substr($campo, 0, 6) == 'st_cpf' || substr($campo, 0, 7) == 'st_cnpj') {
                                $dados['valor' . $i] = preg_replace("/\D/", "", $dados['valor' . $i]);
                            }

                            if ($dados['valor' . $i] == 'null') {
                                $dados['valor' . $i] = null;
                                continue;
                            }
                            break;
                        case 'date':
                            $aspas = true;
                            if ($dados['valor' . $i] == 'null') {
                                $dados['valor' . $i] = null;
                                continue;
                            }
                            $dados['valor' . $i] = $this->converteDataBanco($dados['valor' . $i]);
                            break;
                        case 'numeric':
                            if ($dados['valor' . $i] == 'null') {
                                $dados['valor' . $i] = null;
                                continue;
                            }
                            $dados['valor' . $i] = ($dados['valor' . $i] = 0 ? null : $dados['valor' . $i]);
                            $aspas = false;
                            break;
                        default:
                            $aspas = false;
                            break;
                    }
                    if (is_array($dados['valor' . $i])) {
                        $dados['valor' . $i] = $dados['valor' . $i]['id'];
                    }
                    $dados['valor' . $i] = ($aspas && isset($dados['valor' . $i]) && strtolower($dados['operador']) !== 'like' ? "'{$dados['valor' . $i]}'" : $dados['valor' . $i]);
                    if ($dados['valor' . $i] == "''") {
                        $dados['valor' . $i] = null;
                    }
                }
                if (isset($dados['operador'])) {
                    switch (strtolower($dados['operador'])) {
                        case 'like':
                            if (!empty($dados['valor1'])) {
                                $where[] = " $campo like '%{$dados['valor1']}%'";
                            }
                            break;
                        case 'igual':
                            if (!empty($dados['valor1'])) {
                                $where[] = " $campo = {$dados['valor1']}";
                            }
                            break;
                        case 'entre':
                            //verifica se o tipo é data, verifica se o valor1 é igual ao valor2 e escreve hora inicial e final
                            if ($dados['tipo'] == 'date') {
                                if ((!empty($dados['valor1']) && !empty($dados['valor2']))) {
//                                if ((!empty($dados['valor1']) && !empty($dados['valor2'])) && ($dados['valor1'] == $dados['valor2'])) {
                                    $dados['valor1'] = "'" . str_replace("'", "", $dados['valor1']) . " 00:00:00'";
                                    $dados['valor2'] = "'" . str_replace("'", "", $dados['valor2']) . " 23:59:59'";
                                }
                            }

                            if (isset($dados['valor1']) && !empty($dados['valor1'])) {
                                $where[] = " $campo >= {$dados['valor1']}";
                            }
                            if (isset($dados['valor2']) && !empty($dados['valor2'])) {
                                $where[] = " $campo <= {$dados['valor2']}";
                            }
                            break;
                    }
                }
            }
            if (!empty($where) && is_array($where)) {
                $whereFiltro = implode(" AND ", $where);
            } else {
                $whereFiltro = "";
            }
            if (empty($whereSessao)) {
                $whereSessao = $whereFiltro;
            } else {
                $whereSessao .= (empty($whereFiltro) ? '' : ' AND ' . $whereFiltro);
            }
        }
    }

    /**
     *
     * @param array $configuracao
     * @param array $arFiltro
     * @return array
     */
    public function trataFiltros($configuracao, $arFiltro = array())
    {
        if (!isset($arFiltro['propriedades'])) {
            $arFiltro['propriedades'] = array();
        }
        $arFiltro['propriedades'] = $this->trataPropriedadesFiltro($configuracao, $arFiltro['propriedades']);

        return $arFiltro;
    }

    /**
     * Retorna query a partir de um string
     * @param string $sql
     * @return mixed
     */
    private function returnQuerySubstituiStConsulta($sql)
    {
        try {
            $query = $this->em->getConnection()->prepare($sql);
            $query->execute();
            return $query;
        } catch (Zend_Exception $e) {
            return $e;
        }
    }

    /**
     *
     * @param object $propriedadeCampoRelatorio
     * @param array $arPropriedade
     * @return array
     */
    public function trataPropriedadesFiltro($propriedadeCampoRelatorio, $arPropriedade)
    {
        switch ($propriedadeCampoRelatorio->getId_tipopropriedadecamporel()) {
            case 1: //id_operacaorelatorio
                //Ead1_ORM::instance(new OperacaoRelatorioORM())->find((int) $propriedadeCampoRelatorio->getSt_valor())->current()->st_operacaorelatorio;
                $arPropriedade['operacaorelatorio'] = $this->getOperacaoRelatorio($propriedadeCampoRelatorio->getId_operacaorelatorio());
                $arPropriedade['operacaorelatorioid'] = $propriedadeCampoRelatorio->getSt_valor();
                break;
            case 2: //id_interface
                $arPropriedade['interface_flex'] = $propriedadeCampoRelatorio->getSt_valor();
                break;
            case 3: //id_tipocampo
                //Ead1_ORM::instance(new TipoCampoORM())->find((int) $propriedadeCampoRelatorio->getSt_valor())->current()->st_tipocampo;
                $arPropriedade['tipocampo'] = $this->getTipoCampo($propriedadeCampoRelatorio->getId_tipocampo());
                $arPropriedade['tipocampoid'] = $propriedadeCampoRelatorio->getSt_valor();
                break;
            case 4: //st_default
                $arPropriedade['default'] = $propriedadeCampoRelatorio->getSt_valor();
                break;
            case 5: //st_consulta
                $stConsulta = $this->substituiSt_consulta($propriedadeCampoRelatorio->getSt_valor());
                if (strripos($stConsulta, 'order by') === false) {
                    $stConsulta = $stConsulta . ' order by value';
                }
                $verifStr = explode(' ', strtolower($stConsulta));
                $dados = array();
                if (in_array('select', $verifStr)) {
                    $dadosAr = $this->returnQuerySubstituiStConsulta($stConsulta)->fetchAll();
                    foreach ($dadosAr as $dado) {
                        $dado = array_values($dado);
                        $dados[$dado[0]]['id'] = $dado[0];
                        $dados[$dado[0]]['label'] = $dado[1];
                    }
                }
                $arPropriedade['consulta'] = array_values($dados);
                break;
            case 6: //Descrição
                $arPropriedade['tooltip'] = $propriedadeCampoRelatorio->getSt_valor();
                break;
        }
        foreach ($arPropriedade as $propriedade => $valor) {
            switch ($propriedade) {
                case 'operacaorelatorioid':
                    $operacaorelatorio = $valor;
                    break;
                case 'tipocampoid':
                    $tipocampo = $valor;
                    break;
                case 'interface_flex':
                    $interface = $valor;
                    break;
            }
            if (isset($operacaorelatorio) && isset($tipocampo) && isset($interface)) {
                $sql = "SELECT * FROM tb_tipocampooperacao WHERE id_operacaorelatorio = $operacaorelatorio AND id_tipocampo = $tipocampo AND id_interface = $interface";
                $dado = $this->returnQuerySubstituiStConsulta($sql)->fetch();
                if ($dado) {
                    $arPropriedade['interface_flex'] = $dado['st_classeflex'];
                } else {
                    unset($arPropriedade['interface_flex']);
                }
                unset($arPropriedade['operacaorelatorioid']);
                unset($arPropriedade['tipocampoid']);
            }
        }
        return $arPropriedade;
    }

    /**
     *
     * @param string $consulta
     * @return string
     */
    public function substituiSt_consulta($consulta)
    {
        $posicaoInicialVar = strpos($consulta, '$');
        if ($posicaoInicialVar !== false) {
            $stringAux = substr($consulta, $posicaoInicialVar);
            $posicaoFinalVar = strpos($stringAux, ' ');
            if ($posicaoFinalVar === false) {
                $var = substr($stringAux, 1);
            } else {
                $var = substr($stringAux, 1, $posicaoFinalVar - 1);
            }
            $consulta = str_replace('$' . $var, $this->sessao->$var, $consulta);
            $consulta = $this->substituiSt_consulta($consulta);
        }
        return $consulta;
    }

    /**
     *
     * @param array $array
     * @param boolean $xls
     * @return array
     */
    public function trataArrayExibicao($array, $xls = false)
    {
        $arrayResult = array();
        foreach ($array as $index => $campoString) {
            $stringAux = strrchr($campoString, ',');
            $lengthStringAux = strlen($stringAux);
            $label = substr($stringAux, 1);
            $field = substr($campoString, 0, 0 - $lengthStringAux);
            if ($xls) {
                $arrayResult[$index]['st_header'] = $label;
                $arrayResult[$index]['st_par'] = $field;
            } else {
                $arrayResult[$index]['field'] = $field;
                $arrayResult[$index]['label'] = $label;
            }
        }
        return $arrayResult;
    }

    /**
     *
     * @param array $filtros
     * @param array $filtrosSetados
     * @return string
     */
    public function trataArrayFiltroParaExibirDados($filtros, $filtrosSetados)
    {
        $arrayFinalFiltro = array();
        foreach ($filtrosSetados as $key => $filtroSetado) {
            foreach ($filtros as $filtro) {
                if ($filtro['st_camporelatorio'] == $key && isset($filtro['propriedades'])) {
                    $label = null;
                    if (substr($key, 0, 2) == 'dt') {
                        if ((!empty($filtroSetado['valor1']) && $filtroSetado['valor1'] != 'null') || (!empty($filtroSetado['valor2']) && $filtroSetado['valor2'] != 'null')) {

                            $label = $filtro['st_titulocampo'] . ': ';
                            if (!empty($filtroSetado['valor1']) && $filtroSetado['valor1'] != 'null') {
                                $dateAux = new Zend_Date($filtroSetado['valor1']);
                                $label .= $dateAux->toString('dd/MM/YYYY');
                            }
                            if (!empty($filtroSetado['valor2']) && $filtroSetado['valor2'] != 'null') {
                                $dateAux = new Zend_Date($filtroSetado['valor2']);
                                $label .= ' até ' . $dateAux->toString('dd/MM/YYYY');
//                        } else {
//                            $dateAux = new Zend_Date();
//                            $label .= ' até ' . $dateAux->toString('dd/MM/YYYY');
                            }
                            $arrayFinalFiltro[] = $label;
                        }
                    } elseif (substr($key, 0, 2) == 'nu') {
                        $label = $filtro['st_titulocampo'] . ': ';
                        $label .= $filtroSetado['valor1'];
                        $arrayFinalFiltro[] = $label;
                    } else {
                        if (isset($filtro['propriedades']['consulta'])) {
                            foreach ($filtro['propriedades']['consulta'] as $valor) {
                                if (!empty($filtroSetado['valor1']) && $filtroSetado['valor1'] == $valor['id']) {
                                    $label = $filtro['st_titulocampo'] . ': ';
                                    $label .= $valor['label'];
                                }
                            }
//                        } else {
//                            $label = $filtro['st_titulocampo'] . ': ';
//                            $label .= $filtroSetado['valor1'];
//                            echo $label;
                        }
                        $arrayFinalFiltro[] = $label;
                    }
                }
            }
        }
//        Zend_Debug::dump($arrayFinalFiltro);
//        exit;
        return $arrayFinalFiltro;
    }

    /**
     * Retorna dados referente a tb_interface
     * @param integer $interface
     * @return string
     */
    private function getInterface($interface)
    {
        $st_interface = null;
        switch ($interface) {
            case 1:
                $st_interface = 'TextInput';
                break;
            case 2:
                $st_interface = 'DropDownList';
                break;
            case 3:
                $st_interface = 'DateField';
                break;
            case 4:
                $st_interface = 'Label';
                break;
        }
        return $st_interface;
    }

    /**
     * Retorna dados referentes a tb_operacaorelatorio
     * @param interger $idOperacaoRelatorio
     * @return string
     */
    private function getOperacaoRelatorio($idOperacaoRelatorio)
    {
        $st_operacaorelatorio = null;
        switch ($idOperacaoRelatorio) {
            case 1:
                $st_operacaorelatorio = 'Igual';
                break;
            case 2:
                $st_operacaorelatorio = 'Maior';
                break;
            case 3:
                $st_operacaorelatorio = 'Menor';
                break;
            case 4:
                $st_operacaorelatorio = 'Entre';
                break;
            case 5:
                $st_operacaorelatorio = 'Like';
                break;
        }
        return $st_operacaorelatorio;
    }

    /**
     * Retorna dados da tb_tipocampo
     * @param integer $idTipoCampo
     * @return string
     */
    private function getTipoCampo($idTipoCampo)
    {
        $st_tipocampo = null;
        switch ($idTipoCampo) {
            case 1:
                $st_tipocampo = 'string';
                break;
            case 2:
                $st_tipocampo = 'datetime';
                break;
            case 3:
                $st_tipocampo = 'date';
                break;
            case 4:
                $st_tipocampo = 'numeric';
                break;
        }
        return $st_tipocampo;
    }

    /**
     * Monta a query que busca os dados de um campo, baseado no valor do campo "pai"
     * @param string $idCampoRelatorio
     * @param string $valueParentField
     * @return array
     * @throws Exception
     */
    public function getDadosSelectDinamico($idCampoRelatorio, $valueParentField)
    {
        try {
            //instancia a TO e seta o valor do campo do relatorio
            $pCcampo = new PropriedadeCampoRelatorioTO();
            $pCcampo->setId_camporelatorio($idCampoRelatorio);
            //Busca dados do campo do relatorio
            $return = $this->retornarPropriedadeCampoRelatorio($pCcampo);
            //cria as variaveis para auxiliar na consulta sql
            $strSql = null;
            $where = ' WHERE 1=1 ';
            $returnQuery = array();
            //verifica se encontrou os dados do campo do relatorio
            if ($return->getType() == 'success') {
                //percorre o mensageiro
                foreach ($return->getMensagem() as $row) {
                    //tipo propriedade campo relatorio
                    switch ($row->getId_tipopropriedadecamporel()) {
                        case 5: // st_consulta
                            $strSql = $row->getSt_valor();
                            break;
                        case 7://st_campopai
                            $where .= 'AND ' . $row->getSt_valor() . '=' . $valueParentField;
                            break;
                    }
                }

                $stConsulta = $strSql . $where;
                if (strripos($strSql, 'order by') === false) {
                    $stConsulta = $strSql . $where . ' order by value';
                }

                $sql = $this->substituiSt_consulta($stConsulta);
                $returnQuery = $this->returnQuerySubstituiStConsulta($sql)->fetchAll();
            }
            if ($returnQuery)
                return $returnQuery;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

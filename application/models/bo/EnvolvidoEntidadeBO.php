<?php
/**
 * Classe com regras de negócio para interações de Envolvido de Entidade
 * @author Eduardo Romão - ejushiro@gmail.com
 * @since 18/10/2011
 * 
 * @package models
 * @subpackage bo
 */
class EnvolvidoEntidadeBO extends Ead1_BO {

	public $mensageiro;
	
	/**
	 * @var EnvolvidoEntidadeDAO
	 */
	private $dao;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new EnvolvidoEntidadeDAO();
	}
	
	/**
	 * Metodo cadastra envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			if(!$eeTO->getId_entidade()){
				$eeTO->setId_entidade($eeTO->getSessao()->id_entidade);
			}
			$id_envolvidoentidade = $this->dao->cadastrarEnvolvidoEntidade($eeTO);
			if(!$id_envolvidoentidade){
				throw new Zend_Exception('Erro ao fazer insert');
			}
			$eeTO->setId_envolvidoentidade($id_envolvidoentidade);
			return $this->mensageiro->setMensageiro($eeTO,Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Representante!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo edita envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			if(!$this->dao->editarEnvolvidoEntidade($eeTO)){
				throw new Zend_Exception('Erro ao fazer update');
			}
			return $this->mensageiro->setMensageiro('Representante editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Editar Representante!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo exclui envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			if(!$this->dao->excluirEnvolvidoEntidade($eeTO)){
				throw new Zend_Exception('Erro ao fazer delete');
			}
			return $this->mensageiro->setMensageiro("Representante Deletado com Sucesso!", Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Representante!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar envolvido de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		try{
			if(!$eeTO->getId_entidade()) {
				$eeTO->id_entidade	= $eeTO->getSessao()->id_entidade;
			}
			
			$dados = $this->dao->retornarEnvolvidoEntidade($eeTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Representante Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new EnvolvidoEntidadeTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Representante(s)!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo retornar envolvido de entidade com nome da pessoa e tipo de envolvido
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEnvolvidoEntidadePessoa(EnvolvidoEntidadeTO $eeTO){
		try{
			if(!$eeTO->getId_entidade()) {
				$eeTO->id_entidade	= $eeTO->getSessao()->id_entidade;
			}
			
			$dados = $this->dao->retornarEnvolvidoEntidadePessoa($eeTO)->toArray();
			if(empty($dados)){
				return $this->mensageiro->setMensageiro('Nenhum Registro de Representante Encontrado!',Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro($dados, Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Representante(s)!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a view de envolvidos da entidade
	 * @param VwEntidadeEnvolvidoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntidadeEnvolvido(VwEntidadeEnvolvidoTO $to){
		try{
			$to->setId_entidade(($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade));
			$dados = $this->dao->retornarVwEntidadeEnvolvido($to)->toArray();
			if(empty($dados)){
				$this->mensageiro->setMensageiro('Nenhum Registro Encontrado',Ead1_IMensageiro::AVISO);
			}else{
				$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new VwEntidadeEnvolvidoTO()),Ead1_IMensageiro::SUCESSO);
			}
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar os Envolvidos da Instituição!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
	
	
}

?>
<?php

/**
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage bo
 *
 */
class MoodleBO extends Ead1_BO
{

    public function __construct()
    {
        $this->mensageiro = new Ead1_Mensageiro();
        $this->_dao = new MoodleDAO();
    }

    /**
     * @param null $id_entidadeintegracao
     * @return Ead1_Mensageiro
     */
    public function sincronizarMoodleIntegracao($id_entidadeintegracao = null)
    {
        $mensagem['salas'] = $this->sincronizacaoMoodle($id_entidadeintegracao);
        if ($mensagem['salas']->getTipo() == Ead1_IMensageiro::ERRO) {
            return $mensagem['salas'];
        }
        $mensagem['salas'] = $mensagem['salas']->getMensagem();

        //sincroniza os perfils
        $mensagem['perfis'] = $this->sincronizacaoMoodlePerfil($id_entidadeintegracao);
        if ($mensagem['perfis']->getTipo() == Ead1_IMensageiro::ERRO) {
            return $mensagem['perfis'];
        }
        $mensagem['perfis'] = $mensagem['perfis']->getMensagem();
        return $this->mensageiro->setMensageiro($mensagem);
    }

    /**
     * @param null $id_entidadeintegracao
     * @return Ead1_Mensageiro
     */
    public function sincronizacaoMoodle($id_entidadeintegracao = null)
    {
        try {
            $erros = array();
            $sucessos = array();
            $dados = $this->_dao->retornarVwSincronizarMoodle($id_entidadeintegracao);

            if (!empty($dados)) {
                $vwSincronizarTO = new VwSincronizarMoodleTO();
                $vwSincronizarTO = Ead1_TO_Dinamico::encapsularTo($dados, $vwSincronizarTO);

                foreach ($vwSincronizarTO as $key => $to) {
                    $mensagem = $this->sincronizar($vwSincronizarTO[$key]);
                    if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $sucessos[$key]['nome'] = $vwSincronizarTO[$key]->getSt_nomecompleto();
                        $sucessos[$key]['sala'] = $vwSincronizarTO[$key]->getSt_saladeaula();
                    } else {
                        $erros[$key]['nome'] = $vwSincronizarTO[$key]->getSt_nomecompleto();
                        $erros[$key]['sala'] = $vwSincronizarTO[$key]->getSt_saladeaula();
                        $erros[$key]['erro'] = $mensagem->getFirstMensagem();
                    }
                }
            }
            $arrMensagem['erros'] = $erros;
            $arrMensagem['sucessos'] = $sucessos;
            $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao sincronizar perfil no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @param null $id_entidadeintegracao
     * @return Ead1_Mensageiro
     */
    public function sincronizacaoMoodlePerfil($id_entidadeintegracao = null)
    {
        try {
            $erros = array();
            $sucessos = array();

            $dados = $this->_dao->retornarVwSincronizarCategoriaMoodle($id_entidadeintegracao);

            if (!empty($dados)) {
                $vwSincronizarTO = new VwSincronizarCategoriaMoodleTO();
                $vwSincronizarTO = Ead1_TO_Dinamico::encapsularTo($dados, $vwSincronizarTO);

                foreach ($vwSincronizarTO as $key => $to) {

                    $mensagem = $this->sincronizarPerfil($to);
                    if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $sucessos[$key]['nome'] = $vwSincronizarTO[$key]->getSt_nomecompleto();
                    } else {
                        $erros[$key]['nome'] = $vwSincronizarTO[$key]->getSt_nomecompleto();
                        $erros[$key]['erro'] = $mensagem->getFirstMensagem();
                    }
                }
            }
            $arrMensagem['erros'] = $erros;
            $arrMensagem['sucessos'] = $sucessos;
            $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao sincronizar perfil no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @param VwSincronizarMoodleTO $vwSincronizarTO
     * @return Ead1_Mensageiro
     */
    public function sincronizar(VwSincronizarMoodleTO $vwSincronizarTO)
    {
        try {
            if (!$vwSincronizarTO->getId_usuario() || !$vwSincronizarTO->getId_saladeaula() || !$vwSincronizarTO->getId_entidade() || !$vwSincronizarTO->getNu_codigoperfil()) {
                throw new Zend_Exception('O id_usuario e/ou id_saladeaula e/ou id_entidade e/ou nu_codigoperfil são obrigatórios e não foram passados!');
            }

            $moodle = new MoodleAlocacaoWebServices($vwSincronizarTO->getId_entidade(), $vwSincronizarTO->getId_entidadeintegracao());

            $mensagem = $moodle->cadastrarPapel(
                $vwSincronizarTO->getId_usuario(),
                $vwSincronizarTO->getId_saladeaula(),
                $vwSincronizarTO->getNu_codigoperfil(),
                $vwSincronizarTO->getId_entidade(),
                false,
                'cadastrarAlocacaoIntegracao');

            if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $toPRI = new PerfilReferenciaIntegracaoTO();
                $toPRI->setId_sistema(SistemaTO::MOODLE);
                $toPRI->setId_perfilreferencia($vwSincronizarTO->getId_perfilreferencia());
                $toPRI->setId_usuariocadastro($vwSincronizarTO->getId_usuario());
                $toPRI->setId_saladeaulaintegracao($vwSincronizarTO->getId_saladeaulaintegracao());
                $toPRI->setSt_codsistema($vwSincronizarTO->getNu_codigoperfil());
                $id = $this->_dao->salvarPerfilReferencia($toPRI);
            } else {
                throw new Zend_Exception($mensagem->getFirstMensagem());
            }
            $this->mensageiro->setMensageiro($vwSincronizarTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @param VwSincronizarCategoriaMoodleTO $vwSincronizarTO
     * @return Ead1_Mensageiro
     */
    public function sincronizarPerfil(VwSincronizarCategoriaMoodleTO $vwSincronizarTO)
    {
        try {
            $moodle = new MoodleAlocacaoWebServices(null, $vwSincronizarTO->getId_entidadeintegracao());
            $mensagem = $moodle->vincularPerfil(
                $vwSincronizarTO->getId_usuario(),
                $vwSincronizarTO->getNu_perfil(),
                $vwSincronizarTO->getSt_codarea(),
                $vwSincronizarTO->getId_entidade());

            if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $toPRI = new PerfilReferenciaIntegracaoTO();
                $toPRI->setId_sistema(SistemaTO::MOODLE);
                $toPRI->setId_perfilreferencia($vwSincronizarTO->getId_perfilreferencia());
                $toPRI->setId_usuariocadastro($vwSincronizarTO->getId_usuario());
                $toPRI->setSt_codsistema($vwSincronizarTO->getNu_perfil());
                $id = $this->_dao->salvarPerfilReferencia($toPRI);
            } else {
                throw new Zend_Exception($mensagem->getFirstMensagem());
            }
            $this->mensageiro->setMensageiro($vwSincronizarTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @return Ead1_Mensageiro
     */
    public function sincronizacaoAlunosMoodle()
    {
        $erros = array();
        $sucessos = array();
        try {
            $dados = $this->_dao->retornarVwSincronizarAlunosMoodle();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum aluno para ser sincronizado!");
            } else {
                $arrVwSincronizar = Ead1_TO_Dinamico::encapsularTo($dados, new VwSincronizarAlunosMoodleTO());
                foreach ($arrVwSincronizar as $key => $to) {
                    $moodle = new SalaDeAulaBO();
                    $alocacao = new AlocacaoTO();
                    $alocacao->setId_alocacao($to->getId_alocacao());
                    $alocacao->setId_saladeaula($to->getId_saladeaula());
                    $mensagem = $moodle->salvarIntegracaoAlunoWS($alocacao);
                    if ($mensagem->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $sucessos[$key]['nome'] = $to->getSt_nomecompleto();
                        $sucessos[$key]['sala'] = $to->getSt_saladeaula();
                        $sucessos[$key]['entidade'] = $to->getId_entidade();
                    } else {
                        $erros[$key]['nome'] = $to->getSt_nomecompleto();
                        $erros[$key]['sala'] = $to->getSt_saladeaula();
                        $erros[$key]['erro'] = $mensagem->getFirstMensagem();
                        $erros[$key]['entidade'] = $to->getId_entidade();
                    }
                }
            }
            $arrMensagem['erros'] = $erros;
            $arrMensagem['sucessos'] = $sucessos;
            $this->mensageiro->setMensageiro($arrMensagem, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao sincronizar aluno no moodle! - "
                . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }


    /**
     * Método que decidide se encerra os alunos do moodle de sala periodica ou permanente
     * @param array $alunos
     * @return Ead1_Mensageiro
     */
    public function alterarPapelAluno(array $alunos = null, array $params = [])
    {
        if (!empty($alunos)):
            $retorno = $this->alteraAlunosSalaPermanente($alunos);
        else:
            $retorno = $this->alteraAlunosSalaPeriodica($params);

        endif;


        return $retorno;
    }


    /**
     * Altera perfil de alunos de sala permanente
     * @param array $alunos
     * @return Ead1_Mensageiro
     */
    public function alteraAlunosSalaPermanente(array $alunos)
    {
        try {
            foreach ($alunos as $aluno) {

                $salaDeAulaTO = new SalaDeAulaTO();
                $salaDeAulaTO->setId_saladeaula($aluno->getId_saladeaula());
                $salaDeAulaTO->fetch(false, true, false);
                $id_entidadeintegracao = $salaDeAulaTO->getId_entidadeintegracao();

                $moodle = new MoodleAlocacaoWebServices($aluno->getId_entidade(), $id_entidadeintegracao);

                if ($aluno->getId_sistema() == SistemaTO::MOODLE):
                    $mensagem_ativa_encerrado = $moodle->cadastrarPapel(
                        $aluno->getId_usuario(),
                        $aluno->getId_saladeaula(),
                        $aluno->getNu_perfilalunoencerrado(),
                        $aluno->getId_entidade(),
                        false,
                        'alteraAlunosSalaPermanente');

                    if ($mensagem_ativa_encerrado->getTipo() == Ead1_IMensageiro::SUCESSO) {

                        $alocacaoIntegracaoTo = new AlocacaoIntegracaoTO();
                        $alocacaoIntegracaoTo->setId_alocacaointegracao($aluno->getId_alocacao());
                        $alocacaoIntegracaoTo->getid_entidadeintegracao($id_entidadeintegracao);
                        $alocacaoIntegracaoTo->fetch(false, true, true);

                        if ($alocacaoIntegracaoTo) {
                            $alocacaoIntegracaoTo->setBl_encerrado(true);
                            $this->_dao->updateAlocacaoIntegracao($alocacaoIntegracaoTo);
                        }
                    }

                endif;
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao alterar perfil do aluno no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }


    /**
     * Altera perfil dos alunos de sala periodica
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function alteraAlunosSalaPeriodica(array $params = [])
    {
        try {
            if ($params) {
                $to = new VwAlunosEncerradosMoodleTO();
                $to->montaToDinamico($params);
                $dados = $this->_dao->retornaVwAlunosEncerradosMoodle($to);
            } else {
                $dados = $this->_dao->retornaVwAlunosEncerradosMoodle();
            }

            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhum aluno para ser alterado!");
            } else {
                $vwTO = new VwAlunosEncerradosMoodleTO();
                $vwTO = Ead1_TO_Dinamico::encapsularTo($dados, $vwTO);
                foreach ($vwTO as $key => $to) {
                    $id_entidadeintegracao = $to->getId_entidadeintegracao();

                    //procura o id_entidaeintegracao pelo id da sala de aula
                    if (!$id_entidadeintegracao) {
                        $salaDeAulaTO = new SalaDeAulaTO();
                        $salaDeAulaTO->setId_saladeaula($to->getId_saladeaula());
                        $salaDeAulaTO->fetch(false, true, false);
                        $id_entidadeintegracao = $salaDeAulaTO->getId_entidadeintegracao();
                    }


                    $moodle = new MoodleAlocacaoWebServices($to->getId_entidade(), $id_entidadeintegracao);


                    $mensagem_ativa_encerrado = $moodle->cadastrarPapel(
                        $to->getId_usuario(),
                        $to->getId_saladeaula(),
                        $to->getNu_perfilalunoencerrado(),
                        $to->getId_entidade(),
                        false,
                        'alteraAlunosSalaPeriodica');
                    if ($mensagem_ativa_encerrado->getTipo() == Ead1_IMensageiro::SUCESSO) {

                        $alocacaoIntegracaoTo = new AlocacaoIntegracaoTO();
                        $alocacaoIntegracaoTo->setId_alocacao($to->getId_alocacao());
                        $alocacaoIntegracaoTo->setid_entidadeintegracao($id_entidadeintegracao);
                        $alocacaoIntegracaoTo->fetch(false, true, true);
                        $alocacaoIntegracaoTo->setBl_encerrado(true);

                        $this->_dao->updateAlocacaoIntegracao($alocacaoIntegracaoTo);

                    } //Caso o usuário tenha sido deletado do moodle
                    elseif (strpos($mensagem_ativa_encerrado->getFirstMensagem(), 'User ID does not exist or is deleted!') !== false) {
                        $alocacaoIntegracaoTo = new AlocacaoIntegracaoTO();
                        $alocacaoIntegracaoTo->setId_alocacao($to->getId_alocacao());
                        $alocacaoIntegracaoTo->setid_entidadeintegracao($id_entidadeintegracao);
                        $alocacaoIntegracaoTo->fetch(false, true, true);
                        $alocacaoIntegracaoTo->setBl_encerrado(true);

                        $this->_dao->updateAlocacaoIntegracao($alocacaoIntegracaoTo);
                    } else {
                        throw new Zend_Exception($mensagem_ativa_encerrado->getFirstMensagem());
                    }

                }
            }
            $this->mensageiro->setMensageiro($vwTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao alterar perfil do aluno no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }


    /**
     * Método que altera as salas que serao abertas nos proximos 7 dias e que tenham conteudo para visiveis no moodle
     * @return Ead1_Mensageiro
     */
    public function alteraSalaVisivelMoodle()
    {
        try {
            $dados = $this->_dao->retornaVwSalaDeaulaVisivelMoodle();
            if (empty($dados)) {
                THROW new Zend_Validate_Exception("Nenhuma sala de aula para ser alterada!");
            } else {
                $vwTO = new VwSalaDeAulaVisivelMoodleTO();
                $vwTO = Ead1_TO_Dinamico::encapsularTo($dados, $vwTO);

                foreach ($vwTO as $key => $to) {
                    $moodle = new MoodleCursosWebServices();
                    $mensagem_ativa_visivel_curso = $moodle->alterarVisibilidadeCurso($to);

                    if ($mensagem_ativa_visivel_curso->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $arraySalas[] = $mensagem_ativa_visivel_curso->getFirstMensagem();

                    } else {
                        throw new Zend_Exception($mensagem_ativa_visivel_curso->getFirstMensagem());
                    }

                }
            }
            $vwTO = Ead1_TO_Dinamico::encapsularTo($arraySalas, $vwTO);
            $this->mensageiro->setMensageiro($vwTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao alterar visibilidade da sala de aula no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    public function retornaUsuarioIntegracaoTO(UsuarioIntegracaoTO $to)
    {
        try {
            if (!$to->getId_usuario() || !$to->getId_sistema() || !$to->getId_entidade()) {
                throw new Zend_Exception('O id_usuario e/ou id_sistema e/ou id_entidade  são obrigatórios e não foram passados!');
            }
            $to = Ead1_TO_Dinamico::encapsularTo($this->_dao->retornaUsuarioIntegracao($to), new UsuarioIntegracaoTO(), true);
            return $to;

        } catch (Exception $e) {
            return new UsuarioIntegracaoTO();
        }

    }

    /**
     * Altera perfil de alunos de sala reaberta
     * @param array $alunos
     * @return Ead1_Mensageiro
     */
    public function alteraAlunosSalaReaberta(array $alunos)
    {
        try {
            /** @var \G2\Entity\VwAlunosReativarMoodle $aluno */
            foreach ($alunos as $aluno) {
                $moodle = new MoodleAlocacaoWebServices($aluno->getId_entidade(), $aluno->getid_entidadeintegracao());

                if ($aluno->getId_sistema() == SistemaTO::MOODLE) {
                    $mensagem_ativa_encerrado = $moodle->desvincularPerfil(
                        $aluno->getId_usuario(),
                        $aluno->getNu_perfilalunoencerrado(),
                        $aluno->getSt_codsistemacurso(),
                        $aluno->getId_entidade()
                    );
                    if ($mensagem_ativa_encerrado->getTipo() == Ead1_IMensageiro::SUCESSO) {

                        $alocacaoIntegracaoTo = new AlocacaoIntegracaoTO();
                        $alocacaoIntegracaoTo->setId_alocacaointegracao($aluno->getId_alocacaointegracao());
                        $alocacaoIntegracaoTo->setid_entidadeintegracao($aluno->getid_entidadeintegracao());
                        $alocacaoIntegracaoTo->fetch(false, true, true);
                        if ($alocacaoIntegracaoTo && $alocacaoIntegracaoTo->getDt_cadastro()) {
                            $alocacaoIntegracaoTo->setBl_encerrado(false);
                            $this->_dao->updateAlocacaoIntegracao($alocacaoIntegracaoTo);
                        }

                    }
                    $this->mensageiro->addMensagem($mensagem_ativa_encerrado);
                }
            }

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao alterar perfil do aluno no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * @param VwSalaDeAulaProfessorIntegracaoTO $professor
     * @return Ead1_Mensageiro
     */
    public function retirarProfessorSalaMoodle(VwSalaDeAulaProfessorIntegracaoTO $professor)
    {
        try {
            //busca os dados no banco
            $professor->fetch(false, true, true);

            $moodle = new MoodleAlocacaoWebServices(null, $professor->getid_entidadeintegracao());
            $mensagem_ativa_encerrado = $moodle->desvincularPerfil($professor->getId_usuario(), $moodle::ROLEID_PROFESSOR, $professor->getSt_codsistemacurso(), $professor->getId_entidade());
            if ($mensagem_ativa_encerrado->getTipo() == Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro->setMensageiro('Perfil de Professor retirado da sala moodle!', Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Não foi possível retirar o papel do professor!', Ead1_IMensageiro::ERRO);
            }

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao retirar perfil de professor no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Altera perfil de alunos de sala reaberta
     * @param array $coodernadores
     * @return Ead1_Mensageiro
     */
    public function retirarCoordenadoresSalaMoodle(array $coodernadores)
    {
        try {

            /** @var \G2\Entity\VwDesativarCoordenadoresMoodle $coodernador */
            foreach ($coodernadores as $key => $coodernador) {
                if ($coodernador->getId_sistema() == SistemaTO::MOODLE) {

                    $moodle = new MoodleAlocacaoWebServices(null, $coodernador->getId_entidadeintegracao());
                    $entidadeIntegracao = $moodle->getEntidadeIntegracaoTO();

                    //desvincula o perfil no moodle
                    $mensagem_desativar = $moodle->desvincularPerfil(
                        $coodernador->getId_usuario(),
                        $entidadeIntegracao->getNu_perfilprojeto(),
                        $coodernador->getSt_codsistemacurso(),
                        $coodernador->getId_entidade()
                    );

                    if ($mensagem_desativar->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        $perfilIntegracaoTO = new PerfilReferenciaIntegracaoTO();
                        $perfilIntegracaoTO->setId_perfilreferenciaintegracao($coodernador->getId_perfilreferenciaintegracao());
                        $perfilIntegracaoTO->fetch(true, true, true);
                        if ($perfilIntegracaoTO) {
                            $perfilIntegracaoTO->setBl_encerrado(true);
                            $updateIntegracao = $this->_dao->salvarPerfilReferencia($perfilIntegracaoTO);
                        }

                    }
                    $this->mensageiro->addMensagem($mensagem_desativar);
                }
            }

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao alterar perfil do aluno no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     *
     */
    public function desvinculaProfessorSalaMoodle(array $professores)
    {
        try {
            $moodle = new MoodleAlocacaoWebServices();
            $perfilNegocio = new \G2\Negocio\Perfil();
            $erros = 0;
            $sucesso = 0;
            $aviso = 0;
            foreach ($professores as $professor) {
                if ($professor instanceof \G2\Entity\VwDesativarProfessoresMoodle) {
                    $mensagem_ativa_encerrado = $moodle->desvincularPerfil($professor->getId_usuario(), $moodle::ROLEID_PROFESSOR, $professor->getSt_codsistemacurso(), $professor->getId_entidade());
                    if ($mensagem_ativa_encerrado->getTipo() == Ead1_IMensageiro::SUCESSO) {
                        if ($perfilNegocio->alteraRemocaoProfessorMoodle($professor)) {
                            $sucesso++;
                        }
                    } else {
                        $aviso++;
                    }
                }
            }
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao retirar perfil de professor no moodle! -" . $e->getMessage(), Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        if ($sucesso || $aviso) {
            $this->mensageiro->setMensageiro("Professores retirados do moodle! -" . $sucesso . ". Avisos: " . $aviso, Ead1_IMensageiro::SUCESSO);
        }
        return $this->mensageiro;
    }
}

?>

<?php


class BraspagRecorrenteBO extends Ead1_BO  {

	private $integracao = false;


    /**
	 * seta a integração
	 * @throws Zend_Exception
	 */
	private function setIntegracao($id_entidade = null){

		$this->integracao = new EntidadeIntegracaoTO();
		$this->integracao->setId_entidade($id_entidade ? $id_entidade : $this->integracao->getSessao()->id_entidade);
		$this->integracao->setId_sistema(SistemaTO::BRASPAG_RECORRENTE);
		$this->integracao->fetch(false,true, true);

		if(!$this->integracao->getSt_codchave()){
			throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
		}

	}

	/**
	 * Retorna a Integração
	 * @return EntidadeIntegracaoTO
	 */
	private function getIntegracao(){
		return $this->integracao;
	}

	/**
	 * Conecta ao WebService da operadora ou gateway de pagamento
	 */
	public function __construct($id_entidade = null){
		$this->setIntegracao($id_entidade);
	}

	/**
	 * Solicita uma autorização de pagamento
	 * @param array $dadosCartao - Array com dados do Cartão que será convertido no TO correspondente
	 * @return Ead1_Mensageiro
	 */
	public function autorizar(array $dadosCartao){

		try {


			$confirmacaoPagador = false;

            $integracao = $this->getIntegracao();
			if(!$integracao){
				throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
			}

			if(!isset($dadosCartao['id_cartaobandeira']) || $dadosCartao['id_cartaobandeira']==false){
				throw new Zend_Exception("Por favor, informe a bandeira do seu cartão!");
			}

			/**
			 * Primeira parcela é feita no Pagador
			 */
			if(isset($dadosCartao['id_modelovenda']) && $dadosCartao['id_modelovenda']==\G2\Entity\ModeloVenda::PADRAO){

				$pagador = new BraspagPagadorBO();
				$dadosPagador = $dadosCartao;
				$dadosPagador['id_meiopagamento'] 	= MeioPagamentoTO::CARTAO;
				$dadosPagador['nu_parcelas'] 		= 1;
				$dadosPagador['nu_valortotal'] 		= $dadosPagador['nu_valorparcela'];

                $retornoPagador = $pagador->autorizar($dadosPagador);
				if($retornoPagador->getTipo()!=Ead1_IMensageiro::SUCESSO || $dadosCartao["nu_parcelas"] == 1){
					return $retornoPagador;
				}

            } //if($dadosCartao['id_campanhacomercial']==false){
// 				$retornoPagador = new Ead1_Mensageiro(new TransacaoFinanceiraTO(), Ead1_IMensageiro::SUCESSO);
				$confirmacaoPagador = true;
			///// CONTINUA EM CASO DE MAIS PRESTAÇÕES ////////////


            $entCartao = new VwEntidadeCartaoTO();
			$entCartao->setId_entidade($integracao->getId_entidade());
			$entCartao->setId_cartaobandeira($dadosCartao['id_cartaobandeira']);
			$entCartao->fetch(false,true,true);


            $cartao = new PagamentoCartaoBrasPagTO();
			$cartao->montaToDinamico($dadosCartao);

            $cartao->setId_cartaooperadora($entCartao->getId_cartaooperadora());

            $cartao->setNu_valorparcela($dadosCartao['nu_valorparcela'] / 100);
			if(!$cartao->getId_cartaobandeira()){
				throw new Zend_Exception('Não foi encontrada a relação REDE -> BANDEIRA para este pagamento');
			}


            $venda = new VendaTO();
			$venda->setId_venda($cartao->getId_venda());
			$venda->fetch(true,true,true);

            $usuario = new VwPessoaTO();
			$usuario->setId_usuario($venda->getId_usuario());
			$usuario->setId_entidade($venda->getId_entidade());
			$usuario->fetch(false,true,true);


            $atTO = new CreateCreditCardOrderTO();
			$atTO->setUrlBraspag($integracao->getSt_caminho());
			$atTO->setMerchantId($integracao->getSt_codchave());
			$atTO->setOrderId($venda->getId_venda().'G2U');


			/** Período de recorrência **/

            if (isset($dadosCartao['id_modelovenda']) && $dadosCartao['id_modelovenda'] == \G2\Entity\ModeloVenda::ASSINATURA) {
				$cartao->setDt_inicio($dadosCartao['dt_iniciorecorrencia']);
			} else {
				$dt_recorrencia = new Zend_Date();
				$dt_recorrencia->addMonth(1);
				$cartao->setDt_inicio($dt_recorrencia->toString("dd/MM/yyyy"));
				if($cartao->getnu_parcelas()>1){
					$dt_recorrencia->addMonth($cartao->getnu_parcelas()-2);
				}
                $dt_recorrencia->addDay(25);
				$cartao->setDt_termino($dt_recorrencia->toString("dd/MM/yyyy"));
			}

            $atTO->setStartDate($cartao->getDt_inicio());
			$atTO->setEndDate($cartao->getDt_termino() ? $cartao->getDt_termino() : null);
			$atTO->setRecurrenceInterval(CreateCreditCardOrderTO::INTERVALO_MENSAL);

            $atTO->setPaymentMethod($cartao->getId_cartaobandeira());

            $atTO->setHolder($cartao->getSt_nomeimpresso());
			$atTO->setCardNumber($cartao->getSt_cartao());
			$atTO->setSecurityCode($cartao->getSt_codigoseguranca());
			$atTO->setExpirationDate($cartao->getNu_mesvalidade().'/'.$cartao->getNu_anovalidade());

            $atTO->setAmount($cartao->getNu_valorparcela());

           /*if($atTO->getUrlBraspag() == 'https://homologacao.pagador.com.br/webservice/Recorrente.asmx'){
                $atTO->setCardNumber('4551870000000183');
            }*/
            //\Zend_Debug::dump($atTO,__CLASS__.'('.__LINE__.')');exit;

			$pagador = new Braspag_Recorrente();
			$retorno = $pagador->autorizar($atTO);
			if($retorno->getTipo()!=Ead1_IMensageiro::SUCESSO){
				throw new Zend_Exception($retorno->getFirstMensagem());
			}
			if($dadosCartao['id_modelovenda']==\G2\Entity\ModeloVenda::ASSINATURA){
				$retornoPagador = $retorno;
			}
            $pedidoTransacao = $this->pedidoIntegracao(array('st_orderid' => $cartao->getId_venda().'G2U', 'id_venda' => $cartao->getId_venda(), 'dt_vencimentocartao' => $cartao->getNu_anovalidade ().'-'.$cartao->getNu_mesvalidade().'-01'));
			return $retornoPagador; // Retorna o resultado do Pagador para salvar a TransacaoFinanceira

        } catch (Exception $e) {
			if($confirmacaoPagador){
				return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
			} else {
				return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
			}

        }

    }

    /**
	 * Verifica uma solicitação de pagamento
	 */
    public function verificar(G2\Entity\VwVendaLancamento $to, $simulando = false)
    {

        $negocio = new \G2\Negocio\Negocio();
        $negocio->beginTransaction();

		try {
            $venda = new \G2\Negocio\Venda();
            $recorrente = new \Braspag_Recorrente();
            $subtrairNuOrdem    = 0;
			$parcelaRecorrente 	= false;

            $integracao = $this->getIntegracao();
			if(!$integracao){
				throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
			}

            if ($to->getRecorrenteOrderid()){
                $subtrairNuOrdem = 1;
            }

            $orderTO = new GetOrderTO();
            $orderTO->setOrderId($to->getRecorrenteOrderid() ? $to->getRecorrenteOrderid() : $to->getIdVenda().'G2U');
            $orderTO->setMerchantId($integracao->getSt_codchave());
            $orderTO->setUrlBraspag($integracao->getSt_caminho());

            $mensageiroOrder = $recorrente->getOrder($orderTO);

            if($mensageiroOrder->getTipo() == \Ead1_IMensageiro::SUCESSO){
                $order = $mensageiroOrder->getFirstMensagem();

                $pedidoTransacao = $negocio->getRepository('G2\Entity\PedidoIntegracao')->findOneBy(array('id_venda' => $to->getIdVenda()));

                if(empty($pedidoTransacao))
                    $pedidoTransacao = new G2\Entity\PedidoIntegracao();

                $pedidoTransacao->setIdVenda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));
                $pedidoTransacao->setNuValor($order['GetOrderResult']['Order']['Amount']);

                $situacaoIntegracao = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('nu_status' => $order['GetOrderResult']['Order']['Status'], 'id_sistema' => \G2\Constante\Sistema::BRASPAG_RECORRENTE));

                $pedidoTransacao->setIdSituacaointegracao($situacaoIntegracao->getIdSituacaointegracao());
                $pedidoTransacao->setDtEnvio(new \DateTime($order['GetOrderResult']['Order']['OrderDate']));
                $pedidoTransacao->setDtInicio(new \DateTime($order['GetOrderResult']['Order']['StartDate']));

                if(!empty($order['GetOrderResult']['Order']['NextRetry']))
                    $pedidoTransacao->setDtProximatentativa(new \DateTime($order['GetOrderResult']['Order']['NextRetry']));

                if($order['GetOrderResult']['Order']['NextPayment'])
                    $pedidoTransacao->setDtProximopagamento(new \DateTime($order['GetOrderResult']['Order']['NextPayment']));

                if($order['GetOrderResult']['Order']['EndDate'])
                    $pedidoTransacao->setDtTermino(new \DateTime($order['GetOrderResult']['Order']['EndDate']));

                $pedidoTransacao->setIdSistema($negocio->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::BRASPAG_RECORRENTE));
                $pedidoTransacao->setNuBandeiracartao($order['GetOrderResult']['Order']['PaymentMethod']);
                $pedidoTransacao->setNuDiavencimento($order['GetOrderResult']['Order']['PaymentDay']);
                $pedidoTransacao->setNuIntervalo($order['GetOrderResult']['Order']['RecurrenceInterval']);
                $pedidoTransacao->setNuTentativas($order['GetOrderResult']['Order']['Tries']);
                $pedidoTransacao->setNuTentativasfeitas($order['GetOrderResult']['Order']['NumberPayments']);
                $pedidoTransacao->setStMensagem($order['GetOrderResult']['Description']);
                $pedidoTransacao->setStOrderid($to->getRecorrenteOrderid() ? $to->getRecorrenteOrderid() : $to->getIdVenda().'G2U');

                $negocio->save($pedidoTransacao);
            }

			$go = new GetOrderRecurrenciesTO();
			$go->setUrlBraspag($integracao->getSt_caminho());
            $go->setOrderId($to->getRecorrenteOrderid() ? $to->getRecorrenteOrderid() : $to->getIdVenda().'G2U');
			$go->setMerchantId($integracao->getSt_codchave());

			$mensageiro = $recorrente->GetOrderRecurrencies($go);

			if($mensageiro->getTipo()==Ead1_IMensageiro::SUCESSO){
				$pedido = $mensageiro->getMensagem();
				$recorrencias = $pedido['GetOrderRecurrenciesResult']['Recurrencies']['Recurrence'];

				if(isset($recorrencias[0])){
					foreach($recorrencias as $recorrencia){
						if(($to->getNuOrdem()-$subtrairNuOrdem)==$recorrencia['RecurrencyNumber']){
							$parcelaRecorrente = $recorrencia;
						}
					}
				} else {
					if(($to->getNuOrdem()-$subtrairNuOrdem)==$recorrencias['RecurrencyNumber']){
						$parcelaRecorrente = $recorrencias;
					}
				}
			}

			$mensagem = 'não possui parcela no recorrente.';
 			if($parcelaRecorrente){

                $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));

                $lancamento->setNuVerificacao($lancamento->getNuVerificacao()+1);
                $lancamento->setNuTentativabraspag($parcelaRecorrente['TryNumber']);
                $lancamento->setStRetornoverificacao(serialize($parcelaRecorrente));

                $negocio->save($lancamento);

                $mensagem = 'verificado pela '.$lancamento->getNuVerificacao().' vez!';
                if($parcelaRecorrente['Status'] == 0 || $parcelaRecorrente['Status'] == 1){
                    $lancamento->setNuQuitado((float)$parcelaRecorrente['Amount']);
                    $lancamento->setDtQuitado(new \DateTime($parcelaRecorrente['RecurrencyDate']));
                    $lancamento->setBlBaixadofluxus(true);

                    $venda->quitarCredito(array($lancamento));

                    $mensagem = 'quitado com sucesso!';
                }

                $transacaoFinanceira = new \G2\Entity\TransacaoFinanceira();
                $transacaoFinanceira->setStMensagem($parcelaRecorrente['AcquirerMessage']);
                $transacaoFinanceira->setDtCadastro(new \DateTime() );

                $situacaoIntegracaoLancamento = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('nu_status' => $parcelaRecorrente['Status'], 'id_sistema' => \G2\Constante\Sistema::BRASPAG_LANCAMENTO));
                $transacaoFinanceira->setIdSituacaointegracao($situacaoIntegracaoLancamento->getIdSituacaointegracao());

                $usuario = $negocio->getRepository('G2\Entity\Usuario')->findOneBy(array('id_usuario' => 1));

                $transacaoFinanceira->setIdUsuariocadastro($usuario);
                $transacaoFinanceira->setIdVenda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));
                $transacaoFinanceira->setNuStatus($parcelaRecorrente['Status']);
                $transacaoFinanceira->setNuVerificacao($parcelaRecorrente['TryNumber']);
                $transacaoFinanceira->setUnTransacaofinanceira('00000000-0000-0000-0000-000000000000');

                $negocio->save($transacaoFinanceira);

                $transacaoLancamento = new \G2\Entity\TransacaoLancamento();
                $transacaoLancamento->setIdLancamento($lancamento->getIdLancamento());
                $transacaoLancamento->setIdTransacaofinanceira($transacaoFinanceira->getIdTransacaofinanceira());

                $negocio->save($transacaoLancamento);

			} else {
 			    throw new Exception($mensagem);
            }
            $negocio->commit();
			return new \Ead1_Mensageiro('Braspag V1: Lançamento '.$to->getStNomecompleto().': '.$to->getIdLancamento().' '.$mensagem, \Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
            $negocio->rollback();

            $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));
            $lancamento->setNuVerificacao($lancamento->getNuVerificacao()+1);
            $lancamento->setDt_atualizado(new \DateTime());
            $negocio->save($lancamento);


			return new \Ead1_Mensageiro('Braspag V1: Lançamento '.$to->getStNomecompleto().': '.$e->getMessage(), \Ead1_IMensageiro::ERRO);
		}

    }

    /**
	 * Solicita um cancelamento de pagamento
	 */
	public function cancelar(TransacaoFinanceiraTO $to, VendaTO $venda){

// 		try {

// 			$integracao = $this->getIntegracao();

// 			if(!$integracao){
// 				throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
// 			}

// 			$vcctTO = new VoidCreditCardTransactionTO();
// 			$vcctTO->setMerchantId($integracao->getSt_codchave());
// 			$vcctTO->setAmount($venda->getNu_valorliquido());
// 			$vcctTO->setBraspagTransactionId($to->getUn_transacaofinanceira());
// 			$vcctTO->setUrlBraspag($integracao->getSt_caminho());


// 			$pagador = new Braspag_Pagador();
// 			$retorno = $pagador->cancelar($vcctTO);
// 			if($retorno->getTipo()!=Ead1_IMensageiro::SUCESSO){
// 				throw new Zend_Exception($retorno->getMensagem());
// 			}

// 			return $retorno;
// 		} catch (Exception $e) {
// 			return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
// 		}


    }

    /**
	 * Solicita o estorno de um pagamento
	 */
	public function estornar(){

        return new Ead1_Mensageiro('Não implementado!', Ead1_IMensageiro::ERRO);

    }

    /**
     * Solicita a mudança do número do cartão de cédito
     * @param array $dadosCartao - Array com dados do Cartão que será convertido no TO correspondente
     * @return Ead1_Mensageiro
     */
    public function mudarCartaoRecorrente(array $dadosCartao){

        try {
            $dadosCartao['id_cartaobandeira'] = $dadosCartao['id_cartaobandeira'] ? $dadosCartao['id_cartaobandeira'] : CartaoBandeiraTO::BANDEIRA_VISA;
            $integracao = $this->getIntegracao();
            if(!$integracao){
                throw new Zend_Exception("Não existe interface de integração para o sistema BRASPAG nesta entidade!");
            }

            if(!isset($dadosCartao['id_cartaobandeira']) || $dadosCartao['id_cartaobandeira']==false){
                throw new Zend_Exception("Por favor, informe a bandeira do seu cartão!");
            }

            $entCartao = new VwEntidadeCartaoTO();
            $entCartao->setId_entidade($integracao->getId_entidade());
            $entCartao->setId_cartaobandeira($dadosCartao['id_cartaobandeira']);
            $entCartao->fetch(false,true,true);

            $cartao 	= new PagamentoCartaoBrasPagTO();
            $cartao->montaToDinamico($dadosCartao);

            $cartao->setId_cartaooperadora($entCartao->getId_cartaooperadora());
            $cartao->setNu_valorparcela($dadosCartao['nu_valorparcela']/100);

            if(!$cartao->getId_cartaobandeira()){
                throw new Zend_Exception('Não foi encontrada a relação REDE -> BANDEIRA para este pagamento');
            }

            $venda = new VendaTO();
            $venda->setId_venda($cartao->getId_venda());
            $venda->fetch(true,true,true);

            $usuario = new VwPessoaTO();
            $usuario->setId_usuario($venda->getId_usuario());
            $usuario->setId_entidade($venda->getId_entidade());
            $usuario->fetch(false,true,true);


            $atTO = new CreateCreditCardOrderTO();

            $atTO->setUrlBraspag($integracao->getSt_caminho());
            $atTO->setMerchantId($integracao->getSt_codchave());
            $atTO->setOrderId($venda->getRecorrenteOrderid() ? $venda->getRecorrenteOrderid() : $venda->getId_venda().'G2U');
            $atTO->setPaymentMethod($cartao->getId_cartaobandeira());
            $atTO->setCardNumber($cartao->getSt_cartao());
            $atTO->setSecurityCode($cartao->getSt_codigoseguranca());
            $atTO->setExpirationDate($cartao->getNu_mesvalidade().'/'.$cartao->getNu_anovalidade());
            $atTO->setHolder($cartao->getSt_nomeimpresso());

            /*if($atTO->getUrlBraspag() == 'https://homologacao.pagador.com.br/webservice/Recorrente.asmx'){
                $atTO->setCardNumber('4551870000000183');
            }*/

            $pagador = new Braspag_Recorrente();
            $retorno = $pagador->changePaymentMethodCreditCard($atTO);
            //\Zend_Debug::dump($retorno,__CLASS__.'('.__LINE__.')');exit;
            if($retorno->getTipo() != Ead1_IMensageiro::SUCESSO){
                throw new Zend_Exception($retorno->getFirstMensagem());
            }
            $this->pedidoIntegracao(array('st_orderid' => $cartao->getId_venda().'G2U', 'id_venda' => $cartao->getId_venda(), 'dt_vencimentocartao' => $cartao->getNu_anovalidade ().'-'.$cartao->getNu_mesvalidade().'-01', 'dt_mudancavencimento' => date('Y-m-d')));
            return $retorno; // Retorna o resultado do Pagador para salvar a TransacaoFinanceira

        } catch (Exception $e) {
                return new Ead1_Mensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Método para verificar na braspag um pedido de recorrencia e inserir/alterar os dados retornados no banco.
     * array('st_orderid', 'id_venda', 'dt_vencimentocartao')
     */
    public function pedidoIntegracao(array $params){
        try{

            $recorrente = new \Braspag_Recorrente();
            $negocio = new \G2\Negocio\Negocio();
            $integracao = $this->getIntegracao();

            $orderTO = new GetOrderTO();
            $orderTO->setOrderId($params['st_orderid']);
            $orderTO->setMerchantId($integracao->getSt_codchave());
            $orderTO->setUrlBraspag($integracao->getSt_caminho());

            $mensageiroOrder = $recorrente->getOrder($orderTO);

            if($mensageiroOrder->getTipo() == \Ead1_IMensageiro::SUCESSO){
                $order = $mensageiroOrder->getFirstMensagem();

                $pedidoTransacao = $negocio->getRepository('G2\Entity\PedidoIntegracao')->findOneBy(array('id_venda' => $params['id_venda']));

                if(empty($pedidoTransacao))
                    $pedidoTransacao = new G2\Entity\PedidoIntegracao();


                $venda = $negocio->getRepository('G2\Entity\Venda')->findOneBy(array('id_venda' => $params['id_venda']));
                $pedidoTransacao->setIdVenda($venda);
                $pedidoTransacao->setNuValor($order['GetOrderResult']['Order']['Amount']);

                $situacaoIntegracao = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('nu_status' => $order['GetOrderResult']['Order']['Status'], 'id_sistema' => \G2\Constante\Sistema::BRASPAG_RECORRENTE));

                $pedidoTransacao->setIdSituacaointegracao($situacaoIntegracao->getIdSituacaointegracao());
                $pedidoTransacao->setDtEnvio(new \DateTime($order['GetOrderResult']['Order']['OrderDate']));
                $pedidoTransacao->setDtInicio(new \DateTime($order['GetOrderResult']['Order']['StartDate']));

                if(!empty($order['GetOrderResult']['Order']['NextRetry']))
                    $pedidoTransacao->setDtProximatentativa(new \DateTime($order['GetOrderResult']['Order']['NextRetry']));


                if (!empty($params['dt_vencimentocartao']))
                    $pedidoTransacao->setDt_vencimentocartao(new \DateTime($params['dt_vencimentocartao']));

                if (!empty($params['dt_mudancavencimento']))
                    $pedidoTransacao->setDt_mudancavencimento(new \DateTime($params['dt_mudancavencimento']));

                if(!empty($order['GetOrderResult']['Order']['EndDate']))
                    $pedidoTransacao->setDtTermino(new \DateTime($order['GetOrderResult']['Order']['EndDate']));

                $pedidoTransacao->setDtProximopagamento(new \DateTime($order['GetOrderResult']['Order']['NextPayment']));
                $pedidoTransacao->setIdSistema($negocio->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::BRASPAG_RECORRENTE));
                $pedidoTransacao->setNuBandeiracartao($order['GetOrderResult']['Order']['PaymentMethod']);
                $pedidoTransacao->setNuDiavencimento($order['GetOrderResult']['Order']['PaymentDay']);
                $pedidoTransacao->setNuIntervalo($order['GetOrderResult']['Order']['RecurrenceInterval']);
                $pedidoTransacao->setNuTentativas($order['GetOrderResult']['Order']['Tries']);
                $pedidoTransacao->setNuTentativasfeitas($order['GetOrderResult']['Order']['NumberPayments']);
                $pedidoTransacao->setStMensagem($order['GetOrderResult']['Description']);
                $pedidoTransacao->setStOrderid($params['st_orderid']);

                $negocio->save($pedidoTransacao);

                return new \Ead1_Mensageiro($pedidoTransacao, \Ead1_IMensageiro::SUCESSO);
            }else{
                return new \Ead1_Mensageiro('test', \Ead1_IMensageiro::ERRO);
            }

        }catch(Exception $message) {
            throw $message;
        }
    }

}

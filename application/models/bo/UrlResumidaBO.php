<?php

/**
 * UrlResumidaBO
 *
 * @author <rafaelbruno.ti@gmail.com>
 */
class UrlResumidaBO extends Ead1_BO {

    public function __construct() {
        ;
    }

    /**
     * @param UrlResumidaTO $urlResumidaTO
     * @return UrlResumidaBO retorna dados de um unico registro de uma url resumida
     */
    public function buscaPorUrlResumida(UrlResumidaTO $urlResumidaTO) {
        return $urlResumidaTO->fetch(false, true);
    }

    public function insert(UrlResumidaTO $urlResumidaTO){
        $orm = new UrlResumidaORM();
        
        $dataInsert = array(
            'st_urlresumida' => $urlResumidaTO->getSt_urlresumida(),
            'st_urlcompleta' => $urlResumidaTO->getSt_urlcompleta()
        );
        
        return $orm->insert($dataInsert);
    }
}
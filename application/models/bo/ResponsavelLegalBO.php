<?php
/**
 * Classe com regras de negócio para interações de Responsavel Legal
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage bo
 */
class ResponsavelLegalBO extends SecretariaBO{
	
	private $dao;
	public $mensageiro;
	
	const DIRETOR = 1;
	
	public function __construct(){
		$this->dao = new ResponsavelLegalDAO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Metodo que cadastra o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		if(!$erlTO->getId_entidade()){
			$erlTO->setId_entidade($erlTO->getSessao()->id_entidade);
		}
		$id_entidadeResponsavelLegal = $this->dao->cadastrarEntidadeResponsavelLegal($erlTO);
		if(!$id_entidadeResponsavelLegal){
			return $this->mensageiro->setMensageiro('Erro ao Cadastrar Responsável Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$erlTO->setId_entidaderesponsavellegal($id_entidadeResponsavelLegal);
		return $this->mensageiro->setMensageiro($erlTO,Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que edita o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		if(!$erlTO->getId_entidade()){
			$erlTO->setId_entidade($erlTO->getSessao()->id_entidade);
		}
		if(!$this->dao->editarEntidadeResponsavelLegal($erlTO)){
			return $this->mensageiro->setMensageiro('Erro ao Editar Responsável Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Responsável Legal Editado com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que exclui o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		if(!$erlTO->getId_entidade()){
			$erlTO->setId_entidade($erlTO->getSessao()->id_entidade);
		}
		if(!$this->dao->deletarEntidadeResponsavelLegal($erlTO)){
			return $this->mensageiro->setMensageiro('Erro ao Excluir Responsável Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		return $this->mensageiro->setMensageiro('Responsável Legal Excluido com Sucesso!',Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Responsavel Legal da Entidade 
	 * @param VwResponsavelLegalTO $vrlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntidadeResponsavelLegal(VwResponsavelLegalTO  $vrlTO){
		if(!$vrlTO->getId_entidade()){
			$vrlTO->setId_entidade($vrlTO->getSessao()->id_entidade);
		}
		$responsavel = $this->dao->retornarEntidadeResponsavelLegal($vrlTO);
		if(!is_object($responsavel)){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Responsável Legal!',Ead1_IMensageiro::ERRO,$this->dao->excecao);
		}
		$responsavel = $responsavel->toArray();
		if(empty($responsavel)){
			return $this->mensageiro->setMensageiro('Nenhum Registro de Responsável Encontrado!',Ead1_IMensageiro::AVISO);
		}
		return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($responsavel, new VwResponsavelLegalTO()),Ead1_IMensageiro::SUCESSO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $terlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoEntidadeResponsavelLegal(TipoEntidadeResponsavelLegalTO $terlTO){
		try{
			$responsavel = $this->dao->retornarTipoEntidadeResponsavelLegal($terlTO)->toArray();
			if(empty($responsavel)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
			}
			return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($responsavel, new TipoEntidadeResponsavelLegalTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			return $this->mensageiro->setMensageiro('Erro ao Retornar Tipo de Responsável Legal!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
}
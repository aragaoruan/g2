<?php
class SincronizacaoPessoaBO extends SincronizacoImportacaoBO{
	
	private $dao;
	
	public function __construct(){
		parent::__construct();
		$this->dao = new ImportacaoDAO();
		$estadoCivil = self::returnClassInstance(new ImportacaoRO())->retornarEstadoCivilImportacao(new EstadocivilImportacaoTO());
		$this->arrEstadoCivilOrigem = ($estadoCivil->getTipo() != Ead1_IMensageiro::SUCESSO ? array() : $estadoCivil->getMensagem());
		$etnia = self::returnClassInstance(new ImportacaoRO())->retornarEtniaImportacao(new EtniaImportacaoTO());
		$this->arrEtniaOrigem = ($etnia->getTipo() != Ead1_IMensageiro::SUCESSO ? array() : $etnia->getMensagem());
	}
	
	/**
	 * Variavel que contem um array dos vinculos dos estados civis da importacao 
	 * @var array
	 */
	protected $arrEstadoCivilOrigem;
	
	/**
	 * Variavel que contem um array dos vinculos das etnias da importacao 
	 * @var array
	 */
	protected $arrEtniaOrigem;
	
	/**
	 * Metodo que retorna a o Estado Civil pelo cod estado civil origem
	 * @param String $nu_codestadocivilorigem
	 * @return EstadocivilImportacaoTO | false
	 */
	protected function _retornaEstadoCivilImportacao($nu_codestadocivilorigem){
		if(empty($this->arrEstadoCivilOrigem)){
			return false;
		}
		foreach ($this->arrEstadoCivilOrigem as $eciTO){
			if($this->_substituirCaracteresEspeciais(strtolower($eciTO->getNu_codestadocivilorigem())) == $this->_substituirCaracteresEspeciais(strtolower($nu_codestadocivilorigem))){
				return $eciTO;
			}
		}
		return false;
	}
	
	/**
	 * Metodo que retorna a etnia pelo codetniaorigem
	 * @param String $nu_codetniaorigem
	 * @return EtniaImportacaoTO | false
	 */
	protected function _retornaEtniaImportacao($nu_codetniaorigem){
		if(empty($this->arrEtniaOrigem)){
			return false;
		}
		foreach ($this->arrEtniaOrigem as $eiTO){
			if($this->_substituirCaracteresEspeciais(strtolower($eiTO->getNu_codestadocivilorigem())) == $this->_substituirCaracteresEspeciais(strtolower($nu_codetniaorigem))){
				return $eiTO;
			}
		}
		return false;
	}
	
	/**
	 * Metodo que sincroniza os dados da tabela tb_pessoaimportacao com as tabelas do sistema
	 * @return Ead1_Mensageiro
	 */
	public function sincronizarPessoaImportacao(){
		try{
			$this->_log = array();
			$select = new PessoaImportacaoORM();
			$where = $select->select()->from(array('psii' => 'tb_pessoaimportacao'))
								->joinLeft(array('psi' => 'tb_pessoaimportacao'), 
												'psi.nu_codpessoaorigem = psii.nu_codpessoaorigem 
												 and psi.id_sistemaimportacao = psii.id_sistemaimportacao
												 and psi.id_pessoaimportacao != psii.id_pessoaimportacao',
												'psi.id_usuarioimportado as id_usuarioimportadoexistente')
								->where('psii.id_usuarioimportado is null');
								
			$dadosPessoaImportacao = $this->dao->retornarPessoaImportacao(new PessoaImportacaoTO(),$where)->toArray();
			if(empty($dadosPessoaImportacao)){
				return $this->mensageiro->setMensageiro('Nenhum Registro Encontradoa Pessoa a Sincronizar!',Ead1_IMensageiro::AVISO);
			}
			//Encapsulando e Organizando Dados
			$dadosPessoaImportacao = Ead1_TO_Dinamico::encapsularTo($dadosPessoaImportacao, new MasterTO());
			$arrOrganizador = array();
			foreach ($dadosPessoaImportacao as $importacao){
				$importacao->setId_usuarioimportado($importacao->getId_usuarioimportadoexistente());
				unset($importacao->id_usuarioimportadoexistente);
				$piTO = new PessoaImportacaoTO();
				$piTO->mergeTO($piTO, $importacao,false,false);
				$arrOrganizador[$importacao->getNu_codpessoaorigem()]['dados'][$importacao->getId_pessoaimportacao()] = $piTO;
			}
			$relatorio = array();
			//Sincronizando dados e Gerando Relatorio
			foreach ($arrOrganizador as $dados){
				foreach($dados['dados'] as $pessoaImportacaoTO){
					//Preparando Log
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'] = null;
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['validacao'] = null;
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'] = null;
					try{
						$this->_processoSincronizarPessoa($pessoaImportacaoTO);
						Zend_Debug::dump($this->_log);
					}catch (Zend_Exception $e){
						Zend_Debug::dump($this->_log);
						continue;
					}
				}
			}
			exit;
		}catch (Zend_Exception $e){
			
			
		}
	}
	
	/**
	 * Processo que cadastra usuario e pessoa e depois edita a pessoaimportacao
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaTO $pessoaTO
	 * @throws Zend_Db_Exception
	 * @throws Zend_Validate_Exception
	 */
	protected function _processoSincronizarPessoa(PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$this->dao->beginTransaction();
			//TO's para referencia
			$usuarioTO = new UsuarioTO();
			$pessoaTO = new PessoaTO();
			
			$pessoaTO->setBl_ativo(true);
			$usuarioTO->setBl_ativo(true);
			$usuarioTO->setId_usuario($pessoaImportacaoTO->getId_usuarioimportado());
			$usuarioTO->setId_registropessoa(4); //Fixo
			$usuarioTO->setSt_cpf($pessoaImportacaoTO->getNu_cpf());
			$usuarioTO->setSt_nomecompleto($pessoaImportacaoTO->getSt_nomecompleto());
			if(!$usuarioTO->getId_usuario()){
				//Salvando Usuario
				try{
					self::returnClassInstance(new PessoaRO())->cadastrarUsuario($usuarioTO);
				}catch (Zend_Exception $e){
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
							"Erro ao Cadastrar Usuário! Erro: ".$e->getMessage();
					THROW $e;
				}
			}else{
				$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
						'O Processo não Cadastrou um Novo Usuário Pois Já Existe um Usuário Cadastrado id_usuario: '.$usuarioTO->getId_usuario();
			}
			$pessoaTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$pessoaTO->setId_usuario($usuarioTO->getId_usuario());
			$pessoaImportacaoTO->setId_usuarioimportado($usuarioTO->getId_usuario());
			if(!self::returnClassInstance(new PessoaORM())->consulta($pessoaTO,true,true)){
				$municipioPessoa = $this->_retornaMunicipioPeloNome($pessoaImportacaoTO->getSt_municipionascimento());
				if(!$municipioPessoa){
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
							'Municipio de Nascimento não encontrado!';
				}else{
					$pessoaTO->setId_municipio($municipioPessoa->getId_municipio());
				}
				$ufPessoa = $this->_retornaUfPelaUF($pessoaImportacaoTO->getSg_ufnascimento());
				if(!$ufPessoa){
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
							'UF de Nascimento não encontrado!';
				}else{
					$pessoaTO->setSg_uf($ufPessoa->getSg_uf());
				}
				$paisPessoa = $this->_retornaPaisPeloNome($pessoaImportacaoTO->getSt_paisnascimento());
				if(!$paisPessoa){
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
							'Pais de Nascimento não encontrado!';
				}else{
					$pessoaTO->setId_pais($paisPessoa->getId_pais());
				}
				$estadoCivilPessoa = $this->_retornaEstadoCivilImportacao($pessoaImportacaoTO->getNu_estadocivilorigem());
				if(!$estadoCivilPessoa){
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
							'Estado Civil não encontrado!';
				}else{
					$pessoaTO->setId_estadocivil($estadoCivilPessoa->getId_estadocivil());
				}
				$pessoaTO->setSt_nomeexibicao($pessoaImportacaoTO->getSt_nomecompleto());
				$pessoaTO->setSt_nomemae($pessoaImportacaoTO->getSt_nomemae());
				$pessoaTO->setSt_nomepai($pessoaImportacaoTO->getSt_nomepai());
				$pessoaTO->setSt_passaporte($pessoaImportacaoTO->getSt_passaporte());
				$pessoaTO->setSt_sexo($pessoaImportacaoTO->getSt_sexo());
				$pessoaTO->setDt_nascimento($pessoaImportacaoTO->getDt_nascimento());
				//@todo retirar
//				$pessoaTO->setId_estadocivil(1);
				try{
					self::returnClassInstance(new PessoaRO())->cadastrarPessoa($usuarioTO,$pessoaTO);
				}catch (Zend_Exception $e){
					$this->dao->rollBack();
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
							"Erro ao Cadastrar Pessoa. Erro: ".$e->getMessage();
					THROW $e;
				}
				try{
					$pessoaImportacaoTO->setId_usuarioimportado($usuarioTO->getId_usuario());
					$this->editarPessoaImportacao($pessoaImportacaoTO);
				}catch (Zend_Db_Exception $e){
					$this->dao->rollBack();
					$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
							"Erro ao Editar Importação da Pessoa. Erro: ".$e->getMessage();
					THROW $e;
				}
				$this->dao->commit();
				Zend_Debug::dump($this->_log);
				exit;
				//Processos de salvar dados da Pessoa
				$this->_processoCadastrarDocumentoIdentidade($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarDocumentoCertidaoNascimento($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarDocumentoTituloEleitor($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarEmail($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarTelefone($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarEndereco($usuarioTO,$pessoaImportacaoTO);
				$this->_processoCadastrarDadosBiomedicos($usuarioTO,$pessoaImportacaoTO);
			}else{
				$this->dao->commit();
				$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
						'O Processo Importou a Pessoa e seus Dados pois Já Existe uma Pessoa Cadastrada:  id_usuario: '.$usuarioTO->getId_usuario().
						' e id_entidade: '.$pessoaTO->getId_entidade();
			}
		}catch (Zend_Exception $e){
			THROW $e;
		}
	}
	
	/**
	 * Processo que cadastra o documento de identidade da pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarDocumentoIdentidade(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$documentoIdentidadeTO = new DocumentoIdentidadeTO();
			$documentoIdentidadeTO->setDt_dataexpedicao($pessoaImportacaoTO->getDt_dataexpedicaorg());
			$documentoIdentidadeTO->setSt_orgaoexpeditor($pessoaImportacaoTO->getSt_orgaoexpeditorrg());
			$documentoIdentidadeTO->setSt_rg($pessoaImportacaoTO->getSt_rg());
			$documentoIdentidadeTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$documentoIdentidadeTO->setId_usuario($usuarioTO->getId_usuario());
			self::returnClassInstance(new PessoaRO())->cadastrarDocumentoIdentidade($usuarioTO,$documentoIdentidadeTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos dados de Identidade. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Documento de Identidade(RG). Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra o documento de certidão de nascimento da pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarDocumentoCertidaoNascimento(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$documentoCertidaoDeNascimentoTO = new DocumentoCertidaoDeNascimentoTO();
			$documentoCertidaoDeNascimentoTO->setDt_dataexpedicao($pessoaImportacaoTO->getDt_dataexpedicaocertidao());
			$documentoCertidaoDeNascimentoTO->setSt_numero($pessoaImportacaoTO->getSt_numerocertidaonascimento());
			$documentoCertidaoDeNascimentoTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$documentoCertidaoDeNascimentoTO->setId_usuario($usuarioTO->getId_usuario());
			$documentoCertidaoDeNascimentoTO->setSt_descricaocartorio($pessoaImportacaoTO->getSt_descricaocartorio());
			$documentoCertidaoDeNascimentoTO->setSt_folhas($pessoaImportacaoTO->getSt_folhacertidaonascimento());
			$documentoCertidaoDeNascimentoTO->setSt_livro($pessoaImportacaoTO->getSt_livrocertidaonascimento());
			self::returnClassInstance(new PessoaRO())->cadastrarDocumentoCertidaoNascimento($usuarioTO,$documentoCertidaoDeNascimentoTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos Dados de Certidão de Nascimento. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Documento de Certidão de Nascimento. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra o documento de reservista da pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarDocumentoReservista(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$documentoDeReservistaTO = new DocumentoDeReservistaTO();
			//@todo Nao existe esta informação
//			$documentoDeReservistaTO->setDt_datareservista($pessoaImportacaoTO->getdt)
			$documentoDeReservistaTO->setSt_numero($pessoaImportacaoTO->getNu_numeroreservista());
			$documentoDeReservistaTO->setSt_serie($pessoaImportacaoTO->getSt_seriereservista());
			$ufReservista = $this->_retornaUfPelaUF($pessoaImportacaoTO->getSg_ufreservista());
			if(!$ufReservista){
				THROW new Zend_Validate_Exception('A UF do Documento de Reservista não foi Encontrada!');
			}else{
				$documentoDeReservistaTO->setSg_uf($ufReservista->getSg_uf());
			}
			$documentoDeReservistaTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$documentoDeReservistaTO->setId_usuario($usuarioTO->getId_usuario());
			$municipioReservista = $this->_retornaMunicipioPeloNome($pessoaImportacaoTO->getSt_municipioreservista());
			$documentoDeReservistaTO->setId_municipio($municipioReservista->getId_municipio());
			self::returnClassInstance(new PessoaRO())->cadastrarDocumentoReservista($usuarioTO,$documentoDeReservistaTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos dados de Reservista. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Documento de Reservista. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra o documento de reservista da pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarDocumentoTituloEleitor(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$documentoTituloDeEleitorTO = new DocumentoTituloDeEleitorTO();
			$documentoTituloDeEleitorTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$documentoTituloDeEleitorTO->setId_usuario($usuarioTO->getId_usuario());
			$documentoTituloDeEleitorTO->setDt_dataexpedicao($pessoaImportacaoTO->getDt_dataexpedicaoeleitor());
			$documentoTituloDeEleitorTO->setSt_numero($pessoaImportacaoTO->getSt_numeroeleitor());
			$documentoTituloDeEleitorTO->setSt_zona($pessoaImportacaoTO->getNu_zonaeleitor());
			$documentoTituloDeEleitorTO->setSt_area($pessoaImportacaoTO->getSt_areaeleitor());
			self::returnClassInstance(new PessoaRO())->cadastrarDocumentoTituloEleitor($usuarioTO,$documentoTituloDeEleitorTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos dados de Titulo de Eleitor. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Documento de Reservista. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra um email para pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarEmail(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$contatosEmailTO = new ContatosEmailTO();
			$contatosEmailPessoaPerfilTO = new ContatosEmailPessoaPerfilTO();
			$contatosEmailTO->setSt_email($pessoaImportacaoTO->getSt_email());
			$contatosEmailPessoaPerfilTO->setBl_ativo(true);
			$contatosEmailPessoaPerfilTO->setBl_padrao(true);
			$contatosEmailPessoaPerfilTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$contatosEmailPessoaPerfilTO->setId_usuario($usuarioTO->getId_usuario());
			self::returnClassInstance(new PessoaRO())->cadastrarEmail($usuarioTO,$contatosEmailTO,$contatosEmailPessoaPerfilTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos Dados de Email. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Email da Pessoa. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra um telefone para pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarTelefone(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$contatosTelefoneTO = new ContatosTelefoneTO();
			$contatosTelefonePessoaTO = new ContatosTelefonePessoaTO();
			$contatosTelefoneTO->setId_tipotelefone(1); //Fixo
			$arTelefone = $this->trataTelefone($pessoaImportacaoTO->getSt_telefone());
			$contatosTelefoneTO->setNu_ddi($arTelefone['ddi']);
			$contatosTelefoneTO->setNu_ddd($arTelefone['ddd']);
			$contatosTelefoneTO->setNu_telefone($arTelefone['numero']);
			$contatosTelefonePessoaTO->setBl_padrao(true);
			$contatosTelefonePessoaTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$contatosTelefonePessoaTO->setId_usuario($usuarioTO->getId_usuario());
			self::returnClassInstance(new PessoaRO())->cadastrarTelefone($usuarioTO,$contatosTelefoneTO,$contatosTelefonePessoaTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos Dados de Telefone. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Telefone da Pessoa. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra um endereço para pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarEndereco(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$enderecoTO = new EnderecoTO();
			$pessoaEnderecoTO = new PessoaEnderecoTO();
			$enderecoTO->setBl_ativo(true);
			//@todo Não tem numero de endereço
//			$enderecoTO->setSt_numero()
			$enderecoTO->setSt_bairro($pessoaImportacaoTO->getSt_bairroendereco());
			$enderecoTO->setSt_cep($pessoaImportacaoTO->getSt_cependereco());
			//@todo Não tem o nome da cidade
//			$enderecoTO->setSt_cidade();
			$enderecoTO->setSt_bairro($pessoaImportacaoTO->getSt_bairroendereco());
			$enderecoTO->setSt_complemento($pessoaImportacaoTO->getSt_complementoendereco());
			$enderecoTO->setSt_endereco($pessoaImportacaoTO->getSt_endereco());
			$uf = $this->_retornaUfPelaUF($pessoaImportacaoTO->getSg_ufendereco());
			if(!$uf){
				THROW new Zend_Validate_Exception('A UF do Endereço não foi Encontrada!');
			}else{
				$enderecoTO->setSg_uf($uf->getSg_uf());
			}
			$municipio = $this->_retornaMunicipioPeloNome($pessoaImportacaoTO->getSt_municipioendereco());
			if(!$municipio){
				THROW new Zend_Validate_Exception('O Municipio do Endereço não foi Encontrado!');
			}else{
				$enderecoTO->setId_municipio($municipio->getId_municipio());
			}
			$pais = $this->_retornaPaisPeloNome($pessoaImportacaoTO->getSt_paisendereco());
			if(!$pais){
				THROW new Zend_Validate_Exception('O Pais do Endereço não foi Encontrado!');
			}else{
				$enderecoTO->setId_pais($pais->getId_pais());
			}
			$enderecoTO->setId_tipoendereco(1); //Fixo
			//@todo Não tem o Estado Provincia
//			$enderecoTO->setSt_estadoprovincia()
			$pessoaEnderecoTO->setBl_padrao(true);
			$pessoaEnderecoTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$pessoaEnderecoTO->setId_usuario($usuarioTO->getId_usuario());
			self::returnClassInstance(new PessoaRO())->cadastrarEndereco($usuarioTO,$enderecoTO,$pessoaEnderecoTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos Dados de Endereço. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Endereço da Pessoa. Erro: '.$e->getMessage();
		}
	}
	
	/**
	 * Processo que cadastra os dados biomedicos da pessoa
	 * @param UsuarioTO $usuarioTO
	 * @param PessoaImportacaoTO $pessoaImportacaoTO
	 */
	protected function _processoCadastrarDadosBiomedicos(UsuarioTO $usuarioTO, PessoaImportacaoTO $pessoaImportacaoTO){
		try{
			$dadosBiomedicosTO = new DadosBiomedicosTO();
			$dadosBiomedicosTO->setId_entidade($pessoaImportacaoTO->getId_entidadeimportado());
			$dadosBiomedicosTO->setId_usuario($usuarioTO->getId_usuario());
			$dadosBiomedicosTO->setNu_glicemia($pessoaImportacaoTO->getSt_glicemia());
			$dadosBiomedicosTO->setSt_alergias($pessoaImportacaoTO->getSt_alergias());
			//@todo Não possui deficiencias
//			$dadosBiomedicosTO->setSt_deficiencias()
			$dadosBiomedicosTO->setSt_necessidadesespeciais($pessoaImportacaoTO->getSt_necessidadesespeciais());
			$etnia = $this->_retornaEtniaImportacao($pessoaImportacaoTO->getNu_codetnia());
			if(!$etnia){
				THROW new Zend_Validate_Exception('Etnia não Encontrada!');
			}else{
				$dadosBiomedicosTO->setId_etnia($etnia->getId_etnia());
			}
			//@todo não tem tabela de vinculo de importacao
//			$dadosBiomedicosTO->setId_tiposanguineo();
			self::returnClassInstance(new PessoaRO())->cadastrarEndereco($usuarioTO,$dadosBiomedicosTO);
		}catch (Zend_Validate_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['aviso'][] = 
					'Erro de Validação nos Dados de Endereço. Erro: '.$e->getMessage();
		}catch (Zend_Db_Exception $e){
			$this->_log['usuarios'][$pessoaImportacaoTO->getNu_codpessoaorigem()]['importacoes'][$pessoaImportacaoTO->getId_pessoaimportacao()]['erro'][] = 
					'Erro ao Cadastrar Endereço da Pessoa. Erro: '.$e->getMessage();
		}
	}
	
}
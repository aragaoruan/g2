<?php

/**
 * Classe com regras de negocio para interacoes de Pesquisa
 * @author Eduardo Romao - ejushiro@gmail.com
 * @since 21/09/2010
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @update Kayo Silva <kayo.silva@unyleya.com.br> - 2013-04-11
 * @package models
 * @subpackage bo
 */

use Ead1\Filtro;
use Ead1\Pesquisa;

//use G2\Negocio\Pessoa;

class PesquisarBO extends Ead1_BO
{

    public $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que retorna os campos de pesquisa e colunas da grid
     * @param Pesquisa $pTO
     * @return bool|Pesquisa
     * @throws Zend_Exception
     */
    public function retornarCamposPesquisa(Pesquisa $pTO)
    {
        switch ($pTO->getTipo()) {
            case 'GradeHoraria':
                $pTO->setId_funcionalidade(646);
                $pTO->setMetodo('pesquisarGrade');
                $pTO->setTo_envia('PesquisarGradeTO');
                $filtros[] = $this->setFiltro('Nome', 'st_nomegradehoraria', 'CheckBox');
                $filtros[] = $this->setFiltro('Data Início', 'dt_iniciogradehoraria', 'DateField');
                $filtros[] = $this->setFiltro('Data Término', 'dt_fimgradehoraria', 'DateField');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_gradehoraria')));
                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos("ID", "id_gradehoraria");
                $arCampos[] = $this->setCampos("Nome", "st_nomegradehoraria");
                $arCampos[] = $this->setCampos("Período", "st_periodo");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarPessoa':
                $pTO->setId_funcionalidade(8);
                $pTO->setMetodo('pesquisarPessoa');
                $pTO->setTo_envia('PesquisarPessoaTO');
                $filtros[] = $this->setFiltro('E-mail', 'st_email', 'CheckBox');
                $filtros[] = $this->setFiltro('Nome', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('CPF', 'st_cpf', 'CheckBox');
                $filtros[] = $this->setFiltro('Código', 'id_usuario', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_pessoa')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_usuario");
                $arCampos[] = $this->setCampos("Nome", "st_nomecompleto");
                $arCampos[] = $this->setCampos("E-mail", "st_email");
                $arCampos[] = $this->setCampos("CPF", "st_cpf");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Data Nascimento", "dt_nascimento", false);
                $arCampos[] = $this->setCampos("Sexo", "st_sexo", false);

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastroRapidoPessoa':
                /*
                 * Cadastro Rápido de Pessoa
                 * Devido a pesquisa ser a mesma de CadastrarPessoa foi necessário chamar esse mesmo método
                 * chamando o laço do case CadastrarPessoa e setando o id da funcionalidade para o correspondente
                 */
                $pTO->setTipo('CadastrarPessoa');
                $pto = $this->retornarCamposPesquisa($pTO);
                $pto->setId_funcionalidade(573);
                return $pTO;
                break;
            case 'Entidade':
                $pTO->setId_funcionalidade(286);
                $pTO->setMetodo('pesquisarOrganizacao');
                $pTO->setTo_envia('PesquisarOrganizacaoTO');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_entidade')));
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_entidade");
                $arCampos[] = $this->setCampos("Nome", "st_nomeentidade");
                $arCampos[] = $this->setCampos("Razão Social", "st_razaosocial");
                $arCampos[] = $this->setCampos("Número CNPJ", "st_cnpj");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarInstituicao':
                $pTO->setMetodo('pesquisarOrganizacao');
                $pTO->setTo_envia('PesquisarOrganizacaoTO');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_entidade')));
                $filtros[] = $this->setFiltro('Instituição: ', 'bl_grupo', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_entidade");
                $arCampos[] = $this->setCampos("Nome", "st_nomeentidade");
                $arCampos[] = $this->setCampos("Razão Social", "st_razaosocial");
                $arCampos[] = $this->setCampos("Número CNPJ", "st_cnpj");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarPolo':
                $pTO->setMetodo('pesquisarOrganizacao');
                $pTO->setTo_envia('PesquisarOrganizacaoTO');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_entidade')));
                $filtros[] = $this->setFiltro('Pólo: ', 'bl_grupo', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_entidade");
                $arCampos[] = $this->setCampos("Nome", "st_nomeentidade");
                $arCampos[] = $this->setCampos("Razão Social", "st_razaosocial");
                $arCampos[] = $this->setCampos("Número CNPJ", "st_cnpj");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarNucleo':
                $pTO->setId_funcionalidade(421);
                $pTO->setMetodo('pesquisarOrganizacao');
                $pTO->setTo_envia('PesquisarOrganizacaoTO');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_entidade')));
                $filtros[] = $this->setFiltro('Núcleo: ', 'bl_grupo', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_entidade");
                $arCampos[] = $this->setCampos("Nome", "st_nomeentidade");
                $arCampos[] = $this->setCampos("Razão Social", "st_razaosocial");
                $arCampos[] = $this->setCampos("Número CNPJ", "st_cnpj");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'Telemarketing':
                $pTO->setMetodo('pesquisarTelemarketing');
                $pTO->setTo_envia('PesquisarTelemarketingTO');
                $sTO = array('st_tabela' => 'tb_entidade');
                return $pTO;
                break;
            case 'CadastrarRegraContrato';
                $pTO->setId_funcionalidade(210);
                $pTO->setMetodo('pesquisarRegraContrato');
                $pTO->setTo_envia('PesquisaRegraContratoTO');
                $filtros[] = $this->setFiltro('Regra Contrato:', 'st_contratoregra', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_contratoregra");
                $arCampos[] = $this->setCampos('Regra Contrato', "st_contratoregra");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'GeradorRelatorio';
                $pTO->setId_funcionalidade(360);
                $pTO->setMetodo('pesquisarRelatorio');
                $pTO->setTo_envia('PesquisaRelatorioTO');
                $filtros[] = $this->setFiltro('Relatório: ', 'st_relatorio', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID: ", "id_relatorio");
                $arCampos[] = $this->setCampos("Relatório: ", "st_relatorio");
                $arCampos[] = $this->setCampos("Origem: ", "st_origem");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarProtocolo';
                $pTO->setId_funcionalidade(370);
                $pTO->setMetodo('pesquisarProtocolo');
                $pTO->setTo_envia('PesquisarProtocoloTO');
                $filtros[] = $this->setFiltro('Data de Cadastro: ', 'dt_cadastro', 'DateField');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('Protocolo', "id_protocolo");
                $arCampos[] = $this->setCampos('Usuário', "st_nomecompleto");
                $arCampos[] = $this->setCampos('Data de Cadastro', "dt_cadastro");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarVenda';
                $pTO->setId_funcionalidade(196);
                $pTO->setMetodo('pesquisarVenda');
                $pTO->setTo_envia('PesquisarVendaTO');
                $sTO = array('st_tabela' => 'tb_venda');
                $eTO = array('st_tabela' => 'tb_venda');
                $filtros[] = $this->setFiltro('E-mail: ', 'st_email', 'CheckBox');
                $filtros[] = $this->setFiltro('Nome: ', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('CPF: ', 'st_cpf', 'CheckBox');
                $filtros[] = $this->setFiltro('Venda: ', 'id_venda', 'TextInput');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao($sTO));
                $filtros[] = $this->setFiltro('Evolução: ', 'id_evolucao', 'DropDownList', $this->retornaEvolucao($eTO));
                $filtros[] = $this->setFiltro('Data do Início: ', 'dt_inicio', "DateField");
                $filtros[] = $this->setFiltro('Data do Fim: ', 'dt_fim', "DateField");
                $filtros[] = $this->setFiltro('Possui Contrato: ', 'bl_contrato', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_venda");
                $arCampos[] = $this->setCampos("Usuário", "st_nomecompleto");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Evolução", "st_evolucao");
                $arCampos[] = $this->setCampos("Valor Bruto", "nu_valorbruto");
                $arCampos[] = $this->setCampos("Data", "dt_cadastro");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarVendaGraduacao';
                $pTO->setId_funcionalidade(735);
                $pTO->setMetodo('pesquisarVendaGraduacao');
                $pTO->setTo_envia('PesquisarVendaTO');
                $sTO = array('st_tabela' => 'tb_venda');
                $eTO = array('st_tabela' => 'tb_venda');
                $filtros[] = $this->setFiltro('Nome: ', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('Venda: ', 'id_venda', 'TextInput');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao($sTO));
                $filtros[] = $this->setFiltro('Evolução: ', 'id_evolucao', 'DropDownList', $this->retornaEvolucao($eTO));
                $filtros[] = $this->setFiltro('Data do Início: ', 'dt_inicio', "DateField");
                $filtros[] = $this->setFiltro('Data do Fim: ', 'dt_fim', "DateField");
                $filtros[] = $this->setFiltro('Possui Contrato: ', 'bl_contrato', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_venda");
                $arCampos[] = $this->setCampos("Usuário", "st_nomecompleto");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Evolução", "st_evolucao");
                $arCampos[] = $this->setCampos("Valor Bruto", "nu_valorbruto");
                $arCampos[] = $this->setCampos("Data", "dt_cadastro");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarTurma';
                $pTO->setMetodo('PesquisarTurma');
                $pTO->setTo_envia('PesquisarTurmaTO');
                $pTO->setId_funcionalidade(409);
                $sTO = array('st_tabela' => 'tb_turma');
                $filtros[] = $this->setFiltro('Título: ', 'st_turma', "CheckBox");
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', "DropDownList", $this->retornaSituacao($sTO));
                $filtros[] = $this->setFiltro('Data de Abertura: ', 'dt_inicio', "DateField");
                $filtros[] = $this->setFiltro('Data de Encerramento: ', 'dt_fim', "DateField");
                $filtros[] = $this->setFiltro('Data de Início de Inscrição: ', 'dt_inicioinscricao', "DateField");
                $filtros[] = $this->setFiltro('Data de Término de Inscrição: ', 'dt_fiminscricao', "DateField");
                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos("ID", "id_turma");
                $arCampos[] = $this->setCampos("Título", "st_turma");
                $arCampos[] = $this->setCampos("Título de Exibição", "st_tituloexibicao");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Início Inscrição", "dt_inicioinscricao");
                $arCampos[] = $this->setCampos("Término Inscrição", "dt_fiminscricao");
                $arCampos[] = $this->setCampos("Abertura", "dt_inicio");
                $arCampos[] = $this->setCampos("Encerramento", "dt_fim");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarPerfil';
                $pTO->setId_funcionalidade(56);
                $pTO->setMetodo('pesquisarPerfil');
                $pTO->setTo_envia('ItemDeMaterialTO');
                $arCampos[] = $this->setCampos("ID", "id_itemdematerial");
                $arCampos[] = $this->setCampos("Item de Material", "st_itemdematerial");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarDisciplina':
                $pTO->setId_funcionalidade(24); //94 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarDisciplina');
                $pTO->setTo_envia('PesquisarDisciplinaTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome', 'st_disciplina', 'CheckBox');
                $filtros[] = $this->setFiltro('Área Conhecimento', 'st_areaconhecimento', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação:', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_disciplina')));
                $filtros[] = $this->setFiltro('Tipo de ' . $label->getLabel(24) . ': ', 'id_tipodisciplina', 'DropDownList', $this->retornaTipoDisciplina());
                $filtros[] = $this->setFiltro('Nível de Ensino: ', 'id_nivelensino', 'DropDownList', $this->retornaNivelEnsino(array(), true));
                $filtros[] = $this->setFiltro('Grupo: ', 'id_grupodisciplina', 'DropDownList', $this->retornaGrupoDisciplina(array(), true));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_disciplina");
                $arCampos[] = $this->setCampos($label->getLabel(24), "st_disciplina");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Tipo de " . $label->getLabel(24), "st_tipodisciplina");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarAreaConhecimento':
                $pTO->setId_funcionalidade(20); // 90 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarArea');
                $pTO->setTo_envia('PesquisarAreaTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome', 'st_areaconhecimento', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_areaconhecimento')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_areaconhecimento");
                $arCampos[] = $this->setCampos($label->getLabel(20), "st_areaconhecimento");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarSalaDeAula':
                $pTO->setId_funcionalidade(23); //93 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarSalaDeAula');
                $pTO->setTo_envia('PesquisarSalaDeAulaTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro($label->getLabel(24), 'st_disciplina', 'CheckBox');
                $filtros[] = $this->setFiltro('Sala de Aula', 'st_saladeaula', 'CheckBox');
                $filtros[] = $this->setFiltro('Sistema', 'st_sistema', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_saladeaula')));
                $filtros[] = $this->setFiltro('Professor', 'st_professor', 'CheckBox');
                $filtros[] = $this->setFiltro('Tipo de ' . $label->getLabel(23) . ': ', 'id_tiposaladeaula', 'DropDownList', $this->retornarTipoSalaDeAula());
                $filtros[] = $this->setFiltro('Modalidade de ' . $label->getLabel(23) . ': ', 'id_modalidadesaladeaula', 'DropDownList', $this->retornarModalidadeSalaDeAula());
                $filtros[] = $this->setFiltro('Data de Abertura Início:', 'dt_abertura_inicio', 'DateField');
                $filtros[] = $this->setFiltro('Data de Abertura Fim:', 'dt_abertura_fim', 'DateField');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_saladeaula");
                $arCampos[] = $this->setCampos($label->getLabel(23), "st_saladeaula");
                $arCampos[] = $this->setCampos($label->getLabel(24), "st_disciplina");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Modalidade", "st_modalidadesaladeaula", false);
                $arCampos[] = $this->setCampos("Tipo", "st_tiposaladeaula", false);
                $arCampos[] = $this->setCampos("Abertura", "dt_abertura");
                $arCampos[] = $this->setCampos("Encerramento", "dt_encerramento");
                $arCampos[] = $this->setCampos("Início da Inscrição", "dt_inicioinscricao");
                $arCampos[] = $this->setCampos("Término da Inscrição", "dt_fiminscricao");
                $arCampos[] = $this->setCampos("Professor", "st_nomeprofessor");
                $arCampos[] = $this->setCampos("Encerramento após extensão", "dt_comextensao");
                $arCampos[] = $this->setCampos("Sistema", "st_sistema");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarAproveitamento':
                $pTO->setId_funcionalidade(241); //93 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarAproveitamento');
                $pTO->setTo_envia('PesquisarAproveitamentoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Aluno: ', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro($label->getLabel(24) . ': ', 'st_disciplinaoriginal', 'CheckBox');
                $filtros[] = $this->setFiltro($label->getLabel(19) . ': ', 'st_tituloexibicao', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("Aproveitamento", "id_aproveitamentodisciplina");
                $arCampos[] = $this->setCampos("Aluno: ", "st_nomecompleto");
                $arCampos[] = $this->setCampos($label->getLabel(24), "st_disciplinaoriginal");
                $arCampos[] = $this->setCampos($label->getLabel(19), "st_tituloexibicao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarCampanha':

                $pTO->setId_funcionalidade(308); //93 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarCampanhaComercial');
                $pTO->setTo_envia('PesquisarCampanhaComercialTO');
                $campanhaBO = new CampanhaComercialBO();
                $tc = new TipoCampanhaTO();
                $sTO = array('st_tabela' => 'tb_campanhacomercial');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome da Campanha ', 'st_campanhacomercial', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao($sTO));
                $filtros[] = $this->setFiltro('Tipo de Campanha : ', 'id_tipocampanha', 'DropDownList', $campanhaBO->retornarTipoCampanha($tc));


                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID ", "id_campanhacomercial");
                $arCampos[] = $this->setCampos("Campanha ", "st_campanhacomercial");
                $arCampos[] = $this->setCampos("Tipo de Campanha", "st_tipocampanha");
                $arCampos[] = $this->setCampos("Finalidade ", "st_finalidadecampanha");
                $arCampos[] = $this->setCampos('Situação', "st_situacao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarConjuntoAvaliacao':
                $pTO->setId_funcionalidade(249); //93 tira a funcionalidade excluir
                $pTO->setMetodo('pesquisarConjuntoAvaliacao');
                $pTO->setTo_envia('PesquisarAvaliacaoConjuntoTO');
                $filtros[] = $this->setFiltro('Conjunto de Avaliação: ', 'st_avaliacaoconjunto', 'CheckBox');
                $filtros[] = $this->setFiltro('Tipo de Prova: ', 'id_tipoprova', 'DropDownList', $this->retornarTipoProva(array()));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID: ", "id_avaliacaoconjunto");
                $arCampos[] = $this->setCampos("Conjunto de Avaliação: ", "st_avaliacaoconjunto");
                $arCampos[] = $this->setCampos("Tipo de prova", "st_tipoprova");
                $arCampos[] = $this->setCampos('Situação', "st_situacao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarProjetoPedagogico':
                $pTO->setId_funcionalidade(19);
                $pTO->setMetodo('pesquisarProjetoPedagogico');
                $pTO->setTo_envia('PesquisarProjetoPedagogicoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Descrição: ', 'st_descricao', 'CheckBox');
                $filtros[] = $this->setFiltro($label->getLabel(19) . ': ', 'st_projetopedagogico', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_projetopedagogico')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_projetopedagogico");
                $arCampos[] = $this->setCampos($label->getLabel(19), "st_projetopedagogico");
                $arCampos[] = $this->setCampos("Descrição", "st_descricao");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarProjetoPedagogicoBasico':
                $pTO->setId_funcionalidade(586);
                $pTO->setMetodo('pesquisarProjetoPedagogico');
                $pTO->setTo_envia('PesquisarProjetoPedagogicoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Descrição: ', 'st_descricao', 'CheckBox');
                $filtros[] = $this->setFiltro('Projeto Pedagógico: ', 'st_projetopedagogico', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_projetopedagogico')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_projetopedagogico");
                $arCampos[] = $this->setCampos('Projeto Pedagógico', "st_projetopedagogico");
                $arCampos[] = $this->setCampos("Descrição", "st_descricao");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarMatricula':
                $pTO->setId_funcionalidade(187);
                $pTO->setMetodo('pesquisarContrato');
                $pTO->setTo_envia('PesquisarContratoTO');
                $filtros[] = $this->setFiltro('Nome: ', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_contrato')));
                $filtros[] = $this->setFiltro('Evolução: ', 'id_evolucao', 'DropDownList', $this->retornaEvolucao(array('st_tabela' => 'tb_contrato')));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos("Código", "id_contrato");
                $arCampos[] = $this->setCampos("Nome Aluno", "st_nomecompleto");
                $arCampos[] = $this->setCampos("Evolução", "st_evolucao");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarContratoAfiliado':
                $pTO->setId_funcionalidade(515);
                $pTO->setMetodo('pesquisarContratoAfiliado');
                $pTO->setTo_envia('PesquisarContratoAfiliadoTO');
                $filtros[] = $this->setFiltro('Código: ', 'id_contratoafiliado', 'CheckBox');
                $filtros[] = $this->setFiltro('Afiliado: ', 'st_contratoafiliado', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_contratoafiliado')));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos("Código", "id_contratoafiliado");
                $arCampos[] = $this->setCampos("Afiliado", "st_contratoafiliado");
                $arCampos[] = $this->setCampos("Responsável", "st_nomecompletoresponsavel");
                $arCampos[] = $this->setCampos("Contrato", "st_nomeentidadeafiliada");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarProduto';
                $pTO->setId_funcionalidade(214);
                $pTO->setMetodo('pesquisarProduto');
                $pTO->setTo_envia('PesquisarProdutoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome: ', 'st_produto', 'CheckBox');
                $filtros[] = $this->setFiltro('Tipo de ' . $label->getLabel(214), 'id_tipoproduto', 'DropDownList', $this->retornaTipoProduto(array()));
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_produto')));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('Código: ', 'id_produto');
                $arCampos[] = $this->setCampos('Nome: ', 'st_produto');
                $arCampos[] = $this->setCampos('Tipo de ' . $label->getLabel(214) . ': ', 'st_tipoproduto');
                $arCampos[] = $this->setCampos('Situação: ', 'st_situacao');
                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarMotivo';
                $pTO->setId_funcionalidade(665);
                $pTO->setMetodo('pesquisarMotivo');
                $pTO->setTo_envia('PesquisarMotivoTO');
                $filtros[] = $this->setFiltro('Nome: ', 'st_motivo', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_motivo')));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('Código: ', 'id_motivo');
                $arCampos[] = $this->setCampos('Nome: ', 'st_motivo');
                $arCampos[] = $this->setCampos('Situação: ', 'st_situacao');
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'CadastrarItemMaterial';
                $pTO->setId_funcionalidade(106); //item de material
                $pTO->setMetodo('pesquisarItemMaterial');
                $pTO->setTo_envia('PesquisarItemMaterialTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome: ', 'st_itemdematerial', 'CheckBox');

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID: ', 'id_itemdematerial');
                $arCampos[] = $this->setCampos('Nome: ', 'st_itemdematerial');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarFormaDePagamento';
                $pTO->setId_funcionalidade(54);
                $pTO->setMetodo('pesquisarFormaDePagamento');
                $pTO->setTo_envia('PesquisaFormaPagamentoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome: ', 'st_formapagamento', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_formapagamento')));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID: ', 'id_formapagamento');
                $arCampos[] = $this->setCampos('Nome: ', 'st_formapagamento');
                $arCampos[] = $this->setCampos('Situação: ', 'st_situacao');
                $arCampos[] = $this->setCampos('Aplicação: ', 'st_formapagamentoaplicacao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarAgendamentoAvaliacao';
                $pTO->setId_funcionalidade(270);
                $pTO->setMetodo('pesquisarAgendamentoAvaliacao');
                $pTO->setTo_envia('PesquisaAvaliacaoAgendamentoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Avaliação: ', 'st_avaliacao', 'CheckBox');
                $filtros[] = $this->setFiltro('Data de Início: ', 'dt_agendamentoinicio', 'DateField');
                $filtros[] = $this->setFiltro('Data de Término: ', 'dt_agendamentofim', 'DateField');
                $filtros[] = $this->setFiltro('Tipo: ', 'id_tipoprova', 'DropDownList', $this->retornarTipoProva(array()));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID: ', 'id_avaliacaoagendamento');
                $arCampos[] = $this->setCampos('Aluno: ', 'st_nomecompleto');
                $arCampos[] = $this->setCampos('Avaliação: ', 'st_avaliacao');
                $arCampos[] = $this->setCampos('Tipo: ', 'st_tipoprova');
                $arCampos[] = $this->setCampos('Data do Agendamento: ', 'dt_agendamento');
                $arCampos[] = $this->setCampos('Situação: ', 'st_situacao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarTextos';
                $pTO->setId_funcionalidade(233);
                $pTO->setMetodo('pesquisarTextosSistema');
                $pTO->setTo_envia('PesquisarTextoSistemaTO');
                $filtros[] = $this->setFiltro('Título: ', 'st_textosistema', 'CheckBox');
                $filtros[] = $this->setFiltro('Exibição: ', 'id_textoexibicao', 'DropDownList', $this->retornarTextoExibicao(array()));

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID: ', 'id_textosistema');
                $arCampos[] = $this->setCampos('Título: ', 'st_textosistema');
                $arCampos[] = $this->setCampos('Exibição: ', 'st_textoexibicao');
                $arCampos[] = $this->setCampos('Categoria: ', 'st_textocategoria');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarPessoaJuridica';
                $pTO->setId_funcionalidade(286);
                $pTO->setMetodo('pesquisarPessoaJuridica');
                $pTO->setTo_envia('PesquisaEntidadeTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome: ', 'st_nomeentidade', 'CheckBox');
                $filtros[] = $this->setFiltro('Razão Social: ', 'st_razaosocial', 'CheckBox');
                $filtros[] = $this->setFiltro('CNPJ: ', 'st_cnpj', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID: ', 'id_entidade');
                $arCampos[] = $this->setCampos('Nome: ', 'st_nomeentidade');
                $arCampos[] = $this->setCampos('Razão Social: ', 'st_razaosocial');
                $arCampos[] = $this->setCampos('CNPJ: ', 'st_cnpj');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarAplicacaoAvaliacao';
                $pTO->setId_funcionalidade(253);
                $pTO->setMetodo('pesquisarAplicacaoAvaliacao');
                $pTO->setTo_envia('PesquisaAvaliacaoAplicacaoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Data da Aplicação: ', 'dt_aplicacao', 'DateField');
                $filtros[] = $this->setFiltro('UF: ', 'sg_uf', 'DropDownList', $this->retornarSelectUf());

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID: ', 'id_avaliacaoaplicacao');
                $arCampos[] = $this->setCampos('Aplicador: ', 'st_aplicadorprova');
                $arCampos[] = $this->setCampos('Local: ', 'st_endereco');
                $arCampos[] = $this->setCampos('Horário: ', 'st_horarioaula');
                $arCampos[] = $this->setCampos('Data: ', 'dt_aplicacao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarAvaliacao';
                $pTO->setId_funcionalidade(245);
                $pTO->setMetodo('pesquisarAvaliacao');
                $pTO->setTo_envia('PesquisaAvaliacaoTO');
                $label = new Ead1_LabelFuncionalidade();
                $filtros[] = $this->setFiltro('Nome: ', 'st_avaliacao', 'CheckBox');
                $filtros[] = $this->setFiltro('Tipo de Avaliação: ', 'id_tipoavaliacao', 'DropDownList', $this->retornaTipoAvaliacao(array()));
                $pTO->setFiltros($filtros);
                $arrCampos[] = $this->setCampos('ID: ', 'id_avaliacao');
                $arrCampos[] = $this->setCampos('Nome: ', 'st_avaliacao');
                $arrCampos[] = $this->setCampos('Valor: ', 'nu_valor');
                $arrCampos[] = $this->setCampos('Tipo de Avaliação:', 'st_tipoavaliacao');
                $pTO->setCamposGrid($arrCampos);
                return $pTO;
                break;
            case 'CadastrarAssunto':
                $pTO->setId_funcionalidade(414);
                $pTO->setMetodo('pesquisarAssuntoCo');
                $pTO->setTo_envia('PesquisarAssuntoCoTO');
                $filtros[] = $this->setFiltro('Assunto', 'st_assuntoco', 'CheckBox');
                $filtros[] = $this->setFiltro('Assunto Pai', 'st_assuntocopai', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_assuntoco')));
                $filtros[] = $this->setFiltro('Tipo Interessado', 'id_tipoocorrencia', 'DropDownList', $this->retornarAssuntoCo());
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_assuntoco");
                $arCampos[] = $this->setCampos("Assunto", "st_assuntocopai");
                $arCampos[] = $this->setCampos("Subassunto", "st_assuntoco");
                $arCampos[] = $this->setCampos("Tipo Interessado", "st_tipoocorrencia");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarMotivacional';
                $pTO->setId_funcionalidade(377);
                $pTO->setMetodo('pesquisarMensagemMotivacional');
                $pTO->setTo_envia('PesquisarMensagemMotivacionalTO');
                $filtros[] = $this->setFiltro('Mensagem: ', 'st_mensagemmotivacional', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arrCampos[] = $this->setCampos('ID: ', 'id_mensagemmotivacional');
                $arrCampos[] = $this->setCampos('Mensagem: ', 'st_mensagemmotivacional');
                $pTO->setCamposGrid($arrCampos);
                return $pTO;
            case 'CadastrarBaixaBoleto':
                $pTO->setId_funcionalidade(278);
                $pTO->setMetodo('pesquisarArquivoRetorno');
                $pTO->setTo_envia('PesquisarArquivoRetornoTO');
                $filtros[] = $this->setFiltro('Arquivo retorno', 'st_arquivoretorno', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_arquivoretorno");
                $arCampos[] = $this->setCampos("Arquivo retorno", "st_arquivoretorno");
                $arCampos[] = $this->setCampos("Data Cadastro", "dt_cadastro");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarCategoria':
                $pTO->setId_funcionalidade(497);
                $pTO->setMetodo('pesquisarCategoria');
                $pTO->setTo_envia('PesquisarCategoriaTO');
                $filtros[] = $this->setFiltro('Categoria', 'st_categoria', 'CheckBox');
                $filtros[] = $this->setFiltro('Categoria Pai', 'st_categoriapai', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_categoria')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_categoria");
                $arCampos[] = $this->setCampos("Categoria", "st_categoria");
                $arCampos[] = $this->setCampos("Categoria Pai", "st_categoriapai");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarClasseAfiliado':
                $pTO->setId_funcionalidade(501);
                $pTO->setMetodo('pesquisarClasseAfiliado');
                $pTO->setTo_envia('PesquisarClasseAfiliadoTO');
                $filtros[] = $this->setFiltro('Classe Afiliado', 'st_classeafiliado', 'TextInput');
                $filtros[] = $this->setFiltro('Tipo', 'st_tipopessoa', 'DropDownList', $this->retornaTipoPessoa());
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_classeafiliado')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_classeafiliado');
                $arCampos[] = $this->setCampos('Classe Afiliado', 'st_classeafiliado');
                $arCampos[] = $this->setCampos('Tipo', 'st_tipopessoa');
                $arCampos[] = $this->setCampos('Contratos', 'st_contratoafiliado');
                $arCampos[] = $this->setCampos("Situação", "st_situacao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarSetor':
                $pTO->setId_funcionalidade(526);
                $pTO->setMetodo('pesquisarSetor');
                $pTO->setTo_envia('PesquisarSetorTO');
                $ro = new SetorRO();
                $filtros[] = $this->setFiltro('Setor', 'st_setor', 'TextInput');
                $filtros[] = $this->setFiltro('Setor Pai', 'st_setorpai', 'TextInput');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_setor')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('Cód. Setor', 'id_setor');
                $arCampos[] = $this->setCampos('Setor', 'st_setor');
                $arCampos[] = $this->setCampos('Cód. Setor Pai', 'id_setorpai');
                $arCampos[] = $this->setCampos('Setor Pai', 'st_setorpai');
                $arCampos[] = $this->setCampos("Situação", "st_situacao");

                $pTO->setCamposGrid($arCampos);

                return $pTO;
                break;
            case 'CadastrarLivro':
                $pTO->setId_funcionalidade(542);
                $pTO->setMetodo('pesquisarLivro');
                $pTO->setTo_envia('PesquisarLivroTO');
                $filtros[] = $this->setFiltro('Livro', 'st_livro', 'TextInput');
                $filtros[] = $this->setFiltro('ISBN', 'st_isbn', 'TextInput');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_livro')));
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID Livro', 'id_livro');
                $arCampos[] = $this->setCampos('Livro', 'st_livro');
                $arCampos[] = $this->setCampos('ISBN', 'st_isbn');
                $arCampos[] = $this->setCampos("Situação", "st_situacao");

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastrarAplicadorProva':
                $pTO->setId_funcionalidade(585);
                $pTO->setMetodo('pesquisarAplicadorProva');
                $pTO->setTo_envia('VwPesquisarAplicadorProvaTO');
                $filtros[] = $this->setFiltro('Aplicador', 'st_aplicadorprova', 'TextInput');
                $filtros[] = $this->setFiltro('Tipo aplicador', 'id_tipopessoa', 'DropDownList', $this->retornaTipoPessoa());
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_aplicadorprova');
                $arCampos[] = $this->setCampos('Aplicador Prova', 'st_aplicadorprova');
                $arCampos[] = $this->setCampos('Tipo Pessoa', 'st_tipopessoa');
                $arCampos[] = $this->setCampos("Local", "st_local");

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'CadastrarAgendamentoProva':
                $pTO->setId_funcionalidade(269);
                $pTO->setMetodo('pesquisarAgendamentoProva');
                $pTO->setTo_envia('VwPesquisarAgendamentoProvaTO');
                $filtros[] = $this->setFiltro('Nome', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('E-mail', 'st_email', 'CheckBox');
                $filtros[] = $this->setFiltro('CPF', 'st_cpf', 'CheckBox');
                $filtros[] = $this->setFiltro('Local de prova', 'id_localprova', 'DropDownList', $this->retornaLocaisProva(array('id_entidade' => $this->sessao->id_entidade)));
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_usuario');
                $arCampos[] = $this->setCampos('Nome', 'st_nomecompleto');
                $arCampos[] = $this->setCampos('E-mail', 'st_email');
                $arCampos[] = $this->setCampos("CPF", "st_cpf");

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'CadastrarConcurso':
                $pTO->setId_funcionalidade(640);
                $pTO->setMetodo('pesquisarConcursos');
                $pTO->setTo_envia('ConcursoTO');
                $filtros[] = $this->setFiltro('Nome', 'st_concurso', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_concurso')));
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_concurso');
                $arCampos[] = $this->setCampos('Nome', 'st_concurso');
                $arCampos[] = $this->setCampos('Orgão', 'st_orgao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'CadastrarMensagemPortal';

                $pTO->setId_funcionalidade(664);
                $pTO->setMetodo('pesquisarMensagemPortal');
                $pTO->setTo_envia('PesquisarMensagemPortalTO');
                $filtros[] = $this->setFiltro('Mensagem ', 'st_mensagem', 'CheckBox');
                $filtros[] = $this->setFiltro('Projeto Pedagógico ', 'st_projetopedagogico', 'CheckBox');

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID', 'id_mensagem');
                $arCampos[] = $this->setCampos('Mensagem', 'st_mensagem');
                $arCampos[] = $this->setCampos('Projeto Pedagógico', 'st_projetopedagogico');
                $arCampos[] = $this->setCampos('Área', 'st_areaconhecimento');
                $arCampos[] = $this->setCampos('Turma ', 'st_turma');
                $arCampos[] = $this->setCampos('Data de Início', 'dt_enviar');
                $arCampos[] = $this->setCampos('Data de Término', 'dt_saida');
                $arCampos[] = $this->setCampos('Situação', 'st_situacao');
                $arCampos[] = $this->setCampos('Responsável Pelo Envio', 'st_nomecompleto');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'Atendente';

                $pTO->setId_funcionalidade(673);
                $pTO->setMetodo('pesquisarAtendente');
                $pTO->setTo_envia('PesquisarAtendenteTO');
                $filtros[] = $this->setFiltro('Nome', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('CPF', 'st_cpf', 'CheckBox');
                $filtros[] = $this->setFiltro('Unidade: ', 'id_nucleotm', 'DropDownList', $this->retornaNucleo(array('id_entidadematriz' => $this->sessao->id_entidade)));
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_nucleofuncionariotm')));


                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('Id', 'id_nucleofuncionariotm');
                $arCampos[] = $this->setCampos('Nome', 'st_nomecompleto');
                $arCampos[] = $this->setCampos('Unidade', 'st_nucleotm');
                $arCampos[] = $this->setCampos('Situação', 'st_situacao');
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'Holding';

                $pTO->setId_funcionalidade(694);
                $pTO->setMetodo('pesquisarHolding');
                $pTO->setTo_envia('PesquisarHoldingTO');
                $filtros[] = $this->setFiltro('Nome', 'st_holding', 'CheckBox');
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_holding')));


                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('Id', 'id_holding');
                $arCampos[] = $this->setCampos('Nome', 'st_holding');
                $arCampos[] = $this->setCampos('Status', 'st_situacao');
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;

            case 'EsquemaConfiguracao';
                $pTO->setId_funcionalidade(709);
                $pTO->setMetodo('pesquisarEsquemaConfiguracao');
                $pTO->setTo_envia('PesquisarEsquemaConfiguracaoTO');
                $filtros[] = $this->setFiltro('Nome do Esquema', 'st_esquemaconfiguracao', 'TextInput');

                $pTO->setFiltros($filtros);
                $arCampos[] = $this->setCampos('ID do Esquema', 'id_esquemaconfiguracao');
                $arCampos[] = $this->setCampos('Nome do Esquema', 'st_esquemaconfiguracao');
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'PerguntasFrequentes';
                $pTO->setId_funcionalidade(724);
                $pTO->setMetodo('pesquisarPerguntasFrequentes');
                $pTO->setTo_envia('PesquisarPerguntasFrequentesTO');
                $filtros[] = $this->setFiltro('Nome do Esquema', 'st_esquemapergunta', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_esquemapergunta');
                $arCampos[] = $this->setCampos('Nome do Esquema', 'st_esquemapergunta');
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'NucleoAtendentes':
                $pTO->setId_funcionalidade(747);
                $pTO->setMetodo('pesquisarNucleoTm');
                $pTO->setTo_envia('NucleotmTO');
                $filtros[] = $this->setFiltro('Nome', 'st_nucleotm', 'CheckBox');
                $filtros[] = $this->setFiltro('Núcleo', 'id_nucleotm', 'DropDownList', $this->retornaNucleo(array('id_entidadematriz' => $this->sessao->id_entidade)));
                $filtros[] = $this->setFiltro('Situação', 'id_situacao', 'DropDownList', $this->retornaSituacao(array('st_tabela' => 'tb_nucleotm')));

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_nucleotm");
                $arCampos[] = $this->setCampos("Núcleo", "st_nucleotm");
                $arCampos[] = $this->setCampos('Situação', 'st_situacao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'AlterarVenda';
                $pTO->setId_funcionalidade(751);
                $pTO->setMetodo('pesquisarVendaAlterarNegociacao');
                $pTO->setTo_envia('PesquisarVendaTO');
                $sTO = array('st_tabela' => 'tb_venda');
                $eTO = array('st_tabela' => 'tb_venda');
                $filtros[] = $this->setFiltro('Nome: ', 'st_nomecompleto', 'CheckBox');
                $filtros[] = $this->setFiltro('Venda: ', 'id_venda', 'TextInput');
                $filtros[] = $this->setFiltro('Situação: ', 'id_situacao', 'DropDownList', $this->retornaSituacao($sTO));
                $filtros[] = $this->setFiltro('Data do Início: ', 'dt_inicio', "DateField");
                $filtros[] = $this->setFiltro('Data do Fim: ', 'dt_fim', "DateField");
                $filtros[] = $this->setFiltro('Possui Contrato: ', 'bl_contrato', 'CheckBox');
                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos("ID", "id_venda");
                $arCampos[] = $this->setCampos("Usuário", "st_nomecompleto");
                $arCampos[] = $this->setCampos("Situação", "st_situacao");
                $arCampos[] = $this->setCampos("Evolução", "st_evolucao");
                $arCampos[] = $this->setCampos("Valor Bruto", "nu_valorbruto");
                $arCampos[] = $this->setCampos("Data", "dt_cadastro");
                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'FormaIngresso';
                $pTO->setId_funcionalidade(796);
                $pTO->setMetodo('pesquisarFormaIngresso');
                $pTO->setTo_envia('TipoSelecaoTO');
                $filtros[] = $this->setFiltro('Nome: ', 'st_tiposelecao', 'CheckBox');
                $filtros[] = $this->setFiltro('Descrição: ', 'st_descricao', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID: ', 'id_tiposelecao');
                $arCampos[] = $this->setCampos('Tipo de Ingresso: ', 'st_tiposelecao');
                $arCampos[] = $this->setCampos('Texto da Forma de Ingresso: ', 'st_descricao');

                $pTO->setCamposGrid($arCampos);
                return $pTO;
                break;
            case 'CadastroConfiguracaoCertificadoParcial';
                $pTO->setId_funcionalidade(837);
                $pTO->setMetodo('pesquisaCertificacaoParcial');
                $pTO->setTo_envia('TipoSelecaoTO');
                $filtros[] = $this->setFiltro('Nome Certificado Parcial', 'st_tiposelecao', 'CheckBox');

                $pTO->setFiltros($filtros);

                $arCampos[] = $this->setCampos('ID', 'id_certificadoparcial');
                $arCampos[] = $this->setCampos('Nome do Certificado Parcial', 'st_certificadoparcial');
                $arCampos[] = $this->setCampos('Data Início da Vigência', 'dt_iniciovigencia', "DateField");
                $arCampos[] = $this->setCampos('Data Fim da Vigência', 'dt_fimvigencia', "DateField");

                $pTO->setCamposGrid($arCampos);
                return $pTO;

                return false;
                break;

            default:
                return false;
        }
    }

    /**
     * Método para set de filtros
     * @param String $label
     * @param String $propriedade
     * @param String $tipo
     * @param mixed $valor
     * @return FiltroTO
     */
    private function setFiltro($label, $propriedade, $tipo, $valor = null)
    {
        $filtro = new Filtro();
        $filtro->setSt_label($label);
        $filtro->setSt_propriedade($propriedade);
        $filtro->setSt_tipo($tipo);
        $filtro->setValor($valor);
        return $filtro;
    }

    /**
     * Método que seta o objeto para Flex
     * @param String $label
     * @param String $propriedade
     * @param bool $visible
     * @return Object
     */
    private function setCampos($label, $propriedade, $visible = true)
    {
        $obj = (Object)array(
            'label' => $label,
            'field' => $propriedade,
            'visible' => $visible
        );

        return $obj;
    }

    /**
     * Retorna Modalidade Sala de aula
     * @param array $params
     * @return mixed object
     */
    private function retornarModalidadeSalaDeAula(array $params = array())
    {
        $result = $this->em->getRepository('G2\Entity\ModalidadeSalaDeAula')->findBy($params);
        if ($result) {
            return $this->retornarCombosPesquisa($this->entitySerialize($result), 'st_modalidadesaladeaula', 'id_modalidadesaladeaula');
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
    }

    /**
     * Retorna tipo de prova
     * @param TipoProvaTO $tpTO
     */
    public function retornarTipoProva($params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\TipoProva');
            $result = $repo->findBy($params);
            $arrayRetorno = array();
            if ($result) {
                foreach ($result as $index => $values) {
                    $arrayRetorno[$index]['label'] = $values->getSt_tipoprova();
                    $arrayRetorno[$index]['valor'] = $values->getId_tipoprova();
                }
            }
            return $arrayRetorno;
        } catch (Zend_Exception $e) {
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function retornaTipoPessoa(array $params = array())
    {
        $arDadosTipoPessoa = array();
        $result = $this->em->getRepository('G2\Entity\TipoPessoa')->findBy($params);
        if ($result) {
            foreach ($result as $index => $objeto) {
                $arDadosTipoPessoa[$index]['label'] = $objeto->getSt_tipopessoa();
                $arDadosTipoPessoa[$index]['valor'] = $objeto->getId_tipopessoa();
            }
        }
        return $arDadosTipoPessoa;
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function retornarAssuntoCo($params = array())
    {
        $assuntoCoBO = new AssuntoCoBO();
        $arDadosTipoOcorrencia = array();
        $result = $assuntoCoBO->findBy($params);
        if ($result) {
            foreach ($result as $index => $objeto) {
                $arDadosTipoOcorrencia[$index]['label'] = $objeto->getSt_tipoocorrencia();
                $arDadosTipoOcorrencia[$index]['valor'] = $objeto->getId_tipoocorrencia();
            }
        }
        return $arDadosTipoOcorrencia;
    }

    /**
     * Retorna os tipos de exibição para o texto
     * @param TextoExibicaoTO $to
     */
    public function retornarTextoExibicao($params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\TextoExibicao');
            $result = $repo->findBy($params);
            $arrayRetorno = array();
            if ($result) {
                foreach ($result as $index => $values) {
                    $arrayRetorno[$index]['label'] = $values->getSt_textoexibicao();
                    $arrayRetorno[$index]['valor'] = $values->getId_textoexibicao();
                }
            }
            return $arrayRetorno;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Retorna tipoAvaliacao
     * @param $taTO
     */
    public function retornaTipoAvaliacao($params)
    {
        $tipoAvaBo = new TipoAvaliacaoBO();
        $result = $tipoAvaBo->findBy($params);
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_tipoavaliacao();
                $arrayRetorno[$index]['valor'] = $values->getId_tipoavaliacao();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Metodo que retorna dados da pesquisa da avaliacao
     * @param $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarPesquisaAvaliacao(PesquisaAvaliacaoTO $aTO)
    {
        try {
            $dao = new PesquisarDAO();
            $aTO->setId_entidade($aTO->getSessao()->id_entidade);
            $avaliacao = $dao->retornarPesquisaAvaliacao($aTO)->toArray();

            if (empty($avaliacao)) {
                $this->mensageiro->setMensageiro('Nenhum registro encontrado.', Ead1_IMensageiro::AVISO);
            } else {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($avaliacao, new PesquisaAvaliacaoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao pesquisar avaliação.', Ead1_IMensageiro::ERRO, $dao->excecao);
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Agendamento de Avaliacao
     * @param PesquisaAvaliacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarAgendamentoAvaliacao($params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoAgendamento');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            //trata os parametros de data
            foreach ($params as $key => $value) {
                if (substr($key, 0, 3) == 'dt_') {
                    $params[$key] = $this->converteDataBanco($value);
                }
            }

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['id_tipoprova']) {
                $query->andWhere('vw.id_tipoprova = :id_tipoprova')
                    ->setParameter('id_tipoprova', $params['id_tipoprova']);
            }
            if ($params['st_avaliacao']) {
                $query->andWhere('vw.st_nomecompleto LIKE :st_avaliacao')
                    ->setParameter('st_avaliacao', '%' . $params['st_avaliacao'] . '%');
            }
            if ($params['dt_agendamentoinicio'] && $params['dt_agendamentofim']) {
                $query->andWhere('vw.dt_agendamento BETWEEN :dt_agendamentoinicio AND :dt_agendamentofim')
                    ->setParameter('dt_agendamentoinicio', $params['dt_agendamentoinicio'])
                    ->setParameter('dt_agendamentofim', $params['dt_agendamentofim']);
            } else if (!$params['dt_agendamentofim'] && $params['dt_agendamentoinicio']) {
                $query->andWhere('vw.dt_agendamento >= :dt_agendamentoinicio')
                    ->setParameter('dt_agendamentoinicio', $params['dt_agendamentoinicio']);
            } else if ($params['dt_agendamentofim'] && !$params['dt_agendamentoinicio']) {
                $query->andWhere('vw.dt_agendamento <= :dt_agendamentofim')
                    ->setParameter('dt_agendamentofim', $params['dt_agendamentofim']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_avaliacaoagendamento');

        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao pesquisar agendamento de avaliação.', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Metodo que retorna os dados da Pesquisa de textos do sistema
     * @param PesquisarTextoSistemaTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTextosSistema($params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwTextoSistema');
            //Recupera o parametro da grid e monta um array com apenas as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            //Seta o id da entidade da sessão caso ele não tenha sido passado
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //cria a query
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1')
                ->andWhere('vw.bl_edicao = 1');
            //verifica os parametros passados e seta os AND da WHERE do queryBuilder
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['id_textoexibicao']) {
                $query->andWhere('vw.id_textoexibicao = :id_textoexibicao')
                    ->setParameter('id_textoexibicao', $params['id_textoexibicao']);
            }
            if ($params['st_textosistema']) {
                $query->andWhere('vw.st_textosistema LIKE :st_textosistema')
                    ->setParameter('st_textosistema', '%' . $params['st_textosistema'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_textosistema');

        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Pesquisar Textos do Sistema!', Ead1_IMensageiro::ERRO, $this->dao->excecao);
        }
    }

    /**
     * Método que pesquisa a disciplina
     * @param PesquisarDisciplinaTO $pdTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarDisciplina(PesquisarDisciplinaTO $pdTO)
    {
        $dao = new PesquisarDAO ();
        if (!$pdTO->getId_entidade()) {
            $pdTO->setId_entidade($pdTO->getSessao()->id_entidade);
        }
        $disciplina = $dao->pesquisarDisciplina($pdTO);
        if (!$disciplina) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $dao->excecao);
        }
        return $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($disciplina->toArray(), new PesquisarDisciplinaTO()), Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Retorna as entidades para combo de pesquisa
     * @param array $params
     * @return array
     */
    public function retornarSelectEntidades($params = array())
    {
        if (!isset($params['id_entidade'])) {
            $params['id_entidade'] = $this->sessao->id_entidade;
        }
        $entidades = $this->pesquisarEntidadeFilha($params);
        if ($entidades->getTipo() != Ead1_IMensageiro::SUCESSO) {
            return false;
        }
        $arrayRetorno = array();
        foreach ($entidades->getMensagem() as $index => $to) {
            $arrayRetorno[$index]['label'] = $to->getSt_nomeentidade();
            $arrayRetorno[$index]['valor'] = $to->getId_entidade();
        }
        return $arrayRetorno;
    }

    /**
     * Método que pesquisa a entidade filha
     * @param VwEntidadeClasseTO $ecTO
     * @param boolean $pesquisar
     * @return Ead1_Mensageiro
     */
    public function pesquisarEntidadeFilha($ecTO, $pesquisar = false)
    {
        if (is_object($ecTO)) { //busca com TO
            if (!$ecTO->getId_entidade()) {
                $ecTO->setId_entidade($ecTO->getSessao()->id_entidade);
            }
            $this->verificarEntidadeFilha($ecTO);
            return $this->mensageiro->setMensageiro($this->arrEntidade, Ead1_IMensageiro::SUCESSO);
        } else { // Busca com Doctrine
            $this->verificarEntidadeFilha($ecTO);
            return $this->mensageiro->setMensageiro($this->arrEntidade, Ead1_IMensageiro::SUCESSO);
        }
    }

    public $arrEntidade;

    /**
     * Verifica Entidade filha
     * @param VwEntidadeClasseTO $ecTO
     */
    public function verificarEntidadeFilha($ecTO)
    {
        if (is_object($ecTO)) {
            $dao = new PesquisarDAO ();
            $entidadePai = $dao->pesquisarEntidadeFilha($ecTO);
            if (!$entidadePai) {
                return $this->mensageiro->setMensageiro('Entidade não encontrada', Ead1_IMensageiro::ERRO);
            }
            $entidadePai = $entidadePai->toArray();

            if (!empty($entidadePai)) {
                foreach ($entidadePai as $index => $pai) {
                    $newecTO = new VwEntidadeClasseTO ();
                    $newecTO->setId_entidadepai($pai['id_entidade']);

                    $arrEntidades = Ead1_TO_Dinamico::encapsularTo(array($pai), new VwEntidadeClasseTO());
                    foreach ($arrEntidades as $entidade) {
                        $this->arrEntidade [] = $entidade;
                    }
                    $this->verificarEntidadeFilha($newecTO);
                }
            }
        } else { //Busca com Doctrine
            $repo = $this->em->getRepository('G2\Entity\VwEntidadeClasse');
            $result = $repo->findBy($ecTO);
            if ($result) {
                foreach ($result as $index => $row) {
                    $paramPai['id_entidadepai'] = $row->getId_entidade();
                    $this->arrEntidade[] = $row;
                    $this->verificarEntidadeFilha($paramPai);
                }
            }
        }
    }

    /**
     * Retorna a situação
     * @param SituacaoTO $sTO
     * @return array|false
     */
    public function retornaSituacao($params)
    {
        $arrayRetorno = array();
        $result = $this->em->getRepository('G2\Entity\Situacao')
            ->findBy($params);
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_situacao();
                $arrayRetorno[$index]['valor'] = $values->getId_situacao();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Retorna a evolução
     * @param EvolucaoTO $eTO
     * @return array|false
     */
    public function retornaEvolucao($params)
    {

        $result = $this->em->getRepository('G2\Entity\Evolucao')->findBy($params);
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_evolucao();
                $arrayRetorno[$index]['valor'] = $values->getId_evolucao();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Retorna a Entidade
     * @param Entidade $eTO
     * @return array|false
     */
    public function retornaEntidade($params)
    {

        $result = $this->em->getRepository('G2\Entity\Entidade')->findBy($params);
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_nomeentidade();
                $arrayRetorno[$index]['valor'] = $values->getId_entidade();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Retorna a nucleo
     * @param Entidade $eTO
     */
    public function retornaNucleo($params)
    {
        $result = $this->em->getRepository('G2\Entity\NucleoTm')->findBy($params);
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_nucleotm();
                $arrayRetorno[$index]['valor'] = $values->getId_nucleotm();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Retorna tipo de Sala de aula
     */
    private function retornarTipoSalaDeAula(array $params = array())
    {
        $result = $this->em->getRepository('G2\Entity\TipoSaladeAula')->findBy($params);
        if ($result) {
            return $this->retornarCombosPesquisa($this->entitySerialize($result), 'st_tiposaladeaula', 'id_tiposaladeaula');
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
    }

    /**
     * Retorna o tipo de avaliacao
     * @param TipoAvaliacaoTO $taTO
     * @return array|false
     */
    public function retornarTipoAvaliacao(PesquisarTipoAvaliacaoTO $taTO)
    {
        $dao = new PesquisarDAO();
        $ret = $dao->retornarTipoAvaliacao($taTO);
        if (!$ret) {
            return false;
        } else {
            return $ret->toArray();
        }
    }

    /**
     * Retorna o tipo de produto
     * @param TipoProdutoTO $tdTO
     * @return array|false
     */
    public function retornaTipoProduto($params)
    {
        $result = $this->em->getRepository('G2\Entity\TipoProduto')->findBy($params);
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $values) {
                $arrayRetorno[$index]['label'] = $values->getSt_tipoproduto();
                $arrayRetorno[$index]['valor'] = $values->getId_tipoproduto();
            }
        }
        return $arrayRetorno;
    }

    /**
     * Retorna o tipo da disciplina
     * @param TipoDisciplinaTO $tdTO
     * @return array|false
     */
    public function retornaTipoDisciplina(array $tdTO = array())
    {
        $result = $this->em->getRepository('G2\Entity\TipoDisciplina')->findBy($tdTO);
        if ($result) {
            return $this->retornarCombosPesquisa($this->entitySerialize($result), 'st_tipodisciplina', 'id_tipodisciplina');
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
    }

    /**
     *
     * @param NivelEnsinoTO $neTO
     * @param boolean $pesquisa
     * @param string $consulta
     * @return Ead1_Mensageiro|Array
     */
    public function retornaNivelEnsino($params, $pesquisa = false)
    {
        if ($params instanceOf NivelEnsinoTO) {
            if ($params->id_nivelensino)
                $params = array('id_nivelensino' => $params->id_nivelensino, 'st_nivelensino' => $params->st_nivelensino);
            else
                $params = array();
        }

        $result = $this->em->getRepository('G2\Entity\NivelEnsino')->findBy($params);
        if ($result) {
            if ($pesquisa) {
                return $this->retornarCombosPesquisa($this->entitySerialize($result), 'st_nivelensino', 'id_nivelensino');
            }
            return $this->mensageiro->setMensageiro($this->entitySerialize($result), Ead1_IMensageiro::SUCESSO);
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
    }

    public function retornaGrupoDisciplina($params, $pesquisa = false)
    {
        $result = $this->em->getRepository('G2\Entity\GrupoDisciplina')->findBy($params);
        if ($result) {
            if ($pesquisa) {
                return $this->retornarCombosPesquisa($this->entitySerialize($result), 'st_grupodisciplina', 'id_grupodisciplina');
            }
            return $this->mensageiro->setMensageiro($this->entitySerialize($result), Ead1_IMensageiro::SUCESSO);
        } else {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO, $this->dao->excecao);
        }
    }

    /**
     * Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
     * @param string $prefix
     * @param json string $grid
     * @return array
     */
    private function getColumnsFromPesquisaDinamica($grid, $prefix = '')
    {
        $grids = json_decode($grid);
        $arrGrid = array();
        $prefix = (!empty($prefix) ? $prefix . "." : null);
        if (is_object($grids)) {
            foreach ($grids->grid as $key => $grid) {
                $arrGrid[] = $prefix . $grid->name;
            }
        }
        return $arrGrid;
    }

    /**
     * Retorna pesquisa de pessoa juridica
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaPessoaJuridica(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\Entidade');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "pj");

            //Verifica se não veio nos parametros o id da entidade, e seta o id vindo da sessão
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            //Monta a query builder
            $query = $repo->createQueryBuilder("pj");
            if ($arrColumns) {
                $query->select($arrColumns);
            }
            $query->where('1=1');
            $where = array();
            if (isset($params['st_nomeentidade']) && !empty($params['st_nomeentidade'])) {
                $where[] = "pj.st_nomeentidade LIKE '%" . $params['st_nomeentidade'] . "%'";
            }
            if (isset($params['st_razaosocial']) && !empty($params['st_razaosocial'])) {
                $where[] = "pj.st_razaosocial LIKE '%" . $params['st_razaosocial'] . "%'";
            }
            if (isset($params['st_cnpj']) && !empty($params['st_cnpj'])) {
                $where[] = "pj.st_cnpj LIKE '%" . $params['st_cnpj'] . "%'";
            }

            if ($where)
                $query->andWhere(implode(" OR ", $where));

            if ($params['id_entidade']) {
                $query->andWhere('pj.id_entidadecadastro = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'pj.id_entidade');
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa de relatorios
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaRelatorio(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\Relatorio');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "r");
            //Verifica se não veio nos parametros o id da entidade, e seta o id vindo da sessão
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Monta a query builder
            $query = $repo->createQueryBuilder("r")
                ->select($arrColumns)
                ->where('r.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if ($params['st_relatorio']) {
                $query->andWhere('r.st_relatorio LIKE :st_relatorio')
                    ->setParameter('st_relatorio', '%' . $params['st_relatorio'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'r.id_relatorio');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa de mensagens motivacionais
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaMensgemMotivacional(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\MensagemMotivacional');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "m");
            //Monta a query builder
            $query = $repo->createQueryBuilder("m")
                ->select($arrColumns)
                ->where('1 = 1');
            if ($params['st_mensagemmotivacional']) {
                $query->andWhere('m.st_mensagemmotivacional LIKE :st_mensagemmotivacional')
                    ->setParameter('st_mensagemmotivacional', '%' . $params['st_mensagemmotivacional'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'm.id_mensagemmotivacional');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa do livro
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaSetor(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwSetor');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");
            //Verifica se não veio nos parametros o id da entidade, e seta o id vindo da sessão
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Monta a query builder
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidadecadastro = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if (isset($params['id_situacao'])) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['st_setor']) {
                $query->andWhere('vw.st_setor LIKE :st_setor')
                    ->setParameter('st_setor', '%' . $params['st_setor'] . '%');
            }
            if ($params['st_setorpai']) {
                $query->andWhere('vw.st_setorpai LIKE :st_setorpai')
                    ->setParameter('st_setorpai', '%' . $params['st_setorpai'] . '%');
            }

            if (!$orderBy)
                $orderBy = array('st_setor' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_setor');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa livro
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaLivro(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisarLivro');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");
            //Verifica se não veio nos parametros o id da entidade, e seta o id vindo da sessão
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            if (!isset($params['bl_ativo'])) {
                $params['bl_ativo'] = true;
            }
            //Monta a query builder
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidadecadastro = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['st_isbn']) {
                $query->andWhere('vw.st_isbn = :st_isbn')
                    ->setParameter('st_isbn', $params['st_isbn']);
            }
            if ($params['bl_ativo']) {
                $query->andWhere('vw.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo']);
            }
            if ($params['st_livro']) {
                $query->andWhere('vw.st_livro LIKE :st_livro')
                    ->setParameter('st_livro', '%' . $params['st_livro'] . '%');
            }

            if (!$orderBy)
                $orderBy = array('st_livro' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_livro');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa protocolo
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisarProtocolo(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisarProtocolo');

            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");
            //Monta a query builder
            $query = $repo->createQueryBuilder("vw")->select($arrColumns);

            if ($params['dt_cadastro']) {
                $date = date("Y-m-d", strtotime($params['dt_cadastro']));
                $query->where('vw.dt_cadastro LIKE :dt_cadastro')
                    ->setParameter('dt_cadastro', '%' . $date . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_protocolo');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna a pesquisa de Calsse de Afiados
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaClasseAfiliado(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwClasseAfiliado');
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");
            //Monta a query builder
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if (isset($params['st_tipopessoa'])) {
                $query->andWhere('vw.id_tipopessoa = :st_tipopessoa')
                    ->setParameter('st_tipopessoa', $params['st_tipopessoa']);
            }
            if (isset($params['id_situacao'])) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if (isset($params['st_classeafiliado'])) {
                $query->andWhere('vw.st_classeafiliado LIKE :st_classeafiliado')
                    ->setParameter('st_classeafiliado', '%' . $params['st_classeafiliado'] . '%');
            }

            if (!$orderBy)
                $orderBy = array('st_classeafiliado' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_classeafiliado');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaContratoAfiliado(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwContratoAfiliado');
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidadecadastro = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            //seta os parametros do like
            if ($params['id_contratoafiliado'] || $params['st_contratoafiliado']) { //verifica se um dos dois parametros veio setado
                $where = '';
                if ($params['id_contratoafiliado']) {
                    $where .= "vw.id_contratoafiliado LIKE '%{$params['id_contratoafiliado']}%'";
                }
                //se os dois parametros veio concatena o OR
                if ($params['id_contratoafiliado'] && $params['st_contratoafiliado']) {
                    $where .= " OR ";
                }
                if ($params['st_contratoafiliado']) {
                    $where .= "vw.st_contratoafiliado LIKE '%{$params['st_contratoafiliado']}%'";
                }
                if ($where)
                    $query->andWhere($where);
            }
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_contratoafiliado');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaAssuntoCo(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisaAssuntoCo');
            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");

            $query = $repo->createQueryBuilder("vw")
                ->select('DISTINCT ' . implode(",", $arrColumns))
                ->where('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            //seta os parametros do like
            if ($params['st_assuntoco'] || $params['st_assuntocopai']) { //verifica se um dos dois parametros veio setado
                $where = '';
                if ($params['st_assuntoco']) {
                    $where .= "vw.st_assuntoco LIKE '%{$params['st_assuntoco']}%'";
                }
                //se os dois parametros veio concatena o OR
                if ($params['st_assuntoco'] && $params['st_assuntocopai']) {
                    $where .= " OR ";
                }
                if ($params['st_assuntocopai']) {
                    $where .= "vw.st_assuntocopai LIKE '%{$params['st_assuntocopai']}%'";
                }
                if ($where)
                    $query->andWhere($where);
            }
            if ($params['id_tipoocorrencia']) {
                $query->andWhere('vw.id_tipoocorrencia = :id_tipoocorrencia')
                    ->setParameter('id_tipoocorrencia', $params['id_tipoocorrencia']);
            }
            if ($params['id_entidadecadastro']) {
                $query->andWhere('vw.id_entidadecadastro = :id_entidadecadastro')
                    ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
            }

            if (!$orderBy)
                $orderBy = array('st_assuntoco' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_assuntoc');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaItemMaterial(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repositoryName = 'G2\Entity\ItemDeMaterial';
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'im');
            $query = $this->em->createQueryBuilder()
                ->select($arrColumns)
                ->from($repositoryName, 'im')
                ->join('im.id_tipodematerial', 'tm')
                ->where('tm.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);

            if ($params['st_itemdematerial']) {
                $query->andWhere('im.st_itemdematerial LIKE :st_itemdematerial')
                    ->setParameter('st_itemdematerial', '%' . $params['st_itemdematerial'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'im.id_itemdematerial');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaContrato(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisarContrato');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');

            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);

            if ($params['st_nomecompleto']) {
                $query->andWhere('vw.st_nomecompleto LIKE :st_nomecompleto')
                    ->setParameter('st_nomecompleto', '%' . $params['st_nomecompleto'] . '%');
            }
            if ($params['id_evolucao']) {
                $query->andWhere('vw.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_usuario');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaAproveitamento(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAproveitamento');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');

            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select("DISTINCT " . implode(",", $arrColumns))
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            //seta os parametros do like

            $where = array();
            if ($params['st_nomecompleto']) {
                $where[] = "vw.st_nomecompleto LIKE '%{$params['st_nomecompleto']}%'";
            }
            if ($params['st_disciplinaoriginal']) {
                $where[] = "vw.st_disciplinaoriginal LIKE '%{$params['st_disciplinaoriginal']}%'";
            }
            if ($params['st_tituloexibicao']) {
                $where[] = "vw.st_tituloexibicao LIKE '%{$params['st_tituloexibicao']}%'";
            }

            if ($where)
                $query->andWhere(implode(" OR ", $where));


            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_usuario');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisaAvaliacao');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');

            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);

            if ($params['st_avaliacao']) {
                $query->andWhere('vw.st_avaliacao LIKE :st_avaliacao')
                    ->setParameter('st_avaliacao', '%' . $params['st_avaliacao'] . '%');
            }
            if ($params['id_tipoavaliacao']) {
                $query->andWhere('vw.id_tipoavaliacao = :id_tipoavaliacao')
                    ->setParameter('id_tipoavaliacao', $params['id_tipoavaliacao']);
            }
            $retorno = $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_avaliacao');

            foreach ($retorno->rows as $key => $value) {
                if ($value['nu_valor'] == 0) {
                    $retorno->rows[$key]['nu_valor'] = ' - ';
                }
            }


            return $retorno;

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaConjuntoAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoConjunto');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if ($params['st_avaliacaoconjunto']) {
                $query->andWhere('vw.st_avaliacaoconjunto LIKE :st_avaliacaoconjunto')
                    ->setParameter('st_avaliacaoconjunto', '%' . $params['st_avaliacaoconjunto'] . '%');
            }
            if ($params['id_tipoprova']) {
                $query->andWhere('vw.id_tipoprova = :id_tipoprova')
                    ->setParameter('id_tipoprova', $params['id_tipoprova']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_avaliacaoconjunto');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna pesquisa de aplicação
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaAplicacaoAvaliacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoAplicacao');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas

            $query = $repo->createQueryBuilder("vw")
                ->select('vw.id_avaliacaoaplicacao', 'vw.st_aplicadorprova', 'vw.st_endereco', 'vw.st_horarioaula', 'vw.dt_aplicacao')
                ->where('1=1');

            if (!empty($params['sg_uf'])) {
                $query->andWhere('vw.sg_uf = :sg_uf')
                    ->setParameter('sg_uf', $params['sg_uf']);
            }

            if (isset($params['bl_ativo'])) {
                $query->andWhere('vw.bl_ativo = ' . $params['bl_ativo']);
            }

            if (!empty($params['dt_aplicacao'])) {
                $dataEntrada = new \Zend_Date(new \Zend_Date($params['dt_aplicacao']), \Zend_Date::ISO_8601);
                $dtEntrada = $dataEntrada->toString('yyyy-MM-dd');

                $query->andWhere('vw.dt_aplicacao = :dt_aplicacao')
                    ->setParameter('dt_aplicacao', $dtEntrada);
            }
            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);

            $results = $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_avaliacaoaplicacao');

            if (isset($results->rows))
                $entities = $results->rows;
            else
                $entities = $results;

            if ($entities) {
                foreach ($entities as &$row) {
                    $row = array(
                        'id_avaliacaoaplicacao' => $row['id_avaliacaoaplicacao'],
                        'st_aplicadorprova' => $row['st_aplicadorprova'],
                        'st_endereco' => $row['st_endereco'],
                        'st_horarioaula' => $row['st_horarioaula'],
                        'dt_aplicacao' => ($row['dt_aplicacao'] ? date_format($row['dt_aplicacao'], 'd/m/Y') : 'Sem data')
                    );
                }
            }

            if (isset($results->rows))
                $results->rows = $entities;
            else
                $results = $entities;

            return $results;

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    public function retornaPesquisaProjetoPedagogico(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwProjetoPedagogico');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidadecadastro = :id_entidadecadastro')
                ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);

            $where = array();
            if ($params['st_descricao']) {
                $where[] = "vw.st_descricao LIKE '%{$params['st_descricao']}%'";
            }
            if ($params['st_projetopedagogico']) {
                $where[] = "vw.st_projetopedagogico LIKE '%{$params['st_projetopedagogico']}%'";
            }

            if ($where)
                $query->andWhere(implode(" OR ", $where));

            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_projetopedagogico');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisarArea(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAreaConhecimentoNivelSerie');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select('DISTINCT ' . implode(',', $arrColumns))
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['st_areaconhecimento']) {
                $query->andWhere('vw.st_areaconhecimento LIKE :st_areaconhecimento')
                    ->setParameter('st_areaconhecimento', '%' . $params['st_areaconhecimento'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_areaconhecimento');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisarDisciplina(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisarDisciplinaNivelArea');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            if (!isset($params['bl_ativa'])) {
                $params['bl_ativa'] = true;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select('DISTINCT ' . implode(',', $arrColumns))
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade'])
                ->andWhere('vw.bl_ativa = :bl_ativa')
                ->setParameter('bl_ativa', $params['bl_ativa']);

            if ($params['id_tipodisciplina']) {
                $query->andWhere('vw.id_tipodisciplina = :id_tipodisciplina')
                    ->setParameter('id_tipodisciplina', $params['id_tipodisciplina']);
            }
            if ($params['id_nivelensino']) {
                $query->andWhere('vw.id_nivelensino = :id_nivelensino')
                    ->setParameter('id_nivelensino', $params['id_nivelensino']);
            }
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['id_grupodisciplina']) {
                $query->andWhere('vw.id_grupodisciplina = :id_grupodisciplina')
                    ->setParameter('id_grupodisciplina', $params['id_grupodisciplina']);
            }


            $where = array();
            if ($params['st_disciplina']) {
                $where[] = "vw.st_disciplina LIKE '%{$params['st_disciplina']}%'";
            }
            if ($params['st_areaconhecimento']) {
                $where[] = "vw.st_areaconhecimento LIKE '%{$params['st_areaconhecimento']}%'";
            }
            if ($where)
                $query->andWhere(implode(" OR ", $where));

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_disciplina');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaTurma(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwTurma');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }
            if (!isset($params['bl_ativo'])) {
                $params['bl_ativo'] = true;
            }
            //trata os parametros de data
            foreach ($params as $key => $value) {
                if (substr($key, 0, 3) == 'dt_') {
                    $params[$key] = $this->converteDataBanco($value);
                }
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidadecadastro = :id_entidadecadastro')
                ->setParameter('id_entidadecadastro', $params['id_entidadecadastro'])
                ->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', $params['bl_ativo']);
            if ($params['st_turma']) {
                $query->andWhere('vw.st_turma LIKE :st_turma')
                    ->setParameter('st_turma', '%' . $params['st_turma'] . '%');
            }
            if ($params['dt_inicio']) {
                $query->andWhere('vw.dt_inicio >= :dt_inicio')
                    ->setParameter('dt_inicio', $params['dt_inicio']);
            }
            if ($params['dt_inicio']) {
                $query->andWhere('vw.dt_inicioinscricao >= :dt_inicioinscricao')
                    ->setParameter('dt_inicioinscricao', $params['dt_inicioinscricao']);
            }
            if ($params['dt_fim']) {
                $query->andWhere('vw.dt_fim >= :dt_fim')
                    ->setParameter('dt_fim', $params['dt_fim']);
            }
            if ($params['dt_fiminscricao']) {
                $query->andWhere('vw.dt_fiminscricao >= :dt_fiminscricao')
                    ->setParameter('dt_fiminscricao', $params['dt_fiminscricao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_turma');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaSalaDeAula(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisaSalaDeAula');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
            $where = array();

            if ($params['st_disciplina']) {
                $where[] = 'vw.st_disciplina LIKE :st_disciplina';
                $query->setParameter('st_disciplina', '%' . $params['st_disciplina'] . '%');
            }
            if ($params['st_saladeaula']) {
                $where[] = 'vw.st_saladeaula LIKE :st_saladeaula';
                $query->setParameter('st_saladeaula', '%' . $params['st_saladeaula'] . '%');
            }
            if ($params['st_sistema']) {
                $where[] = 'vw.st_sistema LIKE :st_sistema';
                $query->setParameter('st_sistema', '%' . $params['st_sistema'] . '%');
            }

            if ($params['st_nomeprofessor'] && $params['st_nomeprofessor'] !== "") {
                $where[] = 'vw.st_nomeprofessor LIKE :st_nomeprofessor';
                $query->setParameter('st_nomeprofessor', '%' . $params['st_nomeprofessor'] . '%');
            }

            if ($where) {
                $query->andWhere(implode(" OR ", $where));
            }

            if ($params['dt_abertura_inicio'] && $params['dt_abertura_inicio'] !== '') {
                $query->andWhere('vw.dt_abertura >= :dt_abertura_inicio')
                    ->setParameter('dt_abertura_inicio', date('Y-m-d', strtotime(str_replace("/", "-", $params['dt_abertura_inicio']))));
            }
            if ($params['dt_abertura_fim'] && $params['dt_abertura_fim'] !== '') {
                $query->andWhere('vw.dt_abertura <= :dt_abertura_fim')
                    ->setParameter('dt_abertura_fim', date('Y-m-d', strtotime(str_replace("/", "-", $params['dt_abertura_fim']))));
            }

            if ($params['dt_abertura_fim'] && $params['dt_abertura_inicio']) {
                $query->andWhere('vw.dt_abertura BETWEEN :dt_abertura_inicio AND :dt_abertura_fim')
                    ->setParameter('dt_abertura_inicio', date('Y-m-d', strtotime(str_replace("/", "-", $params['dt_abertura_inicio']))))
                    ->setParameter('dt_abertura_fim', date('Y-m-d', strtotime(str_replace("/", "-", $params['dt_abertura_fim']))));
            }

            if (isset($params['id_situacao']) && $params['id_situacao'] !== "") {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if (isset($params['id_tiposaladeaula']) && $params['id_tiposaladeaula'] !== "") {
                $query->andWhere('vw.id_tiposaladeaula = :id_tiposaladeaula')
                    ->setParameter('id_tiposaladeaula', $params['id_tiposaladeaula']);
            }

            if (isset($params['id_modalidadesaladeaula']) && $params['id_modalidadesaladeaula'] !== "") {
                $query->andWhere('vw.id_modalidadesaladeaula = :id_modalidadesaladeaula')
                    ->setParameter('id_modalidadesaladeaula', $params['id_modalidadesaladeaula']);
            }

            if (!$orderBy) {
                $orderBy = array('st_saladeaula' => 'asc');
            }
            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_saladeaula');

        } catch (\Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaFormaDePagamento(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwFormaPagamento');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['st_formapagamento']) {
                $query->andWhere('vw.st_formapagamento LIKE :st_formapagamento')
                    ->setParameter('st_formapagamento', '%' . $params['st_formapagamento'] . '%');
            }
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_formapagamento');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaRegraContrato(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\ContratoRegra');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['st_contratoregra']) {
                $query->andWhere('vw.st_contratoregra LIKE :st_contratoregra')
                    ->setParameter('st_contratoregra', '%' . $params['st_contratoregra'] . '%');
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_contratoregra');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaProduto(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repositoryName = 'G2\Entity\Produto';
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            if (!isset($params['bl_ativo'])) {
                $params['bl_ativo'] = true;
            }
            $query = $this->em->createQueryBuilder()
                ->select(array('p.id_produto', 'p.st_produto', 'tp.st_tipoproduto', 's.st_situacao'))
                ->from($repositoryName, 'p')
                ->join('p.id_tipoproduto', 'tp')
                ->join('p.id_situacao', 's')
                ->where('1=1');

            if ($params['st_produto']) {
                $query->andWhere('p.st_produto LIKE :st_produto')
                    ->setParameter('st_produto', '%' . $params['st_produto'] . '%');
            }
            if ($params['id_tipoproduto']) {
                $query->andWhere('p.id_tipoproduto = :id_tipoproduto')
                    ->setParameter('id_tipoproduto', $params['id_tipoproduto']);
            }
            if ($params['id_situacao']) {
                $query->andWhere('p.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['id_entidade']) {
                $query->andWhere('p.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['bl_ativo']) {
                $query->andWhere('p.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'p.id_produto');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return mixed array
     * @throws Zend_Exception
     */
    public function retornaPesquisaCampanhaComercial(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {

            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $txtSearch = $params['txtSearch'];
            unset($params['grid'], $params['action'], $params['txtSearch'], $params['to'], $params['sort_by'], $params['order']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;

            $query = $this->em->createQueryBuilder();

            $query->add('select', 'c')
                ->add('from', 'G2\Entity\VwPesquisarCampanha c');

            foreach ($params as $key => $value) {
                if ($value) {
                    if (substr($key, 0, 3) === 'st_') {
                        $query->andWhere('c.' . $key . ' LIKE ' . '\'%' . $txtSearch . '%\'');
                    } else {
                        $query->andWhere('c.' . $key . ' = ' . $value);
                    }
                }
            }

            $results = $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'c.id_campanhacomercial');

            if (isset($results->rows))
                $entities = $results->rows;
            else
                $entities = $results;

            if ($entities) {
                foreach ($entities as &$row) {
                    $row = array(
                        'id_campanhacomercial' => $row->getId_campanhacomercial(),
                        'st_campanhacomercial' => $row->getSt_campanhacomercial(),
                        'st_finalidadecampanha' => $row->getst_finalidadecampanha(),
                        'st_tipocampanha' => $row->getSt_tipocampanha(),
                        'st_situacao' => $row->getSt_situacao()
                    );
                }
            }

            if (isset($results->rows))
                $results->rows = $entities;
            else
                $results = $entities;

            return $results;

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
        }
    }

    public function retornaPesquisaCategoria(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwCategoria');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->distinct()
                ->where('1=1');
            if ($params['id_entidadecadastro']) {
                $query->andWhere('vw.id_entidadecadastro = :id_entidadecadastro')
                    ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
            }
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            $where = array();
            if ($params['st_categoria']) {
                $where[] = "vw.st_categoria LIKE '%{$params['st_categoria']}%'";
            }
            if ($params['st_categoriapai']) {
                $where[] = "vw.st_categoriapai LIKE '%{$params['st_categoriapai']}%'";
            }
            if ($where)
                $query->andWhere(implode(" OR ", $where));

            if (!$orderBy)
                $orderBy = array('st_categoria' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_categoria');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function retornaPesquisaVendaGraduacao(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $repo = $this->em->getRepository('G2\Entity\VwClienteVenda');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['id_evolucao']) {
                $query->andWhere('vw.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['bl_contrato']) {
                $query->andWhere('vw.bl_contrato = :bl_contrato')
                    ->setParameter('bl_contrato', $params['bl_contrato']);
            }
            if ($params['st_nomecompleto']) {
                $query->andWhere('vw.st_nomecompleto LIKE :st_nomecompleto')
                    ->setParameter('st_nomecompleto', '%' . $params['st_nomecompleto'] . '%');
            }
            if ($params['id_venda']) {
                $query->andWhere('vw.id_venda = :id_venda')
                    ->setParameter('id_venda', $params['id_venda']);
            }
            if ($params['dt_inicio'] && $params['dt_fim']) {
                $query->andWhere('vw.dt_cadastro  BETWEEN :dt_inicio AND :dt_fim')
                    ->setParameter('dt_inicio', $negocio->converteDate($params['dt_inicio']) . " 00:00:00")
                    ->setParameter('dt_fim', $negocio->converteDate($params['dt_fim']) . " 23:59:59");
            }

            if ($params['dt_inicio'] && $params['dt_fim'] == '') {
                $query->andWhere('vw.dt_cadastro >= :dt_inicio')
                    ->setParameter('dt_inicio', $negocio->converteDate($params['dt_inicio']) . " 00:00:00");
            }
            if ($params['dt_fim'] && $params['dt_inicio'] == '') {
                $query->andWhere('vw.dt_cadastro <= :dt_fim')
                    ->setParameter('dt_fim', $negocio->converteDate($params['dt_fim']) . " 23:59:59");
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_venda');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     *
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function retornaPesquisaVenda(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $repo = $this->em->getRepository('G2\Entity\VwClienteVenda');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');

            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');

            $where = array();

            if (isset($params['st_nomecompleto']) && $params['st_nomecompleto']) {
                $where[] = "vw.st_nomecompleto LIKE '%" . $params['st_nomecompleto'] . "%'";
            }

            if (isset($params['st_email']) && $params['st_email']) {
                $where[] = "vw.st_email LIKE '%" . $params['st_email'] . "%'";
            }

            if (isset($params['st_cpf']) && $params['st_cpf']) {
                $where[] = "vw.st_cpf = '" . $params['st_cpf'] . "'";
            }
            if ($where)
                $query->andWhere(implode(" OR ", $where));

            if ($params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ($params['id_evolucao']) {
                $query->andWhere('vw.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['bl_contrato']) {
                $query->andWhere('vw.bl_contrato = :bl_contrato')
                    ->setParameter('bl_contrato', $params['bl_contrato']);
            }

            if ($params['id_venda']) {
                $query->andWhere('vw.id_venda = :id_venda')
                    ->setParameter('id_venda', $params['id_venda']);
            }
            if ($params['dt_inicio'] && $params['dt_fim']) {
                $query->andWhere('vw.dt_cadastro  BETWEEN :dt_inicio AND :dt_fim')
                    ->setParameter('dt_inicio', $negocio->converteDate($params['dt_inicio']) . " 00:00:00")
                    ->setParameter('dt_fim', $negocio->converteDate($params['dt_fim']) . " 23:59:59");
            }

            if ($params['dt_inicio'] && $params['dt_fim'] == '') {
                $query->andWhere('vw.dt_cadastro >= :dt_inicio')
                    ->setParameter('dt_inicio', $negocio->converteDate($params['dt_inicio']) . " 00:00:00");
            }
            if ($params['dt_fim'] && $params['dt_inicio'] == '') {
                $query->andWhere('vw.dt_cadastro <= :dt_fim')
                    ->setParameter('dt_fim', $negocio->converteDate($params['dt_fim']) . " 23:59:59");
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_venda');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function retornaPesquisaArquivoRetorno(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\ArquivoRetorno');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_entidade']) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if ($params['st_arquivoretorno']) {
                $query->andWhere('vw.st_arquivoretorno LIKE :st_arquivoretorno')
                    ->setParameter('st_arquivoretorno', '%' . $params['st_arquivoretorno'] . '%');
            }

            if (!$orderBy)
                $orderBy = array('dt_cadastro' => 'ASC');

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_arquivoretorno');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     *
     * @param array $params
     * @return array
     * @throws Zend_Exception
     */
    public function retornaPesquisaAplicadorProva(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAplicadorProva');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);

            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }

            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if ($params['id_tipopessoa']) {
                $query->andWhere('vw.id_tipopessoa = :id_tipopessoa')
                    ->setParameter('id_tipopessoa', $params['id_tipopessoa']);
            }

            if ($params['id_entidadecadastro']) {
                $query->andWhere('vw.id_entidadecadastro = :id_entidadecadastro')
                    ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
            }

            if ($params['st_aplicadorprova']) {
                $query->andWhere('vw.st_aplicadorprova LIKE :st_aplicadorprova')
                    ->setParameter('st_aplicadorprova', '%' . $params['st_aplicadorprova'] . '%');
            }
            $query->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1);

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_aplicadorprova');

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /*
     * Método que retorna todos os Estados (Uf)
     * Retorna Sg_Uf e St_Uf (ex.: MG - Minas Gerais)
     * <debora.castro@unyleya.com.br>
     * AC-814
     */

    public function retornarSelectUf()
    {
        $repo = $this->em->getRepository('G2\Entity\Uf');

        $result = $repo->findAll();
        $arrayRetorno = array();
        if ($result) {
            foreach ($result as $index => $row) {
                $arrayRetorno[$index]['label'] = $row->getSt_uf();
                $arrayRetorno[$index]['valor'] = $row->getSg_uf();
            }
        }
        return $arrayRetorno;
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function retornaLocaisProva(array $params = array())
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAplicador');

            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = array('DISTINCT vw.id_aplicadorprova', 'vw.st_aplicadorprova');

            if (isset($params['sg_uf']) && $params['sg_uf'] != '') {
                $arrColumns[] = 'vw.sg_uf';
            }
            $arDadosLocaisProva = array();
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');
            if (isset($params['sg_uf']) && $params['sg_uf']) {
                $query->andWhere('vw.sg_uf = :sg_uf')
                    ->setParameter('sg_uf', $params['sg_uf']);
            }
            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);

            $query->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1);

            $result = $query->getQuery()->getResult();
            if ($result) {
                foreach ($result as $index => $objeto) {
                    $arDadosLocaisProva[$index]['st_text'] = $objeto['st_aplicadorprova'];
                    $arDadosLocaisProva[$index]['id_value'] = $objeto['id_aplicadorprova'];
                }
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
        return $arDadosLocaisProva;
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function retornaDataProvaAplicacao(array $params = array())
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\AvaliacaoAplicacao');
            $arDatasProva = array();
            $query = $repo->createQueryBuilder("vw")
                ->select('vw.id_avaliacaoaplicacao', 'vw.dt_aplicacao')
                ->where('1=1');
            $query->andWhere('vw.id_aplicadorprova = :id_aplicadorprova')
                ->setParameter('id_aplicadorprova', $params['id_aplicadorprova']);

            $query->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1);

            $query->andWhere('vw.dt_aplicacao is not null');

            $query->andWhere('vw.dt_aplicacao >= :dt_aplicacao')
                ->setParameter('dt_aplicacao', date('Y-m-d'));
            $result = $query->getQuery()->getResult();
            if ($result) {
                foreach ($result as $index => $objeto) {
                    $arDatasProva[$index]['st_text'] = date_format($objeto['dt_aplicacao'], 'd/m/Y');
                    $arDatasProva[$index]['id_value'] = date_format($objeto['dt_aplicacao'], 'd/m/Y'); //$objeto ['id_avaliacaoaplicacao'];
                }
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
        return $arDatasProva;
    }

    /**
     * @param array $params
     */
    public function retornaPesquisaConcurso(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {


            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');
            unset($params['grid'], $params['action'], $params['txtSearch']);

            $repo = $this->em->getRepository('G2\Entity\Concurso');
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('1=1');

            if ($params['st_concurso']) {
                $query->andWhere("vw.st_concurso LIKE :st_concurso")
                    ->setParameter('st_concurso', "%{$params['st_concurso']}%");
            }
            if (array_key_exists('id_situacao', $params) && $params['id_situacao']) {
                $query->andWhere("vw.id_situacao = :id_situacao")
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_concurso');

        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }


    public function retornaPesquisaMensagemPortal(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        $negocio = new \G2\Negocio\Negocio();
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisaMensagemPortal');
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], 'vw');

            //Monta a querybuilder
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns)
                ->where('vw.id_entidade=' . $negocio->sessao->id_entidade);

            $where = array();

            if (isset($params['st_mensagem']) && !empty($params['st_mensagem'])) {
                $where[] = "vw.st_mensagem LIKE '%" . $params['st_mensagem'] . "%' ";
            }

            if (isset($params['st_projetopedagogico']) && !empty($params['st_projetopedagogico'])) {
                $where[] = "vw.st_projetopedagogico LIKE '%" . $params['st_projetopedagogico'] . "%' ";
            }

            if ($where) {
                $query->andWhere(implode(" OR ", $where));
            }

            return $this->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_mensagem');

        } catch (Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que prepara a paginação para as pesquisas
     * @param $queryBuilder
     * @param array|null $order
     * @param int $limit
     * @param int $offset
     * @param bool|false $pk
     * @return object
     */
    public function preparaPaginacaoOpicional($queryBuilder, array $order = null, $limit = 0, $offset = 0, $pk = false)
    {
        // Se possuir o parametro LIMIT, sera paginado.
        if ($limit && $pk) {
            // Calcula o total a partir da mesma query, SEM ORDER BY e LIMITS
            $query_total = clone $queryBuilder;
            $query_total->select("COUNT(DISTINCT {$pk}) as num_rows");
            $query_total->orderBy('num_rows', 'ASC');

            // Trata a exception retornada pelo getSingleScalarResult() caso não retorne um único valor escalar
            try {
                $query_total = $query_total->getQuery()->getSingleScalarResult();
            } catch (Exception $e) {
                $query_total = 0;
            }

            // PAGINACAO
            $queryBuilder->setFirstResult($offset);
            $queryBuilder->setMaxResults($limit);

            $page = round($offset / $limit) + 1;
        }

        // Prepara Order By
        if (is_array($order) && $order) {
            $prefix = strstr($pk, '.', true) . '.';
            $first = true;
            foreach ($order as $attr => $value) {
                if (!$value)
                    $value = 'ASC';

                if ($first) {
                    $first = false;
                    $queryBuilder->orderBy($prefix . $attr, $value);
                } else
                    $queryBuilder->addOrderBy($prefix . $attr, $value);
            }
        }
        $rows = $queryBuilder->getQuery()->getResult();
        if ($limit && $pk) {
            return (object)array(
                'offset' => (int)$offset,
                'limit' => (int)$limit,
                'page' => $page,
                'total' => (int)$query_total,
                'rows' => $rows
            );
        } else {
            return $rows;
        }
    }

}

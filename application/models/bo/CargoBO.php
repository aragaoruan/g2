<?php
/**
 * Classe com regras de negócio para interações de Cargos
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @since 26/02/2013
 * 
 * @package models
 * @subpackage bo
 */
class CargoBO extends Ead1_BO{
	
	public $mensageiro;
	public $dao;
	
	public function __construct(){
		parent::__construct();
		$this->mensageiro = new Ead1_Mensageiro();
		$this->dao = new CargoDAO();
	}
	
	/**
	 * Metodo que retorna cargos
	 * 
	 * @param CargoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarCargo(CargoTO $to){
		try{
			$retorno = $this->dao->retornarCargo($to);
			if(empty($retorno)){
				$this->mensageiro->setMensageiro('Nenhum Cargo Encontrado!',Ead1_IMensageiro::AVISO);
			}
			$this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($retorno, new CargoTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Exception $e){
			$this->mensageiro->setMensageiro('Erro ao Retornar Cargo',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $this->mensageiro;
	}
}
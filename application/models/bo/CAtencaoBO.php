<?php

/**
 * @author Denise
 * Classe de regra de negócios para Central de Atenção - Gestor2
 *
 */
class CAtencaoBO extends PortalCAtencaoBO
{
    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Método que retorna os núcleos e as funções de CA que o usuário logado tenha nos núcleos
     * @param VwNucleoPessoaCoTO $vwnucleopessoa
     * @return Ead1_Mensageiro
     */
    public function retornaNucleoFuncoesCA(VwNucleoPessoaCoTO $vwnucleopessoa)
    {
        if (!$vwnucleopessoa->getId_entidade() || !$vwnucleopessoa->getId_usuario()) {
            $this->mensageiro->setMensageiro('O id_entidade ou o id_usuario são obrigatórios e não foram passados!', Ead1_IMensageiro::AVISO);
        } else {
            $dao = new CAtencaoDAO();
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornaNucleoFuncoesCA($vwnucleopessoa), new VwNucleoPessoaCoTO()));
        }
        return $this->mensageiro;
    }

    /**
     * Retorna as interações de uma ocorrencia
     * @param VwOcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     */
    public function retornaInteracoesOcorrencia(VwOcorrenciaTO $ocorrencia)
    {
        $negocio = new \G2\Negocio\Negocio();
        try {
            $resultado = $negocio->findBy('G2\Entity\VwOcorrenciaInteracao', array('id_ocorrencia' => $ocorrencia->getId_ocorrencia()));

            if (!empty($resultado)){
                $this->mensageiro->setMensageiro($resultado, Ead1_IMensageiro::SUCESSO);
            }
            if ($this->mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                $this->mensageiro->setMensageiro('Nenhuma ocorrência encontrada', Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Interações', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Retorna atendentes [Lista de pessoa vinculadas a aquele assunto com a função Atendente(8) ou Atendente de Setor(10) .
     * @param VwNucleoPessoaCoTO $vwnucleopessoa
     * @return Ead1_Mensageiro
     */
    public function retornaAtendente(VwNucleoPessoaCoTO $vwnucleopessoa)
    {
        try {
            if (!$vwnucleopessoa->getId_entidade()) {
                $vwnucleopessoa->setId_entidade($vwnucleopessoa->getSessao()->id_entidade);
            }
            if (!$vwnucleopessoa->getId_usuario()) {
                $vwnucleopessoa->setId_usuario($vwnucleopessoa->getSessao()->id_usuario);
            }
            $dao = new CAtencaoDAO();
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornaAtendentes($vwnucleopessoa), new VwNucleoPessoaCoTO ()), Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Metodo que retorna ocorrências pendentes para o usuário.
     * @param VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwOcorrenciaAguardando(VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO)
    {
        try {
            if (!$vwOcorrenciaAguardandoTO->getId_usuarioresponsavel()) {
                $vwOcorrenciaAguardandoTO->setId_usuarioresponsavel($vwOcorrenciaAguardandoTO->getSessao()->id_usuario);
            }
            $dao = new CAtencaoDAO();
            $campos = $dao->retornarVwOcorrenciaAguardando($vwOcorrenciaAguardandoTO);
            if (is_object($campos)) {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campos->toArray(), new VwOcorrenciaAguardandoTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar ocorrências pendentes!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Metodo que retorna ocorrências para o usuário.
     * @param VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwOcorrenciasPessoa(VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO)
    {
        try {
            if (!$vwOcorrenciasPessoaTO->getId_usuario()) {
                $vwOcorrenciasPessoaTO->setId_usuario($vwOcorrenciasPessoaTO->getSessao()->id_usuario);
            }
            $dao = new CAtencaoDAO();
            $campos = $dao->retornarVwOcorrenciasPessoa($vwOcorrenciasPessoaTO);
            if (is_object($campos)) {
                $campos = $campos->toArray();
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($campos, new VwOcorrenciasPessoaTO()), Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao retornar ocorrências desse responsável!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Retorna AssuntosPai para G2. $id_nucleoco é obrigatorio
     * @param VwNucleoAssuntoCoTO $assunto
     * @return Ead1_Mensageiro
     */
    public function retornaAssuntoPai(VwNucleoAssuntoCoTO $assunto)
    {
        try {
            if (!$assunto->getId_nucleoco()) {
                $this->mensageiro->setMensageiro('O id_nucleoco é obrigatório e não foi passados!', Ead1_IMensageiro::AVISO);
            } else {
                $dao = new NucleoAssuntoCoDAO();
                $assunto->setId_entidade($assunto->getSessao()->id_entidade);
                $assunto->setBl_ativo(true);
                $this->mensageiro->setMensageiro($assunto->encapsularTo($dao->retornaVwNucleoAssuntoPai($assunto), $assunto), Ead1_IMensageiro::SUCESSO);
            }

        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar AssuntoPai.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * Método que retorna as evoluções vinculadas a função de um atendente no nucleo e a evolucao da ocorrência detalhada
     * @param VwOcorrenciaEvolucaoTO $vwocorrenciaevolucao
     * @return Ead1_Mensageiro
     */
    public function retornaEvolucoesAtendimento(VwOcorrenciaEvolucaoTO $vwocorrenciaevolucao)
    {
        if (!$vwocorrenciaevolucao->getId_ocorrencia() || !$vwocorrenciaevolucao->getId_funcao()) {
            $this->mensageiro->setMensageiro("O id_ocorrencia e/ou o id_funcao não foram setados.", Ead1_IMensageiro::AVISO);
        } else {
            $dao = new CAtencaoDAO();
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->listarEvolucoesAtendimentoOcorrencia($vwocorrenciaevolucao), new VwOcorrenciaEvolucaoTO()));
        }
        return $this->mensageiro;
    }


    /**
     * Método que retorna as ocorrências
     * Obrigatórios: id_usuario, id_funcao e id_nucleoco
     * @param VwOcorrenciasPessoaTO $vwocorrenciaspessoa
     * @return Ead1_Mensageiro
     */
    public function retornarOcorrenciasPessoa(VwOcorrenciasPessoaTO $vwocorrenciaspessoa, $dt_inicial = null, $dt_final = null)
    {
        $dao = new CAtencaoDAO();
        if (!$vwocorrenciaspessoa->getId_usuario() || !$vwocorrenciaspessoa->getId_nucleoco()) {
            $this->mensageiro->setMensageiro('O id_usuario e/ou id_nucleoco são obrigatórios e não foram passados!', Ead1_IMensageiro::AVISO);
        } else {
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->listarOcorrenciasPessoa($vwocorrenciaspessoa, $dt_inicial, $dt_final), new VwOcorrenciasPessoaTO()), Ead1_IMensageiro::SUCESSO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que detalha uma Ocorrencia
     * O método verifica se a funcão é de Atendente (8), caso seja, verifica se a ocorrência já tem atendente.
     * Se não tiver, seta o usuario logado como atendente da mesma
     * @param VwOcorrenciasPessoaTO $to
     * @return Ead1_Mensageiro
     */
    public function detalharOcorrencia(VwOcorrenciasPessoaTO $to)
    {
        if ($to->getId_funcao() == FuncaoTO::ATENDENTE_CA && $to->getId_situacao() == OcorrenciaTO::SITUACAO_PENDENTE) {
            $to->fetch(false, true, true);
// 			if(!$to->getId_atendente()){
            $ocorrenciaresponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaresponsavel->setId_ocorrencia($to->getId_ocorrencia());
            $ocorrenciaresponsavel->fetch(false, true, true);
            $dao = new CAtencaoDAO();
            if (!$ocorrenciaresponsavel->getId_ocorrenciaresponsavel()) {
                $ocorrenciaresponsavel->setId_usuario($to->getSessao()->id_usuario);
                $ocorrenciaresponsavel->setBl_ativo(true);
                $dao->vincularResponsavelOcorrencia($ocorrenciaresponsavel);
            }

            $ocorrencia = new OcorrenciaTO();
            $ocorrencia->setId_ocorrencia((integer)$to->getId_ocorrencia());
            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_SENDO_ATENDIMENTO);
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_EM_ANDAMENTO);

            $dao->updateOcorrencia($ocorrencia);
        }
// 		}
        $vwocorrencia = new VwOcorrenciasPessoaTO();
        $vwocorrencia->setId_ocorrencia((integer)$to->getId_ocorrencia());
        return $this->mensageiro->setMensageiro($vwocorrencia->fetch(false, true, true), Ead1_IMensageiro::SUCESSO);
    }


    /**
     * Retorna atendentes de acordo com o assunto para encaminhar uma ocorência
     * (todos que tem a funcao de atendente ou atendente de setor para o assunto selecionado)
     * @param VwNucleoPessoaCoTO $nucleoPessoa
     * @return Ead1_Mensageiro
     */
    public function retornaAtendentesEncaminharOcorrencia(VwNucleoPessoaCoTO $nucleoPessoa)
    {

        $configTO = new ConfiguracaoEntidadeTO();
        $configTO->setId_entidade($nucleoPessoa->getSessao()->id_entidade);
        $configTO->fetch(true, true, true);

        $nucleoPessoa->setId_entidade(($nucleoPessoa->getId_entidade() ? $nucleoPessoa->getId_entidade() : $configTO->getId_entidade()));
        $nucleoPessoa->setId_assuntoco(($nucleoPessoa->getId_assuntoco() ? $nucleoPessoa->getId_assuntoco() : $configTO->getId_assuntoresgate()));

        if (!$nucleoPessoa->getId_assuntoco()) {
            $this->mensageiro->setMensageiro('O id_assuntoco é obrigatório e não foi setado', Ead1_IMensageiro::AVISO);
        } else {
            $dao = new CAtencaoDAO();
            $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dao->retornaAtendentesEncaminharOcorrencia($nucleoPessoa), new VwNucleoPessoaCoTO()), Ead1_IMensageiro::SUCESSO);
        }
        return $this->mensageiro;
    }

    /**
     * Método que encaminha uma ocorrência - Grava um trâmite para essa alteração
     * @param VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo
     * @return Ead1_Mensageiro
     * @deprecated new \G2\Negocio\CAtencao::encaminharOcorrencia
     */
    public function encaminharOcorrencia(VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo)
    {
        try {
            $dao = new CAtencaoDAO();
            $dao->beginTransaction();
            $negocio = new \G2\Negocio\Ocorrencia();
            /**
             * @var G2\Entity\Usuario $pessoa
             */
            $pessoa = $negocio->getReference('G2\Entity\Usuario', $vwOcorrenciaPessoaTo->getId_atendente());

            $ocorrenciaresponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaresponsavel->setId_ocorrencia($vwOcorrenciaPessoaTo->getId_ocorrencia());
            $ocorrenciaresponsavel->setBl_ativo(true);
            $ocorrenciaresponsavel->fetch(false, true, true);
            if ($ocorrenciaresponsavel->getId_ocorrenciaresponsavel()) {
                /** @var \G2\Entity\OcorrenciaResponsavel $ocorrenciaResp */
                $ocorrenciaResp = $negocio->getReference('G2\Entity\OcorrenciaResponsavel', $ocorrenciaresponsavel->getId_ocorrenciaresponsavel());
            }

            //Seta o atual atendente como inativo
            $negocio->deletarOcorrenciaResponsavel(array('id_ocorrencia' => $vwOcorrenciaPessoaTo->getId_ocorrencia()));


            $ocorrenciaresponsavelAdicionar = new \G2\Entity\OcorrenciaResponsavel();
            $ocorrenciaresponsavelAdicionar->setId_usuario($pessoa);
            $ocorrenciaresponsavelAdicionar->setId_ocorrencia($vwOcorrenciaPessoaTo->getId_ocorrencia());
            $ocorrenciaresponsavelAdicionar->setBl_ativo(true);
            $ocorrenciaresponsavelAdicionar->setDt_cadastro(new DateTime());
            $negocio->save($ocorrenciaresponsavelAdicionar);

            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada
             * sera gerado um tramite.
             */
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoAtual = $arraySituacao[(integer)$vwOcorrenciaPessoaTo->getId_situacao()];
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_EM_ANDAMENTO];
            $stEvolucaoAtual = $arrayEvolucao[(integer)$vwOcorrenciaPessoaTo->getId_evolucao()];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::AGUARDANDDO_ATENDENTE];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova/*, $vwOcorrenciaPessoaTo->getSessao()->nomeUsuario*/);

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova/*, $vwOcorrenciaPessoaTo->getSessao()->nomeUsuario*/);

            //Criando texto para tramite
            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
            //Fim Regra

            //Salva trâmite para o encaminhar
            $ocorrencia = new OcorrenciaTO();
            $tramite = new TramiteTO();

            $msgAssunto = null;
            $stTramiteEncaminhar = null;
            $assuntoco = $negocio->getReference('G2\Entity\AssuntoCo', $vwOcorrenciaPessoaTo->getId_assuntoco());
            if ($assuntoco instanceof \G2\Entity\AssuntoCo) {
                $stAssuntoPaiAtual = $vwOcorrenciaPessoaTo->getSt_assuntopai();
                $stAssuntoAtual = $vwOcorrenciaPessoaTo->getSt_assuntoco();
                $stAssuntoPaiNovo = $assuntoco->getId_assuntocopai()->getSt_assuntoco();
                $stAssuntoNovo = $assuntoco->getSt_assuntoco();
                $stAtentendeAtual = (isset($ocorrenciaResp) && $ocorrenciaResp->getId_usuario() ? $ocorrenciaResp->getId_usuario()->getSt_nomecompleto() : ' Não distribuído');
                $stAtendenteNovo = $pessoa->getSt_nomecompleto();

                $stTramiteEncaminhar = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_ASSUNTO_ENCAMINHAR,
                    $stAssuntoPaiAtual, $stAssuntoAtual, $stAssuntoPaiNovo, $stAssuntoNovo, $stAtentendeAtual, $stAtendenteNovo);
                //Criando texto para tramite
            }

            $ocorrencia->setId_ocorrencia($vwOcorrenciaPessoaTo->getId_ocorrencia());
            $tramite->setBl_visivel(true);
            $tramite->setId_entidade($ocorrencia->getSessao()->id_entidade);
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
            $tramite->setId_usuario($ocorrencia->getSessao()->id_usuario);
//			$tramite->setSt_tramite('Ocorrência N. '.$ocorrencia->getId_ocorrencia().' encaminhada para o assunto '.$vwOcorrenciaPessoaTo->getSt_assuntopai().', sub-assunto '.$vwOcorrenciaPessoaTo->getSt_assuntoco().' e atendente '.$vwOcorrenciaPessoaTo->getSt_atendente().', '.$stTramite);
            $tramite->setSt_tramite('Ocorrência N. ' . $ocorrencia->getId_ocorrencia() . $stTramiteEncaminhar . '. ' . $stTramite);
            $this->novaInteracao($ocorrencia, $tramite);


            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_AGUARDANDO_ATENDENTE);
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_EM_ANDAMENTO);

            //atualiza ocorrencia para novo assunto
            $ocorrencia->setId_assuntoco($vwOcorrenciaPessoaTo->getId_assuntoco());

            $this->alterarOcorrencia($ocorrencia);

            $dao->commit();
            $this->mensageiro->setMensageiro('Ocorrência encaminhada com sucesso', Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Ocorreu um erro: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;

    }

    /**
     * Método que retorna o combo de Categoria da Ocorrência (tipo)
     * @return Ead1_Mensageiro
     */
    public function retornarComboCategoriaOcorrencia()
    {
        try {
            $combo = array();

            $toTipo = new CategoriaOcorrenciaTO();
            $toTipo->setBl_ativo(true);
            $toTipo->setId_entidadecadastro($toTipo->getSessao()->id_entidade);
            $mensageiro = new Ead1_Mensageiro($toTipo->fetch());
            $mensageiro->subtractMensageiro();
            $dadosMensagem = $mensageiro->getMensagem();

            foreach ($dadosMensagem as $obj) {
                $combo[$obj->id_categoriaocorrencia]['label'] = $obj->st_categoriaocorrencia;
                $combo[$obj->id_categoriaocorrencia]['value'] = $obj->id_categoriaocorrencia;
            }

            $this->mensageiro->setMensageiro($combo, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), Ead1_IMensageiro::AVISO);
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro("Erro ao Retornar Módulos.", Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $this->mensageiro;
    }

    /**
     * Método que gera cópia da Ocorrnência- Grava Trâmite
     * @param VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo
     * @return Ead1_Mensageiro
     * @todo Adicionar o metodo que grava o atendente da ocorrencia copiada
     */
    public function gerarCopiaOcorrencia(VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo)
    {
// 		Zend_Debug::dump($vwOcorrenciaPessoaTo,__CLASS__.'('.__LINE__.')');exit;
        try {
            $dao = new CAtencaoDAO();
            $dao->beginTransaction();

            $ocorrencia = new OcorrenciaTO();
            $ocorrencia->setId_categoriaocorrencia($vwOcorrenciaPessoaTo->getId_categoriaocorrencia());
            $ocorrencia->setId_assuntoco($vwOcorrenciaPessoaTo->getId_assuntoco());
            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_AGUARDANDO_ATENDENTE);
            $ocorrencia->setId_matricula($vwOcorrenciaPessoaTo->getId_matricula());
            $ocorrencia->setId_ocorrenciaoriginal($vwOcorrenciaPessoaTo->getId_ocorrencia());
            $ocorrencia->setId_saladeaula($vwOcorrenciaPessoaTo->getId_saladeaula());
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_EM_ANDAMENTO);
            $ocorrencia->setId_usuariointeressado($vwOcorrenciaPessoaTo->getId_usuariointeressado());
            $ocorrencia->setSt_ocorrencia($vwOcorrenciaPessoaTo->getSt_ocorrencia());
            $ocorrencia->setSt_titulo($vwOcorrenciaPessoaTo->getSt_titulo());

            //Grava a nova ocorrencia
            $mensageiro = $this->salvarOcorrencia($ocorrencia);
            $ocorrencia = $mensageiro->getFirstMensagem();

            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada
             * sera gerado um tramite.
             */
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoAtual = $arraySituacao[$vwOcorrenciaPessoaTo->getId_situacao()];
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_EM_ANDAMENTO];
            $stEvolucaoAtual = $arrayEvolucao[$vwOcorrenciaPessoaTo->getId_evolucao()];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::AGUARDANDDO_ATENDENTE];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova, $vwOcorrenciaPessoaTo->getSessao()->nomeUsuario);

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova, $vwOcorrenciaPessoaTo->getSessao()->nomeUsuario);

            //Criando texto para tramite
            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
            //Fim Regra

            //Salva trâmite - duplicação da ocorrência
            $tramite = new TramiteTO();
            $tramite->setBl_visivel(true);
            $tramite->setId_entidade($vwOcorrenciaPessoaTo->getSessao()->id_entidade);
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
            $tramite->setId_usuario($vwOcorrenciaPessoaTo->getSessao()->id_usuario);
            $tramite->setSt_tramite('Ocorrência gerada a partir da ocorrência N. ' . $vwOcorrenciaPessoaTo->getId_ocorrencia() . ', ' . $stTramite);
            $this->salvaTramite($ocorrencia, $tramite);

            // @todo Adicionar o metodo que grava o atendente da ocorrencia copiada
            //Seta o atual atendente como inativo
            $ocorrenciaresponsavelatual = new OcorrenciaResponsavelTO();
            $ocorrenciaresponsavelatual->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $ocorrenciaresponsavelatual->setBl_ativo(true);
            $ocorrenciaresponsavelatual->fetch(false, true, true);
            $ocorrenciaresponsavelatual->setBl_ativo(false);
            //Atualiza antigo responsável para bl_ativo->false

            if ($ocorrenciaresponsavelatual->getId_ocorrenciaresponsavel()) {
                $dao->updateResponsavelOcorrencia($ocorrenciaresponsavelatual);
            }

            $ocorrenciaresponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaresponsavel->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $ocorrenciaresponsavel->setId_usuario($vwOcorrenciaPessoaTo->getId_atendente());
            $ocorrenciaresponsavel->setBl_ativo(true);
            $dao->vincularResponsavelOcorrencia($ocorrenciaresponsavel);

            $dao->commit();
            $this->mensageiro->setMensageiro('Ocorrência copiada com sucesso! ', Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Ocorreu um erro: ' . $e, Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Método que salva interações da ocorrência para o flex
     * @param OcorrenciaTO $ocorrencia
     * @param TramiteTO $tramite
     * @param ArquivoTO $arquivo
     * @param bool $bl_notificacao
     * @param bool $bl_interessado
     * @param int $sistema
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function novaInteracao(OcorrenciaTO $ocorrencia, TramiteTO $tramite, $arquivo = null, $bl_notificacao = false, $bl_interessado = false, $sistema = SistemaTO::GESTOR)
    {
        try {
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_MANUAL);
            $retorno = $this->salvaTramite($ocorrencia, $tramite, $arquivo, $sistema, $bl_notificacao, $bl_interessado);
            return $retorno;
        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao tentar realizar nova interacao: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Gera interações para uma ocorrência baseado em um array de ocorrências
     * @param array $arrOcorrencias
     * @param TramiteTO $tramiteTO
     * @return Ead1_Mensageiro
     */
    public function gerarInteracoesOcorrencias(array $arrOcorrencias, TramiteTO $tramiteTO)
    {
        try {

            if (!$arrOcorrencias) {
                throw new Zend_Exception("Nenhuma ocorrencia passada como parametro.");
            }

            $textoTramite = $tramiteTO->getSt_tramite();


            if ($arrOcorrencias) {
                foreach ($arrOcorrencias as $ocorrenciaTO) {
                    $tramiteTO->setSt_tramite($textoTramite . " " . $ocorrenciaTO->getId_ocorrencia());
                    $mensageiro = $this->novaInteracao($ocorrenciaTO, $tramiteTO);
                    if ($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception($mensageiro->getText());
                    }
                }
                $this->mensageiro->setMensageiro('Interacao(oes) gerada(s) com sucesso.', Ead1_IMensageiro::SUCESSO);
            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao tentar realizar interacao: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }

    /**
     * Método que devolve uma ocorrência para a distribuição
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     */
    public function devolverOcorrencia(OcorrenciaTO $ocorrencia, $strTramite = null)
    {
        try {
            $dao = new CAtencaoDAO();
            $dao->beginTransaction();

            //inativa o atendente da ocorrência
            $ocorrenciaresponsavel = new OcorrenciaResponsavelTO();
            $ocorrenciaresponsavel->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $ocorrenciaresponsavel->setBl_ativo(true);
            $ocorrenciaresponsavel->fetch(false, true, true);
            $ocorrenciaresponsavel->setBl_ativo(false);
            if ($ocorrenciaresponsavel->getId_ocorrenciaresponsavel()) {
                $dao->updateResponsavelOcorrencia($ocorrenciaresponsavel);
            }

            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada
             * sera gerado um tramite.
             */
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoAtual = $arraySituacao[$ocorrencia->getId_situacao()];
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE];
            $stEvolucaoAtual = $arrayEvolucao[$ocorrencia->getId_evolucao()];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::DEVOLVIDO_DISTRIBUICAO];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova, $ocorrencia->getSessao()->nomeUsuario);

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova, $ocorrencia->getSessao()->nomeUsuario);

            //Criando texto para tramite
            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
            //Fim Regra

            //Altera status da ocorrência
            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_DEVOLVIDO_PARA_DISTRIBUICAO);
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_PENDENTE);
            $dao->updateOcorrencia($ocorrencia);

            //GravaTrâmite
            $tramite = new TramiteTO();
            $tramite->setBl_visivel(false);
            $tramite->setId_entidade($ocorrencia->getSessao()->id_entidade);
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
            $tramite->setId_usuario($ocorrencia->getSessao()->id_usuario);
            $tramite->setSt_tramite('Ocorrencia N. ' . $ocorrencia->getId_ocorrencia() . ' devolvida por ' . $ocorrencia->getSessao()->nomeUsuario . ', ' . $stTramite);

            //Se houver um st_tramite passado, substitui no TramiteTO()
            if ($strTramite) {
                $tramite->setSt_tramite($strTramite);
            }

            $this->novaInteracao($ocorrencia, $tramite);

            $dao->commit();
            $this->mensageiro->setMensageiro('Ocorrência devolvida com sucesso! ', Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Ocorreu um erro ao devolver a ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;

    }


    /**
     * Método que encerra uma ocorrência
     * @param OcorrenciaTO $ocorrencia
     * @param null $arquivo
     * @param \G2\Utils\User|null $userObj
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function encerrarOcorrencia(OcorrenciaTO $ocorrencia, $arquivo = null, \G2\Utils\User $userObj = null)
    {
        try {

            $dao = new CAtencaoDAO();
            $dao->beginTransaction();
            if (!$ocorrencia->getId_ocorrencia()) {
                throw new Zend_Exception("Id da ocorrencia não informado.");
            }

            //Guardando valores para reutiliza-los na geracao do texto do tramite
            $evolucaoOcorrencia = \G2\Constante\Evolucao::getArrayOcorrencia();
            $situacaoOcorrencia = \G2\Constante\Situacao::getArrayOcorrencia();
            $stSituacaoAtual = null;
            $stEvolucaoAtual = null;
            if (is_array($situacaoOcorrencia)
                && is_object($ocorrencia)
                && $ocorrencia->getId_situacao()
                && !is_object($ocorrencia->getId_situacao())
            ) {
                $stSituacaoAtual = $situacaoOcorrencia[(integer)$ocorrencia->getId_situacao()];
            }

            if (is_array($evolucaoOcorrencia)
                && is_object($ocorrencia)
                && $ocorrencia->getId_evolucao()
                && !is_object($ocorrencia->getId_evolucao())
            ) {
                $stEvolucaoAtual = $evolucaoOcorrencia[(integer)$ocorrencia->getId_evolucao()];
            }
            //Fim

            if ($ocorrencia->getId_usuariointeressado() == $ocorrencia->getSessao()->id_usuario
                || (isset($userObj) && !is_null($userObj) && $ocorrencia->getId_usuariointeressado() == $userObj->getuser_id())
            ) {
                $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_ENCERRAMENTO_PELO_INTERESSADO);
                $stEvolucaoNova = $evolucaoOcorrencia[\G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO];
            } else {
                $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_ENCERRAMENTO_PELO_ATENDENTE);
                $stEvolucaoNova = $evolucaoOcorrencia[\G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_ATENDENTE];
            }
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_ENCERRADA);


            $dao->updateOcorrencia($ocorrencia);


            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada
             * sera gerado um tramite.
             */
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $stSituacaoNova = $situacaoOcorrencia[\G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova, $this->sessao->nomeUsuario);

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova, $this->sessao->nomeUsuario);

            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;


            $userNameSessao = $ocorrencia->getSessao()->nomeUsuario ? $ocorrencia->getSessao()->nomeUsuario : $userObj->getuser_name();
            $userIdSessao = $ocorrencia->getSessao()->id_usuario ? $ocorrencia->getSessao()->id_usuario : $userObj->getuser_id();
            $userEntidadeSessao = $ocorrencia->getSessao()->id_entidade ? $ocorrencia->getSessao()->id_entidade : $userObj->getentidade_id();

            //GravaTrâmite
            $tramite = new TramiteTO();
            $tramite->setBl_visivel(true);
            $tramite->setId_entidade($userEntidadeSessao);
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
            $tramite->setId_usuario($userIdSessao);


            $tramite->setSt_tramite("Ocorrencia N. {$ocorrencia->getId_ocorrencia()} encerrada por {$userNameSessao}, {$stTramite}");
            $resultTramite = $this->salvaTramite($ocorrencia, $tramite, null, \G2\Constante\Sistema::GESTOR, false, false);

            if ($resultTramite->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new Exception("Erro ao tentar salvar tramite. " . $resultTramite->getText());
            }

            $this->mensageiro = $this->enviaNotificacaoOcorrencia($ocorrencia, true, $userEntidadeSessao);

            $dao->commit();
            $this->mensageiro->setMensageiro('Ocorrência encerrada com sucesso!', Ead1_IMensageiro::SUCESSO);

        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Ocorreu um erro ao encerrar a ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }


    public function reabrirOcorrencia(VwOcorrenciasPessoaTO $vwto)
    {
        try {
            $dao = new CAtencaoDAO();
            $dao->beginTransaction();

            if (!$vwto->getId_ocorrencia()) {
                throw  new Zend_Exception('O id_ocorrencia é obrigatório e não foi passado!');
            }

            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada
             * sera gerado um tramite.
             */
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoAtual = $arraySituacao[$vwto->getId_situacao()];
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_EM_ANDAMENTO];
            $stEvolucaoAtual = $arrayEvolucao[$vwto->getId_evolucao()];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::SENDO_ATENDIDO];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova, ($this->sessao->nomeUsuario ? $this->sessao->nomeUsuario : $vwto->getSt_nomeinteressado()));

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova, ($this->sessao->nomeUsuario ? $this->sessao->nomeUsuario : $vwto->getSt_nomeinteressado()));

            //Criando texto para tramite
            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
            //Fim Regra

            $ocorrencia = new OcorrenciaTO();
            $ocorrencia->setId_ocorrencia($vwto->getId_ocorrencia());
            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_SENDO_ATENDIMENTO);
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_EM_ANDAMENTO);

            $dao->updateOcorrencia($ocorrencia);

            //GravaTrâmite
            $tramite = new TramiteTO();
            $tramite->setBl_visivel(true);
            $tramite->setId_entidade($ocorrencia->getSessao()->id_entidade ? $ocorrencia->getSessao()->id_entidade : $ocorrencia->getId_entidade());
            $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
            $tramite->setId_usuario($ocorrencia->getSessao()->id_usuario ? $ocorrencia->getSessao()->id_usuario : $ocorrencia->getId_usuariointeressado());
            $tramite->setSt_tramite('Ocorrencia N. ' . $ocorrencia->getId_ocorrencia() . ' reaberta por ' . ($ocorrencia->getSessao()->nomeUsuario ? $ocorrencia->getSessao()->nomeUsuario : $vwto->getSt_nomeinteressado()) . ', ' . $stTramite);
            $this->salvaTramite($ocorrencia, $tramite);

            $dao->commit();
            $this->mensageiro->setMensageiro('Ocorrência reaberta com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Ocorreu um erro ao reabrir a ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }

    /**
     * Método que decide se salva ou edita uma ocorrência
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     */
    public function salvarOcorrencias(OcorrenciaTO $ocorrencia)
    {
        if (!$ocorrencia->getId_ocorrencia()):
            return $this->salvarOcorrencia($ocorrencia);
        else:
            return $this->alterarOcorrencia($ocorrencia);
        endif;
    }

    /**
     * Método que salva um array de ocorrências vindas da Pesquisa de Dívidas do Aluno
     * @param array $ocorrencias
     */
    public function salvarArrayOcorrenciasFinanceiro(array $arVwVendaLancamentoAtradosTO, array $dados)
    {

        $mensageiro = new Ead1_Mensageiro();
        $dao = new CAtencaoDAO();
        $dao->beginTransaction();

        try {

            foreach ($arVwVendaLancamentoAtradosTO as $vwTO) {
                //Instanciando um VendaProdutoTO para conseguir o num da Matricula
                $vendaProduto = new VendaProdutoTO();
                $vendaProduto->setId_venda($vwTO->getId_venda());
                $vendaProduto->setId_produto($vwTO->getId_produto());
                $vendaProduto->fetch(false, true, true);

                //Definindo os dados da OcorrenciaTO
                $ocorrencia = new OcorrenciaTO();
                $ocorrencia->setId_assuntoco($dados['id_assuntoco']);
                $ocorrencia->setId_matricula($vendaProduto->getId_matricula());
                $ocorrencia->setId_categoriaocorrencia($dados['id_categoriaocorrencia']);
                $ocorrencia->setId_entidade($vwTO->getSessao()->id_entidade);
                $ocorrencia->setId_usuariocadastro($dados['id_usuario']);
                $ocorrencia->setId_usuariointeressado($vwTO->getId_usuario());
                $ocorrencia->setSt_titulo("Debito ref. Venda: " . $vwTO->getId_venda()
                    . " - Lancamento: " . $vwTO->getId_lancamento()
                    . " - Produto: " . $vwTO->getSt_produto()
                    . " - Valor: " . $vwTO->getNu_valor());

                $assuntoCoTO = new AssuntoCoTO();
                $assuntoCoTO->setId_assuntoco($ocorrencia->getId_assuntoco());
                $assuntoCoTO->fetch(true, true, true);

                $textoSistemaTO = new TextoSistemaTO();
                $textoSistemaTO->setId_textosistema($assuntoCoTO->getId_textosistema());
                $textoSistemaTO->fetch(true, true, true);

                $textoSistemaBO = new TextoSistemaBO();

                $usuario = new UsuarioTO();
                $usuario->setId_usuario($ocorrencia->getId_usuariointeressado());
                $usuario->fetch(true, true, true);

                $params = array(
                    'id_venda' => $vwTO->getId_venda(),
                    'st_nomecompleto' => $usuario->getSt_nomecompleto(),
                    'id_ocorrencia' => $ocorrencia->getId_ocorrencia()
                );
                $texto = $textoSistemaBO->gerarTexto($textoSistemaTO, $params)->getFirstMensagem();

                $ocorrencia->setSt_ocorrencia($texto instanceof TextoSistemaTO ? $texto->getSt_texto() : $texto);
                $this->salvarOcorrencia($ocorrencia);
            }

            $dao->commit();
            return $mensageiro->setMensageiro("Ocorrencias criadas com sucesso!", Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            $dao->rollBack();
            return $mensageiro->setMensageiro("Erro ao criar ocorrencias! - {$e->getMessage()}", Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * @param OcorrenciaTO $ocorrenciaTO
     * @param null $arquivo
     * @return Ead1_Mensageiro
     * @deprecated new \G2\Negocio\Ocorrencia::salvarOcorrencia
     */
    public function salvarOcorrencia(OcorrenciaTO $ocorrenciaTO, $arquivo = null, $atendente = false)
    {
        $dao = new CAtencaoDAO();
        $dao->beginTransaction();
        try {

            $arNaoObrigatorios[] = 'id_ocorrencia';
            $arNaoObrigatorios[] = 'id_saladeaula';
            $arNaoObrigatorios[] = 'id_ocorrenciaoriginal';
            $arNaoObrigatorios[] = 'id_matricula';
            $arNaoObrigatorios[] = 'id_motivoocorrencia';
            $arNaoObrigatorios[] = 'dt_atendimento';
            $arNaoObrigatorios[] = 'dt_cadastroocorrencia';
            $arNaoObrigatorios[] = 'dt_cadastro';
            $arNaoObrigatorios[] = 'id_venda';
            $arNaoObrigatorios[] = 'st_codigorastreio';
            $arNaoObrigatorios[] = 'dt_ultimotramite';//Foi adicionado essa linha porque não estava funcionando o salvar da ocorrencia na abertura
            $arNaoObrigatorios[] = 'st_codissue';//Foi adicionado essa linha porque não estava funcionando o salvar da ocorrencia na abertura

            if (!$ocorrenciaTO->getId_evolucao()) {
                $ocorrenciaTO->setId_evolucao(OcorrenciaTO::EVOLUCAO_AGUARDANDO_ATENDIMENTO);
            }

            if (!$ocorrenciaTO->getId_situacao()) {
                $ocorrenciaTO->setId_situacao(OcorrenciaTO::SITUACAO_PENDENTE);
            }

            if ($ocorrenciaTO->getId_usuariocadastro()) {
                $ocorrenciaTO->setId_usuariocadastro($ocorrenciaTO->getId_usuariocadastro());
            } else {
                $ocorrenciaTO->setId_usuariocadastro($ocorrenciaTO->getSessao()->id_usuario);
            }

            if (!$ocorrenciaTO->getId_usuariointeressado()) {
                $ocorrenciaTO->setId_usuariointeressado($ocorrenciaTO->getSessao()->id_usuario);
            }

            if (!$ocorrenciaTO->getId_entidade()) {
                $ocorrenciaTO->setId_entidade($ocorrenciaTO->getSessao()->id_entidade);
            }
            $this->verificaCamposObrigatorios($ocorrenciaTO, $arNaoObrigatorios);
            $criacao = false;
            if (!$ocorrenciaTO->getId_ocorrencia()) {
                $criacao = true;
            }
            $ocorrenciaTO->setId_ocorrencia($dao->cadastrarOcorrencia($ocorrenciaTO));

            if ($criacao) {
                if (!$atendente) {
                    $this->vinculaResponsavel($ocorrenciaTO);
                }

                $tramite = new TramiteTO();
                $tramite->setSt_tramite('Ocorrência N. ' . $ocorrenciaTO->getId_ocorrencia() . ' criada por ' . $ocorrenciaTO->getSessao()->nomeUsuario . '.');
                $tramite->setId_usuario($ocorrenciaTO->getSessao()->id_usuario);
                $tramite->setId_entidade($ocorrenciaTO->getSessao()->id_entidade);
                $tramite->setBl_visivel(true);
                $this->novaInteracao($ocorrenciaTO, $tramite, $arquivo, false, false);
            }
            $dao->commit();
            return new Ead1_Mensageiro($ocorrenciaTO, Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $this->mensageiro->setMensageiro('Ocorreu um erro ao salvar a ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Retorna lista de ocorrências
     * @param VwOcorrenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarOcorrencias(VwOcorrenciaTO $to)
    {
        try {
            $to->setId_usuariointeressado($to->getSessao()->id_usuario);
            if ($to->getId_usuariointeressado()) {
                $dao = new CAtencaoDAO();
                $ocorrencias = $to->encapsularTo($dao->retornaOcorrencias($to), new VwOcorrenciaTO());
                if (!empty($ocorrencias))
                    $this->mensageiro = $this->mensageiro->setMensageiro($ocorrencias, Ead1_IMensageiro::SUCESSO);
                else
                    $this->mensageiro = $this->mensageiro->setMensageiro('Nenhuma ocorrência encontrada!', Ead1_IMensageiro::AVISO);

            }
        } catch (Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Ocorreu um erro ao retornar as ocorrências: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }

    /**
     * @param MatriculaTO $matriculaTo
     * @return Ead1_Mensageiro
     */
    public function verificaOcorrenciaMatricula(MatriculaTO $matriculaTo)
    {
        try {
            if (!$matriculaTo->id_matricula) {
                throw new Zend_Exception('O id_matricula é obrigatório e não foi passado');
            }
            $dao = new CAtencaoDAO();
            $ocorrencia = $dao->retornaOcorrenciaMatricula($matriculaTo);
            if ($ocorrencia) {
                $ocorrencia = $ocorrencia->toArray();
            }
            if (!empty($ocorrencia)) {
                $this->mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($ocorrencia, new OcorrenciaTO(), true), Ead1_IMensageiro::SUCESSO);
            } else {
                $this->mensageiro->setMensageiro('Nenhuma ocorrência encontrada', Ead1_IMensageiro::AVISO);
            }


        } catch (Zend_Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao retornar Ocorrencia Matricula: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }

        return $this->mensageiro;
    }


    /**
     * Método que solicita o encerramento de uma ocorrencia
     * @param OcorrenciaTO $ocorrencia
     * @return Ead1_Mensageiro
     * @doc : https://docs.google.com/document/d/1SB0uM1Nvqm9HvCyV9MaKb_m-Eyv649DCrwes5dlsjQY/edit
     */
    public function solicitaEncerramentoOcorrencia(OcorrenciaTO $ocorrencia)
    {
        try {
            $dao = new CAtencaoDAO();
            $dao->beginTransaction();

            $ocorrencia->setId_evolucao(OcorrenciaTO::EVOLUCAO_AGUARDANDO_INTERESSADO);
            $ocorrencia->setId_situacao(OcorrenciaTO::SITUACAO_AGUARDANDO_ENCERRAMENTO);
            if ($dao->updateOcorrencia($ocorrencia)) {
                $ocorrencia->fetch(true, true, true);
                $this->mensageiro->setMensageiro('Solicitação de encerramento da ocorrência feita com sucesso!', Ead1_IMensageiro::SUCESSO, $ocorrencia);

                $mensageiro = $this->enviaNotificacaoOcorrencia($ocorrencia, true,
                    ($ocorrencia->getSessao()->id_entidade ? $ocorrencia->getSessao()->id_entidade : $ocorrencia->getId_entidade()));
                if ($mensageiro->getType() != Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw new Zend_Exception('Erro no processo de envio de notificação da solicitação de encerramento.');
                }
            }

            $dao->commit();
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao solicitar encerramento de ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }


    /**
     * Nega o encerramento de uma ocorrência
     * @param OcorrenciaTO $ocorrenciaTO
     * @param TramiteTO $tramiteTO
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function negarEncerramentoOcorrencia(OcorrenciaTO $ocorrenciaTO, TramiteTO $tramiteTO)
    {
        $dao = new CAtencaoDAO();
        try {
            if (!$ocorrenciaTO->getId_ocorrencia()) {
                throw new Zend_Exception('O id_ocorrencia é obrigatório e não foi passado!');
            }
            $dao->beginTransaction();

            if ($dao->updateOcorrencia($ocorrenciaTO)) {
                $tramiteTO->setId_entidade($ocorrenciaTO->getSessao()->id_entidade);
                $tramiteTO->setId_usuario($ocorrenciaTO->getSessao()->id_usuario);
                $this->salvaTramite($ocorrenciaTO, $tramiteTO);
            }
            $dao->commit();
            $this->mensageiro->setMensageiro('Encerramento negado com sucesso!', Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            $this->mensageiro->setMensageiro('Erro ao negar encerramento de ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO, $e);
        }
        return $this->mensageiro;
    }


    /**
     * @param VwOcorrenciasPessoaTO $ocorrencia
     * @throws Zend_Exception
     * @return Ead1_Mensageiro
     */
    public function agendarOcorrencia(VwOcorrenciasPessoaTO $ocorrencia)
    {
        try {
            $mensageiro = new Ead1_Mensageiro();
            if (!$ocorrencia->getDt_atendimento() || !$ocorrencia->getId_ocorrencia()) {
                throw new Zend_Exception('A data de encerramento e/ou o id_ocorrencia não são válidos');
            }
            $ocoTO = new OcorrenciaTO();
            $ocoTO->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $ocoTO->fetch(true, true, true);
            $ocoTO->setDt_atendimento($ocorrencia->getDt_atendimento());

            $dao = new CAtencaoDAO();
            if ($dao->updateOcorrencia($ocoTO)) {
                $tramite = new TramiteTO();
                $tramite->setId_entidade($ocorrencia->getSessao()->id_entidade);
                $tramite->setId_usuario($ocorrencia->getSessao()->id_usuario);
                $tramite->setBl_visivel(true);
                $tramite->setId_tipotramite(OcorrenciaTO::TIPO_TRAMITE_AUTOMATICO);
                $tramite->setSt_tramite(utf8_encode("Ocorrencia n " . $ocorrencia->getId_ocorrencia() . " agendada para " . $this->converteDataCliente($ocorrencia->getDt_atendimento()) . " pelo o usuario " . $ocorrencia->getSt_atendente()));
                if ($this->salvaTramite($ocoTO, $tramite)->getTipo() == Ead1_IMensageiro::SUCESSO) {
                    $mensageiro->setMensageiro('Ocorrencia agendada com sucesso!', Ead1_IMensageiro::SUCESSO);
                }
            }

        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro('Erro ao agendar uma ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * Retorna resultado de busca de ocorrencia por id_ocorrencia
     * @author Rafael Bruno <rafael.bruno@gmail.com>
     * @param VwOcorrenciaTO $vwOcorrenciaTO com atributo id_ocorrencia preenchido
     * @return Ead1_Mensageiro|VwOcorrenciaTO
     */
    public function retornaVwOcorrenciaPorId(VwOcorrenciaTO $vwOcorrenciaTO)
    {
        try {
            if ($vwOcorrenciaTO->getId_ocorrencia()) {
                $ocoTO = new VwOcorrenciaTO();
                $ocoTO->setId_ocorrencia($vwOcorrenciaTO->getId_ocorrencia());
                $ocoTO->fetch(false, true, true);

                return $ocoTO;
            } else {
                return new Ead1_Mensageiro('É necessário o ID da ocorrência que deseja buscar', Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            return new Ead1_Mensageiro('Erro ao buscar a ocorrência: ' . $e->getMessage(), Ead1_IMensageiro::ERRO);
        }
    }
}

?>

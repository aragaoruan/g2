<?php
class AvaliacaoConjuntoReferenciaORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoconjuntoreferencia';
	public $_primary 			= array( 'id_avaliacaoconjuntoreferencia' );
	
	public $_referenceMap 		= array( 'AvaliacaoConjuntoORM'	=> array(
														            'columns'           => 'id_avaliacaoconjunto',
														            'refTableClass'     => 'AvaliacaoConjuntoORM',
														            'refColumns'        => 'id_avaliacaoconjunto'
														),
										'ProjetoPedagogicoORM' 	=> array(
																	'columns'           => 'id_projetopedagogico',
										            				'refTableClass'     => 'ProjetoPedagogicoORM',
										            				'refColumns'        => 'id_projetopedagogico'
														),
										'SalaDeAulaORM' 		=> array(
																	'columns'			=> 'id_saladeaula',
																	'refTableClass'		=> 'SalaDeAulaORM',
																	'refColumns'		=> 'id_saladeaula'
														),
										'ModuloORM' 			=> array(
																	'columns'			=> 'id_modulo',
																	'refTableClass'		=> 'ModuloORM',
																	'refColumns'		=> 'id_modulo'
														)
										);
	
}
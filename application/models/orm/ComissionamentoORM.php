<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_comissionamento
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class ComissionamentoORM extends Ead1_ORM {

	public $_name = 'tb_comissionamento';
	public $_primary = 'id_comissionamento';
	
}

?>
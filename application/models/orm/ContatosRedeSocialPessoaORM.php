<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatosredesocialpessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosRedeSocialPessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_contatosredesocialpessoa';
	public $_primary = array('id_usuario','id_entidade','id_redesocial');
	public $_referenceMap = array(
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'ContatosRedeSocialORM' => array(
	            'columns'           => 'id_redesocial',
	            'refTableClass'     => 'ContatosRedeSocialORM',
	            'refColumns'        => 'id_redesocial'
			)		
	);	
}
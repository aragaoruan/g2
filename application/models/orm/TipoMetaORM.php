<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipometa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoMetaORM extends Ead1_ORM {

	public $_name = 'tb_tipometa';
	public $_primary = array('id_tipometa');
	public $_dependentTables = array('RegraComissaoORM');
}

?>
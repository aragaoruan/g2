<?php
/**
 * Classe ORM que define o VIEW vw_saladisciplinaalocacao
 * @author jgabriel
 *
 * @package models
 * @subpackage orm
 */
class VwSalaDisciplinaAlocacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_saladisciplinaalocacao';
	public $_primary = array('id_matricula','id_saladeaula','id_projetopedagogico','id_disciplina','id_areaconhecimento');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_formapagamentodistribuicao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FormaPagamentoDistribuicaoORM extends Ead1_ORM {

	public $_name = 'tb_formapagamentodistribuicao';
	public $_primary = array('id_formapagamento','id_meiopagamento');
	public $_referenceMap = array(
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	),
	'MeioPagamentoORM' => array(
	            'columns'           => 'id_meiopagamento',
	            'refTableClass'     => 'MeioPagamentoORM',
	            'refColumns'        => 'id_meiopagamento'
	));
}

?>
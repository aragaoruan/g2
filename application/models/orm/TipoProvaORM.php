<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoprova
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoProvaORM extends Ead1_ORM {

	public $_name = 'tb_tipoprova';
	public $_primary = array('id_tipoprova');
	public $_dependentTables = array('ProjetoTipoProvaORM','RecuperacaoTipoProvaORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentosutilizacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosUtilizacaoORM extends Ead1_ORM {

	public $_name = 'tb_documentosutilizacao';
	public $_primary = array('id_documentosutilizacao');
	public $_dependentTables = array('DocumentosUtilizacaoRelacaoORM');
}

?>
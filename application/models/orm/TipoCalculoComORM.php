<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocalculocom
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoCalculoComORM extends Ead1_ORM {

	public $_name = 'tb_tipocalculocom';
	public $_primary = array('id_tipocalculocom');
	public $_dependentTables = array('ComissaoEntidadeORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_interfacepessoa
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class InterfacePessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_interfacepessoa';
	public $_primary = 'id_interfacepessoa';
}
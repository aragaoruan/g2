<?php
/**
 * Mapeamento da view que mostra os dados do documento de reservista da pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaReservistaORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoareservista';
	public $_primary = array('id_municipio', 'id_entidade', 'id_usuario', 'sg_uf');
}
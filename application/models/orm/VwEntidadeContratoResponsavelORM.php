<?php
/**
 * Classe de mapeamento objeto relacional da vw_usuariocontratoresponsavel
 * @author gabriel.vasconcelos
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeContratoResponsavelORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadecontratoresponsavel';
	public $_primary = array('id_contratoresponsavel','id_entidaderesponsavel');
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_evolucao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EvolucaoORM extends Ead1_ORM {

	public $_name = 'tb_evolucao';
	public $_primary = array('id_evolucao');
	public $_dependentTables = array('MatriculaORM', 'ContratoORM', 'VendaORM');
}

?>
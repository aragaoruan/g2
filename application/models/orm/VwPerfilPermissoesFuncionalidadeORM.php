<?php
/**
 * Classe de Mapeamento Objeto Relacional da view vw_perfilpermissoesfuncionalidade
 * @author EAD1
 *
 * @package models
 * @subpackage orm
 */
class VwPerfilPermissoesFuncionalidadeORM extends Ead1_ORM{

	/**
	 * Atributo com o nome da View
	 * @var String
	 */
	public $_name = 'vw_perfilpermissoesfuncionalidade';

	/**
	 * Atributo com o nome da suposta chave primária
	 * @var String
	 */
	public $_primary = 'id_perfil';
}
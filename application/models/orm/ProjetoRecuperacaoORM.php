<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetorecuperacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoRecuperacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_projetorecuperacao';
	public $_primary = array('id_projetopedagogico', 'id_tiporecuperacao');
	public $_dependentTables = array('RecuperacaoTipoProvaORM');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'TipoRecuperacaoORM' => array(
	            'columns'           => 'id_tiporecuperacao',
	            'refTableClass'     => 'TipoRecuperacaoORM',
	            'refColumns'        => 'id_tiporecuperacao'
	));	
}
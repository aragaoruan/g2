<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaatuacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaAtuacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_areaatuacao';
	public $_primary = array('id_areaatuacao');
	public $_dependentTables = array('InformacaoProfissionalPessoaORM');
	
}
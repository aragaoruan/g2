<?php
class TextoVariaveisORM extends Ead1_ORM{
	
	public $_name 				= 'tb_textovariaveis';
	public $_primary 			= array( 'id_textovariaveis' );
	
	public $_referenceMap 		= array( 'TextoCategoriaORM'	=> array(
														            'columns'           => 'id_textocategoria',
														            'refTableClass'     => 'TextoCategoriaORM',
														            'refColumns'        => 'id_textocategoria'
														)
										);
}
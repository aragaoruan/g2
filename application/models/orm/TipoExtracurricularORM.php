<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoextracurricular
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoExtracurricularORM extends Ead1_ORM {

	public $_name = 'tb_tipoextracurricular';
	public $_primary = array('id_tipoextracurricular');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
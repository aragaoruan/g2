<?php
/**
 * Classe que encapsula os dados da view vw_entregamaterial
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwEntregaMaterialORM extends Ead1_ORM{
	
	public $_name = 'vw_entregamaterial';	
	public $_primary = array('id_matricula','id_projetopedagogico','id_modulo','id_disciplina');
	
}
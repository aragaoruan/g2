<?php
/**
 * Classe que faz o mapeamento da view que retorna as séries e nivel de ensino de um determinado modulo
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwSerieModuloORM extends Ead1_ORM{
	public $_name = 'vw_seriemodulo';
	public $_primary = array('id_projetopedagogico', 'id_serie', 'id_modulo', 'id_disciplina', 'id_nivelensino');
}
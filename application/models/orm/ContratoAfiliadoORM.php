<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contratoafiliado
 * @author Denise Xavier denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class ContratoAfiliadoORM extends Ead1_ORM {
	public $_name = 'tb_contratoafiliado';
	public $_primary = array('id_contratoafiliado');
}

?>
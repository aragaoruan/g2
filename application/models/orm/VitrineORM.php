<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_vitrine
 * @author Denise Xavier <denise.xavier07@gmail.com>
 * @package models
 * @subpackage orm
 */
class VitrineORM extends Ead1_ORM {

	public $_name = 'tb_vitrine';
	public $_primary = array('id_vitrine');
}

?>
<?php

/**
 * Tabela para guardar os dados do arquivo retorno 
 * Classe de ArquivoRetornoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class ArquivoRetornoORM extends Ead1_ORM {

	public $_name = 'tb_arquivoretorno';
	public $_primary = array('id_arquivoretorno');

}
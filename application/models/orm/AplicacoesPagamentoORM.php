<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_aplicacoespagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AplicacoesPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_aplicacoespagamento';
	public $_primary = array('id_aplicacoespagamento');
	public $_dependentTables = array('FormaPagamentoAplicacoesORM');
}

?>
<?php
/**
 * Classe de gateway com a view vw_tipodematerial
 * @author Arthur Cláudio de Almeida Pereira
 *
 * @package orm
 * @subpackage material
 */
class Material_VwTipoDeMaterialORM	extends Ead1_ORM {
	
	public $_name 		= 'vw_tipodematerial';
	public $_primary 	= array('id_tipodematerial', 'id_situacao', 'id_entidade');
	
	
}
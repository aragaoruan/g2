<?php
/**
 * Classe de gateway com a view vw_documentos_secretaria
 * @author Arthur Cláudio de Almeida Pereira
 *
 * @package orm
 * @subpackage material
 */
class Material_VwDocumentosSecretariaORM extends Ead1_ORM {
	
	protected $_name		= 'vw_documentossecretaria';
	protected $_primary		= array('id_documentos','id_entidade','id_tipodocumento');
}
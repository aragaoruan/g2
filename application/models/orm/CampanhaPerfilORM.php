<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhaperfil
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaPerfilORM extends Ead1_ORM {

	public $_name = 'tb_campanhaperfil';
	public $_primary = array('id_perfil','id_campanhacomercial');
	public $_referenceMap = array(
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	),
	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        => 'id_perfil'
	));
}

?>
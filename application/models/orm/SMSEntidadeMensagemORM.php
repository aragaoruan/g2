<?php
/**
 * Classe de mapeamento objeto relacional da tabela
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */

class SMSEntidadeMensagemORM extends Ead1_ORM{
    public $_name = 'tb_smsentidademensagem';
    public $_primary = array('id_smsentidademensagem');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodesconto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDescontoORM extends Ead1_ORM {

	public $_name = 'tb_tipodesconto';
	public $_primary = array('id_tipodesconto');
	public $_dependentTables = array('CampanhaComercialORM');
}

?>
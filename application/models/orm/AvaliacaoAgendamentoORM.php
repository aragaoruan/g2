<?php
class AvaliacaoAgendamentoORM extends Ead1_ORM{
	
	public $_name = 'tb_avaliacaoagendamento';
	public $_primary = array('id_avaliacaoagendamento');
	
	public $_referenceMap 		= array(    'AvaliacaoAplicacaoORM'	=> array(
                                                                                        'columns'           => 'id_avaliacaoaplicacao',
                                                                                        'refTableClass'     => 'AvaliacaoAplicacaoORM',
                                                                                        'refColumns'        => 'id_avaliacaoaplicacao'
                                                                                ),
                                                    'MatriculaORM' 		=> array(
                                                                                        'columns'           => 'id_matricula',
                                                                                        'refTableClass'     => 'MatriculaORM',
                                                                                        'refColumns'        => 'id_matricula'
                                                                                ),
                                                    'SituacaoORM' 		=> array(
                                                                                        'columns'           => 'id_situacao',
                                                                                        'refTableClass'     => 'SituacaoORM',
                                                                                        'refColumns'        => 'id_situacao'
                                                                                ),
                                                    'UsuarioORM' 		=> array(
                                                                                        'columns'           => 'id_usuariocadastro',
                                                                                        'refTableClass'     => 'EntidadeEnderecoORM',
                                                                                        'refColumns'        => 'id_usuario'
                                                                                )
                                                    );
}
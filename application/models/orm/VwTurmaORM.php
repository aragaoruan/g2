<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwTurmaORM extends Ead1_ORM{
	public $_name = 'vw_turma';
	public $_primary = array('id_turma');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_sistema
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SistemaORM extends Ead1_ORM {

	public $_name = 'tb_sistema';
	public $_primary = array('id_sistema');
	public $_dependentTables = array('EntidadeSistemaORM');
}

?>
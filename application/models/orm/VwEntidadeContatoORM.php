<?php
/**
 * Classe de mapeamento objeto relacional da view vw_entidadecontato
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeContatoORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadecontato';
	public $_primary = array('id_entidade', 'id_usuario', 'id_contatoentidade');
}
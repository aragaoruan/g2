<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocampooperacao
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class TipoCampoOperacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_tipocampooperacao';
	public $_primary = array('id_tipocampo', 'id_operacaorelatorio');
}
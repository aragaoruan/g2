<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_pessoaimportacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class PessoaImportacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_pessoaimportacao';
	public $_primary = array('id_pessoaimportacao');
	
}
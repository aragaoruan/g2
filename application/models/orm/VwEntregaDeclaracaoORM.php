<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwEntregaDeclaracaoORM extends Ead1_ORM{
	public $_name = 'vw_entregadeclaracao';
	public $_primary = array('id_entregadeclaracao');
}
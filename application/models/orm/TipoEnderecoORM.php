<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoendereco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoEnderecoORM extends Ead1_ORM{
	
	public $_name = 'tb_tipoendereco';
	public $_primary = 'id_categoriaendereco';
	public $_dependentTables = array('TipoEnderecoORM','TipoEnderecoEntidadeORM');
	public $_referenceMap = array(
	'CategoriaEnderecoORM' => array(
	            'columns'           => 'id_categoriaendereco',
	            'refTableClass'     => 'CategoriaEnderecoORM',
	            'refColumns'        =>'id_categoriaendereco'
			)
	);
	
}
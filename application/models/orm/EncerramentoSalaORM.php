<?php
/**
 * Classe ORM que define o tabela tb_encerramentosala
 *
 * @package models
 * @subpackage orm
 */
class EncerramentoSalaORM extends Ead1_ORM{
	
	public $_name = 'tb_encerramentosala';
	public $_primary = array('id_encerramentosala');
}
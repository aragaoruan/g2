<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetoextracurricular
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoExtracurricularORM extends Ead1_ORM {

	public $_name = 'tb_projetoextracurricular';
	public $_primary = array('id_projetopedagogico', 'id_areaconhecimento');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	));
}

?>
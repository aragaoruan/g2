<?php

/**
 * Classe de VwProjetoPlanoMatrizORM
 * @package models
 * @subpackage orm
 */
 class VwProjetoPlanoMatrizORM extends Ead1_ORM {

	public $_name = 'vw_projetoplanomatriz';
	public $_primary = array('id_projetopedagogico');

}

?>
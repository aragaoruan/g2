<?php
/**
    * Classe de mapeamento objeto relacional da tabela tb_produtoprr
    * @author Denise Xavier <denisexavier@ead1.com.br>
     * @package models
    * @subpackage orm
*/
class ProdutoPrrORM extends Ead1_ORM {

    public $_name = 'tb_produtoprr';
    public $_primary = array('id_produto');
}
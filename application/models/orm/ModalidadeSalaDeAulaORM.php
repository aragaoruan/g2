<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_modalidadesaladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ModalidadeSalaDeAulaORM extends Ead1_ORM {

	public $_name = 'tb_modalidadesaladeaula';
	public $_primary = array('id_modalidadesaladeaula');
	public $_dependentTables = array('SalaDeAulaORM');
}

?>
<?php
/**
 * Mapeamento da view que mostra Areas do conhecimento ligadas a disciplina
 * Classe de mapeamento objeto relacional da view vw_areadisciplina
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwAreaDisciplinaORM extends Ead1_ORM{
	
	public $_name = 'vw_areadisciplina';
	public $_primary = array('id_disciplina', 'id_areaconhecimento');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_permissaofuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PermissaoFuncionalidadeORM extends Ead1_ORM{
	public $_name = 'tb_permissaofuncionalidade';
	public $_primary = array('id_funcionalidade', 'id_permissao');
}
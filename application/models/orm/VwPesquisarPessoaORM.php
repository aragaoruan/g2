<?php
/**
 * Mapeamento da view que mostra o resultado de pesquisa de pessoa/s
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarPessoaORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisarpessoa';
	public $_primary = array('id_usuario', 'id_entidade', 'id_municipio', 'sg_uf');
}
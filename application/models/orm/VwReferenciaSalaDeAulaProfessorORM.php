<?php
/**
 * Classe de mapeamento objeto relacional da view vw_referenciasaladeaulaprofessor
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwReferenciaSalaDeAulaProfessorORM extends Ead1_ORM {

	public $_name = 'vw_referenciasaladeaulaprofessor';
	public $_primary = array('id_saladeaula', 'id_saladeaulaprofesssor', 'id_perfilentidadereferencia', 'id_perfil', 'id_entidade', 'id_usuario');
}

?>
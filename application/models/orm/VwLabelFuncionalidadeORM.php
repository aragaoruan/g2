<?php
/**
 * Classe que encapsula os dados da view vw_labelfuncionalidade
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwLabelFuncionalidadeORM extends Ead1_ORM{
	
public $_name = 'vw_labelfuncionalidade';	
public $_primary = array('id_funcionalidade','id_funcionalidadepai','id_entidade');
	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_convenio
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ConvenioORM extends Ead1_ORM {

	public $_name = 'tb_convenio';
	public $_primary = array('id_convenio');
	public $_dependentTables = array('ContratoConvenioORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	));
}

?>
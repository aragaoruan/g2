<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipotelefone
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoTelefoneORM extends Ead1_ORM{
	
	public $_name = 'tb_tipotelefone';
	public $_primary = 'id_tipotelefone';
	public $_dependentTables = array('ContatosTelefoneORM', 'TipoTelefoneEntidadeORM');
	
}
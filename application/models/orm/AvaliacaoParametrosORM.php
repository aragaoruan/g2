<?php

/**
 * Classe de AvaliacaoParametrosORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class AvaliacaoParametrosORM extends Ead1_ORM {

	public $_name = 'tb_avaliacaoparametros';
	public $_primary = array('id_avaliacaoparametros');

}
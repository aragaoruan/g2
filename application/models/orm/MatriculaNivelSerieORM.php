<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class MatriculaNivelSerieORM extends Ead1_ORM{
	public $_name = 'tb_matriculanivelserie';
	public $_primary = array('id_matricula');
}
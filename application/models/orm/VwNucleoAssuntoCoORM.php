<?php

 /**
* Classe que encapsula os dados da view vw_nucleoassuntoco
* @author Denise - denisexavier@ead1.com.br
* @package models
* @subpackage orm
*/
class VwNucleoAssuntoCoORM extends Ead1_ORM {

public $_name = 'vw_nucleoassuntoco';
public $_primary = array('id_nucleoassuntoco');

}
<?php

class EntidadeFinanceiroORM extends Ead1_ORM
{
    protected $_name = 'tb_entidadefinanceiro';
    protected $_primary = 'id_entidadefinanceiro';

    protected $_dependentTables = array('tb_entidadeboletoconfig', 'tb_entidadecartaoconfig');

    protected $_referenceMap = array('EntidadeORM' => array(
        'columns' => 'id_entidade',
        'refTableClass' => 'EntidadeORM',
        'refColumns' => 'id_entidade'
    ));
}
<?php
/**
 * Mapeamento da view que mostra os dados biomédicos de uma pessoa.
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaDadosBiomedicosORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoadadosbiomedicos';
	public $_primary = array('id_etnia', 'id_entidade', 'id_usuario', 'id_tiposanguineo');
}
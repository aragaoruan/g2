<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class FormaPagamentoAplicacaoORM extends Ead1_ORM{
	public $_name = 'tb_formapagamentoaplicacao';
	public $_primary = array('id_formapagamentoaplicacao');
}
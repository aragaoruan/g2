<?php
/**
 * Mapeamento da view que faz a o retorno de pesquisa da(s) sala(s) de aula
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisaSalaDeAulaORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisasaladeaula';
	public $_primary = array('id_saladeaula', 'id_tiposaladeaula', 'id_periodoletivo', 'id_situacao');
}
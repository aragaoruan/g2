<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipopropriedadecamporel
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class TipoPropriedadeCampoRelORM extends Ead1_ORM{
	
	public $_name = 'tb_tipopropriedadecamporel';
	public $_primary = 'id_tipopropriedadecamporel';
}
<?php
/**
 * Mapeamento da view que mostra o(s) contatos(s) rede(s) social cadastro de pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaRedeSocialORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoaredesocial';
	public $_primary = array('id_redesocial', 'id_entidade', 'id_tiporedesocial', 'id_usuario');
}
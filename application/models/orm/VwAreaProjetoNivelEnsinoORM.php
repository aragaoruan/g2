<?php
/**
 * Classe de mapeamento objeto relacional da view vw_areaprojetonivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwAreaProjetoNivelEnsinoORM extends Ead1_ORM  {

	public $_name = 'vw_areaprojetonivelensino';
	public $_primary = array('id_projetopedagogico', 'id_areaconhecimento', 'id_entidade', 'id_nivelensino');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_processo
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class ProcessoORM extends Ead1_ORM {
	
	public $_name = 'tb_processo';
	public $_primary = array('id_processo');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
class VwResgateAlunosORM extends Ead1_ORM {
	public $_name = 'vw_resgatealunos';
	public $_primary = 'id_matricula';
}
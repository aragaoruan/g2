<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_recuperacaotipoprova
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class RecuperacaoTipoProvaORM extends Ead1_ORM {

	public $_name = 'tb_recuperacaotipoprova';
	public $_primary = array('id_tipoprova', 'id_projetopedagogico', 'id_tiporecuperacao');
	public $_referenceMap = array(
	'TipoProvaORM' => array(
	            'columns'           => 'id_tipoprova',
	            'refTableClass'     => 'TipoProvaORM',
	            'refColumns'        => 'id_tipoprova'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_textoexibicaocategoria
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TextoExibicaoCategoriaORM extends Ead1_ORM{
	
	public $_name = 'tb_textoexibicaocategoria';
	public $_primary = array('id_textoexibicao', 'id_textocategoria');
	public $_referenceMap = array(
		'TextoCategoriaORM' => array(
	            'columns'           => 'id_textocategoria',
	            'refTableClass'     => 'TextoCategoriaORM',
	            'refColumns'        =>'id_textocategoria'
			),
		'TextoExibicaoORM' => array(
	            'columns'           => 'id_textoexibicao',
	            'refTableClass'     => 'TextoExibicaoORM',
	            'refColumns'        =>'id_textoexibicao'
		)
	);
}
<?php

/**
 * Classe de VwContratoMatriculaProjeto
 
 * @package models
 * @subpackage orm
 */

 class VwContratoMatriculaProjetoORM extends Ead1_ORM {

	public $_name = 'vw_contratomatriculaprojeto';
	public $_primary = array('id_contrato');
	

}
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_usuariovendaproduto
 * @author Dimas Sulz
 * 
 */
class VwUsuarioVendaProdutoORM extends Ead1_ORM
{
	public $_name 		= 'vw_usuariovendaproduto';
	public $_primary 	= array('id_venda');
}

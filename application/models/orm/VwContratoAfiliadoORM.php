<?php

/**
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwContratoAfiliadoORM extends Ead1_ORM {

	public $_name = 'vw_contratoafiliado';
	public $_primary = array('id_contratoafiliado');
	
}

?>
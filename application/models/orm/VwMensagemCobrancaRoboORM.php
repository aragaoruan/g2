<?php

/**
 * Classe de VwMensagemCobrancaRoboORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwMensagemCobrancaRoboORM extends Ead1_ORM {

	public $_name = 'vw_mensagemcobrancarobo';
	public $_primary = array('id_lancamento');

}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_formapagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FormaPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_formapagamento';
	public $_primary = array('id_formapagamento');
	public $_dependentTables = array('VendaORM', 'FormaMeioPagamentoORM', 'FormaPagamentoDistribuicaoORM', 'FormaPagamentoAplicacoesORM','CampanhaFormaPagamentoORM');
	public $_referenceMap = array(
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	));
}

?>
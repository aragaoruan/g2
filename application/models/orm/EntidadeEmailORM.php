<?php
/**
 *Classe de Mapeamento de Objeto Relacional de Email da Entidade 
 * @author eduardoromao
 * @since 02/08/2011
 */
class EntidadeEmailORM extends Ead1_ORM{
	
	public $_name = 'tb_entidadeemail';
	public $_primary = array('id_entidadeemail');
	
	public $_referenceMap 		= array( 'EntidadeORM'	=> array(
														            'columns'           => 'id_entidade',
														            'refTableClass'     => 'EntidadeORM',
														            'refColumns'        => 'id_entidade'
														),
										'EmailConfigORM' 			=> array(
																	'columns'           => 'id_emailconfig',
										            				'refTableClass'     => 'EmailConfigORM',
										            				'refColumns'        => 'id_emailconfig'
														));
}
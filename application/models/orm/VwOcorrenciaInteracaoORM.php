<?php
/**
 * Classe de mapeamento objeto relacional da view vw_ocorrenciainteracao
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwOcorrenciaInteracaoORM extends Ead1_ORM {
	
	public $_name = 'vw_ocorrenciainteracao';
	public $_primary = array('id_tramite');
}

?>
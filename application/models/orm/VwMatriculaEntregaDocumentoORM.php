<?php
/**
 * Classe que mapeia a view que retorna os documentos e entregas numa determinada matricula
 * @autor Eder Lamar - edermariano@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwMatriculaEntregaDocumentoORM extends Ead1_ORM{
	public $_name = 'vw_matriculaentregadocumento';
	public $_primary = array('id_matricula');
}
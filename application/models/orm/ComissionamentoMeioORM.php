<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_comissionamentomeio
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class ComissionamentoMeioORM extends Ead1_ORM {

	public $_name = 'tb_comissionamentomeio';
	public $_primary = 'id_comissionamento';
	
}

?>
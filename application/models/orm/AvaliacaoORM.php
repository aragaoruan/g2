<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class AvaliacaoORM extends Ead1_ORM{
	public $_name 				= 'tb_avaliacao';
	public $_primary 			= array( 'id_avaliacao' );
	
	public $_referenceMap 		= array( 'TipoAvaliacaoORM'	=> array(
                                                                                'columns'               => 'id_tipoavaliacao',
                                                                                'refTableClass'         => 'TipoAvaliacaoORM',
                                                                                'refColumns'            => 'id_tipoavaliacao'
                                        ),
                                        'SituacaoORM' 			=> array(
                                                                                'columns'               => 'id_situacao',
                                                                                'refTableClass'         => 'SituacaoORM',
                                                                                'refColumns'            => 'id_situacao'
                                        ),
                                        'UsuarioORM' 			=> array(
                                                                                'columns'               => 'id_usuariocadastro',
                                                                                'refTableClass'		=> 'UsuarioORM',
                                                                                'refColumns'		=> 'id_usuario'
                                        ),
                                        'EntidadeORM' 			=> array(
                                                                                'columns'		=> 'id_entidade',
                                                                                'refTableClass'		=> 'EntidadeORM',
                                                                                'refColumns'		=> 'id_entidade'
                                        ),
                                        'TipoRecuperacaoORM' 		=> array(
                                                                                'columns'		=> 'id_tiporecuperacao',
                                                                                'refTableClass'		=> 'TipoRecuperacaoORM',
                                                                                'refColumns'		=> 'id_tiporecuperacao'
                                        ),
                                        'DescricaoORM' 			=> array(
                                                                                'columns'		=> 'st_descricao',
                                                                                'refTableClass'		=> 'DescricaoORM',
                                                                                'refColumns'		=> 'st_descricao'
                                        ),
                                        'QuantquestORM' 		=> array(
                                                                                'columns'		=> 'nu_quantquest',
                                                                                'refTableClass'		=> 'QuantquestORM',
                                                                                'refColumns'		=> 'nu_quantquestoes'
                                        ),
                                        'LinkreferenciaORM' 		=> array(
                                                                                'columns'		=> 'st_linkreferencia',
                                                                                'refTableClass'		=> 'LinkreferenciaORM',
                                                                                'refColumns'		=> 'st_linkreferencia'
                                        )
                                        );
}
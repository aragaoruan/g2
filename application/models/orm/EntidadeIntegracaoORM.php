<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadeintegracao
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class EntidadeIntegracaoORM extends Ead1_ORM {
	public $_name = 'tb_entidadeintegracao';
	public $_primary = 'id_entidadeintegracao';
}
<?php

/**
 * Classe que encapsula os dados da view vw_ocorrenciaevolucao
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class VwOcorrenciaEvolucaoORM extends Ead1_ORM {
	public $_name = 'vw_ocorrenciaevolucao';
	public $_primary = array('id_ocorrencia', 'id_evolucao', 'id_funcao');
}

?>
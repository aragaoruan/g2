<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_calendarioletivo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CalendarioLetivoORM extends Ead1_ORM{
	
	public $_name = 'tb_calendarioletivo';
	public $_primary = array('id_calendarioletivo');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	));	
}
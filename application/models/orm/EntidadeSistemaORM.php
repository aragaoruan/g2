<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadesistema
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeSistemaORM extends Ead1_ORM {

	public $_name = 'tb_entidadesistema';
	public $_primary = array('id_entidade', 'id_sistema');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'SistemaORM' => array(
	            'columns'           => 'id_sistema',
	            'refTableClass'     => 'SistemaORM',
	            'refColumns'        => 'id_sistema'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_permissao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PermissaoORM extends Ead1_ORM{
	public $_name = 'tb_permissao';
	public $_primary = array('id_permissao');
}
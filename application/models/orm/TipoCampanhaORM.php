<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocampanha
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class TipoCampanhaORM extends Ead1_ORM{
	
	public $_name = 'tb_tipocampanha';
	public $_primary = array('id_tipocampanha');
	public $_dependentTables = array('CampanhaComercialORM','VendaORM');
	
}
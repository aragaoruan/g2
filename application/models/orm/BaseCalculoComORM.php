<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_basecalculocom
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class BaseCalculoComORM extends Ead1_ORM {

	public $_name = 'tb_basecalculocom';
	public $_primary = array('id_basecalculocom');
	public $_dependentTables = array('BaseCalculoComORM');
}

?>
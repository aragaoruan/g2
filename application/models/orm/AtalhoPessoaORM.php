<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_atalhopessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AtalhoPessoaORM extends Ead1_ORM {

	public $_name = 'tb_atalhopessoa';
	public $_primary = array('id_atalhopessoa');
	public $_referenceMap = array(
	'FuncionalidadeORM' => array(
	            'columns'           => 'id_funcionalidade',
	            'refTableClass'     => 'FuncionalidadeORM',
	            'refColumns'        => 'id_funcionalidade'
	 ),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	 ),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipofuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoFuncionalidadeORM extends Ead1_ORM {

	public $_name = 'tb_tipofuncionalidade';
	public $_primary = array('id_tipofuncionalidade');
	public $_dependentTables = array('FuncionalidadeORM');
}

?>
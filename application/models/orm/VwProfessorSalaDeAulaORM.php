<?php
/**
 * Classe que mapeia a view que retorna os professores de uma sala de aula
 * @autor Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwProfessorSalaDeAulaORM extends Ead1_ORM{
	public $_name = 'vw_professorsaladeaula';
	public $_primary = array('id_saladeaula', 'id_usuario', 'id_entidade', 'id_saladeaulaprofessor', 'id_perfilreferencia');
}
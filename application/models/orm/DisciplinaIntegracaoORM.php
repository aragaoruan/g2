<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_disciplinaintegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DisciplinaIntegracaoORM extends Ead1_ORM{
	
	public $_name = 'tb_disciplinaintegracao';
	public $_primary = array('id_disciplinaintegracao');
}
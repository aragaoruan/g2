<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocomissao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoComissaoORM extends Ead1_ORM {

	public $_name = 'tb_tipocomissao';
	public $_primary = array('id_tipocomissao');
	public $_dependentTables = array('RegraComissaoORM');
}

?>
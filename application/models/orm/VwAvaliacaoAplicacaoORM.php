<?php
/**
 * Classe que faz o mapeamento da view vw_pesquisaavaliacaoaplicacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoAplicacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisaavaliacaoaplicacao';
	public $_primary = array('id_avaliacaoaplicacao');
	
}
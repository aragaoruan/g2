<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_modulo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ModuloORM extends Ead1_ORM {

	public $_name = 'tb_modulo';
	public $_primary = array('id_modulo');
	public $_dependentTables = array('ModuloSerieNivelEnsinoORM', 'ModuloDisciplinaORM', 'ModuloORM');
	public $_referenceMap = array(
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'ModuloORM' => array(
	            'columns'           => 'id_moduloanterior',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_modulo'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	));
}

?>
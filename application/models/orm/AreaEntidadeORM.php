<?php

class AreaEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_areaentidade';
	public $_primary = array('id_areaconhecimento', 'tb_entidade');
	public $_referenceMap = array(
	'AreaConhecimentoORM' => array(
		'columns'			=> 'id_areaconhecimento',
		'refTableClass'		=> 'AreaConhecimentoORM',
		'refColumns'		=> 'id_areaconhecimento'
	),
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
	)
	);
}

?>
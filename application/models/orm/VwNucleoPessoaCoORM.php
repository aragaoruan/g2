<?php

 /**
* Classe que encapsula os dados da view vw_nucleopessoaco
* @author Denise - denisexavier@ead1.com.br
* @package models
* @subpackage orm
*/
class VwNucleoPessoaCoORM extends Ead1_ORM {

public $_name = 'vw_nucleopessoaco';
public $_primary = array('id_nucleopessoaco');

}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_trilhadisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TrilhaDisciplinaORM extends Ead1_ORM {

	public $_name = 'tb_trilhadisciplina';
	public $_primary = array('id_trilha', 'id_disciplina', 'id_disciplinaprerequisito');
	public $_dependentTables = array('AreaConhecimentoSerieNivelEnsinoORM');
	public $_referenceMap = array(
	'TrilhaORM' => array(
	            'columns'           => 'id_trilha',
	            'refTableClass'     => 'TrilhaORM',
	            'refColumns'        => 'id_trilha'
	),
	'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'TrilhaORM',
	            'refColumns'        => 'id_disciplina'
	),
	'DisciplinaPreRequisitoORM' => array(
	            'columns'           => 'id_disciplinaprerequisito',
	            'refTableClass'     => 'TrilhaORM',
	            'refColumns'        => 'id_disciplina'
	));
}

?>
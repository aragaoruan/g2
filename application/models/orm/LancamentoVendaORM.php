<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_lancamentovenda
 * 
 * [Quem utiliza]
 * 	@see ProdutoBO::salvarLancamentoVenda() (consulta)
 * 	@see ProdutoDAO::cadastrarLancamentoVenda() (insert)
 * 	@see ProdutoDAO::editarLancamentoVenda() (update)
 * 	@see ProdutoDAO::deletarLancamentoVenda() (delete)
 * 	@see ProdutoDAO::retornarLancamentoVenda() (fetchAll)
 * 
 * @author edermariano
 *
 */
class LancamentoVendaORM extends Ead1_ORM {

	public $_name = 'tb_lancamentovenda';
	public $_primary = array('id_lancamento', 'id_venda');
	public $_referenceMap = array(
	'LancamentoORM' => array(
	            'columns'           => 'id_lancamento',
	            'refTableClass'     => 'LancamentoORM',
	            'refColumns'        => 'id_lancamento'
	),
	'VendaORM' => array(
		'columns'			=> 'id_venda',
		'refTableClass'		=> 'VendaORM',
		'refColumns'		=> 'id_venda'
	));
}

?>
<?php

 /**
* Classe que encapsula os dados da view vw_ocorrenciaspessoa
* @author Denise - denisexavier@ead1.com.br
* @package models
* @subpackage orm
*/
class VwOcorrenciasPessoaORM extends Ead1_ORM {
	public $_name = 'vw_ocorrenciaspessoa';
	public $_primary = array('id_usuario', 'id_funcao', 'id_nucleoco');
}

?>
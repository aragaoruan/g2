<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_observacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class ObservacaoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_observacao';
	public $_primary 			= array( 'id_observacao' );
	public $_dependentTables	= array( 'ObservacaoEntregaMaterialORM');
	
	public $_referenceMap 		= array( 'EntidadeORM'	=> array(
														            'columns'           => 'id_entidade',
														            'refTableClass'     => 'EntidadeORM',
														            'refColumns'        => 'id_entidade'
														),
										'UsuarioORM' 			=> array(
																	'columns'			=> 'id_usuariocadastro',
																	'refTableClass'		=> 'UsuarioORM',
																	'refColumns'		=> 'id_usuario'
																	)
										);
}
<?php
/**
 * Classe que faz o mapeamento da view que retorna o projeto pedagogico
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwProjetoPedagogicoORM extends Ead1_ORM{
	public $_name = 'vw_projetopedagogico';
	public $_primary = array('id_projetopedagogico', 'id_tipoextracurricular', 'id_tipoprovafinal', 'id_tipoaplicacaoavaliacao',
							 'id_projetocontratomultatipo', 'id_projetocontratoduracaotipo', 'id_usuario');
}
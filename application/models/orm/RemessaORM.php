<?php

/**
 * Classe de RemessaORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class RemessaORM extends Ead1_ORM {

	public $_name 		= 'tb_remessa';
	public $_primary 	= 'id_remessa';

}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_ocorrencia
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwOcorrenciaORM extends Ead1_ORM {
	
	public $_name = 'vw_ocorrencia';
	public $_primary = array('id_ocorrencia');
}

?>
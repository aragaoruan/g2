<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_serie
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SerieORM extends Ead1_ORM {

	public $_name = 'tb_serie';
	public $_primary = array('id_serie');
	public $_dependentTables = array('SerieORM', 'SerieNivelEnsinoORM');
	public $_referenceMap = array(
	'SerieORM' => array(
	            'columns'           => 'id_serieanterior',
	            'refTableClass'     => 'SerieORM',
	            'refColumns'        => 'id_serie'
	));
}

?>
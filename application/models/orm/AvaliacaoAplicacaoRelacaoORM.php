<?php
class AvaliacaoAplicacaoRelacaoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoaplicacaorelacao';
	public $_primary 			= array( 'id_avaliacao', 'id_avaliacaoaplicacao');
	
	public $_referenceMap 		= array(    'AvaliacaoORM'              => array(
                                                                                            'columns'           => 'id_avaliacao',
                                                                                            'refTableClass'     => 'AvaliacaoORM',
                                                                                            'refColumns'        => 'id_avaliacao'
                                                                                    ),
                                                    'AvaliacaoAplicacaoORM' 	=> array(
                                                                                            'columns'           => 'id_avaliacaoaplicacao',
                                                                                            'refTableClass'     => 'AvaliacaoAplicacaoORM',
                                                                                            'refColumns'        => 'id_avaliacaoaplicacao'
                                                                                    )
                                                    );
}
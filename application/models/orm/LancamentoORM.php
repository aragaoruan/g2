<?php
/**
 * Classe responsável pelo mapeamento objeto relacional da tabela tb_lancamento
 * 
 * @author edermariano
 * 
 * [Quem Utiliza]
 * 	@see ProdutoDAO::cadastrarLancamento() (insert)
 * 	@see ProdutoDAO::editarLancamento() (update)
 * 	@see ProdutoDAO::deletarLancamento() (delete)
 * 	@see ProdutoDAO::retornarLancamento() (fetchAll)
 *
 * @package models
 * @subpackage orm
 */
class LancamentoORM extends Ead1_ORM {

	public $_name = 'tb_lancamento';
	public $_primary = array('id_lancamento');
	public $_dependentTables = array('LancamentoVendaORM');
	public $_referenceMap = array(
		'TipoLancamentoORM' => array(
			'columns'			=> 'id_tipolancamento',
			'refTableClass'		=> 'TipoLancamentoORM',
			'refColumns'		=> 'id_tipolancamento'
		),
		'MeioPagamentoORM' => array(
			'columns'			=> 'id_meiopagamento',
			'refTableClass'		=> 'MeioPagamentoORM',
			'refColumns'		=> 'id_meiopagamento'
		)
	);
}
?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_usuarioperfilentidadereferencia
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class UsuarioPerfilEntidadeReferenciaORM extends Ead1_ORM {

	public $_name = 'tb_usuarioperfilentidadereferencia';
	public $_primary = array('id_perfilreferencia');
	public $_dependentTables = array('SalaDeAulaProfessorORM');
	public $_referenceMap = array(
	'UsuarioPerfilEntidadeORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioPerfilEntidadeORM',
	            'refColumns'        => 'id_usuario'
	),
	'UsuarioPerfilEntidadeORM2' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'UsuarioPerfilEntidadeORM',
	            'refColumns'        => 'id_perfil'
	),
	'UsuarioPerfilEntidadeORM3' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'UsuarioPerfilEntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaORM',
	            'refColumns'        => 'id_disciplina'
	),
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'SalaDeAulaORM' => array(
	            'columns'           => 'id_saladeaula',
	            'refTableClass'     => 'SalaDeAulaORM',
	            'refColumns'        => 'id_saladeaula'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto-relacional para a tabela tb_tipoareaconhecimento
 * @author Arthur Cláudio de Almeida Pereira
 *
 */
class TipoAreaConhecimentoORM extends Ead1_ORM {
	
	/**
	 * Nome da tabela 
	 * @var string
	 */
	protected $_name	= 'tb_tipoareaconhecimento';
	
	/**
	 * Nome da chave primária
	 * @var string
	 */
	protected $_primary	= 'id_tipoareaconhecimento';
	
	/**
	 * Lista de classes ORM que possuem relacionamento com a tabela
	 * @var array
	 */
	protected $_dependentTables	= array( 'AreaConhecimentoORM' );
}
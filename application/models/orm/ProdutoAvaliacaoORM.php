<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class ProdutoAvaliacaoORM extends Ead1_ORM{
	public $_name = 'tb_produtoavaliacao';
	public $_primary = array('id_entidade', 'id_avaliacao');
}
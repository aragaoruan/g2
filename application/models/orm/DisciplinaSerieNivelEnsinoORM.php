<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_disciplinaserienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DisciplinaSerieNivelEnsinoORM extends Ead1_ORM
{

    public $_name = 'tb_disciplinaserienivelensino';
    public $_primary = array('id_disciplina', 'id_serie', 'id_nivelensino');
    public $_dependentTables = array('ModuloDisciplinaORM', 'ItemDeMaterialDisciplinaORM');
    public $_referenceMap = array(
        'SerieNivelEnsinoORM' => array(
            'columns' => 'id_serie',
            'refTableClass' => 'SerieNivelEnsinoORM',
            'refColumns' => 'id_serie'
        ),
        'SerieNivelEnsinoORM' => array(
            'columns' => 'id_nivelensino',
            'refTableClass' => 'SerieNivelEnsinoORM',
            'refColumns' => 'id_nivelensino'
        ),
        'DisciplinaORM' => array(
            'columns' => 'id_disciplina',
            'refTableClass' => 'DisciplinaORM',
            'refColumns' => 'id_disciplina'
    ));

}

?>
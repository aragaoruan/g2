<?php
/**
 * Mapeamento da view vw_disciplinaareaconhecimentonivelserie
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAreaConhecimentoDisciplinaSerieNivelORM extends Ead1_ORM{
	
	public $_name = 'vw_disciplinaareaconhecimentonivelserie';
	public $_primary = array('id_disciplina', 'id_entidade', 'id_areaconhecimento','id_nivelensino','id_serie');
}
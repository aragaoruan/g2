<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avaliacaoconjuntorelacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class AvaliacaoConjuntoRelacaoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoconjuntorelacao';
	public $_primary 			= array( 'id_avaliacaoconjuntorelacao' );
	public $_referenceMap 		= array( 'AvaliacaoORM'	=> array(
														            'columns'           => 'id_avaliacao',
														            'refTableClass'     => 'AvaliacaoORM',
														            'refColumns'        => 'id_avaliacao'
														),
										'AvaliacaoConjuntoORM' 			=> array(
																	'columns'           => 'id_avaliacaoconjunto',
										            				'refTableClass'     => 'AvaliacaoConjuntoORM',
										            				'refColumns'        => 'id_avaliacaoconjunto'
										),
										'AvaliacaoORM' 			=> array(
																	'columns'			=> 'id_avaliacaorecupera',
																	'refTableClass'		=> 'AvaliacaoORM',
																	'refColumns'		=> 'id_avaliacaorecupera'
										)
									);
}
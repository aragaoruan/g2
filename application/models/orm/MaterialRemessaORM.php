<?php

/**
 * Classe de MaterialRemessaORM
 
 * @package models
 * @subpackage orm
 */
 class MaterialRemessaORM extends Ead1_ORM {

	public $_name = 'tb_materialremessa';
	public $_primary = array('id_materialremessa');

}

?>
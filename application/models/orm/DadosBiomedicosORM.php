<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class DadosBiomedicosORM extends Ead1_ORM{
	public $_name = 'tb_dadosbiomedicos';
	public $_primary = array('id_dadosbiomedicos');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_textocategoria
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TextoCategoriaORM extends Ead1_ORM{
	
	public $_name = 'tb_textocategoria';
	public $_primary = 'id_textocategoria';
	
	public $_dependentTables = array('TextoExibicaoCategoriaORM');
}
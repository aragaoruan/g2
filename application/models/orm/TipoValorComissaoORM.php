<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipovalorcomissao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoValorComissaoORM extends Ead1_ORM {

	public $_name = 'tb_tipovalorcomissao';
	public $_primary = array('id_tipovalorcomissao');
	public $_dependentTables = array('RegraComissaoORM');
}

?>
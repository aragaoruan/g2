<?php
/**
 * @package models
 * @subpackage orm
 */
class LivroEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_livroentidade';
	public $_primary = 'id_livroentidade';
}

?>
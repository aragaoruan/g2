<?php
/**
 * Classe que faz o mapeamento da view que retorna os textos, categoria e suas variaveis
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwGerarTextoORM extends Ead1_ORM{
	
	public $_name = 'vw_gerartexto';
	public $_primary = array('id_textosistema','id_categoria');
}
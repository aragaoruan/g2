<?php
/**
 * Classe de mapeamento objeto relacional da view vw_produtovendalivro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwProdutoVendaLivroORM extends Ead1_ORM{
	
	public $_name = 'vw_produtovendalivro';
	public $_primary = array('id_produto', 'id_venda', 'id_livro');
}
<?php
/**
 * Mapeamento da view de avaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoConjuntoORM extends Ead1_ORM{
	public $_name = 'vw_avaliacaoconjunto';
	public $_primary = array('id_avaliacaoconjunto', 'id_situacao', 'id_tipoprova', 'id_tipocalculoavaliacao');
}
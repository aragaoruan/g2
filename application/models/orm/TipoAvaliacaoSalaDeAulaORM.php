<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoavaliacaosaladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoAvaliacaoSalaDeAulaORM extends Ead1_ORM {

	public $_name = 'tb_tipoavaliacaosaladeaula';
	public $_primary = array('id_tipoavaliacaosaladeaula');
	public $_dependentTables = array('SalaDeAulaProfessorORM');
}

?>
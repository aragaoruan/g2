<?php
/**
 * Mapeamento da view que mostra as dependencias da avaliacao conjunto relacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoConjuntoRelacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_avaliacaoconjuntorelacao';
	public $_primary = array('id_avaliacaoconjuntorelacao', 'id_avaliacao', 'id_avaliacaorecupera', 'id_avaliacaoconjunto');
}
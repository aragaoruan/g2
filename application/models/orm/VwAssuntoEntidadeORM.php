<?php

/**
 * VwAssuntoEntidadeORM
 *
 * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
 */
class VwAssuntoEntidadeORM extends Ead1_ORM {
    
    protected $_name = 'vw_assuntoentidade';
    
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class TurmaProjetoORM extends Ead1_ORM{
	public $_name = 'tb_turmaprojeto';
	public $_primary = array('id_turmaprojeto');
}
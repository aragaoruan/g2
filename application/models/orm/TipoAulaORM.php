<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoAulaORM extends Ead1_ORM {

	public $_name = 'tb_tipoaula';
	public $_primary = array('id_tipoaula');
	public $_dependentTables = array('AulaSalaDeAulaORM');
}

?>
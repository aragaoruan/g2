<?php

/**
 * Classe de PacoteEntregaORM
 
 * @package models
 * @subpackage orm
 */
 class PacoteEntregaORM extends Ead1_ORM {

	public $_name = 'tb_pacoteentrega';
	public $_primary = array('id_pacoteentrega');

}

?>
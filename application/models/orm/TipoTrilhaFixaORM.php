<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipotrilhafixa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoTrilhaFixaORM extends Ead1_ORM {

	public $_name = 'tb_tipotrilhafixa';
	public $_primary = array('id_tipotrilhafixa');
	public $_referenceMap = array(
	'TipoTrilhaORM' => array(
	            'columns'           => 'id_tipotrilha',
	            'refTableClass'     => 'TipoTrilhaORM',
	            'refColumns'        => 'id_tipotrilha'
	));
}

?>
<?php
/**
 * Mapeamento da view encapsula dados do horario aula e dia da semana
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwHorarioAulaDiaSemanaORM extends Ead1_ORM{
	
	public $_name = 'vw_horarioauladiasemana';
	public $_primary = array('id_horarioaula', 'id_diasemana');
	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_lancamentovenda
 * @author edermariano
 *
 */
class VendaMeioVencimentoORM extends Ead1_ORM {

	public $_name = 'tb_vendameiovencimento';
	public $_primary = array('id_vendameiovencimento');
}

?>
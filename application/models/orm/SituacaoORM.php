<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_situacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SituacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_situacao';
	public $_primary = array('id_situacao');
	public $_dependentTables = array('EntidadeORM', 'FuncionalidadeORM', 'PessoaORM',
									 'PerfilORM', 'PermissaoORM', 'UsuarioPerfilEntidadeORM', 'EntidadeFuncionalidadeORM',
							 		 'ProjetoPedagogicoORM', 'DisciplinaORM', 'ModuloORM', 'FundamentoLegalORM', 'ItemDeMaterialORM',
									 'TipoDeMaterialORM');
	
}
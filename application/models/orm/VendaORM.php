<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_venda
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VendaORM extends Ead1_ORM {

	public $_name = 'tb_venda';
	public $_primary = array('id_venda');
	public $_dependentTables = array('LancamentoVendaORM', 'VendaProdutoORM', 'ContratoORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	),
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	),
	'EvolucaoORM' => array(
	            'columns'           => 'id_evolucao',
	            'refTableClass'     => 'EvolucaoORM',
	            'refColumns'        => 'id_evolucao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto - relacional da tb_tipolancamentoimportacao
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class TipoLancamentoImportacaoORM extends Ead1_ORM {
	
	
	protected $_name	= 'tb_tipolancamentoimportacao';
	protected $_primary	= 'id_tipolancamentoimportacao';
}
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_avaliacaodisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoDisciplinaORM extends Ead1_ORM{
	public $_name = 'vw_avaliacaodisciplina';
	public $_primary = 'id_avaliacaodisciplina';
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_livroregistro
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class LivroRegistroORM extends Ead1_ORM  {

	public $_name 		= 'tb_livroregistro';
	public $_primary 	= array('id_livroregistroentidade', 'id_matricula');
	
	public $_referenceMap = array(
									'LivroRegistroEntidadeORM' => array(
									            'columns'           => 'id_livroregistroentidade',
									            'refTableClass'     => 'LivroRegistroEntidadeORM',
									            'refColumns'        => 'id_livroregistroentidade'
											),
									'UsuarioORM' => array(
									            'columns'           => 'id_usuariocadastro',
									            'refTableClass'     => 'UsuarioORM',
									            'refColumns'        => 'id_usuario'
											),
									'MatriculaORM' => array(
									            'columns'           => 'id_matricula',
									            'refTableClass'     => 'MatriculaORM',
									            'refColumns'        => 'id_matricula'
											)
									);
}


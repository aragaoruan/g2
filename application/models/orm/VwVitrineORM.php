<?php
/**
 * Classe de mapeamento objeto relacional da vw_vitrine
 * @author Denise Xavier <denise.xavier07@gmail.com>
 * @package models
 * @subpackage orm
 */
class VwVitrineORM extends Ead1_ORM {
	public $_name = 'vw_vitrine';
	public $_primary = array('id_contratoafiliado', 'id_vitrine');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoproduto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoProdutoORM extends Ead1_ORM {

	public $_name = 'tb_tipoproduto';
	public $_primary = array('id_tipoproduto');
	public $_dependentTables = array('ProdutoORM');
}

?>
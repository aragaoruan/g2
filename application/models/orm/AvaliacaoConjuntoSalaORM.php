<?php
class AvaliacaoConjuntoSalaORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoconjuntosala';
	public $_primary 			= array( 'id_avaliacaoconjuntosala' );
	
	public $_referenceMap 		= array( 'AvaliacaoConjuntoORM'	=> array(
														            'columns'           => 'id_avaliacaoconjunto',
														            'refTableClass'     => 'AvaliacaoConjuntoORM',
														            'refColumns'        => 'id_avaliacaoconjunto'
														),
										'SalaDeAulaORM' 			=> array(
																	'columns'			=> 'id_saladeaula',
																	'refTableClass'		=> 'SalaDeAulaORM',
																	'refColumns'		=> 'id_saladeaula'
														)
										);
}
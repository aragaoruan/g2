<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_usuario
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class UsuarioORM extends Ead1_ORM{
	
	public $_name = 'tb_usuario';
	public $_primary = 'id_usuario';
}
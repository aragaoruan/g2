<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodivisaofinanceira
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDivisaoFinanceiraORM extends Ead1_ORM {

	public $_name = 'tb_tipodivisaofinanceira';
	public $_primary = array('id_tipodivisaofinanceira');
	public $_dependentTables = array('FormaMeioPagamentoORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_vendalancamento
 * @author Dimas Sulz
 * 
 */
class VwVendaLancamentoORM extends Ead1_ORM
{
	public $_name = 'vw_vendalancamento';
	public $_primary = array('id_lancamento', 'id_venda');
}
?>
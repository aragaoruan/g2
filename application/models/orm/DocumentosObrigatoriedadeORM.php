<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentosobrigatoriedade
 * @author Arthur Cláudio de Almeida Pereira - arthur.almeida@ead1.com.br
 *
 * @package models
 * @subpackage orm
 */
class DocumentosObrigatoriedadeORM	extends Ead1_ORM {
	
	
	protected $_name	= 'tb_documentosobrigatoriedade';
	protected $_primary	= 'id_documentosobrigatoriedade';
	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilpermissaofuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilPermissaoFuncionalidadeORM extends Ead1_ORM{
	public $_name = 'tb_perfilpermissaofuncionalidade';
	public $_primary = array('id_perfil', 'id_funcionalidade', 'id_permissao');
}
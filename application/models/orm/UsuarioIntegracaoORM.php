<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_usuariointegracao
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class UsuarioIntegracaoORM extends Ead1_ORM {
	public $_name = 'tb_usuariointegracao';
	public $_primary = 'id_usuariointegracao';

}
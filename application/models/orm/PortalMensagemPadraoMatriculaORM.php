<?php

/**
 * Description of PortalMensagemPadraoMatriculaORM
 *
 * @author Yannick Naquis Roulé
 */
class PortalMensagemPadraoMatriculaORM extends Ead1_ORM{
	
	public $_name = 'tb_mensagempadraomatricula';
	public $_primary = array('id_mensagempadrao');
}

?>

<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_matriculadisciplina
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class MatriculaDisciplinaORM extends Ead1_ORM{
	
	public $_name = 'tb_matriculadisciplina';
	public $_primary = 'id_matriculadisciplina';
	public $_dependentTables = array('AproveitamentoMatriculaDisciplinaORM');
	public $_referenceMap = array(
	'EvolucaoORM' => array(
	            'columns'           => 'id_evolucao',
	            'refTableClass'     => 'EvolucaoORM',
	            'refColumns'        => 'id_evolucao'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'MatriculaORM' => array(
	            'columns'           => 'id_matricula',
	            'refTableClass'     => 'MatriculaORM',
	            'refColumns'        => 'id_matricula'
	),
	'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaORM',
	            'refColumns'        => 'id_disciplina'
	));
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwDisciplinasExtrasORM extends Ead1_ORM{
	public $_name = 'vw_disciplinasextras';
	public $_primary = array('id_matricula');
}
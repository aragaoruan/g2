<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_setorfuncionario
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class SetorFuncionarioORM extends Ead1_ORM {

	public $_name = 'tb_setorfuncionario';
	public $_primary = array('id_setorfuncionario');

}
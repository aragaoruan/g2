<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_banco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class BancoORM extends Ead1_ORM{
	
	public $_name = 'tb_banco';
	public $_primary = array('id_banco');
	public $_dependentTables = array('ContaPessoaORM', 'ContaEntidadeORM');
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_meiopagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class MeioPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_meiopagamento';
	public $_primary = array('id_meiopagamento');
	public $_dependentTables = array('FormaMeioPagamentoORM','CampanhaDescontoORM');
}

?>
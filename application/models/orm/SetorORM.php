<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_setor
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class SetorORM extends Ead1_ORM {

	public $_name = 'tb_setor';
	public $_primary = array('id_setor');

}

?>
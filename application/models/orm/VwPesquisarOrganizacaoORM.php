<?php
/**
 * Mapeamento da view que mostra pesquisar organizações
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarOrganizacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisarorganizacao';
	public $_primary = array('id_entidade', 'id_situacao', 'id_entidadepai', 'id_entidadeclasse');
}
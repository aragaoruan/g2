<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_saladeaulaprofessor
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SalaDeAulaProfessorORM extends Ead1_ORM {

	public $_name = 'tb_saladeaulaprofessor';
	public $_primary = array('id_saladeaulaprofessor');
	public $_referenceMap = array(
	'TipoAvaliacaoSalaDeAulaORM' => array(
		'columns'			=> 'id_tipoavaliacaosaladeaula',
		'refTableClass'		=> 'TipoAvaliacaoSalaDeAulaORM',
		'refColumns'		=> 'id_tipoavaliacaosaladeaula'
	),
	'UsuarioPerfilEntidadeReferenciaORM' => array(
		'columns'			=> 'id_perfilreferencia',
		'refTableClass'		=> 'UsuarioPerfilEntidadeReferenciaORM',
		'refColumns'		=> 'id_perfilreferencia'
	));
}

?>
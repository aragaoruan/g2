<?php
/**
 * Classe de CategoriaOcorrenciaORM
 * @package models
 * @subpackage orm
 *
 */
class CategoriaOcorrenciaORM extends Ead1_ORM {
	public $_name = 'tb_categoriaocorrencia';
	public $_primary = array('id_categoriaocorrencia');
}

?>
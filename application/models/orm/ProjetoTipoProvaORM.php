<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetotipoprova
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoTipoProvaORM extends Ead1_ORM{
	
	public $_name = 'tb_projetotipoprova';
	public $_primary = array('id_projetopedagogico', 'id_tipoprova');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'TipoProvaORM' => array(
	            'columns'           => 'id_tipoprova',
	            'refTableClass'     => 'TipoProvaORM',
	            'refColumns'        => 'id_tipoprova'
	));	
}
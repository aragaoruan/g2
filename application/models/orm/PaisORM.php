<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_pais
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PaisORM extends Ead1_ORM{
	
	public $_name = 'tb_pais';
	public $_primary = array('id_pais');
	public $_dependentTables = array('PessoaORM','EnderecoORM','DdiORM');
	
}
<?php
/**
 * Classe de mapeamento objeto-relacional da tabela tb_cartaooperadora
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class CartaoOperadoraORM extends Ead1_ORM {
	
	/**
	 * Nome da tabela
	 * @var string
	 */
	protected $_name	= 'tb_cartaooperadora';
	
	/**
	 * Nome da chave primária 
	 * @var string
	 */
	protected $_primary	= 'id_cartaooperadora';
	
	/**
	 * Lista de classes ORM que possuem dependência de chave estrangeira com a tabela
	 * @var array
	 */
	protected $_dependentTables	= array( 'CartaoConfigORM', 'CartaoOperadoraBandeiraORM' );
}
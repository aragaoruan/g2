<?php
/**
 * Classe que mapeia a view que retorna os meios de pagamento vinculados a uma venda
 * @autor Elcio Mauro Guimarães
 *
 * @package models
 * @subpackage orm
 */
class VwFormaMeioDivisaoVendaORM extends Ead1_ORM{
	public $_name = 'vw_formameiodivisaovenda';
	public $_primary = array('id_formapagamento', 'id_formapagamentoaplicacao', 'id_venda');
}
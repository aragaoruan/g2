<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_categoriaentidade
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class CategoriaEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_categoriaentidade';
	public $_primary = array('id_entidade','id_categoria');
	
}

?>
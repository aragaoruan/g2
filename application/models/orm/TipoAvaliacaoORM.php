<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoavaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class TipoAvaliacaoORM extends Ead1_ORM{
	
	public $_name 			= 'tb_tipoavaliacao';
	public $_primary 		= array( 'id_tipoavaliacao' );
	public $_dependentTables	= array( 'AvaliacaoORM');
	
}
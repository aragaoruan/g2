<?php
/**
 * Vw que lista as pessoas vinculadas a um núcleo
 * Classe de VwPessoasNucleoAssuntoORM
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
 class VwPessoasNucleoAssuntoORM extends Ead1_ORM {
	
 	public $_name = 'vw_pessoasnucleoassunto';
 	public $_primary = array('id_nucleoco');
}

?>
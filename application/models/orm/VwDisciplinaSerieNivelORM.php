<?php
/**
 * Mapeamento da view que as séries e o nivel de ensino de uma disciplina
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwDisciplinaSerieNivelORM extends Ead1_ORM{
	
	public $_name = 'vw_disciplinaserienivel';
	public $_primary = array('id_disciplina', 'id_serie', 'id_nivelensino');
}
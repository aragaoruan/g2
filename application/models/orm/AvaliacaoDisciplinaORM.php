<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avaliacaodisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AvaliacaoDisciplinaORM extends Ead1_ORM{
	public $_name = 'tb_avaliacaodisciplina';
	public $_primary = 'id_avaliacaodisciplina';
	
	public $_referenceMap = array(
	      	'AvaliacaoORM' => array(
	            'columns'           => 'id_avaliacao',
	            'refTableClass'     => 'AvaliacaoORM',
	            'refColumns'        =>'id_avaliacao'
			),
			'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaORM',
	            'refColumns'        => 'id_disciplina'
			)
	);
	
	
}
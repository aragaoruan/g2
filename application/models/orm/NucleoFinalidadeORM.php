<?php

 /**
  * Classe de NucleoFinalidadeORM
  * @package models
  * @subpackage orm
  *	@author Rafael Rocha - rafael.rocha.mg@gmail.com 
  */
class NucleoFinalidadeORM extends Ead1_ORM {

public $_name = 'tb_nucleofinalidade';
public $_primary = array('id_nucleofinalidade');

}

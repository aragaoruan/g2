<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_relatorio
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class RelatorioORM extends Ead1_ORM{
	
	public $_name = 'tb_relatorio';
	public $_primary = array('id_relatorio');
}
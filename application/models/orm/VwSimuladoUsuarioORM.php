<?php
/**
 * Classe de mapeamento objeto relacional da view vw_alocacaointegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwSimuladoUsuarioORM extends Ead1_ORM{
	public $_name = 'vw_simuladousuario';
	public $_primary = 'id_usuario';	
}

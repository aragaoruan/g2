<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author DIMAS SULZ
 * 
 * [Quem utiliza]
 * 	@see LoginDAO::retornaUsuarioPerfilEntidade()
 *
 * @package models
 * @subpackage orm
 */
class VwUsuarioPerfilEntidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_usuarioperfilentidade';
	public $_primary = array('id_usuario');
}
<?php
/**
 * Classe de mapeamento objeto relacional da vw tb_disciplinaintegracaomoodle
 * @author Denise Xavier
 * @package models
 * @subpackage orm
 */
class VwDisciplinaIntegracaoMoodleORM extends Ead1_ORM {
	public $_name = 'vw_disciplinaintegracaomoodle';
	public $_primary = array('id_disciplinaintegracao');
}

?>
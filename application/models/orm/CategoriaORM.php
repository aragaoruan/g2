<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_categoria
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class CategoriaORM extends Ead1_ORM {

	public $_name = 'tb_categoria';
	public $_primary = 'id_categoria';
	
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_pessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_pessoa';
	public $_primary = array('id_usuario','id_entidade');
	public $_dependentTables = array('PessoaEnderecoORM', 'InformacaoProfissionalPessoaORM', 'ContatosRedeSocialPessoaORM',
									 'ContatosEmailPessoaPerfilORM', 'InformacaoAcademicaPessoaORM', 'ContaPessoaORM',
									 'DadosBiomedicosORM', 'DocumentoIdentidadeORM', 'DocumentoTituloEleitorORM',
									 'DocumentoCertidaoNascimentoORM', 'DocumentoReservistaORM', 'ContatosTelefonePessoaORM',
									 'ContatoEntidadeORM');
	public $_referenceMap = array(
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuariocadastro'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'PaisORM' => array(
	            'columns'           => 'id_pais',
	            'refTableClass'     => 'PaisORM',
	            'refColumns'        => 'id_pais'
			),
	'UfORM' => array(
	            'columns'           => 'sg_uf',
	            'refTableClass'     => 'UfORM',
	            'refColumns'        => 'sg_uf'
			),
	'MunicipioORM' => array(
	            'columns'           => 'id_municipio',
	            'refTableClass'     => 'MunicipioORM',
	            'refColumns'        => 'id_municipio'
			),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
			),
	'EstadoCivilORM' => array(
	            'columns'           => 'id_estadocivil',
	            'refTableClass'     => 'EstadoCivilORM',
	            'refColumns'        => 'id_estadocivil'
			),
	'UsuarioCadastroORM' => array(
	            'columns'           => 'id_usuariocadastro',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
			)
	);
	
}
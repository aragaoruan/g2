<?php
/**
 * Mapeamento da view que mostra as dependencias da disciplina de um modulo
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwDependenciaDisciplinaModuloORM extends Ead1_ORM{
	
	public $_name = 'vw_dependenciadisciplinamodulo';
	public $_primary = array('id_projetopedagogico', 'id_modulo', 'id_disciplina', 'id_trilha', 'id_tipotrilha', 'id_serie', 'id_nivelensino');
}
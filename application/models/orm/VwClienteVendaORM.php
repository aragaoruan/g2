<?php
/**
 * Classe de mapeamento objeto relacional da view vw_clientevenda
 * @author Dimas Sulz
 * 
 */
class VwClienteVendaORM extends Ead1_ORM
{
	public $_name = 'vw_clientevenda';
	public $_primary = array('id_venda');
}
?>
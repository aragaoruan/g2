<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatosredesocial
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosRedeSocialORM extends Ead1_ORM{
	
	public $_name = 'tb_contatosredesocial';
	public $_primary = array('id_redesocial');
	public $_dependentTables = array('ContatosRedeSocialPessoaORM');
	public $_referenceMap = array(
	'TipoRedeSocialORM' => array(
	            'columns'           => 'id_tiporedesocial',
	            'refTableClass'     => 'TipoRedeSocialORM',
	            'refColumns'        => 'id_tiporedesocial'
			)		
	);	
}
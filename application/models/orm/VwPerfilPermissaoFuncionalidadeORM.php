<?php
/**
 * Mapeamento da view que mostra as permissoes de um perfil do usuário do sistema
 * @author DIMAS SULZ
 *
 * @package models
 * @subpackage orm
 */
class VwPerfilPermissaoFuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_perfilpermissaofuncionalidade';
	public $_primary = array('id_perfil', 'id_funcionalidade', 'id_situacao', 'id_permissao');
}
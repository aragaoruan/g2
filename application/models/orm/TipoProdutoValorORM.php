<?php
/**
 * Classe responsável pelo mapeamento objeto relacional da tabela tb_tipoprodutovalor
 * 
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package models
 * @subpackage orm
 */
class TipoProdutoValorORM extends Ead1_ORM{
	
	public $_name = 'tb_tipoprodutovalor';
	public $_primary = array('id_tipoprodutovalor');
	public $_dependentTables = array('ProdutoValorORM');
}
<?php
/**
 * Classe de mapeamento objeto relacional da vw_listagem_projetos_sala
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @package models
 * @subpackage orm
 */
class VwListagemProjetosSalaORM extends Ead1_ORM {
	public $_name = 'vw_listagem_projetos_sala';
	public $_primary = array('id_projetopedagogico');
}

?>
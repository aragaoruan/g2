<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocalculoavaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class TipoCalculoAvaliacaoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_tipocalculoavaliacao';
	public $_primary 			= array( 'id_tipocalculoavaliacao' );
	public $_dependentTables	= array( 'AvaliacaoConjuntoORM');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_modelovitrine
 * @author Denise Xavier <denise.xavier07@gmail.com>
 * @package models
 * @subpackage orm
 */
class ModeloVitrineORM extends Ead1_ORM {

	public $_name = 'tb_modelovitrine';
	public $_primary = array('id_modelovitrine');
}

?>
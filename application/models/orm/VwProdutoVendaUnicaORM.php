<?php
/**
 * Classe para encapsular os dados da vw_vitrine
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @package models
 * @subpackage orm
 */
class VwProdutoVendaUnicaORM extends Ead1_ORM {
	public $_name = 'vw_produtovendaunica';
	public $_primary = array('id_produto');
}

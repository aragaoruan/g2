<?php
class FormaPagamentoProdutoORM extends Ead1_ORM{
	
	public $_name = 'tb_formapagamentoproduto';
	public $_primary = array('id_formapagamento', 'id_produto');
	
}
<?php
/**
 * Classe para mapeamento objeto relacional da vw_dadosrelatorio
 * @author Eder Lamar edermariano@gmail.com
 * 
 * @package models
 * @subpackage orm
 */
class VwDadosRelatorioORM extends Ead1_ORM{
	public $_name = 'vw_dadosrelatorio';
	public $_primary = 'id_relatorio';
}
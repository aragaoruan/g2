<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_uf
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class UfORM extends Ead1_ORM{
	
	public $_name = 'tb_uf';
	public $_primary = array('sg_uf');
	public $_dependentTables = array('PessoaORM','EnderecoORM', 'DocumentoReservistaORM', 'MunicipioORM');
	
}
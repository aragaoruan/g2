<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoorigemrelatorio
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class TipoOrigemRelatorioORM extends Ead1_ORM{
	
	public $_name = 'tb_tipoorigemrelatorio';
	public $_primary = 'id_tipoorigemrelatorio';
}
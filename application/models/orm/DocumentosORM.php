<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentos
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosORM extends Ead1_ORM {

	public $_name = 'tb_documentos';
	public $_primary = array('id_documentos');
	public $_dependentTables = array('DocumentosUtilizacaoRelacaoORM', 'DocumentosCategoriaRelacaoORM', 'DocumentosProjetoPedagogicoORM','DocumentosSubstituicaoORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'UsuarioORM' => array(
	            'columns'           => 'id_usuariocadastro',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
	),
	'TipoDocumentosORM' => array(
	            'columns'           => 'id_tipodocumentos',
	            'refTableClass'     => 'TipoDocumentosORM',
	            'refColumns'        => 'id_tipodocumentos'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	));
}

?>
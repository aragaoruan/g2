<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipofundamentolegal
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoFundamentoLegalORM extends Ead1_ORM{

	public $_name = 'tb_tipofundamentolegal';
	public $_primary = array('id_tipofundamentolegal');
	public $_dependentTables = array('TipoFundamentoLegalORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatostelefoneentidadecontato
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosTelefoneEntidadeContatoORM extends Ead1_ORM {

	public $_name = 'tb_contatostelefoneentidadecontato';
	public $_primary = array('id_telefone', 'id_contatoentidade');
	public $_referenceMap = array(
	'ContatosTelefoneORM' => array(
	            'columns'           => 'id_telefone',
	            'refTableClass'     => 'ContatosTelefoneORM',
	            'refColumns'        => 'id_telefone'
	),
	'ContatoEntidadeORM' => array(
	            'columns'           => 'id_contatoentidade',
	            'refTableClass'     => 'ContatoEntidadeORM',
	            'refColumns'        => 'id_contatoentidade'
	));
}

?>
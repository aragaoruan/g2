<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipotelefoneentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoTelefoneEntidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_tipotelefoneentidade';
	public $_primary = array('id_entidade','id_tipotelefone');
	//public $_dependentTables = array('ContatosTelefoneORM', 'TipoTelefoneEntidadeORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'TipoTelefoneORM' => array(
	            'columns'           => 'id_tipotelefone',
	            'refTableClass'     => 'TipoTelefoneORM',
	            'refColumns'        => 'id_tipotelefone'
	));
}
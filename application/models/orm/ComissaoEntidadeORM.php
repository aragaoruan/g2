<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_comissaoentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ComissaoEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_comissaoentidade';
	public $_primary = array('id_comissaoentidade');
	public $_dependentTables = array('RegraComissaoORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	 ),
	'TipoCalculoComORM' => array(
		'columns'			=> 'id_tipocalculocom',
		'refTableClass'		=> 'TipoCalculoComORM',
		'refColumns'		=> 'id_tipocalculocom'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_dadosalocacao 
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwDadosAlocacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_dadosalocacao';
	public $_primary = 'id_matricula';
}
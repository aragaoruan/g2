<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @package models
 * @subpackage orm
 */
 
class VwSalaDeAulaPerfilProjetoIntegracaoORM extends Ead1_ORM{
	public $_name = 'vw_saladeaulaperfilprojetointegracao';
	public $_primary = array('id_usuario');
}
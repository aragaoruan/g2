<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentosprojetopedagogico
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosProjetoPedagogicoORM extends Ead1_ORM {

	public $_name = 'tb_documentosprojetopedagogico';
	public $_primary = array('id_documentos', 'id_projetopedagogico');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'DocumentosORM' => array(
		'columns'			=> 'id_documentos',
		'refTableClass'		=> 'DocumentosORM',
		'refColumns'		=> 'id_documentos'
	));
}

?>
<?php
/**
 * Classe de gateway com a tabela tb_contratoregra
 * Essa tabela armazena as possíveis regras que um contrato pode possuir. 
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class ContratoRegraORM extends Ead1_ORM {
	
	protected $_name	= 'tb_contratoregra';
	protected $_primary	= 'id_contratoregra';
	
	
	protected $_dependentTables	= array( 'ContratoORM' );
	
	protected $_referenceMap	= array( 'ProjetoContratoDuracaoTipoORM'	=>	array(
																				'columns'		=>	'id_projetocontratoduracaotipo', 
																				'refTableClss'	=>	'ProjetoContratoDuracaoTipoORM', 
																				'refColumns'	=>	'id_projetocontratoduracaotipo' 
																				),
										 'EntidadeORM'						=>	array(
																				'columns'		=>	'id_entidade', 
																				'refTableClss'	=>	'EntidadeORM', 
																				'refColumns'	=>	'id_entidade' 
																				) );
																				
	
}
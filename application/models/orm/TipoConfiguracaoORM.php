<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoconfiguracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoConfiguracaoORM extends Ead1_ORM {

	public $_name = 'tb_tipoconfiguracao';
	public $_primary = array('id_tipoconfiguracao');
	public $_dependentTables = array('ConfiguracaoEntidadeORM');
	
}

?>
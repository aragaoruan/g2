<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_processamento
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class ProcessamentoORM extends Ead1_ORM {
	
	public $_name = 'tb_processamento';
	public $_primary = array('id_processamento');
}

?>
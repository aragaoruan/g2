<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_textosistema
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TextoSistemaORM extends Ead1_ORM{
	public $_name = 'tb_textosistema';
	public $_primary = 'id_textosistema';
}
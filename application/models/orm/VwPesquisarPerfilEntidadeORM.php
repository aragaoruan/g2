<?php
/**
 * Mapeamento da view que mostra o pesquisar de perfil de acordo com a entidade (instituição)
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarPerfilEntidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisarperfilentidade';
	public $_primary = array('id_perfil', 'id_entidadeclasse', 'id_entidade');
}
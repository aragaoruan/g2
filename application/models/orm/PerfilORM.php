<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfil
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilORM extends Ead1_ORM{
	
	public $_name = 'tb_perfil';
	public $_primary = array('id_perfil');
	public $_dependentTables = array('PerfilORM','PerfilEntidadeFuncionalidadeORM','PerfilAliasORM','UsuarioPerfilEntidadeORM', 'ContatosEmailPessoaPerfilORM');
	public $_referenceMap = array(
	'PerfilORM' => array(
	            'columns'           => 'id_perfilpai',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        =>'id_perfil'
			),
	'EntidadeClasseORM' => array(
				'columns'			=> 'id_entidadeclasse',
				'refTableClass'		=> 'EntidadeClasseORM',
				'refColumns'		=> 'id_entidadeclasse'
			),
	'SituacaoORM' => array(
				'columns'			=> 'id_situacao',
				'refTableClass'		=> 'SituacaoORM',
				'refColumns'		=> 'id_situacao'
			),	
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
			),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'PerfilPedagogicoORM' => array(
		'columns'			=> 'id_perfilpedagogico',
		'refTableClass'		=> 'PerfilPedagogicoORM',
		'refColumns'		=> 'id_perfilpedagogico'
	),
	'SistemaORM' => array(
		'columns'			=> 'id_sistema',
		'refTableClass'		=> 'SistemaORM',
		'refColumns'		=> 'id_sistema'
	)
	);
	
}
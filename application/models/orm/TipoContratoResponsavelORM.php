<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocontratoresponsavel
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoContratoResponsavelORM extends Ead1_ORM {

	public $_name = 'tb_tipocontratoresponsavel';
	public $_primary = array('id_tipocontratoresponsavel');
	public $_dependentTables = array('ContratoResponsavelORM');
}

?>
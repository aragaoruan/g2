<?php
/**
 * Vw que lista os assuntos para montar combos das ocorrências
 * Classe de VwAssuntosORM
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwAssuntosORM  extends  Ead1_ORM{

    public $_name = 'vw_assuntos';
    public $_primary = array('id_assuntoco');
}
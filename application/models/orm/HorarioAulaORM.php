<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_horarioaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class HorarioAulaORM extends Ead1_ORM {

	public $_name = 'tb_horarioaula';
	public $_primary = array('id_horarioaula');
	public $_dependentTables = array('HorarioDiaSemanaORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'TurnoORM' => array(
	            'columns'           => 'id_turno',
	            'refTableClass'     => 'TurnoORM',
	            'refColumns'        => 'id_turno'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwUsuarioPessoaORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoausuario';
	public $_primary = array('id_registropessoa', 'id_entidade', 'id_usuario');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_acessoafiliado
 * @author Elcio Guimarães - <elcioguimaraes@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class AcessoAfiliadoORM extends Ead1_ORM{
	
	public $_name = 'tb_acessoafiliado';
	public $_primary = array('id_acessoafiliado');
	
	
}
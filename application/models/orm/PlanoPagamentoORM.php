<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_produto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PlanoPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_planopagamento';
	public $_primary = array('id_planopagamento');
	public $_referenceMap = array(
	'ProdutoORM' => array(
		'columns'			=> 'id_produto',
		'refTableClass'		=> 'ProdutoORM',
		'refColumns'		=> 'id_produto'
	));
}

?>
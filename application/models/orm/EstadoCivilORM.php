<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_estadocivil
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */ 
class EstadoCivilORM extends Ead1_ORM{
	public $_name = 'tb_estadocivil';
	public $_primary = array('id_estadocivil');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaintegracao 
 * @author Eder Lamar
 * @package models
 * @subpackage orm
 */
class AreaIntegracaoORM extends Ead1_ORM{
	public $_name = 'tb_areaintegracao';
	public $_primary = 'id_areaintegracao';
}
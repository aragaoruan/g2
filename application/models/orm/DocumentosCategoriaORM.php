<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentoscategoria
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosCategoriaORM extends Ead1_ORM {

	public $_name = 'tb_documentoscategoria';
	public $_primary = array('id_documentoscategoria');
	public $_dependentTables = array('DocumentosCategoriaRelacaoORM');
}

?>
<?php
/**
 * Classe ORM que define o VIEW vw_encerramentosalas
 * 
 * @package models
 * @subpackage orm
 */
class VwEncerramentoSalasCursoORM extends Ead1_ORM{
	
	public $_name = 'vw_encerramentosalascurso';
	public $_primary = array('id_saladeaula','id_disciplina', 'id_usuarioprofessor');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_categoriaendereco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CategoriaEnderecoORM extends Ead1_ORM{
	
	public $_name = 'tb_categoriaendereco';
	public $_primary = array('id_categoriaendereco');
	public $_dependentTables = array('TipoEnderecoORM');
	
}
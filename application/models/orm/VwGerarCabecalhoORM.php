<?php
/**
 * Classe que faz o mapeamento da view vw_gerarcabecalho
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwGerarCabecalhoORM extends Ead1_ORM{
	
	public $_name = 'vw_gerarcabecalho';
	public $_primary = array('id_entidade');
	
}
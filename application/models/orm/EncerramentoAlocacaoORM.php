<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_acesso
 * @author elcioguimaraes
 *
 * @package models
 * @subpackage orm
 */
class EncerramentoAlocacaoORM extends Ead1_ORM{
	
	public $_name = 'tb_encerramentoalocacao';
	public $_primary = array('id_encerramentosala' , 'id_alocacao');
	
}
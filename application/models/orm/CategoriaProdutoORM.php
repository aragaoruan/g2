<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_categoriaproduto
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class CategoriaProdutoORM extends Ead1_ORM {

	public $_name = 'tb_categoriaproduto';
	public $_primary = array('id_categoria', 'id_produto');
	
}

?>
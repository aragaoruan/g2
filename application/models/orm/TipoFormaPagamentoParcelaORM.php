<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoformapagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoFormaPagamentoParcelaORM extends Ead1_ORM {

	public $_name = 'tb_tipoformapagamentoparcela';
	public $_primary = array('id_tipoformapagamentoparcela');
	public $_dependentTables = array('FormaPagamentoORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_afiliadoproduto
 * @author Denise Xavier denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class AfiliadoProdutoORM extends Ead1_ORM {
	public $_name = 'tb_afiliadoproduto';
	public $_primary = array('id_afiliadoproduto');
}

?>
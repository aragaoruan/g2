<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaprojetosala
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaProjetoSalaIntegracaoORM extends Ead1_ORM {
	
	public $_name = 'tb_areaprojetosalaintegracao';
	public $_primary = array('id_areaprojetosalaintegracao');
}


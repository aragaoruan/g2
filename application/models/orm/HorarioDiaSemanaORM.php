<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_horariodiasemana
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class HorarioDiaSemanaORM extends Ead1_ORM {

	public $_name = 'tb_horariodiasemana';
	public $_primary = array('id_horarioaula', 'id_diasemana');
	public $_dependentTables = array('AulaSalaDeAulaORM');
	public $_referenceMap = array(
	'HorarioAulaORM' => array(
	            'columns'           => 'id_horarioaula',
	            'refTableClass'     => 'HorarioAulaORM',
	            'refColumns'        => 'id_horarioaula'
	),
	'DiaSemanaORM' => array(
	            'columns'           => 'id_diasemana',
	            'refTableClass'     => 'DiaSemanaORM',
	            'refColumns'        => 'id_diasemana'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_turnohorarioaula
 * Mapeamento da view que junta a informação do Turno com o Horario da Aula
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwTurnoHorarioAulaORM extends Ead1_ORM{
	
	public $_name = 'vw_turnohorarioaula';
	public $_primary = array('id_horarioaula', 'id_turno', 'id_entidade','id_diasemana');
	
	
}
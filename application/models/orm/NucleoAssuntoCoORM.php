<?php

 /**
* Classe de NucleoAssuntoCoORM

* @package models
* @subpackage orm
*/
class NucleoAssuntoCoORM extends Ead1_ORM {

public $_name = 'tb_nucleoassuntoco';
public $_primary = array('id_nucleoassuntoco');

}

<?php
/**
 * Classe de mapeamento objeto relacional da view vw_materialprojetopedagogico
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwMaterialProjetoPedagogicoORM extends Ead1_ORM{
	public $_name = 'vw_materialprojetopedagogico';
	public $_primary = array('id_materialprojeto','id_projetopedagogico','id_itemdematerial');
}
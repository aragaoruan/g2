<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoprovafinal
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoProvaFinalORM extends Ead1_ORM {

	public $_name = 'tb_tipoprovafinal';
	public $_primary = array('id_tipoprovafinal');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
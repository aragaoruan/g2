<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipotrilha
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoTrilhaORM extends Ead1_ORM {

	public $_name = 'tb_tipotrilha';
	public $_primary = array('id_tipotrilha');
	public $_dependentTables = array('TrilhaORM', 'TipoTrilhaFixaORM');
	
}

?>
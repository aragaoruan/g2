<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_diasemana
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DiaSemanaORM extends Ead1_ORM {

	public $_name = 'tb_diasemana';
	public $_primary = array('id_diasemana');
	public $_dependentTables = array('HorarioDiaSemanaORM');
}
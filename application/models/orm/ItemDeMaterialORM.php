<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_itemmaterial
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ItemDeMaterialORM extends Ead1_ORM {

	public $_name = 'tb_itemdematerial';
	public $_primary = array('id_itemdematerial');
	public $_dependentTables = array('ItemDeMaterialDisciplinaORM', 'ItemDeMaterialAreaORM');
	public $_referenceMap = array(
	'TipoDeMaterialORM' => array(
	            'columns'           => 'id_tipodematerial',
	            'refTableClass'     => 'TipoDeMaterialORM',
	            'refColumns'        => 'id_tipodematerial'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
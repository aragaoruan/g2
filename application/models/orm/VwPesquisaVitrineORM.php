<?php
/**
 * Classe de mapeamento objeto relacional da view vw_vitrine
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
class VwPesquisaVitrineORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisavitrine';
	public $_primary = array('id_contratoafiliado');
}
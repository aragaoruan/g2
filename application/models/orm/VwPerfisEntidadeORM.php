<?php
/**
 * Classe de mapeamento objeto relacional da view vw_perfisentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwPerfisEntidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_perfisentidade';
	public $_primary = array('id_entidade', 'id_perfil', 'id_sistema', 'id_entidadeclasse');
}
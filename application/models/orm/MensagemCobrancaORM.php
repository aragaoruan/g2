<?php

/**
 * Classe de MensagemCobrancaORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class MensagemCobrancaORM extends Ead1_ORM {

	public $_name = 'tb_mensagemcobranca';
	public $_primary = array('id_mensagemcobranca');

}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_formameiopagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FormaMeioPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_formameiopagamento';
	public $_primary = array('id_formapagamento','id_meiopagamento','id_tipodivisaofinanceira');
	public $_referenceMap = array(
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	),
	'MeioPagamentoORM' => array(
	            'columns'           => 'id_meiopagamento',
	            'refTableClass'     => 'MeioPagamentoORM',
	            'refColumns'        => 'id_meiopagamento'
	),
	'MeioPagamentoORM' => array(
	            'columns'           => 'id_tipodivisaofinanceira',
	            'refTableClass'     => 'TipoDivisaoFinanceiraORM',
	            'refColumns'        => 'id_tipodivisaofinanceira'
	));
}

?>
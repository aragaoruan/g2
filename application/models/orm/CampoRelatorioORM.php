<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_camporelatorio
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class CampoRelatorioORM extends Ead1_ORM{
	
	public $_name = 'tb_camporelatorio';
	public $_primary = 'id_camporelatorio';
}
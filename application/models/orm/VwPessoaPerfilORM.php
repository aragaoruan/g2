<?php
/**
 * Mapeamento da view que faz a pesquisa do perfil
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaPerfilORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoaperfil';
	public $_primary = array('id_entidade', 'id_usuario', 'id_perfil');
}
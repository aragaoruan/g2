<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_funcionario
 * @author Rafael Rocha
 * @package models
 * @subpackage orm
 */
class VwFuncionarioORM extends Ead1_ORM{
	public $_name = 'vw_funcionario';
	public $_primary = array('id_usuario','id_entidade');
}
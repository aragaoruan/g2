<?php

/**
 * Classe de VwLogMatricula
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwLogMatriculaORM extends Ead1_ORM {

	public $_name = 'vw_logmatricula';
	public $_primary = array('id_matricula');

}
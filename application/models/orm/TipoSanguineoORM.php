<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tiposanguineo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoSanguineoORM extends Ead1_ORM{
	
	public $_name = 'tb_tiposanguineo';
	public $_primary = array('id_tiposanguineo');
	public $_dependentTables = array('DadosBiomedicosORM');
	
}
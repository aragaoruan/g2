<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadeclasse
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeClasseORM extends Ead1_ORM {

	public $_name = 'tb_entidadeclasse';
	public $_primary = 'id_entidadeclasse';
	public $_dependentTables = array('EntidadeRelacaoORM', 'PerfilORM');
}

?>
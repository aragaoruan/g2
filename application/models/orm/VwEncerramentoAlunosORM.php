<?php
/**
 * Classe de mapeamento objeto relacional da vw_encerramentoalunos
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwEncerramentoAlunosORM extends Ead1_ORM {

	public $_name = 'vw_encerramentoalunos';
	public $_primary = array('id_alocacao', 'id_matricula', 'id_disciplina');
}

<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_saladeaulaintegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SalaDeAulaIntegracaoORM extends Ead1_ORM{
	
	public $_name = 'tb_saladeaulaintegracao';
	public $_primary = array('id_saladeaulaintegracao');
}
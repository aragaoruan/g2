<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areadisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaDisciplinaORM extends Ead1_ORM {

	public $_name = 'tb_areadisciplina';
	public $_primary = array('id_areaconhecimento', 'id_disciplina');
	public $_referenceMap = array(
	'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaORM',
	            'refColumns'        => 'id_disciplina'
	),
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	));
}

?>
<?php
/**
 * Mapeamento da view que mostra as informações de conta bancária de uma pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaContaORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoaconta';
	public $_primary = 'id_usuario';
}
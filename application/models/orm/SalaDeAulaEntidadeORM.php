<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class SalaDeAulaEntidadeORM extends Ead1_ORM{
	public $_name = 'tb_saladeaulaentidade';
	public $_primary = array('id_saladeaula', 'id_entidade');
}
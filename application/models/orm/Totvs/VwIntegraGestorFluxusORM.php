<?php
/**
 * Classe de mapeamento objeto relacional da view de integração do Gestor2 com o Fluxus
 * @author Eder Lamar
 * @package models
 * @subpackage orm
 */
class Totvs_VwIntegraGestorFluxusORM extends Ead1_ORM{
	public $_name = 'vw_Integra_GestorFluxus';
	public $_schema = 'totvs';
	public $_primary = 'IDLAN';
}
<?php
/**
 * @author ederlamar
 *
 */
class Totvs_VwLancamentosORM extends Ead1_ORM {

	public $_name = 'vw_lancamentos';
	public $_primary = 'id_lancamento';
	public $_schema = 'totvs';
}

?>
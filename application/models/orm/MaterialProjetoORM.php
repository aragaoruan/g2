<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_materialprojeto
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class MaterialProjetoORM extends Ead1_ORM {
	
	public $_name = 'tb_materialprojeto';
	public $_primary = array('id_materialprojeto');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'ItemDeMaterialORM' => array(
	            'columns'           => 'id_itemdematerial',
	            'refTableClass'     => 'ItemDeMaterialORM',
	            'refColumns'        => 'id_itemdematerial'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
	
	
}
<?php
/**
 * Classe que faz o mapeamento da view que retorna os endereços de uma entidade
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeEnderecoORM extends Ead1_ORM{
	public $_name = 'vw_entidadeendereco';
	public $_primary = array('id_entidadeendereco');
}
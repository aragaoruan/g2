<?php
/**
 * Mapeamento da view que mostra a informação acadêmica de uma pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaInformacaoAcademicaORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoainformacaoacademica';
	public $_primary = array('id_nivelensino', 'id_entidade', 'id_usuario');
}
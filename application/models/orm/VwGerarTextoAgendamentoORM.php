<?php

/**
 * Classe de VwAlunosSalaORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwGerarTextoAgendamentoORM extends Ead1_ORM {

	public $_name = 'vw_gerartextoagendamento';
        public $_primary = array('id_avaliacaoagendamento');

}
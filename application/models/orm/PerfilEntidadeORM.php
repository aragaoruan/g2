<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_perfilentidade';
	public $_primary = array('id_perfil', 'id_entidade');
	public $_dependentTables = array('PessoaEnderecoORM','EnderecoORM');
	public $_referenceMap = array(
	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        =>'id_perfil'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        =>'id_entidade'
			)
	);
}

?>
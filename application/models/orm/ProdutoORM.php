<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_produto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProdutoORM extends Ead1_ORM {

	public $_name = 'tb_produto';
	public $_primary = array('id_produto');
	public $_dependentTables = array('VendaProdutoORM', 'ProdutoProjetoPedagogicoORM', 'ProdutoValorORM','CampanhaProdutoORM');
	public $_referenceMap = array(
	'TipoProdutoORM' => array(
	            'columns'           => 'id_tipoproduto',
	            'refTableClass'     => 'TipoProdutoORM',
	            'refColumns'        => 'id_tipoproduto'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	));
}

?>
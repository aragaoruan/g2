<?php
/**
 * tb_categoriasala
 * @package models
 * @subpackage orm
 */
class CategoriaSalaORM extends Ead1_ORM {

    public $_name = 'tb_categoriasala';
    public $_primary = array('id_categoriasala');

}
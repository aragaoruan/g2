<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_livro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class LivroORM extends Ead1_ORM {

	public $_name = 'tb_livro';
	public $_primary = 'id_livro';

}
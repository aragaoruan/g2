<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tagproduto
 * @author João Gabriel - gabriel.vasconcelos@ead1.com
 *
 * @package models
 * @subpackage orm
 */
class TagProdutoORM extends Ead1_ORM {
	public $_name = 'tb_tagproduto';
	public $_primary = array('id_tag', 'id_produto');
}
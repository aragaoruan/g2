<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_acesso
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AcessoORM extends Ead1_ORM{
	
	public $_name = 'tb_acesso';
	public $_primary = array('id_perfil','id_entidade','id_funcionalidade');
	public $_referenceMap = array(
	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PefilORM',
	            'refColumns'        =>'id_perfil'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'PefilORM',
	            'refColumns'        =>'id_entidade'
			),
	'FunctionalidadeORM' => array(
	            'columns'           => 'id_funcionalidade',
	            'refTableClass'     => 'FuncionalidadeORM',
	            'refColumns'        =>'id_funcionalidade'
			)
	);
	
}
<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_itemmaterialdisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ItemDeMaterialDisciplinaORM extends Ead1_ORM
{

    public $_name = 'tb_itemdematerialdisciplina';
    public $_primary = array('id_disciplina', 'id_tipodematerial', 'id_serie', 'id_nivelensino');
    public $_referenceMap = array(
        'DisciplinaSerieNivelEnsinoORM' => array(
            'columns' => 'id_disciplina',
            'refTableClass' => 'DisciplinaSerieNivelEnsinoORM',
            'refColumns' => 'id_disciplina'
        ),
        'DisciplinaSerieNivelEnsinoORM1' => array(
            'columns' => 'id_serie',
            'refTableClass' => 'DisciplinaSerieNivelEnsinoORM',
            'refColumns' => 'id_serie'
        ),
        'DisciplinaSerieNivelEnsinoORM2' => array(
            'columns' => 'id_nivelensino',
            'refTableClass' => 'DisciplinaSerieNivelEnsinoORM',
            'refColumns' => 'id_ensino'
        ),
        'ItemDeMaterialORM' => array(
            'columns' => 'id_itemdematerial',
            'refTableClass' => 'ItemDeMaterialORM',
            'refColumns' => 'id_itemdematerial'
    ));

}

?>
<?php

/**
 * Classe de VwPacoteMatriculaORM
 
 * @package models
 * @subpackage orm
 */
 class VwPacoteMatriculaORM extends Ead1_ORM {

	public $_name = 'vw_pacotematricula';
	public $_primary = array('id_projetopedagogico');

}
?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avaliacaoconjunto
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class AvaliacaoConjuntoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoconjunto';
	public $_primary 			= array( 'id_avaliacaoconjunto' );
	public $_dependentTables	= array( 'AvaliacaoConjuntoRelacaoORM');
	public $_referenceMap 		= array( 'TipoProvaORM'	=> array(
														            'columns'           => 'id_tipoprova',
														            'refTableClass'     => 'TipoProvaORM',
														            'refColumns'        => 'id_tipoprova'
														),
										'SituacaoORM' 			=> array(
																	'columns'           => 'id_situacao',
										            				'refTableClass'     => 'SituacaoORM',
										            				'refColumns'        => 'id_situacao'
										),
										'UsuarioORM' 			=> array(
																	'columns'			=> 'id_usuariocadastro',
																	'refTableClass'		=> 'UsuarioORM',
																	'refColumns'		=> 'id_usuario'
										),
										'EntidadeORM' 			=> array(
																	'columns'			=> 'id_entidade',
																	'refTableClass'		=> 'EntidadeORM',
																	'refColumns'		=> 'id_entidade'
										),
										'TipoCalculoAvaliacaoORM' 			=> array(
																	'columns'			=> 'id_tipocalculoavaliacao',
																	'refTableClass'		=> 'TipoCalculoAvaliacaoORM',
																	'refColumns'		=> 'id_tipocalculoavaliacao'
																	)
										);
}
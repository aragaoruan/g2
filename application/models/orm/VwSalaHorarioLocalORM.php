<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @package models
 * @subpackage orm
 */
 
class VwSalaHorarioLocalORM extends Ead1_ORM{
	public $_name = 'vw_salahorariolocal';
	public $_primary = array('id_saladeaula');
}
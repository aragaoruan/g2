<?php

/**
 * Classe de VwCategoriaOcorrenciaORM
 * @package models
 * @subpackage orm
 *
 */
 class VwCategoriaOcorrenciaORM extends Ead1_ORM {

 	public $_name = 'vw_categoriaocorrencia';
 	public $_primary = array('id_categoriaocorrencia');
}

?>
<?php
/**
 * Mapeamento da view que junta a informação do Turno com o Horario da Aula
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwProjetoAreaORM extends Ead1_ORM{
	
	public $_name = 'vw_projetoarea';
	public $_primary = array('id_areaconhecimento', 'id_projetopedagogico');
	
	
}
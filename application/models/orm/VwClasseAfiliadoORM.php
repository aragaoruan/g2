<?php

/**
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwClasseAfiliadoORM extends Ead1_ORM {

	public $_name = 'vw_classeafiliado';
	public $_primary = array('id_classeafiliado');
	
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentossubstituicao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosSubstituicaoORM extends Ead1_ORM {

	public $_name = 'tb_documentossubstituicao';
	public $_primary = array('id_documentossubstituto', 'id_documentos');
	public $_referenceMap = array(
	'DocumentosORM' => array(
	            'columns'           => 'id_documentos',
	            'refTableClass'     => 'DocumentosORM',
	            'refColumns'        => 'id_documentos'
	),
	'Documentos2ORM' => array(
		'columns'			=> 'id_documentossubstituto',
		'refTableClass'		=> 'DocumentosORM',
		'refColumns'		=> 'id_documentos'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_lancamentointegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class LancamentoIntegracaoORM extends Ead1_ORM{
	public $_name = 'tb_lancamentointegracao';
	public $_primary = 'id_lancamentointegracao';
}
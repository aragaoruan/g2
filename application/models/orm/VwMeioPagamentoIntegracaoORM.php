<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_meiopagamentointegracao
 *
 * @package models
 * @subpackage orm
 */
class VwMeioPagamentoIntegracaoORM extends Ead1_ORM {

	public $_name = 'vw_meiopagamentointegracao';
	public $_primary = array('id_meiopagamentointegracao', 'id_meiopagamento');
}

?>
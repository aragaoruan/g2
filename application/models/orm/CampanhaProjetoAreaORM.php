<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhaprojetoarea
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaProjetoAreaORM extends Ead1_ORM {

	public $_name = 'tb_campanhaprojetoarea';
	public $_primary = array('id_campanhacomercial','id_projetopedagogico','id_areaconhecimento');
	public $_referenceMap = array(
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentosutilizacaorelacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosUtilizacaoRelacaoORM extends Ead1_ORM {

	public $_name = 'tb_documentosutilizacaorelacao';
	public $_primary = array('id_documentosutilizacao', 'id_documentos');
	public $_referenceMap = array(
	'DocumentosUtilizacaoORM' => array(
	            'columns'           => 'id_documentosutilizacao',
	            'refTableClass'     => 'DocumentosUtilizacaoORM',
	            'refColumns'        => 'id_documentosutilizacao'
	),
	'DocumentosORM' => array(
	            'columns'           => 'id_documentos',
	            'refTableClass'     => 'DocumentosORM',
	            'refColumns'        => 'id_documentos'
	));
}

?>
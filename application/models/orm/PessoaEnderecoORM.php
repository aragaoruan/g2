<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_pessoaendereco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PessoaEnderecoORM extends Ead1_ORM{
	
	public $_name = 'tb_pessoaendereco';
	public $_primary = array('id_usuario','id_entidade','id_endereco');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        =>'id_entidade'
			),
	'EnderecoORM' => array(
	            'columns'           => 'id_endereco',
	            'refTableClass'     => 'EnderecoORM',
	            'refColumns'        => 'id_endereco'
			),
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
			)		
	);	
}
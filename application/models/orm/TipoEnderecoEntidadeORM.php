<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoenderecoentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoEnderecoEntidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_tipoenderecoentidade';
	public $_primary = array('id_entidade','id_tipoendereco');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
		),
	'TipoEnderecoORM' => array(
	            'columns'           => 'id_tipoendereco',
	            'refTableClass'     => 'TipoEnderecoORM',
	            'refColumns'        => 'id_tipoendereco'
		)
	);
	
}
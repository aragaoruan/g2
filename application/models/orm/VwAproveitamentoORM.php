<?php
/**
 * Mapeamento da view vw_aproveitamento
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Eder Lamar Mariano edermariano@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAproveitamentoORM extends Ead1_ORM {

	public $_name = 'vw_aproveitamento';
	public $_primary = array('id_matricula', 'id_aproveitamentodisciplina', 'id_usuario');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_salahorariolocal
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 *
 * @package models
 * @subpackage orm
 */
class SalaHorarioLocalORM extends Ead1_ORM {

	public $_name = 'tb_salahorariolocal';
	public $_primary = array('id_saladeaula','id_horarioaula', 'id_localaula');
	public $_referenceMap = array(
	'SalaDeAulaORM' => array(
		'columns'			=> 'id_saladeaula',
		'refTableClass'		=> 'SalaDeAulaORM',
		'refColumns'		=> 'id_saladeaula'
	),
	'HorarioAulaORM' => array(
		'columns'			=> 'id_horarioaula',
		'refTableClass'		=> 'HorarioAulaORM',
		'refColumns'		=> 'id_horarioaula'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwUsuarioPerfilPedagogicoORM extends Ead1_ORM{
	public $_name = 'vw_usuarioperfilpedagogico';
	public $_primary = array('id_perfilpedagogico');
}
<?php
/**
 * Classe de mapeamento objeto-relacional da view vw_dadospessoais
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br > 
 *
 */
class VwDadosPessoaisORM extends Ead1_ORM {
	
	/**
	 * Nome da view
	 * @var string
	 */
	protected $_name	= 'vw_dadospessoais';
	
	/**
	 * Lista de nomes de chaves primárias
	 * @var array
	 */
	protected $_primary	= array( 'id_usuario', 'id_entidade' );
}
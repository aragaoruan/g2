<?php
/**
 * Classe de mapeamento objeto relacional da view vw_avaliacaodisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwAproveitarAvaliacaoORM extends Ead1_ORM{
	public $_name = 'vw_aproveitaravaliacao';
	public $_primary = array('id_disciplina', 'id_matricula', 'id_avaliacao');
}
<?php
/**
 * Classe de mapeamento objeto-relacional com a tabela tb_historicosituacao
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class HistoricoSituacaoImportacaoORM extends Ead1_ORM {
	
	protected $_name	= 'tb_historicosituacaoimportacao';
	protected $_primary	= 'id_historicosituacaoimportacao';
	
}
<?php

/**
 * Classe de PacoteORM
 
 * @package models
 * @subpackage orm
 */
 class PacoteORM extends Ead1_ORM {

	public $_name = 'tb_pacote';
	public $_primary = array('id_pacote');

}

?>
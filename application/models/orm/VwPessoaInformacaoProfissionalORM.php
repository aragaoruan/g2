<?php
/**
 * Mapeamento da view que mostra a informação profissional de uma pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaInformacaoProfissionalORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoainformacaoprofissional';
	public $_primary = array('id_areaatuacao', 'id_entidade', 'id_usuario');
}
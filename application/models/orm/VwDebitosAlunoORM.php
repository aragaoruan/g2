<?php
/**
 * Classe de VwDebitosAlunoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwDebitosAlunoORM extends Ead1_ORM {

	public $_name = 'vw_debitosaluno';
	public $_primary = array('id_venda');

}

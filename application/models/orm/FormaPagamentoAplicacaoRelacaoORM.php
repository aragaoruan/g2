<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class FormaPagamentoAplicacaoRelacaoORM extends Ead1_ORM{
	public $_name = 'tb_formapagamentoaplicacaorelacao';
	public $_primary = array('id_formapagamento', 'id_formapagamentoaplicacao');
}
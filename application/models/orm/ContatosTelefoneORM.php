<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatostelefone
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosTelefoneORM extends Ead1_ORM{
	
	public $_name = 'tb_contatostelefone';
	public $_primary = 'id_telefone';
	public $_dependentTables = array('ContatosTelefonePessoaORM','ContatosTelefoneEntidadeORM');
	public $_referenceMap = array(
	'TipoTelefoneORM' => array(
	            'columns'           => 'id_tipotelefone',
	            'refTableClass'     => 'TipoTelefoneORM',
	            'refColumns'        =>'id_tipotelefone'
			),
	'DddORM' => array(
	            'columns'           => 'nu_ddd',
	            'refTableClass'     => 'DddORM',
	            'refColumns'        =>'nu_ddd'
			),
	'DdiORM' => array(
	            'columns'           => 'nu_ddi',
	            'refTableClass'     => 'DdiORM',
	            'refColumns'        =>'nu_ddi'
			)
	);
}
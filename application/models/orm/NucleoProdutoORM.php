<?php

class NucleoProdutoORM extends Ead1_ORM {

	public $_name = 'tb_nucleoproduto';
	public $_primary = array('id_nucleoproduto');
	public $_referenceMap = array(
	'NucleotmORM' => array(
				'columns'			=> 'id_nucleotm',
				'refTableClass'		=> 'NucleotmORM',
				'refColumns'		=> 'id_nucleotm'
			),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	)
	);
}

?>
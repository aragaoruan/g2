<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_ocorrenciaaguardando
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwOcorrenciaAguardandoORM extends Ead1_ORM {
	
	public $_name = 'vw_ocorrenciaaguardando';
	public $_primary = array('id_ocorrencia');
}

?>
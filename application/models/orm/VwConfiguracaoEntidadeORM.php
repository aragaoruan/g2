<?php
/**
 * Classe de mapeamento objeto relacional da view vw_configuracaoentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwConfiguracaoEntidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_configuracaoentidade';
	public $_primary = array('id_entidade', 'id_tipoconfiguracao');
}
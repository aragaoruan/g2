<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetocontratoducacaotipo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoContratoDuracaoTipoORM extends Ead1_ORM {

	public $_name 				= 'tb_projetocontratoduracaotipo';
	public $_primary 			= array('id_projetocontratoduracaotipo');
	public $_dependentTables 	= array('ProjetoPedagogicoORM');
}

?>
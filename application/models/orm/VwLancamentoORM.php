<?php
/**
 * Mapeamento da view que encapsula dados de lançamento
 * 
 * [Lista de Quem utiliza]
 * 	@see FinanceiroDAO::retornarLancamento();
 * 	@see ProdutoDAO::retornarVwLancamento();
 * 
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwLancamentoORM extends Ead1_ORM{
	
	public $_name = 'vw_lancamento';
	public $_primary = array('id_lancamento', 'id_venda', 'id_entidade','id_meiopagamento','id_tipolancamento');
}
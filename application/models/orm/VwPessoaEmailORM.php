<?php
/**
 * Mapeamento da view que mostra o(s) contatos(s) email
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaEmailORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoaemail';
	public $_primary = array('id_email', 'id_entidade', 'id_usuario', 'id_perfil');
}
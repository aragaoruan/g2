<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_contratomatricula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContratoMatriculaORM extends Ead1_ORM
{

    public $_name = 'tb_contratomatricula';
    public $_primary = array('id_contrato', 'id_matricula');
    public $_referenceMap = array(
        'ContratoORM' => array(
            'columns' => 'id_contrato',
            'refTableClass' => 'ContratoORM',
            'refColumns' => 'id_contrato'
        ),
        'MatriculaORM' => array(
            'columns' => 'id_matricula',
            'refTableClass' => 'MatriculaORM',
            'refColumns' => 'id_matricula'
        ));
}
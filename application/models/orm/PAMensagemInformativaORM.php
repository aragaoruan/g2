<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_pa_mensageminformativa
 * @package models
 * @subpackage orm
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PAMensagemInformativaORM extends Ead1_ORM {

    public $_name = 'tb_pa_mensageminformativa';
    public $_primary = 'id_pa_mensageminformativa';

}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contapessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContaPessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_contapessoa';
	public $_primary = array('id_contapessoa');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'BancoORM' => array(
	            'columns'           => 'id_banco',
	            'refTableClass'     => 'BancoORM',
	            'refColumns'        => 'id_banco'
	),
	'TipoContaORM' => array(
	            'columns'           => 'id_tipodeconta',
	            'refTableClass'     => 'TipoContaORM',
	            'refColumns'        => 'id_tipodeconta'
	));	
	
}
<?php
/**
 * Classe de VwProdutoTaxaORM
 
 * @package models
 * @subpackage orm
 */
 class VwProdutoTaxaORM extends Ead1_ORM {

	public $_name = 'vw_produtotaxa';
	public $_primary = array('id_produto');

}

?>
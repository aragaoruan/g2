<?php
/**
 * Classe de mapeamento objeto relacional da vw vw_produtoprr
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */

class VwProdutoPrrORM extends  Ead1_ORM{

    public $_name = 'vw_produtoprr';
    public $_primary = array('id_produto');
}
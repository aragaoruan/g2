<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaconhecimentoserienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaConhecimentoSerieNivelEnsinoORM extends Ead1_ORM {

	public $_name = 'tb_areaconhecimentoserienivelensino';
	public $_primary = array('id_areaconhecimento', 'id_serie', 'id_nivelensino');
	public $_referenceMap = array(
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
			),
	'SerieNivelEnsinoORM' => array(
	            'columns'           => 'id_serie',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_serie'
			),
	'SerieNivelEnsino2ORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
			)
	);
}

?>
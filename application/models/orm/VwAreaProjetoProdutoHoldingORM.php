<?php

/**
 * Classe de VwAreaProjetoProdutoHoldingORM
 * @package models
 * @subpackage orm
 */
 class VwAreaProjetoProdutoHoldingORM extends Ead1_ORM {

	public $_name = 'vw_areaprojetoprodutoholding';
	public $_primary = array('id_entidade', 'id_produto');

}

?>
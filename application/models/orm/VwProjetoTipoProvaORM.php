<?php
/**
 * Classe que mapeia a view que retorna os professores de uma sala de aula
 * @autor Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwProjetoTipoProvaORM extends Ead1_ORM{
	public $_name = 'vw_projetotipoprova';
	public $_primary = array('id_tipoprovaresult', 'id_tipoprova', 'id_projetopedagogico');
}
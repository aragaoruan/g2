<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaconhecimento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaConhecimentoORM extends Ead1_ORM {

	public $_name = 'tb_areaconhecimento';
	public $_primary = array('id_areaconhecimento');
	public $_dependentTables = array('AreaConhecimentoSerieNivelEnsinoORM', 'AreaProjetoPedagogicoORM', 'AreaDisciplinaORM', 'ItemDeMaterialAreaORM','UsuarioPerfilEntidadeReferenciaORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
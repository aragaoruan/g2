<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhaproduto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaProdutoORM extends Ead1_ORM {

	public $_name = 'tb_campanhaproduto';
    public $_primary = array('id_campanhacomercial');
	public $_referenceMap = array(
	'ProdutoORM' => array(
	            'columns'           => 'id_produto',
	            'refTableClass'     => 'ProdutoORM',
	            'refColumns'        => 'id_produto'
	),
	'CampanhaComercialORM' => array(
		'columns'			=> 'id_campanhacomercial',
		'refTableClass'		=> 'CampanhaComercialORM',
		'refColumns'		=> 'id_campanhacomercial'
	));
}

?>

<?php

/**
 * Classe de TransacaoFinanceira
 
 * @package models
 * @subpackage orm
 */
 class TransacaoFinanceiraORM extends Ead1_ORM {

	public $_name = 'tb_transacaofinanceira';
	public $_primary = array('id_transacaofinanceira');

}

?>
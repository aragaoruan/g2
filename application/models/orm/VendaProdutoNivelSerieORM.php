<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_acesso
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VendaProdutoNivelSerieORM extends Ead1_ORM{
	public $_name = "tb_vendaprodutonivelserie";
	public $_primary = "id_vendaproduto";
}
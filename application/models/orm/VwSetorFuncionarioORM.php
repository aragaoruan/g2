<?php
/**
 * Classe de mapeamento objeto relacional da view vw_setorfuncionario
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwSetorFuncionarioORM extends Ead1_ORM {

	public $_name = 'vw_setorfuncionario';
	public $_primary = array('id_setorfuncionario');

}
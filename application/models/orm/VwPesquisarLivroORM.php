<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_pesquisarlivro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarLivroORM extends Ead1_ORM {

	public $_name = 'vw_pesquisarlivro';
	public $_primary = 'id_livro';

}
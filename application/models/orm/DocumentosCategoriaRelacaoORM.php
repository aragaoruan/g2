<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentoscategoriarelacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentosCategoriaRelacaoORM extends Ead1_ORM {

	public $_name = 'tb_documentoscategoriarelacao';
	public $_primary = array('id_documentoscategoria', 'id_documentos');
	public $_referenceMap = array(
	'DocumentosCategoriaORM' => array(
	            'columns'           => 'id_documentoscategoria',
	            'refTableClass'     => 'DocumentosCategoriaORM',
	            'refColumns'        => 'id_documentoscategoria'
	),
	'DocumentosORM' => array(
		'columns'			=> 'id_documentos',
		'refTableClass'		=> 'DocumentosORM',
		'refColumns'		=> 'id_documentos'
	));
}

?>
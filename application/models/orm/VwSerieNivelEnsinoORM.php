<?php
/**
 * Classe de mapeamento objeto relacional da view vw_serienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwSerieNivelEnsinoORM extends Ead1_ORM {

	public $_name = 'vw_serienivelensino';
	public $_primary = array('id_serie', 'id_nivelensino');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodocumento
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 * @package models
 * @subpackage orm
 */
class TipoDocumentoORM extends Ead1_ORM{
	
	protected $_name			= 'tb_tipodocumento';
	protected $_primary			= 'id_tipodocumento';
	protected $_dependentTables	= array('DocumentosORM');
	
	
}
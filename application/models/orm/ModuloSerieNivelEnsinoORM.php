<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_moduloserienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ModuloSerieNivelEnsinoORM extends Ead1_ORM {

	public $_name = 'tb_moduloserienivelensino';
	public $_primary = array('id_modulo', 'id_serie', 'id_nivelensino');
	public $_referenceMap = array(
	'SerieNivelEnsinoORM' => array(
	            'columns'           => 'id_serie',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_serie'
	),
	'SerieNivelEnsinoORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
	),
	'ModuloORM' => array(
	            'columns'           => 'id_modulo',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_modulo'
	));
}

?>
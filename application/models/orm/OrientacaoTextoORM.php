<?php
/**
 * Classe de mapeamento objeto-relacional da tabela tb_orientacaotexto
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class OrientacaoTextoORM extends Ead1_ORM {
	
	/**
	 * Nome da tabela
	 * @var string
	 */
	protected $_name	= 'tb_orientacaotexto';
	
	/**
	 * Nome da chave primária
	 * @var string
	 */
	protected $_primary	= 'id_orientacaotexto';
	
	/**
	 * Lista de tabelas de relacionamento
	 * @var array
	 */
	protected $_referenceMap	= array( 'TextoSistemaORM' );
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class ProdutoTextoSistemaORM extends Ead1_ORM{
	public $_name = 'tb_produtotextosistema';
	public $_primary = array('id_textosistema', 'id_entidade');
}
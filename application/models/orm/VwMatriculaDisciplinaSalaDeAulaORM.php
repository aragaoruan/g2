<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_matriculadisciplinasaladeaula
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class VwMatriculaDisciplinaSalaDeAulaORM extends Ead1_ORM {
	public $_name = 'vw_matriculadisciplinasaladeaula';
	public $_primary = 'id_matriculadisciplina';
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class CompartilhadadosORM extends Ead1_ORM{
	public $_name = 'tb_compartilhadados';
	public $_primary = array('id_entidadeorigem');
}
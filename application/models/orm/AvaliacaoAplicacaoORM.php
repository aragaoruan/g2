<?php
class AvaliacaoAplicacaoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoaplicacao';
	public $_primary 			= array( 'id_avaliacaoaplicacao' );
	
	public $_referenceMap 		= array( 'AvaliacaoORM'	=> array(
														            'columns'           => 'id_avaliacao',
														            'refTableClass'     => 'AvaliacaoORM',
														            'refColumns'        => 'id_avaliacao'
														),
										'HorarioAulaORM' 			=> array(
																	'columns'           => 'id_horarioaula',
										            				'refTableClass'     => 'HorarioAulaORM',
										            				'refColumns'        => 'id_horarioaula'
														),
										'EntidadeORM' 			=> array(
																	'columns'			=> 'id_entidade',
																	'refTableClass'		=> 'EntidadeORM',
																	'refColumns'		=> 'id_entidade'
														),
										'EntidadeEnderecoORM' 			=> array(
																	'columns'			=> 'id_entidadeendereco',
																	'refTableClass'		=> 'EntidadeEnderecoORM',
																	'refColumns'		=> 'id_entidadeendereco'
														)
										);
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_entidadecontatotelefone
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeContatoTelefoneORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadecontatotelefone';
	public $_primary = array('id_entidade', 'id_telefone', 'id_tipotelefone', 'id_contatoentidade');
}
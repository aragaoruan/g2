<?php
/**
 * Classe que mapeia a view que retorna as disciplinas do modulo
 * @autor Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwModuloDisciplinaProjetoTrilhaORM extends Ead1_ORM{
	public $_name = 'vw_modulodisciplinaprojetotrilha';
	public $_primary = array('id_modulo', 'id_disciplina', 'id_entidade', 'id_trilha');
}
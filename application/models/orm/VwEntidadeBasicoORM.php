<?php
/**
 * Classe de mapeamento objeto relacional da view vw_entidadebasico
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeBasicoORM extends Ead1_ORM{
	
	/**
	 * Atributo que contém o nome da View no banco
	 * @var String
	 */
	public $_name = 'vw_entidadebasico';
	
	/**
	 * Atributo que contém um campo representativo da chave primária
	 * @var String
	 */
	public $_primary = 'id_entidade';
}
<?php
/**
 * Mapeamento da view que encapsula dados da Disciplina pela area de conhecimento e nivel de ensino
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarDisciplinaNivelAreaORM extends Ead1_ORM{
	
	public $_name = "vw_pesquisardisciplinanivelarea";
	public $_primary = array("id_disciplina","id_tipodisciplina","id_entidade","id_nivelensino","id_areaconhecimento","id_situacao");
}
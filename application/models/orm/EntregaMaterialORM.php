<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entregamaterial
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class EntregaMaterialORM extends Ead1_ORM {
	
	public $_name = 'tb_entregamaterial';
	public $_primary = array('id_entregamaterial');
	public $_referenceMap 		= array('SituacaoORM' 			=> array(
																	'columns'           => 'id_situacao',
										            				'refTableClass'     => 'SituacaoORM',
										            				'refColumns'        => 'id_situacao'
										),
										'UsuarioORM' 			=> array(
																	'columns'			=> 'id_usuariocadastro',
																	'refTableClass'		=> 'UsuarioORM',
																	'refColumns'		=> 'id_usuario'
																	),
										'MatriculaORM' 			=> array(
																	'columns'			=> 'id_matricula',
																	'refTableClass'		=> 'MatriculaORM',
																	'refColumns'		=> 'id_matricula'
																	)
										);
	
}
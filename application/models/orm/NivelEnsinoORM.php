<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_nivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class NivelEnsinoORM extends Ead1_ORM{
	
	public $_name = 'tb_nivelensino';
	public $_primary = array('id_nivelensino');
	public $_dependentTables = array('InformacaoAcademicaPessoaORM', 'SerieNivelEnsinoORM');
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadeendereco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeEnderecoORM extends Ead1_ORM{
	
	public $_name = 'tb_entidadeendereco';
	public $_primary = array('id_entidadeendereco');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        =>'id_entidade'
			),
	'EnderecoORM' => array(
	            'columns'           => 'id_endereco',
	            'refTableClass'     => 'EnderecoORM',
	            'refColumns'        =>'id_endereco'
			)		
	);	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_produtovalor
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProdutoValorORM extends Ead1_ORM {

	public $_name = 'tb_produtovalor';
	public $_primary = array('id_produtovalor');
	public $_referenceMap = array(
	'ProdutoORM' => array(
	            'columns'           => 'id_produto',
	            'refTableClass'     => 'ProdutoORM',
	            'refColumns'        => 'id_produto'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}


?>
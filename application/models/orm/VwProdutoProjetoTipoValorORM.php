<?php
/**
 * Classe de mapeamento objeto relacional da view vw_produtoprojetotipovalor
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwProdutoProjetoTipoValorORM extends Ead1_ORM{
	public $_name = 'vw_produtoprojetotipovalor';
	public $_primary = array('id_produto');
}
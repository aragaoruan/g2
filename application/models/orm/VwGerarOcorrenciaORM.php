<?php
/**
 * Classe de mapeamento objeto relacional da view vw_gerarocorrencia
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwGerarOcorrenciaORM extends Ead1_ORM {
	
	public $_name = 'vw_gerarocorrencia';
	public $_primary = array('id_ocorrencia');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatosentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatoEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_contatoentidade';
	public $_primary = array('id_contatoentidade');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	));
}

?>
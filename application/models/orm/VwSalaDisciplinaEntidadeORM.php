<?php


/**
 * Class VwSalaDisciplinaEntidadeORM
 */
class VwSalaDisciplinaEntidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_saladisciplinaentidade';
	public $_primary = array('id_entidade','id_projetopedagogico','id_disciplina');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class ProdutoTaxaORM extends Ead1_ORM{
	public $_name = 'tb_produtotaxa';
	public $_primary = array('id_taxa', 'id_entidade');
}
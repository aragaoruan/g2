<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class EnvioDestinatarioORM extends Ead1_ORM{
	public $_name = 'tb_enviodestinatario';
	public $_primary = array('id_enviodestinatario');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eder Lamar
 * @package models
 * @subpackage orm
 */
class ProdutoTaxaProjetoORM extends Ead1_ORM {

	public $_name 		= 'tb_produtotaxaprojeto';
	public $_primary 	= array('id_produto', 'id_taxa', 'id_projeto');
}

?>
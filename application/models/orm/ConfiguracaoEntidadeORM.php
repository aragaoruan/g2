<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class ConfiguracaoEntidadeORM extends Ead1_ORM{
	public $_name = 'tb_configuracaoentidade';
	public $_primary = array('id_configuracaoentidade', 'id_entidade');
}
<?php
/**
 * Mapeamento da view que mostra o(s) endereço(s) do cadastro de pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaEnderecoORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoaendereco';
	public $_primary = array('id_usuario', 'id_entidade', 'id_endereco', 'id_municipio', 'id_categoriaendereco', 'id_pais');
}
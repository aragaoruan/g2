<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidaderelacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeRelacaoORM extends Ead1_ORM {

	public $_name = 'tb_entidaderelacao';
	public $_primary = array('id_entidade', 'id_entidadepai');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'EntidadePaiORM' => array(
	            'columns'           => 'id_entidadepai',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'EntidadeClasseORM' => array(
				'columns'			=> 'id_entidadeclasse',
				'refTableClass'		=> 'EntidadeClasseORM',
				'refColumns'		=> 'id_entidadeclasse'
	));
	
}

?>
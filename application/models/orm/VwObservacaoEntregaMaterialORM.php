<?php
/**
 * Mapeamento da view vw_observacaoentregamaterial
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwObservacaoEntregaMaterialORM extends Ead1_ORM{
	public $_name = 'vw_observacaoentregamaterial';
	public $_primary = array('id_usuario','id_observacao','id_itemdematerial','id_matricula','id_entregamaterial');
}
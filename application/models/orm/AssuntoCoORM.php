<?php
/**
 * tb_assuntoco
 * 
 * @package models
 * @subpackage orm
 */
class AssuntoCoORM extends Ead1_ORM {

	public $_name = 'tb_assuntoco';
	public $_primary = 'id_assuntoco';
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_entidadetelefone
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeTelefoneORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadetelefone';
	public $_primary = array('id_entidade', 'id_telefone', 'id_tipotelefone');
}
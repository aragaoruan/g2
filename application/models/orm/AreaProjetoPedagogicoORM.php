<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaprojetopedagogico
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaProjetoPedagogicoORM extends Ead1_ORM {

	public $_name = 'tb_areaprojetopedagogico';
	public $_primary = array('id_areaconhecimento', 'id_projetopedagogico');
	public $_referenceMap = array(
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	));
}

?>
<?php

class EnvolvidoEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_envolvidoentidade';
	public $_primary = array('id_envolvidoentidade');
	public $_referenceMap = array(
	'TipoEnvolvidoORM' => array(
	            'columns'           => 'id_tipoenvolvido',
	            'refTableClass'     => 'TipoEnvolvidoORM',
	            'refColumns'        => 'id_tipoenvolvido'
			),
	'EntidadeORM' => array(
				'columns'			=> 'id_entidade',
				'refTableClass'		=> 'EntidadeORM',
				'refColumns'		=> 'id_entidade'
			),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	)
	);	
}

?>
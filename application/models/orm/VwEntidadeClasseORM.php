<?php
/**
 * Mapeamento da view que faz a o retorno da classe de uma entidade
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeClasseORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadeclasse';
	public $_primary = array('id_entidade', 'id_entidadepai', 'id_entidadeclasse');
}
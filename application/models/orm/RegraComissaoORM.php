<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_regracomissao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class RegraComissaoORM extends Ead1_ORM {

	public $_name = 'tb_regracomissao';
	public $_primary = array('id_regracomissao');
	public $_referenceMap = array(
	'BaseCalculoComORM' => array(
	            'columns'           => 'id_basecalculocom',
	            'refTableClass'     => 'BaseCalculoComORM',
	            'refColumns'        => 'id_basecalculocom'
	),
	'ComissaoEntidadeORM' => array(
	            'columns'           => 'id_comissaoentidade',
	            'refTableClass'     => 'ComissaoEntidadeORM',
	            'refColumns'        => 'id_comissaoentidade'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'TipoComissaoORM' => array(
	            'columns'           => 'id_tipocomissao',
	            'refTableClass'     => 'TipoComissaoORM',
	            'refColumns'        => 'id_tipocomissao'
	),
	'TipoMetaORM' => array(
	            'columns'           => 'id_tipometa',
	            'refTableClass'     => 'TipoMetaORM',
	            'refColumns'        => 'id_tipometa'
	),
	'TipoValorComissaoORM' => array(
	            'columns'           => 'id_tipovalorcomissao',
	            'refTableClass'     => 'TipoValorComissaoORM',
	            'refColumns'        => 'id_tipovalorcomissao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
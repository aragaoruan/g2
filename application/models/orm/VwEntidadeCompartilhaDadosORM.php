<?php
/**
 * Vw que lista as entidades com vinculos
 * Classe de VwEntidadeCompartilhaDadosORM
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwEntidadeCompartilhaDadosORM extends Ead1_ORM{
    public $_name = 'vw_entidadecompartilhadados';
    public $_primary = array('id_entidadedestino', 'id_entidadeorigem');
}
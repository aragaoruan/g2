<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contratoresponsavel
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContratoResponsavelORM extends Ead1_ORM {

	public $_name = 'tb_contratoresponsavel';
	public $_primary = array('id_contratoresponsavel');
	public $_referenceMap = array(
	'ContratoORM' => array(
	            'columns'           => 'id_tipodisciplina',
	            'refTableClass'     => 'TipoDisciplinaORM',
	            'refColumns'        => 'id_tipodisciplina'
	),
	'TipoContratoResponsavelORM' => array(
	            'columns'           => 'id_tipocontratoresponsavel',
	            'refTableClass'     => 'TipoContratoResponsavelORM',
	            'refColumns'        => 'id_tipocontratoresponsavel'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
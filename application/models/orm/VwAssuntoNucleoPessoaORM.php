<?php
/**
 * Vw que lista as os vinculos de uma pesoa com o nucleo
 * Classe de VwAssuntoNucleoPessoaORM
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwAssuntoNucleoPessoaORM extends Ead1_ORM {
	
	public $_name = 'vw_assuntonucleupessoa';
	public $_primary = array('id_nucleoco', 'id_entidade');
	
}

?>
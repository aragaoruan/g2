<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avalagendaintegracao
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class AvalAgendaIntegracaoORM extends Ead1_ORM{
	public $_name = 'tb_avalagendaintegracao';
	public $_primary = 'id_avalagendaintegracao';
}
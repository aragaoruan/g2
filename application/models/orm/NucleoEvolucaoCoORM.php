<?php
/**
 * Classe de NucleoEvolucaoCoORM
 * @package models
 * @subpackage orm
 *
 */
class NucleoEvolucaoCoORM extends Ead1_ORM {
	public $_name = 'tb_nucleoevolucaoco';
	public $_primary = array('id_funcao', 'id_evolucao', 'id_nucleoco');
}

?>
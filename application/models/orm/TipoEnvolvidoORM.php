<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoenvolvido
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoEnvolvidoORM extends Ead1_ORM {

	public $_name = 'tb_tipoenvolvido';
	public $_primary = array('id_tipoenvolvido');
	public $_dependentTables = array('ContratoEnvolvidoORM');
}

?>
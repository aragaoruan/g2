<?php
/**
 * Mapeamento da view que mostra pela área do projeto pedagógico os módulos e as disciplinas que fazem parte
 * Classe de mapeamento objeto relacional da view vw_areaprojetopedagogico
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwAreaProjetoPedagogicoORM extends Ead1_ORM{
	
	public $_name = 'vw_areaprojetopedagogico';
	public $_primary = array('id_areaconhecimento', 'id_projetopedagogico', 'id_modulo', 'id_nivelensino', 'id_serie', 'id_disciplina');
	
	
}
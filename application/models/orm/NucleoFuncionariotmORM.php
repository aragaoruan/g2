<?php

class NucleoFuncionariotmORM extends Ead1_ORM {

	public $_name = 'tb_nucleofuncionariotm';
	public $_primary = array('id_nucleofuncionariotm');
	public $_referenceMap = array(
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'UsuarioCadastroORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	)
	);
}

?>
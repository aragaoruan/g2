<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadesistema
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeResponsavelLegalORM extends Ead1_ORM {

	public $_name = 'tb_entidaderesponsavellegal';
	public $_primary = array('id_entidaderesponsavellegal');
}

?>
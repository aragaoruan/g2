<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_afiliadoprodutocomissionamento
 * @author Denise Xavier denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class AfiliadoProdutoComissionamentoORM extends Ead1_ORM {
	public $_name = 'tb_afiliadoprodutocomissionamento';
	public $_primary = array('id_afiliadoproduto', 'id_comissionamento');
}

?>
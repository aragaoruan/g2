<?php
/**
 * Classe de mapeamento objeto-relacional da tabela tb_cartaobandeira
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 *
 */
class CartaoBandeiraORM extends Ead1_ORM {
	
	/**
	 * Nome da tabela
	 * @var string
	 */
	protected $_name	= 'tb_cartaobandeira';
	
	/**
	 * Nome de chave primária
	 * @var string
	 */
	protected $_primary	= 'id_cartaobandeira';
	
	
	/**
	 * Relação de tabelas em que a tabela exporta uma chave estrangeira
	 * @var array
	 */
	protected $_dependentTables	= array( 'CartaoConfigORM', 'CartaoOperadoraBandeiraORM' );
}
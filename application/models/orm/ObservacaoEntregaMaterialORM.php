<?php
class ObservacaoEntregaMaterialORM extends Ead1_ORM{
	
	public $_name 				= 'tb_observacaoentregamaterial';
	public $_primary 			= array( 'id_observacao' );
	
	public $_referenceMap 		= array( 'ObservacaoORM'	=> array(
														            'columns'           => 'id_observacao',
														            'refTableClass'     => 'ObservacaoORM',
														            'refColumns'        => 'id_observacao'
														),
										'EntregaMaterialORM' 			=> array(
																	'columns'			=> 'id_entregamaterial',
																	'refTableClass'		=> 'EntregaMaterialORM',
																	'refColumns'		=> 'id_entregamaterial'
																	)
										);
}
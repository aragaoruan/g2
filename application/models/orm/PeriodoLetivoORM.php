<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_periodoletivo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PeriodoLetivoORM extends Ead1_ORM{
	
	public $_name = 'tb_periodoletivo';
	public $_primary = array('id_periodoletivo');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'UsuarioORM' => array(
	            'columns'           => 'id_usuariocadastro',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
	));	
}
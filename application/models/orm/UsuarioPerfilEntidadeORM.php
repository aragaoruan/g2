<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_usuarioperfilentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class UsuarioPerfilEntidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_usuarioperfilentidade';
	public $_primary = array('id_usuario', 'id_perfil', 'id_entidade');
	public $_dependentTables = array('UsuarioPerfilEntidadeReferenciaORM');
	public $_referenceMap = array(
	      	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        =>'id_usuario'
			),
	      	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        =>'id_perfil'
			),
	      	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        =>'id_entidade'
			)
	);
	
}
<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_pa_dadoscadastrais
 * @package models
 * @subpackage orm
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PADadosCadastraisORM extends Ead1_ORM {

    public $_name = 'tb_pa_dadoscadastrais';
    public $_primary = 'id_pa_dadoscadastrais';

}

?>
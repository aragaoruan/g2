<?php
/**
 * Classe que encapsula os dados da view vw_disciplinarequisito
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwDisciplinaRequisitoORM extends Ead1_ORM{
	
public $_name = 'vw_disciplinarequisito';	
public $_primary = array('id_trilha','id_disciplina','id_disciplinaprerequisito');
	
	
}
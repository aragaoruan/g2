<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_aulasaladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AulaSalaDeAulaORM extends Ead1_ORM {

	
	public $_name = 'tb_aulasaladeaula';
	public $_primary = array('id_aulasaladeaula');
	public $_referenceMap = array(
	'HorarioDiaSemanaORM' => array(
	            'columns'           => 'id_diasemana',
	            'refTableClass'     => 'HorarioDiaSemanaORM',
	            'refColumns'        => 'id_diasemana'
	),
	'HorarioDiaSemanaORM2' => array(
	            'columns'           => 'id_horarioaula',
	            'refTableClass'     => 'HorarioDiaSemanaORM',
	            'refColumns'        => 'id_horarioaula'
	),
	'TipoAulaORM' => array(
		'columns'			=> 'id_tipoaula',
		'refTableClass'		=> 'TipoAulaORM',
		'refColumns'		=> 'id_tipoaula'
	));
}

?>
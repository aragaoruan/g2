<?php

/**
 * Classe de TransacaoFinanceira
 
 * @package models
 * @subpackage orm
 */
 class TransacaoLancamentoORM extends Ead1_ORM {

	public $_name = 'tb_transacaolancamento';
	public $_primary = array('id_lancamento');

}

?>
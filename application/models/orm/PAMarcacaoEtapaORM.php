<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_pa_marcacaoetapa
 * @package models
 * @subpackage orm
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PAMarcacaoEtapaORM extends Ead1_ORM {

    public $_name = 'tb_pa_marcacaoetapa';
    public $_primary = 'id_pa_marcacaoetapa';

}

?>
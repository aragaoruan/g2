<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_itemmaterialarea
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ItemDeMaterialAreaORM extends Ead1_ORM {

	public $_name = 'tb_itemdematerialarea';
	public $_primary = array('id_areaconhecimento', 'id_itemdematerial');
	public $_referenceMap = array(
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	),
	'ItemDeMaterialORM' => array(
		'columns'			=> 'id_itemdematerial',
		'refTableClass'		=> 'ItemDeMaterialORM',
		'refColumns'		=> 'id_itemdematerial'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_trilha
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TrilhaORM extends Ead1_ORM {

	public $_name = 'tb_trilha';
	public $_primary = array('id_trilha');
	public $_dependentTables = array('ProjetoPedagogicoORM', 'TrilhaDisciplinaORM');
	public $_referenceMap = array(
	'TipoTrilhaORM' => array(
	            'columns'           => 'id_tipotrilha',
	            'refTableClass'     => 'TipoTrilhaORM',
	            'refColumns'        => 'id_tipotrilha'
	));
}

?>
<?php
/**
 * Mapeamento da view que mostra as funcionalidades do usuário do sistema
 * @author DIMAS SULZ
 *
 * @package models
 * @subpackage orm
 */
class VwPerfilEntidadeFuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_perfilentidadefuncionalidade';
	public $_primary = array('id_entidade', 'id_perfil', 'id_funcionalidade', 'id_entidadeclasse');
}
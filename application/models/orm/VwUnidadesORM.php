<?php

/**
 * Classe de VwUnidades
 
 * @package models
 * @subpackage orm
 */
 class VwUnidadesORM extends Ead1_ORM {

	public $_name = 'vw_unidades';
	public $_primary = array('id_entidade');

}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_vendasrecebimento
 * @author Gabriel Vasconcelos <gabriel.vasconcelos@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwVendasRecebimentoORM extends Ead1_ORM {
	
	public $_name = 'vw_vendasrecebimento';
	public $_primary = array('id_usuario', 'id_venda');
}

?>
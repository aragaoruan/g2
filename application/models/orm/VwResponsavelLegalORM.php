<?php
/**
 * Classe de mapeamento objeto relacional da view vw_responsavellegal
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwResponsavelLegalORM extends Ead1_ORM {

	public $_name = 'vw_responsavellegal';
	public $_primary = array('id_entidaderesponsavellegal');
}

?>
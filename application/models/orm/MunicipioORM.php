<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_municipio
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class MunicipioORM extends Ead1_ORM{
	
	public $_name = 'tb_municipio';
	public $_primary = array('id_municipio');
	public $_dependentTables = array('PessoaORM','EnderecoORM','DocumentoReservistaORM');
	public $_referenceMap = array(
	'UfORM' => array(
	            'columns'           => 'sg_uf',
	            'refTableClass'     => 'UfORM',
	            'refColumns'        => 'sg_uf'
	));
	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_recorrentevencendo
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 *
 * @package models
 * @subpackage orm
 */
class VwRecorrenteVencendoORM extends Ead1_ORM {
	public $_name = 'vw_recorrentevencendo';
	public $_primary = 'id_pedidointegracao';
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodeconta
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDeContaORM extends Ead1_ORM{
	public $_name = 'tb_tipodeconta';
	public $_primary = array('id_tipodeconta');
}
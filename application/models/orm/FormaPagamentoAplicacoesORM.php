<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_formapagamentoaplicacoes
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FormaPagamentoAplicacoesORM extends Ead1_ORM {

	public $_name = 'tb_formapagamentoaplicacoes';
	public $_primary = array('id_formapagamento','id_aplicacoespagamento');
	public $_referenceMap = array(
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	),
	'AplicacoesPagamentoORM' => array(
	            'columns'           => 'id_aplicacoespagamento',
	            'refTableClass'     => 'AplicacoesPagamentoORM',
	            'refColumns'        => 'id_aplicacoespagamento'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_ddi
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DdiORM extends Ead1_ORM{
	
	public $_name = 'tb_ddi';
	public $_primary = 'nu_ddi';
	public $_dependentTables = array('ContatosTelefoneORM');
	public $_referenceMap = array(
	'PaisORM' => array(
	            'columns'           => 'id_pais',
	            'refTableClass'     => 'PaisORM',
	            'refColumns'        => 'id_pais'
	));
	
}
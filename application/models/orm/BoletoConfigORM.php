<?php

/**
 * Classe de mapeamentro entre a tabela tb_boletoconfig
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 *
 */
class BoletoConfigORM extends Ead1_ORM {
	
	
	protected $_name	= 'tb_boletoconfig';
	protected $_primary	= 'id_boletoconfig';
	
	
	protected $_dependentTables	= array( 'EntidadeBoletoConfigORM' );
	
	protected $_referenceMap	= array('ContaEntidadeORM'	=>	array(
																		'columns'		=>	'id_contaentidade'
																	   ,'refTableClass'	=>	'ContaEntidadeORM'
																	   ,'refColumns'	=>	'id_contaentidade'
																		)
										);
}
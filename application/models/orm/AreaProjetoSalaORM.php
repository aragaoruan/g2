<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_areaprojetosala
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class AreaProjetoSalaORM extends Ead1_ORM {
	
	public $_name = 'tb_areaprojetosala';
	public $_primary = array('id_areaprojetosala');
	public $_referenceMap = array(
	'AreaConhecimentoORM' => array(
	            'columns'           => 'id_areaconhecimento',
	            'refTableClass'     => 'AreaConhecimentoORM',
	            'refColumns'        => 'id_areaconhecimento'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'SalaDeAulaORM' => array(
		'columns'			=> 'id_saladeaula',
		'refTableClass'		=> 'SalaDeAulaORM',
		'refColumns'		=> 'id_saladeaula'
	)
	,
	'NivelEnsinoORM' => array(
		'columns'			=> 'id_nivelensino',
		'refTableClass'		=> 'NivelEnsinoORM',
		'refColumns'		=> 'id_nivelensino'
	));
}

?>
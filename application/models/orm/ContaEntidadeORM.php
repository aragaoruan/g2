<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_contaentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContaEntidadeORM extends Ead1_ORM
{

    public $_name = 'tb_contaentidade';
    public $_primary = array('id_contaentidade', 'id_banco', 'id_entidade', 'id_tipodeconta');
    public $_referenceMap = array(
        'EntidadeORM' => array(
            'columns' => 'id_entidade',
            'refTableClass' => 'EntidadeORM',
            'refColumns' => 'id_entidade'
        ),
        'BancoORM' => array(
            'columns' => 'id_banco',
            'refTableClass' => 'BancoORM',
            'refColumns' => 'id_banco'
        ),
        'TipoDeContaORM' => array(
            'columns' => 'id_tipodeconta',
            'refTableClass' => 'TipoDeContaORM',
            'refColumns' => 'id_tipodeconta'
        )
    );
}


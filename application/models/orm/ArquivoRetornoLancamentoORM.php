<?php

/**
 * Salva os dados de cada boleto contido no arquivo retorno
 * Classe de ArquivoRetornoLancamentoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class ArquivoRetornoLancamentoORM extends Ead1_ORM {

	public $_name = 'tb_arquivoretornolancamento';
	public $_primary = array('id_arquivoretornolancamento');

}
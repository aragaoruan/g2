<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_pa_dadoscontato
 * @package models
 * @subpackage orm
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PADadosContatoORM extends Ead1_ORM {

    public $_name = 'tb_pa_dadoscontato';
    public $_primary = 'id_pa_dadoscontato';

}

?>
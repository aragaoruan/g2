<?php

/**
 * Classe de NucleoCoEmailORM
 * @package models
 * @subpackage orm
 */
 class NucleoCoEmailORM extends Ead1_ORM {

	public $_name = 'tb_nucleocoemail';
	public $_primary = array('id_nucleoco', 'id_emailconfig');
}

?>
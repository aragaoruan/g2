<?php
class EntidadeBoletoConfigORM	extends Ead1_ORM {
	
	
	protected $_name	= 'tb_entidadeboletoconfig';
	protected $_primary	= array( 'id_boletoconfig', 'id_entidadefinanceiro' );
	
	protected $_referenceMap	= array(
										'BoletoConfigORM'		=>	array(
																	'columns'		=>	'id_boletoconfig'
																   ,'refTableClass'	=>	'BoletoConfigORM'
																   ,'refColumns'	=>	'id_boletoconfig'
																	 )
																	 
									   ,'EntidadeFinanceiroORM'	=>	array(
																	'columns'		=>	'id_entidadefinanceiro'
																   ,'refTableClass'	=>	'EntidadeFinanceiroORM'
																   ,'refColumns'	=>	'id_entidadefinanceiro'
																	 )
										);
}
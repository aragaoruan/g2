<?php
/**
 * Classe de mapeamento objeto relacional da view vw_clientevenda
 * @author Dimas Sulz
 * 
 */
class VwProdutoContratoORM extends Ead1_ORM
{
	public $_name = 'vw_produtocontrato';
	public $_primary = array('id_contrato');
}
?>
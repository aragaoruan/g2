<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class AproveitamentoMatriculaDisciplinaORM extends Ead1_ORM{
	public $_name = 'tb_aproveitamentomatriculadisciplina';
	public $_primary = array('id_apmatriculadisciplina');
}
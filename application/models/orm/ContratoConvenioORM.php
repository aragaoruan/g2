<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_contratoconvenio
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContratoConvenioORM extends Ead1_ORM
{

    public $_name = 'tb_contratoconvenio';
    public $_primary = array('id_contrato', 'id_convenio');
    public $_referenceMap = array(
        'ContratoORM' => array(
            'columns' => 'id_contrato',
            'refTableClass' => 'ContratoORM',
            'refColumns' => 'id_contrato'
        ),
        'ConvenioORM' => array(
            'columns' => 'id_convenio',
            'refTableClass' => 'ConvenioORM',
            'refColumns' => 'id_convenio'
        ));
}

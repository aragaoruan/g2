<?php
/**
 * Classe de mapeamento objeto relacional da view vw_resumofinanceiro
 * @author Dimas Sulz
 * 
 */
class VwResumoFinanceiroORM extends Ead1_ORM
{
	public $_name = 'vw_resumofinanceiro';
	public $_primary = array('id_lancamento','id_venda','id_meiopagamento','id_usuario','id_entidade');
}
?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_fundamentolegal
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FundamentoLegalORM extends Ead1_ORM {

	public $_name = 'tb_fundamentolegal';
	public $_primary = array('id_fundamentolegal');
	public $_referenceMap = array(
	'TipoFundamentoLegalORM' => array(
	            'columns'           => 'id_tipofundamentolegal',
	            'refTableClass'     => 'TipoFundamentoLegalORM',
	            'refColumns'        => 'id_tipofundamentolegal'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
	));
}

?>
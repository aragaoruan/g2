<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contratoenvolvido
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContratoEnvolvidoORM extends Ead1_ORM {

	public $_name = 'tb_contratoenvolvido';
	public $_primary = array('id_tipoenvolvido', 'id_contrato');
	public $_referenceMap = array(
	'ContratoRM' => array(
	            'columns'           => 'id_contrato',
	            'refTableClass'     => 'ContratoORM',
	            'refColumns'        => 'id_contrato'
	),
	'TipoEnvolvidoORM' => array(
	            'columns'           => 'id_tipoenvolvido',
	            'refTableClass'     => 'TipoEnvolvidoORM',
	            'refColumns'        => 'id_tipoenvolvido'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
	));
}

?>
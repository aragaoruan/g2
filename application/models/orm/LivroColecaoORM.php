<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_livrocolecao
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
class LivroColecaoORM extends Ead1_ORM{
	
	public $_name = 'tb_livrocolecao';
	public $_primary = array('id_livrocolecao');
	
}
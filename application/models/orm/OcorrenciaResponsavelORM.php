<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_ocorrenciaresponsavel
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class OcorrenciaResponsavelORM extends Ead1_ORM {
	public $_name = 'tb_ocorrenciaresponsavel';
	public $_primary = array('id_ocorrenciaresponsavel');
}

?>
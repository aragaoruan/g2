<?php
/**
 * Mapeamento da view vw_avaliacaoaluno
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Eder Lamar Mariano edermariano@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoAlunoORM extends Ead1_ORM{
	
	public $_name = 'vw_avaliacaoaluno';
	public $_primary = array('id_matricula', 'id_avaliacao');
}
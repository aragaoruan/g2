<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_moduloarea
 * @author Arthur Cláudio de Almeida Pereira
 *
 * @package models
 * @subpackage orm
 */
class ModuloAreaORM extends Ead1_ORM {
	
	protected $_name	= 'tb_moduloarea';
	protected $_primary	= 'id_moduloarea';

}
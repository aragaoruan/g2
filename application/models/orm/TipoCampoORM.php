<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipocampo
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class TipoCampoORM extends Ead1_ORM{
	
	public $_name = 'tb_tipocampo';
	public $_primary = 'id_tipocampo';
}
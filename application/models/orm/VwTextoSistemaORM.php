<?php
/**
 * Classe de mapeamento objeto relacional da view vw_textosistema
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwTextoSistemaORM extends Ead1_ORM{
	public $_name = 'vw_textosistema';
	public $_primary = array('id_textosistema', 'id_usuario');
}
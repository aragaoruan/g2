<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilentidadefuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilEntidadeFuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_perfilentidadefuncionalidade';
	public $_primary = array('id_perfil','id_entidade','id_funcionalidade');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        =>'id_entidade'
			),
	'PerfilORM' => array(
	            'columns'           => 'id_perfi',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        =>'id_perfi'
			),
	'FuncionalidadeORM' => array(
	            'columns'           => 'id_funcionalidade',
	            'refTableClass'     => 'FuncionalidadeORM',
	            'refColumns'        =>'id_funcionalidade'
			)
	);
	
}
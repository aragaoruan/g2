<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_comissionamento
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class ClasseAfiliadoComissionamentoORM extends Ead1_ORM {

	public $_name = 'tb_classeafiliadocomissionamento';
	public $_primary = 'id_classeafiliado';
	
}

?>
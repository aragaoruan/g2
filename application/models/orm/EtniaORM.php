<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_etnia
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EtniaORM extends Ead1_ORM{
	
	public $_name = 'tb_etnia';
	public $_primary = array('id_etnia');
	public $_dependentTables = array('DadosBiomedicosORM');
	
}
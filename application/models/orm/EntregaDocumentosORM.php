<?php
/**
 * Classe que mapeia a tabela de entrega de documentos
 * @author edermariano
 * 
 * @package models
 * @subpackage orm
 */
class EntregaDocumentosORM extends Ead1_ORM{
	public $_name = 'tb_entregadocumentos';
	public $_primary = 'id_entregadocumentos';
}
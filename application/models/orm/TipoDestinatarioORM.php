<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class TipoDestinatarioORM extends Ead1_ORM{
	public $_name = 'tb_tipodestinatario';
	public $_primary = array('id_tipodestinatario');
}
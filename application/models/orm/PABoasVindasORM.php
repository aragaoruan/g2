<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_pa_boasvindas
 * @package models
 * @subpackage orm
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 */
class PABoasVindasORM extends Ead1_ORM {

    public $_name = 'tb_pa_boasvindas';
    public $_primary = 'id_pa_boasvindas';

}

?>
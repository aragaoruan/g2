<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class TurmaORM extends Ead1_ORM{
	public $_name = 'tb_turma';
	public $_primary = array('id_turma');
}
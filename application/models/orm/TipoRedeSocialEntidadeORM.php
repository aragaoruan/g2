<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tiporedesocialentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoRedeSocialEntidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_tiporedesocialentidade';
	public $_primary = array('id_usuario','id_entidade','id_redesocial');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'TipoRedeSocialORM' => array(
	            'columns'           => 'id_tiporedesocial',
	            'refTableClass'     => 'TipoRedeSocialORM',
	            'refColumns'        => 'id_tiporedesocial'
			)		
	);	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
 
class VwUsuarioPerfilEntidadeReferenciaORM extends Ead1_ORM{
	public $_name = 'vw_usuarioperfilentidadereferencia';
	public $_primary = array('id_perfilreferencia');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_matricula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class MatriculaORM extends Ead1_ORM {

	public $_name = 'tb_matricula';
	public $_primary = array('id_matricula');
	public $_dependentTables = array('ContratoMatriculaORM');
	public $_referenceMap = array(
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	),
	'VendaProdutoORM' => array(
	            'columns'           => 'id_vendaproduto',
	            'refTableClass'     => 'VendaProdutoORM',
	            'refColumns'        => 'id_vendaproduto'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidadematricula',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'EntidadeORM2' => array(
	            'columns'           => 'id_entidadeatendimento',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'EntidadeORM3' => array(
	            'columns'           => 'id_entidadematriz',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'PeriodoLetivoORM' => array(
	            'columns'           => 'id_periodoletivo',
	            'refTableClass'     => 'PeriodoLetivoORM',
	            'refColumns'        => 'id_periodoletivo'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoRM',
	            'refColumns'        => 'id_situacao'
	),
	'EvolucaoORM' => array(
	            'columns'           => 'id_evolucao',
	            'refTableClass'     => 'EvolucaoORM',
	            'refColumns'        => 'id_evolucao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'UsuarioORM2' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
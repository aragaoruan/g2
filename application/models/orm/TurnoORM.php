<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_turno
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TurnoORM extends Ead1_ORM {

	public $_name = 'tb_turno';
	public $_primary = array('id_turno');
	public $_dependentTables = array('HorarioAulaORM');
}

?>
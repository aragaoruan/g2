<?php
/**
 * Mapeamento da view que mostra os fundamentos legais
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwFundamentoLegalORM extends Ead1_ORM{
	
	public $_name = 'vw_fundamentolegal';
	public $_primary = array('id_fundamentolegal', 'id_tipofundamentolegal', 'id_usuario', 'id_entidade');
}
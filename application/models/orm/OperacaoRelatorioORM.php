<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_operacaorelatorio
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class OperacaoRelatorioORM extends Ead1_ORM{
	
	public $_name = 'tb_operacaorelatorio';
	public $_primary = 'id_operacaorelatorio';
}
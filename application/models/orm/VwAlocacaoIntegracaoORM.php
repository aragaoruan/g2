<?php
/**
 * Classe de mapeamento objeto relacional da view vw_alocacaointegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwAlocacaoIntegracaoORM extends Ead1_ORM{
	public $_name = 'vw_alocacaointegracao';
	public $_primary = 'id_alocacao';	
}

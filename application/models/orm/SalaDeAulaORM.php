<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_saladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SalaDeAulaORM extends Ead1_ORM {

	public $_name = 'tb_saladeaula';
	public $_primary = array('id_saladeaula');
	public $_dependentTables = array('DisciplinaSalaDeAulaORM', 'SalaDeAulaProfessorORM', 'AreaProjetoSalaORM','UsuarioPerfilEntidadeReferenciaORM');
	public $_referenceMap = array(
	'PeriodoLetivoORM' => array(
	            'columns'           => 'id_periodoletivo',
	            'refTableClass'     => 'PeriodoLetivoORM',
	            'refColumns'        => 'id_periodoletivo'
	),
	'ModalidadeSalaDeAulaORM' => array(
	            'columns'           => 'id_modalidadesaladeaula',
	            'refTableClass'     => 'ModalidadeSalaDeAulaORM',
	            'refColumns'        => 'id_modalidadesaladeaula'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	));
}

?>
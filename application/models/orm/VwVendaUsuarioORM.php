<?php
/**
 * Classe de mapeamento objeto relacional da view vw_vendausuario
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwVendaUsuarioORM extends Ead1_ORM{
	
	public $_name = 'vw_vendausuario';
	public $_primary = array('id_venda');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_entidadefuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeFuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_entidadefuncionalidade';
	public $_primary = array('id_entidade', 'id_funcionalidade');
}
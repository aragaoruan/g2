<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_vitrineproduto
 * @author Denise Xavier <denise.xavier07@gmail.com>
 * @package models
 * @subpackage orm
 */
class VitrineProdutoORM extends Ead1_ORM {

	public $_name = 'tb_vitrineproduto';
	public $_primary = array('id_vitrineproduto');
}

?>
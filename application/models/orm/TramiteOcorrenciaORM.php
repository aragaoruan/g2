<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tramiteocorrencia
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class TramiteOcorrenciaORM extends Ead1_ORM {
	public $_name = 'tb_tramiteocorrencia';
	public $_primary = array('id_tramiteocorrencia');
}

?>
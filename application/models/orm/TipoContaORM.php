<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoconta
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoContaORM extends Ead1_ORM{
	
	public $_name = 'tb_tipodeconta';
	public $_primary = array('id_tipodeconta');
	public $_dependentTables = array('ContaPessoaORM');
	
}
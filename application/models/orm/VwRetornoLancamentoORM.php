<?php

/**
 * Classe de VwRetornolancamentoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwRetornolancamentoORM extends Ead1_ORM {

	public $_name = 'vw_retornolancamento';
	public $_primary = array('id_arquivoretorno');

}
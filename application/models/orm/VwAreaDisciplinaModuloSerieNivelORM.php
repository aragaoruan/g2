<?php
/**
 * Classe de mapeamento objeto relacional da view vw_areadisciplinamoduloserienivel
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwAreaDisciplinaModuloSerieNivelORM extends Ead1_ORM {

	public $_name = 'vw_areadisciplinamoduloserienivel';
	public $_primary = array('id_disciplina', 'id_modulo', 'id_areaconhecimento');
}

?>
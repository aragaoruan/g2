<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class MensagemPadraoORM extends Ead1_ORM{
	public $_name = 'tb_mensagempadrao';
	public $_primary = array('id_mensagempadrao');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_mensagemmotivacional.
 * @author Domício de Araújo Medeiros - domicio.medeiros@gmail.com
 * 
 * @package models
 * @subpackage orm
 */
class MensagemMotivacionalORM extends Ead1_ORM {
	
	public $_name = 'tb_mensagemmotivacional';
	public $_primary = array('id_mensagemmotivacional');
}
?>
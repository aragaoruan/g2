<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilpedagogico
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilPedagogicoORM extends Ead1_ORM {

	public $_name 				= 'tb_perfilpedagogico';
	public $_primary 			= 'id_perfilpedagogico';
	public $_dependentTables 	= array( 'PerfilORM' );
}


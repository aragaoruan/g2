<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhaformapagamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaFormaPagamentoORM extends Ead1_ORM {

	public $_name = 'tb_campanhaformapagamento';
	public $_primary = array('id_formapagamento','id_campanhacomercial');
	public $_referenceMap = array(
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	),
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	));
}

?>
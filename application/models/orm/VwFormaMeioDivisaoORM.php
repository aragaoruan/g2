<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_formameiodivisao
 * @author dimassulz
 *
 * @package models
 * @subpackage orm
 */
class VwFormaMeioDivisaoORM extends Ead1_ORM {

	public $_name = 'vw_formameiodivisao';
	public $_primary = array('id_formapagamento', 'id_tipodivisaofinanceira');
}

?>
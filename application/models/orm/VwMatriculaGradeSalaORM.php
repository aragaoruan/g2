<?php

class VwMatriculaGradeSalaORM extends Ead1_ORM {

	public $_name = 'vw_matriculagradesala';
	public $_primary = array('id_matricula', 'id_usuario', 'id_projetopedagogico', 'id_modulo', 'id_disciplina', 'id_saladeaula');
}

?>
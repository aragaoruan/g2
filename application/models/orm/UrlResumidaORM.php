<?php

/**
 * UrlResumidaORM
 *
 * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
 */
class UrlResumidaORM extends Ead1_ORM {
    
    protected $_name = 'tb_urlresumida';
    protected $_primary = array('id_urlresumida');
    
}
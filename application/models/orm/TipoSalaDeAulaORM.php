<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tiposaladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoSalaDeAulaORM extends Ead1_ORM {

	public $_name = 'tb_tiposaladeaula';
	public $_primary = array('id_tiposaladeaula');
	public $_dependentTables = array('SalaDeAulaORM');
}

?>
<?php

/**
 * Tabela para guardar os dados do arquivo retorno 
 * Classe de VwVendaLancamentoQuitadosORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwVendaLancamentoQuitadosORM extends Ead1_ORM {

	public $_name = 'vw_vendalancamentoquitados';
	public $_primary = array('id_venda');

}
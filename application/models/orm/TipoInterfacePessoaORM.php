<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipointerfacepessoa
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class TipoInterfacePessoaORM extends Ead1_ORM{
	public $_name = 'tb_tipointerfacepessoa';
	public $_primary = 'id_tipointerfacepessoa';
}
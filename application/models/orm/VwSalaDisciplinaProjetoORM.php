<?php
/**
 * Classe ORM que define o VIEW vw_saladisciplinaprojeto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwSalaDisciplinaProjetoORM extends Ead1_ORM{
	
	public $_name = 'vw_saladisciplinaprojeto';
	public $_primary = array('id_matricula','id_saladeaula','id_projetopedagogico','id_disciplina','id_areaconhecimento');
}
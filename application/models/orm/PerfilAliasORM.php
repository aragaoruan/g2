<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilalias
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilAliasORM extends Ead1_ORM{
	
	public $_name = 'tb_perfilalias';
	public $_primary = array('id_perfilalias');
	public $_referenceMap = array(
	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        =>'id_perfil'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        =>'id_entidade'
			)
	);
	
}
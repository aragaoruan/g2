<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_medidatempoconclusao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class MedidaTempoConclusaoORM extends Ead1_ORM {

	public $_name = 'tb_medidatempoconclusao';
	public $_primary = array('id_medidatempoconclusao');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
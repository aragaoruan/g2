<?php

/**
 * Mapeamento da tabela tb_produtoimagem
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class ProdutoImagemORM extends Ead1_ORM {
	
	public $_name = 'tb_produtoimagem';
	public $_primary = array('id_produtoimagem');
}

?>
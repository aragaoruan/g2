<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_alocacaointegracao
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class AlocacaoIntegracaoORM extends Ead1_ORM {
	public $_name = 'tb_alocacaointegracao';
	public $_primary = 'id_alocacaointegracao';
}
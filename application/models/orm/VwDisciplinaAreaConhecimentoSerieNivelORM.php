<?php
/**
 * Mapeamento da view que encapsula dados da Disciplina pela area de conhecimento e serie nivel de ensino
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwDisciplinaAreaConhecimentoSerieNivelORM extends Ead1_ORM{
	
	public $_name = 'vw_disciplinaareaconhecimentonivelserie';
	public $_primary = array('id_entidade');
}
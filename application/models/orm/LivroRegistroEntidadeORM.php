<?php

class LivroRegistroEntidadeORM extends Ead1_ORM {

	public $_name = 'tb_livroregistroentidade';
	public $_primary = array('id_livroregistroentidade');
	public $_dependentTables = array('LivroRegistroORM');
	public $_referenceMap = array(
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'UsuarioORM' => array(
	            'columns'           => 'id_usuariocadastro',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
			)
	);
}

?>
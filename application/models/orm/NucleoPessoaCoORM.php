<?php

/**
 * Classe de NucleoPessoaCoORM
 
 * @package models
 * @subpackage orm
 */
 class NucleoPessoaCoORM extends Ead1_ORM {

	public $_name = 'tb_nucleopessoaco';
	public $_primary = array('id_nucleopessoaco');

}

<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_vendaaditivo
 * @author Rafael Leite
 *
 * @package models
 * @subpackage orm
 */
class VendaAditivoORM extends Ead1_ORM
{

    public $_name = 'tb_vendaaditivo';
    public $_primary = array('id_vendaaditivo');
}
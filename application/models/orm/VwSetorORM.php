<?php
/**
 * Classe de mapeamento objeto relacional da view vw_setor
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwSetorORM extends Ead1_ORM{
	
	public $_name = 'vw_setor';
	public $_primary = array('id_setor');
	
}
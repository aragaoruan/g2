<?php
/**
 * Mapeamento da view que mostra as séries e niveis de ensino de uma determina área do conhecimento
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwAreaConhecimentoNivelSerieORM extends Ead1_ORM{
	
	public $_name = 'vw_areaconhecimentonivelserie';
	public $_primary = array('id_areaconhecimento', 'id_nivelensino', 'id_serie');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela td_projetoentidade
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class ProjetoEntidadeORM extends Ead1_ORM{
	
	public $_name 				= 'tb_projetoentidade';
	public $_primary 			= array( 'id_projetoentidade' );
	
	public $_referenceMap 		= array( 'ProjetoPedagogicoORM'	=> array(
														            'columns'           => 'id_projetopedagogico',
														            'refTableClass'     => 'ProjetoPedagogicoORM',
														            'refColumns'        => 'id_projetopedagogico'
														),
										'SituacaoORM' 			=> array(
																	'columns'           => 'id_situacao',
										            				'refTableClass'     => 'SituacaoORM',
										            				'refColumns'        => 'id_situacao'
										),
										'UsuarioORM' 			=> array(
																	'columns'			=> 'id_usuariocadastro',
																	'refTableClass'		=> 'UsuarioORM',
																	'refColumns'		=> 'id_usuario'
																	),
										'EntidadeORM' 			=> array(
																	'columns'			=> 'id_entidade',
																	'refTableClass'		=> 'EntidadeORM',
																	'refColumns'		=> 'id_entidade'
																	)
										);
	
}
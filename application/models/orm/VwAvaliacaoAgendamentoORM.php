<?php
/**
 * Classe responsável pelo mapeamento objeto relacional da view vw_avaliacaoagendamento
 * @author Eder Lamar
 * @package models
 * @subpackage orm
 */
class VwAvaliacaoAgendamentoORM extends Ead1_ORM{
	
	public $_name = 'vw_avaliacaoagendamento';
	public $_primary = array('id_avaliacaoagendamento', 'id_tipoprova','id_usuario');
	
}
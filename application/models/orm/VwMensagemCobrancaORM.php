<?php

/**
 * Classe de VwMensagemCobrancaORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwMensagemCobrancaORM extends Ead1_ORM {

	public $_name = 'vw_mensagemcobranca';
	public $_primary = array('id_mensagemcobranca');

}
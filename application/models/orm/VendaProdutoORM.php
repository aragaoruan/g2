<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_vendaproduto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VendaProdutoORM extends Ead1_ORM {

	public $_name = 'tb_vendaproduto';
	public $_primary = array('id_vendaproduto');
	public $_dependentTables = array('MatriculaORM');
	public $_referenceMap = array(
	'VendaORM' => array(
	            'columns'           => 'id_venda',
	            'refTableClass'     => 'VendaORM',
	            'refColumns'        => 'id_venda'
	),
	'ProdutoORM' => array(
		'columns'			=> 'id_produto',
		'refTableClass'		=> 'ProdutoORM',
		'refColumns'		=> 'id_produto'
	));
}

?>
<?php
/**
 * Classe ORM que define o VIEW vw_modulodisciplinaprerequisito
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwModuloDisciplinaPreRequisitoORM extends Ead1_ORM{
	
	public $_primary = 'id_projetopedagogico';
	
	public $_name = 'vw_modulodisciplinaprerequisito';
}
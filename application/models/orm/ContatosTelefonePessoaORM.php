<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatotelefonepessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosTelefonePessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_contatostelefonepessoa';
	public $_primary = array('id_usuario','id_entidade','id_telefone');
	public $_referenceMap = array(
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'ContatosTelefoneORM' => array(
	            'columns'           => 'id_telefone',
	            'refTableClass'     => 'ContatosTelefoneORM',
	            'refColumns'        => 'id_telefone'
			)		
	);
	
	
}
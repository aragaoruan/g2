<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhaconvenio
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaConvenioORM extends Ead1_ORM {

	public $_name = 'tb_campanhaconvenio';
	public $_primary = array('id_convenio','id_campanhacomercial');
	public $_referenceMap = array(
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	),
	'ConvenioORM' => array(
	            'columns'           => 'id_convenio',
	            'refTableClass'     => 'ConvenioORM',
	            'refColumns'        => 'id_convenio'
	));
}

?>
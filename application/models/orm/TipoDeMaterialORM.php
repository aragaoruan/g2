<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodematerial
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDeMaterialORM extends Ead1_ORM {

	public $_name = 'tb_tipodematerial';
	public $_primary = array('id_tipodematerial');
	public $_dependentTables = array('ItemDeMaterialORM');
	public $_referenceMap = array(
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
	));
}

<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilfuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class PerfilFuncionalidadeORM extends Ead1_ORM{
	public $_name = 'tb_perfilfuncionalidade';
	public $_primary = array('id_perfil', 'id_funcionalidade');
}
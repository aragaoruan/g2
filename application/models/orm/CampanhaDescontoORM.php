<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhadesconto
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaDescontoORM extends Ead1_ORM {

	public $_name = 'tb_campanhadesconto';
	public $_primary = array('id_campanhadesconto');
	public $_referenceMap = array(
	'MeioPagamentoORM' => array(
	            'columns'           => 'id_meiopagamento',
	            'refTableClass'     => 'MeioPagamentoORM',
	            'refColumns'        => 'id_meiopagamento'
	),
	'CampanhaComercialORM' => array(
	            'columns'           => 'id_campanhacomercial',
	            'refTableClass'     => 'CampanhaComercialORM',
	            'refColumns'        => 'id_campanhacomercial'
	));
}

?>
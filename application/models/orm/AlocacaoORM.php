<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_alocacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class AlocacaoORM extends Ead1_ORM {
	public $_name = 'tb_alocacao';
	public $_primary = 'id_alocacao';
	public $_referenceMap = array(
	'SituacaoORM' => array(
				'columns'		=> 'id_situacao',
				'refTableClass'	=> 'SituacaoORM',
				'refColumns'	=> 'id_situacao'),
	'EvolucaoORM' => array(
				'columns'		=> 'id_evolucao',
				'refTableClass'	=> 'EvolucaoORM',
				'refColumns'	=> 'id_evolucao'),
	'SalaDeAulaORM' => array(
				'columns'		=> 'id_saladeaula',
				'refTableClass'	=> 'SalaDeAulaORM',
				'refColumns'	=> 'id_saladeaula')
	);
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_campanhacomercial
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class CampanhaComercialORM extends Ead1_ORM {

	public $_name = 'tb_campanhacomercial';
	public $_primary = array('id_campanhacomercial');
	public $_dependentTables = array('CampanhaFormaPagamentoORM', 'CampanhaConvenioORM', 'CampanhaPerfilORM', 'CampanhaDescontoORM', 'CampanhaProjetoAreaORM', 'VendaORM', 'CampanhaProdutoORM');
	public $_referenceMap = array(
	'TipoDescontoORM' => array(
	            'columns'           => 'id_tipodesconto',
	            'refTableClass'     => 'TipoDescontoORM',
	            'refColumns'        => 'id_tipodesconto'
	),
	'UsuarioORM' => array(
	            'columns'           => 'id_usuariocadastro',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	));
}

?>
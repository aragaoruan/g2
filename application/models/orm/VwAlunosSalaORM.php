<?php

/**
 * Classe de VwAlunosSalaORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwAlunosSalaORM extends Ead1_ORM {

	public $_name = 'vw_alunossala';
	public $_primary = array('id_alocacao');

}
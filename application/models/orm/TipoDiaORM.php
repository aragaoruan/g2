<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodia
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDiaORM extends Ead1_ORM{
	
	public $_name = 'tb_tipodia';
	public $_primary = array('id_tipodia');
	public $_dependentTables = array('DiaLetivoORM');
	
}
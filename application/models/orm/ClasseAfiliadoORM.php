<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_classeafiliado
 * @author Denise Xavier denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class ClasseAfiliadoORM extends Ead1_ORM {
	public $_name = 'tb_classeafiliado';
	public $_primary = array('id_classeafiliado');
}

?>
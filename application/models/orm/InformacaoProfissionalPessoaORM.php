<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_informacaoprofissionalpessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class InformacaoProfissionalPessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_informacaoprofissionalpessoa';
	public $_primary = array('id_informacaoprofissionalpessoa');
	public $_referenceMap = array(
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'AreaAtuacaoORM' => array(
	            'columns'           => 'id_areaatuacao',
	            'refTableClass'     => 'AreaAtuacaoORM',
	            'refColumns'        => 'id_areaatuacao'
	));	
	
}
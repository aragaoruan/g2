<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetopedagogicoserienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoPedagogicoSerieNivelEnsinoORM extends Ead1_ORM {

	public $_name = 'tb_projetopedagogicoserienivelensino';
	public $_primary = array('id_projetopedagogico', 'id_serie', 'id_nivelensino');
	public $_referenceMap = array(
	'SerieNivelEnsinoORM' => array(
	            'columns'           => 'id_serie',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_serie'
	),
	'SerieNivelEnsinoORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'SerieNivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
	),
	'ProjetoPedagogicoORM' => array(
	            'columns'           => 'id_projetopedagogico',
	            'refTableClass'     => 'ProjetoPedagogicoORM',
	            'refColumns'        => 'id_projetopedagogico'
	));
}

?>
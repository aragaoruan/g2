<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatosemailpessoaperfil
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosEmailPessoaPerfilORM extends Ead1_ORM{
	
	public $_name = 'tb_contatosemailpessoaperfil';
	public $_primary = array('id_perfil','id_usuario','id_entidade', 'id_email');
	public $_referenceMap = array(
	'PerfilORM' => array(
	            'columns'           => 'id_perfil',
	            'refTableClass'     => 'PerfilORM',
	            'refColumns'        => 'id_perfil'
			),
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
			),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
			),
	'ContatosEmailORM' => array(
	            'columns'           => 'id_email',
	            'refTableClass'     => 'ContatosEmailORM',
	            'refColumns'        => 'id_email'
			)		
	);	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_interface
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class InterfaceORM extends Ead1_ORM{
	
	public $_name = 'tb_interface';
	public $_primary = 'id_interface';
}
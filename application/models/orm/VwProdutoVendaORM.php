<?php
/**
 * Classe de mapeamento objeto relacional da view vw_produtovenda
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwProdutoVendaORM extends Ead1_ORM{
	
	public $_name = 'vw_produtovenda';
	public $_primary = array('id_produto', 'id_venda');
}
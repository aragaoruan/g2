<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_funcionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_funcionalidade';
	public $_primary = array('id_funcionalidade');
	public $_dependentTables = array('PerfilEntidadeFuncionalidadeORM', 'EntidadeFuncionalidadeORM');
	public $_referenceMap = array(
	'FuncionalidadeORM' => array(
	            'columns'           => 'id_funcionalidadepai',
	            'refTableClass'     => 'FuncionalidadeORM',
	            'refColumns'        =>'id_funcionalidade'
			),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
			)
	);
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_produtolivro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
class ProdutoLivroORM extends Ead1_ORM{
	
	public $_name = 'tb_produtolivro';
	public $_primary = array('id_produto','id_entidade','id_livro');
	
}
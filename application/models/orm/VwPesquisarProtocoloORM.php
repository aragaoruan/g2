<?php
/**
 * Classe de mapeamento objeto relacional da view de protocolo  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwPesquisarProtocoloORM extends Ead1_ORM{
	public $_name = 'vw_pesquisarprotocolo';
	public $_primary = array('id_protocolo', 'id_usuario');
}
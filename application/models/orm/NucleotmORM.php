<?php

class NucleotmORM extends Ead1_ORM {

	public $_name = 'tb_nucleotm';
	public $_primary = array('id_nucleotm');
	public $_dependentTables = array('NucleoFuncionariotmORM', 'NucleoProdutoORM');
	public $_referenceMap = array(
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
			),
	'EntidadeMatrizORM' => array(
				'columns'			=> 'id_entidadematriz',
				'refTableClass'		=> 'EntidadeORM',
				'refColumns'		=> 'id_entidade'
			),
	'EntidadeORM' => array(
				'columns'			=> 'id_entidade',
				'refTableClass'		=> 'EntidadeORM',
				'refColumns'		=> 'id_entidade'
			),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	)
	);
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipopessoa
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
class TipoPessoaORM extends Ead1_ORM {

	public $_name = 'tb_tipopessoa';
	public $_primary = array('id_tipopessoa');
}

?>
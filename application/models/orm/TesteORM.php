<?php

/**
 * Classe criada a fim de testes de CRUD
 * @author Arthur
 *
 */
class TesteORM	extends Zend_Db_Table_Abstract {
	
	protected $_name	= 'tb_teste';
	protected $_primary	= 'id_teste';
	
}
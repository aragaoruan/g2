<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipofuncao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoFuncaoORM extends Ead1_ORM {

	public $_name = 'tb_tipofuncao';
	public $_primary = array('id_tipofuncao');
	public $_dependentTables = array('FuncaoORM');
}

?>
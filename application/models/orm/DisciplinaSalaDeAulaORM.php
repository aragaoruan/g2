<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_disciplinasaladeaula
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DisciplinaSalaDeAulaORM extends Ead1_ORM {

	public $_name = 'tb_disciplinasaladeaula';
	public $_primary = array('id_disciplina', 'id_saladeaula');
	public $_referenceMap = array(
	'DisciplinaORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaORM',
	            'refColumns'        => 'id_tipodisciplina'
	),
	'SalaDeAulaORM' => array(
	            'columns'           => 'id_saladeaula',
	            'refTableClass'     => 'SalaDeAulaORM',
	            'refColumns'        => 'id_saladeaula'
	));
}

?>
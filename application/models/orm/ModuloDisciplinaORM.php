<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_modulodisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ModuloDisciplinaORM extends Ead1_ORM {

	public $_name = 'tb_modulodisciplina';
	public $_primary = array('id_modulodisciplina');
	public $_referenceMap = array(
	'ModuloORM' => array(
	            'columns'           => 'id_modulo',
	            'refTableClass'     => 'ModuloORM',
	            'refColumns'        => 'id_modulo'
	),
	'DisciplinaSerieNivelEnsinoORM' => array(
	            'columns'           => 'id_disciplina',
	            'refTableClass'     => 'DisciplinaNivelEnsinoORM',
	            'refColumns'        => 'id_disciplina'
	),
	'DisciplinaSerieNivelEnsinoORM' => array(
	            'columns'           => 'id_serie',
	            'refTableClass'     => 'DisciplinaNivelEnsinoORM',
	            'refColumns'        => 'id_serie'
	),
	'DisciplinaSerieNivelEnsinoORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'DisciplinaNivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
	));
}

?>
<?php
/**
 * Classe de mapeamento objeto-relacional da tabela tb_projetodisciplinaimportacao
 * @author Arthur Cláudio de Almeida <arthur.almeida@ead1.com.br>
 *
 */
class ProjetoDisciplinaImportacaoORM extends Ead1_ORM {
	
	protected $_name	= 'tb_projetodisciplinaimportacao';
	protected $_primary	= 'id_projetodisciplinaimportacao';
	
}
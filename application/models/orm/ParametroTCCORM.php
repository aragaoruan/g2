<?php

/**
 * Classe de ParametroTCCORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class ParametroTCCORM extends Ead1_ORM {

	public $_name = 'tb_parametrotcc';
	public $_primary = array('id_parametrotcc');

}
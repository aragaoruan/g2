<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_perfilreferenciaintegracao
 * @author Elcio
 *
 * @package models
 * @subpackage orm
 */
class PerfilReferenciaIntegracaoORM extends Ead1_ORM {
	
	public $_name = 'tb_perfilreferenciaintegracao';
	public $_primary = array('id_perfilreferenciaintegracao','id_perfilreferencia', 'id_sistema');
	

}

?>
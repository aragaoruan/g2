<?php
/**
 * Mapeamento da view que mostra os professores do sistema
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisarProfessorORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisarprofessor';
	public $_primary = array('id_usuario', 'id_entidade');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @package models
 * @subpackage orm
 */
 
class ModeloGradeNotasORM extends Ead1_ORM{
	public $_name = 'tb_modelogradenotas';
	public $_primary = array('id_modelogradenotas');
}
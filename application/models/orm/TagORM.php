<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tag
 * @author João Gabriel - gabriel.vasconcelos@ead1.com
 *
 * @package models
 * @subpackage orm
 */
class TagORM extends Ead1_ORM {
	public $_name = 'tb_tag';
	public $_primary = 'id_tag';
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_endereco
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EnderecoORM extends Ead1_ORM{
	
	public $_name = 'tb_endereco';
	public $_primary = 'id_endereco';
	public $_dependentTables = array('EnderecoORM');
	public $_referenceMap = array(
	'PaisORM' => array(
	            'columns'           => 'id_pais',
	            'refTableClass'     => 'PaisORM',
	            'refColumns'        =>'id_pais'
			),
	'TipoEnderecoORM' => array(
	            'columns'           => 'id_tipoendereco',
	            'refTableClass'     => 'TipoEnderecoORM',
	            'refColumns'        =>'id_tipoendereco'
			),
	'MunicipioORM' => array(
	            'columns'           => 'id_municipio',
	            'refTableClass'     => 'MunicipioORM',
	            'refColumns'        =>'id_municipio'
			),
	'UfORM' => array(
	            'columns'           => 'sg_uf',
	            'refTableClass'     => 'UfORM',
	            'refColumns'        =>'sg_uf'
			),
	'EntidadeEnderecoORM' => array(
	            'columns'           => 'id_endereco',
	            'refTableClass'     => 'EntidadeEnderecoORM',
	            'refColumns'        =>'id_endereco'
			)
	);
	
}
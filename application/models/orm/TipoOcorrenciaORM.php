<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoocorrencia
 * 
 * @package models
 * @subpackage orm
 */
class TipoOcorrenciaORM extends Ead1_ORM {

	public $_name = 'tb_tipoocorrencia';
	public $_primary = 'id_tipoocorrencia';
}
?>
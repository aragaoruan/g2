<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_meiopagamentointegracao
 *
 * @package models
 * @subpackage orm
 */
class MeioPagamentoIntegracaoORM extends Ead1_ORM {

	public $_name = 'tb_meiopagamentointegracao';
	public $_primary = array('id_meiopagamentointegracao');
}

?>
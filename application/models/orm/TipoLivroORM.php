<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipolivro
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
class TipoLivroORM extends Ead1_ORM{
	
	public $_name = 'tb_tipolivro';
	public $_primary = array('id_tipolivro');
	
}
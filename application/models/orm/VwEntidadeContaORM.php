<?php
/**
 * Mapeamento da view que faz a o retorno da(s) conta(s) de uma instiuição
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeContaORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadeconta';
	public $_primary = array('id_entidade', 'id_banco', 'id_tipodeconta');
}
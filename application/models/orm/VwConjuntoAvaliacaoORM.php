<?php
/**
 * Mapeamento da view vw_conjuntoavaliacao
 * Classe de mapeamento objeto relacional da tabela tb_
 * @author Eder Lamar Mariano edermariano@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwConjuntoAvaliacaoORM {

	public $_name = 'vw_avaliacaoconjunto';
	public $_primary = array('id_avaliacaoconjunto', 'id_entidade');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_textoexibicao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TextoExibicaoORM extends Ead1_ORM{
	
	public $_name = 'tb_textoexibicao';
	public $_primary = 'id_textoexibicao';
}
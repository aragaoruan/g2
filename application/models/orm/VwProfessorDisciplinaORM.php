<?php
/**
 * Classe que mapeia a view que retorna os professores de uma disciplina
 *
 * @package models
 * @subpackage orm
 */
class VwProfessorDisciplinaORM extends Ead1_ORM{
	public $_name = 'vw_professordisciplina';
	public $_primary = array('id_usuario');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_registropessoa
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class RegistroPessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_registropessoa';
	public $_primary = 'id_registropessoa';
	public $_dependentTables = array('UsuarioORM');
	
}
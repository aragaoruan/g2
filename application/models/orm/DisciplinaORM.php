<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_disciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DisciplinaORM extends Ead1_ORM {

	public $_name 				= 'tb_disciplina';
	public $_primary 			= array( 'id_disciplina' );
	public $_dependentTables	= array( 'DisciplinaSerieNivelEnsinoORM', 'AreaDisciplinaORM', 'TrilhaDisciplinaORM','UsuarioPerfilEntidadeReferenciaORM' );
	
	public $_referenceMap 		= array( 'TipoDisciplinaORM'	=> array(
														            'columns'           => 'id_tipodisciplina',
														            'refTableClass'     => 'TipoDisciplinaORM',
														            'refColumns'        => 'id_tipodisciplina'
														),
										'SituacaoORM' 			=> array(
																	'columns'           => 'id_situacao',
										            				'refTableClass'     => 'SituacaoORM',
										            				'refColumns'        => 'id_situacao'
										),
										'UsuarioORM' 			=> array(
																	'columns'			=> 'id_usuariocadastro',
																	'refTableClass'		=> 'UsuarioORM',
																	'refColumns'		=> 'id_usuario'
																	)
										);
}
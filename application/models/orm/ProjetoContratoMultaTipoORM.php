<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetocontratomultatipo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoContratoMultaTipoORM extends Ead1_ORM {

	public $_name = 'tb_projetocontratomultatipo';
	public $_primary = array('id_projetocontratomultatipo');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_dialetivo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DiaLetivoORM extends Ead1_ORM{
	
	public $_name = 'tb_dialetivo';
	public $_primary = array('id_dialetivo');
	public $_referenceMap = array(
	'CalendarioLetivoORM' => array(
	            'columns'           => 'id_calendarioletivo',
	            'refTableClass'     => 'CalendarioLetivoORM',
	            'refColumns'        => 'id_calendarioletivo'
	),
	'TipoDiaORM' => array(
	            'columns'           => 'id_tipodia',
	            'refTableClass'     => 'TipoDiaORM',
	            'refColumns'        => 'id_tipodia'
	));	
}
<?php

/**
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
class VwPessoaUsuarioORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoausuario';
	public $_primary = array('id_usuario', 'id_entidade');
}
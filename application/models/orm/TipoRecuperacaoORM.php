<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tiporecuperacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoRecuperacaoORM extends Ead1_ORM {

	public $_name = 'tb_tiporecuperacao';
	public $_primary = array('id_tiporecuperacao');
	public $_dependentTables = array('ProjetoRecuperacaoORM');
}

?>
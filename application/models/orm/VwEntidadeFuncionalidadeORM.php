<?php
/**
 * Classe de mapeamento objeto relacional da view vw_entidadefuncionalidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEntidadeFuncionalidadeORM extends Ead1_ORM{
	
	public $_name = 'vw_entidadefuncionalidade';
	public $_primary = array('id_entidade', 'id_funcionalidade', 'id_funcionalidadepai');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentoidentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentoIdentidadeORM extends Ead1_ORM{
	
	public $_name = 'tb_documentoidentidade';
	public $_primary = array('id_usuario', 'id_entidade');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
	),
	'PessoaORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_entidade'
	));	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tiporedesocial
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoRedeSocialORM extends Ead1_ORM{
	
	public $_name = 'tb_tiporedesocial';
	public $_primary = array('id_tiporedesocial');
	public $_dependentTables = array('ContatosRedeSocialORM', 'TipoRedeSocialEntidadeORM');
	
}
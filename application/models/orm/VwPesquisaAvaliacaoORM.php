<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_pesquisaavaliacao
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class VwPesquisaAvaliacaoORM extends Ead1_ORM{
	
	public $_name = 'vw_pesquisaavaliacao';
	public $_primary = array('id_avaliacao','id_tipoavaliacao','id_entidade','id_situacao');
}
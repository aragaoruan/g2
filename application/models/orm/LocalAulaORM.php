<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @package models
 * @subpackage orm
 */
 
class LocalAulaORM extends Ead1_ORM{
	public $_name = 'tb_localaula';
	public $_primary = array('id_localaula');
}
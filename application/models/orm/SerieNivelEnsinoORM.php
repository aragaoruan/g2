<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_serienivelensino
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class SerieNivelEnsinoORM extends Ead1_ORM {
	
	public $_name = 'tb_serienivelensino';
	public $_primary = array('id_serie', 'id_nivelensino');
	public $_dependentTables = array('AreaConhecimentoSerieNivelEnsinoORM', 'ModuloSerieNivelEnsino', 'ProjetoPedagogicoSerieNivelEnsino',
										'DisciplinaSerieNivelEnsino');
	public $_referenceMap = array(
	'NivelEnsinoORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'NivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
			),
	'SerieORM' => array(
	            'columns'           => 'id_serie',
	            'refTableClass'     => 'SerieORM',
	            'refColumns'        => 'id_serie'
			)
	);	
}

?>
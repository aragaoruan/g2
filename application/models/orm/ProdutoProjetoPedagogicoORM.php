<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class ProdutoProjetoPedagogicoORM extends Ead1_ORM{
	public $_name = 'tb_produtoprojetopedagogico';
	public $_primary = array('id_projetopedagogico', 'id_entidade');
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_usuariocontratoresponsavel
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwUsuarioContratoResponsavelORM extends Ead1_ORM{
	
	public $_name = 'vw_usuariocontratoresponsavel';
	public $_primary = array('id_contratoresponsavel','id_usuario');
	
}
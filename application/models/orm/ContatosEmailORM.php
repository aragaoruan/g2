<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatosemail
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosEmailORM extends Ead1_ORM{
	
	public $_name = 'tb_contatosemail';
	public $_primary = array('id_email');
	public $_dependentTables = array('ContatosEmailPessoaPerfilORM');
	
}
<?php
/**
 * Classe ORM que define o VIEW vw_encerramentosalas
 * 
 * @package models
 * @subpackage orm
 */
class VwEncerramentoSalasORM extends Ead1_ORM{
	
	public $_name = 'vw_encerramentosalas';
	public $_primary = array('id_saladeaula');
}
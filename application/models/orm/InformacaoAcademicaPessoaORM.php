<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_informacaoacademica
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class InformacaoAcademicaPessoaORM extends Ead1_ORM{
	
	public $_name = 'tb_informacaoacademicapessoa';
	public $_primary = array('id_informacaoacademicapessoa');
	public $_referenceMap = array(
	'UsuarioORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	),
	'NivelEnsinoORM' => array(
	            'columns'           => 'id_nivelensino',
	            'refTableClass'     => 'NivelEnsinoORM',
	            'refColumns'        => 'id_nivelensino'
	));	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_ocorrencia
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class OcorrenciaORM extends Ead1_ORM {

	public $_name = 'tb_ocorrencia';
	public $_primary = array('id_ocorrencia');
}
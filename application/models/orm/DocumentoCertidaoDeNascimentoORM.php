<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentocertidaonascimento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentoCertidaoDeNascimentoORM extends Ead1_ORM{
	
	public $_name = 'tb_documentocertidaodenascimento';
	public $_primary = array('id_usuario', 'id_entidade');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
	),
	'EntidadeORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'EntidadeORM',
	            'refColumns'        => 'id_entidade'
	));	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_formapagamentoparcela
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class FormaPagamentoParcelaORM extends Ead1_ORM {

	public $_name = 'tb_formapagamentoparcela';
	public $_primary = array('id_formapagamentoparcela');
	public $_referenceMap = array(
	'FormaPagamentoORM' => array(
	            'columns'           => 'id_formapagamento',
	            'refTableClass'     => 'FormaPagamentoORM',
	            'refColumns'        => 'id_formapagamento'
	));
}

?>
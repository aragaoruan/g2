<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipolancamento
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoLancamentoORM extends Ead1_ORM {

	public $_name = 'tb_tipolancamento';
	public $_primary = array('id_tipolancamento');
	public $_dependentTables = array('LancamentoORM');
}

?>
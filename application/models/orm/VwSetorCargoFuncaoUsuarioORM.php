<?php
/**
 * Classe de mapeamento objeto relacional da view vw_setorcargofuncaousuario  
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwSetorCargoFuncaoUsuarioORM extends Ead1_ORM{
	public $_name = 'vw_setorcargofuncaousuario';
	public $_primary = array('id_usuario');
}
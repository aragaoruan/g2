<?php
/**
 * Classe de mapeamento objeto relacional da view vw_formameiopagamento 
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class VwFormaMeioPagamentoORM extends Ead1_ORM{
	
	public $_name = 'vw_formameiopagamento';
	public $_primary = 'id_meiopagamento';
}
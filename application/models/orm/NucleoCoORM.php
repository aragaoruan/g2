<?php

/**
 * Classe de NucleoCoORM
 
 * @package models
 * @subpackage orm
 */
 class NucleoCoORM extends Ead1_ORM {

	public $_name = 'tb_nucleoco';
	public $_primary = array('id_nucleoco');

}

?>
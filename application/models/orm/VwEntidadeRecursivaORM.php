<?php
/**
 * Classe de mapeamento objeto relacional da view de protocolo  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VwEntidadeRecursivaORM extends Ead1_ORM{
	public $_name = 'vw_entidaderecursiva';
	public $_primary = array('id_entidade', 'id_entidadepai');
}
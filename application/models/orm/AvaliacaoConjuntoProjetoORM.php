<?php
class AvaliacaoConjuntoProjetoORM extends Ead1_ORM{
	
	public $_name 				= 'tb_avaliacaoconjuntoprojeto';
	public $_primary 			= array( 'id_avaliacaoconjuntoprojeto' );
	
	public $_referenceMap 		= array( 'AvaliacaoConjuntoORM'	=> array(
														            'columns'           => 'id_avaliacaoconjunto',
														            'refTableClass'     => 'AvaliacaoConjuntoORM',
														            'refColumns'        => 'id_avaliacaoconjunto'
														),
										'ProjetoPedagogicoORM' 			=> array(
																	'columns'			=> 'id_projetopedagogico',
																	'refTableClass'		=> 'ProjetoPedagogicoORM',
																	'refColumns'		=> 'id_projetopedagogico'
														)
										);
}
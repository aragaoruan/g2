<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_cargo
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 *
 * @package models
 * @subpackage orm
 */
class CargoORM extends Ead1_ORM {

	public $_name = 'tb_cargo';
	public $_primary = array('id_cargo');

}

?>
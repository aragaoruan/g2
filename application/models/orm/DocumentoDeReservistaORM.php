<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_documentoreservista
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DocumentoDeReservistaORM extends Ead1_ORM{
	
	public $_name = 'tb_documentodereservista';
	public $_primary = array('id_usuario', 'id_entidade');
	public $_referenceMap = array(
	'PessoaORM' => array(
	            'columns'           => 'id_usuario',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_usuario'
	),
	'PessoaORM' => array(
	            'columns'           => 'id_entidade',
	            'refTableClass'     => 'PessoaORM',
	            'refColumns'        => 'id_entidade'
	),
	'MunicipioORM' => array(
	            'columns'           => 'id_municipio',
	            'refTableClass'     => 'MunicipioORM',
	            'refColumns'        => 'id_municipio'
	),
	'UfORM' => array(
	            'columns'           => 'sg_uf',
	            'refTableClass'     => 'UfORM',
	            'refColumns'        => 'sg_uf'
	));	
	
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_projetopedagogicovalortipo
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoPedagogicoValorTipoORM extends Ead1_ORM {

	public $_name = 'tb_projetopedagogicovalortipo';
	public $_primary = array('id_projetopedagogicovalortipo');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_interfacepessoa
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class VwInterfacePessoaORM extends Ead1_ORM{
	public $_name = 'vw_interfacepessoa';
	public $_primary = 'id_tipointerfacepessoa';
}
<?php

/**
 * Classe de mapeamento objeto relacional da view vw_salaconfiguracao
 * @author Denise Xavier <denisexavier@ead1.com.br>
 * @package models
 * @subpackage orm
 */
class VwSalaConfiguracaoORM extends Ead1_ORM {
	public $_name = 'vw_salaconfiguracao';
	public $_primary = array('id_saladeaula');
}

?>
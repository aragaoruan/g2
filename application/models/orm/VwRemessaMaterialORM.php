<?php

/**
 * Classe de VwRemessaMaterialORM
 
 * @package models
 * @subpackage orm
 */
 class VwRemessaMaterialORM extends Ead1_ORM {

	public $_name = 'vw_remessamaterial';
	public $_primary = array('id_projetopedagogico');

}

?>
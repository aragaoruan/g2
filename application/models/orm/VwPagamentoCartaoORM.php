<?php

/**
 * Classe de VwPagamentoCartaoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
 class VwPagamentoCartaoORM extends Ead1_ORM {

	public $_name = 'vw_pagamentocartao';
	public $_primary = array('id_venda');

}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contatostelefoneentidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContatosTelefoneEntidadeORM extends Ead1_ORM{
	public $_name = 'tb_contatostelefoneentidade';
	public $_primary = array('id_entidade', 'id_telefone');
	
	
}
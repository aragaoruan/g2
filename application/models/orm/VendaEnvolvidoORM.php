<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class VendaEnvolvidoORM extends Ead1_ORM{
	public $_name = 'tb_vendaenvolvido';
	public $_primary = array('id_vendaenvolvido');
}
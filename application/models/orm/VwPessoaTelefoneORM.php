<?php
/**
 * Mapeamento da view que mostra o(s) contatos(s) telefone cadastro de pessoa
 * @author Dimas Sulz <dimassulz@gmail.com>
 *
 * @package models
 * @subpackage orm
 */
class VwPessoaTelefoneORM extends Ead1_ORM{
	
	public $_name = 'vw_pessoatelefone';
	public $_primary = array('id_telefone', 'id_tipotelefone', 'id_usuario', 'id_entidade');
}
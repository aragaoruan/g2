<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_vendaintegracao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VendaIntegracaoORM extends Ead1_ORM{
	
	public $_name 		= 'tb_vendaintegracao';
	public $_primary 	= 'id_vendaintegracao';
	
	
}
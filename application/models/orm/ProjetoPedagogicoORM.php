<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_projetopedagogico
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ProjetoPedagogicoORM extends Ead1_ORM
{

    public $_name = 'tb_projetopedagogico';
    public $_primary = array('id_projetopedagogico');
    public $_dependentTables = array('ProjetoPedagogicoSerieNivelEnsinoORM', 'AreaProjetoPedagogicoORM', 'ModuloORM', 'ProjetoPedagogicoORM', 'UsuarioPerfilEntidadeReferenciaORM');
    public $_referenceMap = array(
        'ProjetoPedagogicoORM' => array(
            'columns' => 'id_projetopedagogicoorigem',
            'refTableClass' => 'ProjetoPedagogicoORM',
            'refColumns' => 'id_projetopedagogico'
        ),
        'SituacaoORM' => array(
            'columns' => 'id_situacao',
            'refTableClass' => 'SituacaoORM',
            'refColumns' => 'id_situacao'
        ),
        'TrilhaORM' => array(
            'columns' => 'id_trilha',
            'refTableClass' => 'TrilhaORM',
            'refColumns' => 'id_trilha'
        ),
        'EntidadeORM' => array(
            'columns' => 'id_entidadecadastro',
            'refTableClass' => 'EntidadeORM',
            'refColumns' => 'id_entidade'
        ),
        'TipoAplicacaoAvaliacaoORM' => array(
            'columns' => 'id_tipoaplicacaoavaliacao',
            'refTableClass' => 'TipoAplicacaoAvaliacaoORM',
            'refColumns' => 'id_tipoaplicacaoavaliacao'
        ),
        'TipoProvaFinalORM' => array(
            'columns' => 'id_tipoprovafinal',
            'refTableClass' => 'TipoProvaFinalORM',
            'refColumns' => 'id_tipoprovafinal'
        ),
        'TipoExtracurricularORM' => array(
            'columns' => 'id_tipoextracurricular',
            'refTableClass' => 'TipoExtracurricularORM',
            'refColumns' => 'id_tipoextracurricular'
        ),
        'ProjetoContratoDuracaoTipoORM' => array(
            'columns' => 'id_projetocontratoduracaotipo',
            'refTableClass' => 'ProjetoContratoDuracaoTipoORM',
            'refColumns' => 'id_projetocontratoduracaotipo'
        ),
        'ProjetoContratoMultaTipoORM' => array(
            'columns' => 'id_projetocontratomultatipo',
            'refTableClass' => 'ProjetoContratoMultaTipoORM',
            'refColumns' => 'id_projetocontratomultatipo'
        ),
        'MedidaTempoConclusaoORM' => array(
            'columns' => 'id_medidatempoconclusao',
            'refTableClass' => 'MedidaTempoConclusaoORM',
            'refColumns' => 'id_medidatempoconclusao'
        ),
        'UsuarioORM' => array(
            'columns' => 'id_usuariocadastro',
            'refTableClass' => 'UsuarioORM',
            'refColumns' => 'id_usuario'
        ),
        'UsuarioORM2' => array(
            'columns' => 'id_usuario',
            'refTableClass' => 'UsuarioORM',
            'refColumns' => 'id_usuario'
        ));
}
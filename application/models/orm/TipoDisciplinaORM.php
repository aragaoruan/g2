<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipodisciplina
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoDisciplinaORM extends Ead1_ORM {

	public $_name = 'tb_tipodisciplina';
	public $_primary = array('id_tipodisciplina');
	public $_dependentTables = array('DisciplinaORM');
}

?>
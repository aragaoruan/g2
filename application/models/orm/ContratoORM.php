<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_contrato
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class ContratoORM extends Ead1_ORM {

	public $_name = 'tb_contrato';
	public $_primary = array('id_contrato');
	public $_dependentTables = array('ContratoMatriculaORM', 'ContratoResponsavelORM', 'ContratoEnvolvidoORM','ContratoConvenioORM');
	public $_referenceMap = array(
	'VendaORM' => array(
	            'columns'           => 'id_venda',
	            'refTableClass'     => 'VendaORM',
	            'refColumns'        => 'id_venda'
	),
	'SituacaoORM' => array(
	            'columns'           => 'id_situacao',
	            'refTableClass'     => 'SituacaoORM',
	            'refColumns'        => 'id_situacao'
	),
	'UsuarioORM' => array(
		'columns'			=> 'id_usuariocadastro',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'UsuarioORM2' => array(
		'columns'			=> 'id_usuario',
		'refTableClass'		=> 'UsuarioORM',
		'refColumns'		=> 'id_usuario'
	),
	'EntidadeORM' => array(
		'columns'			=> 'id_entidade',
		'refTableClass'		=> 'EntidadeORM',
		'refColumns'		=> 'id_entidade'
	),
	'EvolucaoORM' => array(
		'columns'			=> 'id_evolucao',
		'refTableClass'		=> 'EvolucaoORM',
		'refColumns'		=> 'id_evolucao'
	));
}

?>
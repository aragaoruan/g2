<?php

/**
 * Classe de mapeamento objeto relacional da tabela tb_entidade
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class EntidadeORM extends Ead1_ORM
{

    public $_name = 'tb_entidade';
    public $_primary = array('id_entidade');
    public $_dependentTables = array(
        'TipoEnderecoEntidadeORM',
        'UsuarioPerfilEntidadeORM',
        'EntidadeEnderecoORM',
        'PessoaORM',
        'PerfilEntidadeFuncionalidadeORM',
        'EntidadeFuncionalidadeORM',
        'TipoRedeSocialEntidadeORM',
        'ContatosTelefoneEntidadeORM',
        'TipoTelefoneEntidadeORM',
        'EntidadeRelacaoORM',
        'ContaEntidadeORM',
        'PerfilORM',
        'AreaConhecimentoORM',
        'EntidadeORM',
        'PerfilEntidadeORM',
        'ProjetoPedagogicoORM',
        'FundamentoLegalORM',
        'TipoDeMaterialORM',
        'EntidadeSistemaORM'
    );

    public $_referenceMap = array(
        'SituacaoORM' => array(
            'columns' => 'id_situacao',
            'refTableClass' => 'SituacaoORM',
            'refColumns' => 'id_situacao'
        ),
        'ConfiguracaoEntidadeORM' => array(
            'columns' => 'id_configuracaoentidade',
            'refTableClass' => 'ConfiguracaoEntidadeORM',
            'refColumns' => 'id_configuracaoentidade'
        ),
        'EntidadeORM' => array(
            'columns' => 'id_entidadecadastro',
            'refTableClass' => 'EntidadeORM',
            'refColumns' => 'id_entidade'
        ),
        'UsuarioORM' => array(
            'columns' => 'id_usuariocadastro',
            'refTableClass' => 'UsuarioORM',
            'refColumns' => 'id_usuario'
        )
    );

    /**
     * Método que funciona como um trigger do insert
     *    criado para agregar funcionalidades no insert
     * @param array $dados
     */
    public function insert(array $dados = null, $transacao = true)
    {
        $id = parent::insert($dados);

        if ($id) {
            $dadoNovo['st_wschave'] = sha1($id . "servicog2");
            $this->update($dadoNovo, "id_entidade = $id");
        }

        return $id;
    }
}
<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_ddd
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class DddORM extends Ead1_ORM{
	
	public $_name = 'tb_ddd';
	public $_primary = 'nu_ddd';
	public $_dependentTables = array('ContatosTelefoneORM');
	public $_referenceMap = array(
	'PaisORM' => array(
	            'columns'           => 'id_pais',
	            'refTableClass'     => 'UsuarioORM',
	            'refColumns'        => 'id_pais'
	));
}
Í<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @package models
 * @subpackage orm
 */
 
class VwSalaHorarioProfessorORM extends Ead1_ORM{
	public $_name = 'vw_salahorarioprofessor';
	public $_primary = array('id_localaula', 'id_usuario', 'id_horarioaula','id_saladeaula');
}
<?php
/**
 * Classe ORM que define o VIEW vw_encerramentocoordenador
 * 
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwEncerramentoCoordenadorORM extends Ead1_ORM{
	
	public $_name = 'vw_encerramentocoordenador';
	public $_primary = array('id_saladeaula','id_disciplina', 'id_entidade');
}
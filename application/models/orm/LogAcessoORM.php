<?php
/**
 * Classe que encapsula os dados da tb_logacesso
 * @author Denise - denisexavier@ead1.com.br
 * @package models
 * @subpackage orm
 */
class LogAcessoORM extends Ead1_ORM {
	public $_name = 'tb_logacesso';
	public $_primary = array('id_logacesso');
}

?>
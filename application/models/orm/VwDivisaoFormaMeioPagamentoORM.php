<?php
/**
 * Classe de mapeamento objeto relacional da tabela vw_divisaoformameiopagamento
 * @author dimassulz
 *
 * @package models
 * @subpackage orm
 */
class VwDivisaoFormaMeioPagamentoORM extends Ead1_ORM
{
    public $_name = 'vw_divisaoformameiopagamento';
    public $_primary = array('id_formapagamento');
}
?>
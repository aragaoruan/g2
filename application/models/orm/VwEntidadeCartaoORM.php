<?php

/**
 * Classe de VwCartaoBandeiraORM
 * @package models
 * @subpackage orm
 *
 */
 class VwEntidadeCartaoORM extends Ead1_ORM {

 	public $_name = 'vw_entidadecartao';
 	public $_primary = array('id_entidade', 'id_cartaobandeira');
}

?>
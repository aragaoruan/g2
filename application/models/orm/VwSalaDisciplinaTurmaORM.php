<?php
/**
 * Classe ORM que define o VIEW vw_saladisciplinaturma
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class VwSalaDisciplinaTurmaORM extends Ead1_ORM{
	
	public $_name = 'vw_saladisciplinaturma';
	public $_primary = array('id_matricula','id_saladeaula','id_projetopedagogico','id_disciplina');
}
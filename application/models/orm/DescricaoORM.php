<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_avaliacao
 * @author Yannick NR
 *
 * @package models
 * @subpackage orm
 */
class DescricaoORM extends Ead1_ORM {

	public $_name               = 'tb_avaliacao';
	public $_primary            = array('st_descricao');
}

?>
<?php
/**
 * Classe de mapeamento objeto relacional da view vw_pesquisarcontrato
 * @author Dimas Sulz
 * 
 */
class VwPesquisarContratoORM extends Ead1_ORM
{
	public $_name = 'vw_pesquisarcontrato';
	public $_primary = array('id_contrato');
}
?>
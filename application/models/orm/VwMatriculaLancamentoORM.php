<?php
/**
 * Classe de mapeamento objeto relacional da view vw_matriculalancamento
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class VwMatriculaLancamentoORM extends Ead1_ORM {
	public $_name = 'vw_matriculalancamento';
	public $_primary = 'id_matricula';
}
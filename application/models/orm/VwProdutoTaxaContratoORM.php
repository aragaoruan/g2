<?php

/**
 * Classe de VwProdutoTaxaContratoORM
 
 * @package models
 * @subpackage dao
 */
 class VwProdutoTaxaContratoORM extends Ead1_ORM {

	public $_name = 'vw_produtotaxacontrato';
	public $_primary = array('id_contrato');

}
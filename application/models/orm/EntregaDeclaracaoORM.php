<?php
/**
 * Classe de mapeamento objeto relacional da tabela  
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package models
 * @subpackage orm
 */
 
class EntregaDeclaracaoORM extends Ead1_ORM{
	public $_name = 'tb_entregadeclaracao';
	public $_primary = array('id_entregadeclaracao');
}
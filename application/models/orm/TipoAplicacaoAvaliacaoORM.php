<?php
/**
 * Classe de mapeamento objeto relacional da tabela tb_tipoavaliacao
 * @author edermariano
 *
 * @package models
 * @subpackage orm
 */
class TipoAplicacaoAvaliacaoORM extends Ead1_ORM {

	public $_name = 'tb_tipoaplicacaoavaliacao';
	public $_primary = array('id_tipoaplicacaoavaliacao');
	public $_dependentTables = array('ProjetoPedagogicoORM');
}

?>
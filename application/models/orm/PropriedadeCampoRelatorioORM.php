<?php 
/**
 * Classe de mapeamento objeto relacional da tabela tb_propriedadecamporelatorio
 * @author Eder Lamar
 *
 * @package models
 * @subpackage orm
 */
class PropriedadeCampoRelatorioORM extends Ead1_ORM{
	
	public $_name = 'tb_propriedadecamporelatorio';
	public $_primary = 'id_camporelatorio';
}
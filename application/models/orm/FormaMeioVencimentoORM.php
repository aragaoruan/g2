<?php
/**
 * Classe de FormaMeioVencimentoORM
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package models
 * @subpackage orm
 */
class FormaMeioVencimentoORM extends Ead1_ORM {

	public $_name = 'tb_formameiovencimento';
	public $_primary = array('id_formameiovencimento');
	
}
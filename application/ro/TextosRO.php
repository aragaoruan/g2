<?php

class TextosRO extends Ead1_RO {

	/**
	 * @var TextosBO
	 */
	private $bo;
	
	public $mensageiro;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new TextosBO();
	}
	
	/**
	 * Metodo que Retorna o texto de sistema.
	 * @param TextoSistemaTO $tsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoSistema(TextoSistemaTO $tsTO){
		return $this->bo->retornarTextoSistema($tsTO);
	}
	
	/**
	 * Método que retorna as variaveis do texto
	 * @param TextoVariaveisTO $tvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoVariaveis(TextoVariaveisTO $tvTO){
		return $this->bo->retornarTextoVariaveis($tvTO);
	}
	
	/**
	 * Metodo que Retorna a view de texto de sistema.
	 * @param VwTextoSistemaTO $vwtsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwTextoSistema(VwTextoSistemaTO $vwtsTO){
		return $this->bo->retornarVwTextoSistema($vwtsTO);
	}
	
	/**
	 * Método que verifica as Variaveis Dinamicas do Texto para Retornar o Factory de Filtros
	 * @param TextoSistemaTO $textoSistemaTO
	 * @param Object $params
	 * @return Ead1_Mensageiro
	 */
	public function retornaFactoryVariaveisDinamicas(TextoSistemaTO $textoSistemaTO, $params){
		$textoSistemaBO = new TextoSistemaBO();
		return $textoSistemaBO->retornaFactoryVariaveisDinamicas($textoSistemaTO, (is_array($params) ? $params : get_object_vars($params)));
	}
	
	/**
	 * Metodo que exclui o texto sistema
	 * @param TextoSistemaTO $tsTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarTextoSistema(TextoSistemaTO $tsTO){
		return $this->bo->deletarTextoSistema($tsTO);
	}
}

?>
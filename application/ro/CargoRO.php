<?php
/**
 * Classe de comunicação com Flex para interações com objeto cargo
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package application
 * @subpackage ro
 */
class CargoRO extends Ead1_RO{
	
	private $_bo;
	
	public function __construct(){
		$this->_bo = new CargoBO();
	}
	
	/**
	 * Metodo que Retorna o Setor
	 * 
	 * @param CargoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarCargo(CargoTO $to){
		return $this->_bo->retornarCargo($to);
	}
}
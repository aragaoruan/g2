<?php
/**
 * Classe de comunicação com Flex para interações com objeto de Tramites
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class TramiteRO extends Ead1_RO{
	
	/**
	 * Variavel que contem a instancia do TramiteBO 
	 * @var TramiteBO
	 */
	private $bo;
	
	public function __construct(){
		$this->bo = new TramiteBO();
	}
	
	/**
	 * Metodo que cadastra um tramite
	 * @param TramiteTO $to
	 * @param int $idCategoriaTramite
	 * @param int $idChave
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTramite(TramiteTO $to, $idCategoriaTramite, $idChave){
		return $this->bo->cadastrarTramite($to, $idCategoriaTramite, $idChave);
	}
	
	/**
	 * Metodo que cadastra um vinculo de tramite com matricula
	 * @param TramiteMatriculaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTramiteMatricula(TramiteMatriculaTO $to){
		return $this->bo->cadastrarTramiteMatricula($to);
	}
	
	/**
	 * Metodo que retorna os tipos de tramite
	 * @param TipoTramiteTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoTramite(TipoTramiteTO $to){
		return $this->bo->retornarTipoTramite($to);
	}
	
	/**
	 * Metodo que retorna os tipos de tramite
	 * @param CategoriaTramiteTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarCategoriaTramite(CategoriaTramiteTO $to){
		return $this->bo->retornarCategoriaTramite($to);
	}
	
	/**
	 * Metodo que retorna os vinculos de tramite com matricula
	 * @param TramiteMatriculaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarTramiteMatricula(TramiteMatriculaTO $to){
		return $this->bo->retornarTramiteMatricula($to);
	}
	
	/**
	 * Metodo que retorna os Tramites
	 * @param TramiteTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarTramite(TramiteTO $to){
		return $this->bo->retornarTramite($to);
	}
	
	/**
	 * @param int $idCategoria 
	 * @param array $chaves
	 * @param int $idTipoTramite
	 *  
	 * @return Ead1_Mensageiro
	 */
	public function retornarTramiteSP( $idCategoria, array $chaves, $idTipoTramite ){
		return $this->bo->retornarTramiteSP( $idCategoria, $chaves, $idTipoTramite );
	}
	
	
	/**
	 * Método que atualiza a tabela tb_tramite
	 * @param TramiteTO $tramiteTo
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function alterarTramiteTo(TramiteTO $tramiteTo){
		return $this->bo->alterarTramite($tramiteTo);
	}
	
	
}
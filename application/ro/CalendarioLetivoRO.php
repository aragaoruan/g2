<?php
/**
 * Classe de comunicação com Flex para interações com objeto CalendarioLetivoRO
 * @author Eduardo RomÃ£o - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class CalendarioLetivoRO extends Ead1_RO{
	
	public $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new CalendarioLetivoBO();
	}
	
	/**
	 * Metodo que decide se salva ou edita o período letivo
	 * @param PeriodoLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarPeriodoLetivo(PeriodoLetivoTO $plTO){
		if($plTO->getId_periodoletivo()){
			return $this->editarPeriodoLetivo($plTO);
		}
		return $this->cadastrarPeriodoLetivo($plTO);
	}
		
	/**
	 * Metodo que decide se salva ou edita o dia letivo
	 * @param DiaLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDiaLetivo(DiaLetivoTO $plTO){
		if($plTO->getId_dialetivo()){
			return $this->editarDiaLetivo($plTO);
		}
		return $this->cadastrarDiaLetivo($plTO);
	}
	
	/**
	 * @todo usar o metodo cadastrarDiaLetivo para cadastrar o calendario Letivo
	 * Metodo que cadastra o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCalendarioLetivo(CalendarioLetivoTO $clTO){
		return $this->bo->cadastrarCalendarioLetivo($clTO);
	}
		
	/**
	 * Metodo que cadastra o período letivo
	 * @param PeriodoLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPeriodoLetivo(PeriodoLetivoTO $plTO){
		return $this->bo->cadastrarPeriodoLetivo($plTO);
	}
		
	/**
	 * Metodo que cadastra o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDiaLetivo(DiaLetivoTO $dlTO){
		return $this->bo->cadastrarDiaLetivo($dlTO);
	}
		
	/**
	 * Metodo que edita o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCalendarioLetivo(CalendarioLetivoTO $clTO){
		return $this->bo->editarCalendarioLetivo($clTO);
	}
		
	/**
	 * Metodo que edita o período letivo
	 * @param PeriodoLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function editarPeriodoLetivo(PeriodoLetivoTO $plTO){
		return $this->bo->editarPeriodoLetivo($plTO);
	}
		
	/**
	 * Metodo que edita o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function editarDiaLetivo(DiaLetivoTO $dlTO){
		return $this->bo->editarDiaLetivo($dlTO);
	}
		
	/**
	 * Metodo que exclui o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCalendarioLetivo(CalendarioLetivoTO $clTO){
		return $this->bo->deletarCalendarioLetivo($clTO);
	}
		
	/**
	 * Metodo que exclui o período letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarPeriodoLetivo(PeriodoLetivoTO $plTO){
		return $this->bo->deletarPeriodoLetivo($plTO);
	}
		
	/**
	 * Metodo que exclui o dia letivo
	 * @param DiaLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDiaLetivo(DiaLetivoTO $plTO){
		return $this->bo->deletarDiaLetivo($plTO);
	}
		
	/**
	 * Método que retorna o calendário letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCalendarioLetivo(CalendarioLetivoTO $clTO){
		return $this->bo->retornarCalendarioLetivo($clTO);
	}
		
	/**
	 * Método que retorna o período letivo
	 * @param CalendarioLetivoTO $clTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO){
		return $this->bo->retornarPeriodoLetivo($plTO);
	}
		
	/**
	 * Método que retorna o dia letivo
	 * @param DiaLetivoTO $dlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDiaLetivo(DiaLetivoTO $dlTO){
		return $this->bo->retornarDiaLetivo($dlTO);
	}
	
}
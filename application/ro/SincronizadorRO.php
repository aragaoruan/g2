<?php
/**
 * Classe para sincronizações com serviços
 * @author edermariano
 *
 */
class SincronizadorRO extends Ead1_RO{
	
	/**
	 * Método que sincroniza áreas
	 * @param int $idEntidade
	 * @param int $idSistema
	 */
	public function sincronizarAreas($idEntidade, $idSistema){
		$bo = new SincronizadorBO();
		return $bo->areas($idEntidade, $idSistema);
	}
	
	/**
	 * Método que sincroniza lançamentos pendentes no fluxus
	 * @return Ead1_Mensageiro
	 */
	public function sincronizarFluxusPendentes(){
		$bo = new SincronizadorBO();
		return $bo->lancamentosPendentesFluxus();
	}
}
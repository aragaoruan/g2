<?php
/**
 * @author Denise - denisexavier@ead1.com.br
 * Classe para as requisições de Objetos Remotos
 * @package ro
 */
class CAtencaoRO extends Ead1_RO {

	private $bo;
	
	public function __construct(){
		$this->bo = new CAtencaoBO();
	}
	
	/**
	 * Método que retorna as ocorrências
	 * Obrigatórios: id_usuario, id_funcao e id_nucleoco
	 * @param VwOcorrenciasPessoaTO $to
	 * @param $dt_inicial
	 * @param $dt_final
	 * @return Ead1_Mensageiro
	 */
	public function retornaPesquisaOcorrencias(VwOcorrenciasPessoaTO $to, $dt_inicial = null, $dt_final = null){
		return $this->bo->retornarOcorrenciasPessoa($to, $dt_inicial, $dt_final);
	}
	
	/**
	 * Método que retorna ocorrências pendentes do usuário
	 * @param VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwOcorrenciaAguardando(VwOcorrenciaAguardandoTO $vwOcorrenciaAguardandoTO){
		return $this->bo->retornarVwOcorrenciaAguardando($vwOcorrenciaAguardandoTO);
	}
	
	/**
	 * Método que retorna ocorrências do usuário
	 * @param VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwOcorrenciasPessoa(VwOcorrenciasPessoaTO $vwOcorrenciasPessoaTO){
		return $this->bo->retornarVwOcorrenciasPessoa($vwOcorrenciasPessoaTO);
	}
	
	/**
	 * Método que retorna Funcções e Nucleo
	 * @param VwNucleoPessoaCoTO $vwnucleopessoa
	 * @return Ead1_Mensageiro
	 */
	public function retornaNucleoFuncoesCA(VwNucleoPessoaCoTO $vwnucleopessoa){
		return $this->bo->retornaNucleoFuncoesCA($vwnucleopessoa);
	}
	
	/**
	 * Retorna Assuntos-Pai
	 * @param VwNucleoAssuntoCoTO $assunto
	 * @return Ead1_Mensageiro
	 */
	public function retornaAssuntoPai(VwNucleoAssuntoCoTO $assunto){
		return $this->bo->retornaAssuntoPai($assunto);
	}
	
	
	/**
	 * Retorna sub-assuntos a partir de um assunto pai
	 * @param AssuntoCoTO $assunto
	 * @return Ead1_Mensageiro
	 */
	public function retornaAssunto(AssuntoCoTO $assunto){
        $vwassuntos = new VwAssuntosTO();
        $vwassuntos->setBl_ativo(1);
        $vwassuntos->setId_assuntocopai($assunto->getId_assuntocopai());
		return $this->bo->retornaComboAssuntoco($vwassuntos, false);
	}
	
	
	/**
	 * Retorna atendentes [Lista de pessoa vinculadas a aquele assunto com a função Atendente(8) ou Atendente de Setor(10) . 
	 * @param VwNucleoPessoaCoTO $vwnucleopessoa
	 * @return Ead1_Mensageiro
	 */
	public function retornaAtendente(VwNucleoPessoaCoTO $vwnucleopessoa){
		return $this->bo->retornaAtendente($vwnucleopessoa);
	}
	
	/**
	 * Método que detalha uma Ocorrencia
	 * O método verifica se a funcão é de Atendente (8), caso seja, verifica se a ocorrência já tem atendente.
	 * Se não tiver, seta o usuario logado como atendente da mesma
	 * @param VwOcorrenciasPessoaTO $to
	 * @return Ambigous <Ead1_Mensageiro, VwOcorrenciaTO>
	 */
	public function detalharOcorrencia(VwOcorrenciasPessoaTO $to){
		return $this->bo->detalharOcorrencia($to);
	}
	
	/**
	 * Método que retorna as evoluções vinculadas a função de um atendente no nucleo e a evolucao da ocorrência detalhada
	 * @param VwOcorrenciaEvolucao $vwocorrenciaevolucao
	 * @return Ead1_Mensageiro
	 */
	public function retornaEvolucoesAtendimento(VwOcorrenciaEvolucaoTO $to){
		return $this->bo->retornaEvolucoesAtendimento($to);
	}
	
	/**
	 * Retorna atendentes de acordo com o assunto para encaminhar uma ocorência
	 * (todos que tem a funcao de atendente ou atendente de setor para o assunto selecionado)
	 * @param VwNucleoPessoaCoTO $nucleoPessoa
	 * @return Ead1_Mensageiro
	 */
	public function retornaAtendenteEncaminharOcorrencia(VwNucleoPessoaCoTO $nucleoPessoa){
		return $this->bo->retornaAtendentesEncaminharOcorrencia($nucleoPessoa);
	}
	
	/**
	 * Método que encaminha uma ocorrência - Grava um trâmite para essa alteração
	 * @param VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo
	 * @return Ead1_Mensageiro
	 */
	public function encaminharOcorrencia(VwOcorrenciasPessoaTO $to){
		return $this->bo->encaminharOcorrencia($to);
	}
	
	/**
	 * Método que gera cópia da Ocorrnência- Grava Trâmite
	 * @param VwOcorrenciasPessoaTO $vwOcorrenciaPessoaTo
	 * @return Ead1_Mensageiro
	 */
	public function gerarCopiaOcorrencia(VwOcorrenciasPessoaTO $to){
		return $this->bo->gerarCopiaOcorrencia($to);
	}
	
	
	/**
	 * Método que salva interações da ocorrência para o flex
	 * @param OcorrenciaTO $ocorrencia
	 * @param TramiteTO $tramite
	 * @param $_FILES $arquivo
	 * @return Ead1_Mensageiro
	 */
	public function novaInteracao(OcorrenciaTO $ocorrencia, TramiteTO $tramite, $arquivo=null, $bl_notificacao=false, $bl_interessado = false){
		return $this->bo->novaInteracao($ocorrencia, $tramite, $arquivo, $bl_notificacao, $bl_interessado);
	}
	
	/**
	 * Método que gera interação para varias ocorrências
	 * @param $arrOcorrencias
	 * @param TramiteTO $tramiteTO
	 * @return Ead1_Mensageiro
	 */
	public function gerarInteracoesOcorrencias($arrOcorrencias, TramiteTO $tramiteTO){
		return $this->bo->gerarInteracoesOcorrencias($arrOcorrencias, $tramiteTO);
	}
	
	/**
	 * Método que devolve uma ocorrência para a distribuição
	 * @param OcorrenciaTO $ocorrencia
	 * @return Ead1_Mensageiro
	 */
	public function devolverOcorrencia(OcorrenciaTO $to){
		return $this->bo->devolverOcorrencia($to);
	}
	
	/**
	 * Método retorna as ocorrências do usuário da sessão.
	 * @param VwOcorrenciaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarOcorrencias(VwOcorrenciaTO $to)
	{
		return $this->bo->retornarOcorrencias($to);
	}

	/**
	 * Método que encerra uma ocorrência
	 * @param OcorrenciaTO $ocorrencia
	 * @return Ead1_Mensageiro
	 */
	public function encerrarOcorrencia(OcorrenciaTO $to){
		return $this->bo->encerrarOcorrencia($to);
	}
	
	/**
	 * Método que salva a ocorrência
	 * @param OcorrenciaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarOcorrencia(OcorrenciaTO $ocorrenciaTO, $arquivo = null){
		return $this->bo->salvarOcorrencia($ocorrenciaTO, $arquivo);
	}
	
	/**
	 * Método que salva/atualiza a ocorrência
	 * @param OcorrenciaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarOcorrencias(OcorrenciaTO $ocorrenciaTO){
		return $this->bo->salvarOcorrencias($ocorrenciaTO);
	}
	
	/**
	 * Retorna as interações de uma ocorrencia
	 * @param VwOcorrenciaTO $ocorrencia
	 * @return array interacoes |boolean
	 */
	public function retornaInteracoesOcorrencia(VwOcorrenciaTO $ocorrencia){
		return $this->bo->retornaInteracoesOcorrencia($ocorrencia);
	}
	
	/**
	 * Retorna o combo de categorias da ocorrência
	 * @return Ead1_Mensageiro
	 */
	public function retornarComboCategoriaOcorrencia()
	{
		return $this->bo->retornarComboCategoriaOcorrencia();
	}
	
	/**
	 * Método para reabertura de uma ocorrência
	 * @param VwOcorrenciasPessoaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function reabrirOcorrencia(VwOcorrenciasPessoaTO $to){
		return $this->bo->reabrirOcorrencia($to);
	}
	
	/**
	 * Retorna texto para ocorrência padrao do assunto
	 * @param TextoSistemaTO $texto
	 * @return Ead1_Mensageiro|Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornaTextoAssunto(TextoSistemaTO $texto){
		return $this->bo->retornaTextoAssunto($texto);
	}
	
	public function verificaOcorrenciaMatricula(MatriculaTO $to){
		return $this->bo->verificaOcorrenciaMatricula($to);
	}
	
	/**
	 * Método que solicita o encerramento de uma ocorrencia
	 * @param OcorrenciaTO $ocorrencia
	 * @return Ead1_Mensageiro
	 */
	public function solicitaEncerramentoOcorrencia(OcorrenciaTO $ocorrencia){
		return $this->bo->solicitaEncerramentoOcorrencia($ocorrencia);
	}
	
	
	/**
	 * nega o encerramento de uma ocorrencia e retorna para aberta
	 * @param OcorrenciaTO $ocorrenciaTO
	 * @param TramiteTO $tramiteTO
	 * @return Ead1_Mensageiro
	 */
	public function negarEncerramentoOcorrencia(OcorrenciaTO $ocorrenciaTO, TramiteTO $tramiteTO){
		return $this->bo->negarEncerramentoOcorrencia($ocorrenciaTO, $tramiteTO);
	}
	
	
	
	/**
	 * @param VwOcorrenciasPessoaTO $ocorrencia
	 * @return Ead1_Mensageiro
	 */
	public function agendarOcorrencia(VwOcorrenciasPessoaTO $ocorrencia){
		return $this->bo->agendarOcorrencia($ocorrencia);
	}
	
	/**
	 * @param array $ocorrencias
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function salvarArrayOcorrenciasFinanceiro(array $ocorrencias, array $dados){
		return $this->bo->salvarArrayOcorrenciasFinanceiro($ocorrencias, $dados);
	}
	
}

?>
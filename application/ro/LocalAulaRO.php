<?php
/**
 * Classe de comunicação com Flex para interações com objeto LocalAula
 * @author Paulo Silva - paulo.silva@unyleya.com.br
 * @package application
 * @subpackage ro
 */
class LocalAulaRO extends Ead1_RO{
	
	public $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new LocalAulaBO();
	}
	
	/**
	 * Metodo que Decide se Edita ou Cadastra o local da aula
	 * @param LocalAulaTO $laTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarLocalAula(LocalAulaTO $laTO){
		if($laTO->getId_localaula()){
			return $this->editarLocalAula($laTO);
		}
		return $this->cadastrarLocalAula($laTO);
	}
	
	/**
	 * Metodo que Cadastra e Exclui os Vinculos do Local com o Dias da Semanas
	 * @param LocalAulaTO $laTO
	 * @param array (LocalDiaSemanaTO $arrhdsTO)
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayLocalAulaDiaSemana(LocalAulaTO $laTO, $arrhdsTO){
		return $this->bo->salvarArrayLocalAulaDiaSemana($laTO, $arrhdsTO);
	}
		
	/**
	 * Metodo que Cadastra Local da Aula
	 * @param LocalAulaTO $laTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLocalAula(LocalAulaTO $laTO){
		return $this->bo->cadastrarLocalAula($laTO);
	}
		
	/**
	 * Metodo que Edita o Local da Aula
	 * @param LocalAulaTO $laTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLocalAula(LocalAulaTO $laTO){
		return $this->bo->editarLocalAula($laTO);
	}
		
	/**
	 * Metodo que Exclui o Local da Aula
	 * @param LocalAulaTO $laTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarLocalAula(LocalAulaTO $laTO){
		return $this->bo->deletarLocalAula($laTO);
	}
		
	/**
	 * Metodo que Retorna o Local da Aula
	 * @param LocalAulaTO $laTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLocalAula(LocalAulaTO $laTO){
		return $this->bo->retornarLocalAula($laTO);
	}
	
}
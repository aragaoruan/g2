<?php

/**
 * Classe de ponte Flex -> PHP
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 14/06/2012
 * @package ro
 */
class PacoteRO extends Ead1_RO {

	/**
	 * @var PacoteBO
	 */
	private $bo;
	
	public function __construct(){
		$this->bo = new PacoteBO();
	}
	
	/**
	 * Método de Matrículas disponíveis para o Pacote
	 * @param VwPacoteMatriculaTO $to
	 * @return Ambigous <boolean, multitype:>
	 * @see 
	 */
	public function listarPacoteMatricula(VwPacoteMatriculaTO $to){
		return $this->bo->listarPacoteMatricula($to);
	}
	
	
	/**
	 * Listagem de remessas
	 * @param PacoteTO $remessaTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>
	 */
	public function listarPacote(PacoteTO $remessaTO){
		return $this->bo->listarPacote($remessaTO);
	}
	

	
	/**
	 * Salva ou Edita a PacoteTO
	 * @param PacoteTO $reTO
	 * @return Ambigous <Ead1_Mensageiro, Ambigous>
	 */
	public function salvarPacote(PacoteTO $reTO){
		return $this->bo->salvarPacote($reTO);
	}
	
	/**
	 * Cadastra a entrega de um material para o processo de entrega
	 * @param array de EntregaMaterialTO $arem | EntregaMaterialTO $arem
	 * @return Ambigous <Ead1_Mensageiro, Ambigous>
	 */
	public function cadastrarEntregaMaterial($arem, PacoteTO $paTO = null){
		return $this->bo->cadastrarEntregaMaterial($arem,$paTO);
	}
	
	/**
	 * Edita a entrega de um material para o processo de entrega
	 * @param EntregaMaterialTO $arem
	 * @return Ambigous <Ead1_Mensageiro, Ambigous>
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $em){
		return $this->bo->editarEntregaMaterial($em);
	}
	

}

?>
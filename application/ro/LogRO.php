<?php
/**
 * @author Denise - denisexavier@ead1.com.br
 * Classe para as requisições de Objetos Remotos
 * @package ro
 */
class LogRO extends Ead1_RO {
	private $bo;
	
	public function __construct(){
		$this->bo = new LogBO();
	}
	

	/**
	 * Método que salva um log. 
	 * @param LogAcessoTO $logto
	 * @return Ead1_Mensageiro
	 */
	public function gravaLog(LogAcessoTO $logto){
		return $this->bo->gravaLog($logto);
	}
	
	/**
	 * Método de listagem de VwLogMatricula
	 * @param VwLogMatriculaTO $to
	 * @param $dt_cadastroinicio
	 * @param $dt_cadastrofim
	 * @return Ambigous <boolean, multitype:>
	 * @see nome_classeBO::listarnome_classe();
	 */
	public function listarVwLogMatricula(VwLogMatriculaTO $to, $dt_cadastroinicio, $dt_cadastrofim){
		return $this->bo->listarVwLogMatricula($to, $dt_cadastroinicio, $dt_cadastrofim);
	}
	
}

?>
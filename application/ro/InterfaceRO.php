<?php
/**
 * Classe de comunicação com Flex para interações com objeto Interface
 * @author Eder Lamar - edermariano@gmail.com
 * @package application
 * @subpackage ro
 */
class InterfaceRO extends Ead1_RO{
	
	/**
	 * Método que retorna os dados de Interface Pessoa
	 * @param VwInterfacePessoaTO $vwInterfacePessoaTO
	 */
	public function retornarVwInterfacePessoa(VwInterfacePessoaTO $vwInterfacePessoaTO){
		return new Ead1_Mensageiro($vwInterfacePessoaTO->fetch());
	}
	
	/**
	 * Método que cadastra a configuração de interface
	 * @param array $arInterfacePessoaTO array de InterfaceTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArInterfacePessoa(array $arInterfacePessoaTO){
		$bo = new InterfaceBO();
		return $bo->cadastrarArInterfacePessoa($arInterfacePessoaTO);
	}
	
	/**
	 * Método que cadastra a configuração de interface
	 * @param InterfacePessoaTO $interfacePessoaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarInterfacePessoa(InterfacePessoaTO $interfacePessoaTO){
		$bo = new InterfaceBO();
		return $bo->cadastrarInterfacePessoa($interfacePessoaTO);
	}
}
<?php

/**
 * Classe de comunicação com Flex para interações com objeto Lancamento
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class LancamentoRO extends Ead1_RO{
	
	/**
	 * Método que Salva um lancamento
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param LancamentoTO $to
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function salvarLancamento(LancamentoTO $to){
		
		$venda = new VendaBO();
		if($to->getId_lancamento()){
			return $venda->editarLancamento($to);
		} else {
			return $venda->cadastrarLancamento($to);
		}
		
	}
	
	
}
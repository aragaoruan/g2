<?php
/**
 * Classe de comunicação com Flex para interações com objeto Financeiro
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class FinanceiroRO extends Ead1_RO{
	
	
	/**
	 * @param int $idEntidade
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosConfBoleto( $idEntidade ) {
		$financeiroBO	= new FinanceiroBO();
		return $financeiroBO->retornarDadosConfBoleto( $idEntidade );
	}
	
	
	/**
	 * Realiza o pagamento dos lançamentos  de uma venda
	 * @param PagamentoCartaoTO $pagamentoCartaoTO
	 */
	public function pagamentoCartaoLancamentoVenda( PagamentoCartaoTO $pagamentoCartaoTO ){
		$financeiroBO	= new FinanceiroBO();
		return $financeiroBO->pagamentoCartaoLancamentoVenda( $pagamentoCartaoTO );
	} 
	
	/**
	 * Retorna as bandeiras de cartão de crédito configuradas para a entidade de sessão
	 * @param $cartaoBandeiraTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCartaoBandeira( CartaoBandeiraTO $cartaoBandeiraTO ){
		$financeiroBO	= new FinanceiroBO();
		return $financeiroBO->retornarCartaoBandeira( $cartaoBandeiraTO ); 
	}
	
	/**
	 * Método que retorna o status financeiro (Em Atraso, Em Dia ou Pago)
	 * 	para o contrato da matrícula
	 * @param MatriculaTO $matriculaTO
	 * @param Boolean $detalhado
	 * @return Ead1_Mensageiro
	 */
	public function retornarStatusFinanceiro(MatriculaTO $matriculaTO, $detalhado = false){
		$financeiroBO	= new FinanceiroBO();
		return $financeiroBO->retornarStatusFinanceiro($matriculaTO, $detalhado); 
	}
	
	
	/**
	 * Efetua o upload e gera o processo no arquivo retorno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param ArquivoTO $arquivo
	 * @return Ead1_Mensageiro
	 */
	public function salvarArquivoRetorno(ArquivoTO $arquivo){
		$financeiroBO	= new FinanceiroBO();
		return $financeiroBO->salvarArquivoRetorno($arquivo);
	}
}
<?php
/**
 * Classe de comunicação com Flex para interações com objeto Secretaria
 * Data 19 / 01 / 2010
 * @author Arthur Cláudio de Almeida Pereira - <arthur.almeida@ead1.com.br>
 * @package application
 * @subpackage ro
 */
class SecretariaRO extends Ead1_RO{
	public $mensageiro;
	
	public $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new SecretariaBO();
	}
	
	/**
	 * Método que define se deve cadastrar ou editar o documento.
	 * @param DocumentosTO $docTO
	 * @param Array $collectionUtilizacao
	 * @param Array $collectionProjetoPedagogico
	 * @return Ead1_Mensageiro	 */
	public function salvarDocumento(DocumentosTO $docTO, $collectionUtilizacao = null, $collectionProjetoPedagogico = null) {
		if($docTO->getId_documentos()){
			return $this->editarDocumento($docTO, $collectionUtilizacao, $collectionProjetoPedagogico);
		}else{
			return $this->cadastrarDocumento($docTO, $collectionUtilizacao, $collectionProjetoPedagogico);
		}
	}
	
	/**
	 * Metodo que verificase o aluno possui alguma pendencia na secretaria
	 * @param MatriculaTO $mTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarStatusSecretaria(MatriculaTO $mTO){
		return $this->bo->retornarStatusSecretaria($mTO);
	}
	
	/**
	 * Metodo que retorna dados da vw_encerramentocoordenador para coordenador
	 * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoCoordenador_Coordenador(VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO){
		return $this->bo->retornarVwEncerramentoCoordenador_Coordenador($vwEncerramentoCoordenadorTO);
	}
	
	/**
	 * Metodo que retorna dados da vw_encerramentocoordenador para pedagogico
	 * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoCoordenador_Pedagogico(VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO){
		return $this->bo->retornarVwEncerramentoCoordenador_Pedagogico($vwEncerramentoCoordenadorTO);
	}
	
	/**
	 * Metodo que retorna dados da vw_encerramentocoordenador para financeiro
	 * @param VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoCoordenador_Financeiro(VwEncerramentoCoordenadorTO $vwEncerramentoCoordenadorTO){
		return $this->bo->retornarVwEncerramentoCoordenador_Financeiro($vwEncerramentoCoordenadorTO);
	}
	
	/**
	 * Metodo que retorna dados da vw_encerramentosalas para perfil coordenador
	 * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoSalas_Coordenador(VwEncerramentoSalasTO $vwEncerramentoSalasTO){
		return $this->bo->retornarVwEncerramentoSalas_Coordenador($vwEncerramentoSalasTO);
	}
	
	/**
	 * Metodo que retorna dados da vw_encerramentosalas para perfil pedagogico
	 * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoSalas_Pedagogico(VwEncerramentoSalasTO $vwEncerramentoSalasTO){
		return $this->bo->retornarVwEncerramentoSalas_Pedagogico($vwEncerramentoSalasTO);
	}
	/**
	 * Metodo que retorna dados da vw_encerramentosalas para perfil financeiro
	 * @param VwEncerramentoSalasTO $vwEncerramentoSalasTO
	 * @retrun Ead1_Mensageiro
	 */
	public function retornarVwEncerramentoSalas_Financeiro(VwEncerramentoSalasTO $vwEncerramentoSalasTO){
		return $this->bo->retornarVwEncerramentoSalas_Financeiro($vwEncerramentoSalasTO);
	}
	
	/**
	 * Metodo que retorna o o detalhamento do status da secretaria
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDetalhamentoStatusSecretaria(MatriculaTO $mTO){
		return $this->bo->retornarDetalhamentoStatusSecretaria($mTO);
	}
	
	/**
	 * Retorna os tipos de documentos existentes e ativos
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDocumento() {
		$secretariaBO	= new SecretariaBO();
		$mensageiro		= $secretariaBO->retornarTipoDocumento();
		return $mensageiro;
	}
	
	public function retornarDocumentosProjetoPedagogico(DocumentosProjetoPedagogicoTO $dppTO){
		$bo = new SecretariaBO();
		return $bo->retornarDocumentosProjetoPedagogico($dppTO);
	}
	
	/**
	 * Retorna as categorias dos documentos ativas existentes
	 * @return Ead1_Mensageiro
	 */
	public function retornarDocumentoCategoria() {
		$secretariaBO			= new SecretariaBO();
		return $secretariaBO->retornaDocumentoCategoria();
	}
	
	/**
	 * Retorna as possíveis utilizações ativas para um documento
	 * @return Ead1_Mensageiro 
	 */
	public function retornarDocumentoUtilizacao() {
		$secretariaBO		= new SecretariaBO();
		return $secretariaBO->retornarDocumentoUtilizacao();
	}
	
	/**
	 * Retorna as utilizações de um documento
	 * @return Ead1_Mensageiro 
	 */
	public function retornarDocumentoUtilizacaoRelacao(DocumentosUtilizacaoRelacaoTO $durTO) {
		$secretariaBO		= new SecretariaBO();
		return $secretariaBO->retornarDocumentoUtilizacaoRelacao($durTO);
	}
	
	public function retornarDocumentoObrigatoriedade() {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->retornarDocumentoObrigatoriedade();
	}
	
	/**
	 * Retorna os documentos existentes e ativos. 
	 * @return Ead1_Mensageiro
	 */
	public function retornarDocumentoAtivo() {
		$docTO			= new DocumentosTO();
		$secretariaBO	= new SecretariaBO();
		$docTO->setBl_ativo(1);
		return $secretariaBO->retornarDocumentos($docTO);
	}
	
	/**
	 * Retorna o documento setado
	 * 
	 * @param DocumentosTO $docTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDocumento(DocumentosTO $docTO){
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->retornarDocumento($docTO);
	}
	
	/**
	 * Retorna todos os documentos com seus respectivos dados de entrega
	 * 	Projeto pedagógico e responsável por esse documento se houver
	 * 		Tudo através do id da matrícula
	 * 
	 * @param DocumentosTO $docTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDocumentoMatriculaEntrega(VwMatriculaEntregaDocumentoTO $vmedTO){
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->retornarDocumentoMatriculaEntrega($vmedTO);
	}
	
	/**
	 * Retorna os projetos pedagógicos vinculados há um documento
	 * @param int $idDocumento
	 */
	public function retornarProjetoPedagogicoDocumento( $idDocumento ) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->retornarProjetoPedagogicoDocumento( $idDocumento );
	}
	
	/**
	 * Retorna os documentos marcados como substituto de um documento
	 * @param int $idDocumento - id do documento para buscar os documentos substitutos
	 * @return Ead1_Mensageiro
	 */
	public function retornarDocumentoSubstituto( $idDocumento ){
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->retornarDocumentoSubstituto( $idDocumento );
	}
	
	/**
	 * Metodo que retorna a view de entrega de documentos
	 * @param VwEntregaDocumentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntregaDocumento(VwEntregaDocumentoTO $to){
		return $this->bo->retornarVwEntregaDocumento($to);
	}
	
	/**
	 * Cadastra um novo documento
	 * @param DocumentosTO $docTO
	 * @param array $collectionProjetoPedagogico
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDocumento(DocumentosTO  $docTO, $collectionUtilizacao,  $collectionProjetoPedagogico ) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->cadastrarDocumento($docTO, $collectionUtilizacao, $collectionProjetoPedagogico);
	}
	
	
	/**
	 * Atualiza a situação de um documento
	 * @param int $idDocumento
	 * @param int $idSituacao
	 * @return Ead1_Mensageiro
	 */
	public function atualizarSituacaoDocumento($idDocumento,$idSituacao) {
		$docTO			= new DocumentosTO();
		$secretariaBO	= new SecretariaBO();
		$docTO->setId_documentos($idDocumento);
		$docTO->setId_situacao($idSituacao);
		return $secretariaBO->editarDocumento($docTO);
	}
	
	/**
	 * Atualiza a situação de entrega de documento
	 * @param EntregaDocumentosTO $entregaDocumentosTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarSituacaoEntregaDocumento(EntregaDocumentosTO $entregaDocumentosTO) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->salvarSituacaoEntregaDocumento($entregaDocumentosTO);
	}
	
	/**
	 * Edita um documento existente
	 * @param DocumentosTo $docTO
	 * @param Array $collectionUtilizacao
	 * @param Array $collectionProjetoPedagogico
	 * @return Ead1_Mensageiro
	 */
	public function editarDocumento(DocumentosTO $docTO, $collectionUtilizacao, $collectionProjetoPedagogico) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->editarDocumento($docTO, $collectionUtilizacao, $collectionProjetoPedagogico);
	}
	
	/**
	 * Vincula um documento  como substituto para outro documento
	 * @param ìnt $idDocumentoOriginal - id do documento original
	 * @param int $idDocumentoSubstituto - id do documento a ser usado como substituo
	 * @return Ead1_Mensageiro
	 */
	public function vincularDocumentoSubstituto( $idDocumentoOriginal, $idDocumentoSubstituto ) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->vincularDocumentoSubstituto($idDocumentoOriginal,$idDocumentoSubstituto);
	}
	
	/**
	 * Vincula os projetos pedagógicos há um documento
	 * @param array $collectionProjetoPedagogico
	 * @param int $idDocumento
	 */
	public function vincularProjetoPedagogicoDocumento( $collectionProjetoPedagogico, $idDocumento ) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->vincularProjetoPedagogicoDocumento( $collectionProjetoPedagogico, $idDocumento );
	}
	
	/**
	 * Exclui um documento marcada como substituto de outro documento
	 * @param int $idDocumentoOriginal
	 * @param int $idDocumentoSubstituto
	 * @return Ead1_Mensageiro
	 */
	public function excluirDocumentoSubstituto($idDocumentoOriginal, $idDocumentoSubstituto) {
		$secretariaBO	= new SecretariaBO();
		return $secretariaBO->excluirDocumentoSubstituto($idDocumentoOriginal,$idDocumentoSubstituto);
	}
	
/**
	 * Metodo que Cadastra e Exclui as Aplicações de Documentos;
	 * @param array( DocumentosProjetoPedagogicoTO $dppTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayProjeto($arrTO){
		$bo = new SecretariaBO();
		return $bo->salvarArrayProjeto($arrTO);
	}
	
	/**
	 * Libera as salas a encerrar para o Pedagógico
	 * @param array $arrId_encerramentosala
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrEncerramentoSala_Coordenador(array $arrId_encerramentosala){
		return $this->bo->salvarArrEncerramentoSala_Coordenador($arrId_encerramentosala);
	}
	
	/**
	 * Libera as salas a encerrar para o Financeiro
	 * @param array $arrId_encerramentosala
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrEncerramentoSala_Pedagogico(array $arrId_encerramentosala){
		return $this->bo->salvarArrEncerramentoSala_Pedagogico($arrId_encerramentosala);
	}
	
	/**
	 * Encerra salas enviadas.
	 * @param array $arrId_encerramentosala
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrEncerramentoSala_Financeiro(array $arrIdsDatas){
		return $this->bo->salvarArrEncerramentoSala_Financeiro($arrIdsDatas);
	}

	/**
	 * Retorna a VwResgateAlunos com as pesquisas da tela
	 * @param ResgateAlunosTO $resgateTO, array $arrayDatas
	 * @return Ead1_Mensageiro
	 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
	 */
	public function retornarVwResgateAlunos($resgateTO, $arrayDatas){
		return $this->bo->retornarVwResgateAlunos($resgateTO, $arrayDatas);
	}
	
	/**
	 * Gera uma ocorrência relacionada com o Resgate de Alunos
	 * @param ResgateAlunosTO $resgateTO
	 * @return Ead1_Mensageiro
	 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
	 */
	public function gerarOcorrenciaResgate($resgateTO, UsuarioTO $atendente){
		return $this->bo->gerarOcorrenciaResgate($resgateTO, $atendente);
	}
	
	
	/**
	 * Rotina que altera uma matrícula a partir de uma ocorrência ou de uma matrícula
	 * @param MatriculaTO $matriculaTO
	 * @param string $st_acao
	 * @param string $st_motivo
	 * @param OcorrenciaTO $ocorrenciaTO
	 * @param array $arrdatas
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function alterarMatricula(MatriculaTO $matriculaTO, $st_acao, $st_motivo, OcorrenciaTO $ocorrenciaTO = null, array $arrdatas = null){
		return $this->bo->alterarMatricula($matriculaTO, $st_acao, $st_motivo, $ocorrenciaTO, $arrdatas);
	}

    /**
     * Método que gera o link para relatorio de resumo de interacoes de tcc no moodle
     * @param Ead1_TO_Dinamico $vw
     * @return Ead1_Mensageiro
     */
    public function gerarLinkResumoInteracoes(Ead1_TO_Dinamico $vw){
        return $this->bo->gerarLinkResumoInteracoes($vw);
    }
}
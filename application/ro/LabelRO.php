<?php
/**
 * Classe de comunicação com Flex para interações com objeto LabelRO
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class LabelRO extends Ead1_RO{
	
	private $bo;
	
	public function __construct(){
		$this->bo = new LabelBO();
	}
	
	/**
	 * Metodo que retorna as funcionalidades da Entidade com o Label
	 * @param VwLabelFuncionalidadeTO $vwLfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaLabelFuncionalidade(VwLabelFuncionalidadeTO $vwLfTO){
		return $this->bo->retornaLabelFuncionalidade($vwLfTO);
	}
	
	
	
}
<?php
/**
 * Classe de comunicação com Flex para interações com objeto Matricula
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class MatriculaRO extends Ead1_RO{
	
	/**
	 * @var MatriculaBO
	 */
	public $bo;
	private $mensageiro;
	
	public function __construct(){
		$this->bo = new MatriculaBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Metodo que verifica e ativa as matriculas do contrato do aluno.
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function ativarMatricula(ContratoTO $cTO){
		return $this->bo->ativarMatricula($cTO);
	}
	
	/**
	 * Metodo que matricula aluno diretamente
	 * @param int $idProjeto
	 * @param int $idUsuario
	 * @return Ead1_Mensageiro
	 */
	public function matricularDireto($idProjeto, $idUsuario){
		
		$matTO = new MatriculaTO();
		$matTO->setId_projetopedagogico($idProjeto);
		$matTO->setId_usuario($idUsuario);
		
		return $this->bo->matricularDireto($matTO);
	}
	
	
	public function certificarMatricula( MatriculaTO $matriculaTO ) {
		return $this->bo->certificarMatricula( $matriculaTO );
	}
	
	/**
	 * Transfere aluno de curso.
	 * @param MatriculaTO $matriculaTO
	 * @param VwUsuarioMatriculaProjetoTO $matriculaOrigemTO
	 * @param TramiteTO $tramiteTO
	 * @return Ead1_Mensageiro
	 */
	public function transferirAluno(MatriculaTO $matriculaTO, VwUsuarioMatriculaProjetoTO $matriculaOrigemTO, TramiteTO $tramiteTO){
		return $this->bo->transferirAluno($matriculaTO, $matriculaOrigemTO, $tramiteTO);	
	}
	
	/**
	 * Metodo que verifica e confirma o recebimento do contrato do aluno.
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function confirmarRecebimento(ContratoTO $cTO){
			return $this->bo->confirmarRecebimento($cTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a Matricula
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarMatricula(MatriculaTO $mTO){
		if($mTO->getId_matricula()){
			return $this->editarMatricula($mTO);
		}
			return $this->cadastrarMatricula($mTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita Aproveitamento de disciplina
	 * @param AproveitamentoDisciplinaTO $adTO
	 * @param array $arrMatriculaDisciplinaTO
	 * @param array $arrAproveitamentoMatriculaDisciplina
	 * @return Ead1_Mensageiro
	 */
	public function salvarAproveitamento(AproveitamentoDisciplinaTO $adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplina){
		if($adTO->getId_aproveitamentodisciplina()){
			return $this->editarAproveitamento($adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplina);
		}else{
			return $this->cadastrarAproveitamento($adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplina);
		}
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContrato(ContratoTO $cTO){
		if($cTO->getId_contrato()){
			return $this->editarContrato($cTO);
		}
			return $this->cadastrarContrato($cTO);
	}
	
	/**
	 * Gera o contrato de um aluno com seus responsáveis financeiros, o reponsável pedagógico e suas matrículas
	 * É gerado uma venda, um contrato e uma lista matrículas vinculadas aquele contrato
	 * @param ContratoTO $contratoTO 
	 * @param array $arContratoResponsavelFinanceiroTO
	 * @param ContratoResponsavelTO $contratoResponsavelPedagogico
	 * @param array $arMatriculaTO
	 * @param array $arLancamentoTO
	 * @param ContratoRegraTO $contratoRegraTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoMatricula( $contratoTO, $arContratoResponsavelFinanceiroTO, $contratoResponsavelPedagogico, $arMatriculaTO, $arLancamentoTO, $contratoRegraTO ) {
		$matriculaBO	= new MatriculaBO();
		return $matriculaBO->salvarContratoMatricula( $contratoTO, $arContratoResponsavelFinanceiroTO, $contratoResponsavelPedagogico, $arMatriculaTO, $arLancamentoTO, $contratoRegraTO );
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o Convenio
	 * @param ContenioTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarConvenio(ConvenioTO $cTO){
		if($cTO->getId_convenio()){
			return $this->editarConvenio($cTO);
		}
			return $this->cadastrarConvenio($cTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o contrato do Responsavel
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoResponsavel(ContratoResponsavelTO $crTO){
		if($crTO->getId_contratoresponsavel()){
			return $this->editarContratoResponsavel($crTO);
		}
			return $this->cadastrarContratoResponsanvel($crTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o contrato do Envolvido
	 * @param ContratoEnvolvidoTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoEnvolvido(ContratoEnvolvidoTO $ceTO){
			return $this->bo->salvarContratoEnvolvido($ceTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita uma matricula, venda e vendaproduto.
	 * OBS: Este metodo cadastra/edita a venda e sobrescreve(edita/cadastra) a vendaproduto e matricula
	 * @param MatriculaTO $mTO
	 * @param VendaTO $vTO
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarMatriculaVendaProduto(MatriculaTO $mTO,VendaTO $vTO, VendaProdutoTO $vpTO){
		return $this->bo->salvarMatriculaVendaProduto($mTO, $vTO, $vpTO);
	}
	
	/**
	 * Metodo que cadastra a venda e vendaproduto e seta no contrato e na matricula
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoMatriculaVendaProduto(ContratoTO $cTO){
		return $this->bo->salvarContratoMatriculaVendaProduto($cTO);
	}
		
	/**
	 * Metodo que salva os responsaveis do contrato
	 * Salva varios Responsaveis do tipo Financeiro e um unico do tipo Pedagogico
	 * Parametro 1: Array de ContratoResponsavelTO do tipo Financeiro
	 * Parametro 2: ContratoResponsavel do tipo Pedagogico
	 * @param array $arrTO
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayContratoResponsavel($arrTO, ContratoResponsavelTO $crTO){
		return $this->bo->salvarArrayContratoResponsavel($arrTO,$crTO);
	}
	
	/**
	 * Metodo que cadastra o vinculo do contrato com o conveio
	 * @param ContratoConvenioTO $to
	 * @param $soExcluir Exlui apenas todos os convenios do contrato
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoConvenio(ContratoConvenioTO $to,$soExcluir = false){
		return $this->bo->salvarContratoConvenio($to,$soExcluir);
	}
	
	/**
	 * Metodo que cadastra Matricula
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMatricula(MatriculaTO $mTO){
		return $this->bo->cadastrarMatricula($mTO);
	}
	
	/**
	 * Metodo que cadastra o aproveitamento das disciplinas já cursadas em outra matricula do aluno
	 * @param Array $arrVwAproveitamentoDisciplinaInternoTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAproveitamentoDisciplinasInterno($arrVwAproveitamentoDisciplinaInternoTO){
		return $this->bo->cadastrarAproveitamentoDisciplinasInterno($arrVwAproveitamentoDisciplinaInternoTO);
	}
	
	/**
	 * Metodo que cadastra Registro de Matricula
	 * @param LivroRegistroTO $lrTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarRegistro(LivroRegistroTO $lrTO){
		return $this->bo->cadastrarRegistro($lrTO);
	}
	
	/**
	 * Metodo que cadastra Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContrato(ContratoTO $cTO){
		return $this->bo->cadastrarContrato($cTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o Contrato Regra
	 * @param ContratoRegraTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoRegra(ContratoRegraTO $crTO){
		if($crTO->getId_contratoregra()){
			return $this->editarContratoRegra($crTO);
		}
		return $this->cadastrarContratoRegra($crTO);
	}
	
	/**
	 * Metodo que decide se cadastra o Contrato Regra
	 * @param ContratoRegraTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoRegra(ContratoRegraTO $crTO){
		return $this->bo->cadastrarContratoRegra($crTO);
	}
	
	/**
	 * Metodo que cadastra ContratoMatricula
	 * @param ContratoMatriculaTO $cmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoMatricula(ContratoMatriculaTO $cmTO){
		return $this->bo->cadastrarContratoMatricula($cmTO);
	}
	
	/**
	 * Metodo que cadastra ContratoConvenio
	 * @param ContratoConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoConvenio(ContratoConvenioTO $ccTO){
		return $this->bo->cadastrarContratoConvenio($ccTO);
	}
	
	/**
	 * Metodo que cadastra Convenio
	 * @param ConvenioTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarConvenio(ConvenioTO $cTO){
		return $this->bo->cadastrarConvenio($cTO);
	}
	
	/**
	 * Metodo que cadastra Aproveitamento
	 * @param AproveitamentoDisciplinaTO $adTO
	 * @param array $arrMatriculaDisciplinaTO
	 * @param array $arrAproveitamentoMatriculaDisciplinaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAproveitamento(AproveitamentoDisciplinaTO $adTO, $arrMatriculaDisciplinaTO , $arrAproveitamentoMatriculaDisciplinaTO){
		return $this->bo->cadastrarAproveitamento($adTO, $arrMatriculaDisciplinaTO, $arrAproveitamentoMatriculaDisciplinaTO);
	}
	
	/**
	 * Metodo que cadastra Contrato responsavel
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoResponsanvel(ContratoResponsavelTO $crTO){
		return $this->bo->cadastrarContratoResposanvel($crTO);
	}
	
	/**
	 * Metodo que cadastra ContratoEnvolvido
	 * @param ContratoEnvolvidoTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoEnvolvido(ContratoEnvolvidoTO $ceTO){
		return $this->bo->cadastrarContratoEnvolvido($ceTO);
	}
	
	/**
	 * Metodo que Edita Matricula
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function editarMatricula(MatriculaTO $mTO){
		return $this->bo->editarMatricula($mTO);
	}
	
	
	/**
	 * Metodo que Edita Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContrato(ContratoTO $cTO){
		return $this->bo->editarContrato($cTO);
	}
	
	/**
	 * Metodo que Edita Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoRegra(ContratoRegraTO $crTO){
		return $this->bo->editarContratoRegra($crTO);
	}
	
	/**
	 * Metodo que Edita ContratoMatricula
	 * @param ContratoMatriculaTO $cmTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoMatricula(ContratoMatriculaTO $cmTO){
		return $this->bo->editarContratoMatricula($cmTO);
	}
	
	/**
	 * Metodo que Edita ContratoConvenio
	 * @param ContratoConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoConvenio(ContratoConvenioTO $ccTO){
		return $this->bo->editarContratoConvenio($ccTO);
	}
	
	/**
	 * Metodo que Edita Convenio
	 * @param ConvenioTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function editarConvenio(ConvenioTO $cTO){
		return $this->bo->editarConvenio($cTO);
	}
	
	/**
	 * Metodo que Edita Contrato Responsavel
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoResponsavel(ContratoResponsavelTO $crTO){
		return $this->bo->editarContratoResponsavel($crTO);
	}
	
	/**
	 * Metodo que Edita Contrato Responsavel
	 * @param AproveitamentoDisciplinaTO $adTO
	 * @param array $arrMatriculaDisciplina
	 * @param array $arrAproveitamentoMatriculaDisciplina
	 * @return Ead1_Mensageiro
	 */
	public function editarAproveitamento(AproveitamentoDisciplinaTO $adTO, $arrMatriculaDisciplina, $arrAproveitamentoMatriculaDisciplina){
		return $this->bo->editarAproveitamento($adTO, $arrMatriculaDisciplina, $arrAproveitamentoMatriculaDisciplina);
	}
	
	/**
	 * Metodo que Edita ContratoEnvolvido
	 * @param ContratoEnvolvidoTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoEnvolvido(ContratoEnvolvidoTO $ceTO){
		return $this->bo->editarContratoEnvolvido($ceTO);
	}
	
	/**
	 * Metodo que exclui a Matricula
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarMatricula(MatriculaTO $mTO){
		return $this->bo->deletarMatricula($mTO);
	}
	
	/**
	 * Metodo que verifica se existem vinculos com o contrato e exclui
	 * @param ContratoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function verificaExcluiContrato(ContratoTO $to){
		return $this->bo->verificaExcluiContrato($to);
	}
	
	/**
	 * Metodo que Exclui Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarContrato(ContratoTO $cTO){
		return $this->bo->deletarContrato($cTO);
	}
	
	/**
	 * Metodo que Exclui ContratoMatricula
	 * @param ContratoMatriculaTO $cmTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarContratoMatricula(ContratoMatriculaTO $cmTO){
		return $this->bo->deletarContratoMatricula($cmTO);
	}
	
	/**
	 * Metodo que Exclui ContratoConvenio
	 * @param ContratoConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarContratoConvenio(ContratoConvenioTO $ccTO){
		return $this->bo->deletarContratoConvenio($ccTO);
	}
	
	/**
	 * Metodo que Exclui Convenio
	 * @param ConvenioTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarConvenio(ConvenioTO $cTO){
		return $this->bo->deletarConvenio($cTO);
	}
	
	/**
	 * Metodo que Exclui Contrato Responsavel
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarContratoResponsavel(ContratoResponsavelTO $crTO){
		return $this->bo->deletarContratoResponsavel($crTO);
	}
	
	/**
	 * Metodo que Exclui ContratoEnvolvido
	 * @param ContratoEnvolvidoTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarContratoEnvolvido(ContratoEnvolvidoTO $ceTO){
		return $this->bo->deletarContratoEnvolvido($ceTO);
	}
	
	/**
	 * Método que exclui a disciplina aproveitada
	 * @param VwAproveitamentoTO $vwAproveitamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDisciplinaAproveitada(VwAproveitamentoTO $vwAproveitamentoTO){
		return $this->bo->deletarDisciplinaAproveitada($vwAproveitamentoTO);
	}
	
	/**
	 * Metodo que retorna a Matricula
	 * @param MatriculaTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMatricula(MatriculaTO $mTO){
		return $this->bo->retornarMatricula($mTO);
	}
	
	/**
	 * Metodo que retorna os projetos vinculados as Matricula do Usuário
	 * @param VwUsuarioMatriculaProjetoTO $mTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMatriculaUsuarioProjeto(VwUsuarioMatriculaProjetoTO $mTO){
		return $this->bo->retornarMatriculaUsuarioProjeto($mTO);
	}
	
	/**
	 * Metodo que retorna o Contrato
	 * @param ContratoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContrato(ContratoTO $cTO){
		return $this->bo->retornarContrato($cTO);
	}
	
	/**
	 * Metodo que retorna Aproveitamento de disciplina.
	 * @param AproveitamentoDisciplina $adTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAproveitamento(AproveitamentoDisciplinaTO $adTO){
		return $this->bo->retornarAproveitamento($adTO);
	}
	
	/**
	 * Metodo que retorna Aproveitamento de disciplina.
	 * @param AproveitamentoMatriculaDisciplina $amdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDisciplinasAproveitadas(AproveitamentoMatriculaDisciplinaTO $amdTO){
		return $this->bo->retornarDisciplinaAproveitada($amdTO);
	}
	
	/**
	 * Metodo que retorna a view de matrícula.
	 * @param VwMatriculaTO $vmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMatricula(VwMatriculaTO $vmTO){
		return $this->bo->retornarVwMatricula($vmTO);
	} 
	
	/**
	 * Metodo que retorna a view de grade e sala do aluno.
	 * @param VwMatriculaGradeSalaTO $vmgsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMatriculaGradeSala(VwMatriculaGradeSalaTO $vmgsTO){
		return $this->bo->retornarVwMatriculaGradeSala($vmgsTO);
	} 
	
	/**
	 * Metodo que retorna a view de grade e sala do aluno por tipo.
	 * @param VwGradeTipoTO $vwGradeTipoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwGradeTipo(VwGradeTipoTO $vwGradeTipoTO){
		return $this->bo->retornarVwGradeTipo($vwGradeTipoTO);
	} 
	
	/**
	 * Metodo que retorna o ContratoMatricula
	 * @param ContratoMatriculaTO $cmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoMatricula(ContratoMatriculaTO $cmTO){
		return $this->bo->retornarContratoMatricula($cmTO);
	}
	
	/**
	 * Metodo que retorna o ContratoConvenio
	 * @param ContratoConvenioTO $cmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoConvenio(ContratoConvenioTO $ccTO){
		return $this->bo->retornarContratoConvenio($ccTO);
	}
	
	/**
	 * Metodo que retorna o Convenio
	 * @param ConvenioTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarConvenio(ConvenioTO $cTO){
		return $this->bo->retornarConvenio($cTO);
	}
	
	/**
	 * Metodo que retorna o ContratoResponsavel
	 * @param ContratoResponsavelTO $crTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoResponsavel(ContratoResponsavelTO $crTO){
		return $this->bo->retornarContratoResponsavel($crTO);
	}
	
	/**
	 * Metodo que retorna o ContratoEnvolvido
	 * @param ContratoEnvolvidoTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoEnvolvido(ContratoEnvolvidoTO $ceTO){
		return $this->bo->retornarContratoEnvolvido($ceTO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Contrato do Responsavel
	 * @param TipoContratoResponsavelTO $tcrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoContratoResponsavel(TipoContratoResponsavelTO $tcrTO){
		return $this->bo->retornarTipoContratoResponsavel($tcrTO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Envolvido
	 * @param TipoEnvolvidoTO $teTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoEnvolvido(TipoEnvolvidoTO $teTO){
		return $this->bo->retornarTipoEnvolvido($teTO);
	}
	
	/**
	 * Metodo que retorna Evolucao
	 * @param EvolucaoTO $eTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEvolucao(EvolucaoTO $eTO){
		return $this->bo->retornarEvolucao($eTO);
	}
	
	/**
	 * Metodo que retorna a view de usuario com contrato responsavel
	 * @param VwUsuarioContratoResponsavelTO $vwUcrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwUsuarioContratoResponsavel(VwUsuarioContratoResponsavelTO $vwUcrTO){
		return $this->bo->retornarVwUsuarioContratoResponsavel($vwUcrTO);
	}
	
	/**
	 * Metodo que retorna a view de entidade com contrato responsavel
	 * @param VwEntidadeContratoResponsavelTO $vwTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntidadeContratoResponsavel(VwEntidadeContratoResponsavelTO $vwTO){
		return $this->bo->retornarVwEntidadeContratoResponsavel($vwTO);
	}
	
	/**
	 * Metodo que retorna responsaveis para contrato
	 * @param int $id_contrato
	 * @param int $id_venda
	 * @param int $id_tipocontratoresponsavel
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoResponsaveis($id_venda, $id_tipocontratoresponsavel, $id_contrato = 0){
		return $this->bo->retornarContratoResponsaveis($id_venda, $id_tipocontratoresponsavel, $id_contrato);
	}
	
	/**
	 * Metodo que retorna a view de disciplinas para aproveitamento
	 * @param VwMatriculaDisciplinaTO $vwmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMatriculaDisciplina(VwMatriculaDisciplinaTO $vwmdTO){
		return $this->bo->retornarVwMatriculaDisciplina($vwmdTO);
	}
	
	/**
	 * Metodo que retorna a view de disciplinas para aproveitamento por serie
	 * @param VwMatriculaDisciplinaTO $vwmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMatriculaDisciplinaSerie(VwMatriculaDisciplinaTO $vwmdTO){
		return $this->bo->retornarVwMatriculaDisciplinaSerie($vwmdTO);
	}
	
	/**
	 * Metodo que retorna a view de disciplinas para aproveitamento por area
	 * @param VwMatriculaDisciplinaTO $vwmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMatriculaDisciplinaArea(VwMatriculaDisciplinaTO $vwmdTO){

		return $this->bo->retornarVwMatriculaDisciplinaArea($vwmdTO);
	}
	
	/**
	 * Retorna as regras de contrato disponíveis e ativas
	 * @param $contratoRegraTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoRegra( ContratoRegraTO $contratoRegraTO ) {

        if (!$contratoRegraTO->getId_entidade()) {
            $contratoRegraTO->setId_entidade($contratoRegraTO->getSessao()->id_entidade);
        }
		return $this->bo->retornarContratoRegra( $contratoRegraTO );
	}
	
	/**
	 * 
	 * View com os produtos de um contrato
	 * @param VwProdutoContratoTO $contratoProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProdutoContrato(VwProdutoContratoTO $contratoProdutoTO ){
		return $this->bo->retornarVwProdutoContrato( $contratoProdutoTO );
	}
	
	/**
	 * Metodo que retorna a Pesquisa dos Alunos Concluintes
	 * @param PesquisarAlunoTO $paTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAlunosConluintes(PesquisarAlunoTO $paTO){
		return $this->bo->retornarAlunosConluintes($paTO);
	}
	
	/**
	 * @param LivroRegistroTO $livroRegistroTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroRegistro( LivroRegistroTO $livroRegistroTO ){
		return $this->bo->retornarLivroRegistro( $livroRegistroTO );
	}
	
    /**
     * Metodo que retorna as disciplinas que podem ser aproveitadas pelo aluno
     * @param VwAproveitamentoDisciplinaInternoTO $vwAdiTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitamentoDisciplinaInterno(VwAproveitamentoDisciplinaInternoTO $vwAdiTO){
		return $this->bo->retornarVwAproveitamentoDisciplinaInterno($vwAdiTO);
    }
	
    /**
     * Metodo que retorna as disciplinas já aproveitadas pelo aluno
     * @param VwAproveitamentoTO $vwaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitamentoDisciplina(VwAproveitamentoTO $vwaTO){
		return $this->bo->retornarVwAproveitamento($vwaTO);
    }
	
    /**
     * Metodo que retorna as notas a serem aproveitadas.
     * @param VwAproveitarAvaliacaoTO $vwaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAproveitarAvaliacao(VwAproveitarAvaliacaoTO $vwaTO){
		return $this->bo->retornarVwAproveitarAvaliacao($vwaTO);
    }
    
    /**
     * Metodo que retorna as series e niveis do produto da venda
     * @param VendaProdutoNivelSerieTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVendaProdutoNivelSerie(VendaProdutoNivelSerieTO $to){
		return $this->bo->retornarVendaProdutoNivelSerie($to);
    }
    
	/**
	 * Metodo que retorna as matriculas que tenham relação com a entidade da sessão
	 * @param VwMatriculaTO $vmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMatriculaRelacaoEntidadeSessao(VwMatriculaTO $vmTO){
		return $this->bo->retornarMatriculaRelacaoEntidadeSessao($vmTO);
    }
    
     /**
     * Metodo que envia email com o link de envio de boleto.
     * @param VwResumoFinanceiroTO $vwResumoFinanceiroTO
     * @return Ead1_Mensageiro
     */
    public function enviarEmailBoleto(VwResumoFinanceiroTO $vwResumoFinanceiroTO){
    	return $this->bo->enviarEmailBoleto($vwResumoFinanceiroTO);
    }
    

    /**
     * Metodo que envia email com o link de pagamento via cartão de crédito
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param VendaTO $to
     * @return Ead1_Mensageiro
     */
    public function enviarEmailCartao(VendaTO $to){
    	return $this->bo->enviarEmailCartao($to);
    }
    
    
    public function enviarArrayMensagemAtivacaoMatricula(VwMatriculaTO $to){
    	return $this->bo->enviarArrayMensagemAtivacaoMatricula($to);
    }
    
    /**
     * Metodo que retorna responsaveis para contrato - Apenas um array com fisico + juridico
     * @param int $id_contrato
     * @param int $id_venda
     * @param int $id_tipocontratoresponsavel
     * @return Ead1_Mensageiro
     */
    public function retornarContratoResponsaveisArrayUnico($id_venda, $id_tipocontratoresponsavel, $id_contrato = 0){
    	return $this->bo->retornarContratoResponsaveisArrayUnico($id_venda, $id_tipocontratoresponsavel, $id_contrato);
    }
    
}
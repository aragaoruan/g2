<?php

/**
 * Classe de comunicação com Flex para interações com objeto MensagemCobranca
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package application
 * @subpackage ro
 */
class MensagemCobrancaRO extends Ead1_RO {
	
	private $bo;
	
	public function __construct(){
		$this->bo = new MensagemCobrancaBO();
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a MensagemCobranca
	 * @param MensagemCobrancaTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarMensagemCobranca(MensagemCobrancaTO $aTO){
		return $this->bo->salvarMensagemCobranca($aTO);
	}
	
	
	/**
	 * Metodo que cadastra a MensagemCobranca
	 * @param MensagemCobrancaTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMensagemCobranca(MensagemCobrancaTO $aTO){
		return $this->bo->cadastrarMensagemCobranca($aTO);
	}
	
	
	/**
	 * Metodo que edita a MensagemCobranca
	 * @param MensagemCobrancaTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function editarMensagemCobranca(MensagemCobrancaTO $aTO){
		return $this->bo->editarMensagemCobranca($aTO);
	}
	
	
	/**
	 * Metodo que retorna MensagemCobranca
	 * @param MensagemCobrancaTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function listarMensagemCobranca(Ead1_TO $aTO){
		
		if($aTO instanceof MensagemCobrancaTO){
			$to = new VwMensagemCobrancaTO($aTO->toArray()); // dando erro no Flex, tive que mudar
		} else {
			$to = $aTO;
		}
		return $this->bo->listarMensagemCobranca($to);
	}
	

}
<?php
/**
 * Classe de comunicação com Flex para interações com objeto Importacao
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 */
class ImportacaoRO extends Ead1_RO{
	
	/**
	 * Metodo Construtor
	 */
	public function __construct(){
		$this->bo = new ImportacaoBO();
	}
	
	/**
	 * Variavel que contem a instancia do BO de Importação
	 * @var ImportacaoBO
	 */
	private $bo;
	
	/**
	 * Importar os dados
	 * @param ImportacaoTO $iTO
	 * @return Ead1_Mensageiro
	 */
	public function importarDados(ImportacaoTO $iTO){
		return $this->bo->importarDados($iTO);
	}
	
	/**
	 * Metodo que edita uma pessoa na tabela de importacao
	 * @param PessoaImportacaoTO $piTO
	 * @return Ead1_Mensageiro
	 */
	public function editarPessoaImportacao(PessoaImportacaoTO $piTO){
		return $this->bo->editarPessoaImportacao($piTO);
	}
	
	/**
	 * Metodo que retorna os estados civis de importação
	 * @param EstadocivilImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEstadoCivilImportacao(EstadocivilImportacaoTO $to){
		return $this->bo->retornarEstadoCivilImportacao($to);
	}
	
	/**
	 * Metodo que retorna as etnias de importação
	 * @param EtniaImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEtniaImportacao(EtniaImportacaoTO $to){
		return $this->bo->retornarEtniaImportacao($to);
	}
	
	/**
	 * Metodo que retorna os sitemas da importacao
	 * @param SistemaImportacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarSistemaImportacao(SistemaImportacaoTO $to){
		return $this->bo->retornarSistemaImportacao($to);
	}
	
	public function retornarRegistrosRestantesImportacao() {
		
	}
	
	/**
	 * Realiza o processo de importação completo. 
	 * @param bool $blImportarMatricula
	 * @param bool $blImportarAvaliacao
	 * @param bool $blImportarAproveitamento
	 * @return Ead1_Mensageiro
	 */
	public function processarImportacao( $blImportarMatricula, $blImportarAvaliacao, $blImportarAproveitamento ){
		$importacaoBO	= new ImportacaoBO();
		return  $importacaoBO->processarImportacao( $blImportarMatricula, $blImportarAvaliacao, $blImportarAproveitamento );
	}
}
<?php

/**
 * Classe de comunicação com Flex para interações com objeto Entidade
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class EntidadeRO extends Ead1_RO
{

    private $bo;

    public function __construct ()
    {
        $this->bo = new EntidadeBO();
    }

    /**
     * Metodo que decide se Cadastra ou Edita Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidade (EntidadeTO $entTO)
    {
        $entTO->reloadTO();
        if ($entTO->getId_entidade()) {
            return $this->editarEntidade($entTO);
        }
        return $this->cadastrarEntidade($entTO);
    }

    /**
     * Metodo que decide se Cadastra ou Edita Endereço da Entidade
     * @param $entTO
     * @param $endTO
     * @param $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEnderecoEntidade (EntidadeTO $entTO, EnderecoTO $endTO, EntidadeEnderecoTO $eeTO)
    {
        if ($endTO->getId_endereco()) {
            return $this->editarEnderecoEntidade($endTO, $eeTO);
        }
        return $this->cadastrarEnderecoEntidade($entTO, $endTO, $eeTO);
    }

    /**
     * Metodo que Salva o Email da Instituição
     * @param EntidadeEmailTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEmail (EntidadeEmailTO $eeTO)
    {
        return $this->bo->salvarEntidadeEmail($eeTO);
    }

    /**
     * Metodo que decide se Cadastra ou Edita Telefones da Entidade
     * @param EntidadeTO $entTO
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function salvarContatoTelefoneEntidade (EntidadeTO $entTO, ContatosTelefoneTO $ctTO)
    {
        if ($ctTO->getId_telefone()) {
            return $this->editarTelefoneEntidade($ctTO);
        }
        return $this->cadastrarTelefoneEntidade($entTO, $ctTO);
    }

    /**
     * Metodo que decide se Cadastra ou  Edita Contato da Entidade
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContatoEntidade (ContatoEntidadeTO $ceTO)
    {
        if ($ceTO->getId_contatoentidade()) {
            return $this->editarContatoEntidade($ceTO);
        }
        return $this->cadastrarContatoEntidade($ceTO);
    }

    /**
     * Metodo que decide se cadastra ou edita Conta Bancaria da Entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContaEntidade (ContaEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->salvarContaEntidade($ceTO);
    }

    /**
     * Metodo que salva o endereco padrao da entidade
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEnderecoPadrao (EntidadeEnderecoTO $eeTO)
    {
        $bo = new EntidadeBO();
        return $bo->salvarEntidadeEnderecoPadrao($eeTO);
    }

    /**
     * Metodo que salva a conta padrao da entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContaEntidadePadrao (ContaEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->salvarContaEntidadePadrao($ceTO);
    }

    /**
     * Metodo que salva o endereco de uma entidade (Não retorna TO) e vincula a entidade
     * @param EnderecoTO $eTO
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeEndereco (EnderecoTO $eTO, EntidadeEnderecoTO $eeTO)
    {

        $bo = new EntidadeBO();
        $arrEnderecoTO = $eTO->toArray();
        unset($arrEnderecoTO['bl_padrao']);
        $eTO = new EnderecoTO();
        $eTO->montaToDinamico($arrEnderecoTO);
        return $bo->salvarEntidadeEndereco($eTO, $eeTO);
    }

    /**
     * Metodo que salva o relação de entidade.
     * @param EntidadeRelacaoTO $eTO
     * @param array(ProjetoPedagogicoTO $arrProjetos)
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeRelacao (EntidadeRelacaoTO $erTO, $arrProjetos)
    {
        if ($erTO->getId_entidadepai()) {
            return $this->editarEntidadeRelacao($erTO, $arrProjetos);
        }
        return $this->cadastrarEntidadeRelacao($erTO, $arrProjetos);
    }

    /**
     * Metodo que decide se cadastra ou edita Relação de entidade com telefone
     * TODO implementar o método de cadastrar
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function salvarContatosTelefoneEntidade (ContatosTelefoneEntidadeTO $cteTO)
    {
        $bo = new EntidadeBO();
        if ($cteTO->getId_entidade()) {
            return $bo->editarContatosTelefoneEntidade($cteTO);
        }
    }

    /**
     * Metodo que salvar um array de funcionalidades da entidade alterando o bl visivel
     * @param Array $arrEntidadeFuncionalidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrEntidadeFuncionalidadeUsuario ($arrEntidadeFuncionalidadeTO)
    {
        $bo = new EntidadeBO();
        return $bo->salvarArrEntidadeFuncionalidadeUsuario($arrEntidadeFuncionalidadeTO);
    }

    /**
     * Metodo que salvar um array de funcionalidades da entidade alterando o bl ativo
     * @param Array $arrEntidadeFuncionalidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrEntidadeFuncionalidadeSuperUsuario ($arrEntidadeFuncionalidadeTO)
    {
        $bo = new EntidadeBO();
        return $bo->salvarArrEntidadeFuncionalidadeSuperUsuario($arrEntidadeFuncionalidadeTO);
    }

    /**
     * Procedimento que salva dados da entidade no cadastro resumido.
     * @param EntidadeTO $entidadeTO
     * @param ContatosTelefoneTO $contatosTelefoneTO
     * @param ContatosTelefoneEntidadeTO $contatosTelefoneEntidadeTO
     * @param ContaEntidadeTO $contaEntidadeTO
     */
    public function procedimentoSalvarEntidadeResumido (EntidadeTO $entidadeTO, ContatosTelefoneTO $contatosTelefoneTO, ContatosTelefoneEntidadeTO $contatosTelefoneEntidadeTO, EntidadeEnderecoTO $entidadeEnderecoTO, EnderecoTO $enderecoTO, ContaEntidadeTO $contaEntidadeTO)
    {
        try {
            return $this->bo->procedimentoSalvarEntidadeResumido($entidadeTO, $contatosTelefoneTO, $contatosTelefoneEntidadeTO, $entidadeEnderecoTO, $enderecoTO, $contaEntidadeTO);
        } catch (Zend_Exception $e) {
            return $this->bo->mensageiro;
        }
    }

    /**
     * Metodo que Cadastra Entidade
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidade (EntidadeTO $eTO)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarEntidade($eTO);
    }

    /**
     * Metodo que cadastra Endereço da Entidade
     * @param $entTO
     * @param $endTO
     * @param $eeTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEnderecoEntidade (EntidadeTO $entTO, EnderecoTO $endTO, EntidadeEnderecoTO $eeTO)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarEnderecoEntidade($entTO, $endTO, $eeTO);
    }

    /**
     * Metodo que cadastra Telefones da Entidade
     * @param EntidadeTO $entTO
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTelefoneEntidade (EntidadeTO $entTO, ContatosTelefoneTO $ctTO)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarTelefoneEntidade($entTO, $ctTO);
    }

    /**
     * Metodo que cadastra Contatos na Entidade
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContatoEntidade (ContatoEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarContatoEntidade($ceTO);
    }

    /**
     * Metodo que cadastra Telefones do Contato da Entidade
     * @param ContatoEntidadeTO $ceTO
     * @param ContatosTelefoneEntidadeContatoTO $ctecTO
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTelefoneContato (ContatoEntidadeTO $ceTO, ContatosTelefoneEntidadeContatoTO $ctecTO, ContatosTelefoneTO $ctTO)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarTelefoneContato($ceTO, $ctecTO, $ctTO);
    }

    /**
     * Metodo que cadastra Conta Bancaria da Entidade
     * @param EntidadeTO $eTO
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContaEntidade (EntidadeTO $eTO, ContaEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        $ceTO->reloadTO();
        return $bo->cadastrarContaEntidade($eTO, $ceTO);
    }

    /**
     * Metodo que cadastra Relação de entidade.
     * @param EntidadeRelacaoTO $erTO
     * @param array(ProjetoPedagogicoTO $arrProjetos)
     * @return Ead1_Mensageiro
     */
    public function cadastrarEntidadeRelacao (EntidadeRelacaoTO $erTO, $arrProjetos)
    {
        $bo = new EntidadeBO();
        return $bo->cadastrarEntidadeRelacao($erTO, $arrProjetos);
    }

    /**
     * Metodo que edita dados basicos da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function editarEntidade (EntidadeTO $entTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarEntidade($entTO);
    }

    /**
     * Metodo que edita dados basicos de relação da Entidade
     * @param EntidadeRelacaoTO $erTO
     * @param array(ProjetoPedagogicoTO $arrProjetos)
     * @return Ead1_Mensageiro
     */
    public function editarEntidadeRelacao (EntidadeRelacaoTO $erTO, $arrProjetos)
    {
        $bo = new EntidadeBO();
        return $bo->editarEntidadeRelacao($erTO, $arrProjetos);
    }

    /**
     * Metodo que edita Endereço da Entidade
     * @param EnderecoTO $endTO
     * @param EntidadeEnderecoTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function editarEnderecoEntidade (EnderecoTO $endTO, EntidadeEnderecoTO $eeTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarEnderecoEntidade($endTO, $eeTO);
    }

    /**
     * Metodo que edita Telefones da Entidade
     * @param  ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefoneEntidade (ContatosTelefoneTO $ctTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarTelefoneEntidade($ctTO);
    }

    /**
     * Metodo que Edita Contatos da Entidade
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function editarContatoEntidade (ContatoEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarContatoEntidade($ceTO);
    }

    /**
     * Metodo que edita Telefones do Contato da Entidade
     * @param ContatosTelefoneEntidadeContatoTO $ctecTO
     * @param ContatosTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefoneContato (ContatosTelefoneEntidadeContatoTO $ctecTO, ContatosTelefoneTO $ctTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarTelefoneContato($ctecTO, $ctTO);
    }

    /**
     * Metodo que edita Conta Bancaria da Entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function editarContaEntidade (ContaEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->editarContaEntidade($ceTO);
    }

    /**
     * Metodo que retorna Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaEntidade (EntidadeTO $entTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaEntidade($entTO);
    }

    /**
     * Metodo que retorna a Classe da Entidade
     * @param EntidadeRelacaoTO $erTO
     * @param boolean $entidadeSessao
     * @return Ead1_Mensageiro
     */
    public function retornaClasseDaEntidade (EntidadeRelacaoTO $erTO, $entidadeSessao = false)
    {
        $bo = new EntidadeBO();
        return $bo->retornaClasseDaEntidade($erTO, $entidadeSessao);
    }

    /**
     * Retorna dados da Entidade
     * @param EntidadeTO $entidadeTO
     * @return Ead1_Mensageiro
     */
    public function retornarDadosEntidade (EntidadeTO $entidadeTO)
    {
        return $this->bo->retornarDadosEntidade($entidadeTO);
    }

    /**
     * Metodo que retorna a projetos da entidade
     * @param ProjetoEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoEntidade (ProjetoEntidadeTO $peTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornarProjetoEntidade($peTO);
    }

    /**
     * Metodo que retorna classes de entidade.
     * @param EntidadeClasseTO $ecTO
     * @return Ead1_Mensageiro
     */
    public function retornaClasses (EntidadeClasseTO $ecTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaClasses($ecTO);
    }

    /**
     * Metodo que retorna Endereço da Entidade
     * @param Entidade $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaEnderecoEntidade (EntidadeTO $entTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaEnderecoEntidade($entTO);
    }

    /**
     * Metodo que retorna telefones da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaTelefoneEntidade (EntidadeTO $entTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaTelefoneEntidade($entTO);
    }

    /**
     * Metodo que retorna as funcionalidades da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeFuncionalidade (EntidadeFuncionalidadeTO $efTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornarEntidadeFuncionalidade($efTO);
    }

    /**
     * Metodo que retorna todas as funcionalidades da entidade independente do bl_ativo ou visivel, retorna pelo id da entidade
     * @param EntidadeFuncionalidadeTO $efTO
     * @return Ead1_Mensageiro
     */
    public function retornarTodasEntidadeFuncionalidades (EntidadeFuncionalidadeTO $efTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornarTodasEntidadeFuncionalidades($efTO);
    }

    /**
     * Metodo que retorna a view de telefones da Entidade
     * @param VwEntidadeTelefoneTO $vwetTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeTelefone (VwEntidadeTelefoneTO $vwetTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornarVwEntidadeTelefone($vwetTO);
    }

    /**
     * Metodo que Retorna Contatos da Entidade
     * @param EntidadeTO $entTO
     * @return Ead1_Mensageiro
     */
    public function retornaContatosEntidade (EntidadeTO $entTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaContatosEntidade($entTO);
    }

    /**
     * Metodo que retorna Telefones do Contato da Entidade
     * @param EntidadeTO $entTO
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function retornaTelefonesContatoEntidade (EntidadeTO $entTO, ContatoEntidadeTO $ceTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaTelefonesContatoEntidade($entTO, $ceTO);
    }

    /**
     * Metodo que retorna Contas Bancarias da Entidade
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornaContaEntidade (EntidadeTO $eTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornaContaEntidade($eTO);
    }

    /**
     * Metodo que retorna o Email da Instituição
     * @param EntidadeEmailTO $eeTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeEmail (EntidadeEmailTO $eeTO)
    {
        return $this->bo->retornarEntidadeEmail($eeTO);
    }

    /**
     * Método que retorna as entidades filhas de outra entidade
     * @param int $idEntidade
     * @param int $idEntidadeClasse
     * @return Ead1_Mensageiro
     */
    public function retornaEntidadesFilhas ($idEntidade, $idEntidadeClasse = null, $idPerfil = null)
    {
        $bo = new EntidadeBO();
        return $bo->retornaEntidadesFilhas($idEntidade, $idEntidadeClasse, $idPerfil);
    }

    /**
     * Método que retorna as entidades para a árvore
     * @param int $idEntidadeClasse
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreEntidade (VwEntidadeRecursivaTO $vwerTO)
    {
        $bo = new EntidadeBO();
        return $bo->retornarArvoreEntidade($vwerTO);
    }

    /**
     * Método que retorna a entidade da sessão atual e suas filhas
     * @return Ead1_Mensageiro
     */
    public function retornaEntidadeRecursivo ()
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->retornaEntidadeRecursivo();
    }

    /**
     * Metodo que retorna a view de endereço da entidade
     * @param VwEntidadeEnderecoTO $vwEeTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeEndereco (VwEntidadeEnderecoTO $vwEeTO)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->retornarVwEntidadeEndereco($vwEeTO);
    }

    /**
     * Método que retorna as Entidades da Matrícula
     * @param MatriculaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadesMatricula (MatriculaTO $to)
    {
        return $this->bo->retornarEntidadesMatricula($to);
    }

    /**
     * Metodo que deleta telefone da entidade
     * @param ContatoTelefoneTO $ctTO
     * @return Ead1_Mensageiro
     */
    public function deletarContatoTelefoneEntidade (ContatosTelefoneTO $ctTO)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->deletarContatoTelefoneEntidade($ctTO);
    }

    /**
     * Metodo que deleta a conta da entidade
     * @param ContaEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function deletarContaEntidade (ContaEntidadeTO $ceTO)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->deletarContaEntidade($ceTO);
    }

    /**
     * Metodo que deleta a contato da entidade
     * @param ContatoEntidadeTO $ceTO
     * @return Ead1_Mensageiro
     */
    public function deletarContatoEntidade (ContatoEntidadeTO $ceTO)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->deletarContatoEntidade($ceTO);
    }

    /**
     * Metodo que retorna  a EntidadeIntegracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadeIntegracao (EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->retornaEntidadeIntegracao($entidadeIntegracao);
    }

    /**
     * Metodo que salva a EntidadeIntegracao
     * @param EntidadeIntegracaoTO $entidadeIntegracao
     * @return Ead1_Mensageiro
     */
    public function salvarEntidadeIntegracao (EntidadeIntegracaoTO $entidadeIntegracao)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->salvaEntidadeIntegracao($entidadeIntegracao);
    }

    /**
     * Método que retorna as entidades que compartilham dados entre si
     * @param VwEntidadeCompartilhaDadosTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeCompartilhaDados (VwEntidadeCompartilhaDadosTO $to)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->retornarVwEntidadeCompartilhaDados($to);
    }

    /**
     * Método que salva o upload do Manual do Aluno
     * @param ArquivoTO $arquivo
     * @return Ead1_Mensageiro
     */
    public function salvarManualAluno (ArquivoTO $arquivo)
    {
        $entidadeBO = new EntidadeBO();
        return $entidadeBO->salvarManualAluno($arquivo);
    }

}

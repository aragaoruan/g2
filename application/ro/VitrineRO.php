<?php
/**
 * Classe de comunicação com Flex para interações com objeto Vitrine
 * @author Denise Xavier <denis.xavier07@gmail.com>
 * @package application
 * @subpackage ro
 * @author Rafael Rocha rafael.rocha.mg@gmail.com
 */
class VitrineRO extends Ead1_RO {
	
	/**
	 * @var VitrineBO
	 */
	private $bo;
	
	/**
	 * Método construtor
	 */
	public function __construct(){
		$this->bo = new VitrineBO();
	}
	
	/**
	 * @param VwPesquisaVitrineTO $vwPesquisaVitrine
	 * @return Ead1_Mensageiro
	 */
	public function pesquisarAfiliadoResponsavel(VwPesquisaVitrineTO $vwPesquisaVitrine){
		return $this->bo->pesquisarAfiliadoResponsavel($vwPesquisaVitrine);

	}

	/**
	 * @param VitrineTO $vitrine
	 * @param CategoriaTO $categoria
	 * @param array $produtos
	 * @return Ead1_Mensageiro
	 */
	public function salvarVitrine(VitrineTO $vitrine, CategoriaTO $categoria = null, array $produtos = null){
		return $this->bo->salvarVitrine($vitrine, $categoria, $produtos);
	}
	
	/**
	 * @param VitrineTO $vitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarVitrine(VitrineTO $vitrine){
		return $this->bo->retornarVitrine($vitrine);
	}
	
	/**
	 * @param VwVitrineTO $vwVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarArVitrineTO(VwVitrineTO $vwVitrine){
		return $this->bo->retornarArVitrineTO($vwVitrine);
	}
	
	/**
	 * @param VitrineProdutoTO $vitrineProduto
	 * @return Ead1_Mensageiro
	 */
	public function salvarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		return $this->bo->salvarVitrineProduto($vitrineProduto);
	}
	
	/**
	 * @param VitrineProdutoTO $vitrineProduto
	 * @return Ead1_Mensageiro
	 */
	public function retornarVitrineProduto(VitrineProdutoTO $vitrineProduto){
		return $this->bo->retornarVitrineProduto($vitrineProduto);
	}
	
	/**
	 * @param VwVitrineTO $vwVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwVitrine(VwVitrineTO $vwVitrine){
		return $this->bo->retornarVwVitrine($vwVitrine);
	}
	
	/**
	 * @param ModeloVitrineTO $modeloVitrine
	 * @return Ead1_Mensageiro
	 */
	public function retornarModeloVitrine(ModeloVitrineTO $modeloVitrine){
		return $this->bo->retornarModeloVitrine($modeloVitrine);
	}
	
	
	public function removerVitrine(VitrineTO $to){
		return $this->bo->removerVitrine($to);
	}
}

?>
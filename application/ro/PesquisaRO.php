<?php
/**
 * Classe de comunicação com Flex para interações com objeto de pesquisa
 * @author Dimas Sulz <dimassulz@gmail.com>
 * 
 * @package application
 * @subpackage ro
 */
class PesquisaRO extends Ead1_RO{
	
	/**
	 * Armazena os valores dos arrays de retorno de alguns métodos
	 * @var Array
	 */
	public $arrayRetorno = array();
	
	/**
	 * Método que retorna os campos de pesquisa
	 * @param PesquisaTO $pTO
	 */
	public function retornarCamposPesquisa(PesquisaTO $pTO){
		switch ($tipo = $pTO->getTipo()){
			
			case $tipo == 'CadastrarPacote';
			
/* 				$pTO->setRo('PacoteRO');
				$pTO->setId_funcionalidade( 106 );  //item de material
				$pTO->setMetodo('listarPacote');
				$pTO->setTo_envia('PacoteTO');
				
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Nome: ', 'st_itemdematerial', Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Nome: ', 'st_itemdematerial');
				
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex()); */
			break;
			
			case $tipo == 'CadastrarPessoa':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarPessoa');
				$pTO->setTo_envia('PesquisarPessoaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_pessoa');
				$filtros[] = $this->setFiltro('Nome','st_nomecompleto',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('E-mail','st_email',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('CPF','st_cpf', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Código','id_usuario', Ead1_IFlex::CheckBox); 
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Nome","st_nomecompleto");
				$arCampos[] = $this->setCampos("E-mail","st_email");
				$arCampos[] = $this->setCampos("CPF","st_cpf");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$arCampos[] = $this->setCampos("Data Nascimento","dt_nascimento",false);
				$arCampos[] = $this->setCampos("Sexo","st_sexo",false);
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'Entidade':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarOrganizacao');
				$pTO->setTo_envia('PesquisarOrganizacaoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_entidade');
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				//$filtros[] = $this->setFiltro('Instituição: ','bl_grupo',Ead1_IFlex::CheckBox);
				//$filtros[] = $this->setFiltro('Pólo: ','bl_grupo',Ead1_IFlex::CheckBox);
				//$filtros[] = $this->setFiltro('Grupo: ','bl_grupo', Ead1_IFlex::CheckBox);
				//$filtros[] = $this->setFiltro('Núcleo: ','bl_grupo', Ead1_IFlex::CheckBox); 
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Nome","st_nomeentidade");
				$arCampos[] = $this->setCampos("Razão Social","st_razaosocial");
				$arCampos[] = $this->setCampos("Número CNPJ","st_cnpj");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
				case $tipo == 'CadastrarInstituicao':
					$pTO->setRo('PesquisarRO');
					$pTO->setMetodo('pesquisarOrganizacao');
					$pTO->setTo_envia('PesquisarOrganizacaoTO');
					$sTO = new SituacaoTO();
					$sTO->setSt_tabela('tb_entidade');
					$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
					$filtros[] = $this->setFiltro('Instituição: ','bl_grupo',Ead1_IFlex::CheckBox);
					$pTO->setFiltros($filtros);
					
					$arCampos[] = $this->setCampos("Nome","st_nomeentidade");
					$arCampos[] = $this->setCampos("Razão Social","st_razaosocial");
					$arCampos[] = $this->setCampos("Número CNPJ","st_cnpj");
					$pTO->setCamposGrid($arCampos);
					return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
				
				case $tipo == 'CadastrarPolo':
					$pTO->setRo('PesquisarRO');
					$pTO->setMetodo('pesquisarOrganizacao');
					$pTO->setTo_envia('PesquisarOrganizacaoTO');
					$sTO = new SituacaoTO();
					$sTO->setSt_tabela('tb_entidade');
					$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
					$filtros[] = $this->setFiltro('Pólo: ','bl_grupo',Ead1_IFlex::CheckBox);
					$pTO->setFiltros($filtros);
					
					$arCampos[] = $this->setCampos("Nome","st_nomeentidade");
					$arCampos[] = $this->setCampos("Razão Social","st_razaosocial");
					$arCampos[] = $this->setCampos("Número CNPJ","st_cnpj");
					$pTO->setCamposGrid($arCampos);
					return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
				
				case $tipo == 'CadastrarNucleo':
					$pTO->setRo('PesquisarRO');
					$pTO->setMetodo('pesquisarOrganizacao');
					$pTO->setTo_envia('PesquisarOrganizacaoTO');
					$sTO = new SituacaoTO();
					$sTO->setSt_tabela('tb_entidade');
					$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
					$filtros[] = $this->setFiltro('Núcleo: ','bl_grupo',Ead1_IFlex::CheckBox);
					$pTO->setFiltros($filtros);
					
					$arCampos[] = $this->setCampos("Nome","st_nomeentidade");
					$arCampos[] = $this->setCampos("Razão Social","st_razaosocial");
					$arCampos[] = $this->setCampos("Número CNPJ","st_cnpj");
					$pTO->setCamposGrid($arCampos);
					return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
				
			case $tipo == 'Telemarketing':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarTelemarketing');
				$pTO->setTo_envia('PesquisarTelemarketingTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_entidade');
//				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
//				$filtros[] = $this->setFiltro('Todos: ',NULL, Ead1_IFlex::CheckBox);
//				$filtros[] = $this->setFiltro('Núcleo: ','bl_grupo',Ead1_IFlex::CheckBox);
//				$pTO->setFiltros($filtros);
//				
//				$arCampos[] = $this->setCampos("Nome","st_nomeentidade");
//				$arCampos[] = $this->setCampos("Razão Social","st_razaosocial");
//				$arCampos[] = $this->setCampos("Número CNPJ","st_cnpj");
//				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarRegraContrato';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarRegraContrato');
				$pTO->setTo_envia('PesquisaRegraContratoTO');
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro($label->getLabel(210).': ','st_contratoregra',Ead1_IFlex::CheckBox);
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos($label->getLabel(210).": ","st_contratoregra");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'GeradorRelatorio';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarRelatorio');
				$pTO->setTo_envia('PesquisaRelatorioTO');
				$filtros[] = $this->setFiltro('Relatório: ','st_relatorio',Ead1_IFlex::CheckBox);
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Relatório: ","st_relatorio");
				$arCampos[] = $this->setCampos("Origem: ","st_origem");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarProtocolo';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarProtocolo');
				$pTO->setTo_envia('PesquisarProtocoloTO');
				$filtros[] = $this->setFiltro('Data de Cadastro: ', 'dt_cadastro', Ead1_IFlex::DateField);
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos('Protocolo',"id_protocolo");
				$arCampos[] = $this->setCampos('Usuário',"st_nomecompleto");
				$arCampos[] = $this->setCampos('Data de Cadastro',"dt_cadastro");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarVenda';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarVenda');
				$pTO->setTo_envia('PesquisarVendaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_venda');
				$eTO = new EvolucaoTO();
				$eTO->setSt_tabela('tb_venda');
				$filtros[] = $this->setFiltro('Nome: ','st_nomecompleto',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Situação: ','id_situacao',Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Evolução: ','id_evolucao',Ead1_IFlex::DropDownList, $this->retornaEvolucao($eTO));
				$filtros[] = $this->setFiltro('Possui Contrato: ','bl_contrato',Ead1_IFlex::CheckBox);
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("ID","id_venda");
				$arCampos[] = $this->setCampos("Usuário","st_nomecompleto");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$arCampos[] = $this->setCampos("Evolução","st_evolucao");
				$arCampos[] = $this->setCampos("Valor Bruto","nu_valorbruto");
				$arCampos[] = $this->setCampos("Data","dt_cadastro");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarTurma';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('PesquisarTurma');
				$pTO->setTo_envia('PesquisarTurmaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_turma');
				$filtros[] = $this->setFiltro('Título: ','st_turma',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Situação: ','id_situacao',Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Data de Abertura: ', 'dt_inicio', Ead1_IFlex::DateField);
				$filtros[] = $this->setFiltro('Data de Encerramento: ', 'dt_fim', Ead1_IFlex::DateField);
				$filtros[] = $this->setFiltro('Data de Inicio de Inscrição: ', 'dt_inicioinscricao', Ead1_IFlex::DateField);
				$filtros[] = $this->setFiltro('Data de Termino de Inscrição: ', 'dt_fiminscricao', Ead1_IFlex::DateField);
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos("Título","st_turma");
				$arCampos[] = $this->setCampos("Título de Exibição","st_tituloexibicao");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$arCampos[] = $this->setCampos("Início Inscrição","dt_inicioinscricao");
				$arCampos[] = $this->setCampos("Término Inscrição","dt_fiminscricao");
				$arCampos[] = $this->setCampos("Abertura","dt_inicio");
				$arCampos[] = $this->setCampos("Encerramento","dt_fim");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
//			case $tipo == 'CadastrarItemMaterial';
//				$pTO->setRo('PesquisarRO');
//				$pTO->setMetodo('pesquisarItemMaterial');
//				$pTO->setTo_envia('ItemDeMaterialTO');
//				$sTO = new SituacaoTO();
//				$sTO->setSt_tabela('tb_itemdematerial');
//				$pTO->setFiltros($filtros);
//				$label = new Ead1_LabelFuncionalidade();
//				
//				//$arCampos[] = $this->setCampos('Nome: ', 'st_itemdematerial');
//				$arCampos[] = $this->setCampos($label->getLabel(106),"st_itemdematerial");
//				
//				$pTO->setCamposGrid($arCampos);
//				return new Ead1_Mensageiro($pTO->tipaTOFlex());
//			break;
			
			case $tipo == 'CadastrarPerfil';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarPerfil');
				$pTO->setTo_envia('ItemDeMaterialTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_itemdematerial');
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Item de Material","st_itemdematerial");
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarDisciplina':
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade( 24 );  //94 tira a funcionalidade excluir
				$pTO->setMetodo('pesquisarDisciplina');
				$pTO->setTo_envia('PesquisarDisciplinaTO');
				$sTO = new SituacaoTO();
				$tdTO = new TipoDisciplinaTO();
				$tdBO = new DisciplinaBO();
				$sTO->setSt_tabela('tb_disciplina');
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Tipo de '.$label->getLabel(24).': ', 'id_tipodisciplina', Ead1_IFlex::DropDownList, $tdBO->retornaTipoDisciplina($tdTO,true));
				$filtros[] = $this->setFiltro('Nível de Ensino: ', 'id_nivelensino', Ead1_IFlex::DropDownList, $this->nivelEnsino(true));
				$filtros[] = $this->setFiltro('Nome: ','st_disciplina',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro($label->getLabel(20).': ','st_areaconhecimento',Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos($label->getLabel(24),"st_disciplina");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$arCampos[] = $this->setCampos("Tipo de ".$label->getLabel(24),"st_tipodisciplina");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarAreaConhecimento':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarArea');
				$pTO->setId_funcionalidade( 20 ); // 90 tira a funcionalidade excluir
				$pTO->setTo_envia('PesquisarAreaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_areaconhecimento');
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Nível de Ensino', 'id_nivelensino', Ead1_IFlex::DropDownList, $this->nivelEnsino(true));
				//$filtros[] = $this->setFiltro('Todos: ',NULL, Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Nome','st_areaconhecimento',Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos($label->getLabel(20),"st_areaconhecimento");
				$arCampos[] = $this->setCampos("Nível de Ensino","st_nivelensino");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				
				$pTO->setCamposGrid($arCampos);
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarSalaDeAula':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarSalaDeAula');
				$pTO->setId_funcionalidade( 23 );   //93 tira a funcionalidade excluir
				$pTO->setTo_envia('PesquisarSalaDeAulaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_saladeaula');
				$salaBO = new SalaDeAulaBO();
				$pesqBO = new PesquisarBO();
				$tipoSalaDeAulaTO = new TipoSalaDeAulaTO();
				$modalidadeSalaDeAulaTO = new ModalidadeSalaDeAulaTO();
                $categoriSalaTO = new CategoriaSalaTO();
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Tipo de '.$label->getLabel(23).': ', 'id_tiposaladeaula', Ead1_IFlex::DropDownList, $salaBO->retornarTipoSalaDeAula($tipoSalaDeAulaTO, true));
				$filtros[] = $this->setFiltro('Modalidade de '.$label->getLabel(23).': ', 'id_modalidadesaladeaula', Ead1_IFlex::DropDownList, $salaBO->retornarModalidadeSalaDeAula($modalidadeSalaDeAulaTO, true));
				$filtros[] = $this->setFiltro($label->getLabel(24).': ','st_disciplina',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Sala de Aula', 'st_saladeaula', Ead1_IFlex::CheckBox);
                $filtros[] = $this->setFiltro('Categoria de '.$label->getLabel(23).': ', 'id_categoriasala', Ead1_IFlex::DropDownList, $salaBO->retornarCategoriaSala($categoriSalaTO, true));
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos($label->getLabel(23), "st_saladeaula");
				$arCampos[] = $this->setCampos($label->getLabel(24) ,"st_disciplina");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$arCampos[] = $this->setCampos("Modalidade","st_modalidadesaladeaula",false);
				$arCampos[] = $this->setCampos("Tipo","st_tiposaladeaula",false);
				$arCampos[] = $this->setCampos("Abertura","dt_abertura");
				$arCampos[] = $this->setCampos("Encerramento","dt_encerramento");
				$arCampos[] = $this->setCampos("Professor","st_nomeprofessor",false);
                $arCampos[] = $this->setCampos("Categoria","st_categoriasala");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarAproveitamento':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarAproveitamento');
				$pTO->setId_funcionalidade( 23 );   //93 tira a funcionalidade excluir
				$pTO->setTo_envia('PesquisarAproveitamentoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_aproveitamentodisciplina');
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Aluno: ','st_nomecompleto',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro($label->getLabel(24).': ','st_disciplinaoriginal',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro($label->getLabel(19).': ','st_tituloexibicao',Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Aluno: ", "st_nomecompleto");
				$arCampos[] = $this->setCampos("Aproveitamento","id_aproveitamentodisciplina");
				$arCampos[] = $this->setCampos($label->getLabel(24),"st_disciplinaoriginal");
				$arCampos[] = $this->setCampos($label->getLabel(19),"st_tituloexibicao");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarCampanha':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarCampanhaComercial');
				$pTO->setId_funcionalidade( 308 );   //93 tira a funcionalidade excluir
				$pTO->setTo_envia('PesquisarCampanhaComercialTO');
				$sTO = new SituacaoTO();
				$campanhaBO = new CampanhaComercialBO();
				$sTO->setSt_tabela('tb_campanhacomercial');
				$tipoCampanhaTO = new TipoCampanhaTO();
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Tipo de '.$label->getLabel(308).': ', 'id_tipocampanha', Ead1_IFlex::DropDownList, $campanhaBO->retornarTipoCampanha($tipoCampanhaTO, true));
				
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Campanha ", "st_campanhacomercial");
				$arCampos[] = $this->setCampos("Tipo de Campanha","st_tipocampanha");
				$arCampos[] = $this->setCampos('Situação' ,"st_situacao");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarConjuntoAvaliacao':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarConjuntoAvaliacao');
				$pTO->setId_funcionalidade( 259 );   //93 tira a funcionalidade excluir
				$pTO->setTo_envia('PesquisarAvaliacaoConjuntoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_avaliacaoconjunto');
				$projetoPedagogicoBO = new ProjetoPedagogicoBO();
				$filtros[] = $this->setFiltro('Conjunto de Avaliação: ','st_avaliacaoconjunto',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Tipo de Prova: ','id_tipoprova',Ead1_IFlex::DropDownList, $projetoPedagogicoBO->retornarTipoProva(new TipoProvaTO(), true));
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos("Conjunto de Avaliacao: ", "st_avaliacaoconjunto");
				$arCampos[] = $this->setCampos("Tipo de prova","st_tipoprova");
				$arCampos[] = $this->setCampos('Situação' ,"st_situacao");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarProjetoPedagogico':
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade( 92 );
				$pTO->setMetodo('pesquisarProjetoPedagogico');
				$pTO->setTo_envia('PesquisarProjetoPedagogicoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_projetopedagogico');
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Descrição: ','st_descricao',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro($label->getLabel(19).': ','st_projetopedagogico',Ead1_IFlex::CheckBox);
				//$filtros[] = $this->setFiltro('Projeto Pedagogico: ','st_projetopedagogico',Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos($label->getLabel(19) ,"st_projetopedagogico");
				$arCampos[] = $this->setCampos("Descrição","st_descricao");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarMatricula':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarContrato');
				$pTO->setTo_envia('PesquisarContratoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_contrato');
				$eTO = new EvolucaoTO();
				$eTO->setSt_tabela('tb_contrato');
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Evolução: ','id_evolucao',Ead1_IFlex::DropDownList,$this->retornaEvolucao($eTO));
				$filtros[] = $this->setFiltro('Nome: ','st_nomecompleto',Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos("Código" ,"id_contrato");
				$arCampos[] = $this->setCampos("Nome Aluno" ,"st_nomecompleto");
				$arCampos[] = $this->setCampos("Evolução","st_evolucao");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarContratoAfiliado':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarContratoAfiliado');
				$pTO->setTo_envia('PesquisarContratoAfiliadoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_contratoafiliado');
				$filtros[] = $this->setFiltro('Código: ','id_contratoafiliado',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Afiliado: ','st_contratoafiliado',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos("Código" ,"id_contratoafiliado");
				$arCampos[] = $this->setCampos("Afiliado" ,"st_contratoafiliado");
				$arCampos[] = $this->setCampos("Responsável","st_nomecompletoresponsavel");
				$arCampos[] = $this->setCampos("Contrato","st_nomeentidadeafiliada");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
//			case $tipo == 'CadastrarRegraContrato'; ========== felipe/arthur 09/06/11
//				$pTO->setRo('PesquisarRO');
//				$pTO->setMetodo('pesquisarRegraContrato');
//				$pTO->setTo_envia('PesquisarRegraContratoTO');				
//				$sTO = new SituacaoTO();
//				//id_situacao, st_contratoregra, nu_contratoduracao, nu_mesesmulta, nu_mesesmensalidade
//				$sTO->setSt_tabela('tb_contratoregra');
//				$filtros[] = $this->setFiltro('Nome: ', 'st_contratoregra',Ead1_IFlex::CheckBox);
//				$filtros[] = $this->setFiltro('Situação: ', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
//				$filtros[] = $this->setFiltro('Duração do Contrato: ', 'nu_contratoduracao', Ead1_IFlex::CheckBox);
//				$filtros[] = $this->setFiltro('Meses de Multa: ', 'nu_mesesmulta', Ead1_IFlex::CheckBox);
//				$filtros[] = $this->setFiltro('Meses de Mensalidade: ', 'nu_mesesmensalidade', Ead1_IFlex::CheckBox);
//				
//				$pTO->setFiltros($filtros);
//				$arCampos[] = $this->setCampos('Nome: ', 'st_contratoregra');
//				$arCampos[] = $this->setCampos('Situação: ', 'id_situacao');
//				$arCampos[] = $this->setCampos('Duração de Contrato', 'nu_contratoduracao');
//				$arCampos[] = $this->setCampos('Meses de Multa', 'nu_mesesmulta');
//				$arCampos[] = $this->setCampos('Meses de Mensalidade', 'nu_mesesmensalidade');
//				$pTO->setCamposGrid($arCampos);
//				
//				return new Ead1_Mensageiro($pTO->tipaTOFlex());
//			break;

			case $tipo == 'CadastrarProduto';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade( 214 ); 
				$pTO->setMetodo('pesquisarProduto');
				$pTO->setTo_envia('PesquisarProdutoTO');
				
				$ptTO = new ProdutoTO();
				$tpTO = new TipoProdutoTO();
				$label = new Ead1_LabelFuncionalidade();
				
				$filtros[] = $this->setFiltro('Nome: ', 'st_produto', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Tipo de '.$label->getLabel(214), 'id_tipoproduto', Ead1_IFlex::DropDownList, $this->retornaTipoProduto($tpTO));
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Código: ', 'id_produto');
				$arCampos[] = $this->setCampos('Nome: ', 'st_produto');
				$arCampos[] = $this->setCampos('Tipo de '.$label->getLabel(214).': ', 'st_tipoproduto');
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
//			case $tipo == 'CadastrarTipoMaterial';
//				$pTO->setRo('PesquisarRO');
//				//$pTO->setId_funcionalidade( ? ); //tipo de material
//				$pTO->setMetodo('pesquisarTipoMaterial');
//				$pTO->setTo_envia('PesquisarTipoMaterialTO');
//				
//				$tmTO = new TipoDeMaterialTO();
//				$label = new Ead1_LabelFuncionalidade();
//				$filtros[] = $this->setFiltro('Nome: ', 'st_tipodematerial', Ead1_IFlex::CheckBox);
//				
//				$pTO->setFiltros($filtros);
//				$arCampos[] = $this->setCampos('Nome: ', 'st_tipodematerial');
//				
//				$pTO->setCamposGrid($arCampos);
//			return new Ead1_Mensageiro($pTO->tipaTOFlex());
//			break;
			
			//@author felipe beserra
			case $tipo == 'CadastrarItemMaterial';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade( 106 );  //item de material
				$pTO->setMetodo('pesquisarItemMaterial');
				$pTO->setTo_envia('PesquisarItemMaterialTO');
				
				$imTO = new ItemDeMaterialTO();
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Nome: ', 'st_itemdematerial', Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Nome: ', 'st_itemdematerial');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			case $tipo == 'CadastrarFormaDePagamento';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade(54);
				$pTO->setMetodo('pesquisarFormaDePagamento');
				$pTO->setTo_envia('PesquisaFormaPagamentoTO');
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Nome: ', 'st_formapagamento', Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Nome: ', 'st_formapagamento');
				$arCampos[] = $this->setCampos('Situação: ', 'st_situacao');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			case $tipo == 'CadastrarAgendamentoAvaliacao';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade(270);
				$pTO->setMetodo('pesquisarAgendamentoAvaliacao');
				$pTO->setTo_envia('PesquisaAvaliacaoAgendamentoTO');
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Avaliação: ', 'st_avaliacao', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Data de Inicio: ', 'dt_agendamentoinicio', Ead1_IFlex::DateField);
				$filtros[] = $this->setFiltro('Data de Término: ', 'dt_agendamentofim', Ead1_IFlex::DateField);
				$filtros[] = $this->setFiltro('Tipo: ', 'id_tipoprova', Ead1_IFlex::DropDownList,$this->retornarTipoProva(new TipoProvaTO()));
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Aluno: ', 'st_nomecompleto');
				$arCampos[] = $this->setCampos('Avaliação: ', 'st_avaliacao');
				$arCampos[] = $this->setCampos('Tipo: ', 'st_tipoprova');
				$arCampos[] = $this->setCampos('Data do Agendamento: ', 'dt_agendamento');
				$arCampos[] = $this->setCampos('Situação: ', 'st_situacao');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			case $tipo == 'CadastrarTextos';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade(270);
				$pTO->setMetodo('pesquisarTextosSistema');
				$pTO->setTo_envia('PesquisarTextoSistemaTO');
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Título: ', 'st_textosistema', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Exibição: ', 'id_textoexibicao', Ead1_IFlex::DropDownList,$this->retornarTextoExibicao(new TextoExibicaoTO()));
				
				$pTO->setFiltros($filtros);
				$arCampos[] = $this->setCampos('Título: ', 'st_textosistema');
				$arCampos[] = $this->setCampos('Exibição: ', 'st_textoexibicao');
				$arCampos[] = $this->setCampos('Categoria: ', 'st_textocategoria');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			case $tipo == 'CadastrarAplicacaoAvaliacao';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade(253);
				$pTO->setMetodo('pesquisarAplicacaoAvaliacao');
				$pTO->setTo_envia('PesquisaAvaliacaoAplicacaoTO');
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Sem Data: ', 'bl_semdata', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Data da Aplicação: ', 'dt_aplicacao', Ead1_IFlex::DateField);
				//o
				$filtros[] = $this->setFiltro('Local: ', 'id_entidade', Ead1_IFlex::DropDownList,$this->retornarEntidades());
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos('Local: ', 'st_nomeentidade');
				$arCampos[] = $this->setCampos('Horário: ', 'st_horarioaula');
				$arCampos[] = $this->setCampos('Data: ', 'st_dataaplicacao');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			case $tipo == 'CadastrarPessoaJuridica';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade(253);
				$pTO->setMetodo('pesquisarPessoaJuridica');
				$pTO->setTo_envia('PesquisaEntidadeTO');
				
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Nome: ', 'st_nomeentidade', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Razão Social: ', 'st_razaosocial', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('CNPJ: ', 'st_cnpj', Ead1_IFlex::CheckBox);
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos('Nome: ', 'st_nomeentidade');
				$arCampos[] = $this->setCampos('Razão Social: ', 'st_razaosocial');
				$arCampos[] = $this->setCampos('CNPJ: ', 'st_cnpj');
				
				$pTO->setCamposGrid($arCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarAvaliacao';
				$pTO->setRo('PesquisarRO');
				$pTO->setId_funcionalidade( 245 );
				$pTO->setMetodo('pesquisarAvaliacao');
				$pTO->setTo_envia('PesquisaAvaliacaoTO');
				
				$aTO = new AvaliacaoTO();
				$taTO = new TipoAvaliacaoTO();
				$trTO = new TipoRecuperacaoTO();
				$label = new Ead1_LabelFuncionalidade();
				$filtros[] = $this->setFiltro('Nome: ', 'st_avaliacao', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Tipo de Avaliação: ','id_tipoavaliacao', Ead1_IFlex::DropDownList, $this->retornaTipoAvaliacao($taTO));
				//$filtros[] = $this->setFiltro('Tipo de Recuperação: ', 'id_tiporecuperacao', Ead1_IFlex::DropDownList, $this->retornaTipoRecuperacao($trTO));
				
				$pTO->setFiltros($filtros);
				$arrCampos[] = $this->setCampos('Nome: ', 'st_avaliacao');
				$arrCampos[] = $this->setCampos('Valor: ', 'nu_valor');
				$arrCampos[] = $this->setCampos('Tipo de ' . $label->getLabel(245) .':', 'st_tipoavaliacao');
				//$arrCampos[] = $this->setCampos('Tipo de ' . $label->getLabel(245) .':', 'st_tiporecuperacao');
				
				$pTO->setCamposGrid($arrCampos);
			return new Ead1_Mensageiro($pTO->tipaTOFlex());
			break;
			
			case $tipo == 'CadastrarAssunto':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarAssuntoCo');
				$pTO->setTo_envia('PesquisarAssuntoCoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_assuntoco');
				
				$ro = new AssuntoCoRO();
				$mensageiro = $ro->retornaTipoOcorrencia(new TipoOcorrenciaTO());
				foreach($mensageiro->getMensagem() as $index => $objeto){
					$arDadosTipoOcorrencia[$index]['label'] = $objeto->getSt_tipoocorrencia();
					$arDadosTipoOcorrencia[$index]['valor'] = $objeto->getId_tipoocorrencia();
				}
				
				$filtros[] = $this->setFiltro('Assunto','st_assuntoco',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Assunto Pai','st_assuntocopai', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				$filtros[] = $this->setFiltro('Tipo Interessado', 'id_tipoocorrencia', Ead1_IFlex::DropDownList, $arDadosTipoOcorrencia);
			
				$pTO->setFiltros($filtros);
			
				$arCampos[] = $this->setCampos("Assunto","st_assuntoco");
				$arCampos[] = $this->setCampos("Assunto Pai","st_assuntocopai");
				$arCampos[] = $this->setCampos("Tipo Interessado","st_tipoocorrencia");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
			
				$pTO->setCamposGrid($arCampos);
			
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
			
			case $tipo == 'CadastrarMotivacional';
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarMensagemMotivacional');
				$pTO->setTo_envia('PesquisarMensagemMotivacionalTO');

				$filtros[] = $this->setFiltro('Mensagem: ', 'st_mensagemmotivacional', Ead1_IFlex::CheckBox);
				$pTO->setFiltros($filtros);
				
				$arrCampos[] = $this->setCampos('Mensagem: ', 'st_mensagemmotivacional');
				$pTO->setCamposGrid($arrCampos);
				
				return new Ead1_Mensageiro($pTO, Ead1_IMensageiro::SUCESSO);
			
			case $tipo == 'CadastrarBaixaBoleto':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarArquivoRetorno');
				$pTO->setTo_envia('PesquisarArquivoRetornoTO');
			
				$ro = new ArquivoRetornoRO();
			
				$filtros[] = $this->setFiltro('Arquivo retorno','st_arquivoretorno',Ead1_IFlex::CheckBox);
			
				$pTO->setFiltros($filtros);
			
				$arCampos[] = $this->setCampos("Arquivo retorno","st_arquivoretorno");
				$arCampos[] = $this->setCampos("Data Cadastro","dt_cadastro");
			
				$pTO->setCamposGrid($arCampos);
			
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
					
			case $tipo == 'CadastrarCategoria':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarCategoria');
				$pTO->setTo_envia('PesquisarCategoriaTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_categoria');
			
				$ro = new CategoriaRO();

				$filtros[] = $this->setFiltro('Categoria','st_categoria',Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Categoria Pai','st_categoriapai', Ead1_IFlex::CheckBox);
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
			
				$pTO->setFiltros($filtros);
			
				$arCampos[] = $this->setCampos("Categoria","st_categoria");
				$arCampos[] = $this->setCampos("Categoria Pai","st_categoriapai");
				$arCampos[] = $this->setCampos("Situação","st_situacao");
			
				$pTO->setCamposGrid($arCampos);
			
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
				
			case $tipo == 'CadastrarClasseAfiliado':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarClasseAfiliado');
				$pTO->setTo_envia('PesquisarClasseAfiliadoTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_classeafiliado');
					
				$ro = new ClasseAfiliadoRO();
				$mensageiro = $ro->retornaTipoPessoa(new TipoPessoaTO());
				foreach($mensageiro->getMensagem() as $index => $objeto){
					$arDadosTipoPessoa[$index]['label'] = $objeto->getSt_tipopessoa();
					$arDadosTipoPessoa[$index]['valor'] = $objeto->getId_tipopessoa();
				}
				
			
				$filtros[] = $this->setFiltro('Classe Afiliado','st_classeafiliado', Ead1_IFlex::TextInput);
				$filtros[] = $this->setFiltro('Tipo', 'st_tipopessoa', Ead1_IFlex::DropDownList, $arDadosTipoPessoa);
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
					
				$pTO->setFiltros($filtros);
					
				$arCampos[] = $this->setCampos('Classe Afiliado', 'st_classeafiliado');
				$arCampos[] = $this->setCampos('Tipo', 'st_tipopessoa');
				$arCampos[]	= $this->setCampos('Contratos', 'st_contratoafiliado');
				$arCampos[] = $this->setCampos("Situação", "st_situacao");
					
				$pTO->setCamposGrid($arCampos);
					
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
				
			case $tipo == 'CadastrarSetor':
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarSetor');
				$pTO->setTo_envia('PesquisarSetorTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_setor');
					
				$ro = new SetorRO();
					
				$filtros[] = $this->setFiltro('Setor','st_setor', Ead1_IFlex::TextInput);
				$filtros[] = $this->setFiltro('Setor Pai','st_setorpai', Ead1_IFlex::TextInput);
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
					
				$pTO->setFiltros($filtros);
					
				$arCampos[] = $this->setCampos('Cód. Setor', 'id_setor');
				$arCampos[] = $this->setCampos('Setor', 'st_setor');
				$arCampos[] = $this->setCampos('Cód. Setor Pai', 'id_setorpai');
				$arCampos[] = $this->setCampos('Setor Pai', 'st_setorpai');
				$arCampos[] = $this->setCampos("Situação", "st_situacao");
					
				$pTO->setCamposGrid($arCampos);
					
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				break;
			case $tipo == 'CadastrarLivro':
				
				$pTO->setRo('PesquisarRO');
				$pTO->setMetodo('pesquisarLivro');
				$pTO->setTo_envia('PesquisarLivroTO');
				$sTO = new SituacaoTO();
				$sTO->setSt_tabela('tb_livro');
				
				
				$ro = new LivroRO();
				
				$filtros[] = $this->setFiltro('Livro','st_livro', Ead1_IFlex::TextInput);
				$filtros[] = $this->setFiltro('ISBN','st_isbn', Ead1_IFlex::TextInput);
				$filtros[] = $this->setFiltro('Situação', 'id_situacao', Ead1_IFlex::DropDownList, $this->retornaSituacao($sTO));
				
				$pTO->setFiltros($filtros);
				
				$arCampos[] = $this->setCampos('ID Livro', 'id_livro');
				$arCampos[] = $this->setCampos('Livro', 'st_livro');
				$arCampos[] = $this->setCampos('ISBN', 'st_isbn');
				$arCampos[] = $this->setCampos("Situação", "st_situacao");
				
				$pTO->setCamposGrid($arCampos);
				
				return new Ead1_Mensageiro($pTO->tipaTOFlex());
				
				break;
				
			default:
				return new Ead1_Mensageiro('Erro ao retornar os campos de pesquisa', Ead1_IMensageiro::ERRO);
		}
	}
	
	/**
	 * Método de pesquisa genérica, retorna o TO preenchido com o mensageiro
	 * @param PesquisaTO $pTO
	 * @param FiltroTO $fTO utilizado para teste no PHP
	 * @return PesquisaTO
	 */
	public function pesquisar(PesquisaTO $pTO, FiltroTO $fTO = null){
		$to = $pTO->getTo_envia();
		if($fTO !== null){
			$to = new $to();
			$pTO->setFiltros(array($fTO));
			foreach($pTO->getFiltros() as $filter){
				$setPropriedade = 'set'.ucfirst($filter->getSt_propriedade());
				$getPropriedade = 'get'.ucfirst($filter->getSt_propriedade());
				$to->$setPropriedade($filter->getValor());
			}
		}
		$ro = $pTO->getRo();
		$ro = new $ro();
		$metodo = $pTO->getMetodo();
		$pTO->setTo_retorno($ro->$metodo($to));
		return $pTO;
	}
	
	/**
	 * Método que seta o objeto para Flex
	 * @param String $label
	 * @param String $propriedade
	 * @param bool $visible
	 * @return Object
	 */
	private function setCampos($label, $propriedade, $visible = true){
		$obj = (Object) array(
			'label' => $label,
			'field' => $propriedade,
			'visible' => $visible
		);
		
		return $obj;
	}
	
	/**
	 * Método para set de filtros
	 * @param String $label
	 * @param String $propriedade
	 * @param String $tipo
	 * @param mixed $valor
	 * @return FiltroTO
	 */
	private function setFiltro($label, $propriedade, $tipo, $valor = null){
		$filtroTO = new FiltroTO();
		$filtroTO->setSt_label($label);
		$filtroTO->setSt_propriedade($propriedade);
		$filtroTO->setSt_tipo($tipo);
		$filtroTO->setValor($valor);
		
		return $filtroTO;
	}
	
	/**
	 * Retorna a situação
	 * @param SituacaoTO $sTO
	 * @return false|Array
	 */
	private function retornaSituacao(SituacaoTO $sTO){
		$bo = new PesquisarBO();
		$arr = $bo->retornaSituacao($sTO);
		if(!$arr){
			return false;
		}else{
			foreach($arr as $index => $values){
				$this->arrayRetorno[$index]['label'] = $values['st_situacao'];
				$this->arrayRetorno[$index]['valor'] = $values['id_situacao'];
			}
			return $this->arrayRetorno;
		}
	}
	
	/**
	 * Retorna o tipo de pessoa
	 * @param SituacaoTO $sTO
	 * @return false|Array
	 * @todo  
	 */
// 	private function retornaSituacao(SituacaoTO $sTO){
// 		$bo = new PesquisarBO();
// 		$arr = $bo->retornaSituacao($sTO);
// 		if(!$arr){
// 			return false;
// 		}else{
// 			foreach($arr as $index => $values){
// 				$this->arrayRetorno[$index]['label'] = $values['st_situacao'];
// 				$this->arrayRetorno[$index]['valor'] = $values['id_situacao'];
// 			}
// 			return $this->arrayRetorno;
// 		}
// 	}
	
	/**
	 * Retorna a evolução
	 * @param EvolucaoTO $eTO
	 * @return false|Array
	 */
	private function retornaEvolucao(EvolucaoTO $eTO){
		$bo = new PesquisarBO();
		$arr = $bo->retornaEvolucao($eTO);
		if(!$arr){
			return false;
		}else{
			foreach ($arr as $index => $values){
				$this->arrayRetorno[$index]['label'] = $values['st_evolucao'];
				$this->arrayRetorno[$index]['valor'] = $values['id_evolucao'];
			}
			return $this->arrayRetorno;
		}
	}
	
	/**
	 * Retorna tipoproduto
	 * @param $tpTO
	 */
	private function retornaTipoProduto(TipoProdutoTO $tpTO){
		$bo = new PesquisarBO();
		$arr = $bo->retornaTipoProduto($tpTO);
		if(!$arr){
			return false;
		} else {
			$arrayRetorno = array();
			foreach ($arr as $index => $values){
				$arrayRetorno[$index]['label'] = $values['st_tipoproduto'];
				$arrayRetorno[$index]['valor'] = $values['id_tipoproduto'];
			}
			
			return $arrayRetorno; 
		}
	}
	
	
	/**
	 * Retorna tipoMaterial
	 * @param $tmTO
	 */
//	private function retornaTipoMaterial(TipoDeMaterialTO $tmTO){
//		$bo = new PesquisarBO();
//		$arr = $bo->retornaPesquisaTipoMateiral($tmTO);
//		if(!$arr){
//			return false;
//		} else {
//			$arrayRetorno = array();
//			foreach ($arr as $index => $values){
//				$arrayRetorno[$index]['label'] = $values['st_tipodematerial'];
//				$arrayRetorno[$index]['valor'] = $values['id_tipodematerial'];
//			}
//			return $arrayRetorno;
//		}
//	}
	
	/**
	 * Retorna itemMaterial
	 * @param $imTO
	 */
	private function retornaItemMaterial(ItemDeMaterialTO $imTO){
		$bo = new PesquisarBO();
		$arr = $bo->retornarPesquisaItemMaterial($imTO);
		if(!$arr){
			return false;
		} else {
			$arrayretorno = array();
			foreach ($arr as $index => $values){
				$arrayretorno[$index]['label'] = $values['st_itemdematerial'];
				$arrayretorno[$index]['valor'] = $values['id_itemdematerial'];
			}
			return $arrayretorno;
		}
	}
	
	
	/**
	 * Retorna tipoAvaliacao
	 * @param $taTO
	 */
	private function retornaTipoAvaliacao(TipoAvaliacaoTO $taTO){
		$bo = new PesquisarBO();
		$avaliacaoDao = new AvaliacaoDAO();
		$arr = $avaliacaoDao->retornarTipoAvaliacao($taTO)->toArray();
		if(!$arr){
			return false;
		} else {
			$arrayRetorno = array();
			foreach ($arr as $index => $values){
				$arrayRetorno[$index]['label'] = $values['st_tipoavaliacao'];
				$arrayRetorno[$index]['valor'] = $values['id_tipoavaliacao'];
			}
			return $arrayRetorno;
		}
	}
	
	/**
	 * Retorna tipo de prova
	 * @param TipoProvaTO $tpTO
	 */
	private function retornarTipoProva(TipoProvaTO $tpTO){
		try{
			$bo = new PesquisarBO();
			$dao = new ProjetoPedagogicoDAO();
			$arr = $dao->retornarTipoProva($tpTO)->toArray();
			if(empty($arr)){
				return false;
			} else {
				$arrayRetorno = array();
				foreach ($arr as $index => $values){
					$arrayRetorno[$index]['label'] = $values['st_tipoprova'];
					$arrayRetorno[$index]['valor'] = $values['id_tipoprova'];
				}
				return $arrayRetorno;
			}
		}catch (Zend_Exception $e){
			return false;
		}
	}
	
	/**
	 * Retorna os tipos de exibição para o texto
	 * @param TextoExibicaoTO $to
	 */
	private function retornarTextoExibicao(TextoExibicaoTO $to){
		try{
			$dao = new ConfiguracaoEntidadeDAO();
			$arr = $dao->retornarTextoExibicao($to)->toArray();
			if(empty($arr)){
				return false;
			} else {
				$arrayRetorno = array();
				foreach ($arr as $index => $values){
					$arrayRetorno[$index]['label'] = $values['st_textoexibicao'];
					$arrayRetorno[$index]['valor'] = $values['id_textoexibicao'];
				}
				return $arrayRetorno;
			}
		}catch (Exception $e){
			return false;
		}
	}
	
	
	/**
	 * Retorna tipoRecuperacao
	 * @param $trTO
	 */
	private function retornaTipoRecuperacao(TipoRecuperacaoTO $trTO){
		$bo = new PesquisarBO();
		$projetoPedagogicoDao = new ProjetoPedagogicoDAO();
		$arr = $projetoPedagogicoDao->retornarTipoRecuperacao($trTO)->toArray();
		if(!$arr){
			return false;
		} else {
			$arrayRetorno = array();
			foreach ($arr as $index => $values){
				$arrayRetorno[$index]['label'] = $values['st_tiporecuperacao'];
				$arrayRetorno[$index]['valor'] = $values['id_tiporecuperacao'];
			}
			return $arrayRetorno;
		}
	}
	
	/**
	 * Retorna as entidades para combo de pesquisa
	 */
	private function retornarEntidades(){
		$entidades = $this->entidadeFilhas();
		if($entidades->getTipo() != Ead1_IMensageiro::SUCESSO){
			return false;
		}
		$arrayRetorno = array();
		foreach ($entidades->getMensagem() as $index => $to){
			$arrayRetorno[$index]['label'] = $to->st_nomeentidade;
			$arrayRetorno[$index]['valor'] = $to->id_entidade;
		}
		return $arrayRetorno;
	}
}
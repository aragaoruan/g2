<?php
/**
 * Classe de comunicação com Flex para interações com objeto Responsaveis Legais
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package application
 * @subpackage ro
 */
class ResponsavelLegalRO extends SecretariaRO{
	public $mensageiro;
	public $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new ResponsavelLegalBO();
	}
	
	/**
	 * Metodo que decide se cadastra ou edita o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		if($erlTO->getId_entidaderesponsavellegal()){
			return $this->editarEntidadeResponsavelLegal($erlTO);
		}
		return $this->cadastrarEntidadeResponsavelLegal($erlTO);
	}
	
	/**
	 * Metodo que cadastra o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		return $this->bo->cadastrarEntidadeResponsavelLegal($erlTO);
	}
	
	/**
	 * Metodo que edita o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		return $this->bo->editarEntidadeResponsavelLegal($erlTO);
	}
	
	/**
	 * Metodo que exclui o Responsavel Legal da Entidade 
	 * @param EntidadeResponsavelLegalTO $erlTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarEntidadeResponsavelLegal(EntidadeResponsavelLegalTO $erlTO){
		return $this->bo->deletarEntidadeResponsavelLegal($erlTO);
	}
	
	/**
	 * Metodo que retorna o Responsavel Legal da Entidade 
	 * @param VwResponsavelLegalTO $vrlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntidadeResponsavelLegal(VwResponsavelLegalTO $vrlTO){
		return $this->bo->retornarEntidadeResponsavelLegal($vrlTO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Responsavel Legal da Entidade 
	 * @param TipoEntidadeResponsavelLegalTO $terlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoEntidadeResponsavelLegal(TipoEntidadeResponsavelLegalTO $terlTO){
		return $this->bo->retornarTipoEntidadeResponsavelLegal($terlTO);
	}
	
	
	
}
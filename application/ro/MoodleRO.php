<?php

/**
 * Classe para comunicao - MOODLE
 * @author Denise Xavier
 *
 */
class MoodleRO extends Ead1_RO {
	public function sincronizarMoodleIntegracao(){
		$bo = new MoodleBO();
		return $bo->sincronizarMoodleIntegracao();
	}
	public function sincronizarAlunosMoodleIntegracao(){
		$bo = new MoodleBO();
		return $bo->sincronizacaoAlunosMoodle();
	}
}

?>
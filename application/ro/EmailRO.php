<?php
/**
 * Classe de comunicação com Flex para interações com objeto Email
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class EmailRO extends Ead1_RO{

	private $bo;
	
	public function __construct(){
		$this->bo = new EmailBO();
	}
	
	/**
	 * Metodo que salva uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEmailConfig(EmailConfigTO $ecTO){
		return $this->bo->salvarEmailConfig($ecTO);
	}
	
	/**
	 * Metodo que cadastra uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEmailConfig(EmailConfigTO $ecTO){
		return $this->bo->cadastrarEmailConfig($ecTO);
	}
	
	/**
	 * Metodo que edita uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEmailConfig(EmailConfigTO $ecTO){
		return $this->bo->editarEmailConfig($ecTO);
	}
	
	/**
	 * Metodo que retorna uma configuracao de email
	 * @param EmailConfigTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEmailConfig(EmailConfigTO $ecTO){
		return $this->bo->retornarEmailConfig($ecTO);
	}
	
	/**
	 * Metodo que retorna o tipo de conexao do email
	 * @param TipoConexaoEmailTO $tceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoConexaoEmail(TipoConexaoEmailTO $tceTO){
		return $this->bo->retornarTipoConexaoEmail($tceTO);
	}
	
}
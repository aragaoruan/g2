<?php

class FuncaoRO extends Ead1_RO {

	/**
	 *@var FuncaoBO
	 */
	private $bo;
	
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new FuncaoBO();
	}
	
	/**
	 * Metodo que retorna Funcao para perfil
	 * @param FuncaoFuncionalidadeTO $ffTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoFuncionalidade(FuncaoFuncionalidadeTO $ffTO){
		return $this->bo->retornarFuncao($ffTO);
	}
	
	/**
	 * Método que retorna a função 
	 * @param FuncaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncao(FuncaoTO $to){
		return $this->bo->retornarFuncao($to);
	}
	
	/**
	 * Metodo que retorna dados completos de função
	 * @param FuncaoTO $fTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoCompleto(FuncaoTO $fTO){
		return $this->bo->retornarFuncaoCompleto($fTO);
	}
	
	/**
	 * Metodo que retorna perfil para função
	 * @param FuncaoPerfilTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncaoPerfil(FuncaoPerfilTO $fpTO){
		return $this->bo->retornarFuncaoPerfil($fpTO);
	}
	
	/**
	 * Metodo que retorna perfil para função
	 * @param VwFuncaoUsuarioTO $vwfuTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwFuncaoUsuario(VwFuncaoUsuarioTO $vwfuTO){
		return $this->bo->retornarVwFuncaoUsuario($vwfuTO);
	}
	
	/**
	 * Metodo que cadastra perfis para função
	 * @param $arrayPerfis
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFuncaoPerfil($arrayPerfis){
		return $this->bo->cadastrarFuncaoPerfil($arrayPerfis);
	}
}

?>
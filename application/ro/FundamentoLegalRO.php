<?php
/**
 * Classe de comunica√ß√£o com Flex para intera√ß√µes com objeto FundamentoLegal
 * @author Eduardo Rom√£o - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class FundamentoLegalRO extends SecretariaRO{
	
	public $mensageiro;
	public $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new FundamentoLegalBO();
	}
	
	/**
	 * Metodo que Decide se Cadastra ou Edita o Fundamento Legal
	 * @param FundamentoLegal $flTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFundamentoLegal(FundamentoLegalTO $flTO){
		if($flTO->getId_fundamentolegal()){
			return $this->editarFundamentoLegal($flTO);
		}
		return $this->cadastrarFundamentoLegal($flTO);
	}
		
	/**
	 * Metodo que cadastra Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFundamentoLegal(FundamentoLegalTO $flTO){
		return $this->bo->cadastrarFundamentoLegal($flTO);
	}
		
	/**
	 * Metodo que Edita Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFundamentoLegal(FundamentoLegalTO $flTO){
		return $this->bo->editarFundamentoLegal($flTO);
	}
		
	/**
	 * Metodo Que Exclui o Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFundamentoLegal(FundamentoLegalTO $flTO){
		return $this->bo->deletarFundamentoLegal($flTO);
	}
		
	/**
	 * Metodo que retorna o Fundamento Legal
	 * @param FundamentoLegalTO $flTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFundamentoLegal(VwFundamentoLegalTO $vwflTO){
		return $this->bo->retornarFundamentoLegal($vwflTO);
	}
		
	/**
	 * Metodo que Retorna o Tipo do Fundamento Legal
	 * @param TipoFundamentoLegalTO $tflTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoFundamentoLegal(TipoFundamentoLegalTO $tflTO){
		return $this->bo->retornarTipoFundamentoLegal($tflTO);
	}
	
}
<?php

/**
 * Classe de comunicaÃ§Ã£o com Flex para interaÃ§Ãµes com objeto AvaliaÃ§Ã£o
 * @author Eduardo RomÃ£o - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class AvaliacaoRO extends Ead1_RO
{

    private $bo;
    public $mensageiro;

    public function __construct()
    {
        $this->bo = new AvaliacaoBO();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    /**
     * Metodo que decide se cadastra ou edita a avaliacao
     * @param AvaliacaoTO $aTO
     * @param AvaliacaoIntegracaoTO $AvaliacaoIntegracoes
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacao(AvaliacaoTO $aTO, $AvaliacaoIntegracoes, array $arrDisciplinas)
    {
        return $this->bo->salvarAvaliacao($aTO, $AvaliacaoIntegracoes, $arrDisciplinas);
    }

    /**
     * Metodo que salva vÃ¡rias avaliaÃ§Ãµes.
     * @param array $arrAvaliacaoAluno
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacoesAluno(array $arrAvaliacaoAluno)
    {
        return $this->bo->salvarAvaliacoesAluno($arrAvaliacaoAluno);
    }

    /**
     * Metodo que decide se cadastra ou edita a avaliacao do aluno
     * @param AvaliacaoAlunoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAluno(AvaliacaoAlunoTO $aaTO)
    {
        return $this->bo->salvarAvaliacaoAluno($aaTO);
    }

    /**
     * Metodo que decide se cadastra ou edita o conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        return $this->bo->salvarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a relacao do conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        return $this->bo->salvarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que decide se cadastra ou edita um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        return $this->bo->salvarAvaliacaoAgendamento($aaTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->salvarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que altera data dt_defesa dos TCCs.
     * @param array $arrAvaliacaoAluno
     * @return Ead1_Mensageiro
     * @deprecated Método não utilizado
     */
    public function entregarDefesasTCC($arrAvaliacaoAluno)
    {
        return $this->bo->entregarDefesasTCC($arrAvaliacaoAluno);
    }

    /**
     * Metodo que decide se cadastra ou edita um conjunto de avaliacao do projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        return $this->bo->salvarAvaliacaoConjuntoProjeto($acpTO);
    }

    /**
     * Metodo que decide se cadastra ou edita um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        return $this->bo->salvarAvaliacaoConjuntoSala($acsTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a referencia do conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        return $this->bo->salvarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que salva a avaliacaoConjunto e o array de relaÃ§Ãµes
     * @param AvaliacaoConjuntoTO $acTO
     * @param array $arrAvaliacaoConjuntoRelacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoConjuntoRelacao(AvaliacaoConjuntoTO $acTO, array $arrAvaliacaoConjuntoRelacaoTO)
    {

        if (!empty($acTO->id_avaliacaoconjunto) && $acTO->id_avaliacaoconjunto != 0) {
            return $this->bo->editarArrAvaliacaoConjuntoRelacao($acTO, $arrAvaliacaoConjuntoRelacaoTO);
        }
        return $this->bo->salvarArrAvaliacaoConjuntoRelacao($acTO, $arrAvaliacaoConjuntoRelacaoTO);

    }

    /**
     * Metodo que cadastra/edita a avaliacao aplicacao e recadastra a avaliacao aplicacao relacao
     * @param AvaliacaoAplicacaoTO $aaTO
     * @param array $arrAvaliacaoAplicacaoRelacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO, array $arrAvaliacaoAplicacaoRelacaoTO)
    {
        return $this->bo->salvarArrAvaliacaoAplicacaoRelacao($aaTO, $arrAvaliacaoAplicacaoRelacaoTO);
    }

    /**
     * Metodo que salva as referencias do conjunto de avaliacao
     * @param $arrAvaliacaoConjuntoRefenciaTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrAvaliacaoConjuntoReferencia($arrAvaliacaoConjuntoRefenciaTO)
    {
        return $this->bo->salvarArrAvaliacaoConjuntoReferencia($arrAvaliacaoConjuntoRefenciaTO);
    }

    /**
     * MÃ©todo que salva a integraÃ§Ã£o da avaliacao
     * @param array $arrAvaliacaoIntegracaoTO
     * @param AvaliacaoTO $avaliacaoTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoIntegracao($arrAvaliacaoIntegracaoTO, AvaliacaoTO $avaliacaoTO)
    {
        return $this->bo->salvarAvaliacaoIntegracao($arrAvaliacaoIntegracaoTO, $avaliacaoTO);
    }

    /**
     * Metodo que cadastra a avaliacao
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacao(AvaliacaoTO $aTO)
    {
        return $this->bo->cadastrarAvaliacao($aTO);
    }

    /**
     * Metodo que cadastra um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        return $this->bo->cadastrarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que cadastra uma Relacao de Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        return $this->bo->cadastrarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que cadastra a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->cadastrarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que cadastra a relaÃ§Ã£o de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        return $this->bo->cadastrarAvaliacaoAplicacaoRelacao($aprTO);
    }

    /**
     * Metodo que cadastra a referencia de um conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        return $this->bo->cadastrarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que edita a avaliacao
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacao(AvaliacaoTO $aTO)
    {
        return $this->bo->editarAvaliacao($aTO);
    }

    /**
     * Metodo que edita um Conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        return $this->bo->editarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que edita uma relacao de Conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        return $this->bo->editarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que edita uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->editarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que edita uma referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function editarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        return $this->bo->editarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que exclui a avaliacao
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacao(AvaliacaoTO $aTO)
    {
        return $this->bo->deletarAvaliacao($aTO);
    }

    /**
     * Metodo que exclui o conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        return $this->bo->deletarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que exclui a relaÃ§Ã£o do conjunto de avaliacao
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        return $this->bo->deletarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que exclui uma aplicacao de uma avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->deletarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que edita uma aplicacao de uma avaliacao colocando apenas o bl_ativo para 0
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function desativarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->desativarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que exclui a relaÃ§Ã£o de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        return $this->bo->deletarAvaliacaoAplicacaoRelacao($aprTO);
    }

    /**
     * Metodo que exclui um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        return $this->bo->deletarAvaliacaoAgendamento($aaTO);
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        return $this->bo->deletarAvaliacaoConjuntoProjeto($acpTO);
    }

    /**
     * Metodo que exclui um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        return $this->bo->deletarAvaliacaoConjuntoSala($acsTO);
    }

    /**
     * Metodo que verifica se existem vinculos com a avaliacao e exclui
     * @param AvaliacaoTO $to
     * @return Ead1_Mensageiro
     */
    public function verificaExcluiAvaliacao(AvaliacaoTO $to)
    {
        return $this->bo->verificaExcluiAvaliacao($to);
    }

    /**
     * Metodo que verifica se existem vinculos com a avaliacao conjunto e exclui
     * @param AvaliacaoConjuntoTO $to
     * @return Ead1_Mensageiro
     */
    public function verificaExcluiAvaliacaoConjunto(AvaliacaoConjuntoTO $to)
    {
        return $this->bo->verificaExcluiAvaliacaoConjunto($to);
    }

    /**
     * Metodo que exclui a referencia de  conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function deletarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        return $this->bo->deletarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que retorna avaliacao
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacao(AvaliacaoTO $aTO)
    {
        return $this->bo->retornarAvaliacao($aTO);
    }

    /**
     * Metodo que retorna avaliacaoIntegracao
     * @param AvaliacaoIntegracaoTO $aiTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoIntegracao(AvaliacaoIntegracaoTO $aiTO)
    {
        return $this->bo->retornarAvaliacaoIntegracao($aiTO);
    }

    /**
     * Metodo que retorna avaliacao disciplina
     * @param AvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */

    public function retornarAvaliacaoDisciplina(AvaliacaoTO $aTO)
    {
        return $this->bo->retornarAvaliacaoDisciplina($aTO);
    }

    /**
     * Metodo que retorna Tipo Avaliacao
     * @param TipoAvaliacaoTO $aTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAvaliacao(TipoAvaliacaoTO $taTO)
    {
        return $this->bo->retornarTipoAvaliacao($taTO);
    }

    /**
     * Metodo que retorna a relacao do conjunto de avaliacoes
     * @param AvaliacaoConjuntoRelacaoTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoRelacao(AvaliacaoConjuntoRelacaoTO $acrTO)
    {
        return $this->bo->retornarAvaliacaoConjuntoRelacao($acrTO);
    }

    /**
     * Metodo que retorna tipo de calculo de avaliacao
     * @param TipoCalculoAvaliacaoTO $tcaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoCalculoAvaliacao(TipoCalculoAvaliacaoTO $tcaTO)
    {
        return $this->bo->retornarTipoCalculoAvaliacao($tcaTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao
     * @param AvaliacaoConjuntoTO $acTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjunto(AvaliacaoConjuntoTO $acTO)
    {
        return $this->bo->retornarAvaliacaoConjunto($acTO);
    }

    /**
     * Metodo que retorna a aplicacao da avaliacao
     * @param AvaliacaoAplicacaoTO $apTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAplicacao(AvaliacaoAplicacaoTO $apTO)
    {
        return $this->bo->retornarAvaliacaoAplicacao($apTO);
    }

    /**
     * Metodo que retorna a relaÃ§Ã£o de aplicacao da avaliacao
     * @param AvaliacaoAplicacaoRelacaoTO $aprTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAplicacaoRelacao(AvaliacaoAplicacaoRelacaoTO $aprTO)
    {
        return $this->bo->retornarAvaliacaoAplicacaoRelacao($aprTO);
    }

    /**
     * Metodo que retorna as RelaÃ§Ãµes das AvaliaÃ§Ãµes da aplicaÃ§Ã£o no formato de AvaliacaoTO
     * @param AvaliacaoAplicacaoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacoesAplicacaoRelacao(AvaliacaoAplicacaoTO $aaTO)
    {
        return $this->bo->retornarAvaliacoesAplicacaoRelacao($aaTO);
    }

    /**
     * Metodo que retorna a view de avaliacao
     * @param VwPesquisaAvaliacaoTO $vwpaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwPesquisaAvaliacao(VwPesquisaAvaliacaoTO $vwpaTO)
    {
        return $this->bo->retornarVwPesquisaAvaliacao($vwpaTO);
    }

    /**
     * Metodo que retorna a view de avaliacaoconjunto
     * @param VwAvaliacaoConjuntoTO $vwacTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjunto(VwAvaliacaoConjuntoTO $vwacTO)
    {
        return $this->bo->retornarVwAvaliacaoConjunto($vwacTO);
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntorelacao
     * @param VwAvaliacaoConjuntoRelacaoTO $vwacrTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjuntoRelacao(VwAvaliacaoConjuntoRelacaoTO $vwacrTO)
    {
        return $this->bo->retornarVwAvaliacaoConjuntoRelacao($vwacrTO);
    }

    /**
     * Metodo que retorna um agendamento de avaliacao
     * @param AvaliacaoAgendamentoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoAgendamento(AvaliacaoAgendamentoTO $aaTO)
    {
        return $this->bo->retornarAvaliacaoAgendamento($aaTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de sala de aula
     * @param AvaliacaoConjuntoSalaTO $acsTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoSala(AvaliacaoConjuntoSalaTO $acsTO)
    {
        return $this->bo->retornarAvaliacaoConjuntoSala($acsTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao de projeto pedagogico
     * @param AvaliacaoConjuntoProjetoTO $acpTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoProjeto(AvaliacaoConjuntoProjetoTO $acpTO)
    {
        return $this->bo->retornarAvaliacaoConjuntoProjeto($acpTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, aplicacao e agendamento
     * @param VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAplicacaoAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $vaaaTO)
    {
        return $this->bo->retornarVwAvaliacaoAplicacaoAgendamento($vaaaTO);
    }

    /**
     * Metodo que retorna a referencia conjunto de avaliacao
     * @param AvaliacaoConjuntoReferenciaTO $acrTO
     * @return Ead1_Mensageiro
     */
    public function retornarAvaliacaoConjuntoReferencia(AvaliacaoConjuntoReferenciaTO $acrTO)
    {
        return $this->bo->retornarAvaliacaoConjuntoReferencia($acrTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, agendamento, aluno, projeto pedagÃ³gico, entidade
     * @param VwAvaliacaoAgendamentoTO $vaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAgendamento(VwAvaliacaoAgendamentoTO $vaaTO)
    {
        return $this->bo->retornarVwAvaliacaoAgendamento($vaaTO);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, aluno, nota, matricula e modulo
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAluno(VwAvaliacaoAlunoTO $vaaTO, $blTodos = false)
    {
        return $this->bo->retornarVwAvaliacaoAluno($vaaTO, $blTodos);
    }

    /**
     * Metodo que pesquisa as Defesas TCC
     * @param VwAvaliacaoAlunoTO $vwAvaliacaoAlunoTO
     * @param boolean $bl_entregue
     * @return Ead1_Mensageiro
     */
    public function pesquisarDefesaTCC(VwAvaliacaoAlunoTO $vwAvaliacaoAlunoTO, $bl_entregue = false)
    {
        return $this->bo->pesquisarDefesaTCC($vwAvaliacaoAlunoTO, $bl_entregue);
    }

    /**
     * Metodo que retorna um conjunto de avaliacao, aluno, nota, matricula e modulo para o histÃ³rico
     * @param VwAvaliacaoAlunoHistoricoTO $vaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAlunoHistorico(VwAvaliacaoAlunoHistoricoTO $vaaTO)
    {
        return $this->bo->retornarVwAvaliacaoAlunoHistorico($vaaTO);
    }

    /**
     * Metodo que retorna um conjunto de disciplina, aluno, nota, matricula e modulo
     * @param VwAvaliacaoAlunoTO $vaaTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoAlunoDisciplina(VwAvaliacaoAlunoTO $vaaTO)
    {
        return $this->bo->retornarVwAvaliacaoAlunoDisciplina($vaaTO);
    }

    /**
     * Metodo que retorna a view de avaliacaoconjuntoreferencia
     * @param VwAvaliacaoConjuntoReferenciaTO $vwacrTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwAvaliacaoConjuntoReferencia(VwAvaliacaoConjuntoReferenciaTO $vwacrTO)
    {
        return $this->bo->retornarVwAvaliacaoConjuntoReferencia($vwacrTO);
    }

    /**
     * Metodo que retorna as avaliacos disponiveis para agendamento
     * @param VwAvaliacaoAplicacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornaAvaliacaoAplicacaoDisponiveisAgendamento(VwAvaliacaoAplicacaoAgendamentoTO $to)
    {
        return $this->bo->retornaAvaliacaoAplicacaoDisponiveisAgendamento($to);
    }

    /**
     * MÃ©todo que retorna os modelos de avaliacao do sistema de avaliacao
     * @param int $codTipoAvaliacao
     * @param Boolean $ativo
     * @param String $nome
     * @param int $pagina
     * @param int $limit
     * @return Ead1_Mensageiro
     */
    public function retornarModeloAvaliacaoSA($codTipoAvaliacao = null, $ativo = null, $nome = null, $pagina = 1, $limit = 9000000)
    {
        $service = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
        return $service->retornarModeloAvaliacao($codTipoAvaliacao, $ativo, $nome, $pagina, $limit);
    }

    /**
     * MÃ©todo que retorna o vinculo de integraÃ§Ã£o da AvaliaÃ§Ã£o com o Modelo de AvaliaÃ§Ã£o do Sistema de Avaliacao
     * @param AvaliacaoTO $avaliacaoTO
     * @return Ead1_Mensageiro
     */
    public function retornarVinculoAvaliacaoModeloAvaliacaoSA(AvaliacaoTO $avaliacaoTO)
    {
        return $this->bo->retornarVinculoAvaliacaoModeloAvaliacaoSA($avaliacaoTO);
    }

    /**
     * MÃ©todo que retorna as questÃµes, os itens e as respostas de uma determinada avaliaÃ§Ã£o
     * @param string $guidAvaliacao
     * @return Ead1_Mensageiro
     */
    public function retornarQuestoesItensRespostasSA($guidAvaliacao)
    {
        return $this->bo->retornarQuestoesItensRespostasSA($guidAvaliacao);
    }

    /**
     * MÃ©todo que cadastra via WebService as respostas no Sistema de AvaliaÃ§Ã£o
     * @param string $guidAvaliacao
     * @param int $idQuestao
     * @param int $idItemRespondido
     * @return Ead1_Mensageiro
     */
    public function cadastrarRespostasSA($guidAvaliacao, $idQuestao, $idItemRespondido)
    {
        return $this->bo->cadastrarRespostasSA($guidAvaliacao, $idQuestao, $idItemRespondido);
    }

    /**
     * MÃ©todo que cadastra via WebService as respostas no Sistema de AvaliaÃ§Ã£o
     * @param string $guidAvaliacao
     * @param array $arQuestoes
     * @return Ead1_Mensageiro
     */
    public function cadastrarBlocoRespostasSA($guidAvaliacao, array $arQuestoes)
    {
        return $this->bo->cadastrarBlocoRespostasSA($guidAvaliacao, $arQuestoes);
    }

    /**
     * Metodo que exclui logicamente a AvaliacaoAluno
     * @param AvaliacaoAlunoTO $aaTO
     * @return Ead1_Mensageiro
     */
    public function excluirNotaAvaliacao(AvaliacaoAlunoTO $aaTO)
    {
        return $this->bo->excluirNotaAvaliacao($aaTO);
    }

    /**
     * Metodo que exclui logicamente a AvaliacaoAluno
     * @param AvaliacaoDisciplinaTO $adTO
     * @return Ead1_Mensageiro
     */
    public function excluirAvaliacaoDisciplina(AvaliacaoDisciplinaTO $adTO)
    {
        return $this->bo->excluirAvaliacaoDisciplina($adTO);
    }

    /**
     * MÃ©todo que retorna o link para gerar as avaliaÃ§Ãµes
     * @param array $arGuidAvaliacao
     * @return Ead1_Mensageiro
     */
    public function gerarLinkAvaliacao($arGuidAvaliacao)
    {
        $ws = new SaAvaliacaoWebServices(Ead1_BO::geradorLinkWebService(SistemaTO::SISTEMA_AVALIACAO));
        return new Ead1_Mensageiro((string)$ws->impressaoAvaliacoes($arGuidAvaliacao)->getCurrent());
    }
}

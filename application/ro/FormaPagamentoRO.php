<?php
/**
 * Classe de comunicação com Flex para interações com objeto FormaPagamento
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class FormaPagamentoRO extends FinanceiroRO{
	
	public $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new FormaPagamentoBO();
	}
	
	/**
	 * Metodo que cadastra FormaPagamentoProduto
	 * @param FormaPagamentoProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamentoProduto(FormaPagamentoProdutoTO $cpTO){
		return $this->bo->cadastrarFormaPagamentoProduto($cpTO);
	}
	
	
	/**
	 * @param unknown_type $arrayFormaPagamentoProduto
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function cadastrarArrayFormaPagamentoProduto($arrayFormaPagamentoProduto){
		return $this->bo->cadastrarArrayFormaPagamentoProduto($arrayFormaPagamentoProduto);
	}
	
	/**
	 * Exclui a forma de pagamento produto
	 * @param FormaPagamentoProdutoTO $to
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function deletarFormaPagamentoProduto(FormaPagamentoProdutoTO $to){
		return $this->bo->deletarFormaPagamentoProduto($to);
	}
	
	/**
	 * @param VwFormaPagamentoProdutoTO $vwcpTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornarVwFormaPagamentoProduto(VwFormaPagamentoProdutoTO $vwcpTO){
		return $this->bo->retornarVwFormaPagamentoProduto($vwcpTO);
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita Forma de Pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFormaPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->salvarFormaPagamento($fpTO);
	}
	
	/**
	 * Metodo que decide se salva ou exclui a Aplicação da forma de pagamento
	 * @param FormaPagamentoAplicacoesTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO){
		return $this->bo->salvarFormaPagamentoAplicacoes($fpaTO);
	}
	
	/**
	 * @deprecated
	 * Metodo que decide se salva ou edita a Distribuição do Meio de Pagamento
	 * @param FormaPagamentoDistribuicaoTO $fpdTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO){
		return $this->bo->salvarFormaPagamentoDistribuicao($fpdTO);
	}
	
	/**
	 * Metodo que decide se salva ou exclui a Forma do Meio de Pagamento
	 * @param FormaMeioPagamentoTO $fmpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO){
		return $this->bo->salvarFormaMeioPagamento($fmpTO);
	}
	
	/**
	 * Metodo que decide se salva ou exclui a campanha da forma de pagamento
	 * @param CampanhaFormaPagamentoTO $cfpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO){
		return $this->bo->salvarCampanhaFormaPagamento($cfpTO);
	}
	
	/**
	 * Metodo que decide se salva ou exclui a campanha da forma de pagamento
	 * @param $arrayCampanhaFormaPagamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayCampanhaFormaPagamento($arrayCampanhaFormaPagamentoTO){
		return $this->bo->salvarArrayCampanhaFormaPagamento($arrayCampanhaFormaPagamentoTO);
	}
	
	/**
	 * Metodo que vincula todas as Formas de Pagamento a uma campanha
	 * @param CampanhaFormaPagamentoTO $cTO
	 * @return Ead1_Mensageiro
	 */
	public function vincularTodasCampanhasFormaPagamento(CampanhaFormaPagamentoTO $cTO){
		return $this->bo->vincularTodasCampanhasFormaPagamento($cTO);
	}	
	
	
	/**
	 * Metodo que Decide se Edita ou Exclui a Forma de Pagamento da Parcela
	 * @param FormaPagamentoParcelaTO $fppTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO){
		if($fppTO->getId_formapagamentoparcela()){
			return $this->editarFormaPagamentoParcela($fppTO);
		}
		return $this->cadastrarFormaPagamentoParcela($fppTO);
		
	}
	
	/**
	 * Metodo que Decide se Edita ou Cadastra Meio de Pagamento Integracao
	 * @param MeioPagamentoIntegracao $mpiTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO){
		if($mpiTO->getId_meiopagamentointegracao()){
			return $this->editarMeioPagamentoIntegracao($mpiTO);
		}
		return $this->cadastrarMeioPagamentoIntegracao($mpiTO);
		
	}
	
	/**
	 * Método que salva os dias de vencimento da forma de pagamento
	 * @param $arrFormaDiaVencimento
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrFormaDiaVencimento($arrFormaDiaVencimento){
		return $this->bo->salvarArrFormaDiaVencimento($arrFormaDiaVencimento);
	}
	
	/**
	 * Método que salva os dias de vencimento do meio de pagamento
	 * @param $arrFormaMeioVencimento
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrFormaMeioVencimento($arrFormaMeioVencimento){
		return $this->bo->salvarArrFormaMeioVencimento($arrFormaMeioVencimento);
	}
		
	/**
	 * Metodo de procedimento que salva e edita tela de Forma de Pagamento no Flex
	 * @param FormaPagamentoTO $fpTO
	 * @param array( FormaPagamentoParcelaTO $arrFormaPagamentoParcelaTO )
	 * @param array( FormaMeioPagamentoTO $arrFormaMeioPagamentoTOParcela )
	 * @param array( FormaMeioPagamentoTO $arrFormaMeioPgamentoTOEntrada )
	 * @param array( FormaPagamentoAplicacoesTO $arrFormaPagamentoAplicacoesTO )
	 * @param array( CampanhaFormaPagamentoTO $arrCampanhaFormaPagamentoTO )
	 * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
	 * @return Ead1_Mensageiro
	 */
		public function salvarProcedimentoFormaPagamento(FormaPagamentoTO $fpTO, 
													 $arrFormaPagamentoParcelaTO = null, 
													 $arrFormaMeioPagamentoTOParcela = null, 
													 $arrFormaMeioPgamentoTOEntrada = null, 
													 $arrFormaPagamentoAplicacoesTO = null, 
													 $arrCampanhaFormaPagamentoTO = null,
													 FormaPagamentoAplicacaoRelacaoTO $fparTO = null){
		return $this->bo->salvarProcedimentoFormaPagamento($fpTO, $arrFormaPagamentoParcelaTO, $arrFormaMeioPagamentoTOParcela, 
														   $arrFormaMeioPgamentoTOEntrada, $arrFormaPagamentoAplicacoesTO, $arrCampanhaFormaPagamentoTO,$fparTO);
	}
													 
	/**
	 * Metodo que salva Meios de Pagamento para integração com fluxos
	 * @param array $arrMeioPagamentoIntegracao
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrMeioPagamentoIntegracao($arrMeioPagamentoIntegracao){
		return $this->bo->salvarArrMeioPagamentoIntegracao($arrMeioPagamentoIntegracao);
	}
													 
	/**
	 * Metodo que cadastra a Forma de Pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->cadastrarFormaPagamento($fpTO);
	}
													 
	/**
	 * Metodo que cadastra Meio pagamento para integracao
	 * @param MeioPagamentoIntegracaoTO $mpiTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO){
		return $this->bo->cadastrarMeioPagamentoIntegracao($fpTO);
	}
	
	/**
	 * Metodo que cadastra as Aplicações da Forma de Pagamento
	 * @param FormaPagamentoAplicacoesTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO){
		return $this->bo->cadastrarFormaPagamentoAplicacoes($fpaTO);
	}
	
	/**
	 * @deprecated
	 * Metodo que cadastra a Distribuição do Meio de Pagamento
	 * @param FormaPagamentoDistribuicaoTO $fpdTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO){
		return $this->bo->cadastrarFormaPagamentoDistribuicao($fpdTO);
	}
	
	/**
	 * Metodo que cadastra a Forma do Meio de Pagamento
	 * @param FormaMeioPagamentoTO $fmpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO){
		return $this->bo->cadastrarFormaMeioPagamento($fmpTO);
	}
	
	/**
	 * Metodo que cadastra as Campanhas da Forma de Pagamento
	 * @param CampanhaFormaPagamentoTO $cfpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO){
		return $this->bo->cadastrarCampanhaFormaPagamento($cfpTO);
	}
	
	/**
	 * Metodo que Cadastra a Forma de Pagamento da Parcela
	 * @param FormaPagamentoParcelaTO $fppTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO){
		return $this->bo->cadastrarFormaPagamentoParcela($fppTO);
	}
	
	/**
	 * Metodo que cadastra a relacao da forma de pagamento com a aplicacao
	 * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO){
		return $this->bo->cadastrarFormaPagamentoAplicacaoRelacao($fparTO);
	}
	
	/**
	 * Metodo que Edita a Forma de Pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFormaPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->editarFormaPagamento($fpTO);
	}
	
	/**
	 * Metodo que Edita Meio de Pagamento Integração
	 * @param MeioPagamentoIntegracaoTO $mpiTO
	 * @return Ead1_Mensageiro
	 */
	public function editarMeioPagamentoIntegracao(MeioPagamentoIntegracaoTO $mpiTO){
		return $this->bo->editarMeioPagamentoIntegracao($mpiTO);
	}
	
	/**
	 * @deprecated
	 * Metodo que Edita a Distribuição do Meio de Pagamento
	 * @param FormaPagamentoDistribuicaoTO $fpdTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO){
		return $this->bo->editarFormaPagamentoDistribuicao($fpdTO);
	}
		
	/**
	 * Metodo que Edita a Forma de Pagamento da Parcela
	 * @param FormaPagamentoParcelaTO $fppTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO){
		return $this->bo->editarFormaPagamentoParcela($fppTO);
	}
	
	/**
	 * Metodo que edita a relacao da forma de pagamento com a aplicacao
	 * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO){
		return $this->bo->editarFormaPagamentoAplicacaoRelacao($fparTO);
	}
	
	/**
	 * Metodo que deleta a relacao da forma de pagamento com a aplicacao
	 * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO){
		return $this->bo->deletarFormaPagamentoAplicacaoRelacao($fparTO);
	}
	
	/**
	 * Metodo que Exclui as Aplicações da Forma de Pagamento
	 * @param FormaPagamentoAplicacoesTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO){
		return $this->bo->deletarFormaPagamentoAplicacoes($fpaTO);
	}
	
	/**
	 * Metodo que Exclui a Forma do Meio de Pagamento
	 * @param FormaMeioPagamentoTO $fmpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO){
		return $this->bo->deletarFormaMeioPagamento($fmpTO);
	}
	
	/**
	 * Metodo que Exclui a Forma de Pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->deletarFormaPagamento($fpTO);
	}
	
	/**
	 * Metodo que Exclui a Distribuição do Meio de Pagamento
	 * @param FormaPagamentoDistribuicaoTO $fpdTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO){
		return $this->bo->deletarFormaPagamentoDistribuicao($fpdTO);
	}
	
	/**
	 * Metodo que Exclui as Campanhas da Forma de Pagamento
	 * @param CampanhaFormaPagamentoTO $cfpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO){
		return $this->bo->deletarCampanhaFormaPagamento($cfpTO);
	}
		
	/**
	 * Metodo que Exclui a Forma de Pagamento da Parcela
	 * @param FormaPagamentoParcelaTO $fppTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO){
		return $this->bo->deletarFormaPagamentoParcela($fppTO);
	}
	
	/**
	 * Metodo que retorna a Forma de Pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->retornarFormaPagamento($fpTO);
	}
	
	/**
	 * Metodo que retorna a Forma de Pagamento por Aplicação
	 * @param FormaPagamentoTO $fpTO
	 * @param FormaPagamentoAplicacaoTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoPorAplicacao(FormaPagamentoTO $fpTO,FormaPagamentoAplicacaoTO $fpaTO){
		return $this->bo->retornarFormaPagamentoPorAplicacao($fpTO, $fpaTO);
	}
	
	/**
	 * Metodo que retorna as Aplicações da Forma de Pagamento
	 * @param FormaPagamentoAplicacoesTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoAplicacoes(FormaPagamentoAplicacoesTO $fpaTO){
		return $this->bo->retornarFormaPagamentoAplicacoes($fpaTO);
	}
	
	/**
	 * Metodo que retorna as Aplicações
	 * @param FormaPagamentoAplicacoesTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAplicacoesPagamento(AplicacoesPagamentoTO $apTO){
		return $this->bo->retornarAplicacoesPagamento($apTO);
	}
	
	/**
	 * @deprecated
	 * Metodo que retorna a Distribuição da Forma de Pagamento
	 * @param FormaPagamentoDistribuicaoTO $fpdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoDistribuicao(FormaPagamentoDistribuicaoTO $fpdTO){
		return $this->bo->retornarAplicacoesPagamento($fpdTO);
	}
	
	/**
	 * Metodo que retorna a Forma do Meio de Pagamento
	 * @param FormaMeioPagamentoTO $fmpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaMeioPagamento(FormaMeioPagamentoTO $fmpTO){
		return $this->bo->retornarFormaMeioPagamento($fmpTO);
	}
	
	/**
	 * Metodo que retorna o de Meio de Pagamento
	 * @param MeioPagamentoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMeioPagamento(MeioPagamentoTO $mpTO){
		return $this->bo->retornarMeioPagamento($mpTO);
	}
	
	/**
	 * Metodo que retorna o de Meio de Pagamento
	 * @param VwMeioPagamentoIntegracaoTO $vwmpiTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMeioPagamentoIntegracao(VwMeioPagamentoIntegracaoTO $vwmpiTO){
		return $this->bo->retornarVwMeioPagamentoIntegracao($vwmpiTO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Divisão Financeira
	 * @param VwFormaMeioDivisaoTO $fmdfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDivisaoFinanceira(VwFormaMeioDivisaoTO $fmdfTO){
		return $this->bo->retornarTipoDivisaoFinanceira($fmdfTO);
	}
	
	/**
	 * Metodo que retorna a Campanha da Forma de Pagamento
	 * @param CampanhaFormaPagamentoTO $cfpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaFormaPagamento(CampanhaFormaPagamentoTO $cfpTO){
		return $this->bo->retornarCampanhaFormaPagamento($cfpTO);
	}
			
	/**
	 * Metodo que retorna o Tipo de Forma de Pagamento Parcela
	 * @param TipoFormaPagamentoParcelaTO $tfppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoFormaPagamentoParcela(TipoFormaPagamentoParcelaTO $tfppTO){
		return $this->bo->retornarTipoFormaPagamentoParcela($tfppTO);
	}
		
	/**
	 * Metodo que Retorna a Forma de Pagamento da Parcela
	 * @param FormaPagamentoParcelaTO $fppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoParcela(FormaPagamentoParcelaTO $fppTO){
		return $this->bo->retornarFormaPagamentoParcela($fppTO);
	}
	/**
	 * 
	 * Retorna os vinculos de parcela meio pagamento
	 * @param FormaPagamentoTO $fpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarParcelasMeioPagamento(FormaPagamentoTO $fpTO){
		return $this->bo->retornarParcelasMeioPagamento($fpTO);
	}
	
	/**
	 * Metodo que retorna a relação da aplicação da forma de pagamento 
	 * @param FormaPagamentoAplicacaoTO $fpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoAplicacao(FormaPagamentoAplicacaoTO $fpaTO){
		return $this->bo->retornarFormaPagamentoAplicacao($fpaTO);
	}
	
	/**
	 * Metodo que retorna a relação da aplicação da forma de pagamento 
	 * @param FormaPagamentoAplicacaoRelacaoTO $fparTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaPagamentoAplicacaoRelacao(FormaPagamentoAplicacaoRelacaoTO $fparTO){
		return $this->bo->retornarFormaPagamentoAplicacaoRelacao($fparTO);
	}
	
	/**
	 * Método que retorna os dias de vencimento da forma de pagamento
	 * @param FormaDiaVencimentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaDiaVencimento(FormaDiaVencimentoTO $to){
		return $this->bo->retornarFormaDiaVencimento($to);
	}
	
	/**
	 * Método que retorna os dias de vencimento do meio de pagamento
	 * @param FormaMeioVencimentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaMeioVencimento(FormaMeioVencimentoTO $to){
		return $this->bo->retornarFormaMeioVencimento($to);
	}
	
	/**
	 * Método que retorna os dias de vencimento do meio de pagamento com mais dados
	 * @param FormaMeioVencimentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarFormaMeioVencimentoCompleto(FormaMeioVencimentoTO $to){
		return $this->bo->retornarFormaMeioVencimentoCompleto($to);
	}
	
}
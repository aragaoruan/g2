<?php
/**
 * Classe de comunicação com Flex para interações com objeto ConfiguracaoEntidadeRO
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @since 2011-06-10
 * @package application
 * @subpackage ro
 */
class ConfiguracaoEntidadeRO extends Ead1_RO
{
	private $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new ConfiguracaoEntidadeBO();
	}
	
	/**
	 * Define se é para cadastrar ou atualizar o texto do sistema
	 * @param TextoSistemaTO $textoSistemaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarTextoSistema(TextoSistemaTO $textoSistemaTO){
		return $this->bo->salvarTextoSistema($textoSistemaTO);
	}
	
	/**
	 * Salva uma lista de EmailEntidadeMensagemTO
	 * Todos os itens devem ser da mesma entidade
	 * @param array $arrEmailEntidadeMensagemTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrEmailEntidadeMensagem( array $arrEmailEntidadeMensagemTO ){
		return $this->bo->salvarArrEmailEntidadeMensagem( $arrEmailEntidadeMensagemTO );
	}
	
	/**
	 * Metodo que salva a configuracao financeira da entidade
	 * @param EntidadeFinanceiroTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntidadeFinanceiro(EntidadeFinanceiroTO $to){
		return $this->bo->salvarEntidadeFinanceiro($to);
	}
	
	/**
	 * Metodo que salva a configuracao da entidade
	 * @param ConfiguracaoEntidadeTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarConfiguracaoEntidade(ConfiguracaoEntidadeTO $to){
		return $this->bo->salvarConfiguracaoEntidade($to);
	}
	
	/**
	 * Retorna o(s) texto(s) do sistema
	 * @param VwTextoSistemaTO $vwTextoSistemaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoSistema(VwTextoSistemaTO $vwTextoSistemaTO){
		return $this->bo->retornarTextoSistema($vwTextoSistemaTO);
	}
	
	/**
	 * Retorna a exibição do texto
	 * @param TextoExibicaoTO $textoExibicaoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoExibicao(TextoExibicaoTO $textoExibicaoTO){
		return $this->bo->retornarTextoExibicao($textoExibicaoTO);
	}
	
	/**
	 * Retorna a categoria do texto
	 * @param TextoCategoriaTO $textoCategoriaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoCategoria(TextoCategoriaTO $textoCategoriaTO){
		return $this->bo->retornarTextoCategoria($textoCategoriaTO);
	}
	
	/**
	 * Retorna a exibição da categoria do texto
	 * @param TextoExibicaoTO $textoExibicaoCategoriaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTextoExibicaoCategoria(TextoExibicaoTO $textoExibicaoCategoriaTO){
		return $this->bo->retornarTextoExbicaoCategoria($textoExibicaoCategoriaTO);
	}
	
	/**
	 * 
	 * @param OrientacaoTextoTO $orientacaoTextoTO
	 */
	public function retornarOrientacaoTexto( OrientacaoTextoTO $orientacaoTextoTO ) {
		return $this->bo->retornarOrientacaoTexto( $orientacaoTextoTO );
	}  
	
	/**
	 * Retorna as mensagens padrões do sistema
	 * @param MensagemPadraoTO $mensagemPadraoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMensagemPadrao( MensagemPadraoTO $mensagemPadraoTO ) {
		return $this->bo->retornarMensagemPadrao( $mensagemPadraoTO );
	}
	
	/**
	 * @param EmailEntidadeMensagemTO $emailEntidadeMensagemTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEmailEntidadeMensagem( EmailEntidadeMensagemTO $emailEntidadeMensagemTO ){
		return $this->bo->retornarEmailEntidadeMensagem( $emailEntidadeMensagemTO );
	}
	
	/**
	 * Metodo que retorna a configuração da entidade 
	 * @param ConfiguracaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarConfiguracaoEntidade(ConfiguracaoEntidadeTO $ceTO){
		return $this->bo->retornarConfiguracaoEntidade( $ceTO );
	}
	
	/**
	 * Metodo que retorna modelo de grade de notas
	 * @param ModeloGradeNotasTO $modeloGradeNotas
	 * @return Ead1_Mensageiro
	 */
	public function retornarModeloGradeNotas(ModeloGradeNotasTO $modeloGradeNotas){
		return $this->bo->retornarModeloGradeNotas( $modeloGradeNotas );
	}
	
	/**
	 * Metodo que retorna a configuração financeira da entidade 
	 * @param EntidadeFinanceiroTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntidadeFinanceiro(EntidadeFinanceiroTO $to){
		return $this->bo->retornarEntidadeFinanceiro($to);
	}
	
	/**
	 * Metodo que retorna o upload do manual do aluno
	 * @param UploadTO $uploadTO
	 */
	public function retornarUploadManualAluno(UploadTO $uploadTO){
		return $this->bo->retornarUploadManualAluno($uploadTO);
	}
	
	/**
	 * Metodo que retorna o upload da tabela preco
	 * @param UploadTO $uploadTO
	 */
	public function retornarUploadTabelaPreco(UploadTO $uploadTO){
		return $this->bo->retornarUploadTabelaPreco($uploadTO);
	}
	
	
	/**
	 * Método que salva o upload da tabela de preços
	 * @param ArquivoTO $arquivo
	 * @return Ead1_Mensageiro
	 */
	public function salvarTabelaPreco(ArquivoTO $arquivo){
		$entidadeBO	= new EntidadeBO();
		return $entidadeBO->salvarTabelaPreco($arquivo);
	}
}

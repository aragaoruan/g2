<?php

class ProtocoloRO {

	/**
	 * @var ProtocoloBO
	 */
	private $bo;
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new ProtocoloBO();
	}
	
	/**
	 * Metodo que cadastra protocolo
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProtocolo(ProtocoloTO $pTO){
		return $this->bo->cadastrarProtocolo($pTO);
	}
	
	/**
	 * Metodo que retorna protocólo
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProtocolo(ProtocoloTO $pTO){
		return $this->bo->retornarProtocolo($pTO);
	}
	
	/**
	 * Metodo que retorna protocólo com o nome do usuário
	 * @param ProtocoloTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProtocoloUsuario(ProtocoloTO $pTO){
		return $this->bo->retornarProtocoloUsuario($pTO);
	}
}

?>
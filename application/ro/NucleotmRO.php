<?php

class NucleotmRO extends Ead1_RO {

	/**
	 * @var NucleotmBO
	 */
	private $bo;
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new NucleotmBO();
	}
	
	/**
	 * Metodo que decide se edita ou cadastra núcleo
	 * @param NucleotmTO $nTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarNucleo(NucleotmTO $nTO){
		if($nTO->getId_nucleotm()){
			return $this->editarNucleo($nTO);
		}
		return $this->cadastrarNucleo($nTO);
	}
	
	/**
	 * Metodo que decide se edita ou cadastra funcionário
	 * @param NucleoFuncionariotmTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO){
		if($nfTO->getId_nucleofuncionariotm()){
			return $this->editarFuncionarioNucleo($nfTO);
		}
		return $this->cadastrarFuncionarioNucleo($nfTO);
	}
	
	/**
	 * Metodo que cadastra núcleo
	 * @param NucleotmTO $nTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarNucleo(NucleotmTO $nTO){
		return $this->bo->cadastrarNucleo($nTO);
	}
	
	/**
	 * Metodo que edita núcleo
	 * @param NucleotmTO $nTO
	 * @return Ead1_Mensageiro
	 */
	public function editarNucleo(NucleotmTO $nTO){
		return $this->bo->editarNucleo($nTO);
	}
	
	/**
	 * Metodo que cadastra funcionário de núcleo
	 * @param NucleoFuncionariotmTO $nfTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO){
		return $this->bo->cadastrarFuncionarioNucleo($nfTO);
	}
	
	/**
	 * Metodo que cadastra produtos de núcleo
	 * @param NucleotmTO $nTO
	 * @param array $arrNucleoProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProdutoNucleo(NucleotmTO $nTO, $arrNucleoProduto){
		return $this->bo->cadastrarProdutoNucleo($nTO, $arrNucleoProduto);
	}
	
	/**
	 * Metodo que edita funcionário de núcleo
	 * @param NucleoFuncionariotmTO $nfTO
	 * @return Ead1_Mensageiro
	 */
	public function editarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO){
		return $this->bo->editarFuncionarioNucleo($nfTO);
	}
	
	/**
	 * Metodo que retorna núcleo
	 * @param NucleotmTO $nTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarNucleoCompleto(NucleotmTO $nTO){
		return $this->bo->retornarNucleoCompleto($nTO);
	}
	
	/**
	 * Método que retorna os nucleos
	 * @param NucleotmTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarNucleo(NucleotmTO $to){
		$mensageiro = new Ead1_Mensageiro();
		try{
			$dados = $this->bo->retornarNucleo($to);
			$mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new NucleotmTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$mensageiro->setMensageiro("Erro ao Retornar Núcleo!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $mensageiro;
	}
	
	/**
	 * Método que retorna os nucleos da funcao da pessoa logada
	 * @param NucleotmTO $nucleoTO
	 * @param FuncaoTO $funcaoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarNucleoFuncao(NucleotmTO $nucleoTO,FuncaoTO $funcaoTO){
		$mensageiro = new Ead1_Mensageiro();
		try{
			$dados = $this->bo->retornarNucleoFuncao($nucleoTO,$funcaoTO);
			$mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($dados, new NucleotmTO()),Ead1_IMensageiro::SUCESSO);
		}catch (Zend_Validate_Exception $e){
			$mensageiro->setMensageiro($e->getMessage(),Ead1_IMensageiro::AVISO);
		}catch (Zend_Exception $e){
			$mensageiro->setMensageiro("Erro ao Retornar Núcleo!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
		return $mensageiro;
	}
	
	/**
	 * Metodo que retorna produtos de núcleo
	 * @param NucleoProdutoTO $npTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoNucleo(NucleoProdutoTO $npTO){
		return $this->bo->retornarProdutoNucleo($npTO);
	}
	
	/**
	 * Metodo que retorna funcionário de núcleo
	 * @param NucleoFuncionariotmTO $nfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncionarioNucleo(NucleoFuncionariotmTO $nfTO){
		return $this->bo->retornarFuncionarioNucleo($nfTO);
	}
	
	/**
	 * Metodo que retorna funcionários do núcleo de uma determinada entidade
	 * @param NucleoFuncionariotmTO $nfTO
	 * @param EntidadeTO $eTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarFuncionarioNucleoEntidade(NucleoFuncionariotmTO $nfTO,EntidadeTO $eTO) {
		return $this->bo->retornarFuncionarioNucleoEntidade($nfTO,$eTO);
	}
	
	
}

?>
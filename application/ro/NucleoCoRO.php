<?php

class NucleoCoRO extends Ead1_RO {
	
	/**
	 * @var NucleoCoBO
	 */
	private $bo;
	
	/**
	 * Construtor 
	 */
	public function __construct(){
		$this->bo = new NucleoCoBO();
	}
	
	
	/**
	 * @param NucleoCoTO $nucleo
	 * @return Ead1_Mensageiro
	 */
	public function salvarNucleo(NucleoCoTO $nucleo){
		return $this->bo->salvarNucleo($nucleo);
	}
	
	/**
	 * Método que retorna os dados da VwNucleoCO
	 * @param VwNucleoCoTO $nucleo
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwNucleo(VwNucleoCoTO $vwnucleo){
		return $this->bo->retornaVwNucleo($vwnucleo);
	}
	/**
	 * Método que deleta de forma lógica o núcleo
	 * @param array|NucleoCoTO $nucleo
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function deletarNucleo($nucleo){
		return $this->bo->deletarNucleo($nucleo);
	}
	/**
	 * @param VwNucleoAssuntoCoTO $nucleoassunto
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwNucleoAssunto(VwNucleoAssuntoCoTO $nucleoassunto){
		return $this->bo->retornaVwNucleoAssunto($nucleoassunto);
	}
	
	/**
	 * Método que retorna NucleoCoTO
	 * @param NucleoCoTO $nucleo
	 * @return Ead1_Mensageiro
	 */
	public function listarNucleoCo(NucleoCoTO $nucleo){
		return $this->bo->listarNucleoCo($nucleo);
	}
	
	/**
	 * Método que retorna os Assuntos que podem ser adicionados a um núcleo
	 * @param VwNucleoAssuntoCoTO $vwNucleoAssunto
	 * @return Ead1_Mensageiro
	 */
	public function listarAssuntosNucleoAdicionar(VwNucleoAssuntoCoTO $vwNucleoAssunto){
		return $this->bo->listarAssuntosNucleoAdicionar($vwNucleoAssunto);
	}
	
	/**
	 * Método que cadastra os assuntos para o núcleo
	 * @param array $arAssuntos
	 * @param NucleoCoTO $nucleoCoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAssuntosNucleo(array $arAssuntos, NucleoCoTO $nucleoCoTO){
		return $this->bo->salvarAssuntosNucleo($arAssuntos, $nucleoCoTO);
	}
	
	/**
	 * Método que exclui o vínculo de um Assunto com o Núcleo
	 * @param VwNucleoAssuntoCoTO $vwNucleoAssunto
	 * @return Ead1_Mensageiro
	 */
	public function excluirNucleoAssunto(VwNucleoAssuntoCoTO $vwNucleoAssunto){
		return $this->bo->excluirNucleoAssuntoCo($vwNucleoAssunto);
	}
	
	/**
	 * Método que busca os responsáveis por um Assunto
	 * @param VwNucleoPessoaCoTO $vwNucleoPessoa
	 * @return Ead1_Mensageiro
	 */
	public function retornaResponsaveisAssunto(VwNucleoPessoaCoTO $vwNucleoPessoa){
		return $this->bo->retornaResponsaveisAssunto($vwNucleoPessoa);
	}
	
	
	/**
	 * Salva Pessoa como responsavel ao atendente no Nucleo
	 * @param NucleoPessoaCoTO $nucleoPessoaCo
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvarNucleoPessoaCo(NucleoPessoaCoTO $nucleoPessoaCo){
		return $this->bo->salvarNucleoPessoaCo($nucleoPessoaCo);
	}
	
	
	/**
	 * Método que exclui vincula da Pessoa com o núcleo
	 * @param NucleoPessoaCoTO $nucleoPessoaCo
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 * @see NucleoCoRO::excluirPessoaNucleo
	 */
	public function excluirPessoaNucleo(NucleoPessoaCoTO $nucleoPessoaCo){
		return $this->bo->excluirPessoaNucleo($nucleoPessoaCo);
	}
	
	
	/**
	 * Lista Emails do NucleoCo
	 * @param VwNucleoCoEmailTO $nucleoCoEmail
	 * @return Ead1_Mensageiro
	 */
	public function retornaEmail(VwNucleoCoEmailTO $nucleoEmailCo){
		return $this->bo->retornaEmail($nucleoEmailCo);
	}
	
	
	/**
	 * Vincula e-mails ao núcleo
	 * @param array $arEmail
	 * @param NucleoCoEmailTO $nucleoEmail
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvaEmail(array $arEmail, NucleoCoEmailTO $nucleoEmail){
		return $this->bo->salvarEmails($arEmail,$nucleoEmail);
	}
	
	
	/**
	 * Exclui Vinculo do e-mail com o núcleo
	 * @param NucleoCoEmailTO $nucleoEmail
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function excluirEmail(NucleoCoEmailTO $nucleoEmail){
		return $this->bo->excluirEmail($nucleoEmail);
	}
	
	
	/**
	 * Retorna VwCategoriaOcorrencia
	 * @param VwCategoriaOcorrenciaTO $categoriaOcorrenciaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaTipoOcorrencia(VwCategoriaOcorrenciaTO $categoriaOcorrencia){
		return $this->bo->retornaCategoriaOcorrencia($categoriaOcorrencia);
	}
	
	/**
	 * Método que decide se edita/altera/reativa CategoriaOCorrencia
	 * @param CategoriaOcorrenciaTO $categoria
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvarCategoriaOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		return $this->bo->salvarCategoriaOcorrencia($categoriaOcorrencia);
	}
	
	/**
	 * Método que exclui de forma lógica uma CategoriaOCorrencia
	 * @param CategoriaOcorrenciaTO $categoria
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function excluirCategoriaOcorrencia(CategoriaOcorrenciaTO $categoriaOcorrencia){
		return $this->bo->excluirCategoriaOcorrencia($categoriaOcorrencia);
	}
	
	/**
	 * Lista Evoluções relacionadas ao núcleo
	 * @param NucleoEvolucaoCoTO $nucleoEvolucao
	 * @return Ead1_Mensageiro
	 */
	public function listarNucleoEvolucaoCo(NucleoEvolucaoCoTO $nucleoEvolucao){
		return $this->bo->retornaNucleoEvolucaoCo($nucleoEvolucao);
	}
	
	
	/**
	 * Método que deleta as evoluções relacionadas ao núcleo e salva novamente
	 * @param array $nucleosEvolucao
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function salvarNucleoEvolucaoCo(array $nucleoEvolucaoCo){
		return $this->bo->salvarNucleoEvolucaoCo($nucleoEvolucaoCo);
	}
	
	/**
	 * Método que retornam as finalidades do nucleo (NucleoFinalidadeTO)
	 * @param NucleoFinalidadeTO $nucleoFinalidade
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornarNucleoFinalidadeTO(NucleoFinalidadeTO $nucleoFinalidade){
		return $this->bo->retornarNucleoFinalidadeTO($nucleoFinalidade);
	}
	
	/**
	 * Método que retorna objetos da VwNucleoOcorrenciaTO retornados pela vw_nucleoocorrencia
	 * @param VwNucleoOcorrenciaTO $vw
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornarVwNucleoOcorrenciaTO(VwNucleoOcorrenciaTO $vw){
		return $this->bo->retornarVwNucleoOcorrenciaTO($vw);
	}
	

	/**
	 * Retorna as pesoas vinculadas a um núcleo
	 * @param VwPessoasNucleoAssuntoTO $vw
	 * @return Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornaPessoasNucleoAssunto(VwPessoasNucleoAssuntoTO $vw){
		return $this->bo->retornaPessoasNucleoAssunto($vw);
	}


    /**
     * @param VwAssuntoNucleoPessoaTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornaFuncoesPessoaNucleo(VwAssuntoNucleoPessoaTO $vw){
        return $this->bo->retornaFuncoesPessoaNucleo($vw);
    }

    /**
     * @param array $arrTo
     * @return Ead1_Mensageiro
     */
    public function salvarArrayFuncoes(array $arrTo){
        return $this->bo->salvarArrayFuncoes($arrTo);
    }

    /**
     * @param VwPessoaTO $vwPessoa
     * @param VwEntidadeCompartilhaDadosTO $compartilhaTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuarioEntidade(VwPessoaTO $vwPessoa, VwEntidadeCompartilhaDadosTO $compartilhaTO){
        return $this->bo->verificaUsuarioEntidade($vwPessoa, $compartilhaTO);
    }

    /**
     * @param VwPessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoas(VwPessoaTO $pTO){
        return $this->bo->retornaPessoas($pTO);
    }
}

?>
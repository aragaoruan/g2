<?php
/**
 * Classe de comunicação com o Flex para interações de Mensagem Motivacional.
 * 
 * @author Domício de Araújo Medeiros - domicio.medeiros@gmail.com
 */
class MensagemMotivacionalRO extends Ead1_RO{
	
	private $bo;
	
	/**
	 * Método construtor
	 */
	public function __construct() {
		$this->bo = new MensagemMotivacionalBO();
	}
	
	/**
	 * Método que cadastra uma Mensagem Motivacional
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastraMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->cadastraMensagemMotivacional($msgMotTO);
	}
	
	/**
	 * Método que retorna uma Mensagem Motivacional
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->retornaMensagemMotivacional($msgMotTO);
	}
	
	/**
	 * Método que edita uma Mensagem Motivacional
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function editarMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->editarMensagemMotivacional($msgMotTO);
	}
	
	/**
	 * Metodo que retorna uma mensagem motivacional aletatória
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaMensagemMotivacionalAleatoria(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->retornaMensagemMotivacionalAleatoria($msgMotTO);
	}
	
	/**
	 * Método que exclui uma Mensagem Motivacional
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->excluirMensagemMotivacional($msgMotTO);
	}
	
	/**
	 * Método que salva uma Mensagem Motivacional
	 * 
	 * @param MensagemMotivacionalTO $msgMotTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarMensagemMotivacional(MensagemMotivacionalTO $msgMotTO) {
		return $this->bo->salvarMensagemMotivacional($msgMotTO);
	}
}
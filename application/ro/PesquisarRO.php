<?php

/**
 * Classe de comunicação com Flex para interações com objeto Pesquisar
 * @author Eduardo Romão - ejushiro@gmail.com
 *
 * @package application
 * @subpackage ro
 */
class PesquisarRO extends Ead1_RO
{


    /**
     * Metodo que retorna os dados da Pesquisa de Avaliacao
     * @param PesquisarAvaliacao $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTipoRecuperacao(PesquisarTipoRecuperacaoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisaTipoRecuperacao($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Avaliacao
     * @param PesquisarTurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTurma(PesquisarTurmaTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisarTurma($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de atendimentos da venda
     * @param PesquisarVendaAtendimentoTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarVendaAtendimento(PesquisarVendaAtendimentoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarVendaAtendimento($to);
    }


    /**
     * Metodo que retorna os dados da Pesquisa de Tipo de Avaliacao
     * @param PesquisarTipoAvaliacao $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTipoAvaliacao(PesquisarTipoAvaliacaoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisaTipoAvaliacao($to);
    }


    /**
     * Metodo que retorna os dados da Pesquisa de Avaliacao
     * @param PesquisaAvaliacaoTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarAvaliacao(PesquisaAvaliacaoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisaAvaliacao($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Agendamento de Avaliacao
     * @param PesquisaAvaliacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarAgendamentoAvaliacao(PesquisaAvaliacaoAgendamentoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarAgendamentoAvaliacao($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de aplicacao de Avaliacao
     * @param PesquisaAvaliacaoAgendamentoTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarAplicacaoAvaliacao(PesquisaAvaliacaoAplicacaoTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarAplicacaoAvaliacao($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de textos do sistema
     * @param PesquisarTextoSistemaTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTextosSistema(PesquisarTextoSistemaTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarTextosSistema($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de pessoa juridica
     * @param PesquisaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarPessoaJuridica(PesquisaEntidadeTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarPessoaJuridica($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de protocolo
     * @param PesquisarProtocoloTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarProtocolo(PesquisarProtocoloTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarProtocolo($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Item de Material
     * @param PesquisarTipoMaterial $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarItemMaterial(PesquisarItemMaterialTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisaItemMaterial($to);
    }

    /**
     * Metodo que retorna os dados da Pesquisa de Tipo de Material
     * @param PesquisarTipoMaterial $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarTipoMaterial(PesquisarTipoMaterialTO $to)
    {
        $bo = new PesquisarBO();
        return $bo->retornarPesquisaTipoMaterial($to);
    }

    /**
     * Metodo que retorna os dados da pesquisaProduto
     * @param PesquisarProduto $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarProduto(PesquisarProdutoTO $to)
    {
        $bo = new PesquisarBO();
        $to->setBl_ativo(1);
        return $bo->retornarPesquisaProduto($to);
    }


    /**
     * Metodo que retorna dados da pesquisa de pessoa
     * @param PesquisarPessoaTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarPessoa(PesquisarPessoaTO $to)
    {
//        $bo = new PesquisarBO();
//        return $bo->retornaPesquisaPessoa($to);

        $mensageiro = new Ead1_Mensageiro();
        $negocio = new \G2\Negocio\Pessoa();

        $result = $negocio->retornaPesquisarPessoa($to->toArray());
        if ($result) {
            foreach ($result as $i => $pessoa) {
                $result[$i] = $negocio->entityToTO($pessoa);
            }
            $mensageiro->setMensageiro($result, Ead1_IMensageiro::SUCESSO);
        }

        return $mensageiro;
    }

    /**
     * Metodo que retorna dados de pesquisa de perfis da entidade
     * @param PerfilTO $pTO
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarPerfilEntidade(PerfilTO $pTO, PerfilEntidadeTO $peTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarPerfilEntidade($pTO, $peTO);
    }

    /**
     * Metodo que retorna dados de Pesquisa de Organizações (Grupo, Instituição, Nucleo, Polo)
     * @param PesquisarOrganizacaoTO $poTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarOrganizacao(PesquisarOrganizacaoTO $poTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarOrganizacao($poTO);
    }

    /**
     *
     * Método que retorna a pesquisa de documentos
     * @param DocumentosTO $docTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarDocumentos(DocumentosTO $docTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarDocumentos($docTO);
    }

    /**
     * MEtodo que retorna dados da Pesquisa do Fundamento Legal
     * @param PesquisarFundamentoLegalTO $pflTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarFundamentoLegal(PesquisarFundamentoLegalTO $pflTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarFundamentoLegal($pflTO);
    }

    /**
     * Método que retorna dados da Pesquisa de Afiliado
     * @param PesquisarContratoAfiliadoTO $pcaTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarContratoAfiliado(PesquisarContratoAfiliadoTO $pcaTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarContratoAfiliado($pcaTO);
    }

    /**
     * Método que pesquisa as disciplinas
     * @param PesquisarDisciplinaTO $pdTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarDisciplina(PesquisarDisciplinaTO $pdTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarDisciplina($pdTO);
    }

    /**
     * Método que pesquisa as áreas de conhecimento
     * @param PesquisarAreaTO $paTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarArea(PesquisarAreaTO $paTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarArea($paTO);
    }

    /**
     * Método que pesquisa as Entidades filhas de outra entidade
     * @param VwEntidadeClasseTO $ecTO
     * @return Ead1_Mensageiro
     * @todo Verificar se esse método funciona
     */
    public function pesquisarEntidadeFilha(VwEntidadeClasseTO $ecTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarEntidadeFilha($ecTO);
    }

    /**
     * Método que pesquisa as salas de aula de uma entidade
     * @param PesquisarSalaDeAulaTO $psaTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarSalaDeAula(PesquisarSalaDeAulaTO $psaTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarSalaDeAula($psaTO);
    }

    /**
     * Método que pesquisa o aproveitamento de uma entidade
     * @param PesquisarAproveitamentoTO $paTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarAproveitamento(PesquisarAproveitamentoTO $paTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarAproveitamento($paTO);
    }

    /**
     * Método que pesquisa campanha
     * @param PesquisarCampanhaComercialTO $pccTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarCampanhaComercial(PesquisarCampanhaComercialTO $pccTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarCampanhaComercial($pccTO);
    }

    /**
     * Método que pesquisa o conjunto de avaliacao
     * @param PesquisarAvaliacaoConjunto $pcaTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarConjuntoAvaliacao(PesquisarAvaliacaoConjuntoTO $pcaTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarConjuntoAvaliacao($pcaTO);
    }

    /**
     * Retorna o professor
     * @param VwPesquisarProfessorTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarProfessor(VwPesquisarProfessorTO $ppTO)
    {
        $bo = new PesquisarBO();
        return $bo->retornaProfessor($ppTO);
    }

    /**
     *
     * Método que pesquisa os projetos pedagógicos
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    //FIXME REMOVER A MONTAGEM DO WHERE FIXA
    public function pesquisarProjetoPedagogico(PesquisarProjetoPedagogicoTO $pppTO)
    {
        $mensageiro = new Ead1_Mensageiro();
        try {

            $dao = new ProjetoPedagogicoDAO();
            $eTO = new EntidadeTO();
            $ppTO = new ProjetoPedagogicoTO();

            $ppTO->setId_projetopedagogico($pppTO->getId_projetopedagogico());
            $ppTO->setId_situacao($pppTO->getId_situacao());
            $ppTO->setSt_descricao($pppTO->getSt_descricao());
            $ppTO->setSt_nomeversao($pppTO->getSt_nomeversao());
            $ppTO->setSt_projetopedagogico($pppTO->getSt_projetopedagogico());
            $ppTO->setSt_tituloexibicao($pppTO->getSt_tituloexibicao());

            if (!$ppTO->getId_entidadecadastro()) {
                if (!$eTO->getId_entidade()) {
                    $eTO->setId_entidade($eTO->getSessao()->id_entidade);
                }
                $ppTO->setId_entidadecadastro($eTO->getId_entidade());
            }

            $where = ' id_entidadecadastro = ' . $ppTO->id_entidadecadastro;
            $where .= " AND ( st_projetopedagogico like '%" . $ppTO->st_projetopedagogico . "%' OR st_descricao like '%" . $ppTO->st_descricao . "%' )";

            $arrProjeto = $dao->retornarVwProjetoPedagogico($ppTO, $where)->toArray();
            if (empty($arrProjeto)) {
                return $mensageiro->setMensageiro('Nenhum Registro Encontrado!', Ead1_IMensageiro::AVISO);
            }
            $mensageiro->setMensageiro(Ead1_TO_Dinamico::encapsularTo($arrProjeto, new PesquisarProjetoPedagogicoTO()), Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            $mensageiro->setMensageiro('Erro ao Retornar Projeto Pedagógico.', Ead1_IMensageiro::ERRO, $e->getMessage());
        }

        return $mensageiro;
    }

    /**
     *
     * Pesquisa o contrato
     * @param PesquisarContratoTO $pcTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarContrato(PesquisarContratoTO $pcTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarContrato($pcTO);
    }

    /**
     * Pesquisa a regra de contrato
     * @param PesquisaRegraContratoTO $crTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarRegraContrato(PesquisaRegraContratoTO $crTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarRegraContrato($crTO);
    }

    /**
     * Pesquisa relatórios
     * @param PesquisaRelatorioTO $prTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarRelatorio(PesquisaRelatorioTO $prTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarRelatorio($prTO);
    }

    /**
     * Pesquisa a venda
     * @param PesquisarVendaTO pvrTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarVenda(PesquisarVendaTO $pvTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarVenda($pvTO);
    }

    /**
     * Pesquisa a Forma de Pagamento
     * @param PesquisaFormaPagamentoTO $fpTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarFormaDePagamento(PesquisaFormaPagamentoTO $fpTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarFormaDePagamento($fpTO);
    }

    /**
     * Pesquisa de Assunto do Central de Ocorrências
     * @param PesquisarAssuntoCoTO $pesquisaCategoriaTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarAssuntoCo(PesquisarAssuntoCoTO $pesquisaAssuntoCoTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarAssuntoCo($pesquisaAssuntoCoTO);
    }

    /**
     * Somenta para testar o retorno do nivel de ensino
     * @param NivelEnsinoTO $neTO
     * @return Ead1_Mensageiro
     */
    public function retornaNivelEnsino(NivelEnsinoTO $neTO)
    {
        $bo = new PesquisarBO();
        return $bo->retornaNivelEnsino($neTO);
    }

    public function pesquisarMensagemMotivacional(PesquisarMensagemMotivacionalTO $pesquisarMsgMotTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarMensagemMotivacional($pesquisarMsgMotTO);
    }

    public function pesquisarArquivoRetorno(PesquisarArquivoRetornoTO $arTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarArquivoRetorno($arTO);
    }

    /**
     * Pesquisa de Categorias de Produtos do Financeiro
     * @param PesquisarCategoriaTO $pesquisaCategoriaTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarCategoria(PesquisarCategoriaTO $pesquisaCategoriaTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarCategoria($pesquisaCategoriaTO);
    }

    /**
     * Pesquisa de Classes de Afiliados
     * @param PesquisarClasseAfiliadoTO $pesquisaClasseAfiliadoTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarClasseAfiliado(PesquisarClasseAfiliadoTO $pesquisaClasseAfiliadoTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarClasseAfiliado($pesquisaClasseAfiliadoTO);
    }

    /**
     * Pesquisa de Setores
     * @param PesquisarSetorTO $pesquisaSetorTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarSetor(PesquisarSetorTO $pesquisaSetorTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarSetor($pesquisaSetorTO);
    }

    /**
     * Pesquisa de Livros
     * @param PesquisarLivroTO $pesquisalivroTO
     * @return Ead1_Mensageiro
     */
    public function pesquisarLivro(PesquisarLivroTO $pesquisalivroTO)
    {
        $bo = new PesquisarBO();
        return $bo->pesquisarLivro($pesquisalivroTO);
    }
}
<?php

class ComissaoRO extends Ead1_RO {

	/**
	 *@var ComissaoBO
	 */
	private $bo;
	
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new ComissaoBO();
	}
	
	/**
	 * Metodo que retorna comissao com função
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissaoEntidadeFuncao(ComissaoEntidadeTO $ceTO){
		return $this->bo->retornarComissaoEntidadeFuncao($ceTO);
	}
	
	/**
	 * Metodo que retorna comissao
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		return $this->bo->retornarComissaoEntidade($ceTO);
	}
	
	/**
	 * Metodo que retorna tipo de comissão
	 * @param TipoComissaoTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoComissao(TipoComissaoTO $tcTO){
		return $this->bo->retornarTipoComissao($tcTO);
	}
	
	/**
	 * Metodo que retorna tipo de meta
	 * @param TipoMetaTO $tmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoMeta(TipoMetaTO $tmTO){
		return $this->bo->retornarTipoMeta($tmTO);
	}
	
	/**
	 * Metodo que retorna tipo de valor de comissão
	 * @param TipoValorComissaoTO $tvcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoValorComissao(TipoValorComissaoTO $tvcTO){
		return $this->bo->retornarTipoValorComissao($tvcTO);
	}
	
	/**
	 * Metodo que retorna base de cálculo de comissão.
	 * @param BaseCalculoComTO $bccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarBaseCalculoCom(BaseCalculoComTO $bccTO){
		return $this->bo->retornarBaseCalculoCom($bccTO);
	}
	
	/**
	 * Metodo que retorna tipo de cálculo de comissão
	 * @param TipoCalculoComTO $tccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCalculoCom(TipoCalculoComTO $tccTO){
		return $this->bo->retornarTipoCalculoCom($tccTO);
	}
	
	/**
	 * Metodo que retorna regra de comissão
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarRegraComissao(RegraComissaoTO $rcTO){
		return $this->bo->retornarRegraComissao($rcTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita comissão
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		if($ceTO->getId_comissaoentidade()){
			return $this->editarComissaoEntidade($ceTO);
		}
		return $this->cadastrarComissaoEntidade($ceTO);
	}
	
	/**
	 * Metodo que decide se cadastra ou edita regra de comissão
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarRegraComissao(RegraComissaoTO $rcTO){
		if($rcTO->getId_regracomissao()){
			return $this->editarRegraComissao($rcTO);
		}
		return $this->cadastrarRegraComissao($rcTO);
	}
	
	/**
	 * Metodo que cadastra comissão
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		return $this->bo->cadastrarComissaoEntidade($ceTO);
	}
	
	/**
	 * Metodo que cadastra regra de comissão
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarRegraComissao(RegraComissaoTO $rcTO){
		return $this->bo->cadastrarRegraComissao($rcTO);
	}
	
	/**
	 * Metodo que cadastra as regras de comissão
	 * @param array $arrayRegras
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArrayRegraComissao($arrayRegras){
		return $this->bo->cadastrarArrayRegraComissao($arrayRegras);
	}
	
	/**
	 * Metodo que edita comissão
	 * @param ComissaoEntidadeTO $ceTO
	 * @return Ead1_Mensageiro
	 */
	public function editarComissaoEntidade(ComissaoEntidadeTO $ceTO){
		return $this->bo->editarComissaoEntidade($ceTO);
	}
	
	/**
	 * Metodo que edita regra de comissão
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function editarRegraComissao(RegraComissaoTO $rcTO){
		return $this->bo->editarRegraComissao($rcTO);
	}
	
	/**
	 * Metodo que exclui regra de comissão
	 * @param RegraComissaoTO $rcTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirRegraComissao(RegraComissaoTO $rcTO){
		return $this->bo->excluirRegraComissao($rcTO);
	}
}
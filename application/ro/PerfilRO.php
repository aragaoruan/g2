<?php

/**
 * Classe de comunicação com Flex para interações com objeto Perfil
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class PerfilRO extends Ead1_RO
{

    /**
     * Método responsável por duplicar as configurações gerando um novo perfil
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function duplicarPerfil (PerfilTO $pTO)
    {
        $bo = new PerfilBO();
        return $bo->duplicarPerfil($pTO);
    }

    /**
     * Metodo que decide se cadastra ou edita Perfil
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function salvarPerfil (PerfilTO $pTO, $nomeExibicao = NULL)
    {

        if ($pTO->getId_perfil()) {
            return $this->editarPerfil($pTO, $nomeExibicao);
        }
        return $this->cadastrarPerfil($pTO, $nomeExibicao);
    }

    /**
     * Metodo que decide se cadastra ou edita o Perfil da Entidade
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function salvarPerfilEntidade (PerfilEntidadeTO $peTO)
    {
        if ($peTO->getId_perfil()) {
            return $this->editarPerfilEntidade($peTO);
        }
        return $this->cadastrarPerfilEntidade($peTO);
    }

    /**
     * Metodo que decide se cadastra ou edita as funcionalidades do Perfil
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function salvarPerfilFuncionalidade (PerfilFuncionalidadeTO $pfTO)
    {
        $bo = new PerfilBO();
        return $bo->salvarPerfilFuncionalidade($pfTO);
    }

    /**
     * Metodo que decide se edita ou cadastra dados/ Usuario de/a um Perfil
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     */
    public function salvarUsuarioPerfilEntidade (UsuarioPerfilEntidadeTO $upeTO)
    {
        $bo = new PerfilBO();
        return $bo->salvarUsuarioPerfilEntidade($upeTO);
    }

    /**
     * Metodo que cadastra/edita as Funcionalidades de um perfil
     * @param array $arrTO
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayPerfilFuncionalidade ($arrTO, PerfilTO $pTO)
    {
        $bo = new PerfilBO();
        return $bo->salvarArrayPerfilFuncionalidade($arrTO, $pTO);
    }

    /**
     * Metodo que cadastra Perfil no Sistema
     * @param PerfilTO $pTO
     * @param String $nomeExibicao 
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfil (PerfilTO $pTO, $nomeExibicao = NULL)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarPerfil($pTO, $nomeExibicao);
    }

    /**
     * Metodo que cadastra entidades do perfil
     * @param array $arrPerfilEntidades
     * @param PerfilTO $pTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilEntidadeArvore ($arrPerfilEntidades, PerfilTO $pTO)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarPerfilEntidadeArvore($arrPerfilEntidades, $pTO);
    }

    /**
     * Metodo que adiciona Permissao ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilFuncionalidade (PerfilFuncionalidadeTO $pfTO)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarPerfilFuncionalidade($pfTO);
    }

    /**
     * Metodo que Metodo que cadastra o Perfil da Entidade
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilEntidade (PerfilEntidadeTO $peTO)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarPerfilEntidade($peTO);
    }

    /**
     * Cadastra uma lista de permissões de uma funcionalidade à um determinado perfil 
     * @param array $ppfTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPerfilPermissaoFuncionalidade ($arPerfilPermissaoFuncionalidadeTO)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarPerfilPermissaoFuncionalidade($arPerfilPermissaoFuncionalidadeTO);
    }

    /**
     * Metodo que Adiciona Usuario a um Perfil
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuarioPerfilEntidade (UsuarioPerfilEntidadeTO $upeTO)
    {
        $bo = new PerfilBO();
        return $bo->cadastrarUsuarioPerfilEntidade($upeTO);
    }

    /**
     * Metodo que edita Pefil no Sistema
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function editarPerfil (PerfilTO $pTO, $nomeExibicao = NULL)
    {
        $bo = new PerfilBO();
        return $bo->editarPerfil($pTO, $nomeExibicao);
    }

    /**
     * Metodo que edita Permissao ao perfil a determinada funcionalidade
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function editarPerfilFuncionalidade (PerfilFuncionalidadeTO $pfTO)
    {
        $bo = new PerfilBO();
        return $bo->editarPerfilFuncionalidade($pfTO);
    }

    /**
     * Metodo que Edita o Perfil da Entidade
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function editarPerfilEntidade (PerfilEntidadeTO $peTO)
    {
        $bo = new PerfilBO();
        return $bo->editarPerfilEntidade($peTO);
    }

    /**
     * Metodo que edita Permissao ao perfil a determinada funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO
     * @return Ead1_Mensageiro
     */
    public function editarPerfilPermissaoFuncionalidade (PerfilPermissaoFuncionalidadeTo $ppfTO)
    {
        $bo = new PerfilBO();
        return $bo->editarPerfilPermissaoFuncionalidade($ppfTO);
    }

    /**
     * Metodo que Edita Usuario a um Perfil
     * @param UsuarioPerfilEntidadeTO $upeTO
     * @return Ead1_Mensageiro
     */
    public function editarUsuarioPerfilEntidade (UsuarioPerfilEntidadeTO $upeTO)
    {
        $bo = new PerfilBO();
        return $bo->editarUsuarioPerfilEntidade($upeTO);
    }

    /**
     * Metodo que deleta permissão a funcionalidade
     * @param PerfilPermissaoFuncionalidadeTO $ppfTO
     * @return Ead1_Mensageiro
     */
    public function deletarPerfilPermissaoFuncionalidade (PerfilPermissaoFuncionalidadeTO $ppfTO)
    {
        $bo = new PerfilBO();
        return $bo->deletarPerfilPermissaoFuncionalidade($ppfTO);
    }

    /**
     * Metodo que retorna todos os Perfis da entidade
     * @param VwPerfisEntidadeTO $vwPeTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfisEntidade (VwPerfisEntidadeTO $vwPeTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaPerfisEntidade($vwPeTO);
    }

    /*
     * Metodo que retorna todos as entidades de forma recursiva que possuem o perfil em questão ou não
     * @param VwPerfisEntidadeTO $vwPeTO
     * @return Ead1_Mensageiro
     */

    public function retornaEntidadeRecursivaPerfil (VwPerfisEntidadeTO $vwPeTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaEntidadeRecursivaPerfil($vwPeTO);
    }

    /**
     * Metodo que retorna as Pessoas em determinado Perfil
     * @param VwPessoaPerfilTO $vwPpTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoasPerfil (VwPessoaPerfilTO $vwPpTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaPessoasPerfil($vwPpTO);
    }

    /**
     * Metodo que retorna as Pessoas disponíveis para um determinado Perfil
     * @param VwUsuarioPerfilEntidadeTO $vwPpTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoasDisponiveisPerfil (VwUsuarioPerfilEntidadeTO $vwPpTO)
    {
        $bo = new PerfilBO;
        return $bo->retornaPessoasDisponiveisPerfil($vwPpTO);
    }

    /**
     * Metodo que retorna os perfis da entidade(instituição) relacionado com as funcionalidades
     * @param VwPerfilEntidadeFuncionalidadeTO $vwPefTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfilEntidadeFuncionalidade (VwPerfilEntidadeFuncionalidadeTO $vwPefTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaPerfilEntidadeFuncionalidade($vwPefTO);
    }

    /**
     * Metodo que retorna as permissoes de uma determinada funcionalidade
     * @param VwPerfilPermissaoFuncionalidadeTO $vwPpfTO
     * @return Ead1_Mensageiro
     */
    public function retornaPermissaoFuncionalidade (VwPerfilPermissaoFuncionalidadeTO $vwPpfTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaPerfilPermissaoFuncionalidade($vwPpfTO);
    }

    /**
     * Metodo que retorna a arvore de Funcionalidades da Entidade
     * @param VwEntidadeFuncionalidadeTO $vwEfTO
     * @return Ead1_Mensageiro
     */
    public function retornaArvoreEntidadeFuncionalidade (VwEntidadeFuncionalidadeTO $vwEfTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaArvoreEntidadeFuncionalidade($vwEfTO);
    }

    /**
     * Retorna as funcionalidades da entidade.
     * @param $vwefTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeFuncionalidade (VwEntidadeFuncionalidadeTO $vwefTO)
    {
        $bo = new PerfilBO();
        return $bo->retornarVwEntidadeFuncionalidade($vwefTO);
    }

    /**
     * Metodo que retorna entidades de perfil
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilEntidade (PerfilEntidadeTO $peTO)
    {
        $bo = new PerfilBO();
        return $bo->retornarPerfilEntidade($peTO);
    }

    /**
     * Metodo que retorna entidades de perfil com dados a mais.
     * @param PerfilEntidadeTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilEntidadeCompleto (PerfilEntidadeTO $peTO)
    {
        $bo = new PerfilBO();
        return $bo->retornarPerfilEntidadeCompleto($peTO);
    }

    /**
     * Metodo que retorna a arvore de Funcionalidades da Entidade para o Super Usuario
     * @param VwEntidadeFuncionalidadeTO $vwEfTO
     * @return Ead1_Mensageiro
     */
    public function retornaArvoreEntidadeFuncionalidadeSuperUsuario (VwEntidadeFuncionalidadeTO $vwEfTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaArvoreEntidadeFuncionalidadeSuperUsuario($vwEfTO);
    }

    /**
     * Metodo que retorna as funcionalidades do perfil
     * @param PerfilFuncionalidadeTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function retornaFuncionalidadePerfil (PerfilFuncionalidadeTO $pfTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaFuncionalidadePerfil($pfTO);
    }

    /**
     * Metodo que retorna as permissões do perfil
     * @param PerfilTO $pfTO
     * @return Ead1_Mensageiro
     */
    public function retornaPermissoesPerfil (PerfilTO $pTO)
    {
        $bo = new PerfilBO();
        return $bo->retornaPermissoesPerfil($pTO);
    }

    /**
     * Retorna os perfis pedagógicos selecionados
     * Utiliza os atributos do TO como filtro
     * @param PerfilPedagogicoTO $perfilPedagogicoTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilPedagogico (PerfilPedagogicoTO $perfilPedagogicoTO)
    {
        $perfilBO = new PerfilBO();
        return $perfilBO->retornarPerfilPedagogico($perfilPedagogicoTO);
    }

    /**
     * Retorna os perfis pedagógicos disponíveis
     * Utiliza os atributos do TO como filtro
     * @param PerfilTO $perfilTO
     * @return Ead1_Mensageiro
     */
    public function retornarPerfilPedagogicoDisponivel (PerfilTO $perfilTO)
    {
        $perfilBO = new PerfilBO();
        return $perfilBO->retornarPerfilPedagogicoDisponivel($perfilTO);
    }

    /**
     * Metodo que exclui(Logicamente) um perfil
     * @param PerfilTO $pTO
     * @param String $nomeExibicao
     * @return Ead1_Mensageiro
     */
    public function excluirPerfil (PerfilTO $pTO, $nomeExibicao = null)
    {
        $perfilBO = new PerfilBO();
        return $perfilBO->excluirPerfil($pTO, $nomeExibicao);
    }

    public function retornaPerfil (PerfilTO $pTO)
    {
        $perfilBO = new PerfilBO();
        return $perfilBO->retornaPerfil($pTO);
    }

}
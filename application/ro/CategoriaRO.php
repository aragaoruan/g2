<?php
/**
 * Classe de comunicação com Flex para interações com objeto Categoria
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package application
 * @subpackage ro
 */
class CategoriaRO extends Ead1_RO{
	
	private $_bo;
	
	public function __construct(){
		$this->_bo = new CategoriaBO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita Categoria
	 * @param CategoriaTO $to
	 * @param array $arEntidadeTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCategoria(CategoriaTO $to, array $arEntidadeTO = null, ArquivoTO $arquivoTO = null){
		return $this->_bo->salvarCategoria($to, $arEntidadeTO, $arquivoTO);
	}

	/**
	 * Metodo que Exclui uma Categoria
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirCategoria(CategoriaTO $to){
		return $this->_bo->desvincularCategoria($to);
	}
	
	/**
	 * Metodo que Retorna a Categoria
	 * @param CategoriaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarCategoria(CategoriaTO $to){
		return $this->_bo->retornarCategoria($to);
	}
	
	/**
	 * Metodo que retorna uma árvore de objetos CategoriaTO a partir do nó raiz
	 * @param CategoriaTO $raiz
	 * @return Ead1_Mensageiro
	 */
	public function retornarArvoreCategorias(CategoriaTO $raiz = null){
		return $this->_bo->retornarArvoreCategoria($raiz);
	}
	
	/**
	 * Metodo que retorna um array CategoriaTO formatada para o dataProvider do cbCategoriaPai
	 * @return array
	 */
	public function retornarArrayCategoriasPai(){
		return $this->_bo->retornarArrayCategoriasPai();
	}
	
	/**
	 * Método que retorna um array de Entidades para popular a árvore de Entidades.
	 * @param CategoriaTO $categoriaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArCategoriaEntidade(CategoriaTO $categoriaTO){
		return $this->_bo->retornarArCategoriaEntidade($categoriaTO);
	}
	
	/**
	 * Método que retorna um UploadTO com a imagem da Categoria
	 * @param CategoriaTO $categoria
	 * @return Ead1_Mensageiro
	 */
	public function retornarUploadCategoria(CategoriaTO $categoria){
		return $this->_bo->retornarUploadCategoria($categoria);
	}
}
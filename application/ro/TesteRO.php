<?php
/**
 * Classe de testes na camada RO
 * @author edermariano
 *
 * @package application
 * @subpackage ro
 */
class TesteRO extends  Ead1_RO{
	
	public function teste(){
		
		//$tbUsuario = new UsuarioORM();
		//return $tbUsuario->fetchRow("id_usuario = 3")->st_nomecompleto;
		
		$to = new LoginTO();
		$to->usuario = 'Oi!';
		$to->senha = 'Senha';
		return $to;
	}
	
	public function testeUsuario(UsuarioTO $usuario){
		
		return new Ead1_Mensageiro($usuario, Ead1_IMensageiro::SUCESSO, "O id do usuário é: {$usuario->getId_usuario()}");
	}
	
	public function tipoTrilhaTeste() {
		$tbTipoTrilha = new TipoTrilhaORM();
		return new Ead1_Mensageiro($tbTipoTrilha->consulta(new TipoTrilhaTO()));
	}
	
	public function retornaVariaveisTOTeste() {
		$to = "VwSerieNivelEnsinoTO";
		debug("cheguei.");
		$to = new $to;
		$variaveis = get_object_vars($to);
		return new Ead1_Mensageiro($variaveis);
	}
	
	public function data(PessoaTO $to){
		$data = new Zend_Date("2010-11-27");
		return array($to->getDt_cadastro()->toString('y-M-d'), new Zend_Date($data));
	}

}
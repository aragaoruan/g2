<?php

class MinasAulaRO extends Ead1_RO {

	/**
	 * Método que Recebe um objeto TO do Flex, 
	 * insere ou altera o texto no banco e retorna o resultado para o Flex
	 * @param MinasAulaTO $minasAulaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarTexto(MinasAulaTO $minasAulaTO){
		$minasAulaBO = new MinasAulaBO();
		return $minasAulaBO->salvarTexto($minasAulaTO);
	}
}

?>
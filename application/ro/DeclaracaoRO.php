<?php
/**
 * Classe de comunicação com o Flex para interações de Declaracao
 * Enter description here ...
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class DeclaracaoRO extends Ead1_RO{
	
	private $bo;
	
	/**
	 * Método construtor
	 */
	public function __construct(){
		$this->bo = new DeclaracaoBO();
	}
	
	/**
	 * Método que salva uma Entrega de Declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		return $this->bo->salvarEntregaDeclaracao($to);
	}
	
	/**
	 * Método que cadastra uma Entrega de Declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		return $this->bo->cadastrarEntregaDeclaracao($to);
	}
	
	/**
	 * Método que edita uma entrega de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		return $this->bo->editarEntregaDeclaracao($to);
	}
	
	/**
	 * Método que deleta uma entrega de declaração
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirEntregaDeclaracao(EntregaDeclaracaoTO $to){
		return $this->bo->excluirEntregaDeclaracao($to);
	}
	
	/**
	 * Método que retorna as entregas de declaracao
	 * @param EntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntregaDeclaracao(EntregaDeclaracaoTO $to){
		return $this->bo->retornarEntregaDeclaracao($to);
	}
	
	/**
	 * Método que retorna os dados da view de entregas de declaracao
	 * @param VwEntregaDeclaracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntregaDeclaracao(VwEntregaDeclaracaoTO $to){
		return $this->bo->retornarVwEntregaDeclaracao($to);
	}
}
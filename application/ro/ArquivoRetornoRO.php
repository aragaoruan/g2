<?php

/**
 * Classe de comunicação com Flex para interações com objeto ArquivoRetorno
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package application
 * @subpackage ro
 *
 */
class ArquivoRetornoRO extends Ead1_RO{
	
	/**
	 * Metodo que Decide se Cadastra ou Edita ArquivoRetorno
	 * @param ArquivoRetornoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArquivoRetorno(ArquivoRetornoTO $acTO){
		if($acTO->getid_arquivoretorno()){
			return $this->editarArquivoRetorno($acTO);
		}
		return $this->cadastrarArquivoRetorno($acTO);
	}
	
	
	/**
	 * Metodo que Cadastra ArquivoRetorno
	 * @param ArquivoRetornoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArquivoRetorno(ArquivoRetornoTO $acTO){
		$bo = new ArquivoRetornoBO();
		return $bo->cadastrarArquivoRetorno($acTO);
	}
	

	/**
	 * Metodo Que Edita ArquivoRetorno
	 * @param ArquivoRetornoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function editarArquivoRetorno(ArquivoRetornoTO $acTO){
		$bo = new ArquivoRetornoBO();
		return $bo->editarArquivoRetorno($acTO);
	}
	
	
	/**
	 * Metodo que retorna ArquivoRetorno
	 * @param ArquivoRetornoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArquivoRetorno(ArquivoRetornoTO $acTO){
		$bo = new ArquivoRetornoBO();
		return $bo->listarArquivoRetorno($acTO);
	}
	
	
	/**
	 * Metodo que retorna ArquivoRetornoLancamento
	 * @param ArquivoRetornoLancamentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArquivoRetornoLancamento(ArquivoRetornoLancamentoTO $to){
		$bo = new ArquivoRetornoBO();
		return $bo->listarArquivoRetornoLancamento($to);
	}
	
	
	
	
	/**
	 * Retorna os dados da Vw RetornoLancamento
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VwRetornoLancamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwRetornoLancamento(VwRetornoLancamentoTO $to){
		$bo = new ArquivoRetornoBO();
		return $bo->retornarVwRetornoLancamento($to);
	}
	
	
	/**
	 * quita os lancamentos se baseando no arquivo retorno
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param array $arrTo
	 * @return Ead1_Mensageiro
	 */
	public function quitarLancamentosArquivoRetorno(array $arrTo){
		$bo = new ArquivoRetornoBO();
		return $bo->quitarLancamentosArquivoRetorno($arrTo);
	}
	
 	
}
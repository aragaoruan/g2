<?php
/**
 * Classe de comunicação com Flex para interações com objeto Area de Conhecimento
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class AreaRO extends Ead1_RO{
	
	/**
	 * Metodo que Decide se Cadastra ou Edita Area
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArea(AreaConhecimentoTO $acTO){
		if($acTO->getId_areaconhecimento()){
			return $this->editarArea($acTO);
		}
		return $this->cadastrarArea($acTO);
	}
	
	/**
	 * Metodo que Decide se Cadastra ou Edita Seria e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$bo = new AreaBO();
		return $bo->salvarAreaSerieNivel($acsneTO);
	}
		
	/**
	 * MEtodo que cadastra e exclui vinculos de Area de Conhecimento com Serie Nivel de Ensino
	 * @param array(AreaConhecimentoSerieNivelEnsinoTO $arrTO)
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayAreaSerieNivel($arrTO){
		$bo = new AreaBO();
		return $bo->salvarArrayAreaSerieNivel($arrTO);
	}
	
	/**
	 * Metodo que Cadastra Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArea(AreaConhecimentoTO $acTO){
		$bo = new AreaBO();
		return $bo->cadastrarArea($acTO);
	}
	
	/**
	 * Metodo que Cadastra Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$bo = new AreaBO();
		return $bo->cadastrarAreaSerieNivel($acsneTO);
	}
	
	/**
	 * Metodo que Cadastra entidades da área
	 * @param array $arrayDeEntidades
	 * @param AreaConhecimentoTO $arrayDeEntidades
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaEntidade($arrayDeEntidades, AreaConhecimentoTO $aTO){
		$bo = new AreaBO();
		return $bo->cadastrarAreaEntidade($arrayDeEntidades, $aTO);
	}
	
	/**
	 * Metodo Que Edita Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function editarArea(AreaConhecimentoTO $acTO){
		$bo = new AreaBO();
		return $bo->editarArea($acTO);
	}
	
	/**
	 * Metodo que Exclui a Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaConhecimento(AreaConhecimentoTO $acTO){
		$bo = new AreaBO();
		return $bo->deletarAreaConhecimento($acTO);
	}
	
	/**
	 * Metodo que deleta Serie e Nivel de Ensino da Area
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$bo = new AreaBO();
		return $bo->deletarAreaSerieNivel($acsneTO);
	}
	
	/**
	 * Metodo que retorna Area de Conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArea(AreaConhecimentoTO $acTO){
		$bo = new AreaBO();
		return $bo->retornaArea($acTO);
	}
	
	/**
	 * Metodo que retorna entidades da área
	 * @param AreaEntidadeTO $aeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaAreaEntidade(AreaEntidadeTO $aeTO){
		$bo = new AreaBO();
		return $bo->retornarAreaEntidade($aeTO);
	}
	
	/**
	 * Metodo que retorna Area de Conhecimento
	 * @param VwProjetoAreaTO $paTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwAreaProjetoPedagogico(VwProjetoAreaTO $paTO){
		$bo = new AreaBO();
		return $bo->retornaVwAreaProjetoPedagogico($paTO);
	}
	
	/**
	 * Meotodo que retorna Nivel de Ensino e Serie da Area de Conhecimento
	 * @param AreaConhecimentoSerieNivelEnsinoTO $acsneTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaAreaSerieNivel(AreaConhecimentoSerieNivelEnsinoTO $acsneTO){
		$bo = new AreaBO();
		return $bo->retornaAreaSerieNivel($acsneTO);
	}
	
	/**
	 * Metodo que Retorna a Area de Conhecimento para o Componente da Arvore de Area de Conhecimento no Flex
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaArvoreAreaConhecimento(AreaConhecimentoTO $acTO){
		$bo = new AreaBO();
		return $bo->retornaArvoreAreaConhecimento($acTO);
	}
	 	
 	/**
 	 * Metodo que Retorna O Projeto Pedagógico Pela Area de Conhecimento para o Componente da Arvore no Flex
 	 * @param $acTO
 	 * @return Ead1_Mensageiro
 	 */
 	public function retornaArvoreAreaProjetoPedagogico(AreaConhecimentoTO $acTO){
 		$bo = new AreaBO();
		return $bo->retornaArvoreAreaProjetoPedagogico($acTO);
 	}
 	
 	/**
 	 * Retorna os tipos de área conhecimento existentes
 	 * @param TipoAreaConhecimentoTO $tipoAreaConhecimentoTO
 	 * @return Ead1_Mensageiro
 	 */
 	public function retornarTipoAreaConhecimento( TipoAreaConhecimentoTO $tipoAreaConhecimentoTO ) {
 		$areaBO	= new AreaBO();
 		return $areaBO->retornarTipoAreaConhecimento( $tipoAreaConhecimentoTO );
 	}
}
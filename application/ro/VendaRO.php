<?php
/**
 * Classe de comunicação com Flex para interações com objeto Venda
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class VendaRO extends Ead1_RO{
	
	/**
	 * @var VendaBO
	 */
	private $bo;
	private $mensageiro;
	
	public function __construct(){
		$this->bo = new VendaBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}

	
	/**
	 * Método que faz o cancelamento do recebimento de uma Venda.
	 * Este método pode executar algum webservice
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cancelarRecebimentoVenda(VendaTO $vTO){
		return $this->bo->cancelarRecebimentoVenda($vTO);
	}
	
	
	
	/**
	 * Metodo que salva o array de VendaProdutoTO
	 * @param array $arrVendaProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrVendaProduto($arrVendaProdutoTO){
		return $this->bo->salvarArrVendaProduto($arrVendaProdutoTO);
	}
	
	/**
	 * Metodo que salva o protocolo nas vendas
	 * @param ProtocoloTO $pTO
	 * @param array $arrVendas
	 * @return Ead1_Mensageiro
	 */
	public function editarVendaProtocolo(ProtocoloTO $pTO, $arrVendas){
		return $this->bo->editaVendaProtocolo($pTO, $arrVendas);
	}
	
	/**
	 * Metodo que salva o array de VendaProdutoTO
	 * @param array $arrVwVendaLancamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrLancamentoVenda(array $arrVwVendaLancamentoTO){
		return $this->bo->salvarArrLancamentoVenda($arrVwVendaLancamentoTO);
	}
	
	/**
	 * Metodo que exclui os lançamentos de uma venda e cadastra um array de Lançamentos e seus vinculos com a venda
	 * @param array $arrLancamentoTO
	 * @param VendaTO $vendaTO
	 * @param $gerarDataPrimeiroLancamento | indica que sera gerada a data do primeiro lançamento
	 * @param Boolean $gerarDatasVencimento | Indica se sera gerado automaticamente os vencimentos dos lançamentos pós o primeiro lançamento
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayLancamentos($arrLancamentoTO,VendaTO $vendaTO, $gerarDataPrimeiroLancamento = false, $gerarDatasVencimento = true){
		try{
			$this->bo->salvarArrayLancamentos($arrLancamentoTO, $vendaTO);
		}catch (Zend_Exception $e){}
		return $this->bo->mensageiro;
	}
	
	/**
	 * Metodo que marca o lançamento (Dinheiro) como recebido
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function receberDinheiroLancamento(LancamentoTO $lTO){
		return $this->bo->receberDinheiroLancamento($lTO);
	}
	
	/**
	 * Metodo que edita os Lançamentos com os dados do Cheque
	 * O parametro recebido pode ser um array de LançamentoTO ou LançamentoTO
	 * @param array $arrLTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDadosChequeLancamento(array $arrLTO){
		return $this->bo->salvarDadosChequeLancamento($arrLTO);
	}
	
	/**
	 * Metodo que salva o envolvido da venda
	 * @param VendaEnvolvidoTO $to
	 * @param $soExcluir Indica se ira somente deixar todos os envolvidos como bl_ativo = 0
	 * @return Ead1_Mensageiro
	 */
	public function salvarVendaEnvolvido(VendaEnvolvidoTO $to,$soExcluir = false){
		return $this->bo->salvarVendaEnvolvido($to,$soExcluir);
	}
	
	/**
	 * Método que quita o Boleto
	 * @param LancamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function quitarBoleto(LancamentoTO $to){
		return $this->bo->quitarBoleto($to);
	}
	
	/**
	 * Método que quita o Cheque
	 * @param LancamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function quitarCheque(LancamentoTO $to){
		return $this->bo->quitarCheque($to);
	}
	
	/**
	 * Método que quita o Débito
	 * @param LancamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function quitarDebito(LancamentoTO $to){
		return $this->bo->quitarDebito($to);
	}
	
	/**
	 * Método que quita o Débito
	 * @param array $arrayLancamentos
	 * @return Ead1_Mensageiro
	 */
	public function quitarCredito($arrayLancamentos){
		return $this->bo->quitarCredito($arrayLancamentos);
	}
	
	/**
	 * Método que altera o usuario da Venda, Contrato, Responsável Financeiro e Lançamentos com o Id_usuario Anterior 
	 * @param UsuarioTO $uTO
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoAlterarUsuarioVendaContratoResponsavelLancamento(UsuarioTO $uTO,VendaTO $vTO){
		return $this->bo->procedimentoAlterarUsuarioVendaContratoResponsavelLancamento($uTO,$vTO);
	}
	
	/**
	 * Metodo que transforma uma pre-venda em venda
	 * @param PreVendaTO $pvTO
	 * @param AtendenteVendaTO $avTO
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoGerarVenda(PreVendaTO $pvTO, AtendenteVendaTO $avTO){
		return $this->bo->procedimentoGerarVenda($pvTO, $avTO);
	}
	
	/**
	 * Metodo que retorna os Produtos da Venda
	 * @param VwProdutoVendaTO $vwProdutoVendaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProdutoVenda(VwProdutoVendaTO $vwProdutoVendaTO){
		return $this->bo->retornarVwProdutoVenda($vwProdutoVendaTO);
	}
	
	/**
	 * 
	 * Retorna campanhas baseada na venda ou no produto
	 * @param SpCampanhaVendaProdutoTO $cvpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaSpCampanhaVendaProduto(SpCampanhaVendaProdutoTO $cvpTO){
		return $this->bo->retornaSpCampanhaVendaProduto($cvpTO);
	}
	
	/**
	 * 
	 * Retorna os clientes de uma venda
	 * @param VwClienteVendaTO $vwCVTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarClienteVenda(VwClienteVendaTO $vwCVTO){
		return $this->bo->retornarClienteVenda($vwCVTO);
	}
	/**
	 * 
	 * Retorna os lançamentos de uma venda
	 * @param VwVendaLancamentoTO $vlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaLancamento(VwVendaLancamentoTO $vlTO){
		return $this->bo->retornarVendaLancamento($vlTO);
	}
	
	/**
	 *
	 * Retorna os lançamentos de uma venda em renegociação
	 * @param VwVendaLancamentoTO $vlTO
	 * @return Ead1_Mensageiro
	 * @author Rafael Rocha rafael.rocha.mg@gmail.com
	 */
	public function retornarVendaLancamentoRenegociacao(VwVendaLancamentoTO $vlTO, $datasPesquisa = null){
		return $this->bo->retornarVendaLancamentoRenegociacao($vlTO, $datasPesquisa);
	}
	
	/**
	 * Retorna venda para processo de ativação automática
	 * @param VwVendasRecebimentoTO $vwVendasRecebimentoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwVendasRecebimento(VwVendasRecebimentoTO $vwVendasRecebimentoTO){
		if(!$vwVendasRecebimentoTO->getId_entidade()){
			$vwVendasRecebimentoTO->setId_entidade($vwVendasRecebimentoTO->getSessao()->id_entidade);
		}
		return $this->bo->retornarVwVendasRecebimento($vwVendasRecebimentoTO);
	}
	
	/**
	 * 
	 * Retorna a venda com o produto de um determinado usuário
	 * @param VwUsuarioVendaProdutoTO $uvpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarUsuarioVendaProduto(VwUsuarioVendaProdutoTO $uvpTO){
		return $this->bo->retornarUsuarioVendaProduto($uvpTO);
	}
	
	/**
	 * Retorna o resumo financeiro de uma venda
	 * @param VwResumoFinanceiroTO $rfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarResumoFinanceiroVenda(VwResumoFinanceiroTO $rfTO){
        $rfTO->setBl_ativo(true);
		return $this->bo->retornarResumoFinanceiroVenda($rfTO);
	}
	
	/**
	 * Metodo que retorna os meios de pagamento do resumo da venda
	 * @param VwResumoFinanceiroTO $vwRfTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarMeioPagamentoResumoVenda(VwResumoFinanceiroTO $vwRfTO){
		return $this->bo->retornarMeioPagamentoResumoVenda($vwRfTO);
	}

    /**
     * Metodo que retorna o lançamento com o valor atualizado (com valor da multa e aplicação de juros)
     * @param LancamentoTO $lancamentoTO
     * @return Ead1_Mensageiro
     */
    public function retornarLancamentoValorAtualizado(LancamentoTO $lancamentoTO){
		return $this->bo->retornarLancamentoValorAtualizado($lancamentoTO);
    }
    
	/**
	 * Metodo que retorna o envolvido da venda
	 * @param VendaEnvolvidoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaEnvolvido(VendaEnvolvidoTO $to){
		return $this->bo->retornarVendaEnvolvido($to);
	}
	
	/**
	 * Metodo que retorna as Vendas Pendentes de uma Pessoa
	 * @param VwVendaUsuarioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaPendentesPessoa(VwVendaUsuarioTO $to){
		return $this->bo->retornarVendaPendentesPessoa($to);
	}
	
	/**
	 * Método que Confirma o Recebimento de qualquer Venda
	 * @param VendaTO $vendaTO
	 * @return Ead1_Mensageiro 
	 */
	public function confirmaRecebimento(VendaTO $vendaTO){
		return $this->bo->confirmaRecebimento($vendaTO);
	}
	
	/**
	 * Método que edita um lancamento em renegociação
	 * @param LancamentoTO $lancamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLancamentoVendaRenegociacao(LancamentoTO $lancamentoTO){
		return $this->bo->editarLancamentoVendaRenegociacao($lancamentoTO);
	}
	
	/**
	 * Método que cadastra os lancamentos da renegociação
	 * @param LancamentoTO $lancamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLancamentoVendaRenegociacao(array $arrLancamentoTO){
		Zend_Debug::dump($arrLancamentoTO,__CLASS__.'('.__LINE__.')');exit;
		return $this->bo->cadastrarLancamentoVendaRenegociacao($arrLancamentoTO);
	}
	
	/**
	 * Método que pesquisa dívidas do aluno
	 * @param VwVendaLancamentoAtrasadoTO $to
	 * @param array $datas
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function retornarVwVendaLancamentoAtrasado(VwVendaLancamentoAtrasadoTO $to, array $datas = null){
		return $this->bo->retornarVwVendaLancamentoAtrasado($to, $datas);	
	}
	
}
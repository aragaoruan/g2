<?php

class EnvolvidoEntidadeRO extends Ead1_RO {

	/**
	 * @var EnvolvidoEntidadeBO
	 */
	private $bo;
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new EnvolvidoEntidadeBO();
	}
	
	/**
	 * Metodo que decide se edita ou cadastra representante de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		if($eeTO->getId_envolvidoentidade()){
			return $this->editarEnvolvidoEntidade($eeTO);
		}
		return $this->cadastrarEnvolvidoEntidade($eeTO);
	}
	
	/**
	 * Metodo que cadastra representante de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		return $this->bo->cadastrarEnvolvidoEntidade($eeTO);
	}
	
	/**
	 * Metodo que edita representante de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		return $this->bo->editarEnvolvidoEntidade($eeTO);
	}
	
	/**
	 * Metodo que exclui representante de entidade
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		return $this->bo->excluirEnvolvidoEntidade($eeTO);
	}
	
	/**
	 * Metodo que retorna representante de entidade.
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEnvolvidoEntidade(EnvolvidoEntidadeTO $eeTO){
		return $this->bo->retornarEnvolvidoEntidade($eeTO);
	}
	
	/**
	 * Metodo que retorna representante de entidade com o nome da pessoa e tipo de envolvido.
	 * @param EnvolvidoEntidadeTO $eeTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEnvolvidoEntidadePessoa(EnvolvidoEntidadeTO $eeTO){
		return $this->bo->retornarEnvolvidoEntidadePessoa($eeTO);
	}
	
	/**
	 * Metodo que retorna a view de envolvidos da entidade
	 * @param VwEntidadeEnvolvidoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntidadeEnvolvido(VwEntidadeEnvolvidoTO $to){
		return $this->bo->retornarVwEntidadeEnvolvido($to);
	}
	
}

?>
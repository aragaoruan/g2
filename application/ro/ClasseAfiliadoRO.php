<?php
/**
 * Classe de comunicação com Flex para interações com objeto de ClasseAfiliado
 * @author Denise Xavier - denisexavier@ead1.com.br
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package application
 * @subpackage ro
 */
class ClasseAfiliadoRO extends Ead1_RO {
	/**
	 * @var ClasseAFiliadoBO
	 */
	private $bo;
	private $mensageiro;
	
	public function __construct(){
		$this->bo = new ClasseAfiliadoBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * @param ClasseAfiliadoTO $classeAfiliadoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarClasseAfiliado(ClasseAfiliadoTO $classeAfiliadoTO){
		return $this->bo->salvarClasseAfiliado($classeAfiliadoTO);
	}
	
	/**
	 * @param ClasseAfiliadoTO $classeAfiliado
	 * @return Ead1_Mensageiro
	 */
	public function retornarClasseAfiliado(ClasseAfiliadoTO $classeAfiliado){
		return $this->bo->retornarClasseAfiliado($classeAfiliado);
	}
	
	/**
	 * @param AfiliadoProdutoTO $afiliadoProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAfiliadoProduto(AfiliadoProdutoTO $afiliadoProdutoTO){
		return $this->bo->retornarAfiliadoProduto($afiliadoProdutoTO);
	}
	
	/**
	 * @param int $id_contratoafiliado
	 * @return Ead1_Mensageiro
	 */
	public function retornarAfiliadoCategoriaProdutoComissao($id_contratoafiliado){
		return $this->bo->retornarAfiliadoCategoriaProdutoComissao($id_contratoafiliado);
	}
	
	/**
	 * @param ClasseAfiliadoTO $classeAfiliado
	 * @param ComissionamentoTO $comissionamento
	 * @return Ead1_Mensageiro
	 */
	public function salvarComissionamento(ClasseAfiliadoTO $classeAfiliado, ComissionamentoTO $comissionamento, array $meiosPagamento = null){
		return $this->bo->salvarComissionamento($classeAfiliado, $comissionamento, $meiosPagamento);
	}
	
	/**
	 * @param ClasseAfiliadoTO $classeAfiliado
	 * @param array $comissionamentos
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayComissionamentos(ClasseAfiliadoTO $classeAfiliado, array $comissionamentos){
		return $this->bo->salvarArrayComissionamentos($classeAfiliado, $comissionamentos);
	}
	
	/**
	 * @param ComissionamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissionamento(ComissionamentoTO $to){
		return $this->bo->retornarComissionamento($to);
	}
	
	/**
	 * @param ClasseAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarComissionamentosClasseAfiliado(ClasseAfiliadoTO $to){
		return $this->bo->retornarComissionamentosClasseAfiliados($to);
	}
	
	/**
	 * @param ComissionamentoTO $comissionamento
	 * @param array $meiosPagamento
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMeiosPagamentoComissionamento(ComissionamentoTO $comissionamento, array $meiosPagamento){
		return $this->bo->cadastrarMeiosPagamentoComissionamento($comissionamento, $meiosPagamento);
	}
	
	/**
	 * @param ComissionamentoTO $comissionamento
	 * @return Ead1_Mensageiro
	 */
	public function retornarMeiosPagamentoComissionamento(ComissionamentoTO $comissionamento){
		return $this->bo->retornarMeiosPagamentoComissionamento($comissionamento);
	}
	
	/**
	 * Método que retorna TipoPessoaTO
	 * @param TipoPessoaTO $tipoPessoaTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornaTipoPessoa(TipoPessoaTO $tipoPessoaTO) {
		return $this->bo->retornaTipoPessoa($tipoPessoaTO);
	}
	
	/**
	 * @param ClasseAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function desvincularComissionamentos(ClasseAfiliadoTO $to){
		return $this->bo->desvincularComissionamentosClasseAfiliado($to);
	}
	
	/**
	 * @param ComissionamentoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function deletarComissionamento(ComissionamentoTO $to){
		return $this->bo->deletarComissionamento($to);
	}
	
	/**
	 * Cadastra Afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarContratoAfiliado(ContratoAfiliadoTO $to){
		return $this->bo->cadastrarContratoAfiliado($to);
	}
	
	/**
	 * Cadastra Produto, Categoria e Comissão para um afiliado. Como Parâmetro recebe um array de ComissionamentoTO e um de AfiliadoProdutoTO
	 * @param $arrAfiliadoProduto
	 * @param $arrComissionamento
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArrayAfiliadoProdutoComissionamento($arrAfiliadoProduto, $arrComissionamento){
		return $this->bo->cadastrarArrayAfiliadoProdutoComissionamento($arrAfiliadoProduto, $arrComissionamento);
	}
	
	/**
	 * Edita Afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarContratoAfiliado(ContratoAfiliadoTO $to){
		return $this->bo->editarContratoAfiliado($to);
	}
	
	/**
	 * Edita Vínculo de Afiliado com Categoria, Produto e Comissão.
	 * @param AfiliadoProdutoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarAfiliadoProduto(AfiliadoProdutoTO $to){
		return $this->bo->editarAfiliadoProduto($to);
	}
	
	/**
	 * Deleta Vínculo de Afiliado com Categoria, Produto e Comissão.
	 * @param AfiliadoProdutoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function deletarAfiliadoProduto(AfiliadoProdutoTO $to){
		$to->setBl_ativo(false);
		return $this->bo->editarAfiliadoProduto($to);
	}
	
	/**
	 * Deleta Afiliado
	 * @param ContratoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function deletarContratoAfiliado(ContratoAfiliadoTO $to){
		return $this->bo->deletarContratoAfiliado($to);
	}
	
	/**
	 * Salva Afiliado. Verifica se é pra cadastrar ou salvar.
	 * @param ContratoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoAfiliado(ContratoAfiliadoTO $to){
		if ($to->getId_contratoafiliado()) {
			return $this->editarContratoAfiliado($to);
		}
		return $this->cadastrarContratoAfiliado($to);
	}
	
	/**
	 * Retorna Afiliados
	 * @param ContratoAfiliadoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarContratoAfiliado(ContratoAfiliadoTO $to){
		return $this->bo->retornarContratoAfiliado($to);
	}
}

?>
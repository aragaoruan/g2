<?php
/**
 * Classe de comunicação com Flex para interações com objeto Contrato
 * @author Eder Lamar - edermariano@gmail.com
 * @package application
 * @subpackage ro
 */
class ContratoRO extends Ead1_RO{
	
	/**
	 * @var ContratoBO
	 */
	private $bo;
	
	/**
	 * @var Ead1_Mensageiro
	 */
	private $mensageiro;

	/**
	 * Construtor da Classe
	 */
	public function __construct(){
		$this->bo = new ContratoBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}

	/**
	 * Gera o contrato de um aluno com seus responsáveis financeiros, o reponsável pedagógico e suas matrículas
	 * É gerado uma venda, um contrato e uma lista matrículas vinculadas aquele contrato
	 * @param ContratoTO $contratoTO 
	 * @param array $arContratoResponsavelFinanceiroTO
	 * @param ContratoResponsavelTO $contratoResponsavelPedagogico
	 * @param array $arMatriculaTO
	 * @param array $arSeries
	 * @param array $arLancamentoTO
	 * @param ContratoRegraTO $contratoRegraTO
	 * @param array $arVwTurmasDisponiveis
	 * @return Ead1_Mensageiro
	 */
	public function salvarContratoMatricula(ContratoTO $contratoTO, $arContratoResponsavelFinanceiroTO, ContratoResponsavelTO $contratoResponsavelPedagogico, $arMatriculaTO, $arSeries, $arLancamentoTO, ContratoRegraTO $contratoRegraTO, $arVwTurmasDisponiveis) {
		return $this->bo->salvarContratoMatricula($contratoTO, $arContratoResponsavelFinanceiroTO, $contratoResponsavelPedagogico, $arMatriculaTO, $arSeries, $arLancamentoTO, $contratoRegraTO, $arVwTurmasDisponiveis);
	}
	
	/**
	 * Solicita a extensão de um Contrato
	 * @param ContratoTO $contratoTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function solicitarExtensaoContrato(ContratoTO $contratoTO){
		return $this->bo->solicitarExtensaoContrato($contratoTO);
	}
	/**
	 * Solicita a alteração de um contrato mediante uma Taxa
	 * @param ContratoTO $contratoTO
	 * @param TaxaTO $taxaTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
	 */
	public function solicitarAlteracaoContratoTaxa(ContratoTO $contratoTO, TaxaTO $taxaTO){
		return $this->bo->solicitarAlteracaoContratoTaxa($contratoTO, $taxaTO);
	}
}	
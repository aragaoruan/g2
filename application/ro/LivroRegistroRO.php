<?php

class LivroRegistroRO extends Ead1_RO {

	/**
	 * @var LivroRegistroBO
	 */
	private $bo;
	public $mensageiro;
	
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new LivroRegistroBO();
	}
	
	/**
	 * Metodo que decide se edita ou cadastra livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		if($lreTO->getId_livroregistroentidade()){
			return $this->editarLivroRegistro($lreTO);
		}
		return $this->cadastrarLivroRegistro($lreTO);
	}
	
	/**
	 * Metodo que cadastra livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		return $this->bo->cadastrarLivroRegistro($lreTO);
	}
	
	/**
	 * Metodo que edita livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		return $this->bo->editarLivroRegistro($lreTO);
	}
	
	/**
	 * Metodo que exclui livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirLivroRegistro(LivroRegistroEntidadeTO $lreTO){
		return $this->bo->excluirLivroRegistro($lreTO);
	}
	
	/**
	 * Metodo que retorna livro de registro
	 * @param LivroRegistroEntidadeTO $lreTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroRegistroEntidade(LivroRegistroEntidadeTO $lreTO){
		return $this->bo->retornarLivroRegistroEntidade($lreTO);
	}
	
	/**
	 * Metodo que retorna o livro registro
	 * @param LivroRegistroTO $lrTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroRegistro(LivroRegistroTO $lrTO){
		return $this->bo->retornarLivroRegistro($lrTO);
	}
}

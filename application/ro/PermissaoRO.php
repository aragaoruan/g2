<?php
/**
 * Classe de comunicação com a PermissaoRO do FLEX
 * @author Arthur Cláudio de Almeida Pereira - arthur.almeida@ead1.com.br
 *
 * @package application
 * @subpackage ro
 */
class PermissaoRO extends Ead1_RO {
	
	/**
	 * Retorna as permissões do perfil da sessão atual  
	 * @return Ead1_Mensageiro
	 */
	public function retornarPermissoesPerfilLogado( ) {
		$perfilBO	= new PerfilBO();
		$sessao 	= new Zend_Session_Namespace('geral');
		$vwPerfilPermissaoFuncionalidadeTO  = new VwPerfilPermissaoFuncionalidadeTO();
		$vwPerfilPermissaoFuncionalidadeTO->setId_perfil( $sessao->id_perfil );
		return $perfilBO->retornaPerfilPermissaoFuncionalidade( $vwPerfilPermissaoFuncionalidadeTO );
	}

}


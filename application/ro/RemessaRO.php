<?php

/**
 * Classe de ponte Flex -> PHP
 * @author Elcio Mauo Guimarães - <elcioguimaraes@gmail.com>
 * @since 05/06/2012
 * @package ro
 */
class RemessaRO extends Ead1_RO {

	/**
	 * @var RemessaBO
	 */
	private $bo;
	
	public function __construct(){
		$this->bo = new RemessaBO();
	}
	
	/**
	 * Listagem de remessas
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>
	 */
	public function listarRemessa(RemessaTO $remessaTO){
		return $this->bo->listarRemessa($remessaTO);
	}
	
	/**
	 * Listagem de remessas com os materiais
	 * @param RemessaTO $remessaTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>
	 */
	public function listarRemessaComMaterial(RemessaTO $remessaTO){
		return $this->bo->listarRemessaComMaterial($remessaTO);
	}
	
	
	
	/**
	 * Listagem de Material sem Remessa
	 * @param VwMaterialSemRemessaTO $matSemRemessaTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>
	 */
	public function listarMaterialSemRemessa(VwMaterialSemRemessaTO $matSemRemessaTO){
		return $this->bo->listarMaterialSemRemessa($matSemRemessaTO);
	}
	
	
	/**
	 * Listagem de Material da Remessa
	 * @param VwRemessaMaterialTO VwRemessaMaterialTO
	 * @return Ambigous <Ambigous, Ead1_Mensageiro>
	 */
	public function listarRemessaMaterial(VwRemessaMaterialTO $remessaMaterialTO){
		return $this->bo->listarRemessaMaterial($remessaMaterialTO);
	}
	
	/**
	 * Salva ou Edita a RemessaTO
	 * @param RemessaTO $reTO
	 * @return Ambigous <Ead1_Mensageiro, Ambigous>
	 */
	public function salvarRemessa(RemessaTO $reTO){
		return $this->bo->salvarRemessa($reTO);
	}
	
	
	/**
	 * Inativa uma Remessa
	 * @param RemessaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function inativarRemessa(RemessaTO $to){
		return $this->bo->inativarRemessa($to);
	}
	
	
	/**
	 * Vincula o material de uma remessa
	 * @param array $arMatreTO - array de MaterialRemessaTO
	 */
	public function cadastrarMaterialRemessa(array $arMatreTO){
		return $this->bo->cadastrarMaterialRemessa($arMatreTO);
	}
	
	/**
	 * Delete o Vinculo de um material com uma remessa
	 * @param MaterialRemessaTO $to
	 * @param array $where
	 */
	public function deletarMaterialRemessa(MaterialRemessaTO $to, array $where = null){
		return $this->bo->deletarMaterialRemessa($to,$where);
	}
	
}

?>
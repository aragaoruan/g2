<?php
/**
 * Classe para as requisições de Objetos Remotos
 * @package ro
 */
class AssuntoCoRO extends Ead1_RO {
	
	/**
	 * @var AssuntoCoBO
	 */
	private $bo;
	
	/**
	 * Método construtor
	 */
	public function __construct(){
		$this->bo = new AssuntoCoBO();	
	}

	/**
	 * Método com regras de negócio para cadastro de assunto com ou sem vínculo de entidades
	 * @param AssuntoCoTO $assuntoTO
	 * @param array $assuntoEntidadeCoTO [OPTIONAL] array de AssuntoEntidadeCoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAssunto(AssuntoCoTO $assuntoTO, array $arAssuntoEntidadeCoTO = null){
		return $this->bo->salvarAssunto($assuntoTO, $arAssuntoEntidadeCoTO);
	}
	
	/**
	 * Método que retorna um Assunto
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAssunto(AssuntoCoTO $assuntoTO){
		return new Ead1_Mensageiro($assuntoTO->fetch(true, true, true));
	}
	
	/**
	 * Método que retorna um Array de Assuntos
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAssuntos(AssuntoCoTO $assuntoTO){
		return $this->bo->retornarAssuntosTipoAluno($assuntoTO);
	}
	
	/**
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArAssuntoEntidade(AssuntoCoTO $assuntoTO){
		return $this->bo->retornarArAssuntoEntidade($assuntoTO);
	}
	
	/**
	 * @param VwAssuntoCoTO $VwAssuntoCoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwAssuntoCoPai(VwAssuntoCoTO $VwAssuntoCoTO){
		return $this->bo->retornaVwAssuntoCoPai($VwAssuntoCoTO);
	}
	
	/**
	 * @param TipoOcorrenciaTO $tipoocorrencia
	 * @return Ead1_Mensageiro
	 */
	public function retornaTipoOcorrencia(TipoOcorrenciaTO $tipoocorrencia){
		return $this->bo->retornaTipoOcorrencia($tipoocorrencia);
	}
	
	/**
	 * Método para deletar um assunto
	 * @param AssuntoCoTO $assuntoCoTO
	 * @return Ead1_Mensageiro
	 */
	public function deletaAssuntoCo(AssuntoCoTO $assuntoCoTO){
		return $this->bo->deletaAssuntoCo($assuntoCoTO);
	} 
	
        /**
         * Retorna lista de assuntos de uma determinada entidade
         * @param VwAssuntoEntidadeTO $vwAssuntoEntidadeTO
         * @return \VwAssuntoEntidadeTO
         */
        public function retornarVwAssuntoEntidade(VwAssuntoEntidadeTO $vwAssuntoEntidadeTO) {
            return $this->bo->retornarVwAssuntoEntidade($vwAssuntoEntidadeTO);
}

        /**
         * Atualiza compartilhamento de assunto feito por entidade.
         *
         * @param EntidadeTO $entidadeTO
         * @param AssuntoCoTO $arrVwAssuntoEntidadeTO
         * 
         * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
         */
        public function atualizarCompartilhamentoAssunto(EntidadeTO $entidadeTO, array $arrVwAssuntoEntidadeTO){
            return $this->bo->atualizarCompartilhamentoAssunto($arrVwAssuntoEntidadeTO, $entidadeTO);
        }
}

?>
<?php
/**
 * Classe de comunicação com Flex para informações de Login
 * @author edermariano
 * @package application
 * @subpackage ro
 */
class LoginRO extends Ead1_RO{
	
	
	public function lembrarSenhaUsuario( UsuarioLembrarTO $usuarioLembrarTO  ) {
		$loginBO	= new LoginBO();
		return $loginBO->lembrarSenhaUsuario( $usuarioLembrarTO );
	}
	
	/**
	 * Método para efetuar Login
	 * @param LoginTO $to
	 */
	public function logar(LoginTO $to){
		$login = new LoginBO();
		return $login->logar($to);
	}
	
	public function logarDaSessao(){
		$login = new LoginBO();
		return $login->logarDaSessao(); 
	}
	
	
	/**
	 * Método para efetuar seleção de perfil
	 * @param SelecionarPerfilTO $to
	 */
	public function selecionarPerfil(SelecionarPerfilTO $to){
		$login = new LoginBO();
		return new Ead1_Mensageiro( $login->selecaoPerfil($to) );
	}
	
	/**
	 * Método que seta os dados da sessão.
	 * @param int $id_usuario
	 * @param string $nomeUsuario
	 * @param int $id_entidade
	 * @param int $id_perfil
	 */
	public function setaSessao($id_usuario, $nomeUsuario, $id_entidade, $id_perfil){
		$login = new LoginBO();
		return $login->setaSessaoGeral($id_usuario, $nomeUsuario, $id_entidade, $id_perfil);
	}
	
	/**
	 * Método para simulação de usuários
	 * @param PerfilTO $to
	 */
	public function simularUsuario(LoginTO $to){
		$login = new LoginBO();
		return new Ead1_Mensageiro($login->logar($to, true));
	}
	
	/**
	 * Método invocado para logout do sistema
	 */
	public function sair(){
		Zend_Auth::getInstance()->clearIdentity();
		Zend_Session::destroy();
	}
}
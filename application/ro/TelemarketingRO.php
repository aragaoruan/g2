<?php
/**
 * Classe de comunicação com Flex para interações com objeto de Telemarketing
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class TelemarketingRO extends Ead1_RO{

	private $bo;
	private $mensageiro;
	
	public function __construct(){
		$this->bo = new TelemarketingBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Método que conclui o atendimento 
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoConcluirAtendimento(VendaTO $vTO){
		return $this->bo->procedimentoConcluirAtendimento($vTO);
	}
	
	/**
	 * Procedimento que marca a evolução da venda como em andamento quando o atendimento for do proprio atendente
	 * @param PesquisarVendaAtendimentoTO $pesquisarVendaAtendimentoTO
	 * @return Ead1_Mensageiro
	 */
	public function procedimentoAtenderVenda(PesquisarVendaAtendimentoTO $pesquisarVendaAtendimentoTO){
		return $this->bo->procedimentoAtenderVenda($pesquisarVendaAtendimentoTO);
	}
	
	/**
	 * Metodo que cadastra um atendente para a venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAtendenteVenda(AtendenteVendaTO $to){
		return $this->bo->cadastrarAtendenteVenda($to);
	}
	
	/**
	 * Metodo que edita um vinculo de atendente e venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarAtendenteVenda(AtendenteVendaTO $to){
		return $this->bo->editarAtendenteVenda($to);
		
	}
	
	/**
	 * Metodo que retorna o vinculo de atendente e venda
	 * @param AtendenteVendaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarAtendenteVenda(AtendenteVendaTO $to){
		return $this->bo->retornarAtendenteVenda($to);
	}
}
<?php
/**
 * Classe de comunicação com Flex para interações com objeto livro
 * @author Elcio mauro Guimarães - elcioguimaraes@gmail.com
 * @package application
 * @subpackage ro
 */
class LivroRO extends Ead1_RO{
	
	private $_bo;
	
	public function __construct(){
		$this->_bo = new LivroBO();
	}
	
	/**
	 * Metodo que Retorna o Licro
	 * 
	 * @param LivroTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivro(LivroTO $to){
		return $this->_bo->retornarLivro($to);
	}
	
	/**
	 * @param AssuntoCoTO $assuntoTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarArLivroEntidade(LivroTO $livroTO){
		return $this->_bo->retornarArLivroEntidade($livroTO);
	}
	
	/**
	 * Método que retorna os tipos de livros 
	 * @param TipoLivroTO $to
	 */
	public function retornarTipoLivro(TipoLivroTO $to){
		return $this->_bo->retornarTipoLivro($to);
	}
	
	/**
	 * Método que Salva um Livro
	 * @param LivroTO $to
	 * @return Ambigous <LivroTO, Ead1_Mensageiro>
	 */
	public function salvarLivro(LivroTO $to, array $arLivroEntidadeCoTO = null){
		return $this->_bo->salvarLivro($to, $arLivroEntidadeCoTO);
	}
	
	
	/**
	 * Método que busca a lista de livros no WS da Leya
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivrosLeya(){
		return $this->_bo->retornarLivrosLeya();
	}
	
	
	/**
	 * Retorna o Link do Livro para Download
	 * @param LivroTO $livro
	 * @param VendaTO $venda
	 * @return Ead1_Mensageiro
	 */
	public function retornaLinkLivroLeya(LivroTO $livro, VendaTO $venda){
		return $this->_bo->retornaLinkLivroLeya($livro, $venda);
	}
	
	/**
	 * Retorna as categorias disponíveis na Leya Bookstore
	 * @return Ead1_Mensageiro
	 */
	public function retornaCategoriasLivroLeya(){
		return $this->_bo->retornaCategoriasLivroLeya();
	}
	
	
	
	/**
	 * Metodo que retorna Coleções
	 *
	 * @param LivroColecaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarLivroColecao(LivroColecaoTO $to){
		return $this->_bo->retornarLivroColecao($to);
	}
	
	
	/**
	 * Método que salva um LivroColecaoTO
	 * @param LivroColecaoTO $to
	 * @return LivroColecaoTO|Ead1_Mensageiro
	 */
	public function salvarLivroColecao(LivroColecaoTO $to){
		return $this->_bo->salvarLivroColecao($to);
	}
	
	/**
	 * Método que salva um LivroColecaoTO
	 * @param LivroColecaoTO $to
	 * @return LivroColecaoTO|Ead1_Mensageiro
	 */
	public function excluirLivroColecao(LivroColecaoTO $to){
		
		if(!$to->getId_livrocolecao()){
			return new Ead1_Mensageiro("Coleção não informada!", Ead1_IMensageiro::AVISO);
		}
		
		$to->setBl_ativo(false);
		return $this->_bo->salvarLivroColecao($to);
	}
	
	/**
	 * Método para vincular um Produto a um Livro em uma Entidade
	 * @param ProdutoLivroTO $to
	 * @return ProdutoLivroTO|Ead1_Mensageiro
	 */
	public function inserirProdutoLivro(ProdutoLivroTO $to){
		return $this->_bo->inserirProdutoLivro($to);	
	}
	
	/**
	 * Pesquisa de Livros
	 * @param VwPesquisarLivroTO $pesquisalivroTO
	 * @return Ead1_Mensageiro
	 */
	public function pesquisarLivro(VwPesquisarLivroTO $to){
		return $this->_bo->retornarVwPesquisarLivro($to);
	}
}
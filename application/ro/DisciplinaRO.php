<?php
/**
 * Classe de comunicação com Flex para interações com objeto Disciplina
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class DisciplinaRO extends Ead1_RO{
	
	/**
	 * Metodo que decide se Cadastra ou Edita Disciplina
	 * @param DisciplinaTO $dTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDisciplina(DisciplinaTO $dTO){
		if($dTO->getId_disciplina()){
			return $this->editarDisciplina($dTO);
		}
		return $this->cadastrarDisciplina($dTO);
	}
	
	/**
	 * Metodo que decide se Cadastra ou Exclui Area de Conhecimento da Disciplina
	 * @param AreaDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAreaDisciplina(AreaDisciplinaTO $adTO){
		$orm = new AreaDisciplinaORM();
		if($orm->consulta($adTO,true)){
			return $this->deletarAreaDisciplina($adTO);
		}
		return $this->cadastrarAreaDisciplina($adTO);
	}
	
	/**
	 * Metodo que decide se Cadastra ou Exclui Serie, Nivel de Ensino da Disciplina
	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO){
		$orm = new DisciplinaSerieNivelEnsinoORM();
		if($orm->consulta($dsneTO,true)){
			return $this->deletarDisciplinaSerieNivel($dsneTO);
		}
		return $this->cadastrarDisciplinaSerieNivel($dsneTO);
	}
	
	/**
	 * Metodo que Cadastra e Exclui o vinculo da Area com as Disciplinas
	 * @param array (AreaConhecimentoTO $arrTO)
	 * @param AreaDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayAreaDisciplina($arrTO, AreaDisciplinaTO $adTO){
		$bo = new DisciplinaBO();
		return $bo->salvarArrayAreaDisciplina($arrTO,$adTO);
	}
		
	/**
	 * Metodo que Cadastra e Exclui vinculos da Disciplina com a Serie e Nivel de Ensino
	 * @param array( DisciplinaSerieNivelEnsinoTO $arrTO)
	 * @return Ead1_DAO
	 */
	public function salvarArrayDisciplinaSerieNivelEnsino($arrTO){
		$bo = new DisciplinaBO();
		return $bo->salvarArrayDisciplinaSerieNivelEnsino($arrTO);
	}
	
	/**
	 * Meotodo que Cadastra Disciplina
	 * @param DisciplinaTO $dTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDisciplina(DisciplinaTO $dTO){
		$bo = new DisciplinaBO();
		return $bo->cadastrarDisciplina($dTO);
	}
	
	/**
	 * Metodo que cadastra a Area a Disciplina
	 * @param AreaDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaDisciplina(AreaDisciplinaTO $adTO){
		$bo = new DisciplinaBO();
		return $bo->cadastrarAreaDisciplina($adTO);
	}
	
	/**
	 * Metodo que Cadastra Serie, Nivel de Ensino da Area de Conhecimento na Disciplina
	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO){
		$bo = new DisciplinaBO();
		return $bo->cadastrarDisciplinaSerieNivel($dsneTO);
	}
	
	/**
	 * Meotodo que Edita Disciplina
	 * @param DisciplinaTO $dTO
	 * @return Ead1_Mensageiro
	 */
	public function editarDisciplina(DisciplinaTO $dTO){
		$bo = new DisciplinaBO();
		return $bo->editarDisciplina($dTO);
	}
	
	/**
	 * Metodo que Exclui a Area a Disciplina
	 * @param AreaDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaDisciplina(AreaDisciplinaTO $adTO){
		$bo = new DisciplinaBO();
		return $bo->deletarAreaDisciplina($adTO);
	}
	
	/**
	 * Metodo que Exclui Serie, Nivel de Ensino da Area de Conhecimento da Disciplina
	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO){
		$bo = new DisciplinaBO();
		return $bo->deletarDisciplinaSerieNivel($dsneTO);
	}
	
	/**
	 * Exclui uma disciplina
	 * @param DisciplinaTO $disciplinaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDisciplina( DisciplinaTO $disciplinaTO ) {
		$bo	= new DisciplinaBO();
		return $bo->deletarDisciplina( $disciplinaTO );
	}
	
	/**
	 * Metodo que Retorna a Disciplina
	 * @param DisciplinaTO $dTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaDisciplina(DisciplinaTO $dTO){
		$bo = new DisciplinaBO();
		return $bo->retornaDisciplina($dTO);
	}
	
	/**
	 * Metodo que retorna Disciplina da Area de Conhecimento
	 * @param AreaDisciplinaTO $adTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaAreaDisciplina(AreaDisciplinaTO $adTO){
		$bo = new DisciplinaBO();
		return $bo->retornaAreaDisciplina($adTO);
	}
	
	/**
	 * @todo Deu erro de Conexção no Flex e problemas no Zend reflection...
	 * Metodo que Retorna Serie no Nivel de Ensino na Area de Conhecimento da Disciplina
	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaDisciplinaSerieNivel(DisciplinaSerieNivelEnsinoTO $dsneTO){
		$bo = new DisciplinaBO();
		return $bo->retornaDisciplinaSerieNivel($dsneTO);
	}
	
	/**
	 * Metodo que retorna Tipo de disciplina
	 * @param TipoDisciplinaTO $tdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaTipoDisciplina(TipoDisciplinaTO $tdTO) {
		$bo = new DisciplinaBO();
		return $bo->retornaTipoDisciplina($tdTO);
	}
	
	/**
	 * Metodo que retorna Professores
	 * @param VwProfessorDisciplinaTO $vwProfessorDisciplinaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProfessorDisciplina(VwProfessorDisciplinaTO $vwProfessorDisciplinaTO) {
		$bo = new DisciplinaBO();
		return $bo->retornarVwProfessorDisciplina($vwProfessorDisciplinaTO);
	}
		
	/**
	 * Metodo que Retorna dados de Disciplina por Area de Conhecimento e serie e nivel de ensino
	 * @param VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaDisciplinaAreaConhecimentoSerieNivel(VwDisciplinaAreaConhecimentoNivelSerieTO $vwdacsnTO){
		$bo = new DisciplinaBO();
		return $bo->retornaDisciplinaAreaConhecimentoSerieNivel($vwdacsnTO);
	}
	
	/**
	 * Método que retorna VwDisciplinaIntegracaoMoodle para disciplina
	 * @param DisciplinaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornaVwDisciplinaIntegracaoMoodle(DisciplinaTO $to){
		$bo = new DisciplinaBO();
		return $bo->retornarVwDisciplinaIntegracaoMoodle($to);
	}

	
	public function retornarEntidadesDisciplinaIntegracao(DisciplinaTO $to){
		$bo = new DisciplinaBO();
		return $bo->retornarEntidadesDisciplinaIntegracao($to);
	}
	
	
	/**
	 * Método que salva o st_codsistema para disciplinas integradas ao moodle
	 * @param DisciplinaIntegracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarDisciplinaIntegracao(DisciplinaIntegracaoTO $to){
		$bo = new DisciplinaBO();
		return $bo->salvarDisciplinaIntegracao($to);
	}
	
	/**
	 * Exclui vinculo da disciplina com o moodle e a integracao
	 * @param DisciplinaIntegracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirDisciplinaIntegracao(DisciplinaIntegracaoTO $to){
		$bo = new DisciplinaBO();
		return $bo->excluirDisciplinaIntegracao($to);
	}
	
	
	/**
	 * Método que retorna DisciplinaIntegracao para disciplinas integradas ao moodle
	 * @param DisciplinaIntegracaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornaDisciplinaIntegracao(DisciplinaIntegracaoTO $to){
		$bo = new DisciplinaBO();
		return $bo->retornarDisciplinaIntegracao($to);
	}
	
//	
//	/**
//	 * Meotod que retorna as Disciplinas por Entidade, Area de Conhecimento e Serie.
//	 * @param EntidadeTO $eTO
//	 * @param AreaConhecimentoTO $acTO
//	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
//	 * @param DisciplinaSerieNivelEnsinoTO $dsneTO
//	 */
//	public function retornarDisciplinaxAreaSerie(EntidadeTO $eTO, AreaConhecimentoTO $acTO, DisciplinaSerieNivelEnsinoTO $dsneTO){
//		$bo = new DisciplinaBO();
//		return $bo->retornarDisciplinaxAreaSerie($eTO,$acTO,$dsneTO);
//	}
	
}
<?php
/**
 * Classe de comunicação com Flex para interações com objeto HorarioAula
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class HorarioAulaRO extends Ead1_RO{
	
	public $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new HorarioAulaBO();
	}
	
	/**
	 * Metodo que Decide se Edita ou Cadastra o horario da aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarHorarioAula(HorarioAulaTO $haTO){
		if($haTO->getId_horarioaula()){
			return $this->editarHorarioAula($haTO);
		}
		return $this->cadastrarHorarioAula($haTO);
	}
	
	/**
	 * Metodo que Decide se Cadastra ou Exclui o HorarioDiaSemana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		return $this->bo->salvarHorarioDiaSemana($hdsTO);
	}
	
	/**
	 * Metodo que Cadastra e Exclui os Vinculos do Horario com o Dias da Semanas
	 * @param HorarioAulaTO $haTO
	 * @param array (HorarioDiaSemanaTO $arrhdsTO)
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayHorarioAulaDiaSemana(HorarioAulaTO $haTO, $arrhdsTO){
		return $this->bo->salvarArrayHorarioAulaDiaSemana($haTO, $arrhdsTO);
	}
		
	/**
	 * Metodo que Cadastra Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarHorarioAula(HorarioAulaTO $haTO){
		return $this->bo->cadastrarHorarioAula($haTO);
	}
		
	/**
	 * Metodo que Cadastra o Horario em Determinado Dia da Semana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		return $this->bo->cadastrarHorarioDiaSemana($hdsTO);
	}
		
	/**
	 * Metodo que Edita o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function editarHorarioAula(HorarioAulaTO $haTO){
		return $this->bo->editarHorarioAula($haTO);
	}
		
	/**
	 * Metodo que Exclui o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioAula(HorarioAulaTO $haTO){
		return $this->bo->deletarHorarioAula($haTO);
	}
		
	/**
	 * Metodo que Exclui o Horario da Aula e os dias de semana vinculados a este horário
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioAulaDiaSemana(HorarioAulaTO $haTO){
		return $this->bo->deletarHorarioAulaDiaSemana($haTO);
	}
		
	/**
	 * Metodo que Exclui Vinculo do Horario da Aula com o Dia da Semana
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		return $this->bo->deletarHorarioDiaSemana($hdsTO);
	}
		
	/**
	 * Metodo que Retorna o Horario da Aula
	 * @param HorarioAulaTO $haTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarHorarioAula(HorarioAulaTO $haTO){
		return $this->bo->retornarHorarioAula($haTO);
	}
		
	/**
	 * Metodo que Retorna o Dia da Semana do Horario da Aula
	 * @param HorarioDiaSemanaTO $hdsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarHorarioDiaSemana(HorarioDiaSemanaTO $hdsTO){
		return $this->bo->retornarHorarioDiaSemana($hdsTO);
	}
		
	/**
	 * Metodo que Retorna o Turno
	 * @param TurnoTO $tTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurno(TurnoTO $tTO){
		return $this->bo->retornarTurno($tTO);
	}
		
	/**
	 * Metodo que Retorna DIa da Semana
	 * @param DiaSemanaTO $dsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDiaSemana(DiaSemanaTO $dsTO){
		return $this->bo->retornarDiaSemana($dsTO);
	}
	
	/**
	 * Metodo que Retorna o Turno e o Horario da Aula
	 * @param VwTurnoHorarioAulaTO $vwthaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurnoHorarioAula(VwTurnoHorarioAulaTO $vwthaTO){
		return $this->bo->retornarTurnoHorarioAula($vwthaTO);
	}
		
	/**
	 * Metodo que retorna o Turno, horario da aula e os Dias da Semana
	 * @param VwTurnoHorarioAulaTO $vwthaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTurnoHorarioAulaArrDiaSemana(VwTurnoHorarioAulaTO $vwthaTO){
		return $this->bo->retornarTurnoHorarioAulaArrDiaSemana($vwthaTO);
	}
}
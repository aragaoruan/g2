<?php
/**
 * Classe de comunicação com Flex para interações com objeto LIvro de Registro
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class PreVendaRO extends Ead1_RO {

	public $mensageiro;
	
	/**
	 * @var PreVendaBO
	 */
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new PreVendaBO();
	}
	
	/**
	 * Metodo que decide se cadastra ou edita pré-venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarPreVenda(PreVendaTO $pvTO){
		if($pvTO->getId_prevenda()){
			return $this->editarPreVenda($pvTO);
		}
		return $this->cadastrarPreVenda($pvTO);
	}
	
	/**
	 * Metodo que cadastra pré-venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVenda(PreVendaTO $pvTO){
		return $this->bo->cadastrarPreVenda($pvTO);
	}
	
	/**
	 * Metodo que edita pré-venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function editarPreVenda(PreVendaTO $pvTO){
		return $this->bo->editarPreVenda($pvTO);
	}
	
	/**
	 * Metodo que cadastra pré-venda com produto valor
	 * @param $arrProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVendaProdutoValor($arrProduto){
		return $this->bo->cadastrarPreVendaProdutoValor($arrProduto);
	}
	
	
	/**
	 * Metodo que cadastra pré-venda com produto
	 * @param $arrProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarPreVendaProduto($arrProduto){
		return $this->bo->cadastrarPreVendaProduto($arrProduto);
	}
	
	/**
	 * Metodo que retorna pré-venda
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVenda(PreVendaTO $pvTO){
		return $this->bo->retornarPreVenda($pvTO);
	}
	
	/**
	 * Metodo que retorna uma lista das pessoas que batem com as informações de nome completo cpf e email
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarListaPessoasPreVenda(PreVendaTO $pvTO){
		return $this->bo->retornarListaPessoasPreVenda($pvTO);
	}
	
	/**
	 * Método que retorna a soma das possiveis duplicidades de pessoas e a soma de suas vendas pendentes
	 * @param PreVendaTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaCountPessoasVendaPendentes(PreVendaTO $pvTO){
		return $this->bo->retornaCountPessoasVendaPendentes($pvTO);
	}
	
	/**
	 * Metodo que retorna pré-venda com produto valor
	 * @param VwPreVendaProdutoValorTO $vpvpvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaProdutoValor(VwPreVendaProdutoValorTO $vpvpvTO){
		return $this->bo->retornarPreVendaProdutoValor($vpvpvTO);
	}
	
	
	/**
	 * Metodo que retorna pré-venda com produto
	 * @param VwPreVendaProdutoTO $vpvpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaProduto(VwPreVendaProdutoTO $vpvpTO){
		return $this->bo->retornarPreVendaProduto($vpvpTO);
	}
	
	/**
	 * Metodo que retorna a pesquisa de PreVenda por periodo
	 * @param PesquisarPreVendaTO $ppvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPreVendaPeriodo(PesquisarPreVendaTO $ppvTO){
		return $this->bo->retornarPreVendaPeriodo($ppvTO);
	}
	
	/**
	 * Metodo que retorna as formas de Pagamento pelo Meio de Pagamento da pre venda
	 * @param PesquisarFormaPagamentoXParcelaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarComboFormaPagamentoPreVenda(PesquisarFormaPagamentoXParcelaTO $to){
		return $this->bo->retornarComboFormaPagamentoPreVenda($to);
	}
	
	/**
	 * Método que retorna as pre vendas disponiveis ao distribuidor logado
	 * @param VwPrevendaDistribuicaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwPreVendaDistribuicao(VwPrevendaDistribuicaoTO $to){
		return $this->bo->retornarVwPreVendaDistribuicao($to);
	}
	
	/**
	 * Metodo que exclui produto
	 * @param VwPreVendaProdutoValorTO $pvpvTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirPreVendaProdutoValor(PreVendaProdutoValorTO $pvpvTO){
		return $this->bo->excluirPreVendaProdutoValor($pvpvTO);
	}
	
	/**
	 * Metodo que exclui produto da pré venda
	 * @param VwPreVendaProdutoTO $pvpTO
	 * @return Ead1_Mensageiro
	 */
	public function excluirPreVendaProduto(PreVendaProdutoTO $pvpTO){
		return $this->bo->excluirPreVendaProduto($pvpTO);
	}
	
	/**
	 * Metodo que verifica dados do usuário a partir do CPF.
	 * @param UsuarioTO $uTO
	 * @return Ead1_Mensageiro
	 */
	public function verificarCpfPreVenda(UsuarioTO $uTO){
		return $this->bo->verificarCpfPreVenda($uTO);
	}
}

?>
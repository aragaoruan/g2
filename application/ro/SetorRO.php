<?php
/**
 * Classe de comunicação com Flex para interações com objeto Setor
 * @author Rafael Rocha - rafael.rocha.mg@gmail.com
 * @package application
 * @subpackage ro
 */
class SetorRO extends Ead1_RO{
	
	private $_bo;
	
	public function __construct(){
		$this->_bo = new SetorBO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita Setor
	 * @param SetorTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarSetor(SetorTO $to){
		return $this->_bo->salvarSetor($to);
	}

	/**
	 * Metodo que Exclui um Setor
	 * @param SetorTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirSetor(SetorTO $to){
		return $this->_bo->desvincularSetor($to);
	}
	
	/**
	 * Metodo que Retorna o Setor
	 * @param SetorTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarSetor(SetorTO $to){
		return $this->_bo->retornarSetor($to);
	}
	
	/**
	 * Metodo que retorna uma árvore de objetos SetorTO a partir do nó raiz
	 * @param SetorTO $raiz
	 * @return Ead1_Mensageiro
	 */
	public function retornarArvoreSetores(SetorTO $raiz = null){
		return $this->_bo->retornarArvoreSetor($raiz);
	}
	
	/**
	 * Metodo que retorna um array SetorTO formatada para o dataProvider do cbSetorPai
	 * @return array
	 */
	public function retornarArraySetoresPai(){
		return $this->_bo->retornarArraySetoresPai();
	}
	
	/**
	 * Método que retorna VwSetorCargoFuncaoUsuarioTO
	 * @param VwSetorCargoFuncaoUsuarioTO $vw
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSetorCargoFuncaoUsuario(VwSetorCargoFuncaoUsuarioTO $vw){
		return $this->_bo->retornarVwSetorCargoFuncaoUsuario($vw);
	}
	
	/**
	 * Método que salvar um vinculo entre Setor e Funcionario
	 * @param SetorFuncionarioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarSetorFuncionario(SetorFuncionarioTO $to){
		return $this->_bo->salvarSetorFuncionario($to);
	}
	
	/**
	 * Metodo que retorna um vinculo entre Setor e Funcionario usando a VwSetorFuncionario
	 * @param VwSetorFuncionarioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSetorFuncionario(VwSetorFuncionarioTO $to){
		return $this->_bo->retornarVwSetorFuncionario($to);
	}
	
	/**
	 * Método que exclui um vinculo entre Setor e Funcionario
	 * @param SetorFuncionarioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function excluirSetorFuncionario(SetorFuncionarioTO $to){
		return $this->_bo->desvincularSetorFuncionario($to);
	}
}
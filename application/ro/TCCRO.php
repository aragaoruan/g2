<?php 


/**
 * Classe de comunicação com Flex para interações com objeto ParametroTCC
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package application
 * @subpackage ro
 */
class TCCRO extends Ead1_RO {
	
	private $bo;
	
	public function __construct(){
		$this->bo = new TCCBO();
	}
	
	/**
	 * Metodo que decide se cadastra ou edita a ParametroTCC
	 * @param ParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarParametroTCC(ParametroTCCTO $to){
		return $this->bo->salvarParametroTCC($to);
	}
	
	
	/**
	 * Metodo que cadastra a ParametroTCC
	 * @param ParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarParametroTCC(ParametroTCCTO $to){
		return $this->bo->cadastrarParametroTCC($to);
	}
	
	
	/**
	 * Metodo que edita a ParametroTCC
	 * @param ParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function editarParametroTCC(ParametroTCCTO $to){
		return $this->bo->editarParametroTCC($to);
	}
	
	
	/**
	 * Metodo que retorna ParametroTCC
	 * @param ParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarParametroTCC(ParametroTCCTO $to){
		return $this->bo->retornarParametroTCC($to);
	}
	
	
	/**
	 * Metodo que retorna ParametroTCC
	 * @param VwAvaliacaoParametroTCCTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwAvaliacaoParametroTCC(VwAvaliacaoParametroTCCTO $to){
		return $this->bo->retornaVwAvaliacaoParametroTCC($to);
	}
	
	
	/**
	 * Realiza o upload de arquivos de TCC
	 * Pega o arquivo informado e salva na pasta informada.
	 * @param ArquivoTO $arquivoTO
	 * @return Ead1_Mensageiro
	 */
	public function uploadTCC(ArquivoTO $arquivoTO, Ead1_TO_Dinamico $to) {
		return $this->bo->upload($arquivoTO, $to);
	}
	

}



<?php
/**
 * Classe de comunicação com Flex para interações com objeto Projeto Pedagogico
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class ProjetoPedagogicoRO extends Ead1_RO{

    /**
     * Método que retorna as séries do projeto pedagógico
     * @param int $idProjetoPedagogico
     * @return Ead1_Mensageiro
     */
    public function retornarSerieProjetoPedagogico($idProjetoPedagogico){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarSerieProjetoPedagogico($idProjetoPedagogico);
    }


    /**
     * Metodo que verifica se o aluno possui alguma pendencia pedagogica
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro;
     */
    public function retornarStatusPedagogico(MatriculaTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarStatusPedagogico($mTO);
    }

    /**
     * Metodo que retorna o o detalhamento do status pedagogico
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarDetalhamentoStatusPedagogico(MatriculaTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarDetalhamentoStatusPedagogico($mTO);
    }

    /**
     * Metodo que decide se cadastra ou edita Projeto Pedagógico
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarTrilhaDisciplina($tdTO);
    }

    /**
     * Metodo que decide se cadastra ou edita Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, TrilhaTO $tTO = null){
        if($ppTO->getId_projetopedagogico()){
            return $this->editarProjetoPedagogico($ppTO);
        }
        return $this->cadastrarProjetoPedagogico($ppTO,$tTO);
    }

    /**
     * Metodo que salva Informações Sobre o Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function salvarDadosProjetoPedagogico(ProjetoPedagogicoTO $ppTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarDadosProjetoPedagogico($ppTO);
    }

    /**
     * Metodo que salva os Meios de Avaliação do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @param Array $arrTO
     * @return Ead1_Mensageiro
     */
    public function salvarAvaliacaoProjetoPedagogico(ProjetoPedagogicoTO $ppTO, $arrTO = null){
        $bo = new ProjetoPedagogicoBO();
        if($arrTO){
            return $bo->salvarAvaliacaoProjetoPedagogico($ppTO, $arrTO);
        }else{
            return $bo->salvarAvaliacaoProjetoPedagogico($ppTO);
        }
    }

    /**
     * Metodo que salva o Contrato e Valores do Projeto Pedagógico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function salvarContratoProjetoPedagogico(ProjetoPedagogicoTO $ppTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarContratoProjetoPedagogico($ppTO);
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Serie e Nivel de Ensino do Porjeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarProjetoPedagogicoSerieNivelEnsino($ppsneTO);
    }

    /**
     * Metodo que Cadastra e Exclui os vinculos do ProjetoPedagogico com a Serie e Nivel de Ensino
     * @param array( ProjetoPedagogicoSerieNivelEnsinoTO $arrTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayProjetoPedagogicoSerieNivelEnsino($arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarArrayProjetoPedagogicoSerieNivelEnsino($arrTO);
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Area de Conhecimento do Porjeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function salvarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarAreaProjetoPedagogico($appTO);
    }

    /**
     * Metodo que decide se Cadastra ou Edita Projeto Extracurricular do Porjeto Pedagógico
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoExtracurricular(ProjetoExtracurricularTO $peTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarProjetoExtracurricular($peTO);
    }

    /**
     * Metodo que decide se Cadastra ou edita a Trilha do Porjeto Pedagógico
     * @param TrilhaTO $tTO
     * @param int $idProjetoPegagogico OPTIONAL
     * @return Ead1_Mensageiro
     */
    public function salvarTrilha( TrilhaTO $tTO, $idProjetoPegagogico = null ){
        if(!$tTO->getId_trilha()){
            return $this->cadastrarTrilha( $tTO, $idProjetoPegagogico );
        }
        return $this->editarDadosTrilha($tTO);
    }

    /**
     * Metodo que decide se Cadastra ou edita o Modulo do Porjeto Pedagógico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function salvarModulo(ModuloTO $mTO){
        if(!$mTO->getId_modulo()){
            return $this->cadastrarModulo($mTO);
        }
        return $this->editarModulo($mTO);
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Serie e Nivel de Ensino do Modulo do Porjeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloSerieNivelEnsinoTO $msneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarModuloSerieNivelEnsino($ppsneTO,$msneTO);
    }

    /**
     * Metodo que cadastra e exclui o vinculo do modulo com a serie e nivel de ensino
     * @param array ModuloSerieNivelEnsinoTO $arrTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayModuloSerieNivelEnsino($arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarArrayModuloSerieNivelEnsino($arrTO);
    }

    /**
     * Metodo que decide se Cadastra ou Exclui Disciplina do Modulo
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloDisciplina(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloDisciplinaTO $mdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarModuloDisciplina($ppsneTO,$mdTO);
    }

    /**
     * Metodo que Cadastra e Exclui os vinculos do Módulo com a Disciplina
     * @param array( ModuloDisciplinaTO $arrTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayModuloDisciplina($arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarArrayModuloDisciplina($arrTO);
    }

    /**
     * Metodo que Salva ou exclui o vinculo das trilhas na disciplina
     * @param ModuloDisciplinaTO $mdTO
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function salvarTrilhaDisciplina(ModuloDisciplinaTO $mdTO, TrilhaDisciplinaTO $tdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarTrilhaDisciplina($mdTO,$tdTO);
    }

    /**
     * Cadastra (vincula) as disciplinas na trilha de um projeto pedagógico
     * Recebe um array com as disciplinas
     * @param array $arDisciplinaTrilha
     * @return Ead1_Mensageiro
     */
    public function salvarDisciplinasTrilhaProjetoPedagogico( $arDisciplinaTrilha ){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarDisciplinasTrilhaProjetoPedagogico( $arDisciplinaTrilha );
    }



    /**
     * Metodo que decide se cadastra ou exclui o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que decide se cadastra ou edita a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarProjetoRecuperacao($prTO);
    }

    /**
     * Metodo que salva as recuperações do projeto pedagogico e os tipos de prova
     * @param ProjetoRecuperacaoTO $prTO
     * @param Array (RecuperacaoTipoProvaTO $rtpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarProjetoRecuperacaoTipoProva(ProjetoRecuperacaoTO $prTO, $arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarProjetoRecuperacaoTipoProva($prTO,$arrTO);
    }

    /**
     * Metodo que cadastra e exclui os vinculos do projeto com os tipos de prova
     * @param array( ProjetoTipoProvaTO $ptpTO)
     * @return Ead1_Mensageiro
     */
    public function salvarArrayProjetoTipoProva($arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarArrayProjetoTipoProva($arrTO);
    }

    /**
     * Metodo que Cadastra e Exclui o vinculo da Area com as Disciplinas
     * @param array (AreaConhecimentoTO $acTO)
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrayAreaProjetoPedagogico($arrTO, AreaProjetoPedagogicoTO $appTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->salvarArrayAreaProjetoPedagogico($arrTO,$appTO);
    }

    /**
     * Salvar/editar um vínculo entre módulo e área
     * @param $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function salvarModuloArea( ModuloAreaTO $moduloAreaTO ) {
        $bo	= new ProjetoPedagogicoBO();
        return $bo->salvarModuloArea( $moduloAreaTO );
    }

    /**
     * Metodo que Cadastra Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, TrilhaTO $tTO = null){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarProjetoPedagogico($ppTO,$tTO);
    }

    /**
     * Metodo que Cadastra Serie e Nivel de Ensino do Projeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarProjetoPedagogicoSerieNivelEnsino($ppsneTO);
    }

    /**
     * Metodo que vincula Area ao Projeto Pedagogico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarAreaProjetoPedagogico($appTO);
    }

    /**
     * Metodo que vincula Disciplina Extracurricular ao Projeto Pedagógico
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoExtracurricular(ProjetoExtracurricularTO $peTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarProjetoExtracurricular($peTO);
    }

    /**
     * Metodo que cadastra Trilha
     * @param TrilhaTO $tTO
     * @param int $idProjetoPedagogico
     * @return Ead1_Mensageiro
     */
    public function cadastrarTrilha( TrilhaTO $tTO, $idProjetoPedagogico ) {
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarTrilha( $tTO, $idProjetoPedagogico );
    }

    /**
     * Metodo que Cadastra Modulo do Projeto Pedagógico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModulo(ModuloTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarModulo($mTO);
    }

    /**
     * Metodo que Cadastra Serie e Nivel de Ensino do Modulo
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModuloSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloSerieNivelEnsinoTO $msneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarModuloSerieNivelEnsino($ppsneTO,$msneTO);
    }

    /**
     * Metodo que vincula o Modulo as Disciplinas
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarModuloDisciplina(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO, ModuloDisciplinaTO $mdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarModuloDisciplina($ppsneTO,$mdTO);
    }

    /**
     * Metodo que cadastra tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que Cadastra a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->cadastrarProjetoRecuperacao($prTO);
    }

    /**
     * Metodo que Edita Projeto Pedagógicp
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoPedagogico(ProjetoPedagogicoTO $ppTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarProjetoPedagogico( $ppTO );
    }


    public function editarModulo(ModuloTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarModulo($mTO);
    }

    /**
     * Metodo que Edita os Dados da Trilha do Projeto Pedagógico
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function editarDadosTrilha(TrilhaTO $tTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarDadosTrilha($tTO);
    }

    /**
     * Metodo que Edita o Tipo da Trilha do Projeto Pedagogico
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function editarTipoTrilha(TrilhaTO $tTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarTipoTrilha($tTO);
    }

    /**
     * Metodo que edita o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que Edita a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function editarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->editarProjetoRecuperacao($prTO);
    }

    /**
     * Exclui logicamente o projeto pedagógico
     * seta o bl_ativo para 0
     * @param ProjetoPedagogicoTO $projetoPedagogicoTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoPedagogico( ProjetoPedagogicoTO $projetoPedagogicoTO ){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarProjetoPedagogico( $projetoPedagogicoTO );
    }

    /**
     * Metodo que Exclui Area do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function deletarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarAreaProjetoPedagogico($appTO);
    }

    /**
     * Metodo que Exclui Serie do Projeto Pedagógico
     * @param ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoSerieNivelEnsinoTO $ppsneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarProjetoPedagogicoSerieNivelEnsino($ppsneTO);
    }

    /**
     * Metodo que Exclui o Projeto Extracurricular
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoExtracurricular(ProjetoExtracurricularTO $peTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarProjetoExtracurricular($peTO);
    }

    /**
     * Metodo que exclui Serie do Modulo
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function deletarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarModuloSerieNivelEnsino($msneTO);
    }

    /**
     * Metodo que Exclui a Disciplina do Modulo
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function deletarDisciplinaModulo(ModuloDisciplinaTO $mdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarDisciplinaModulo($mdTO);
    }

    /**
     * Metodo que Exclui o tipo da Prova do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoTipoProva(ProjetoTipoProvaTO $ptpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que Exclui a Recuperação do Projeto Pedagogico
     * @param ProjetoRecuperacaoTO $prTO
     * @return Ead1_Mensageiro
     */
    public function deletarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarProjetoRecuperacao($prTO);
    }

    /**
     * Metodo que Exclui a Disciplina pré-requisito de uma outra disciplina.
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function deletarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->deletarTrilhaDisciplina($tdTO);
    }

    /**
     * Deleta um vínculo entre módulo e área
     * @param $moduloAreaTO
     * @return Ead1_Mensageiro
     */
    public function deletarModuloArea( ModuloAreaTO $moduloAreaTO ) {
        $bo	= new ProjetoPedagogicoBO();
        return $bo->deletarModuloArea( $moduloAreaTO );
    }

    /**
     * Metodo que retorna Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogico(ProjetoPedagogicoTO $ppTO, EntidadeTO $eTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoPedagogico($ppTO,$eTO);
    }

    /**
     * Metodo que retorna Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @param EntidadeTO $eTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoPedagogico(ProjetoPedagogicoTO $ppTO, EntidadeTO $eTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarVwProjetoPedagogico($ppTO,$eTO);
    }

    /**
     * Metodo que retorna Projeto Pedagogico por Área
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @return Ead1_Mensageiro
     */
    public function retornaVwProjetoArea(VwProjetoAreaTO $vwProjetoAreaTO){
        $vwProjetoAreaTO->setId_entidade($vwProjetoAreaTO->getSessao()->id_entidade);
        return new Ead1_Mensageiro($vwProjetoAreaTO->fetch());
    }

    /**
     * Metodo Generico que retorna os tipos das Disciplinas Extra Curricular
     * @param TipoExtracurricularTO $teTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoExtracurricular(TipoExtracurricularTO $teTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoExtracurricular($teTO);
    }

    /**
     * Metodo que retorna lista de areas relacionadas a projeto pedagogico
     * sem repetir as areas
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function retornarVwProjetoAreaUnico(VwProjetoAreaTO $vwProjetoAreaTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarVwProjetoAreaUnico($vwProjetoAreaTO);
    }


    /**
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @return Ambigous <Ambigous, Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function retornarVwProjetoAreaCadastro(VwProjetoAreaTO $vwProjetoAreaTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarVwProjetoAreaCadastro($vwProjetoAreaTO);
    }


    /**
     * Metodo que retorna as Series e Nivel de Ensino do Projeto Pedagogico
     * @param ProjetoPedagogicoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogicoSerieNivelEnsino(ProjetoPedagogicoTO $ppTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoPedagogicoSerieNivelEnsino($ppTO);
    }

    /**
     * Metodo que retorna as Areas de Conhecimento do Projeto Pedagogico
     * @param ProjetoExtracurricularTO $peTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoExtracurricular(ProjetoExtracurricularTO $peTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoExtracurricular($peTO);
    }

    /**
     * Metodo que retorna os Modulos do Projeto Pedagogico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloProjetoPedagogico(ModuloTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarModuloProjetoPedagogico($mTO);
    }

    /**
     * Metodo que retorna a Serie e Nivel de Ensino do Modulo do Projeto Pedagogico
     * @param ModuloSerieNivelEnsinoTO $msneTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloSerieNivelEnsino(ModuloSerieNivelEnsinoTO $msneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarModuloSerieNivelEnsino($msneTO);
    }

    /**
     * Metodo que retorna as Disciplinas do Modulo do Projeto Pedagogico
     * @param ModuloDisciplinaTO $mdTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloDisciplina(ModuloDisciplinaTO $mdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarModuloDisciplina($mdTO);
    }

    /**
     * Metodo que retorna as Disciplinas da Trilha e seus Requisitos
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function retornarTrilhaDisciplina(TrilhaDisciplinaTO $tdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTrilhaDisciplina($tdTO);
    }

    //--

    /**
     * Metodo que retorna o tipo da Trilha
     * @param TipoTrilhaTO $ttTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoTrilha(TipoTrilhaTO $ttTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoTrilha($ttTO);
    }

    /**
     * Metodo que retorna o tipo de Trilha Fixa
     * @param TipoTrilhaFixaTO $ttfTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoTrilhaFixa(TipoTrilhaFixaTO $ttfTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoTrilhaFixa($ttfTO);
    }

    /**
     * Metodo que retorna a Trilha do Projeto Pedagogico
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function retornarTrilha(TrilhaTO $tTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTrilha($tTO);
    }

    /**
     * Metodo que retorna o Tipo de Aplicação da Avaliação
     * @param TipoAplicacaoAvaliacaoTO $taaTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoAplicacaoAvaliacao(TipoAplicacaoAvaliacaoTO $taaTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoAplicacaoAvaliacao($taaTO);
    }

    /**
     * Metodo que retorna o tipo de Duração do Contrato
     * @param ProjetoContratoDuracaoTipoTO $pcdtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoContratoDuracaoTipo(ProjetoContratoDuracaoTipoTO $pcdtTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoContratoDuracaoTipo($pcdtTO);
    }

    /**
     * Metodo que retorna o Tipo da Multa do Contrato
     * @param ProjetoContratoMultaTipoTO $pcmtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoContratoMultaTipo(ProjetoContratoMultaTipoTO $pcmtTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoContratoMultaTipo($pcmtTO);
    }

    /**
     * Metodo que retorna o tipo de Valor do projeto Pedagogico
     * @param ProjetoPedagogicoValorTipoTO $ppvtTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoPedagogicoValorTipo(ProjetoPedagogicoValorTipoTO $ppvtTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoPedagogicoValorTipo($ppvtTO);
    }

    /**
     * Metodo que retorna o Tipo da Prova Final
     * @param TipoProvaTO $tpTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoProva(TipoProvaTO $tpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoProva($tpTO);
    }

    /**
     * Metodo que retorna o Tipo de Recuperação
     * @param TipoRecuperacaoTO $trTO
     * @return Ead1_Mensageiro
     */
    public function retornarTipoRecuperacao(TipoRecuperacaoTO $trTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarTipoRecuperacao($trTO);
    }

    /**
     * Metodo que retorna o Tipo de Prova do Projeto Pedagogico
     * @param VwProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoTipoProva(VwProjetoTipoProvaTO $ptpTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoTipoProva($ptpTO);
    }

    /**
     * Metodo que retorna a Recuperação do Projeto Pedagogico
     * @param ProjetoTipoProvaTO $ptpTO
     * @return Ead1_Mensageiro
     */
    public function retornarProjetoRecuperacao(ProjetoRecuperacaoTO $prTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarProjetoRecuperacao($prTO);
    }

    /**
     * Metodo que retorna um array de ModuloTO com um array dentro de DisciplinaTO
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplina(ModuloTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarArvoreModuloDisciplina($mTO);
    }

    /**
     * Metodo que retorna uma arvode de modulos com disciplinas para o flex
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplinaFlex(ModuloTO $mTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarArvoreModuloDisciplinaFlex($mTO);
    }

    /**
     * Metodo que retorna um array de DisciplinaTO com um array dentro de DisciplinaTO (prerequisitos)
     * @param TrilhaDisciplinaTO $tdTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreTrilhaDisciplinaPrerequisitos(TrilhaDisciplinaTO $tdTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarArvoreTrilhaDisciplinaPrerequisitos($tdTO);
    }

    /**
     * Metodo que retorna um array de ModuloTO com um array dentro de DisciplinaTO e que contem um array dentro de DisciplinaTO (prerequisitos)
     * @param ModuloTO $mTO
     * @param TrilhaTO $tTO
     * @return Ead1_Mensageiro
     */
    public function retornarArvoreModuloDisciplinaTrilhaDisciplina(ModuloTO $mTO, TrilhaTO $tTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarArvoreModuloDisciplinaTrilhaDisciplina($mTO,$tTO);
    }

    /**
     * Metodo que Retorna as Disciplinas Disponivei como Pre-Requisito Para a Disciplina da Trilha
     * @param TrilhaDisciplinaTO $tdTO
     * @param array( ModuloDisciplinaTO $arrTO )
     * @return Ead1_Mensageiro
     */
    public function retornarModuloDisciplinaTrilha(TrilhaDisciplinaTO $tdTO, $arrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarModuloDisciplinaTrilha($tdTO,$arrTO);

    }

    /**
     * Metodo que Retorna as Disciplinas Disponivei como Pre-Requisito Para a Disciplina da Trilha
     * @param VwModuloDisciplinaPreRequisitoTO $ppTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloSerieDisciplinaPreRequisito(VwModuloDisciplinaPreRequisitoTO $ppTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarModuloSerieDisciplinaPreRequisito($ppTO);
    }

    /**
     * Metodo que Retorna as Disciplinas Disponiveis como Pre-Requisito Para a Disciplina da Trilha
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasPreRequisito(VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarDisciplinasPreRequisito($vwMdptTO);
    }

    /**
     * Metodo que Retorna os pre requisitos da disciplina
     * @param VwDisciplinaRequisitoTO $vwDrTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinaRequisito(VwDisciplinaRequisitoTO $vwDrTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarDisciplinaRequisito($vwDrTO);
    }

    /**
     * Retorna a área do projeto nível ensino para o componente de pesquisa do projeto pedagógico (Contrato - Matrícula).
     * @param VwAreaProjetoNivelEnsinoTO $apneTO
     * @return Ead1_Mensageiro
     */
    public function retonarAreaProjetoNivelEnsino(VwAreaProjetoNivelEnsinoTO $apneTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retonarAreaProjetoNivelEnsino($apneTO);
    }

    /**
     * Retorna os projetos pedagógicos vinculados como produto
     * @param VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoProjetoTipoValor( VwProdutoProjetoTipoValorTO $vwProdutoProjetoTipoValorTO ) {
        $projetoPedagogicoBO	= new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwProdutoProjetoTipoValor( $vwProdutoProjetoTipoValorTO );
    }

    /**
     * Retorna as disciplinas do modulo
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwModuloDisciplinaProjetoTrilha( VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO ) {
        $projetoPedagogicoBO	= new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwModuloDisciplinaProjetoTrilha( $vwMdptTO );
    }

    /**
     * Retorna as disciplinas do projeto
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasProjeto( VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO ) {
        $projetoPedagogicoBO	= new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarDisciplinasProjeto( $vwMdptTO );
    }

    /**
     * Retorna os modulos do projeto pedagogico
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModulo( ModuloTO $mTO ) {
        $projetoPedagogicoBO	= new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarModulo( $mTO );
    }

    /**
     * Retorna as disciplinas do modulo e seu status para de acordo com a validação dos pre requisitos
     * @param VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloDisciplinaStatus( VwModuloDisciplinaProjetoTrilhaTO $vwMdptTO, MatriculaTO $mTO ) {
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarModuloDisciplinaStatus($vwMdptTO,$mTO);
    }

    /**
     * Metodo que retorna os modulos do projeto pedagogico da matricula
     * @param MatriculaTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarModuloMatricula(MatriculaTO $mTO){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarModuloMatricula($mTO);
    }

    /**
     * Metodo que retorna as disciplinas do modulo
     * @param ModuloTO $mTO
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasModulo(ModuloTO $mTO){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarDisciplinasModulo($mTO);
    }

    /**
     * Metodo que retorna a view das disciplinas do modulo
     * @param VwModuloDisciplinaTO $vwMdTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwModuloDisciplina(VwModuloDisciplinaTO $vwMdTO){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwModuloDisciplina($vwMdTO);
    }

    /**
     * Metodo que retorna a entidade da sessão e as entidades que compartilham os projetos pedagogicos com a entidade da sessão
     * @param EntidadeTO $entidadeTO
     * @return Ead1_Mensageiro
     */
    public function retornarEntidadesProjetos(EntidadeTO $entidadeTO){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarEntidadesProjetos($entidadeTO);
    }

    /**
     * Metodo que retorna as disciplinas de uma entidade de acordo com os projetos pedagógicos
     * @param VwEntidadeProjetoDisciplinaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwEntidadeProjetoDisciplina(VwEntidadeProjetoDisciplinaTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwEntidadeProjetoDisciplina($to);
    }

    /**
     * Método que retorna os projetos pedagógicos da entidade
     * @param VwProjetoEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwProjetoEntidade(VwProjetoEntidadeTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwProjetoEntidade($to);
    }

    /**
     * Método que retorna as turmas disponiveis pra matricula do projeto
     * @param VwTurmasDisponiveisTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwTurmasDisponiveis(VwTurmasDisponiveisTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwTurmasDisponiveis($to);
    }


    /**
     * Método que retorna usuários que podem ser cadastrados como coordenadores
     * @param VwUsuarioPerfilPedagogicoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornaUsuarioPedagogico(VwUsuarioPerfilPedagogicoTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwUsuarioPerfilPedagogico($to);
    }


    /**
     * Método que retorna Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param VwUsuarioPerfilEntidadeReferenciaTO $to
     */
    public function retornaCoordenador(VwUsuarioPerfilEntidadeReferenciaTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwUsuarioPerfilEntidadeReferencia($to);
    }

    /**
     * Método que retorna pessoas Coordenador de projeto pedagogico ou de disciplinas ou professor - integração moodle
     * @param VwUsuarioPerfilEntidadeTO $to
     */
    public function retornarVwUsuarioPerfilEntidade(VwUsuarioPerfilEntidadeTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->retornarVwUsuarioPerfilEntidade($to);
    }

    /**
     * Método que salva Coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarCoordenador(UsuarioPerfilEntidadeReferenciaTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->salvarCoordenador($to);
    }


    /** Método que seta como titular o de projeto pedagogico ou de disciplinas - integração moodle
     * Apenas 1 pode ser coordenador titular de cada perfil
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function setarCoordenadorTitular(UsuarioPerfilEntidadeReferenciaTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->setarCoordenador($to);
    }

    /**
     * Exclui de forma lógica coordenador de projeto pedagogico ou de disciplinas - integração moodle
     * @param UsuarioPerfilEntidadeReferenciaTO $to
     * @return Ead1_Mensageiro
     */
    public function excluirCoordenador(UsuarioPerfilEntidadeReferenciaTO $to){
        $projetoPedagogicoBO = new ProjetoPedagogicoBO();
        return $projetoPedagogicoBO->excluirCoordenador($to);

    }

    /**
     * Metodo que retorna somente as areas de de conhecimento
     * @param VwProjetoAreaTO $vwProjetoAreaTO
     * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
     */
    public function retornarVwAreaProjetoPedagogico(VwProjetoAreaTO $vwProjetoAreaTO){

    }

    /**
     * Metodo que retorna Areas de Conhecimento do Projeto Pedagógico
     * @param AreaProjetoPedagogicoTO $appTO
     * @return Ead1_Mensageiro
     */
    public function retornarAreaProjetoPedagogico(AreaProjetoPedagogicoTO $appTO){
        $bo = new ProjetoPedagogicoBO();
        return $bo->retornarAreaProjetoPedagogico($appTO);
    }

}

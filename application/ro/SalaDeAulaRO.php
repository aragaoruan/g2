<?php
/**
 * Classe de comunicação com Flex para interações com objeto SalaDeAula
 * @author Eduardo Romão - ejushiro@gmail.com
 * 
 * @package application
 * @subpackage ro
 */
class SalaDeAulaRO extends Ead1_RO{
	
	public $mensageiro;
	/**
	 * @var SalaDeAulaBO
	 */
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new SalaDeAulaBO();
	}
	
	/**
	 * Metodo que Decide se Cadastra ou Edita os Dados Basicos da Sala de Aula
	 * @param SalaDeAulaTO $sdaTO
	 * @param DisciplinaSalaDeAulaTO $dsaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDadosBasicosSalaDeAula(SalaDeAulaTO $sdaTO, DisciplinaSalaDeAulaTO $dsaTO){
		if($sdaTO->getId_saladeaula()){
			return $this->editarSalaDeAula($sdaTO, $dsaTO);
		}
		return $this->cadastrarDadosBasicosSalaDeAula($sdaTO, $dsaTO);
	}
	
	/**
	 * Metodo que Decide se Cadastra* ou Edita as Datas da Sala de Aula
	 * @param SalaDeAulaTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDatasSalaDeAula(SalaDeAulaTO $sdaTO){
		return $this->cadastrarDatasSalaDeAula($sdaTO);
	}
	
	/**
	 * Metodo que cadastra entidades na sala de aula
	 * @param SalaDeAulaEntidadeTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $sdaeTO){
		return $this->cadastrarSalaDeAulaEntidade($sdaeTO);
	}
	
	/**
	 * Metodo que cadastra entidades na sala de aula
	 * @param array $arrEntidades
	 * @param SalaDeAulaTO $sTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarSalaDeAulaEntidadeArvore($arrEntidades, SalaDeAulaTO $sTO){
		return $this->bo->cadastrarSalaDeAulaEntidadeArvore($arrEntidades, $sTO);
	}
					
	/**
	 * Metodo que Decide se Cadastra ou Edita o Professor da Sala de Aula
	 * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO){
		if($sdapTO->getId_saladeaulaprofessor()){
			return $this->editarSalaDeAulaProfessor($uperTO,$sdapTO);
		}
		return $this->cadastrarSalaDeAulaProfessor($uperTO,$sdapTO);
	}
	
	/**
	 * Metodo que salva o vinculo entre sala de aula, horario e local
	 * @param SalaHorarioLocalTO $shlTO
	 * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
	 * return Ead1_Mensageiro
	 */
	
	public function salvarSalaHorarioLocal(SalaHorarioLocalTO $shlTO, UsuarioPerfilEntidadeReferenciaTO $uperTO, $oldSalaHorarioLocalTO) {
		return $this->bo->cadastrarSalaHorarioLocal($shlTO, $uperTO, $oldSalaHorarioLocalTO);
	}
	
			
	/**
	 * Metodo que decide se Cadastra ou Exclui a Aula da Sala de Aula
	 * @param AulaSalaDeAulaTO $asdaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO){
		if($asdaTO->getId_aulasaladeaula()){
			return $this->editarAulaSalaDeAula($asdaTO);
		}
		return $this->cadastrarAulaSalaDeAula($asdaTO);
	}
			
	/**
	 * Metodo que decide se Cadastra ou Exclui vinculo
	 * @param AreaProjetoSalaTO $apsTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAreaProjetoSala(AreaProjetoSalaTO $apsTO){
		if(!$apsTO->getId_areaprojetosala()){
			return $this->cadastrarAreaProjetoSala($apsTO);
		}
		return $this->deletarAreaProjetoSala($apsTO);
	}
	
	/**
	 * Metodo que Cadastra e Exclui as Permissões na Area, Projeto e Sala de Aula;
	 * @param array( AreaProjetoSalaTO $apsTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayAreaProjetoSala($arrTO){
		return $this->bo->salvarArrayAreaProjetoSala($arrTO);
	}
			
	/**
	 * Metodo que cadastra a disciplina da sala de aula, caso aja mais vinculos os exclui para ficar 1 só
	 * @param DisciplinaSalaDeAulaTO $dsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO){
		return $this->bo->salvarDisciplinaSalaDeAula($dsdaTO);
	}
			
	/**
	 * Metodo que verifica e salva a alocacao do aluno
	 * @param AlocacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarAlocacaoAluno(AlocacaoTO $aTO){
		return $this->bo->salvarAlocacaoAluno($aTO);
	}
			
	/**
	 * Metodo que reintegra o aluno na sala de aula
	 * @param AlocacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function reintegrarAlocacaoAluno(AlocacaoTO $aTO){
		return $this->bo->reintegrarAlocacaoAluno($aTO);
	}
			
	/**
	 * Metodo que reintegra todos os alunos da sala de aula
	 * @param Ead1_TO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function reintegrarAlocacaoAlunoSalaDeAula(Ead1_TO $to){
		
		if($to instanceof AlocacaoTO){
			
			$nto = new SalaDeAulaTO();
			$nto->setId_saladeaula($to->getId_saladeaula());
			$nto->fetch(true, true, true);
			
			$to = $nto;
		}
		
		return $this->bo->reintegrarAlocacaoAlunoSalaDeAula($to);
	}
	
	/**
	 * Metodo que reintegra todos os professores da sala de aula
	 * @param SalaDeAulaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function reintegrarProfessoresSalaDeAula(SalaDeAulaTO $to){
		return $this->bo->reintegrarProfessoresSalaDeAula($to);
	}
	
	/**
	 * Metodo que salva um array de SalaDeAulaEntidadeTO
	 * @param Array $arrSalaDeAulaEntidadeTO
	 * @throws Zend_Exception
	 */
	public function salvarArraySalaDeAulaEntidade($arrSalaDeAulaEntidadeTO){
		return $this->bo->salvarArraySalaDeAulaEntidade($arrSalaDeAulaEntidadeTO);
	}
		
	/**
	 * Metodo que cadastra dados Basicos da Sala de Aula, menos as datas.
	 * @param SalaDeAulaTO $sdaTO
	 * @param DisciplinaSalaDeAulaTO $dsaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDadosBasicosSalaDeAula(SalaDeAulaTO $sdaTO, DisciplinaSalaDeAulaTO $dsaTO){
		return $this->bo->cadastrarDadosBasicosSalaDeAula($sdaTO, $dsaTO);
	}
		
	/**
	 * Metodo que Cadastra as Datas da Sala de Aula, (edita a sala de aula).
	 * @param SalaDeAulaTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDatasSalaDeAula(SalaDeAulaTO $sdaTO){
		return $this->bo->cadastrarDatasSalaDeAula($sdaTO);
	}
	
	/**
	 * Metodo que Cadastra a Alocação do Aluno
	 * @param AlocacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAlocacao(AlocacaoTO $aTO){
		return $this->bo->cadastrarAlocacao($aTO);
	}
	/**
	 * Metodo que Cadastra várias Salas de aula
	 * @param array $arrSalas
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArraySalas($arrSalas){
		return $this->bo->cadastrarArraySalas($arrSalas);
	}
		
	/**
	 * Metodo que Vincula o Professor a Sala de Aula
	 * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO){
		return $this->bo->cadastrarSalaDeAulaProfessor($uperTO,$sdapTO);
	}
		
	/**
	 * Metodo que Cadastra as Aulas da Sala de Aula
	 * @param AulaSalaDeAulaTO $asdaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO){
		return $this->bo->cadastrarAulaSalaDeAula($asdaTO);
	}
		
	/**
	 * Metodo que vincula Sala de Aula com a Area de Conhecimento ou Nivel de Ensino ou Projeto Pedagogico
	 * @param AreaProjetoSalaTO $apsTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarAreaProjetoSala(AreaProjetoSalaTO $apsTO){
		return $this->bo->cadastrarAreaProjetoSala($apsTO);
	}
		
	/**
	 * Metodo que Vincula a Disciplina a Sala de Aula
	 * @param DisciplinaSalaDeAulaTO $dsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO){
		return $this->bo->cadastrarDisciplinaSalaDeAula($dsdaTO);
	}
		
	/**
	 * Metodo que Edita Sala de Aula
	 * @param SalaDeAulaTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function editarSalaDeAula(SalaDeAulaTO $sdaTO){
		return $this->bo->editarSalaDeAula($sdaTO);
	}
	
	/**
	 * Metodo que Edita a Alocação do Aluno
	 * @param AlocacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAlocacao(AlocacaoTO $aTO){
		return $this->bo->editarAlocacao($aTO);
	}
		
	/**
	 * MEtodo que Edita a Aula da Sala de Aula
	 * @param AulaSalaDeAulaTO $asdaTO
	 * @return Ead1_Mensageiro
	 */
	public function editarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO){
		return $this->bo->editarAulaSalaDeAula($asdaTO);
	}
	
	/**
	 * Metodo que Edita a Sala de Aula do Professor
	 * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function editarSalaDeAulaProfessor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO){
		return $this->bo->editarSalaDeAulaProfessor($uperTO,$sdapTO);
	}
		
	/**
	 * Metodo que Exclui o Professor da Sala de Aula
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sdapTO){
		return $this->bo->deletarSalaDeAulaProfessor($sdapTO);
	}
		
	/**
	 * Metodo que Exclui vinculo da Sala de Aula com a Area de Conhecimento ou Projeto Pedagogico ou Nivel de Ensino
	 * @param AreaProjetoSalaTO $apsTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarAreaProjetoSala(AreaProjetoSalaTO $apsTO){
		return $this->bo->deletarAreaProjetoSala($apsTO);
	}
		
	/**
	 * @todo Usar Bl_ativo!!!
	 * Metodo que Exclui Sala de Aula
	 * @param SalaDeAulaTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarSalaDeAula( SalaDeAulaTO $sdaTO ){
		return $this->bo->deletarSalaDeAula($sdaTO);
	}
			
	/**
	 * Metodo que Exclui o Vinculo da Disciplina com a Sala de Aula
	 * @param DisciplinaSalaDeAulaTO $dsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO){
		return $this->bo->deletarDisciplinaSalaDeAula($dsdaTO);
	}
	
	/**
	 * Metodo que Inativa o Professor do vinculo da Sala de Aula
	 * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarProfessorSalaDeAula(UsuarioPerfilEntidadeReferenciaTO $uperTO){
		return $this->bo->deletarProfessorSalaDeAula($uperTO);
	}
	
	/**
	 * Metodo que remove o vinculo entre um horário e uma Sala de Aula
	 * @param SalaHorarioLocalTO $shlTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarSalaHorarioLocal(SalaHorarioLocalTO $shlTO){
		return $this->bo->deletarSalaHorarioLocal($shlTO);
	}
		
	/**
	 * Metodo que Retorna Sala de Aula
	 * @param SalaDeAulaTO $sdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAula(SalaDeAulaTO $sdaTO){
		return $this->bo->retornarSalaDeAula($sdaTO);
	}
		
	/**
	 * Metodo que Retorna o Professor da Sala de Aula
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAulaProfessor(SalaDeAulaProfessorTO $sdapTO){
		return $this->bo->retornarSalaDeAulaProfessor($sdapTO);
	}
		
	
	/**
	 * Metodo que Retorna os Horarios de uma Sala de Aula
	 * @param Ead1_TO $shlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSalaHorarioLocal(Ead1_TO $shlTO){
		
		if($shlTO instanceof SalaHorarioLocalTO){
			$to = new VwSalaHorarioLocalTO();
			$to->montaToDinamico($shlTO->toArray());
		} else {
			$to = $shlTO;
		}
		
		return $this->bo->retornarVwSalaHorarioLocal($to);
	}
	
	/**
	 * Metodo que Retorna Vinculos da Sala de Aula
	 * @param AreaProjetoSalaTO $apsTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAreaProjetoSala(AreaProjetoSalaTO $apsTO){
		return $this->bo->retornarAreaProjetoSala($apsTO);
	}
		
	/**
	 * Metodo que Retorna as Modalidades da Sala de Aula
	 * @param ModalidadeSalaDeAulaTO $msdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarModalidadeSalaDeAula(ModalidadeSalaDeAulaTO $msdaTO){
		return $this->bo->retornarModalidadeSalaDeAula($msdaTO);
	}
		
	/**
	 * Metodo que Retorna a Disciplina da Sala de Aula
	 * @param DisciplinaSalaDeAulaTO $dsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDisciplinaSalaDeAula(DisciplinaSalaDeAulaTO $dsdaTO){
		return $this->bo->retornarDisciplinaSalaDeAula($dsdaTO);
	}
		
	/**
	 * MXetodo que Retorna o Tipo da Aula
	 * @param TipoAulaTO $taTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoAula(TipoAulaTO $taTO){
		return $this->bo->retornarTipoAula($taTO);
	}
		
	/**
	 * Metodo que Retorna a Aula da Sala de Aula
	 * @param AulaSalaDeAulaTO $asdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAulaSalaDeAula(AulaSalaDeAulaTO $asdaTO){
		return $this->bo->retornarAulaSalaDeAula($asdaTO);
	}
		
	/**
	 * Metodo que Retorna o Tipo da Sala de Aula
	 * @param TipoSalaDeAulaTO $tsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoSalaDeAula(TipoSalaDeAulaTO $tsdaTO){
		return $this->bo->retornarTipoSalaDeAula($tsdaTO);
	}
	
	/**
	 * Metodo que Retorna o Tipo de Avaliação da Sala de Aula
	 * @param TipoAvaliacaoSalaDeAulaTO $tasdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoAvaliacaoSalaDeAula(TipoAvaliacaoSalaDeAulaTO $tasdaTO){
		return $this->bo->retornarTipoAvaliacaoSalaDeAula($tasdaTO);
	}
	
	/**
	 * Método que Retorna o Período Letivo.
	 * @param PeriodoLetivoTO $plTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPeriodoLetivo(PeriodoLetivoTO $plTO) {
		return $this->bo->retornarPeriodoLetivo($plTO);
	}
	
	/**
	 * Metodo que Retorna o Professor da Sala de Aula e a referencia
	 * @param SalaDeAulaProfessorTO $sdapTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAulaProfessorReferencia(SalaDeAulaProfessorTO $sdapTO){
		return $this->bo->retornarSalaDeAulaProfessorReferencia($sdapTO);
	}
	
	/**
	 * Metodo que retorna o professor de uma sala de aula
	 * @param VwProfessorSalaDeAulaTO $vwPsdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProfessorSalaDeAula(VwProfessorSalaDeAulaTO $vwPsdaTO){
		return $this->bo->retornarProfessorSalaDeAula($vwPsdaTO);
	}
	
	/**
	 * Metodo que retorna as Salas de Aula disponiveis ao aluno
	 * @param VwSalaDisciplinaProjetoTO $vwSdpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSalaDisciplinaProjeto(VwSalaDisciplinaProjetoTO $vwSdpTO){
		return $this->bo->retornarVwSalaDisciplinaProjeto($vwSdpTO);
	}
	
	/**
	 * Metodo que retorna as Salas de Aula disponiveis ao aluno
	 * @param VwSalaDisciplinaAlocacaoTO $vwSdaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSalaDisciplinaAlocacao(VwSalaDisciplinaAlocacaoTO $vwSdaTO){
		return $this->bo->retornarVwSalaDisciplinaAlocacao($vwSdaTO);
	}

	/**
	 * Metodo que retorna a Alocação do Aluno
	 * @param AlocacaoTO $aTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarAlocacao(AlocacaoTO $aTO){
		return $this->bo->retornarAlocacao($aTO);
	}

	/**
	 * Metodo que retorna dados detalhadados de uma sala de aula a encerrar.
	 * @param int $id_saladeaula
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosDetalhamentoSala($id_saladeaula){
		return $this->bo->retornarDadosDetalhamentoSala($id_saladeaula);
	}

	/**
	 * Metodo que seta o professor titular da sala de aula professor
	 * @param VwProfessorSalaDeAulaTO $vwsdapTO
	 * @return Ead1_Mensageiro
	 */
	public function setarProfessorTitularSalaDeAula(VwProfessorSalaDeAulaTO $vwsdapTO){
		return $this->bo->setarProfessorTitularSalaDeAula($vwsdapTO);
	}
	
	/**
	 * Metodo que retorna as Instituições da sala de aula
	 * @param SalaDeAulaEntidadeTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAulaEntidade(SalaDeAulaEntidadeTO $to){
		return $this->bo->retornarSalaDeAulaEntidade($to);
	}

	/**
	 * Método que chama o WebService para a sala de aula
	 * @param SalaDeAulaTO $sdaTO
	 * @param int $idSistema
	 * @return Ead1_Mensageiro
	 */
	public function wsSalaDeAula(SalaDeAulaTO $sdaTO, $idSistema){
		return $this->bo->wsSalaDeAula($sdaTO, $idSistema);
	}
	
	/**
	 * Chama WebService para várias salas de aula.
	 * @param $arrSalas
	 * @param int $idSistema
	 */
	public function wsSalasDeAula($arrSalas, $idSistema){
		return $this->bo->wsSalasDeAula($arrSalas, $idSistema);
	}

	/**
	 * Método que retorna integração da sala de aula
	 * @param SalaDeAulaIntegracaoTO $sdaiTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarSalaDeAulaIntegracao(SalaDeAulaIntegracaoTO $sdaiTO){
		return $this->bo->retornarSalaDeAulaIntegracao($sdaiTO);
	}
	
	
	/**
	 * Metodo que retorna as Salas de Aula do aluno
	 * @param VwSalaDisciplinaProjetoTO $vwSdpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwSalaDisciplinaProjetoAlocadas(VwSalaDisciplinaProjetoTO $vwSdpTO){
		return $this->bo->retornarVwSalaDisciplinaProjetoAlocadas($vwSdpTO);
	}
	
	
	/**
	 * Método que executa alocação automática solicitada pelo Felipe com vase na VW vw_alunosautoalocar
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @return Ambigous <multitype:, boolean, Ead1_Mensageiro>
	 */
	public function sincronizarAutoAlocar(){
		return $this->bo->sincronizarAutoAlocar();
	}
        
	/**
	 * Método que chama o ws de importacao de conteudos para as salas do moodle
	 * @param SalaDeAulaTO $salaDeAula
	 * @return Ead1_Mensageiro
	 */
	public function sincronizarConteudoMoodle(SalaDeAulaTO $salaDeAula){
		return $this->bo->sincronizarConteudoMoodle($salaDeAula);
	}
	
        /**
         * Executa auto alocação por aluno
         * @param MatriculaTO $matriculaTO
         * @return SalaDeAulaBO
         * 
         * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
         */
        public function sincronizarAutoAlocarPorAluno(MatriculaTO $matriculaTO){
		return $this->bo->sincronizarAutoAlocarPorAluno($matriculaTO);
	}

    /**
     * Retorna a tb_categoriasala
     * @param CategoriaSalaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarCategoriaSala(CategoriaSalaTO $to){
        return $this->bo->retornarCategoriaSala($to);
    }
	

}

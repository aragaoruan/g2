<?php
/**
 * Classe que controla requisições referentes a Mensagem
 * @author eduardoromao
 */
class MensagemRO extends Ead1_RO{

	
	public function __construct(){
		$this->_bo = new MensagemBO();
	}
	
	private $_bo;
	
	/**
	 * Método que reenvia o email de ativação de matrícula
	 * @param MatriculaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function reenviarEmailAtivacaoMatricula(MatriculaTO $to){
		return $this->_bo->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::MATRICULA, $to, TipoEnvioTO::EMAIL,true);
	}
	
	/**
	 * Método que reenvia o email de dados de acesso
	 * @param PessoaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function enviarEmailDadosAcesso(Ead1_TO_Dinamico $to){
		return $this->_bo->procedimentoGerarEnvioMensagemPadraoEntidade(MensagemPadraoTO::DADOS_ACESSO, $to, TipoEnvioTO::EMAIL,true);
	}
	
}
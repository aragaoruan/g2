<?php
/**
 * Classe Responsável por todas as configurações e geração dos relatórios 
 * @author edermariano
 *
 */
class RelatoriosRO extends Ead1_RO{
	
	/**
	 * @var RelatorioBO
	 */
	public $bo;
	
	public function __construct(){
		$this->bo = new RelatorioBO();
	}
	
	/**
	 * Método que cadastra um relatório com suas propriedades
	 * @param RelatorioTO $relatorioTO
	 * @param array $dadosCampos
	 * @return Ead1_Mensageiro
	 */
	public function salvarRelatorio(RelatorioTO $relatorioTO, $dadosCampos){
		$bo = new RelatorioBO();
		return $bo->salvarRelatorio($relatorioTO, $dadosCampos);
	}
	
	/**
	 * Método que busca as propriedades de configuração do Relatório
	 * @param TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPropriedadesRelatorio(TipoPropriedadeCampoRelTO $tipoPropriedadeCampoRelatorioTO){
		$bo = new RelatorioBO();
		return $bo->retornarPropriedadesRelatorio($tipoPropriedadeCampoRelatorioTO);
	}
	
	/**
	 * Método que busca os tipos de origem do Relatório
	 * @param TipoOrigemRelatorioTO $tipoOrigemRelatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoOrigemRelatorio(TipoOrigemRelatorioTO $tipoOrigemRelatorioTO){
		$bo = new RelatorioBO();
		return $bo->retornarTipoOrigemRelatorio($tipoOrigemRelatorioTO);
	}
	
	/**
	 * Método que retorna as views que não são do esquema principal DBO
	 * @return Ead1_Mensageiro
	 */
	public function retornarViewsRelatorio(){
		$bo = new RelatorioBO();
		return $bo->retornarViewsRelatorio();
	}
	
	/**
	 * Método que retorna relatório
	 * @param RelatorioTO $relatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarRelatorio(RelatorioTO $relatorioTO){
		return $this->bo->retornarRelatorio($relatorioTO);
	}
	
	/**
	 * Método que retorna campos de relatório
	 * @param CampoRelatorioTO $campoRelatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampoRelatorio(CampoRelatorioTO $campoRelatorioTO){
		return $this->bo->retornarCampoRelatorio($campoRelatorioTO);
	}
	
	/**
	 * Método que retorna propriedades de campo de relatório
	 * @param PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarPropriedadeCampoRelatorio(PropriedadeCampoRelatorioTO $propriedadeCampoRelatorioTO){
		return $this->bo->retornarPropriedadeCampoRelatorio($propriedadeCampoRelatorioTO);
	}
	
	/**
	 * Método que retorna os dados de configuração do relatório para confecção dos campos
	 * @param VwDadosRelatorioTO $vwDadosRelatorioTO
	 * @param string $resposta
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwDadosRelatorio(VwDadosRelatorioTO $vwDadosRelatorioTO, $resposta = null){
		$bo = new RelatorioBO();
		return $bo->retornarVwDadosRelatorio($vwDadosRelatorioTO, $resposta);
	}
	
	/**
	 * Método que busca os dados de acordo com a configuração dos filtros pelo usuário
	 * @param int $idRelatorio id do relatório em questão
	 * @param mixed $arFiltros array com os filtros setados pelo usuário
	 * @param int $pagina número da página a ser exibida
	 * @param int $qtdRegistros quantidade de registros por página
	 * @param boolean $soDados parametro que define se irá só os dados ou todas as informações do relatório
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosRelatorio($idRelatorio, $arFiltros = null, $pagina = 1, $qtdRegistros = 30, $soDados = false){
		$bo = new RelatorioBO();
		return $bo->retornarDadosRelatorio($idRelatorio, $arFiltros, $pagina, $qtdRegistros, $soDados);
	}
	
	/**
	 * Método que busca os dados de acordo com a configuração dos filtros pelo usuário
	 * @param int $idFuncionalidade id da funcionalidade em questão
	 * @param mixed $arFiltros array com os filtros setados pelo usuário
	 * @param int $pagina número da página a ser exibida
	 * @param int $qtdRegistros quantidade de registros por página
	 * @param boolean $soDados parametro que define se irá só os dados ou todas as informações do relatório
	 * @return Ead1_Mensageiro
	 */
	public function retornarDadosRelatorioPorFuncionalidade($idFuncionalidade, $arFiltros = null, $pagina = 1, $qtdRegistros = 30, $soDados = false){
		$bo = new RelatorioBO();
		return $bo->retornarDadosRelatorio(null, $arFiltros, $pagina, $qtdRegistros, $soDados, $idFuncionalidade);
	}
}
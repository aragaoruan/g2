<?php
/**
 * Classe de comunicação com Flex para interações com objeto de Produtos
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class ProdutoRO extends Ead1_RO{
	
	/**
	 * @var ProdutoBO
	 */
	private $bo;
	private $mensageiro;
	
	public function __construct(){
		$this->bo = new ProdutoBO();
		$this->mensageiro = new Ead1_Mensageiro();
	}
	
	/**
	 * Metodo que decide se salva ou edita o produto
	 * 
	 * @param ProdutoTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarProduto(ProdutoTO $pTO, array $arcategoriasProduto = null){
		if($pTO->getId_produto()){
			return $this->editarProduto($pTO, $arcategoriasProduto);
		}
		return $this->cadastrarProduto($pTO, $arcategoriasProduto);
	}

    /**
     * Metodo que decide se salva ou edita o produto do tipo PRR
     * @param ProdutoTO $pTO
     * @param ProdutoPrrTO $prrTO
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function salvarProdutoPrr(ProdutoTO $pTO, ProdutoPrrTO $prrTO, ProdutoValorTO $pvTO){
        if($pTO->getId_produto()){
            return $this->editarProdutoPrr($pTO, $pvTO);
        }
        return $this->cadastrarProdutoPrr($pTO, $prrTO, $pvTO);
    }
	
	/**
	 * Método que salva o produto e cria o vinculo entre o produto e avaliacao
	 * @param ProdutoTO $produtoTO
	 * @param AvaliacaoTO $avaliacaoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoProdutoAvaliacao(ProdutoTO $produtoTO,AvaliacaoTO $avaliacaoTO){
		return $this->bo->salvarProdutoProdutoAvaliacao($produtoTO,$avaliacaoTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita o produto projeto pedagogico
	 * @param ProdutoProjetoPedagogicoTO $pppTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
		return $this->bo->salvarProdutoProjetoPedagogico($pppTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita o produto valor
	 * @param ProdutoValorTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoValor(ProdutoValorTO $pvTO){
		if($pvTO->getId_produtovalor()){
			return $this->editarProdutoValor($pvTO);
		}
		return $this->cadastrarProdutoValor($pvTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita o Lançamento da Venda
	 * @param LancamentoVendaTO $lvTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarLancamentoVenda(LancamentoVendaTO $lvTO){
		return $this->salvarLancamentoVenda($lvTO);
	}
	
	/**
	 * Método para salvar array de venda de produtos!
	 * 	Paleativo! Criar no BO
	 * @param array $arVendaProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArVendaProduto(array $arVendaProdutoTO){
		$dao = new ProdutoDAO();
		$dao->beginTransaction();
		try{
			foreach($arVendaProdutoTO as $vendaProdutoTO){
				$mensageiro = $this->salvarVendaProduto($vendaProdutoTO);
				if($mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO){
					throw new Zend_Exception("Erro ao Salvar Venda Produto!");
				}
			}
			$dao->commit();
			return new Ead1_Mensageiro("Produto(s) Cadastrado(s) com Sucesso!");
		}catch(Zend_Exception $e){
			$dao->rollBack();
			return $mensageiro;
		}
	}
	
	/**
	 * Metodo que decide se salva ou edita a venda do produto
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarVendaProduto(VendaProdutoTO $vpTO){
		if($vpTO->getId_vendaproduto()){
			return $this->editarVendaProduto($vpTO);
		}
		return $this->cadastrarVendaProduto($vpTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita a venda
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarVenda(VendaTO $vTO){
		if($vTO->getId_venda()){
			return $this->editarVenda($vTO);
		}
		return $this->cadastrarVenda($vTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita o Lançamento
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarLancamento(LancamentoTO $lTO){
		if($lTO->getId_lancamento()){
			return $this->editarLancamento($lTO);
		}
		return $this->cadastrarLancamento($lTO);
	}
	
	/**
	 * Metodo que decide se salva ou edita Tag
	 * @param TagTO $tagTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarTag(TagTO $tagTO){
		if($tagTO->getId_tag()){
			return $this->editarTag($tagTO);
		}
		return $this->cadastrarTag($tagTO);
	}
	
	/**
	 * Salva os planos de pagamento do produto.
	 * @param array $arrPlanoPagamentoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrPlanoPagamento($arrPlanoPagamentoTO){
		return $this->bo->salvarArrPlanoPagamento($arrPlanoPagamentoTO);
	}
	
	/**
	 * Metodo que exclui os vinculos do produto com o texto sistema e cadastra um novo
	 * @param ProdutoTextoSistemaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoTextoSistema(ProdutoTextoSistemaTO $to){
		return $this->bo->salvarProdutoTextoSistema($to);
	}
	
	/**
	 * Metodo que exclui os vinculos do produto com a taxa e cadastra um novo
	 * @param ProdutoTaxaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoTaxa(ProdutoTaxaTO $to){
		return $this->bo->salvarProdutoTaxa($to);
	}
	
	/**
	 * Metodo que exclui os vinculos do produto com o produto e cadastra um novo
	 * @param ProdutoLivroTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoLivro(ProdutoLivroTO $to){
		return $this->bo->salvarProdutoLivro($to);
	}
	
	/**
	 * Metodo que exclui os vinculos do produto com a taxa e o projeto e cadastra os novos
	 * @param array $arProdutoTaxaProjetoTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArProdutoTaxaProjeto(array $arProdutoTaxaProjetoTO){
		return $this->bo->salvarArProdutoTaxaProjeto($arProdutoTaxaProjetoTO);
	}
	
	/**
	 * Metodo que cadastra CampanhaProduto
	 * @param CampanhaProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaProduto(CampanhaProdutoTO $cpTO){
		return $this->bo->cadastrarCampanhaProduto($cpTO);
	}
	
	/**
	 * Metodo que cadastra Tag
	 * @param TagTO $tagTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTag(TagTO $tagTO){
		return $this->bo->cadastrarTag($tagTO);
	}
	
	/**
	 * Metodo que cadastra Tag de Produto
	 * @param TagProdutoTO $tagProdutoTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTagProduto(TagProdutoTO $tagProdutoTO){
		return $this->bo->cadastrarTagProduto($tagProdutoTO);
	}
	
	/**
	 * Metodo que cadastra array CampanhaProduto
	 * @param $arrayCampanhaProduto
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArrayCampanhaProduto($arrayCampanhaProduto){
		return $this->bo->cadastrarArrayCampanhaProduto($arrayCampanhaProduto);
	}
	
	/**
	 * Metodo que cadastra tags para um produto.
	 * @param int $id_produto
	 * @param array $arrTags
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarArrTagProduto($id_produto, $arrTags){
		return $this->bo->cadastrarArrTagProduto($id_produto, $arrTags);
	}
	
	/**
	 * Metodo que vincula todos os produtos a uma campanha
	 * @param $arrayCampanhaProduto
	 * @return Ead1_Mensageiro
	 */
	public function vincularTodasCampanhaProduto(CampanhaProdutoTO  $capoTO){
		return $this->bo->vincularTodasCampanhaProduto($capoTO);
	}
	
	/**
	 * Metodo que cadastra Produto
	 * @param ProdutoTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProduto(ProdutoTO $pTO , array $arcategoriasProduto = null){
		return $this->bo->cadastrarProduto($pTO , $arcategoriasProduto);
	}

    /**
     * Metodo que cadastra ProdutoPrr
     * @param ProdutoTO $pTO
     * @param ProdutoPrrTO $prrTO
     * @param ProdutoValorTO $pvTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarProdutoPrr(ProdutoTO $pTO, ProdutoPrrTO $prrTO, ProdutoValorTO $pvTO){
        return $this->bo->cadastrarProdutoPrr($pTO, $prrTO, $pvTO);
    }
	
	/**
	 * Método que cadastra um Projeto pedagógico a taxa em um produto
	 * @param ProdutoTaxaProjetoTO $ptpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProdutoTaxaProjeto(ProdutoTaxaProjetoTO $ptpTO){
		return $this->bo->cadastrarProdutoTaxaProjeto($ptpTO);
	}
	
	/**
	 * Metodo que cadastra ProdutoProjetoPedagogico
	 * @param ProdutoProjetoPedagogicoTO $pppTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
		return $this->bo->cadastrarProdutoProjetoPedagogico($pppTO);
	}
	
	/**
	 * Metodo que cadastra ProdutoValor
	 * @param ProdutoValorTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProdutoValor(ProdutoValorTO $pvTO){
		return $this->bo->cadastrarProdutoValor($pvTO);
	}
	
	/**
	 * Metodo que cadastra LancamentoVenda
	 * @param LancamentoVendaTO $lvTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLancamentoVenda(LancamentoVendaTO $lvTO){
		return $this->bo->cadastrarLancamentoVenda($lvTO);
	}
	
	/**
	 * Metodo que cadastra VendaProduto
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVendaProduto(VendaProdutoTO $vpTO){
		return $this->bo->cadastrarVendaProduto($vpTO);
	}
	
	/**
	 * Metodo que cadastra o vinculo do produto com a declaracao (textosistema)
	 * @param ProdutoTextoSistemaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarProdutoTextoSistema(ProdutoTextoSistemaTO $to){
		return $this->bo->cadastrarProdutoTextoSistema($to);
	}
	
	/**
	 * Metodo que cadastra Venda
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarVenda(VendaTO $vTO){
		return $this->bo->cadastrarVenda($vTO);
	}
	
	/**
	 * Metodo que cadastra Lancamento
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarLancamento(LancamentoTO $lTO){
		return $this->bo->cadastrarLancamento($$lTO);
	}
	
	/**
	 * Metodo que Edita Produto
	 * @param ProdutoTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function editarProduto(ProdutoTO $pTO , array $arcategoriasProduto = null){
		return $this->bo->editarProduto($pTO, $arcategoriasProduto);
	}

    /**
     * Metodo que Edita Produto do tipo PRR
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     * AC-433
     */
    public function editarProdutoPrr(ProdutoTO $pTO, ProdutoValorTO $pvTO){
        return $this->bo->editarProdutoPrr($pTO, $pvTO);
    }
	
	/**
	 * Metodo que Edita ProdutoProjetoPedagogico
	 * @param ProdutoProjetoPedagogicoTO $pppTO
	 * @return Ead1_Mensageiro
	 */
	public function editarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
		return $this->bo->editarProdutoProjetoPedagogico($pppTO);
	}
	
	/**
	 * Metodo que Edita ProdutoValor
	 * @param ProdutoValorTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function editarProdutoValor(ProdutoValorTO $pvTO){
		return $this->bo->editarProdutoValor($pvTO);
	}
	
	/**
	 * Metodo que Edita LancamentoVenda
	 * @param LancamentoVendaTO $lvTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLancamentoVenda(LancamentoVendaTO $lvTO){
		return $this->bo->editarLancamentoVenda($lvTO);
	}
	
	/**
	 * Metodo que Edita VendaProduto
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function editarVendaProduto(VendaProdutoTO $vpTO){
		return $this->bo->editarVendaProduto($vpTO);
	}
	
	/**
	 * Metodo que Edita Venda
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function editarVenda(VendaTO $vTO){
		return $this->bo->editarVenda($vTO);
	}
	
	/**
	 * Metodo que Edita Lancamento
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function editarLancamento(LancamentoTO $lTO){
		return $this->bo->editarLancamento($lTO);
	}
	
	/**
	 * Metodo que Edita Tag
	 * @param TagTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function editarTag(TagTO $tagTO){
		return $this->bo->editarTag($tagTO);
	}
	
	/**
	 * Metodo que Exclui CampanhaProduto
	 * @param CampanhaProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO){
		return $this->bo->deletarCampanhaProduto($cpTO);
	}
	
	/**
	 * Metodo que Exclui Tag
	 * @param TagTO $tagTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarTag(TagTO $tagTO){
		return $this->bo->deletarTag($tagTO);
	}
	
	/**
	 * Metodo que Exclui relação de Tag com produto
	 * @param TagTO $tagTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarTagProduto(TagProdutoTO $tagProdutoTO){
		return $this->bo->deletarTagProduto($tagProdutoTO);
	}
	
	/**
	 * Metodo que Exclui Produto
	 * @param ProdutoTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarProduto(ProdutoTO $pTO){
		return $this->bo->deletarProduto($pTO);
	}

    /**
     * Metodo que Exclui(inativa) ProdutoPRR
     * @param ProdutoTO $pTO
     * @return Ead1_Mensageiro
     */
    public function excluirProdutoPrr(ProdutoTO $pTO){
        return $this->bo->excluirProdutoPrr($pTO);
    }
	
	/**
	 * Metodo que Exclui ProdutoProjetoPedagogico
	 * @param ProdutoProjetoPedagogicoTO $pppTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
		return $this->bo->deletarProdutoProjetoPedagogico($pppTO);
	}
	
	/**
	 * Metodo que Exclui ProdutoValor
	 * @param ProdutoValorTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarProdutoValor(ProdutoValorTO $pvTO){
		return $this->bo->deletarProdutoValor($pvTO);
	}
	
	/**
	 * Metodo que Exclui LancamentoVenda
	 * @param LancamentoVendaTO $lvTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarLancamentoVenda(LancamentoVendaTO $lvTO){
		return $this->bo->deletarLancamentoVenda($lvTO);
	}
	
	/**
	 * Metodo que Exclui VendaProduto
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVendaProduto(VendaProdutoTO $vpTO){
		return $this->bo->deletarVendaProduto($vpTO);
	}
	
	/**
	 * Metodo que exclui o vinculo do produto com a declaracao (textosistema)
	 * @param ProdutoTextoSistemaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function deletarProdutoTextoSistema(ProdutoTextoSistemaTO $to){
		return $this->bo->deletarProdutoTextoSistema($to);
	}
	
	/**
	 * Metodo que Exclui Venda
	 * @param VendaTO $vTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarVenda(VendaTO $vTO){
		return $this->bo->deletarVenda($vTO);
	}
	
	/**
	 * Metodo que Exclui Lancamento
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarLancamento(LancamentoTO $lTO){
		return $this->bo->deletarLancamento($lTO);
	}
	
	/**
	 * Metodo que retorna Campanha Produto
	 * @param CampanhaProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaProduto(CampanhaProdutoTO $cpTO){
		return $this->bo->retornarCampanhaProduto($cpTO);
	}
	
	/**
	 * Metodo que retorna Tag
	 * @param TagTO $tagTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTag(TagTO $tagTO){
		return $this->bo->retornarTag($tagTO);
	}
	
	/**
	 * Metodo que retorna Tag de Produto
	 * @param TagProdutoTO $tagProdutoTOTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTagProduto(TagProdutoTO $tagProdutoTO){
		return $this->bo->retornarTagProduto($tagProdutoTO);
	}
	
	/**
	 * Metodo que retorna Tags de Produto
	 * @param int $id_produto
	 * @return Ead1_Mensageiro
	 */
	public function retornarTagsDeProduto($id_produto){
		return $this->bo->retornarTagsDeProduto($id_produto);
	}
	
	/**
	 * Retorna os planos de pagamento do produto
	 * @param PlanoPagamentoTO $planoPagamentoTO
	 */
	public function retornarPlanoPagamento(PlanoPagamentoTO $planoPagamentoTO){
		return $this->bo->retornarPlanoPagamento($planoPagamentoTO);
	}
	
	/**
	 * Metodo que retorna  view de produto.
	 * @param VwProdutoTO $vwpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProduto(VwProdutoTO $vwpTO){
		return $this->bo->retornarVwProduto($vwpTO);
	}

    /**
     * Metodo que retorna  view de produto prr.
     * @param VwProdutoPrrTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarVwProdutoPrr(VwProdutoPrrTO $to){
        return $this->bo->retornarVwProdutoPrr($to);
    }
	
	/**
	 * Metodo que retorna o Tipo do Produto
	 * @param TipoProdutoTO $tpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoProduto(TipoProdutoTO $tpTO){
		return $this->bo->retornarTipoProduto($tpTO);
	}
	
	/**
	 * Metodo que retorna o Produto
	 * @param ProdutoTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProduto(ProdutoTO $pTO){
		return $this->bo->retornarProduto($pTO);
	}
	
	/**
	 * Metodo que retorna o ProdutoProjetoPedagogico
	 * @param ProdutoProjetoPedagogicoTO $pppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoProjetoPedagogico(ProdutoProjetoPedagogicoTO $pppTO){
		return $this->bo->retornarProdutoProjetoPedagogico($pppTO);
	}
	
	/**
	 * Metodo que retorna o ProdutoValor
	 * @param ProdutoValorTO $pvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoValor(ProdutoValorTO $pvTO){
		return $this->bo->retornarProdutoValor($pvTO);
	}
	
	/**
	 * Metodo que retorna o LancamentoVenda
	 * @param LancamentoVendaTO $lvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLancamentoVenda(LancamentoVendaTO $lvTO){
		return $this->bo->retornarLancamentoVenda($lvTO);
	}
	
	/**
	 * Metodo que retorna o VendaProduto
	 * @param VendaProdutoTO $vpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaProduto(VendaProdutoTO $vpTO){
		return $this->bo->retornarVendaProduto($vpTO);
	}
	
	/**
	 * Metodo que retorna o Venda
	 * @param VendaTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVenda(VendaTO $vTO){
		return $this->bo->retornarVenda($vTO);
	}
	
	/**
	 * Metodo que retorna o View da Venda usuário
	 * @param VwVendaUsuarioTO $pTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVendaUsuario(VwVendaUsuarioTO $vTO){
		return $this->bo->retornarVendaUsuario($vTO);
	}
	
	/**
	 * Metodo que retorna o Lançamento
	 * @param LancamentoTO $lTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarLancamento(LancamentoTO $lTO){
		return $this->bo->retornarLancamento($lTO);
	}
	
	/**
	 * Metodo que retorna o Tipo de Lançamento
	 * @param TipoLancamentoTO $tlTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoLancamento(TipoLancamentoTO $tlTO){
		return $this->bo->retornarTipoLancamento($tlTO);
	}
	
	/**
	 * Metodo que retorna os dados da view de lancamento
	 * @param VwLancamentoTO $vwLTO
	 * @return Zend_Db_Table_Rowset_Abstract|false
	 */
	public function retornarVwLancamento(VwLancamentoTO $vwLTO){
		return $this->bo->retornarVwLancamento($vwLTO);
	}
	
	/**
	 * Metodo que retorna o tipo de valor do produto
	 * @param TipoProdutoValorTO $tpvTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoProdutoValor(TipoProdutoValorTO $tpvTO){
		return $this->bo->retornarTipoProdutoValor($tpvTO);
	}
	
	/**
	 * Metodo que retorna o vinculo do produto com a declaracao (textosistema)
	 * @param ProdutoTextoSistemaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoTextoSistema(ProdutoTextoSistemaTO $to){
		return $this->bo->retornarProdutoTextoSistema($to);
	}
	
	/**
	 * Metodo que retorna o vinculo do produto com a Taxa
	 * @param ProdutoTaxaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoTaxa(ProdutoTaxaTO $to){
		return $this->bo->retornarProdutoTaxa($to);
	}
	
	/**
	 * Metodo que retorna o vinculo do produto com o livro
	 * @param ProdutoLivroTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoLivro(ProdutoLivroTO $to){
		return $this->bo->retornarProdutoLivro($to);
	}
	
	/**
	 * Metodo que retorna a Taxa
	 * @param TaxaTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarTaxa(TaxaTO $to){
		return $this->bo->retornarTaxa($to);
	}
	
	/**
	 * Metodo que retorna os projetos em uma view para recarregar os projetos da taxa
	 * @param ProdutoTaxaProjetoTO $produtoTaxaProjetoTO
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoTaxaProjetoVw(ProdutoTaxaProjetoTO $produtoTaxaProjetoTO){
		return $this->bo->retornarProdutoTaxaProjetoVw($produtoTaxaProjetoTO);
	}
	
	
	
	/**
	 * Metodo que retorna as taxas disponiveis para o produto da entidade
	 * @param ProdutoTaxaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarTaxasDisponiveisProdutoEntidade(ProdutoTaxaTO $to){
		return $this->bo->retornarTaxasDisponiveisProdutoEntidade($to);
	}

	
	/**
	 * Método que retorna o Produto Pelo id da avaliacao e da entidade
	 * @param ProdutoAvaliacaoTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoAvaliacaoEntidade(ProdutoAvaliacaoTO $to){
		return $this->bo->retornarProdutoAvaliacaoEntidade($to);
	}
	
	/**
	 * Método que retorna os dados da view dos Produtos do textosistema
	 * @param VwProdutoTextoSistemaTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProdutoTextoSistema(VwProdutoTextoSistemaTO $to){
		return $this->bo->retornarVwProdutoTextoSistema($to);
	}
	
	
	/**
	 * Método que retorna as imagens vinculadas a um produto
	 * @param ProdutoImagemTO $to
	 * @throws Zend_Validate
	 * @throws Zend_Validate_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarProdutoImagem(ProdutoImagemTO $to){
		return $this->bo->retornarProdutoImagem($to);
	}
	
	/**
	 * Upload de imagem para produto
	 * @param ProdutoImagemTO $produtoTo
	 * @param ArquivoTO $arquivoImagem
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function uploadImagemProduto(ProdutoImagemTO $to, ArquivoTO $arquivoImagem){
		return $this->bo->uploadImagemProduto($to, $arquivoImagem);
	}
	
	
	/**
	 * Deleta de forma lógica uma imagem
	 * @param ProdutoImagemTO $to
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function deletarProdutoImagem(ProdutoImagemTO $to){
		return $this->bo->deletarProdutoImagem($to);
	}
	
	
	/**
	 * Salva imagem padrao do Produto
	 * @param ProdutoImagemTO $to
	 * @return Ead1_Mensageiro
	 */
	public function salvarProdutoImagemPadrao(ProdutoImagemTO $to){
		return $this->bo->salvarProdutoImagemPadrao($to);
	}
	
	
	/**
	 * Retorna o array de categorias do produto
	 * @param ProdutoTO $produtoTo
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornaCategoriasProduto(ProdutoTO $to){
		return $this->bo->retornaCategoriasProduto($to);
	}
	
	/**
	 * Retorna o array de produtos da categoria
	 * @param int $id_categoria
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function retornarCategoriaProduto($id_categoria){
		return $this->bo->retornarCategoriaProduto($id_categoria);
	}
}
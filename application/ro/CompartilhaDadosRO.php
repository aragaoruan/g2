<?php
/**
 * Classe de comunicação de compartilhamento de dados
 * @author eduardoromao
 */
class CompartilhaDadosRO extends Ead1_RO{
	
	private $_bo;
	
	public function __construct(){
		$this->_bo = new CompartilhaDadosBO();
	}
	
	/**
	 * Método que salva um array de dados compartilhados
	 * @param array $arrCompartilhaDadosTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrCompartilhaDados($arrCompartilhaDadosTO){
		return $this->_bo->salvarArrCompartilhaDados($arrCompartilhaDadosTO);
	}
	
	/**
	 * Método que retorna os dados compartilhados
	 * @param CompartilhaDadosTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarCompartilhaDados(CompartilhaDadosTO $to){
		return $this->_bo->retornarCompartilhaDados($to);
	}
	
}
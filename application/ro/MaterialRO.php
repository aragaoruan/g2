<?php
/**
 * Classe de comunicação com Flex para interações com objeto Material
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class MaterialRO extends Ead1_RO{
	
	private $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro 	= new Ead1_Mensageiro();
		$this->bo 			= new MaterialBO();
	}
	
	/**
	 * Metodo que decide se Cadastra ou Edita o Tipo do Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTipoMaterial(TipoDeMaterialTO $tdmTO){
		return $this->bo->salvarTipoDeMaterial($tdmTO);
	}
	
	/**
	 * Metodo que cadastra o projeto pedagogico do material
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarMaterialProjeto(MaterialProjetoTO $mpTO){
		return $this->bo->cadastrarMaterialProjeto($mpTO);
	}
	
	/**
	 * Metodo que Decide se Cadastra ou Edita o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarItemDeMaterial(ItemDeMaterialTO $idmTO){
		if($idmTO->getId_itemdematerial()){
			return $this->editarItemDeMaterial($idmTO);
		}
		return $this->cadastrarItemDeMaterial($idmTO);
	}
	
	/**
	 * Metodo que decide se Cadastra ou Exclui o vinculo do Item de Material com a Area
	 * @param ItemDeMaterialAreaTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		return $this->bo->salvarItemDeMaterialArea($idmaTO);
	}
	
	/**
	 * Metodo que decide se Cadastra ou Exclui o vinculo do Item de Material com a Disciplina
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		return $this->bo->salvarItemDeMaterialDisciplina($idmdTO);
	}
		
	/**
	 * Metodo que Salva as Areas de Conhecimento do item de material
	 * @param array( ItemDeMaterialAreaTO $arrTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayItemDeMaterialArea($arrTO){
		return $this->bo->salvarArrayItemDeMaterialArea($arrTO);
	}
		
	/**
	 * Metodo que Salva as Disciplinas do item de material
	 * @param array( ItemDeMaterialDisciplinaTO $arrTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayItemDeMaterialDisciplina($arrTO){
		return $this->bo->salvarArrayItemDeMaterialDisciplina($arrTO);
	}
		
	/**
	 * Metodo que salvar ou Edita o Tipo de Material
	 * @param array(TipoDeMaterialTO $arrTO )
	 * @return Ead1_Mensageiro
	 */
	public function salvarArrayTipoDeMaterial($arrTO){
		return $this->bo->salvarArrayTipoDeMaterial($arrTO);
	}
	
	/**
	 * Metodo que cadastra ou edita a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarEntregaMaterial(EntregaMaterialTO $emTO){
		return $this->bo->salvarEntregaMaterial($emTO);
	}
	
	/**
	 * Metodo que Cadastra o Tipo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		return $this->bo->cadastrarTipoDeMaterial($tdmTO);
	}
	
	/**
	 * Metodo que cadastra a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaMaterial(EntregaMaterialTO $emTO){
		return $this->bo->cadastrarEntregaMaterial($emTO);
	}
	
	/**
	 * Metodo que cadastra e vincula uma observação a uma entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarEntregaMaterialObservacao(EntregaMaterialTO $emTO, ObservacaoTO $oTO){
		return $this->bo->cadastrarEntregaMaterialObservacao($emTO,$oTO);
	}
		
	/**
	 * Metodo que Cadastra o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterial(ItemDeMaterialTO $idmTO){
		return $this->bo->cadastrarItemDeMaterial($idmTO);
	}
		
	/**
	 * Metodo que Vincula a Area ao Item de Material
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		return $this->bo->cadastrarItemDeMaterialArea($idmaTO);
	}
		
	/**
	 * Metodo que Vincula a Disciplina ao Item de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		return $this->bo->cadastrarItemDeMaterialDisciplina($idmdTO);
	}
	
	/**
	 * Metodo que cadastra a observacao
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarObservacao(ObservacaoTO $oTO){
		return $this->bo->cadastrarObservacao($oTO);
	}
	
	/**
	 * Metodo que cadastra a observacao da entrega de material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO){
		return $this->bo->cadastrarObservacaoEntregaMaterial($oemTO);
	}
		
	/**
	 * Metodo que Edita o Tipo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function editarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		return $this->bo->editarTipoDeMaterial($tdmTO);
	}
		
	/**
	 * Metodo que Edita o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function editarItemDeMaterial(ItemDeMaterialTO $idmTO){
		return $this->bo->editarItemDeMaterial($idmTO);
	}
	
	/**
	 * Metodo que edita a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterial(EntregaMaterialTO $emTO){
		return $this->bo->editarEntregaMaterial($emTO);
	}
	
	/**
	 * Metodo que edita a entrega de material que foi devolvido
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function editarEntregaMaterialDevolucao(EntregaMaterialTO $emTO){
		return $this->bo->editarEntregaMaterialDevolucao($emTO);
	}
	
	/**
	 * Metodo que Exclui o TIpo de Material
	 * @param TipoDeMaterialTO $tdmTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarTipoDeMaterial(TipoDeMaterialTO $tdmTO){
		return $this->bo->deletarTipoDeMaterial($tdmTO);
	}
	
	/**
	 * Metodo que Exclui o Projeto do Material
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarMaterialProjeto(MaterialProjetoTO $mpTO){
		return $this->bo->deletarMaterialProjeto($mpTO);
	}
		
	/**
	 * Metodo que Exclue o Item de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterial(ItemDeMaterialTO $idmTO){
		return $this->bo->deletarItemDeMaterial($idmTO);
	}
	
	/**
	 * Metodo que verifica se existem vinculos com o item de material e exclui
	 * @param ItemDeMaterialTO $to
	 * @return Ead1_Mensageiro
	 */
	public function verificaExcluiItemDeMaterial(ItemDeMaterialTO $to){
		return $this->bo->verificaExcluiItemDeMaterial($to);
	}
		
	/**
	 * Metodo que Exclue o Vinculo da Area com o Item de Material
	 * @param ItemDeMaterialAreaTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		return $this->bo->deletarItemDeMaterialArea($idmaTO);
	}
		
	/**
	 * Metodo que Exclue o Vinculo da Disciplina com o Item de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		return $this->bo->deletarItemDeMaterialDisciplina($idmdTO);
	}
	
	/**
	 * Metodo que retorna todos os tipos de materiais 
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDeMaterial(){
		return $this->bo->retornarTipoDeMaterial();
	}
	
	/**
	 * Metodo que Retorna os projetos do item de material como ProjetoPedagogicoTO
	 * @param MaterialProjetoTO $mpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProjetosMaterial(MaterialProjetoTO $mpTO){
		return $this->bo->retornarProjetosMaterial($mpTO);
	}
	
	/**
	 * Metodo que Retorna a view do material com o projeto pedagogico
	 * @param VwMaterialProjetoPedagogicoTO $vwMppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwMaterialProjetoPedagogico(VwMaterialProjetoPedagogicoTO $vwMppTO){
		return $this->bo->retornarVwMaterialProjetoPedagogico($vwMppTO);
	}
	
	/**
	 * Metodo que Retorna os Items de Material
	 * @param ItemDeMaterialTO $idmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterial(ItemDeMaterialTO $idmTO){
		return $this->bo->retornarItemDeMaterial($idmTO);
	}
		
	/**
	 * Metodo que Retorna as Areas dos Items de Material
	 * @param ItemDeMaterialTO $idmaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterialArea(ItemDeMaterialAreaTO $idmaTO){
		return $this->bo->retornarItemDeMaterialArea($idmaTO);
	}
		
	/**
	 * Metodo que Retorna as disciplinas do item de material como disciplinaTO
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarDisciplinasItemDeMaterial(ItemDeMaterialDisciplinaTO $idmdTO){
		return $this->bo->retornarDisciplinasItemDeMaterial($idmdTO);
	}
		
	/**
	 * Metodo que Retorna as Disciplinas dos Items de Material
	 * @param ItemDeMaterialDisciplinaTO $idmdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarItemDeMaterialDisciplina(ItemDeMaterialDisciplinaTO $idmdTO){
		return $this->bo->retornarItemDeMaterialDisciplina($idmdTO);
	}
	
	/**
	 * Metodo que retorna a entrega de material
	 * @param EntregaMaterialTO $emTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarEntregaMaterial(EntregaMaterialTO $emTO){
		return $this->bo->retornarEntregaMaterial($emTO);
	}
	
	/**
	 * Retorna as áreas de conhecimento para vincular ao item de material
	 */
	public function retornarAreaConhecimento(){
		return $this->bo->retornarAreaConhecimento();
	}
	
	/**
	 * Metodo que retorna a Observacao
	 * @param ObservacaoTO $oTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarObservacao(ObservacaoTO $oTO){
		return $this->bo->retornarObservacao($oTO);
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param ObservacaoEntregaMaterialTO $oemTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarObservacaoEntregaMaterial(ObservacaoEntregaMaterialTO $oemTO){
		return $this->bo->retornarObservacaoEntregaMaterial($oemTO);
	}
	
	/**
	 * Metodo que retorna os dados da view vw_entregamaterial para pesquisa
	 * @param VwEntregaMaterialTO $vwEmTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwEntregaMaterial(VwEntregaMaterialTO $vwEmTO){
		return $this->bo->retornarVwEntregaMaterial($vwEmTO);
	}
	
	/**
	 * Metodo que retorna a observacao da entrega de material
	 * @param VwObservacaoEntregaMaterialTO $vwOemTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwObservacaoEntregaMaterial(VwObservacaoEntregaMaterialTO $vwOemTO){
		return $this->bo->retornarVwObservacaoEntregaMaterial($vwOemTO);
	}
}
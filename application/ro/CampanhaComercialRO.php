<?php
/**
 * Classe de comunicação com Flex para interações com objeto CampanhaComercial
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class CampanhaComercialRO extends FinanceiroRO{
	
	public $mensageiro;
	private $bo;
	
	public function __construct(){
		$this->mensageiro = new Ead1_Mensageiro();
		$this->bo = new CampanhaComercialBO();
	}
	
	/**
	 * Metodo Que decide se cadastra ou edita a campanha comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaComercial(CampanhaComercialTO $ccTO){
		if($ccTO->getId_campanhacomercial()){
			return $this->editarCampanhaComercial($ccTO);
		}
			return $this->cadastrarCampanhaComercial($ccTO);
	}
			
	/**
	 * Metodo que decide se Cadastra ou edita o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		if($cdTO->getId_campanhadesconto()){
			return $this->editarCampanhaDesconto($cdTO);
		}
			return $this->cadastrarCampanhaDesconto($cdTO);
	}
			
	/**
	 * Metodo que Decide se Cadastra ou Exclui o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaConvenio(CampanhaConvenioTO $ccTO){
			return $this->bo->salvarCampanhaConvenio($ccTO);
	}
			
	/**
	 * Metodo que Cadastra ou Exclui o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		return $this->bo->salvarCampanhaPerfil($cpTO);
	}
				
	/**
	 * Metodo que Cadastra ou Exclui o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function salvarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
			return $this->bo->salvarCampanhaProjetoArea($cpaTO);
	}
	
	/**
	 * Metodo que Cadastra a Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaComercial(CampanhaComercialTO $ccTO){
		return $this->bo->cadastrarCampanhaComercial($ccTO);
	}
		
	/**
	 * Metodo que Cadastra o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		return $this->bo->cadastrarCampanhaDesconto($cdTO);
	}
		
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		return $this->bo->cadastrarCampanhaConvenio($ccTO);
	}
		
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		return $this->bo->cadastrarCampanhaPerfil($cpTO);
	}
		
	/**
	 * Metodo que Cadastra o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function cadastrarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		return $this->bo->cadastrarCampanhaProjetoArea($cpaTO);
	}
		
	/**
	 * Metodo que Edita Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCampanhaComercial(CampanhaComercialTO $ccTO){
		return $this->bo->editarCampanhaComercial($ccTO);
	}
			
	/**
	 * Metodo que Edita o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function editarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		return $this->bo->editarCampanhaDesconto($cdTO);
	}
		
	/**
	 * Metodo que Exclui Campanha Comercial
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaComercial(CampanhaComercialTO $ccTO){
		return $this->bo->deletarCampanhaComercial($ccTO);
	}
				
	/**
	 * Metodo que Exclui o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		return $this->bo->deletarCampanhaDesconto($cdTO);
	}
				
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		return $this->bo->deletarCampanhaConvenio($ccTO);
	}
				
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		return $this->bo->deletarCampanhaPerfil($cpTO);
	}
				
	/**
	 * Metodo que Exclui o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		return $this->bo->deletarCampanhaProjetoArea($cpaTO);
	}
	
	/**
	 * Metodo que Exclui o vinculo da campanha com o produto
	 * @param CampanhaProdutoTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function deletarCampanhaProduto(CampanhaProdutoTO $cpTO){
		return $this->bo->deletarCampanhaProduto($cpTO);
	}

	/**
	 * Metodo que retorna a Campanha Comercial da Forma de Pagamento
	 * @param CampanhaComercialTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaComercial(CampanhaComercialTO $ccTO){
		return $this->bo->retornarCampanhaComercial($ccTO);
	}
	
	/**
	 * Metodo que retorna a view Campanha Comercial do Produto
	 * @param VwCampanhaProdutoTO $vwcpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaProduto(VwCampanhaProdutoTO $vwcpTO){
		return $this->bo->retornarVwCampanhaProduto($vwcpTO);
	}
	
	/**
	 * Metodo que retorna a view Campanha Comercial da Forma de Pagamento.
	 * @param VwCampanhaFormaPagamentoTO $vwcfppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaFormaPagamento(VwCampanhaFormaPagamentoTO $vwcfpTO){
		return $this->bo->retornarVwCampanhaFormaPagamento($vwcfpTO);
	}
	
	/**
	 * Metodo que retorna a view Campanha Comercial da Forma de Pagamento e Produto
	 * @param VwCampanhaFormaPagamentoProduto $vwcfppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwCampanhaFormaPagamentoProduto(VwCampanhaFormaPagamentoProdutoTO $vwcfppTO){
		return $this->bo->retornarVwCampanhaFormaPagamentoProduto($vwcfppTO);
	}
		
	/**
	 * Metodo que retorna o Desconto da Campanha
	 * @param CampanhaDescontoTO $cdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaDesconto(CampanhaDescontoTO $cdTO){
		return $this->bo->retornarCampanhaDesconto($cdTO);
	}
		
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Convenio
	 * @param CampanhaConvenioTO $ccTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaConvenio(CampanhaConvenioTO $ccTO){
		return $this->bo->retornarCampanhaConvenio($ccTO);
	}
		
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Perfil
	 * @param CampanhaPerfilTO $cpTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaPerfil(CampanhaPerfilTO $cpTO){
		return $this->bo->retornarCampanhaPerfil($cpTO);
	}
		
	/**
	 * Metodo que retorna o Tipo de Desconto
	 * @param TipoDescontoTO $tdTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoDesconto(TipoDescontoTO $tdTO){
		return $this->bo->retornarTipoDesconto($tdTO);
	}
		
	/**
	 * Metodo que retorna o Vinculo da Campanha com o Projeto Pedagógico e a Area
	 * @param CampanhaProjetoAreaTO $cpaTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarCampanhaProjetoArea(CampanhaProjetoAreaTO $cpaTO){
		return $this->bo->retornarCampanhaProjetoArea($cpaTO);
	}
	
	/**
	 * Metodo que retorna o tipo da campanha
	 * @param TipoCampanhaTO $tcTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarTipoCampanha(TipoCampanhaTO $tcTO){
		return $this->bo->retornarTipoCampanha($tcTO);
	}
}
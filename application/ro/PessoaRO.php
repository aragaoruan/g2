<?php

/**
 * Classe de comunicação com Flex para interações com objeto Pessoa
 * @author Eduardo Romão - ejushiro@gmail.com
 * @package application
 * @subpackage ro
 */
class PessoaRO extends Ead1_RO
{

    /** @var \G2\Negocio\Pessoa */
    private $_negocio;

    public function __construct()
    {
        $this->_negocio = new \G2\Negocio\Pessoa();
    }

    /**
     * Metodo que retorna dados de usuario e/ou pessoa ou retorna false.
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function verificaUsuario(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->verificaUsuario($uTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que atualiza e define a conta bancaria padrao da pessoa
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function setaContaBancariaPadrao(ContaPessoaTO $cpTO)
    {
        $bo = new PessoaBO();
        return $bo->setaContaBancariaPadrao($cpTO);
    }

    /**
     * Metodo que Retorna Usuario
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaUsuario(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        $usuario = ($bo->retornaUsuario($uTO));
        if (!$usuario) {
            return new Ead1_Mensageiro(false);
        }
        $uTO = $usuario;
        return new Ead1_Mensageiro($uTO);
    }

    /**
     * Metodo que retorna Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaPessoa(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        $pessoa = ($bo->retornaPessoa($uTO));
        if (!$pessoa) {
            return new Ead1_Mensageiro(false);
        }
        $pTO = new PessoaTO();
        $pTO->setAtributos($pessoa);
        return new Ead1_Mensageiro($pTO);
    }

    /**
     * Retorna as atalhos pra pessoa em funcionalidades.
     * @param AtalhoPessoaTO $aTO
     */
    public function retornarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $bo = new PessoaBO();
        return $bo->retornarAtalhoPessoa($aTO);
    }

    /**
     * Metodo que retorna Endereços da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaEnderecos(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaEnderecos($uTO));
    }

    /**
     * Metodo que retorna os endereços da pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornarVwPessoaEnderecoTO(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return $bo->retornarVwPessoaEnderecoTO($uTO);
    }


    /**
     * Metodo que retornar o resultado da view vw_funcionario do banco de dados
     * @param VwFuncionarioTO $vw
     * @return Ead1_Mensageiro
     */
    public function retornarVwFuncionario(VwFuncionarioTO $vw)
    {
        $bo = new PessoaBO();
// 		Zend_Debug::dump(($bo->retornarVwFuncionario($vw)),__CLASS__.'('.__LINE__.')');exit;
        return $bo->retornarVwFuncionario($vw);
    }

    /**
     * Metodo que retorna Emails da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaEmails(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaEmails($uTO));
    }


    /**
     * Metodo que retorna Informação Profissional da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaInformacaoProfissional(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaInformacaoProfissional($uTO));
    }

    /**
     * Metodo que retorna Redes Sociais da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaRedesSociais(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaRedesSociais($uTO));
    }

    /**
     * Metodo que retorna Telefones da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaTelefones(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaTelefones($uTO));
    }

    /**
     * Metodo que retorna Informações Academicas da Pessoa
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaInformacaoAcademica(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaInformacaoAcademica($uTO));
    }

    /**
     * Metodo que retorna Contas Bancarias
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaContaBancaria(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return $bo->retornaContaBancaria($uTO);
    }

    /**
     * Metodo que retorna informações de dados biomédicos
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaDadosBiomedicos(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaDadosBiomedicos($uTO));
    }

    /**
     * Metodo que retorna Documento de Identidade
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaIdentidade(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaIdentidade($uTO));
    }

    /**
     * Metodo que retorna o documento de titulo eleitor
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaTituloEleitor(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaTituloEleitor($uTO));
    }

    /**
     * Metodo que retorna o documento de certidao de nascimento
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaCertidaoNascimento(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaCertidaoNascimento($uTO));
    }

    /**
     * Metodo que retorna o Reservista
     * @param UsuarioTO $uTO
     * @return Ead1_Mensageiro
     */
    public function retornaReservista(UsuarioTO $uTO)
    {
        $bo = new PessoaBO();
        return new Ead1_Mensageiro($bo->retornaReservista($uTO));
    }

    /**
     * Retorna os dados básicos do usuário vinculado há uma entidade
     * Caso os dados do usuário não sejam da entidade atual da sessão
     * será realizada uma importação dos dados da entidade informada
     * @param UsuarioEntidadeTO $usuarioEntidadeTO
     * @return Ead1_Mensageiro
     */
    public function retornarDadosUsuarioEntidade($usuarioEntidadeTO)
    {
        $pessoaBO = new PessoaBO();
        return $pessoaBO->retornarDadosUsuarioEntidade($usuarioEntidadeTO);
    }

    /**
     * Método que define se deve cadastrar ou editar o Usuário
     * @param UsuarioTO $uTO
     * @param Boolean $validaCPF
     * @return Ead1_Mensageiro
     */
    public function salvarUsuario(UsuarioTO $uTO, $validaCPF = true, $enviarSenha = false)
    {
        if ($uTO->getId_usuario()) {
            return $this->editarUsuario($uTO, $validaCPF, $enviarSenha);
        } else {
            return $this->cadastrarUsuario($uTO, $validaCPF);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar a Pessoa
     * @param UsuarioTO $uTO
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function salvarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        $pTO->setId_usuario($uTO->getId_usuario());
        $pTO->setId_entidade(($pTO->getId_entidade() ? $pTO->getId_entidade() : $pTO->getSessao()->id_entidade));

        //Verifica usando Doctrine porque a instancia do zend[TO] sempre retorna as duas chaves setadas acima
        $aux = $this->_negocio->findOneBy('\G2\Entity\Pessoa', array(
            'id_usuario' => (int)$uTO->getId_usuario(), 'id_entidade' => (int)$pTO->getId_entidade()));

        if ($aux instanceof \G2\Entity\Pessoa && $aux->getId_usuario()) {
            return $this->editarPessoa($uTO, $pTO);
        } else {
            return $this->cadastrarPessoa($uTO, $pTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar o endereço
     * @param UsuarioTO $uTO
     * @param EnderecoTO $eTO
     * @return Ead1_Mensageiro
     */
    public function salvarEndereco(UsuarioTO $uTO, EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        $eTO->setId_endereco($peTO->getId_endereco());
        $peTO->setId_entidade(($peTO->getId_entidade() ? $peTO->getId_entidade() : $peTO->getSessao()->id_entidade));
        $peTO->setId_usuario($uTO->getId_usuario());

        if ($peTO->getId_endereco()) {
            return $this->editarEndereco($eTO, $peTO);
        } else {
            return $this->cadastrarEndereco($uTO, $eTO, $peTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar o email
     * @param UsuarioTO $uTO
     * @param ContatosEmailTO $ceTO
     * @param ContatosEmailPessoaPerfilTO $ceppTO
     * @return Ead1_Mensageiro
     */
    public function salvarEmail(UsuarioTO $uTO, ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        $ceppTO->setId_usuario($uTO->getId_usuario());
        $ceppTO->setId_entidade($ceppTO->getId_entidade()
            ? $ceppTO->getId_entidade() : $ceppTO->getSessao()->id_entidade);

        if ($ceppTO->getId_email()) {
            return $this->editarEmail($ceTO, $ceppTO);
        } else {
            return $this->cadastrarEmail($uTO, $ceTO, $ceppTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar a informação profissional
     * @param UsuarioTO $uTO
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function salvarInformacaoProfissional(UsuarioTO $uTO, InformacaoProfissionalPessoaTO $ippTO)
    {
        $ippTO->setId_usuario($uTO->getId_usuario());
        $ippTO->setId_entidade($ippTO->getSessao()->id_entidade);
        $orm = new InformacaoProfissionalPessoaORM();
        if ($ippTO->getId_informacaoprofissionalpessoa()) {
            return $this->editarInformacaoProfissional($ippTO);
        } else {
            return $this->cadastrarInformacaoProfissional($uTO, $ippTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar a rede social
     * @param UsuarioTO $uTO
     * @param ContatosRedeSocialTO $crsTO
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function salvarRedeSocial(UsuarioTO $uTO, ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $crspTO->setId_usuario($uTO->getId_usuario());
        $crspTO->setId_entidade($crspTO->getSessao()->id_entidade);
        if ($crspTO->getId_redesocial()) {
            return $this->editarRedeSocial($crsTO, $crspTO);
        } else {
            return $this->cadastrarRedeSocial($uTO, $crsTO, $crspTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar o telefone
     * @param UsuarioTO $uTO
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function salvarTelefone(UsuarioTO $uTO, ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        $ctpTO->setId_usuario($ctpTO->getId_usuario() ? $ctpTO->getId_usuario() : $uTO->getId_usuario());
        $ctpTO->setId_entidade($ctpTO->getId_entidade() ? $ctpTO->getId_entidade() : $ctpTO->getSessao()->id_entidade);
        if ($ctpTO->getId_telefone()) {
            return $this->editarTelefone($ctTO, $ctpTO);
        } else {
            return $this->cadastrarTelefone($uTO, $ctTO, $ctpTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar o informação Academica
     * @param UsuarioTO $uTO
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function salvarInformacaoAcademica(UsuarioTO $uTO, InformacaoAcademicaPessoaTO $iapTO)
    {
        $iapTO->setId_usuario($uTO->getId_usuario());

        if (!$iapTO->getId_entidade()) {
            $iapTO->setId_entidade($iapTO->getSessao()->id_entidade);
        }
        if ($iapTO->getId_informacaoacademicapessoa()) {
            return $this->editarInformacaoAcademica($iapTO);
        } else {
            return $this->cadastrarInformacaoAcademica($uTO, $iapTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar uma conta bancária
     * @param UsuarioTO $uTO
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function salvarContaPessoa(UsuarioTO $uTO, ContaPessoaTO $cpTO)
    {
        $cpTO->setId_usuario($uTO->getId_usuario());
        $cpTO->setId_entidade($cpTO->getSessao()->id_entidade);
        if ($cpTO->getId_contapessoa()) {
            return $this->editarContaPessoa($cpTO);
        } else {
            return $this->cadastrarContaPessoa($uTO, $cpTO);
        }
    }

    /**
     * Define se cadastra ou edita o atalho de pessoa pra funcionalidade
     * @param AtalhoPessoaTO $aTO
     */
    public function salvarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        if ($aTO->getId_atalhopessoa()) {
            return $this->editarAtalhoPessoa($aTO);
        } else {
            return $this->cadastrarAtalhoPessoa($aTO);
        }
    }

    /**
     * Salva todos os atalhos de pessoa pra funcionalidade
     * @param array $arrayAtalhoPessoaTO
     */
    public function salvarArrayAtalhoPessoa($arrayAtalhoPessoaTO)
    {
        $bo = new PessoaBO();
        return $bo->salvarArrayAtalhoPessoa($arrayAtalhoPessoaTO);
    }

    /**
     * Método que define se deve cadastrar ou editar dados biomédicos
     * @param UsuarioTO $uTO
     * @param DadosBiomedicosTO $dbTO
     * @return Ead1_Mensageiro
     */
    public function salvarDadosBiomedicos(UsuarioTO $uTO, DadosBiomedicosTO $dbTO)
    {
        if ($dbTO->getId_dadosbiomedicos()) {
            return $this->editarDadosBiomedicos($dbTO);
        } else {
            return $this->cadastrarDadosBiomedicos($uTO, $dbTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar documento de identidade
     * @param UsuarioTO $uTO
     * @param DocumentoIdentidadeTO $diTO
     * @return Ead1_Mensageiro
     */
    public function salvarDocumentoIdentidade(UsuarioTO $uTO, DocumentoIdentidadeTO $diTO)
    {
        $diTO->setId_usuario($uTO->getId_usuario());
        $diTO->setId_entidade($diTO->getSessao()->id_entidade);
        $diTO->fetch(false, true, true);
        if ($diTO->getSt_rg()) {
            return $this->editarDocumentoIdentidade($diTO);
        } else {
            return $this->cadastrarDocumentoIdentidade($uTO, $diTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar titulo de eleitor
     * @param UsuarioTO $uTO
     * @param DocumentoTituloEleitorTO $dtiTO
     * @return Ead1_Mensageiro
     */
    public function salvarDocumentoTituloEleitor(UsuarioTO $uTO, DocumentoTituloDeEleitorTO $dtiTO)
    {
        $dtiTO->setId_usuario($uTO->getId_usuario());
        $dtiTO->setId_entidade($dtiTO->getSessao()->id_entidade);
        $orm = new DocumentoTituloDeEleitorORM();
        if ($orm->consulta($dtiTO, true)) {
            return $this->editarDocumentoTituloEleitor($dtiTO);
        } else {
            return $this->cadastrarDocumentoTituloEleitor($uTO, $dtiTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar certidao de nascimento
     * @param UsuarioTO $uTO
     * @param DocumentoCertidaoDeNascimentoTO $dcnTO
     * @return Ead1_Mensageiro
     */
    public function salvarDocumentoCertidaoNascimento(UsuarioTO $uTO, DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        $dcnTO->setId_usuario($uTO->getId_usuario());
        $dcnTO->setId_entidade($dcnTO->getSessao()->id_entidade);
        $orm = new DocumentoCertidaoDeNascimentoORM();
        if ($orm->consulta($dcnTO, true)) {
            return $this->editarDocumentoCertidaoNascimento($dcnTO);
        } else {
            return $this->cadastrarDocumentoCertidaoNascimento($uTO, $dcnTO);
        }
    }

    /**
     * Método que define se deve cadastrar ou editar documento de reservista
     * @param UsuarioTO $uTO
     * @param DocumentoDeReservistaTO $drTO
     * @return Ead1_Mensageiro
     */
    public function salvarDocumentoReservista(UsuarioTO $uTO, DocumentoDeReservistaTO $drTO)
    {
        $orm = new DocumentoDeReservistaORM();
        $drTO->setId_usuario($uTO->getId_usuario());
        $drTO->setId_entidade($drTO->getSessao()->id_entidade);
        if ($orm->consulta($drTO, true)) {
            return $this->editarDocumentoReservista($drTO);
        } else {
            return $this->cadastrarDocumentoReservista($uTO, $drTO);
        }
    }

    /**
     * Metodo que Cadastra Usuario
     * @param UsuarioTO $uTO
     * @param $validaCPF
     * @return Ead1_Mensageiro
     */
    public function cadastrarUsuario(UsuarioTO $uTO, $validaCPF)
    {
        $bo = new PessoaBO();
        $idEntidade = $uTO->getSessao()->id_entidade;
        try {
            $bo->cadastrarUsuario($uTO, $idEntidade, $validaCPF);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Pessoa
     * @param UsuarioTO $uTO
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        $bo = new PessoaBO();
        try {
            return $bo->cadastrarPessoa($uTO, $pTO);
        } catch (Zend_Exception $e) {
        }
    }

    /**
     * Metodo que Cadastra Endereço
     * @param $uTO
     * @param $eTO
     * @param $peTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEndereco(UsuarioTO $uTO, EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarEndereco($uTO, $eTO, $peTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Email
     * @param UsuarioTO $uTO
     * @param ContatosEmailTO $ceTO
     * @param ContatosEmailPessoaPerfilTO $ceppTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarEmail(UsuarioTO $uTO, ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarEmail($uTO, $ceTO, $ceppTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Informação Profissional
     * @param UsuarioTO $uTO
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarInformacaoProfissional(UsuarioTO $uTO, InformacaoProfissionalPessoaTO $ippTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarInformacaoProfissional($uTO, $ippTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Rede Social
     * @param UsuarioTO $uTO
     * @param ContatosRedeSocialTO $crsTO
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarRedeSocial(UsuarioTO $uTO, ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarRedeSocial($uTO, $crsTO, $crspTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Telefone
     * @param UsuarioTO $uTO
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarTelefone(UsuarioTO $uTO, ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarTelefone($uTO, $ctTO, $ctpTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Informações Academicas da Pessoa
     * @param UsuarioTO $uTO
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function cadastrarInformacaoAcademica(UsuarioTO $uTO, InformacaoAcademicaPessoaTO $iapTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarInformacaoAcademica($uTO, $iapTO);
        } catch (Zend_Exception $e) {
            throw $e;
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Conta Bancaria
     * @param UsuarioTO $uTO
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarContaPessoa(UsuarioTO $uTO, ContaPessoaTO $cpTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarContaPessoa($uTO, $cpTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Cadastra atalho de pessoa pra funcionalidade
     * @param AtalhoPessoaTO $aTO
     */
    public function cadastrarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $bo = new PessoaBO();
        return $bo->cadastrarAtalhoPessoa($aTO);
    }

    /**
     * Metodo que Cadastra Dados Biomedicos
     * @param UsuarioTO $uTO
     * @param DadosBiomedicosTO $dbTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDadosBiomedicos(UsuarioTO $uTO, DadosBiomedicosTO $dbTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarDadosBiomedicos($uTO, $dbTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento de Identidade
     * @param UsuarioTO $uTO
     * @param DocumentoIdentidadeTO $diTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoIdentidade(UsuarioTO $uTO, DocumentoIdentidadeTO $diTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarDocumentoIdentidade($diTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Titulo de Eleitor
     * @param UsuarioTO $uTO
     * @param DocumentoTituloDeEleitorTO $dtiTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoTituloEleitor(UsuarioTO $uTO, DocumentoTituloDeEleitorTO $dtiTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarDocumentoTituloEleitor($uTO, $dtiTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Certidão de Nascimento
     * @param UsuarioTO $uTO
     * @param DocumentoCertidaoDeNascimentoTO $dcnTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoCertidaoNascimento(UsuarioTO $uTO, DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarDocumentoCertidaoNascimento($uTO, $dcnTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Cadastra Documento Reservista
     * @param UsuarioTO $uTO
     * @param DocumentoDeReservistaTO $drTO
     * @return Ead1_Mensageiro
     */
    public function cadastrarDocumentoReservista(UsuarioTO $uTO, DocumentoDeReservistaTO $drTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->cadastrarDocumentoReservista($uTO, $drTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Usuario
     * @param UsuarioTO $uTO
     * @param $validaCPF
     * @param $enviarSenha
     * @return Ead1_Mensageiro
     */
    public function editarUsuario(UsuarioTO $uTO, $validaCPF = true, $enviarSenha = false)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarUsuario($uTO, true, $enviarSenha);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Pessoa
     * @param UsuarioTO $uTO
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function editarPessoa(UsuarioTO $uTO, PessoaTO $pTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarPessoa($uTO, $pTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Endereço
     * @param EnderecoTO $eTO
     * @param PessoaEnderecoTO $peTO
     * @return Ead1_Mensageiro
     */
    public function editarEndereco(EnderecoTO $eTO, PessoaEnderecoTO $peTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarEndereco($eTO, $peTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Email
     * @param ContatosEmailTO $ceTO
     * @param ContatosEmailPessoaPerfilTO $ceppTO
     * @return Ead1_Mensageiro
     */
    public function editarEmail(ContatosEmailTO $ceTO, ContatosEmailPessoaPerfilTO $ceppTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarEmail($ceTO, $ceppTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Informação Profissional
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function editarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarInformacaoProfissional($ippTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Rede Social
     * @param ContatosRedeSocialTO $crsTO
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function editarRedeSocial(ContatosRedeSocialTO $crsTO, ContatosRedeSocialPessoaTO $crspTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarRedeSocial($crsTO, $crspTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Telefone
     * @param ContatosTelefoneTO $ctTO
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function editarTelefone(ContatosTelefoneTO $ctTO, ContatosTelefonePessoaTO $ctpTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarTelefone($ctTO, $ctpTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Informações Academicas da Pessoa
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function editarInformacaoAcademica(InformacaoAcademicaPessoaTO $iapTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarInformacaoAcademica($iapTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Conta Bancaria
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function editarContaPessoa(ContaPessoaTO $cpTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarContaPessoa($cpTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Edita atalho de pessoa.
     * @param AtalhoPessoaTO $aTO
     */
    public function editarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $bo = new PessoaBO();
        return $bo->editarAtalhoPessoa($aTO);
    }

    /**
     * Metodo que Edita Dados Biomedicos
     * @param DadosBiomedicosTO $dbTO
     * @return Ead1_Mensageiro
     */
    public function editarDadosBiomedicos(DadosBiomedicosTO $dbTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarDadosBiomedicos($dbTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Documento de Identidade
     * @param DocumentoIdentidadeTO $diTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoIdentidade(DocumentoIdentidadeTO $diTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarDocumentoIdentidade($diTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Documento Titulo de Eleitor
     * @param DocumentoTituloDeEleitorTO $dtiTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoTituloEleitor(DocumentoTituloDeEleitorTO $dtiTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarDocumentoTituloEleitor($dtiTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Documento Certidão de Nascimento
     * @param DocumentoCertidaoDeNascimentoTO $dcnTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoCertidaoNascimento(DocumentoCertidaoDeNascimentoTO $dcnTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarDocumentoCertidaoNascimento($dcnTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Metodo que Edita Documento Reservista
     * @param DocumentoDeReservistaTO $drTO
     * @return Ead1_Mensageiro
     */
    public function editarDocumentoReservista(DocumentoDeReservistaTO $drTO)
    {
        $bo = new PessoaBO();
        try {
            $bo->editarDocumentoReservista($drTO);
        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Método para deletar telefone
     * @param ContatosTelefonePessoaTO $ctpTO
     * @return Ead1_Mensageiro
     */
    public function deletarTelefone(ContatosTelefonePessoaTO $ctpTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarTelefone($ctpTO);
    }

    /**
     * Método que deleta a rede social da pessoa
     * @param ContatosRedeSocialPessoaTO $crspTO
     * @return Ead1_Mensageiro
     */
    public function deletarRedeSocial(ContatosRedeSocialPessoaTO $crspTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarRedeSocial($crspTO);
    }

    /**
     * Metodo que exclui Informações Academicas da Pessoa
     * @param InformacaoAcademicaPessoaTO $iapTO
     * @return Ead1_Mensageiro
     */
    public function deletarInformacaoAcademica(InformacaoAcademicaPessoaTO $iapTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarInformacaoAcademica($iapTO);
    }

    /**
     * Metodo que exclui Informações Profissionais da Pessoa
     * @param InformacaoProfissionalPessoaTO $ippTO
     * @return Ead1_Mensageiro
     */
    public function deletarInformacaoProfissional(InformacaoProfissionalPessoaTO $ippTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarInformacaoProfissional($ippTO);
    }

    /**
     * Metodo que exclui Conta da Pessoa
     * @param ContaPessoaTO $cpTO
     * @return Ead1_Mensageiro
     */
    public function deletarContaPessoa(ContaPessoaTO $cpTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarContaPessoa($cpTO);
    }

    /**
     * Deleta atalho de pessoa pra funcionalidade.
     * @param AtalhoPessoaTO $aTO
     */
    public function deletarAtalhoPessoa(AtalhoPessoaTO $aTO)
    {
        $bo = new PessoaBO();
        return $bo->deletarAtalhoPessoa($aTO);
    }

    /**
     * Método que retorna os perfils das Pessoas
     * @param PessoaTO $pTO
     * @return Ead1_Mensageiro
     */
    public function retornaPerfilPessoa(PessoaTO $pTO)
    {
        return new Ead1_Mensageiro(false);
    }

    /**
     * retorna endereço CEP
     * @param EnderecoTO $eTO
     */
    public function retornaEnderecoCEP(EnderecoTO $eTO)
    {
        return parent::retornarEnderecoCEP($eTO);
    }

    /**
     * Procedimento de salvar dados de usuario e pessoa para o cadastro de pessoa resumido
     * @param UsuarioTO $usuarioTO
     * @param PessoaTO $pessoaTO
     * @param DocumentoIdentidadeTO $documentoIdentidadeTO
     * @param ContatosTelefoneTO $contatosTelefoneTO
     * @param ContatosTelefonePessoaTO $contatosTelefonePessoaTO
     * @param ContatosEmailTO $contatosEmailTO
     * @param ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO
     * @param PessoaEnderecoTO $pessoaEnderecoTO
     * @param EnderecoTO $enderecoTO
     * @return Ead1_Mensageiro
     */
    public function procedimentoSalvarPessoaCadastroResumido(UsuarioTO $usuarioTO, PessoaTO $pessoaTO,
                                                             DocumentoIdentidadeTO $documentoIdentidadeTO,
                                                             ContatosTelefoneTO $contatosTelefoneTO,
                                                             ContatosTelefonePessoaTO $contatosTelefonePessoaTO,
                                                             ContatosEmailTO $contatosEmailTO,
                                                             ContatosEmailPessoaPerfilTO $contatosEmailPessoaPerfilTO,
                                                             PessoaEnderecoTO $pessoaEnderecoTO = null,
                                                             EnderecoTO $enderecoTO = null)
    {
        $bo = new PessoaBO();
        try {
            $bo->procedimentoSalvarPessoaCadastroResumido($usuarioTO, $pessoaTO, $documentoIdentidadeTO,
                $contatosTelefoneTO, $contatosTelefonePessoaTO,
                $contatosEmailTO, $contatosEmailPessoaPerfilTO, $pessoaEnderecoTO, $enderecoTO);

        } catch (Zend_Exception $e) {
        }
        return $bo->mensageiro;
    }

    /**
     * Retorna as pessoas para o autocomplete do flex.
     * @param PesquisarPessoaTO $to
     * @return Ead1_Mensageiro
     */
    public function pesquisarPessoaAutoComplete(PesquisarPessoaTO $to)
    {
        $bo = new PessoaBO();
        return $bo->pesquisarPessoaAutoComplete($to);
    }

}

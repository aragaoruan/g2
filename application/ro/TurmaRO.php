<?php
/**
 * Classe de comunicação remota com o Flex para interações de Turma
 * @author eduardoromao
 */
class TurmaRO extends Ead1_RO
{

    private $_bo;

    public function __construct()
    {
        $this->_bo = new TurmaBO();
    }

    /**
     * Metodo que salva a Turma
     * @param TurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function salvarTurma(TurmaTO $to)
    {
        return $this->_bo->salvarTurma($to);
    }

    /**
     * Método que salva um array de turmamatricula
     * @param array $arrTurmaMatriculaTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaMatricula($arrTurmaMatriculaTO)
    {
        return $this->_bo->salvarArrTurmaMatricula($arrTurmaMatriculaTO);
    }

    /**
     * Método que salva um array de turmaentidade
     * @param array $arrTurmaEntidadeTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaEntidade($arrTurmaEntidadeTO)
    {
        return $this->_bo->salvarArrTurmaEntidade($arrTurmaEntidadeTO);
    }

    /**
     * Método que salva um array de turmaprojeto
     * @param array $arrTurmaProjetoTO
     * @return Ead1_Mensageiro
     */
    public function salvarArrTurmaProjeto($arrTurmaProjetoTO)
    {
        return $this->_bo->salvarArrTurmaProjeto($arrTurmaProjetoTO);
    }

    /**
     * Método que salva o vinculo da matricula com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaMatricula(TurmaMatriculaTO $to)
    {
        return $this->_bo->salvarTurmaMatricula($to);
    }

    /**
     * Método que salva o vinculo da entidade com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaEntidade(TurmaEntidadeTO $to)
    {
        return $this->_bo->salvarTurmaEntidade($to);
    }

    /**
     * Método que salva o vinculo do projeto com a turma
     * @return Ead1_Mensageiro
     */
    public function salvarTurmaProjeto(TurmaProjetoTO $to)
    {
        return $this->_bo->salvarTurmaProjeto($to);
    }

    /**
     * Método que retorna a turma
     * @param TurmaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurma(TurmaTO $to)
    {
        return $this->_bo->retornarTurma($to);
    }

    /**
     * Método que retorna os vinculos da turma com a Matricula
     * @param TurmaMatriculaTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaMatricula(TurmaMatriculaTO $to)
    {
        return $this->_bo->retornarTurmaMatricula($to);
    }

    /**
     * Método que retorna os vinculos da turma com a Entidade
     * @param TurmaEntidadeTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaEntidade(TurmaEntidadeTO $to)
    {
        return $this->_bo->retornarTurmaEntidade($to);
    }

    /**
     * Método que retorna os vinculos da turma com o projeto
     * @param TurmaProjetoTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarTurmaProjeto(TurmaProjetoTO $to)
    {
        return $this->_bo->retornarTurmaProjeto($to);
    }

    /**
     * Método retorna as ocorrências do usuário da sessão.
     * @param VwConferenciaTurmasTO $to
     * @return Ead1_Mensageiro
     */
    public function retornarDisciplinasProjetos(VwConferenciaTurmasTO $to)
    {
        return $this->_bo->retornarConferenciaTurmas($to);
    }

    public function retornarProjetosTurmas(VwConferenciaProjetoConsolidadoTO $to)
    {
        return $this->_bo->retornarProjetosTurmas($to);
    }

}
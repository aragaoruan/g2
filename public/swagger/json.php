<?php
zray_disable();
error_reporting(0);
require(__DIR__."/../../library/vendor/autoload.php");
$swagger = \Swagger\scan(array(
    __DIR__.'/../../application/apps/apiv2/controllers',
    __DIR__.'/../../library/G2/Entity',
    __DIR__.'/../../library/G2/Utils',
    __DIR__.'/../../library/Ead1/Mensageiro.php',
),
array('exclude'=>array(
    __DIR__.'library/G2/Entity/Integracao',
    __DIR__.'library/G2/Entity/Proxy'
))
);
header('Content-Type: application/json');
echo $swagger;
<?php

zray_disable();

defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

defined('APPLICATION_SYS')
|| define('APPLICATION_SYS', 'GESTOR');

defined('URL_APP')
|| define('URL_APP', $_SERVER['HTTP_HOST']);

defined('APPLICATION_REAL_PATH')
|| define('APPLICATION_REAL_PATH', realpath(dirname(__FILE__) ));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'desenvolvimento'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/'),
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../library/Imagine/lib'),
    realpath(APPLICATION_PATH . '/configs'),
    realpath(APPLICATION_PATH . '/ro'),
    realpath(APPLICATION_PATH . '/apps/portal/controllers'),
    realpath(APPLICATION_PATH . '/models'),
    realpath(APPLICATION_PATH . '/apps/portal/models'),
    realpath(APPLICATION_PATH . '/apps/portal/models/bo'),
    realpath(APPLICATION_PATH . '/models/to'),
    realpath(APPLICATION_PATH . '/models/bo'),
    realpath(APPLICATION_PATH . '/models/dao'),
    realpath(APPLICATION_PATH . '/models/orm'),
    realpath(APPLICATION_PATH . '/apps/default/forms'),
    realpath(APPLICATION_PATH . '/webservices/server'),
    realpath(APPLICATION_PATH . '/webservices/clients/'),
    realpath(APPLICATION_PATH . '/webservices/clients/sa'),
    realpath(APPLICATION_PATH . '/webservices/clients/actor'),
    realpath(APPLICATION_PATH . '/webservices/clients/moodle'),
    realpath(APPLICATION_PATH . '/webservices/clients/totvs'),
    realpath(APPLICATION_PATH . '/webservices/clients/pagamento'),
    realpath(APPLICATION_PATH . '/webservices/clients/pagamento/Braspag/to'),
    realpath(APPLICATION_PATH . '/webservices/clients/BlackBoard'),
    realpath(APPLICATION_PATH . '/webservices/clients/BlackBoard/VO'),
    realpath(APPLICATION_PATH . '/webservices/clients/BlackBoard/Service'),
    realpath(APPLICATION_PATH . '/webservices/clients/Maximize'),
    get_include_path()
)));

// define('DEBUG_APP_MODE', true, false);
// require dirname(__DIR__) . '/dump.php';

include('Zend/Loader/Autoloader.php');
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

/** Zend_Application */
require_once 'Zend/Application.php';

$config = new Zend_Config_Ini(
    APPLICATION_PATH . '/configs/application.ini',
    APPLICATION_ENV
);

Zend_Registry::set('config', $config);

// Create application, bootstrap, and run

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

/**
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 * Verificar se o sistema esta e manutencao (application.ini, var maintenance)
 */
$inMaintenance = $application->getOption('maintenance');
if ($inMaintenance) {
    include(APPLICATION_PATH . '/views/scripts/index/manutencao.phtml');
    exit;
}

$application->bootstrap()->run();

<?php

zray_disable();

// Define path to application directory
define('APPLICATION_ROOT', dirname(dirname(__FILE__)));
defined('APPLICATION_SYS')
|| define('APPLICATION_SYS', 'GESTOR');

defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

defined('APPLICATION_REAL_PATH')
|| define('APPLICATION_REAL_PATH', realpath(dirname(__FILE__) ));

defined('URL_APP')
|| define('URL_APP', $_SERVER['HTTP_HOST']);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/configs'),
    get_include_path(),
)));


include('Zend/Loader/Autoloader.php');

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'producao'));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$options = $application->getOptions();
$options['resources']['frontController']['defaultModule'] = 'appapi';
$options['resources']['frontController']['prefixDefaultModule'] = TRUE;
$options['resources']['layout']['layoutPath'] = APPLICATION_PATH . '/apps/appapi/views/layouts';
$application->setOptions($options);

$application->bootstrap()->run();

		
            
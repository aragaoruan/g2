/* 
 * Funções js do Primeiro Acesso
 */

$(document).ready(function() {
    $('#pa-aba-endereco-residencial').click(function() {
        $('#pa-aba-endereco-residencial').addClass('active');
        $('#pa-conteudo-endereco-residencial').removeClass('hidden');
        $('#pa-aba-endereco-correspondencia').removeClass('active');
        $('#pa-conteudo-endereco-correspondencia').addClass('hidden');
    });

    $('#pa-aba-endereco-correspondencia').click(function() {
        $('#pa-aba-endereco-correspondencia').addClass('active');
        $('#pa-conteudo-endereco-correspondencia').removeClass('hidden');
        $('#pa-aba-endereco-residencial').removeClass('active');
        $('#pa-conteudo-endereco-residencial').addClass('hidden');
    });

    $('#motivo').keyup(function() {
        if ($('#motivo').prop('value').trim().length > 0) {
            if (verificarPreenchimento()) {
                $('#btnAlterar').removeClass('hidden');
            }
        } else {
            $('#btnAlterar').addClass('hidden');
        }
    });

    $('#pa-btn-corrigir').click(exibirAlteracoes);

    $('#btnCancelar').click(exibirAlteracoes);

    $('#pa-aceitacao-contrato-btn-prosseguir').click(function() {
        if (!$('#pa-aceitacao-contrato-checkbox').prop('checked')) {
            alert('Até o contrato ser lido e aceito você não poderá prosseguir.')
        }
        return true;
    });

});

exibirAlteracoes = function() {
    if ($('#pa-alteracoes-container').hasClass('hidden')) {
        $('#pa-alteracoes-container').show(500);
        $('#pa-alteracoes-container').removeClass('hidden');
    } else {
        $('#pa-alteracoes-container').hide(500);
        $('#pa-alteracoes-container').addClass('hidden');
    }
    return false;
};

verificarPreenchimento = function() {
    ok = false;
    $(".frm-alteracoes").find('input').each(function() {
        if ($(this).prop('value') != '' && $(this).prop('id') != 'motivo' && $(this).prop('id') != 'etapa') {
            ok = true;
        }
    });
    return ok;
    alert(ok);
};

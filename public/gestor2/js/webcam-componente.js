/**
 * Webcam componente
 * @type Object
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-27-11
 */
var webcamComponente = {
    /**
     * element
     * @type object
     */
    defaults: {
        element: null,
        width: 320,
        height: 240,
    },
    /**
     * Main componente
     * @param {object} params
     * @returns {void}
     */
    main: function(params) {
        if (this.validCallMethod(params)) {
            this.setDefaults(params);
            this.render();
        }
    },
    /**
     * Renderiza jquery webcam
     * @returns {void}
     */
    render: function() {
        that = this;
        var pos = 0;
        var ctx = null;
        var cam = null;
        var image = null;
        $(that.defaults.element).parent().append('<div id="status"></div>');
        $(that.defaults.element).parent().append('<canvas id="canvas" height="'+that.defaults.height+'" width="'+that.defaults.width+'"></canvas>');
        $("#canvas").hide();
        $("body").append('<div id="flash"></div>');
        var canvas = document.getElementById("canvas");

        if (canvas.getContext) {
            ctx = document.getElementById("canvas").getContext("2d");
            ctx.clearRect(0, 0, that.defaults.width, that.defaults.height);

            var img = new Image();
            img.onload = function() {
                ctx.drawImage(img, that.defaults.width, that.defaults.height);
            }
            image = ctx.getImageData(0, 0, that.defaults.width, that.defaults.height);

        }

        $(that.defaults.element).webcam({
            width: that.defaults.width,
            height: that.defaults.height,
            mode: "callback",
            swffile: "/js/jquery-webcam/jscam_canvas_only.swf",
            onTick: function(remain) {
                if (0 === remain) {
                    loaded();
                } else {
                    loading();
                }
            },
            onSave: function(data) {
                var col = data.split(";");
                var img = image;
                for (var i = 0; i < 320; i++) {
                    var tmp = parseInt(col[i]);
                    img.data[pos + 0] = (tmp >> 16) & 0xff;
                    img.data[pos + 1] = (tmp >> 8) & 0xff;
                    img.data[pos + 2] = tmp & 0xff;
                    img.data[pos + 3] = 0xff;
                    pos += 4;
                }
                if (pos >= 0x4B000) {
                    ctx.putImageData(img, 0, 0);
                    var canvas = document.getElementById("canvas");
                    var save_image = canvas.toDataURL("image/png");
                    $("#str_avatar").val(save_image);
                    pos = 0;
                }
            },
            onCapture: function() {
                $("#canvas").show();
                $(that.defaults.element).hide();
                webcam.save();
                $("#flash").css("display", "block");
                $("#flash").fadeOut(100, function() {
                    $("#flash").css("opacity", 1);
                });
            }
        });
    },
    /**
     * Verifica alguns objetos setados e seta os mesmo no default
     * @param {object} params
     * @returns {void}
     */
    setDefaults: function(params) {
        if (!params.element)
            params.element = this.defaults.element;
        if (!params.height)
            params.height = this.defaults.height;
        if (!params.width)
            params.width = this.defaults.width;
        //set defaults
        this.defaults = params;
    },
    /**
     * Valida os defaults setados na instancia 
     * @param {object} e
     * @returns {Boolean}
     */
    validCallMethod: function(e) {
        if (typeof(e) !== 'object') {
            throw 'expected an object';
            return false;
        }
        if (!e.element) {
            throw 'element for rendering undefined';
            return false;
        }
        return true;
    }
};

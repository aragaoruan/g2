/**
 * Componente de Auto Complete
 * @type Object
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-18-10
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @update 2015-02-23
 * @author Helder Silva <helder.sivla@unyleya.com.br>
 * @update 2015-04-27
 */
var componenteAutoComplete = {
    /**
     * Objeto com defaults
     * @type object
     */
    defaults: {
        model: null,
        element: null,
        delay: 400,
        urlApi: '/api/pessoa',
        localStorageName: 'pessoaAutocomplete',
        elementTemplateComponente: '#template-componente',
        elementTemplateListaComponente: '#template-componente-list-autocomplete',
        containerItens: '#autocomplete-results ul',
        tagNameItem: 'li',
        allEntidades: false,
    },
    onFix: null,
    onClear: null,
    autoCompleteCollection: null,
    stringSearch: null,
    modelRender: null,
    /**
     * Instancia e starta o componente
     * @param {object} params
     * @returns {undefined}
     */
    main: function (params, onFix, onClear) {
        if (this.validCallMethod(params)) {
            this.setDefaults(params);
            this.setCollection();
            this.renderElemento();
            //vefifica se existe pessoa fixada no storage e renderiza o autocomplete
            if (this.getFixModel()) {
                this.renderAutoCompleteFix();

                if (onFix && typeof (onFix) === "function") {   //fix do bug onde o restante da tela não vinha preenchido em caso
                    onFix();                                    //de usuário já fixado no componente de autocomplete de pessoa
                }
            }
        }

        if (onFix) {
            this.onFix = onFix;
        }
        if (onClear) {
            this.onClear = onClear;
        }
    },
    /**
     * Verifica alguns objetos setados e seta os mesmo no default
     * @param {object} params
     * @returns {undefined}
     */
    setDefaults: function (params) {
        if (!params.delay)
            params.delay = this.defaults.delay;

        if (!params.urlApi)
            params.urlApi = this.defaults.urlApi;

        if (!params.localStorageName)
            params.localStorageName = this.defaults.localStorageName;

        if (!params.elementTemplateComponente)
            params.elementTemplateComponente = this.defaults.elementTemplateComponente;

        if (!params.elementTemplateListaComponente)
            params.elementTemplateListaComponente = this.defaults.elementTemplateListaComponente;

        if (!params.containerItens)
            params.containerItens = this.defaults.containerItens;

        if (!params.tagNameItem)
            params.tagNameItem = this.defaults.tagNameItem;

        if (!params.allEntidades)
            params.allEntidades = this.defaults.allEntidades;

        this.defaults = params;
    },
    /**
     * Cria a collection e seta na variavel do escopo
     * @returns {undefined}
     */
    setCollection: function () {
        var self = this;
        self.autoCompleteCollection = Backbone.Collection.extend({
            url: self.defaults.urlApi,
            model: self.defaults.model
        });
    },
    /**
     * Renderiza elemento view do auto complete
     * @returns {undefined}
     */
    renderElemento: function () {
        var self = this;
        var InputAutoCompleteView = Backbone.View.extend({
            tagName: 'input',
            className: '',
            render: function () {
                var elTemplate = _.template($(self.defaults.elementTemplateComponente).html());
                this.$el.append(elTemplate);
                return this;
            },
            keyup: function (e) {
                var valorInput = $(this.el).find('input').val();
                if (valorInput.length >= 7 && self.stringSearch !== valorInput) {
                    clearTimeout(self.timer);
                    self.stringSearch = valorInput;
                    self.timer = setTimeout(function () {
                        self.renderAutoComplete();
                    }, self.defaults.delay);
                }
                return this;
            },
            keydown: function (e) {
                // Esc
                if (e.keyCode === 27) {
                    this.quitElement();
                    return false;
                }
                return self.disabledKeys(e);
            },
            quitElement: function (e) {
                this.clearAutoComplete();
            },
            clearAutoComplete: function () {
                this.$el.empty();
                self.clearFixModel();
                this.render();
            },
            fixAutoComplete: function () {
                self.setFixModel();
            },

            events: {
                "keyup input": "keyup",
                "keydown input": "keydown",
                "click #btnClearAutocomplete": "clearAutoComplete",
                "click #btnFixAutocomplete": "fixAutoComplete",
            }
        });
        new InputAutoCompleteView({el: this.defaults.element}).render();
    },
    /**
     * Render view para pessoa fixada
     * @returns {undefined}
     */
    renderAutoCompleteFix: function () {

        var self = this;

        var AutoCompleteItemView = Marionette.ItemView.extend({
            className: 'item-autocomplete',
            template: self.defaults.elementTemplateListaComponente,
            tagName: self.defaults.tagNameItem,
            onRender: function () {
                this.toggleState();
                return this;
            },
            toggleState: function () {
                this.$el.parent('div').show();
                this.$el.addClass('active');

                this.$el.siblings().remove();
                this.$el.closest('#search-wrapper').find('input#search-backbone').remove();

                $("#btnClearAutocomplete").show();
                $("#btnFixAutocomplete").show().attr('disabled', 'disabled');

                this.$el.find('a').css({
                    'text-decoration': 'none',
                    'color': '#777777',
                    'cursor': 'default'
                });

            }
        });


        var view = new AutoCompleteItemView({
            el: self.defaults.containerItens,
            model: new self.defaults.model(self.getFixModel())
        });
        view.render();

    },
    /**
     * Executa o auto complete, faz requisição e renderiza a view do autocomplete
     * @returns {undefined}
     */
    ajaxRequest: false,
    renderAutoComplete: function () {
        var self = this;

        var AutoCompleteItemView = Marionette.ItemView.extend({
            className: 'item-autocomplete',
            template: self.defaults.elementTemplateListaComponente,
            tagName: self.defaults.tagNameItem,
            toggleState: function () {
                this.$el.parent('div').show();
                this.$el.addClass('active');
                
                this.$el.siblings().remove();
                this.$el.closest('#search-wrapper').find('input#search-backbone').remove();

                $("#btnClearAutocomplete").show();

                $("#btnFixAutocomplete").show().attr('disabled', 'disabled');
                this.$el.find('a').css({
                    'text-decoration': 'none',
                    'color': '#777777',
                    'cursor': 'default'
                });
                this.$el.find('a').removeClass('link-alter-template');
                self.modelRender = this.model.toJSON();
                self.setFixModel();
                //Esconde o formulário
                if (typeof telaVendas != 'undefined')
                    telaVendas.ocultarFormulario();
            },
            setModelClicked: function () {
                self.modelRender = this.model.toJSON();
            },

            events: {
                'click .link-alter-template': 'toggleState',
                'click *[data-get-model="true"]': 'setModelClicked'
            }
        });

        var ContainerAutoCompleteCollectionView = Marionette.CollectionView.extend({
            childView: AutoCompleteItemView,
            tagName: 'ul'
        });

        var valor = self.stringSearch;
        var collection = new self.autoCompleteCollection;

        if (this.ajaxRequest)
            this.ajaxRequest.abort();

        collection.url = collection.url + "/str/" + valor;

        if (self.defaults.allEntidades) {
            collection.url = collection.url + "/all/1";
        } else if (self.defaults.entidadesMesmaHolding) {
            collection.url = collection.url + "/entidadesMesmaHolding/1";
        }

        this.ajaxRequest = collection.fetch({
            beforeSend: function () {
                loading();
                $(self.defaults.containerItens).empty();
            },
            success: function () {

                var renderView = new ContainerAutoCompleteCollectionView({
                    collection: collection,
                    el: self.defaults.containerItens
                });
                renderView.render();
            },
            complete: function () {
                if ($('#search-backbone').val().length >= 10 && !isNaN($('#search-backbone').val()) && collection.length == 1) {
                    $('#autocomplete-results>ul>li>a').trigger('click');
                }
                loaded();
            }
        });
    },
    /**
     * Desabiliza as ações para algumas teclas
     * @param {object} key
     * @returns {Boolean}
     */
    disabledKeys: function (key) {
        // Down
        if (key.keyCode === 40)
            return false;
        // Up
        if (key.keyCode === 38)
            return false;
        // Right
        if (key.keyCode === 39)
            return false;
        // Left
        if (key.keyCode === 37)
            return false;

        return true;
    },
    /**
     * Set LocalStorage com modelo da passoa selecionada
     * @returns {undefined}
     */
    setFixModel: function () {
        localStorage.setItem(this.defaults.localStorageName, JSON.stringify(this.modelRender));
//        localStorage.setItem('pessoaAutocomplete', JSON.stringify(this.modelRender));
        if (this.onFix && typeof (this.onFix) === "function") {
            this.onFix();
        }
    },
    /**
     * Get LocalStorage pessoaAutocomplete
     * @returns {@exp;localStorage@call;getItem}
     */
    getFixModel: function () {
        return JSON.parse(localStorage.getItem(this.defaults.localStorageName));
//        return JSON.parse(localStorage.getItem('pessoaAutocomplete'));
    },
    /**
     * Remove pessoaAutocomplete de localStorage
     * @returns {undefined}
     */
    clearFixModel: function () {
//        localStorage.removeItem('pessoaAutocomplete');
        localStorage.removeItem(this.defaults.localStorageName);
        if (this.onClear && typeof (this.onClear) == "function") {
            this.onClear();
        }

    },
    /**
     * Retorna Model fixada no localStorage mesmo que getFixModel()
     * @returns {this.getFixModel}
     */
    getFixPessoa: function () {
        return this.getFixModel();
    },
    /**
     * Valida os defaults setados na instancia
     * @param {object} e
     * @returns {Boolean}
     */
    validCallMethod: function (e) {
        if (typeof (e) !== 'object') {
            throw 'expected an object';
        }

        if (!e.element) {
            throw 'element for rendering undefined';
        }

        if (!e.model) {
            throw 'model undefined';
        }

        return true;
    }
};
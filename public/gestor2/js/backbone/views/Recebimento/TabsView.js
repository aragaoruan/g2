/**
 * View geral para a funcionalidade de cadastro de recebimento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/

var tplTabs = '';
$.get('/js/backbone/templates/recebimento/tpl-tabs.html?_=' + new Date().getTime(), '', function (data) {
    tplTabs = data;
});

$.getScript('/js/backbone/views/Util/ModalLayout.js');
$.getScript('/js/backbone/views/Recebimento/ResumoView.js?_=' + new Date().getTime());
$.getScript('/js/backbone/views/Recebimento/CartaoCreditoView.js');
$.getScript('/js/backbone/views/Recebimento/BoletoView.js');
$.getScript('/js/backbone/views/Recebimento/DinheiroView.js');
$.getScript('/js/backbone/views/Recebimento/ChequeView.js');
$.getScript('/js/backbone/views/Recebimento/EmpenhoView.js');
$.getScript('/js/backbone/views/Recebimento/DepositoView.js');
$.getScript('/js/backbone/views/Recebimento/CartaoDebitoView.js');
$.getScript('/js/backbone/views/Recebimento/TransferenciaView.js');
$.getScript('/js/backbone/views/Recebimento/CartaoRecorrenteView.js?id=' + new Date().getTime());



/***********************
 *   RecebimentoView
 **********************/

var TabsView = Backbone.Marionette.LayoutView.extend({
    template: _.template(tplTabs),
    events: {
        'click #btn-back': 'backToSearch',
        'click #link-resumo': 'renderResumoView',
        'click #link-cartao-credito': 'renderCartaoCreditoView',
        'click #link-boleto': 'renderBoletoView',
        'click #link-dinheiro': 'renderDinheiroView',
        'click #link-cheque': 'renderChequeView',
        'click #link-empenho': 'renderEmpenhoView',
        'click #link-deposito': 'renderDepositoView',
        'click #link-cartao-debito': 'renderCartaoDebitoView',
        'click #link-transferencia': 'renderTransferenciaView',
        'click #link-permuta': 'renderPermutaView',
        'click #link-bolsa': 'renderBolsaView',
        'click #link-cartao-recorrente': 'renderCartaoRecorrenteView',
        'click #link-estorno-cartao': 'renderEstornoCartaoView'
    },
    regions: {
        'modalRegion': '#container-modal',
        'resumoRegion': '#container-resumo',
        "cartaoCreditoRegion": '#container-cartao-credito',
        "boletoRegion": '#container-boleto',
        'dinheiroRegion': '#container-dinheiro',
        'chequeRegion': '#container-cheque',
        'empenhoRegion': '#container-empenho',
        'depositoRegion': '#container-deposito',
        'cartaoDebitoRegion': '#container-cartao-debito',
        'transferenciaRegion': '#container-transferencia',
        'permutaRegion': '#container-permuta',
        'bolsaRegion': '#container-bolsa',
        'cartaoRecorrenteRegion': '#container-cartao-recorrente',
        'estornoCartaoRegion': '#container-estorno-cartao'
    },
    renderResumoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/ResumoView.js');
            this.resumoRegion.show(new ResumoView());
        $.ajaxSetup({'async': true});
    },
    renderCartaoCreditoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/CartaoCreditoView.js');
            this.cartaoCreditoRegion.show(new CartaoCreditoView());
        $.ajaxSetup({'async': true});
    },
    renderBoletoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/BoletoView.js');
            this.boletoRegion.show(new BoletoView());
        $.ajaxSetup({'async': true});
    },
    renderDinheiroView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/DinheiroView.js');
            this.dinheiroRegion.show(new DinheiroView());
        $.ajaxSetup({'async': true});
    },
    renderChequeView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/ChequeView.js');
            this.chequeRegion.show(new ChequeView());
        $.ajaxSetup({'async': true});
    },
    renderEmpenhoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/EmpenhoView.js');
            this.empenhoRegion.show(new EmpenhoView());
        $.ajaxSetup({'async': true});
    },
    renderDepositoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/DepositoView.js');
            this.depositoRegion.show(new DepositoView());
        $.ajaxSetup({'async': true});
    },
    renderCartaoDebitoView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/CartaoDebitoView.js');
            this.cartaoDebitoRegion.show(new CartaoDebitoView());
        $.ajaxSetup({'async': true});
    },
    renderTransferenciaView: function () {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/TransferenciaView.js');
            this.transferenciaRegion.show(new TransferenciaView());
        $.ajaxSetup({'async': true});
    },
    renderPermutaView: function () {
        //this.permutaRegion.show(new PermutaView());
    },
    renderBolsaView: function () {
        //this.bolsaRegion.show(new BolsaView());
    },
    renderCartaoRecorrenteView: function () {
        $.ajaxSetup({'async': false});
        $.getScript('/js/backbone/views/Recebimento/CartaoRecorrenteView.js?id=' + new Date().getTime());
        this.cartaoRecorrenteRegion.show(new CartaoRecorrenteView());
        $.ajaxSetup({'async': true});
    },
    renderEstornoCartaoView: function () {
        //this.estornoCartaoRegion.show(new EstornoCartaoView());
    },
    onShow: function () {
        this.ocultarTabs();
        this.renderResumoView();
    },
    backToSearch: function () {
        G2S.layout.content.currentView.pesquisaRegion.show(new PesquisaView());
        G2S.layout.content.currentView.tabsRegion.reset();
        G2S.layout.content.currentView.vwClienteVenda = null;
    },
    ocultarTabs: function () {
        $.ajaxSetup({'async': false});
            var collection = new Backbone.Collection();
            collection.model = Backbone.Model;
            collection.url = '/recebimento/get-meios-pagamento-venda/id_venda/' + G2S.layout.content.currentView.vwClienteVenda.get('id_venda');
            collection.fetch();

            $('#recebimento-abas li').addClass('hide');
            $('#recebimento-abas #link-resumo').parent().removeClass('hide');

            collection.each(function (model) {
                switch (model.get('id_meiopagamento')) {
                    case 1:
                        //cartão de crédito
                        $('#recebimento-abas #link-cartao-credito').parent().removeClass('hide');
                        break;
                    case 2:
                        //boleto
                        $('#recebimento-abas #link-boleto').parent().removeClass('hide');
                        break;
                    case 3:
                        //dinheiro
                        $('#recebimento-abas #link-dinheiro').parent().removeClass('hide');
                        break;
                    case 4:
                        //cheque
                        $('#recebimento-abas #link-cheque').parent().removeClass('hide');
                        break;
                    case 5:
                        //empenho
                        $('#recebimento-abas #link-empenho').parent().removeClass('hide');
                        break;
                    case 6:
                        //depósito
                        $('#recebimento-abas #link-deposito').parent().removeClass('hide');
                        break;
                    case 7:
                        //cartão débito
                        $('#recebimento-abas #link-cartao-debito').parent().removeClass('hide');
                        break;
                    case 8:
                        //transferência
                        $('#recebimento-abas #link-transferencia').parent().removeClass('hide');
                        break;
                    case 9:
                        //permuta
                        $('#recebimento-abas #link-permuta').parent().removeClass('hide');
                        break;
                    case 10:
                        //bolsa
                        $('#recebimento-abas #link-bolsa').parent().removeClass('hide');
                        break;
                    case 11:
                        //cartão recorrente
                        $('#recebimento-abas #link-cartao-recorrente').parent().removeClass('hide');
                        break;
                    case 12:
                        //estorno de cartão
                        $('#recebimento-abas #link-estorno-cartao').parent().removeClass('hide');
                        break;
                    default:
                        break;
                }

            });
        $.ajaxSetup({'async': true});
    }
});

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/ResumoRowView.js');

var tplResumoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-resumo-table.html?_=' + new Date().getTime(), '', function (data) {
    tplResumoTable = data;
});

/***********************
 *  ResumoTableView
 *  Reponsavel por renderizar a tabela ResumoTableView
 ***********************/

var ResumoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplResumoTable),
    childView: ResumoRowView,
    childViewContainer: "#resumo-row-container",
    initialize: function () {
        var Collection = Backbone.Collection.extend({
            model: VwResumoFinanceiro,
            url: function () {
                return '/recebimento/retornar-resumo-financeiro?id_venda' + G2S.layout.currentView.tabsRegion.currentView.vwClienteVenda.id_venda;
            }
        });
        this.collection = new Collection();
        //this.collection.fetch();
    }
});
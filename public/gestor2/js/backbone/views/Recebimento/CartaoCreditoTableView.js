/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/CartaoCreditoRowView.js');

var tplCartaoCreditoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-credito-table.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoCreditoTable = data;
});

/***********************
 *  CartaoCreditoTableView
 *  Reponsavel por renderizar a tabela CartaoCreditoTableView
 ***********************/

var CartaoCreditoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplCartaoCreditoTable),
    childView: CartaoCreditoRowView,
    childViewContainer: "#cartao-credito-row-container",
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
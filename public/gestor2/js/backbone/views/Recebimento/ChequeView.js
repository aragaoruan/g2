/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/ChequeTableView.js');

var tplCheque = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque.html?_=' + new Date().getTime(), '', function (data) {
    tplCheque = data;
});

/***********************
 *   ChequeView
 **********************/

var ChequeView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplCheque);
    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new ChequeTableView();

        //MeioPagamento::CHEQUE = 4
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&id_meiopagamento=4';
        tableView.collection.fetch();
        tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-cheque-list").html(tableView.render().$el);
    },
    events: {
        'click #btn-cheque-devolvidos' : 'listaChequeDevolvido'
    },
    listaChequeDevolvido: function(){
        $.ajaxSetup({'async': false});
            $.getScript('js/backbone/views/Recebimento/ChequesDevolvidosView.js');
            // a modal está sendo abrigada na tpl-tabs.html div#container-modal
            var view = new ChequesDevolvidosView();
            view.model = this.model;

            var modal = new ModalLayout({view: view, label: 'Cheque(s) Devolvido(s)'});

            G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.show(modal);
        $.ajaxSetup({'async': true});
    }
});
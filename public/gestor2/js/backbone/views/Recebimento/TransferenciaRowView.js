/***********************
 *   imports
 **********************/

var tplTransferenciaRow = '';
$.get('/js/backbone/templates/recebimento/tpl-transferencia-row.html?_=' + new Date().getTime(), '', function (data) {
    tplTransferenciaRow = data;
});

/***********************
 *  TransferenciaRowView
 *  Reponsavel por renderizar uma linha da TransferenciaRowView
 ***********************/

var TransferenciaRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplTransferenciaRow),
    events: {
        'click .btn-pagar': 'pagar'
    },
    onShow: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    onRender: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    verificaPermissaoBotaoQuitar: function () {
        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (this.options.permissao.length) {
            $(this.el).find('.btn-pagar').removeClass('hide');
        } else {
            $(this.el).find('.btn-pagar').addClass('hide');
        }
    },
    pagar: function () {
        if (confirm('Deseja confirmar o recebimento?')) {
            var that = this;
            $.post(
                '/recebimento/pagar-transferencia',
                {
                    'id_lancamento': that.model.get('id_lancamento'),
                    'id_meiopagamento': that.model.get('id_meiopagamento')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    $('#link-transferencia').trigger('click');
                }
            );
        } else {
            return false;
        }
    }
});
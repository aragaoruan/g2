/***********************
 *   imports
 **********************/

var tplCartaoCreditoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-credito-row.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoCreditoRow = data;
});

/***********************
 *  CartaoCreditoRowView
 *  Reponsavel por renderizar uma linha da CartaoCreditoRowView
 ***********************/

var CartaoCreditoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplCartaoCreditoRow),
    events: {
        'click #btn-enviar-link-cc-acordo': 'enviarLinkCcAcordo'
    },
    enviarLinkCcAcordo: function () {
        if (confirm('Deseja enviar o link de pagamento?')) {
            loading();
            $.post(
                '/recebimento/enviar-link-cartao-credito-acordo',
                {
                    id_venda: this.model.get('id_venda'),
                    id_acordo: this.model.get('id_acordo'),
                    id_lancamento: this.model.get('id_lancamento')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    loaded();
                }
            );
        } else {
            return false;
        }
    },
});
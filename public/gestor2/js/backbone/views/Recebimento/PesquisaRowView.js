/***********************
 *   imports
 **********************/

var tplPesquisaRow = '';
$.get('/js/backbone/templates/recebimento/tpl-pesquisa-row.html?_=' + new Date().getTime(), '', function(data) {
    tplPesquisaRow = data;
});

$.getScript('/js/backbone/views/Recebimento/TabsView.js?_=' + new Date().getTime());

/***********************
 *  ProjetoRowView
 *  Reponsavel por renderizar uma linha da PesquisaRowView
 ***********************/

var PesquisaRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplPesquisaRow),
    events: {
        'click .btn-edit': 'showForm'
    },
    showForm: function() {
        G2S.layout.content.currentView.vwClienteVenda = this.model;
        G2S.layout.content.currentView.pesquisaRegion.reset();
        var view = new TabsView();
        view.model = this.model;
        G2S.layout.content.currentView.tabsRegion.show(view);
        //G2S.layout.content.currentView.projetoRegion.show(view);
    }
});
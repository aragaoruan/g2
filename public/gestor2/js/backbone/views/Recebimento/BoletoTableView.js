/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/BoletoRowView.js?_='+ new Date().getTime());

var tplBoletoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-boleto-table.html?_=' + new Date().getTime(), '', function (data) {
    tplBoletoTable = data;
});

/***********************
 *  BoletoTableView
 *  Reponsavel por renderizar a tabela BoletoTableView
 ***********************/

var BoletoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplBoletoTable),
    childView: BoletoRowView,
    childViewContainer: "#boleto-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    permissao: null,
    initialize: function ( ) {
        this.collection = new Backbone.Collection();
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    }
});
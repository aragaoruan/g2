/***********************
 *   imports
 **********************/

var tplCartaoDebitoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-debito-row.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoDebitoRow = data;
});

//$.getScript('/js/backbone/views/Recebimento/CartaoDebitoQuitarView.js');

/***********************
 *  CartaoDebitoRowView
 *  Reponsavel por renderizar uma linha da CartaoDebitoRowView
 ***********************/

var CartaoDebitoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplCartaoDebitoRow),
    events: {
        'click .btn-imprimir': 'imprimir'
    }
});
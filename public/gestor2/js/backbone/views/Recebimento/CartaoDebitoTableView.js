/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/CartaoDebitoRowView.js');

var tplCartaoDebitoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-debito-table.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoDebitoTable = data;
});

/***********************
 *  CartaoDebitoTableView
 *  Reponsavel por renderizar a tabela CartaoDebitoTableView
 ***********************/

var CartaoDebitoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplCartaoDebitoTable),
    childView: CartaoDebitoRowView,
    childViewContainer: "#cartao-debito-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
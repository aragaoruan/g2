/**
 * View geral para a funcionalidade de cadastro de recebimento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

$.ajaxSetup({'async': false});
loading();

/***********************
 *   imports
 **********************/

$.getScript('/js/backbone/views/Recebimento/PesquisaView.js?_=' + new Date().getTime());
$.getScript('/js/moment-with-langs.min.js');

var tpl = '';
$.get('/js/backbone/templates/recebimento/tpl.html?_=' + new Date().getTime(), '', function (data) {
    tpl = data;
});

/***********************
 *   RecebimentoView
 **********************/

var RecebimentoView = Backbone.Marionette.LayoutView.extend({
    template: _.template(tpl),
    vwClienteVenda: null,
    events: {
        'click #link-pesquisa': 'renderResumoView'
    },
    regions: {
        'pesquisaRegion': '#container-pesquisa',
        'tabsRegion': '#container-tabs'
    },
    renderPesquisaView: function () {
        this.pesquisaRegion.show(new PesquisaView());
    },
    onShow: function () {
        this.renderPesquisaView();
        loaded();
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/DepositoRowView.js');

var tplDepositoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-deposito-table.html?_=' + new Date().getTime(), '', function (data) {
    tplDepositoTable = data;
});

/***********************
 *  DepositoTableView
 *  Reponsavel por renderizar a tabela DepositoTableView
 ***********************/

var DepositoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplDepositoTable),
    childView: DepositoRowView,
    childViewContainer: "#deposito-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
/***********************
 *   imports
 **********************/

var tplDepositoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-deposito-row.html?_=' + new Date().getTime(), '', function (data) {
    tplDepositoRow = data;
});

/***********************
 *  DepositoRowView
 *  Reponsavel por renderizar uma linha da DepositoRowView
 ***********************/

var DepositoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplDepositoRow),
    events: {
        'click .btn-pagar': 'pagar'
    },
    onShow: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    onRender: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    verificaPermissaoBotaoQuitar: function () {
        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (this.options.permissao.length) {
            $(this.el).find('.btn-pagar').removeClass('hide');
        } else {
            $(this.el).find('.btn-pagar').addClass('hide');
        }
    },
    pagar: function () {
        if (confirm('Deseja confirmar o recebimento?')) {
            var that = this;
            $.post(
                '/recebimento/pagar-deposito',
                {
                    'id_lancamento': that.model.get('id_lancamento'),
                    'id_meiopagamento': that.model.get('id_meiopagamento')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    $('#link-deposito').trigger('click');
                }
            );
        } else {
            return false;
        }
    }
});
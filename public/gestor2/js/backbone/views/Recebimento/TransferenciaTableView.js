/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/TransferenciaRowView.js');

var tplTransferenciaTable = '';
$.get('/js/backbone/templates/recebimento/tpl-transferencia-table.html?_=' + new Date().getTime(), '', function (data) {
    tplTransferenciaTable = data;
});

/***********************
 *  TransferenciaTableView
 *  Reponsavel por renderizar a tabela TransferenciaTableView
 ***********************/

var TransferenciaTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTransferenciaTable),
    childView: TransferenciaRowView,
    childViewContainer: "#transferencia-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
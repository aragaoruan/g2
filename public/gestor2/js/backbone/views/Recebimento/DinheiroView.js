/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/DinheiroTableView.js');

var tplDinheiro = '';
$.get('/js/backbone/templates/recebimento/tpl-dinheiro.html?_=' + new Date().getTime(), '', function (data) {
    tplDinheiro = data;
});

/***********************
 *   DinheiroView
 **********************/

var DinheiroView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplDinheiro);
    },
    events: {

    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new DinheiroTableView();

        //MeioPagamento::DINHEIRO = 3
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val()+'&id_meiopagamento=3';
        tableView.collection.fetch();
        tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-dinheiro-list").html(tableView.render().$el);
    }
});
var template= '';
$.get(
    '/recebimento/cartao-recorrente',
    {
        id_venda: G2S.layout.content.currentView.vwClienteVenda.id
    },
    function (data) {
        template = _.template(data,{});
}, 'html');
var CartaoRecorrenteView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        // this.model = new VwResumoFinanceiro();
        this.template = _.template(template);
    },
    events: {
        'click #btn-trocar-recorrente': 'trocarCartaoRecorrente'
    },
    onShow: function () {
    },
    trocarCartaoRecorrente: function(data) {
        window.open($("#btn-trocar-recorrente").attr(
            "data-urlloja")+'/pagamento/mudar-cartao-recorrente/id_venda/'+G2S.layout.content.currentView.vwClienteVenda.id, '_blank'
        );
    }
});
/***********************
 *   imports
 **********************/

var tplChequeDevolvido = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-devolvido.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeDevolvido = data;
});

$.getScript('js/backbone/views/Recebimento/ChequeDevolvidoTableView.js');

/***********************
 *   BoletoView
 **********************/

var ChequesDevolvidosView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplChequeDevolvido);
    },
    events: {
        'click #btn-email-cheque-devolvido': 'enviarEmailChequeDevolvido',
        'click #btn-cancelar-cheque-devolvido': 'fecharModal'
    },
    fecharModal: function () {
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.currentView.hideModal();
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.reset();
    },
    enviarEmailChequeDevolvido: function () {
        var that = this;
        $.post(
            '/recebimento/enviar-email-lista-cheques-devolvidos',
            $('#form-cheques-devolvidos').serialize(),
            function (data) {
                $.pnotify($.parseJSON(data));
                that.fecharModal();
            }
        );
    },
    onShow: function () {
        this.renderList();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new ChequeDevolvidoTableView();

        //MeioPagamento::BOLETO = 2
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&bl_chequedevolvido=1';
        tableView.collection.fetch();
        if (tableView.collection.length != 0) {
            $("#container-cheque-devolvido-list").html(tableView.render().$el);
            $('#container-botons-cheque-devolvidos').show();
        } else {
            $("#container-cheque-devolvido-list").html('Essa venda não possui cheque devolvido');
            $('#container-botons-cheque-devolvidos').hide();
        }

    }
});
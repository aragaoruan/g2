/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/TransferenciaTableView.js');

var tplTransferencia = '';
$.get('/js/backbone/templates/recebimento/tpl-transferencia.html?_=' + new Date().getTime(), '', function (data) {
    tplTransferencia = data;
});

/***********************
 *   TransferenciaView
 **********************/

var TransferenciaView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplTransferencia);
    },
    events: {

    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new TransferenciaTableView();

        //MeioPagamento::TRANSFERENCIA = 8
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val()+'&id_meiopagamento=8';
        tableView.collection.fetch();
        tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-transferencia-list").html(tableView.render().$el);
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/BoletoTableView.js?_='+ new Date().getTime());

var tplBoleto = '';
$.get('/js/backbone/templates/recebimento/tpl-boleto.html?_=' + new Date().getTime(), '', function (data) {
    tplBoleto = data;
});

/***********************
 *   BoletoView
 **********************/

var BoletoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        $.ajaxSetup({'async': false});
            this.model = new VwResumoFinanceiro();
            this.template = _.template(tplBoleto);
        this.permissao = VerificarPerfilPermissao(11, 201, true);
        $.ajaxSetup({'async': true});

    },
    onShow: function () {
        loading();
        console.log('inicializando o boleto')
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new BoletoTableView({permissao: this.permissao});

        //MeioPagamento::BOLETO = 2
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&id_meiopagamento=2';
        tableView.collection.fetch();
        tableView.setaPermissao(this.permissao);

        $("#container-boleto-list").html(tableView.render().$el);
    }
});
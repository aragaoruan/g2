/***********************
 *   imports
 **********************/

var tplBoletoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-boleto-row.html?_=' + new Date().getTime(), '', function (data) {
    tplBoletoRow = data;
});

$.getScript('/js/backbone/views/Recebimento/BoletoQuitarView.js?_='+ new Date().getTime());

/***********************
 *  BoletoRowView
 *  Reponsavel por renderizar uma linha da BoletoRowView
 ***********************/

var BoletoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplBoletoRow),
    events: {
        'click .btn-imprimir': 'imprimir',
        'click #btn-enviar-boleto': 'enviar',
        'click .btn-pagar': 'abrirModal',
        'click #btn-gerar-boleto': 'gerarBoletoRegistrado',
        'change .datepicker': 'changeDtVencimento'
    },
    onShow: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    onRender: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    verificaPermissaoBotaoQuitar: function () {
        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (this.options.permissao.length) {
            $(this.el).find('.btn-pagar').removeClass('hide');
        } else {
            $(this.el).find('.btn-pagar').addClass('hide');
        }
    },
    abrirModal: function () {

        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new BoletoQuitarView();
        view.model = this.model;

        var modal = new ModalLayout({view: view, label: 'Quitar boleto'});

        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.show(modal);
    },
    enviar: function () {

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/recebimento/enviar-boleto',
            data: {id_lancamento: this.model.get('id_lancamento')},
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            },
            complete: function () {
                loaded();
            },
            error: function (response) {
                $.pnotify({
                    title: 'Erro! 3154',
                    text: response.responseJSON.text,
                    type: 'error'
                });
            }
        });
    },
    gerarBoletoRegistrado: function () {

        var that = this;

        bootbox.confirm("Deseja realmente gerar o boleto?", function (result) {
            if (result) {

                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: '/Recebimento/gerar-boleto',
                    data: {'id_lancamento': that.model.get('id_lancamento'), 'funcao': 'getbarcode'},
                    beforeSend: function () {
                        loading();
                    },
                    success: function (data) {
                        setTimeout(that.model.collection.fetch({}), 10000);

                        $.pnotify({
                            title: 'Operação efetuada com sucesso',
                            text: 'Solicitação para geração de boleto enviada com sucesso!' +
                            '<br>Enviaremos o boleto ao e-mail quando estiver disponível.',
                            type: 'success'
                        });
                    },
                    complete: function () {
                        loaded();
                    },
                    error: function (response) {
                        $.pnotify({
                            title: 'Erro! 3154',
                            text: 'Não foi possível solicitar o boleto. Tente novamente',
                            type: 'error'
                        });
                    }
                });
            }
        });
    },
    changeDtVencimento: function (event) {

        var dt_vencimento = $(event.target).val();

        if (dt_vencimento && (this.model.get('dt_vencimento').substr(0, 10) == dt_vencimento.substr(0, 10))) {
            return false;
        }

        let now = moment();
        let dt_vencimento_ = moment(dt_vencimento, 'DD/MM/YYYY');

        if (now.diff(dt_vencimento_, 'days') > 0) {
            $.pnotify({
                title: 'Data de vencimento inválida',
                text: 'Data de vencimento não pode ser menor que a data corrente!',
                type: 'warning'
            });
            return this;
        } else {
            let that = this;

            this.model.set('dt_vencimento', dt_vencimento);

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/venda/salvar-lancamento',
                data: {params: that.model.toJSON()},
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    setTimeout(that.model.collection.fetch({}), 12000);
                },
                complete: function () {
                    loaded();
                },
                error: function (response) {
                    $.pnotify({
                        title: 'Erro! 3154',
                        text: response.responseJSON.text,
                        type: 'error'
                    });
                }
            });
        }
    }
});
/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/EmpenhoTableView.js');

var tplEmpenho = '';
$.get('/js/backbone/templates/recebimento/tpl-empenho.html?_=' + new Date().getTime(), '', function (data) {
    tplEmpenho = data;
});

/***********************
 *   EmpenhoView
 **********************/

var EmpenhoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplEmpenho);
    },
    events: {},
    onShow: function () {
        loading();
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new EmpenhoTableView();

        //MeioPagamento::EMPENHO = 5
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&id_meiopagamento=5';
        tableView.collection.fetch();
        tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-empenho-list").html(tableView.render().$el);
    }
});
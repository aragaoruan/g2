/***********************
 *   imports
 **********************/

var tplCartaoCreditoQuitarRow = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-credito-quitar-row.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoCreditoQuitarRow = data;
});

/***********************
 *   CartaoCreditoQuitarRowView
 **********************/

var CartaoCreditoQuitarRowView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.template = _.template(tplCartaoCreditoQuitarRow);
    },
    events: {
        'change input': function (e) {
            e.preventDefault();
            var elem = $(e.target);
            this.model.set(elem.attr('name'), elem.val());
        }
    },
    tagName: "tr"
});
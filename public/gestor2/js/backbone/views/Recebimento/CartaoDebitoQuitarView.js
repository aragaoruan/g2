/***********************
 *   imports
 **********************/

var tplCartaoDebitoQuitar = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-debito-quitar.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoDebitoQuitar = data;
});

/***********************
 *   CartaoDebitoView
 **********************/

var CartaoDebitoQuitarView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplCartaoDebitoQuitar);
    },
    events: {
        'click #btn-quitar-cartao-debito': 'quitarCartaoDebito',
        'click #btn-cancelar': 'fecharModal'
    },
    onShow: function () {
        this.mostrarValorTotal();
    },
    validate: function () {
        if (!$('#form-quitar-cartao-debito #dt_prevquitado').val() || !$('#form-quitar-cartao-debito #st_coddocumento').val()) {
            $.pnotify({
                'type': 'warning',
                'title': 'Dados incorretos',
                'text': 'Os campos Código do Documento e Data de Previsão do Pagamento precisam ser preenchidos.'
            });
            return false;
        }
        return true;
    },
    quitarCartaoDebito: function () {
        if (this.validate()) {

            var dados = this.lancamentosAQuitar();

            if (confirm('Deseja quitar os lançamentos?')) {
                var that = this;
                $.post(
                    '/recebimento/pagar-cartao-debito',
                    {
                        'id_meiopagamento': that.model.get('id_meiopagamento'),
                        'dt_prevquitado': $('#form-quitar-cartao-debito #dt_prevquitado').val(),
                        'st_coddocumento': $('#form-quitar-cartao-debito #st_coddocumento').val(),
                        'nu_quitado': $('#form-quitar-cartao-debito #nu_quitado').val(),
                        'lancamentos': JSON.stringify(dados)
                    },
                    function (data) {
                        $.pnotify($.parseJSON(data));
                        $('#link-cartao-debito').trigger('click');
                        that.fecharModal();
                    }
                );
            } else {
                return false;
            }
        }
    },
    lancamentosAQuitar: function () {
        var collection = G2S.layout.content.currentView.tabsRegion.currentView.cartaoDebitoRegion.currentView.tableView.collection;

        var dados = [];
        _(collection.models).each(function (model, d) {
            //Verifica se é cartão de débito
            if (model.get('id_meiopagamento') == 7 && model.get('bl_quitado') == 0) {
                this.nu_valortotal += model.get('nu_valor') + model.get('nu_juros') - model.get('nu_desconto');
                dados.push(model.toJSON());
            }
        });

        return dados;
    },
    fecharModal: function () {
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.currentView.hideModal();
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.reset();
    },
    mostrarValorTotal: function(){
        var collection = G2S.layout.content.currentView.tabsRegion.currentView.cartaoDebitoRegion.currentView.tableView.collection;

        var total = 0;
        _(collection.models).each(function (model, d) {
            //Verifica se é cartão de débito e se nao foi quitado
            if (model.get('id_meiopagamento') == 7 && model.get('bl_quitado') == 0) {
                total += model.get('nu_valor') + model.get('nu_juros') - model.get('nu_desconto');
            }
        });

        $('#form-quitar-cartao-debito #nu_quitado').val(total.toFixed(2));
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/DinheiroRowView.js');

var tplDinheiroTable = '';
$.get('/js/backbone/templates/recebimento/tpl-dinheiro-table.html?_=' + new Date().getTime(), '', function (data) {
    tplDinheiroTable = data;
});

/***********************
 *  DinheiroTableView
 *  Reponsavel por renderizar a tabela DinheiroTableView
 ***********************/

var DinheiroTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplDinheiroTable),
    childView: DinheiroRowView,
    childViewContainer: "#dinheiro-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    }
});
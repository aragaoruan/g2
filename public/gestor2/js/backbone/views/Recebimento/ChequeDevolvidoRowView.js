/***********************
 *   imports
 **********************/

var tplChequeDevolvidoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-devolvido-row.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeDevolvidoRow = data;
});


/***********************
 *  BoletoRowView
 *  Reponsavel por renderizar uma linha da BoletoRowView
 ***********************/

var ChequeDevolvidoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplChequeDevolvidoRow),
    events: {
        //'click .btn-imprimir': 'imprimir',
        //'click .btn-enviar': 'enviar',
        //'click .btn-pagar': 'abrirModal'
    }
});
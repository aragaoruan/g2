/***********************
 *   imports
 **********************/

var tplChequeQuitar = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-quitar.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeQuitar = data;
});

/***********************
 *   ChequeView
 **********************/

var ChequeQuitarView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplChequeQuitar);
    },
    events: {
        'click #btn-quitar-cheque': 'QuitarCheque'
    },
    onShow: function () {
        $('#nu_quitado').maskMoney({precision: 2, decimal: ".", thousands: ""});
    },
    validate: function () {
        if (!$('#form-quitar-cheque #dt_quitado').val() || !$('#form-quitar-cheque #nu_quitado').val()) {
            $.pnotify({
                'type': 'warning',
                'title': 'Dados incorretos',
                'text': 'Os campos Valor e Data de Pagamento precisam ser preenchidos.'
            });
            return false;
        }
        return true;
    },
    QuitarCheque: function () {
        if (this.validate()) {
            if (confirm('Deseja quitar o lançamento?')) {
                var that = this;
                $.post(
                    '/recebimento/pagar-cheque',
                    {
                        'id_lancamento': that.model.get('id_lancamento'),
                        'id_meiopagamento': that.model.get('id_meiopagamento'),
                        'bl_ativo': true,
                        'bl_quitado': true,
                        'dt_quitado': $('#form-quitar-cheque #dt_quitado').val(),
                        'nu_quitado': $('#form-quitar-cheque #nu_quitado').val()
                    },
                    function (data) {
                        $.pnotify($.parseJSON(data));
                        $('#link-cheque').trigger('click');
                        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.currentView.hideModal();
                        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.reset();
                    }
                );
            } else {
                return false;
            }
        }
    }
});
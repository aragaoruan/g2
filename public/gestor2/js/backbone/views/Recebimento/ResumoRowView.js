/***********************
 *   imports
 **********************/

var tplResumoRow = '';
$.get('/js/backbone/templates/recebimento/tpl-resumo-row.html?_=' + new Date().getTime(), '', function (data) {
    tplResumoRow = data;
});

/***********************
 *  ResumoRowView
 *  Reponsavel por renderizar uma linha da ResumoRowView
 ***********************/

var ResumoRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplResumoRow),
    events: {
        'click .btn-print': 'print'
    },
    print: function () {

    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/ChequeRowView.js');

var tplChequeTable = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-table.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeTable = data;
});

/***********************
 *  ChequeTableView
 *  Reponsavel por renderizar a tabela ChequeTableView
 ***********************/

var ChequeTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplChequeTable),
    childView: ChequeRowView,
    childViewContainer: "#cheque-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
/***********************
 *   imports
 **********************/

$.getScript('/js/backbone/views/Recebimento/CartaoCreditoQuitarRowView.js');

var tplCartaoCreditoQuitar = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-credito-quitar.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoCreditoQuitar = data;
});

/***********************
 *   CartaoCreditoView
 **********************/

var CartaoCreditoQuitarView = Backbone.Marionette.CompositeView.extend({
    initialize: function () {
        this.template = _.template(tplCartaoCreditoQuitar);
    },
    childView: CartaoCreditoQuitarRowView,
    childViewContainer: '#container-lancamento',
    events: {
        'click #btn-quitar': 'quitarCartaoCredito',
        'click #btn-cancelar': 'fecharModal'
    },
    onShow: function () {

    },
    validate: function () {
        if (!$('#form-quitar-cartao-credito [name="dt_prevquitado"]').val() || !$('#form-quitar-cartao-credito [name="st_coddocumento"]').val()) {
            $.pnotify({
                'type': 'warning',
                'title': 'Dados incorretos',
                'text': 'Os campos Cód. do Documento e Data de Previsão de Pagamento precisam ser preenchidos.'
            });
            return false;
        }
        return true;
    },
    quitarCartaoCredito: function () {
        if (confirm('Deseja quitar os lançamentos?')) {
            if (this.validate()) {
                var that = this;
                var lancamentos = [];
                this.lancamentosAQuitar.each(function (m) {
                    m.set({'dt_prevquitado': $('#form-quitar-cartao-credito [name="dt_prevquitado"]').val()});
                    m.set({'st_coddocumento': $('#form-quitar-cartao-credito [name="st_coddocumento"]').val()});
                    lancamentos.push(m.toJSON());
                });

                $.ajax({
                    url: '/recebimento/pagar-cartao-credito',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(lancamentos),
                    dataType: 'json',
                    success: function (data) {
                        $.pnotify(data);
                        that.fecharModal();
                        G2S.layout.content.currentView.tabsRegion.currentView.renderCartaoCreditoView();
                    }
                });
            }
        } else {
            return false;
        }
    },
    fecharModal: function () {
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.currentView.hideModal();
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.reset();
    }
});
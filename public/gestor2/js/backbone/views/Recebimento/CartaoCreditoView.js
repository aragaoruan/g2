/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/CartaoCreditoTableView.js');
$.getScript('js/backbone/views/Recebimento/CartaoCreditoQuitarView.js');

var tplCartaoCredito = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-credito.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoCredito = data;
});

/***********************
 *   CartaoCreditoView
 **********************/

var CartaoCreditoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplCartaoCredito);
    },
    events: {
        'click #btn-quitar-cartao-credito': 'abrirModalQuitar',
        'click #btn-enviar-link-cartao-credito': 'enviarLinkPagamento',
        'click #btn-cancelar-cartao-credito': 'cancelarTransacao',
        'click #btn-estornar-cartao-credito': 'estornarPagamento'
    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();

        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (VerificarPerfilPermissao(11, 201).length) {
            $('#btn-quitar-cartao-credito').removeClass('hide');
        } else {
            $('#btn-quitar-cartao-credito').addClass('hide');
        }
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        this.tableView = new CartaoCreditoTableView();

        //MeioPagamento::CARTAO_CREDITO = 1
        this.tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&id_meiopagamento=1';
        this.tableView.collection.fetch({
            error: function (collection, response) {
                $.pnotify(response.responseJSON);
            }
        });
        $("#container-cartao-credito-list").html(this.tableView.render().$el);


        this.tableView.collection.each(function (model) {
            if (model.get('st_situacao') === 'Autorizado') {
                $('#btn-enviar-link-cartao-credito').attr('disabled', true);
            }
        });
    },
    abrirModalQuitar: function () {

        var quitado = true;
        this.tableView.collection.each(function (model) {
            if (model.get('st_situacao') === 'Atrasado'
                || model.get('st_situacao') === 'Vencido'
                || model.get('st_situacao') === 'Pendente') {
                quitado = false;
            }
        });

        if (quitado) {
            $.pnotify({
                'type': 'warning',
                'title': 'Atenção!',
                'text': 'Não há nenhum lançamento a ser quitado.'
            });
            return false;
        }

        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CartaoCreditoQuitarView();
        view.collection = this.tableView.collection.clone();

        //Collection completa a ser enviada para o RecebimentoCOntroller para serem quitados
        view.lancamentosAQuitar = this.tableView.collection.clone();

        //Esta operação é para deixar apenas o primeiro model que será usado de parametro
        //pra quitar os lancamentos quando é Cartão, gerando apenas uma linha na collectionView
        view.collection.models = [view.collection.models[0]];

        var modal = new ModalLayout({view: view, label: 'Quitar lançamentos'});

        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.show(modal);
    },
    enviarLinkPagamento: function () {
        if (confirm('Deseja enviar o link de pagamento?')) {
            loading();
            $.post(
                '/recebimento/enviar-link-cartao-credito',
                {
                    'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    loaded();
                }
            );
        } else {
            return false;
        }
    },
    cancelarTransacao: function () {
        if (confirm('Deseja realmente cancelar a transação?')) {
            loading();
            $.post(
                '/recebimento/cancelar-transacao-cartao-credito',
                {
                    'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    loaded();
                }
            );
        } else {
            return false;
        }
    },
    estornarPagamento: function () {
        alert('Em construção');
        return;
        if (confirm('Deseja realmente cancelar a transação?')) {
            loading();
            $.post(
                '/recebimento/cancelar-transacao-cartao-credito',
                {
                    'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    loaded();
                }
            );
        } else {
            return false;
        }
    }
});
/***********************
 *   imports
 **********************/

var tplDinheiroRow = '';
$.get('/js/backbone/templates/recebimento/tpl-dinheiro-row.html?_=' + new Date().getTime(), '', function (data) {
    tplDinheiroRow = data;
});

/***********************
 *  DinheiroRowView
 *  Reponsavel por renderizar uma linha da DinheiroRowView
 ***********************/

var DinheiroRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplDinheiroRow),
    events: {
        'click .btn-pagar': 'pagar'
    },
    onShow: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    onRender: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    verificaPermissaoBotaoQuitar: function () {
        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (this.options.permissao.length) {
            $(this.el).find('.btn-pagar').removeClass('hide');
        } else {
            $(this.el).find('.btn-pagar').addClass('hide');
        }
    },
    pagar: function () {
        if (confirm('Deseja confirmar o recebimento?')) {
            var that = this;
            $.post(
                '/recebimento/pagar-dinheiro',
                {
                    'id_lancamento': that.model.get('id_lancamento'),
                    'id_meiopagamento': that.model.get('id_meiopagamento')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    $('#link-dinheiro').trigger('click');
                }
            );
        } else {
            return false;
        }
    }
});
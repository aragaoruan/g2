/***********************
 *   imports
 **********************/

var tplBoletoQuitar = '';
$.get('/js/backbone/templates/recebimento/tpl-boleto-quitar.html?_=' + new Date().getTime(), '', function (data) {
    tplBoletoQuitar = data;
});

/***********************
 *   BoletoView
 **********************/

var BoletoQuitarView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplBoletoQuitar);
    },
    events: {
        'click #btn-quitar-boleto': 'quitarBoleto',
        'click #btn-cancelar': 'fecharModal'
    },
    onShow: function () {
        $('#nu_quitado').maskMoney({precision: 2, decimal: ".", thousands: ""});
        $('#nu_jurosboleto').maskMoney({precision: 2, decimal: ".", thousands: ""});
        $('#nu_descontoboleto').maskMoney({precision: 2, decimal: ".", thousands: ""});
    },
    validate: function () {
        if (!$('#form-quitar-boleto #dt_quitado').val() || !$('#form-quitar-boleto #nu_quitado').val()) {
            $.pnotify({
                'type': 'warning',
                'title': 'Dados incorretos',
                'text': 'Os campos Valor e Data de Pagamento precisam ser preenchidos.'
            });
            return false;
        }
        return true;
    },
    quitarBoleto: function () {
        if (this.validate()) {
            if (confirm('Deseja quitar o lançamento?')) {
                var that = this;
                $.post(
                    '/recebimento/pagar-boleto',
                    {
                        'id_lancamento': that.model.get('id_lancamento'),
                        'id_meiopagamento': that.model.get('id_meiopagamento'),
                        'dt_quitado': $('#form-quitar-boleto #dt_quitado').val(),
                        'nu_quitado': $('#form-quitar-boleto #nu_quitado').val(),
                        'nu_jurosboleto': $('#form-quitar-boleto #nu_jurosboleto').val(),
                        'nu_descontoboleto': $('#form-quitar-boleto #nu_descontoboleto').val()
                    },
                    function (data) {
                        $.pnotify($.parseJSON(data));
                        $('#link-boleto').trigger('click');
                        that.fecharModal();
                    }
                );
            } else {
                return false;
            }
        }
    },
    fecharModal: function () {
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.currentView.hideModal();
        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.reset();
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/EmpenhoRowView.js');

var tplEmpenhoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-empenho-table.html?_=' + new Date().getTime(), '', function (data) {
    tplEmpenhoTable = data;
});

/***********************
 *  EmpenhoTableView
 *  Reponsavel por renderizar a tabela EmpenhoTableView
 ***********************/

var EmpenhoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplEmpenhoTable),
    childView: EmpenhoRowView,
    childViewContainer: "#empenho-row-container",
    childViewOptions: function(model, index) {
        return {
            permissao: this.permissao
        }
    },
    setaPermissao: function(permissao){
        this.permissao = permissao;
    },
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
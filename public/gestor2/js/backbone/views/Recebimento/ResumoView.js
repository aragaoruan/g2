/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/vw-resumo-financeiro.js');
$.getScript('js/backbone/views/Recebimento/ResumoTableView.js');
//$.getScript('js/backbone/views/Recebimento/ChequesDevolvidosView.js');

var tplResumo = '';
$.get('/js/backbone/templates/recebimento/tpl-resumo.html?_=' + new Date().getTime(), '', function (data) {
    tplResumo = data;
});

/***********************
 *   PesquisaView
 **********************/
var ResumoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplResumo);
    },
    events: {
        'click #btn-confirmar-recebimento': 'confirmarRecebimento',
        'click #btn-ativar-matricula': 'ativarMatricula',
        'click #btn-sincronizar': 'sincronizarVenda'
    },
    onShow: function () {
        this.renderList();
        this.showActionButtons();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        $.ajaxSetup({'async': false});
        loading();
        var tableView = new ResumoTableView();
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + G2S.layout.content.currentView.vwClienteVenda.get('id_venda');
        tableView.collection.fetch();
        if (tableView.collection.length > 0) {
            $("#container-resumo-list").html(tableView.render().$el);
        } else {
            $.pnotify({'title': 'Atenção!', 'type': 'warning', 'text': 'Nenhum lançamento encontrado.'});
        }
        loaded();
    },
    showActionButtons: function () {
        var vw = G2S.layout.content.currentView.vwClienteVenda;
        if ((vw.get('id_evolucao') == 10 && vw.get('id_evolucaocontrato') == 4 ) || !VerificarPerfilPermissao(11, 201).length) {
            $('#btn-confirmar-recebimento').addClass('hide');
        } else {
            $('#btn-confirmar-recebimento').removeClass('hide');
        }

        if ((vw.get('id_situacao') == 39 && vw.get('id_situacaocontrato') == 44)) {
            $('#btn-ativar-matricula').addClass('hide');
        } else {
            $('#btn-ativar-matricula').removeClass('hide');
        }
    },
    confirmarRecebimento: function () {
        if (confirm('Deseja confirmar o recebimento?')) {
            loading();
            $.post(
                '/recebimento/confirmar-recebimento',
                {
                    'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')
                },
                function (data) {
                    $.pnotify($.parseJSON(data));
                    $('#btn-ativar-matricula').removeClass('hide');
                    $('#btn-confirmar-recebimento').addClass('hide');
                    loaded();
                }
            );
        } else {
            return false;
        }
    },
    ativarMatricula: function () {
        if (confirm('Deseja ativar esta matrícula?')) {
            loading();
            $.post(
                '/recebimento/ativar-matricula',
                {
                    'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')
                },
                function (data) {
                    //console.log(data.mensagem)
                    if (data.length) {
                        $.pnotify($.parseJSON(data));
                    } else {
                        $.pnotify({
                            title: 'Alerta',
                            type: 'warning',
                            text: data.mensagem
                        });
                    }
                    $('#btn-ativar-matricula').addClass('hide');
                    $('#btn-confirmar-recebimento').addClass('hide');
                    loaded();
                }, 'json'
            );
        } else {
            return false;
        }
    },
    sincronizarVenda: function () {
        if (confirm('Deseja sincronizar esta venda com o Fluxus?')) {
            loading();
            $('.icon-spin').addClass('icon-refresh-animate');
            $('#btn-sincronizar').attr('disabled', 'true');
            $.ajax({
                url: '/robo/sincronizar-dados-fluxus-gestor-w-s',
                type: 'post',
                dataType: 'json',
                data: {'id_venda': G2S.layout.content.currentView.vwClienteVenda.get('id_venda')},
                success: function (response) {
                    $.pnotify({
                        title: 'Alerta',
                        type: response.type,
                        text: response.message
                    });
                },
                error: function (response) {
                    $.pnotify({
                        title: 'Alerta',
                        type: response.type,
                        text: response.message
                    });
                },
                complete: function () {
                    loaded();
                    $('#btn-sincronizar').removeAttr('disabled');
                    $('.icon-spin').removeClass('icon-refresh-animate');
                }
            });

        } else {
            return false;
        }
    }
});



/***********************
 *   imports
 **********************/

var tplChequeRow = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-row.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeRow = data;
});

$.getScript('/js/backbone/views/Recebimento/ChequeQuitarView.js');

/***********************
 *  ChequeRowView
 *  Reponsavel por renderizar uma linha da ChequeRowView
 ***********************/

var ChequeRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplChequeRow),
    events: {
        'click .btn-imprimir': 'imprimir',
        'click .btn-enviar': 'enviar',
        'click .btn-pagar': 'abrirModal'
    },
    onShow: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    onRender: function () {
        this.verificaPermissaoBotaoQuitar();
    },
    verificaPermissaoBotaoQuitar: function () {
        //Permissão 11 Quitar cartão crédito
        //Funcionalidade 201 Recebimento
        if (this.options.permissao.length) {
            $(this.el).find('.btn-pagar').removeClass('hide');
        } else {
            $(this.el).find('.btn-pagar').addClass('hide');
        }
    },
    abrirModal: function () {

        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new ChequeQuitarView();
        view.model = this.model;

        var modal = new ModalLayout({view: view, label: 'Quitar cheque'});

        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.show(modal);
    }
});
/***********************
 *   imports
 **********************/

$.getScript('js/backbone/views/Recebimento/PesquisaRowView.js?_=' + new Date().getTime());
$.getScript('js/backbone/models/vw-cliente-venda.js');

var tplPesquisaRow = "  <td><%= id_venda %></td>\
                        <td><%= st_nomecompleto %></td>\
                        <td><%= st_cpf %></td>\
                        <td><%= st_evolucao %></td>\
                        <td>R$ <%= Number(nu_valorliquido).toFixed(2) %></td>\
                        <td>\
                        <% if(id_venda) { %>\
                        <button class='btn btn-success btn-edit' type='button' title='Ir para a venda nº <%= id_venda %>'><i class='icon icon-arrow-right icon-white'></i></button>\
                            <% } %>\
                        </td>";
/***********************
 *  ProjetoRowView
 *  Reponsavel por renderizar uma linha da PesquisaRowView
 ***********************/

var PesquisaRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplPesquisaRow),
    events: {
        'click .btn-edit': 'showForm'
    },
    showForm: function() {
        $.ajaxSetup({'async': false});
            $.getScript('/js/backbone/views/Recebimento/TabsView.js?id='+ new Date().getTime());
            G2S.layout.content.currentView.vwClienteVenda = this.model;
            G2S.layout.content.currentView.pesquisaRegion.reset();
            var view = new TabsView();
            view.model = this.model;
            G2S.layout.content.currentView.tabsRegion.show(view);
        $.ajaxSetup({'async': true});
    }
});

var tplPesquisaTable = "<div class='well'>\
                            <legend>Resultado da Busca</legend>\
                            <div class='table-responsive'>\
                                <table id='table-pesquisa' class='backgrid table table-bordered table-hover table-striped'>\
                                    <thead>\
                                        <tr>\
                                        <th>Venda</th>\
                                        <th>Cliente</th>\
                                        <th>CPF</th>\
                                        <th>Evolução</th>\
                                        <th>Valor (R$)</th>\
                                        <th>Ações</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody id='pesquisa-row-container'></tbody>\
                                </table>\
                            </div>\
                        </div>";
/***********************
 *  PesquisaTableView
 *  Reponsavel por renderizar a tabela PesquisaTableView
 ***********************/

var PesquisaTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplPesquisaTable),
    childView: PesquisaRowView,
    childViewContainer: "#pesquisa-row-container",
    initialize: function () {
        var Collection = Backbone.Collection.extend({
            model: VwClienteVenda,
            url: function () {
                return '/api/vw-cliente-venda';
            }
        });
        this.collection = new Collection();
        //this.collection.fetch();
    }
});
/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/DepositoTableView.js');

var tplDeposito = '';
$.get('/js/backbone/templates/recebimento/tpl-deposito.html?_=' + new Date().getTime(), '', function (data) {
    tplDeposito = data;
});

/***********************
 *   DepositoView
 **********************/

var DepositoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplDeposito);
    },
    events: {

    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new DepositoTableView();

        //MeioPagamento::DEPOSITO = 6
        tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val()+'&id_meiopagamento=6';
        tableView.collection.fetch();
        tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-deposito-list").html(tableView.render().$el);
    }
});
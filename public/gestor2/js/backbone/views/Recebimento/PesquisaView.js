/**
 * View para a funcionalidade de pesquisa de vendas
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/

$.getScript('js/backbone/models/evolucao.js');
$.getScript('js/backbone/models/vw-cliente-venda.js');
$.getScript('js/backbone/views/Util/SelectView.js');
$.getScript('js/backbone/views/Recebimento/PesquisaTableView.js?_=' + new Date().getTime());

var tplPesquisa = '';
$.get('/js/backbone/templates/recebimento/tpl-pesquisa.html?_=' + new Date().getTime(), '', function (data) {
    tplPesquisa = data;
});

var tplPesquisa = $('#container-pesquisa').html();

/***********************
 *   PesquisaView
 **********************/

var PesquisaView = Backbone.Marionette.ItemView.extend({
    initialize: function () {

        this.model = new VwClienteVenda();
        this.template = _.template(tplPesquisa);
    },
    events: {
        'submit form': 'search',
        'click #btn-clear': 'clearForm'
    },
    clearForm: function () {
        $('#form-pesquisa #id_venda').val('');
        $('#form-pesquisa #id_evolucao').val('');
        $('#form-pesquisa #st_nomecompleto').val('');
    },
    onShow: function () {
        this.renderEvolucaoSelect();
    },
    renderEvolucaoSelect: function () {

        var collection = new Evolucoes();
        collection.data = {st_tabela: 'tb_venda'}
        collection.fetch();

        var view = new SelectView({
            el: $('#form-pesquisa #id_evolucao'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_evolucao',
                optionValue: 'id_evolucao',
                optionSelected: 10 // Evolucao::CONFIRMADA
            }
        });

        view.render();
    },
    search: function (e) {
        $.ajaxSetup({'async': false});
            $.getScript('js/backbone/views/Recebimento/PesquisaTableView.js');
            e.preventDefault();
            //Renderiza a listagem de acordo com os criterios de pesquisa

            var tableView = new PesquisaTableView();
            tableView.collection.url = '/api/vw-cliente-venda?' + $('#form-pesquisa').serialize();
            tableView.collection.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                async: false
            });

            if (tableView.collection.length > 0) {
                $("#container-pesquisa-list").html(tableView.render().$el);
            } else {
                $("#container-pesquisa-list").html('');
                $.pnotify({'title': 'Atenção!', 'type': 'warning', 'text': 'Nenhuma venda encontrada.'});
            }
            return false;
        $.ajaxSetup({'async': true});
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/CartaoDebitoTableView.js');

var tplCartaoDebito = '';
$.get('/js/backbone/templates/recebimento/tpl-cartao-debito.html?_=' + new Date().getTime(), '', function (data) {
    tplCartaoDebito = data;
});

/***********************
 *   CartaoDebitoView
 **********************/

var CartaoDebitoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new VwResumoFinanceiro();
        this.template = _.template(tplCartaoDebito);
    },
    events: {
        'click .btn-pagar-cartao-debito': 'abrirModal'
    },
    onShow: function () {
        loading();
        this.renderList();
        loaded();
        this.showBtnPagar();
    },
    renderList: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        this.tableView = new CartaoDebitoTableView();

        //MeioPagamento::CARTAO_DEBITO = 7
        this.tableView.collection.url = '/recebimento/retornar-resumo-financeiro?id_venda=' + $('#id_venda').val() + '&id_meiopagamento=7';
        this.tableView.collection.fetch();
        this.tableView.setaPermissao(VerificarPerfilPermissao(11, 201));
        $("#container-cartao-debito-list").html(this.tableView.render().$el);
    },
    abrirModal: function () {

        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CartaoDebitoQuitarView();
        view.model = this.model;

        var modal = new ModalLayout({view: view, label: 'Quitar Cartão de Débito'});

        G2S.layout.content.currentView.tabsRegion.currentView.modalRegion.show(modal);
    },
    showBtnPagar: function () {
        _(this.tableView.collection.models).each(function (model, d) {
            //Verifica se não está quitado
            //Permissão 11 Quitar cartão crédito
            //Funcionalidade 201 Recebimento
            if (model.get('bl_quitado') == 0 && VerificarPerfilPermissao(11, 201).length) {
                $('.btn-pagar-cartao-debito').removeClass('hide');
            }
        });
    }
});
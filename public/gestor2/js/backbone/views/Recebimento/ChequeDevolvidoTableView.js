/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/Recebimento/ChequeDevolvidoRowView.js');

var tplChequeDevolvidoTable = '';
$.get('/js/backbone/templates/recebimento/tpl-cheque-devolvido-table.html?_=' + new Date().getTime(), '', function (data) {
    tplChequeDevolvidoTable = data;
});

/***********************
 *  BoletoTableView
 *  Reponsavel por renderizar a tabela BoletoTableView
 ***********************/

var ChequeDevolvidoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplChequeDevolvidoTable),
    childView: ChequeDevolvidoRowView,
   childViewwContainer: "#cheque-devolvido-row-container",
    initialize: function () {
        this.collection = new Backbone.Collection();
    }
});
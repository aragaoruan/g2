/**
 * Created by UNYLEYA on 10/09/14.
 */
var variaveis = '';
var temp = '';
var html = '';
var templates = {
    modalExtenderAlocacao: function () {
         html = '<div id="modalBasica" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="titulo_modal_basica" aria-hidden="true">'
            + '<div class="modal-header">'
            + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
            + '<h3 id="titulo_modal_basica"><%=titulo_modal%></h3>'
            + '</div>'
            + '<div class="modal-body">'
            + '<%=conteudo_modal_body%>'
            + '</div>'
            + '<div class="modal-footer">'
            + '<%=conteudo_modal_footer%>'
            + '</div>'
            + '</div>';

        return html;
    },
    botaoModal: function () {
         html = ' <a class="btn btExibeModal" href="#modalBasica" id="btExibeModal"'
            + 'data-toggle="modal" id="btExibeModal" name="btExibeModal">'
            + '<%=texto_botao%>'
            + '</a>';

        return html;
    }
}

var ModalBasicaView = Backbone.View.extend({
    el: '',
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        variaveis = this.model.toJSON();
        temp = _.template( _.template(templates.botaoModal(), variaveis));
        this.$el.html(temp);
        return this;
    },
    renderModal: function () {

        variaveis = this.model.toJSON();
        temp = _.template( _.template(templates.modalExtenderAlocacao(), variaveis));
        this.$el.html(temp);
        return this;
    },
    events: {
        'click #btExibeModal': 'modalShow'
    },
    modalShow: function () {
        $('#modalBasica').modal('show');
    }
});

//
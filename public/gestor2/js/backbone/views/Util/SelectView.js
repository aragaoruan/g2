/**
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @type @exp;Backbone@pro;Marionette@pro;Layout@call;extend
 */

var itemViewSelect = Marionette.ItemView.extend({
    tagName: 'option',
    initialize: function (options) {
        this.optionSelected = (options.optionSelected || 0 );
        this.optionValue = options.optionValue ? options.optionValue : 'id';
        this.template = _.template('<%= ' + options.optionLabel + ' %>');
    },
    onRender: function () {
        this.$el.attr('value', this.model.get(this.optionValue));
        if (this.optionSelected == this.model.get(this.optionValue)) {
            this.$el.attr('selected', 'selected');
        }
    }
});

var SelectView = Backbone.Marionette.CollectionView.extend({
    childView: itemViewSelect,
    initialize: function (options) {
        //verifica se foi passado um atributo na inicialização chamado sort para
        var sort = ((typeof options.sort == 'undefined') || options.sort == true ? true : false);
        //verifica se é para ordenar
        if (sort) {
            //atribui o label do select a variavel para ordenar
            var sortBy = options.childViewOptions.optionLabel;
            //ordena os valores
            this.collection.models.sort(function (a, b) {
                if (a.attributes[sortBy] < b.attributes[sortBy])
                    return -1;
                if (a.attributes[sortBy] > b.attributes[sortBy])
                    return 1;
                return 0;
            });
        }

        if (options.clearView) {
            this.$el.empty();
            this.$el.html('<option value="" >Selecione</option>');
        }

        return this;
    },
    onRender: function (options) {
        //create callbacks
    },
    onShow: function (options) {
        //create callbacks
    }
});
/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com>
 * @date 2015-05-24
 * @documentation https://bitbucket.org/unyleya/g2/wiki/Componente%20SideBySideView
 */

var SideBySideView = Marionette.LayoutView.extend({
    template: _.template('<div class="row-fluid"><div class="span6 list-left"></div><div class="span6 list-right"></div></div>'),
    // REGIOES DE CADA COLLECTION
    regions: {
        left: '.list-left',
        right: '.list-right'
    },
    // COLLECTIONS DAS REGIOES
    collections: {
        left: null,
        right: null
    },
    initialize: function (options) {
        this.options = options || {};
        this.collections.left = options.collections.left;
        this.collections.right = options.collections.right || new Backbone.Collection();
        return this;
    },
    onRender: function () {
        var that = this;

        var LeftView, RightView, EmptyView, LeftComposite, RightComposite;

        // ITEM VIEW NOT ADDED
        LeftView = Marionette.ItemView.extend({
            tagName: 'tr',
            onBeforeRender: function () {
                if (typeof this.model.get('editable') == 'undefined' || this.model.get('editable') == true) {
                    this.template = _.template('<td>' + this.model.get(that.options.label) + '</td><td><button class="btn btn-success btn-small" type="button" title="Adicionar"><i class="icon-white icon-arrow-right"></i></button></td>');
                }else{
                    this.template = _.template('<td>' + this.model.get(that.options.label) + '</td><td><button class="btn btn-success btn-small" disabled type="button" title="Adicionar"><i class="icon-white icon-arrow-right"></i></button></td>');
                }
            },
            onRender: function () {
                this.$el.attr('data-label', this.model.get(that.options.label).trim().removeAccents().toLowerCase())
            },
            events: {
                'click .btn-success': 'addItem'
            },
            addItem: function (e) {
                e.preventDefault();
                that.collections.right.add(this.model);
                that.collections.left.remove(this.model);
            }
        });

        // ITEM VIEW
        RightView = Marionette.ItemView.extend({
            tagName: 'tr',
            onBeforeRender: function () {
                if (typeof this.model.get('editable') == 'undefined' || this.model.get('editable') == true) {
                    this.template = _.template('<td><button class="btn btn-danger btn-small" type="button" title="Remover"><i class="icon-white icon-arrow-left"></i></button></td><td>' + this.model.get(that.options.label) + '</td>');
                }else{
                    this.template = _.template('<td><button class="btn btn-danger btn-small" disabled type="button" title="Remover"><i class="icon-white icon-arrow-left"></i></button></td><td>' + this.model.get(that.options.label) + '</td>');
                }

            },
            onRender: function () {
                this.$el.attr('data-label', this.model.get(that.options.label).trim().removeAccents().toLowerCase());
            },
            events: {
                'click .btn-danger': 'removeItem'
            },
            removeItem: function (e) {
                e.preventDefault(e);
                var self = this;
                //inicia a varivel de remove como false
                var remove = false;

                //verifica se o atributo do componente confirmRemove foi passado e é true
                if (typeof that.options.confirmRemove != 'undefined' && that.options.confirmRemove == true) {

                    //se o atributo for passado e for == true ele solicita a confirmação antes de remover o registro
                    bootbox.confirm("Deseja realmente remover este item?", function (result) {
                        if (result) {
                            that.collections.left.add(self.model);
                            that.collections.right.remove(self.model);
                        }
                    });
                    /*
                     *se ele for false ou não tiver sido passado ele automaticamente entende que não deve ter confirmação
                     */
                } else {
                    that.collections.left.add(this.model);
                    that.collections.right.remove(this.model);
                }
            }
        });

        // EMPTY ITEM VIEW
        EmptyView = Marionette.ItemView.extend({
            template: _.template('<td colspan="2">Nenhum registro.</td>'),
            tagName: 'tr'
        });

        // COMPOSITE VIEW
        LeftComposite = Marionette.CompositeView.extend({
            collection: that.collections.left,
            childView: LeftView,
            emptyView: EmptyView,
            childViewContainer: 'tbody',
            events: {
                'keyup input': 'fetchList',
                'click .addAll': 'addAll'
            },
            searchRequest: null,
            searchString: null,
            onBeforeRender: function () {
                if (typeof that.options.addAll == 'undefined' || that.options.addAll == true) {
                    this.template = _.template('<div class="row-fluid"><input class="span12" type="text" placeholder="Pesquisar registro" title="Digite no mínimo 3 caracteres para realizar a pesquisa"/></div><div style="max-height: 210px; overflow: auto;"><table class="table table-bordered table-hover table-striped backgrid" style="margin: 0;"><thead><tr><th>Descrição</th><th style="width: 30px;">' + (that.options.addAll ? '<button class="btn btn-success btn-small addAll" type="button" title="Adicionar todos os itens"><i class="icon-white icon-arrow-right"></i></button>' : 'Ação') + '</th></tr></thead><tbody></tbody></table></div>');
                }else{
                    this.template = _.template('<div class="row-fluid"><input class="span12" type="text" placeholder="Pesquisar registro" title="Digite no mínimo 3 caracteres para realizar a pesquisa"/></div><div style="max-height: 210px; overflow: auto;"><table class="table table-bordered table-hover table-striped backgrid" style="margin: 0;"><thead><tr><th>Descrição</th><th style="width: 30px;">' + (that.options.addAll ? '<button class="btn btn-success btn-small addAll" disabled type="button" title="Adicionar todos os itens"><i class="icon-white icon-arrow-right"></i></button>' : 'Ação') + '</th></tr></thead><tbody></tbody></table></div>');
                }
            },
            addAll: function () {
                that.collections.right.add(this.collection.models);
                that.collections.left.reset();
            },
            fetchList: function (e) {
                var value = $(e.target).val().trim();
                var childs = this.$el.find('tbody tr');


                if (!that.options.fetch) {
                    // Se parametro fetch for false, fazer a busca somente filtrando os elementos já na collection

                    if (that.options.globalSearch)
                        childs = that.$el.find('tbody tr');

                    if (value)
                        childs.hide().filter('[data-label*="' + value.removeAccents().toLowerCase() + '"]').show();
                    else
                        childs.show();

                } else if (value.length >= 3) {
                    // Se parametro fetch for true, fazer a busca utilizando o fetch da collection

                    if (that.options.globalSearch)
                        childs = that.$el.find('tbody.direita tr');

                    if (this.searchString == value)
                        return;

                    var params = {};
                    params[that.options.label] = value;

                    if (this.searchRequest)
                        this.searchRequest.abort();

                    this.searchString = value;
                    this.searchRequest = that.collections.left.fetch({
                        data: params,
                        reset: true,
                        success: function () {
                            var toRemove = [];

                            if (that.options.idAttributeGlobal) {
                                that.collections.right.each(function (modelR) {
                                    var filter = {};
                                    filter[that.options.idAttributeGlobal] = modelR.get(that.options.idAttributeGlobal);

                                    var first_model = that.collections.left.findWhere(filter);

                                    if (first_model) {
                                        toRemove.push(first_model);
                                    }
                                });
                            } else {
                                toRemove = that.collections.right.models;
                            }

                            that.collections.left.remove(toRemove);

                        }

                        /*that.collections.right.filter(function (child) {
                         if (child.get(that.options.label).toLowerCase().indexOf(value) != -1) {
                         filter = true;
                         childs.hide().filter('[data-label*="' + child.get(that.options.label).trim().removeAccents().toLowerCase() + '"]').show();
                         console.log(childs.filter('[data-label*="' + child.get(that.options.label).trim().removeAccents().toLowerCase() + '"]'));
                         }
                         });

                         if (!filter) {
                         childs.show();
                         }*/
                    });
                }
            }
        });

        RightComposite = Marionette.CompositeView.extend({
            collection: that.collections.right,
            childView: RightView,
            emptyView: EmptyView,
            childViewContainer: 'tbody',
            events: {
                'click .removeAll': 'removeAll'
            },
            onBeforeRender: function () {
                if (typeof that.options.removeAll == 'undefined' || that.options.removeAll == true) {
                    this.template = _.template('<div class="row-fluid"><legend style="line-height: 20px;">Adicionados</legend></div><div style="max-height: 210px; overflow: auto;"><table class="table table-bordered table-hover table-striped backgrid" style="margin: 0;"><thead><tr><th style="width: 30px;">' + (that.options.addAll ? '<button class="btn btn-danger btn-small removeAll" type="button" title="Remover todos os itens"><i class="icon-white icon-arrow-left"></i></button>' : 'Ação') + '</th><th>Descrição</th></tr></thead><tbody class="direita"></tbody></table></div>');
                }else{
                    this.template = _.template('<div class="row-fluid"><legend style="line-height: 20px;">Adicionados</legend></div><div style="max-height: 210px; overflow: auto;"><table class="table table-bordered table-hover table-striped backgrid" style="margin: 0;"><thead><tr><th style="width: 30px;">' + (that.options.addAll ? '<button class="btn btn-danger btn-small removeAll" disabled type="button" title="Remover todos os itens"><i class="icon-white icon-arrow-left"></i></button>' : 'Ação') + '</th><th>Descrição</th></tr></thead><tbody class="direita"></tbody></table></div>')
                }
            },
            removeAll: function () {
                that.collections.left.add(this.collection.models);
                that.collections.right.remove(that.collections.right.models);
            }
        });

        var leftIni = new LeftComposite();
        var rightIni = new RightComposite();

        // RENDER TABLES
        this.left.show(leftIni);
        this.right.show(rightIni);
    }
});

/**
 * Layout Modal
 */
var ModalLayout = Backbone.Marionette.LayoutView.extend({
    initialize: function (options) {
        this.view = options.view;
        this.label = options.label;
    },
    template: _.template('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>' +
        '   <h4 class="modal-title" id="myModalLabel">Modal title</h4>' +
        '</div>' +
        '<div class="modal-body" id="myModalBody">' +
        'Corpo' +
        '</div>' +
        '    <div class="modal-footer">' +
        //'   <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>' +
        //'   <button type="button" class="btn btn-primary">Save changes</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>'),

    ui: {
        id: "#myModal",
        label: "#myModalLabel"
    },
    regions: {
        body: "#myModalBody"
    },
    onShow: function () {
        this.ui.label.html(this.label);
        if (this.view) {
            this.body.show(this.view);
            this.showModal();
        }
    },
    showModal: function () {
        this.ui.id.modal('show');
    },
    hideModal: function () {
        this.ui.id.modal('hide');
    }
});
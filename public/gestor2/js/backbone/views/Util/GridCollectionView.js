var GridCollectionView = Marionette.CompositeView.extend({
    groups: [],
    groupAttribute: "id_group",
    initialize: function (options) {
        var that = this;

        $.each(options, function (i, option) {
            that[i] = option;
        });

        this.childViewOptions.collectionSelected.previousModels = _.clone(this.childViewOptions.collectionSelected.models);

        this.groups = this.groups || [];

        // Usado SIZE do UNDERSCORE para retornar mesmo se for ARRAY ou OBJETO
        var length = this.groups.length || _.size(this.groups) || 0;
        var width = (100 - length) / length;

        var template = "";

        for (var i in this.groups) {
            var group = this.groups[i];
            template += '<div id="col' + i + '" style="float: left; margin: 0 0.5%; width: ' + width + '%;"><h5 class="text-center" style="background-color: #ddd; padding: 5px;">' + group + '</h5></div>';
        }

        this.template = _.template(template);
    },
    attachHtml: function (collectionView, childView) {
        var group = childView.model.get(this.groupAttribute);

        this.getChildViewContainer(this).find("#col" + group).append(childView.$el);
    },
    childView: Marionette.ItemView.extend({
        template: "#template-grade-item",
        ui: {
            input: "input"
        },
        modelEvents: {
            change: "render",
            toggleCollection: "toggleCollection",
            calculate: "calculate"
        },
        events: {
            "change @ui.input": "aplicaCreditos"
        },
        initialize: function (options) {
            var that = this;

            $.each(options, function (i, option) {
                if (i !== "model") {
                    that[i] = option;
                }
            });

            this.model.set("disabled", false);
            this.model.set("checked", false);

            var color;

            switch (+this.model.get("id_evolucao")) {
                // AMARELO (NAO CURSADO)
                case 0:
                case EVOLUCAO.TB_MATRICULA_DISCIPLINA.NAO_ALOCADO:
                    color = "#F9A825";
                    break;
                // AZUL (APROVADO)
                case EVOLUCAO.TB_MATRICULA_DISCIPLINA.APROVADO:
                    color = "#1565C0";
                    break;
                // VERDE (CURSANDO)
                case EVOLUCAO.TB_MATRICULA_DISCIPLINA.CURSANDO:
                    color = "#2E7D32";
                    break;
                // VERMELHO (INSATISFATORIO)
                case EVOLUCAO.TB_MATRICULA_DISCIPLINA.INSATISFATORIO:
                    color = "#C62828";
                    break;
                // LARANJA (TRANCADO)
                case EVOLUCAO.TB_MATRICULA_DISCIPLINA.TRANCADA:
                    color = "#ff8a0d";
                    break;
                default:
                    color = "none";
            }

            this.model.set("color", color);

            this.toggleCollection();
        },
        onBeforeRender: function () {
            var model = this.model;

            // Se a evolução for APROVADA ou CURSANDO
            // Não é permitido selecioná-la
            if (+model.get("id_evolucao") === 12 || +model.get("id_evolucao") === 13) {
                model.set("checked", false);
                model.set("disabled", true);
                this.aplicarAtributos();
                return;
            }

            // Verificar se alguma sala da MESMA DISCIPLINA já foi selecionada
            var added = this.collectionSelected.models.find(function (item) {
                return +item.get("id_disciplina") === +model.get("id_disciplina");
            });

            // Se a disciplina for Obrigatória e não tiver adicionado nenhuma sala da mesma disciplina, então adicionar
            if (model.get("bl_obrigatorio")) {
                if (!added) {
                    this.collectionSelected.add(model);
                    added = model;
                }

                added.set("bl_obrigatorio", true);
            }

            // Verificar se a sala atual já está adicionada às "Salas Selecionadas"
            var selected = this.collectionSelected.models.find(function (item) {
                return +item.get("id_disciplina") === +model.get("id_disciplina") &&
                    +item.get("id_saladeaula") === +model.get("id_saladeaula");
            });

            // Se estiver adicionada, então marca a sala como SELECIONADA
            if (selected) {
                model.set("checked", true);
                model.set("disabled", false);
            }

            // Se já possui uma disciplina igual a atual já selecionada, então não permitir selecionar novamente
            if (added && +added.get("id_saladeaula") !== +model.get("id_saladeaula")) {
                model.set("checked", false);
                model.set("disabled", true);
            }

            // Preparar atributos
            this.aplicarAtributos();
        },
        aplicarAtributos: function () {
            var attrs = {
                label: "Selecionar",
                btnClass: "btn-primary"
            };

            var checked = this.model.get("checked");
            var disabled = this.model.get("disabled");

            if (disabled) {
                attrs.label = "Disc. já selecionada";
            }

            if (checked) {
                attrs.label = "Selecionada";
                attrs.btnClass = "btn-success active";

                if (this.model.get("bl_obrigatorio")) {
                    attrs.label = "Disciplina obrigatória";
                }
            }

            this.model.set(attrs);
        },
        toggleCollection: function () {
            var that = this;

            var checked = this.model.get("checked");

            $.each(this.collectionsToCheck, function (i, collection) {
                collection.each(function (model) {
                    if (
                        +model.get("id_disciplina") === +that.model.get("id_disciplina") &&
                        +model.get("id_saladeaula") !== +that.model.get("id_saladeaula")
                    ) {
                        model.set("disabled", !!checked);
                    }
                });
            });
        },
        aplicaCreditos: function () {
            var that = this;

            var $input = this.ui.input;
            var checked = $input.prop("checked");
            var do_fetch = true;
            var msgs = [];

            // Adicionar / Remover da collection
            if (checked) {
                if (!this.collectionSelected.models.find(function (item) {
                        return +item.get("id_saladeaula") === +that.model.get("id_saladeaula");
                    })) {
                    this.collectionSelected.add(this.model);
                }
            } else {
                this.collectionSelected.remove(
                    this.collectionSelected.models.find(function (item) {
                        return +item.get("id_saladeaula") === +that.model.get("id_saladeaula");
                    })
                );
            }

            // Se a disciplina estiver sendo SELECIONADA
            if (checked) {
                var countPeriodo = this.collectionSelected.models.filter(function (item) {
                    return +item.get("id_periodoletivo") === +that.model.get("id_periodoletivo");
                }).length;

                // GII-7046
                // Não permitir selecionar mais de duas disciplinas para a mesma oferta
                if (countPeriodo > 2) {
                    checked = false;
                    do_fetch = false;
                    msgs.push("Não é possível selecionar mais de duas disciplinas na mesma oferta.");
                }

                // GII-7046
                // Quando o aluno selecionar duas disciplinas na mesma oferta o sistema deve exibir uma mensagem
                else if (+countPeriodo === 2) {
                    msgs.push("O aluno cursará duas disciplinas no mesmo período de tempo.");
                }
            }

            // Se a disciplina estiver sendo DESMARCADA
            else {
                // Não permitir DESMARCAR disciplinas com a opção "Obrigatório"
                if (this.model.get("bl_obrigatorio")) {
                    checked = true;
                    do_fetch = false;
                    msgs.push(
                        "De acordo com o cronograma acadêmico a disciplina \"" +
                        this.model.get("st_disciplina") +
                        "\" é obrigatória nesse período."
                    );
                }
            }

            // Mostrar mensagens
            if (msgs.length) {
                msgs.forEach(function (msg) {
                    $.pnotify({
                        type: "warning",
                        title: "Aviso",
                        text: msg
                    });
                });
            }

            // Adicionar / Remover da collection
            if (checked) {
                if (!this.collectionSelected.models.find(function (item) {
                        return +item.get("id_saladeaula") === +that.model.get("id_saladeaula");
                    })) {
                    this.collectionSelected.add(this.model);
                }
            } else {
                this.collectionSelected.remove(
                    this.collectionSelected.models.find(function (item) {
                        return +item.get("id_saladeaula") === +that.model.get("id_saladeaula");
                    })
                );
            }

            // Seta o atributo final, apos validação
            this.model.set("checked", checked);

            if (!do_fetch) {
                that.render();
                that.selectedCountElem.html(that.collectionSelected.length);
                that.toggleCollection();
            } else {
                this.calculate();
            }
        },
        calculate: function () {
            var that = this;

            var ar_disciplinas = this.collectionSelected.map(function (model) {
                return model.get("id_disciplina");
            });

            // Calcular valores
            this.gridModel.fetch({
                data: $.extend({id_disciplina: ar_disciplinas}, this.fetchData),
                beforeSend: loading,
                error: function (model, response) {
                    that.collectionSelected.remove(
                        that.collectionSelected.models.find(function (item) {
                            return +item.get("id_saladeaula") === +that.model.get("id_saladeaula");
                        })
                    );

                    $.pnotify(response.responseText ? JSON.parse(response.responseText) :
                        {
                            type: "error",
                            title: "Erro",
                            text: "Houve um erro ao realizar o procedimento."
                        });

                    that.model.set("checked", false);
                },
                complete: function () {
                    that.render();
                    that.selectedCountElem.html(that.collectionSelected.length);
                    that.toggleCollection();
                    loaded();
                }
            });
        }
    }),
    onRender: function () {
        // Se tiver alguma já selecionada, então recalcular tudo
        if (this.childViewOptions.collectionSelected.length) {
            this.childViewOptions.collectionSelected.at(0).trigger("calculate");
        }
    }
});
/**
 * View para a funcionalidade de envio de vendas para o fluxus
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('modelo-backbone/Venda');

var tplEnvioVenda = '';
$.get('/js/backbone/templates/envio-venda/tpl-envio-venda.html?_=' + new Date().getTime(), '', function(data) {
    tplEnvioVenda = data;
});

/***********************
 *   RegraPagamentoEntidadeView
 **********************/

var EnvioVendaView = Backbone.Marionette.ItemView.extend({
    initialize: function() {
        this.model = new Venda();
        this.template = _.template(tplEnvioVenda);
    },
    events: {
        'click #btn-enviar': 'enviarVenda'
    },
    enviarVenda: function() {
        $.ajaxSetup({'async': false});
        loading();
        var that = this;
        if ($('#id_venda').val()) {
            $.get('envio-venda/enviar/id_venda/' + $('#id_venda').val(), '', function(data) {
                $.pnotify(data);
                if (data.type == 'success') {
                    that.clearForm();
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                $.pnotify($.parseJSON(xhr.responseText));
            });
        } else {
            $.pnotify({
                'type': 'warning',
                'text': 'O código da venda precisa ser informado.',
                'title': ''
            });
        }
        loaded();
    },
    clearForm: function() {
        $('#id_venda').val('');
        this.model = new Venda();
    },
    onShow: function() {
        this.mask();
    },
    mask: function() {
        $('#id_venda').mask('?9999999999');
    }
});

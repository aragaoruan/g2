/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/LancarFrequencia/AlunosFrequenciaRowView.js');
$.getScript('js/CollectionView.js');
$.getScript('/modelo-backbone/VwAvaliacaoAgendamento');




/***********************
 *  AlunosTableView
 *  Reponsavel por renderizar a tabela com a lista de alunos para lançar presença
 ***********************/

var EmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    className: 'alert alert-warning',
    template: _.template('<td colspan="10" style="text-align: center">Nenhum registro encontrado</td>')
});

var AlunosFrequenciaTableView  = Marionette.CompositeView.extend({
    template: '#table-alunos',
    childView: AlunosFrequenciaRowView,
    childViewContainer: '#alunos-row-container',
    emptyView: EmptyView,
    onRender: function() {

        this.collection.fetch({
            beforeSend: function(){
                loading();
            },
            complete: function(){
                loaded();
            }
        });
    }
});




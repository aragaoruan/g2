/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/LancarFrequencia/PesquisaAlunosFrequenciaView.js');

var tpl = '';
$.get('/js/backbone/templates/lancar-frequencia/tpl.html?_=' + new Date().getTime(), '', function(data) {
    tpl = data;
});
/***********************
 *   LancarFrequenciaView
 **********************/

var LancarFrequenciaView = Backbone.Marionette.LayoutView.extend({
    initialize: function() {

        this.template = _.template(tpl);
    },
    regions: {
        'pesquisaRegion': '#container-pesquisa',
        'listaAlunosRegion': '#container-lista-alunos'
    },
    renderPesquisaAlunosFrequenciaView: function() {
        this.pesquisaRegion.show(new PesquisaAlunosFrequenciaView());
    },
    onShow: function() {
        this.renderPesquisaAlunosFrequenciaView();
    }
});
var bl_editarlancamento = false;
VerificarPerfilPermissao(32, 749, true).done(function (response) {
    if ((response && response.length)) {
        bl_editarlancamento = true;
    }
});

/***********************
 *   imports
 **********************/
$.getScript('/modelo-backbone/AvaliacaoAgendamento');

var tplRowAlunos = '';
$.get('/js/backbone/templates/lancar-frequencia/tpl-row-alunos.html?_=' + new Date().getTime(), '', function (data) {
    tplRowAlunos = data;
});

/***********************
 *  AlunosFrequenciaRowView
 *  Reponsavel por renderizar uma linha da AlunosFrequenciaTableView
 ***********************/

var AlunosFrequenciaRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplRowAlunos),
    ui: {
        nuPresenca:   '#nu_presenca',
        btnHistorico: '.historico',
        btnEditar:    '.editarlancamento',
        containerModal : '#container-modal-historico-lancamento'
    },
    onRender: function () {
        var that = this;
        that.ui.btnEditar.popover();
        that.ui.btnHistorico.popover();
        if ( parseInt(this.model.get('nu_presenca')) === 0 || parseInt(this.model.get('nu_presenca')) === 1) {
            that.ui.nuPresenca.attr('disabled', true);
        }
        if(bl_editarlancamento){
            that.ui.btnEditar.removeAttr('disabled');
        }
    },
    events: {
        'change #nu_presenca': 'lancarPresenca',
        'click @ui.btnEditar' : 'changeEdit',
        'click @ui.btnHistorico' : 'modalHistorico'
    },
    changeEdit: function () {
        var that = this;
        if(bl_editarlancamento){
            that.ui.nuPresenca.removeAttr('disabled');
            that.ui.nuPresenca.focus();
        }
    },
    lancarPresenca: function () {
        if (this.ui.nuPresenca.val() != '') {
            var that = this;
            var param = {
                'nu_presenca': this.ui.nuPresenca.val()
            };
            if (param.nu_presenca) {
                this.model.url = '/api/lancar-frequencia';
                var save = this.model.save(param, {
                    beforeSend: loading,
                    success: function (model, reponse) {
                        $.pnotify(reponse);

                        that.model.set('nu_presenca', param.nu_presenca);
                        that.model.set('st_usuariolancamento', G2S.loggedUser.get('nomeUsuario'));
                        that.render();
                    },
                    error: function (model, reponse) {
                        console.log(reponse);
                        $.pnotify(reponse);
                    },
                    complete: loaded
                });
            }
        } else {
            $.pnotify({title: 'Alerta', text: 'Alteração descartada.', type: 'alert'});
        }

    },
    modalHistorico: function () {
        var that = this;
        that.ui.containerModal.empty();
        var modalTramiteLancamento = $(
            '<div class="modal hide fade" id="modal-historico-lancamento" tabindex="-1" role="dialog" aria-labelledby="Histórico de Alteração" aria-hidden="false">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                            '<h4 class="modal-title">Histórico de lançamentos:</h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div id="container-grid-historico-lancamento" class="control-group">' +
                            '</div>' +
                        '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );

        that.ui.containerModal.append(modalTramiteLancamento);

        $('.modal .hide').show();
        $("#modal-historico-lancamento").modal('show');

        var modelTramites = Backbone.Model.extend({});
        var DataGridTramites = Backbone.PageableCollection.extend({
            model: modelTramites,
            state: { pageSize: 15},
            mode: "client"
        });

        var urlTramites = '/lancar-frequencia/retorna-tramites';
        var dataResponseTramites;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: urlTramites,
            data: {id_avaliacaoagendamento: this.model.get('id_avaliacaoagendamento')},
            success: function(data) {
                dataResponseTramites = data;
            },
            complete: function() {
                var dataGridTramite = new DataGridTramites(dataResponseTramites);
                var columnsTramite = [
                     { name: "st_nomecompleto", label: "Nome", editable: false, cell: "string"}
                    ,{ name: "dt_cadastro", label: "Data", editable: false, cell: "string"}
                    ,{ name: "st_situacao", label: "Situação", editable: false,  cell: "string"}
                ];
                $("#container-grid-historico-lancamento").empty();
                if (dataGridTramite.length > 0) {
                    var pageableGridTramite = new Backgrid.Grid({
                        columns: columnsTramite,
                        collection: dataGridTramite,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover'
                    });
                    $("#container-grid-historico-lancamento").append(pageableGridTramite.render().$el);
                   /* var paginatorTramites = new Backgrid.Extension.Paginator({
                        collection: dataGridTramite
                    });
                    $("#container-grid-historico-lancamento").append(paginatorTramites.render().$el);*/
                } else {
                    $("#container-grid-historico-lancamento").html('Nenhum trâmite cadastrado para o lançamento de frequência!');
                }
                loaded();
            }
        });
    }
});

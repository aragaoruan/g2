/**
 * Created by UNYLEYA on 13/10/14.
 */


/***********************
 *   imports
 **********************/

$.getScript('js/backbone/models/PesquisaAlunoFrequencia.js');
$.getScript('/modelo-backbone/Uf');
$.getScript('/modelo-backbone/Municipio');
$.getScript('/modelo-backbone/AplicadorProva');
$.getScript('/modelo-backbone/VwAvaliacaoAgendamento');
$.getScript('/modelo-backbone/AvaliacaoAgendamento');
$.getScript('js/backbone/views/LancarFrequencia/AlunosFrequenciaTableView.js');


//var tplTableAlunos = '';
//$.get('/js/backbone/templates/lancar-frequencia/tpl-table-alunos.html?_=' + new Date().getTime(), '', function (data) {
//    tplTableAlunos = data;
//});
var formPesquisa = '';
$.get('/js/backbone/templates/lancar-frequencia/form-pesquisa-alunos-frequencia.html?_=' + new Date().getTime(), '', function (data) {
    formPesquisa = data;
});


var VwAvaliacaoAgendamentoCollection = Backbone.Collection.extend({
    model: VwAvaliacaoAgendamento
});

/***********************
 *   PesquisaAlunosFrequenciaView
 **********************/


var PesquisaAlunosFrequenciaView = Backbone.Marionette.ItemView.extend({
    template: _.template(formPesquisa),
    ui: {
        formSearch: '.form-search',
        formInputNome: '#st_nomecompleto',
        formInputUf: '#sg_uf',
        formInputMunicipio: '#id_municipio',
        formInputAplicador: '#id_aplicadorprova',
        formInputData: '#dt_aplicacao',
        btnPrint: '#botao-print'
    },
    initialize: function () {
        this.model = new PesquisaAlunoFrequencia();
    },
    events: {
        'submit @ui.formSearch': 'renderListagem',
        'click @ui.btnPrint': 'imprimirLista',
        'change #sg_uf': function () {
            this.renderMunicipioSelect($('#formPesquisaAlunosAplicacao #sg_uf').val());
            $('#formPesquisaAlunosAplicacao #id_aplicadorprova').html('<option></option>');
        },
        'change #id_municipio': function () {
            this.renderAplicadorSelect($('#formPesquisaAlunosAplicacao #sg_uf').val(), $('#formPesquisaAlunosAplicacao #id_municipio').val());
        }
    },
    onShow: function () {
        this.renderUfSelect();
    },
    renderUfSelect: function () {
        var selected = '';

        var UfCollection = Backbone.Collection.extend({
            model: Uf
        });

        var collection = new UfCollection();
        collection.url = '/Util/retorna-uf/';
        collection.fetch();

        var view = new SelectView({
            el: $('#formPesquisaAlunosAplicacao #sg_uf'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_uf',
                optionValue: 'sg_uf',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderMunicipioSelect: function (uf) {

        if (uf != '') {

            var elem = $('#formPesquisaAlunosAplicacao #id_municipio');

            var MunicipioCollection = Backbone.Collection.extend({
                model: Municipio
            });

            var collection = new MunicipioCollection();
            collection.url = '/Util/retorna-municipio-agendamento/sg_uf/' + uf;
            elem.html('');

            var view = new SelectView({
                el: elem,
                collection: collection,
                childViewOptions: {
                    optionLabel: 'st_municipio',
                    optionValue: 'id_municipio'
                }
            });

            view.render();
            collection.fetch().done(function () {
                elem.trigger('change');
            });

        } else {
            $('#formPesquisaAlunosAplicacao #id_municipio').html('<option></option>');
        }
    },
    renderAplicadorSelect: function (uf, municipio) {

        if (uf != '') {
            $('#formPesquisaAlunosAplicacao #id_aplicadorprova').html('<option value="">[Selecione]</option>');

            var AplicadorProvaCollection = Backbone.Collection.extend({
                model: AplicadorProva
            });

            var collection = new AplicadorProvaCollection();
            collection.url = '/aplicacao-avaliacao/retorna-aplicador-entidade-aplicacao/sg_uf/' + uf + '/id_municipio/' + municipio + '/id_entidade/' + G2S.loggedUser.get('id_entidade');
            collection.fetch({
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                }
            });

            var view = new SelectView({
                el: $('#formPesquisaAlunosAplicacao #id_aplicadorprova'),
                collection: collection,
                childViewOptions: {
                    optionLabel: 'st_aplicadorprova',
                    optionValue: 'id_aplicadorprova'
                }
            });

            view.render();
        } else {
            $('#formPesquisaAlunosAplicacao #id_aplicadorprova').html('<option></option>');
        }
    },
    validateSearch: function () {
        var is_valid = true;

        if (!this.ui.formInputNome.val() && !this.ui.formInputAplicador.val()) {
            $.pnotify({
                type: 'warning',
                //title: 'Campos obrigatórios',
                text: 'Preencha o nome do aluno ou UF, município e aplicador.'
            });
            is_valid = false;
        }

        return is_valid;
    },
    renderListagem: function (e) {
        e.preventDefault();

        if (!this.validateSearch()) {
            return false;
        }

        var $dados = $('#formPesquisaAlunosAplicacao').serialize();
        var url = '/default/lancar-frequencia/pesquisa-alunos-frequencia?' + $dados;

        this.ui.btnPrint.attr('href', url + '&print=true').removeClass('hidden');

        var collectionAlunos = new VwAvaliacaoAgendamentoCollection();
        collectionAlunos.url = url;
        $('#alunos-row-container').html('');

        var view = new AlunosFrequenciaTableView({
            collection: collectionAlunos,
            el: '#table-alunos'
        });
        view.render();

        $('#table-alunos').removeClass('hide');
    },
    imprimirLista: function () {
        var tplFormImprimir = '';
        $.get('/js/backbone/templates/lancar-frequencia/imprimir-frequencia.phtml?_=' + new Date().getTime(), '', function (data) {
            tplFormImprimir = data;
        });

    }
});




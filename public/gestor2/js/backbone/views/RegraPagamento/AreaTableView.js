/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/RegraPagamento/AreaRowView.js');
$.getScript('js/backbone/models/VwRegraPagamento.js');

var tplTableArea = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-table-area.html?_=' + new Date().getTime(), '', function(data) {
    tplTableArea = data;
});

/***********************
 *  AreaTableView
 *  Reponsavel por renderizar a tabela AreaTableView
 ***********************/

var AreaTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTableArea),
    childView: AreaRowView,
    childViewContainer: "#area-row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: VwRegraPagamento,
            url: function() {
                return 'regra-pagamento/retornar-regra-area-conhecimento';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});
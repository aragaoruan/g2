/***********************
 *   imports
 **********************/

var tplCargaHoraria = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-carga-horaria.html?_=' + new Date().getTime(), '', function (data) {
    tplCargaHoraria = data;
});

/***********************
 *   CargaHorariaView
 **********************/

var CargaHorariaView = Backbone.Marionette.ItemView.extend({
    initialize: function (opt) {
        if (opt && typeof opt.saveCallback !== 'undefined') {
            this.saveCallback = opt.saveCallback;
        }
        this.model = new CargaHoraria();
        this.template = _.template(tplCargaHoraria);
    },
    events: {
        'click #btn-save': 'actionSave',
        'submit #form-carga-horaria': 'actionSave',
        'click #btn-cancelar': 'fecharModal'
    },
    onShow: function () {
        $('#nu_cargahoraria').mask("?999");
    },
    validate: function () {
        if (!$('#form-carga-horaria #nu_cargahoraria').val()) {
            $.pnotify({
                'type': 'warning',
                'title': 'Dados incorretos',
                'text': 'O campo Carga Horária precisa ser preenchido.'
            });
            return false;
        }
        return true;
    },
    actionSave: function () {
        this.model.set({'nu_cargahoraria': $('#form-carga-horaria #nu_cargahoraria').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(that.model.toJSON(), {
                success: function (model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Carga Horária salva com sucesso.'});
                    that.saveCallback();
                    comboCaches.id_cargahoraria = null;
                    carregaComboBox('id_cargahoraria');
                },
                error: function (model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar Carga Horária.'});
                }
            });
        }
        return false;
    },
    fecharModal: function () {
        G2S.layout.content.currentView.modalRegion.currentView.hideModal();
        G2S.layout.content.currentView.modalRegion.reset();
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/RegraPagamento/ProfessorRowView.js');
$.getScript('js/backbone/models/VwRegraPagamento.js');

var tplTableProfessor = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-table-professor.html?_=' + new Date().getTime(), '', function(data) {
    tplTableProfessor = data;
});

/***********************
 *  ProfessorTableView
 *  Reponsavel por renderizar a tabela ProfessorTableView
 ***********************/

var ProfessorTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTableProfessor),
    childView: ProfessorRowView,
    childViewContainer: "#professor-row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: VwRegraPagamento,
            url: function() {
                return 'regra-pagamento/retornar-regra-professor';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});
/**
 * View para a funcionalidade de cadastro de regras de pagamento para o professor
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/RegraPagamento.js');
$.getScript('js/backbone/models/TipoDisciplina.js');
$.getScript('js/backbone/models/CargaHoraria.js');
$.getScript('js/backbone/models/VwProfessor.js');
$.getScript('js/backbone/views/RegraPagamento/ProfessorTableView.js');

var tplFormProfessor = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-professor.html?_=' + new Date().getTime(), '', function(data) {
    tplFormProfessor = data;
});

/***********************
 *   RegraPagamentoProfessorView
 **********************/

var RegraPagamentoProfessorView = Backbone.Marionette.ItemView.extend({
    initialize: function() {
        this.model = new RegraPagamento();
        this.template = _.template(tplFormProfessor);
    },
    events: {
        'click #btn-save-professor': 'save',
        'click #btn-new-professor': 'clearForm',
        'click #btn-search-professor': 'search',
        'click #btn-clear-professor': 'clearSearch',
        'click #btn-carga-horaria': 'openModal',
        'submit #form-professor': function() {
            return false;
        }
    },
    validate: function() {
        var message = '';

        if (!this.model.get('nu_valor')) {
            message += 'Valor, ';
        }

        if (!this.model.get('id_tipodisciplina')) {
            message += 'Tipo da disciplina, ';
        }

        if (!this.model.get('id_professor')) {
            message += 'Professor, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function() {

        this.model.set({'id_tiporegrapagamento': 4}); // TipoRegraPagaemento::PROFESSOR
        this.model.set({'id_cargahoraria': $('#form-professor #id_cargahoraria option:selected').val()});
        this.model.set({'id_tipodisciplina': $('#form-professor #id_tipodisciplina option:selected').val()});
        this.model.set({'nu_valor': $('#form-professor #nu_valor').val()});
        this.model.set({'id_professor': $('#form-professor #id_professor option:selected').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function(model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Regra de pagamento salva com sucesso.'});
                    that.renderListagem();
                    that.clearForm();
                },
                error: function(model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar regra de pagamento.'});
                }
            });

        }
    },
    clearForm: function() {
        $('#form-professor #id_cargahoraria').val('').select2('val', '');
        $('#form-professor #id_tipodisciplina').val('').select2('val', '');
        $('#form-professor #nu_valor').val('');
        $('#form-professor #id_professor').val('').select2('val', '');
        this.model = new RegraPagamento();
    },
    onShow: function() {
        this.renderTipoDisciplinaSelect();
        this.renderCargaHorariaSelect();
        this.renderProfessorSelect();
        this.renderListagem();
        this.mask();
    },
    renderTipoDisciplinaSelect: function() {
        carregaComboBox('id_tipodisciplina', this.model.get('id_tipodisciplina'));
    },
    renderCargaHorariaSelect: function() {
        carregaComboBox('id_cargahoraria', this.model.get('id_cargahoraria'));
    },
    renderProfessorSelect: function(id) {
        carregaComboBox('id_professor', this.model.get('id_professor'));
    },
    renderListagem: function() {
        //Renderiza a listagem
        var tableView = new ProfessorTableView();
        tableView.render();
        $("#container-professor-list").html(tableView.render().$el);
    },
    mask: function() {
        $('#form-professor #nu_valor').maskMoney();
    },
    search: function() {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new ProfessorTableView();
        tableView.collection.url = 'regra-pagamento/retornar-regra-professor?' + $('#form-professor').serialize();
        tableView.collection.fetch({
            success: function () {
                $("#container-professor-list").html(tableView.render().$el);
            }
        });
    },
    clearSearch: function() {
        //Renderiza a listagem
        this.clearForm();
        var tableView = new ProfessorTableView();
        $("#container-professor-list").html(tableView.render().$el);
    },
    openModal: function () {
        var that = this;
        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CargaHorariaView({
            saveCallback: function () {
                view.fecharModal();
                that.renderCargaHorariaSelect();
            }
        });
        var modal = new ModalLayout({
            view: view,
            label: 'Nova Carga Horária'
        });

        G2S.layout.content.currentView.modalRegion.show(modal);
    }
});
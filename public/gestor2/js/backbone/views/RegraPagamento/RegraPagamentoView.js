/**
 * View geral para a funcionalidade de cadastro de regras de pagamento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   Cache do Combobox
 **********************/

var comboCaches = {};

var carregaComboBox = function(selectName, selectedId, customOptions){
    customOptions = customOptions || {};

    switch (selectName) {
        case 'id_cargahoraria':
            newOptions = {
                url: '/api/carga-horaria/',
                optionLabel: 'nu_cargahoraria',
                model: CargaHoraria
            };
            break;
        case 'id_areaconhecimento':
            newOptions = {
                url: '/area-conhecimento/get-areas-com-projeto',
                optionLabel: 'st_areaconhecimento',
                model: AreaConhecimentoModel
            };
            break;
        case 'id_tipodisciplina':
            newOptions = {
                url: '/api/tipo-disciplina/',
                optionLabel: 'st_tipodisciplina',
                model: TipoDisciplina
            };
            break;
        case 'id_professor':
            newOptions = {
                url: '/api/vw-professor',
                optionLabel: 'st_nomecompleto',
                model: VwProfessor
            };
            break;
        case 'id_projetopedagogico':
            newOptions = {
                url: '/api/projeto-pedagogico/',
                optionLabel: 'st_projetopedagogico',
                model: ProjetoPedagogicoModel
            };
            break;
    }

    var options = $.extend({
        el: $('[name="' + selectName + '"]'),
        collection: comboCaches[selectName] || null,
        optionSelectedId: selectedId,
        emptyOption: '[Selecione]'
    }, newOptions, customOptions);

    ComboboxView(options, function(el, composite){
        if (!comboCaches[selectName] && !customOptions.url)
            comboCaches[selectName] = composite.collection.toJSON();
        el.select2('destroy');
        el.select2();
    });
};

/***********************
 *   imports
 **********************/

$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoEntidadeView.js');
$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoAreaView.js');
$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoProjetoView.js');
$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoProfessorView.js');
$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoProfessorProjetoView.js');
$.getScript('/js/backbone/views/Util/ModalLayout.js');
$.getScript('/js/backbone/views/RegraPagamento/CargaHorariaView.js');

var tpl = '';
$.get('/js/backbone/templates/regra-pagamento/tpl.html?_=' + new Date().getTime(), '', function(data) {
    tpl = data;
});

/***********************
 *   RegraPagamentoView
 **********************/

var RegraPagamentoView = Backbone.Marionette.LayoutView.extend({
    initialize: function() {
        this.template = _.template(tpl);
    },
    events: {
        'click #link-entidade': 'renderRegraEntidadeView',
        'click #link-area': 'renderRegraAreaView',
        'click #link-projeto': 'renderRegraProjetoView',
        'click #link-professor': 'renderRegraProfessorView',
        'click #link-professor-projeto': 'renderRegraProfessorProjetoView'
    },
    regions: {
        'entidadeRegion': '#container-entidade',
        'areaRegion': '#container-area',
        'projetoRegion': '#container-projeto',
        'professorRegion': '#container-professor',
        'professorProjetoRegion': '#container-professor-projeto',
        'modalRegion': '#container-modal'
    },
    renderRegraEntidadeView: function() {
        this.entidadeRegion.show(new RegraPagamentoEntidadeView());
    },
    renderRegraAreaView: function() {
        this.areaRegion.show(new RegraPagamentoAreaView());
    },
    renderRegraProjetoView: function() {
        this.projetoRegion.show(new RegraPagamentoProjetoView());
    },
    renderRegraProfessorView: function() {
        this.professorRegion.show(new RegraPagamentoProfessorView());
    },
    renderRegraProfessorProjetoView: function() {
        this.professorProjetoRegion.show(new RegraPagamentoProfessorProjetoView());
    },
    onShow: function() {
        this.renderRegraEntidadeView();
    }
});
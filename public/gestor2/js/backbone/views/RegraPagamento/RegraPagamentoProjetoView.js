/**
 * View para a funcionalidade de cadastro de regras de pagamento para a projeto
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/RegraPagamento.js');
$.getScript('js/backbone/models/TipoDisciplina.js');
$.getScript('js/backbone/models/CargaHoraria.js');
$.getScript('js/backbone/models/projeto-pedagogico.js');
$.getScript('js/backbone/views/RegraPagamento/ProjetoTableView.js');

var tplFormProjeto = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-projeto.html?_=' + new Date().getTime(), '', function(data) {
    tplFormProjeto = data;
});

/***********************
 *   RegraPagamentoProjetoView
 **********************/

var RegraPagamentoProjetoView = Backbone.Marionette.ItemView.extend({
    initialize: function() {
        this.model = new RegraPagamento();
        this.template = _.template(tplFormProjeto);
    },
    events: {
        'click #btn-save-projeto': 'save',
        'click #btn-new-projeto': 'clearForm',
        'click #btn-search-projeto': 'search',
        'click #btn-clear-projeto': 'clearSearch',
        'click #btn-carga-horaria': 'openModal',
        'change #id_areaconhecimento': function() {
            this.renderProjetoPedagogicoSelect($('#form-projeto #id_areaconhecimento').val());
        },
        'submit #form-projeto': function() {
            return false;
        }
    },
    validate: function() {
        var message = '';

        if (!this.model.get('nu_valor')) {
            message += 'Valor, ';
        }

        if (!this.model.get('id_tipodisciplina')) {
            message += 'Tipo da disciplina, ';
        }

        if (!this.model.get('id_areaconhecimento')) {
            message += 'Área do conhecimento, ';
        }

        if (!this.model.get('id_projetopedagogico')) {
            message += 'Projeto pedagógico, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function() {

        this.model.set({'id_tiporegrapagamento': 3}); // TipoRegraPagaemento::PROJETO_PEDAGOGICO
        this.model.set({'id_cargahoraria': $('#form-projeto #id_cargahoraria option:selected').val()});
        this.model.set({'id_tipodisciplina': $('#form-projeto #id_tipodisciplina option:selected').val()});
        this.model.set({'nu_valor': $('#form-projeto #nu_valor').val()});
        this.model.set({'id_areaconhecimento': $('#form-projeto #id_areaconhecimento option:selected').val()});
        this.model.set({'id_projetopedagogico': $('#form-projeto #id_projetopedagogico option:selected').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function(model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Regra de pagamento salva com sucesso.'});
                    that.renderListagem();
                    that.clearForm();
                },
                error: function(model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar regra de pagamento.'});
                }
            });

        }
    },
    clearForm: function() {
        $('#form-projeto #id_cargahoraria').val('').select2('val', '');
        $('#form-projeto #id_tipodisciplina').val('').select2('val', '');
        $('#form-projeto #nu_valor').val('');
        $('#form-projeto #id_areaconhecimento').val('').select2('val', '');
        $('#form-projeto #id_projetopedagogico').val('').select2('val', '');
        this.model = new RegraPagamento();
    },
    onShow: function() {
        this.renderTipoDisciplinaSelect();
        this.renderCargaHorariaSelect();
        this.renderAreaConhecimentoSelect();
        if (this.model.get('id_projetopedagogico')) {
            this.renderProjetoPedagogicoSelect();
        }
        this.renderListagem();
        this.mask();
    },
    renderTipoDisciplinaSelect: function() {
        carregaComboBox('id_tipodisciplina', this.model.get('id_tipodisciplina'));
    },
    renderCargaHorariaSelect: function() {
        carregaComboBox('id_cargahoraria', this.model.get('id_cargahoraria'));
    },
    renderAreaConhecimentoSelect: function() {
        carregaComboBox('id_areaconhecimento', this.model.get('id_areaconhecimento'));
    },
    renderProjetoPedagogicoSelect: function(id) {
        carregaComboBox('id_projetopedagogico', this.model.get('id_projetopedagogico'), id ? {
            url: 'projeto-pedagogico/get-by-area/id_areaconhecimento/' + id
        }: false);
    },
    renderListagem: function() {
        //Renderiza a listagem
        var tableView = new ProjetoTableView();
        tableView.render();
        $("#container-projeto-list").html(tableView.render().$el);
    },
    mask: function() {
        $('#form-projeto #nu_valor').maskMoney();
    },
    search: function() {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new ProjetoTableView();
        tableView.collection.url = 'regra-pagamento/retornar-regra-projeto-pedagogico?' + $('#form-projeto').serialize();
        tableView.collection.fetch({
            success: function () {
                $("#container-projeto-list").html(tableView.render().$el);
            }
        });
    },
    clearSearch: function() {
        //Renderiza a listagem
        this.clearForm();
        var tableView = new ProjetoTableView();
        $("#container-projeto-list").html(tableView.render().$el);
    },
    openModal: function () {
        var that = this;
        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CargaHorariaView({
            saveCallback: function () {
                view.fecharModal();
                that.renderCargaHorariaSelect();
            }
        });
        var modal = new ModalLayout({
            view: view,
            label: 'Nova Carga Horária'
        });

        G2S.layout.content.currentView.modalRegion.show(modal);
    }
});
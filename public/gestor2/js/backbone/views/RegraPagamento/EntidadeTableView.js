/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/RegraPagamento/EntidadeRowView.js');
$.getScript('js/backbone/models/VwRegraPagamento.js');

var tplTableEntidade = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-table-entidade.html?_=' + new Date().getTime(), '', function(data) {
    tplTableEntidade = data;
});

/***********************
 *  EntidadeTableView
 *  Reponsavel por renderizar a tabela EntidadeTableView
 ***********************/

var EntidadeTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTableEntidade),
    childView: EntidadeRowView,
    childViewContainer: "#entidade-row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: VwRegraPagamento,
            url: function() {
                return 'regra-pagamento/retornar-regra-entidade';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});
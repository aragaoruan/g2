/**
 * View para a funcionalidade de cadastro de regras de pagamento para a area
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/RegraPagamento.js');
$.getScript('js/backbone/models/TipoDisciplina.js');
$.getScript('js/backbone/models/CargaHoraria.js');
$.getScript('js/backbone/models/area-conhecimento.js');
$.getScript('js/backbone/views/RegraPagamento/AreaTableView.js');

var tplFormArea = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-area.html?_=' + new Date().getTime(), '', function(data) {
    tplFormArea = data;
});

/***********************
 *   RegraPagamentoAreaView
 **********************/

var RegraPagamentoAreaView = Backbone.Marionette.ItemView.extend({
    initialize: function() {
        this.model = new RegraPagamento();
        this.template = _.template(tplFormArea);
    },
    events: {
        'click #btn-save-area': 'save',
        'click #btn-new-area': 'clearForm',
        'click #btn-search-area': 'search',
        'click #btn-clear-area': 'clearSearch',
        'click #btn-carga-horaria': 'openModal',
        'submit #form-area': function() {
            return false;
        }
    },
    validate: function() {
        var message = '';

        if (!this.model.get('nu_valor')) {
            message += 'Valor, ';
        }

        if (!this.model.get('id_tipodisciplina')) {
            message += 'Tipo da disciplina, ';
        }

        if (!this.model.get('id_areaconhecimento')) {
            message += 'Área do conhecimento, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function() {

        this.model.set({'id_tiporegrapagamento': 2}); // TipoRegraPagaemento::AREA_CONHECIMENTO
        this.model.set({'id_cargahoraria': $('#form-area #id_cargahoraria option:selected').val()});
        this.model.set({'id_tipodisciplina': $('#form-area #id_tipodisciplina option:selected').val()});
        this.model.set({'nu_valor': $('#form-area #nu_valor').val()});
        this.model.set({'id_areaconhecimento': $('#form-area #id_areaconhecimento option:selected').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function(model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Regra de pagamento salva com sucesso.'});
                    that.renderListagem();
                    that.clearForm();
                },
                error: function(model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar regra de pagamento.'});
                }
            });

        }
    },
    clearForm: function() {
        $('#form-area #id_cargahoraria').val('').select2('val', '');
        $('#form-area #id_tipodisciplina').val('').select2('val', '');
        $('#form-area #nu_valor').val('');
        $('#form-area #id_areaconhecimento').val('').select2('val', '');
        this.model = new RegraPagamento();
    },
    onShow: function() {
        this.renderTipoDisciplinaSelect();
        this.renderCargaHorariaSelect();
        this.renderAreaConhecimentoSelect();
        this.renderListagem();
        this.mask();
    },
    renderTipoDisciplinaSelect: function() {

        carregaComboBox('id_tipodisciplina', this.model.get('id_tipodisciplina'));
    },
    renderCargaHorariaSelect: function() {

        carregaComboBox('id_cargahoraria', this.model.get('id_cargahoraria'));
    },
    renderAreaConhecimentoSelect: function() {

        carregaComboBox('id_areaconhecimento', this.model.get('id_areaconhecimento'));
    },
    renderListagem: function() {
        //Renderiza a listagem
        var tableView = new AreaTableView();
        tableView.render();
        $("#container-area-list").html(tableView.render().$el);
    },
    mask: function() {
        $('#form-area #nu_valor').maskMoney();
    },
    search: function() {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new AreaTableView();
        tableView.collection.url = 'regra-pagamento/retornar-regra-area-conhecimento?' + $('#form-area').serialize();
        tableView.collection.fetch({
            success: function () {
                $("#container-area-list").html(tableView.render().$el);
            }
        });
    },
    clearSearch: function() {
        //Renderiza a listagem
        this.clearForm();
        var tableView = new AreaTableView();
        $("#container-area-list").html(tableView.render().$el);
    },
    openModal: function () {
        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var that = this;

        var view = new CargaHorariaView({
            saveCallback: function () {
                view.fecharModal();
                that.renderCargaHorariaSelect();
            }
        });
        var modal = new ModalLayout({
            view: view,
            label: 'Nova Carga Horária'
        });

        G2S.layout.content.currentView.modalRegion.show(modal);
    }
});
/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/RegraPagamento/ProjetoRowView.js');
$.getScript('js/backbone/models/VwRegraPagamento.js');

var tplTableProjeto = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-table-projeto.html?_=' + new Date().getTime(), '', function(data) {
    tplTableProjeto = data;
});

/***********************
 *  ProjetoTableView
 *  Reponsavel por renderizar a tabela ProjetoTableView
 ***********************/

var ProjetoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTableProjeto),
    childView: ProjetoRowView,
    childViewContainer: "#projeto-row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: VwRegraPagamento,
            url: function() {
                return 'regra-pagamento/retornar-regra-projeto-pedagogico';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});
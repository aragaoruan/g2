/**
 * View para a funcionalidade de cadastro de regras de pagamento para a professor no projeto pedagógico
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/RegraPagamento.js');
$.getScript('js/backbone/models/TipoDisciplina.js');
$.getScript('js/backbone/models/CargaHoraria.js');
$.getScript('js/backbone/models/VwProfessor.js');
$.getScript('js/backbone/views/RegraPagamento/ProfessorProjetoTableView.js');

var tplFormProfessorProjeto = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-professor-projeto.html?_=' + new Date().getTime(), '', function(data) {
    tplFormProfessorProjeto = data;
});

/***********************
 *   RegraPagamentoProfessorProjetoView
 **********************/

var RegraPagamentoProfessorProjetoView = Backbone.Marionette.ItemView.extend({
    initialize: function() {
        this.model = new RegraPagamento();
        this.template = _.template(tplFormProfessorProjeto);
    },
    events: {
        'click #btn-save-professor-projeto': 'save',
        'click #btn-new-professor-projeto': 'clearForm',
        'click #btn-search-professor-projeto': 'search',
        'click #btn-clear-professor-projeto': 'clearSearch',
        'click #btn-carga-horaria': 'openModal',
        'change #id_areaconhecimento': function() {
            this.renderProjetoPedagogicoSelect($('#form-professor-projeto #id_areaconhecimento').val());
        },
        'submit #form-professor-projeto': function() {
            return false;
        }
    },
    validate: function() {
        var message = '';

        if (!this.model.get('nu_valor')) {
            message += 'Valor, ';
        }

        if (!this.model.get('id_tipodisciplina')) {
            message += 'Tipo da disciplina, ';
        }

        if (!this.model.get('id_areaconhecimento')) {
            message += 'Área do conhecimento, ';
        }

        if (!this.model.get('id_projetopedagogico')) {
            message += 'Projeto pedagógico, ';
        }
        console.log(this.model.get('id_professor'));

        if (!this.model.get('id_professor')) {
            message += 'Professor, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function() {

        this.model.set({'id_tiporegrapagamento': 5}); // TipoRegraPagaemento::PROFESSOR_PROJETO
        this.model.set({'id_cargahoraria': $('#form-professor-projeto #id_cargahoraria option:selected').val()});
        this.model.set({'id_tipodisciplina': $('#form-professor-projeto #id_tipodisciplina option:selected').val()});
        this.model.set({'nu_valor': $('#form-professor-projeto #nu_valor').val()});
        this.model.set({'id_areaconhecimento': $('#form-professor-projeto #id_areaconhecimento option:selected').val()});
        this.model.set({'id_projetopedagogico': $('#form-professor-projeto #id_projetopedagogico option:selected').val()});
        this.model.set({'id_professor': $('#form-professor-projeto #id_professor option:selected').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function(model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Regra de pagamento salva com sucesso.'});
                    that.renderListagem();
                    that.clearForm();
                },
                error: function(model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar regra de pagamento.'});
                }
            });

        }
    },
    clearForm: function() {
        $('#form-professor-projeto #id_cargahoraria').val('').select2('val', '');
        $('#form-professor-projeto #id_tipodisciplina').val('').select2('val', '');
        $('#form-professor-projeto #nu_valor').val('');
        $('#form-professor-projeto #id_areaconhecimento').val('').select2('val', '');
        $('#form-professor-projeto #id_projetopedagogico').val('').select2('val', '');
        $('#form-professor-projeto #id_professor').val('').select2('val', '');
        this.model = new RegraPagamento();
    },
    onShow: function() {
        this.renderTipoDisciplinaSelect();
        this.renderCargaHorariaSelect();
        this.renderAreaConhecimentoSelect();
        this.renderProfessorSelect();

        if (this.model.get('id_projetopedagogico')) {
            this.renderProjetoPedagogicoSelect();
        }
        this.renderListagem();
        this.mask();
    },
    renderTipoDisciplinaSelect: function() {
        carregaComboBox('id_tipodisciplina', this.model.get('id_tipodisciplina'));
    },
    renderCargaHorariaSelect: function() {
        carregaComboBox('id_cargahoraria', this.model.get('id_cargahoraria'));
    },
    renderAreaConhecimentoSelect: function() {
        carregaComboBox('id_areaconhecimento', this.model.get('id_areaconhecimento'));
    },
    renderProjetoPedagogicoSelect: function(id) {

        carregaComboBox('id_projetopedagogico', this.model.get('id_projetopedagogico'), id ? {
            url: 'projeto-pedagogico/get-by-area/id_areaconhecimento/' + id
        }: false);
    },
    renderProfessorSelect: function(id) {
        carregaComboBox('id_professor', this.model.get('id_professor'));
    },
    renderListagem: function() {
        //Renderiza a listagem
        var tableView = new ProfessorProjetoTableView();
        tableView.render();
        $("#container-professor-projeto-list").html(tableView.render().$el);
    },
    mask: function() {
        $('#form-professor-projeto #nu_valor').maskMoney();
    },
    search: function() {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new ProfessorProjetoTableView();
        tableView.collection.url = 'regra-pagamento/retornar-regra-professor-projeto?' + $('#form-professor-projeto').serialize();
        tableView.collection.fetch({
            success: function () {
                $("#container-professor-projeto-list").html(tableView.render().$el);
            }
        });
    },
    clearSearch: function() {
        //Renderiza a listagem
        this.clearForm();
        var tableView = new ProfessorProjetoTableView();
        $("#container-professor-projeto-list").html(tableView.render().$el);
    },
    openModal: function () {
        var that = this;
        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CargaHorariaView({
            saveCallback: function () {
                view.fecharModal();
                that.renderCargaHorariaSelect();
            }
        });
        var modal = new ModalLayout({
            view: view,
            label: 'Nova Carga Horária'
        });

        G2S.layout.content.currentView.modalRegion.show(modal);
    }
});
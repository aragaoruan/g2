/***********************
 *   imports
 **********************/

var tplRowArea = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-row-area.html?_=' + new Date().getTime(), '', function(data) {
    tplRowArea = data;
});

/***********************
 *  AreaRowView
 *  Reponsavel por renderizar uma linha da AreaTableView
 ***********************/

var AreaRowView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplRowArea),
    events: {
        'click .btn-edit': 'showForm',
        'click .btn-delete': 'delete'
    },
    delete: function() {
        G2S.layout.content.currentView.areaRegion.currentView.clearForm();
        var that = this;
        this.model.url = 'api/regra-pagamento/' + this.model.get('id_regrapagamento');
        this.model.destroy({
            wait: true,
            success: function(model, response) {
                that.remove();
                $.pnotify({'title': 'Sucesso', 'text': 'Regra de pagamento removida com sucesso.', 'type': 'success'});
            },
            error: function(model, response) {
                $.pnotify({'title': 'Erro', 'text': 'Erro ao remover Regra de Pagamento.', 'type': 'error'});
            }
        });
    },
    showForm: function() {
        var view = G2S.layout.content.currentView.areaRegion.currentView;
        view.model = new RegraPagamento(this.model.toJSON());
        G2S.layout.content.currentView.areaRegion.show(view);
    }
});
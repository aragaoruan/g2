/**
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @type @exp;Backbone@pro;Marionette@pro;Layout@call;extend
 */

/***********************
 *   imports
 **********************/

//$.getScript('/js/backbone/views/RegraPagamento/RegraPagamentoEntidadeView.js');

/***********************
 *   RegraPagamentoView
 **********************/

var SelectView = Backbone.Marionette.CollectionView.extend({
    initialize: function() {
        this.itemView = Marionette.ItemView.extend({
            tagName: 'option',
            initialize: function(options) {
                this.optionSelected = options.optionSelected;
                this.optionValue = options.optionValue ? options.optionValue : 'id';
                this.template = _.template('<%= ' + options.optionLabel + ' %>');
            },
            onRender: function() {
                this.$el.attr('value', this.model.get(this.optionValue));
                if (this.optionSelected == this.model.get(this.optionValue)) {
                    this.$el.attr('selected', 'selected');
                }
            }
        });
    }

});
/**
 * View para a funcionalidade de cadastro de regras de pagamento para a entidade
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
$.getScript('js/backbone/models/RegraPagamento.js');
$.getScript('js/backbone/models/TipoDisciplina.js');
$.getScript('js/backbone/models/CargaHoraria.js');
$.getScript('js/backbone/views/RegraPagamento/EntidadeTableView.js');

var tplFormEntidade = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-form-entidade.html?_=' + new Date().getTime(), '', function (data) {
    tplFormEntidade = data;
});

/***********************
 *   RegraPagamentoEntidadeView
 **********************/

var RegraPagamentoEntidadeView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.model = new RegraPagamento();
        this.template = _.template(tplFormEntidade);
    },
    events: {
        'click #btn-save-entidade': 'save',
        'click #btn-new-entidade': 'clearForm',
        'click #btn-search-entidade': 'search',
        'click #btn-clear-entidade': 'clearSearch',
        'click #btn-carga-horaria': 'openModal',
        'submit #form-entidade': function () {
            return false;
        }
    },
    validate: function () {
        var message = '';

        if (!this.model.get('nu_valor')) {
            message += 'Valor, ';
        }

        if (!this.model.get('id_tipodisciplina')) {
            message += 'Tipo da disciplina, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    save: function () {

        this.model.set({'id_tiporegrapagamento': 1}); // TipoRegraPagaemento::ENTIDADE
        this.model.set({'id_cargahoraria': $('#form-entidade #id_cargahoraria option:selected').val()});
        this.model.set({'id_tipodisciplina': $('#form-entidade #id_tipodisciplina option:selected').val()});
        this.model.set({'nu_valor': $('#form-entidade #nu_valor').val()});

        var that = this;
        if (this.validate()) {
            this.model.save(this.model.toJSON(), {
                success: function (model, response) {
                    $.pnotify({'title': 'Sucesso', 'type': 'success', 'text': 'Regra de pagamento salva com sucesso.'});
                    that.renderListagem();
                    that.clearForm();
                },
                error: function (model, response) {
                    $.pnotify({'title': 'Erro', 'type': 'error', 'text': 'Erro ao salvar regra de pagamento.'});
                }
            });

        }
    },
    clearForm: function () {
        $('#form-entidade #id_cargahoraria').val('');
        $('#form-entidade #id_cargahoraria').val('').select2('val', '');
        $('#form-entidade #id_tipodisciplina').val('').select2('val', '');
        this.model = new RegraPagamento();
    },
    onShow: function () {
        this.renderTipoDisciplinaSelect();
        this.renderCargaHorariaSelect();
        this.renderListagem();
        this.mask();
    },
    renderTipoDisciplinaSelect: function () {
        carregaComboBox('id_tipodisciplina', this.model.get('id_tipodisciplina'));
    },

    renderCargaHorariaSelect: function () {
        carregaComboBox('id_cargahoraria', this.model.get('id_cargahoraria'));

    },
    renderListagem: function () {
        //Renderiza a listagem
        var tableView = new EntidadeTableView();
        tableView.render();
        $("#container-entidade-list").html(tableView.render().$el);
    },
    mask: function () {
        $('#form-entidade #nu_valor').maskMoney();

    },
    search: function () {
        //Renderiza a listagem de acordo com os criterios de pesquisa
        var tableView = new EntidadeTableView();
        tableView.collection.url = 'regra-pagamento/retornar-regra-entidade?' + $('#form-entidade').serialize();
        tableView.collection.fetch({
            success: function () {
                $("#container-entidade-list").html(tableView.render().$el);
            }
        });
    },
    clearSearch: function () {
        //Renderiza a listagem
        this.clearForm();
        var tableView = new EntidadeTableView();
        $("#container-entidade-list").html(tableView.render().$el);
    },
    openModal: function () {
        var that = this;
        // a modal está sendo abrigada na tpl-tabs.html div#container-modal
        var view = new CargaHorariaView({
            saveCallback: function () {
                view.fecharModal();
                that.renderCargaHorariaSelect();
            }
        });
        var modal = new ModalLayout({
            view: view,
            label: 'Nova Carga Horária'
        });

        G2S.layout.content.currentView.modalRegion.show(modal);
    }
});
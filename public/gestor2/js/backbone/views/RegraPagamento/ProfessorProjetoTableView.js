/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/RegraPagamento/ProfessorProjetoRowView.js');
$.getScript('js/backbone/models/VwRegraPagamento.js');

var tplTableProfessorProjeto = '';
$.get('/js/backbone/templates/regra-pagamento/tpl-table-professor-projeto.html?_=' + new Date().getTime(), '', function(data) {
    tplTableProfessorProjeto = data;
});

/***********************
 *  ProfessorProjetoTableView
 *  Reponsavel por renderizar a tabela ProfessorProjetoTableView
 ***********************/

var ProfessorProjetoTableView = Backbone.Marionette.CompositeView.extend({
    template: _.template(tplTableProfessorProjeto),
    childView: ProfessorProjetoRowView,
    childViewContainer: "#professor-projeto-row-container",
    initialize: function() {
        var Collection = Backbone.Collection.extend({
            model: VwRegraPagamento,
            url: function() {
                return 'regra-pagamento/retornar-regra-professor-projeto';
            }
        });
        this.collection = new Collection();
        this.collection.fetch();
    }
});
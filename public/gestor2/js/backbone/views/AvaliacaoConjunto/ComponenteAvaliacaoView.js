/**
 * Created by Débora Castro on 21/10/14.
 */

/***********************
 *   imports
 **********************/

$.getScript('modelo-backbone/VwPesquisaAvaliacao');
$.getScript('modelo-backbone/TipoAvaliacao');
$.getScript('modelo-backbone/VwAvaliacaoConjuntoRelacao');
$.getScript('js/CollectionView.js');


ModelAvaliacoesSelecionadas = Backbone.Model.extend({
    defaults:{
        id_avaliacao: '',
        st_tipoavaliacao: '',
        nu_valor: '',
        bl_recuperacao: false,
        id_avaliacaorecupera: ''
    }
});
var VwPesquisaAvaliacaoCollection =  Backbone.Collection.extend({
    model: VwPesquisaAvaliacao
});

collectionAvaliacoes = new VwPesquisaAvaliacaoCollection();


var tplFormAvaliacoes = '';
$.get('/js/backbone/templates/avaliacao-conjunto/form-avaliacoes.html?_=' + new Date().getTime(), '', function (data) {
    tplFormAvaliacoes = data;
});

var rowAvaliacoes = '';
$.get('/js/backbone/templates/avaliacao-conjunto/row-avaliacao.html?_=' + new Date().getTime(), '', function (data) {
    rowAvaliacoes = data;
});

var rowAvaliacoesSelecionadas = '';
$.get('/js/backbone/templates/avaliacao-conjunto/row-avaliacao-selecionada.html?_=' + new Date().getTime(), '', function (data) {
    rowAvaliacoesSelecionadas = data;
});
/***********************
 *   ComponenteAvaliacaoView
 **********************/

var ComponenteAvaliacaoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.template = _.template(tplFormAvaliacoes);
    },
    events: {
        "keyup .input-search": "pesquisaAvaliacoes",
        "change #id_tipoavaliacao": "pesquisaTipoAvaliacoes",
        "click #marcaTodosI": "marcaTodos"
    },
    pesquisaAvaliacoes: function(e) {
        var valorDigitado = e.currentTarget.value;
        var uper = valorDigitado.toUpperCase();
        var tableRows = $(".pesquisa-avaliacoes tbody>tr");
        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + uper + "')").parent("tr").show();
        } else {
            tableRows.show();
        }

    },
    pesquisaTipoAvaliacoes: function(e) {
        var valorDigitado = $(e.currentTarget).find(' :selected').text();
        var tableRows = $(".pesquisa-avaliacoes tbody>tr");
        if (valorDigitado.length > 0) {
            tableRows.hide().find("td:contains('" + valorDigitado + "')").parent("tr").show();
        } else {
            tableRows.show();
        }

    },
    onShow: function () {
        this.carregarGridAvaliacoes();
        this.renderTipoAvaliacao();
    },
    renderTipoAvaliacao:function(){

        var TipoAvaliacaoCollection = Backbone.Collection.extend({
            model: TipoAvaliacao
        });

        var collection = new TipoAvaliacaoCollection();
        collection.url = '/default/avaliacao-conjunto/retorna-tipo-avaliacao';
        collection.fetch();

        var view = new SelectView({
            el: $('#id_tipoavaliacao'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_tipoavaliacao',
                optionValue: 'id_tipoavaliacao'
            }
        });

        view.render();
    },
    carregarGridAvaliacoes: function(){
        var that = this;

        collectionAvaliacoes.url = '/default/avaliacao-conjunto/retorna-pesquisa-avaliacoes?id_avaliacaoconjunto='+this.model.get('id_avaliacaoconjunto');
//        loading();
        $('#lista-avaliacoes tbody').html('Carregando...');
        collectionAvaliacoes.fetch({
            success: function() {
                var tabelaProjetoView = new CollectionView({
                    collection: collectionAvaliacoes,
                    childViewTagName: 'tr',
                    childViewConstructor: listaAvaliacoesView,
                    el: $('#lista-avaliacoes tbody')
                });
                tabelaProjetoView.render();
            },
            complete: function() {
                if(that.model.get('id_avaliacaoconjunto')){
                    that.carregaAvaliacoesSelecionadosEdit();
                }
            }
        });
    },
    carregaAvaliacoesSelecionadosEdit: function(){

        var VwAvaliacaoConjuntoRelacaoCollection = Backbone.Collection.extend({
            model: VwAvaliacaoConjuntoRelacao
        });

        var collectionAvaliacoesSa = new VwAvaliacaoConjuntoRelacaoCollection();
        collectionAvaliacoesSa.url = '/default/avaliacao-conjunto/retorna-avaliacao-conjunto-relacao/id_avaliacaoconjunto/' + this.model.get('id_avaliacaoconjunto');
        //loading();
        collectionAvaliacoesSa.fetch({
            success: function() {
                var tabelaProjeto1View = new CollectionView({
                    collection: collectionAvaliacoesSa,
                    childViewTagName: 'tr',
                    childViewConstructor: avaliacoesSelecionadosView,
                    el: $('#lista-avaliacoes-selecionados tbody')
                });
                tabelaProjeto1View.render();
            },
            complete: function() {
                //loaded();
            }
        });
    },
    marcaTodos: function(e) {
        if (e.target.checked === true) {
            $(":checkbox[class*=checkall]").each(function() {
                this.checked = true;
                this.disabled = true;
                $('#lista-avaliacoes-selecionados tbody').html('<tr><td colspan="2">Todos as avaliações foram selecionadas!</td></tr>');
            });
        } else {
            $(":checkbox[class*=checkall]").each(function() {
                this.checked = false;
                this.disabled = false;
                $('#lista-avaliacoes-selecionados tbody').html('');
            });
        }
    }
});

var listaAvaliacoesView = Backbone.View.extend({
    model: VwPesquisaAvaliacao,
    initialize: function() {
        this.render = _.bind(this.render, this);
    },
    render: function() {
        var variables = this.model.toJSON();
        var temp;
        temp = _.template(rowAvaliacoes, variables);
        this.$el.html(temp);
        return this;
    },
    events: {
        "click .addAvaliacao": "selecionaAvaliacao"
    },
    selecionaAvaliacao: function(e) {

        if (this.model.get('id_tipoavaliacao') === TIPO_AVALIACAO.CONCEITUAL) {
            if ($("button.remover").length > 0) {
                $('#id_avaliacao' + this.model.get('id_avaliacao')).attr('checked', false);
                $.pnotify({
                    title: 'Atenção!',
                    text: 'Avaliação conceitual não pode ser vinculada com outros tipos de avalições !',
                    type: 'warning'
                });
                return;
            }
        }
        $(e.currentTarget).attr('disabled', true);
        var element = $('<tr></tr>');
        $('#lista-avaliacoes-selecionados tbody').prepend(element);
        var that = this;
        var teste = new avaliacoesSelecionadosView({model: that.model.attributes, el: element });
        teste.render();
    }
});

var avaliacoesSelecionadosView = Backbone.View.extend({
    model: ModelAvaliacoesSelecionadas,
    render: function() {
        var variables;

        if (this.model.attributes) {
            if(!(this.model.get('id_avaliacaorecupera'))){
                this.model.set('id_avaliacaorecupera',null);
            }
            variables = this.model.toJSON();
            $('#id_avaliacao' + this.model.get('id_avaliacao')).attr('disabled', true);
            $('#id_avaliacao' + this.model.get('id_avaliacao')).attr('checked', true);

            if (this.model.get('id_tipoavaliacao') === TIPO_AVALIACAO.CONCEITUAL) {
                $('#id_avaliacao' + this.model.get('id_avaliacao')).attr('checked', true);
                $('.addAvaliacao').attr('disabled', true);
                $('#calculo').hide();

            }
        } else {
          variables = this.model;
            if(!(this.model.id_avaliacaorecupera)){
                variables.id_avaliacaorecupera = null;
            }
            if (this.model.id_tipoavaliacao === TIPO_AVALIACAO.CONCEITUAL) {
                $('.addAvaliacao').attr('checked', false);
                document.getElementById("id_avaliacao" + this.model.id_avaliacao).checked = true;
                $('.addAvaliacao').attr('disabled', true);
                $('#calculo').hide();

            }
        }

        this.$el.html(_.template(rowAvaliacoesSelecionadas, variables));
        return this;
    },
    events: {
        "click .remover": "removerProjetoSelecionado",
        "keyup .validarordem" : 'validarOrdemRecuperacao',
        "blur .validarordem" : 'campoVazio'
    },
    campoVazio: function(e) {
        id = e.currentTarget.id;
        inicio = e.currentTarget.value;
        contador =0;
        if (e.currentTarget.value==''){
        $('div#divAvaliacoesSelecionadas table#lista-avaliacoes-selecionados td.span1 input').each(function (index, currentObject) {
                valor = currentObject.value;
            if ( (id != currentObject.id)) {
                    if(valor>contador){
                        contador=valor;
                    }
            }
        });

        e.currentTarget.value = ++contador;
        }
    },
        validarOrdemRecuperacao: function(e){
        var inicio = -1;
        var id = '';

            id = e.currentTarget.id;
            inicio = e.currentTarget.value;

        if (inicio> 10 || inicio<1) {
            $.pnotify({
                title: 'Atenção!',
                text: 'Os valores de ordem vão de 1 a 10.',
                type: 'warning'
            });
            e.currentTarget.value = '';
            return false;
        }

            $('div#divAvaliacoesSelecionadas table#lista-avaliacoes-selecionados td.span1 input').each(function (index, currentObject) {
                if ( (id != currentObject.id) && $(this).val()==inicio) {
                    $.pnotify({title: 'Atenção!', text: 'Os valores de ordem não podem ser iguais.', type: 'warning'});
                    e.currentTarget.value = '';
                    return false;
                }
            });
    },
    removerProjetoSelecionado: function(e) {
        var id_avaliacao = $(e.target).val();
        $('#id_avaliacao' + id_avaliacao).removeAttr('checked');
        $('#id_avaliacao' + id_avaliacao).removeAttr('disabled');
        $(e.target).parent().parent().remove();

        if (this.model.attributes) {
            $('.addAvaliacao').attr('checked', false);
            $('.addAvaliacao').attr('disabled', false);
            $('#calculo').show();
        } else {
            if (this.model.id_tipoavaliacao === TIPO_AVALIACAO.CONCEITUAL) {
                $('.addAvaliacao').attr('checked', false);
                $('.addAvaliacao').attr('disabled', false);
                $('#calculo').show();
            }
        }

        return false;
    },
    renderComboAvaliacao:function(idAva){

         var colecaoAvalRecuperacao = new VwPesquisaAvaliacaoCollection();

        colecaoAvalRecuperacao.url = '/default/avaliacao-conjunto/retorna-pesquisa-avaliacoes';
        colecaoAvalRecuperacao.fetch();

        var view = new SelectView({
            el: this.$el.find("#id_avaliacaorecuperacao-"+idAva),
            collection: colecaoAvalRecuperacao,
            childViewOptions: {
                optionLabel: 'st_avaliacao',
                optionValue: 'id_avaliacao'
            }
        });

        view.render();
    }

});
/**
 * View geral para a funcionalidade de cadastro de Avaliacao Conjunto
 * @author Débora Castro
 */

/***********************
 *   imports
 **********************/

var nocache = Math.random().toString(36).substring(7);
$.ajaxSetup({async:false});
$.getScript('/js/backbone/models/modalbasica.js?d=' + nocache);
$.getScript('js/backbone/views/Util/modal_basica.js?d=' + nocache);
$.getScript('js/backbone/views/AvaliacaoConjunto/DadosBasicosView.js?d=' + nocache);
$.getScript('js/backbone/views/AvaliacaoConjunto/ComponenteAvaliacaoView.js?d=' + nocache);
$.getScript('modelo-backbone/AvaliacaoConjunto?d=' + nocache);
$.getScript('js/backbone/views/Util/SelectView.js?d=' + nocache);




var tpl = '';
$.get('/js/backbone/templates/avaliacao-conjunto/tpl.html?_=' + new Date().getTime(), '', function (data) {
    tpl = data;
});

/***********************
 *   AvaliacaoConjuntoView
 **********************/

var AvaliacaoConjuntoView = Backbone.Marionette.LayoutView.extend({
    model: AvaliacaoConjunto,
    initialize: function () {
        this.template = _.template(tpl);
    },
    events: {
        "click #salvarConjuntoAvaliacao": "salvar"
    },
    validate: function () {
        var message = '';

        if (!this.model.get('st_avaliacaoconjunto')) {
            message += 'Título, ';
        }

        if (!this.model.get('id_situacao')) {
            message += 'Situação, ';
        }

        if (!$("#calculo").is(":hidden")) {
            if (!this.model.get('id_tipocalculoavaliacao') ||
                this.model.get('id_tipocalculoavaliacao') === 'Escolha') {
                message += 'Cálculo, ';
            }
        }

        if (!this.model.get('id_tipoprova')) {
            message += 'Tipo, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    validateTD: function (ids_tipodisciplina) {
        var message = '';

        if (ids_tipodisciplina == '') {
            message += 'Tipo de Disciplina, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    validateAV: function (ids_avaliacoes, id_ordem) {
        var message = '';

        if (ids_avaliacoes == '') {
            message += 'Avaliação, ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });

            return false;
        }

        return true;
    },
    salvar: function () {

        this.model.set('st_avaliacaoconjunto', $("input[name='st_avaliacaoconjunto']").val());
        this.model.set('id_situacao', $('#id_situacao option:selected').val());
        if ($("#calculo").is(":hidden")) {
            this.model.set('id_tipocalculoavaliacao', null);
        } else {
            this.model.set('id_tipocalculoavaliacao', $('#id_tipocalculoavaliacao option:selected').val());
        }
        this.model.set('id_tipoprova', $('#id_tipoprova option:selected').val());

        loading();
        that = this;

        if (this.validate()) {
            $.ajax({
                url: '/default/avaliacao-conjunto/salvar-avaliacao-conjunto',
                data: {avaliacaoconjunto: that.model.toJSON()},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                    that.model.set('id_avaliacaoconjunto', data.id);
                    that.salvarAvaliacoesRelacao(data.id);
                    that.salvarTipoDisciplina(data.id);
                },
                error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                }
            });
        }

        loaded();

    },
    salvarAvaliacoesRelacao: function (idAvaliacaoConjunto) {
        var ids_avaliacoes = [];
        //var ids_recuperacao = [];
        var ids_ordem = [];

        $('div#idAvaliacoesSel table#lista-avaliacoes input:checkbox:checked').each(function (index, currentObject) {
            ids_avaliacoes.push(currentObject.value);
        });

        //$('div#divAvaliacoesSelecionadas table#lista-avaliacoes-selecionados select').each(function (index, currentObject) {
        //    ids_recuperacao.push(currentObject.id+'-avaliacao-'+$(this).val());
        //});

        $('div#divAvaliacoesSelecionadas').find('.validarordem').each(function (index, currentObject) {
             ids_ordem.push(currentObject.id+'-'+$(this).val());
        });

        loading();
        if (this.validateAV(ids_avaliacoes)) {
            $.ajax({
                url: '/default/avaliacao-conjunto/salvar-avaliacao-conjunto-relacao',
                //data: {ids_avaliacoes: ids_avaliacoes, id_avaliacaoconjunto: idAvaliacaoConjunto, id_avaliacaorecupera: ids_ordem,id_recuperacao: ids_recuperacao},
                data: {ids_avaliacoes: ids_avaliacoes, id_avaliacaoconjunto: idAvaliacaoConjunto, id_avaliacaorecupera: ids_ordem},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                },
                error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                }
            });
        }
        loaded();

    },
    salvarTipoDisciplina: function (idAvaliacaoConjunto) {
        var ids_tipodisciplina = [];

        $('table#idTipoAvaliacao input:checkbox:checked').each(function (index, currentObject) {
            ids_tipodisciplina.push(currentObject.value);
        });

        loading();
        if (this.validateTD(ids_tipodisciplina)) {
            $.ajax({
                url: '/default/avaliacao-conjunto/salvar-avaliacao-conjunto-disciplina',
                data: {ids_tipodisciplina: ids_tipodisciplina, id_avaliacaoconjunto: idAvaliacaoConjunto},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                },
                error: function (data) {
                    $.pnotify({title: data.title, text: data.text, type: data.type});
                }
            });
        }
        loaded();

    },
    regions: {
        'dadosBasicosRegion': '#dadosBasicosConjunto',
        'componenteAvaliacaoRegion': '#componenteAvaliacao'
    },
    renderDadosBasicosView: function () {
        this.dadosBasicosRegion.show(new DadosBasicosView({model: this.model}));
    },
    renderComponenteAvaliacaoView: function () {
        this.componenteAvaliacaoRegion.show(new ComponenteAvaliacaoView({model: this.model}));
    },
    onShow: function () {
        this.renderDadosBasicosView();
        this.renderComponenteAvaliacaoView();
    }
});

$.ajaxSetup({async:true});
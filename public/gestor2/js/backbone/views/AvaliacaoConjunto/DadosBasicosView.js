/**
 * View para a funcionalidade de cadastro de avaliações de conjunto
 * @author Débora Castro
 */

/***********************
 *   imports
 **********************/
$.getScript('modelo-backbone/Situacao');
$.getScript('modelo-backbone/TipoProva');
$.getScript('modelo-backbone/TipoCalculoAvaliacao');
$.getScript('modelo-backbone/VwAvaliacaoConjunto');
$.getScript('modelo-backbone/TipoDisciplina');


var tplFormDados = '';
$.get('/js/backbone/templates/avaliacao-conjunto/form-dados-basicos.html?_=' + new Date().getTime(), '', function (data) {
    tplFormDados = data;
});

/***********************
 *   DadosBasicosView
 **********************/

var DadosBasicosView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.template = _.template(tplFormDados);
    },
    onShow: function () {
        this.renderSituacaoSelect();
        this.renderTipoProvaSelect();
        this.renderTipoCalculoSelect();
        this.renderTipoDisciplina();
        if (this.model.get('id_avaliacaoconjunto')) {
            this.populaform();
        }
    },
    renderSituacaoSelect: function () {
        var selected = '';

        if (this.model.get('id_situacao')) {
            selected = this.model.get('id_situacao');
        }

        var SituacaoCollection = Backbone.Collection.extend({
            model: Situacao
        });

        var collection = new SituacaoCollection();
        collection.url = '/default/avaliacao-conjunto/retorna-situacao-avaliacao-conjunto?st_tabela=tb_avaliacaoconjunto';
        collection.fetch();

        var view = new SelectView({
            el: $('#form-dados-basicos #id_situacao'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_situacao',
                optionValue: 'id_situacao',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderTipoProvaSelect: function () {
        var selected = '';

        if (this.model.get('id_tipoprova')) {
            selected = this.model.get('id_tipoprova');
        }

        var TipoProvaCollection = Backbone.Collection.extend({
            model: TipoProva
        });

        var collection = new TipoProvaCollection();
        collection.url = '/default/avaliacao-conjunto/retorna-tipo-prova';
        collection.fetch();

        var view = new SelectView({
            el: $('#form-dados-basicos #id_tipoprova'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_tipoprova',
                optionValue: 'id_tipoprova',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderTipoCalculoSelect: function () {
        var selected = '';

        if (this.model.get('id_tipocalculoavaliacao')) {
            selected = this.model.get('id_tipocalculoavaliacao');
        }

        var TipoCalculoCollection = Backbone.Collection.extend({
            model: TipoCalculoAvaliacao
        });

        var collection = new TipoCalculoCollection();
        collection.url = '/default/avaliacao-conjunto/retorna-tipo-calculo-avaliacao';
        collection.fetch();

        var view = new SelectView({
            el: $('#form-dados-basicos #id_tipocalculoavaliacao'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_tipocalculoavaliacao',
                optionValue: 'id_tipocalculoavaliacao',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderTipoDisciplina: function () {
        var that = this;
        var TipoDisciplinaCollection = Backbone.Collection.extend({
            model: TipoDisciplina
        });
        var collectionTipoDisciplina = new TipoDisciplinaCollection();
        collectionTipoDisciplina.url = '/default/avaliacao-conjunto/retorna-tipo-disciplina';
//        loading();
        $('#idTipoAvaliacao tbody').html('Carregando...');

        collectionTipoDisciplina.fetch({
            success: function () {
                var tabelaTDView = new CollectionView({
                    collection: collectionTipoDisciplina,
                    childViewTagName: 'tr',
                    childViewConstructor: listaTipoDisciplinaView,
                    el: $('#idTipoAvaliacao tbody')
                });
                tabelaTDView.render();
                that.populaform();
            }
        });

        if (that.model.get('id_avaliacaoconjunto') != null) {

        }
    },
    populaform: function () {
        $('#st_avaliacaoconjunto').val(this.model.get('st_avaliacaoconjunto'));
        this.tipoDisiciplinaEdit(this.model.get('id_avaliacaoconjunto'));
    },
    tipoDisiciplinaEdit: function (id_avaliacaocon) {
        $.ajax({
            url: '/default/avaliacao-conjunto/retorna-avaliacao-disciplina',
            data: {id_avaliacaoconjunto: id_avaliacaocon},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        $('table#idTipoAvaliacao input:checkbox').each(function (index, currentObject) {
                            if(currentObject.value == data[i].id_tipodisciplina){
                                currentObject.checked = true;
                            }
                        });
                    }
                }
            }
        });
    }
});

var listaTipoDisciplinaView = Backbone.View.extend({
    model: TipoDisciplina,
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp;
        temp = _.template('<td><input type="checkbox" id="id_tipodisciplina" name="id_tipodisciplina" value="<%=id_tipodisciplina%>"></td><td><%=st_tipodisciplina%></td>', variables);
        this.$el.html(temp);
        return this;
    }
});
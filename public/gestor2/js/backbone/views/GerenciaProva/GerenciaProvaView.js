/**
 * View geral para a funcionalidade Gerência Prova, responsável pelo agendamento de prova dos alunos
 * @author Débora Castro
 */

/***********************
 *   imports
 **********************/

$.getScript('js/backbone/views/Util/SelectView.js');
$.getScript('js/backbone/views/GerenciaProva/SituacaoAgendamentoView.js');
$.getScript('js/backbone/views/GerenciaProva/PesquisaAlunosAgendamentoView.js');
$.getScript('/modelo-backbone/Uf');
$.getScript('/modelo-backbone/AvaliacaoAgendamento');
$.getScript('/modelo-backbone/VwAplicadorProva');
$.getScript('/modelo-backbone/VwAlunosAgendamento');
$.getScript('/modelo-backbone/VwAvaliacaoAgendamento');
$.getScript('/modelo-backbone/VwAvaliacaoConjuntoRelacao');
$.getScript('/modelo-backbone/VwHistoricoAgendamento');
$.getScript('/modelo-backbone/VwDisciplinasAgendamento');


/**********************
 *   Templates
 **********************/

var tpl = '';
$.get('/js/backbone/templates/gerencia-prova/tpl-regioes.html?_=' + new Date().getTime(), '', function (data) {
    tpl = data;
});

var tplFormPesquisa = '';
$.get('/js/backbone/templates/gerencia-prova/form-pesquisa-alunos-agendamento.html?_=' + new Date().getTime(), '', function (data) {
    tplFormPesquisa = data;
});

var tplFormHistorico = '';
$.get('/js/backbone/templates/gerencia-prova/form-historico.html?_=' + new Date().getTime(), '', function (data) {
    tplFormHistorico = data;
});


/***********************
 *   URLS globais
 **********************/

url_datasaplicacao = '/default/gerencia-prova/find-data-aplicacao';
url_pesquisaalunos = '/default/gerencia-prova/pesquisa-alunos';
url_dadosagendamento = '/default/gerencia-prova/retorna-dados-agendamento';
url_salvar = '/api/gerencia-prova';

situacaoAgendamento = 68;
situacaoReagendamento = 69;
situacaoCancelado = 70;
situacaoLiberado = 129;
situacaoAbonado = 130;

matricula = false;
provasrealizadas = false;
viewdadosAgendamento = false;
viewHistoricoAgendamento = false;
tipoavaliacao = 0;
id_usuario = false;


/***********************
 *   Collections de para formulário de Pesquisa
 **********************/


var AplicadorProvaCollection = Backbone.Collection.extend({
    model: VwAplicadorProva
});

var VwAlunosAgendamentoCollection = Backbone.Collection.extend({
    model: VwAlunosAgendamento
});

var UfCollection = Backbone.Collection.extend({
    model: Uf
});

var VwAvaliacaoAgendamentoCollection = Backbone.Collection.extend({
    model: VwAvaliacaoAgendamento
});

var VwAvaliacaoConjuntoRelacaoCollection = Backbone.Collection.extend({
    model: VwAvaliacaoConjuntoRelacao,
    url: '/default/gerencia-prova/retorna-provas'
});

var VwHistoricoAgendamentoCollection = Backbone.Collection.extend({
    model: VwHistoricoAgendamento
});

var VwDisciplinasAgendamentoCollection = Backbone.Collection.extend({
    model: VwDisciplinasAgendamento
});

collectionAgendamento = new VwAvaliacaoAgendamentoCollection();
collectionAbas = new VwAvaliacaoConjuntoRelacaoCollection();
collectionDataProva = new AplicadorProvaCollection();
collectionListaHistorico = new VwHistoricoAgendamentoCollection();
collectionDisciplinas = new VwDisciplinasAgendamentoCollection();


/***********************
 *   GerenciaProvaView
 *   Inicia com a pesquisa do aluno
 **********************/

var GerenciaProvaView = Backbone.Marionette.LayoutView.extend({
    initialize: function () {
        this.template = _.template(tpl);
    },
    regions: {
        'tempPesquisaRegion': '#divPesquisaAlunosAgendamento',
        'tempDadosAlunoRegion': '#listaDeAlunos',
        'tempDadosAgendamentoRegion': '#dadosAgendamentoAluno'
    },
    ui: {
        itemListaAlunos: '.bodyListaAlunos',
        itemHistoricoAluno: '.historicoAluno'
    },
    renderDadosBasicosView: function () {
        this.tempPesquisaRegion.show(new PesquisaAlunosAgendamentoView({}));
    },
    onShow: function () {
        this.renderDadosBasicosView();
        $('#telaPrincipalAgendamento').hide();

    },
    events: {
        'click button.btnPesquisarAlunos': 'renderListagem'
    },
    validate: function () {
        var message = '';

        var nome = $('#st_str').val();
        var data = $('#dt_aplicacao option:selected').val();

        if ((nome == '') && (data == '')) {
            message += 'com dados do aluno ou da aplicação ';
        }

        var sgUf = $('#sg_uf option:selected').val();

        if ((sgUf !== '') && (data == '' || data == undefined)) {
            message += ' uf, aplicador e data da aplicação ';
        }

        if (message) {
            message = 'O(s) campo(s) ' + message + 'precisa(m) ser preenchido(s)';
            $.pnotify({
                'title': 'Erro!',
                'text': message,
                'type': 'error'
            });
            return false;
        }

        return true;
    },
    renderListagem: function () {
        this.ui.itemListaAlunos.html('');
        this.ui.itemHistoricoAluno.hide();
        $('#dadosAgendamentoAluno').hide();
        if (this.validate()) {
            loading();
            var collectionAlunosAgendamento = new VwAlunosAgendamentoCollection();
            var parametros = $("#formPesquisaAlunosAgendamento").serialize();
            collectionAlunosAgendamento.url = url_pesquisaalunos + '?' + parametros;

            this.tempDadosAlunoRegion.show(new listaAlunosAgendamentoView({
                collection: collectionAlunosAgendamento,
                el: '#tableListaAlunos'
            }));
        }
    }
});

//var listaAlunosItemView = Marionette.ItemView.extend({
//    template: _.template('<td class="exibeagendamento" style="cursor: pointer;"><b><%=st_nomecompleto%></b><br /><%=st_projetopedagogico%></td>'),
//    tagName: 'tr',
//    ui: {
//        itemDadosAluno: '.situacaoAgendamentoAluno',
//        itemTopoDadosDoAluno: '.topoDadosBasicosAluno',
//        itemTopoAbas: '.topoAbas',
//        itemDadosAbas: '.dadosAbas'
//    },
//    initialize: function(){
//        if(this.model.get('st_nomecompleto')==''){
//            $('#telaPrincipalAgendamento').hide();
//
//            $.pnotify({
//                'title': 'Aviso!',
//                'text': 'Nenhum Aluno Encontrado!',
//                'type': 'warning'
//            });
//        }else{
//            $('#telaPrincipalAgendamento').show();
//        }
//        $('#dadosAgendamentoAluno').hide();
//    },
//    events: {
//        'click .exibeagendamento': 'situacaoAgendamento'
//    },
//    situacaoAgendamento: function () {
//        matricula = this.model.get('id_matricula');
//        id_usuario = this.model.get('id_usuario');
//        new listaDadosSituacaoAgendamentoAlunoView({
//            model: this.model,
//            el: '#dadosAgendamentoAluno'
//        }).render();
//        $('#dadosAgendamentoAluno').show();
//
//    }
//});

//var listaAlunosAgendamentoView = Marionette.CollectionView.extend({
//    template: '#tableListaAlunos',
//    childView: listaAlunosItemView,
//    childViewContainer: '#bodyListaAlunos',
//    onShow: function () {
//        this.collection.fetch({
//            error: function(){
//                $('#telaPrincipalAgendamento').hide();
//                $.pnotify({
//                    'title': 'Aviso!',
//                    'text': 'Nenhum Aluno Encontrado!',
//                    'type': 'warning'
//                });
//            },
//            complete: function () {
//                 loaded();
//            }
//        });
//    }
//});


var situacaoAlunoItemView = Marionette.ItemView.extend({});

var listaDadosSituacaoAgendamentoAlunoView = Marionette.CompositeView.extend({
    template: '#dadosAgendamentoAluno',
    childView: situacaoAlunoItemView,
    childViewContainer: '#dadosPrincipais',
    onRender: function () {
        this.dadosAba();
        this.dadosBasicosAluno();
        this.dadosPrimeiraAba();
        this.historicoAgendamentoAluno();
    },
    dadosAba: function () {
        var viewAbas = new ListarAbasView({
            collection: collectionAbas,
            el: '#menu-abas'
        });
        viewAbas.render();
    },
    dadosBasicosAluno: function () {
        var templateDados = '<label><strong>Nome:</strong> ' + this.model.get('st_nomecompleto') + '</label>' +
            '<label><strong>Curso:</strong> ' + this.model.get('st_projetopedagogico') + '</label>';
        $('div #div-dados-basicos-aluno').html(templateDados);
    },
    dadosPrimeiraAba: function() {
        collectionAgendamento.url = url_dadosagendamento + '?id_matricula=' + matricula + '&id_tipodeavaliacao=0';
        collectionAgendamento.fetch();

        var viewDados = new dadosAgendamentoView({
            collection: collectionAgendamento,
            el: '#dadosAba'
        });
        viewDados.render();
    },
    historicoAgendamentoAluno: function(){


    }
});

var dadosAgendamentoView = Marionette.CompositeView.extend({
    template: _.template('<div class="tab-content" id="listaDeAbas"></div>'),
    childView: SituacaoAgendamentoView,
    childViewContainer: '#listaDeAbas'
});

var AbasView = Backbone.Marionette.ItemView.extend({
    template: _.template('<a id="id-<%=id_avaliacaorecupera%>-<%=id_avaliacao%>" class="aba" name="id_prova-<%=id_avaliacaorecupera%>" data-toggle="tab" href="avaliacao-<%=id_avaliacao%>"><%=st_avaliacao%></a>'),
    tagName: 'li',
    events: {
        'click .aba': 'controlaTela'
    },
    controlaTela: function () {

        $('#idAvaliacao').val(this.model.get('id_avaliacao'));
        $('#idAvaliacaoRecupera').val(this.model.get('id_avaliacaorecupera'));
        tipoavaliacao = this.model.get('id_avaliacaorecupera');

        collectionAgendamento.url = url_dadosagendamento + '?id_matricula=' + matricula + '&id_tipodeavaliacao=' + this.model.get('id_avaliacaorecupera');
        collectionAgendamento.fetch();
    }
});

var ListarAbasView = Marionette.CompositeView.extend({
    template: _.template('<ul id="ul_menu" class="nav nav-tabs"></ul>'),
    childView: AbasView,
    childViewContainer: '#ul_menu',
    onRender: function () {
        this.collection.fetch({
            beforeSend: function () {
                loading();
            },
            complete: function () {
                loaded();
            }
        });
        this.$el.find('a [name=id_prova-0]').addClass('active');

        this.$el.show();
    }
});

var HistoricoItemView = Backbone.Marionette.ItemView.extend({
    template: _.template(' <td><%=st_tipodeavaliacao%></td>' +
    '<td class="pesquisa-historico"><%=st_situacao%></td>' +
    '<td><%=st_nomecompleto%></td>' +
    '<td><%=dt_cadastro%></td>' +
    '<td><%=st_tramite%></td>'),
    tagName: 'tr'
});

var ListarHistoricoView = Marionette.CompositeView.extend({
    template: _.template(tplFormHistorico),
    childView: HistoricoItemView,
    childViewContainer: '#itens-historico',
    onRender: function () {
        this.listaAnos();
    },
    listaAnos: function() {
        var data = new Date();
        var ano = data.getFullYear();
        var num_anos = ano - 10;
        var options = '<option value="">--</option>';
        for (var i = num_anos; i < ano + 1; i++) {
            options += '<option value="' + i + '">' + i + '</option>';
        }
        $('#id_anos').html(options).show();
    },
    events: {
        'click #btnPesquisaHistorico': 'pesquisaHistorico'
    },
    pesquisaHistorico: function () {

        collectionListaHistorico.url = '/gerencia-prova/historico-agendamento?id_matricula=' + matricula +'&id_situacao='+$('#id_situacao option:selected').val()+'&id_anos='+$('#id_anos option:selected').val();
        collectionListaHistorico.fetch();
        //viewHistoricoAgendamento.render();
    }
});



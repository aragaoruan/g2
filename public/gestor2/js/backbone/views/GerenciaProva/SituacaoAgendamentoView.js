/**
 * Created by Débora Castro on 11/12/14.
 * <debora.castro@unyleya.com.br
 */


$.getScript('/modelo-backbone/Municipio');
$.getScript('/modelo-backbone/VwAvaliacaoAplicacao');

var VwAvaliacaoAplicacaoCollection = Backbone.Collection.extend({
    model: VwAvaliacaoAplicacao
});

var tplDadosAgendamento = '';
$.get('/js/backbone/templates/gerencia-prova/div-dados-agendamento.html?_=' + new Date().getTime(), '', function (data) {
    tplDadosAgendamento = data;
});

var tplAgendar = '';
$.get('/js/backbone/templates/gerencia-prova/tela-agendar.html?_=' + new Date().getTime(), '', function (data) {
    tplAgendar = data;
});

var tplReagendar = '';
$.get('/js/backbone/templates/gerencia-prova/tela-reagendar.html?_=' + new Date().getTime(), '', function (data) {
    tplReagendar = data;
});

var tplLiberar = '';
$.get('/js/backbone/templates/gerencia-prova/tela-liberar.html?_=' + new Date().getTime(), '', function (data) {
    tplLiberar = data;
});

var tplAbonar = '';
$.get('/js/backbone/templates/gerencia-prova/tela-abonar.html?_=' + new Date().getTime(), '', function (data) {
    tplAbonar = data;
});

var tplCancelar = '';
$.get('/js/backbone/templates/gerencia-prova/tela-cancelar.html?_=' + new Date().getTime(), '', function (data) {
    tplCancelar = data;
});


var SituacaoAgendamentoView = Backbone.Marionette.ItemView.extend({
    template: _.template(tplDadosAgendamento),
    onRender: function () {

        if (this.model.get('id_tipodeavaliacao') > 0 ) {
            this.abasPorAluno(this.model.get('id_tipodeavaliacao'));
        } else {
            this.abasPorAluno(this.model.get('id_provasrealizadas'));
        }

        if((this.model.get('id_situacao') == situacaoLiberado || this.model.get('id_situacao') == situacaoAbonado) && this.model.get('bl_provaglobal')==0 ){
            this.mostraDisciplinasAgendadas(this.model.get('id_avaliacaoagendamento'));
        }
    },
    events: {
        'click #btnAgendar': 'Agendamento',
        'click #btnReagendar': 'Reagendamento',
        'click #btnSalvarAgendamento': 'SalvarAgendamento',
        'click #btnSalvarReagendamento': 'SalvarReagendamento',
        'click .pesqPolos': 'PesquisaPolos',
        'click #btnNovoPLocais': 'novaPesquisaPolos',
        'change #uf_agendamento': function () {
            this.renderMunicipioSelect($('#form_agendamento #uf_agendamento').val());
        },
        'click #btnLiberar': 'Liberar',
        'click #btnSalvarLiberacao': 'SalvarLiberacao',
        'click #btnCancelar': 'Cancelar',
        'click #btnSalvarCancelamento': 'CancelarAgendamento',
        'click .CancelarAcao': 'cancelarAcao',
        'click #btnAbonar': 'Abonar',
        'click #btnSalvarAbono': 'SalvarAbono',
        'click #id_provaglobal': 'checkGlobal'

    },
    abasPorAluno: function (ids) {
        $.each($('.aba'), function () {
            var dadosId = $(this).attr('id').split('-');
            if (dadosId[1] > ids) {
                $(this).hide();
            }
        });
        $('#menu-abas').removeClass('hide');
    },
    cancelarAcao: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html('');
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').show();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').show();
    },
    checkGlobal: function() {
        $('#tabela-disciplinas').slideToggle();
    },
    AtualizarTela: function (idAvaliacaoAgendamento) {

        if (idAvaliacaoAgendamento == '' || idAvaliacaoAgendamento == undefined) {
            collectionAgendamento.url = url_dadosagendamento + '?id_matricula=' + matricula + '&id_avaliacaorecupera=' + $('#idAvaliacaoRecupera').val();
        } else {
            collectionAgendamento.url = url_dadosagendamento + '?id_avaliacaoagendamento=' + idAvaliacaoAgendamento;
        }
        collectionAgendamento.fetch();

        collectionListaHistorico.url = '/default/gerencia-prova/historico-agendamento?id_matricula=' + matricula;
        collectionListaHistorico.fetch();


    },
    listarDisciplinas: function() {
        loading();
        var avaliacao = $('#idAvaliacao').val();

        collectionDisciplinas.url = '/default/gerencia-prova/busca-disciplinas?id_avaliacao=' + avaliacao + '&id_matricula=' + matricula;
        collectionDisciplinas.fetch({
            success: function() {
                var lisaDView = new CollectionView({
                    collection: collectionDisciplinas,
                    childViewTagName: 'tr',
                    childViewConstructor: listaDisciplinasAgendarView,
                    el: $('#tabela-disciplinas tbody')
                });
                lisaDView.render();
            },
            complete: function() {
                loaded();
            }});
    },
    mostraDisciplinasAgendadas: function(idAval) {
    $('#span_global').hide();

    collectionDisciplinas.url = '/default/gerencia-prova/lista-disciplinas?id_avaliacaoagendamento=' + idAval;
    loading();
        collectionDisciplinas.fetch({
        success: function() {
            var DisView = new CollectionView({
                collection: collectionDisciplinas,
                childViewTagName: 'label',
                childViewConstructor: listaDisciplinasView,
                el: '#lista_disciplinas'
            });
            DisView.render();
            $('#lista_disciplinas').prepend('<h4>Disciplinas incluídas:</h4>');
        },
        complete: function() {
            loaded();
        }
    });
},
    Agendamento: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html(tplAgendar);
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').hide();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').hide();

        this.renderUfSelect();
    },
    Reagendamento: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html(tplReagendar);
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').hide();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').hide();

        this.renderUfSelect();
    },
    Liberar: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html(tplLiberar);
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').hide();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').hide();

        if(tipoavaliacao>0){
            $('#tabela-disciplinas').hide();
            $('#provaglobal').show();
            this.listarDisciplinas();
        }

    },
    Cancelar: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html(tplCancelar);
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').hide();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').hide();
    },
    Abonar: function () {
        this.$el.find("div.conteudos").find('div #telasDeAcao').html(tplAbonar);
        this.$el.find("div.conteudos").find('div #situacaoAgendamento').hide();
        this.$el.find("div.conteudos").find('div #botoesAgendamento').hide();

        if(tipoavaliacao>0){
            $('#tabela-disciplinas').hide();
            $('#provaglobal').show();
            this.listarDisciplinas();
        }
    },
    renderUfSelect: function () {
        var selected = '';

        var collection = new UfCollection();
        collection.url = 'default/util/retorna-uf/';
        collection.fetch();

        var view = new SelectView({
            el: '#uf_agendamento',
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_uf',
                optionValue: 'sg_uf',
                optionSelected: selected
            }
        });

        view.render();
    },
    renderMunicipioSelect: function (uf) {

        if (uf != '') {

            var MunicipioCollection = Backbone.Collection.extend({
                model: Municipio
            });

            var collection = new MunicipioCollection();
            collection.url = '/default/util/retorna-municipio-agendamento/sg_uf/' + uf;
            collection.fetch();
            $('#form_agendamento #municipio_agendamento').html('<option value="">Todos</option>');

            var view = new SelectView({
                el: $('#form_agendamento #municipio_agendamento'),
                collection: collection,
                childViewOptions: {
                    optionLabel: 'st_municipio',
                    optionValue: 'id_municipio'
                }
            });

            view.render();

        } else {
            $('#form_agendamento #id_municipio').html('<option value=""></option>');
        }
    },
    SalvarLiberacao: function () {
        var that = this;
        if ($('#st_justifliberar').val() === '') {
            $.pnotify({title: 'Aviso!', text: 'Por favor, preencha o campo justificativa!', type: 'warning'});
        } else {
            var bl_prova = 0;

            if ($('#id_provaglobal').is(':checked')) {
                bl_prova = 1;
            }

            var ids_avaconjunto = [];

            $('form#form_liberar table#tabela-disciplinas input:checkbox:checked').each(function (index, currentObject) {
                ids_avaconjunto.push(currentObject.value);
            });

            var avaliacaoAgendamento = new AvaliacaoAgendamento();
            avaliacaoAgendamento.url = url_salvar;
            avaliacaoAgendamento.set('id_situacao', situacaoLiberado);
            avaliacaoAgendamento.set('id_avaliacao', $('#idAvaliacao').val());
            avaliacaoAgendamento.set('id_tipodeavaliacao', $('#idAvaliacaoRecupera').val());
            avaliacaoAgendamento.set('id_matricula', matricula);
            avaliacaoAgendamento.set('st_justificativa', $('#st_justifliberar').val());
            avaliacaoAgendamento.set('bl_provaglobal',bl_prova);
            avaliacaoAgendamento.set('id_usuario',id_usuario);
            avaliacaoAgendamento.set('id_avaliacaoconjuntoreferencia',ids_avaconjunto);

            var save = avaliacaoAgendamento.save();
            save.success(function (reponse) {
                $.pnotify(reponse);
                that.AtualizarTela(reponse.id);
            });
            save.fail(function (reponse) {
                $.pnotify(reponse);
            });

        }
    },
    SalvarAbono: function () {
        var that = this;
        if ($('#st_justifliberar').val() === '') {
            $.pnotify({title: 'Aviso!', text: 'Por favor, preencha o campo justificativa!', type: 'warning'});
        } else {

            var bl_prova = 0;

            if ($('#id_provaglobal').is(':checked')) {
                bl_prova = 1;
            }

            var ids_avaconjunto = [];

            $('form#form_liberar table#tabela-disciplinas input:checkbox:checked').each(function (index, currentObject) {
                ids_avaconjunto.push(currentObject.value);
            });

            var avaliacaoAgendamento = new AvaliacaoAgendamento();
            avaliacaoAgendamento.url = url_salvar;
            avaliacaoAgendamento.set('id_situacao', situacaoAbonado);
            avaliacaoAgendamento.set('id_avaliacao', $('#idAvaliacao').val());
            avaliacaoAgendamento.set('id_tipodeavaliacao', $('#idAvaliacaoRecupera').val());
            avaliacaoAgendamento.set('id_matricula', matricula);
            avaliacaoAgendamento.set('st_justificativa', $('#st_justifliberar').val());
            avaliacaoAgendamento.set('bl_provaglobal',bl_prova);
            avaliacaoAgendamento.set('id_usuario',id_usuario);
            avaliacaoAgendamento.set('id_avaliacaoconjuntoreferencia',ids_avaconjunto);

            var save = avaliacaoAgendamento.save();
            save.success(function (reponse) {
                $.pnotify(reponse);
                that.AtualizarTela(reponse.id);
            });
            save.fail(function (reponse) {
                $.pnotify(reponse);
            });

        }
    },
    SalvarAgendamento: function () {
        var that = this;

        if ($('#stJustificativa').val() === '') {
            $.pnotify({title: 'Aviso!', text: 'Por Favor preencha a justificativa!', type: 'warning'});
        } else {

            this.model.url = '/api/gerencia-prova';
            this.model.set('id_situacao', situacaoAgendamento);
            this.model.set('id_avaliacaoaplicacao', $('input[name=id_avaliacaoaplicacao]:checked', '#form_agendamento').val());
            this.model.set('st_justificativa', $('#stJustificativa').val());

            var save = this.model.save();

            save.success(function (reponse) {
                $.pnotify(reponse);
                that.AtualizarTela(reponse.id);
            });
            save.fail(function (reponse) {
                $.pnotify(reponse);
            });
        }
    },
    SalvarReagendamento: function () {
        var that = this;
        if ($('#stJustificativa').val() === '') {
            $.pnotify({title: 'Aviso!', text: 'Por Favor preencha a justificativa!', type: 'warning'});
        } else {

            this.model.url = '/api/gerencia-prova';
            this.model.set('id_situacao', situacaoReagendamento);
            this.model.set('id_avaliacaoaplicacao', $('input[name=id_avaliacaoaplicacao]:checked', '#form_agendamento').val());
            this.model.set('st_justificativa', $('#stJustificativa').val());

            var save = this.model.save();

            save.success(function (reponse) {
                $.pnotify(reponse);
                that.AtualizarTela(reponse.id);
            });
            save.fail(function (reponse) {
                $.pnotify(reponse);
            });
        }
    },
    CancelarAgendamento: function () {
        var that = this;

        if ($('#st_justifcancelamento').val() === '') {
            $.pnotify({title: 'Aviso!', text: 'Por favor, preencha o campo justificativa!', type: 'warning'});
        } else {
            var avaliacaoAgendamento = new AvaliacaoAgendamento();
            avaliacaoAgendamento.url = url_salvar;
            avaliacaoAgendamento.set('id_situacao', situacaoCancelado);
            avaliacaoAgendamento.set('id_avaliacao', $('#idAvaliacao').val());
            avaliacaoAgendamento.set('id_matricula', matricula);
            avaliacaoAgendamento.set('st_justificativa', $('#st_justifcancelamento').val());

            var save = avaliacaoAgendamento.save();
            save.success(function (reponse) {
                $.pnotify(reponse);
                that.AtualizarTela(reponse.id);
            });
            save.fail(function (reponse) {
                $.pnotify(reponse);
            });

        }
    },
    PesquisaPolos: function () {
        var uf = $('#uf_agendamento').val();
        var municipio = $('#municipio_agendamento').val();

        var colletionLocais = new VwAvaliacaoAplicacaoCollection();
        colletionLocais.url = '/default/gerencia-prova/busca-avaliacoes?sg_uf=' + uf + '&id_municipio=' + municipio;

        colletionLocais.fetch({
                success: function () {
                    if (colletionLocais.length) {

                        $('#fieldLocalProva').removeClass('hide');
                        $('#fieldPesquisaPolo').addClass('hide');
                        $('#div_justificativa').removeClass('hide');

                        var comboView1 = new CollectionView({
                            collection: colletionLocais,
                            childViewTagName: 'tr',
                            childViewConstructor: ListaLocaisProvaView,
                            el: $('#tabela-retorno-pesquisa-agendamento tbody')
                        });
                        comboView1.render();
                    } else {
                        $('#tabela-retorno-pesquisa-agendamento tbody').html('<td colspan="4">Não foram encontrados locais ativos!</td>')

                    }
                }
            }
        );
    },
    novaPesquisaPolos: function () {
        $('#fieldLocalProva').addClass('hide');
        $('#fieldPesquisaPolo').removeClass('hide');
        $('#div_justificativa').addClass('hide');
        $('#tabela-retorno-pesquisa-agendamento tbody').html('');
    }
});


var ListaLocaisProvaView = Backbone.View.extend({
    initialize: function () {
        this.render = _.bind(this.render, this);
    },
    render: function () {
        var variables = this.model.toJSON();
        var temp;
        temp = _.template('    <td class="span1"><input type="radio" value="<%=id_avaliacaoaplicacao%>" id="id_avaliacaoaplicacao" name="id_avaliacaoaplicacao"></td>' +
        '<td class="span3"><%=st_aplicadorprova%></td>' +
        '<td class="span4"><%=st_endereco%></td>' +
        '<td class="span1"><%=sg_uf%></td>' +
        ' <td class="span2"><%=dt_aplicacao%></td>' +
        '<td class="span1"><%=hr_inicioprova%></td>' +
        ' <td class="span1"><%=hr_fimprova%></td>', variables);
        this.$el.html(temp);
        return this;
    }
});

var listaDisciplinasView = Backbone.View.extend({
    initialize: function() {
        this.render = _.bind(this.render, this);
        this.$el.css('color', '#f44');
    },
    render: function() {
        var temp;
        var variables = this.model.toJSON();
        temp = _.template(('<%=st_disciplina%>'), variables);
        this.$el.html(temp);
        return this;
    }
});

var listaDisciplinasAgendarView = Backbone.View.extend({
    initialize: function() {
        this.render = _.bind(this.render, this);
    },
    render: function() {
        var temp;
        var variables = this.model.toJSON();
        var html = ' <td class="span1"><input type="checkbox" id="id_avaliacaoconjuntoreferencia" name="id_avaliacaoconjuntoreferencia" value="<%=id_avaliacaoconjuntoreferencia%>"></td>' +
        '<td class="span4"><%=st_disciplina%></td>' +
        '<td class="span2"><%=st_status%></td>';
        temp = _.template(html, variables);
        this.$el.html(temp);
        return this;
    }
});

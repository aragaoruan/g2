/**
 * Created by UNYLEYA on 13/10/14.
 */


/***********************
 *   imports
 **********************/

$.getScript('js/jquery.alphanumeric.pack.js');
$.getScript('/modelo-backbone/VwProjetoEntidade');
$.getScript('/modelo-backbone/VwEntidadeClasse');
$.getScript('/modelo-backbone/VwMatriculaLancamento');
$.getScript('/modelo-backbone/VwMatriculaEntregaDocumento');
$.getScript('/modelo-backbone/VwMatriculaDisciplina');
$.getScript('/modelo-backbone/MatriculaCertificacao');
$.getScript('/modelo-backbone/VwMatricula');
$.getScript('/modelo-backbone/TextoSistema');
$.getScript('js/backbone/views/Util/modal_basica.js');
$.getScript('js/MensagensG2.js');

//Conferindo se há a permissão para edição da data concluinte.
var permissaoEditarDataConcluinte = false;

VerificarPerfilPermissao(30, 739, true).done(function(resp){
    if(resp.length) {
        permissaoEditarDataConcluinte = true;
    }
});

var viewModal = false;

/**
 * Declaracao para a variavel ultilizada para comportar o template renderizado da composite view
 * da grid, necessario para limpar os eventos criados em cada busca.
 */
var ListaAlunosComposite = null;

var EvolucaoCertificado = {
    EVOLUCAO_CERT_NAO_APTO: 60,
    EVOLUCAO_CERT_APTO: 61,
    EVOLUCAO_CERT_GERADO: 62,
    EVOLUCAO_CERT_ENVIADO_CERTIFICADORA: 63,
    EVOLUCAO_CERT_RETORNADO_CERTIFICADORA: 64,
    EVOLUCAO_CERT_ENVIADO_ALUNO: 65
};

//Setando atributo que sera utilizado para verificar se registro necessita ser editado ou nao
VwMatricula.isUpdateMatriculaCertificacao = false;

var VwMatriculaCollection = Backbone.Collection.extend({
    model: VwMatricula,
    async: false,
    url: '/default/conclusao/retorna-alunos-concluintes'
});

var DocumentosCollection = Backbone.Collection.extend({
    model: VwMatriculaEntregaDocumento,
    async: false,
    url: '/default/conclusao/retorna-documentacao-aluno-secretaria'
});

var DetalhesFinanceirosCollection = Backbone.Collection.extend({
    model: VwMatriculaLancamento,
    async: false,
    url: '/default/conclusao/retorna-documentacao-aluno-financeiro'
});


var DetalhesPedagogicosCollection = Backbone.Collection.extend({
    model: VwMatriculaDisciplina,
    async: false,
    url: '/default/conclusao/retorna-documentacao-aluno-pedagogico'
});

var MatriculaCertificacaoCollectionExtend = Backbone.Collection.extend({
    model: MatriculaCertificacao
});


var tplFormPesquisa = '';
$.get('/js/backbone/templates/conclusao/form-pesquisa-alunos-conclusao.html?_=' + new Date().getTime(), '', function (data) {
    tplFormPesquisa = data;
});

var tplResutladoPesquisa = '';
$.get('/js/backbone/templates/conclusao/rows-pesquisa-alunos-conclusao.html', function (response) {
    tplResutladoPesquisa = response;
});

var tplRowsAlunos = '';
$.get('/js/backbone/templates/conclusao/rows-alunos-concluintes.html?_=' + new Date().getTime(), '', function (data1) {
    tplRowsAlunos = data1;
});


var PesquisaAlunosConclusaoView = Backbone.Marionette.ItemView.extend({
    initialize: function () {
        this.template = _.template(tplFormPesquisa);
    },
    ui: {
        selectDocumentacao: '#bl_documentacao',
        selectEvolucaoCertificacao: '#id_evolucaocertificacao',
        divDataCertificacao: '#div-dt-certificado',
        divDataEnvioAoAluno: '#div-dt-envio-aluno',
        divDataRetornoCertificadora: '#div-dt-retorno-certificadora',
        divDataEnviadoCertificadora: '#div-dt-enviado-certificadora'
    },
    events: {
        // 'click #btGeraCertificado' : function () {
        //     viewModal.exibirDadosCertificado();
        // },
        'submit #pesquisaAlunosConlusao': 'renderListagem',
        'change #id_entidade': function () {
            this.renderProjetoSelect($('#pesquisaAlunosConlusao #id_entidade').val());
            $('#pesquisaAlunosConlusao #id_projetopedagogico').html('<option></option>');
        },
        'change @ui.selectEvolucaoCertificacao': 'changeEvolucao'
    },
    onShow: function () {
        this.renderEntidadeSelect();
    },
    changeEvolucao: function (e) {
        var id_evolucao = $(e.currentTarget).val();
        this.ui.divDataCertificacao.addClass('hide');
        this.ui.divDataEnvioAoAluno.addClass('hide');
        this.ui.divDataRetornoCertificadora.addClass('hide');
        this.ui.divDataEnviadoCertificadora.addClass('hide');
        $('#btn-gerar-etiquetas').addClass('hide');

        $('.data-extra').val('');

        if (id_evolucao == EvolucaoCertificado.EVOLUCAO_CERT_GERADO) {
            this.ui.divDataCertificacao.removeClass('hide');
        } else if (id_evolucao == EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_ALUNO) {
            this.ui.divDataEnvioAoAluno.removeClass('hide');
        } else if (id_evolucao == EvolucaoCertificado.EVOLUCAO_CERT_RETORNADO_CERTIFICADORA) {
            this.ui.divDataRetornoCertificadora.removeClass('hide');
            //$('#btn-gerar-etiquetas').removeClass('hide');
        } else if (id_evolucao == EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_CERTIFICADORA) {
            this.ui.divDataEnviadoCertificadora.removeClass('hide');
        }
    },
    renderEntidadeSelect: function () {
        var selected = '';

        var EntidadeCollection = Backbone.Collection.extend({
            model: VwEntidadeClasse
        });

        var collection = new EntidadeCollection();
        collection.url = '/default/conclusao/retorna-entidades-filha/';
        collection.fetch();

        var view = new SelectView({
            el: $('#pesquisaAlunosConlusao #id_entidade'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_nomeentidade',
                optionValue: 'id_entidade',
                optionSelected: selected
            }
        });

        //Carregando projetos pedagogicos pela primeira entidade do combo
        if (collection.length) {
            this.renderProjetoSelect(collection.models[0].get('id_entidade'));
        }

        view.render();
    },
    renderProjetoSelect: function (id_entidade) {

        if (id_entidade != '') {

            var VwProjetoEntidadeCollection = Backbone.Collection.extend({
                model: VwProjetoEntidade
            });

            var collection = new VwProjetoEntidadeCollection();
            collection.url = '/default/conclusao/retorna-projeto-entidade/id_entidade/' + id_entidade;
            collection.fetch();
            $('#pesquisaAlunosConlusao #id_projetopedagogico').html('<option value=""></option><option value="">Todos</option>');

            var view = new SelectView({
                el: $('#pesquisaAlunosConlusao #id_projetopedagogico'),
                collection: collection,
                childViewOptions: {
                    optionLabel: 'st_projetopedagogico',
                    optionValue: 'id_projetopedagogico'
                }
            });

            view.render();

        } else {
            $('#pesquisaAlunosConlusao #id_projetopedagogico').html('<option></option>');
        }
    },
    renderListagem: function (e) {
        e.preventDefault();

        if ($('#id_entidade').val() == '') {
            $.pnotify({title: 'Aviso!', text: 'Escolha pelo menos uma Instituiçao!', type: 'warning'});
        } else if ($('#id_evolucaocertificacao').val() == '' && ($('#st_nomecompleto').val() == '') && ($('#id_projetopedagogico').val() == '')
            && ($('#dt_concluinte_inicio').val() == '') && ($('#dt_concluinte_fim').val() == '')
        ) {
            $.pnotify({title: 'Aviso!', text: 'Preencha o nome, projeto pedagógico, data concluinte ou altere a evolução do certificado antes de realizar a pesquisa.', type: 'warning'});
            return false;
        } else {

            $dados = $('#pesquisaAlunosConlusao').serialize();
            $dados += '&bl_disciplinacomplementar=0';

            var collectionAlunosConcluintes = new VwMatriculaCollection();
            collectionAlunosConcluintes.url = collectionAlunosConcluintes.url + '?' + $dados;
            collectionAlunosConcluintes.fetch({
                beforeSend: loading,
                success: function () {

                    //Verifica se a composite ja foi gerada antes para limpar os eventos anteriores
                    if (ListaAlunosComposite) {
                        ListaAlunosComposite.undelegateEvents();
                    }
                    //mostra a pesquisa apenas se a entidade selecionada no combo Instituçao tiver a sigla da entidade configurada ou se a pesquisa retornar vazia
                    if (!collectionAlunosConcluintes.length || (collectionAlunosConcluintes.length && collectionAlunosConcluintes.models[0].get('st_siglaentidade'))) {
                        ListaAlunosComposite = new ListaAlunosCompositeView({
                            collection: collectionAlunosConcluintes,
                            el: '#containerCompositeViewAlunosConclusao'
                        }).render();
                    } else {
                        $('#containerCompositeViewAlunosConclusao').html('');
                        $.pnotify({title: 'Aviso!', text: 'Essa entidade não tem sigla de certificado configurada!', type: 'warning'});
                    }
                    loaded();
                }
            });
        }
    }
});

/***********************
 *   views responsaveis pelo detalhamento dos documentos.
 **********************/
var DocumentosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template(' <td><%=st_documentos%></td> \n\
                           <td><%=st_situacao%></td>'),
    onRender: function () {
    }
});

var DocumentosFinanceirosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template(' <td>R$ <%=nu_valor%></td> \n\
                           <td><%=dt_vencimento%></td>\n\
                           <td><%=st_meiopagamento%></td>'),
    onRender: function () {
    }
});

var DocumentosPedagogicosItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template(' <td><%=st_disciplina%></td> \n\
                           <td><%=st_evolucao%></td>'),
    onRender: function () {
    }
});

var ListaDocumentacaoSecretariaCollectionView = Marionette.CollectionView.extend({
    template: '#tableSecretaria',
    childView: DocumentosItemView,
    childViewContainer: 'tbody',
    onRender: function () {
        this.collection.fetch();
    }
});

var ListaDocumentacaoFinanceiroCollectionView = Marionette.CollectionView.extend({
    template: '#tableFinanceiro',
    childView: DocumentosFinanceirosItemView,
    childViewContainer: 'tbody',
    onRender: function () {
        this.collection.fetch();

    }
});

var ListaDocumentacaoPedagogicoCollectionView = Marionette.CollectionView.extend({
    template: '#tablePedagogico',
    childView: DocumentosPedagogicosItemView,
    childViewContainer: 'tbody',
    onRender: function () {
        this.collection.fetch();

    }
});

/***********************
 *   Views responsáveis pela listagem dos alunos
 **********************/

var AlunosConcluintesItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    ui: {
        checkEnvioCertificado: '.chk-enviocertificado',
        checkRetornoCertificadora: '.chk-retornocertificadora',
        tdRetornoCertificadora: '.td-retornocertificadora',
        tdEnvioCertificado: '.td-enviocertificado',
        inputDtEnvioAluno: '.input-dt-envio-aluno',
        inputCodigoAcompanhamento: '.input-codigo-acompanhamento',
        tdEnvioAluno: '.td-envioaluno',
        dtConcluinteExibicao: '#dt-concluinte-exibicao',
        divEdit: '#div-edit'
    },
    template: _.template(tplResutladoPesquisa),
    events: {
        'click .modalCertificacao': 'modalCertificacaoAluno',
        'change .chk-enviocertificado': 'checkEnvioCertificado',
        'change .chk-retornocertificadora': 'checkRetornoCertificadora',
        'change input[type=text]': 'inputDtEnvioAlunoCodigoAcompanhamento',
        'keyup .input-codigo-acompanhamento': 'inputDtEnvioAlunoCodigoAcompanhamento',
        'click .btn-reload-item': 'removeItemView',
        'click .btn-editdataconcluinte' : 'editarDataConcluinte',
        'click .btn-saveeditdataconcluinte' : 'salvarEditDataConcluinte',
        'change .edit-data-conclusao': 'validaDataEncerramento'
    },
    onRender: function(){
        if (permissaoEditarDataConcluinte) {
            //Remove a exibição da data concluinte e exibe o input de edição se houver permissão.
            $(this.ui.dtConcluinteExibicao).remove();
        } else {
            //Remove o input de edição exibe apenas a data concluinte se não houver permissão.
            $(this.ui.divEdit).remove();
        }
    },
    salvarEditDataConcluinte: function(e){

        var dtString = converteStringDataBRToUSA($(e.currentTarget).prev().val());
        $.ajax({
            type: 'POST',
            url: '/conclusao/salvar-edicao-data-conclusao',
            data: {
                'id_matricula': this.model.get('id_matricula'),
                'dt_concluinte': dtString
            },
            beforeSend: function(){
                loading();
            },
            success: function(){
                //Fecha o combo pra edição e altera os elementos visuais.
                var $button = $(e.currentTarget);
                var $input = $(e.currentTarget).prev();

                $button.removeClass('btn-success btn-saveeditdataconcluinte').addClass('btn-editdataconcluinte');
                $button.children().removeClass('icon-ok icon-white').addClass('icon-edit');
                $input.prop('disabled', true);

            },
            complete: function(){
                $.pnotify({
                    'type': 'success',
                    'text': 'Data concluinte alterada com sucesso.',
                    'title': 'Sucesso'
                });
                loaded();
            }
        });
    },
    validaDataEncerramento: function(e) {
        var stringData = $(e.currentTarget).val();
        var $button = $(e.currentTarget).next();

        /*Caso data válida, altera a aparência do botão e adiciona
         * a classe necessária para que seja possível usar a função de salvar. */

        if (validaDataBR(stringData)) {
            $button.addClass('btn-success');
            $button.children().removeClass('icon-edit').addClass('icon-ok icon-white');
            $button.removeClass('btn-editdataconcluinte').addClass('btn-saveeditdataconcluinte');
        }
    },
    editarDataConcluinte: function(e){
        //Habiitar o input
        $(e.currentTarget).prev().prop('disabled', false);
    },
    modalCertificacaoAluno: function () {
        if (viewModal) {
            viewModal.model = this.model.clone();
        } else {
            viewModal = new modalAlunoConclusaoView({
                el: $("#modalAlunoConclusao").find(".modal-body"),
                model: this.model.clone()
            });
        }
        viewModal.render();
    },
    checkEnvioCertificado: function () {
        if (this.ui.checkEnvioCertificado.is(':checked')) {
            var date = new Date();
            this.model.set('dt_enviocertificado', getTodayDate().join('/'));
            this.model.set('id_usuarioenviocertificado', G2S.loggedUser.get('id_usuario'));
            this.model.set('id_evolucaocertificacao', EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_CERTIFICADORA);
            this.model.isUpdateMatriculaCertificacao = true;

            this.ui.tdEnvioCertificado.find('span').text(this.model.get('dt_enviocertificado'));
        } else {
            this.model.set('dt_enviocertificado', '');
            this.model.set('id_evolucaocertificacao', '');
            this.model.isUpdateMatriculaCertificacao = false;

            this.ui.tdEnvioCertificado.find('span').empty();
            this.ui.tdRetornoCertificadora.empty();
        }
    },
    checkRetornoCertificadora: function () {
        if (this.ui.checkRetornoCertificadora.is(':checked')) {
            this.model.set('dt_retornocertificadora', getTodayDate().join('/'));
            this.model.set('id_usuarioretornocertificadora', G2S.loggedUser.get('id_usuario'));
            this.model.set('id_evolucaocertificacao', EvolucaoCertificado.EVOLUCAO_CERT_RETORNADO_CERTIFICADORA);

            this.ui.tdRetornoCertificadora.find('span').text(this.model.get('dt_retornocertificadora'));
            this.model.isUpdateMatriculaCertificacao = true;
        } else {
            this.model.set('dt_retornocertificadora', '');
            this.model.set('id_evolucaocertificacao', '');

            this.ui.tdRetornoCertificadora.find('span').empty();
            this.model.isUpdateMatriculaCertificacao = false;
        }
    },
    inputDtEnvioAlunoCodigoAcompanhamento: function () {
        /**
         * verifica se o campo data foi preenchido e se o campo codigo rastreamento atende
         * as exigencias de vazio ou preenchido com 13 caracters
         */
        var lengthInputCodigo = this.ui.inputCodigoAcompanhamento.val().length;
        this.ui.inputCodigoAcompanhamento.focus();

        //Validadores dos campos de codigo do acompanhamento e data de envio
        this.validaCampoCodigoAcompanhamento();
        this.validaCampoDtEnvioAluno();

        //13 e o tamanho do codigo de rastreamento dos correios
        if (this.ui.inputDtEnvioAluno.val() && (lengthInputCodigo == 13 || lengthInputCodigo == 0 )) {
            this.model.set('dt_envioaluno', this.ui.inputDtEnvioAluno.val());
            this.model.set('st_codigoacompanhamento', this.ui.inputCodigoAcompanhamento.val());
            this.model.set('id_usuarioenvioaluno', G2S.loggedUser.get('id_usuario'));
            this.model.set('id_evolucaocertificacao', EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_ALUNO);

            //Remove o label de informacao do usuario de obrigatoriedade de preenchimento
            this.ui.tdEnvioAluno.find('.item-informe')
                .empty()
                .removeClass('label label-important');
            this.model.isUpdateMatriculaCertificacao = true;
        }

        else {
            //Adiciona um label de informacao ao usuario de obrigatoriedade de preenchimento
            this.ui.tdEnvioAluno.find('.item-informe')
                .empty()
                .addClass('label label-important')
                .text('Verifique os campos obrigatórios');
            this.model.isUpdateMatriculaCertificacao = false;
        }
    },
    removeItemView: function () {
        this.model.destroy();
    },
    validaCampoCodigoAcompanhamento: function () {
        this.ui.inputCodigoAcompanhamento.alphanumeric();

        var length = this.ui.inputCodigoAcompanhamento.val().length;
        if (length != 0 && length < 13) { //campo invalido, digitado e menor que 13
            this.ui.tdEnvioAluno.find('.el-codigoacompanhamento')
                .removeClass('btn-success')
                .addClass('btn-warning');
            this.ui.tdEnvioAluno.find('.el-codigoacompanhamento i')
                .removeClass('icon-ok')
                .addClass('icon-info-sign');
        } else if (length == 0 || length == 13) {
            this.ui.tdEnvioAluno.find('.el-codigoacompanhamento')
                .removeClass('btn-warning')
                .addClass('btn-success');
            this.ui.tdEnvioAluno.find('.el-codigoacompanhamento i')
                .removeClass('icon-info-sign')
                .addClass('icon-ok');
        }
        //comentado porque o rastremento pode ser salvo como vazio
        /*else {
         this.ui.tdEnvioAluno.find('.el-codigoacompanhamento')
         .removeClass('btn-success')
         .addClass('btn-warning');
         this.ui.tdEnvioAluno.find('.el-codigoacompanhamento i')
         .removeClass('icon-ok')
         .addClass('icon-info-sign');
         }*/
    },
    validaCampoDtEnvioAluno: function () {

        if (this.ui.inputDtEnvioAluno.val()) {
            this.ui.tdEnvioAluno.find('.el-dtenvioaluno')
                .removeClass('btn-warning')
                .addClass('btn-success');
            this.ui.tdEnvioAluno.find('.el-dtenvioaluno i')
                .removeClass('icon-info-sign')
                .addClass('icon-ok');
        } else {
            this.ui.tdEnvioAluno.find('.el-dtenvioaluno')
                .removeClass('btn-success')
                .addClass('btn-warning');
            this.ui.tdEnvioAluno.find('.el-dtenvioaluno i')
                .removeClass('icon-ok')
                .addClass('icon-info-sign');
        }
    }
});

var AlunosConcluintesItemViewEmpty = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td colspan="13" class="text-center">Nenhum Aluno Encontrado!</td>')
});

var ListaAlunosCompositeView = Marionette.CompositeView.extend({
    template: '#templateGridAlunosConclusao',
    childView: AlunosConcluintesItemView,
    childViewContainer: '#tbodyAlunosConcluintes',
    emptyView: AlunosConcluintesItemViewEmpty,
    ui: {
        btnSalvarProcessoCertificacao: '.btn-salvar-processo-certificacao',
        checkAllEnvioCertificado: '#check-all-envio-certificadora',
        checkAllRetornoCertificadora: '#check-all-retorno-certificadora',
        checkRetornoCertificadora: '.chk-retornocertificadora',
        checkEnvioCertificadora: '.chk-enviocertificado',
        btnGerarXls: '#btn-gerar-xls',
        btnGerarEtiquetas: '#btn-gerar-etiquetas'
    },
    onRender: function () {
        /**
         * Criando datepicker customizado para os campos data de envio do aluno
         */
        $('.datepickerConclusao').datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: '-1y',
            endDate: new Date()
        });

        $('.item-popover').popover({trigger: 'hover'});

        if ($('#id_evolucaocertificacao').val() == EvolucaoCertificado.EVOLUCAO_CERT_RETORNADO_CERTIFICADORA) {
            this.ui.btnGerarEtiquetas.removeClass('hide');
        } else {
            this.ui.btnGerarEtiquetas.addClass('hide');
        }

        this.ui.btnGerarXls.hide();

        if (this.collection.length) {
            this.ui.btnGerarXls.show();

            /**
             * Verificand necessidade de habilitar checkbox Checkall para retorno e envio de certificado
             */
            var boolCheckAllEnvio = this.collection.where({
                id_evolucaocertificacao: EvolucaoCertificado.EVOLUCAO_CERT_GERADO
            }).length;

            var boolCheckAllRetorno = this.collection.where(
                {id_evolucaocertificacao: EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_CERTIFICADORA}).length;

            var boolRetornoCertificadora = this.collection.where(
                {id_evolucaocertificacao: EvolucaoCertificado.EVOLUCAO_CERT_RETORNADO_CERTIFICADORA}).length;


            if (boolCheckAllEnvio) {
                this.ui.checkAllEnvioCertificado.show();
            }

            if (boolCheckAllRetorno) {
                this.ui.checkAllRetornoCertificadora.show();
            }

            //Habilita botao salvar do processo de certificacao
            if (boolCheckAllEnvio || boolCheckAllRetorno || boolRetornoCertificadora) {
                this.ui.btnSalvarProcessoCertificacao.show();
            }
        }
    },
    events: {
        'click .btn-salvar-processo-certificacao': 'salvarProcessoCertificacao',
        'change #check-all-envio-certificadora': 'checkAllEnvioCertificadora',
        'change #check-all-retorno-certificadora': 'checkAllRetornoCertificadora',
        'click @ui.btnGerarXls': 'gerarXlsResultados',
        'click @ui.btnGerarEtiquetas': 'gerearEtiquetasXls'
    },
    salvarProcessoCertificacao: function () {
        var data = [];
        var that = this;

        //Montando a lista de dados que serão enviados para o metodo que salvar evolucoes da certificacao
        this.collection.each(function (VwMatricula) {
            if (VwMatricula.isUpdateMatriculaCertificacao && VwMatricula.get('id_indicecertificado')) {
                data.push({
                    id_indicecertificado: VwMatricula.get('id_indicecertificado'),
                    id_entidade: VwMatricula.get('id_entidade'),
                    id_matricula: VwMatricula.get('id_matricula'),
                    nu_ano: VwMatricula.get('nu_ano'),
                    id_usuarioenviocertificado: VwMatricula.get('id_usuarioenviocertificado'),
                    id_usuarioretornocertificadora: VwMatricula.get('id_usuarioretornocertificadora'),
                    id_usuarioenvioaluno: VwMatricula.get('id_usuarioenvioaluno'),
                    id_usuariocadastro: VwMatricula.get('id_usuariocadastro'),
                    dt_cadastro: VwMatricula.get('st_dtcadastrocertificacao'),
                    dt_enviocertificado: VwMatricula.get('dt_enviocertificado'),
                    dt_retornocertificadora: VwMatricula.get('dt_retornocertificadora'),
                    dt_envioaluno: VwMatricula.get('dt_envioaluno'),
                    bl_ativo: VwMatricula.get('bl_ativo'),
                    st_siglaentidade: VwMatricula.get('st_siglaentidade'),
                    st_codigoacompanhamento: VwMatricula.get('st_codigoacompanhamento'),
                    id_evolucaocertificacao: VwMatricula.get('id_evolucaocertificacao')
                });
            }
        });

        if (!data.length) {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_CONCLUSAO_04, type: 'warning'});
            return false;
        }

        $.ajax({
            url: '/conclusao/salvar-list-processo-matricula-certificacao',
            type: 'post',
            data: {data: data},
            beforeSend: loading(),
            success: function () {
                _.each(data, function (model) {
                    //Disparando um gatilho para atualizacao do itemView gerado a certificacao
                    if ($('#id_evolucaocertificacao').val() != 65) // quando ele tem evolucao 65 (Enviado ao Aluno) ele atualiza apenas o codigo de rastreamento
                        that.$el.find('#btn-remove-' + model.id_matricula).trigger('click');
                });
            },
            complete: function (response) {

                //Verificando
                if (that.$el.find('.item-informe').hasClass('label label-important')) {
                    $.pnotify({title: 'Alerta', text: MensagensG2.MSG_CONCLUSAO_06, type: 'warning'});
                }
                $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_CONCLUSAO_05, type: 'success'});
                that.ui.checkAllEnvioCertificado.removeAttr('checked');
                that.ui.checkAllRetornoCertificadora.removeAttr('checked');
                loaded();
            },
            error: function () {
                $.pnotify({
                    title: 'Erro',
                    text: sprintf(MensagensG2.MSG_ERROR_CUSTOM, 'executar esta operação'),
                    type: 'danger'
                });
                loaded();
            }
        });
    },
    checkAllEnvioCertificadora: function () {

        if (this.$el.find('.chk-enviocertificado').length) {
            loading();

            if (this.ui.checkAllEnvioCertificado.is(':checked')) {
                this.$el.find('.chk-enviocertificado').prop('checked', true);
                this.$el.find('.td-enviocertificado span').text(getTodayDate().join('/'));

                this.collection.each(function (model) {
                    if (!model.get('st_dtenviocertificado') && model.get('st_dtcadastrocertificacao')) {
                        model.set('id_usuarioenvioaluno', G2S.loggedUser.get('id_usuario'));
                        model.set('id_evolucaocertificacao', EvolucaoCertificado.EVOLUCAO_CERT_ENVIADO_CERTIFICADORA);
                        model.set('dt_enviocertificado', getTodayDate().join('/'));
                        model.isUpdateMatriculaCertificacao = true;
                    }
                });

            } else {
                this.$el.find('.chk-enviocertificado').removeAttr('checked');
                this.$el.find('.td-enviocertificado span').empty();

                this.collection.each(function (model) {
                    if (model.isUpdateMatriculaCertificacao) {
                        model.isUpdateMatriculaCertificacao = false;
                    }
                });
            }
        }

        loaded();
    },
    checkAllRetornoCertificadora: function () {

        if (this.$el.find('.chk-retornocertificadora').length) {
            loading();

            if (this.ui.checkAllRetornoCertificadora.is(':checked')) {
                this.$el.find('.chk-retornocertificadora').prop('checked', true);
                this.$el.find('.td-retornocertificadora span').text(getTodayDate().join('/'));

                this.collection.each(function (model) {
                    if (!model.get('st_dtretornocertificadora') && model.get('st_dtenviocertificado')) {

                        model.set('id_usuarioretornocertificadora', G2S.loggedUser.get('id_usuario'));
                        model.set('id_evolucaocertificacao', EvolucaoCertificado.EVOLUCAO_CERT_RETORNADO_CERTIFICADORA);
                        model.set('dt_retornocertificadora', getTodayDate().join('/'));
                        model.isUpdateMatriculaCertificacao = true;
                    }
                });

            } else {
                this.$el.find('.chk-retornocertificadora').removeAttr('checked');
                this.$el.find('.td-retornocertificadora span').empty();

                this.collection.each(function (model) {
                    if (model.isUpdateMatriculaCertificacao) {
                        model.isUpdateMatriculaCertificacao = false;
                    }
                });
            }
        }

        loaded();
    },
    gerarXlsResultados: function () {
        var url = '/default/conclusao/retorna-alunos-concluintes?' + $('#pesquisaAlunosConlusao').serialize() + '&bl_disciplinacomplementar=0&xls=true';

        var win = window.open(url, "_blank");

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador não permitiu abrir o popup.'
            });
        }
    },
    gerearEtiquetasXls: function () {
        var url = '/default/conclusao/retorna-alunos-concluintes?' + $('#pesquisaAlunosConlusao').serialize() + '&bl_disciplinacomplementar=0&xls_etiquetas=true';

        var win = window.open(url, "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=360, height=200, top=0, left=0");

        if (!win) {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Seu navegador não permitiu abrir o popup.'
            });
        }
    }
});

/***********************
 *   View da modal com os detalhes de documentação e geração de certificado de cada aluno
 **********************/

var modalAlunoConclusaoView = Marionette.CompositeView.extend({
    // props: {
    //     certificado : {
    //         id_impressaocertificado: null,
    //         st_textocertificado: null
    //     }
    // },
    template: _.template(tplRowsAlunos),
    model: VwMatricula,
    // ui: {
    //     btEmitirCertificado      : '#btEmitirCertificado',
    //     btVisualizarCertificado  : '#btVisualizarCertificado',
    //     btCancelaCertificado     : '#btCancelarCertificado',
    //     selectModeloCertificado  : '#id_textosistema'
    // },
    // events: {
    //     'click #btVisualizarCertificado' : 'visualizaNovoCertificado',
    //     'click #btCancelarCertificado'   : 'ocultarFieldCertificado'
    // },
    onRender: function () {
        var that = this;

        this.verificarStatus();

        //Verifica se existe certificado para o aluno.
        // this.buscaCertificadoExistente();

        //Verifica o status da documentação do aluno.
        // this.verificarStatusDocumentacaoAluno();

        $("#modalAlunoConclusao").modal('show');
        $('.informacoesDetalhar').popover();

        if (!this.model.get('bl_documentacao')) {
            $('#dadosCertificado').html('');
            $('#dadosCertificado').html('<div class="alert alert-block">' +
                '<h4>Aviso!</h4>Apenas alunos com documentação regularizada podem emitir certificado!</div>');
        } else if (!this.model.get('bl_academico')) {
            $('#dadosCertificado').html('');
            $('#dadosCertificado').html('<div class="alert alert-block">' +
                '<h4>Aviso!</h4>Apenas alunos sem pendências acadêmicas podem emitir certificado!</div>');
        } else {
            this.renderTextoSistemaSelect();
        }

        loading();

        var ComplementaresCollection = new VwMatriculaCollection();
        ComplementaresCollection.url =
            '/conclusao/retorna-disciplinas-complementares'
            + '/id_usuario/' + this.model.get('id_usuario');
        ComplementaresCollection.fetch({async: false});

        (new DisciplinaComplementarCompositeViewExtend({
            collection: ComplementaresCollection,
            el: '#container-disciplinas-complementares'
        })).render();

        loaded();

        return this;
    },
    onClose: function () {
        this.unbind();
    },
    events: {
        'click #btEmitirCertificado': function () {
            var that = this;
            if ($('#id_textosistema').val() == '') {
                $.pnotify({
                    title: 'Atenção',
                    text: 'Selecione um modelo para emissão de certificado!',
                    type: 'warning'
                });
            } else {
                this.certificarAluno().done(function () {
                    that.abreCertificado();
                });
                return false;
            }
        },
        'click #btVisualizarCertificado': function (e) {
            if (!$('#id_textosistema').val()) {
                $.pnotify({
                    title: 'Atenção',
                    text: 'Selecione um modelo para emissão de certificado!',
                    type: 'warning'
                });
            } else {
                this.abreCertificado(true);
                return false;
            }
        },
        'click #btCancelarCertificado': 'ocultarFildCertificado'
    },
    abreCertificado: function (draft) {
        window.open('/default/textos/geracao/?filename=Emissão Certificado&id_textosistema=' + $('#id_textosistema').val() + '&arrParams[id_matricula]=' + this.model.get('id_matricula') + '&type=html' + (draft ? '&draft=true' : ''), '_blank');
    },
    // buscaCertificadoExistente: function() {
    //     var that = this;
    //     return $.ajax({
    //         url: '/default/conclusao/retorna-certificado-impresso',
    //         type: 'get',
    //         dataType: 'json',
    //         data: {id_matricula: this.model.get('id_matricula')},
    //         success: function (response) {
    //             that.props.certificado.id_impressaocertificado  = response.id_impressaocertificado;
    //             that.props.certificado.st_textocertificado      = response.st_textocertificado;
    //             //Remove o container com a exibição das disciplinas complementares.
    //             $('#container-disciplinas-complementares').remove();
    //         },
    //         error: function() {
    //             $.pnotify({title : 'Erro',
    //                 text: 'Não foi possível verificar se existe certificado para essa matrícula.',
    //                 type: 'error'});
    //         }
    //     })
    // },
    certificarAluno: function () {
        loading();
        var that = this;
        return $.ajax({
            url: '/default/conclusao/certificar-aluno',
            type: 'post',
            dataType: 'json',
            data: {id_matricula: this.model.get('id_matricula'), id_evolucaocertificacao: this.model.get('id_evolucaocertificacao')},
            success: function (response) {
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            error: function (response) {
                $.pnotify({title: response.title, text: response.text, type: response.type});//mensagem de retorno
            },
            complete: function () {
                //Disparando um gatilho para atualizacao do itemView gerado a certificacao
                $('#btn-remove-' + that.model.get('id_matricula')).trigger('click');
                $('#modalAlunoConclusao').modal('hide');
                loaded();
            }
        });
    },
    ocultarFildCertificado: function () {
        // ocultarFieldCertificado: function () {
        $('#dadosCertificado').hide();
    },
    verificarStatus: function () {
        var that = this;
        $.get('/default/conclusao/retorna-status-documentacao-aluno?id_matricula=' + this.model.get('id_matricula'), function (data) {
            that.listaSecretaria(data['secretaria']);
            that.listaFinanceiro(data['financeiro']);
            that.listaPedagogico(data['pedagogico']);
        });
    },
    // visualizaNovoCertificado: function () {
    //     if (!this.ui.selectModeloCertificado.val()) {
    //         $.pnotify({title: 'Atenção', text: 'Selecione um modelo para emissão de certificado!', type: 'warning'});
    //     } else {
    //         var url = this.retornaURLCertificado(true);
    //         window.open(url, '_blank');
    //     }
    // },
    // verificarStatusDocumentacaoAluno: function () {
    //     var that = this;
    //     $.get('/default/conclusao/retorna-status-documentacao-aluno?id_matricula=' + this.model.get('id_matricula'), function (data) {
    //         that.listaSecretaria(data['secretaria']);
    //         that.listaFinanceiro(data['financeiro']);
    //         that.listaPedagogico(data['pedagogico']);
    //     });
    // },
    listaSecretaria: function (situacao) {
        if (situacao == 'Confirmado') {
            $('#labelSecretariaOk').removeClass('hide');
            $('#labelSecretariaP').addClass('hide');
            $('#detalharSecretraria').hide();

        } else {
            $('#labelSecretariaOk').addClass('hide');
            $('#labelSecretariaP').removeClass('hide');

            var collectionDocumentaoSecretaria = new DocumentosCollection();
            collectionDocumentaoSecretaria.url += '?id_matricula=' + this.model.get('id_matricula');

            new ListaDocumentacaoSecretariaCollectionView({
                collection: collectionDocumentaoSecretaria,
                el: '#tableSecretaria'
            }).render();

        }
    },
    listaFinanceiro: function (situacao) {
        if (situacao == 'Confirmado') {
            $('#labelFinanceiroOk').removeClass('hide');
            $('#labelFinanceiroP').addClass('hide');
            $('#detalharFinanceiro').addClass('hide');

        } else {
            $('#labelFinanceiroOk').addClass('hide');
            $('#labelFinanceiroP').removeClass('hide');

            var collectionDocumentaoFinanceiro = new DetalhesFinanceirosCollection();
            collectionDocumentaoFinanceiro.url += '?id_matricula=' + this.model.get('id_matricula');

            new ListaDocumentacaoFinanceiroCollectionView({
                collection: collectionDocumentaoFinanceiro,
                el: '#tableFinanceiro'
            }).render();

        }
    },
    listaPedagogico: function (situacao) {
        if (situacao == 'Confirmado') {
            $('#labelPedagogicoOk').removeClass('hide');
            $('#labelPedagogicoP').addClass('hide');
            $('#detalharPedagogico').hide();

        } else {
            $('#labelPedagogicoOk').addClass('hide');
            $('#labelPedagogicoP').removeClass('hide');

            var collectionDocumentaoPedagogico = new DetalhesPedagogicosCollection();
            collectionDocumentaoPedagogico.url += '?id_matricula=' + this.model.get('id_matricula');

            new ListaDocumentacaoPedagogicoCollectionView({
                collection: collectionDocumentaoPedagogico,
                el: '#tablePedagogico'
            }).render();


        }
    },
    renderTextoSistemaSelect: function () {
        var selected = '';

        var textoCollection = Backbone.Collection.extend({
            model: TextoSistema
        });

        var collection = new textoCollection();
        collection.url = '/default/conclusao/retorna-textos-certificacao?id_textocategoria=4';
        collection.fetch();

        var view = new SelectView({
            el: $('#dadosCertificado #id_textosistema'),
            collection: collection,
            childViewOptions: {
                optionLabel: 'st_textosistema',
                optionValue: 'id_textosistema',
                optionSelected: selected
            }
        });

        view.render();
    },
    // exibirDadosCertificado: function () {
    //
    //     var that = this;
    //
    //     $('#dadosCertificado').show();
    //
    //     if (this.props.certificado.id_impressaocertificado) {
    //         this.ui.selectModeloCertificado.prop('disabled', true);
    //         this.ui.btVisualizarCertificado.prop('disabled', true);
    //         this.ui.btEmitirCertificado.click(function(){
    //             that.abrirCertificadoSalvo();
    //         });
    //     } else {
    //         this.ui.btEmitirCertificado.click(function(){
    //             that.emitirCertificado();
    //         });
    //     }
    //
    //     //Faz a firula de carregar a visao do conteudo para o final da modal
    //     var body = $('#bodyModal-dadosAluno');
    //     body.animate({scrollTop: 400}, '900');
    //
    // },
    // emitirCertificado: function () {
    //     var that = this;
    //     if (!this.ui.selectModeloCertificado.val()) {
    //         $.pnotify({title: 'Atenção', text: 'Selecione um modelo para emissão de certificado!', type: 'warning'});
    //     } else {
    //         this.certificarAluno().done(function() {
    //
    //             /* Faz um request para a URL de geração do certificado.
    //              * A resposta do request será o HTML do certificado, que é
    //              * passado como parâmetro no método salvarImpressaoCertificado()
    //              * para armazenamento na tb_impressaocertificado. */
    //
    //             var url = that.retornaURLCertificado(false);
    //
    //             $.ajax({
    //                 url : url,
    //                 success : function(response) {
    //
    //                     that.salvarImpressaoCertificado(response);
    //
    //                     var wndObj = window.open(url, "Certificado");
    //
    //                     // Caso o bloqueador de popup do browser esteja ativo, wndObj será NULL.
    //                     if (!wndObj) {
    //                         $.pnotify({title: 'Alerta',
    //                             text: 'Para visualizar o certificado, é necessário desativar o bloqueio de pop-ups no seu navegador.',
    //                             type: 'alert'});
    //                     }
    //                 },
    //                 error : function (response) {}
    //             });
    //
    //         });
    //     }
    // },
    // salvarImpressaoCertificado: function(html) {
    //     $.ajax({
    //         type: 'post',
    //         url: '/conclusao/salvar-impressao-certificado',
    //         data: {
    //             st_textocertificado: html,
    //             id_matricula: this.model.get('id_matricula'),
    //             id_usuario: G2S.loggedUser.get('id_usuario')
    //         },
    //         success: function(response) {},
    //         error: function() {}
    //     })
    // },
    // abrirCertificadoSalvo: function() {
    //     var wndObj = window.open("about:blank", "Certificado");
    //     wndObj.document.write(this.props.certificado.st_textocertificado);
    //     wndObj.document.close();
    // },
    // retornaURLCertificado: function(draft) {
    //     return '/default/textos/geracao/?filename=Emissão Certificado&id_textosistema='
    //         + $('#id_textosistema').val()
    //         + '&arrParams[id_matricula]='
    //         + this.model.get('id_matricula')
    //         + '&type=html' + (draft ? '&draft=true' : '');
    // }
});

var DisciplinaComplementarItemViewExtend = Marionette.ItemView.extend({
    template: _.template('<td><%=id_matricula%></td>\
                <td><%=st_projetopedagogico%></td>\
                <td><%=st_evolucao%></td>\
                <td><center>\
                <% if(!id_matriculavinculada && id_evolucao == 15){ %>\
                    <input type="checkbox" class="chk-vinculo"></center></td>\
                <% } else if(id_evolucao == 15 && id_matriculavinculada){ %>\
                    Já Vinculado\
                <% } else { %> - <% } %>'),
    tagName: 'tr',
    ui: {
        chkVinculo: '.chk-vinculo'
    },
    events: {
        'change .chk-vinculo': 'vincularDisciplina'
    },
    vincularDisciplina: function () {

        if (this.ui.chkVinculo.is(':checked')) {
            this.model.set('id_matriculavinculada', viewModal.model.get('id_matricula'));
            this.model.set('isChecked', true);
        } else {
            this.model.set('id_matriculavinculada', '');
            this.model.set('isChecked', true);
        }
    }
});

var DisciplinaComplementarCompositeViewExtend = Marionette.CompositeView.extend({
    childView: DisciplinaComplementarItemViewExtend,
    template: '#tpl-disciplinas-complementares',
    childViewContainer: 'tbody',
    events: {
        'click button': 'vincularMatriculasComplementares'
    },
    vincularMatriculasComplementares: function () {

        var that = this;
        var collectionSave = this.collection.clone();

        //Filtrando somente as disciplinas checadas para serem vinculadas
        collectionSave.set(that.collection.where({isChecked: true}));

        if (collectionSave.length) {
            Backbone.sync('create', collectionSave, {
                    url: '/conclusao/vincular-disciplinas-complementares',
                    success: function (response) {
                        that.collection.set(response);
                        $.pnotify({title: 'Sucesso', text: MensagensG2.MSG_CONCLUSAO_07, type: 'success'});
                    },
                    error: function (response) {
                        $.pnotify({title: 'Erro', text: MensagensG2.MSG_CONCLUSAO_08, type: 'error'});
                    }
                }
            );
        } else {
            $.pnotify({title: 'Alerta', text: MensagensG2.MSG_CONCLUSAO_04, type: 'warning'});
        }
    }
});

function getTodayDate() {
    var date = new Date();
    var day = date.getDate() <= 9 ? '0' + (date.getDate()) : date.getDate();
    var month = date.getMonth() <= 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);

    return new Array(day, month, date.getFullYear());
}
function fieldCertificado() {
    $('#dadosCertificado').show();

    //Faz a firula de carregar a visao do conteudo para o final da modal
    var body = $('#bodyModal-dadosAluno');
    body.animate({
            scrollTop: 400
        }, '900',
        function () {
        } // callback method use this space how you like
    );
}
loaded();
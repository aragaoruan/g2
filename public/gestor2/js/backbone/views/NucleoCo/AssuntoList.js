var AssuntoListModel = Backbone.Model.extend({
    idAttribute: 'id_assuntoco',
    defaults: {
        id_assuntoco: null,
        st_assuntoco: '',
        bl_ativo: false,
        bl_abertura: false,
        st_situacao: '',
        bl_autodistribuicao: false,
        id_nucleoco: null,
        st_tipoocorrencia: '',
        st_assuntopai: '',
        id_tipoocorrencia: null,
        id_textonotificacao: null,
        id_entidade: null,
        bl_notificaatendente: false,
        id_nucleofinalidade: null,
        id_situacao: null
    },
    url: function() {
        return '/api/nucleo-co-assunto/' + (this.id || '');
    }
});
var AssuntoListView = Marionette.ItemView.extend({
    model: new AssuntoListModel(),
    template: '#assunto-list-view',
    tagName: 'tr'
});
var AssuntoListCollection = Backbone.Collection.extend({
    model: AssuntoListModel,
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});
var AssuntoListComposite = Marionette.CompositeView.extend({
    template: '#assunto-list-template',
    collection: new AssuntoListCollection(),
    childView: AssuntoListView,
    childViewContainer: 'tbody',
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="3">Nenhum assunto encontrado.<br/>Se você alterou o "Tipo" na aba "Núcleo", salve antes de adicionar um assunto.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});
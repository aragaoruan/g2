var EmailAddedModel = Backbone.Model.extend({
    idAttribute: 'id_emailconfig',
    defaults: {
        id_emailconfig: 0,
        id_nucleoco: 0,
        id_entidade: 0,
        adicionar: 0,
        st_titulo: '',
        st_usuario: ''
    },
    url: function() {
        return '/api/vw-nucleo-co-email/' + (this.id || '');
    }
});

var EmailAddedView = Marionette.ItemView.extend({
    model: new EmailAddedModel(),
    template: '#email-added-view',
    tagName: 'tr',
    events: {
        'click .actionDeleteEmail': 'actionDeleteEmail'
    },
    actionDeleteEmail: function() {
        var that = this;
        bootbox.confirm('Deseja realmente remover o e-mail deste núcleo?', function(response) {
            if (response) {
                // Adicionar o parametro id_nucleoco no URL do DELETE
                that.model.url = that.model.url() + ('?id_nucleoco=' + that.model.get('id_nucleoco'));
                that.model.destroy({
                    success: function (model, response) {
                    },
                    complete: function (response) {
                        response = $.parseJSON(response.responseText);
                        $.pnotify(response);

                        NucleoCoIni.openEmail();
                        loaded();
                    }
                });
            }
        });
    }
});

var EmailAddedCollection = Backbone.Collection.extend({
    model: EmailAddedModel,
    //url: '/api/vw-nucleo-co-email?adicionar=0',
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});

var EmailAddedComposite = Marionette.CompositeView.extend({
    template: '#email-added-template',
    childView: EmailAddedView,
    childViewContainer: 'tbody',
    initialize: function() {
        this.collection = new EmailAddedCollection();
        this.collection.url = '/api/vw-nucleo-co-email?adicionar=0&id_nucleoco=' + NucleoCoIni.model.get('id_nucleoco');
        this.collection.fetch();
    },
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="2">Nenhum e-mail adicionado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});
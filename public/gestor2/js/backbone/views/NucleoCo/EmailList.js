var EmailListModel = Backbone.Model.extend({
    idAttribute: 'id_emailconfig',
    defaults: {
        id_emailconfig: 0,
        id_nucleoco: 0,
        id_entidade: 0,
        adicionar: 1,
        st_titulo: '',
        st_usuario: ''
    },
    url: function() {
        return '/api/vw-nucleo-co-email/' + (this.id || '');
    }
});

var EmailListView = Marionette.ItemView.extend({
    model: new EmailListModel(),
    template: '#email-list-view',
    tagName: 'tr'
});

var EmailListCollection = Backbone.Collection.extend({
    model: EmailListModel,
    //url: '/api/vw-nucleo-co-email?adicionar=1',
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});

var EmailListComposite = Marionette.CompositeView.extend({
    template: '#email-list-template',
    childView: EmailListView,
    childViewContainer: 'tbody',
    initialize: function() {
        this.collection = new EmailListCollection();
        this.collection.url = '/api/vw-nucleo-co-email?adicionar=1&id_nucleoco=' + NucleoCoIni.model.get('id_nucleoco');
        this.collection.fetch();
    },
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="2">Nenhum e-mail encontrado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});
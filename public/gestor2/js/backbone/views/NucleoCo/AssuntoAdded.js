var AssuntoAddedModel = Backbone.Model.extend({
    idAttribute: 'id_nucleoassuntoco',
    defaults: {
        id_nucleoassuntoco: null,
        id_assuntoco: null,
        st_assuntoco: '',
        bl_ativo: false,
        bl_abertura: false,
        st_situacao: '',
        bl_autodistribuicao: false,
        id_nucleoco: null,
        st_tipoocorrencia: '',
        st_assuntopai: '',
        id_tipoocorrencia: null,
        id_textonotificacao: null,
        id_entidade: null,
        bl_notificaatendente: false,
        id_nucleofinalidade: null,
        id_situacao: null
    },
    url: function() {
        return '/api/nucleo-co-assunto/' + (this.id || '');
    }
});
var AssuntoAddedView = Marionette.ItemView.extend({
    model: new AssuntoAddedModel(),
    template: '#assunto-view',
    tagName: 'tr',
    events: {
        'click .actionDeleteAssunto': 'actionDeleteAssunto',
        'click .openResponsavel': 'openResponsavel'
    },
    actionDeleteAssunto: function() {
        var that = this;

        bootbox.confirm('Deseja remover este registro?', function(response) {
            if (response) {
                that.model.destroy({
                    complete: function(response) {
                        response = $.parseJSON(response.responseText);
                        loaded();
                        $.pnotify(response);
                    }
                });
            }
        });
    },
    openResponsavel: function() {
        var idNucleoCo = this.model.get('id_nucleoco');
        var idAssuntoCo = this.model.get('id_assuntoco');

        $.ajaxSetup({ async: false });
        if (typeof ResponsavelComposite === 'undefined')
            $.getScript('/js/backbone/views/NucleoCo/Responsavel.js');

        $.getJSON('/api/nucleo-pessoa-co/', {id_nucleoco: idNucleoCo, id_assuntoco: idAssuntoCo}, function(response) {
            ResponsavelIni = new ResponsavelComposite({
                collection: new ResponsavelCollection(response),
                el: '#assunto-content'
            });
            ResponsavelIni.idNucleoCo = idNucleoCo;
            ResponsavelIni.idAssuntoCo = idAssuntoCo;
            //$('#assunto-content').html( ResponsavelIni.render().$el );
            ResponsavelIni.render();
        });
        $.ajaxSetup({ async: true });
    }
});

var AssuntoAddedCollection = Backbone.Collection.extend({
    model: AssuntoAddedModel,
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});
var AssuntoAddedComposite = Marionette.CompositeView.extend({
    template: '#assunto-added-template',
    childView: AssuntoAddedView,
    childViewContainer: '#assunto-added-content',
    events: {
        'click .actionAdd': 'actionAdd'
    },
    actionAdd: function() {
        var elem = this.$el;

        AssuntoListIni = new AssuntoListComposite();
        AssuntoListIni.collection.url = '/api/nucleo-co-assunto/?id_tipoocorrencia=' + $('[name="id_tipoocorrencia"]').val() + '&id_nucleoco=' + NucleoCoIni.model.get('id_nucleoco');
        AssuntoListIni.collection.fetch({
            success: function() {
                elem.find('#assunto-list-content').html( AssuntoListIni.render().$el );

                var modal = elem.find('#modal-assunto');

                modal.on('click', '.actionAddAssunto', function() {
                    var AssuntosMarcados = [];
                    modal.find('input:checked').each(function() {
                        AssuntosMarcados.push($(this).val());
                    });
                    if (AssuntosMarcados.length) {
                        loading();

                        NucleoCoIni.model.set('assuntos_adicionados', AssuntosMarcados);
                        NucleoCoIni.model.save(null, {
                            complete: function(response) {
                                NucleoCoIni.model.set('assuntos_adicionados', []);
                                response = $.parseJSON(response.responseText);
                                $('#modal-assunto').modal('hide');
                                $.pnotify(response);
                                NucleoCoIni.model.fetch({
                                    success: function() {
                                        NucleoCoIni.render();
                                        NucleoCoIni.$el.find('a[href="#tab-assuntos"]').trigger('click');
                                        NucleoCoIni.onShow();
                                    }
                                });
                                loaded();
                            }
                        });

                    }
                });

            }
        });

    },
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="4">Nenhum assunto adicionado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

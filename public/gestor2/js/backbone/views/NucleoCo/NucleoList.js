var NucleoListCollection = Backbone.Collection.extend({
    model: NucleoModel,
    url: '/api/nucleo-co/',
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});


var NucleoListView = Marionette.ItemView.extend({
    model: new NucleoModel(),
    template: '#nucleo-list-view',
    tagName: 'tr',
    onRender: function() {
    },
    events: {
        'click .actionDelete': 'actionDelete',
        'click .actionEdit': 'actionEdit'
    },
    actionDelete: function() {
        var that = this;
        bootbox.confirm('Deseja remover este registro?', function(response) {
            if (response) {
                loading();
                that.model.destroy().done(function (success, type, response) {
                    response = $.parseJSON(response.responseText);
                    $.pnotify(response);
                    loaded();
                });
            }
        });
    },

    actionEdit: function() {
        var that = this;
        loading();
        var NucleoCoIni = new NucleoView();
        if(that.model.attributes){
            NucleoCoIni.model.set(this.model.attributes);
            NucleoCoIni.model.fetch({
                success: function () {
                    G2S.layout.global.show(NucleoCoIni);
                }
            });
        }
        loaded();
    }
});


var NucleoListComposite = Marionette.CompositeView.extend({
    template: '#nucleo-list-template',
    collection: new NucleoListCollection(),
    childView: NucleoListView,
    childViewContainer: 'tbody',
    onBeforeRender: function() {
        this.collection.fetch();
    },
    onRender: function() {
        this.verifyEmpty();
    },
    events: {
        'click .actionAddNucleo': 'actionAddNucleo'
    },
    actionAddNucleo: function() {
        var newView = new NucleoListView();
        newView.actionEdit();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<td colspan="4">Nenhum núcleo encontrado.</td>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});

var EvolucaoModel = Backbone.Model.extend({
    idAttribute: 'id_evolucao',
    defaults: {
        id_evolucao: null,
        st_evolucao: '',
        st_tabela: '',
        st_campo: ''
    }
});
var EvolucaoView = Marionette.ItemView.extend({
    template: '#evolucao-view',
    model: new EvolucaoModel(),
    tagName: 'div',
    className: 'row-fluid',
    onRender: function() {
        var that = this;
        var elem = this.$el;

        if (NucleoCoIni.model.get('id_tipoocorrencia')) {
            var FuncaoModel = Backbone.Model.extend({});

            var FuncaoCollection = Marionette.CollectionView.extend({
                initialize: function () {
                    this.childView = Marionette.ItemView.extend({
                        model: new FuncaoModel(),
                        template: '#evolucao-funcao',
                        tagName: 'label',
                        className: 'checkbox inline',
                        initialize: function () {
                            this.render();
                        },
                        onBeforeRender: function () {
                            this.model.set('id_evolucao', that.model.get('id_evolucao'));
                        },
                        onRender: function () {
                            // Verificar quais funcoes estão marcadas, e marca-las
                            var evolucoes = NucleoCoIni.model.get('evolucoes');
                            evolucoes = evolucoes[this.model.get('id_evolucao')];

                            if (evolucoes) {
                                if (evolucoes.indexOf(this.model.get('id_funcao')) != -1) {
                                    this.$el.find('input').attr('checked', 'checked');
                                }
                            }
                        }
                    });
                }
            });
            var Collection = Backbone.Collection.extend({});
            var FuncaoIni = new FuncaoCollection({
                collection: new Collection(arrayFuncao)
            });
            elem.find('#funcao-content').html(FuncaoIni.render().$el);
        }

    }
});

var EvolucaoCollection = Backbone.Collection.extend({
    model: EvolucaoModel,
    url: '/api/evolucao?st_tabela=tb_ocorrencia'
});
var EvolucaoComposite = Marionette.CompositeView.extend({
    template: _.template(''),
    collection: new EvolucaoCollection(arrayEvolucao),
    childView: EvolucaoView,
    //childViewContainer: 'tbody',
    initialize: function() {
        this.collection.fetch();
    },
    onRender: function() {
        this.verifyEmpty();
    },
    collectionEvents: {
        'add': 'verifyEmpty',
        'remove': 'verifyEmpty'
    },
    verifyEmpty: function() {
        var elem = this.$el;

        if (this.collection.length) {
            elem.find('.empty-view').remove();
        } else {
            var ViewEmpty = Marionette.ItemView.extend({
                tagName: 'tr',
                className: 'empty-view',
                template: _.template('<div>Nenhuma evolução encontrada.</div>')
            });
            elem.find('tbody').append((new ViewEmpty()).render().$el);
        }
    }
});
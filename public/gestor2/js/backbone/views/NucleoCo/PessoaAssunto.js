var PessoaAssuntoModel = Backbone.Model.extend({
    //idAttribute: 'id_nucleopessoaco',
    defaults: {
        id_nucleopessoaco: 0,
        st_nomeentidade: '',
        st_assuntopai: '',
        id_assuntopai: 0,
        st_subassunto: '',
        id_subassunto: 0,
        id_assunto: 0,
        id_entidade: 0,
        id_entidadecadastro: 0,
        id_usuario: 0,
        st_nomecompleto: '',
        st_nucleoco: '',
        st_funcao: '',
        id_nucleoco: 0,
        id_funcao: 0,
        st_tipoocorrencia: '',
        bl_todos: 0
    }
});

var PessoaAssuntoView = Marionette.ItemView.extend({
    model: new PessoaAssuntoModel(),
    template: '#assunto-pessoa-view',
    tagName: 'tr',
    events: {
        'change [name="changed_items"]': 'addChangedItems'
    },
    addChangedItems: function(e) {
        // Adicionar ao array de Assuntos da Pessoa, somente os itens que foram editados na lista.
        // Os que nao foram editados, continuam intactos
        var input = $(e.target);
        var checked = input.is(':checked');

        var newItem = {
            id_nucleopessoaco: this.model.get('id_nucleopessoaco'), // SE JA TIVER ADICIONADO, SO EDITA PASSANDO O ID
            id_nucleoco: checked ? this.model.get('id_nucleoco') : 0,
            id_usuario: checked ? this.model.get('id_usuario') : 0,
            id_assuntoco: checked ? this.model.get('id_assunto') : 0,
            id_funcao: this.model.get('id_funcao'),
            bl_todos: checked ? this.model.get('bl_todos') : 0
        };
        var ChangedItems = PessoaAssuntoGlobalIni.model.attributes.changed_items;

        for (var i in ChangedItems) {
            var obj = ChangedItems[i];

            if (obj.id_nucleopessoaco == newItem.id_nucleopessoaco && obj.id_assuntoco == newItem.id_assuntoco && obj.id_funcao == newItem.id_funcao) {
                ChangedItems.splice(i, 1);
                break;
            }
        }

        if ( (input.attr('data-checked') == 'true' && checked) || (input.attr('data-checked') == 'false' && !checked) ) {
            // Caso o CHECKED do elemento esteja igual do banco
            // (nao teve alteracoes reais), nao adicona novamente no array
            return;
        }
        ChangedItems.push(newItem);
    }
});

var PessoaAssuntoCollection = Backbone.Collection.extend({
    model: PessoaAssuntoModel,
    parse: function(response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});

var PessoaAssuntoComposite = Marionette.CompositeView.extend({
    template: '#assunto-pessoa-template',
    childView: PessoaAssuntoView,
    childViewContainer: 'tbody',
    onBeforeRender: function() {
        var bl_todos = this.collection.models[0].get('bl_todos');
        if (!bl_todos) {
            // SENAO POSSUI O ELEMENTO REFERENTE AO BL_TODOS, ADICIONAR
            var newModelBltodos = new PessoaAssuntoModel();
            newModelBltodos.set(this.collection.models[0].attributes);
            newModelBltodos.set({
                id_nucleopessoaco: 0,
                id_assunto: 0,
                id_assuntoco: 0,
                id_assuntopai: 0,
                bl_todos: 1
            });
            this.collection.models.unshift(newModelBltodos);
        }
    },
    onRender: function() {
        // Reposicionar o elemento BL TODOS no topo, ao lado da legenda
        var blTodosElem = this.$el.find('tbody tr:first');
        blTodosElem.find('td').not(':first').remove();

        var newHtml = $('<label class="checkbox inline">' + blTodosElem.find('td').html() + ' Todos</label>');
        blTodosElem.addClass('bl_todos').find('td').attr('colspan', 4).html(newHtml);

        // SE JA POSSUIR O ITEM DO BL_TODOS, MARCAR A OPCAO NO ELEMENTO
        var bl_todos = this.collection.models[0].get('id_nucleopessoaco');
        if (bl_todos) {
            this.$el.find('.bl_todos input').prop('checked', bl_todos).trigger('change');
        }
    },
    events: {
        'change .bl_todos input': 'toggleCheck'
    },
    toggleCheck: function(e) {
        var elem = this.$el;
        var input = $(e.target);
        var checked = input.prop('checked');

        if (checked) {
            elem.find('table thead').hide();
            elem.find('table tbody tr').not(':first').hide();
        } else {
            elem.find('table thead').show();
            elem.find('table tbody tr').not(':first').show();
        }

        // Adicionar ao BL TODOS SE MARCADO
        var bl_todos = this.collection.models[0].get('bl_todos');
        if (bl_todos) {
            this.collection.models[0].set('bl_todos', checked ? 1 : 0);
        }
    }
});




var PessoaAssuntoGlobal = Marionette.ItemView.extend({
    template: '#assunto-pessoa-global',
    tagName: 'div',
    onRender: function() {
        var that = this;
        var elem = this.$el;

        var idUsuario = this.model.get('id_usuario');
        var idEntidade = this.model.get('id_entidade');
        var idNucleoCo = this.model.get('id_nucleoco');

        $.getJSON('/api/vw-pessoas-nucleo-assunto/' + idUsuario + '?id_entidade=' + idEntidade + '&id_nucleoco=' + idNucleoCo, function(response) {

            for (var i in response.mensagem) {
                var newComposite = new PessoaAssuntoComposite({
                    collection: new PessoaAssuntoCollection(response.mensagem[i])
                });

                var rendered = newComposite.render().$el;
                rendered.find('legend').html(i.replace('_ca', '_').replace('_', ' '));

                elem.find('#assunto-pessoa-list').append(rendered);
                PessoaAssuntoIni.push(newComposite);
            }

        });
    },
    events: {
        'click .actionBack': 'actionBack',
        'click .actionSaveAssuntos': 'actionSaveAssuntos'
    },
    actionBack: function() {
        $('#global-content').show();
        $('#pessoa-assunto-content').hide();
        G2S.layout.pessoaAssunto.reset();
    },
    actionSaveAssuntos: function() {
        var arrAssuntos = PessoaAssuntoGlobalIni.model.attributes.changed_items;

        if (arrAssuntos.length) {
            loading();
            this.model.save(null, {
                success: function(model, response) {
                    // BUSCAR E RENDERIZAR TODA A LISTA DE PESSOAS>ASSUNTOS NOVAMENTE
                    PessoaAssuntoGlobalIni.model.attributes.changed_items = [];
                    G2S.layout.pessoaAssunto.show(PessoaAssuntoGlobalIni);
                },
                complete: function(response) {
                    response = $.parseJSON(response.responseText);
                    $.pnotify(response);
                    loaded();
                }
            });
        } else {
            $.pnotify({
                type: 'warning',
                title: 'Aviso',
                text: 'Nenhuma alteração foi feita'
            });
        }
    }
});
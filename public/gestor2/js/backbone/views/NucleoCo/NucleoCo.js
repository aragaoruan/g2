var NucleoModel = Backbone.Model.extend({
    defaults: {
        id_nucleoco: null,
        id_tipoocorrencia: null,
        id_entidade: null,
        id_situacao: null,
        id_textonotificacao: null,
        st_nucleoco: "",
        bl_notificaatendente: false,
        bl_notificaresponsavel: false,
        bl_ativo: true,
        st_tipoocorrencia: "",
        st_situacao: "",
        id_assuntorecuperacao: null,
        id_assuntocoresgate: null,
        id_assuntorenovacao: null,
        id_holdingcompartilhamento: null,

        //id_usuariocadastro: null,
        //dt_cadastro: {},

        // TELA EDIT
        id_nucleofinalidade: null,
        nu_horasmeta: 0,
        st_corinteracao: "",
        st_cordevolvida: "",
        st_coratrasada: "",
        st_cordefault: "",
        id_assuntocancelamento: "",
        // ARRAYS
        evolucoes: {},
        assuntos: []
    },
    idAttribute: "id_nucleoco",
    url: function () {
        return "/api/nucleo-co/" + (this.id || "");
    }
});

var NucleoView = Marionette.ItemView.extend({
    model: new NucleoModel(),
    template: "#nucleo-edit",
    tagName: "div",
    ui: {
        inputTipoOcorrencia: "#id_tipoocorrencia",
        inputNucleoFinalidade: "#id_nucleofinalidade",
        inputTextoNotificacao: "#id_textonotificacao",
        inputSituacao: "#id_situacao",
        checkNotificacaoResponsavel: "#bl_notificaresponsavel",
        checkNotificacaoAtendente: "#bl_notificaatendente",
        inputHorasMeta: "#nu_horasmeta",
        inputNucleoCo: "#st_nucleoco",
        inputCorAtrasada: "#st_coratrasada",
        inputCorDefault: "#st_cordefault",
        inputCorInteracao: "#st_corinteracao",
        inputCorDevolvida: "#st_cordevolvida",
        togglerHoldingComp: "#bl_compartilharholding",
        inputHoldingComp: "#id_holdingcompartilhamento"
    },
    events: {
        "submit form": "actionSave",
        "click .actionCancel": "actionCancel",
        "click .openEmail": "openEmail",
        "click .actionSaveEmail": "actionSaveEmail",
        "change [name='id_tipoocorrencia']": "refreshEvolucoes",
        "change @ui.togglerHoldingComp": "toggleHoldingCompartilhamento"
    },
    onRender: function () {
        var that = this;
        var elem = this.$el;

        //COLOR PICKER
        if (!$("[data-minicolors='css']").length) {
            $("head")
                .append("<link rel='stylesheet' data-minicolors='css' href='/css/minicolors.css' type='text/css' />");
        }

        elem.find(".minicolors").minicolors({});

        // COMBOBOXES
        //TIPO
        ComboboxView({
            el: elem.find("[name='id_tipoocorrencia']"),
            collection: arrayTipo,
            emptyOption: "[Selecione]",
            optionLabel: "st_tipoocorrencia",
            optionSelectedId: this.model.get("id_tipoocorrencia"),
            model: Backbone.Model.extend({
                defaults: {
                    id_tipoocorrencia: null,
                    st_tipoocorrencia: ""
                },
                idAttribute: "id_tipoocorrencia"
            })
        }, function () {
            that.refreshEvolucoes();
        });

        //SITUACAO
        ComboboxView({
            el: elem.find("[name='id_situacao']"),
            collection: arraySituacao,
            emptyOption: "[Selecione]",
            optionLabel: "st_situacao",
            optionSelectedId: this.model.get("id_situacao"),
            model: Backbone.Model.extend({
                defaults: {
                    id_situacao: null,
                    st_situacao: ""
                },
                idAttribute: "id_situacao"
            })
        });

        //FINALIDADE
        ComboboxView({
            el: elem.find("[name='id_nucleofinalidade']"),
            collection: arrayFinalidade,
            emptyOption: "[Selecione]",
            optionLabel: "st_nucleofinalidade",
            optionSelectedId: this.model.get("id_nucleofinalidade"),
            model: Backbone.Model.extend({
                defaults: {
                    id_nucleofinalidade: null,
                    st_nucleofinalidade: ""
                },
                idAttribute: "id_nucleofinalidade"
            })
        });

        //TEXTO DA NOTIFICACAO
        ComboboxView({
            el: elem.find("[name='id_textonotificacao']"),
            collection: arrayTexto,
            emptyOption: "[Selecione]",
            optionLabel: "st_textosistema",
            optionSelectedId: this.model.get("id_textonotificacao"),
            model: Backbone.Model.extend({
                defaults: {
                    id_textosistema: null,
                    st_textosistema: ""
                },
                idAttribute: "id_textosistema"
            })
        });

        //HOLDINGS
        ComboboxView({
            el: this.ui.inputHoldingComp,
            url: "/nucleo-co/get-holdings?id_entidade=" + this.model.get('id_entidade'),
            emptyOption: "[Selecione]",
            optionLabel: "st_holding",
            optionSelectedId: this.model.get("id_holdingcompartilhamento"),
            model: Backbone.Model.extend({
                defaults: {
                    id_holding: null,
                    st_holding: ""
                },
                idAttribute: "id_holding"
            })
        });

        Model = Backbone.Model.extend({
            idAttribute: 'id_textosistema',
            defaults: {
                'id_textosistema': null,
                'st_textosistema': ''
            }
        });


        ComboboxView({
            el: elem.find('[name="id_textonotificacao"]'),
            model: Model,
            collection: arrayTexto,
            emptyOption: '[Selecione]',
            optionLabel: 'st_textosistema',
            optionSelectedId: this.model.get('id_textonotificacao')
        });

        //carrega os dados do select de cancelamento somente quando o tipo da ocorrencia for para aluno
        if (parseInt(this.model.get("id_tipoocorrencia")) === TIPO_OCORRENCIA.ALUNO) {
            this.populaSelectAssuntoCancelamento();
        }

        var filter1 = {
            bl_interno: true,
            bl_recuperacao_resgate: true
        };

        this.populaSelectAssuntos(elem.find("[name='id_assuntorecuperacao']"),
            this.model.get("id_assuntorecuperacao"), filter1);
        this.populaSelectAssuntos(elem.find("[name='id_assuntocoresgate']"),
            this.model.get("id_assuntocoresgate"), filter1);
        this.populaSelectAssuntos(elem.find("[name='id_assuntorenovacao']"),
            this.model.get("id_assuntorenovacao"), {id_assuntocategoria: 2});
    },

    populaSelectAssuntos: function (elem, value, filter) {
        var ModelAssuntos = Backbone.Model.extend({
            idAttribute: 'id_assuntoco',
            defaults: {
                'id_assuntoco': null,
                'st_assuntoco': ''
            }
        });
        var CollectionAssuntos = Backbone.Collection.extend({
            model: ModelAssuntos
        });
        var assuntosNucleo = new CollectionAssuntos(this.model.get("assuntos"));
        var arrAssuntos = assuntosNucleo.where(filter);
        var collectionAssuntos = new CollectionAssuntos(arrAssuntos);

        (new SelectView({
            el: elem,
            collection: collectionAssuntos,
            childViewOptions: {
                optionLabel: "st_assuntoco",
                optionValue: "id_assuntoco",
                optionSelected: value
            }
        })).render();

    },
    onShow: function () {
        var that = this;
        var elem = this.$el;
        AssuntoAddedIni = new AssuntoAddedComposite();
        AssuntoAddedIni.collection = new AssuntoAddedCollection(that.model.get("assuntos"));
        elem.find("#assunto-content").html(AssuntoAddedIni.render().$el);

        PessoaListIni = new PessoaListComposite();
        PessoaListIni.collection = new PessoaListCollection();
        PessoaListIni.collection.fetch({
            url: "/api/vw-pessoas-nucleo-assunto",
            data: {
                id_nucleoco: NucleoCoIni.model.id
            }
        });

        elem.find("#pessoa-content").html(PessoaListIni.render().$el);
    },
    toggleHoldingCompartilhamento: function () {
        var $input = this.ui.inputHoldingComp;
        var $label = $input.closest("label");
        var checked = this.ui.togglerHoldingComp.is(":checked");

        $input.val("");
        $input.prop("required", checked);
        $label.toggleClass("hidden", !checked);
    },
    getErrors: function(values) {
        var errors = [];

        if (!values.st_nucleoco) {
            errors.push("Insira o nome do núcleo.");
        }

        if (!values.id_tipoocorrencia) {
            errors.push("Informe o tipo do núcleo.");
        }

        if (!values.id_situacao) {
            errors.push("Informe a situação do perfil.");
        }

        if (!values.id_nucleofinalidade) {
            errors.push("Informe a finalidade do núcleo.");
        }

        if (this.ui.togglerHoldingComp.is(":checked") && !values.id_holdingcompartilhamento) {
            errors.push("Informe a Holding para compartilhamento.");
        }

        return errors;
    },

    populaSelectAssuntoCancelamento: function () {

        var AssuntosCancelamento = Backbone.Collection.extend({
            model: VwNucleoAssuntoCo
        });
        var collection = new AssuntosCancelamento(this.model.get("assuntos"));

        var arrAssuntos = collection.where({
            "bl_cancelamento": true
        });

        var collectionAssuntos = new AssuntosCancelamento(arrAssuntos);
        (new SelectView({
            el: this.$el.find("#id_assuntocancelamento"),
            collection: collectionAssuntos,
            childViewOptions: {
                optionLabel: "st_assuntoco",
                optionValue: "id_assuntoco",
                optionSelected: this.model.get("id_assuntocancelamento")
            }
        })).render();

    },

    actionSave: function (e) {
        e.preventDefault();

        var elem = this.$el;

        var values = {
            id_tipoocorrencia: Number(this.ui.inputTipoOcorrencia.val()),
            id_nucleofinalidade: Number(this.ui.inputNucleoFinalidade.val()),
            id_textonotificacao: Number(this.ui.inputTextoNotificacao.val()),
            id_situacao: Number(this.ui.inputSituacao.val()),
            id_assuntorecuperacao: Number(elem.find('[name="id_assuntorecuperacao"]').val()),
            id_assuntocoresgate: Number(elem.find('[name="id_assuntocoresgate"]').val()),
            id_assuntorenovacao: Number(elem.find('[name="id_assuntorenovacao"]').val()),
            bl_notificaresponsavel: this.ui.checkNotificacaoResponsavel.is(":checked") ? 1 : 0,
            bl_notificaatendente: this.ui.checkNotificacaoAtendente.is(":checked") ? 1 : 0,
            nu_horasmeta: Number(this.ui.inputHorasMeta.val()),
            st_nucleoco: this.ui.inputNucleoCo.val(),
            st_coratrasada: this.ui.inputCorAtrasada.val().replace("#", ""),
            st_cordefault: this.ui.inputCorDefault.val().replace("#", ""),
            st_corinteracao: this.ui.inputCorInteracao.val().replace("#", ""),
            st_cordevolvida: this.ui.inputCorDevolvida.val().replace("#", ""),
            id_holdingcompartilhamento: +this.ui.inputHoldingComp.val() || null,
            id_assuntocancelamento: elem.find('[name="id_assuntocancelamento"]').val(),
            evolucoes: {}
        };

        // Validacao dos campos
        var errors = this.getErrors(values);

        if (errors.length) {
            $.pnotify({
                type: "error",
                title: "Campos Obrigatórios",
                text: errors.shift()
            });
            return false;
        }

        // Montar o array de evolucoes que sera manipulado no controller
        elem.find("input[data-id-evolucao]:checked").each(function () {
            var input = $(this);
            var index = input.attr("data-id-evolucao");

            if (!values.evolucoes[index]) {
                values.evolucoes[index] = [];
            }

            values.evolucoes[index].push(input.val());
        });

        this.model.save(values, {
            beforeSend: loading,
            success: function (model, response) {
                if (response.type === "success") {
                    var NucleoListIni = new NucleoListComposite();
                    G2S.layout.global.show(NucleoListIni);
                }
            },
            complete: function (response) {
                response = $.parseJSON(response.responseText);
                $.pnotify(response);
                loaded();
            }
        });

    },
    actionCancel: function () {
        NucleoListIni = new NucleoListComposite();
        G2S.layout.global.show(NucleoListIni);
    },
    openEmail: function () {
        $.ajaxSetup({async: false});

        if (typeof EmailListComposite === "undefined") {
            $.getScript("/js/backbone/views/NucleoCo/EmailList.js");
        }

        if (typeof EmailAddedComposite === "undefined") {
            $.getScript("/js/backbone/views/NucleoCo/EmailAdded.js");
        }

        $.ajaxSetup({async: true});

        G2S.layout.addRegions({
            emailList: "#email-list-content",
            emailAdded: "#email-added-content"
        });

        EmailListIni = new EmailListComposite();
        G2S.layout.emailList.show(EmailListIni);

        EmailAddedIni = new EmailAddedComposite();
        G2S.layout.emailAdded.show(EmailAddedIni);

    },
    actionSaveEmail: function () {
        var elem = this.$el;

        var emails = [];

        elem.find("input[name='id_emailconfig']:checked").each(function () {
            emails.push(parseInt($(this).attr("data-id")));
        });

        if (emails.length) {
            $.ajax({
                url: EmailListIni.collection.url,
                method: "post",
                data: JSON.stringify({emails: emails}),
                contentType: "application/json",
                beforeSend: loading,
                success: function () {
                    NucleoCoIni.openEmail();
                },
                complete: function (response) {
                    response = $.parseJSON(response.responseText);
                    $.pnotify(response);
                    loaded();
                }
            });
        }
    },
    refreshEvolucoes: function () {
        var elem = this.$el;

        this.model.set("id_tipoocorrencia", elem.find("[name='id_tipoocorrencia']").val());

        $.ajaxSetup({async: false});
        $.getJSON("/api/funcao?id_tipofuncao=" + idTipoFuncao, function (response) {
            if (response.type === "success") {
                arrayFuncao = response.mensagem;
            }
        });
        $.ajaxSetup({async: true});

        EvolucaoIni = new EvolucaoComposite();
        elem.find("#evolucao-content").html(EvolucaoIni.render().$el);
    }
});


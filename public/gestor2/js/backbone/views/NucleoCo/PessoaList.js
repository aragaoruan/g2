var PessoaListModel = Backbone.Model.extend({
    idAttribute: 'id_usuario',
    defaults: {
        id_usuario: null,
        id_nucleoco: null,
        st_nomecompleto: ''
    },
    url: function () {
        return '/api/vw-pessoas-nucleo-assunto/' + (this.id || '');
    }
});

var PessoaListView = Marionette.ItemView.extend({
    model: new PessoaListModel(),
    template: '#pessoa-view',
    tagName: 'tr',
    events: {
        'click .removerPessoa': 'removerPessoa',
        'click .openAssuntos': 'openAssuntos'
    },
    removerPessoa: function(e){
        var that = this;

        var requestDeletarUsuarios = function() {
            loading();
            $.ajax({
                'type': 'delete',
                'url': '/api/nucleo-pessoa-co/?' + $.param({
                    'id_usuario': that.model.get('id_usuario'),
                    'id_nucleoco': that.model.get('id_nucleoco'),
                    'deletarPessoaNucleo': true
                }),
                success: function(){
                    $.pnotify({
                        'type': 'success',
                        'title': 'Sucesso',
                        'text': that.model.get('st_nomecompleto') + ' removido(a) do núcleo com sucesso.'
                    });
                    //Remove a linha da tabela./
                    $(e.target).closest('tr').remove();
                },
                error: function(){
                    $.pnotify({
                        'type': 'error',
                        'title': 'Erro',
                        'text': 'Ocorreu um erro ao remover a pessoa do nucleo.'
                    });
                },
                complete: function(){
                    loaded();
                }
            });
        };

        bootbox.confirm('Tem certeza que deseja remover essa pessoa do núcleo?', function(res){
            if(res) {
                requestDeletarUsuarios();
            }
        });
    },
    openAssuntos: function () {
        $.ajaxSetup({async: false});
        if (typeof PessoaAssuntoGlobal === 'undefined')
            $.getScript('/js/backbone/views/NucleoCo/PessoaAssunto.js');
        $.ajaxSetup({async: true});

        var Model = Backbone.Model.extend({
            defaults: {
                id_usuario: this.model.get('id_usuario'),
                id_entidade: NucleoCoIni.model.get('id_entidade'),
                id_nucleoco: this.model.get('id_nucleoco'),
                changed_items: []
            },
            url: function () {
                return '/api/vw-pessoas-nucleo-assunto/' + (this.id || '');
            }
        });
        PessoaAssuntoGlobalIni = new PessoaAssuntoGlobal({
            model: new Model()
        });
        $('#global-content').hide();
        $('#pessoa-assunto-content').show();
        G2S.layout.pessoaAssunto.show(PessoaAssuntoGlobalIni);
    }
});

var PessoaEditView = Marionette.ItemView.extend({
    model: new PessoaListModel(),
    template: '#pessoa-edit',
    tagName: 'tr',
    onRender: function () {
        var that = this;
        var elem = this.$el;

        var Model = Backbone.Model.extend({
            idAttribute: 'id_entidadeorigem',
            defaults: {
                id_entidadeorigem: null,
                id_entidadedestino: null,
                id_tipodados: null,
                st_nomeentidade: '',
                st_nomentidadeorigem: ''
            }
        });
        ComboboxView({
            el: elem.find('[name="id_instituicao"]'),
            model: Model,
            url: '/nucleo-co/get-pessoa-instituicao?id_tipodados=1&id_entidadedestino=' + NucleoCoIni.model.get('id_entidade'),
            emptyOption: '[Selecione]',
            optionLabel: 'st_nomentidadeorigem'
        });
    },
    events: {
        'click .actionSavePessoa': 'actionSavePessoa',
        'change [name="id_instituicao"]': 'toggleAutocomplete',
        // Eventos autocomplete
        'keyup [name="autocomplete"]': 'autocompleteSearch',    // String da busca do autocomplete
        'keydown [name="autocomplete"]': 'autocompletePrevent', // Interromper o evento default
        'change [name="autocomplete"]': 'autocompleteVerify',   // Ao alterar a string de um resultado ja selecionado
        'change [name="id_usuario"]': 'autocompleteChange'      // Ao escolher um resultado do autocomplete
    },
    actionSavePessoa: function () {
        var that = this;
        var elem = this.$el;
        var parametros = {
            id_usuario: Number(elem.find('[name="id_usuario"]').val()),
            id_entidade: $('[name="id_instituicao"]').val()
        }
        $.ajax({
            type: 'post',
            url: '/pessoa/salvar-usuario-importacao-nucleo',
            data: parametros,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                $.pnotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            },
            complete: function () {
                loaded();

            }
        });
        var values = {
            id_usuario: Number(elem.find('[name="id_usuario"]').val()),
            id_nucleoco: NucleoCoIni.model.id,
            st_nomecompleto: elem.find('[name="id_usuario"] option:selected').text(),
        };
        this.model.set(values);
        this.$el.remove();
        PessoaListIni.collection.add(this.model);
        PessoaListIni.$el.find('tr:last .openAssuntos').trigger('click');

    },
    toggleAutocomplete: function (e) {
        if (this.autocompleteRequest)
            this.autocompleteRequest.abort();
        var autocompleteEl = $('[name="autocomplete"]');
        autocompleteEl.prop('disabled', $(e.target).val().trim() ? false : true);
        autocompleteEl.val('');

    },
    autocompleteRequest: false,
    autocompleteStr: '',
    autocompleteSearch: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        var value = input.val().trim();

        /**
         * KeyCode 48 até 90 = 0 (zero) até Z (letra Z)
         * KeyCode 8 = backspace (apagar)
         * KeyCode 46 = delete (del)
         * KeyCode 38/40 = seta pra cima/baixo
         */
        if ((e.keyCode == 38 || e.keyCode == 40) && combobox.is(':visible')) {
            // Se usar setas do teclado par cima/baixo e o <select> estiver visivel, navegar pelas opções do mesmo
            var focused = combobox.find('option:selected');

            if (e.keyCode == 38) {
                focused = focused.prev();
                if (!focused.length)
                    focused = combobox.find('option:last');
            } else {
                focused = focused.next();
                if (!focused.length)
                    focused = combobox.find('option:first');
            }

            combobox.find('option').prop('selected', false);
            focused.prop('selected', true);
            e.preventDefault();
        } else if ((e.keyCode == 8 || e.keyCode == 46) && combobox.find('option:selected').length && combobox.find('option:selected').val()) {
            // Se usar tecla BACKSPACE ou DELETE e tiver um item selecionado, desmarcar este item, e limpar o input
            input.val('');
            combobox.html('').hide();
        } else if (((e.keyCode >= 48 && e.keyCode <= 90) || (e.keyCode == 8 || e.keyCode == 46)) && value != this.autocompleteStr && value.length >= 7) {
            // Abortar/Cancelar busca do autocomplete anterior
            if (this.autocompleteRequest)
                this.autocompleteRequest.abort();

            $('body').css('cursor', 'wait');
            var url = '/nucleo-co/get-pessoa-autocomplete?id_entidade=' + $('[name="id_instituicao"]').val() + '&st_nomecompleto=' + value + '&st_email=' + value;
            this.autocompleteRequest = $.get(url, function (response) {
                $('body').css('cursor', 'default');
                combobox.children().not(':selected').remove();
                if (response.type == 'success') {
                    combobox.show();
                    for (var i in response.mensagem) {
                        var obj = response.mensagem[i];

                        if (!combobox.find('[value="' + obj.id_usuario + '"]').length)
                            combobox.append('<option value="' + obj.id_usuario + '">' + obj.st_nomecompleto + '</option>');
                    }
                }

            });
        }
    },
    autocompleteChange: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        input.val(combobox.find('option:selected').text()).focus();
        combobox.children().not(':selected').remove();
        combobox.hide();
    },
    autocompleteVerify: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        setTimeout(function () {
            var value = input.val().trim();
            var selected = combobox.find('option:selected');

            if (value && selected.length) {
                input.val(selected.text());
            } else {
                input.val('');
                combobox.html('').hide();
            }
        }, 200);
    },
    autocompletePrevent: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');

        if (e.keyCode == 13) {
            // Se apertar a tecla enter, selecionar o item
            combobox.trigger('change');
            e.preventDefault();
        }
    }
});

var PessoaListCollection = Backbone.Collection.extend({
    model: PessoaListModel,
    parse: function (response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});

var PessoaListComposite = Marionette.CompositeView.extend({
    template: '#pessoa-template',
    childView: PessoaListView,
    childViewContainer: 'tbody',
    events: {
        'click .actionAddPessoa': 'actionAddPessoa'
    },
    actionAddPessoa: function () {
        //var PessoaEditIni = new PessoaEditView();
        //$('#pessoa-content').html( PessoaEditIni.render().$el );

        //var newModel = new PessoaListModel();
        //PessoaListIni.collection.add(newModel);

        var newView = new PessoaEditView();
        PessoaListIni.$el.find('tbody').append(newView.render().$el);
    }
});
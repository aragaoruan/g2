var ResponsavelModel = Backbone.Model.extend({
    idAttribute: 'id_nucleopessoaco',
    defaults: {
        id_nucleopessoaco: null,
        id_usuario: null,
        st_nomecompleto: '',
        st_funcao: '',
        bl_prioritario: 0,
        bl_todos: 0
    },
    url: function () {
        return '/api/nucleo-pessoa-co/' + (this.id || '');
    }
});

/**
 ******************************************************************************************
 ******************************************************************************************
 ******************************** COLLECTIONS *********************************************
 ******************************************************************************************
 ******************************************************************************************
 */

var PessoaCollection = Backbone.Collection.extend({
    model: Pessoa,
    url: '/api/pessoa'
});

var FuncaoCollection = Backbone.Collection.extend({
    model: Funcao,
    url: '/api/funcao'
});

var EmptyView = Marionette.ItemView.extend({
    template: '#template-lista-vazia',
    tagName: 'tr'
});

var ResponsavelView = Marionette.ItemView.extend({
    model: new ResponsavelModel(),
    template: '#responsavel-view',
    tagName: 'tr',
    events: {
        'click .actionDelete': 'actionDelete',
        'click .actionEdit': 'actionEdit',
        'click .actionSave': 'actionSave',
        // Eventos autocomplete
        'keypress [name="autocomplete"]': 'autocompleteSearch',    // String da busca do autocomplete
        /*'keydown [name="autocomplete"]': 'autocompletePrevent',*/ // Interromper o evento default
        /*'change [name="autocomplete"]': 'autocompleteVerify',*/   // Ao alterar a string de um resultado ja selecionado
        'change [name="id_usuario"]': 'autocompleteChange'      // Ao escolher um resultado do autocomplete
    },
    actionDelete: function () {
        var that = this;
        bootbox.confirm('Deseja remover este registro?', function (response) {
            if (response) {
                that.model.url = '/api/nucleo-pessoa-co?id=' + (that.model.id || '')
                    +'&id_nucleoco=' + (that.model.get('id_nucleoco') || '')
                    + '&id_assuntoco=' + (that.model.get('id_assuntoco') || '');

                that.model.destroy({
                    complete: function (response) {
                        response = $.parseJSON(response.responseText);
                        loaded();
                        $.pnotify(response);
                    }
                });
            }
        });
    },
    actionSave: function () {
        var that = this;
        var elem = this.$el;

        var values = {
            id_nucleoco: ResponsavelIni.idNucleoCo,
            id_assuntoco: ResponsavelIni.idAssuntoCo,
            id_funcao: elem.find('[name="id_funcao"]').val(),
            id_usuario: Number(elem.find('[name="id_usuario"]').val()),
            bl_prioritario: Number(elem.find('[name="bl_prioritario"]').val()),
            // SOMENTE PARA O ITEMVIEW
            st_nomecompleto: elem.find('[name="autocomplete"]').val(),
            st_funcao: elem.find('[name="id_funcao"] option:selected').text()
        };

        if (!values.id_funcao) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção',
                text: 'Selecione a função do usuário'
            });

            elem.find('[name="id_funcao"]').focus();
            return false;
        }

        /* Criação de um model temporário apenas para comparação, evitando problemas com edições. */

        var modelComparacao = new ResponsavelModel();
        modelComparacao.set(values);

        /* Valida se já existe um usuário prioritário com a mesma função no núcleo em questão.
         Caso negativo, impede o salvamento e exibe uma mensagem de erro. */
        var temPrioritario = false;

        this.model.collection.each(function(model){
            if(modelComparacao.get('id_nucleopessoaco') != model.get('id_nucleopessoaco')
                && modelComparacao.get('id_funcao') == model.get('id_funcao')
                && model.get('bl_prioritario')
                && modelComparacao.get('bl_prioritario')){
                temPrioritario = true;
            }
        });

        if(!temPrioritario){
            //Se não tiver prioritario, salva.
            loading();
            this.model.save(values, {
                success: function (model, response) {
                    that.model.set('id_nucleopessoaco', response.mensagem.id_nucleopessoaco);
                    that.actionEdit();
                },
                complete: function (response) {
                    response = $.parseJSON(response.responseText);
                    loaded();
                    $.pnotify(response);
                }
            });
        } else {
            //Impede o salvamento e exibe mensagem de erro.
            $.pnotify({
                title: 'Impossível salvar!',
                text: 'Já existe um usuário cadastrado como prioritário para essa função.',
                type: 'error'
            });
            return false;
        }
    },
    isEditing: false,
    actionEdit: function () {
        var that = this;
        var elem = this.$el;

        if (!this.isEditing) {
            this.template = '#responsavel-edit';

            var Model = Backbone.Model.extend({
                idAttribute: 'id_funcao',
                defaults: {
                    id_funcao: null,
                    id_tipofuncao: null,
                    st_funcao: ''
                }
            });
            this.render();

            collectionFuncao = new FuncaoCollection();
            collectionFuncao.url += '/id_tipofuncao/' + idTipoFuncao;
            collectionFuncao.fetch({
                async: false,
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    var view = new SelectView({
                        el: elem.find('[name="id_funcao"]'),
                        collection: collectionFuncao,
                        childViewOptions: {
                            optionLabel: 'st_funcao',
                            optionValue: 'id_funcao',
                            optionSelected: that.model.get('id_funcao')
                        }
                    });

                    view.$el.prepend('<option value="">[Selecione]</option>');
                    view.render();
                },
                error: function () {

                }
            });


        } else {
            if (!this.model.id)
                this.model.destroy();
            else {
                this.template = '#responsavel-view';
                this.render();
            }
        }

        this.isEditing = !this.isEditing;
    },
    autocompleteRequest: false,
    autocompleteStr: '',
    autocompleteSearch: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        var value = input.val().trim();

        /**
         * KeyCode 48 até 90 = 0 (zero) até Z (letra Z)
         * KeyCode 8 = backspace (apagar)
         * KeyCode 46 = delete (del)
         * KeyCode 38/40 = seta pra cima/baixo
         */

        if ((e.keyCode == 38 || e.keyCode == 40) && combobox.is(':visible')) {
            // Se usar setas do teclado par cima/baixo e o <select> estiver visivel, navegar pelas opções do mesmo
            var focused = combobox.find('option:selected');

            if (e.keyCode == 38) {
                focused = focused.prev();
                if (!focused.length)
                    focused = combobox.find('option:last');
            } else {
                focused = focused.next();
                if (!focused.length)
                    focused = combobox.find('option:first');
            }

            combobox.find('option').prop('selected', false);
            focused.prop('selected', true);
            e.preventDefault();
        } else if ((e.keyCode == 8 || e.keyCode == 46) && combobox.find('option:selected').length && combobox.find('option:selected').val()) {
            // Se usar tecla BACKSPACE ou DELETE e tiver um item selecionado, desmarcar este item, e limpar o input
            input.val('');
            combobox.html('').hide();
        } else if (value.length >= 6) {
            // Abortar/Cancelar busca do autocomplete anterior
            //if (this.autocompleteRequest)
            //    this.autocompleteRequest.abort();

            //combobox.empty();
            //$('body').css('cursor', 'wait');

            collectionPessoa = new PessoaCollection();
            collectionPessoa.url += '?st_nomecompleto=' + value;
            collectionPessoa.fetch({
                async: false,
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function () {
                    if (this.collectionPessoa.length) {
                        this.collectionPessoa.each(function (model) {
                            combobox.show();
                            combobox.children().not(':selected').remove();
                            combobox.append('<option value="' + model.get('id_usuario') + '">' + model.get('st_nomecompleto') + '</option>');
                        });
                    } else {
                        $.pnotify({
                            title: 'Aviso',
                            text: 'Não encontramos usuário com esse nome. Tente novamente.!',
                            type: 'danger'
                        });
                    }
                },
                error: function () {

                }
            });
        }
    },
    autocompleteChange: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        input.val(combobox.find('option:selected').text()).focus();
        combobox.children().not(':selected').remove();
        combobox.hide();
    },
    autocompleteVerify: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');
        var input = elem.find('input');

        setTimeout(function () {
            var value = input.val().trim();
            var selected = combobox.find('option:selected');

            if (value && selected.length) {
                input.val(selected.text());
            } else {
                input.val('');
                combobox.html('').hide();
            }
        }, 200);
    },
    autocompletePrevent: function (e) {
        var elem = $(e.target).closest('.autocomplete');
        var combobox = elem.find('select');

        if (e.keyCode == 13) {
            // Se apertar a tecla enter, selecionar o item
            combobox.trigger('change');
            e.preventDefault();
        }
    }
});

var ResponsavelCollection = Backbone.Collection.extend({
    model: ResponsavelModel,
    parse: function (response) {
        return (response.type == 'success') ? response.mensagem : [];
    }
});

var ResponsavelComposite = Marionette.CompositeView.extend({
    template: '#responsavel-template',
    childView: ResponsavelView,
    emptyView: EmptyView,
    childViewContainer: 'tbody',
    events: {
        'click .actionBack': 'actionBack',
        'click .actionAdd': 'actionAdd'
    },
    actionBack: function () {
        G2S.layout.addRegions({
            assuntoAdded: '#assunto-content'
        });
        AssuntoAddedIni = new AssuntoAddedComposite();
        AssuntoAddedIni.collection = new AssuntoAddedCollection(NucleoCoIni.model.get('assuntos'));
        G2S.layout.assuntoAdded.show(AssuntoAddedIni);
    },
    actionAdd: function () {
        var elem = this.$el;

        var NewModel = new ResponsavelModel();
        NewModel.set(NewModel.defaults);
        this.collection.add(NewModel);
        this.render();

        elem.find('tbody tr:last .actionEdit').trigger('click');
        elem.find('tbody tr:last input:first').focus();
    }
});
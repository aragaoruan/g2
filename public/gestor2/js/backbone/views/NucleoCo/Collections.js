var arrayTipo = [];
var arraySituacao = [];
var arrayFinalidade = [];
var arrayTexto = [];
var arrayEvolucao = [];
var arrayFuncao = [];
var idTipoFuncao = 3;

$.getJSON('/nucleo-co/get-tipo-ocorrencia', function(response) {
    if (response.type == 'success')
        arrayTipo = response.mensagem;
});
$.getJSON('/api/situacao/?st_tabela=tb_nucleoco', function(response) {
    arraySituacao = response;
});
$.getJSON('/api/nucleo-finalidade', function(response) {
    if (response.type == 'success')
        arrayFinalidade = response.mensagem;
});
$.getJSON('/api/vw-texto-sistema?id_textocategoria=7', function(response) {
    if (response.type == 'success')
        arrayTexto = response.mensagem;
});
$.getJSON('/api/evolucao?st_tabela=tb_ocorrencia', function(response) {
        arrayEvolucao = response;
});
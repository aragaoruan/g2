/*******************************************************************************
 * Imports
 ******************************************************************************/
$.getScript('/js/backbone/views/ArquivoRetorno/ArquivoRetornoRowView.js?_=' + new Date().getTime());
$.getScript('/js/backbone/views/ArquivoRetorno/ArquivoRetornoTableView.js?_=' + new Date().getTime());

var tpl = '';
$.get('/js/backbone/templates/arquivo-retorno/tpl.html?_=' + new Date().getTime(), '', function (data) {
    tpl = data;
});

var ArquivoRetornoGridView = Marionette.LayoutView.extend({
    initialize: function () {
        this.template = _.template(tpl);
        var url = window.location.hash;
        var piecesUrl = url.split("/");
        this.idAquivoRetorno = piecesUrl[piecesUrl.length - 1];

    },
    events: {
        'click #btn-gerar-xls-retorno-boleto': 'gerarXLSRetornoBoleto'
    },
    regions: {
        'abertosRegion': '#container-boletos-abertos',
        'quitadosRegion': '#container-boletos-quitados',
        'naoLocalizadosRegion': '#container-boletos-nao-localizados',
        'pagamentoMenorRegion' : '#container-boletos-pagamento-menor-original',
        'g1Region': '#container-boletos-g1'
    },
    gerarXLSRetornoBoleto: function () {
        $('#gerar-xls-arquivo-retorno').attr('action', 'arquivo-retorno/gerar-xls-arquivo-retorno');
        $('#gerar-xls-arquivo-retorno').attr('target', '_blank');
        $('#id-arquivo-retorno').val(this.idAquivoRetorno)
        $('#gerar-xls-arquivo-retorno').submit();
    },
    onShow: function () {
        $.ajax({
            dataType: 'json',
            url: '/arquivo-retorno/retorna-arquivo-retorno',
            type: "POST",
            data: {idAquivoRetorno: this.idAquivoRetorno},
            success: function (data) {
                console.log(data.mensagem[0]['st_arquivoretorno'])
                $('#file-grid').val(data.mensagem[0]['st_arquivoretorno']);
            }
        });
        $('#div-grid-arquivo-retorno').show();
        this.boletosInativos();
        this.boletosG1();
        this.boletosNaoLocalizados();
        this.boletosPagamentomenorOriginal();
        this.boletosQuitados();
    },
    boletosInativos: function () {
        $("#container-boletos-abertos").html('<div class="row-fluid" style="background-color: #9FC5F8; line-height: 30px" ><span class="span9">Boletos Inativos no Sistema</span><span class="span2 text-right"><div id="total-boletos-abertos">Total: 0,00</div></span> </div>');
        var tableView = new ArquivoRetornoTableView();
        tableView.collection.url = '/api/arquivo-retorno?id=' + this.idAquivoRetorno + '&bl_ativo=0&st_carteira=109';
        tableView.container = "#total-boletos-abertos";
        tableView.collection.fetch();
        $("#container-boletos-abertos").append(tableView.render().$el);
    },
    boletosQuitados: function () {
        $("#container-boletos-quitados").html('<div class="row-fluid" style="background-color: #B6D7A8; line-height: 30px"><span class="span9">Boletos quitados que constam no arquivo de retorno</span><span class="span2 text-right"><div id="total-boletos-quitados">Total: 0,00</div> </span> </div>');
        var tableView = new ArquivoRetornoTableView();
        tableView.collection.url = '/api/arquivo-retorno?id=' + this.idAquivoRetorno + '&bl_quitado=1&st_carteira=109&nu_maior=0';
        tableView.container = "#total-boletos-quitados";
        tableView.collection.fetch();
        $("#container-boletos-quitados").append(tableView.render().$el);
    },
    boletosNaoLocalizados: function () {
        $("#container-boletos-nao-localizados").html('<div class="row-fluid" style="background-color: #FFE599; line-height: 30px"><span class="span9">Boletos não encontrados no sistema</span><span class="span2 text-right"><div id="total-boletos-nao-localizados">Total: 0,00</div> </span> </div>');
        var tableView = new ArquivoRetornoTableView();
        tableView.collection.url = '/api/arquivo-retorno?id=' + this.idAquivoRetorno + '&id_lancamento=is null&st_carteira=109';
        tableView.container = "#total-boletos-nao-localizados";
        tableView.collection.fetch();
        $("#container-boletos-nao-localizados").append(tableView.render().$el);
    },
    boletosPagamentomenorOriginal: function(){
      $(this.regions.pagamentoMenorRegion).html('<div class="row-fluid" style="background-color: #e9dae9; line-height: 30px"><span class="span9">Boletos não quitados no sistema - Pagamento Parcial</span><span class="span2 text-right"><div id="total-boletos-pagamento-menor-original">Total: 0,00</div> </span> </div>');
        var tableView = new ArquivoRetornoTableView();
        tableView.collection.url = '/api/arquivo-retorno?id=' + this.idAquivoRetorno + '&id_lancamento=is not null&st_carteira=109&nu_maior=1';
        tableView.container = "#total-boletos-pagamento-menor-original";
        tableView.collection.fetch();
        $(this.regions.pagamentoMenorRegion).append(tableView.render().$el);
    },
    boletosG1: function () {
        $("#container-boletos-g1").html('<div class="row-fluid" style="background-color: #EA9999; line-height: 30px"><span class="span9">Boletos do sistema Gestor 1</span><span class="span2 text-right"><div id="total-boletos-g1">Total: 0,00</div> </span> </div>');
        var tableView = new ArquivoRetornoTableView();
        tableView.collection.url = '/api/arquivo-retorno?id=' + this.idAquivoRetorno + '&st_carteira=175';
        tableView.container = "#total-boletos-g1";
        tableView.collection.fetch();
        $("#container-boletos-g1").append(tableView.render().$el);
    }
});
/***********************
 *   imports
 **********************/

var tplRow = '';
$.get('/js/backbone/templates/arquivo-retorno/tpl-row.html?_=' + new Date().getTime(), '', function(data) {
    tplRow = data;
});

/***********************
 *  AreaRowView
 *  Reponsavel por renderizar uma linha da AreaTableView
 ***********************/

var ArquivoRetornoRowView = Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template(tplRow)
});
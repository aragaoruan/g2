/***********************
 *   imports
 **********************/
$.getScript('js/backbone/views/ArquivoRetorno/ArquivoRetornoRowView.js?_=' + new Date().getTime());
$.getScript('js/backbone/models/ArquivoRetorno.js?_=' + new Date().getTime());

var tplArquivoRetorno = '';
$.get('/js/backbone/templates/arquivo-retorno/tpl-table.html?_=' + new Date().getTime(), '', function (data) {
    tplArquivoRetorno = data;
});

/***********************
 *  AreaTableView
 *  Reponsavel por renderizar a tabela AreaTableView
 ***********************/

var ArquivoRetornoTableView = Marionette.CompositeView.extend({
    template: _.template(tplArquivoRetorno),
    childView: ArquivoRetornoRowView,
    childViewContainer: 'tbody',
    initialize: function () {
        var that = this;
        var Collection = Backbone.Collection.extend({
            model: ArquivoRetorno,
            parse: function (response) {
                if (response.length == 0) {
                    that.childView = Backbone.Marionette.ItemView.extend({
                        tagName: "tr",
                        template: _.template('<td colspan="9">Nenhum resultado Encontrado</td>'),
                    });

                    response = [{}]
                } else {
                    $(that.container).html('Total: ' + response[0].nu_valortotal);
                }
                return response;
            }
        });
        this.collection = new Collection();
    }
});
/**
 * View geral para a funcionalidade de cadastro de regras de pagamento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */

/***********************
 *   imports
 **********************/
var tpl = '';
$.get('/js/backbone/templates/arquivo-retorno/tpl.html?_=' + new Date().getTime(), '', function (data) {
    tpl = data;
});

/***********************
 *   RegraPagamentoView
 **********************/
var upload_error = false;

var ArquivoRetornoView = Marionette.LayoutView.extend({
    initialize: function () {
        this.template = _.template(tpl);
        console.log('montou');
    },
    events: {
        'click #btn-upload': 'uploadArquivoRetorno',
        'click #btn-processar-arquivo-retorno' : 'quitarBoletosArquivoRetorno'
    },
    uploadArquivoRetorno: function (e) {
        e.preventDefault();

        // Prevent multiple ajax
        if ($(e.currentTarget).hasClass('disabled')) return;

        var that = this;

        var arquivos = $('#file')[0].files;
        var formData = new FormData();

        for (var i = 0; i < arquivos.length; i++) {
            var file = arquivos[i];
            formData.append('arquivo', file, file.name);
        }

        $.ajax({
            url: 'arquivo-retorno/upload',
            type: 'post',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            progressUpload: function (evt) {
                /*if (evt.lengthComputable) {
                 var value = parseInt((evt.loaded / evt.total * 100));
                 progressBar.find('.bar').css('width', value + '%');
                 }*/
            },
            beforeSend: function () {
                $(e.currentTarget).addClass('disabled');

                $.pnotify({
                    title: 'Processando',
                    text: 'Aguarde, o sistema está processando o arquivo enviado...',
                    type: 'warning',
                    hide: false
                });
                $('.ui-pnotify').css({'z-index': 1000002});

                loading();
            },
            success: function (response, textStatus, jqXHR) {
                if (response.type == 'success') {
                    $.pnotify({title: 'Concluído', text: 'O novo arquivo foi salvo!', type: 'success'});
                    $('#id_arquivoretorno_processar').val(response.mensagem[0].id_arquivoretorno);
                    that.quitarBoletosArquivoRetorno(e, response.mensagem[0].id_arquivoretorno);
                    /*$('.ui-pnotify').css({'z-index': 1000002});
                    Backbone.history.navigate('#CadastrarBaixaBoleto/editar/' + response.mensagem[0].id_arquivoretorno, {
                        trigger: true,
                        replace: true
                    })*/
                } else
                    $.pnotify({title: 'Erro', text: response.mensagem[0], type: 'error'});
                //this.error();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                upload_error = true;
                $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
                $('.ui-pnotify').css({'z-index': 1000002});
            }
        });
    },
    quitarBoletosArquivoRetorno : function(e, id_arquivoretorno){

        e.preventDefault();
        var that = this;

        $('#btn-processar-arquivo-retorno').addClass('hidden');
        if(id_arquivoretorno == undefined || id_arquivoretorno == ''){
            id_arquivoretorno = $('#id_arquivoretorno_processar').val();
        }

        $.ajax({
            dataType: 'json',
            url: '/arquivo-retorno/quitar-lancamentos-arquivo-retorno',
            type: "POST",
            data: { id_arquivoretorno : id_arquivoretorno },
            success: function (response) {
                if (response.type == 'success') {
                    $.pnotify({title: 'Concluído', text: 'O novo arquivo foi salvo!', type: 'success'});
                    $('.ui-pnotify').css({'z-index': 1000002});
                     Backbone.history.navigate('#CadastrarBaixaBoleto/editar/' + response.mensagem[0].id_arquivoretorno, {
                         trigger: true,
                         replace: true
                     })
                } else
                    $.pnotify({title: 'Erro', text: response.mensagem[0], type: 'error'});
            },
            complete: function (response) {
                loaded();
                $(".icon-remove").trigger("click");
                $(e.currentTarget).removeClass('disabled');

                if (!error) {
                    that.render();
                    $(":file").filestyle({buttonText: "Buscar", iconName: "icon-search", buttonName: 'btn-primary'});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btn-processar-arquivo-retorno').removeClass('hidden');
                upload_error = true;
                $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
                $('.ui-pnotify').css({'z-index': 1000002});
            }
        });
    }
});
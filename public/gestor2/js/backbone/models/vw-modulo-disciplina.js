var VwModuloDisciplinaModel = Backbone.Model.extend({
	defaults: {
		 id_modulodisciplina: null
		,id_projetopedagogico: ''
		,st_projetopedagogico: ''
		,id_modulo: ''
		,st_modulo: ''
		,id_disciplina: ''
		,st_disciplina: ''
		,id_areaconhecimento: ''
		,st_areaconhecimento: ''
		,id_serie: ''
		,st_serie: ''
		,id_nivelensino: ''
		,st_nivelensino: ''
		,bl_obrigatoria: 1
		,nu_ordem: ''
		,bl_ativo: 1
		,nu_ponderacaocalculada: 0
		,nu_ponderacaoaplicada: 0
		,id_usuarioponderacao: ''
		,dt_cadastroponderacao: ''
		,nu_importancia: ''
        ,nu_cargahorariad: ''
        ,nu_repeticao : ''
        ,st_estudio: ''
        ,nu_cargahoraria: ''

    },
	url: function() {
		return this.id ? '/api/vw-modulo-disciplina/' + this.id : '/api/vw-modulo-disciplina';
	},
	idAttribute: 'id_modulodisciplina',
    toString: function(){
        return this.get("st_modulo") + ' - '+ this.get("st_disciplina");
    } 
});


var VwModuloDisciplinaPesquisaCollection = Backbone.Collection.extend({
    model: VwModuloDisciplinaModel,
    initialize: function(models, options) {
        if(typeof options != "undefined"){
            if(typeof options.id_projetopedagogico != "undefined"){
                this.data.id_projetopedagogico = options.id_projetopedagogico;
            }
        }
    },
    url: function(){
        return "/api/vw-modulo-disciplina/?"   + $.param(_.defaults(this.data));
    },
    parse: function(data) {
        if(typeof data.mensagem == "undefined"){
            return data;
        } else {
            return data.mensagem;
        }
    },
    data: {
        id_projetopedagogico: null
    }
});
var VwUsuarioPerfilEntidadeModel = Backbone.Model.extend({
	defaults: {
		 
		 id_usuario: ''
		,st_cpf: ''
		,st_nomecompleto: ''
		,id_perfil: ''
		,st_nomeperfil: ''
		,st_perfilalias: ''
		,dt_inicio: ''
		,dt_termino: ''
		,id_situacaoperfil: ''
		,st_situacaoperfil: ''
		,id_entidade: ''
		,st_nomeentidade: ''
		,st_urlimglogo: ''
		,bl_ativo: ''
		,id_sistema: ''
		,id_perfilpedagogico: ''
		,id_entidadecadastro: ''

		
    },
	url: function() {
		return this.id ? '/api/vw-usuario-perfil-entidade/' + this.id : '/api/vw-usuario-perfil-entidade';
	},
	idAttribute: 'id_usuario',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_nomecompleto");
    } 
});

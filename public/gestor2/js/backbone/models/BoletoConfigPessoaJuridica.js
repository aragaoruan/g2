var BoletoConfigPessoaJuridica = Backbone.Model.extend({
    defaults: {
        id_cartaoconfig: '',
        id_cartaobandeira: '',
        id_cartaooperadora: '',
        id_contaentidade: '',
        st_gateway: '',
        st_contratooperadora: '',
        id_sistema: '',
        st_codchave: '',
        st_caminho: ''
    }
});
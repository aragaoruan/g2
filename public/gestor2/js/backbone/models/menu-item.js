var MenuItem = Backbone.Model.extend({
    defaults: {
        migrada: false,
        id_funcionalidade: 0,
        id_tipo_funcionalidade: 3,
        id_situacao: 3,
        bl_pesquisa: false,
        bl_relatorio: false,
        classeFlexBotao: '',
        children: [],
        caption: 'Item de Menu',
        link: '#',
        icone: '',
        url: '/',
        url_edicao: '/',
        bl_delete: ''
    }
});

var MenuItemView = Marionette.ItemView.extend({
    tagName: 'li',
    template: '#link-template',
    events: { 
      'click .lvl3': 'abreLink'  
    },
    onRender: function() {
        if (this.model.get('id_tipo_funcionalidade') > 3)
            return false;
        switch (this.model.get('id_tipo_funcionalidade')) {
            case 1:
                this.$el.addClass('area');
                break;
            case 2:
                this.$el.addClass('subarea');
                break;
            case 3:
                this.$el.addClass('sub');
                this.$el.addClass('crumb');
                break;
            default:
                break;
        }

        var items = new Backbone.Collection();
        items.add(this.model.get('children'));

        if (items.size() > 0 && this.model.get('id_tipo_funcionalidade') < 3)
        {
            var id = 'submenu_' + this.model.get('id_funcionalidade');
            var ele = $('<ul/>').attr('id', id);

            if (this.model.get('id_tipo_funcionalidade') === 1)
                ele.addClass('subs');
            else
                ele.addClass('submenu');

            //this.$el.append(ele);

            var menuInterno = new CollectionView({
                collection: items,
                childViewConstructor: MenuItemView,
                childViewTagName: 'li',
                el: ele
            });

            this.$el.append(menuInterno.render().el);
        }
        return this;
    },
    abreLink: function (){
        
        if(this.model.get('migrada')){
            console.log('Funcionalidade Migrada');
        } else {
            if(this.model.get('bl_pesquisa')) {
                console.log('Pesquisa');
            } else {
                if(this.model.get('bl_relatorio')) {
                    console.log('Relatorio');
                } else {
                    console.log('Funcionalidade Flex');
                }
            }
        }

        return false; // acaba com o bubble do evento para o resto da hierarquia do menu
    }
    
});

var Menu = Backbone.Collection.extend({
    model: MenuItem,
    url: '/menu/json'
});


/*
 * Inserção do Marionette.js para substituir o modelo de Aplicação que estava 
 * sendo criado manualmente
 */

var G2S = new Marionette.Application();
G2S.addRegions({
    main: 'body'
})

var G2SController = Marionette.Controller.extend({
    mostraHome: function() {

        console.log('Mostrando Home ');

    }
});
var G2SRouter = Marionette.AppRouter.extend({
    controller: G2SController,
    appRoutes: {
        "": "mostraHome"
    }
});

G2S.addInitializer(function() {

    var controller = new G2SController();
    var router = new G2SRouter({controller: controller});
    console.log('created the main app');

});

G2S.on('initialize:after', function() {
    if (Backbone.history) {
        Backbone.history.start();
    }
    console.log('after initialize method called');
});


if (!menuData.length) {
    m = new Menu();
    m.fetch({success: function() {
            menuPrincipal = new Marionette.CompositeView({
                tagName: 'div',
                className: 'navbar-inner',
                template: '#menu-template',
                itemViewContainer: '#nav',
                collection: m,
                itemView: MenuItemView,
            });
        }
    });
} else {
    m = new Menu(menuData);
    menuPrincipal = new Marionette.CompositeView({
        tagName: 'div',
        className: 'navbar-inner',
        template: '#menu-template',
        itemViewContainer: '#nav',
        collection: m,
        itemView: MenuItemView,
    });
}
var G2SLayout = Marionette.Layout.extend({
    template: '#mainApp',
    regions: {
        breadcrumb: '#breadcrumb',
        menu: '#menu',
        content: '#content'
    },
    onRender: function() {
        console.log('Kickoff the application');
//        console.log(menuPrincipal.render().el);
    }
});

var BreadCrumbLayout = Marionette.Layout.extend({
    template: "#breadcrumb-template",
    className: 'container',
    regions: {
        path: "#path",
        extra: "#extra"
    }
});


$(document).ready(function(){
    g2sLayout = new G2SLayout();
    breadcrumbLayout = new BreadCrumbLayout();
    G2S.start();
    G2S.main.show(g2sLayout);
    g2sLayout.menu.show(menuPrincipal);
    g2sLayout.breadcrumb.show(breadcrumbLayout);
    
    G2S.loadingTimer = null;
    
    $('.subs > li > a').attr('href','javascript:');
    $('.area > a').attr('href', 'javascript:'); // remove o link dos items principais

    
});


/* Final do código referente ao Marionette.js */
var ProdutoComboModel = Backbone.Model.extend({ 
	defaults: { 
		 id_produtocombo: null
		,id_produto: ''
		,id_produtoitem: ''
		,id_usuario: ''
		,dt_cadastro: ''
		,nu_descontoporcentagem: ''
	}
	,url: function() { return this.id ? '/api/produto-combo/' + this.id : '/api/produto-combo'; }
    ,idAttribute: 'id_produtocombo'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_produtocombo");
    }
});

var EntidadeRelacaoModel = Backbone.Model.extend({
    defaults: {
        id_entidade: "",
        bl_ativo: "",
        st_nomeentidade: "",
        st_urlimglogo: "",
        bl_acessasistema: "",
        nu_cnpj: "",
        st_razaosocial: "",
        nu_inscricaoestadual: "",
        st_urlsite: "",
        dt_cadastro: "",
        st_cnpj: "",
        st_wschave: "",
        st_conversao: "",
        id_situacao: "",
        id_entidadecadastro: "",
        id_usuariocadastro: "",
        sistemas: "",
        projetoPedagogico: "",
        bl_relacao: "",
        st_siglaentidade: "",
        st_urlportal: "",
        st_urlnovoportal: "",
        st_apelido: "",
        st_urllogoutportal: "",
        id_usuariosecretariado: ""
    }
});
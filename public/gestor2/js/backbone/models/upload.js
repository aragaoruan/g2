var UploadModel = Backbone.Model.extend({ 
	defaults: { 
		id_upload: '',
		st_upload: ''
	}, 
	idAttribute: 'id_upload',
	url: function() { 
		return this.id ? '/api/upload/' + this.id : '/api/upload'; 
	},
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_upload");
    }     
});
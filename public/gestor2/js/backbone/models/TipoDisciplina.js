var TipoDisciplina = Backbone.Model.extend({
    defaults: {
        id_tipodisciplina: '',
        st_tipodisciplina: '',
        st_descricao: ''
    },
    idAttribute: 'id_tipodisciplina',
    url: function() {
        return '/api/tipo-disciplina/' + this.get('id') ? this.get('id') : '';
    }
});
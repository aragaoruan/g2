var AreaConhecimentoModel = Backbone.Model.extend({
    defaults: {
        id_areaconhecimento: null,
		id_situacao: null,
        st_descricao: '',
        bl_ativo: true,
        st_areaconhecimento: '',
        id_entidade: null,
        id_usuariocadastro: null,
        id_areaconhecimentopai: 0,
        id_tipoareaconhecimento: null,
        st_tituloexibicao: ''
    },
    url: function() {
        return this.id ? '/api/area-conhecimento/' + this.id : '/api/area-conhecimento';
    },
	idAttribute: 'id_areaconhecimento',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_areaconhecimento");
    }
});
var CargaHoraria = Backbone.Model.extend({
    defaults: {
        nu_cargahoraria: 0
    },
    idAttribute: 'id_cargahoraria',
    url: function () {
        return 'api/carga-horaria/' + (this.get('id') || '');
    }
});
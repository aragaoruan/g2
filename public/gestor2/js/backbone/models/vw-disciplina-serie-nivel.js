var VwDisciplinaSerieNivelModel = Backbone.Model.extend({
	defaults: {
		 id_disciplina: ''
		, st_disciplina: ''
		, id_entidade: ''
		, st_descricao: ''
		, nu_identificador: ''
		, nu_cargahoraria: ''
		, bl_ativa: ''
		, id_situacao: ''
		, id_serie: ''
		, st_serie: ''
		, id_serieanterior: ''
		, id_nivelensino: ''
		, st_nivelensino: ''
		, bl_obrigatoria: ''
		, nu_repeticao: ''
    },
	url: function() {
		return this.id ? '/api/vw-disciplina-serie-nivel/' + this.id : '/api/vw-disciplina-serie-nivel';
	},
	idAttribute: 'id_disciplina',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_disciplina");
    } 
});

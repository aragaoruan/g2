var TextoSistema = Backbone.Model.extend({
    defaults: {
        id_textosistema: '',
        st_textosistema: '',
        st_texto: '',
        dt_inicio: '',
        dt_fim: '',
        dt_cadastro: '',
        id_usuario: '',
        id_entidade: '',
        id_textoexibicao: '',
        id_textocategoria: '',
        id_situacao: '',
        id_orientacaotexto: ''
    },
    url: function() {
        return 'api/textos/' + this.get('id') ? this.get('id') : '';
    }
});
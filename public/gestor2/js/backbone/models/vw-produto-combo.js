var VwProdutoComboModel = Backbone.Model.extend({ 
	defaults: { 
		 
		 id_produtocombo: ''
		,id_combo: ''
		,st_combo: ''
		,id_produtoitem: ''
		,nu_descontoporcentagem: ''
		,nu_valorprodutoitem: ''
		,nu_valordescontoitem: ''
		,nu_valor: ''
		,nu_valormensal: ''
		,id_tipoprodutovalor: ''
		,st_tipoprodutovalor: ''
		,id_produto: ''
		,st_produto: ''
		,id_entidade: ''
		,nu_gratuito: ''
		,id_tipoproduto: ''
		,st_tipoproduto: ''
		,id_produtovalor: ''
		,id_situacao: ''
		,st_situacao: ''

	}
	,url: function() { return this.id ? '/api/vw-produto-combo/' + this.id : '/api/vw-produto-combo'; }
    ,idAttribute: 'id_produtocombo'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_combo") + ' - ' + this.get("st_produto");
    }
});

/**
 * Model for Disciplina
 * @type Backbone Model Disciplina
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @version 2013-18-12
 */
var Disciplina = Backbone.Model.extend({
    defaults: {
        id_disciplina: '',
        st_disciplina: '',
        st_descricao: '',
        nu_identificador: '',
        nu_cargahoraria: '',
        nu_creditos: '',
        nu_codigoparceiro: '',
        bl_ativa: '',
        bl_compartilhargrupo: '',
        id_situacao: '',
        st_tituloexibicao: '',
        id_tipodisciplina: '',
        id_entidade: '',
        dt_cadastro: '',
        id_usuariocadastro: '',
        st_identificador: '',
        st_ementacertificado: '',
        bl_provamontada: '',
        bl_disciplinasemestre: '',
        nu_repeticao: '',
        sg_uf: '',
        st_imagem: '',
        is_editing: false,
        st_apelido:'',
        id_grupodisciplina: '',
        st_provarecuperacaointegracao: '',
        st_provaintegracao: '',
        st_codprovarecuperacaointegracao: '',
        st_codprovaintegracao: ''
    },
    idAttribute: 'id_disciplina',
    url: function() {
        return this.id ? '/api/disciplina/' + this.id : '/api/disciplina';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

/**
 * @type Backbone Collection Disciplina
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @version 2013-18-12
 */
var DisciplinaCollection = Backbone.Collection.extend({
    model: Disciplina,
    url: "/api/disciplina"
});
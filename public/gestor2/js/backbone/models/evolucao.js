/*
 * 
 * Modelo para guardar os Tipos
 */
var Evolucao = Backbone.Model.extend({
    defaults: {
        id_evolucao: null,
        st_evolucao: '',
        st_tabela: '',
        st_campo: ''
    },
    idAttribute: 'id_evolucao',
    url: function () {
        return this.id ? '/api/evolucao/' + this.id : '/api/evolucao';
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    toString: function () {
        return this.get("st_evolucao");
    }
});


/*
 *  Coleção das Evoluções na requisição
 *  padrão /api/evolucao.
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var Evolucoes = Backbone.Collection.extend({
    model: Evolucao,
    url: function () {
        return "/api/evolucao/?" + $.param(_.defaults(this.data));
    },
    parse: function (data) {
        return data;
    },
    data: {
        st_tabela: null
    }
});
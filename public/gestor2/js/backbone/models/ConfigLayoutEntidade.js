var ConfigLayoutEntidade = Backbone.Model.extend({
    defaults: {
        bl_logogrande: '',
        bl_cssportal: '',
        bl_logopadrao: '',
        bl_urlimglogoentidade: '',
        bl_favicon: '',
        st_urlimgapp: ''
    }
});
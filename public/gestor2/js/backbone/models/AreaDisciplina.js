/**
 * @type Model AreaDisciplina
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var AreaDisciplina = Backbone.Model.extend({
    defaults: {
        id_disciplina: '',
        id_areaconhecimento: ''
    },
//    idAttribute: 'id_disciplina',
    url: function() {
        return this.id ? '/api/area-disciplina/' + this.id : '/api/area-disciplina';
    }
});
/**
 * @type Collection AreaDisciplina
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var AreaDisciplinaCollection = Backbone.Collection.extend({
    model: AreaDisciplina,
    url: "/api/area-disciplina"
});
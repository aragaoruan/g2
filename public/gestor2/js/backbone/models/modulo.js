var ModuloModel = Backbone.Model.extend({
	defaults: {
		 id_modulo: null
		,st_modulo: ''
		,st_tituloexibicao: ''
		,bl_ativo: ''
		,st_descricao: ''
		,id_situacao: ''
		,id_projetopedagogico: ''
		,id_moduloanterior: ''		
    },
	url: function() {
		return this.id ? '/api/modulo/' + this.id : '/api/modulo';
	},
	idAttribute: 'id_modulo',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

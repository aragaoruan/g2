var VwSalaDisciplinaParaTransferencia = Backbone.Model.extend({
    defaults: {
        st_disciplina: '',
        st_notaead: '',
        st_notatcc: '',
        st_notafinal: '',
        dt_abertura: '',
        dt_encerramento: '',
        bl_check: '',
        id_encerramentosala: '',
        id_projetotransfere: '',
        id_entidadetransfere: '',
        id_projetopedagogico: '',
        id_saladeaula: '',
        st_saladeaula: '',
        id_disciplina: '',
        id_matricula: ''
    }
});
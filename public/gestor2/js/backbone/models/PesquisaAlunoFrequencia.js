/**
 * Created by UNYLEYA on 13/10/14.
 */
var PesquisaAlunoFrequencia = Backbone.Model.extend({
    defaults: {
        sg_uf: '',
        id_aplicadorprova: '',
        st_aplicadorprova: '',
        id_municipio: '',
        st_municipio: '',
        dt_aplicacao: ''
    }
});
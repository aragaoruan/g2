var VwTextoSistema = Backbone.Model.extend({
    idAttribute: 'id_textosistema',
    url: function () {
        return 'api/vw-texto-sistema/' + (this.id ? this.id : '');
    },
    parse: function (data) {
        var returns = {};
        if (typeof data == 'object') {
            if (data.mensagem) {
                returns = data.mensagem;
            } else {
                returns = data;
            }
        } else {
            if (data != "Nenhum registro encontrado!") {
                data = $.parseJSON(data);
                returns = data.mensagem ? data.mensagem : data;
            } else {
                returns = {};
            }

        }
        return returns;
    }
});

var VwTextoSistemaCollection = Backbone.Collection.extend({
    model: VwTextoSistema,
    url: function () {
        return 'api/vw-texto-sistema/' + (this.id ? this.id : '');
    },
    parse: function (data) {
        var returns = [];
        if (typeof data == 'object') {
            if (data.type == 'success') {
                if (data.mensagem) {
                    returns = data.mensagem;
                } else {
                    returns = data;
                }
            }
        } else {
            data = $.parseJSON(data);
            returns = data.mensagem ? data.mensagem : data;
        }

        return returns;
    }
});

var ModeloVenda = Backbone.Model.extend({
    defaults: {
        id_modelovenda: null,
        st_modelovenda: ''
    },
    idAttribute: 'id_modelovenda',
    url: function() {
        return this.id ? '/api/modelo-venda/' + this.id : '/api/modelo-venda';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_modelovenda");
    },        
    schema: {
        st_modelovenda: { type: 'Text', title: 'Tipo' },
        id_modelovenda: { type: 'Hidden' }
    }
});


var ModelosVendaCollection = Backbone.Collection.extend({
    model: ModeloVenda,
    url: function(){ 
        return "/api/modelo-venda/?"   + $.param(_.defaults(this.data));
    },
    data: {
        st_modelovenda: null
    }
});
var EntidadeFinanceiro = Backbone.Model.extend({
    idAttribute: 'id_entidadefinanceiro',
    url: function () {
        return 'api/entidade-financeiro/' + (this.id ? this.id : '');
    },
    parse: function (data) {
        var returns = {};
        if (typeof data == 'object') {
            if (data.mensagem) {
                returns = data.mensagem;
            } else {
                returns = data;
            }
        } else {
            data = $.parseJSON(data);
            returns = data.mensagem ? data.mensagem : data;
        }
        return returns;
    }
});

var EntidadeFinanceiroCollection = Backbone.Collection.extend({
    model: EntidadeFinanceiro,
    url: function () {
        return 'api/entidade-financeiro/' + (this.id ? this.id : '');
    },
    parse: function (data) {
        var returns = {};
        if (typeof data == 'object') {
            if (data.mensagem) {
                returns = data.mensagem;
            } else {
                returns = data;
            }
        } else {
            data = $.parseJSON(data);
            returns = data.mensagem ? data.mensagem : data;
        }

        return returns;
    }
});
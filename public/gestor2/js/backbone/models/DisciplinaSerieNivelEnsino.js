/**
 * Modelo do Backbone de DisciplinaSerieNivelEnsino
 * @type DisciplinaSerieNivelEnsino
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var DisciplinaSerieNivelEnsino = Backbone.Model.extend({
    defaults: {
        id_disciplina: '',
        id_serie: '',
        id_nivelensino: '',
        is_editing: false,
    },
    url: function() {
        return this.id ? '/api/disciplina-serie-nivel-ensino/' + this.id : '/api/disciplina-serie-nivel-ensino';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

/**
 * Collection do Backbone para DisciplinaSerieNivelEnsino
 * @type DisciplinaSerieNivelEnsinoCollection
 * @exp;Backbone
 * @pro;Collection
 * @call;extend
 */
var DisciplinaSerieNivelEnsinoCollection = Backbone.Collection.extend({
    model: DisciplinaSerieNivelEnsino,
    url: "/api/disciplina-serie-nivel-ensino"
});
/**
 * Model for Categoria
 * @type Backbone Model Categoria
 * @author Elcio Guimarães <elcioguimares@gmail.com>
 * 
 */
var VwCategoriaProdutoModel = Backbone.Model.extend({
    defaults: {
    	id_categoria: null,
    	st_nomeexibicao: '',
    	st_categoria: '',
    	id_usuariocadastro: '',
    	id_entidadecadastro: '',
    	id_uploadimagem: '',
    	id_categoriapai: '',
    	id_situacao: '',
    	dt_cadastro: '',
    	bl_ativo: '',
    	id_produto: ''
    },
    idAttribute: 'id_categoria',
    url: function() {
        return this.id ? '/api/vw-categoria-produto/' + this.id : '/api/vw-categoria-produto';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
    	return this.get('st_categoria');
    }
});

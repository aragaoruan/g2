var LivroColecaoModel = Backbone.Model.extend({ 
	defaults: { 
		id_livrocolecao: ''
		,id_entidadecadastro: ''
		,id_usuariocadastro: ''
		,st_livrocolecao: ''
		,bl_ativo: ''
		,dt_cadastro: '' 
	}
	, url: function() { 
		return this.id ? '/api/livro-colecao/' + this.id : '/api/livro-colecao'; 
	}
    ,idAttribute: 'id_livrocolecao'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_livrocolecao");
    }
});


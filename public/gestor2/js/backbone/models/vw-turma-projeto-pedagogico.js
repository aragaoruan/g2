/**
 * Created by Ilson Nóbrega on 24/09/14.
 */
var VwTurmaProjetoPedagogico = Backbone.Model.extend({

    defaults: {
        id_projetopedagogico: '',
        st_projetopedagogico: '',
        st_tituloexibicao: '',
        id_turma: '',
        id_entidadecadastro: '',
        st_turma: '',
        bl_ativo: ''
    },
    idAttribute: 'id_turma',
//    data: {
//        id_projetopedagogico: null
//    },
    url: function() {
        return this.id ? '/api/produto-projeto-pedagogico/' + this.id : '/api/produto-projeto-pedagogico';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        if(this.get("id_turma")) {
            return this.get("id_turma") + ' - ' + this.get("st_turma");
        } else {
            return this.get("st_turma");
        }

    }
});


var VwTurmaProjetoPedagogicoCollection = Backbone.Collection.extend({
    initialize: function(models, options) {

        if(typeof options != "undefined"){

            options.data || (options.data = null);

            if(options.data)
                this.data = options.data;

        }


    },
    model: VwTurmaProjetoPedagogico,
    url: function(){
        return "/api/turmas-projeto/?"   + $.param(_.defaults(this.data));
    },
    data: {
        id_projetopedagogico: null
    }
});
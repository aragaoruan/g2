var VwRegraPagamento = Backbone.Model.extend({
    defaults: {
        id_regrapagamento: '',
        id_entidade: '',
        st_entidade: '',
        id_areaconhecimento: '',
        st_areaconhecimento: '',
        id_projetopedagogico: '',
        st_projetopedagogico: '',
        id_professor: '',
        st_professor: '',
        id_cargahoraria: '',
        nu_cargahoraria: '',
        nu_valor: '',
        id_tipodisciplina: '',
        st_tipodisciplina: '',
        dt_cadastro: ''
    },
    idAttribute: 'id_regrapagamento',
    url: function() {
        return 'api/vw-regra-pagamento/' + (this.id ? this.id : '');
    }
});
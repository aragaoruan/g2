var VwSalaDisciplinaTransferencia = Backbone.Model.extend({
    defaults: {
        id_saladeaula:'',
        st_saladeaula:'',
        nu_maxalunos:'',
        id_projetopedagogico:'',
        id_trilha:'',
        id_turma:'',
        id_disciplina:'',
        id_entidade:'',
        nu_alocados:'',
        dt_abertura:'',
        dt_encerramento:'',
        st_disciplina:''
    }
});
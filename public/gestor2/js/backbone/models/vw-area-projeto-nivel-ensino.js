var VwAreaProjetoNivelEnsinoModel = Backbone.Model.extend({
    defaults: {
        id_areaconhecimento: '',
        id_projetopedagogico: '',
        id_entidade: '',
        st_projetopedagogico: '',
        st_tituloexibicao: '',
        id_nivelensino: '',
        st_nivelensino: '',
        st_situacao: '',
        st_nomeentidade: ''
    },
    url: function () {
        return this.id ? '/api/vw-area-projeto-nivel-ensino/' + this.id : '/api/vw-area-projeto-nivel-ensino';
    },
    idAttribute: 'id_projetopedagogico',
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    toString: function () {
        return this.get("st_projetopedagogico");
    }
});

var ContratoRegraModel = Backbone.Model.extend({
    defaults: {
        st_contratoregra: '', 
        nu_contratoduracao: 0,
        nu_mesesmulta: 1,
        bl_proporcaomes: '',
        id_projetocontratoduracaotipo: '',
        id_entidade: '',
        id_usuariocadastro: '',
        nu_contratomultavalor: '',
        bl_renovarcontrato: '',
        nu_mesesmensalidade: 0,
        bl_ativo: true,
        dt_cadastro: '1970-01-01 00:00',
        nu_tempoextensao: 0,
        id_contratomodelo: '',
        id_extensaomodelo: '',
        id_contratoregra: '',
        id_modelocarteirinha: ''
    },
    idAttribute: 'id_contratoregra',
    url: function() {
        return this.id ? '/api/contrato-regra/' + this.id : '/api/contrato-regra';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_contratoregra");
    }
});
var SerieModel = Backbone.Model.extend({ 
	defaults: { 
		 id_serie: 			''
		,st_serie: 			''
		,id_serieanterior: 	''
		,nivelEnsino: 		''
	},
	url: function() { 
		return this.id ? '/api/serie/' + this.id : '/api/serie'; 
	},
	idAttribute: 'id_serie',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_serie");
    }
});
var ProdutoImagemModel = Backbone.Model.extend({ 
	defaults: { 
		id_produtoimagem: '',
		id_produto: '',
		id_upload: '',
		bl_ativo: ''
	}, 
	idAttribute: 'id_produtoimagem',
	url: function() { 
		return this.id ? '/api/produto-imagem/' + this.id : '/api/produto-imagem'; 
	},
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_upload");
    }     
});
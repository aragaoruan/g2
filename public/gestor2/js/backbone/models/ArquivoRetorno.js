var ArquivoRetorno = Backbone.Model.extend({
    defaults: {
        id_arquivoretorno: ''
    },
    idAttribute: 'id_arquivoretornolancamento',
    url: function() {
        return 'api/arquivo-retorno' + this.get('id') ? this.get('id') : '';
    }
});
var MensagemCobranca = Backbone.Model.extend({
    defaults: {
        id_mensagemcobranca: '',
        nu_diasatraso: '',
        dt_cadastro: '',
        bl_ativo: '',
        bl_antecedencia: '',
        id_textosistema: '',
        id_entidade: ''
    },
    idAttribute: 'id_mensagemcobranca',
    url: function() {
        return this.id ? 'api/mensagem-cobranca/' + this.id : 'api/mensagem-cobranca/';
    }
});
var NivelEnsinoModel = Backbone.Model.extend({
    defaults: {
        id_nivelensino: null,
        st_nivelensino: ''
    },
    url: function() {
        return this.id ? '/api/nivel-ensino/' + this.id : '/api/nivel-ensino';
    },
	idAttribute: 'id_nivelensino',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_nivelensino");
    }
});
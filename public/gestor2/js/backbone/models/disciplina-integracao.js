/**
 * Modelo do Backbone de DisciplinaIntegracao
 * @type DisciplinaIntegracao
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
var DisciplinaIntegracaoModel = Backbone.Model.extend({
    defaults: {
        id_disciplinaintegracao: '',
        id_disciplina: '',
        id_sistema: '',
        id_usuariocadastro: '',
        st_codsistema: '',
        dt_cadastro: '',
        id_entidade: '',
        bl_ativo: '',
        st_salareferencia: '',
        is_editing: false
    },
    url: function() {
        return this.id ? '/api/disciplina-integracao/' + this.id : '/api/disciplina-integracao';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});
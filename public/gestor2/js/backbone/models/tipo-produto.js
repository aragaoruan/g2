/*
 * 
 * Modelo para guardar os Tipos
 */
var TipoProduto = Backbone.Model.extend({
    defaults: {
        id_tipoproduto: null,
        st_tabelarelacao: '',
        st_tipoproduto: ''
    },
    idAttribute: 'id_tipoproduto',
    url: function() {
        return this.id ? '/api/tipo-produto/' + this.id : '/api/tipo-produto';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_tipoproduto");
    },        
    schema: {
        st_tabelarelacao: { type: 'Text', title: 'Tabela de Relação' },
        st_tipoproduto: { type: 'Text', title: 'Tipo' },
        id_tipoproduto: { type: 'Hidden' }
    }
});


/*
 *  Coleção dos tipos de produto na requisição 
 *  padrão /api/tipo-produto. 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var TiposProduto = Backbone.Collection.extend({
    model: TipoProduto,
    url: "/api/tipo-produto",
    parse: function(data) {
        return data.mensagem;
    }
});
var VwDisciplinaIntegracaoMoodleModel = Backbone.Model.extend({
	defaults: {
        id_disciplinaintegracao: '',
        id_disciplina: '',
        id_sistema: '',
        id_usuariocadastro: '',
        st_codsistema: '',
        dt_cadastro: '',
        id_entidade: '',
        bl_ativo: '',
        st_nomeentidade: '',
        st_sistema: '',
        st_salareferencia:''
	},
	url: function() { 
		return this.id ? '/api/vw-disciplina-integracao-moodle/' + this.id : '/api/vw-disciplina-integracao-moodle';
	},
	idAttribute: 'id_serie',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_nomeentidade");
    }
});
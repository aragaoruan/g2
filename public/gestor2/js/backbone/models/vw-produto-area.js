/**
 * Model for VwProdutoArea
 * @type Backbone Model VwProdutoArea
 * @author Elcio Guimarães <elcioguimares@gmail.com>
 * 
 */
var VwProdutoAreaModel = Backbone.Model.extend({ 
	
	defaults: { 
		id_produtoarea: null,
		id_produto: '',
		id_areaconhecimento: '',
		st_areaconhecimento: '',
		id_entidade: '',
		id_situacao: '',
		st_situacao: ''
	}, 
	idAttribute: 'id_produtoarea',
	url: function() { 
		return this.id ? '/api/vw-produto-area/' + this.id : '/api/vw-produto-area'; 
	},
    
	toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
    	return this.get('st_areaconhecimento');
    }
	
});
/* 
 * Model AssuntoModel Backbone
 */

var VwOcorrenciasPessoaModel = Backbone.Model.extend({
    defaults: {
        id:null,
        st_nomeinteressado: null,
        id_ocorrencia: null,
        st_titulo: null,
        st_atendente: null,
        id_atendente: null,
        st_evolucao: null,
        id_evolucao: null,
        dt_cadastroocorrencia: null,
        dt_ultimotramite: null,
        nu_horasatraso: null,
        nu_diasaberto: null,
        st_assuntoco: null,
        st_cor: null,
        st_urlportal: null,
        st_emailinteressado: null,
        id_matricula: null,
        st_telefoneinteressado: null,
        st_ocorrencia: null,
        st_assuntocopai:null,
        id_assuntocopai:null,
        id_assuntoco: null,
        st_urlacesso: null,
        st_saladeaula: null,
        st_tituloexibicao: null,
        id_ocorrenciaoriginal: null,
        st_coordenadortitular: null
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
});

var VwOcorrenciasPessoaCollection = Backbone.Collection.extend({
    model: VwOcorrenciasPessoaModel,
    url: '/api/ocorrencia/'
});
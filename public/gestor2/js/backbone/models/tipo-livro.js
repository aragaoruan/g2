var TipoLivroModel = Backbone.Model.extend({ 
	defaults: { 
		id_tipolivro: ''
		,st_tipolivro: ''
	}
	, url: function() { return this.id ? '/api/tipo-livro/' + this.id : '/api/tipo-livro'; }
    ,idAttribute: 'id_tipolivro'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_tipolivro");
    }
});

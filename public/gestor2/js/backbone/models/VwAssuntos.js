/* 
 * Model AssuntoModel Backbone
 */

var VwAssuntosModel = Backbone.Model.extend({
    defaults: {
        id_assuntoco: 1,
        id_assuntocopai: 1,
        id_entidade: 1,
        id_situacao: 1,
        id_tipoocorrencia: 1,
        bl_ativo: true,
        st_assuntoco: 'Situacao',
        bl_notificaresponsavel: true,
        bl_notificaatendente: true,
        bl_autodistribuicao: true,
        bl_abertura: true
    }
});

var VwAssuntosCollection = Backbone.Collection.extend({
    model: VwAssuntosModel,
    url: "/ocorrencia/find-sub-assunto/"
});
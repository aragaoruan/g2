/**
 * Model for CategoriaProduto
 * @type Backbone Model CategoriaProduto
 * @author Elcio Guimarães <elcioguimares@gmail.com>
 * 
 */
var CategoriaProdutoModel = Backbone.Model.extend({ 
	
	defaults: { 
		id_categoria: '',
		id_produto: '',
		id_usuariocadastro: '',
		dt_cadastro: ''
	}, 
	
	url: function() { 
		return this.id ? '/api/categoria-produto/' + this.id : '/api/categoria-produto'; 
	},
    
	toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
	
});
var ProdutoValorModel = Backbone.Model.extend({
    defaults: {
        
        id_produtovalor: null,
        id_produto: '',
        dt_cadastro: null,
        dt_termino: null,
        dt_inicio: null,
        id_usuariocadastro: '',
        id_tipoprodutovalor: 4, // Fixo
        nu_valor: '0.00',
        nu_valormensal: null,
        nu_basepropor: null

    },
    idAttribute: 'id_produtovalor',
    url: function() {
        return this.id ? '/api/produto-valor/' + this.id : '/api/produto-valor';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("nu_valor");
    },        
    schema: {
        nu_valor: { 
            type: 'Text', 
            title: 'Valor',
            editorAttrs: {
                class: 'span11 desabilitactrlv',
                maxlength: 10, max: 9999999999
            }
        },
        nu_valormensal: {
        	type: 'Text', 
        	title: 'Valor Mensal', 
        	// validators: [{ type: 'required', message: 'Você precisa informar o Valor!' } ], 
        	editorAttrs: { class: 'span11 desabilitactrlv', maxlength: 10, max: 9999999999 } //, 'oninput': 'maxLengthCheck(this)'  }
        },
        
        dt_inicio: { 
            type: 'Text', 
            title: 'Início', 
            editorAttrs: { class: 'span12 datepicker_range_min maskDate desabilitactrlv', 'data-maximo': 'dt_termino' }
        },
        
        dt_termino: { 
            type: 'Text', 
            title: 'Término', 
            editorAttrs: { class: 'span12 datepicker_range_max maskDate desabilitactrlv' , 'data-minimo': 'dt_inicio' 	}
        }
    }
});

/*
 *  Coleção das Situações na requisição
 *  padrão /api/situacao.
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var ProdutoValorCollection = Backbone.Collection.extend({
    model: ProdutoValorModel,

    initialize: function(models, options) {
        if(typeof options != "undefined"){
            if(typeof options.id_produto != "undefined"){
                this.data.id_produto = options.id_produto;
            }
            if(typeof options.id_tipoprodutovalor != "undefined"){
                this.data.id_tipoprodutovalor = options.id_tipoprodutovalor;
            }
        }

    },

    url: function(){
        return "/api/produto-valor/?"   + $.param(_.defaults(this.data));
    },
//    parse: function(data) {
//        return data.mensagem;
//    },
    data: {
        id_produto: null, id_tipoprodutovalor: null
    }
});
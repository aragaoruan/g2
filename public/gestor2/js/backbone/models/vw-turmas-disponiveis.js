var VwTurmasDisponiveis = Backbone.Model.extend({
    defaults: {
        id_turmaprojeto: '',
        id_turma: '',
        id_projetopedagogico: '',
        nu_alunos: '',
        id_usuariocadastro: '',
        id_situacao: '',
        id_entidadecadastro: '',
        st_turma: '',
        st_tituloexibicao: '',
        nu_maxalunos: '',
        dt_inicioinscricao: '',
        dt_fiminscricao: '',
        dt_inicio: '',
        dt_fim: '',
        dt_cadastro: '',
        bl_ativo: ''
    },
    idAttribute: 'id_turmaprojeto',
    url: function() {
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("id_turma") + ' - ' + this.get("st_turma");
    }
});
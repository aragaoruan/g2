var Matricula = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        dt_concluinte: '',
        dt_termino: '',
        dt_cadastro: '',
        dt_inicio: '',
        id_periodoletivo: '',
        id_vendaproduto: '',
        id_entidadeatendimento: '',
        id_entidadematriz: '',
        id_entidadematricula: '',
        bl_ativo: '',
        bl_institucional: '',
        id_mensagemrecuperacao: '',
        id_evolucaoagendamento: '',
        bl_documentacao: '',
        st_codcertificacao: '',
        id_projetopedagogico: '',
        id_usuario: '',
        id_usuariocadastro: '',
        id_evolucao: '',
        id_matriculaorigem: '',
        id_situacaoagendamento: '',
        id_situacao: '',
        id_turma: '',
        id_cancelamento: ''
    },
    idAttribute: 'id_matricula',
    url: function() {
        return this.id ? '/api/matricula/' + this.id : '/api/matricula';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("id_matricula");
    }
});
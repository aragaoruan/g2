/**
 * Model for ProdutoArea
 * @type Backbone Model ProdutoArea
 * @author Elcio Guimarães <elcioguimares@gmail.com>
 * 
 */
var ProdutoAreaModel = Backbone.Model.extend({ 
	
	defaults: { 
		id_produtoarea: null,
		id_produto: '',
		id_areaconhecimento: ''
	}, 
	idAttribute: 'id_produtoarea',
	url: function() { 
		return this.id ? '/api/produto-area/' + this.id : '/api/produto-area'; 
	},
    
	toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
	
});
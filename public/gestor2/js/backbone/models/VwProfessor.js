var VwProfessor = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        id_entidade: '',
        st_nomecompleto: '',
        st_cpf: '',
        id_perfil: '',
        st_nomeperfil: '',
        st_nomeentidade: ''
    },
    idAttribute: 'id_usuario',
    url: function() {
        return 'api/vw-professor/' + this.id ? this.id : '';
    }
});
var Produto = Backbone.Model.extend({
    defaults: {

        id_produto: null,
        st_produto: '',
        id_tipoproduto: '',
        id_situacao: '',
        id_usuariocadastro: '',
        id_entidade: '',
        id_modelovenda: '',
        bl_ativo: '',
        bl_todasformas: '',
        bl_todascampanhas: '',
        nu_gratuito: 0,
        id_produtoimagempadrao: '',
        bl_unico: '',
        st_descricao: '',
        st_observacoes: '',
        st_informacoesadicionais: '',
        st_estruturacurricular: '',
        st_subtitulo: '',
        st_slug: '',
        bl_destaque: '',
        bl_mostrarpreco: '',
        dt_cadastro: '',
        dt_atualizado: '',
        dt_iniciopontosprom: '',
        dt_fimpontosprom: '',
        nu_pontos: 0,
        nu_pontospromocional: 0,
        nu_estoque: 0,
        bl_todasentidades: null,
        st_cargahoraria: ''
    },
    idAttribute: 'id_produto',
    url: function () {
        return this.id ? '/api/produto/' + this.id : '/api/produto';
    },
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    toString: function () {
        return this.get("st_produto");
    }
});
var VwClienteVenda = Backbone.Model.extend({
    defaults: {
        id_venda: null,
        bl_contrato: '',
        dt_cadastro: '',
        id_entidade: '',
        id_evolucao: '',
        id_evolucaocontrato: '',
        id_reciboconsolidado: '',
        id_situacao: '',
        id_situacaocontrato: '',
        id_textosistemarecibo: '',
        id_usuario: '',
        nu_valorbruto: '',
        nu_valorliquido: '',
        st_cpf: '',
        st_evolucao: '',
        st_nomecompleto: '',
        st_nomeentidade: '',
        st_situacao: ''
    },
    idAttribute: 'id_venda',
    url: function () {
        return this.id ? '/api/vw-cliente-venda/' + this.id : '/api/vw-cliente-venda';
    },
    toString: function () {
        return this.get("id_venda") + ' - ' + this.get("st_nomecompleto");
    },
    parse: function (data) {
        return data;
    }
});
var VwUsuarioPerfilEntidadeReferenciaModel = Backbone.Model.extend({
    defaults: {
        id_perfilreferencia: ''
                , id_usuario: ''
                , id_entidade: ''
                , id_perfil: ''
                , id_areaconhecimento: ''
                , id_projetopedagogico: ''
                , st_projetopedagogico: ''
                , id_saladeaula: ''
                , id_disciplina: ''
                , bl_ativo: ''
                , bl_titular: ''
                , st_nomecompleto: ''
                , st_saladeaula: ''
                , id_perfilreferenciaintegracao: ''
                , is_editing: false

    },
    url: function() {
        return this.id ? '/api/vw-usuario-perfil-entidade-referencia/' + this.id : '/api/vw-usuario-perfil-entidade-referencia';
    },
    idAttribute: 'id_perfilreferencia',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("st_nomecompleto");
    }
});

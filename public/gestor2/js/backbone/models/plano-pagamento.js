var PlanoPagamentoModel = Backbone.Model.extend({
    defaults: {
        id_planopagamento: null,
        id_produto: '',
        nu_parcelas: '0',
        nu_valorentrada: '0.00',
        nu_valorparcela: '0.00',
        bl_padrao: '1'

    },
    idAttribute: 'id_planopagamento',
    url: function() {
        return this.id ? '/api/plano-pagamento/' + this.id : '/api/plano-pagamento';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("nu_valorentrada");
    },
    schema: {
        nu_parcelas: {
            type: 'Number',
            title: 'Parcelas',
            editorAttrs: {class: 'span12 desabilitactrlv', min: 0, maxlength: 5, max: 99999, 'oninput': 'maxLengthCheck(this)'}
        },
        nu_valorentrada: {
            type: 'Text',
            title: 'Entrada',
            editorAttrs: {class: 'span11 desabilitactrlv', min: 0, max: 999999999, maxlength: 10, 'oninput': 'maxLengthCheck(this)'}
        },
        nu_valorparcela: {
            type: 'Text',
            title: 'Parcela',
            editorAttrs: {class: 'span11 desabilitactrlv', min: 0, max: 999999999, maxlength: 10, 'oninput': 'maxLengthCheck(this)'}
        }
    }
});
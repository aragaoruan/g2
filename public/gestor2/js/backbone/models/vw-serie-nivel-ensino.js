var VwSerieNivelEnsinoModel = Backbone.Model.extend({ 
	defaults: { 
		 id_serie: 			''
		,st_serie: 			''
		,id_serieanterior: 	''
		,id_nivelensino: ''
		,st_nivelensino: ''
	},
	url: function() { 
		return this.id ? '/api/vw-serie-nivel-ensino/' + this.id : '/api/vw-serie-nivel-ensino'; 
	},
	idAttribute: 'id_serie',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_serie");
    }
});
var ProdutoLivroModel = Backbone.Model.extend({ 
	defaults: { 
		 id_produto: ''
		,id_entidade: ''
		,id_livro: '' 
	}
	,url: function() { 
		return this.id ? '/api/produto-livro/' + this.id : '/api/produto-livro'; 
	}
    ,idAttribute: 'id_produto'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_produto");
    },
	schema: {

		id_livro: {
			type: 'Hidden',
			validators: [{type: 'required', message: 'Você precisa informar o Livro!'}]
		},
		id_produto: {
			type: 'Hidden'
		}
	}
});


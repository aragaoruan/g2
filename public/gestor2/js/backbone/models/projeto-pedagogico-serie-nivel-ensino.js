var ProjetoPedagogicoSerieNivelEnsinoModel = Backbone.Model.extend({
    defaults: {
		id_projetopedagogico:''
		,id_serie:''
		,id_nivelensino:''
    },
    url: function() {
        return this.id ? '/api/projeto-pedagogico-serie-nivel-ensino/' + this.id : '/api/projeto-pedagogico-serie-nivel-ensino';
    },
	idAttribute: 'id_projetopedagogico',  
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_projetopedagogico");
    }
});
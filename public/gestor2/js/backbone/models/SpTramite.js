/**
 * Model para Sp sp_tramite
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @@since 2014-11-04
 * @type Model 
 * @exp Backbone
 * @pro Model
 * @call extend
 */
var SpTramite = Backbone.Model.extend({
    defaults: {
        id_tramite: '',
        id_upload: '',
        st_upload: '',
        st_tramite: '',
        id_tipotramite: '',
        st_tipotramite: '',
        id_categoriatramite: '',
        st_categoriatramite: '',
        id_usuario: '',
        st_nomecompleto: '',
        id_entidade: '',
        dt_cadastro: '',
        bl_visivel: '',
        id_campo: ''
    },
    idAttribute: 'id_tramite',
    url: function () {
        return (this.id ? '/api/sp-tramite/' + this.id : '/api/sp-tramite');
    }
});
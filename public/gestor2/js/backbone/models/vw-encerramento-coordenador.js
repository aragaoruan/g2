var VwEncerramentoCoordenador = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        id_saladeaula: '',
        id_encerramentosala: '',
        id_disciplina: '',
        id_usuariocoordenador: '',
        id_usuariopedagogico: '',
        id_usuarioprofessor: '',
        st_professor: '',
        id_usuariofinanceiro: '',
        dt_encerramentoprofessor: '',
        dt_encerramentocoordenador: '',
        dt_encerramentopedagogico: '',
        dt_encerramentofinanceiro: '',
        st_disciplina: '',
        st_saladeaula: '',
        id_entidade: '',
        id_coordenador: '',
        id_areaconhecimento: '',
        st_areaconhecimento: '',
        st_aluno: '',
        st_tituloavaliacao: '',
        id_tipodisciplina: '',
        st_upload: '',
        id_aluno: ''
    },
    id_attribute: 'id_encerramentosala'
});
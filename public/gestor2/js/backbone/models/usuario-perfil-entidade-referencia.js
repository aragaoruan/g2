var UsuarioPerfilEntidadeReferenciaModel = Backbone.Model.extend({
    defaults: {
        id_perfilreferencia: ''
        , id_usuario: ''
        , id_entidade: ''
        , id_perfil: ''
        , id_areaconhecimento: ''
        , id_projetopedagogico: ''
        , id_saladeaula: ''
        , id_disciplina: ''
        , bl_ativo: ''
        , bl_titular: ''
        , nu_porcentagem: ''
        , is_editing: false

    },
    url: function() {
        return this.id ? '/api/usuario-perfil-entidade-referencia/' + this.id : '/api/usuario-perfil-entidade-referencia';
    },
    idAttribute: 'id_perfilreferencia',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("id_perfilreferencia");
    }
});

var VwProdutoModel = Backbone.Model.extend({ 
	defaults: { 
		id_produto: ''
		,st_produto: ''
		,bl_ativo: ''
		,bl_todasformas: ''
		,bl_todascampanhas: ''
		,nu_gratuito: ''
		,id_produtoimagempadrao: ''
		,bl_unico: ''
		,st_tipoproduto: ''
		,id_produtovalor: ''
		,nu_valor: ''
		,nu_valormensal: ''
		,id_tipoprodutovalor: ''
		,st_tipoprodutovalor: ''
		,st_situacao: ''
		,id_planopagamento: ''
		,nu_parcelas: ''
		,nu_valorentrada: ''
		,nu_valorparcela: '' 
	}
	,url: function() { return this.id ? '/api/vw-produto/' + this.id : '/api/vw-produto'; }
    ,idAttribute: 'id_produto'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_produto");
    }
});

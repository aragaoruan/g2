var VwPesquisarLivroModel = Backbone.Model.extend({ 
	defaults: { 
		id_livro: ''
		,id_situacao: ''
		,id_entidadecadastro: ''
		,id_usuariocadastro: ''
		,id_tipolivro: ''
		,id_livrocolecao: ''
		,st_livro: ''
		,st_isbn: ''
		,st_codigocontrole: ''
		,bl_ativo: ''
		,st_livrocolecao: ''
		,st_tipolivro: ''
		,st_situacao: ''
	}
	,url: function() { 
		return this.id ? '/api/vw-pesquisar-livro/' + this.id : '/api/vw-pesquisar-livro'; 
	}
    ,idAttribute: 'id_livro'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_livro");
    }
});

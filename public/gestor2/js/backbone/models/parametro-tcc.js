
var ParamentroTCC = Backbone.Model.extend({
    defaults: {
        id_parametrotcc: null,
        id_projetopedagogico: '',
        st_parametrotcc: '',
        nu_peso: '',
        bl_ativo: '',
        is_editing: false
    },
    url: function() {
        return this.id ? '/api/parametro-tcc/' + this.id : '/api/parametro-tcc';
    },
        idAttribute: 'id_parametrotcc',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

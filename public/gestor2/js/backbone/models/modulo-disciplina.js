var ModuloDisciplinaModel = Backbone.Model.extend({
	defaults: {
		 id_modulodisciplina: null
		,id_modulo: ''
		,id_disciplina: ''
		,id_serie: ''
		,id_nivelensino: ''
		,id_areaconhecimento: ''
		,bl_ativo: ''
		,nu_ordem: ''
		,bl_obrigatoria: ''
		,nu_ponderacaocalculada: ''
		,nu_ponderacaoaplicada: ''
		,id_usuarioponderacao: ''
		,dt_cadastroponderacao: ''	
		,nu_importancia: ''
        ,nu_cargahoraria: ''
    },
	url: function() {
		return this.id ? '/api/modulo-disciplina/' + this.id : '/api/modulo-disciplina';
	},
	idAttribute: 'id_modulodisciplina',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

var VwResumoFinanceiro = Backbone.Model.extend({
    defaults: {
    },
    idAttribute: 'id_lancamento',
    url: function () {
        return this.id ? '/recebimento/retornar-resumo-financeiro' + this.id : '/recebimento/retornar-resumo-financeiro';
    },
    toString: function () {
        return this.get("id_lancamento");
    },
    parse: function (data) {
        return data;
    }
});
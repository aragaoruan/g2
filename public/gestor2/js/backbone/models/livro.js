var LivroModel = Backbone.Model.extend({ 
	defaults: { 
		 id_livro: ''
		,id_entidadecadastro: ''
		,id_usuariocadastro: ''
		,id_situacao: ''
		,id_tipolivro: ''
		,id_livrocolecao: ''
		,st_livro: ''
		,st_isbn: ''
		,st_codigocontrole: ''
		,bl_ativo: 1
	}
	,url: function() { 
		return this.id ? '/api/livro/' + this.id : '/api/livro'; 
	} 
    ,idAttribute: 'id_livro'
	,toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_livro");
    }
});

var ProdutoProjetoPedagogicoModel = Backbone.Model.extend({ 
	defaults: { 
		id_produtoprojetopedagogico: null,
        id_projetopedagogico: '',
		id_produto: '',
		id_entidade: '',
		nu_tempoacesso: '',
        id_turma: '',
        bl_indeterminado: '',
        nu_cargahoraria: '',
        st_disciplina: ''
	},
	idAttribute: 'id_produtoprojetopedagogico',
	url: function() {
		return this.id ? '/api/produto-projeto-pedagogico/' + this.id : '/api/produto-projeto-pedagogico'; 
	},
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_projetopedagogico");
    }     
});

/*
 *  Coleção das Situações na requisição 
 *  padrão /api/situacao. 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var ProdutoProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: ProdutoProjetoPedagogicoModel,
    initialize: function(models, options) {
        if(typeof options != "undefined"){
            if(typeof options.id_produto != "undefined"){
                this.data.id_produto = options.id_produto;
            }
        }
    },
    url: function(){ 
        return "/api/produto-projeto-pedagogico/?"   + $.param(_.defaults(this.data));
    },
    parse: function(data) {
        if(typeof data.mensagem == "undefined"){
            return data;
        } else {
            return data.mensagem;
        }
    },
    data: {
        id_produto: null
    }
});

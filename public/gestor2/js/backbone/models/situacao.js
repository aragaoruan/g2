/*
 * 
 * Modelo para guardar os Tipos
 */
var Situacao = Backbone.Model.extend({
    defaults: {
        id_situacao: null,
        st_situacao: '',
        st_tabela: '',
        st_campo: '',
        st_descricaosituacao: ''
    },
    idAttribute: 'id_situacao',
    url: function() {
        return this.id ? '/api/situacao/' + this.id : '/api/situacao';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_situacao");
    },        
    schema: {
        st_tabela: { type: 'Text', title: 'Tabela de Relação' },
        st_campo: { type: 'Text', title: 'Campo de Relação' },
        st_descricaosituacao: { type: 'Text', title: 'Descrição' },
        st_situacao: { type: 'Text', title: 'Tipo' },
        id_situacao: { type: 'Hidden' }
    }
});


/*
 *  Coleção das Situações na requisição 
 *  padrão /api/situacao. 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var Situacoes = Backbone.Collection.extend({
    model: Situacao,
    url: function(){ 
        return "/api/situacao/?"   + $.param(_.defaults(this.data));
    },
    parse: function(data) {
        return data.mensagem;
    },
    data: {
        st_tabela: null
    }
});

var ProdutoEntidade = Backbone.Model.extend({
    defaults: {
        id_produtoentidade: null,
        id_entidade: null,
        id_produto: null
    },
    idAttribute: 'id_produtoentidade',
    url: function() {
        return this.id ? '/api/produto-entidade/' + this.id : '/api/produto-entidade';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    }
});

var ProdutoEntidades = Backbone.Collection.extend({
    model: ProdutoEntidade,
    url: function(){ 
    	return "/api/produto-entidade/?"   + $.param(_.defaults(this.data));
    },
	data: {
	    id_produto: null, 
	    id_entidade: null, 
	}

});
var VwProjetoAreaModel = Backbone.Model.extend({
	defaults: {

		 id_areaconhecimento: ''
		,st_areaconhecimento: ''
		,id_projetopedagogico: ''
		,st_projetopedagogico: ''
		,st_tituloexibicao: ''
		,tipo: ''
		,id_entidade: ''
		
    },
	url: function() {
		return this.id ? '/api/vw-projeto-area/' + this.id : '/api/vw-projeto-area';
	},
	idAttribute: 'id_areaconhecimento',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_areaconhecimento");
    } 
});

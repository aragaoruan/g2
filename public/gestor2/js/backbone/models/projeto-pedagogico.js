var ProjetoPedagogicoModel = Backbone.Model.extend({
    defaults: {
        id_projetopedagogico: null
        , st_descricao: ''
        , nu_idademinimainiciar: ''
        , bl_ativo: ''
        , nu_tempominimoconcluir: ''
        , nu_idademinimaconcluir: ''
        , nu_tempomaximoconcluir: ''
        , nu_tempopreviconcluir: ''
        , st_projetopedagogico: ''
        , st_tituloexibicao: ''
        , bl_autoalocaraluno: ''
        , id_situacao: ''
        , id_trilha: ''
        , id_projetopedagogicoorigem: ''
        , st_nomeversao: ''
        , id_tipoextracurricular: ''
        , nu_notamaxima: ''
        , nu_percentualaprovacao: ''
        , nu_valorprovafinal: ''
        , id_entidadecadastro: ''
        , bl_provafinal: ''
        , bl_salasemprovafinal: ''
        , st_objetivo: ''
        , st_estruturacurricular: ''
        , st_metodologiaavaliacao: ''
        , st_certificacao: ''
        , st_valorapresentacao: ''
        , nu_cargahoraria: ''
        , dt_cadastro: ''
        , id_usuariocadastro: ''
        , bl_disciplinaextracurricular: ''
        , id_usuario: ''
        , st_conteudoprogramatico: ''
        , id_medidatempoconclusao: ''
        , id_contratoregra: ''
        , id_livroregistroentidade: ''
        , nu_prazoagendamento: ''
        , st_perfilprojeto: ''
        , st_coordenacao: ''
        , st_horario: ''
        , st_publicoalvo: ''
        , st_mercadotrabalho: ''
        , bl_turma: ''
        , projetoEntidade: ''
        , projetoSala: ''
        , bl_gradepronta: ''
        , dt_criagrade: ''
        , id_usuariograde: ''
        , st_apelido: ''
        , bl_disciplinacomplementar: ''
        , bl_disciplinacomplementar: ''
        , id_anexoementa: {
            id_upload: '',
            st_upload: ''
        },
        bl_enviaremail: 1
    },
    url: function () {
        return this.id ? '/api/projeto-pedagogico/' + this.id : '/api/projeto-pedagogico';
    },
    idAttribute: 'id_projetopedagogico',
    toggleEdit: function () {
        this.is_editing = !this.is_editing;
    },
    toString: function () {
        return this.get("st_projetopedagogico");
    }
});


/*
 *  Coleção das Situações na requisição 
 *  padrão /api/situacao. 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var ProjetoPedagogicoCollection = Backbone.Collection.extend({
    model: ProjetoPedagogicoModel,
    url: function () {
        return "/api/projeto-pedagogico/?" + $.param(_.defaults(this.data));
    }
});
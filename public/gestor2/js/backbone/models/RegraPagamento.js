var RegraPagamento = Backbone.Model.extend({
    defaults: {
        id_regrapagamento: null,
        nu_valor: 0,
        dt_cadastro: null,
        id_entidade: null,
        id_areaconhecimento: null,
        id_projetopedagogico: null,
        id_professor: null,
        id_cargahoraria: null,
        id_tipodisciplina: null,
        bl_ativo: 0
    },
    idAttribute: 'id_regrapagamento',
    url: function() {
        return 'api/regra-pagamento/' + (this.id ? this.id : '');
    }
});
var VwAvaliacaoConjuntoReferenciaModel = Backbone.Model.extend({
	defaults: {
        id_projetopedagogico: '',
        st_avaliacaoconjunto: '',
        st_tipoprova: '',
        id_avaliacaoconjuntoreferencia: '',
        id_avaliacaoconjunto: '',
        id_saladeaula: '',
        id_modulo: '',
        dt_inicio: '',
        dt_fim: ''
    },
	url: function() {
		return this.id ? '/api/vw-avaliacao-conjunto-referencia/' + this.id : '/api/vw-avaliacao-conjunto-referencia';
	},
	idAttribute: 'id_projetopedagogico',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("st_avaliacaoconjunto");
    } 
});
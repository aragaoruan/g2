var VwMatricula = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        id_usuario: '',
        st_nomecompleto: '',
        st_cpf: '',
        dt_nascimento: '',
        st_nomepai: '',
        st_nomemae: '',
        st_projetopedagogico: '',
        id_situacao: '',
        st_situacao: '',
        id_evolucao: '',
        st_evolucao: '',
        dt_concluinte: '',
        id_entidadematriz: '',
        st_entidadematriz: '',
        id_entidadematricula: '',
        st_entidadematricula: '',
        id_entidadeatendimento: '',
        st_entidadeatendimento: '',
        bl_ativo: '',
        id_contrato: '',
        id_projetopedagogico: '',
        dt_termino: '',
        dt_cadastro: '',
        dt_inicio: '',
        id_turma: '',
        st_turma: '',
        dt_inicioturma: '',
        st_email: '',
        st_telefone: '',
        bl_institucional: '',
        dt_terminoturma: '',
        st_urlacesso: '',
        bl_disciplinacomplementar: '',
        id_matriculavinculada: '',
        dt_aceitecontrato: '',
        id_esquemaconfiguracao: '',
        nu_cargahorariaintegralizada: ''
    },
    idAttribute: 'id_matricula',
    url: function() {
        return this.id ? '/api/vw-matricula/' + this.id : '/api/vw-matricula';
    },
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function() {
        return this.get("id_matricula") + ' - ' + this.get("st_turma");
    }
});
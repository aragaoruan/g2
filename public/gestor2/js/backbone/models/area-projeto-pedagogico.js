var AreaProjetoPedagogicoModel = Backbone.Model.extend({
	defaults: {
		 id_projetopedagogico: ''
		,id_areaconhecimento: ''
    },
	url: function() {
		return this.id ? '/api/area-projeto-pedagogico/' + this.id : '/api/area-projeto-pedagogico';
	},
	idAttribute: 'id_projetopedagogico',
    toggleEdit: function() {
        this.is_editing = !this.is_editing;
    },
    toString: function(){
        return this.get("id_projetopedagogico");
    } 
});

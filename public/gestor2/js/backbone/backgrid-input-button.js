/*
 backgrid-select-all
 http://github.com/wyuenho/backgrid
 
 Copyright (c) 2013 Jimmy Yuen Ho Wong and contributors
 Licensed under the MIT @license.
 */
(function(window, $, _, Backbone, Backgrid) {

    /**
     Renders a checkbox for row selection.
     
     @class Backgrid.Extension.SelectRowCell
     @extends Backbone.View
     */
    var ButtonRowCell = Backgrid.Extension.ButtonRowCell = Backbone.View.extend({
        className: "button-row-cell",
        tagName: "td",
        classInput: 'btn',
        valueInput: '',
        initialize: function(options) {
            Backgrid.requireOptions(options, ["model", "column"]);
            this.column = options.column;
        },
        render: function() {
            var nameInput = this.column.attributes.name + '[]';
            this.$el.empty().append('<input class="' + this.classInput + '" value="' + this.valueInput + '" name="' + nameInput + '" type="button" />');
            this.delegateEvents();
            return this;
        }

    });

}(window, jQuery, _, Backbone, Backgrid));

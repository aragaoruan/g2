/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 28/11/2013
 * Formulário para pesquisa de disciplinas
 */

/**
 * Dependências
 */
if(typeof VwDisciplinaSerieNivelModel == "undefined") {
   $.getScript('/js/backbone/models/vw-disciplina-serie-nivel.js');
}

var VwDisciplinaSerieNivelCollection = Backbone.Collection.extend({
	model: VwDisciplinaSerieNivelModel,
	url: function(){
		if(typeof this.data != "undefined") {
			return "/api/vw-disciplina-serie-nivel/?"   + $.param(_.defaults(this.data));
		} else {
			return "/api/vw-disciplina-serie-nivel/";
		}
	}
});

var FormVwDisciplinaSerieNivelModel = VwDisciplinaSerieNivelModel.extend({
	
	schema: {
        st_disciplina: { 
            type: 'Text', 
            title: 'Título', 
            validators: [
                    { 
                      type: 'required', 
                      message: 'Você precisa informar o Título da Disciplina!' 
                    } 
                ],
            editorAttrs: { class: 'span4', placeholder: 'Título' }
        }/*,
        id_tipodisciplina:  { 
            type: 'Select', 
            title: 'Tipo', 
            editorAttrs: { class: 'span2' },
            options: new TiposDisciplina()
        }*/
		
    }
	
});

/**
 * Formulário da Pesquisa de Disciplinas
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormDisciplinaBusca = Backbone.View.extend({
  events: {
	  "click button#btn-pesquisar-disciplina": "pesquisar",
          "keypress input": "handleKeypress"
  },
  handleKeypress: function (event) {
    if(event.keyCode == 13)
      {
         this.pesquisar(); //Remove o ENTER do campo de busca de disciplina e faz pesquisa
        return false;
      }

  },
   initialize: function() {
	this.model          = new FormVwDisciplinaSerieNivelModel();
  },
  render: function() {
	  
        var that = this;
          
        $.get('/js/backbone/templates/form-disciplina-busca.html', { "_": $.now() }, function(template){
            
            that.form = new Backbone.Form({
                    template: _.template(template),
                    model: that.model
            });
           
            that.$el.append(that.form.render().el);
        
        });   
	
	
	return this;
  },
  pesquisar: function(){
		
		var that = this;
		
		$.each(this.form.fields, function(index, value) {
//			value.editor.$el.css("border-color" , "#CCCCCC");
			value.editor.$el.removeAttr('style');
		});
		var validate  = this.form.validate();
		if(validate){
			$.each(validate, function(index, value) {
				that.$el.find("[name='"+index+"']").css("border-color" , "red");
				$.pnotify({title:'Aviso' ,type: 'warning', text: value.message});
			});
			return false;
		}
		
		
		
		var table = that.$el.find('table#form-disciplina-busca-grid');
		var tbody = that.$el.find('table#form-disciplina-busca-grid tbody');
		tbody.empty();
		table.hide();
				
		try {		
			$.ajaxSetup({async: false});
			loading();
			var VwCollection = new VwDisciplinaSerieNivelCollection();
				VwCollection.data = that.form.getValue();	
				VwCollection.fetch({
					success: function(collection, response, options) {
						table.show(); 
						VwCollection.each(function(vw) {
							tbody.append('<tr><td><i data-id_disciplina="'+vw.get('id_disciplina')+'" data-st_disciplina="'+vw.get('st_disciplina')+'" class="icon-plus" id="disciplina-busca-'+vw.get('id_disciplina')+'"></i></td>' 
							+'<td>'+vw.get('st_disciplina')+'</td>'
							+'<td>'+vw.get('st_nivelensino')+'</td>'
							+'<td>'+vw.get('st_serie')+'</td></tr>');
							
							$('#disciplina-busca-'+vw.get('id_disciplina')).click({ disciplina: vw }, function(event){
								that.trigger("FormDisciplinaBuscaEvent", event.data );  
							}).css({'cursor':'pointer'});
						});

					},
					error : function(collection, response, options) {
                                                //alert(response.text);
						var errorData = eval("(" + response.responseText + ")");
						$.pnotify(errorData.text);
						table.hide();
						}
				});	
			loaded();	
			$.ajaxSetup({async: true});		
		} catch (error) { console.log(error) }
		
	return false;		
 }
});
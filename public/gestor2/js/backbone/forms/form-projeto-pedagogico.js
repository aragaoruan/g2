/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 10/12/2013
 * Formulário para cadastro do Projeto Pedagógico
 * Aqui criamos um conjunto de Abas onde chamamos cada formulário, que poderão ser salvos um por um ou todos ao mesmo tempo.
 * teste
 */

/**
 * Dependências
 */

//ProjetoPedagogicoModel

//var TesteCollection = Backbone.Collection.extend({
//    model: ProjetoPedagogicoModel
//});
//
$.ajaxSetup({async: false});
if(typeof ProjetoPedagogicoModel == "undefined") {
   $.getScript('/js/backbone/models/projeto-pedagogico.js');
};
if(typeof FormProjetoPedagogicoBasico == "undefined") {
   $.getScript('/js/backbone/forms/form-projeto-pedagogico-basico.js');
};
if(typeof FormProjetoPedagogicoTabDisciplina == "undefined") {
    $.getScript('/js/backbone/forms/form-projeto-pedagogico-tab-disciplina.js');
};

if(typeof VwAvaliacaoConjuntoReferenciaModel == "undefined") {
    $.getScript('/js/backbone/models/VwAvaliacaoConjuntoReferencia.js');
};

if(typeof ParametroTccView == "undefined") {
    $.getScript('/js/backbone/models/parametro-tcc.js');
};

var FormProjetoPedagogico = Backbone.View.extend({
	events: {
	   "click button#btn-salvar-projeto-pedagogico": "salvar"
	},
	id_projetopedagogico: null,
	initialize: function() {
		this.forms[0]  = new FormProjetoPedagogicoBasico();
		this.forms[1]  = new FormProjetoPedagogicoTabDisciplina();
	},
	
	events: {
//		'click li.tab' : 'switchTab',
        "click #form-projeto-tab-basico": "renderBasico",
        "click #form-projeto-tab-disciplina": "renderDisciplina",
        "click #form-projeto-tab-texto": "renderTexto",
        "click #form-projeto-tab-avaliacao": "renderAvaliacao",
        "click #btnAddTCC": "novoParamentroTcc"
	},
	
	forms: new Array(),
	
	tagName: 'div',

	render: function() {
		this.renderTabs();
		return this;
	},
	renderTabs: function() {
	  
		var that = this;
		$.ajaxSetup({async: false});
		$.get('/js/backbone/templates/form-projeto-pedagogico.html', { "_": $.now() }, function(template){
			
			that.$el.html(template);
			if(that.id_projetopedagogico){
				that.forms[0].model.set({id_projetopedagogico : that.id_projetopedagogico});
			}
			
			that.forms[0].on( "FormProjetoPedagogicoBasicoSaveEvent", function(Obj) {
                that.id_projetopedagogico =  Obj.get('id_projetopedagogico');
			});
			
			// Cadastro Básico do Projeto Pedagógico
			that.$el.find('#container-form-projeto-tab-basico').html(that.forms[0].render().el);
			
		});

	},
    renderBasico: function() {
        $('#container-form-projeto-tab-basico').show();
    },

    renderDisciplina: function(event){
        $('#container-form-projeto-tab-disciplina').html('');
        loading();
        var that = this;
        if(that.id_projetopedagogico){
            that.forms[1].model.set({id_projetopedagogico : that.id_projetopedagogico});
            // Cadastro das Disciplinas ao Projeto Pedagógico
            that.$el.find('#container-form-projeto-tab-disciplina').html(that.forms[1].render().el);
        }else{
            $('#container-form-projeto-tab-disciplina').html('Salve o projeto pedagógico antes de prosseguir');
        }
        loaded();
        $('#container-form-projeto-tab-disciplina').show();
    },

    renderTexto: function(){
        $('#container-form-projeto-tab-texto').html('');
        loading();
        var that = this;
        if(that.id_projetopedagogico){
            var textoModel = new ProjetoPedagogicoModel({ id_projetopedagogico: that.id_projetopedagogico });
            textoModel.fetch({
                success: function() {
                    var	cardView = new FormProjetoPedagogicoTextoView({
                            model: textoModel,
                            childViewTagName: 'div',
                            el: $('#container-form-projeto-tab-texto')
                        }
                    );
                    cardView.render();
                }
            });
        }else{
            $('#container-form-projeto-tab-texto').html('Salve o projeto pedagógico na primeira aba antes de prosseguir');
        }
        $('#container-form-projeto-tab-texto').show();
        loaded();

    },
    renderAvaliacao: function(){
        $('#container-form-projeto-tab-avaliacao').html('');
        loading();
        var that = this;
        if(that.id_projetopedagogico){
            var avaliacaoModel =  new ProjetoPedagogicoModel({ id_projetopedagogico: that.id_projetopedagogico });
                avaliacaoModel.fetch({
                success: function(model, response, options) {
                    var	avaliacaoView = new FormDadosdeAvaliacaoView({
                            model: avaliacaoModel,
                            childViewTagName: 'div',
                            el: $('#container-form-projeto-tab-avaliacao')
                        }
                    );
                    avaliacaoView.render();
                }
            });
        }else{
            $('#container-form-projeto-tab-avaliacao').html('Salve o projeto pedagógico na primeira aba antes de prosseguir');
        }
        $('#container-form-projeto-tab-avaliacao').show();
        loaded();
    },
    novoParamentroTcc: function(){
        var that = this;
        var nLa = new ParamentroTCC({id_projetopedagogico: that.id_projetopedagogico});
        nLa.toggleEdit();
        listaParametrosTccView.add(nLa);
    },
    switchTab: function(event) {
	  
		var tabAtiva = this.$('li.active');
			tabAtiva.removeClass('active');
			this.$('#container-' + tabAtiva.attr('id')).hide();
		  
		var selectedTab = event.currentTarget;
		this.$(selectedTab).addClass('active');
		this.$('#container-' + $(selectedTab).attr('id')).show();
		this.$('#container-' + $(selectedTab).attr('id')).css({display: 'table'});
	
	},
	salvar: function(){
		
	}
	
});




//---------- VIEW DE TEXTOS ------//
var FormProjetoPedagogicoTextoView = Backbone.View.extend({
    id_projetopedagogico: null,
    initialize: function() {
    },
    render: function() {
        //if(tinymce.get('st_descricao')){
        //tinymce.remove(tinymce.get('st_descricao'));
        //}
        //if(tinymce.get('st_horario')) {
        //    tinymce.remove(tinymce.get('st_horario'));
        //}
        this.renderTabTexto();
        return this;
    },
    loadTinyMCE: function() {

        /* Try-Catch recursivo.
        * Assegura que o TinyMCE seja
        * carregado adequadamente.
        */

        try {
            while (tinymce.editors.length > 0) {
                tinymce.remove(tinymce.editors[0]);
            }
        } catch (e) {
            debug("Erro ao carregar TinyMCE. Tentando novamente...");
            this.loadTinyMCE();
        }

        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

            menubar: false,
            toolbar_items_size: 'small',
            theme: 'modern',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
        });
    },
    renderTabTexto: function() {
        var that = this;
        var content = '';
        if(that.model.get('id_projetopedagogico')){
            $.get('/js/backbone/templates/projeto/form-texto.html', { "_": $.now() }, function(template){
                content = _.template(template);
                $('#container-form-projeto-tab-texto').html(content(that.model.toJSON()));
                that.loadTinyMCE();
            });
        }
    },
    events: {
        "click #btn-salvar-projeto-textos" : "salvarTextos"

    },
    salvarTextos: function(){
        var that = this;

        var st_descricao = tinymce.get('st_descricao').getContent();
        $('#st_descricao').val(st_descricao);

        var st_objetivo = tinymce.get('st_objetivo').getContent();
        $('#st_objetivo').val(st_objetivo);

        var st_estruturacurricular = tinymce.get('st_estruturacurricular').getContent();
        $('#st_estruturacurricular').val(st_estruturacurricular);

        var st_conteudoprogramatico = tinymce.get('st_conteudoprogramatico').getContent();
        $('#st_conteudoprogramatico').val(st_conteudoprogramatico);

        var st_metodologiaavaliacao = tinymce.get('st_metodologiaavaliacao').getContent();
        $('#st_metodologiaavaliacao').val(st_metodologiaavaliacao);

        var st_certificacao = tinymce.get('st_certificacao').getContent();
        $('#st_certificacao').val(st_certificacao);

        //Modulação
        var st_horario = tinymce.get('st_horario').getContent();
        $('#st_horario').val(st_horario);

        var st_publicoalvo = tinymce.get('st_publicoalvo').getContent();
        $('#st_publicoalvo').val(st_publicoalvo);

        var st_mercadotrabalho = tinymce.get('st_mercadotrabalho').getContent();
        $('#st_mercadotrabalho').val(st_mercadotrabalho);

        var campos = formToObject($('#form-textos-projeto-pedagogico'));
        that.model.set(campos);
        that.model.save(null, {
            success: function(response) {
                loaded();
                response.title 	= 'Projeto Pedagógico Textos - Sucesso';
                response.text 	= 'Textos salvos com sucesso!';
                response.type 	= 'success';
                $.pnotify(response);//mensagem de retorno
            },
            error: function(response) {
                loaded();
                $.pnotify({title: "Projeto Pedagógico - Erro", text: response.mensagem, type: 'error'});
            },
            complete: function() {
                loaded();
            }
        });
    }

});
//---------- VIEW DE TEXTOS ------//



//---------- VIEW DE DADOS de Avaliacao ------//
var FormDadosdeAvaliacaoView = Backbone.View.extend({
    initialize: function() {
    },
    render: function() {
        this.renderDadosAvaliacao();
        this.renderParamentrosTCC();
        return this;
    },
    renderParamentrosTCC: function(){
        var that = this;
        if(that.model.get('id_projetopedagogico')){
            var tccsModel = new ParamentroTCC({ id_projetopedagogico: that.model.get('id_projetopedagogico') });
             ParametrosTccColletion = Backbone.Collection.extend({
                model: ParamentroTCC,
                url: "/api/parametro-tcc/?id_projetopedagogico="+that.model.get('id_projetopedagogico')
            });

            parametrostcc = new ParametrosTccColletion;
            parametrostcc.fetch({
                success: function() {
                    listaParametrosTccView = new parametrosTccView({
                        collection: parametrostcc,
                        childViewConstructor: ParametroTccView,
                        childViewTagName: 'tr',
                        el: $('#container-form-projeto-tab-avaliacao-tcc-table')
                    });

                    listaParametrosTccView.render();
                }
            });
        }
        $('#container-form-projeto-tab-avaliacao-tcc').show();
    },
    renderDadosAvaliacao: function() {
        var that = this;
        var content = '';
        if(that.model.get('id_projetopedagogico')){
            $.get('/js/backbone/templates/projeto/form-dados-avaliacao.html', { "_": $.now() }, function(template){
                content = _.template(template);
                $('#container-form-projeto-tab-avaliacao').html(content(that.model.toJSON()));
            });
        }
    },
    events: {
        "click #btn-salvar-projeto-avaliacao" : "salvarAvaliacao"

    },
    salvarAvaliacao: function(){
        var that = this;
        var campos = formToObject($('#form-dados-avaliacao-projeto-pedagogico'));
        that.model.set(campos);
        that.model.save(null, {
            success: function(response) {
                loaded();
                response.title 	= 'Projeto Pedagógico Avaliação - Sucesso';
                response.text 	= 'Textos salvos com sucesso!';
                response.type 	= 'success';
                $.pnotify(response);//mensagem de retorno
            },
            error: function(response) {
                loaded();
                $.pnotify({title: "Projeto Pedagógico - Erro", text: response.mensagem, type: 'error'});
            },
            complete: function() {
                loaded();
            }
        });
    }

});
//---------- VIEW DE TEXTOS ------//


//----------- PArametros de TCC -----------------//
ParametroTccView = Backbone.View.extend({
    initialize: function(options) {
        this.render = _.bind(this.render, this);
        this.model.bind('change', this.render);
    },
    tagName: "tr",
    className: "parametrotcc",
    bindings: {
        'input[name=st_parametrotcc]': 'st_parametrotcc',
        'input[name=nu_peso]': 'nu_peso'
    },
    render: function() {
        var variables = {st_parametrotcc: this.model.get('st_parametrotcc'), id_projetopedagogico: this.model.get('id_projetopedagogico')
        ,id_parametrotcc: this.model.get('id_parametrotcc'), nu_peso: this.model.get('nu_peso'), id: this.model.get('id_parametrotcc')};
        var tr;
        if (this.model.is_editing) {
            tr = _.template('<form>'+
                '<td><input name="st_parametrotcc" required="required" value="<%= st_parametrotcc %>"/></td>'+
                '<td><input name="nu_peso" required="required" class="numeric" value="<%= nu_peso %>"/></td>' +
                '<td> <a href="javascript:" class="delete-link btn btn-mini btn-danger">Deletar</a> '+
                ' <a href="javascript:" class="save-link btn btn-mini btn-success">Salvar</a>'+
                ' <a href="javascript:" class="cancel-link btn btn-mini">Cancelar</a></td>'+
                '</form>', variables);
        } else {
            tr = _.template('<form>'+
                '<td><%= st_parametrotcc %></td>'+
                '<td><%= nu_peso %> %</td>' +
                '<td> <a class="edit-link btn btn-mini btn-custom" href="javascript:">Editar</a> </td>'+
                '</form>', variables);
        }
        this.el.innerHTML = tr;
        if(!this.model.get('id_parametrotcc')) {
            $(this.el).find('.delete-link').hide();
        }
        return this;
    },
    delete: function() {
        that = this;
        bootbox.confirm("Tem certeza de que deseja apagar o registro?", function(result) {
            if (result) {
                that.model.destroy({});
                that.remove();
                $.pnotify({title: 'Registro Removido', text: 'O registro foi removido do banco de dados com sucesso!', type: 'success'});
            }
        });
    },
    saveChanges: function() {
        this.model.set({'st_parametrotcc': $(this.el).find('input[name=st_parametrotcc]').val(), 'nu_peso': $(this.el).find('input[name=nu_peso]').val()});
        that = this;
        this.model.save(null,  {
            success : function (model, response) {
                    $.pnotify({title: response.title, text: response.text, type: response.type});
                   if(response.type == 'success'){
                       that.toggleEdit();
                       that.render();
                   }

            },
            error: function (model, response) {
                $.pnotify({title: 'Erro ao atualizar o registro', text: 'Houve um problema ao atualizar o registro, tente novamente', type: 'error'});
            }
        });
    },
    toggleEdit: function() {
        this.oldSettings = this.model.toJSON();
        this.model.toggleEdit();
        this.render();
    },
    resetEdit: function() {
        this.model.set(this.oldSettings);
        this.toggleEdit();
        if(!this.model.id) {
            this.remove();
        }
    },
    events: {
        'click .edit-link': 'toggleEdit',
        'click .save-link': 'saveChanges',
        'click .delete-link': 'delete',
        'click .cancel-link': 'resetEdit'
    }
});






var parametrosTccView = Backbone.View.extend({
    initialize: function(options) {
        _(this).bindAll('add', 'remove');

        if (!options.childViewConstructor)
            throw "no child view constructor provided";
        if (!options.childViewTagName)
            throw "no child view tag name provided";

        this._childViewConstructor = options.childViewConstructor;
        this._childViewTagName = options.childViewTagName;

        this._childViews = [];

        this.collection.each(this.add);

        this.collection.bind('add', this.add);
        this.collection.bind('remove', this.remove);
    },
    add: function(model) {
        var childView = new this._childViewConstructor({
            tagName: this._childViewTagName,
            model: model
        });

        this._childViews.push(childView);

        if (this._rendered) {
            $(this.el).append(childView.render().el);
        }
    },
    remove: function(model) {
        var viewToRemove = _(this._childViews).select(function(cv) {
            return cv.model === model;
        })[0];
        this._childViews = _(this._childViews).without(viewToRemove);
        if (this._rendered)
            $(viewToRemove.el).remove();
    },
    render: function() {
        var that = this;
        this._rendered = true;

        $(this.el).empty();
        _(this._childViews).each(function(childView) {
            $(that.el).append(childView.render().el);
        });

        return this;
    }
});
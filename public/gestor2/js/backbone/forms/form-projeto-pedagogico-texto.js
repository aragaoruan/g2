$.ajaxSetup({async: false});
if(typeof ProjetoPedagogicoModel == "undefined") {
   $.getScript('/js/backbone/models/projeto-pedagogico.js');
};

var FormProjetoPedagogicoTextos = Backbone.View.extend({
    tagName: 'textos',
    className: 'tab-textos',
    el: '#container-form-projeto-tab-texto',
    model: new ProjetoPedagogicoModel(),
    template: _.template($("#template-aba-projeto-texto").html(), this.model.toJSON()),
    events: {
        'click #st_descricao' : 'salvarTextos'
    },

    render: function() {
        var that = this;
//        $.get('/js/backbone/templates/projeto/form-texto.html', {"_": $.now()}, function(template) {
//            console.log(that.templateTexto);
//            that.templateTexto  = _.template(template);
//            if(that.model.get('id_projetopedagogico')){
//                that.model.fetch();
//            }
//        });
        that.el.html(this.template(that.model.toJSON));
        console.log(that.el);
        console.log(that.model);
    },
    salvarTextos: function(){
        alert('um clique!');
        var that = this;
        console.log(that.model);
    }
});

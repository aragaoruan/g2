/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 28/11/2013
 * Formulário para cadastro dos Valores dos Produtos
 */

/**
 * Dependências
 */

if (typeof VwPesquisarLivroModel == "undefined") {
    console.log('A Model VwPesquisarLivroModel é Obrigatória!');
}
;
if (typeof TipoLivroModel == "undefined") {
    console.log('A Model TipoLivroModel é Obrigatória!');
}
;
if (typeof LivroColecaoModel == "undefined") {
    console.log('A Model LivroColecaoModel é Obrigatória!');
}
;


/*
 *  Coleção das Areas de Conhecimento 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var TipoLivroFormCollection = Backbone.Collection.extend({
    model: TipoLivroModel,
    url: function() {
        return "/api/tipo-livro/";
    },
    parse: function(data) {
        var retorno = new Array();
        retorno[0] = new TipoLivroModel({id_tipolivro: '', st_tipolivro: 'Todos'});
        $.each(data.mensagem, function(i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;

    }
});

/*
 *  Coleção dos Níveis de Ensino 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var LivroColecaoFormCollection = Backbone.Collection.extend({
    model: LivroColecaoModel,
    url: function() {
        return "/api/livro-colecao/";
    },
    parse: function(data) {
        var retorno = new Array();
        retorno[0] = new LivroColecaoModel({id_livrocolecao: '', st_livrocolecao: 'Todas'});
        $.each(data, function(i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;

    }
});


var FormVwPesquisarLivroModel = VwPesquisarLivroModel.extend({
    schema: {
        st_livro: {
            type: 'Text',
            title: 'Título',
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar o Título do Lívro!'
                }
            ],
            editorAttrs: {class: 'span4', placeholder: 'Título'}
        },
        id_tipolivro: {
            type: 'Select',
            title: 'Formato',
            editorAttrs: {class: 'span2'},
            options: new TipoLivroFormCollection()
        },
        id_livrocolecao: {
            type: 'Select',
            title: 'Coleção',
            editorAttrs: {class: 'span2'},
            options: new LivroColecaoFormCollection()
        }

    }

});

/**
 * Formulário da Pesquisa de Livros
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormLivroBusca = Backbone.View.extend({
    events: {
        "click button#btn-pesquisar-livro": "pesquisar",
        "keyup input[name='st_livro']": 'callPesquisa',
        "keyup select[name='id_tipolivro']": 'callPesquisa',
        "keyup select[name='id_livrocolecao']": 'callPesquisa'
    },
    callPesquisa: function(e) {
        if (e.keyCode === 13)
            this.pesquisar();
        return false;
    },
    initialize: function() {
        this.model = new FormVwPesquisarLivroModel();
    },
    render: function() {

        var that = this;

        $.get('/js/backbone/templates/form-livro-busca.html', {"_": $.now()}, function(template) {

            that.form = new Backbone.Form({
                template: _.template(template),
                model: that.model
            });

            that.$el.append(that.form.render().el);

        });


        return this;
    },
    pesquisar: function() {

        var that = this;

        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function(index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        loading();

        var table = that.$el.find('table#form-livro-busca-grid');

        var tbody = that.$el.find('table#form-livro-busca-grid tbody');
        tbody.empty();

        $.ajax('/livro/pesquisar', {
            data: that.form.getValue(),
            dataType: 'json'
        }).done(function(response) {
            if (response.tipo === 1) {

                table.show();

                $.each(response.mensagem, function(index, value) {
                    tbody.append('<tr><td><i data-id_livro="' + value.id_livro + '" data-st_livro="' + value.st_livro + '" class="icon-plus"></i></td>'
                            + '<td>' + value.st_livro + '</td>'
                            + '<td>' + value.st_tipolivro + '</td>'
                            + '<td>' + value.st_livrocolecao + '</td></tr>');
                });

                tbody.find('.icon-plus').click(function() {
                    that.form.trigger("FormLivroBuscaEvent", $(this).data());
                }).css({'cursor': 'pointer'});
            } else {
                table.hide();
            }
            response.title = 'Livros - ' + response.title;
            loaded();
            $.pnotify(response);//mensagem de retorno

        });

        return false;
    }
});
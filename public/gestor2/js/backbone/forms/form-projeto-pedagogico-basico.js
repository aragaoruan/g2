if (typeof ProjetoPedagogicoModel == "undefined") {
    $.getScript('/js/backbone/models/projeto-pedagogico.js');
}
if (typeof Situacoes == "undefined") {
    $.getScript('/js/backbone/models/situacao.js');
}
if (typeof ContratoRegraModel == "undefined") {
    $.getScript('/js/backbone/models/contrato-regra.js');
}
if (typeof NivelEnsinoModel == "undefined") {
    $.getScript('/js/backbone/models/nivel-ensino.js');
}
if (typeof FormProjetoPedagogicoSerieNivelEnsino == "undefined") {
    $.getScript('/js/backbone/forms/form-projeto-pedagogico-serie-nivel-ensino.js');
}
if (typeof FormAreaProjetoPedagogico == "undefined") {
    $.getScript('/js/backbone/forms/form-area-projeto-pedagogico.js');
}
if (typeof FormProjetoPedagogicoCoordenadores == "undefined") {
    $.getScript('/js/backbone/forms/form-projeto-pedagogico-coordenadores.js');
}
if (typeof ModuloModel == "undefined") {
    $.getScript('/js/backbone/models/modulo.js');
}

$.getScript('/js/MensagensG2.js');
/**
 * Alterações necessárias na Collection Situacoes
 * @type @exp;Situacoes@call;extend
 */
var SituacoesCollection = Situacoes.extend({
    data: {
        st_tabela: 'tb_projetopedagogico'
    },
    parse: function (data) {
        var retorno = [];
        retorno[0] = new Situacao({id_situacao: '', st_situacao: 'Selecione uma Situação'});
        $.each(data, function (i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;
    }
});

var ContratosRegraCollection = Backbone.Collection.extend({
    model: ContratoRegraModel,
    url: "/api/contrato-regra",
    parse: function (data) {
        var retorno = [];
        retorno[0] = new ContratoRegraModel({
            id_contratoregra: '',
            st_contratoregra: 'Selecione uma Regra de Contrato'
        });
        $.each(data, function (i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;
    }
});

var FormProjetoPedagogicoModelBasico = ProjetoPedagogicoModel.extend({

    schema: {
        st_projetopedagogico: {
            type: 'Text',
            title: 'Título',
            validators: [{type: 'required', message: 'Você precisa informar o Título!'}],
            editorAttrs: {class: 'span12'}
        },
        id_situacao: {
            type: 'Select',
            title: 'Situação',
            editorAttrs: {class: 'span12'},
            options: new SituacoesCollection(),
            validators: [{type: 'required', message: 'Você precisa informar a Situação!'}]
        },
        st_tituloexibicao: {
            type: 'Text',
            title: 'Título Exibição',
            validators: [{type: 'required', message: 'Você precisa informar o Título exibição!'}],
            editorAttrs: {class: 'span12'}
        },
        nu_cargahoraria: {
            type: 'Number',
            title: 'Carga Horária',
            validators: [{type: 'required', message: 'Você precisa informar uma carga horária!'}],
            editorAttrs: {class: 'span12', maxlength: 4, min: "0", max: 9999}
        },
        st_apelido: {
            type: 'Text', title: 'Apelido', editorAttrs: {class: 'span12', maxlength: '15'}
        },
        id_contratoregra: {
            type: 'Select',
            title: 'Regra do Contrato',
            editorAttrs: {class: 'span13'},
            options: new ContratosRegraCollection(),
            validators: [{type: 'required', message: 'Você precisa informar a Regra do Contrato!'}]
        },
        bl_autoalocaraluno: {
            type: 'Checkbox', title: 'Auto alocar alunos?', editorAttrs: {class: ''}
        },
        bl_turma: {
            type: 'Checkbox', title: 'Trabalha com turmas?', editorAttrs: {class: ''}
        },
        bl_gradepronta: {
            type: 'Checkbox', title: 'Possui grade curricular?', editorAttrs: {class: ''}
        },
        bl_disciplinacomplementar: {
            type: 'Checkbox', title: 'É de disciplina complementar', editorAttrs: {class: ''}
        },
        bl_enviaremail: {
            type: 'Checkbox', title: 'Enviar email', editorAttrs: {class: ''}
        },
        bl_novoportal: {
            type: 'Checkbox', title: 'Acessado pelo Portal do Aluno?', editorAttrs: {class: ''}
        },
        bl_atendimentovirtual: {
            type: 'Checkbox', title: 'ChatBot Ativo?', editorAttrs: {class: ''}
        }
    }
});

var FormProjetoPedagogicoBasico = Backbone.View.extend({
    events: {
        "click button#btn-salvar-projeto-pedagogico-basico": "salvar",
        'change #id_anexoementa': 'validaExtensao'
    },

    initialize: function () {
        this.model = new FormProjetoPedagogicoModelBasico();
        this.formEscolaridade = new FormProjetoPedagogicoSerieNivelEnsino();
        this.formArea = new FormAreaProjetoPedagogico();
        this.formCoordenadores = new FormProjetoPedagogicoCoordenadores();
        this.modelModulo = new ModuloModel();
    },

    render: function () {
        var that = this;

        $.ajaxSetup({async: false});
        $.get('/js/backbone/templates/form-projeto-pedagogico-basico.html', {"_": $.now()}, function (template) {

            if (that.model.get('id_projetopedagogico')) {
                that.model.fetch();
            }

            that.form = new Backbone.Form({
                template: _.template(template),
                model: that.model
            }).render();

            that.$el.html(that.form.el);

            if (that.model.get('id_projetopedagogico')) {

                that.formEscolaridade.model.set({id_projetopedagogico: that.model.get('id_projetopedagogico')});
                that.formEscolaridade.model.fetch();

                that.formArea.model.set({id_projetopedagogico: that.model.get('id_projetopedagogico')});
                that.formArea.model.fetch();

                that.formCoordenadores.model = that.model;

            }

            that.$el.find("#form-projeto-pedagogico-escolaridade").html(that.formEscolaridade.render().el);

            that.$el.find("#form-projeto-pedagogico-area-conhecimento").html(that.formArea.render().el);

            that.$el.find("#form-projeto-pedagogico-coordenadores").html(that.formCoordenadores.render().el);

            that.mostraArquivoSelecionado();

        });
        $.ajaxSetup({async: true});
        return this;
    },
    validaExtensao: function (e) {
        var input = $(e.target);
        // Validar extensao do arquivo
        var extensions = ['.pdf', '.doc', '.docx'];
        var extensaoValida = (new RegExp('(' + extensions.join('|').replace(/\./g, '\\.') + ')$')).test(input.val());

        if (!extensaoValida) {
            input.val('');
            $.pnotify({title: 'Aviso', text: 'Esta extensão de arquivo não é permitida.', type: 'warning'});
            return false;
        }

        return true;
    },
    mostraArquivoSelecionado: function () {
        $.getJSON('/api/upload/' + this.model.get('id_anexoementa'), function (response) {
            if (response && response.id_upload) {
                $('.ementa-label')
                    .attr('href', '/upload/ementa/' + response.st_upload)
                    .html(response.st_upload);
            } else {
                $('.ementa-label')
                    .removeAttr('href')
                    .html('Nenhum');
            }
        });
    },
    salvar: function () {

        var that = this;
        var disparaEvento = false;
        if (this.form.model.get('id_projetopedagogico') == null) {
            disparaEvento = true;
        }

        $.each(this.form.fields, function (index, value) {
            value.editor.$el.removeAttr('style');
        });

        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function (index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        this.form.model.set(this.form.getValue());

        var elem = this.$el;
        var errormsg = null;
        var id_nivelensino = elem.find('[name="id_nivelensino"]').val();
        var id_serie = elem.find('ul[name="id_serie"] input:checked').length;
        var id_areaconhecimento = elem.find('ul[name="id_areaconhecimento"] input:checked').length;

        if (!id_nivelensino) {
            errormsg = "selecione o nível de ensino.";
        } else if (!id_serie) {
            errormsg = "selecione a série.";
        } else if (!id_areaconhecimento) {
            errormsg = "selecione a área de conhecimento.";
        }

        if (errormsg) {
            $.pnotify({
                title: 'Campo obrigatório:',
                type: 'error',
                text: errormsg
            });

            return false;
        }

        loading();


        this.form.model.save(null, {
            // SE SALVAR COM SUCESSO, PROCEDER SALVANDO DEPENDENCIAS
            success: function (model, response) {
                // SALVAR ESCOLARIDADE
                that.formEscolaridade.model.set({id_projetopedagogico: response.id_projetopedagogico});
                that.formEscolaridade.salvar();

                // SALVAR AREAS DE CONHECIMENTO
                that.formArea.model.set({id_projetopedagogico: response.id_projetopedagogico});
                that.formArea.salvar();

                // SALVAR COORDENADORES
                that.formCoordenadores.model.set({id_projetopedagogico: response.id_projetopedagogico});
                var sucessoCoordenador = that.formCoordenadores.salvar();


                // SALVAR ARQUIVO ANEXO EMENTA
                var input_anexo = $('#id_anexoementa');
                if (input_anexo.val()) {
                    var arquivos = input_anexo[0].files;
                    var formData = new FormData();

                    for (var i = 0; i < arquivos.length; i++) {
                        var file = arquivos[i];
                        formData.append('arquivo', file, file.name);
                    }
                    $.ajax({
                        url: 'projeto-pedagogico/salvar-anexo-ementa?id_projetopedagogico=' + response.id_projetopedagogico,
                        type: 'post',
                        data: formData,
                        cache: false,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.type == 'success') {
                                that.model.set('id_anexoementa', response.codigo);
                                that.mostraArquivoSelecionado();
                                $.pnotify({
                                    title: 'Concluído',
                                    text: 'O novo arquivo de Ementa foi salvo!',
                                    type: 'success'
                                });
                            } else {
                                this.error();
                            }
                        },
                        error: function () {
                            $.pnotify({title: 'Erro', text: 'Houve um erro ao salvar o arquivo.', type: 'error'});
                        },
                        complete: function () {
                            input_anexo.val('');
                        }
                    });
                }


                if (disparaEvento) {
                    that.trigger("FormProjetoPedagogicoBasicoSaveEvent", model);
                }

                loaded();

                if (sucessoCoordenador) {
                    response.title = 'Sucesso!';
                    response.text = 'Projeto pedagógico salvo com sucesso.';
                    response.type = 'success';
                } else {
                    response.title = 'Concluído';
                    response.text = 'Projeto pedagógico salvo, porém um ou mais coordenadores não foram inseridos. Verfique a lista de coordenadores.';
                    response.type = 'warning';
                }
                $.pnotify(response);//mensagem de retorno

            },
            error: function (model, response) {
                loaded();
                $.pnotify({
                    title: "Projeto Pedagógico - Erro",
                    text: "Houve um erro ao salvar o projeto pedagógico.",
                    type: 'error'
                });
            }
        });

        return false;
    }
});
/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 07/01/2014
 * Formulário para cadastro das Disciplinas no Projeto Pedagógico
 */

/**
 * Dependências
 */
// form-projeto-pedagogico-coordenadores
if (typeof FormProjetoModuloDisciplina == "undefined") {
    $.getScript('/js/backbone/forms/form-projeto-modulo-disciplina.js');
}
;
if (typeof FormDisciplinaBusca == "undefined") {
    $.getScript('/js/backbone/forms/form-disciplina-busca.js');
}
;
if (typeof VwModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-modulo-disciplina.js');
}
;
if (typeof ModuloModel == "undefined") {
    $.getScript('/js/backbone/models/modulo.js');
}
;
if (typeof ModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/modulo-disciplina.js');
}
;

//novo formulário de pesquisa de disciplinas
if (typeof VwDisciplinaSerieNivelModel == "undefined") {
    $.getScript('/js/backbone/models/vw-disciplina-serie-nivel.js');
}

var VwDisciplinaSerieNivelCollection = Backbone.Collection.extend({
    model: VwDisciplinaSerieNivelModel,
    async: false,
    url: '/api/vw-disciplina-serie-nivel/'
});

var VwModuloDisciplinaCollection = Backbone.Collection.extend({
    model: VwModuloDisciplinaModel,
    url: function () {
        return "/api/vw-modulo-disciplina/?" + $.param(_.defaults(this.data));
    },
    parse: function (data) {
        return data;
    },
    data: {
        id_projetopedagogico: null, bl_ativo: null
    }
});

/**
 * ITENS DA NOVA PESQUISA DE DISCIPLINAS
 */

var collectionSelecionados = new VwModuloDisciplinaCollection();

var collectionDisciplinas = new VwDisciplinaSerieNivelCollection();

var DisciplinasItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td><%=st_disciplina%></td>' +
    '<td><%=st_nivelensino%></td>' +
    '<td><%=st_serie%></td>' +
    '<td><a class="btn btn-success pull-right" id="btn-add"><i class="icon  icon-arrow-right icon-white"></i></a></td>'),
    onRender: function () {
        return this;
    },
    addModel: function () {
        collectionSelecionados.add(this.model.clone());
        this.remove();
        //this.model.destroy({});
    },
    events: {
        'click #btn-add': 'addModel'
    }
});

var DisciplinasSelecionadasItemView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: _.template('<td><a class="btn btn-danger pull-left" id="btn-remove">' +
    '<i class="icon  icon-arrow-left icon-white"></i></a></td>'
    + '<td><%=st_disciplina%></td>'),
    collection: collectionSelecionados,
    removeModel: function () {
        var that = this;

        if (that.model.get('id_modulodisciplina')) {
            bootbox.confirm("Esta disciplina será removida do projeto pedagógico! Deseja continuar?", function (result) {
                if (result) {
                    collectionDisciplinas.add(that.model.clone());
                    that.remove();
                    that.model.url = '/api/modulo-disciplina/id/' + that.model.get('id_modulodisciplina');
                    that.model.destroy({});
                    $.pnotify({title: 'Disciplina removida', type: 'success'});
                }
            });
        } else {
            collectionDisciplinas.add(that.model.clone());
            that.remove();
            that.model.destroy();
            collectionDisciplinas.comparator = 'st_disciplina';
            collectionDisciplinas.sort();

        }
    },
    onRender: function () {
        collectionDisciplinas.remove(this.model);
        return this;
    },
    events: {
        'click #btn-remove': 'removeModel'
    }
});

var DisciplinasCollectionSelecionadosView = Marionette.CollectionView.extend({
    template: '#form-projeto-pedagogico-modal-disciplina-busca',
    childView: DisciplinasSelecionadasItemView,
    initialize: function () {
        this.collection.fetch();
    }
});

var DisciplinasCollectionView = Marionette.CollectionView.extend({
    template: '#form-projeto-pedagogico-modal-disciplina-busca',
    childView: DisciplinasItemView,
    initialize: function () {
        this.collection.fetch();
    }
});

/**
 * FIM DOS ITENS DA NOVA PESQUISA DE DISCIPLINAS
 */



var ModuloCollection = Backbone.Collection.extend({
    model: ModuloModel,
    url: function () {
        return "/api/modulo/?" + $.param(_.defaults(this.data));
    },
    data: {
        id_projetopedagogico: null
    }
});

/**
 *
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormProjetoPedagogicoDisciplina = Backbone.View.extend({
    initialize: function () {
        this.model = new ProjetoPedagogicoModel();
        this.modelModulo = new ModuloModel();
    },
    events: {
        "click button#form-projeto-pedagogico-tab-disciplina-btn-adicionar": "pesquisarDisciplinas",
        "click button#form-projeto-pedagogico-tab-disciplina-btn-salvar": "salvar",
        "click button#form-projeto-pedagogico-tab-disciplina-btn-corrigir": "corrigirMatriculas",
        "click button#form-projeto-modulo-disciplina-btn-excluir": "excluir",
        "click a#salvar-disciplinas-modal": "salvarDisciplinasModal"
    },
    forms: new Array(),
    render: function () {

        if (!this.model.get('id_projetopedagogico')) {
            console.log('FormProjetoPedagogicoDisciplina sem ProjetoPedagógico setado');
            return this;
        }

        $.ajaxSetup({async: false});
        loading();
        var that = this;

        var MCollection = new ModuloCollection();
        MCollection.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        MCollection.fetch({
            success: function (collection, response, options) {
                MCollection.each(function (ModelModulo) {
                    that.modelModulo = ModelModulo;
                    return false;
                });
            }, error: function () {
            }
        });

        that.$el.html('Aguarde...');
        that.$el.empty();
        $.get('/js/backbone/templates/form-projeto-pedagogico-disciplina.html', {"_": $.now()}, function (template) {
            that.$el.html(template);

            var myModel = VwModuloDisciplinaModel.extend({
                idAttribute: 'id_disciplina',
                defaults: {
                    id_disciplina: null,
                    st_disciplina: ''
                }
            });
            var disciplinaAdded = new VwModuloDisciplinaCollection([], {model: myModel});
            disciplinaAdded.data = {
                id_projetopedagogico: that.model.get('id_projetopedagogico'),
                bl_ativo: 1
            };

            disciplinaAdded.on('add', function (model) {
                collectionSelecionados.add(model);
            });

            var sideBySideDisciplinas = new SideBySideView({
                collections: {
                    left: new VwDisciplinaSerieNivelCollection([], {model: myModel}),
                    right: disciplinaAdded
                },
                label: 'st_disciplina',
                fetch: true
            });

            that.$el.find('.disciplinas-sidebyside').html(sideBySideDisciplinas.render().$el);

            disciplinaAdded.fetch().done(function () {
                that.tbody = that.$el.find('div#form-projeto-pedagogico-tab-disciplina-grid .tablebody');
                that.forms.length = 0;
                disciplinaAdded.each(function (vw) {
                    if (vw.get('id_disciplina'))
                        that.adicionar(vw);
                });
            });

        });

        //this.tbody = that.$el.find('div#form-projeto-pedagogico-tab-disciplina-grid .tablebody');
        //this.forms.length = 0;
        //var disciplinas = new VwModuloDisciplinaCollection();
        //disciplinas.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        //disciplinas.data.bl_ativo = 1;
        //disciplinas.fetch({
        //    success: function (collection, response, options) {
        //        disciplinas.each(function (vw) {
        //            if (vw.get('id_disciplina')) {
        //                that.adicionar(vw);
        //            }
        //        });
        //    }
        //});
        loaded();
        $.ajaxSetup({async: true});

        return this;
    },
    pesquisarDisciplinas: function () {
        collectionSelecionados.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        collectionSelecionados.data.bl_ativo = 1;

        $("#form-projeto-pedagogico-modal-disciplina-busca").modal('show');
    },
    salvarDisciplinasModal: function () {
        var that = this;

        $.ajaxSetup({async: false});

        if (that.modelModulo.get('id_modulo') == null) {
            that.modelModulo.set({
                st_modulo: that.model.get('st_projetopedagogico'),
                st_tituloexibicao: that.model.get('st_tituloexibicao'),
                bl_ativo: true,
                st_descricao: that.model.get('st_descricao'),
                id_projetopedagogico: that.model.get('id_projetopedagogico')
            });
            that.modelModulo.save();
        }

        //Array que armazena os models a serem removidos posteriormente.
        var toRemove = [];

        _.each(collectionSelecionados.models, function (modelo) {
            if (!modelo.get('id_modulodisciplina')) {
                var moduloDisciplina = new ModuloDisciplinaModel();
                moduloDisciplina.set({
                    id_modulodisciplina: null,
                    id_modulo: that.modelModulo.get('id_modulo'),
                    id_disciplina: modelo.get('id_disciplina'),
                    id_serie: modelo.get('id_serie'),
                    id_nivelensino: modelo.get('id_nivelensino'),
                    bl_ativo: true,
                    bl_obrigatoria: true
                });
                moduloDisciplina.save(null, {
                    success: function (model, response) {
                        /*var vw = new VwModuloDisciplinaModel();
                         vw.set('id_modulodisciplina', model.get('id_modulodisciplina'));
                         vw.fetch({
                         success: function(model, response, options) {
                         that.adicionar(model);
                         that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
                         }
                         });*/
                        //Adiciona o model no array para posterior remoção.
                        toRemove.push(modelo);
                        response.title = 'Módulo Disciplina - Sucesso';
                        response.text = 'Operação executada com sucesso!';
                        response.type = 'success';
                        $.pnotify(response);//mensagem de retorno
                    },
                    error: function (model, response) {
                        loaded();
                        $.pnotify({title: "Módulo Disciplina - Erro", text: response.mensagem, type: 'error'});
                    },
                    complete: function () {
                    }
                });
            }
        });
        /*Remove o modelo adicionado da Collection, mantendo-a
         "limpa" e evitando duplicidade na inserção de novas disciplinas.*/
        collectionSelecionados.remove(toRemove);
        $.ajaxSetup({async: true});
        this.render();
        this.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
    },
    salvarModuloDisciplina: function (Obj) {
        $.ajaxSetup({async: false});
        loading();
        var that = this;
        try {
            if (this.modelModulo.get('id_modulo') == null) {
                this.modelModulo.set({
                    st_modulo: Obj.disciplina.get('st_disciplina'),
                    st_tituloexibicao: Obj.disciplina.get('st_disciplina'),
                    bl_ativo: true,
                    st_descricao: Obj.disciplina.get('st_descricao'),
                    id_projetopedagogico: this.model.get('id_projetopedagogico')
                });
                this.modelModulo.save();
            }
            if (this.modelModulo.get('id_modulo')) {
                var moduloDisciplina = new ModuloDisciplinaModel();
                moduloDisciplina.set({
                    id_modulodisciplina: null,
                    id_modulo: this.modelModulo.get('id_modulo'),
                    id_disciplina: Obj.disciplina.get('id_disciplina'),
                    id_serie: Obj.disciplina.get('id_serie'),
                    id_nivelensino: Obj.disciplina.get('id_nivelensino'),
                    bl_ativo: true,
                    bl_obrigatoria: true
                });
                moduloDisciplina.save(null, {
                    success: function (model, response) {
                        var vw = new VwModuloDisciplinaModel();
                        vw.set('id_modulodisciplina', model.get('id_modulodisciplina'));
                        vw.fetch({
                            success: function (model, response, options) {
                                that.adicionar(model);
                                that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
                            }
                        });
                        response.title = 'Módulo Disciplina - Sucesso';
                        response.text = 'Operação executada com sucesso!';
                        response.type = 'success';
                        $.pnotify(response);//mensagem de retorno

                    },
                    error: function (model, response) {
                        loaded();
                        $.pnotify({title: "Módulo Disciplina - Erro", text: response.mensagem, type: 'error'});
                    },
                    complete: function () {

                    }
                });

                //console.log(Obj.disciplina);  
                //console.log(moduloDisciplina);
            } //if(this.modelModulo.get('id_modulo')
        } catch (err) {
            //console.log(err);
        }
        loaded();
        $.ajaxSetup({async: true});
    },
    adicionar: function (vw) {

        try {

            var newForm = new FormProjetoModuloDisciplina();
            $.each(newForm.model.attributes, function (index, value) {
                newForm.model.set(index, vw.get(index));
            });
            newForm.parentIndex = this.forms.length;
            newForm.vw = vw;

            newForm.on("FormProjetoModuloDisciplinaDeleteEvent", function (Obj) {
                that.forms[newForm.parentIndex] = false;
                that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
            });

            this.tbody.append(newForm.render().el);
            this.forms[newForm.parentIndex] = newForm;

        } catch (err) {
            //console.log(err);
        }


    },
    salvar: function () {
        $.ajaxSetup({async: false});
        var that = this;
        $.each(this.forms, function (index, formIndividual) {
            if (formIndividual) {
                formIndividual.salvar();
            }
        });
        that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
        $.ajaxSetup({async: true});
    },
    corrigirMatriculas: function () {

        if (!this.model.get('id_projetopedagogico')) {
            $.pnotify({
                title: "Módulo Disciplina - Erro",
                text: "É preciso salvar o Projeto Pedagógico antes de corrigir as Matrículas.",
                type: 'error'
            });
            return false;
        }

        loading();

        var id_projetopedagogico = this.model.get('id_projetopedagogico');

        $.post("/Matricula/corrigir-matriculas", {id_projetopedagogico: id_projetopedagogico}, function (data) {
            $.pnotify(data);
            loaded();
        }, "json");

    },
    excluir: function () {
        $.ajaxSetup({async: false});
        var that = this;
        var nu_excluir = 0;
        $.each(this.forms, function (index, formIndividual) {
            if (formIndividual.form.$el.find('input[name="modulo-disciplina-ck-excluir"]').is(":checked")) {
                nu_excluir += 1;
            }
        });

        //return false;
        if (nu_excluir > 0) {
            bootbox.confirm("Deseja realmente excluir " + nu_excluir + " disciplina" + (nu_excluir > 1 ? "s" : "" ) + " ?", function (result) {
                if (result) {
                    loading();
                    $.each(that.forms, function (index, formIndividual) {

                        if (formIndividual.form.$el.find('input[name="modulo-disciplina-ck-excluir"]').is(":checked")) {
                            if (typeof formIndividual.form.model.get('id_modulodisciplina') == "undefined" || formIndividual.form.model.get('id_modulodisciplina') == false) {
                                formIndividual.$el.remove();
                                that.forms.remove(formIndividual);
                                delete that.forms[index];
                                loaded();
                            } else {
                                formIndividual.model.destroy({
                                    success: function (model, response) {
                                        formIndividual.$el.remove();
                                        //formIndividual.form.$el.remove();
                                        response.title = 'Disciplina - Sucesso';
                                        response.text = 'Registro excluído com sucesso!';
                                        response.type = 'success';
                                        $.pnotify(response);
                                        delete that.forms[index];
                                        loaded();
                                    },
                                    error: function (model, response) {
                                        $.pnotify($.parseJSON(response.responseText));
                                        loaded();
                                    },
                                    complete: function () {
                                    }
                                });

                            }
                        }

                    });
                    that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
                }
            });

        }

        $.ajaxSetup({async: true});
    }
});

if (typeof ProdutoAreaModel == "undefined") {
    $.getScript('/js/backbone/models/produto-area.js');
}
;

if (typeof VwProdutoAreaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-produto-area.js');
}
;

if (typeof AreaConhecimentoModel == "undefined") {
    $.getScript('/js/backbone/models/area-conhecimento.js');
}
;

var AreaConhecimentoCollection = Backbone.Collection.extend({
    model: AreaConhecimentoModel,
    url: function () {
        return "/api/area-conhecimento/?" + $.param(_.defaults(this.data));
    },
    data: {
        bl_ativo: true,
        backbone: true
    }
});

var VwProdutoAreaCollection = Backbone.Collection.extend({
    model: VwProdutoAreaModel,
    url: function () {
        debug(this.data);
        return "/api/vw-produto-area/?" + $.param(_.defaults(this.data));
    },
    data: {
        id_produto: null
    }
});


var FormProdutoAreaModel = ProdutoAreaModel.extend({
    schema: {
        id_areaconhecimento: {
            type: 'Checkboxes', title: 'Área', editorAttrs: {class: 'span12'}, options: new AreaConhecimentoCollection(), validators: [
                {type: 'required', message: 'Você precisa informar a Área!'}
            ]
        }
    }
});


var FormProdutoArea = Backbone.View.extend({
    initialize: function () {
        this.model = new FormProdutoAreaModel();
    },
    render: function () {
        $.ajaxSetup({async: false});
        var that = this;

        if (this.model.get('id_produto') && !this.model.get('id_areaconhecimento')) {

            var arrayAreas = new Array();
            var collection = new VwProdutoAreaCollection();
//            collection.data.id_produto = this.model.get('id_produto');
            collection.url = "/api/vw-produto-area/id_produto/" + this.model.get('id_produto');
            collection.fetch();
            collection.each(function (vw) {
                arrayAreas.push(vw.get('id_areaconhecimento'));
            }, this);

//		collection.data.id_produto = null;
            this.model.set({id_areaconhecimento: arrayAreas});

        }
        ;


        this.form = new Backbone.Form({
            template: _.template('<form><fieldset><legend>Áreas</legend>' +
            '<div data-editors="id_areaconhecimento" class="span12 correcaoulli" style="margin-left: 0px"></div></fieldset></form>'),
            model: this.model
        });

        this.$el.html(this.form.render().el);

        $.ajaxSetup({async: true});
        return this;
    },
    salvar: function () {

        var that = this;

        $.each(this.form.fields, function (index, value) {
            value.editor.$el.removeAttr('style');
        });
        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function (index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        var dados = this.form.getValue();
        //dados.id_areaconhecimento = dados.id_areaconhecimento.map(customParseInt);
        dados.id_areaconhecimento = parseInt(dados.id_areaconhecimento);

        this.form.model.set(dados);
        if (!this.form.model.hasChanged('id_areaconhecimento')) {
            return false;
        }


        this.form.model.save(null, {
            success: function (model, response) {
                response.title = 'Área - Sucesso';
                response.text = 'Operação executada com sucesso!';
                response.type = 'success';
                $.pnotify(response);//mensagem de retorno
            },
            error: function (model, response) {
                $.pnotify({title: "Área", text: response.mensagem, type: 'error'});
            }
        });
        return false;
    }
});
if (typeof AreaProjetoPedagogicoModel == "undefined") {
    $.getScript('/js/backbone/models/area-projeto-pedagogico.js');
}
;

if (typeof VwProjetoAreaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-projeto-area.js');
}
;

var VwProjetoAreaCollection = Backbone.Collection.extend({
    model: VwProjetoAreaModel,
    url: function () {
        return "/api/vw-projeto-area/?" + $.param(_.defaults(this.data));
    },
    data: {
        id_projetopedagogico: null
    }
});

var FormAreaProjetoPedagogicoModel = AreaProjetoPedagogicoModel.extend({
    schema: {
        id_areaconhecimento: {
            type: 'Radio',
            title: 'Área',
            editorAttrs: {class: 'span5'},
            options: new VwProjetoAreaCollection(),
            validators: [{type: 'required', message: 'Você precisa informar a Área!'}]
        }
    }
});

var FormAreaProjetoPedagogico = Backbone.View.extend({

    initialize: function () {
        this.model = new FormAreaProjetoPedagogicoModel();
    },
    render: function () {
        $.ajaxSetup({async: false});
        var that = this;

        if (this.model.get('id_projetopedagogico') && !this.model.get('id_areaconhecimento')) {

            var arrayAreas = [];
            var collection = new VwProjetoAreaCollection();
            collection.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
            collection.fetch();
            collection.each(function (vw) {
                if (vw.get('id_projetopedagogico')) {
                    //arrayAreas.push(vw.get('id_areaconhecimento'));
                    arrayAreas = vw.get('id_areaconhecimento');
                }
                ;

            }, this);
            this.model.set({id_areaconhecimento: arrayAreas});

        }
        ;

        this.form = new Backbone.Form({
            template: _.template('<div class="span5" style="margin-left: 0px"><div data-editors="id_areaconhecimento" class="span5" style="margin-left: 0px"><label>Área</label></div></div>'),
            model: this.model
        });

        this.$el.html(this.form.render().el);
        $.ajaxSetup({async: true});
        return this;
    },

    salvar: function () {

        var that = this;

        $.each(this.form.fields, function (index, value) {
            value.editor.$el.removeAttr('style');
        });
        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function (index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        var dados = this.form.getValue();
        //dados.id_areaconhecimento = dados.id_areaconhecimento.map(customParseInt);
        dados.id_areaconhecimento = [parseInt(dados.id_areaconhecimento)];

        this.form.model.set(dados);
        if (!this.form.model.hasChanged('id_areaconhecimento')) {
            return false;
        }

        this.form.model.save(null, {
            success: function (model, response) {
                response.title = 'Área - Sucesso';
                response.text = 'Operação executada com sucesso!';
                response.type = 'success';
                $.pnotify(response);//mensagem de retorno
            },
            error: function (model, response) {
                $.pnotify({title: "Área", text: response.mensagem, type: 'error'});
            }
        });
        return false;
    }
});
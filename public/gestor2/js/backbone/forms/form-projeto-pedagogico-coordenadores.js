/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 14/11/2013
 * Formulário para cadastro dos Coordenadoress de Produtos
 */

/**
 * Dependências
 */
// form-projeto-pedagogico-coordenadores
if (typeof FormUsuarioPerfilEntidadeReferencia == "undefined") {
    $.getScript('/js/backbone/forms/form-usuario-perfil-entidade-referencia.js');
}
;

if (typeof VwUsuarioPerfilEntidadeReferenciaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-usuario-perfil-entidade-referencia.js');
}
;


var VwUsuarioPerfilEntidadeReferenciaCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeReferenciaModel,
    url: function () {
        return "/api/vw-usuario-perfil-entidade-referencia/?" + $.param(_.defaults(this.data)) + "&id_perfilpedagogico=2";
    },
    parse: function (data) {
        if(data[0] && data[0].id_perfilreferencia){
            return data;
        } else {
            return [];
        }
    },
    data: {
        id_projetopedagogico: null
    }
});


/**
 * Formulário dos Coordenadoress
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormProjetoPedagogicoCoordenadores = Backbone.View.extend({
    forms: new Array(),
    initialize: function () {
        this.forms.length = 0;
        this.model = new ProjetoPedagogicoModel();
    },
    events: {
        "click button#form-projeto-pedagogico-coordenadores-btn-adicionar": "adicionarForm"

    },
    tbody: null,
    render: function () {
        $.ajaxSetup({async: false});
        var that = this;

        that.$el.empty();
        $.get('/js/backbone/templates/form-projeto-pedagogico-coordenadores.html', {"_": $.now()}, function (template) {
            that.$el.html(template);
        });

        this.tbody = that.$el.find('div#form-projeto-pedagogico-coordenadores-grid .tablebody');

        if (!this.model.get('id_projetopedagogico')) {
            return this;
        }

        var x = 0;
        var coordenadores = new VwUsuarioPerfilEntidadeReferenciaCollection();
        coordenadores.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        coordenadores.fetch();

        coordenadores.each(function (vw) {
            that.adicionaFormUsuarioPerfilEntidadeReferencia(vw);
        });
        $.ajaxSetup({async: true});
        return this;
    },
    adicionarForm: function () {

        var coordenadores = new VwUsuarioPerfilEntidadeReferenciaCollection();
        coordenadores.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        this.adicionaFormUsuarioPerfilEntidadeReferencia(coordenadores);

    },
    adicionaFormUsuarioPerfilEntidadeReferencia: function (vw) {
        var that = this;
        var ja = false;

        if (typeof vw.get('id_usuario') != "undefined") {
            $.each(that.forms, function (index, formIndividual) {
                if (formIndividual) {
                    if (formIndividual.model.get('id_usuario') == vw.get('id_usuario')) {
                        $.pnotify({
                            title: "Coordenadores - Aviso",
                            text: "O Usuário " + vw.get('st_nomecompleto') + " já está na lista de Coordenadores!",
                            type: 'warnning'
                        });
                        ja = true;
                    }
                }
            });
        }


        if (ja) {
            return false;
        }
        ;


        var newForm = new FormUsuarioPerfilEntidadeReferencia();
        $.each(newForm.model.attributes, function (index, value) {
            newForm.model.set(index, vw.get(index));
        });
        newForm.parentIndex = this.forms.length;

        newForm.on("FormUsuarioPerfilEntidadeReferenciaDeleteEvent", function (Obj) {
            that.forms[newForm.parentIndex] = false;
        });

        this.tbody.append(newForm.render().el);
        this.forms[newForm.parentIndex] = newForm;

    },
    calculaValores: function () {

        var valorTotal = 0.00;
        var valorAtual = 0.00;
        //console.log(this.forms);
        $.each(this.forms, function (index, formIndividual) {
            // console.log(formIndividual);
            if (formIndividual) {
                valorAtual = parseFloat(formIndividual.form.getValue('nu_porcentagem'), 10).toFixed(2);
                //console.log(valorAtual);
                valorTotal = Number(valorTotal) + Number(valorAtual);
            }
        });
        if (valorTotal > 100) {
            $.pnotify({
                title: "Coordenador - Erro",
                text: "A soma da porcentagem não pode ser maior que 100. " + valorTotal + " foi o valor total informado!",
                type: 'error'
            });
            return false;
        }
        return true;
    },

    salvar: function () {

        if (this.calculaValores()) {
            var success = true;
            var that = this;
            $.each(this.forms, function (index, formIndividual) {
                if (formIndividual) {
                    formIndividual.model.set({
                        id_projetopedagogico: that.model.get('id_projetopedagogico'),
                        id_usuario: formIndividual.$el.find('[name="id_usuario"]').val()
                    });

                    formIndividual.$el.removeClass('alert-success alert-danger');

                    //Validação do usuário
                    if (formIndividual.model.get('id_usuario')) {
                        formIndividual.$el.addClass('alert alert-success');
                        formIndividual.salvar();
                    } else {
                        //formIndividual.model.destroy();
                        formIndividual.$el.addClass('alert alert-warning');
                        success=false;
                    }
                }
            });
            return success;
        };
    }

});
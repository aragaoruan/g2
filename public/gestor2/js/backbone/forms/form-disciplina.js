var FormDisciplina = Backbone.Form.extend({
    template: function() {
        $.get('/js/backbone/templates/form-disciplina.html', {"_": $.now()}, function(template) {
            return _.template(template);
        });
    },
    schema: {
        st_disciplina: {
            type: 'Text',
            title: 'Título',
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar o Título!'
                }
            ],
            editorAttrs: {
                class: 'span12'
            }
        },
        st_descricao: {
            type: 'Text',
            title: 'Descrição',
            editorAttrs: {
                class: 'span12'
            }
        },
        nu_identificador: {
            type: 'Text',
            title: 'Identificador',
            editorAttrs: {
                class: 'span12'
            }
        },
        nu_cargahoraria: {
            type: 'Number',
            title: 'Carga Horária',
            editorAttrs: {
                class: 'span12'
            },
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar a Carga Horária!'
                }
            ],
        },
        nu_creditos: {
            type: 'Number',
            title: 'Créditos',
            editorAttrs: {
                class: 'span12'
            },
        },
        nu_codigoparceiro: {
            type: 'Text',
            title: 'Código do Parceiro',
            editorAttrs: {
                class: 'span12'
            },
        },
        bl_compartilhargrupo: {
            type: 'Select',
            title: 'Compartilhar com Grupo',
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar se deseja Compartilhar com Grupo!'
                }
            ],
            options: [
                {
                    val: 0,
                    label: 'Sim'
                },
                {
                    val: 1,
                    label: 'Não'
                }
            ]
        },
        id_situacao: {
            type: 'Number',
            title: 'Situação',
            editorAttrs: {
                class: 'span12'
            },
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar a Situação!'
                }
            ],
        },
        st_tituloexibicao: {
            type: 'Text',
            title: 'Título de Exibição',
            editorAttrs: {
                class: 'span12'
            },
        },
        id_tipodisciplina: {
            type: 'Number',
            title: 'Tipo',
            editorAttrs: {
                class: 'span12'
            },
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar a Situação!'
                }
            ],
        },
        st_ementacertificado: {
            type: 'Text',
            title: 'Ementa do Certificado',
            editorAttrs: {
                class: 'span12'
            },
        }
    }
});
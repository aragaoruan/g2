/**
 * BACKBONE-FORMS
 * Autor: Denise Xavier <denise.xavier@unyleya.com.br>
 * Data: 11/04/2014
 * Formulário para cadastro das avaliações no Projeto Pedagógico
 */

/**
 * Dependências
 */
// form-projeto-pedagogico-disciplina
if(typeof FormProjetoPedagogicoDisciplina == "undefined") {
   $.getScript('/js/backbone/forms/form-projeto-pedagogico-disciplina.js');
};

if(typeof FormProjetoPedagogicoDisciplinaPonderacao == "undefined") {
   $.getScript('/js/backbone/forms/form-projeto-pedagogico-disciplina-ponderacao.js');
};


var FormProjetoPedagogicoTabAvaliacao = Backbone.View.extend({
  initialize: function() {
	this.model          = new ProjetoPedagogicoModel();
	this.modelModulo    = new ModuloModel();
	this.formDisciplina = new FormProjetoPedagogicoDisciplina();
	this.formPonderacao = new FormProjetoPedagogicoDisciplinaPonderacao();
  },
  render: function() {
		
		if(!this.model.get('id_projetopedagogico')){
			console.log('FormProjetoPedagogicoTabDisciplina sem ProjetoPedagógico setado');
			return this;
		}
		
        var that = this;
			that.$el.empty();  
            that.$el.html('<div class="row span16"><div class="row" id="tab-disciplina-disciplina">Disciplinas</div><div class="row" id="tab-disciplina-ponderacao">Ponderação</div></div>');

			// Renderiza o form de Disciplinas
			that.renderFormDisciplina();
			that.renderFormDisciplinaPonderacao();
		
	return this;
  },
  renderFormDisciplina: function(){
	  
	var that = this;
	
	this.formDisciplina = new FormProjetoPedagogicoDisciplina();  
	this.formDisciplina.on( "FormProjetoPedagogicoDisciplinaChangeEvent", function(Obj) {
		that.formPonderacao.$el.remove();
		that.renderFormDisciplinaPonderacao();
	});
	this.formDisciplina.model.set({id_projetopedagogico : this.model.get('id_projetopedagogico')});
	this.$el.find('#tab-disciplina-disciplina').html(this.formDisciplina.render().el);
	  
  },
  renderFormDisciplinaPonderacao: function(){
	  
	var that = this;
	
	this.formPonderacao = new FormProjetoPedagogicoDisciplinaPonderacao();  
	this.formPonderacao.on( "FormProjetoPedagogicoDisciplinaPonderacaoChangeEvent", function(Obj) {
		that.formDisciplina.$el.remove();
		that.renderFormDisciplina();
	});
	this.formPonderacao.model.set({id_projetopedagogico : this.model.get('id_projetopedagogico')});
	this.$el.find('#tab-disciplina-ponderacao').html(this.formPonderacao.render().el);
	  
  }
  
});
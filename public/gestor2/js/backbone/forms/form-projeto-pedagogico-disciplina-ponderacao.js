/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 07/01/2014
 * Formulário para cadastro das Disciplinas no Projeto Pedagógico
 */

/**
 * Dependências
 */
// form-projeto-pedagogico-coordenadores
if (typeof FormProjetoModuloDisciplinaPonderacao == "undefined") {
    $.getScript('/js/backbone/forms/form-projeto-modulo-disciplina-ponderacao.js');
}

if (typeof VwModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-modulo-disciplina.js');
}

var VwModuloDisciplinaCollection = Backbone.Collection.extend({
    model: VwModuloDisciplinaModel,
    url: function () {
        return "/api/vw-modulo-disciplina/?" + $.param(_.defaults(this.data));
    },
    parse: function (data) {
        return data;
    },
    data: {
        id_projetopedagogico: null,
        bl_ativo: null
    }
});

/**
 *
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormProjetoPedagogicoDisciplinaPonderacao = Backbone.View.extend({
    initialize: function () {
        this.model = new ProjetoPedagogicoModel();
    },
    events: {
        "click button#form-projeto-pedagogico-tab-disciplina-ponderacao-btn-salvar": "salvar",
        "change input[name='nu_ponderacaoaplicada']": 'calculaPondAplicada'
    },
    calculaPondAplicada: function (e) {
        var totalPonAplicada = 0;
        $("input[name='nu_ponderacaoaplicada']").each(function (i, elem) {
            var valor = $(elem).val() ? $(elem).val().replace(',', '.') : 0;
            totalPonAplicada += parseFloat(valor);
        });

        if (totalPonAplicada > 100) {
            $.pnotify({
                title: "Atenção!",
                text: "O valor total de Pond. Aplicada não pode ultrapassar 100%!",
                type: "warning"
            });
            return false;
        } else {
            this.$el.find("#total-soma-ponderacao").val(totalPonAplicada);
            return true;
        }
    },
    forms: new Array(),
    render: function () {

        if (!this.model.get('id_projetopedagogico')) {
            //console.log('FormProjetoPedagogicoDisciplinaPonderacao sem ProjetoPedagógico setado');
            return this;
        }
        $.ajaxSetup({async: false});
        loading();
        var that = this;
        that.$el.empty();
        that.$el.html('Aguarde...');

        that.atualizaPonderacaoCalculada();

        $.get('/js/backbone/templates/form-projeto-pedagogico-disciplina-ponderacao.html', {"_": $.now()}, function (template) {
            that.$el.html(template);
        });

        this.tbody = that.$el.find('div#form-projeto-pedagogico-tab-disciplina-ponderacao-grid .tablebody');
        this.forms.length = 0;
        var disciplinas = new VwModuloDisciplinaCollection();
        disciplinas.data.id_projetopedagogico = this.model.get('id_projetopedagogico');
        disciplinas.data.bl_ativo = 1;
        var totalPonAplicada = 0;
        var totalPondCalculada = 0;


        disciplinas.fetch({
            success: function (collection, response, options) {
                disciplinas.each(function (vw) {
                    if (vw.get('id_disciplina')) {

                        if (vw.get('nu_ponderacaocalculada')) {
                            var valorCalc = parseFloat(vw.get('nu_ponderacaocalculada'));
                            totalPondCalculada += parseFloat(valorCalc.toFixed(2));
                        }

                        if (vw.get('nu_ponderacaoaplicada')) {
                            totalPonAplicada += parseFloat(vw.get('nu_ponderacaoaplicada'));
                        } else {
                            totalPonAplicada += parseFloat(vw.get('nu_ponderacaocalculada'));
                            vw.set('nu_ponderacaoaplicada', vw.get('nu_ponderacaocalculada'));
                        }

                        that.adicionar(vw);
                    }
                });
                totalPondCalculada = Math.round(totalPondCalculada);
            }
        });

        loaded();
        this.$el.find("#total-soma-ponderacao").val(totalPonAplicada.toFixed(2));
        this.$el.find("#total-soma-pond-calculada").html(totalPondCalculada.toFixed(2) + "%");


        $.ajaxSetup({async: true});

        return this;
    },
    adicionar: function (vw) {

        try {

            var newForm = new FormProjetoModuloDisciplinaPonderacao();
            $.each(newForm.model.attributes, function (index, value) {
                newForm.model.set(index, vw.get(index));
            });
            newForm.parentIndex = this.forms.length;
            newForm.vw = vw;

            newForm.on("FormProjetoModuloDisciplinaPonderacaoDeleteEvent", function (Obj) {
                that.forms[newForm.parentIndex] = false;
                that.trigger("FormProjetoPedagogicoDisciplinaPonderacaoChangeEvent", that);
            });

            this.tbody.append(newForm.render().el);
            this.forms[newForm.parentIndex] = newForm;
            this.calculaPondAplicada();
        } catch (err) {
            //console.log(err);
        }


    },
    atualizaPonderacaoCalculada: function () {
        var id = this.model.get('id_projetopedagogico');
        if (id) {
            //var id = G2S.editModel.id;
            $.ajax({
                type: 'post',
                url: '/projeto-pedagogico/atualiza-ponderacao-calculada-disciplina',
                data: {id_projetopedagogico: id},
                dataType: 'json',
                beforeSend: function () {
                    loading();
                },
                complete: function () {
                    loaded();
                },
                success: function (data) {
                    $.pnotify(data);
                }
            });
        }
    },
    salvar: function () {

        var that = this;
        if (that.calculaPondAplicada()) {
            $.each(that.forms, function (index, formIndividual) {
                if (formIndividual) {
                    formIndividual.salvar();
                }
            });
            that.trigger("FormProjetoPedagogicoDisciplinaPonderacaoChangeEvent", that);
//            that.atualizaPonderacaoCalculada();
            that.render();
        }

//
//        var that = this;
//        $.each(this.forms, function (index, formIndividual) {
//            if (formIndividual) {
//                formIndividual.salvar();
//            }
//        });
//	  that.trigger("FormProjetoPedagogicoDisciplinaPonderacaoChangeEvent", that );

    }
});
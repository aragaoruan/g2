/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 06/03/2014
 * Formulário para fazer a integração entre G2 e softwares parceiros
 */

/**
 * Dependências
 */
//if(typeof IntegracaoModel == "undefined") {
//   $.getScript('/js/backbone/models/integracao.js');
//};

 
var FormIntegracao = Backbone.View.extend({
	
	events: {
	   "click #btn-integrar-sala":  "integrarSalas",
	   "click #btn-verificar-sala": "verificarSalas",
	   "click #btn-integrar-alunos": "integrarAlunos",
	   "click #btn-integrar-observador": "integrarObservador",
	   "click #btn-integrar-professores": "integrarProfessores"
	},
	initialize: function() { },
		
	tagName: 'div',

	render: function() {
		var that = this;
		$.ajaxSetup({async: false}); 
		$.get('/js/backbone/templates/form-integracao.html', { "_": $.now() }, function(template){
			that.$el.html(template);
		});
		
		this.$el.find('#id_sistema').change(function() {
			if($( this ).val()){
				that.$el.find('#linha-integracao').show();
			} else {
				that.$el.find('#linha-integracao').hide();
			} 
			if($( this ).val()==15){
				that.$el.find('#btn-verificar-sala').show();
			} else { 
				that.$el.find('#btn-verificar-sala').hide();
			};
		});
		
		$.ajaxSetup({async: true});
		return this;
	},
	integrarSalas: function(event){
		this.requisicao("/integracao/integrar-salas");
	},
	verificarSalas: function(event){
		this.requisicao("/integracao/verificar-salas");
	},
	integrarAlunos: function(event){
		this.requisicao("/integracao/integrar-alunos");
	},
	integrarObservador: function(event){
		this.requisicao("/integracao/integrar-observador");
	},
	integrarProfessores: function(event){
		this.requisicao("/integracao/integrar-professores");
	},
	requisicao: function(url){

		if(this.$el.find('#id_sistema').val()==''){
			$.pnotify({title:'Integração - Aviso' ,type: 'warning', text: "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Escolha o Sistema!"});
			return false;
		}
		
		var that = this;
		loading();
		$.get(url, { id_sistema: this.$el.find('#id_sistema').val(), bl_debug: this.$el.find('#bl_debug').val() }, function(html) {
			that.$el.find('#integracao-saida-container').html(html);
		}).fail(function() {
			$.pnotify({title:'Integração - Erro' ,type: 'error', text: "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ocorreu um erro na requisição!"});
		}).always(function() {
			loaded();
		});
		
	}
	/*renderTabs: function() {
	  
		var that = this;
		$.ajaxSetup({async: false}); 
		$.get('/js/backbone/templates/form-projeto-pedagogico.html', { "_": $.now() }, function(template){
			
			that.$el.html(template);
			if(that.id_projetopedagogico){
				that.forms[0].model.set({id_projetopedagogico : that.id_projetopedagogico});
				that.forms[1].model.set({id_projetopedagogico : that.id_projetopedagogico});
				// Cadastro das Disciplinas ao Projeto Pedagógico
				that.$el.find('#container-form-projeto-tab-disciplina').html(that.forms[1].render().el);
			}
			
			that.forms[0].on( "FormProjetoPedagogicoBasicoSaveEvent", function(Obj) {
				that.forms[1].model.set({id_projetopedagogico : Obj.get('id_projetopedagogico')});
				that.$el.find('#container-form-projeto-tab-disciplina').html(that.forms[1].render().el);
			});
			
			// Cadastro Básico do Projeto Pedagógico
			that.$el.find('#container-form-projeto-tab-basico').html(that.forms[0].render().el);
			
			var tab = that.$('li:first');
				tab.addClass('active');
				that.$('#container-' + tab.attr('id')).show();
				that.$('#container-' + tab.attr('id')).css({display: 'table'});
			
		});
	  	$.ajaxSetup({async: true});
	
	},
	
	switchTab: function(event) {
	  
		var tabAtiva = this.$('li.active');
			tabAtiva.removeClass('active');
			this.$('#container-' + tabAtiva.attr('id')).hide();
		  
		var selectedTab = event.currentTarget;
		this.$(selectedTab).addClass('active');
		this.$('#container-' + $(selectedTab).attr('id')).show();
		this.$('#container-' + $(selectedTab).attr('id')).css({display: 'table'});
	
	},*/

	
});


if(typeof ProjetoPedagogicoSerieNivelEnsinoModel == "undefined") {
   $.getScript('/js/backbone/models/projeto-pedagogico-serie-nivel-ensino.js');
};

if(typeof VwSerieNivelEnsinoModel == "undefined") {
   $.getScript('/js/backbone/models/vw-serie-nivel-ensino.js');
};

if(typeof NivelEnsinoModel == "undefined") {
   $.getScript('/js/backbone/models/nivel-ensino.js');
};

var VwSerieNivelEnsinoCollection = Backbone.Collection.extend({
    model: VwSerieNivelEnsinoModel,
	data : { id_nivelensino: '' },
    url: function(){ return "/api/vw-serie-nivel-ensino/?" + $.param(_.defaults(this.data)) }
});

/*
 *  Coleção dos Níveis de Ensino 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var NivelEnsinoFormCollection = Backbone.Collection.extend({
    model: NivelEnsinoModel,
    url: function(){ 
        return "/api/nivel-ensino/";
    },
	parse:function(data) {
	    var retorno = new Array();
            retorno[0] = new NivelEnsinoModel({ id_nivelensino: '', st_nivelensino: 'Selecione...'});
            $.each(data, function(i, item) {
                retorno[retorno.length] = item;
            });
        return retorno;
		
    }
});


var FormProjetoPedagogicoSerieNivelEnsinoModel = ProjetoPedagogicoSerieNivelEnsinoModel.extend({
	
	schema: {

        id_nivelensino:  { type: 'Select', title: 'Nível', editorAttrs: { class: 'span2' },options: new NivelEnsinoFormCollection(), validators: [{ type: 'required', message: 'Você precisa informar o Nível!' } ] 
        },
		
        id_serie:  { type: 'Checkboxes', title: 'Série', editorAttrs: { class: 'span2' }, options: {}, validators: [{ type: 'required', message: 'Você precisa informar a Série!' } ] 
        }
		
    }
	
});


var FormProjetoPedagogicoSerieNivelEnsino = Backbone.View.extend({
  initialize: function() {
	this.model          = new FormProjetoPedagogicoSerieNivelEnsinoModel();
  },
  render: function() {

	var that = this;
	
	if(this.model.get('id_serie')){
	  var 	vwSerieNivelEnsino = new VwSerieNivelEnsinoCollection();
	  		vwSerieNivelEnsino.data.id_nivelensino = this.model.get('id_nivelensino');
	  		this.model.schema.id_serie.options = vwSerieNivelEnsino;	
	};
	
	this.form = new Backbone.Form({
		template: 	_.template('<div class="span5" style="margin-left: 0px"><div data-editors="id_nivelensino" class="span2" style="margin-left: 0px"><label>Nível</label></div><div data-editors="id_serie" class="span2"><label>Série</label></div></div>'),
		model: 		this.model
	});
	  
	this.$el.html(this.form.render().el);
	

	this.form.on('id_nivelensino:change', function(form, titleEditor, extra) {
		  that.carregaSerie(form, titleEditor, extra);
	});  
	

	return this;
  },
  
  carregaSerie: function(form, titleEditor, extra){

	  var 	vwSerieNivelEnsino = new VwSerieNivelEnsinoCollection();
	  		vwSerieNivelEnsino.data.id_nivelensino = titleEditor.getValue();
	  		form.model.set( { id_nivelensino: titleEditor.getValue() });
	  		form.model.schema.id_serie.options = vwSerieNivelEnsino;
	  		
			this.render();
	  
	  },
  salvar: function(){
		
		var that = this;
		$.each(this.form.fields, function(index, value) {
//			value.editor.$el.css("border-color" , "#CCCCCC");
			value.editor.$el.removeAttr('style');
		});
		var validate  = this.form.validate();
		if(validate){
			$.each(validate, function(index, value) {
				that.$el.find("[name='"+index+"']").css("border-color" , "red");
				$.pnotify({title:'Aviso' ,type: 'warning', text: value.message});
			});
			return false;
		}
		//this.form.model.set({id_produto : id_produto} );
		
		var dados 					= this.form.getValue();
			dados.id_serie 			= dados.id_serie.map(customParseInt);
			dados.id_nivelensino 	= parseInt(dados.id_nivelensino);
			
		this.form.model.set(dados);

		if(!this.form.model.hasChanged('id_serie') && !this.form.model.hasChanged('id_nivelensino')){
			return false;
		}

		this.form.model.save(null, {
			success: function(model, response) {

				response.title 	= 'Escolaridade - Sucesso';
				response.text 	= 'Operação executada com sucesso!';
				response.type 	= 'success';
				$.pnotify(response);//mensagem de retorno

			},
			error: function(model, response) {
				$.pnotify({title: "Escolaridade", text: response.mensagem, type: 'error'});
			}
		});
	 return false;
 }
});
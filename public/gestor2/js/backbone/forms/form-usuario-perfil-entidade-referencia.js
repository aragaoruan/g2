if(typeof UsuarioPerfilEntidadeReferenciaModel == "undefined") {
   $.getScript('/js/backbone/models/usuario-perfil-entidade-referencia.js');
};

if(typeof VwUsuarioPerfilEntidadeReferenciaModel == "undefined") {
   $.getScript('/js/backbone/models/vw-usuario-perfil-entidade-referencia.js');
};

if(typeof VwUsuarioPerfilEntidadeModel == "undefined") {
   $.getScript('/js/backbone/models/vw-usuario-perfil-entidade.js');
};


var VwUsuarioPerfilEntidadeCollection = Backbone.Collection.extend({
    model: VwUsuarioPerfilEntidadeModel,
    url: function(){ 
        return "/api/vw-usuario-perfil-entidade/?"   + $.param(_.defaults(this.data));
    },
    parse: function(data) {
        var model = new VwUsuarioPerfilEntidadeReferenciaModel();
        model.set(model.defaults);
        model.set('st_nomecompleto', 'Selecione...');
        data.unshift(model.attributes);
        return data;
    },
    data: {
        id_perfilpedagogico : 2,
		id_situacaoperfil : 15
    }
});

var FormUsuarioPerfilEntidadeReferenciaModel = UsuarioPerfilEntidadeReferenciaModel.extend({

	schema: {
        id_usuario:  { 
            type: 'Select', 
            title: 'Nome', 
            editorAttrs: { class: 'span4' },
            options: new VwUsuarioPerfilEntidadeCollection(),
            validators: [{ type: 'required', message: 'Você precisa informar a Situação!' } ] 
        },
		nu_porcentagem: {
            type: 'Number', 
            title: 'Porcentagem', 
			editorAttrs: { class: 'span1', min: 0, max: 100, maxlength: 5, max: 99999, 'oninput': 'maxLengthCheck(this)'  },
			validators: [
					function checkValue(value, formValues) {
						var err = {
							type: 'Number',
							message: 'A porcentagem não pode ser menor que 0(zero) e nem maior que 100'
						};
						if(formValues.nu_porcentagem < 0 || formValues.nu_porcentagem > 100) return err;

					}
				]
			},
		bl_titular: {
        	type: 'Radio', options: [ { val: true, label: 'Titular' } ],
        	title: 'Titular', 
        	editorAttrs: { class: 'span2' }
			}
    }
});


var FormUsuarioPerfilEntidadeReferencia = Backbone.View.extend({
  
  initialize: function() {
	this.model          = new FormUsuarioPerfilEntidadeReferenciaModel();
  },
  events: {
	  "click button#form-projeto-pedagogico-coordenadores-btn-excluir": "excluir"

  },
  parentIndex: null,
  render: function() {
	  
	$.ajaxSetup({async: false});
	
	var that = this;

	this.form = new Backbone.Form({
		template: 	_.template('<div class="row" style="margin-top: 5px;"><div class="span2"><button type="button" class="btn" id="form-projeto-pedagogico-coordenadores-btn-excluir">Excluir</button></div>'
        +'<div class="span5" data-editors="id_usuario"></div>'
        +'<div class="span2" data-editors="nu_porcentagem"></div>'
        +'<div class="span2 correcaoulli" data-editors="bl_titular"></div></div>'),
		model: 		this.model
	});
	  
	this.$el.html(this.form.render().el);
    var collectionModels = this.model.schema.id_usuario.options.models;

    for (var i in collectionModels){
        var obj = collectionModels[i];
        if (obj.attributes.id_perfil){
            this.model.set('id_perfil', obj.get('id_perfil'));
            break;
        }
    }
      
	$.ajaxSetup({async: true});
	return this;
  },
  
  salvar: function(){

		var that = this;

		var validate  = this.form.validate();
		if(validate){
			$.each(validate, function(index, value) {
				that.$el.find("[name='"+index+"']").css("border-color" , "red");
				$.pnotify({title:'Coordenador - Aviso' ,type: 'warning', text: value.message});
			});
			return false;
		}

		this.form.model.set(this.form.getValue());
		
		this.form.model.save(null, {
			success: function(model, response) {
				/*if(response.tipo===1){
					model.id = response.id;
					model.fetch();
				} 
*/
                //Alerta substituido por um mais claro.

				//response.title 	= 'Coordenador - Sucesso';
				//response.text 	= 'Operação executada com sucesso!';
				//response.type 	= 'success';
				//$.pnotify(response);//mensagem de retorno

			},
			error: function(model, response) {
				$.pnotify($.parseJSON(response.responseText));
			}
		});
	 return false;

 },
 excluir: function(){
	 
	 var that = this;
	 
	 
	bootbox.confirm("Deseja realmente excluir este Coordenador?", function(result) {
	  if(result){
		// box confirm
		  
		loading();
		if(typeof that.form.model.get('id_perfilreferencia') == "undefined" || that.form.model.get('id_perfilreferencia')==false){
			that.$el.remove();
			loaded();
			that.trigger("FormUsuarioPerfilEntidadeReferenciaDeleteEvent", that );
		} else {

			that.form.model.destroy({
				success: function(model, response) {
					that.$el.remove();
					response.title 	= 'Coordenador - Sucesso';
					response.text 	= 'Registro excluído com sucesso!';
					response.type 	= 'success';
					$.pnotify(response);//mensagem de retorno
					loaded();
					that.trigger("FormUsuarioPerfilEntidadeReferenciaDeleteEvent", that );
				},
				error: function(model, response) {
					$.pnotify({title: "Coordenador - Erro", text: "Ocorreu um erro na requisição", type: 'error'});
					loaded();
				}
			});

		}
		
		// box confirm
	  }
	});

 }
});
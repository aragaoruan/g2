/**
 * BACKBONE-FORMS
 * URL: https://github.com/powmedia/backbone-forms
 * Autor: Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * Data: 14/11/2013
 * Formulário para cadastro dos Valores dos Produtos
 */

/**
 * Dependências
 */
if (typeof VwAreaProjetoNivelEnsinoModel == "undefined") {
    console.log('A Model VwAreaProjetoNivelEnsinoModel é Obrigatória!');
}
;
if (typeof AreaConhecimentoModel == "undefined") {
    console.log('A Model AreaConhecimentoModel é Obrigatória!');
}
;
if (typeof NivelEnsinoModel == "undefined") {
    console.log('A Model NivelEnsinoModel é Obrigatória!');
}
;


/*
 *  Coleção das Areas de Conhecimento 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var AreaConhecimentoFormCollection = Backbone.Collection.extend({
    model: AreaConhecimentoModel,
    url: function () {
        return "/api/area-conhecimento/";
    },
    parse: function (data) {
        var retorno = new Array();
        retorno[0] = new AreaConhecimentoModel({id_areaconhecimento: '', st_areaconhecimento: 'Todas'});
        $.each(data, function (i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;

    }
});

/*
 *  Coleção dos Níveis de Ensino 
 *  Alterando o parse para tratar o resultado do Mensageiro
 */
var NivelEnsinoFormCollection = Backbone.Collection.extend({
    model: NivelEnsinoModel,
    url: function () {
        return "/api/nivel-ensino/";
    },
    parse: function (data) {
        var retorno = new Array();
        retorno[0] = new NivelEnsinoModel({id_nivelensino: '', st_nivelensino: 'Todos'});
        $.each(data, function (i, item) {
            retorno[retorno.length] = item;
        });
        return retorno;

    }
});


var FormVwAreaProjetoNivelEnsinoModel = VwAreaProjetoNivelEnsinoModel.extend({
    schema: {
        st_projetopedagogico: {
            type: 'Text',
            title: 'Título',
            validators: [
                {
                    type: 'required',
                    message: 'Você precisa informar o Título do Projeto Pedagógico!'
                }
            ],
            editorAttrs: {class: 'span12', placeholder: 'Título'}
        },
        id_areaconhecimento: {
            type: 'Select',
            title: 'Área',
            editorAttrs: {class: 'span12'},
            options: new AreaConhecimentoFormCollection()
        },
        id_nivelensino: {
            type: 'Select',
            title: 'Nível',
            editorAttrs: {class: 'span12'},
            options: new NivelEnsinoFormCollection()
        }

    }

});

/**
 * Formulário do Produto Valor
 * @type @exp;Backbone@pro;View@call;extend
 */
var FormProjetoPedagogicoBusca = Backbone.View.extend({
    events: {
        "click button#btn-pesquisar-projeto-pedagogico": "pesquisar",
        "keyup input[name='st_projetopedagogico']": 'callPesquisa',
        "keyup select[name='id_areaconhecimento']": 'callPesquisa',
        "keyup select[name='id_nivelensino']": 'callPesquisa'
    },
    callPesquisa: function (e) {
        if (e.keyCode === 13)
            this.pesquisar();
        return false;
    },
    initialize: function () {
        this.model = new FormVwAreaProjetoNivelEnsinoModel();
    },
    render: function () {

        var that = this;

        $.get('/js/backbone/templates/form-projeto-pedagogico-busca.html', {"_": $.now()}, function (template) {

            that.form = new Backbone.Form({
                template: _.template(template),
                model: that.model
            });

            that.$el.append(that.form.render().el);

        });


        return this;
    },
    pesquisar: function () {

        var that = this;

        $.each(this.form.fields, function (index, value) {
//			value.editor.$el.css("border-color" , "#CCCCCC");
            value.editor.$el.removeAttr('style');
        });
        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function (index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        loading();

        var table = that.$el.find('table#form-projeto-pedagogico-busca-grid');

        var tbody = that.$el.find('table#form-projeto-pedagogico-busca-grid tbody');
        tbody.empty();

        $.ajax('/vw-area-projeto-nivel-ensino/pesquisar', {
            data: that.form.getValue(),
            dataType: 'json'
        }).done(function (response) {
            if (response.tipo === 1) {

                table.show();
                //Percorre a mensagem e preenche a tabela com os dados retornados
                $.each(response.mensagem, function (index, value) {
                    tbody.append('<tr>' +
                    '<td class="text-center"><i data-id_projetopedagogico="' + value.id_projetopedagogico + '" data-st_projetopedagogico="' + value.st_projetopedagogico + '" class="icon-plus"></i></td>'
                    + '<td>' + value.st_projetopedagogico + '</td>'
                    + '<td>' + value.st_nivelensino + '</td>'
                    + '<td>' + value.st_situacao + '</td></tr>');
                });

                /**
                 * Evento responsável por pegar os dados da célula em que foi clicada e dispara o evento para
                 * o form-produto-projeto-pedagógico.js
                 */
                tbody.find('.icon-plus').click(function () {
                    that.form.trigger("FormProjetoPedagogicoBuscaEvent", $(this).data());
                }).css({'cursor': 'pointer'});
            } else {
                table.hide();
            }
            response.title = 'Projetos - ' + response.title;
            loaded();
            $.pnotify(response);//mensagem de retorno

        });


        //this.form.trigger("FormProjetoPedagogicoEvent", this.form.getValue() );

//		return this.form.getValue();
//		console.log(this.form.getValue());


        return false;
    }
});
if (typeof ModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/modulo-disciplina.js');
}

if (typeof ModuloModel == "undefined") {
    $.getScript('/js/backbone/models/modulo.js');
}

if (typeof VwModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-modulo-disciplina.js');
}


var FormProjetoModuloDisciplinaPonderacaoModel = ModuloDisciplinaModel.extend({
    schema: {
        nu_ponderacaoaplicada: {
            type: 'Text',
            title: 'Pond. Aplicada',
            editorAttrs: { class: 'span1 desabilitactrlv', min: 0, max: 100, maxlength: 5 }
        }
    }
});


var FormProjetoModuloDisciplinaPonderacao = Backbone.View.extend({

        initialize: function () {
            this.model = new FormProjetoModuloDisciplinaPonderacaoModel();
            this.vw = new VwModuloDisciplinaModel();
        },
        parentIndex: null,
        render: function () {

            //$.ajaxSetup({async: false});

            this.form = new Backbone.Form({
                template: _.template(
                    '<div class="row" style="margin-top: 5px;">'
                    + '<div class="span6" style="text-align:left">' + this.vw.get('st_disciplina') + '</div>'
                    + '<div class="span2">' + this.vw.get('nu_ponderacaocalculada') + '%</div>'
                    + '<div class="input-append span2" style="margin-left:0;">'
                    + '<div style="margin-left:0;" class="span1" data-editors="nu_ponderacaoaplicada"></div>'
                    + '<span class="add-on">%</span>'
                    + '</div>'
                    + '</div>'),
                model: this.model
            });


            this.$el.html(this.form.render().el);

//$.ajaxSetup({async: true});
            return this;
        },

        salvar: function () {

            var that = this;

            $.each(this.form.fields, function (index, value) {
//			value.editor.$el.css("border-color" , "#CCCCCC");
                value.editor.$el.removeAttr('style');
            });
            var validate = this.form.validate();
            if (validate) {
                $.each(validate, function (index, value) {
                    that.$el.find("[name='" + index + "']").css("border-color", "red");
                    $.pnotify({title: 'Disciplina - Aviso', type: 'warning', text: value.message});
                });
                return false;
            }

            this.form.model.set(this.form.getValue());

            this.form.model.save(null, {
                success: function (model, response) {

                    response.title = 'Ponderação - Sucesso';
                    response.text = 'Operação executada com sucesso!';
                    response.type = 'success';
                    $.pnotify(response);//mensagem de retorno

                },
                error: function (model, response) {
                    $.pnotify($.parseJSON(response.responseText));
                }
            });

            return false;

        }
    })
    ;
if (typeof ModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/modulo-disciplina.js');
}
;

if (typeof ModuloModel == "undefined") {
    $.getScript('/js/backbone/models/modulo.js');
}
;

if (typeof VwModuloDisciplinaModel == "undefined") {
    $.getScript('/js/backbone/models/vw-modulo-disciplina.js');
}
;


var FormProjetoModuloDisciplinaModel = ModuloDisciplinaModel.extend({
    schema: {
        nu_importancia: {
            type: 'Select',
            title: 'Importância',
            editorAttrs: {class: 'span1'},
            options: [{val: 1, label: 1}, {val: 2, label: 2}, {val: 3, label: 3}]
        },
        nu_repeticao: {
            type: 'Text',
            title: 'Repetição',
            editorAttrs: {class: 'span1 disabled', min: 0, max: 3, maxlength: 3, disabled: 'disabled'}
        },
        bl_obrigatoria: {
            type: 'Checkbox', options: [{val: true, label: 'Obg.'}],
            title: 'Obrigatória',
            editorAttrs: {class: 'span1'}
        },
        nu_cargahoraria: {
            type: 'Number',
            title: 'Carga Horária',
            editorAttrs: {class: 'span1', maxlength: 3}
        }
    }
});


var FormProjetoModuloDisciplina = Backbone.View.extend({
    initialize: function () {
        this.model = new FormProjetoModuloDisciplinaModel();
        this.vw = new VwModuloDisciplinaModel();
    },
    events: {
        "click button.form-projeto-modulo-disciplina-btn-excluir": "excluir",

        //Esse change foi retirado a pedido da issue COM-11497. Caso necessite voltar essa validação
        // apenas descomente o código.

        //'change input[name="nu_cargahoraria"]': 'validaCargaHoraria'
    },
    validaCargaHoraria: function () {
        var valorCh = this.$el.find('input[name="nu_cargahoraria"]').val();
        if (parseInt(valorCh) > parseInt(this.vw.get('nu_cargahorariad'))) {
            $.pnotify({
                type: 'warning',
                title: 'Atenção!',
                text: 'O  valor informado para carga horária da disciplina ' + this.vw.get('st_disciplina') + ' não pode ser maior que o valor definido (' + this.vw.get('nu_cargahorariad') + 'h)'
            });
            this.$el.find('input[name="nu_cargahoraria"]').val(this.vw.get('nu_cargahorariad')).focus();
            return false;
        } else {
            return true;
        }
    },
    parentIndex: null,
    render: function () {

        //$.ajaxSetup({async: false});

        var that = this;
        //<div class="row" style="margin-top: 5px;"><div class="span1"><button type="button" class="btn form-projeto-modulo-disciplina-btn-excluir">Excluir</button></div>
        this.form = new Backbone.Form({
            template: _.template('<div class="row" style="margin-top: 5px;"><div class="span1 text-center"><input type="checkbox" class="btn form-projeto-modulo-disciplina-ck-excluir" name="modulo-disciplina-ck-excluir" alt="Excluir"></div>'
            + '<div class="span3" style="text-align:left">' + this.vw.get('st_disciplina') + '</div>'
            + '<div class="span1 text-right">' + this.vw.get('nu_cargahorariad') + ' h </div>'
            + '<div class="span1" data-editors="nu_cargahoraria"></div>'
            + '<div class="span1" data-editors="bl_obrigatoria"></div>'
            + '<div class="span1">' + this.vw.get('nu_repeticao') + '</div>'
            + '<div class="span1" data-editors="nu_importancia"></div></div>'),
            model: this.model
        });

        this.$el.html(this.form.render().el);

        //$.ajaxSetup({async: true});
        return this;
    },
    salvar: function () {
        var that = this;

        //Validação retirada a pedido da ISSUE COM-11497
        ////valida o valor da carga horaria
        //if (!that.validaCargaHoraria()) {
        //    return false;
        //}

        $.each(this.form.fields, function (index, value) {
//			value.editor.$el.css("border-color" , "#CCCCCC");
            value.editor.$el.removeAttr('style');
        });
        var validate = this.form.validate();
        if (validate) {
            $.each(validate, function (index, value) {
                that.$el.find("[name='" + index + "']").css("border-color", "red");
                $.pnotify({title: 'Disciplina - Aviso', type: 'warning', text: value.message});
            });
            return false;
        }

        this.form.model.set(this.form.getValue());

        this.form.model.save(null, {
            success: function (model, response) {

                response.title = 'Disciplina - Sucesso';
                response.text = 'Operação executada com sucesso!';
                response.type = 'success';
                $.pnotify(response);//mensagem de retorno

            },
            error: function (model, response) {
                $.pnotify($.parseJSON(response.responseText));
            }
        });

        return false;

    },
    excluir: function () {
        var that = this;
        bootbox.confirm("Deseja realmente excluir esta Disciplina?", function (result) {
            if (result) {
                // box confirm
                loading();
                if (typeof that.form.model.get('id_modulodisciplina') == "undefined" || that.form.model.get('id_modulodisciplina') == false) {
                    that.$el.remove();
                    loaded();
                    that.trigger("FormProjetoModuloDisciplinaDeleteEvent", that);
                } else {

                    that.form.model.destroy({
                        success: function (model, response) {
                            that.$el.remove();
                            response.title = 'Disciplina - Sucesso';
                            response.text = 'Registro excluído com sucesso!';
                            response.type = 'success';
                            $.pnotify(response);//mensagem de retorno
                            loaded();
                            that.trigger("FormProjetoModuloDisciplinaDeleteEvent", that);
                            that.trigger("FormProjetoModuloDisciplinaPonderacaoDeleteEvent", that);
                        },
                        error: function (model, response) {
                            $.pnotify($.parseJSON(response.responseText));
                            loaded();
                        },
                        complete: function () {
//                            that.trigger("FormProjetoPedagogicoDisciplinaChangeEvent", that);
                        }
                    });

                }

                // box confirm
            }
        });

    }
});
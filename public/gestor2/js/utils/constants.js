/**
 * Textos sistemas fixo
 */
var TEXTO_SISTEMA_FIXO = {
    HISTORICO_ESCOLAR_DEFINITIVO: 1487,
    DIPLOMA_GRADUACAO: 1488
};

/**
 * tb_linhadenegocio
 */
var LINHA_DE_NEGOCIO = {
    CONCURSO: 1,
    GRADUACAO: 2,
    POS_GRADUACAO: 3
};

/**
 * tb_situacao
 */
var SITUACAO = {
    TB_AVALIACAOAGENDAMENTO: {
        AGENDADO: 68,
        REAGENDADO: 69,
        CANCELADO: 70,
        LIBERADO: 129,
        ABONADO: 130
    },
    TB_MATRICULA: {
        AGENDAMENTO_NAO_APTO: 119,
        AGENDAMENTO_APTO: 120
    },
    TB_OCORRENCIA: {
        PENDENTE: 97,
        EM_ANDAMENTO: 98,
        ENCERRADA: 99,
        AGUARDANDO_ENCERRAMENTO: 112,
        MOTIVO_OCORRENCIA: 2,
    },
    TB_PRODUTO: {
        ATIVO: 45,
        INDISPONIVEL: 100
    },
    TB_VENDA: {
        ATIVO: 39,
        INATIVO: 40,
        PENDENTE: 46
    },
    TB_TURMA: {
        ATIVA: 83,
        INATIVA: 84
    },
    TB_ATIVIDADECOMPLEMENTAR: {
        DEFERIDO: 200,
        INDEFERIDO: 201,
        EM_ANALISE: 202,
        INATIVA: 84
    },
    TB_ALOCACAO: {
        ATIVA: 55,
        INATIVA: 56,
        NAO_APTO: 121,
        APTO: 122,
        PENDENTE: 203,
        EM_ANALISE: 204,
        DEVOLVIDO_ALUNO: 205,
        APROVADO: 206,
        DEVOLVIDO_TUTOR: 207
    },
    TB_ENTIDADEINTEGRACAO: {
        ATIVA: 198,
        INATIVA: 199
    }
};

/**
 * tb_evolucao
 */
var EVOLUCAO = {
    TB_MATRICULA: {
        AGUARDANDO_CONFIRMACAO: 5,
        CURSANDO: 6,
        CONCLUINTE: 15,
        CERTIFICADO: 16,
        TRANSFERIDO: 20,
        TRANCADO: 21,
        SEM_RENOVACAO: 25,
        RETORNADO: 26,
        CANCELADO: 27,
        BLOQUEADO: 40,
        ANULADO: 41,
        TRANSFERIDO_DE_ENTIDADE: 47,
        DECURSO_DE_PRAZO: 69,
        FORMADO: 81,
        DIPLOMADO: 82,
        DIPLOMACAO: {
            APTO_A_DIPLOMAR: 71,
            DIPLOMA_GERADO: 72,
            ENVIADO_ASSINATURA: 73,
            RETORNADO_ASSINATURA: 74,
            ENVIADO_SECRETARIA: 75,
            ENVIADO_REGISTRO: 76,
            RETORNADO_REGISTRO: 77,
            ENVIADO_POLO: 78,
            ENTREGUE_ALUNO: 79,
            HISTORICO_GERADO: 80,
            EQUIVALENCIA_GERADO: 83,
            SEGUNDA_VIA_DIPLOMA_GERADO: 84,
            SEGUNDA_VIA_HISTORICO_GERADO: 85,
            SEGUNDA_VIA_EQUIVALENCIA_GERADO: 86
        }
    },
    TB_OCORRENCIA: {
        AGUARDANDO_ATENDIMENTO: 28,
        SENDO_ATENDIDO: 29,
        AGUARDANDO_INTERESSADO: 30,
        AGUARDANDO_ATENDENTE: 31,
        INTERESSADO_INTERAGIU: 32,
        AGUARDANDO_DOCUMENTACAO: 33,
        AGUARDANDO_PAGAMENTO: 34,
        AGUARDANDO_OUTROS_SETORES: 35,
        ENCERRADO_PELO_INTERESSADO: 36,
        REABERTO_PELO_INTERESSADO: 37,
        ENCERRADO_PELO_ATENDENTE: 38,
        DEVOLVIDO_PARA_DISTRIBUICAO: 39
    },
    TB_VENDA: {
        AGUARDANDO_NEGOCIACAO: 7,
        CANCELADA: 8,
        AGUARDANDO_RECEBIMENTO: 9,
        CONFIRMADA: 10,
        NEGOCIADA: 17,
        EM_ANDAMENTO: 18,
        EM_PROCESSAMENTO: 44
    },
    TB_MATRICULA_DISCIPLINA: {
        NAO_ALOCADO: 11,
        APROVADO: 12,
        CURSANDO: 13,
        INSATISFATORIO: 19,
        TRANCADA: 70
    }
};

/**
 * tb_textocategoria
 */
var TEXTO_CATEGORIA = {
    MATRICULA: 1,
    CONTRATO: 2,
    DECLARACOES: 3,
    CERTIFICADO: 4,
    DADOS_PESSOAIS: 5,
    VENDA: 6,
    OCORRENCIAS: 7,
    ASSUNTO: 8,
    PRIMEIRO_ACESSO: 9,
    AGENDAMENTO: 10,
    ENTIDADE_LOJA: 11,
    PLANO_DE_PAGAMENTO: 12,
    RESPOSTA_CARTAO_CREDITO: 14,
    CABECALHO: 15,
    RODAPE: 16
};

/**
 * tb_sistema
 */
var SISTEMAS = {
    GESTOR: 1,
    ACTOR: 2,
    FLUXUS: 3,
    PORTAL_ALUNO: 4,
    SISTEMA_AVALIACAO: 5,
    MOODLE: 6,
    BRASPAG: 7,
    LOCAWEB: 8,
    WORDPRESS_ECOMMERCE: 9,
    BRASPAG_RECORRENTE: 10,
    AMAIS: 11,
    HENRY7: 12,
    LEYA_BOOKSTORE: 13,
    BLACKBOARD: 15,
    BRASPAG_LANCAMENTO: 19,
    PONTOSOFT: 20,
    HUBSPOT: 22,
    PEARSON: 23,
    FLUXUS_WS: 24,
    FACEBOOK: 26,
    GOOGLE: 27,
    LOCASMS: 25,
    JIRA: 28,
    ALUMNUS: 29,
    PAGARME: 30
};

/**
 * tb_permissao
 */
var PERMISSAO = {
    // Geral
    ADICIONAR: 1,
    DELETAR: 2,
    ATUALIZAR: 3,

    // Alocação
    ALOCAR_ALUNOS_SALA_DE_AULA: 8,
    INTEGRACAO_DE_ALUNOS: 9,
    PROGRAMA_DE_RECUPERACAO_DE_ALUNOS: 10,
    ALOCAR_EM_SALAS_ENCERRADAS: 42,

    // Diplomação
    DIPLOMACAO_EDITAR_DATA_ENVIO_POLO: 44,
    DIPLOMACAO_EDITAR_DATA_ENTREGUE_ALUNO: 45,
    GERAR_VENDA: 53,
    DIPLOMACAO_EDITAR_REGISTRO_DIPLOMA: 50,
    VINCULAR_OCORRENCIAS: 54,

    // Tela de Matrículas
    EXIBIR_BOTAO_DE_HISTORICO: 59,

    // Transferência de Polo
    TRANSFERIR_ALUNO_INADIMPLENTE: 56
};

/**
 * tb_meiopagamento
 */
var MEIO_PAGAMENTO = {
    CARTAO_CREDITO: 1,
    BOLETO_BANCARIO: 2,
    DINHEIRO: 3,
    CHEQUE: 4,
    EMPENHO: 5,
    DEPOSITO_BANCARIO: 6,
    CARTAO_DEBITO: 7,
    TRANSFERENCIA: 8,
    PAGAMENTO_EM_DINHEIRO: 9
};

/**
 * tb_tipodesconto
 */
var TIPO_DESCONTO = {
    VALOR: 1,
    PORCENTAGEM: 2
};

/**
 * tb_tipoavaliacao
 */
var TIPO_AVALIACAO = {
    ATIVIDADE_PRESENCIAL: 1,
    AVALIACAO_RECUPERACAO: 2,
    AVALIACAO_SIMULADA: 3,
    AVALIACAO_PRESENCIAL: 4,
    ATIVIDADE_EAD: 5,
    TCC: 6,
    CONCEITUAL: 7
};

/**
 * tb_notaconceitual
 */
var NOTA_CONCEITUAL = {
    NAO_CONCLUIDO: 1,
    CONCLUIDO: 2
};

/**
 * tb_tipotramite
 */
var TIPO_TRAMITE = {
    AGENDAMENTO: {
        SITUACAO: 14,
        LANCAMENTO_FREQUENCIA: 23
    },
    MATERIAL: {
        ENTREGA: 4
    },
    MATRICULA: {
        SITUACAO: 1,
        EVOLUCAO: 2,
        APROVEITAMENTO_DISCIPLINA: 3,
        TRANSFERENCIA: 11,
        TCC: 15,
        DOCUMENTACAO: 16,
        CARTEIRINHA: 24,
        TRANCAMENTO_DISCIPLINA: 28,
        TRANSFERENCIA_TURMA: 29,
        COLACAO_GRAU: 30
    },
    OCORRENCIA: {
        AUTOMATICA: 12
    },
    VENDA: {
        SITUACAO: 5,
        EVOLUCAO: 6
    }
};

/**
 * @todo Verificar uso desta constante, modificar para usar a constante TIPO_TRAMITE, e então remover a constante TRAMITE
 * tb_tipotramite simplificada
 */
var TRAMITE = {
    MATRICULA_COLACAO_GRAU: TIPO_TRAMITE.MATRICULA.COLACAO_GRAU
};


/**
 * tb_perfilpedagogico
 */
var PERFIL_PEDAGOGICO = {
    PROFESSOR: 1,
    COORDENADOR_PROJETO: 2,
    OBSERVADOR_INSTITUCIONAL: 3,
    COORDENADOR_DISCIPLINA: 4,
    ALUNO: 5,
    ALUNO_INSTITUCIONAL: 6,
    SUPORTE: 7,
    PROFESSOR_AUTOR_LIVRO: 8,
    TITULAR_CERTIFICACAO: 9
};

/**
 * tb_categoriatramite
 */
var CATEGORIA_TRAMITE = {
    MATRICULA: 1,
    MATERIAL: 2,
    VENDA: 3,
    OCORRENCIA: 4,
    AGENDAMENTO: 5,
    SALA_DE_AULA: 6
};

/**
 * tb_tipoDisciplina
 */
var TIPO_DISCIPLINA = {
    PADRAO: 1,
    TCC: 2,
    AMBIENTACAO: 3
};
/**
 * tb_disciplina
 */
var TB_DISCIPLINA_ATIVA = {
    ATIVO: 1,
    INATIVO: 0
};

/**
 * Constantes para definir o tipo de prova na tela de graduação.
 * @type {{PRESENCIAL: 1, FINAL: 2}}
 */

var TIPO_PROVA_GRADUACAO = {
    PRESENCIAL: 1,
    FINAL: 2 //Recuperação
};

var FUNCIONALIDADE = {
    TRANSFERENCIA_CURSO: 472,
    CANCELAMENTO_POS: 816,
    OCORRENCIA: 429,
    MATRICULAS: 275,
    TRANSFERENCIA_POLO: 831
};

/**
 * tb_tipoproduto
 */
var TIPO_PRODUTO = {
    PROJETO_PEDAGOGICO: 1,
    TAXA: 2,
    DECLARACOES: 3,
    MATERIAL: 4,
    AVALIACAO: 5,
    LIVRO: 6,
    COMBO: 7,
    PRR: 8,
    SIMULADO: 9,
    VIDEO: 10
};

/**
 * tb_modelovenda
 */
var MODELO_VENDA = {
    PADRAO: 1,
    ASSINATURA: 2,
    CREDITO: 3
};

/**
 * tb_tipoprodutovalor
 */
var TIPO_PRODUTO_VALOR = {
    NORMAL: 4,
    ENTRADA_E_MENSAL: 5,
    ENTRADA_E_MENSAL_PROPORCIONAL: 6,
    PROMOCIONAL: 7
};

/**
 * tb_esquemaconfiguracao
 */
var ESQUEMA_CONFIGURACAO = {
    GRADUACAO_UNYLEYA: 1,
    POS_GRADUACAO: 2,
    IMP: 3,
    TDC: 4,
    DEFAULT: 5,
    POS_PRESENCIAL: 6,
    POS_GRADUACAO_UCAM: 7,
    CORPORATIVO: 8,
    ESCOLA_TECNICA: 9
};

/**
 * tb_origemareaconhecimento
 */

var ORIGEM_AREA_CONHECIMENTO = {
    CAPES: 1,
    MEC: 2
};

/**
 * tb_modelocarteirinha
 */

var MODELO_CARTEIRINHA = {
    PLATINUM: 1,
    OUTROS: 2,
    GRADUACAO: 3
};

var TIPO_OCORRENCIA = {
    ALUNO: 1,
    TUTOR: 2,
    INTERNO: 3
}
/**
 * Tipo de arquivo
 */
var EXTENSAO_ARQUIVO = {
    PDF: 1,
    ZIP: 2
};
var VwOcorrenciaAguardandoCollectionExtend = Backbone.Collection.extend({
    model: Backbone.Model.extend(),
    url: '/ocorrencia/find-ocorrencias-pendentes'
});

function loadOcorrenciasPendentes() {

    var VwOcorrenciaAguardandoCollection = new VwOcorrenciaAguardandoCollectionExtend();

    VwOcorrenciaAguardandoCollection.fetch({
        success: function (collection) {

            if (collection.length) {

                var ocorrenciaAguardandoLayoutView = new OcorrenciaAguardandoLayoutView({
                    collection: collection
                });

                G2S.layout.menu.$el.hide();
                G2S.layout.ocorrencia.show(ocorrenciaAguardandoLayoutView);

            }
        }
    });

}


var OcorrenciaAguardandoLayoutView = Marionette.LayoutView.extend({
    template: '#modal-ocorrencia',
    regions: {
        ocorrenciaPendente: '#ocorrencia-pendente',
        ocorrenciaEncerradas: '#ocorrencia-encerradas',
    },
    events: {
        "click #btn-ir-para-ocorrencia": "eventAbrirOcorrenciaSelecionada",
        "click #btn-confirmar-ciencia": "eventConfirmarCiencia"
    },
    initialize(option) {

        var encerrada = option.collection.filter(function (collection) {
            return collection.get('id_situacao') === 99;
        });

        this.encerrada = encerrada;

        var pendente = option.collection.filter(function (collection) {
            return collection.get('id_situacao') !== 99;
        });
        this.pendente = pendente;
        this.collection = option.collection;
        this.model = new Backbone.Model(this.collection);

    },
    onRender: function () {

        if (this.pendente.length) {
            this.getRegion('ocorrenciaPendente')
                .show(new ocorrenciaPendenteCompositeView({
                    collection: new Backbone.Collection(this.pendente)
                }));
        }

        if (this.encerrada.length) {
            this.getRegion('ocorrenciaEncerradas')
                .show(new ocorrenciaEncerradaCompositeView({
                    collection: new Backbone.Collection(this.encerrada)
                }));
        }

        this.$el.find('#ocorrencias-pendentes-modal').modal({show: true, backdrop: 'static', keyboard: false});
        return this;
    },

    eventAbrirOcorrenciaSelecionada: function () {
        if (G2S.editModel) {
            G2S.router.navigate('#/ocorrencia/' + G2S.editModel.id_ocorrencia, {trigger: true});
            this.$el.find('#ocorrencias-pendentes-modal').modal('hide');
        } else {
            this.$el.find('.modal-body').prepend('<div class="alert alert-warning">Nenhuma ocorrencia selecionada</div>');
        }
    },
    eventConfirmarCiencia: function () {
        that = this;
        var data = [];

        this.collection.each(function (model) {
            data.push(model.attributes);
        });

        loading();
        $.ajax({
            url: '/ocorrencia/confirmar-ciencia-ocorrencia-pendente',
            type: 'post',
            dataType: 'json',
            data: {data: data},
            success: function () {
                G2S.layout.menu.$el.show();
                that.$el.find('#btn-ir-para-ocorrencia').show();
                that.$el.find('#btn-fechar').show();
                that.$el.find('#btn-confirmar-ciencia').hide();
                that.$el.find('.modal-body')
                    .prepend('<div class="alert alert-success">Atendente  confirmou de ciência</div>');
                loaded();
            },
            error: function () {
                that.$el.find('.modal-body')
                    .prepend('<div class="alert alert-danger">Erro ao tentar salvar confirmação de ciência</div>');
            }
        });
    }

});

var ocorrenciaEncerradaItemView = Marionette.ItemView.extend({
    template: '#template-row-ocorrencia-encerrada',
    tagName: 'tr',
    onShow: function () {
        return this;
    }
});

var ocorrenciaPendenteItemView = Marionette.ItemView.extend({
    template: '#template-row-ocorrencia-pendente',
    tagName: 'tr',
    onShow: function () {
        this.$el.css({'cursor': 'pointer'});
        this.$el.attr('title', 'Clique para ver mais detalhes');
        return this;
    },
    events: {
        "mouseover": function () {
            this.$el.tooltip('show');
        },
        "click ": function () {
            this.$el.parent().find('tr').removeClass('alert').removeClass('alert-success');
            this.$el.parents('.div-aviso-ocorrencias').css('height', '200px');
            this.$el.addClass('alert alert-success');
            $('#details-ocorrencia').show();
            $('#btn-ir-para-ocorrencia').removeAttr('disabled');

            G2S.editModel = this.model.toJSON();
            G2S.layout.addRegions({previewOcorrencia: '#details-ocorrencia'});
            G2S.layout.previewOcorrencia.show(new DetailOcorrenciaItemView({
                model: this.model
            }));
        }
    }
});

var DetailOcorrenciaItemView = Marionette.ItemView.extend({
    template: '#detalhe-ocorrencia',
    onRender: function () {
        return this;
    }
});

var ocorrenciaEncerradaCompositeView = Marionette.CompositeView.extend({
    template: '#regiao-ocorrencia-encerradas',
    tagName: 'div',
    childView: ocorrenciaEncerradaItemView,
    childViewContainer: "#table-ocorrencia-encerrada",
    onShow: function () {
        return this;
    }
});

var ocorrenciaPendenteCompositeView = Marionette.CompositeView.extend({
    template: '#regiao-ocorrencia-pendente',
    tagName: 'div',
    childView: ocorrenciaPendenteItemView,
    childViewContainer: "#table-ocorrencia-pendente",
    onRender: function () {
        this.$el.css({'cursor': 'pointer'});
        this.$el.attr('title', 'Clique para ver mais detalhes');
    }

});



var doc = $(document);

//renderiza mascara de entrada para data no formato pt para elemento com classe .maskDate
doc.on("focus", ".maskDate", function () {
    var $el = $(this);
    if (!$el.data('maskLoaded')) {
        $el.mask('99/99/9999');
        $el.data('maskLoaded', true);
    }
});

doc.on("focus", "[data-mask]", function () {
    var $el = $(this);
    if (!$el.data('maskLoaded')) {
        $el.mask($el.data('mask'));
        $el.data('maskLoaded', true);
    }
});

doc.on("input", "[data-counter]", function () {
    var $el = $(this);
    var $target = $($el.data('counter'));
    $target.text($el.val().trim().length);
});

/**
 * @doc http://confluence.unyleya.com.br/pages/viewpage.action?pageId=8359093
 * @topic Buscar Opções em um Combobox
 */
doc.on("input", "input[data-search]", function () {
    var $input = $(this);
    var $target = $($input.data("search"));

    if (!$target.length) {
        return;
    }

    var value = $input.val().toLowerCase().removeAccents()
        .replace(/\n|\r|\t/g, " ")
        .replace(/\s+/g, " ")
        .trim();

    var isCombo = $target.is("select");
    var $options = isCombo ? $target.find("option") : $target;

    $options.each(function () {
        var $opt = $(this);

        if (!$opt.attr("data-label")) {
            $opt.attr("data-label", $opt.text().toLowerCase().removeAccents()
                .replace(/\n|\r|\t/g, " ")
                .replace(/\s+/g, " ")
                .trim());
        }
    });

    $options.show();

    if (value) {
        $options.not("[data-label*='" + value + "']").not(":selected").hide();
    }
});

// Dynamic Tooltiper,
// chamada para atualização de mensagens do usuário e
// definição do idioma do bootbox para Português do Brasil.
doc.ready(function () {

    bootbox.setDefaults({
        locale: 'br'
    });

    $.extend($.pnotify.defaults, {
        addclass: 'stack-bottomleft',
        stack: {
            dir1: "right",
            dir2: "up"
        },
        delay: 5000
    });

    setTimeout(function () {
        setup_tooltips_types();
    }, 500);

    /*
     * Verifica se existem novas mensagens/notificações para o usuário e atualiza o contador do menu inicial.
     */
    setInterval(function () {
        atualizarMensagemUsuario();
    }, 300000);
});

/**
 * Desabilita o uso do Ctrl+v nos campos com a classe "desabilitactrlv" útil para impedir que colem texto em campos de valor
 **/
doc.on("focus", ".desabilitactrlv", function () {
    $(this).bind("cut copy paste", function (e) {
        e.preventDefault();
    });
});


doc.on("focus", ".numeric", function () {
    $(this).numeric();
});

//renderiza o datepicker para o elemento com classe do css .datepicker
doc.on("focus", ".datepicker", function () {
    var $el = $(this);

    if (!$el.data('datepicker')) {
        $el.datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: '-20y'
        });
    }

    if ($el.data('button')) {
        $("#" + $el.data('button')).click(function () {
            $el.datepicker("show");
        });
    }

    // Resetar o calendario para o Dia Atual caso ele não possua valor
    if (!$el.val()) {
        var dt = (new Date()).toISOString().substr(0, 10).split('-').reverse().join('/');
        $el.val(dt);
        $el.datepicker('update', dt);
    }
});

//renderiza o datepicker para o elemento com classe do css .datepicker_mn100
doc.on("focus", ".datepicker_mn100", function () {
    var $el = $(this);

    if (!$el.data("datepicker")) {
        $el.datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: "-100y"
        });
    }

    if($el.data("button")){
        $("#"+$el.data("button")).click(function() {
            $el.datepicker("show");
        });
    }

    // Resetar o calendario para o Dia Atual caso ele não possua valor
    if (!$el.val()) {
        var dt = (new Date()).toISOString().substr(0, 10).split("-").reverse().join("/");
        $el.val(dt);
        $el.datepicker("update", dt);
    }
});

//renderiza o datepicker exibindo somente os meses e anos para o elemento com classe do css .datepicker_meses
doc.on("focus", ".datepicker_meses", function () {
    $(this).datepicker({
        format: 'mm/yyyy',
        startView: "months",
        minViewMode: "months",
        language: "pt-BR",
        autoclose: true,
        clearBtn: true
    });
});


/**
 * Abre o calendar ao clicar no botão com ícone de calendário
 * <button class="btn js-bt-calendar" data-calendar="dt_termino_confirmacao" type="button">
 * <i class="icon icon-calendar"></i>
 * </button>
 * @todo tratar quando o campo for do tipo range datepicker_range_min/datepicker_range_max
 */
doc.on("click", ".js-bt-calendar", function () {
    var id = '#' + $(this).data('calendar');

    if (!$(id).data('datepicker')) {

        $(id).datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        });
    }

    if ($(id).val()) {
        $(id).datepicker('update', $(id).val().substr(0, 10));
    }
    $(id).datepicker('show');

});
//versão para exibir somente os meses e anos
doc.on("click", ".js-bt-calendar_meses", function () {
    var id = '#' + $(this).data('calendar');

    $(id).datepicker({
        format: 'mm/yyyy',
        startView: "months",
        minViewMode: "months",
        language: "pt-BR",
        autoclose: true,
        clearBtn: true
    });

    $(id).datepicker('show');
});

/**
 * renderiza o datepicker para o elemento com classe do css .datepickerStart
 * O datapicker apresenta apenas datas a partir de dia atual
 */
doc.on("focus", ".datepickerStart", function () {
    $(this).datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true,
        clearBtn: true,
        startDate: new Date()
    });
});

/**
 * Usado em conjunto com o ".datepicker_range_max" para criar um período no seletor de datas
 * O campo data-maximo deve ser informado com o NOME do input que deverá ter a data de término
 * Exemplo: <input id="dt_inicio" class="datepicker_range_min maskDate" type="text" name="dt_inicio" data-maximo="dt_termino">
 **/
doc.on("focus", ".datepicker_range_min", function () {
    try {

        if (typeof $(this).data('maximo') == "undefined") {
            throw('Você precisa definir o campo data-minimo no input para poder usar este DatePicker');
        }

        var termino = $(this).closest('form').find("input[name='" + $(this).data('maximo') + "']");
        termino.datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        });

        $(this).datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        }).on('hide', function (e) {
            termino.datepicker('setStartDate', e.date);
        });

        if (termino.val() != '') {
            $(this).datepicker('setEndDate', termino.datepicker('getDate'));
        }

    } catch (err) {
//        console.log(err);
    }
});

/**
 * Usado em conjunto com o ".datepicker_range_max" para criar um período no seletor de datas
 * O campo data-minimo deve ser informado com o NOME do input que deverá ter a data de início
 * Exemplo: <input id="dt_termino" class="datepicker_range_max maskDate" type="text" name="dt_termino" data-minimo="dt_termino">
 **/
function setDatePickerMax() {

    var termino = $('.datepicker_range_max');

    if (typeof termino.data('minimo') == "undefined") {
        throw('Você precisa definir o campo data-minimo no input para poder usar este DatePicker');
    }

    var inicio = termino.closest('form').find("input[name='" + termino.data('minimo') + "']");

    if (!inicio.data('datepicker')) {
        inicio.datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        });
        if (inicio.data('button')) {
            $("#" + inicio.data('button')).show().click(function () {
                inicio.datepicker("show");
            });
        }
    }

    if (!termino.data('datepicker')) {
        termino.datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        }).on('hide', function (e) {
            inicio.datepicker('setEndDate', e.date);
        }).on('show', function (e) {
            if (inicio.val() != '') {
                if (inicio.datepicker('getDate')) {
                    if (termino.datepicker('getDate') && termino.datepicker('getDate').getTime() < inicio.datepicker('getDate').getTime()) {
                        termino.datepicker('setDate', inicio.datepicker('getDate'));
                    }
                    termino.datepicker('setStartDate', inicio.datepicker('getDate'));
                }
            }
        });

        if (termino.data('button')) {
            $("#" + termino.data('button')).show().click(function () {
                termino.datepicker("show");
            });
        }
    }

    if (inicio.val() != '') {
        termino.datepicker('setStartDate', inicio.datepicker('getDate'));
    }

}

doc.on("focus", ".datepicker_range_max", function () {
    setDatePickerMax();
});

/**
 * Caputurando todos os erros JS e exibindo no console
 * @param obj
 * @param evnt
 * @param handler
 */
function addHandler(obj, evnt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evnt.replace(/^on/, ''), handler, false);
    } else {
        if (obj[evnt]) {
            var origHandler = obj[evnt];
            obj[evnt] = function (evt) {
                origHandler(evt);
                handler(evt);
            };
        } else {
            obj[evnt] = function (evt) {
                handler(evt);
            };
        }
    }
}

// Tooltips types setup
function setup_tooltips_types() {
    tipers = $('.tooltiper.top');
    set_up_tiper(tipers, 'top');

    tipers = $('.tooltiper.bottom');
    set_up_tiper(tipers, 'bottom');

    tipers = $('.tooltiper.left');
    set_up_tiper(tipers, 'left');

    tipers = $('.tooltiper.right');
    set_up_tiper(tipers, 'right');

    $('.tooltiper').tooltip({trigger: 'hover'});
}

/*
 * placement = left,right,top,bottom
 */
function set_up_tiper(tipers, placement) {
    if (tipers.length == 0) return;

    // SetUp the tipers
    for (i = 0; i < tipers.length; i++) {
        tiper = $(tipers[i]);
        tiper.attr('data-placement', placement);
    }
}

function loading() {
    //clearTimeout(window.G2.timerLoading);
    $('#overlay').addClass('loading');
}

function loaded() {
    //clearTimeout(window.G2.timerLoading);
    $('#overlay').removeClass('loading');
    $('#htmlContent').show();
}

function loadHtml(url, fn, er) {
    return $.ajax({
        url: url,
        type: 'get',
        async: true,
        beforeSend: function () {
            loading();
            G2S.breadcrumb.render();
            this.htmlContent = $('#htmlContent').empty();
        },
        success: function (response) {
            this.htmlContent.html(response)
        },
        complete: function () {
            loaded();
            if (typeof fn === 'function')
                fn();
        },
        error: function (response) {
            loaded();

            if (typeof er == 'function') {
                er(response);
            }

            if (console && console.error) {
                console.error('Houve um erro ao carregar a rota. Clique para mais informações.', response);
            }

            if (response.responseJSON && !response.responseJSON.logado) {
                G2S.loggedUser.set('logged', false);
                G2S.deslogaUsuario();
            }
        }

    });
}

function formToObject(form_id) {
    var paramObj = {};

    $.each($(form_id).serializeArray(), function (_, kv) {
        if (paramObj.hasOwnProperty(kv.name)) {
            paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
            paramObj[kv.name].push(kv.value);
        }
        else {
            paramObj[kv.name] = kv.value;
        }
    });

    return paramObj;
}

function descobreTO(tipoCadastro) {
    var testeTO;
    var request = $.ajax({
        url: "/pesquisa/" + tipoCadastro,
        type: "GET",
        dataFilter: function (response) {

            var grid = response.match('name="grid" value="([^"]*)')[1];
            var to = response.match('name="to" value="([^"]*)')[1];
            var obj = {'grid': grid, 'to': to};
            return obj;
        },
        dataType: "html",
        async: false
    });

    request.done(function (msg) {
        testeTO = msg;
    });
    request.fail(function (jqXHR, textStatus) {
        testeTO = "Request failed: " + textStatus;
    });
    return testeTO;
}

function customParseInt(element) {
    return parseInt(element, 10);
}

function debug() {
    if (console && console.log) {
        for (var i in arguments) {
            console.log(arguments[i]);
        }
    }
}


/**
 * Metodo responsavel por retornar se um determinado perfil tem permissao, em
 * determinada funcionalidade.
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@gmail.com>
 */
function VerificarPerfilPermissao(id_permissao, id_funcionalidade, async) {
    if (typeof async === 'undefined') {
        async = false;
    }

    var Model = Backbone.Model.extend({
        defaults: {
            id_perfil: "",
            id_funcionalidade: "",
            id_permissao: ""
        }
    });

    var Collection = Backbone.Collection.extend({
        model: Model,
        url: '/util/perfil-permissao-funcionalidade'
    });

    var collectionIni = new Collection();

    var fetch = collectionIni.fetch({
        async: async,
        data: {
            id_permissao: id_permissao,
            id_funcionalidade: id_funcionalidade
        }
    });

    return async ? fetch : collectionIni;
}

function retornarEsquemaConfiguracao(id_itemconfiguracao, async) {
    if (typeof async === 'undefined')
        async = false;

    var EsquemaConfiguracaoModel = Backbone.Model.extend({
        defaults: {
            id_esquemaconfiguracao: '',
            st_esquemaconfiguracao: '',
            id_usuariocadastro: '',
            dt_cadastro: '',
            id_usuarioatualizacao: '',
            dt_atualizacao: '',
            id_itemconfiguracao: '',
            st_itemconfiguracao: '',
            st_descricao: '',
            st_default: '',
            st_valor: '',
            st_nomeentidade: '',
            id_entidade: '',
        }
    });

    var EsquemaConfiguracaoCollection = Backbone.Collection.extend({
        model: EsquemaConfiguracaoModel,
        url: '/esquema-configuracao/retornar-esquema-valores'
    });

    var esquemaCollection = new EsquemaConfiguracaoCollection();
    esquemaCollection.url = '/esquema-configuracao/retornar-esquema-valores/id_itemconfiguracao/' + id_itemconfiguracao;

    var fetch = esquemaCollection.fetch({
        async: async
    });

    if (async)
        return fetch;

    return esquemaCollection;
}

function htmlEncode(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

function htmlDecode(value) {
    return String(value).replace(/&quot;/g, '"').replace(/&#39;/g, "'").replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
}

/*
 * ---------------------------------------------
 * Funções comuns a várias funcionalidades
 * ---------------------------------------------
 */

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}


// Funcao usada pra ordenar objetos pelo atributo selecionado
function compareSortEntity(a, b) {
    if (a.attributes.st_nomeentidade < b.attributes.st_nomeentidade)
        return -1;
    if (a.attributes.st_nomeentidade > b.attributes.st_nomeentidade)
        return 1;
    return 0;
}

/**
 * Retorna string aplicado regex para descartar caracteres especiais
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @returns String
 */
G2S_StringRegex = function (s) {
    return s.toUpperCase()
        .replace(new RegExp(/\s/g), "")
        .replace(new RegExp(/[àáâãäå]/g), "a")
        .replace(new RegExp(/æ/g), "ae")
        .replace(new RegExp(/ç/g), "c")
        .replace(new RegExp(/[èéêë]/g), "e")
        .replace(new RegExp(/[ìíîï]/g), "i")
        .replace(new RegExp(/ñ/g), "n")
        .replace(new RegExp(/[òóôõö]/g), "o")
        .replace(new RegExp(/œ/g), "oe")
        .replace(new RegExp(/[ùúûü]/g), "u")
        .replace(new RegExp(/[ýÿ]/g), "y")
        .replace(new RegExp(/\W/g), "");
};


/**
 * Prototipo (str.removeAccents()) usado para remover acentos de strings
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @returns String
 */
var LatinaMapCaracteres = {
    "Á": "A",
    "Ă": "A",
    "Ắ": "A",
    "Ặ": "A",
    "Ằ": "A",
    "Ẳ": "A",
    "Ẵ": "A",
    "Ǎ": "A",
    "Â": "A",
    "Ấ": "A",
    "Ậ": "A",
    "Ầ": "A",
    "Ẩ": "A",
    "Ẫ": "A",
    "Ä": "A",
    "Ǟ": "A",
    "Ȧ": "A",
    "Ǡ": "A",
    "Ạ": "A",
    "Ȁ": "A",
    "À": "A",
    "Ả": "A",
    "Ȃ": "A",
    "Ā": "A",
    "Ą": "A",
    "Å": "A",
    "Ǻ": "A",
    "Ḁ": "A",
    "Ⱥ": "A",
    "Ã": "A",
    "Ꜳ": "AA",
    "Æ": "AE",
    "Ǽ": "AE",
    "Ǣ": "AE",
    "Ꜵ": "AO",
    "Ꜷ": "AU",
    "Ꜹ": "AV",
    "Ꜻ": "AV",
    "Ꜽ": "AY",
    "Ḃ": "B",
    "Ḅ": "B",
    "Ɓ": "B",
    "Ḇ": "B",
    "Ƀ": "B",
    "Ƃ": "B",
    "Ć": "C",
    "Č": "C",
    "Ç": "C",
    "Ḉ": "C",
    "Ĉ": "C",
    "Ċ": "C",
    "Ƈ": "C",
    "Ȼ": "C",
    "Ď": "D",
    "Ḑ": "D",
    "Ḓ": "D",
    "Ḋ": "D",
    "Ḍ": "D",
    "Ɗ": "D",
    "Ḏ": "D",
    "ǲ": "D",
    "ǅ": "D",
    "Đ": "D",
    "Ƌ": "D",
    "Ǳ": "DZ",
    "Ǆ": "DZ",
    "É": "E",
    "Ĕ": "E",
    "Ě": "E",
    "Ȩ": "E",
    "Ḝ": "E",
    "Ê": "E",
    "Ế": "E",
    "Ệ": "E",
    "Ề": "E",
    "Ể": "E",
    "Ễ": "E",
    "Ḙ": "E",
    "Ë": "E",
    "Ė": "E",
    "Ẹ": "E",
    "Ȅ": "E",
    "È": "E",
    "Ẻ": "E",
    "Ȇ": "E",
    "Ē": "E",
    "Ḗ": "E",
    "Ḕ": "E",
    "Ę": "E",
    "Ɇ": "E",
    "Ẽ": "E",
    "Ḛ": "E",
    "Ꝫ": "ET",
    "Ḟ": "F",
    "Ƒ": "F",
    "Ǵ": "G",
    "Ğ": "G",
    "Ǧ": "G",
    "Ģ": "G",
    "Ĝ": "G",
    "Ġ": "G",
    "Ɠ": "G",
    "Ḡ": "G",
    "Ǥ": "G",
    "Ḫ": "H",
    "Ȟ": "H",
    "Ḩ": "H",
    "Ĥ": "H",
    "Ⱨ": "H",
    "Ḧ": "H",
    "Ḣ": "H",
    "Ḥ": "H",
    "Ħ": "H",
    "Í": "I",
    "Ĭ": "I",
    "Ǐ": "I",
    "Î": "I",
    "Ï": "I",
    "Ḯ": "I",
    "İ": "I",
    "Ị": "I",
    "Ȉ": "I",
    "Ì": "I",
    "Ỉ": "I",
    "Ȋ": "I",
    "Ī": "I",
    "Į": "I",
    "Ɨ": "I",
    "Ĩ": "I",
    "Ḭ": "I",
    "Ꝺ": "D",
    "Ꝼ": "F",
    "Ᵹ": "G",
    "Ꞃ": "R",
    "Ꞅ": "S",
    "Ꞇ": "T",
    "Ꝭ": "IS",
    "Ĵ": "J",
    "Ɉ": "J",
    "Ḱ": "K",
    "Ǩ": "K",
    "Ķ": "K",
    "Ⱪ": "K",
    "Ꝃ": "K",
    "Ḳ": "K",
    "Ƙ": "K",
    "Ḵ": "K",
    "Ꝁ": "K",
    "Ꝅ": "K",
    "Ĺ": "L",
    "Ƚ": "L",
    "Ľ": "L",
    "Ļ": "L",
    "Ḽ": "L",
    "Ḷ": "L",
    "Ḹ": "L",
    "Ⱡ": "L",
    "Ꝉ": "L",
    "Ḻ": "L",
    "Ŀ": "L",
    "Ɫ": "L",
    "ǈ": "L",
    "Ł": "L",
    "Ǉ": "LJ",
    "Ḿ": "M",
    "Ṁ": "M",
    "Ṃ": "M",
    "Ɱ": "M",
    "Ń": "N",
    "Ň": "N",
    "Ņ": "N",
    "Ṋ": "N",
    "Ṅ": "N",
    "Ṇ": "N",
    "Ǹ": "N",
    "Ɲ": "N",
    "Ṉ": "N",
    "Ƞ": "N",
    "ǋ": "N",
    "Ñ": "N",
    "Ǌ": "NJ",
    "Ó": "O",
    "Ŏ": "O",
    "Ǒ": "O",
    "Ô": "O",
    "Ố": "O",
    "Ộ": "O",
    "Ồ": "O",
    "Ổ": "O",
    "Ỗ": "O",
    "Ö": "O",
    "Ȫ": "O",
    "Ȯ": "O",
    "Ȱ": "O",
    "Ọ": "O",
    "Ő": "O",
    "Ȍ": "O",
    "Ò": "O",
    "Ỏ": "O",
    "Ơ": "O",
    "Ớ": "O",
    "Ợ": "O",
    "Ờ": "O",
    "Ở": "O",
    "Ỡ": "O",
    "Ȏ": "O",
    "Ꝋ": "O",
    "Ꝍ": "O",
    "Ō": "O",
    "Ṓ": "O",
    "Ṑ": "O",
    "Ɵ": "O",
    "Ǫ": "O",
    "Ǭ": "O",
    "Ø": "O",
    "Ǿ": "O",
    "Õ": "O",
    "Ṍ": "O",
    "Ṏ": "O",
    "Ȭ": "O",
    "Ƣ": "OI",
    "Ꝏ": "OO",
    "Ɛ": "E",
    "Ɔ": "O",
    "Ȣ": "OU",
    "Ṕ": "P",
    "Ṗ": "P",
    "Ꝓ": "P",
    "Ƥ": "P",
    "Ꝕ": "P",
    "Ᵽ": "P",
    "Ꝑ": "P",
    "Ꝙ": "Q",
    "Ꝗ": "Q",
    "Ŕ": "R",
    "Ř": "R",
    "Ŗ": "R",
    "Ṙ": "R",
    "Ṛ": "R",
    "Ṝ": "R",
    "Ȑ": "R",
    "Ȓ": "R",
    "Ṟ": "R",
    "Ɍ": "R",
    "Ɽ": "R",
    "Ꜿ": "C",
    "Ǝ": "E",
    "Ś": "S",
    "Ṥ": "S",
    "Š": "S",
    "Ṧ": "S",
    "Ş": "S",
    "Ŝ": "S",
    "Ș": "S",
    "Ṡ": "S",
    "Ṣ": "S",
    "Ṩ": "S",
    "Ť": "T",
    "Ţ": "T",
    "Ṱ": "T",
    "Ț": "T",
    "Ⱦ": "T",
    "Ṫ": "T",
    "Ṭ": "T",
    "Ƭ": "T",
    "Ṯ": "T",
    "Ʈ": "T",
    "Ŧ": "T",
    "Ɐ": "A",
    "Ꞁ": "L",
    "Ɯ": "M",
    "Ʌ": "V",
    "Ꜩ": "TZ",
    "Ú": "U",
    "Ŭ": "U",
    "Ǔ": "U",
    "Û": "U",
    "Ṷ": "U",
    "Ü": "U",
    "Ǘ": "U",
    "Ǚ": "U",
    "Ǜ": "U",
    "Ǖ": "U",
    "Ṳ": "U",
    "Ụ": "U",
    "Ű": "U",
    "Ȕ": "U",
    "Ù": "U",
    "Ủ": "U",
    "Ư": "U",
    "Ứ": "U",
    "Ự": "U",
    "Ừ": "U",
    "Ử": "U",
    "Ữ": "U",
    "Ȗ": "U",
    "Ū": "U",
    "Ṻ": "U",
    "Ų": "U",
    "Ů": "U",
    "Ũ": "U",
    "Ṹ": "U",
    "Ṵ": "U",
    "Ꝟ": "V",
    "Ṿ": "V",
    "Ʋ": "V",
    "Ṽ": "V",
    "Ꝡ": "VY",
    "Ẃ": "W",
    "Ŵ": "W",
    "Ẅ": "W",
    "Ẇ": "W",
    "Ẉ": "W",
    "Ẁ": "W",
    "Ⱳ": "W",
    "Ẍ": "X",
    "Ẋ": "X",
    "Ý": "Y",
    "Ŷ": "Y",
    "Ÿ": "Y",
    "Ẏ": "Y",
    "Ỵ": "Y",
    "Ỳ": "Y",
    "Ƴ": "Y",
    "Ỷ": "Y",
    "Ỿ": "Y",
    "Ȳ": "Y",
    "Ɏ": "Y",
    "Ỹ": "Y",
    "Ź": "Z",
    "Ž": "Z",
    "Ẑ": "Z",
    "Ⱬ": "Z",
    "Ż": "Z",
    "Ẓ": "Z",
    "Ȥ": "Z",
    "Ẕ": "Z",
    "Ƶ": "Z",
    "Ĳ": "IJ",
    "Œ": "OE",
    "ᴀ": "A",
    "ᴁ": "AE",
    "ʙ": "B",
    "ᴃ": "B",
    "ᴄ": "C",
    "ᴅ": "D",
    "ᴇ": "E",
    "ꜰ": "F",
    "ɢ": "G",
    "ʛ": "G",
    "ʜ": "H",
    "ɪ": "I",
    "ʁ": "R",
    "ᴊ": "J",
    "ᴋ": "K",
    "ʟ": "L",
    "ᴌ": "L",
    "ᴍ": "M",
    "ɴ": "N",
    "ᴏ": "O",
    "ɶ": "OE",
    "ᴐ": "O",
    "ᴕ": "OU",
    "ᴘ": "P",
    "ʀ": "R",
    "ᴎ": "N",
    "ᴙ": "R",
    "ꜱ": "S",
    "ᴛ": "T",
    "ⱻ": "E",
    "ᴚ": "R",
    "ᴜ": "U",
    "ᴠ": "V",
    "ᴡ": "W",
    "ʏ": "Y",
    "ᴢ": "Z",
    "á": "a",
    "ă": "a",
    "ắ": "a",
    "ặ": "a",
    "ằ": "a",
    "ẳ": "a",
    "ẵ": "a",
    "ǎ": "a",
    "â": "a",
    "ấ": "a",
    "ậ": "a",
    "ầ": "a",
    "ẩ": "a",
    "ẫ": "a",
    "ä": "a",
    "ǟ": "a",
    "ȧ": "a",
    "ǡ": "a",
    "ạ": "a",
    "ȁ": "a",
    "à": "a",
    "ả": "a",
    "ȃ": "a",
    "ā": "a",
    "ą": "a",
    "ᶏ": "a",
    "ẚ": "a",
    "å": "a",
    "ǻ": "a",
    "ḁ": "a",
    "ⱥ": "a",
    "ã": "a",
    "ꜳ": "aa",
    "æ": "ae",
    "ǽ": "ae",
    "ǣ": "ae",
    "ꜵ": "ao",
    "ꜷ": "au",
    "ꜹ": "av",
    "ꜻ": "av",
    "ꜽ": "ay",
    "ḃ": "b",
    "ḅ": "b",
    "ɓ": "b",
    "ḇ": "b",
    "ᵬ": "b",
    "ᶀ": "b",
    "ƀ": "b",
    "ƃ": "b",
    "ɵ": "o",
    "ć": "c",
    "č": "c",
    "ç": "c",
    "ḉ": "c",
    "ĉ": "c",
    "ɕ": "c",
    "ċ": "c",
    "ƈ": "c",
    "ȼ": "c",
    "ď": "d",
    "ḑ": "d",
    "ḓ": "d",
    "ȡ": "d",
    "ḋ": "d",
    "ḍ": "d",
    "ɗ": "d",
    "ᶑ": "d",
    "ḏ": "d",
    "ᵭ": "d",
    "ᶁ": "d",
    "đ": "d",
    "ɖ": "d",
    "ƌ": "d",
    "ı": "i",
    "ȷ": "j",
    "ɟ": "j",
    "ʄ": "j",
    "ǳ": "dz",
    "ǆ": "dz",
    "é": "e",
    "ĕ": "e",
    "ě": "e",
    "ȩ": "e",
    "ḝ": "e",
    "ê": "e",
    "ế": "e",
    "ệ": "e",
    "ề": "e",
    "ể": "e",
    "ễ": "e",
    "ḙ": "e",
    "ë": "e",
    "ė": "e",
    "ẹ": "e",
    "ȅ": "e",
    "è": "e",
    "ẻ": "e",
    "ȇ": "e",
    "ē": "e",
    "ḗ": "e",
    "ḕ": "e",
    "ⱸ": "e",
    "ę": "e",
    "ᶒ": "e",
    "ɇ": "e",
    "ẽ": "e",
    "ḛ": "e",
    "ꝫ": "et",
    "ḟ": "f",
    "ƒ": "f",
    "ᵮ": "f",
    "ᶂ": "f",
    "ǵ": "g",
    "ğ": "g",
    "ǧ": "g",
    "ģ": "g",
    "ĝ": "g",
    "ġ": "g",
    "ɠ": "g",
    "ḡ": "g",
    "ᶃ": "g",
    "ǥ": "g",
    "ḫ": "h",
    "ȟ": "h",
    "ḩ": "h",
    "ĥ": "h",
    "ⱨ": "h",
    "ḧ": "h",
    "ḣ": "h",
    "ḥ": "h",
    "ɦ": "h",
    "ẖ": "h",
    "ħ": "h",
    "ƕ": "hv",
    "í": "i",
    "ĭ": "i",
    "ǐ": "i",
    "î": "i",
    "ï": "i",
    "ḯ": "i",
    "ị": "i",
    "ȉ": "i",
    "ì": "i",
    "ỉ": "i",
    "ȋ": "i",
    "ī": "i",
    "į": "i",
    "ᶖ": "i",
    "ɨ": "i",
    "ĩ": "i",
    "ḭ": "i",
    "ꝺ": "d",
    "ꝼ": "f",
    "ᵹ": "g",
    "ꞃ": "r",
    "ꞅ": "s",
    "ꞇ": "t",
    "ꝭ": "is",
    "ǰ": "j",
    "ĵ": "j",
    "ʝ": "j",
    "ɉ": "j",
    "ḱ": "k",
    "ǩ": "k",
    "ķ": "k",
    "ⱪ": "k",
    "ꝃ": "k",
    "ḳ": "k",
    "ƙ": "k",
    "ḵ": "k",
    "ᶄ": "k",
    "ꝁ": "k",
    "ꝅ": "k",
    "ĺ": "l",
    "ƚ": "l",
    "ɬ": "l",
    "ľ": "l",
    "ļ": "l",
    "ḽ": "l",
    "ȴ": "l",
    "ḷ": "l",
    "ḹ": "l",
    "ⱡ": "l",
    "ꝉ": "l",
    "ḻ": "l",
    "ŀ": "l",
    "ɫ": "l",
    "ᶅ": "l",
    "ɭ": "l",
    "ł": "l",
    "ǉ": "lj",
    "ſ": "s",
    "ẜ": "s",
    "ẛ": "s",
    "ẝ": "s",
    "ḿ": "m",
    "ṁ": "m",
    "ṃ": "m",
    "ɱ": "m",
    "ᵯ": "m",
    "ᶆ": "m",
    "ń": "n",
    "ň": "n",
    "ņ": "n",
    "ṋ": "n",
    "ȵ": "n",
    "ṅ": "n",
    "ṇ": "n",
    "ǹ": "n",
    "ɲ": "n",
    "ṉ": "n",
    "ƞ": "n",
    "ᵰ": "n",
    "ᶇ": "n",
    "ɳ": "n",
    "ñ": "n",
    "ǌ": "nj",
    "ó": "o",
    "ŏ": "o",
    "ǒ": "o",
    "ô": "o",
    "ố": "o",
    "ộ": "o",
    "ồ": "o",
    "ổ": "o",
    "ỗ": "o",
    "ö": "o",
    "ȫ": "o",
    "ȯ": "o",
    "ȱ": "o",
    "ọ": "o",
    "ő": "o",
    "ȍ": "o",
    "ò": "o",
    "ỏ": "o",
    "ơ": "o",
    "ớ": "o",
    "ợ": "o",
    "ờ": "o",
    "ở": "o",
    "ỡ": "o",
    "ȏ": "o",
    "ꝋ": "o",
    "ꝍ": "o",
    "ⱺ": "o",
    "ō": "o",
    "ṓ": "o",
    "ṑ": "o",
    "ǫ": "o",
    "ǭ": "o",
    "ø": "o",
    "ǿ": "o",
    "õ": "o",
    "ṍ": "o",
    "ṏ": "o",
    "ȭ": "o",
    "ƣ": "oi",
    "ꝏ": "oo",
    "ɛ": "e",
    "ᶓ": "e",
    "ɔ": "o",
    "ᶗ": "o",
    "ȣ": "ou",
    "ṕ": "p",
    "ṗ": "p",
    "ꝓ": "p",
    "ƥ": "p",
    "ᵱ": "p",
    "ᶈ": "p",
    "ꝕ": "p",
    "ᵽ": "p",
    "ꝑ": "p",
    "ꝙ": "q",
    "ʠ": "q",
    "ɋ": "q",
    "ꝗ": "q",
    "ŕ": "r",
    "ř": "r",
    "ŗ": "r",
    "ṙ": "r",
    "ṛ": "r",
    "ṝ": "r",
    "ȑ": "r",
    "ɾ": "r",
    "ᵳ": "r",
    "ȓ": "r",
    "ṟ": "r",
    "ɼ": "r",
    "ᵲ": "r",
    "ᶉ": "r",
    "ɍ": "r",
    "ɽ": "r",
    "ↄ": "c",
    "ꜿ": "c",
    "ɘ": "e",
    "ɿ": "r",
    "ś": "s",
    "ṥ": "s",
    "š": "s",
    "ṧ": "s",
    "ş": "s",
    "ŝ": "s",
    "ș": "s",
    "ṡ": "s",
    "ṣ": "s",
    "ṩ": "s",
    "ʂ": "s",
    "ᵴ": "s",
    "ᶊ": "s",
    "ȿ": "s",
    "ɡ": "g",
    "ᴑ": "o",
    "ᴓ": "o",
    "ᴝ": "u",
    "ť": "t",
    "ţ": "t",
    "ṱ": "t",
    "ț": "t",
    "ȶ": "t",
    "ẗ": "t",
    "ⱦ": "t",
    "ṫ": "t",
    "ṭ": "t",
    "ƭ": "t",
    "ṯ": "t",
    "ᵵ": "t",
    "ƫ": "t",
    "ʈ": "t",
    "ŧ": "t",
    "ᵺ": "th",
    "ɐ": "a",
    "ᴂ": "ae",
    "ǝ": "e",
    "ᵷ": "g",
    "ɥ": "h",
    "ʮ": "h",
    "ʯ": "h",
    "ᴉ": "i",
    "ʞ": "k",
    "ꞁ": "l",
    "ɯ": "m",
    "ɰ": "m",
    "ᴔ": "oe",
    "ɹ": "r",
    "ɻ": "r",
    "ɺ": "r",
    "ⱹ": "r",
    "ʇ": "t",
    "ʌ": "v",
    "ʍ": "w",
    "ʎ": "y",
    "ꜩ": "tz",
    "ú": "u",
    "ŭ": "u",
    "ǔ": "u",
    "û": "u",
    "ṷ": "u",
    "ü": "u",
    "ǘ": "u",
    "ǚ": "u",
    "ǜ": "u",
    "ǖ": "u",
    "ṳ": "u",
    "ụ": "u",
    "ű": "u",
    "ȕ": "u",
    "ù": "u",
    "ủ": "u",
    "ư": "u",
    "ứ": "u",
    "ự": "u",
    "ừ": "u",
    "ử": "u",
    "ữ": "u",
    "ȗ": "u",
    "ū": "u",
    "ṻ": "u",
    "ų": "u",
    "ᶙ": "u",
    "ů": "u",
    "ũ": "u",
    "ṹ": "u",
    "ṵ": "u",
    "ᵫ": "ue",
    "ꝸ": "um",
    "ⱴ": "v",
    "ꝟ": "v",
    "ṿ": "v",
    "ʋ": "v",
    "ᶌ": "v",
    "ⱱ": "v",
    "ṽ": "v",
    "ꝡ": "vy",
    "ẃ": "w",
    "ŵ": "w",
    "ẅ": "w",
    "ẇ": "w",
    "ẉ": "w",
    "ẁ": "w",
    "ⱳ": "w",
    "ẘ": "w",
    "ẍ": "x",
    "ẋ": "x",
    "ᶍ": "x",
    "ý": "y",
    "ŷ": "y",
    "ÿ": "y",
    "ẏ": "y",
    "ỵ": "y",
    "ỳ": "y",
    "ƴ": "y",
    "ỷ": "y",
    "ỿ": "y",
    "ȳ": "y",
    "ẙ": "y",
    "ɏ": "y",
    "ỹ": "y",
    "ź": "z",
    "ž": "z",
    "ẑ": "z",
    "ʑ": "z",
    "ⱬ": "z",
    "ż": "z",
    "ẓ": "z",
    "ȥ": "z",
    "ẕ": "z",
    "ᵶ": "z",
    "ᶎ": "z",
    "ʐ": "z",
    "ƶ": "z",
    "ɀ": "z",
    "ﬀ": "ff",
    "ﬃ": "ffi",
    "ﬄ": "ffl",
    "ﬁ": "fi",
    "ﬂ": "fl",
    "ĳ": "ij",
    "œ": "oe",
    "ﬆ": "st",
    "ₐ": "a",
    "ₑ": "e",
    "ᵢ": "i",
    "ⱼ": "j",
    "ₒ": "o",
    "ᵣ": "r",
    "ᵤ": "u",
    "ᵥ": "v",
    "ₓ": "x"
};
String.prototype.removeAccents = function () {
    return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
        return LatinaMapCaracteres[a] || a;
    });
};

String.prototype.removerCharEspecial = function() {
    return this.replace(/[^a-z0-9.]+/g, "");
}
function in_array(needle, haystack, argStrict) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true
    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '',
        strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}


function comboDinamico(url, idCampo, params) {
    $.getJSON(url, params, function (dados) {
        var options = '<option value="">--</option>';
        for (var i = 0; i < dados.length; i++) {
            options += '<option value="' + dados[i].id_value + '">' + dados[i].st_text + '</option>';
        }
        $('#' + idCampo).html(options).show();
    });
}

/**
 * Valida CPF
 * @param {string} cpf
 * @returns {Boolean}
 */
function validaCPF(cpf) {
    cpf = cpf.replace(/[^0-9]/g, "");
    var soma;
    var resto;
    soma = 0;
    if ((cpf == '11111111111') || (cpf == '22222222222') || (cpf ==
        '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') ||
        (cpf == '66666666666') || (cpf == '77777777777') || (cpf ==
        '88888888888') || (cpf == '99999999999') || (cpf == '00000000000')) {
        return false;
    } else {
        for (i = 1; i <= 9; i++) {
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
        }
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(9, 10)))
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(10, 11)))
            return false;

        return true;
    }
}

/**
 * Testa se o valor é um número através da expressão regular
 * @param {mixed} valor
 * @returns {Boolean}
 */
function validaNumero(valor) {
    var er = /^[0-9]+$/;
    return er.test(valor);
}

/**
 * Formata valor float para string no formato #.###,##
 * @param float valor
 * @returns {string}
 */
function formatValueToShow(valor) {
    var tmp = valor + '';
    tmp = tmp.replace(".", ",");
    if (tmp.length > 6) {
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }
    return tmp;
}

/**
 * Formata valor para float
 * @param string value
 * @returns {float}
 */
function formatValueToReal(value) {
    // Pegando valor monetario para conversao
    var money = new String(value);
    // Valor armazenado em array
    money = money.split(",");
    if (money[1]) {
        // Valor Real
        var real = money[0];

        // Valor Decimal
        var decimal = money[1];

        // Incremento de zero na casa decimal se necessario
        decimal = (decimal.length == 1) ? decimal + '0' : decimal.substring(0, 2);
        //monta o retorno
        money = real.replace('.', '') + '.' + decimal;

    }
    // Retornando valor
    return parseFloat(money);
}

/**
 * Calcula a proxima data a partir de uma data passada
 * @param value data inicial formato dia/mês/ano
 * @param increment quantidade de dias, mês ou ano a ser incrementando
 * @param local posição em que será incremetado, d = dia, m = mês, y = ano
 * @returns {Date}
 */
function proximaDataVencimento(value, increment, local) {

    //quebra a string pelas / (barras)
    var split = value.split('/');
    //pega as posições e monta uma nova string para data
    var newDate = split[2] + '/' + split[1] + '/' + split[0];

    //cria uma nova data
    var data = new Date(newDate);

    //força a variavel incremente a ser um inteiro
    increment = parseInt(increment);

    //verifica os locais passados para incrementar
    if (local == 'd') {
        data.setDate(data.getDate() + increment);
    }

    if (local == 'm') {
        data.setMonth(data.getMonth() + increment);
    }

    if (local == 'y') {
        data.setFullYear(data.getFullYear() + increment);
    }
    return data;
    //monta a string de retorno
//    return ((parseInt(data.getDate()) >= 10) ? data.getDate() : "0" + data.getDate()) + '/' + ((parseInt(data.getMonth()) >= 10) ? data.getMonth() + 1 : '0' + (data.getMonth() + 1)) + '/' + data.getFullYear();
}
/**
 * Formata uma data com hora ou sem
 * @param $data data no formato pt_br para en_us com hora ou sem
 * @returns {Date}
 */
function formatarData($data) {
    var split = $data.split('/');
    if (split) {
        //pega a hora
        var splitHora = split[2].split(" ");
        var dia = split[0];
        var mes = split[1];
        var ano = splitHora[0];
        var hora = splitHora[1] ? splitHora[1] : null;
    }
    var date = ano + "-" + mes + "-" + dia;
    var dt_return = new Date(date);
    return dt_return;
}
/**
 * Calcula a diferença entre duas datas
 * @param $dt_inicio data inicio no formato pt_br
 * @param $dt_fim data fim no formato pt_br
 * @returns {Number}
 */
function diferencaEntreDatas($dt_inicio, $dt_fim) {
    var dt_inicio = formatarData($dt_inicio);
    var dt_fim = formatarData($dt_fim);
    var datediff = Math.abs(dt_inicio.getTime() - dt_fim.getTime()); // difference
    return parseInt(datediff / (24 * 60 * 60 * 1000), 10); //Convert values days and return value
}

/**
 * Função para comparar duas datas, envie os dados caputados nos inputs
 *
 * retorno
 * data 1 > data 2 => -1
 * data 1 == data 2 => 0
 * data 1 < data 2 => 1
 *
 * @param str1
 * @param str2
 * @returns {number}
 */
function compareDate(str1, str2) {
    var dt1 = parseInt(str1.substring(0, 2));
    var mon1 = parseInt(str1.substring(3, 5));
    var yr1 = parseInt(str1.substring(6, 10));
    var date1 = new Date(yr1, mon1 - 1, dt1);

    var dt2 = parseInt(str2.substring(0, 2));
    var mon2 = parseInt(str2.substring(3, 5));
    var yr2 = parseInt(str2.substring(6, 10));
    var date2 = new Date(yr2, mon2 - 1, dt2);

    if (date1 > date2) {
        return -1;
    } else if (date1 == date2) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * @param string dt_inicio
 * @param string dt_termino
 */
function daysDiff(dt_inicio, dt_termino) {
    // dt_inicio = dt_inicio.substr(0, 10).split('/').reverse().join('-').replace(/\D/g, '');
    // dt_termino = dt_termino.substr(0, 10).split('/').reverse().join('-').replace(/\D/g, '');
    dt_inicio = dt_inicio.substr(0, 10).split('/').reverse().join('-');
    dt_termino = dt_termino.substr(0, 10).split('/').reverse().join('-');

    dt_inicio = new Date(dt_inicio);
    dt_termino = new Date(dt_termino);

    var time_diff = dt_termino.getTime() - dt_inicio.getTime();

    return Math.ceil(time_diff / (1000 * 3600 * 24));
}


/*
 * Métodos responsáveis pela verificação de novas mensagens do usuário logado.
 */
var ContadorMensagemUsuario = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        id_entidade: '',
        nu_mensagens: ''
    },
    url: function () {
        return this.id ? '/default/util/atualizar-mensagem-sessao' + this.id : '/default/util/atualizar-mensagem-sessao';
    }
});

function atualizarMensagemUsuario() {

    var ModelMensagemUsuario = new ContadorMensagemUsuario();
    ModelMensagemUsuario.fetch({
        success: function () {
            if (ModelMensagemUsuario.get('nu_mensagens') > 0) {
                $(document).find('.count-login').text(ModelMensagemUsuario.get('nu_mensagens'));
            } else {
                $(document).find('.count-login').text('');
            }
        }
    });
}


/**
 * Formata uma STRING com data e hora
 * @param data no formato AMERICANO para PT_BR COM HORA
 * @returns {String}
 */
function formatarDataToPtBR(data) {
    var split = data.split('-');
    if (split) {
        var splitHora = split[2].split(" ");
        var ano = split[0];
        var mes = split[1];
        var dia = splitHora[0];
        var hora = splitHora[1] ? splitHora[1] : null;
        var horaFormatada = splitHora[1].split('.');
    }
    var date = dia + "/" + mes + "/" + ano + " ás " + horaFormatada[0];
    return date;
}

/**
 * Metodo que recebe uma data em padrao americano e retorna em formato brasileiro
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @param param
 * @returns {string}
 */
function dateToPtBr(param) {

    var datePtBr = '';

    if (param instanceof Date || param == undefined) {
        var month = (param.getMonth() >= 9 ? (param.getMonth() + 1) : '0' + (param.getMonth() + 1));
        var date = (param.getDate() >= 9 ? param.getDate() : '0' + param.getDate());
        var year = param.getFullYear();
        datePtBr = date + '/' + month + '/' + year;
    }

    return datePtBr;
}

function formatarDataParaString(date) {

    var d = parseInt(date.getDate());
    var m = parseInt(date.getMonth());
    var y = parseInt(date.getFullYear());

    return ((d >= 10) ? d : "0" + d) + '/' + (((m + 1) >= 10) ? m + 1 : '0' + (m + 1)) + '/' + y;
}


/**
 * Funções retiradas do site http://www.w3schools.com/js/js_cookies.asp para leitura e escrita de cookie
 * @author Felipe Pastor
 * */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getDate() + exdays ? exdays : 20);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

/**
 * Funções retiradas do site http://www.w3schools.com/js/js_cookies.asp para leitura e escrita de cookie
 * @author Felipe Pastor
 * */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

/**
 * @description Funcão responsável por renderizar o tinymce em todos os textareas da tela. Antes, irá remover caso exista as instancias do editor.
 * @param elements
 * @param maxTrys
 * @return Object
 */
function loadTinyMCE(elements, maxTrys, fullPage) {
    // injeção de dependencia para remover o bug que impede re recuperar o conteúdo do editor sem as tags html/head/body
    // removendo o atributo fullpage, a recuperação funciona perfeitamente.
    if (typeof fullPage == 'undefined' || fullPage == false) {
        fullPage = 'fullpage';
    } else {
        fullPage = '';
    }

    if (typeof maxTrys == 'undefined') {
        maxTrys = 10;
    }

    var editors = elements ? elements.split(',') : tinymce.editors;

    try {
        tinyMCE.remove(); //limpa todos os editores ativos no momento
    } catch (e) {
        if (!maxTrys) {
            debug("Limite de tentativa para carregar TinyMCE excedido.");
            return false;
        } else {
            debug("Erro ao carregar TinyMCE. Tentando novamente...");
            return loadTinyMCE(elements, --maxTrys);
        }
    }

    var valid_elements = "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
        + "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
        + "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
        + "name|href|target|title|class|onfocus|onblur],strong/b,em/i,strike,u,"
        + "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
        + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
        + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
        + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
        + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
        + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
        + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
        + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
        + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
        + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
        + "|height|src|*],map[name],area[shape|coords|href|alt|target],bdo,"
        + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
        + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
        + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
        + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
        + "q[cite],samp,select[disabled|multiple|name|size],small,"
        + "textarea[cols|rows|disabled|name|readonly],tt,var,big";

    tinymce.init({
        mode: elements ? 'exact' : 'textareas',
        content_css: ['../../css/tinymce/fonts/agenda.css'],
        font_formats: 'Agenda Bold=Agenda_Bold; Agenda Medium=Agenda_Medium;Agenda Light=Agenda_Light;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
        elements: elements,
        selector: elements ? null : 'textarea',
        font_size_style_values: "8px,10px,12px,13px,14px,16px,18px,20px,22px",
        extended_valid_elements: 'img[class=myclass|!src|border:0|alt|title|width|height|style],span[style|color]',
        valid_elements: valid_elements,
        media_strict: false,
        plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste " + fullPage + " textcolor"
        ],

        toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
        toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
        toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
        menubar: false,
        toolbar_items_size: 'small',
        theme: 'modern',
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    return true;
}


function get_html_translation_table(table, quote_style) {
    //  discuss at: http://phpjs.org/functions/get_html_translation_table/
    // original by: Philip Peterson
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: noname
    // bugfixed by: Alex
    // bugfixed by: Marco
    // bugfixed by: madipta
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: T.Wild
    // improved by: KELAN
    // improved by: Brett Zamir (http://brett-zamir.me)
    //    input by: Frank Forte
    //    input by: Ratheous
    //        note: It has been decided that we're not going to add global
    //        note: dependencies to php.js, meaning the constants are not
    //        note: real constants, but strings instead. Integers are also supported if someone
    //        note: chooses to create the constants themselves.
    //   example 1: get_html_translation_table('HTML_SPECIALCHARS');
    //   returns 1: {'"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;'}

    var entities = {},
        hash_map = {},
        decimal;
    var constMappingTable = {},
        constMappingQuoteStyle = {};
    var useTable = {},
        useQuoteStyle = {};

    // Translate arguments
    constMappingTable[0] = 'HTML_SPECIALCHARS';
    constMappingTable[1] = 'HTML_ENTITIES';
    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
    constMappingQuoteStyle[2] = 'ENT_COMPAT';
    constMappingQuoteStyle[3] = 'ENT_QUOTES';

    useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
    useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() :
        'ENT_COMPAT';

    if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
        throw new Error('Table: ' + useTable + ' not supported');
        // return false;
    }

    entities['38'] = '&amp;';
    if (useTable === 'HTML_ENTITIES') {
        entities['160'] = '&nbsp;';
        entities['161'] = '&iexcl;';
        entities['162'] = '&cent;';
        entities['163'] = '&pound;';
        entities['164'] = '&curren;';
        entities['165'] = '&yen;';
        entities['166'] = '&brvbar;';
        entities['167'] = '&sect;';
        entities['168'] = '&uml;';
        entities['169'] = '&copy;';
        entities['170'] = '&ordf;';
        entities['171'] = '&laquo;';
        entities['172'] = '&not;';
        entities['173'] = '&shy;';
        entities['174'] = '&reg;';
        entities['175'] = '&macr;';
        entities['176'] = '&deg;';
        entities['177'] = '&plusmn;';
        entities['178'] = '&sup2;';
        entities['179'] = '&sup3;';
        entities['180'] = '&acute;';
        entities['181'] = '&micro;';
        entities['182'] = '&para;';
        entities['183'] = '&middot;';
        entities['184'] = '&cedil;';
        entities['185'] = '&sup1;';
        entities['186'] = '&ordm;';
        entities['187'] = '&raquo;';
        entities['188'] = '&frac14;';
        entities['189'] = '&frac12;';
        entities['190'] = '&frac34;';
        entities['191'] = '&iquest;';
        entities['192'] = '&Agrave;';
        entities['193'] = '&Aacute;';
        entities['194'] = '&Acirc;';
        entities['195'] = '&Atilde;';
        entities['196'] = '&Auml;';
        entities['197'] = '&Aring;';
        entities['198'] = '&AElig;';
        entities['199'] = '&Ccedil;';
        entities['200'] = '&Egrave;';
        entities['201'] = '&Eacute;';
        entities['202'] = '&Ecirc;';
        entities['203'] = '&Euml;';
        entities['204'] = '&Igrave;';
        entities['205'] = '&Iacute;';
        entities['206'] = '&Icirc;';
        entities['207'] = '&Iuml;';
        entities['208'] = '&ETH;';
        entities['209'] = '&Ntilde;';
        entities['210'] = '&Ograve;';
        entities['211'] = '&Oacute;';
        entities['212'] = '&Ocirc;';
        entities['213'] = '&Otilde;';
        entities['214'] = '&Ouml;';
        entities['215'] = '&times;';
        entities['216'] = '&Oslash;';
        entities['217'] = '&Ugrave;';
        entities['218'] = '&Uacute;';
        entities['219'] = '&Ucirc;';
        entities['220'] = '&Uuml;';
        entities['221'] = '&Yacute;';
        entities['222'] = '&THORN;';
        entities['223'] = '&szlig;';
        entities['224'] = '&agrave;';
        entities['225'] = '&aacute;';
        entities['226'] = '&acirc;';
        entities['227'] = '&atilde;';
        entities['228'] = '&auml;';
        entities['229'] = '&aring;';
        entities['230'] = '&aelig;';
        entities['231'] = '&ccedil;';
        entities['232'] = '&egrave;';
        entities['233'] = '&eacute;';
        entities['234'] = '&ecirc;';
        entities['235'] = '&euml;';
        entities['236'] = '&igrave;';
        entities['237'] = '&iacute;';
        entities['238'] = '&icirc;';
        entities['239'] = '&iuml;';
        entities['240'] = '&eth;';
        entities['241'] = '&ntilde;';
        entities['242'] = '&ograve;';
        entities['243'] = '&oacute;';
        entities['244'] = '&ocirc;';
        entities['245'] = '&otilde;';
        entities['246'] = '&ouml;';
        entities['247'] = '&divide;';
        entities['248'] = '&oslash;';
        entities['249'] = '&ugrave;';
        entities['250'] = '&uacute;';
        entities['251'] = '&ucirc;';
        entities['252'] = '&uuml;';
        entities['253'] = '&yacute;';
        entities['254'] = '&thorn;';
        entities['255'] = '&yuml;';
    }

    if (useQuoteStyle !== 'ENT_NOQUOTES') {
        entities['34'] = '&quot;';
    }
    if (useQuoteStyle === 'ENT_QUOTES') {
        entities['39'] = '&#39;';
    }
    entities['60'] = '&lt;';
    entities['62'] = '&gt;';

    // ascii decimals to real symbols
    for (decimal in entities) {
        if (entities.hasOwnProperty(decimal)) {
            hash_map[String.fromCharCode(decimal)] = entities[decimal];
        }
    }

    return hash_map;
}

function htmlentities(string, quote_style, charset, double_encode) {
    //  discuss at: http://phpjs.org/functions/htmlentities/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: nobbler
    // improved by: Jack
    // improved by: Rafał Kukawski (http://blog.kukawski.pl)
    // improved by: Dj (http://phpjs.org/functions/htmlentities:425#comment_134018)
    // bugfixed by: Onno Marsman
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //    input by: Ratheous
    //  depends on: get_html_translation_table
    //        note: function is compatible with PHP 5.2 and older
    //   example 1: htmlentities('Kevin & van Zonneveld');
    //   returns 1: 'Kevin &amp; van Zonneveld'
    //   example 2: htmlentities("foo'bar","ENT_QUOTES");
    //   returns 2: 'foo&#039;bar'

    var hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style),
        symbol = '';

    string = string == null ? '' : string + '';

    if (!hash_map) {
        return false;
    }

    if (quote_style && quote_style === 'ENT_QUOTES') {
        hash_map["'"] = '&#039;';
    }

    double_encode = double_encode == null || !!double_encode;

    var regex = new RegExp("&(?:#\\d+|#x[\\da-f]+|[a-zA-Z][\\da-z]*);|[" +
        Object.keys(hash_map)
            .join("")
            // replace regexp special chars
            .replace(/([()[\]{}\-.*+?^$|\/\\])/g, "\\$1") + "]",
        "g");

    return string.replace(regex, function (ent) {
        if (ent.length > 1) {
            return double_encode ? hash_map["&"] + ent.substr(1) : ent;
        }

        return hash_map[ent];
    });
}

/**
 *
 * @param birthMonth
 * @param birthDay
 * @param birthYear
 * @returns {number|*}
 */
function calculaIdade(birthMonth, birthDay, birthYear) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
        age--;
    }
    return age;
}

/**
 * Verifica se uma data no formato DD/MM/YYYY é válida.
 * @param stringData
 * @returns boolean
 * */

function validaDataBR (value) {
    var rule = /^\d{2}\/\d{2}\/\d{4}$/;
    if (rule.test(value)) {
        var date = value.split('/');
        var dayobj = new Date(date[2], date[1] - 1, date[0]);
        if ((dayobj.getDate() == date[0]) && (dayobj.getMonth() + 1 == date[1]) && (dayobj.getFullYear() == date[2])) {
            return true
        }
    }
    return false;
}


/**
 * Funções que convertem strings de datas entre os formatos BR e USA.
 * @param dtString
 * @returns {string}
 */

function converteStringDataBRToUSA(dtString){
    if (dtString) {
        dtString = dtString.split('/');
        return dtString[2] + '-' + dtString[1] + '-' + dtString[0];
    }

    return null;
}

function converteStringDataUSAToBR(dtString){
    if (dtString) {
        dtString = dtString.split('-');
        return dtString[2] + '/' + dtString[1] + '/' + dtString[0];
    }

    return null;
}

/**
 * Funções que convertem strings de datas entre os formatos BR e USA
 * @param dtString
 * @returns {string}
 */
function converteStringData(dtString, type) {
    dtString = dtString.split('/');
    if (type == 1) {
        //formato YYYY-mm-dd
        return dtString[2] + '-' + dtString[1] + '-' + dtString[0];
    }

    //formato dd-mm-YYYY
    return dtString[0] + '-' + dtString[1] + '-' + dtString[2];
}

/**
 * @param string url
 * @param boolean notify
 * @param string attrs
 * @returns {Window}
 */
function abrirPopup (url, notify, attrs) {
    attrs = attrs || 'width=800, height=600, top=100, left=100, scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no';

    if (typeof notify == 'undefined') {
        notify = true;
    }

    var win = window.open(url, '_blank', attrs);

    if (notify && !win) {
        $.pnotify({
            type: 'warning',
            title: 'Aviso',
            text: 'Desbloqueie a abertura de popups para continuar.'
        });
    }

    return win;
}

function getCorEvolucaoNota(st_status) {
    if (st_status === 'Aprovado') {
        return '#45BF55';
    } else if (st_status === 'Satisfatório') {
        return '#292BE7';
    } else if (st_status === 'Insatisfatório') {
        return '#D40D12';
    }
    return '';
}

function proximaDataVencimentoComAnoBissexto(dateStr, incMonths) {
    incMonths = typeof incMonths === 'undefined' ? 1 : parseInt(incMonths);

    var split = dateStr.substr(0, 10).split('/').reverse().join('-').split('-');

    // console.log(dateStr.toUTCString());
    // console.log(split);

    var day = parseInt(split[2]),
        month = parseInt(split[1]) + incMonths - 1,
        year = parseInt(split[0]);

    var date = new Date(year, month, day);

    while (date.getMonth() !== (month % 12)) {
        date.setDate(date.getDate() - 1);
    }

    return date.toJSON().substr(0, 10);
}

//verifica se um ano é bissexto
function isBissexto(ano) {
    if (((ano % 4) == 0 && (ano % 100) != 0) || (ano % 400) == 0)
        return true;
    else
        return false;
}

/**
 * Valida CNPJ
 * @param {string} cnpj
 * @returns {Boolean}
 */
function validaCNPJ(cnpj) {
    if (cnpj.length == 14) {
        var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
        digitos_iguais = 1;
        if (cnpj.length < 14 && cnpj.length < 15)
            return false;
        for (i = 0; i < cnpj.length - 1; i++)
            if (cnpj.charAt(i) != cnpj.charAt(i + 1))
            {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais)
        {
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0, tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
            {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
            {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1)) {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }else {
        return false;
    }

}

function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function datePtBr(date) {
    if (date instanceof Date) {
        date = date.toISOString().substr(0, 10);
    }
    return date.split('-').reverse().join('/');
}

function dateEnString() {
    var date = new Date();
    if (date instanceof Date) {
        date = date.toISOString().substr(0, 10);
    }
    return date;
}

/**
 * Retorna a data de hoje como string
 * @returns {*}
 */
function getTodayStringDate() {
    var date = new Date();
    return datePtBr(date);
}

/**
 * Metodo para limitar input
 * @param e
 * @param quantidade
 */
function limitarInput(e, quantidade){
    e.value = e.value.substring(0, quantidade);
}

function today(format) {
    format = format ? format : 'DB';

    switch (format) {
        case 'DB':
            return todayDB();
        case 'Br':
            return todayBr();
        default:
            break;
    }
}

function todayDB() {
    var today = new Date();
    return today.getFullYear()
        + '-' + (today.getMonth() < 9 ? '0' + (today.getMonth() + 1) : today.getMonth() + 1)
        + '-' + today.getDate();
}

function todayBr() {
    var today = new Date();
    return (today.getDate() < 10 ? '0' + today.getDate() : today.getDate())
        + '/' + (today.getMonth() < 9 ? '0' + (today.getMonth() + 1) : today.getMonth() + 1)
        + '/' + today.getFullYear();
}

function oneYearAgo(strDate) {
    var date = new Date(dateBrToDB(strDate));
    return (date.getDate() < 9 ? '0' + date.getDate() + 1 : date.getDate() + 1)
        + '/' + (date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1)
        + '/' + (date.getFullYear() - 1);
}

function dateBrToDB(strDate) {
    var day = strDate.substring(0, 2),
        month = strDate.substring(3, 5),
        year = strDate.substring(6);
    return new Date(parseInt(year) + '-' + month + '-' + day);

}

function getMensageiro(data) {
    var type, title, text;

    if (typeof data === "string") {
        data = {text: data};
    }

    if (typeof data === "object") {
        if ("responseJSON" in data) {
            data = data.responseJSON;
        }

        type = "type" in data ? data.type : null;
        title = "title" in data ? data.title : null;
        text = "text" in data ? data.text : null;
    }

    if (type || title || text) {
        return {
            type: type,
            title: title,
            text: text
        };
    }

    return null;
}

// verifica se existe texto digitado no editor TINYMCE
function verificaEntradaTinymce(texto) {
    var div = $('<div>');
    div.append(texto);
    if (div.find('p').text() == "") {
        return false;
    } else {
        return true;
    }
}
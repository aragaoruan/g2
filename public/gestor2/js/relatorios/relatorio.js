$(function () {

    $("#qntPorPagina").spinner({
        step: 10,
        max: 99999,
        min: 1
    });
    loaded();
    //Submit do formulário
    $("#formRelatorio").submit(function () {
        var params = $(this).serialize();
        var url = $(this).attr('action');
        var dataResponse;
        var gridContainer = $('#container-DataGrid');
        var id_funcionalidade = $('#id_funcionalidade').val();
        var RelatorioModel = Backbone.Model.extend({});
        var Columns;
        var qtdPorPag = $('#qntPorPagina').val();
        var totalRegistros;
        var page;
        var RelatorioCollection = Backbone.PageableCollection.extend({
            model: RelatorioModel,
            url: url,
            //mode: 'client',
            state: {
                pageSize: parseInt(qtdPorPag)
            },
            queryParams: {
                id_funcionalidade: id_funcionalidade,
                total: totalRegistros,
                data: params
            },
            parseState: function (response, queryParams, state, options) {
                if (response)
                    return {totalRecords: (response.total || 0)};
                else
                    return {totalRecords: 0};
            },
            parseRecords: function (response, options) {
                if (response) {
                    Columns = response.columns;
                    qtdPorPag = response.limit;
                    totalRegistros = response.total;
                    page = response.page;
                    return response.rows || response;
                } else {
                    return [];
                }
            },
            sync: function (method, model, options) {
                options.beforeSend = loading;
                options.complete = loaded;
                return Backbone.sync(method, model, options);
            }

        });

        function getFormParams() {

            var data = {};
            var form = $('#formRelatorio');

            var serialize = form.serializeArray();
            $.each(serialize, function (index, obj) {
                data[obj.name] = obj.value;
            });

            ////pega o serialize do formulário e monta um objeto com os parametros
            //var params = new Object();//cria um objeto
            ////percorre o objeto do serialize do formulario
            //$.each(this.$el.find("#formPesquisa").serializeArray(), function (i, param) {
            //    var nome = param.name;//recupera o nome do input
            //    var valor = param.value;//recupera o valor
            //
            //    var obj = $('#' + nome);
            //    if (obj.length) {
            //        if (obj.is(':checkbox') && obj.is(':checked')) {
            //            valor = 1;
            //        }
            //        params[nome] = valor;//monta o objeto
            //    }
            //});
            return data;//retorna
        }

        G2S.dataGridCollection = new RelatorioCollection();
        G2S.dataGridCollection.queryParams = $.extend({}, G2S.dataGridCollection.queryParams, getFormParams());
        G2S.dataGridCollection.fetch({
            //dataType: 'json',
            // type: 'post',
            url: url,
            //data: params,
            beforeSend: function () {
                loading();
            },
            success: function (data) {
                dataResponse = data;

                gridContainer.html('');
                var collection = dataResponse;
                if (totalRegistros > 0) {
                    $("#leftInfoForm").show();
                    $("#box-qtdRegistros").html(totalRegistros);
                    var grid = new Backgrid.Grid({
                        columns: Columns,
                        collection: collection,
                        className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover'
                    });
                    var paginator = new Backgrid.Extension.Paginator({
                        //columns: dataResponse.columns, alteracao para COM-11647, tornando a paginação de relatórios server-side
                        collection: collection
                    });
                    //renderiza o grid
                    gridContainer.append(grid.render().$el);
                    gridContainer.append(paginator.render().$el);
                } else {
                    $.pnotify({title: 'Aviso!', text: 'Sem resultado para exibição!', type: 'alert'});
                }
            },
            complete: function () {
                loaded();
            },
            error: function (data) {
                $.pnotify({title: 'Error!', text: 'Erro no banco de dados', type: 'error'});
            }
        });
        return false;
    });

    $('#btnPesquisar').click(function () {
        if (!validarCamposObrigatorios()) {
            return false;
        }
        return true;
    });

    $("#botaoImpressaoNormal").click(function (e) {
        e.preventDefault();
        if (validarCamposObrigatorios()) {
            var dataForm = $('#formRelatorio').serialize();
            var url = '/relatorio/impressao/?' + dataForm;
            window.open(url, '_blank');
        }
    });

    $("#botaoImpressaoXLS").click(function (e) {
        e.preventDefault();
        if (validarCamposObrigatorios()) {
            var dataForm = $('#formRelatorio').serialize();
            var url = '/relatorio/impressao/?xls=true&' + dataForm;
            window.open(url, '_blank')
        }
    });


    //disable fields children
    disableFieldsChildren();

    //trigger change event selects
    $("select").change(function () {
        getFieldsChildren(this);
    });
    /**
     * Get fields children by parent
     * @param {object} parentField
     * @returns {void}
     */
    function getFieldsChildren(parentField) {
        valueParentField = $(parentField).val();
        $('select[data-campopai=' + parentField.id + ']').each(function (i, el) {
            if ($(el).data('idself')) {
                setValueFieldsChildren(el, valueParentField);
            } else {
                throw 'Self ID undefined (id_camporelatorio not set in attribute "data-idself" on field).';
            }
        });
    }

    /**
     * @param {Object} field
     * @param {Integer} valueParentField
     * @returns {Void}
     */
    function setValueFieldsChildren(field, valueParentField) {
        //Verifica se passou o valor do campo
        if (valueParentField) {
            //Limpa o campo filho
            $(field).empty();
            $(field).append('<option value="">Carregando...</option>');
            //Seta os parametros para fazer o post
            var params = {
                idCampoRelatorio: $(field).data('idself'),
                valueParentField: parseInt(valueParentField),
                nameParentField: $(field).data('campopai')
            };
            //Faz o ajax
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/relatorio/get-dados-select-dinamico',
                data: params,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    //recupera o retorno
                    if (data) {
                        //Remove o atributo disable do campo filho
                        $(field).removeAttr('disabled');
                        $(field).html('<option value="">Selecione</option>');
                        //Escreve o resultado do json
                        for (var i = 0; i < data.length; i++) {
                            $(field).append('<option value="' + data[i].id + '">' + data[i].value + '</option>');
                        }
                    } else {
                        //Se não tiver valor no resultado
                        $(field).html('<option value="">Sem resultado</option>');
                    }
                },
                complete: function () {
                    loaded();
                },
                error: function (data) {
                    $.pnotify({title: 'Error!', text: 'Ops! Houve um erro inesperado!', type: 'error'});
                }
            });
        } else {
            disableFieldsChildren()
            $(field).attr('disabled', 'disabled');
        }
    }

    /**
     * each fields option and set disable fields children
     * @returns {void}
     */
    function disableFieldsChildren() {
        $('select[data-campopai]').each(function (i, e) {
            $(e).attr('disabled', 'disabled');
        });
    }

    function getCustonsSelect() {
        if ($("select.select2")) {
            $("select.select2").select2();
        }
    }

    /**
     * Valida o formulário
     * @returns {boolean}
     */
    function validarCamposObrigatorios() {
        var errors = [];
        $('#formRelatorio').find('[required]').each(function () {
            if (!$(this).val().trim())
                errors.push($(this).parent().parent().find('label.required').text());
        });
        if (errors.length) {
            $.pnotify({
                type: 'warning',
                title: 'Campos Obrigatórios',
                text: (errors.length > 1 ? 'Os campos ' : 'O campo ') + errors.join(',') + (errors.length > 1 ? ' são obrigatórios.' : ' é obrigatório')
            });
            return false;
        }

        return true;
    }

    getCustonsSelect();

    $('.relatorio_dt_min, .relatorio_dt_max').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true,
        clearBtn: true
    });

    $('.relatorio_dt_min').on('change', function () {
        $min = $(this);
        $max = $min.closest('form').find('input[name="' + $min.data('maximo') + '"]');

        var dt_min = $min.val();
        var dt_max = $max.val();

        if (dt_min) {
            $max.datepicker('setStartDate', $min.datepicker('getDate'));
        } else {
            $max.datepicker('setStartDate', null);
        }

        // Seta o valor padrão na segunda data caso possua um RANGE definido
        var range = $min.data('range');

        if (dt_min && range && !dt_max) {
            var dtProxima = proximaDataVencimento(dt_min, range, 'd');
            $max.val(formatarDataParaString(dtProxima));
        }

        validaRangeData(dt_min, dt_max, $min);
    });

    $('.relatorio_dt_max').on('change', function () {
        $max = $(this);
        $min = $max.closest('form').find('input[name="' + $max.data('minimo') + '"]');

        var dt_min = $min.val();
        var dt_max = $max.val();

        if (dt_max) {
            $min.datepicker('setEndDate', $max.datepicker('getDate'));
        } else {
            $min.datepicker('setEndDate', null);
        }

        validaRangeData(dt_min, dt_max, $max);
    });

    var exibida = false;

    /**
     * Valida a quantidade de dias entre as datas e notifica
     * @param dataInicial
     * @param dataFinal
     * @param $elem
     */
    function validaRangeData(dataInicial, dataFinal, $elem) {
        if (dataInicial && dataFinal) {
            var maxDias = $elem.data('range');
            var valorDiferenca = daysDiff(dataInicial, dataFinal);
            if (valorDiferenca > maxDias) {
                if (!exibida) {
                    $.pnotify({
                        type: 'warning',
                        title: 'Data Incorreta',
                        text: 'O intervalo entre as datas deve ser no máximo ' + maxDias + ' dias'
                    });
                    exibida = true;
                } else {
                    exibida = false;
                }

                var dtProxima;

                if ($elem.data('minimo')) {
                    dtProxima = proximaDataVencimento($('input[name="' + $elem.data('minimo') + '"]').val(), maxDias, 'd');
                    $elem.val(formatarDataParaString(dtProxima));
                }

                if ($elem.data('maximo')) {
                    dtProxima = proximaDataVencimento($elem.val(), maxDias, 'd');
                    $('input[name="' + $elem.data('maximo') + '"]').val(formatarDataParaString(dtProxima));
                }

            }
        }
    }

});
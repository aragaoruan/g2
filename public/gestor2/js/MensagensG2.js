/* 
 * Utilidade: Este arquivo tem objetivo de registrar e agrupar de forma organizada
 * as mensagens utilizadas no sistema, de forma a facilitar a manutenção e padronizar
 * o texto destas mensagens.
 */

/**
 * Import funcao sprintf
 * Utilidade: Possibilidade de reutilizacao de mensagens por substituicao de parametros.
 * Documentation: https://github.com/alexei/sprintf.js
 */
$.getScript('/js/sprintf.js');

MensagensG2 = {
    
    /**
     * Mensagens para sistema geral
     */
    MSG_SAVE_SUCCESS: 'Dados salvos com sucesso',
    MSG_OPTION_TRUE_CHECKED: 'A opção já encontra-se marcada',
    MSG_SAVE_ERROR: 'Erro ao tentar salvar os dados',
    MSG_CAMPO_OBRIGATORIO: 'O campo %s é obrigatório',
    MSG_ERROR: 'Desculpe, houve um erro no sistema',
    MSG_ERROR_CUSTOM: 'Desculpe, houve um erro ao tentar %',
    MSG_QUESTION_OPERATION: 'Tem certeza que deseja executar esta ação?',
    
    /**
     * Mensagens para sistema de ocorrencia
     */
    MSG_OCORRENCIA_01: 'A opção já encontra-se marcada',
    MSG_OCORRENCIA_02: 'Se utilizar data na pesquisa deverá ser preenchido os dois campos',
    MSG_OCORRENCIA_03: 'Não foram encontradas ocorrências com os parâmetros informados',
    MSG_OCORRENCIA_04: 'Não foram encontrados assuntos para esta função',
    MSG_OCORRENCIA_05: 'Não foram encontrados subassuntos para este assunto',
    MSG_OCORRENCIA_06: 'Não foram encontradas funções neste núcleo',
    MSG_OCORRENCIA_07: 'Não foram encontrados atendentes com esta função',
    MSG_OCORRENCIA_08: 'Não foram encontradas funções nesta entidade',
    MSG_OCORRENCIA_09: 'Tem certeza de que deseja %s esta ocorrência?',
    MSG_OCORRENCIA_10: 'Ocorrência %s com sucesso',
    MSG_OCORRENCIA_11: 'Erro ao %s ocorrência',
    MSG_OCORRENCIA_12: 'Visualização de trâmite salva com sucesso',
    MSG_OCORRENCIA_13: 'Erro ao tentar alterar visibilidade',
    MSG_OCORRENCIA_14: 'Ocorrência atribuída a %s',
    MSG_OCORRENCIA_15: 'Erro ao tentar atribuir ocorrência',
    MSG_OCORRENCIA_16: 'Encaminhamento realizado com sucesso',
    MSG_OCORRENCIA_17: 'Erro na ação de encaminhar ocorrência',
    MSG_OCORRENCIA_18: 'Cópia realizada com sucesso',
    MSG_OCORRENCIA_19: 'Erro na ação de copiar ocorrência',
    MSG_OCORRENCIA_20: 'Não foram encontrados atendentes para este assunto',
    MSG_OCORRENCIA_21: 'Solicitação de encerramento efetuada com sucesso',
    MSG_OCORRENCIA_22: 'Erro ao solicitar encerramento ocorrência',
    MSG_OCORRENCIA_23: 'Tem certeza que deseja tornar esta interação visível para o interessado?',
    MSG_OCORRENCIA_24: 'Tem certeza que deseja tornar esta interação não visível para o interessado?',
    MSG_OCORRENCIA_25: 'Visível ao Interessado',
    MSG_OCORRENCIA_26: 'Texto da Interação',
    MSG_OCORRENCIA_27: 'Um trâmite foi gerado para esta ação',
    MSG_OCORRENCIA_28: 'Houve um erro ao tentar gerar o trâmite',
    MSG_OCORRENCIA_29: 'A interação Nº %s, gerada na data %s, foi alterada por %s para %s',
    MSG_OCORRENCIA_30: 'visível',
    MSG_OCORRENCIA_31: 'não visível',
    MSG_OCORRENCIA_32: '%s ocorrência(s) encontrada(s)',
    MSG_OCORRENCIA_33: 'Já existe um usuário atendendo essa ocorrência',
    MSG_OCORRENCIA_34: 'Você fixou os dados do(a) aluno(a) %s',
    MSG_OCORRENCIA_35: 'Situação alterada para "Em andamento".',
    MSG_OCORRENCIA_36: 'Erro ao tentar alterada a situacao da ocorrência.',
    MSG_OCORRENCIA_37: "Erro ao tentar gerar a url do botão acessar como aluno.",

   /**
    * Mensagens para funcionalidade de prr
    */
    MSG_PRR_01: 'Não foram encontradas salas de PRR disponiveis para este aluno',
    MSG_PRR_02: 'Nenhuma sala foi selecionada',
    MSG_PRR_03: 'Nova sala de PRR salva com sucesso',
    MSG_PRR_04: 'Tem certeza que deseja apagar esta sala',
    MSG_PRR_05: 'Erro ao tentar apagar sala de aula',
    MSG_PRR_06: 'Sala de aula %s apagada com sucesso',
    MSG_PRR_07: 'O campo data deve ser preenchido',
    MSG_PRR_08: 'Sala já selecionada contem uma disciplina, exclua a sala marcada e então escolha novamente a sala',
    MSG_PRR_09: 'Não foram encontrados os dados obrigatórios [Matricula e Ocorrencia] para busca dos dados de PRR',
    MSG_PRR_10: 'A venda para esta sala já foi confirmada, por tanto não podem ser adicionadas novas salas',
    MSG_PRR_11: 'Tem certeza que deseja concluír esta venda?',
    MSG_PRR_12: 'Venda concluída com sucesso',
    MSG_PRR_13: 'Esta venda já se encontra na evolução (CONFIRMADA)',
    MSG_PRR_14: 'Deseja realmente apagar esta parcela?',
    MSG_PRR_15: 'Deseja realmente enviar uma mensagem de cobrança?',
    MSG_PRR_16: 'Mensagem de cobrança enviada com sucesso',
    MSG_PRR_17: 'Erro ao enviar a mensagem de cobrança',
    MSG_PRR_18: 'O valor para parcelamento já foi totalmente útilizado',
    MSG_PRR_19: 'Informe o numero de parcelas',
    MSG_PRR_20: 'Selecione um meio de pagamento',
    MSG_PRR_21: 'Informe o valor da parcela',
    MSG_PRR_22: 'Informe a data de vencimento da parcela',
    MSG_PRR_23: 'A parcela de numero %s excede o valor, e foi alterada para o valor restante',
    MSG_PRR_24: 'Erro ao tentar carregar tipos de pagamento',
    MSG_PRR_25: 'Deseja salvar as parcelas criadas?',
    MSG_PRR_26: 'Nenhuma nova parcela foi adicionada',
    MSG_PRR_27: 'Erro ao tentar salvar lançamentos',
    MSG_PRR_28: 'O pagamento do PRR foi abonado. Motivo: %s',
    MSG_PRR_29: 'Venda abonada com sucesso',
    MSG_PRR_30: 'Um trâmite foi gerado para esta ação',
    MSG_PRR_31: 'Um erro ocorreu ao tentar concluir a venda',


    /**
    * MSG_ENCERRAMENTO_SUCESSO
    */
    MSG_ENCERRAMENTO_SUCESSO: "Encerramento salvo com sucesso.",
    
    /**
     * MSG_DEFESA_TCC
     */
    MSG_DEFESA_TCC_01: 'Lançamentos salvos com sucesso',
    MSG_DEFESA_TCC_02: 'Não há nenhum lançamento para ser salvo',
    MSG_DEFESA_TCC_03: 'Erro ao tentar salvar lançamentos',
    MSG_DEFESA_TCC_04: 'Não foram encontrados dados conforme filtros informados na pesquisa',


    /**
     * Mensagens TRANSFERENCIA
     */
    MSG_TRANSFERENCIA_SUCESSO: "Transferência realizada com sucesso.",

    /**
     * Mensagens INTEGRAÇÃO
     */
    MSG_SINCRONIZACAO_SUCESSO: 'Sincronização concluída com sucesso.',
    MSG_SINCRONIZACAO_WARNING: 'Não há nenhum dado para sincronizar.',

    /**
     * Mensagens PROJETO PEDAGÓGICO
     */
    ERRO_NOME_EXISTENTE_PROJETO_PEDAGOGICO: 'Não é possível adicionar dois projetos com o mesmo nome',
    
    /**
     * Mensagens GERENCIA NOTAS
     */
    MSG_GERENCIA_NOTAS_01: 'Valor inserido no campo nota inválido!',
    MSG_GERENCIA_NOTAS_02: 'Valor inserido no campo nota é maior que %s'


    /**
     * Mensagem para FUNCIONALIDADE ENTREGA DE DOCUMENTOS
     * 15/10/2014 <denise.xavier@unyleya.com.br>
     */
    , MSG_ENTREGA_DOCUMENTO_01 : 'Deseja trocar a situação deste documento para %s?',
    MSG_ENTREGA_DOCUMENTO_02 : 'Entrega de documentos salva com sucesso.',
    MSG_ENTREGA_DOCUMENTO_03 : 'Erro ao salvar entrega de documentos.',
    MSG_ENTREGA_DOCUMENTO_04 : 'Escolha uma opção válida para alteração.'


    /**
     * Mensagem para ABA REVISÃO DA FUNCIONALIDADE ENTREGA DE DOCUMENTOS
     * 03/11/2014 <denise.xavier@unyleya.com.br>
     */
    , MSG_ENTREGA_DOCUMENTO_05 : 'Tem certeza que deseja alterar a condição de documento do aluno para %s?'
    , MSG_ENTREGA_DOCUMENTO_06 : 'Alteração de documentos da matrícula feita com sucesso.'
    , MSG_ENTREGA_DOCUMENTO_07 : 'Ocorreu um erro ao fazer a alteração de documentos da matrícula.'
    /**
     * Mensagem para ABA REVISÃO DA FUNCIONALIDADE ENTREGA DE DOCUMENTOS -> marcação academica
     * 22/07/2015 <denise.xavier@unyleya.com.br>
     */
    , MSG_ENTREGA_DOCUMENTO_08 : 'Tem certeza que deseja alterar a condição acadêmica para certificação do aluno para %s ?'
    , MSG_ENTREGA_DOCUMENTO_09 : 'Marcação acadêmica para certificação da matrícula feita com sucesso.'
    , MSG_ENTREGA_DOCUMENTO_10 : 'Ocorreu um erro ao fazer a marcação acadêmica para certificação da matrícula.'


    /**
     * Mensagem para funcionalidade CONCLUSAO
     * 15/02/2015 <rafael.oliveira@unyleya.com.br>
     */
    , MSG_CONCLUSAO_03 : 'As informações da matricula %s não puderam ser salvas, verifique os campos obrigatórios'
    , MSG_CONCLUSAO_04 : 'Não há informações para serem salvas'
    , MSG_CONCLUSAO_05 : 'Informações salvas com sucesso'
    , MSG_CONCLUSAO_06 : 'Existem processos que não foram salvos, verifique as indicações no formulário'
    , MSG_CONCLUSAO_07 : 'Vinculos realizados com sucesso!'
    , MSG_CONCLUSAO_08 : "Erro ao realizar vínculos. <br /> Verifique se está logado no sistema ou se há duplicidade de disciplinas entre as matriculas mescladas"

    /**
     * Mensagem para funcionalidade DIVIDIR SALAS
     * 15/02/2015 <rafael.oliveira@unyleya.com.br>
     */
    , MSG_DIVIDIR_SALA_01: 'Escolha ao menos um projeto pedagogico'
    , MSG_DIVIDIR_SALA_02: 'Divisão de sala executado com sucesso!'
    , MSG_DIVIDIR_SALA_03: 'Escolha uma sala de destino e uma sala de origem'
    , MSG_DIVIDIR_SALA_04: 'Informe a quantidade de alunos'

    /**
     * Mensagem para funcionalidade DISCIPLINA
     * 30/06/2015 <rafael.oliveira@unyleya.com.br>
     */
    , MSG_DISCIPLINA_01: 'Conteúdo enviado com sucesso!'
    , MSG_DISCIPLINA_02: 'Conteúdo já enviado!'
    , MSG_DISCIPLINA_03: 'Erro ao tentar enviar um ou mais conteúdos!'
    ,
    MSG_DISCIPLINA_04: 'Extensão não permitida!'


    ,
    MSG_DIPLOMACAO_01: 'Nenhuma alteração selecionada. Verifique as marcações.'
    ,
    MSG_DIPLOMACAO_02: 'Erro ao executar operação.'
    ,
    MSG_DIPLOMACAO_03: 'Selecione pelo menos uma opção para prosseguir.'
    ,
    MSG_DIPLOMACAO_04: 'Verifique se o seu navegador permite abertura de pop-ups e tente novamente.'
    ,
    MSG_DIPLOMACAO_05: 'A data de Enviado ao Pólo do Aluno não deve ser vazia.'
    ,
    MSG_DIPLOMACAO_06: 'A data de Entregue ao Aluno não deve ser vazia.'
    ,
    MSG_DIPLOMACAO_07: 'A data de Enviado ao Pólo do Aluno  não deve ser MENOR que a data Retorno do Registro e/ou não deve ser MAIOR que a data de Entregue ao Aluno.'
    ,
    MSG_DIPLOMACAO_08: 'A data de Entregue ao Aluno não deve ser menor que a data Enviado ao Pólo do Aluno.'

    ,
    MSG_COLACAO_01: 'Nenhuma alteração disponível.'
    ,
    MSG_COLACAO_02: 'Erro ao executar operação.'
    ,
    MSG_COLACAO_03: 'Dados salvos com sucesso.'
    ,
    MSG_COLACAO_04: 'Esta Data não é válida.'

    ,
    MSG_CONCEITO: 'Escolha Concluido ou Não Concluido'

};


/**
 * Funcao que monta Comboboxes <select>
 * @param {Object} params
 * @param params.el                     // Elemento tag SELECT
 * @param params.model                  // Variavel Backbone.Model.extend()
 * @param params.url                    // URL para fetch, usado somente se nao passar collection
 * @param params.collection             // Se já possuir a collection, nao sera necessario fazer o fetch
 * @param params.optionLabel            // Nome do indice (atributo) para aparecer na tag option
 * @param params.optionLabelConstruct   // Caso o label do option nao seja somente um atributo do model, definir qual string retornar
 * @param params.optionSelectedId       // Definir o option com X id como selected
 * @param params.emptyOption            // String para o primeiro option ser vazio, string com o label do option
 * @param params.parse                  // Parse para o collection
 * @param params.childViewOptions       // Options para cada <option>
 * @param params.onChange               // Função executada após selecionar um novo valor no combo
 * @param fn($el, CompositeView)        // Funcao para executar ao terminar tudo
 * @return CompositeView
 */
function ComboboxView(params, fn) {
    fn = fn || function () {
    };

    var defaults = {
        optionLabel: "",
        optionLabelConstruct: false,
        optionSelectedId: null,
        parse: function (response) {
            if (response.type && response.type !== "success") {
                $.pnotify(response);
                return [];
            }
            return response.mensagem || response;
        },
        childViewOptions: {},
        onChange: function () {
        }
    };

    params = $.extend({}, defaults, params);
    params.el = $(params.el);

    var Collection = Backbone.Collection.extend({
        model: params.model,
        url: params.url,
        parse: params.parse
    });

    params.childViewOptions = $.extend({}, {
        model: new params.model(),
        tagName: "option",
        render: function () {
            if ("onBeforeRender" in this) {
                this.onBeforeRender();
            }

            var html = "";

            if (typeof params.optionLabelConstruct === "function") {
                html += params.optionLabelConstruct(this.model);
            } else {
                html += this.model.get(params.optionLabel);
            }

            this.el.setAttribute("value", this.model.id);
            this.$el.attr("title", html);
            this.el.innerHTML = html;

            if ("onRender" in this) {
                this.onRender();
            }

            return this;
        }
    }, params.childViewOptions);

    var ItemView = Marionette.ItemView.extend(params.childViewOptions);

    var CompositeView = Marionette.CompositeView.extend({
        el: params.el,
        template: _.template(""),
        collection: new Collection(),
        childView: ItemView,
        events: {
            change: function () {
                var that = this;
                var model = that.collection.find(function (item) {
                    return item.id == that.$el.val();
                });
                params.onChange.call(that, model);
            }
        },
        initialize: function () {
            var that = this;
            //debug(params.el);
            if (params.collection) {
                this.collection.add(params.collection);
                this.render().$el.val(params.optionSelectedId);
                fn(params.el, that);
            } else {
                this.collection.fetch({
                    beforeSend: loading,
                    complete: loaded,
                    success: function () {
                        that.render().$el.val(params.optionSelectedId);
                        fn(params.el, that);
                    },
                    error: function (collection, error) {
                        if (error.responseJSON) {
                            $.pnotify(error);
                        } else {
                            $.pnotify({
                                type: "error",
                                title: "Erro",
                                text: "Houve um erro não identificado ao carregar os dados."
                            });
                        }

                        if (window.console && window.console.error) {
                            window.console.error(error);
                        }
                    }

                });
            }
        },
        onRender: function () {
            if (params.emptyOption) {
                this.$el.prepend("<option value=''>" + params.emptyOption + "</option>");
            }
        }
    });

    return new CompositeView();
}
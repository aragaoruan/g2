function in_array(needle, haystack, argStrict) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true
    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '',
        strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}


function comboDinamico(url, idCampo, params) {
    $.getJSON(url, params, function (dados) {
        var options = '<option value="">--</option>';
        for (var i = 0; i < dados.length; i++) {
            options += '<option value="' + dados[i].id_value + '">' + dados[i].st_text + '</option>';
        }
        $('#' + idCampo).html(options).show();
    });
}

/**
 * Valida CPF
 * @param {string} cpf
 * @returns {Boolean}
 */
function validaCPF(cpf) {
    cpf = cpf.replace(/[^0-9]/g, "");
    var soma;
    var resto;
    soma = 0;
    if ((cpf == '11111111111') || (cpf == '22222222222') || (cpf ==
        '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') ||
        (cpf == '66666666666') || (cpf == '77777777777') || (cpf ==
        '88888888888') || (cpf == '99999999999') || (cpf == '00000000000')) {
        return false;
    } else {
        for (i = 1; i <= 9; i++) {
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
        }
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(9, 10)))
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(10, 11)))
            return false;

        return true;
    }
}

//Valida CNPJ
function validaCnpj(cnpj) {
    cnpj = cnpj.replace(/[^0-9]/g, '');

    var retorno = true;
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999") {
        retorno = false;
    }

    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0, tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0)) {
        retorno = false;
    }

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1)) {
        retorno = false;
    }

    return retorno;
}

/**
 * Testa se o valor Ã© um nÃºmero atravÃ©s da expressÃ£o regular
 * @param {mixed} valor
 * @returns {Boolean}
 */
function validaNumero(valor) {
    var er = /^[0-9]+$/;
    return er.test(valor);
}

/**
 * Formata valor float para string no formato #.###,##
 * @param float valor
 * @returns {string}
 */
function formatValueToShow(valor) {
    var tmp = valor + '';
    tmp = tmp.replace(".", ",");
    if (tmp.length > 6) {
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }
    return tmp;
}

/**
 * Formata valor para float
 * @param string value
 * @returns {float}
 */
function formatValueToReal(value) {
    // Pegando valor monetario para conversao
    var money = new String(value);
    // Valor armazenado em array
    money = money.split(",");
    if (money[1]) {
        // Valor Real
        var real = money[0];

        // Valor Decimal
        var decimal = money[1];

        // Incremento de zero na casa decimal se necessario
        decimal = (decimal.length == 1) ? decimal + '0' : decimal.substring(0, 2);
        //monta o retorno
        money = real.replace('.', '') + '.' + decimal;

    }
    // Retornando valor
    return parseFloat(money);
}

/**
 * Calcula a proxima data a partir de uma data passada
 * @param value data inicial formato dia/mÃªs/ano
 * @param increment quantidade de dias, mÃªs ou ano a ser incrementando
 * @param local posiÃ§Ã£o em que serÃ¡ incremetado, d = dia, m = mÃªs, y = ano
 * @returns {Date}
 */
function proximaDataVencimento(value, increment, local) {

    //quebra a string pelas / (barras)
    var split = value.split('/');
    //pega as posiÃ§Ãµes e monta uma nova string para data
    var newDate = split[2] + '/' + split[1] + '/' + split[0];

    //cria uma nova data
    var data = new Date(newDate);

    //forÃ§a a variavel incremente a ser um inteiro
    increment = parseInt(increment);

    //verifica os locais passados para incrementar
    if (local == 'd') {
        data.setDate(data.getDate() + increment);
    }

    if (local == 'm') {
        data.setMonth(data.getMonth() + increment);
    }

    if (local == 'y') {
        data.setFullYear(data.getFullYear() + increment);
    }
    return data;
    //monta a string de retorno
//    return ((parseInt(data.getDate()) >= 10) ? data.getDate() : "0" + data.getDate()) + '/' + ((parseInt(data.getMonth()) >= 10) ? data.getMonth() + 1 : '0' + (data.getMonth() + 1)) + '/' + data.getFullYear();
}
/**
 * Formata uma data com hora ou sem
 * @param $data data no formato pt_br para en_us com hora ou sem
 * @returns {Date}
 */
function formatarData($data) {
    var split = $data.split('/');
    if (split) {
        //pega a hora
        var splitHora = split[2].split(" ");
        var dia = split[0];
        var mes = split[1];
        var ano = splitHora[0];
        var hora = splitHora[1] ? splitHora[1] : null;
    }
    var date = ano + "-" + mes + "-" + dia;
    var dt_return = new Date(date);
    return dt_return;
}

/**
 * Formata uma instancia de Date para string
 * @param {Date} date
 * @returns {string}
 */
function formatarDataParaString(date) {
    var d = parseInt(date.getDate());
    var m = parseInt(date.getMonth());
    var y = parseInt(date.getFullYear());
    return ((d >= 10) ? d : "0" + d) + '/' + (((m + 1) >= 10) ? m + 1 : '0' + (m + 1)) + '/' + y;
}


/**
 * Calcula a diferenÃ§a entre duas datas
 * @param $dt_inicio data inicio no formato pt_br
 * @param $dt_fim data fim no formato pt_br
 * @returns {Number}
 */
function diferencaEntreDatas($dt_inicio, $dt_fim) {
    var dt_inicio = formatarData($dt_inicio);
    var dt_fim = formatarData($dt_fim);
    var datediff = Math.abs(dt_inicio.getTime() - dt_fim.getTime()); // difference
    return parseInt(datediff / (24 * 60 * 60 * 1000), 10); //Convert values days and return value
}

/**
 * FunÃ§Ã£o para comparar duas datas, envie os dados caputados nos inputs
 *
 * retorno
 * data 1 > data 2 => -1
 * data 1 == data 2 => 0
 * data 1 < data 2 => 1
 *
 * @param str1
 * @param str2
 * @returns {number}
 */
function compareDate(str1, str2) {
    var dt1 = parseInt(str1.substring(0, 2));
    var mon1 = parseInt(str1.substring(3, 5));
    var yr1 = parseInt(str1.substring(6, 10));
    var date1 = new Date(yr1, mon1 - 1, dt1);

    var dt2 = parseInt(str2.substring(0, 2));
    var mon2 = parseInt(str2.substring(3, 5));
    var yr2 = parseInt(str2.substring(6, 10));
    var date2 = new Date(yr2, mon2 - 1, dt2);

    if (date1 > date2) {
        return -1;
    } else if (date1 == date2) {
        return 0;
    } else {
        return 1;
    }

}

/**
 *
 * @param birthMonth
 * @param birthDay
 * @param birthYear
 * @returns {number|*}
 */
function calculaIdade(birthMonth, birthDay, birthYear) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
        age--;
    }
    return age;
}

//verifica se um ano é bissexto
function isBissexto(ano) {
    if (((ano % 4) == 0 && (ano % 100) != 0) || (ano % 400) == 0)
        return true;
    else
        return false;
}

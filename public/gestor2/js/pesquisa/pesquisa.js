/**
 * Eh extremamente importante que nao exista no sistema uma variavel o mesmo nome. Obrigado.
 * Qualquer duvida: caio.teixeira@unyleya.com.br
 * @var PesquisaGenericaCache
 */
if (!PesquisaGenericaCache)
    var PesquisaGenericaCache = {};

/**
 * Model generica para backgrid
 * @type Model
 * @exp;Backbone
 * @pro;Model
 * @call;extend
 */
PesquisaModelo = Backbone.Model.extend({
    defaults: function () {
        var arrDefaults = new Object();//cria um objeto vazio
        //Pega o ColunasGrid que é gerado no phtml com a grid que sera escrito na grid
        if (typeof ColunasGrid != 'undefined') {
            //Percorre o ColunasGrid e seta os objetos com os itens para montar o default da model
            $.each(ColunasGrid, function (i, item) {
                var itemName = item.name;//pega o name
                arrDefaults[itemName] = '';//atribui o valor vazio no objeto
            });
        }
        return arrDefaults;//retorna o objeto para montar o defaults
    },
    setUrl: function () {
        //alterado nome do metodo para setUrl, que mudara a url da model
        var attr = G2S.funcionalidadeAtual.get('url');//pega a url da funcionalidade atual
        var urlStr = null;
        if (attr) {
            urlStr = this.id ? '/api' + attr + '/' + this.id : '/api' + attr;//monta a url
        } else {
            throw 'A ClasseFlexBotao dessa funcionalidade precisa ser configurada';
        }
        this.url = urlStr;
//        return url;
    }
});

/**
 * Backgrid pageable
 * @exp;Backbone
 * @pro;PageableCollection
 * @call;extend
 */
var PesquisaCollection = Backbone.PageableCollection.extend({
    model: PesquisaModelo,
    url: "/pesquisa/pesquisar",
    state: {
        pageSize: 15
    },
    //initialize: function(options) {
    //    this.queryParams = $.extend({}, this.queryParams, options);
    //},
    //mode: "client" // define que a paginação será no lado client
    parseState: function (response, queryParams, state, options) {
        if (response)
            return {totalRecords: (response.total || 0)};
        else
            return {totalRecords: 0};
    },
    parseRecords: function (response, options) {
        if (response) {
            return response.rows || response;
        } else {
            return [];
        }
    },
    sync: function (method, model, options) {
        options.beforeSend = loading;
        options.complete = loaded;
        return Backbone.sync(method, model, options);
    }
});

/**
 * @type Backgrid Cell
 * @exp;Backgrid
 * @pro;Cell
 * @call;extend
 */
AbreRegistro = Backgrid.Cell.extend({
    template: _.template("<a href='<%=to%>/editar/<%=id%>' class='btn btn-small btn-inverse edit'><i class='icon icon-white icon-pencil'></i> Editar</a> <a class='btn btn-small btn-danger delete'><i class='icon icon-white icon-trash'></i> Apagar</a> <a class='btn btn-small btn-success historico' rel='tooltip' data-trigger='hover' data-placement='top' data-toggle='popover' data-content='Exibe o histórico de alterações.' data-original-title='Histórico de Alterações'><i class='icon icon-white icon-list-alt'></i></a>"),
    ui: {
        btnHistorico: '.historico'
    },
    events: {
        "click .historico": "historicoFinanceiro",
        "click .edit": "abreLinha",
        "click .delete": "deleteLinha"
    },
    abreLinha: function (e) {
        e.preventDefault();
        G2S.editModel = this.model;
        var tipoCadastro = G2S.funcionalidadeAtual.get('classeFlexBotao');
        G2S.router.navigate("#" + tipoCadastro + '/editar/' + this.model.id, {trigger: true});
    },
    deleteLinha: function (e) {
        var that = this;
        that.model.setUrl();//pega a url da modelo para excluir
        //Recupera os atributos da DOM
        var tipoCadastro = G2S.funcionalidadeAtual.get('classeFlexBotao');
        var hasmigrated = G2S.funcionalidadeAtual.get('migrada');
        bootbox.confirm('Tem certeza de que deseja apagar o registro?', function (result) {
            if (result) { // return do bootbox
                if (hasmigrated) {//Se a funcionalidade for para html
                    that.model.destroy({
                        wait: true,
                        success: function (model, response) {
                            $.pnotify({
                                title: "Sucesso!",
                                text: "Registro removido com sucesso!",
                                type: "success"
                            });
                            $(that.el).parent('tr').remove();
                        },
                        error: function (model, response) {
                            $.pnotify({
                                title: "Erro ao remover!",
                                text: "Erro ao tentar remover registro. " + response.responseText,
                                type: "error"
                            });
                        }
                    });
                }

            }
        });
    },
    showBtnDelete: function () {
        if (G2S.funcionalidadeAtual != undefined) {
            return G2S.funcionalidadeAtual.get('bl_delete');
        } else {
            return true;
        }

    },
    showBtnHistorico: function () {
        return G2S.funcionalidadeAtual.get('id_funcionalidade') == 286;
    },
    render: function () {
        var tipoTO = G2S.funcionalidadeAtual.get('classeFlexBotao');
        this.$el.css('width', '160px');
        this.$el.html(this.template({"id": this.model.id, "to": tipoTO}));
        this.delegateEvents();
        //hide btn .delete
        if (!this.showBtnDelete()) {
            this.$el.find('.delete').hide();
        }

        if (!this.showBtnHistorico()) {
            this.$el.find('.historico').hide();
        }
        //habilita o popover do historico de alocação
        this.$el.find('.historico').popover();

        return this;
    },
    historicoFinanceiro: function (event) {
        event.preventDefault();

        /**
         * Cria a modal de Histórico Financeiro
         */
        $('#container-modal-historico-financeiro').empty();
        var customModal = $(
            '<div class="modal hide fade" id="modal-historico-financeiro" tabindex="-1" role="dialog" aria-labelledby="Histórico de Alterações" aria-hidden="false">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title" id="myModalLabel">Histórico de alterações:</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div id="container-grid-historico-financeiro" class="control-group">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        $('#container-modal-historico-financeiro').append(customModal);

        $('.modal .hide').show();
        $("#modal-historico-financeiro").modal('show');

        //inicio da busca dos históricos da alocação
        var modelHistorico = Backbone.Model.extend({});
        var DataGridHistorico = Backbone.PageableCollection.extend({
            model: modelHistorico,
            url: "/",
            state: {pageSize: 15},
            mode: "client"
        });

        var urlHistorico = '/log/retorna-historico-financeiro';
        var dataResponseHistorico;
        $.ajax({
            dataType: 'json',
            type: 'get',
            url: urlHistorico,
            data: {id_entidade: this.model.id},
            beforeSend: function () {
            },
            success: function (data) {
                dataResponseHistorico = data;
            },
            complete: function () {
                var dataGridHistorico = new DataGridHistorico(dataResponseHistorico);
                var columnsHistorico = [{
                    name: "dt_cadastro",
                    label: "Data",
                    editable: false,
                    cell: "string"
                },
                    {
                        name: "st_campo",
                        label: "Campo",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_depois",
                        label: "Alterado para",
                        editable: false,
                        cell: "string"
                    },
                    {
                        name: "st_usuario",
                        label: "Responsável",
                        editable: false,
                        cell: "string"
                    }
                ];
                $("#container-grid-historico-financeiro").html('');
                if (dataGridHistorico.length > 0) {
                    var pageableGridHistorico = new Backgrid.Grid({
                        columns: columnsHistorico,
                        collection: dataGridHistorico,
                        className: 'backgrid backgrid-selectall table table-bordered table-hover'
                    });
                    $("#container-grid-historico-financeiro").append(pageableGridHistorico.render().$el);
                    var paginatorHistorico = new Backgrid.Extension.Paginator({
                        collection: dataGridHistorico
                    });


                    $("#container-grid-historico-financeiro").append(paginatorHistorico.render().$el);


                } else {
                    $("#container-grid-historico-financeiro").html('Não há histórico financeiro!');
                }
                $('#modal-historico-financeiro').width('50%');
                loaded();
            }
        });
        //fim da busca dos históricos da alocação
    }
});

PesquisaView = Marionette.ItemView.extend({
    template: function () {
        return $('#pesquisa-template').html();
    },
    ui: {
        tipo: '#tipo'
    },
    initialize: function () {
        thatPesquisa = this;
    },
    onShow: function () {
        if (G2S.funcionalidadeAtual) {
            var cache = PesquisaGenericaCache[G2S.funcionalidadeAtual.get('classeFlexBotao')] || null;

            if (paramUrl != null) {
                this.setParamsUrl(this, paramUrl)
            } else if (cache) {
                this.setParamsUrl(this, cache);
            } else if (G2S.dataGridCollection != null) {
                this.setParamsUrl(this, $.param(G2S.dataGridCollection.queryParams))
            }
        }
    },
    onRender: function () {
        var that = this;

        if (!G2S.funcionalidadeAtual) { //caso seja um acesso direto à url ou um refresh da url
            var hash = this.ui.tipo.val();
            G2S.funcionalidadeAtual = G2S.pegaFuncionalidade('classeFlexBotao', hash);
        }

        //if (!G2S.dataGridCollection)
        G2S.dataGridCollection = new PesquisaCollection();
        //G2S.dataGridCollection.queryParams = $.extend({}, G2S.dataGridCollection.queryParams, that.getFormParams());

        return this;
    },
    //setParams
    setParamsUrl: function (context, paramUrlContext) {
        var pairs = paramUrlContext.slice(0).split('&');

        var result = {};
        pairs.forEach(function (pair) {
            pair = pair.split('=');
            result[pair[0]] = "#" + pair[0];

            var objJquery = $("#" + decodeURIComponent(pair[0]));

            objJquery.val(decodeURIComponent(pair[1].replace(/\+/g, '%20')));

            if (objJquery.is(':checkbox')) {
                if (objJquery.val() == 0)
                    objJquery.removeAttr('checked');
                else
                    objJquery.attr('checked', 'checked');
            }
        });
        //removido para evitar que a pesquisa fosse realizada automaticamente ao clicar em voltar a pesquisa(TEC-390)
        //if(G2S.rotaAtual.equals == true)
        //$('form#formPesquisa').trigger('submit');
    },
    addNewAction: function () {
        G2S.router.navigate("#" + G2S.funcionalidadeAtual.get('classeFlexBotao') + '/novo', {trigger: true});
    },
    submitForm: function () {
        var that = this;

        var firstSubmit = 1;

        // Limpar a grid
        that.$el.find("#container-grid").empty();

        // Pega os parametros dos campos e adiciona aos parametros do request
        G2S.dataGridCollection.queryParams = $.extend({}, G2S.dataGridCollection.queryParams, that.getFormParams());

        var queryParams = $.extend({}, G2S.dataGridCollection.queryParams);
        var indexRemove = ['actionMethod', 'grid', 'to', 'tipo', 'currentPage', 'pageSize', 'totalPages', 'totalRecords', 'directions', 'order', 'sortKey'];

        $.each(indexRemove, function (index, value) {
            delete queryParams[value]
        });

        var queryString = $.param(queryParams);
        history.pushState("", null, hashOriginal + '/' + queryString);
        PesquisaGenericaCache[G2S.funcionalidadeAtual.get('classeFlexBotao')] = queryString;

        var pageableGrid = new Backgrid.Grid({
            columns: ColunasGrid,
            collection: G2S.dataGridCollection,
            className: 'backgrid backgrid-selectall table table-striped table-bordered table-hover'
        });
        //renderiza o grid

        //configura o paginator
        var paginator = new Backgrid.Extension.Paginator({
            collection: G2S.dataGridCollection,
            goBackFirstOnSort: false
        });

        var success = function(collection, response, options){
            if(collection.length==0){
                $.pnotify({
                    title: "Sem resultado!",
                    text: "Sem resultado para exibição!",
                    type: "warning"
                });
                return false;
            } else {
                that.$el.find("#container-grid").append(pageableGrid.render().$el);
                pageableGrid.insertColumn([{
                    name: 'options',
                    label: 'Opções',
                    sortable: false,
                    editable: false,
                    cell: AbreRegistro
                }]);
                that.$el.find("#container-grid").append(paginator.render().$el);
            }
           loaded();
        };

        var error = function(collection, response, options){
            $.pnotify({
                title: "Erro!",
                text: "Ocorreu um erro na requisição, favor tentar novamente ou entrar em contato com o suporte!",
                type: "error"
            });
            loaded();
            return false;
        };

        if(!firstSubmit){
            var page = G2S.dataGridCollection.queryParams.currentPage;
            G2S.dataGridCollection.getPage(page, { success: success, error: error  });
        } else {
            G2S.dataGridCollection.getFirstPage({ success: success, error: error  });
        }

        return false;
    },
    getFormParams: function () {

        var params = {};
        var form = this.$el.find('#formPesquisa');

        var serialize = form.serializeArray();
        $.each(serialize, function (index, obj) {
            params[obj.name] = obj.value;
        });

        return params;//retorna
    },
    events: {
        'click #btnAddNew': 'addNewAction',
        'submit form#formPesquisa': 'submitForm'
    }
});


var pesquisa = new PesquisaView();
G2S.layout.content.show(pesquisa);
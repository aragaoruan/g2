/**
 * Classe de Notificações de Mensagens
 */
var Mensageiro = {};
	Mensageiro.ERRO 	= 0;
	Mensageiro.SUCESSO 	= 1;
	Mensageiro.AVISO 	= 2;
/**
 * Método Estatico que mostra uma Determinada Mensagem
 */
Mensageiro.mostrar = function(mensageiro, tipo){
	
	var titulo 		 = 'Erro';
	var stateclass   = 'ui-state-error';
	tipo 			 = (typeof tipo == 'undefined' || tipo == null) ? 'ALERTA' : tipo;
	
	switch(mensageiro.tipo){
		case Mensageiro.SUCESSO: 	titulo 	= 'Sucesso'; 	stateclass   = 'ui-state-highlight';  break;
		case Mensageiro.AVISO: 		titulo 	= 'Aviso'; 		stateclass   = null; break;
	}
	
	if(tipo == 'ALERTA'){
		$('body').append('<div id=\'mensageiro-entrega\'>'+ mensageiro.mensagem +'</div>');
		$( "#mensageiro-entrega" ).dialog({
				modal: true,
				title: titulo,
				draggable: false,
				buttons: {
					Ok: function() {
						$(this).dialog("destroy");
						$("#mensageiro-entrega").remove();
						if(typeof mensageiro.redirecionar != 'undefined'){
							Mensageiro.redirecionar(mensageiro.redirecionar);
						}
					}
				}
		}).prev().addClass(stateclass);
	}
		
}

Mensageiro.redirecionar = function(propriedades){
	/*
	Tipos:
		1 - replace: Substitui a pagina.
		2 - normal: abre uma pagina sobre a atual
	*/
	
	propriedades.tempo = propriedades.tempo ? propriedades.tempo*1000 : 0;
	propriedades.destino = propriedades.destino ? propriedades.destino : '_self';
	
	switch(propriedades.tipo){
		case 'replace': 
		
			var direcionando = location.replace(propriedades.url);
			//setTimeout(direcionando,propriedades.tempo);
			
		break;
		case 'ajax': 
			
			propriedades.parametros = propriedades.parametros ? propriedades.parametros : '';
			
			var direcionando = 	$.post(propriedades.url, propriedades.parametros,
				  function( data ) {
					  $(propriedades.destino).html(data);
				  }
				);
		
		break;
		default: var direcionando = window.open(propriedades.url, propriedades.destino);		
	}

	setTimeout(direcionando,propriedades.tempo);
}
/*
 * Inicio da app Marionette e todas as configs da mesma
 *
 * -----------------------------------------
 * MODELS
 * -----------------------------------------
 * Esses modelos estão sendo inicializados todos aqui pois são imprescindiveis
 * para o funcionamento da aplicação. Precisamos dos itens de menu e do controle
 * das views desde o início e durante todo o ciclo da aplicação.
 *
 */

jQuery.ajaxSetup({
    cache: true
});

var LoggedUser = Backbone.Model.extend({
    defaults: {
        st_nomeentidade: '',
        st_nomeperfil: '',
        nomeUsuario: '',
        id_entidade: 0,
        id_perfil: 0,
        id_usuario: 0,
        logged: false,
        msgLogout: false,
        msgEntidade: true,
        id_linhadenegocio: 0,
        bl_provapordisciplina: 0
    }
});

var PerfilModel = Backbone.Model.extend({
    defaults: {
        st_nomeperfil: 'Nome do Perfil',
        id_entidade: 0
    },
    idAttribute: 'id_perfil'
});

var EntidadeModel = Backbone.Model.extend({
    defaults: {
        st_nomeentidade: 'Nome da Entidade',
        st_urllogoentidade: '/none.png',
        perfil: {}
    },
    idAttribute: 'id_entidade',
    initialize: function () {
        var collection = new PerfilCollection();
        _.map(this.get('perfil'), function (obj, indice) {
            var objPerfil = new PerfilModel(obj);
            collection.add(objPerfil);
        });
        this.set('perfil', collection);
    }
});

var MenuItem = Backbone.Model.extend({
    defaults: {
        migrada: true,
        id_funcionalidade: 0,
        id_tipo_funcionalidade: 3,
        id_situacao: 3,
        bl_pesquisa: false,
        bl_relatorio: false,
        classeFlexBotao: '',
        children: [],
        caption: 'Item de Menu',
        link: '#',
        icone: '',
        url: null,
        url_edicao: null,
        bl_delete: true,
        st_ajuda: null
    },
    initialize: function () {
        this.set('children',
            _.map(this.get('children'), function (obj, idx) {
                return new MenuItem(obj);
            }));
    }
});

var ButtonModel = Backbone.Model.extend({
    defaults: {
        btn_id: 'btn_' + Math.random(),
        target: "_self",
        url: "#",
        caption: "Botão",
        color: "custom",
        size: "small",
        icon: "",
        class_btn: ""
    }
});

/**
 * Backbone Model para LogAcesso
 * @type {Model}
 */
var LogAcesso = Backbone.Model.extend({
    defaults: {
        id_logacesso: '',
        dt_cadastro: '',
        id_funcionalidade: '',
        id_perfil: '',
        id_usuario: '',
        id_saladeaula: '',
        id_entidade: ''
    },
    url: function () {
        return this.id ? '/api/log-acesso/' + this.id : '/api/log-acesso';
    }
});

/**
 * @type @exp;Backbone@pro;Collection@call;extend
 * Chama o gerador de modelo do backbone para gerar o modelo da entity de LogAcesso
 */
//var LogAcessoCollection;
//$.getScript('/modelo-backbone/LogAcesso/log-acesso', function(data, textStatus, jqxhr) {
//LogAcessoCollection = Backbone.Collection.extend({
//model: LogAcesso
//});
//});


/*
 * ------------------------------------------
 * COLLECTIONS
 * ------------------------------------------
 */
/**
 * Collection para LogAcesso
 * @type {Collection}
 */
var LogAcessoCollection = Backbone.Collection.extend({
    model: LogAcesso
});

var PerfilCollection = Backbone.Collection.extend({
    model: PerfilModel
});

var EntidadeCollection = Backbone.Collection.extend({
    model: EntidadeModel
});

var ButtonCollection = Backbone.Collection.extend({
    model: ButtonModel,
    url: "#"
});

var Menu = Backbone.Collection.extend({
    model: MenuItem,
    url: '/menu/json',
    localizaItem: function (campo, valor) {

        var idBusca = valor;
        var objRetorno = false;

        _.each(this.models, localizaItem);

        function localizaItem(objeto) {

            if (!objRetorno) {
                if (objeto.get(campo) === valor) {
                    objRetorno = objeto;
                    return true;
                }

                _.each(objeto.get('children'), localizaItem);
            }
        }

        return objRetorno;
    }

});


/*
 *  ------------------------------
 *  VIEWS
 *  ------------------------------
 *  Aqui estão as views do Backbone e Marionette que fazem o controler geral da
 *  aplicação, menus e botões diversos
 */

var UserInfoView = Marionette.ItemView.extend({
    template: '#user-profile-template'
});

var EntidadesMesmaHoldingCollection = Backbone.Collection.extend({
    model: Backbone.Model,
    url: "/holding-filiada/get-perfis-entidades-holding-filiadas/"
});

var EntidadesMesmaHoldingItem = Marionette.ItemView.extend({
    template: '#entidades-mesma-holding-item'
});

var ListaEntidadesHoldingComposite = Marionette.CompositeView.extend({
    template: '#lista-entidades-composite',
    className: 'wrapper-perfil-lista-entidade',
    collection: new EntidadesMesmaHoldingCollection(),
    childView: EntidadesMesmaHoldingItem,
    childViewContainer: '#entidade-holding-template-item',

    emptyView: Marionette.ItemView.extend({
        template: _.template("<li><a>Sem entidades disponíveis.</a></li>")
    }),

    ui: {
        stBuscaEntidadeHolding: '#st_busca_entidade_holding',
        entidadeHoldingTemplateItem: '#entidade-holding-template-item',
        listaEntidades: '#lista-entidades',
        openCloseListaEntidades: '#open-close-lista-entidades'
    },

    buscarEntidadeHolding: function (e) {
        var valorDigitado = e.currentTarget.value;

        var upperVal = valorDigitado.trim().removeAccents().toUpperCase();
        var tableRows = this.ui.entidadeHoldingTemplateItem.find("li");
        if (valorDigitado.length > 0) {
            tableRows.hide();
            tableRows.each(function () {
                if ($(this).find('a').text().trim().removeAccents().toUpperCase().indexOf(upperVal) >= 0) {
                    $(this).show();
                }
            });
        } else {
            tableRows.show();
        }
    },

    focusBuscarEntidadeHolding: function () {
        var buscaEntidade = this.ui.stBuscaEntidadeHolding;
        buscaEntidade.val('');

        this.ui.entidadeHoldingTemplateItem.find("li").show();

        if (!this.ui.listaEntidades.hasClass('open')) {
            var actionFocusBuscar = function () {
                buscaEntidade.trigger("focus");
            };

            setTimeout(actionFocusBuscar, 500);
        }
    },

    hideBuscarEntidadeHolding: function (e) {
        if (e.which === 0) {
            return;
        }

        var buscaEntidade = this.ui.stBuscaEntidadeHolding;
        var entidadeHoldingLi = this.ui.entidadeHoldingTemplateItem.find("li");
        buscaEntidade.val('');

        var actionHideBuscar = function () {
            buscaEntidade.val('');
            entidadeHoldingLi.show();
        };

        setTimeout(actionHideBuscar, 500);
        return;
    },

    clickBuscarEntidadeHolding: function () {
        this.ui.listaEntidades.addClass('open');

        var openLista = this.ui.openCloseListaEntidades;
        var actionOpenLista = function () {
            openLista.trigger('click');
        };

        setTimeout(actionOpenLista, 150);
    },

    events: {
        'click #open-close-lista-entidades': 'focusBuscarEntidadeHolding',
        'keyup #st_busca_entidade_holding': 'buscarEntidadeHolding',
        'blur #st_busca_entidade_holding': 'hideBuscarEntidadeHolding',
        'click #st_busca_entidade_holding': 'clickBuscarEntidadeHolding'
    },

    initialize: function () {

        this.collection.fetch({
            data: {
                id_perfil: this.model.get('id_perfil'),
                id_usuario: this.model.get('id_usuario')
            }
        });

    }
});

var PerfilView = Marionette.ItemView.extend({
    tagName: "li",
    className: "item-perfil",
    template: _.template('<a class="btn" href="/index/perfil/<%=id_entidade%>/<%=id_perfil%>"><i class="icon icon-user"></i><%=st_nomeperfil%></a>')
});

// Lista de entidades na tela de login.
var EntidadeView = Marionette.CompositeView.extend({
    tagName: "li",
    className: 'item-entidade  row-fluid',
    childView: PerfilView,
    childViewContainer: '.lista-perfis',
    template: _.template('<div class="row-fluid"><div class="toggle clearfix"><div class="thumb"><img src="<%=st_urllogoentidade%>"/></div> <p><%=st_nomeentidade%></p></div><ul class="lista-perfis"></ul></div>'),
    initialize: function () {
        this.collection = this.model.get('perfil');
    },
    events: {
        'click .toggle': 'mostraPerfis',
        'click .item-perfil a': 'selecionaPerfil'
    },
    onRender: function () {
        this.$el.attr('data-name', this.model.get('st_nomeentidade').toLowerCase());
        return this;
    },
    mostraPerfis: function (e) {
        $('.lista-perfis').slideUp();
        this.$el.find('.lista-perfis').slideDown();
    },
    selecionaPerfil: function (e) {
        loading();
        e.preventDefault();
        $.get($(e.currentTarget).attr('href'), this.montaMenu);
    },
    montaMenu: function (data) {
        var m = new Menu(data.menuprincipal);

        G2S.loggedUser.set({
            nomeUsuario: data.st_nomecompleto,
            st_nomeentidade: data.st_nomeentidade,
            st_nomeperfil: data.st_nomeperfil,
            id_entidade: data.id_entidade,
            id_perfil: data.id_perfil,
            id_usuario: data.id_usuario,
            id_linhadenegocio: data.id_linhadenegocio,
            bl_provapordisciplina: data.bl_provapordisciplina,
            logged: true
        });

        var profileInfo = new UserInfoView({model: G2S.loggedUser});

        G2S.layout.login.show(profileInfo);

        var entidadesHoldingModel = new Backbone.Model({
            id_usuario: data.id_usuario,
            id_perfil: data.id_perfil,
            st_nomeentidade: data.st_nomeentidade,
            st_nomeperfil: data.st_nomeperfil
        });

        G2S.layout.entidadesHolding.show(new ListaEntidadesHoldingComposite({model: entidadesHoldingModel}));

        G2S.menuPrincipal = new Marionette.CompositeView({
            tagName: 'div',
            className: 'navbar-inner',
            template: '#menu-template',
            childViewContainer: '#nav',
            collection: m,
            childView: MenuItemView,
            onShow: function () {
                $('.subs > li > a').attr('href', 'javascript:');
                $('.area > a').attr('href', 'javascript:'); // remove o link dos items principais
            }
        });
        atualizarMensagemUsuario(); //verifica se existe mensagem para o usuário
        G2S.layout.menu.show(G2S.menuPrincipal);
        G2S.router.navigate("", {trigger: true});
        loaded();
    },
    atualizaMensagensUsuario: function (idUsuario, idEntidade) {
    }
});

var EntidadeListView = Marionette.CompositeView.extend({
    tagName: 'ul',
    className: 'lista-entidades-principal row-fluid',
    childView: EntidadeView,
    template: '#selecao-entidade-template',
    childViewContainer: '#container-entidades',
    timer: null,
    ordenaCollection: function () {
        this.collection.models.sort(compareSortEntity);
    },
    ui: {
        txtSearch: 'input[name="st_busca"]'
    },
    buscarEntidade: function () {
        var that = this;
        //pega o valor do input e força para minusculo
        var txtSearch = that.ui.txtSearch.val().toLowerCase();

        //limpa o temporizador
        clearTimeout(that.timer);

        //adiciona um delay para executar tudo para não ficar travando a digitação
        that.timer = setTimeout(function () {
            loading();
            //limpa a coleção
            that.collection.remove(that.collection.models);

            //pega a variavel com a string json das entidades e seus atributos e faz o parse
            var colecao = JSON.parse(collectionBk);

            //adiciona todas elas na collection
            that.collection.add(colecao);

            if (txtSearch) {
                var newCollection = that.collection.filter(function (child, index, collection) {
                    return child.get('st_nomeentidade').toLowerCase().indexOf(txtSearch) != -1;
                });

                that.collection.set(newCollection);
            }
            that.ordenaCollection();
            loaded();
        }, 100);
    },
    events: {
        'keyup input[name="st_busca"]': 'buscarEntidade'
    }
});


var ButtonView = Marionette.ItemView.extend({
    tagName: "span",
    template: _.template('<a class="btn btn-<%=size%> btn-<%=color%> <%=class_btn%>" id="<%=btn_id%>" href="<%=url%>" target="<%=target%>"><% if(icon){ %><i class="icon icon-white icon-<%=icon%>"></i> <% } %><%=caption%></a>'),
    events: {
        click: function (e) {
            if ($(e.target).closest('#extra').length) {
                e.preventDefault();
                G2S.router.navigate($(e.target).attr('href'), {trigger: true});
            }
        }
    }
});

var AuxButtons = Marionette.CollectionView.extend({
    tagName: "div",
    childView: ButtonView,
    className: "btn-group",
    collectionChange: function () {
        alert('Mudou a colecao');
    }
});

var LoginView = Marionette.ItemView.extend({
    template: '#login-template',
    events: {
        'submit #auth-form': 'doAuth'
    },
    onRender: function () {
        loaded();
    },
    doAuth: function (e) {
        loading();
        e.preventDefault();
        $.ajax({
            url: '/index/auth',
            async: false,
            data: $('#auth-form').serialize(),
            dataType: 'json',
            method: 'POST',
            success: function (data) {
                if (data.type === 'error') {
                    $.pnotify(data);
                } else {
                    _.map(data.mensagem, function (objeto, indice) {
                        var obj = new EntidadeModel(objeto);
                        G2S.entidadesUsuario.add(obj);
                        return obj;
                    });
                    //faz o clone da collection e cria uma string JSON para backup
                    var entidadesUsuarioCollection = G2S.entidadesUsuario.clone();
                    collectionBk = JSON.stringify(entidadesUsuarioCollection.toJSON());

                    /*
                     * Set do usuário logado para controles posteriores
                     * */
                    G2S.loggedUser.set('logged', true);

                    /*
                     * Set do usuário logado para controles posteriores
                     * */
                    G2S.loggedUser.set('msgLogout', true);

                    //console.log('Direciona para a seleção de perfil');
                    G2S.router.navigate('#seleciona-perfil', {trigger: true});

                }
                loaded();
            }
        });

    }
});

var MenuItemView = Marionette.ItemView.extend({
    tagName: 'li',
    template: '#link-template',
    events: {
        'click .lvl1': 'abreSub',
        'click .lvl3': 'abreLink'
    },
    onRender: function () {
        if (this.model.get('id_tipo_funcionalidade') > 3)
            return false;
        switch (this.model.get('id_tipo_funcionalidade')) {
            case 1:
                this.$el.addClass('area');
                break;
            case 2:
                this.$el.addClass('subarea');
                break;
            case 3:
                this.$el.addClass('sub');
                this.$el.addClass('crumb');
                break;
            default:
                break;
        }

        var items = new Backbone.Collection();
        _.each(this.model.get('children'), function (obj) {
            items.add(obj);
        });

        if (items.size() > 0 && this.model.get('id_tipo_funcionalidade') < 3) {
            var id = 'submenu_' + this.model.get('id_funcionalidade');
            var ele = $('<ul/>').attr('id', id);

            if (this.model.get('id_tipo_funcionalidade') === 1)
                ele.addClass('subs');
            else
                ele.addClass('submenu');

            //this.$el.append(ele);

            var menuInterno = new CollectionView({
                collection: items,
                childViewConstructor: MenuItemView,
                childViewTagName: 'li',
                el: ele
            });

            this.$el.append(menuInterno.render().el);
        }
        return this;
    },
    abreSub: function (e) {
        var anchor = $(e.target);
        var submenu = anchor.next('ul');
        submenu.toggleClass('visible');
    },
    abreLink: function (e) {
        e.preventDefault();
        e.stopPropagation();

        var route;
        G2S.funcionalidadeAtual = this.model;
        if (this.model.get('migrada')) {
            if (this.model.get('bl_pesquisa')) {
                route = '#/pesquisa/' + this.model.get('classeFlexBotao');
            } else {
                if (this.model.get('bl_relatorio')) {
                    route = '#/relatorio/' + this.model.get('id_funcionalidade');
                } else {
                    route = ('#/' + this.model.get('url')).replace("//", "/");
                }
            }
        } else {
            $.pnotify({
                type: "error",
                title: 'Erro',
                text: "Não foi possível carregar a funcionalidade"
            });
        }
        this.registrarLogAccesso(route);//registra a rota no analytics e no banco
        G2S.router.navigate(route, {trigger: false});//navega para a funcionalidade
        return false; // acaba com o bubble do evento para o resto da hierarquia do menu
    },
    registrarLogAccesso: function (route) {
        ga('send', 'pageview', route);//registra rota no Analytics
        var logAcesso = new LogAcesso();//instancia o modelo de logAcesso
        //seta os atributos no modelo
        logAcesso.set({
            id_funcionalidade: this.model.get('id_funcionalidade'),
            id_entidade: G2S.loggedUser.get('id_entidade'),
            id_perfil: G2S.loggedUser.get('id_perfil'),
            id_usuario: G2S.loggedUser.get('id_usuario')
        });
        //salva
        logAcesso.save(null, {
            timeout: 600,
            error: function (model, response) {
                if (console && console.error) {
                    console.error('Houve um erro ao salvar o log de acesso. Clique para mais informações. ', response);
                }

                if (response.responseJSON && !response.responseJSON.logado) {
                    G2S.loggedUser.set('logged', false);
                }

            }
        });
    }
});

/*
 * Inserção do Marionette.js para substituir o modelo de Aplicação que estava
 * sendo criado manualmente
 */

var G2SLayout = Marionette.LayoutView.extend({
    template: '#mainApp',
    className: 'G2S',
    regions: {
        breadcrumb: '#breadcrumb',
        menu: '#menu',
        content: '#htmlContent',
        ocorrencia: '#ocorrencia',
        login: '#login-container',
        entidadesHolding: '#holding-container'
    },
    onRender: function () {
        $('#content').height($(window).height() - 165);
    },
    events: {
//        'click#btnAddNew': "mostraHome"
    }
});

var BreadCrumbLayout = Marionette.LayoutView.extend({
    template: "#breadcrumb-template",
    className: 'container-fluid',
    regions: {
        path: "#path",
        extra: "#extra",
        help: "#help"
    },
    onRender: function () {
        if (G2S.funcionalidadeAtual) {
            // BREADCRUM PATH
            if (G2S.funcionalidadeAtual.get('id_tipo_funcionalidade') == 3) {
                var path = [];
                var elem = $('#link_menu_' + G2S.funcionalidadeAtual.get('id_funcionalidade'));

                path.push(elem.closest('.area').find('> .crumb.lvl1').text().trim()); // Menu principal
                path.push(elem.closest('.subarea').find('> .crumb.lvl2').text().trim()); // Submenu
                path.push('<b style="font-size: 12px;">' + elem.text().trim() + '</b>'); // Submenu final

                path = path.join('&nbsp;&nbsp; » &nbsp;&nbsp;');
                $(G2S.breadcrumb.path.el).html(path);
            } else {
                $(G2S.breadcrumb.path.el).html('');
            }

            // HELP ICON
            if (G2S.funcionalidadeAtual.get('st_ajuda'))
                $(G2S.breadcrumb.help.el).attr('href', G2S.funcionalidadeAtual.get('st_ajuda')).removeClass('hide');
            else
                $(G2S.breadcrumb.help.el).addClass('hide');

            // BOTOES DE PESQUISAR/ADICIONAR
            if (G2S.funcionalidadeAtual.get('bl_pesquisa'))
                G2S.geraBotoesPesquisa();
        }
    }
});

var G2SApp = Marionette.Application.extend({
    layout: new G2SLayout(),
    loggedUser: new LoggedUser(),
    breadcrumb: new BreadCrumbLayout(),
    entidadesUsuario: new EntidadeCollection(),
    menuPrincipal: new Menu(),
    timerLoading: null,
    funcionalidadeAtual: null,
    rotaAtual: {
        equals: false,
        route: null
    },
    editModel: null,
    dataGridCollection: null,
    botoesAuxiliares: new ButtonCollection(),
    addRoute: function (route, fn) {
        if (this.hasOwnProperty(fn)) {
            throw("Essa rota já está definida");
        }
    },
    pegaFuncionalidade: function (campo, valor) {
        var retorno = false;
        if (typeof (G2S.layout.menu.currentView) != 'undefined')
            retorno = G2S.layout.menu.currentView.collection.localizaItem(campo, valor);
        return retorno;
    },
    usuarioAutenticado: function () {
        return (G2S.loggedUser.get('logged'));
    },
    perfilSelecionado: function () {
        return (G2S.loggedUser.get('id_perfil') !== 0);
    },
    fechaViews: function () {
        if (typeof (G2S.layout.content.currentView) != 'undefined' && typeof G2S.layout.content.currentView.destroy === 'function') {
            G2S.layout.content.currentView.destroy();
            $('#htmlContent').empty(); // suporte às views que não são Marionette ainda
        }
    },
    deslogaUsuario: function () {
        G2S.router.navigate("#login", {trigger: true});
        if (G2S.loggedUser.get('msgLogout')) {
            $.pnotify({
                title: 'Erro!',
                type: 'error',
                text: 'Sua sessão pode ter expirado, faça login novamente.'
            });
        }
    },
    selecionaEntidade: function () {
        G2S.router.navigate("#seleciona-perfil", {trigger: true});
        if (G2S.loggedUser.get('msgEntidade')) {
            $.pnotify({
                title: 'Alerta!',
                type: 'warning',
                text: 'Selecione uma entidade para continuar!'
            });
        }
    },
    geraBotoesPesquisa: function () {
        var classeFlexBotao = G2S.funcionalidadeAtual.get('classeFlexBotao') ? G2S.funcionalidadeAtual.get('classeFlexBotao') : null;
        var urlPesquisa = "#pesquisa/" + classeFlexBotao;
        var urlAdicionar = "#/" + classeFlexBotao + "/novo";
        var botaoPesquisa = new ButtonModel({
            caption: "Voltar à Pesquisa",
            url: urlPesquisa,
            color: "custom",
            icon: "arrow-left"
        });

        var botaoAdicionar = new ButtonModel({
            caption: "Adicionar",
            url: urlAdicionar,
            color: "default"
        });

        G2S.botoesAuxiliares = new ButtonCollection();
        if (G2S.funcionalidadeAtual.get('bl_pesquisa') && window.location.hash.replace(/\//g, '') != urlPesquisa.replace(/\//g, '')) {
            G2S.botoesAuxiliares.add(botaoPesquisa);
            G2S.botoesAuxiliares.add(botaoAdicionar);
        }
        var aux = new AuxButtons({collection: G2S.botoesAuxiliares});
        G2S.breadcrumb.extra.show(aux);
    },
    show: function (path) {
        G2S.layout.content.show(path);
    },
    recuperaEntidade: function () {
        return G2S.loggedUser.get('id_entidade');
    }
});

var G2S = new G2SApp();

G2S.addRegions({
    main: '.div-g2s'
});

var collectionBk;

/*
 * Callbacks para as rotas
 * */
var G2SController = Marionette.Controller.extend({
    mostraHome: function () {
        /*
         * Além da adição normal do callback before, inseri essa verificação para os casos de logout;
         * */
        if (!G2S.usuarioAutenticado()) {
            G2S.router.navigate("#login", {trigger: true});
            return false;
        }

        debug("Mostrando Home");

        // Carregar os Dashboards
        if (window.jQuery) {
            if (G2S.entidadesUsuario && G2S.entidadesUsuario.length && G2S.recuperaEntidade() != 0)
                loadHtml('/index/dashboard');
        }

        if (typeof (G2S.layout.content.currentView) != 'undefined' && typeof G2S.layout.content.currentView.destroy === 'function')
            G2S.layout.content.currentView.destroy();

        if (window.location.href.indexOf("localhost") === -1)
            loadOcorrenciasPendentes();

        loaded();
    },
    default: function (rota) {
        //var that = this;
        loadHtml(("/" + rota).replace("//", "/"), function () {
            debug('View Carregada para a rota :: ' + rota);
        });
    },
    abreRegistro: function (rota, id) {
        if (!G2S.editModel) {
            G2S.editModel = {
                id_ocorrencia: id
            }
        }

        loadHtml(("/" + rota).replace("//", "/"), function () {
            debug('View Carregada para a rota :: ' + rota);
        });
    },
    pesquisa: function (rota, param) {
        paramUrl = null;
        if (param != null) {
            paramUrl = param;
        }

        if (G2S.rotaAtual.route == rota) {
            G2S.rotaAtual.equals = true;
        } else {
            G2S.rotaAtual.route = rota;
            G2S.rotaAtual.equals = false;
        }

        hashOriginal = '#/pesquisa/' + rota;

        loadHtml('/pesquisa/' + rota);
    },
    relatorio: function (id) {
        loadHtml('/relatorio/master-relatorio/' + id);
    },
    editar: function (rota, id) {
        var tipoTO = $('#to').val();

        if (!G2S.funcionalidadeAtual) {
            G2S.funcionalidadeAtual = G2S.pegaFuncionalidade('classeFlexBotao', rota);
            if (G2S.funcionalidadeAtual == null) {
                $.pnotify({
                    type: "error",
                    title: 'Erro',
                    text: "Não foi possível carregar a funcionalidade"
                });
            }
        }

        if (G2S.funcionalidadeAtual.get('migrada')) {
            var url = '';
            if (G2S.funcionalidadeAtual.get('url_edicao') != '' && G2S.funcionalidadeAtual.get('url_edicao') != null) {
                url = G2S.funcionalidadeAtual.get('url_edicao') + (id ? '/id/' + id : '');
            } else {
                url = G2S.funcionalidadeAtual.get('url') + (id ? '/index/id/' + id : '');
            }

            loadHtml(url, function () {
                //$.getScript(G2S.funcionalidadeAtual.get('url') + "/js", loaded);
            });
        } else {
            $.pnotify({
                type: "error",
                title: 'Erro',
                text: "Não foi possível carregar a funcionalidade"
            });
        }
    },
    novoRegistro: function (rota) {
        G2S.editModel = null; // zerando o editModel pra não entrar no modo de edição

        var tipoTO = $('#to').val();

        if (!G2S.funcionalidadeAtual) { //caso seja um acesso direto à url ou um refresh da url
            var hash = window.location.hash.replace("#", "").replace("/novo", "");
            G2S.funcionalidadeAtual = G2S.pegaFuncionalidade('classeFlexBotao', hash);
        }

        if (typeof (tipoTO) == "undefined" || tipoTO == '') {
            tipoTO = descobreTO(G2S.funcionalidadeAtual.get('classeFlexBotao')).to;
        }

        if (G2S.funcionalidadeAtual.get('migrada')) {
            loadHtml(G2S.funcionalidadeAtual.get('url'), function () {
                //$.getScript(G2S.funcionalidadeAtual.get('url') + '/js', loaded);
            });
        } else {
            $.pnotify({
                type: "error",
                title: 'Erro',
                text: "Não foi possível carregar a funcionalidade"
            });
        }
    },
    abrePrimeiroAcesso: function (rota) {
        loadHtml('/primeiro-acesso', function () {
            $.getScript('/primeiro-acesso/js', loaded);
        });
    },
    abreLogin: function () {
        viewLoginForm = new LoginView();
        G2S.layout.menu.empty();
        G2S.layout.login.empty();
        G2S.funcionalidadeAtual = null;
        G2S.breadcrumb.render();
        G2S.layout.content.show(viewLoginForm);
        loaded();
    },
    mostraEntidades: function () {
        //Recupera string JSON de backup das entidades e faz o parse
        var entidadesObj = JSON.parse(collectionBk);
        //Verifica se a collection é diferente do backup seta novamente os objetos
        if ((typeof entidadeObj === "object") && G2S.entidadesUsuario.length != entidadesObj.length)
            G2S.entidadesUsuario.set(entidadesObj);


        // Ordenas as entidades em ordem alfabetica
        if (G2S.entidadesUsuario && G2S.entidadesUsuario.models)
            G2S.entidadesUsuario.models.sort(compareSortEntity);

        var listaDeEntidadesView = new EntidadeListView({collection: G2S.entidadesUsuario});

        G2S.funcionalidadeAtual = null;
        G2S.breadcrumb.render();
        G2S.layout.content.show(listaDeEntidadesView);

        loaded();
    },
    mostraMensagensUsuario: function () {
        loadHtml('/mensagem-usuario');
    },

    ocorrencia: function (id) {
        if (!G2S.editModel)
            G2S.editModel = {id_ocorrencia: id};

        loadHtml('/ocorrencia/');
    },
    minhasOcorrencias: function (id) {
        if (!G2S.editModel)
            G2S.editModel = {id_ocorrencia: id};

        loadHtml('/minhas-ocorrencias/');
    },
    usuarioLogado: function () {
        return G2S.loggedUser.get('logged');
    },
    detalharColaborador: function (id, perfil) {
        loadHtml('/pagamento-colaborador/detalhar/id/' + id + '/perfil/' + perfil);
    },
    autorizacaoEntrada: function () {
        loadHtml('/matricula/autorizacao-de-entrada');
    },
    cancelarAssinatura: function () {
        loadHtml('/venda/cancelar-assinatura');
    },
    gerenciaNotas: function () {
        loadHtml('/avaliacao/gerencia-notas');
    },
    compartilhar: function () {
        loadHtml('/assunto/compartilhar');
    },
    notificacoes: function () {
        loadHtml('/organizacao/notificacoes');
    },
    entregaDeMaterialPresencial: function () {
        loadHtml('/item-de-material-presencial/entrega')
    },
    dividirSala: function () {
        loadHtml('/sala-de-aula/dividir-sala')
    },
    relatorioProvasPolo: function () {
        loadHtml('/provas-polo')
    },
    abreRelatorioDivisaoCarteira: function () {
        loadHtml('/ultimo-acesso-aluno/divisao-de-carteira');
    },
    relatorioLancamentoVendaProdutoAtendente: function () {
        loadHtml('relatorio-lancamento-venda-produto/atendente');
    },
    abreRotaTipoAtividadeComplementar: function () {
        loadHtml('/atividade-complementar/tipo-atividade');
    },
    arquivoCenso: function () {
        loadHtml('/relatorio/download-censo');
    }
});

/*
 * Principais rotas da aplicação
 *
 * */
var G2SRouter = Marionette.AppRouter.extend({
    controller: G2SController,
    appRoutes: {
        "": "mostraHome",
        'login': 'abreLogin',
        'seleciona-perfil': 'mostraEntidades',
        'ultimo-acesso-aluno/divisao-de-carteira': 'abreRelatorioDivisaoCarteira',
        'pagamento-colaborador/detalhar/:id/perfil/:perfil': 'detalharColaborador',
        'matricula/autorizacao-de-entrada': 'autorizacaoEntrada',
        'item-de-material-presencial/entrega': 'entregaDeMaterialPresencial',
        'sala-de-aula/dividir-sala': 'dividirSala',
        'pesquisa/:area(/:param)': 'pesquisa',
        'relatorio/provas-por-polo': 'relatorioProvasPolo',
        'relatorio/download-censo': 'arquivoCenso',
        'relatorio/:id': 'relatorio',
        'organizacao/notificacoes': 'notificacoes',
        '*editar/editar/:id': 'editar',
        '*classe/novo': 'novoRegistro',
        'primeiro-acesso/:area': 'abrePrimeiroAcesso',
        'assunto': 'compartilhar',
        'usuario-minhas-mensagens': 'mostraMensagensUsuario',
        'atividade-complementar/tipo': 'abreRotaTipoAtividadeComplementar',
        'venda/cancelar-assinatura': 'cancelarAssinatura',
        'avaliacao/gerencia-notas': 'gerenciaNotas',
        'relatorio-lancamento-venda-produto/atendente': 'relatorioLancamentoVendaProdutoAtendente',
        // Inserir novas rotas apenas ACIMA dessa linha!
        ':modulo/:id': 'abreRegistro',
        'minhas-ocorrencias/:id': 'minhasOcorrencias',
        '*default': 'default'
    },
    /*
     * Callback para todas as rotas, verificando pelo objeto se o usuário está logado ou não.
     * */
    before: function (route) {
        if (!G2S.usuarioAutenticado() && route != 'login') {
            G2S.deslogaUsuario();
            return false;
        }

        if (!G2S.recuperaEntidade() && route != 'login' && route != 'seleciona-perfil') {
            G2S.selecionaEntidade();
            return false;
        }
    },
    after: function () {
        /*
         * Caso necessite também podemos chamar callbacks após a execução da rota.
         * */
    }
});

/*
 *  Inicializador que cria o controller e o router para a aplicação
 */
G2S.addInitializer(function () {
    var controller = new G2SController();
    G2S.router = new G2SRouter({controller: controller});

    /*
     * Mostrando o layout principal da aplicação
     * */
    this.main.show(this.layout);

    /*
     * Verificando a sessão do usuário
     * */
    $.ajax({
        url: '/index/checkSession',
        async: false,
        beforeSend: function () {
            loading();
        },
        success: function (data) {
            if (data.usuarioLogado) {
                _.map(data.entidades, function (objeto, indice) {
                    var obj = new EntidadeModel(objeto);
                    G2S.entidadesUsuario.add(obj);
                    return obj;
                });

                G2S.loggedUser = new LoggedUser(data.usuario);
                G2S.loggedUser.set('logged', true);

                //faz o clone da collection e cria uma string JSON para backup
                var entidadesUsuarioCollection = G2S.entidadesUsuario.clone();
                collectionBk = JSON.stringify(entidadesUsuarioCollection.toJSON());

                /*
                 * Caso o usuário já esteja logado, constroi o menu
                 * */
                if (data.menu) {
                    G2S.menuPrincipal = new Marionette.CompositeView({
                        tagName: 'div',
                        className: 'navbar-inner',
                        template: '#menu-template',
                        childViewContainer: '#nav',
                        collection: new Menu(data.menu),
                        childView: MenuItemView,
                        onShow: function () {
                            $('.subs > li > a').attr('href', 'javascript:');
                            $('.area > a').attr('href', 'javascript:'); // remove o link dos items principais
                        }
                    });
                    G2S.layout.menu.show(G2S.menuPrincipal);

                    var profileInfo = new UserInfoView({
                        model: G2S.loggedUser
                    });

                    G2S.layout.login.show(profileInfo);

                    var entidadesHoldingModel = new Backbone.Model({
                        id_usuario: data.usuario.id_usuario,
                        id_perfil: data.usuario.id_perfil,
                        st_nomeentidade: data.usuario.st_nomeentidade,
                        st_nomeperfil: data.usuario.st_nomeperfil
                    });

                    G2S.layout.entidadesHolding.show(new ListaEntidadesHoldingComposite({model: entidadesHoldingModel}));
                }

                if (!G2S.loggedUser.get('id_perfil')) {
                    G2S.router.navigate("#seleciona-perfil", {trigger: true});
                }

                if (!G2S.funcionalidadeAtual) {
                    var hash = window.location.hash.replace("#/pesquisa/", "");
                    G2S.funcionalidadeAtual = G2S.pegaFuncionalidade('classeFlexBotao', hash);
                }
            }
        },
        complete: function () {
            loaded();
        }
    });
});

/*
 * Após a inicialização da aplicação irá startar o history
 *
 * */
G2S.on('start', function () {
    if (Backbone.history)
        Backbone.history.start();

    if (G2S.usuarioAutenticado() === false) {
        G2S.router.navigate("#login", {trigger: true});
    } else if (!G2S.perfilSelecionado()) {
        G2S.router.navigate("#seleciona-perfil", {trigger: true});
    }

    G2S.layout.breadcrumb.show(G2S.breadcrumb);
});


/*
 * Incialização do App
 * */

$(document).ready(function () {
    if (getCookie('reload') == 'true') {
        window.location.reload(true);
        setCookie('reload', 'false')
    }

    G2S.start();
    //G2S.layout.breadcrumb.show(G2S.breadcrumb);
});
/* Final do código referente ao Marionette.js */

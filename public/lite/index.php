<?php
exit;
define('APPLICATION_ROOT', dirname(dirname(__FILE__)));
defined('APPLICATION_SYS')
    || define('APPLICATION_SYS', 'LITE');
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
    
    
defined('APPLICATION_REAL_PATH')
    || define('APPLICATION_REAL_PATH', realpath(dirname(__FILE__) ));
    
defined('URL_APP')
    || define('URL_APP', $_SERVER['HTTP_HOST'] );

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/configs'),
    realpath(APPLICATION_PATH . '/ro'),
    realpath(APPLICATION_PATH . '/models'),
    realpath(APPLICATION_PATH . '/models/to'),
    realpath(APPLICATION_PATH . '/models/bo'),
    realpath(APPLICATION_PATH . '/models/dao'),
    realpath(APPLICATION_PATH . '/models/orm'),
    realpath(APPLICATION_PATH . '/apps/lite/controllers'),
    realpath(APPLICATION_PATH . '/apps/lite/models'),
    realpath(APPLICATION_PATH . '/apps/lite/models/bo'),
    realpath(APPLICATION_PATH . '/apps/lite/models/to'),
    realpath(APPLICATION_PATH . '/webservices/clients/sa'),
    realpath(APPLICATION_PATH . '/webservices/clients/actor'),
    realpath(APPLICATION_PATH . '/webservices/clients/moodle'),
    realpath(APPLICATION_PATH . '/webservices/clients/pagamento'),
    realpath(APPLICATION_PATH . '/webservices/server'),
    get_include_path(),
)));



include('Zend/Loader/Autoloader.php');

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);


// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : Ead1_Ambiente::getAmbiente()));

/** Zend_Application */
require_once 'Zend/Application.php';

try{
	// Create application, bootstrap, and run
	$application = new Zend_Application(
	    APPLICATION_ENV,
	    APPLICATION_PATH . '/configs/application.ini'
	);

	$application->bootstrap()
	            ->run();
	
	
} catch (Exception $exception) {
	
	echo 'Erro de aplicação. Erro: '. $exception->getMessage();
	
}
		
            
/**
 * Objeto para startar o loading do bootstrap
 * @type Object
 * @argument {object} $el Objeto jquery para o elemento
 * @argument {function} start Starta o loading
 * @argument {function} finish finaliza o loading e reseta o objeto
 * @default $el recebe como default qualquer elemento da dom com atributo [data-loading-text]
 */
var Loading = {
    $el: $('*[data-loading-text]'),
    start: function () {
        this.$el.button('loading');
    },
    finish: function () {
        this.$el.button('reset');
    }
};
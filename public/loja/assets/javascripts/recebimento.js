$(document).ready(function () {

        $('#btn_pagar_cartao').on('click', function (e) {//EFETUA O PAGAMENTO COM CARTÃO NORMAL
            var that = this;//atribui o this numa variavel para escopo
            e.preventDefault();
            if (validaCamposCartao()) {
                solicitaCartao($(that), '/loja/recebimento/autorizar-cartao');
            }
            return false;
        });

        $('#btn_pagar_cartao_recorrente').on('click', function (e) { //EFETUA O PAGAMENTO COM CARTÃO RECORRENTE
            var that = this;//atribui o this numa variavel para escopo
            e.preventDefault();

            if (validaCamposCartaoRecorrente()) {
                solicitaCartaoRecorrente($(that), '/loja/recebimento/autorizar-cartao');
            }
            return false;
        });

        $('#btn_boleto').on('click', function (e) { //EFETUA O PAGAMENTO COM BOLETO 
            var that = this;
            e.preventDefault();
            solicitaBoleto($(that));
            return false;
        });

});

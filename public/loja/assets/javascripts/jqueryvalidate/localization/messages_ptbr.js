/**
 * Translated default messages for the jQuery validation plugin.
 * Language: PT_BR
 * Translator: Francisco Ernesto Teixeira <fco_ernesto@yahoo.com.br>
 */
jQuery.extend(jQuery.validator.messages, {
	required: "<br>Campo Obrigatório",
	remote: "<br>Por favor, corrija este campo.",
	email: "<br>Forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
	url: "<br>Forne&ccedil;a uma URL v&aacute;lida.",
	date: "<br>Forne&ccedil;a uma data v&aacute;lida.",
	dateISO: "<br>Forne&ccedil;a uma data v&aacute;lida (ISO).",
	dateDE: "<br>Bitte geben Sie ein gültiges Datum ein.",
	number: "<br>Forne&ccedil;a um n&uacute;mero v&aacute;lida.",
	numberDE: "<br>Bitte geben Sie eine Nummer ein.",
	digits: "<br>Forne&ccedil;a somente d&iacute;gitos.",
	creditcard: "<br>Forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
	equalTo: "<br>Forne&ccedil;a o mesmo valor novamente.",
	accept: "<br>Forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
	maxlength: jQuery.validator.format("<br>M&aacute;ximo de {0} caracteres."),
	minlength: jQuery.validator.format("<br>M&iacute;nimo de {0} caracteres."),
	rangelength: jQuery.validator.format("<br>Forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
	range: jQuery.validator.format("<br>Forne&ccedil;a um valor entre {0} e {1}."),
	max: jQuery.validator.format("<br>Forne&ccedil;a um valor menor ou igual a {0}."),
	min: jQuery.validator.format("<br>Forne&ccedil;a um valor maior ou igual a {0}.")
});

jQuery.validator.addMethod("datePTBR", function(value) { 
  return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d?\d?$/.test(value); 
}, "<br>Forne&ccedil;a uma data v&aacute;lida. DD/MM/YYYY");
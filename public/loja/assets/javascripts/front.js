$(document).ready(function () {

    $("#st_cep").mask("99.999-999"); //MASCARA DE CPF
    $("#st_cep_correspondencia").mask("99.999-999"); //MASCARA DE CPF
    $("#dt_nascimento").mask("99/99/9999"); //MASCARA DE CPF
    $("#nu_cpfRecorrente").mask("99999999999999", {placeholder: ""});
    $("#nu_cartaoRecorrente").mask("9999 9999 9999 9999", {placeholder: ""});
    $("#st_codigoSegurancaRecorrente").mask("999", {placeholder: ""});

    if ($('#pagamento').length) { // SE HOUVER ALGUM PAGAMENTO
        $('#btn_cep').click(function () {//BUSCA OS DADOS DO CEP
            getCep($('#st_cep').val());
        });

        $('#form-endereco').submit(function (e) {
            var error = false;
            if ($('#st_cep').val() == "") {
                $('#div_cep').addClass('has-error');
                error = true;
            }
            ;
            if ($('#st_endereco').val() == "") {
                $('#div_endereco').addClass('has-error');
                error = true;
            }
            ;
            if ($('#nu_numero').val() == "") {
                $('#div_numero').addClass('has-error');
                error = true;
            }
            ;
            if ($('#st_bairro').val() == "") {
                $('#div_bairro').addClass('has-error');
                error = true;
            }
            ;
            if ($('#sg_uf').val() == "") {
                $('#div_bairro').addClass('has-error');
                error = true;
            }
            ;
            if ($('#municipios').val() == "") {
                $('#div_cidade').addClass('has-error');
                error = true;
            }
            ;
            if (error === false) {
                $('#div_cidade').removeClass('has-error');
                $('#div_numero').removeClass('has-error');
                $('#div_endereco').removeClass('has-error');
                $('#div_cep').removeClass('has-error');
                $('#div_uf').removeClass('has-error');
                $('#div_bairro').removeClass('has-error');
                $('#div_alerta').addClass('hidden');
                e.preventDefault();
                cadastraEndereco();

                return false;
            } else {
                $('#div_alerta').removeClass('hidden');
                return false;
            }
        }); // EFETUA O CADASTRO OU ALTERAÇÃO DO ENDEREÇO

        $('#btn_pagar_cartao').on('click', function (e) {//EFETUA O PAGAMENTO COM CARTÃO NORMAL
            var that = this;//atribui o this numa variavel para escopo
            e.preventDefault();
            if (validaCamposCartao()) {
                solicitaCartao($(that));
            }
            return false;
        });

        $('#btn_pagar_cartao_recorrente').on('click', function (e) { //EFETUA O PAGAMENTO COM CARTÃO RECORRENTE
            var that = this;//atribui o this numa variavel para escopo
            e.preventDefault();

            if (validaCamposCartaoRecorrente()) {
                solicitaCartaoRecorrente($(that));
            }
            return false;
        });

        $('#btn_boleto').on('click', function (e) { //EFETUA O PAGAMENTO COM BOLETO
            var that = this;
            e.preventDefault();
            solicitaBoleto($(that));
            return false;
        });
    }

    //VERIFICA SE O PRODUTO É DO TIPO "LIVRO"
    if ($("#bl_livro").val() == true) {
        //OCULTA AS DIVS DE PAGAMENTO E O TÍTULO
        $("#div_pagtitulo").hide();
        $("#div_pagboleto").hide();
        $("#div_pagcartao").hide();

        //MODIFICA O NOME DO BOTÃO DEPENDENDO DE TER OU NÃO ENDEREÇO
        if($("#st_cep").val()){
            $("#btn_cadastrarEndereco").text("Confirmar endereço de entrega e prosseguir");
        }else{
            $("#btn_cadastrarEndereco").text("Cadastrar endereço de entrega e prosseguir");
        }
    }

    //DEFINE QUE O CAMPO NUMERO SÓ PODE SER NUMEROS
    $(function () {
        $('#nu_numero_correspondencia').bind('keydown', somenteNumeros); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
    });
    $(function () {
        $('#nu_numero').bind('keydown', somenteNumeros); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
    });

    //CARREGAMENTO DO ENDERECO DA LOJA
    $('#sg_uf').change(function () { // CARREGA OS MUNÍCIPIOS DE ACORDO COM A UF SELECIONADA
        if ($("#id_municipio")) {
            getMunicipiosByUf($('#sg_uf').val(), null, $("#id_municipio"));
        } else {
            getMunicipiosByUf($('#sg_uf option:selected').val());
        }
    });

    $('#sg_uf').val($('#id_uf').val()).prop('selected', true); // CARREGA NO COMBO O MUNÍCIPIO DE ACORDO COM O ID SELECIONADO

    $('#sg_uf').ready(function () { // EFETUA O CARREGAMENTO DOS MUNÍCIPIOS DE ACORDO COM O ESTADO SELECIONADO
        getMunicipiosByUf($('#sg_uf option:selected').val());
        $('#municipios').val($('#id_mun').val()).prop('selected', true);
    });

    //*CARREGAMENTO DO ENDEREÇO DE CORRESPONDENCIA DA LOJA

    $('#sg_uf_corresp').change(function () { // CARREGA OS MUNÍCIPIOS DE ACORDO COM A UF SELECIONADA
        if ($("#municipio_correspondencia")) {
            getMunicipiosByUf($('#sg_uf_corresp').val(), null, $("#municipios_correspondencia"));
        } else {
            getMunicipiosByUf($('#sg_uf_corresp option:selected').val());
        }
    });

    $('#sg_uf_corresp').val($('#id_uf').val()).prop('selected', true);

    $('#sg_uf_corresp').ready(function () { // EFETUA O CARREGAMENTO DOS MUNÍCIPIOS DE ACORDO COM O ESTADO SELECIONADO
        getMunicipiosByUf($('#sg_uf_corresp option:selected').val(), null, $('#municipio_correspondencia'));
        $('#municipio_correspondencia').val($('#id_mun').val()).prop('selected', true);
    });


    $('#btn_cep_contrato').click(function () {//BUSCA OS DADOS DO CEP
        if ($('#st_cep').val() != "")
            getCep($('#st_cep').val());

    });

    $('#btn_cep_contrato_correspondencia').click(function () {//BUSCA OS DADOS DO CEP
        if ($('#st_cep_correspondencia').val() != "")
            getCepCorrespondencia($('#st_cep_correspondencia').val());
    });


    $('#st_cpf').keyup(function () { // VALIDA O CPF OU CNPJ
        ValidarCPFRG($('#st_cpf').val());
    });

    $('#btn_endereco_correspondecia').click(function () {
        var enderecoCorrespondencia = $('#enderecoCorrespondencia');
        var inputs = $('input[type=text], select', enderecoCorrespondencia);

        if ($('#correspondencia').val() == 0) {
            $('#correspondencia').val(1);
            $('#enderecoCorrespondencia').removeClass('hidden');
            $('#btn_endereco_correspondecia').text('Utilizar somente meu endereço residêncial');

            inputs.attr('required', 'required');

        } else {
            $('#correspondencia').val(0);
            $('#enderecoCorrespondencia').addClass('hidden');
            $('#btn_endereco_correspondecia').text('Quero informar endereço diferente para Correspondência');

            inputs.removeAttr('required');
        }
    });

    $('#st_cpfRecorrente').keyup(function () { // VALIDA O CPF OU CNPJ
        ValidarCPFRG($('#st_cpfRecorrente').val());
    });
    $(function () {
        $('#nu_cartao').bind('keydown', somenteNumeros); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
    });
    $(function () {
        $('#st_cpf').bind('keydown', somenteNumeros); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
    });
    $(function () {
        $('#st_codigoSeguranca').bind('keydown', somenteNumeros); // DEFINE QUE O CAMPO SÓ RECEBERÁ NÚMEROS
    });
    function somenteNumeros(e) {
        //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
        keyCodesPermitidos = new Array(8, 9, 37, 39, 46);
        //numeros e 0 a 9 do teclado alfanumerico
        for (x = 48; x <= 57; x++) {
            keyCodesPermitidos.push(x);
        }

        //numeros e 0 a 9 do teclado numerico
        for (x = 96; x <= 105; x++) {
            keyCodesPermitidos.push(x);
        }

        //Pega a tecla digitada
        keyCode = e.which;
        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, keyCodesPermitidos) != -1) {
            return true;
        }
        return false;
    } // LIMITA OS CARACTERES DIGITADOS SOMENTE A NÚMEROS

    $('#nu_cartao').keyup(function () {
        verificaCartao();
    });
    $('#nu_cartaoRecorrente').keyup(function () {
        verificaCartao();
    });
});

/**
 * Tratamento das bandeiras dos cartões, pois os id's do banco e do plugin utilizado para definir a bandeira
 * não corresponde.
 *
 * @param id_card
 * @returns {*}
 */
function codigoTipoCartao(id_card) {
    var card = null;

    switch (id_card) {
        //ID 5 no plugin corresponde a bandeira ELO e no banco o ELO tem ID 4
        case 5:
            card = 4; // ID correspondente ao nosso banco de dados para a bandeira ELO
            break;
        //ID 4 no plugin corresponde a bandeira DINNERS e no banco de dados DINERS tem o ID 5
        case 4:
            card = 5; // ID correspondente ao nosso banco de dados para a bandeira DINERS
            break;
        default:
            card = id_card;
            break;
    }

    return card;
}

/**
 * Função para validação do cartão de crédito e seta a bandeira do mesmo conforme a entrada do usuário.
 * @returns {Boolean}
 */
function verificaCartao() {
    var retorno;
    $('#nu_cartao').validateCreditCard(function (result) {
        $('#div_cartao').addClass('has-error');
        $('#div_cartao').removeClass('has-success');
        $('#img_MASTERCARD').addClass('imgopacity');
        $('#img_Elo').addClass('imgopacity');
        $('#img_Amex').addClass('imgopacity');
        $('#img_VISA').addClass('imgopacity');
        $('#img_Diners').addClass('imgopacity');
        $('#img_Hipercard').addClass('imgopacity');

        if (result.card_type != null) {
            cartao = '#img_' + result.card_type.name;
            $(cartao).removeClass('imgopacity');
            $(cartao).addClass('has-error');
            $('#div_cartao').addClass('has-success');
            $('#div_cartao').removeClass('has-error');
            $('#nu_cartaoUtilizado').val(codigoTipoCartao(result.card_type.id_card));

            retorno = true;
        } else {
            $('#div_cartao').addClass('has-error');
            $('#div_cartao').removeClass('has-success');
            $('#img_MASTERCARD').addClass('imgopacity');
            $('#img_Elo').addClass('imgopacity');
            $('#img_Amex').addClass('imgopacity');
            $('#img_VISA').addClass('imgopacity');
            $('#img_Diners').addClass('imgopacity');
            $('#img_Hipercard').addClass('imgopacity');
            retorno = false;
        }

    }, {
        accept: ['VISA', 'MASTERCARD', 'Amex', 'Elo', 'Diners', 'Hipercard']
    });

    $('#nu_cartaoRecorrente').validateCreditCard(function (result) {
        $('#div_cartaoRecorrente').addClass('has-error');
        $('#div_cartaoRecorrente').removeClass('has-success');
        $('#img_MASTERCARD').addClass('imgopacity');
        $('#img_Elo').addClass('imgopacity');
        $('#img_Amex').addClass('imgopacity');
        $('#img_Hipercard').addClass('imgopacity');
        $('#img_VISA').addClass('imgopacity');
        $('#img_Diners').addClass('imgopacity');
        $('#img_Hipercard').addClass('imgopacity');

        if (result.card_type != null) {
            cartao = '#img_' + result.card_type.name;
            $(cartao).removeClass('imgopacity');
            $(cartao).addClass('has-error');
            $('#div_cartaoRecorrente').addClass('has-success');
            $('#div_cartaoRecorrente').removeClass('has-error');
            $('#nu_cartaoUtilizadoRecorrente').val(codigoTipoCartao(result.card_type.id_card));

            retorno = true;
        } else {
            $('#div_cartaoRecorrente').addClass('has-error');
            $('#div_cartaoRecorrente').removeClass('has-success');
            $('#img_MASTERCARD').addClass('imgopacity');
            $('#img_Hipercard').addClass('imgopacity');
            $('#img_Elo').addClass('imgopacity');
            $('#img_Amex').addClass('imgopacity');
            $('#img_VISA').addClass('imgopacity');
            $('#img_Diners').addClass('imgopacity');
            $('#img_Hipercard').addClass('imgopacity');
            retorno = false;
        }

    }, {
        accept: ['VISA', 'MASTERCARD', 'Amex', 'Elo', 'Diners', 'Hipercard']
    });

    if (typeof AMBIENTE != 'undefined' && AMBIENTE != 'producao') {
        retorno = true;
    }

    return retorno;
}

function getCep(st_cep) { //BUSCA O ENDEREÇO DE ACORDO COM O CEP
    $.ajax({
        url: "/default/pessoa/get-endereco-by-cep/primeiro/1/st_cep/" + st_cep,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {

        },
        async: false,
        success: function (response) {
            $('#municipios').empty();
            $('#st_endereco').val(response.st_endereco);
            $('#nu_numero').val(response.nu_numero);
            $('#st_complemento').val(response.st_complemento);
            $('#st_bairro').val(response.st_bairro);
            $('#sg_uf').val(response.sg_uf);
            if (response.sg_uf) {
                getMunicipiosByUf(response.sg_uf, response.st_cidade);
            }
        },
        complete: function () {

        },
        error: function (data) {
            console.log("Erro")
        }
    });
}

function getCepCorrespondencia(st_cep) { //BUSCA O ENDEREÇO DE ACORDO COM O CEP
    $.ajax({
        url: "/default/pessoa/get-endereco-by-cep/primeiro/1/st_cep/" + st_cep,
        type: 'POST',
        dataType: 'json',
        beforeSend: function () {

        },
        async: false,
        success: function (response) {
            $('#municipio_correspondencia').empty();
            $('#st_endereco_correspondencia').val(response.st_endereco);
            $('#nu_numero_correspondencia').val(response.nu_numero);
            $('#st_complemento_correspondencia').val(response.st_complemento);
            $('#st_bairro_correspondencia').val(response.st_bairro);
            $('#sg_uf_corresp').val(response.sg_uf);
            if (response.sg_uf) {
                getMunicipiosByUf(response.sg_uf, response.st_cidade, $('#municipio_correspondencia'));
            }
        },
        complete: function () {

        },
        error: function (data) {
            console.log("Erro")
        }
    });
}

function getMunicipiosByUf(uf, cidade, $el) {//BUSCA OS MUNICIPIOS DE ACORDO COM UF
    $el = $el ? $el : $('#municipios');
    if (uf) {
        $.ajax({
            url: '/default/pessoa/get-municipio-by-uf/sg_uf/' + uf,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $el.empty();
                $el.html('<option value="">Carregando...</option>')
            },
            async: false,
            success: function (response) {
                $combo = '<option value="">Selecione</option>';
                for (var obj in response)
                    $combo += "<option value='" + response[obj].id_municipio + "' >" + response[obj].st_nomemunicipio + "</option>";
                $el.empty();
                $el.html($combo);
                $el.find('option:contains(' + cidade + ')').attr('selected', true);
//                $('#municipios option:contains(' + cidade + ')').attr('selected', true);
            },
            complete: function () {

            },
            error: function (data) {

            }
        });
    }
}

function validaEndereco() {
    ret = true;
    if ($('#bl_livro').val() == true) {
        $.ajax({
            url: "valida-endereco/endereco",
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                'id_venda': $('#id_vendaendereco').val()
            },
            success: function (response) {
                if (response == true)
                    ret = true;
                else
                    ret = false;
            },
            error: function (response) {
                ret = response;
            },
            complete: function () {

            }
        });
        return ret;
    } else {
        return ret;
    }
}

function cadastraEndereco() {// EFETUA O CADASTRAMENTO DE ENDEREÇO
    $.ajax({
        url: "/pagamento/salva-endereco/",
        type: 'POST',
        dataType: 'json',
        data: {
            'id_vendaendereco': $('#id_vendaendereco').val(),
            'st_cep': $('#st_cep').val(),
            'st_endereco': $('#st_endereco').val(),
            'nu_numero': $('#nu_numero').val(),
            'st_complemento': $('#st_complemento').val(),
            'st_bairro': $('#st_bairro').val(),
            'st_cidade': $('#municipios option:selected').text(),
            'id_municipio': $('#municipios option:selected').val(),
            'sg_uf': $('#sg_uf').val(),
            'id_pais': $('#id_pais').val(),
            'id_enderecoentrega': $('#id_enderecoentrega').val()
        },
        beforeSend: function () {
        },
        success: function (response) {
            bootbox.dialog({
                message: "Endereço cadastrado com sucesso!",
                title: "Sucesso",
                buttons: {
                    successful: {
                        label: "OK!",
                        className: "btn-success",
                        callback: function(){
                            //HABILITA CAMPOS DE PAGAMENTO DEPOIS QUE CLICAR EM "OK"
                            $("#div_pagtitulo").show();
                            $("#div_pagboleto").show();
                            $("#div_pagcartao").show();
                        }
                    }
                }
            });
        },
        complete: function () {
        },
        error: function (data) {
            bootbox.dialog({
                message: "Endereço não cadastrado com sucesso!",
                title: "Problema",
                buttons: {
                    successful: {
                        label: "OK!",
                        className: "btn-warning"
                    }
                }
            });
        }
    });
}

/**
 * Valida CNPJ e CPF
 * @returns {Boolean}
 */
function ValidarCPFRG(cpf) { //VALIDA O CPF E O CNPJ
    if (cpf.length != 11 && cpf.length != 14) {
        $('#div_cpf').removeClass('has-success').addClass('has-error');
        $('#div_cpfRecorrente').removeClass('has-success').addClass('has-error');
        return false;
    }

    if (validaCNPJ(cpf) || validaCPF(cpf)) {
        $('#div_cpf').removeClass('has-error').addClass('has-success');
        $('#div_cpfRecorrente').removeClass('has-error').addClass('has-success');
        return true;
    } else {
        $('#div_cpf').removeClass('has-success').addClass('has-error');
        $('#div_cpfRecorrente').removeClass('has-success').addClass('has-error');
        return false;
    }

}


function solicitaBoleto($elemento) { //SOLICITA O BOLETO BANCÁRIO
    Loading.$el = $elemento;
    if (validaEndereco()) {
        $.ajax({
            url: "/pagamento/solicitar-boleto/",
            type: 'POST',
            dataType: 'json',
            data: {
                'id_venda': $('#id_venda').val(),
                'nu_parcelas': $('#nu_parcelas').val()
            },
            beforeSend: function () {
                Loading.start()
            },
            success: function (response) {
                if (response.type == 'success') {
                    $("#form-boleto").submit();
                } else {
                    Loading.finish();
                    console.log("Erro na solicitação de boleto");
                }
            },
            complete: function () {

            },
            error: function (data) {
                Loading.finish();
                console.log("Erro");
            }
        });
    } else {
        bootbox.dialog({
            message: 'É necessário cadastrar um endereço para prosseguir.',
            title: 'Aviso',
            buttons: {
                successful: {
                    label: "Fechar",
                    className: "btn-alert"
                }
            }
        });
    }
}

/**
 * Limpa os dados do cartão
 * @param resetDados
 */
function resetarCamposFormulario(resetDados) {
    resetDados = resetDados || false;
    if (resetDados) {
        $("#form-cartao").trigger('reset');
    }
    $('#st_titular').removeClass('has-success').removeClass('has-error');
    $('#st_mesValidade').removeClass('has-success').removeClass('has-error');
    $('#st_anoValidade').removeClass('has-success').removeClass('has-error');
    $('#nu_cartao').removeClass('has-success').removeClass('has-error');
    $('#st_codigoSeguranca').removeClass('has-success').removeClass('has-error');
}

function bloquearDesbloquearCampos(bloquear) {
    if (bloquear) {
        $('#st_anoValidade, #nu_cartao, #st_codigoSeguranca, #st_cpf, #st_parcelamento, #st_titular, #st_mesValidade')
            .attr("disabled", "disabled");
    } else {
        $('#st_anoValidade, #nu_cartao, #st_codigoSeguranca, #st_cpf, #st_parcelamento, #st_titular, #st_mesValidade')
            .removeAttr("disabled");
    }
}

function solicitaCartao($elemento, url) {
    Loading.$el = $elemento;
    PagarMe.encryption_key = window.encryption_key;
    resetarCamposFormulario();
    if (validaEndereco() == true) {

        var creditCard = new PagarMe.creditCard();
        creditCard.cardHolderName = $('#st_titular').val();
        creditCard.cardExpirationMonth = $('#st_mesValidade option:selected').val();
        creditCard.cardExpirationYear = $('#st_anoValidade option:selected').val();
        creditCard.cardNumber = $('#nu_cartao').val();
        creditCard.cardCVV = $('#st_codigoSeguranca').val();

        var fieldErrors = creditCard.fieldErrors();

        //Verifica se há erros
        var hasErrors = false;
        for (var field in fieldErrors) {
            hasErrors = true;
            break;
        }

        if (hasErrors) {
            // realiza o tratamento de errors
            var errors = '';
            for (var field in fieldErrors) {

                errors = errors + fieldErrors[field] + '<br>';

                if (field == 'card_number') {
                    $('#nu_cartao').removeClass('has-success').addClass('has-error');
                }
                if (field == 'card_cvv') {
                    $('#st_codigoSeguranca').removeClass('has-success').addClass('has-error');
                }
            }

            bootbox.dialog({
                message: errors,
                title: 'Aviso',
                buttons: {
                    successful: {
                        label: "Fechar",
                        className: "btn-alert"
                    }
                }
            });
        } else {
            creditCard.generateHash(function (cardHash) {
                var params = {
                    'id_venda': $('#id_venda').val(),
                    'id_cartaobandeira': $('#nu_cartaoUtilizado').val(),
                    'id_meiopagamento': $('#id_meiopagamentoCartao').val(),
                    'nu_parcelas': $('#st_parcelamento option:selected').val(),
                    'card_hash': cardHash,
                    'id_lancamento': $('#id_lancamento').val(),
                    'id_acordo': null
                };

                var id_acordo = $('#id_acordo').val();
                if (id_acordo) {
                    params['id_acordo'] = id_acordo;
                }

                $.ajax({
                    url: '/pagamento/autorizar-cartao',
                    type: 'post',
                    dataType: 'json',
                    data: params,
                    beforeSend: function () {
                        bloquearDesbloquearCampos(true);
                        Loading.start();
                    },
                    success: function (response) {
                        if (response.type == 'warning') {
                            Loading.finish();
                            bootbox.dialog({
                                message: response['mensagem'],
                                title: response['title'],
                                buttons: {
                                    successful: {
                                        label: "Fechar",
                                        className: "btn-alert"
                                    }
                                }
                            });
                        }

                        if (response.type == 'success') {
                            $("#form-cartao").submit();
                        }

                        if (response.type == 'error') {
                            Loading.finish();
                            bootbox.dialog({
                                message: response['mensagem'],
                                title: response['title'],
                                buttons: {
                                    successful: {
                                        label: "Fechar",
                                        className: "btn-danger"
                                    }
                                }
                            });
                        }
                    },
                    complete: function (response) {
                        bloquearDesbloquearCampos(false);
                        resetarCamposFormulario(true);
                    },
                    error: function (response) {
                        Loading.finish();
                        bootbox.dialog({
                            message: response['mensagem'],
                            title: response['title'],
                            buttons: {
                                successful: {
                                    label: "Fechar",
                                    className: "btn-alert"
                                }
                            }
                        });
                        $("#form-cartao").trigger('reset');
                    }
                });
            });

        }
    } else {
        bootbox.dialog({
            message: 'É necessário cadastrar um endereço para prosseguir.',
            title: 'Aviso',
            buttons: {
                successful: {
                    label: "Fechar",
                    className: "btn-alert"
                }
            }
        });
    }
}

function solicitaCartaoBraspag($elemento, url) { //SOLICITA O CARTAO
    Loading.$el = $elemento;
    if (validaEndereco() == true) {
        $.ajax({
            url: "/pagamento/autorizar-cartao/",
            type: 'POST',
            dataType: 'json',
            data: {
                'id_venda': $('#id_venda').val(),
                'st_cartao': $('#nu_cartao').val(),
                'st_codigoseguranca': $('#st_codigoSeguranca').val(),
                'st_nomeimpresso': $('#st_titular').val(),
                'nu_mesvalidade': $('#st_mesValidade option:selected').val(),
                'nu_anovalidade': $('#st_anoValidade option:selected').val(),
                'id_cartaobandeira': $('#nu_cartaoUtilizado').val(),
                'id_meiopagamento': $('#id_meiopagamentoCartao').val(),
                'nu_parcelas': $('#st_parcelamento option:selected').val()
            },
            beforeSend: function () {
                Loading.start();
            },
            success: function (response) {
                if (response.type == 'warning') {
                    Loading.finish();
                    bootbox.dialog({
                        message: response['mensagem'],
                        title: response['title'],
                        buttons: {
                            successful: {
                                label: "Fechar",
                                className: "btn-alert"
                            }
                        }
                    });
                }
                if (response.type == 'success') {
                    $("#form-cartao").submit();
                }

                if (response.type == 'error') {
                    Loading.finish();
                    bootbox.dialog({
                        message: response['mensagem'],
                        title: response['title'],
                        buttons: {
                            successful: {
                                label: "Fechar",
                                className: "btn-danger"
                            }
                        }
                    });
                }
            },
            complete: function () {

            },
            error: function (response) {
                Loading.finish();
                bootbox.dialog({
                    message: response['mensagem'],
                    title: response['title'],
                    buttons: {
                        successful: {
                            label: "Fechar",
                            className: "btn-alert"
                        }
                    }
                });
            }
        });
    } else {
        bootbox.dialog({
            message: 'É necessário cadastrar um endereço para prosseguir.',
            title: 'Aviso',
            buttons: {
                successful: {
                    label: "Fechar",
                    className: "btn-alert"
                }
            }
        });
    }
}

function solicitaCartaoRecorrente($elemento, url) { //SOLICITA O CARTAO
    Loading.$el = $elemento;
    if (validaEndereco() == true) {

        PagarMe.encryption_key = window.encryption_key;
        $('#st_titular').removeClass('has-success').removeClass('has-error');
        $('#st_mesValidade').removeClass('has-success').removeClass('has-error');
        $('#st_anoValidade').removeClass('has-success').removeClass('has-error');
        $('#nu_cartao').removeClass('has-success').removeClass('has-error');
        $('#st_codigoSeguranca').removeClass('has-success').removeClass('has-error');

        var creditCard = new PagarMe.creditCard();
        creditCard.cardHolderName = $('#st_titularRecorrente').val();
        creditCard.cardExpirationMonth = $('#st_mesValidadeRecorrente option:selected').val();
        creditCard.cardExpirationYear = $('#st_anoValidadeRecorrente option:selected').val();
        creditCard.cardNumber = $('#nu_cartaoRecorrente').val();
        creditCard.cardCVV = $('#st_codigoSegurancaRecorrente').val();

        var fieldErrors = creditCard.fieldErrors();

        //Verifica se há erros
        var hasErrors = false;
        for (var field in fieldErrors) {
            hasErrors = true;
            break;
        }

        if (hasErrors) {
            // realiza o tratamento de errors
            var errors = '';
            for (var field in fieldErrors) {

                errors = errors + fieldErrors[field] + '<br>';

                if (field == 'card_number') {
                    $('#nu_cartaoRecorrente').removeClass('has-success').addClass('has-error');
                }
                if (field == 'card_cvv') {
                    $('#st_codigoSegurancaRecorrente').removeClass('has-success').addClass('has-error');
                }
            }

            bootbox.dialog({
                message: errors,
                title: 'Aviso',
                buttons: {
                    successful: {
                        label: "Fechar",
                        className: "btn-alert"
                    }
                }
            });
        } else {

            creditCard.generateHash(function (cardHash) {
                var params = {
                    'id_venda': $('#id_vendaRecorrente').val(),
                    'id_cartaobandeira': $('#nu_cartaoUtilizadoRecorrente').val(),
                    'id_meiopagamento': $('#id_meiopagamentoCartaoRecorrente').val(),
                    'nu_parcelas': $('#st_parcelamentoRecorrente option:selected').val(),
                    'card_hash': cardHash,
                    'id_lancamento': $('#id_lancamentoRecorrente').val()
                };

                $.ajax({
                    url: '/pagamento/autorizar-cartao',
                    type: 'post',
                    dataType: 'json',
                    data: params,
                    beforeSend: function () {
                        Loading.start();
                    },
                    success: function (response) {
                        if (response.type == 'warning') {
                            Loading.finish();
                            bootbox.dialog({
                                message: response['mensagem'],
                                title: response['title'],
                                buttons: {
                                    successful: {
                                        label: "Fechar",
                                        className: "btn-alert"
                                    }
                                }
                            });
                        }
                        if (response.type == 'success') {
                            $("#form-cartao").submit();
                        }

                        if (response.type == 'error') {
                            Loading.finish();
                            bootbox.dialog({
                                message: response['mensagem'],
                                title: response['title'],
                                buttons: {
                                    successful: {
                                        label: "Fechar",
                                        className: "btn-danger"
                                    }
                                }
                            });
                        }
                    },
                    complete: function () {

                    },
                    error: function (response) {
                        Loading.finish();
                        bootbox.dialog({
                            message: response['mensagem'],
                            title: response['title'],
                            buttons: {
                                successful: {
                                    label: "Fechar",
                                    className: "btn-alert"
                                }
                            }
                        });
                    }
                });
            });
        }

    } else {
        bootbox.dialog({
            message: 'É necessário cadastrar um endereço para prosseguir.',
            title: 'Aviso',
            buttons: {
                successful: {
                    label: "Fechar",
                    className: "btn-alert"
                }
            }
        });
    }
}

function validaCamposCartao() { //VALIDA OS CAMPOS DO CARTÃO  || !verificaCartao()
    var flag = true;
    if ($('#nu_cartao').val() == "" || !verificaCartao()) {
        $('#div_cartao').addClass('has-error');
        flag = false;
    } else {
        $('#div_cartao').removeClass('has-error');
        $('#div_cartao').addClass('has-success');
    }

    if ($('#st_titular').val() == "") {
        $('#div_titular').addClass('has-error');
        flag = false;
    } else {
        $('#div_titular').removeClass('has-error');
        $('#div_titular').addClass('has-success');
    }

    if (!ValidarCPFRG($('#st_cpf').val())) {
        $('#div_cpf').addClass('has-error');
        flag = false;
    } else {
        $('#div_cpf').removeClass('has-error');
        $('#div_cpf').addClass('has-success');
    }
    if ($('#st_codigoSeguranca').val() == "") {
        $('#div_codigoSeguranca').addClass('has-error');
        flag = false;
    } else {
        $('#div_codigoSeguranca').removeClass('has-error');
        $('#div_codigoSeguranca').addClass('has-success');
    }

    if ($('#st_mesValidade option:selected').val() == "") {
        $('#div_mesValidade').addClass('has-error');
        flag = false;
    } else {
        $('#div_mesValidade').removeClass('has-error');
        $('#div_mesValidade').addClass('has-success');
    }

    if ($('#st_anoValidade option:selected').val() == "") {
        $('#div_anoValidade').addClass('has-error');
        flag = false;
    } else {
        $('#div_anoValidade').removeClass('has-error');
        $('#div_anoValidade').addClass('has-success');
    }

    if ($('#st_parcelamento option:selected').val() == "") {
        $('#div_parcelamento').addClass('has-error');
        flag = false;
    } else {
        $('#div_parcelamento').removeClass('has-error');
        $('#div_parcelamento').addClass('has-success');
    }

    if (flag == false) {
        $('#div_alertaCartao').removeClass('hidden');
    } else {
        $('#div_alertaCartao').addClass('hidden');
    }
    if (flag) {
        return true;
    }
    return false;

}

function validaCamposCartaoRecorrente() { //VALIDA OS CAMPOS DO CARTÃO
    var flag = true;
    if ($('#nu_cartaoRecorrente').val() == "" || !verificaCartao()) {
        $('#div_cartaoRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_cartaoRecorrente').removeClass('has-error');
        $('#div_cartaoRecorrente').addClass('has-success');
    }

    if ($('#st_titularRecorrente').val() == "") {
        $('#div_titularRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_titularRecorrente').removeClass('has-error');
        $('#div_titularRecorrente').addClass('has-success');
    }

    if (!ValidarCPFRG($('#st_cpfRecorrente').val())) {
        $('#div_cpfRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_cpfRecorrente').removeClass('has-error');
        $('#div_cpfRecorrente').addClass('has-success');
    }
    if ($('#st_codigoSegurancaRecorrente').val() == "") {
        $('#div_codigoSegurancaRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_codigoSegurancaRecorrente').removeClass('has-error');
        $('#div_codigoSegurancaRecorrente').addClass('has-success');
    }

    if ($('#st_mesValidadeRecorrente option:selected').val() == "") {
        $('#div_mesValidadeRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_mesValidadeRecorrente').removeClass('has-error');
        $('#div_mesValidadeRecorrente').addClass('has-success');
    }

    if ($('#st_anoValidadeRecorrente option:selected').val() == "") {
        $('#div_anoValidadeRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_anoValidadeRecorrente').removeClass('has-error');
        $('#div_anoValidadeRecorrente').addClass('has-success');
    }

    if ($('#st_parcelamentoRecorrente option:selected').val() == "") {
        $('#div_parcelamentoRecorrente').addClass('has-error');
        flag = false;
    } else {
        $('#div_parcelamentoRecorrente').removeClass('has-error');
        $('#div_parcelamentoRecorrente').addClass('has-success');
    }

    if (flag == false) {
        $('#div_alertaCartaoRecorrente').removeClass('hidden');
    } else {
        $('#div_alertaCartaoRecorrente').addClass('hidden');
    }
    if (flag) {
        return true;
    }
    return false;

}

function in_array(needle, haystack, argStrict) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true
    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '',
            strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}

/*
 Fun��o para gravar Cookie no cliente
 */

if (typeof window.setCookie != 'function') {

    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }

}

if (typeof window.carregaSelect != 'function') {
    function carregaSelect(idselect, iddestino, url, callback) {

        var queryString = $(idselect).attr('id').toString() + '=' + $(idselect).val();
        var municipio = null;
        if ($(idselect).val() != '') {
            $.post(url, queryString,
                    function (data) {
                        if (data.tipo == 1) {

                            $(iddestino).removeOption(/./);
                            if (data.mensagem.length > 0) {
                                selecione = {"": "Selecione..."};
                                $(iddestino).addOption(selecione, false);
                                for (var i = 0; i < data.mensagem.length; i++) {

                                    $(iddestino).append('<option value="' + data.mensagem[i][0] + '">' + data.mensagem[i][1] + '</option>');

                                } //for (var k in data) {
                            } else {
                                data = {"": "Nenhum registro encontrado."};
                                $(iddestino).addOption(data, false).focus();
                            }


                            if (typeof callback != 'undefined') {
                                return callback($(iddestino));
                            }


                        }

                    }, "json");
        } else {
            $(iddestino).removeOption(/./);
        }//if($('#codufnaturalidade').val()>0){
        return true;
    }
}

/**
 * Valida CPF
 * @param {string} cpf
 * @returns {Boolean}
 */
function validaCPF(cpf) {
    cpf = cpf.replace(/[^0-9]/g, "");
    var soma;
    var resto;
    soma = 0;
    if ((cpf == '11111111111') || (cpf == '22222222222') || (cpf ==
            '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') ||
            (cpf == '66666666666') || (cpf == '77777777777') || (cpf ==
            '88888888888') || (cpf == '99999999999') || (cpf == '00000000000')) {
        return false;
    } else {
        for (i = 1; i <= 9; i++) {
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
        }
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(9, 10)))
            return false;
        soma = 0;
        for (i = 1; i <= 10; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(cpf.substring(10, 11)))
            return false;

        return true;
    }
}

/**
 * Valida CNPJ
 * @param {string} cnpj
 * @returns {Boolean}
 */
function validaCNPJ(cnpj) {
    if (cnpj.length == 14) {
        var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
        digitos_iguais = 1;
        if (cnpj.length < 14 && cnpj.length < 15)
            return false;
        for (i = 0; i < cnpj.length - 1; i++)
            if (cnpj.charAt(i) != cnpj.charAt(i + 1))
            {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais)
        {
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0, tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
            {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
            {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1)) {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }else{
        return false;
    }

}

/**
 * Função para requisitar nova senha
 * @param {obj} btnEl
 * @param {string} st_email
 * @returns {undefined}
 */
function recuperarSenha(btnEl, st_email) {
    if (st_email) {
        Loading.$el = btnEl;

        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/loja/senha/recuperar',
            data: {
                'st_email': st_email
            },
            beforeSend: function () {
                Loading.start();
            },
            success: function (data) {
                $(".popover").hide();
                //se sucesso
                bootbox.dialog({
                    message: data.mensagem,
                    title: data.title,
                    buttons: {
                        successful: {
                            label: "OK!"
                        }
                    }
                });
            },
            complete: function () {
                Loading.finish();
            },
            error: function (response) {
                bootbox.dialog({
                    message: "Ocorreu um erro inesperado.",
                    title: "Erro!",
                    buttons: {
                        successful: {
                            label: "OK!"
                        }
                    }
                });
            }
        });


    }
}

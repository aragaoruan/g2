$(function () {
    var bl_emailunico = $("#bl_emailunico").val();
    var email;

    /**
     * Pega o evento de click do botão btn-prosseguir para verificar se o email existe
     */
    $("#btn-prosseguir").on('click', function () {
        var that = this;//atribui o this numa variavel para escopo
        Loading.$el = $(that);//diz para o loading qual é o elemento
        var st_login = $("#st_login");//pega o elemento login e atribui o objeto numa variavel
        if (st_login.val()) {//vefifica se tem valor
            st_login.parent().parent().removeClass('has-error');//remove a classe de erro
            st_login.popover('hide');//apaga o popover

            //busca o email
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/pagamento/verifica-email-entidade',
                data: {
                    'st_email': st_login.val(),
                    'bl_emailunico': bl_emailunico
                },
                beforeSend: function () {
                    Loading.start();
                },
                success: function (data) {
                    //se sucesso mostra o campo de email
                    if (data.type == 'success') {
                        $(that).addClass('hide');
                        $("#container-senha, #container-btn-login").removeClass('hide');
                        email = st_login.val();
                    } else if (data.type == 'warning') {//se não encontrar manda pra tela de cadastro
                        window.location = '/pagamento/cadastro?st_email=' + st_login.val();
                    } else {
                        bootbox.dialog({
                            message: data.mensagem,
                            title: data.title,
                            buttons: {
                                successful: {
                                    label: "OK!"
                                }
                            }
                        });
                    }
                },
                complete: function () {
                    Loading.finish();
                },
                error: function (response) {
                    bootbox.dialog({
                        message: "Ocorreu um erro inesperado.",
                        title: "Erro!",
                        buttons: {
                            successful: {
                                label: "OK!"
                            }
                        }
                    });
                }
            });

        } else {
            st_login.parent().parent().addClass('has-error');
            st_login.popover('show');
        }
    });

    /**
     * Pega o evento de keyup do input
     */
    $("#st_login, #st_senha").on('keyup', function (e) {
        //pega o valor
        var valor = $(this).val();
        //verifica se o valor existe
        if (valor) {

            //remove a classe de erro
            $(this).parent().parent().removeClass('has-error');
            $(this).popover('hide');

            if (($(this).attr('name') == 'st_login' && $("#btn-prosseguir").hasClass('hide')) && email != valor) {
                $("#btn-prosseguir").removeClass('hide');
                $("#container-senha, #container-btn-login").addClass('hide');
            }
        } else {//se o valor estiver vazio coloca a classe de erro

            //se o campo com valor vazio for o st_login e se o botao de pesquisar o email estiver oculto ele reseta a tela
            if ($(this).attr('name') == 'st_login' && $("#btn-prosseguir").hasClass('hide')) {
                $("#btn-prosseguir").removeClass('hide');
                $("#container-senha, #container-btn-login").addClass('hide');
            }

            $(this).parent().parent().addClass('has-error');
            $(this).popover('show');
        }
    });

    /**
     * onKeyPress verifica se apertou o enter e dispara o evento de click do botão
     */
    $("#st_login").on('keypress', function (e) {
        if (e.keyCode == 13 && !$("#btn-prosseguir").hasClass('hide')) {
            $("#btn-prosseguir").click();
            return false;
        }
    });

    /**
     * onClick no botão de recuperar senha
     */
    $("#btn-recuperar-senha").on('click', function () {
        var st_email = $("#st_login").val();
        if (!st_email) {
            $("#st_login").parent().parent().addClass('has-error');
            $("#st_login").popover('show');
        } else {
            recuperarSenha($(this), st_email);
        }

    });

    /**
     * Submit do formulario
     */
    $("form[name='form-email']").submit(function () {
        //pega os valores
        var st_login = $("#st_login");
        var st_senha = $("#st_senha");

        //valida o formulario
        if (validarForm()) {
            Loading.$el = $("#btn-logar");

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: $(this).attr('action'),
                data: {
                    'st_login': st_login.val(),
                    'st_senha': st_senha.val()
                },
                beforeSend: function () {
                    Loading.start();
                },
                success: function (data) {
                    //se sucesso
                    if (data.type == 'success') {
                        $('#btn-logar').prop('disabled', true);
                        window.location.href = '/pagamento/?venda=' + data.codigo.id_venda
                    } else if (data.type == 'warning') {//se não encontrar
                        $('#btn-logar').prop('disabled', false);
                        window.location = '/pagamento/cadastro?st_email=' + st_login.val();
                    } else {
                        bootbox.dialog({
                            message: data.mensagem,
                            title: data.title,
                            buttons: {
                                successful: {
                                    label: "OK!"
                                }
                            }
                        });
                        st_login.parent().parent().addClass('has-error');
                        st_senha.parent().parent().addClass('has-error');
                        Loading.finish();
                        $('#btn-logar').prop('disabled', false);
                    }
                },
                complete: function () {

                },
                error: function (response) {
                    Loading.finish();
                    $('#btn-logar').prop('disabled', false);
                    bootbox.dialog({
                        message: "Ocorreu um erro inesperado.",
                        title: "Erro!",
                        buttons: {
                            successful: {
                                label: "OK!"
                            }
                        }
                    });
                }
            });

        }

        return false;
    });

});

/**
 * Valida se os campos do formulário foram preenchidos
 * @returns {Boolean}
 */
function validarForm() {
    var st_login = $("#st_login").val();
    var st_senha = $("#st_senha").val();
    var valid = true;

    if (!st_login) {
        $("#st_login").parent().parent().addClass('has-error');
        $("#st_login").popover('show');
        valid = false;
    } else {
        $("#st_login").parent().parent().removeClass('has-error');
        $("#st_login").popover('hide');
        valid = true;
    }

    if (!st_senha) {
        $("#st_senha").parent().parent().addClass('has-error');
        $("#st_senha").popover('show');
        valid = false;
    } else {
        $("#st_senha").parent().parent().removeClass('has-error');
        $("#st_senha").popover('hide');
        valid = true;
    }


    return valid;
}

var helper = (function () {
    var authResult = undefined;

    return {
        /**
         * Hides the sign-in button and connects the server-side app after
         * the user successfully signs in.
         *
         * @param {Object} authResult An Object which contains the access token and
         *   other authentication information.
         */
        onSignInCallback: function (authResult) {
            if (authResult['access_token']) {
                // The user is signed in
                this.authResult = authResult;
                helper.connectServer();

                /*
                 * Desloga o usuário do google
                 */
                //helper.disconnectServer(authResult);


                // After we load the Google+ API, render the profile data from Google+.
                gapi.auth.setToken(authResult);
                gapi.client.load('plus', 'v1', this.renderProfile);

                gapi.client.load('oauth2', 'v2', function () {
                    var request = gapi.client.oauth2.userinfo.get();
                    request.execute(getEmailCallback);
                });
                function getEmailCallback(obj) {

                    $.ajax({
                        type: 'post',
                        url: '/loja/pagamento/google',
                        dataType: 'json',
                        data: {
                            'st_nomecompleto': obj['name'],
                            'st_email': obj['email'],
                            'id_google': obj['id']
                        },
                        success: function (result) {
                            console.log(result);
                            //se sucesso redireciona para concluír o cadastro
                            window.location.href = '/pagamento/' + result;
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }
            } else if (authResult['error']) {
                // There was an error, which means the user is not signed in.
                // As an example, you can troubleshoot by writing to the console:
            }
        },
        /**
         * Retrieves and renders the authenticated user's Google+ profile.
         */
        renderProfile: function () {
            var request = gapi.client.plus.people.get({'userId': 'me'});
            request.execute(function (profile) {
                $('#profile').empty();
                if (profile.error) {
                    $('#profile').append(profile.error);
                    return;
                }
                $('#profile').append(
                    $('<p><img src=\"' + profile.image.url + '\"></p>'));
                $('#profile').append(
                    $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
                    profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
                if (profile.cover && profile.coverPhoto) {
                    $('#profile').append(
                        $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
                }
            });
            $('#authOps').show('slow');
            $('#gConnect').hide();
        },
        /**
         * Calls the server endpoint to disconnect the app for the user.
         */
        disconnectServer: function (authResult) {
            // Revoke the server tokens
            var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' +
                authResult['access_token'];

            // Realizar uma solicitação GET assíncrona.
            $.ajax({
                type: 'GET',
                url: revokeUrl,
                async: false,
                contentType: "application/json",
                dataType: 'jsonp',
                success: function (nullResponse) {
                    // Fazer algo agora que o usuário está desconectado
                    // A resposta é sempre indefinida.
                },
                error: function (e) {
                    // Gerenciar o erro
                    // console.log(e);
                    // Você pode apontar usuários para desconectar manualmente se não for bem-sucedido
                    // https://plus.google.com/apps
                }
            });
        },
        /**
         * Calls the server endpoint to connect the app for the user. The client
         * sends the one-time authorization code to the server and the server
         * exchanges the code for its own tokens to use for offline API access.
         * For more information, see:
         *   https://developers.google.com/+/web/signin/server-side-flow
         */
        connectServer: function () {
            $.ajax({
                type: 'POST',
                url: window.location.href + '/connect?state={{ STATE }}',
                contentType: 'application/octet-stream; charset=utf-8',
                success: function (result) {
                    helper.people();
                },
                processData: false,
                data: this.authResult.code
            });
        },
        /**
         * Calls the server endpoint to get the list of people visible to this app.
         */
        people: function () {
            $.ajax({
                type: 'GET',
                url: window.location.href + '/people',
                contentType: 'application/octet-stream; charset=utf-8',
                success: function (result) {
                    helper.appendCircled(result);
                },
                processData: false
            });
        },
        /**
         * Displays visible People retrieved from server.
         *
         * @param {Object} people A list of Google+ Person resources.
         */
        appendCircled: function (people) {
            $('#visiblePeople').empty();

            $('#visiblePeople').append('Number of people visible to this app: ' +
            people.totalItems + '<br/>');
            for (var personIndex in people.items) {
                person = people.items[personIndex];
                $('#visiblePeople').append('<img src="' + person.image.url + '">');
            }
        }
    };
})();

/**
 * Calls the helper method that handles the authentication flow.
 *
 * @param {Object} authResult An Object which contains the access token and
 *   other authentication information.
 */
function onSignInCallback(authResult) {
    helper.onSignInCallback(authResult);
}
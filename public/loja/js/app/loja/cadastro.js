var formSubmit = true;
$(function () {
    var baseUrlModulo = $("#baseUrl").val();


    //Percorre os inputs com required e escreve um span dizendo que é obrigatorio
    $('*[required]').each(function (i, elem) {
        var name = $(elem).attr('name');
        $('label[for="' + name + '"]').append(' <span style="color:red">*</span>');
    });

    $('#st_cpf').numeric().mask('999.999.999-99');
    $('#nu_dddcelular').numeric();
    $('#nu_telefonecelular').numeric();
    $('#nu_dddresidencial').numeric();
    $('#nu_telefoneresidencial').numeric();
    $('#nu_numero').numeric();
    $('#entrega_nu_numero').numeric();

    //verifica o email
    verificaEmailEntidade($('#st_email'));
    $('#st_email').tooltip('hide');

    //onBlur verifica o email novamente
    $("#st_email").on('change', function () {
        var st_email = $(this);
        verificaEmailEntidade(st_email);
    });

    //onKeyup no campo de numero de ddd
    $("#nu_dddcelular").on('keyup', function () {
        var valor = $(this).val();
        if (valor.length == 2) {
            $("#nu_telefonecelular").focus();
        } else {
            $(this).focus();
        }
    });

    //onChange do campo senha a confirmar senha
    $("#st_confirmarsenha, #st_senha").on('change', function () {
        validaForm();
    });

    //onKeyup no campo de cpf
    $("#st_cpf").on("change", function () {
        var cpf = $(this).val();
        $(this).popover('destroy');
        var cpfClean = cpf.replace(/[^0-9]/g, "");
        if (cpfClean.length === 11) {
            if (!validaCPF(cpf)) {
                $(this).parent('div.form-group').removeClass('has-success').addClass('has-error');
                $(this).popover({
                    content: "CPF inválido! Digite o CPF corretamente.",
                    placement: 'top'
                }).popover('toggle');
                return false;
            } else {
                verificaCpfEntidade();
                $(this).parent('div.form-group').removeClass('has-error').addClass('has-success');
                return true;
            }

        }
    });

    //Envia o formulario
    $("#frmCadastroPagamentoPessoa").submit(function () {
        Loading.start();
        if (!formSubmit) {
            verificaEmailEntidade($('#st_email'));
            verificaCpfEntidade();
            Loading.finish();
            return false;
        }
        if (validaForm()) {
            var that = this;
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: $(that).attr('action'),
                data: $(that).serialize(),
                beforeSend: function () {
//                    Loading.start();
                },
                success: function (data) {
                    //se sucesso mostra o campo de email
                    if (data.type == 'success') {
                        window.location.href = '/pagamento/?venda=' + data.codigo.id_venda
                    } else {
                        bootbox.dialog({
                            message: data.mensagem,
                            title: data.title,
                            buttons: {
                                successful: {
                                    label: "OK!"
                                }
                            }
                        });
                    }
                },
                complete: function () {
                    Loading.finish();
                },
                error: function (response) {
                    bootbox.dialog({
                        message: "Ocorreu um erro inesperado.",
                        title: "Erro!",
                        buttons: {
                            successful: {
                                label: "OK!"
                            }
                        }
                    });
                }
            });
        }
        return false;
    });

});


function validaForm() {
    var valid = true;
    var st_confirmarsenha = $("#st_confirmarsenha");
    var st_senha = $("#st_senha");
    var st_cpf = $("#st_cpf");


    $('*[required]').each(function (i, elem) {
        var valor = $(elem).val();
        if (!valor) {
            valid = false;
            $(elem).parent('div.form-group').removeClass('has-success').addClass('has-error');
        } else {
            $(elem).parent('div.form-group').removeClass('has-error').addClass('has-success');
        }
    });


    if (st_confirmarsenha.val() != st_senha.val()) {
        st_confirmarsenha.parent('div.form-group').removeClass('has-success').addClass('has-error');
        st_confirmarsenha.popover({
            content: "O campo Confirmar senha deve ser igual o campo Senha.",
            placement: 'top'
        }).popover('toggle');
        valid = false;
    } else {
        st_confirmarsenha.popover('destroy');
        st_confirmarsenha.parent('div.form-group').removeClass('has-error').addClass('has-success');
    }

    //verifica o tamanho da senha
    if (st_senha.val().length < 6 || st_senha.val().length > 15) {
        st_senha.parent('div.form-group').removeClass('has-success').addClass('has-error');
        st_senha.popover({
            content: "O campo Senha deve conter no mínimo 6 caracteres e no máximo 15.",
            placement: 'top'
        }).popover('toggle');
        valid = false;
    } else {
        st_senha.popover('destroy');
        st_senha.parent('div.form-group').removeClass('has-error').addClass('has-success');
    }


    if (!validaCPF(st_cpf.val()) && st_cpf.val()) {
        st_cpf.parent('div.form-group').removeClass('has-success').addClass('has-error');
        st_cpf.popover({
            content: "CPF inválido! Digite o CPF corretamente.",
            placement: 'top'
        }).popover('toggle');
        valid = false;
    } else {
        st_cpf.popover('destroy');
        st_cpf.parent('div.form-group').removeClass('has-error').addClass('has-success');

    }
//    verificaCpfEntidade();
//    verificaEmailEntidade($("#st_email"));
    return valid;
}

function verificaEmailEntidade(elemObj) {
    var st_email = elemObj;
    st_email.popover('destroy');
    if (!st_email.val()) {
        st_email.parent('div.form-group').removeClass('has-success').addClass('has-error');
        st_email.popover({
            content: "O campo de e-mail é de preenchimento obrigatório.",
            placement: 'top'
        }).popover('toggle');
        formSubmit = false;
        return false;
    }


    $.ajax({
        dataType: 'json',
        type: 'post',
        url: $("#baseUrl").val() + '/pagamento/verifica-email-entidade',
        data: {
            'st_email': st_email.val()
        },
        beforeSend: function () {
        },
        success: function (data) {
            //se sucesso mostra o campo de email
            if (data.type == 'success') {
                st_email.parent('div.form-group').removeClass('has-success').addClass('has-error');
                st_email.popover({
                    html: true,
                    content: data.text,
                    placement: 'top'
                }).popover('toggle');
                formSubmit = false;
            } else if (data.type == 'warning') {
                st_email.parent('div.form-group').removeClass('has-error').addClass('has-success');
                st_email.attr('data-toggle', 'tooltip').attr('title', data.text).tooltip('toggle');
                formSubmit = true;
            } else {
                bootbox.dialog({
                    message: data.mensagem,
                    title: data.title,
                    buttons: {
                        successful: {
                            label: "OK!"
                        }
                    }
                });
                formSubmit = true;
            }
        },
        complete: function () {

        },
        error: function (response) {
            bootbox.dialog({
                message: "Ocorreu um erro inesperado.",
                title: "Erro!",
                buttons: {
                    successful: {
                        label: "OK!"
                    }
                }
            });
        }
    });
}

function verificaCpfEntidade() {
    var st_cpf = $('#st_cpf').val();

    if (!st_cpf) {
        if (!$('#st_cpf').val()) {
            $('#st_cpf').parent('div.form-group').removeClass('has-success').addClass('has-error');
            $('#st_cpf').popover({
                content: "O campo de e-mail é de preenchimento obrigatório.",
                placement: 'top'
            }).popover('toggle');
            formSubmit = false;
            return false;
        }
    }

    $.ajax({
        dataType: 'json',
        type: 'post',
        url: '/loja/pagamento/verifica-cpf-entidade',
        data: {
            'st_cpf': st_cpf
        },
        beforeSend: function () {
        },
        success: function (mensageiro) {
            if (mensageiro.type == 'warning') {
                formSubmit = true;
                $('#st_cpf').parent('div.form-group').removeClass('has-error').addClass('has-success');
            } else {
                $('#st_cpf').parent('div.form-group').removeClass('has-success').addClass('has-error');
                $('#st_cpf').popover({
                    html: true,
                    content: mensageiro.text,
                    placement: 'top'
                }).popover('toggle');
                formSubmit = false;
            }
        },
        complete: function () {

        },
        error: function (response) {
            bootbox.dialog({
                message: "Ocorreu um erro inesperado.",
                title: "Erro!",
                buttons: {
                    successful: {
                        label: "OK!"
                    }
                }
            });
        }
    });

}

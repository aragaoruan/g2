/**
 * Created by Débora Castro on 12/05/2015.
 * <debora.castro@unyleya.com.br
 */
var VwDisciplinasAgendamento = Backbone.Model.extend({
    defaults: {
        id_matricula: '',
        id_projetopedagogico: '',
        id_usuario: '',
        id_avaliacaoagendamento: '',
        id_tipodeavaliacao: '',
        id_avaliacao: '',
        id_disciplina: '',
        id_tipodisciplina: '',
        id_avaliacaoconjuntoreferencia: '',
        nu_notafinal: '',
        st_disciplina: '',
        st_status: '',
        bl_status: '',
        bl_agendado: '',
        bl_provaglobal: ''
    }
});
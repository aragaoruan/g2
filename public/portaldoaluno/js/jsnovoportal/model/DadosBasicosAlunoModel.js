var DadosBasicosAlunoModel = Backbone.Model.extend({
    url: "/portal/avaliacao/agendamento",
    defaults: {
        id: '',
        st_cpf: '',
        st_nomecompleto: '',
        st_login: '',
        st_urlavatar: '',
        id_endereco: '',
        sg_uf: '',
        st_cep: '',
        id_municipio: '',
        id_tipoendereco: '',
        st_endereco: '',
        st_bairro: '',
        st_cidade: '',
        st_complemento: '',
        nu_numero: '',
        id_email: '',
        st_email: '',
        id_telefone: '',
        id_tipotelefone: '',
        nu_ddi: '',
        nu_ddd: '',
        nu_telefone: ''
    }
});

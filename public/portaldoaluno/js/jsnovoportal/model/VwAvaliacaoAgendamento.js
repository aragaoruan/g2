/**
 * Created by Débora Castro on 12/05/2015.
 * <debora.castro@unyleya.com.br
 */
var VwAvaliacaoAgendamento = Backbone.Model.extend({
    defaults: {
        id_avaliacaoagendamento: '',
        dt_agendamento: '',
        dt_aplicacao: '',
        id_matricula: '',
        id_usuario: '',
        id_situacao: '',
        id_avaliacaoaplicacao: '',
        id_primeirachamada: '',
        id_ultimachamada: '',
        id_evolucao: '',
        bl_provaglobal: '',
        id_mensagens: '',
        id_provasrealizadas: '',
        id_entidade: '',
        id_horarioaula: '',
        id_avaliacao: '',
        id_projetopedagogico: '',
        id_tipodeavaliacao: '',
        id_situacaopresenca: '',
        id_chamada: '',
        bl_ativo: '',
        st_nomecompleto: '',
        st_avaliacao: '',
        st_situacao: '',
        st_nomeentidade: '',
        st_horarioaula: '',
        st_mensagens: '',
        st_projetopedagogico: '',
        st_codsistema: '',
        st_endereco: '',
        st_aplicadorprova: '',
        st_cidade: '',
        st_situacaopresenca: '',
        nu_primeirapresenca: '',
        nu_ultimapresenca: '',
        id_aplicadorprova: '',
        nu_presenca: '',
        id_usuariolancamento: '',
        st_usuariolancamento: '',
        bl_temnotafinal: '',
        st_email: '',
        nu_telefone: '',
        nu_ddd: '',
        nu_ddi: '',
        hr_inicio: '',
        hr_fim: '',
        sg_uf: '',
        st_complemento: '',
        nu_numero: '',
        st_bairro: '',
        st_telefoneaplicador: ''
    }
});

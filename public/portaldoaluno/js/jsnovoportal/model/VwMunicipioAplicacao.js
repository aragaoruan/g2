var VwMunicipioAplicacaoModel = Backbone.Model.extend({
    defaults: {
        id_municipio: '',
        st_nomemunicipio: '',
        id_entidade: '',
        sg_uf: ''
    }
});
/**
 * Created by Débora Castro on 12/05/2015.
 * <debora.castro@unyleya.com.br
 */
var VwEnviarMensagemAluno = Backbone.Model.extend({
    defaults: {
        id_enviomensagem: '',
        dt_cadastro: '',
        dt_enviar: '',
        dt_cadastromsg: '',
        id_usuario: '',
        bl_importante: '',
        id_matricula: '',
        id_mensagem: '',
        id_tipoenvio: '',
        id_evolucao: '',
        st_endereco: '',
        st_mensagem: '',
        st_texto: '',
        id_entidade: ''
    }
});
/**
 * Created by Helder on 23/02/2016.
 */

var VwSalaOfertaTurma = Backbone.Model.extend({
    defaults: {
        id_saladeaula: '',
        dt_inicioinscricao: '',
        dt_fiminscricao: '',
        dt_abertura: '',
        dt_encerramento: '',
        id_turma: '',
        id_entidade: '',
        id_projetopedagogico: '',
        id_modulo: '',
        nu_maxalunos: '',
        id_trilha: '',
        id_disciplina: '',
        id_tipodisciplina: '',
        nu_creditos: '',
        st_modulo: '',
        st_saladeaula: '',
        id_periodoletivo: '',
        st_periodoletivo: '',
        bl_ofertaexcepcional: ''
    },
    url: function() {
        return this.id ? '/api/vw-sala-oferta-turma/' + this.id : '/api/vw-sala-oferta-turma';
    }
});
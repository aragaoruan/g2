var VwUsuarioPerfilPedagogicoTOModel = Backbone.Model.extend({
    urlRoot: '/portal/login/selecionaperfilacesso',
    defaults: {
        id_perfil: '',
        id_perfilpedagogico: '',
        id_projetopedagogico: '',
        id_entidade: '',
        id_matricula: ''
    }
});

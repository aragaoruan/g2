var VwEncerramentoSalas = Backbone.Model.extend({
    defaults: {
        id_saladeaula: '',
        id_encerramentosala: '',
        id_disciplina: '',
        id_usuariocoordenador: '',
        id_usuariopedagogico: '',
        id_usuarioprofessor: '',
        id_usuariofinanceiro: '',
        id_areaconhecimento: '',
        id_entidade: '',
        id_tipodisciplina: '',
        id_aluno: '',

        dt_encerramentoprofessor: '',
        dt_encerramentocoordenador: '',
        dt_encerramentopedagogico: '',
        dt_encerramentofinanceiro: '',

        st_saladeaula: '',
        st_disciplina: '',
        st_professor: '',
        st_areaconhecimento: '',
        st_aluno: '',
        st_tituloavaliacao: '',
        st_upload: '',
        st_urlinteracoes: '',
        st_categoriasala: '',

        nu_cargahoraria: '',

        nu_valor: 0,
        nu_valorpago: 0
    }
});
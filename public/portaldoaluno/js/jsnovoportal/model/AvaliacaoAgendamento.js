var AvaliacaoAgendamentoModel = Backbone.Model.extend({
    defaults: {
        id_avaliacaoagendamento: '',
        id_avaliacaoaplicacao: '',
        id_matricula: '',
        id_situacao: '',
        id_usuariocadastro: '',
        id_avaliacao: '',
        dt_agendamento: '',
        dt_cadastro: '',
        bl_ativo: '',
        id_usuario: '',
        id_entidade: '',
        nu_presenca: '',
        st_justificativa: '',
        id_tipodeavaliacao: '',
        st_justifcancelamento: '',
        bl_provaglobal: '',
        id_usuariolancamento: ''
    }
});

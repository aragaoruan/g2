var VwAlunosAgendamentoModel = Backbone.Model.extend({
    defaults: {
        id_usuario: '',
        st_nomecompleto: '',
        st_cpf: '',
        st_email: '',
        id_situacaoagendamento: '',
        id_matricula: '',
        id_aplicadorprova: '',
        st_aplicadorprova: '',
        bl_ativo: '',
        dt_cadastro: '',
        bl_unica: '',
        dt_antecedenciaminima: '',
        dt_alteracaolimite: '',
        dt_aplicacao: '',
        id_usuariocadastro: '',
        id_horarioaula: '',
        nu_maxaplicacao: '',
        id_endereco: '',
        id_avaliacaoaplicacao: '',
        st_projetopedagogico: '',
        id_projetopedagogico: '',
        id_entidade: '',
        sg_uf: '',
        bl_ativoavagendamento: '',
        id_avaliacaoagendamento: ''
    }
});

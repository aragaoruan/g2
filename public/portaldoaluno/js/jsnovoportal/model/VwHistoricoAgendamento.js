/**
 * Created by Débora Castro on 12/05/2015.
 * <debora.castro@unyleya.com.br
 */
var VwHistoricoAgendamento = Backbone.Model.extend({
    defaults: {
        id_avaliacaoagendamento: '',
        dt_cadastro: '',
        id_tipodeavaliacao: '',
        id_matricula: '',
        id_tramite: '',
        id_situacao: '',
        id_entidade: '',
        id_usuario: '',
        bl_visivel: '',
        st_tipodeavaliacao: '',
        st_tramite: '',
        st_situacao: '',
        st_nomecompleto: '',
        id_ano: ''
    }
});
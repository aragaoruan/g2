// Habilitando as abas
$(document).ready(function () {
    $('#abas-financeiro').find('a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#lnk-parcelas').click(function () {
        $("#info-segunda-via").css("display", "block");
    });

    $('#lnk-plano-pagamento').click(function (e) {
        e.preventDefault();
        $("#info-segunda-via").css("display", "none");
        $.get('/portal/financeiro/plano-pagamento', '', function (data) {
            $('#tab-plano-pagamento').html(data);
        });
    });

    $('#lnk-trocar-cartao-recorrente').click(function (e) {
        e.preventDefault();
        $.get('/Financeiro/mudar-cartao-recorrente', {'id_venda': id_venda_trocar_cartao_recorrente}, function (data) {
            $('#tab-trocar-cartao-recorrente').html(data);
        });
    });
});
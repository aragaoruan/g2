/**
 * variável 'planoPag' instanciada em financeiro/index.phtml
 */
if ($("#tab-parcelas").length > 0) {
    $.getScript('/portal/financeiro/js-plano-pagamento');
    var id_venda_trocar_cartao_recorrente = null;
    document.onreadystatechange = function () {
        if (document.readyState === 'complete') {
            $.getScript(planoPag.urlG2 + "/js/backbone/models/vw-matricula.js?_=$.now()");
            var gridContainer = $("#tab-parcelas");
            gridContainer.html('');
            
            var mensagemSegundaViaExibida = false;

            exibirMensagemInfoSegundaViaBoleto = function (vendas) {
                var quantidadeVendasBoleto = vendas.filter(function (venda) {
                    return venda.lancamentos.filter(function (lancamento) {
                        return +lancamento.id_meiopagamento === MEIO_PAGAMENTO.BOLETO_BANCARIO;
                    }).length;
                });
                if (quantidadeVendasBoleto) {
                    var mensagemSegundaVia = "<div id=\"info-segunda-via\" class=\"alert alert-info\">\n" +
                        "<strong>Informação:</strong> é possível gerar a 2ª via do boleto atualizado" +
                        " no Portal do Aluno.</div>";
                    $('#container-mensagens-info').append(mensagemSegundaVia);
                    mensagemSegundaViaExibida = true;
                    return true;
                }
                return false;
            };

            renderParcelas = function () {

                if (typeof modal_boleto !== 'undefined') {
                    modal_boleto.modal('hide');
                }

                var st_projetopedagogico = '';
                gridContainer.append('<p/>&nbsp;<p/>');
                gridContainer.html('' +
                    '<div class="modal" role="dialog" aria-labelledby="modalBoletoLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" id="container-modal-boleto">' +
                    '<div class="modal-dialog" role="document">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">Emissão de Boletos</div>' +
                    '<div class="modal-body"></div>' +
                    '<div class="modal-footer" id="footerModal-botoes">' +
                    '<button class="btn btn-success bt-emitir-boleto">Confirmar</button>' +
                    '<button class="btn btn-success bt-ok-boleto">Entendi!</button>' +
                    '<button class="btn btn-warning cancel bt-cancel-boleto" data-dismiss="modal" aria-hidden="true"> Cancelar</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="text-right" style="padding-top: 10px; display: none;" id="div-mudar-cartao-recorrente"> Clique <a id="abrir-tela-mudar-cartao-recorrente" style="cursor: pointer;">AQUI</a> para informar os dados do Cartão de Crédito.</div><br/><br/>');
                var that = this;

                $.ajax({
                    url: '/default/matricula/retornar-responsavel-financeiro',
                    data: {
                        id_matricula: planoPag.id_matricula
                    },
                    success: function (response) {
                        if (response.type && response.type === 'success') {
                            that.responsaveis = response.text;

                            $.ajax({
                                dataType: 'json',
                                type: 'get',
                                url: '/portal/financeiro/retornar-vendas-aluno-json',
                                data: {
                                    id_evolucao: 10
                                },
                                success: function (data) {
                                    $.ajaxSetup({'async': false});

                                    // Ordenando da venda mais recente para a mais antiga.
                                    data.mensagem = _.sortBy(data.mensagem, 'id_venda').reverse();

                                    if (data.type === 'success') {
                                        var dados = data.mensagem;
                                        for (var i in dados) {
                                            st_projetopedagogico = '';

                                            for (var j in dados[i].produtos) {
                                                st_projetopedagogico += dados[i].produtos[j].st_produto;
                                                if (j < dados[i].produtos.length - 1) {
                                                    st_projetopedagogico += ', ';
                                                }
                                            }

                                            that.gerarGridParcelas(dados[i].id_venda,
                                                st_projetopedagogico
                                            );
                                        }

                                        if (!mensagemSegundaViaExibida) {
                                            that.exibirMensagemInfoSegundaViaBoleto(data.mensagem);
                                        }

                                    } else {
                                        gridContainer.append('Erro na operação, por favor tente novamente.');
                                    }
                                    $.ajaxSetup({'async': true});
                                },
                                complete: function () {
                                }
                            });

                        }
                    }
                });

            };

            emitirBoleto = function (id_lancamento, primeiraVez, modal_boleto, id_venda) {
                if (!modal_boleto) {
                    alert('para usar este método você precisa usar uma modal ou alterar o código para tal');
                    return false;
                }

                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: '/portal/financeiro/gerar-boleto',
                    data: {
                        id_lancamento: id_lancamento
                    },
                    success: function (data) {
                        if (data.st_urlboleto) {

                            if (id_venda) {
                                window.open('/loja/recebimento/?venda=' + id_venda + '&lancamento=' + id_lancamento,
                                    '_blank');
                            } else {
                                window.open(data.st_urlboleto, '_blank');
                            }

                            modal_boleto.modal('hide');
                            renderParcelas();
                        } else if (primeiraVez) {
                            setTimeout(emitirBoleto(id_lancamento, false, modal_boleto, id_venda), 700);
                        }

                        if (!primeiraVez && !data.st_urlboleto) {
                            modal_boleto.find('.bt-ok-boleto').show();
                            modal_boleto.find('.modal-body').html(data.st_mensagem);
                        }

                    },
                    error: function () {
                        modal_boleto.find('.bt-ok-boleto').show();
                        modal_boleto.find('.modal-body').html('Houve um problema ao tentar gerar o boleto. Por favor tente novamente mais tarde.');
                    }
                });

            };

            modalEmissaoBoleto = function (id_lancamento, id_venda) {
                var modal_boleto = $('#container-modal-boleto');
                modal_boleto.find('.modal-body').html("Deseja realmente emitir este boleto?");
                modal_boleto.find('.bt-ok-boleto').hide();
                modal_boleto.find('.bt-emitir-boleto').show();
                modal_boleto.find('.bt-cancel-boleto').show();
                modal_boleto.find('.bt-ok-boleto').unbind("click");
                modal_boleto.find('.bt-ok-boleto').click(function () {
                    modal_boleto.modal('hide');
                    renderParcelas();
                });
                modal_boleto.find('.bt-emitir-boleto').unbind("click");
                modal_boleto.find('.bt-emitir-boleto').click(function () {
                    modal_boleto.find('.modal-body').html('Por favor, aguarde...');
                    modal_boleto.find('.bt-emitir-boleto').hide();
                    modal_boleto.find('.bt-cancel-boleto').hide();
                    emitirBoleto(id_lancamento, true, modal_boleto, id_venda);
                });
                modal_boleto.modal('show');
            };

            gerarGridParcelas = function (id_venda, st_projetopedagogico) {
                var that = this;

                $('#abrir-tela-mudar-cartao-recorrente').click(function () {
                    window.open(planoPag.urlLoja + '/loja/pagamento/mudar-cartao-recorrente?venda=' + id_venda)
                });
                var DataGrid = Backbone.PageableCollection.extend({
                    url: '/default/matricula',
                    state: {
                        pageSize: 1000
                    },
                    mode: "client",
                    valorParcelasAPagar: function () {
                        var valorAPagar = 0;
                        this.fullCollection.each(function (i) {
                            if (!i.get('bl_quitado')) {
                                valorAPagar += parseFloat(i.get('nu_valor'));
                            }
                        });

                        return valorAPagar;
                    },
                    valorParcelasEmAtraso: function () {
                        var valorAtrasados = 0;
                        this.fullCollection.each(function (i) {
                            if (!i.get('bl_quitado')) {
                                var dt_vencimento = moment(i.get('dt_vencimento'), "DD/MM/YYYY");

                                var hoje = moment();
                                var diferenca = hoje.diff(dt_vencimento, 'days');
                                if (diferenca > 0) {
                                    valorAtrasados += parseFloat(i.get('nu_valor'));
                                }
                            }
                        });
                        return valorAtrasados;
                    }
                });

                // recupara a url e os parametros passados
                var dataResponse;

                if (that.responsaveis) {
                    for (var i in that.responsaveis) {
                        $.ajax({
                            dataType: 'json',
                            type: 'get',
                            url: '/default/matricula/retornar-vw-resumo-financeiro',
                            data: {
                                id_matricula: planoPag.id_matricula,
                                id_usuariolancamento: that.responsaveis[i].id_usuariolancamento,
                                id_venda: id_venda
                            },
                            success: function (data) {
                                dataResponse = data;
                            },
                            complete: function () {
                                if (dataResponse.type === 'success') {
                                    var dataGrid = new DataGrid(dataResponse.mensagem);
                                    var objTemp = dataResponse.mensagem[0];

                                    if (objTemp != undefined && objTemp && objTemp.get('bl_recorrente') && +objTemp.get('id_vendamatricula') === +objTemp.get('id_venda')) {
                                        $('#lnk-trocar-cartao-recorrente').show();
                                        id_venda_trocar_cartao_recorrente = objTemp.get('id_venda');
                                    }

                                    //Colunas comuns a grid de parcelas originais e atuais
                                    var columns = [
                                        {
                                            name: "nu_ordemparcela",
                                            label: "Nº Parcelas",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "nu_valor",
                                            label: "Valor",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_meiopagamento",
                                            label: "Meio de Pagamento",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_situacao",
                                            label: "Status",
                                            editable: false,
                                            cell: Backgrid.StringCell.extend({ // Pendente
                                                className: 'situacao-cell'
                                                , render: function () {
                                                    if (this.model.get('st_situacao') === 'Pendente' && this.model.get('st_urlboleto')) {
                                                        this.$el.html('Disponível');
                                                    } else {
                                                        this.$el.html(this.model.get('st_situacao'));
                                                    }
                                                    return this;
                                                }
                                            })
                                        },
                                        {
                                            name: "dt_vencimento",
                                            label: "Vencimento",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "dt_quitado",
                                            label: "Data da Baixa",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_link",
                                            label: "Recibo/2ª Via",
                                            editable: false,
                                            cell: Backgrid.UriCell.extend({
                                                render: function () {
                                                    var htmlGroup = '<div class="btn-group center">';

                                                    if (this.model.get('bl_quitado')) {
                                                        htmlGroup += '<a class="btn btn-default" href="' + this.model.get('st_link') + '" target="_blank"><i class="icon icon-file"></i></a>';
                                                    } else {
                                                        if (+this.model.get('id_meiopagamento') === MEIO_PAGAMENTO.BOLETO_BANCARIO) {
                                                            if (this.model.get('st_situacao') === 'Pendente' && this.model.get('st_urlboleto')) {
                                                                htmlGroup += '<a class="btn btn-default" style="color: green" title="Emitir Boleto" ' +
                                                                    'onclick="window.open(\'/loja/recebimento/?venda=' + this.model.get('id_venda') + '&lancamento=' + this.model.get('id_lancamento') + '\')"><i class="icon icon-print"></i> Visualizar Boleto</a>';
                                                            } else if (this.model.get('id_transacaoexterna') && !this.model.get('st_urlboleto')) {
                                                                htmlGroup += 'Boleto Solicitado';
                                                            } else {
                                                                htmlGroup += '<a class="btn btn-default" title="Gerar Boleto" onclick="modalEmissaoBoleto(' + this.model.get('id_lancamento') + ', ' + this.model.get('id_venda') + ')"><i class="icon icon-print"></i> Gerar Boleto</a>';
                                                            }
                                                        }
                                                    }

                                                    htmlGroup += '</div>';

                                                    this.$el.html(htmlGroup);

                                                    return this;
                                                }
                                            })
                                        }
                                    ];

                                    var RowGrid = Backgrid.Row.extend({
                                        initialize: function (options) {
                                            RowGrid.__super__.initialize.apply(this, arguments);
                                        }
                                    });

                                    //verifica se o json retornou algum resultado
                                    if (dataGrid.length > 0) {
                                        //monta a grid
                                        var pageableGrid = new Backgrid.Grid({
                                            row: RowGrid,
                                            columns: columns,
                                            collection: dataGrid,
                                            className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube table_pagamento'
                                        });

                                        // Renderiza o Grid
                                        gridContainer.append('<div class="row titulo-grid-parcelas">' +
                                            '<div class="col-md-12"><h4>' + st_projetopedagogico + " <small>" +
                                            "- Venda: " + id_venda + '</small></h4></div><div class="col-md-12"><h6>'
                                            + objTemp.get('st_nomecompleto') + '</h6></div>');

                                        // Insere alerta de desconto no boleto.
                                        if (+objTemp.get('id_meiopagamento') === MEIO_PAGAMENTO.BOLETO_BANCARIO) {
                                            gridContainer.append("<div class=\"alert alert-info info-boletos\" role=\"alert\"><strong>Importante:</strong>\n" +
                                                "        o desconto será exibido ao gerar o boleto.</div>")
                                        }

                                        gridContainer.append(pageableGrid.render().$el);

                                        //Colorem o status
                                        $('.situacao-cell').each(function () {
                                            if ($(this).text() === 'Pago') {
                                                $(this).addClass('pago');
                                            } else if ($(this).text() === 'Vencido') {
                                                $(this).addClass('vencido');
                                            }
                                        });

                                        //renderiza o paginator
                                        dataGrid.fetch({reset: true});
                                        gridContainer.append('</div><p>&nbsp;<br/></p></div>');
                                    }
                                } else {
                                    gridContainer.append(dataResponse.text);
                                }
                            }
                        });
                    }
                }
            };

            renderParcelas();
        }
    };
}
(function () {

    function loading() {
        $('.cortina-loader').show();
    }

    function loaded() {
        $('.cortina-loader').hide();
    }

    if (typeof dadosUsuario === 'undefined' || typeof dadosEndereco === 'undefined') {
        return;
    }

    /**
     * MODELS
     */
    var usuarioModel = new Backbone.Model(dadosUsuario),
        enderecoModel;

    /**
     * VIEWS
     */
    var layoutInit,
        enderecoInit,
        disciplinaInit,
        agendamentosInit,
        formInit,
        aplicacoesInit,
        historicoInit;

    /**
     * COLLECTIONS EXTEND
     */
    var UfCollection = Backbone.Collection.extend({
        model: Backbone.Model,
        url: '/default/util/retorna-uf'
    });

    var MunicipioCollection = Backbone.Collection.extend({
        model: Backbone.Model,
        url: '/default/util/retorna-municipio'
    });

    var ComboboxView = Marionette.CollectionView.extend({
        // template: _.template(''),
        tagName: 'select',
        collection: new Backbone.Collection(),
        childView: Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template('<%= st_item %>'),
            initialize: function (options) {
                this.model.set('id_item', this.model.get(options.id));
                this.model.set('st_item', this.model.get(options.label));
            },
            onRender: function () {
                this.$el.prop('value', this.model.get('id_item'));
            }
        }),
        initialize: function (options) {
            this.childViewOptions = {
                id: options.id,
                label: options.label
            };
        }
    });

    var MainLayout = Marionette.LayoutView.extend({
        el: '#main-layout',
        template: '#main-layout-view',
        regions: {
            enderecoRegion: '#endereco-region',
            disciplinasRegion: '#disciplinas-region',
            agendamentosRegion: '#agendamentos-region',
            formRegion: '#form-region'
        },
        ui: {
            inputDisciplina: '#id_disciplina'
        },
        onRender: function () {
            var bl_reagendamento = (window.location.search.indexOf('reagendamento=1') > -1);

            if (bl_reagendamento) {
                this.carregarAgendamentos();
            } else {
                enderecoInit = new EnderecoView();
                this.enderecoRegion.show(enderecoInit);
            }
        },
        carregarAgendamentos: function () {
            var that = this;

            if (usuarioModel.get('bl_provapordisciplina')) {
                $.getJSON('/default/gerencia-prova/get-disciplinas-apto?id_matricula=' + usuarioModel.get('id_matricula'), function (response) {
                    disciplinaInit = new DisciplinasView();
                    disciplinaInit.model.set('disciplinas', response);
                    that.disciplinasRegion.show(disciplinaInit);
                    that.carregarAvaliacoes();
                });
            } else {
                that.carregarAvaliacoes();
            }
        },
        carregarAvaliacoes: function () {
            var that = this;

            layoutInit.enderecoRegion.$el.hide();
            layoutInit.disciplinasRegion.$el.show();
            layoutInit.agendamentosRegion.$el.show();
            layoutInit.formRegion.$el.hide();

            var data = {
                id_matricula: usuarioModel.get('id_matricula')
            };

            if (usuarioModel.get('bl_provapordisciplina')) {
                data.id_disciplina = disciplinaInit.ui.inputDisciplina.val();
                data.id_tipoavaliacao = [1, 2, 4];
            } else {
                data.id_tipoavaliacao = 4;
            }

            agendamentosInit = new AgendamentosComposite();

            agendamentosInit.collection.reset();
            agendamentosInit.collection.fetch({
                data: data,
                complete: function () {
                    that.agendamentosRegion.show(agendamentosInit);
                }
            });
        }
    });

    var EnderecoModel = Backbone.Model.extend({
        url: '/portal/avaliacao/salvar-dados-basicos-usuario',
        defaults: {
            id: null,
            st_cpf: null,
            st_nomecompleto: null,
            st_login: null,
            id_titulacao: null,
            st_urlavatar: null,
            dt_nascimento: null,
            id_pais: null,
            st_nomepai: null,
            st_nomemae: null,
            st_sexo: null,
            st_identificacao: null,
            st_passaporte: null,
            st_tituloeleitor: null,
            st_certificadoreservista: null,
            sg_ufnascimento: null,
            id_municipionascimento: null,
            st_rg: null,
            st_orgaoexpeditor: null,
            dt_dataexpedicao: null,
            id_paisendereco: null,
            id_endereco: null,
            sg_uf: null,
            st_cep: null,
            id_municipio: null,
            id_tipoendereco: null,
            st_endereco: null,
            st_bairro: null,
            st_cidade: null,
            st_complemento: null,
            nu_numero: null,
            id_paisendereco_corresp: null,
            id_endereco_corresp: null,
            sg_uf_corresp: null,
            st_cep_corresp: null,
            id_municipio_corresp: null,
            id_tipoendereco_corresp: null,
            st_endereco_corresp: null,
            st_bairro_corresp: null,
            st_cidade_corresp: null,
            st_complemento_corresp: null,
            nu_numero_corresp: null,
            id_email: null,
            st_email: null,
            id_telefone2: null,
            id_tipotelefone2: null,
            nu_ddi2: null,
            nu_ddd2: null,
            nu_telefone2: null,
            id_telefone: null,
            id_tipotelefone: null,
            nu_ddi: null,
            nu_ddd: null,
            nu_telefone: null,
            id_informacaoacademicapessoa: null,
            id_nivelensino: null,
            st_curso: null,
            st_nomeinstituicao: null
        }
    });

    var EnderecoView = Marionette.ItemView.extend({
        template: '#endereco-view',
        ui: {
            formEndereco: '#form-endereco',
            inputUf: '#sg_uf',
            inputMunicipio: '#id_municipio',
            inputCep: '#st_cep'
        },
        events: {
            'change @ui.inputUf': 'carregarMunicipio',
            'submit @ui.formEndereco': 'salvarEndereco'
        },
        initialize: function () {
            enderecoModel = new EnderecoModel(dadosEndereco);
            this.model = enderecoModel;
        },
        onRender: function () {
            this.carregarUf();
            this.ui.inputCep.mask('99.999-999');
        },
        carregarUf: function () {
            var that = this;
            var selecionado = that.model.get('sg_uf_corresp');

            var ufComposite = new ComboboxView({
                el: this.ui.inputUf,
                collection: new UfCollection(),
                id: 'sg_uf',
                label: 'st_uf'
            });

            this.ui.inputUf.html('<option value="">[Carregando...]</option>');

            ufComposite.collection.fetch().done(function () {
                that.ui.inputUf.empty();
                ufComposite.render();
                if (selecionado) {
                    that.ui.inputUf.val(selecionado).trigger('change');
                }
            });
        },
        carregarMunicipio: function () {
            var that = this;
            var selecionado = that.model.get('id_municipio_corresp');

            var sg_uf = this.ui.inputUf.val();

            var municipioComposite = new ComboboxView({
                el: this.ui.inputMunicipio,
                collection: new MunicipioCollection(),
                id: 'id_municipio',
                label: 'st_nomemunicipio'
            });

            this.ui.inputMunicipio.html('<option value="">[Carregando...]</option>');

            municipioComposite.collection.fetch({
                data: {
                    sg_uf: sg_uf
                }
            }).done(function () {
                that.ui.inputMunicipio.empty();
                municipioComposite.render();
                if (selecionado) {
                    that.ui.inputMunicipio.val(selecionado);
                }
            });
        },
        salvarEndereco: function (e) {
            e && e.preventDefault ? e.preventDefault() : null;

            var data = {};
            var serialize = this.ui.formEndereco.serializeArray();

            serialize.forEach(function (input) {
                data[input.name] = input.value;
            });

            this.model.clear().save(data, {
                beforeSend: loading,
                complete: loaded,
                success: function () {
                    new PNotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'O endereço de correspondência foi atualizado.'
                    });
                    layoutInit.carregarAgendamentos();
                },
                error: function () {
                    new PNotify({
                        type: 'error',
                        title: 'Erro',
                        text: 'Houve um erro ao salvar o endereço, tente novamente.'
                    });
                }
            });
        }
    });

    var DisciplinasView = Marionette.ItemView.extend({
        model: new Backbone.Model(),
        template: '#disciplinas-view',
        ui: {
            inputDisciplina: '#id_disciplina'
        },
        events: {
            'change @ui.inputDisciplina': 'carregarAgendamentos'
        },
        carregarAgendamentos: function () {
            layoutInit.carregarAvaliacoes();
        }
    });

    var AgendamentosModel = Backbone.Model.extend({
        url: '/portal/avaliacao/salvar-agendamento',
        defaults: {
            bl_dataativa: 0
        }
    });

    var AgendamentosCollection = Backbone.Collection.extend({
        model: AgendamentosModel,
        url: '/default/gerencia-prova/get-agendamentos-chamadas/',
        //?id_matricula=442131&id_disciplina=8884&id_tipoavaliacao%5B%5D=1&id_tipoavaliacao%5B%5D=2&id_tipoavaliacao%5B%5D=4
        parse: function (response) {
            return response.avaliacoes || response;
        }
    });

    var AgendamentosView = Marionette.ItemView.extend({
        template: '#agendamentos-view',
        tagName: 'tr',
        className: 'vertical-middle',
        ui: {
            btnAbrirAgendamento: '.btn-abrir'
        },
        events: {
            'click @ui.btnAbrirAgendamento': 'carregarFormAgendamento'
        },
        onBeforeRender: function () {
            var agendamentos = this.model.get('agendamentos');

            var ultimo_agendamento = agendamentos[agendamentos.length - 1];

            var agendamento_ativo = agendamentos.filter(function (item) {
                return item.id_avaliacaoagendamento;
            }).pop();

            if (!agendamento_ativo) {
                agendamento_ativo = agendamentos[agendamentos.length - 1];
            }

            this.model.set({
                ultimo_agendamento: ultimo_agendamento,
                agendamento_ativo: agendamento_ativo,
                bl_agendamentoativo: ultimo_agendamento.id_avaliacaoagendamento && ultimo_agendamento.id_avaliacaoaplicacao
            });
        },
        onRender: function () {
            var agendamento_ativo = this.model.get('agendamento_ativo');
            var id_linhadenegocio = this.model.get('id_linhadenegocio');

            // Remover item se ele POSSUIR AGENDAMENTO com PRESENÇA lançada e não for GRADUAÇÃO
            if (agendamento_ativo.id_avaliacaoagendamento && agendamento_ativo.nu_presenca === 0 && id_linhadenegocio != 2) {
                this.$el.hide();
            }

            // Se a prova NÃO TEM um agendamento E NÃO TEM Aplicador disponível, então remover o item
            if (!agendamento_ativo.id_avaliacaoagendamento && !this.model.get('bl_aplicacaodisponivel')) {
                this.$el.hide();
            }

            // Remover item se for prova de RECUPERAÇÃO mas ainda não está liberado (Recuperação não fica apto automaticamente)
            if (this.model.get('id_tipodeavaliacao') && !agendamento_ativo.id_avaliacaoagendamento) {
                this.$el.hide();
            }
        },
        carregarFormAgendamento: function () {
            var model = new AgendamentosModel(this.model.get('ultimo_agendamento'));
            model.set(this.model.toJSON());

            formInit = new FormView({
                model: model
            });

            layoutInit.enderecoRegion.$el.hide();
            layoutInit.disciplinasRegion.$el.hide();
            layoutInit.agendamentosRegion.$el.hide();
            layoutInit.formRegion.show(formInit).$el.show();
        }
    });

    var AgendamentosComposite = Marionette.CompositeView.extend({
        template: '#agendamentos-composite',
        collection: new AgendamentosCollection(),
        childViewContainer: 'tbody',
        childView: AgendamentosView,
        ui: {
            btnHistorico: '#btn-historico',
            containerHistorico: '#historico-container'
        },
        events: {
            'click @ui.btnHistorico': 'carregarHistorico'
        },
        onRender: function () {
            this.$el.find('.loading-view').remove();
        },
        onShow: function () {
            var $container = this.$el.find(this.childViewContainer);
            if (!$container.find('tr:visible').length) {
                $container.append('<tr><td colspan="5">Nenhuma prova disponível para agendamento</td></tr>')
            }
        },
        carregarHistorico: function () {
            historicoInit = new HistoricoComposite({el: this.ui.containerHistorico});
            historicoInit.render().$el.modal('show');

            if (!historicoInit.collection.length) {
                historicoInit.collection.fetch();
            }
        }
    });

    // Form de Agendamento
    var FormView = Marionette.LayoutView.extend({
        template: '#form-view',
        regions: {
            aplicacoesRegion: '#aplicacoes-region'
        },
        ui: {
            formPesquisar: '#form-pesquisar',
            formSalvarAgendamento: '#form-salvar-agendamento',
            btnVoltar: "#btn-voltar",
            inputUf: '#sg_uf',
            inputMunicipio: '#id_municipio'
        },
        events: {
            'submit @ui.formPesquisar': 'pesquisarAplicacoes',
            'submit @ui.formSalvarAgendamento': 'salvarAgendamento',
            'change @ui.inputUf': 'carregarMunicipio',
            'click @ui.btnVoltar': 'voltar'
        },
        onRender: function () {
            this.carregarUf();
        },
        voltar: function () {
            layoutInit.carregarAvaliacoes();
        },
        pesquisarAplicacoes: function (e) {
            e && e.preventDefault ? e.preventDefault() : null;

            this.ui.formSalvarAgendamento.show();

            var data = {
                sg_uf: this.ui.inputUf.val(),
                id_municipio: this.ui.inputMunicipio.val(),
                id_avaliacao: this.model.get('id_avaliacao')
            };

            aplicacoesInit = new AplicacoesComposite();

            this.aplicacoesRegion.show(aplicacoesInit);

            aplicacoesInit.collection.fetch({
                data: data
            });
        },
        salvarAgendamento: function (e) {
            e && e.preventDefault ? e.preventDefault() : null;

            // Resetar o historico pra mais tarde refazer o fetch
            if (historicoInit) {
                historicoInit.collection.reset();
            }

            var id_avaliacaoaplicacao = this.$el.find('input[name="id_avaliacaoaplicacao"]:checked').val();

            if (!id_avaliacaoaplicacao) {
                new PNotify({
                    type: 'warning',
                    title: 'Aviso',
                    text: 'Selecione um local de prova'
                });
                return false;
            }

            this.model.set('id_avaliacaoaplicacao', id_avaliacaoaplicacao);

            this.model.save(usuarioModel.toJSON(), {
                beforeSend: loading,
                complete: loaded,
                success: function () {
                    new PNotify({
                        type: 'success',
                        title: 'Sucesso',
                        text: 'O agendamento foi salvo.'
                    });
                    layoutInit.carregarAvaliacoes();
                },
                error: function (model, response) {
                    new PNotify(JSON.parse(response.responseText));
                }
            });
        },
        carregarUf: function () {
            var that = this;
            this.ui.formSalvarAgendamento.hide();

            var ufComposite = new ComboboxView({
                el: this.ui.inputUf,
                collection: new UfCollection(),
                id: 'sg_uf',
                label: 'st_uf'
            });

            this.ui.inputUf.html('<option value="">[Carregando...]</option>');

            ufComposite.collection.fetch().done(function () {
                that.ui.inputUf.empty();
                ufComposite.render();
            });
        },
        carregarMunicipio: function () {
            var that = this;
            this.ui.formSalvarAgendamento.hide();

            var sg_uf = this.ui.inputUf.val();

            var MunicipioCollectionExtend = MunicipioCollection.extend({
                url: '/avaliacao/retorna-municipio-aplicacao'
            });

            var municipioComposite = new ComboboxView({
                el: this.ui.inputMunicipio,
                collection: new MunicipioCollectionExtend(),
                id: 'id_municipio',
                label: 'st_nomemunicipio'
            });

            this.ui.inputMunicipio.html('<option value="">[Carregando...]</option>');

            municipioComposite.collection.fetch({
                data: {
                    sg_uf: sg_uf
                }
            }).done(function () {
                that.ui.inputMunicipio.empty();
                municipioComposite.render();
            });
        }
    });

    /**
     * Lista Aplicações
     */
    var AplicacoesCollection = Backbone.Collection.extend({
        model: Backbone.Model,
        url: '/default/gerencia-prova/busca-avaliacoes/'
    });

    var AplicacoesView = Marionette.ItemView.extend({
        tagName: 'tr',
        template: '#aplicacoes-view'
    });

    var AplicacoesEmpty = Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template(''),
        onRender: function () {
            this.$el.html('<td colspan="8">Nenhuma local de prova disponível</td>');
        }
    });

    var AplicacoesComposite = Marionette.CompositeView.extend({
        template: '#aplicacoes-composite',
        collection: new AplicacoesCollection(),
        childViewContainer: 'tbody',
        childView: AplicacoesView,
        emptyView: AplicacoesEmpty,
        onRender: function () {
            this.$el.find('.loading-view').remove();
        }
    });

    var HistoricoCollection = Backbone.Collection.extend({
        model: Backbone.Model,
        url: '/portal/avaliacao/historico-agendamento'
    });

    var HistoricoView = Marionette.ItemView.extend({
        tagName: 'tr',
        template: '#historico-view'
    });

    var HistoricoEmpty = Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template(''),
        onRender: function () {
            this.$el.html('<td colspan="4">Nenhum registro de histórico</td>');
        }
    });

    var HistoricoComposite = Marionette.CompositeView.extend({
        template: '#historico-composite',
        collection: new HistoricoCollection(),
        childViewContainer: 'tbody',
        childView: HistoricoView,
        emptyView: HistoricoEmpty
    });

    layoutInit = new MainLayout();
    layoutInit.render();

})();
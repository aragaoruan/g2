/**
 *Array de Objetoso com indice e nome dos meses
 * @type {array}
 */
var Meses = [
    {
        id: 1,
        text: "Janeiro"
    },
    {
        id: 2,
        text: "Fevereiro"
    },
    {
        id: 3,
        text: "Março"
    },
    {
        id: 4,
        text: "Abril"
    },
    {
        id: 5,
        text: "Maio"
    },
    {
        id: 6,
        text: "Junho"
    },
    {
        id: 7,
        text: "Julho"
    },
    {
        id: 8,
        text: "Agosto"
    },
    {
        id: 9,
        text: "Setembro"
    },
    {
        id: 10,
        text: "Outubro"
    },
    {
        id: 11,
        text: "Novembro"
    },
    {
        id: 12,
        text: "Dezembro"
    }
];


var ItemExtratoColaborador = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-item-extrato'
});

var ExtratoColaboradorView = Marionette.CompositeView.extend({
    template: '#template-extrato',
    childView: ItemExtratoColaborador,
    childViewContainer: 'tbody',
    onRender: function () {
        var total = this.collection.total;
        this.$el.find('table tfoot tr td#col-total b').html(total.toFixed(2));
        this.populaSelectMes();
        var mes = this.$el.find('select[name="mes"]').val();
        var ano = this.$el.find('select[name="ano"]').val();
        this.getPeriodo(ano, mes);
    },
    populaSelectMes: function () {

        var MesModel = Backbone.Model.extend({
            defaults: {
                id: '',
                text: '',
                selected: false
            }
        });
        var MesesCollection = Backbone.Collection.extend({
            model: MesModel
        });
        var ItemViewMes = Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template("<%=text%>"),
            initialize: function () {
                this.$el.attr('value', this.model.id);
                if (this.model.get('selected')) {
                    this.$el.attr('selected', 'selected');
                }
                return this;
            }
        });
        var MesesCollectionView = Marionette.CollectionView.extend({
            childView: ItemViewMes,
            onRender: function () {
                this.$el.prepend('<option value="">Selecione</option>');
                return this;
            }
        });

        var ano = this.$el.find("select[name='ano']").val();
        var dtAtual = new Date();
        var mesCollection = new MesesCollection();
        var lacos = 11;
        if (ano == dtAtual.getFullYear()) {
            lacos = dtAtual.getMonth();
        }
        for (var i = 0; i <= lacos; i++) {
            var selected = false;
            if (ano == dtAtual.getFullYear() && i == dtAtual.getMonth()) {
                selected = true;
            }
            mesCollection.add({
                id: Meses[i]['id'],
                text: Meses[i]['text'],
                selected: selected
            });
        }
        this.$el.find("select[name='mes']").empty();
        var viewMeses = new MesesCollectionView({
            el: this.$el.find("select[name='mes']"),
            collection: mesCollection
        });
        viewMeses.render();
    },
    pesquisar: function (e) {
        var form = $(e.currentTarget);
        var that = this;
        form.validate({
            rules: {
                ano: "required",
                mes: "required"
            },
            messages: {
                ano: {required: 'Selecione o ano'},
                mes: {required: 'Selecione o mês'}
            },
            submitHandler: function () {
                var params = form.serialize();
                var collectionExtrato = new ExtratoColaboradorCollection();
                collectionExtrato.url = '/portal/financeiro/retornar-extrato?' + params;
                collectionExtrato.fetch({
                    beforeSend: function () {
                        $(".cortina").show();
                    },
                    success: function (collection) {
                        if (collection.length) {
                            that.collection = collection;
                            var mes = that.$el.find('select[name="mes"]').val();
                            var ano = that.$el.find('select[name="ano"]').val();
                            that.getPeriodo(ano, mes);
                            that.render();
                        }
                    },
                    complete: function () {
                        $(".cortina").hide();
                    }
                });
            }
        });
        return false;
    },
    getPeriodo: function (ano, mes) {
        var dtAtual = new Date();
        var lastDay = new Date(ano, mes, 0).getDate();
        var strPeriodo = '';
        strPeriodo = "01/" + mes + "/" + ano + " até " + lastDay + "/" + mes + "/" + ano;
        if (ano == dtAtual.getFullYear() && mes == (dtAtual.getMonth() + 1)) {
            strPeriodo = "01/" + mes + "/" + ano + " até " + dtAtual.getDate() + "/" + mes + "/" + ano;
        }
        this.$el.find('#periodo-filtro').html(strPeriodo);
    },
    events: {
        'change select[name="ano"]': 'populaSelectMes',
        'submit form#form-filtro': 'pesquisar'
    }
});

if ($('#container-extrato').length) {
    var collectionExtrato = new ExtratoColaboradorCollection();
    collectionExtrato.fetch({
        beforeSend: function () {
            $(".cortina").show();
        },
        success: function (collection) {
            if (collection.length) {
                var extrato = new ExtratoColaboradorView({
                    el: '#container-extrato',
                    collection: collection
                });
                extrato.render();
            }
        },
        complete: function () {
            $(".cortina").hide();
        }
    });
}
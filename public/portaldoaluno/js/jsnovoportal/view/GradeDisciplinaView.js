/**
 * JS da Grade Disciplina do portal.
 * @author helder.silva <helder.silva@unyleya.com.br>
 * @since 01-03-2016
 */

/**
 * MODELS
 */

var ModelSalaDeAula = Backbone.Model.extend({
    idAttribute: 'id_saladeaula'
});

var informacoesModel = Backbone.Model.extend({
    defaults: {
        'disciplinas': "",
        'id_campanhacomercial': "",
        'id_campanhapontualidade': "",
        'id_matricula': "",
        'id_produto': "",
        'id_projetopedagogico': "",
        'id_meiopagamento': "2", // meio de pagamento boleto como default
        'id_turma': "",
        'id_usuario': "",
        'nu_creditos': "",
        'nu_descontovalor': "",
        'nu_descontovalormostrar': "0,00",
        'nu_descontomostrar': "",
        'nu_maxdisciplinas': "",
        'nu_mindisciplinas': "",
        'nu_valorcontrato': "",
        'nu_valorcontratomostrar': "0,00",
        'nu_valorcredito': "",
        'nu_valorcreditomostrar': "0,00",
        'nu_valorentrada': "",
        'nu_valorentradamostrar': "0,00",
        'nu_valormensalidade': "",
        'nu_valormensalidademostrar': "0,00",
        'nu_valornegociacao': "",
        'nu_valornegociacaomostrar': "0,00",
        'st_curso': "",
        'st_nomecompleto': "",
        'st_turma': "",
        'st_campanhapontualidade': "",
        'nu_disciplinasselecionadas': 0,
        'nu_valormensalidadepontualidademostrar': "0,00"
    },
    url: '/grade-disciplina/calcula-valores/'
});


/**
 *  Collections
 */
var GridCollection = Backbone.Collection.extend({
    url: '/grade-disciplina/get-salas-by-turma/',
    parse: function (response) {
        this.groups = response.grupos || [];
        return response.salas || response || [];
    }
});

var CollectionSalaDeAula = Backbone.Collection.extend({
    model: ModelSalaDeAula
});

/**
 * Instâncias das modelos
 */
var model = new informacoesModel();

/**
 * Instâncias das collections
 */
var collectionDisciplinasSelecionadas = new CollectionSalaDeAula();
var collectionGrid = new GridCollection();
var collectionGridExcepcional = new GridCollection();
var collectionGridEstendidas = new GridCollection();

/**
 * LAYOUT PRINCIPAL
 */
var LayoutGrid = Backbone.Marionette.LayoutView.extend({
    template: '#layout-grid',
    tagName: 'div',
    regions: {
        gridDisciplinas: '#wrapper-disciplinas',
        gridDisciplinasExcepcionais: '#wrapper-disciplinas-excepcionais',
        gridDisciplinasEstendidas: '#wrapper-disciplinas-estendidas',
        regiao3: '#grid-contrato',
        regiao4: '#grid-mensalidade',
        regiao5: '#wrapper-informacoes-superior'
    },
    ui: {
        disciplinasSelecionadas: '#disciplinas_selec',
        meioPagamento: '#id_meiopagamento',
        linkPagamento: '#linkPagamento',
        btnSave: '.btnSave',
        btnCancel: '.btnCancel'
    },
    events: {
        'change @ui.meioPagamento': 'changeMeioPagamento',
        'click @ui.btnSave': 'actionSave',
        'click @ui.btnCancel': 'actionCancel'
    },
    initialize: function () {

    },
    beforeRender: function () {

    },
    onRender: function () {
        var that = this;

        var id_turma = this.model.get('id_turma');
        var id_produto = this.model.get('id_produto');
        var id_usuario = this.model.get('id_usuario');
        var id_matricula = this.model.get('id_matricula');
        var id_projetopedagogico = this.model.get('id_projetopedagogico');

        collectionDisciplinasSelecionadas.set(this.model.get('disciplinas'));

        // Armazenar os DEFERREDS do jQuery (fetchs), e executar uma ação após todos estiverem concluídos
        var deferreds = [];

        loading();

        deferreds.push(
            collectionGrid.fetch({
                async: true,
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    id_matricula: id_matricula,
                    id_projetopedagogico: id_projetopedagogico,
                    bl_ofertaexcepcional: false,
                    id_tipooferta: 1 // Padrão
                },
                error: function () {
                    that.gridError(that.gridDisciplinas);
                }
            })
        );

        deferreds.push(
            collectionGridExcepcional.fetch({
                async: true,
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    id_matricula: id_matricula,
                    id_projetopedagogico: id_projetopedagogico,
                    bl_ofertaexcepcional: true,
                    id_tipooferta: 1 // Padrão
                },
                error: function () {
                    that.gridError(that.gridDisciplinasExcepcionais);
                }
            })
        );

        deferreds.push(
            collectionGridEstendidas.fetch({
                async: true,
                data: {
                    id_turma: id_turma,
                    id_produto: id_produto,
                    id_usuario: id_usuario,
                    id_matricula: id_matricula,
                    id_projetopedagogico: id_projetopedagogico,
                    id_tipooferta: 2 // Estendida
                },
                error: function (data) {
                    that.gridError(that.gridDisciplinasEstendidas, data.length);
                }
            })
        );

        // Quando concluir o fetch das collections
        $.when.apply($, deferreds).always(function () {
            var gridPadrao = new GridCollectionView({
                groups: collectionGrid.groups,
                collection: collectionGrid,

                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGrid, collectionGridExcepcional, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_produto: that.model.get('id_produto'),
                        id_campanhacomercial: that.model.get('id_campanhacomercial'),
                        id_campanhapontualidade: that.model.get('id_campanhapontualidade')
                    }
                }
            });

            that.gridDisciplinas.show(gridPadrao);

            var gridExcepcional = new GridCollectionView({
                model: that.model,
                groups: collectionGridExcepcional.groups,
                collection: collectionGridExcepcional,

                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGrid, collectionGridExcepcional, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_produto: that.model.get('id_produto'),
                        id_campanhacomercial: that.model.get('id_campanhacomercial'),
                        id_campanhapontualidade: that.model.get('id_campanhapontualidade')
                    }
                }
            });

            that.gridDisciplinasExcepcionais.show(gridExcepcional);

            var gridEstendida = new GridCollectionView({
                model: that.model,
                groups: collectionGridEstendidas.groups,
                collection: collectionGridEstendidas,

                childViewOptions: {
                    gridModel: that.model,
                    collectionSelected: collectionDisciplinasSelecionadas,
                    collectionsToCheck: [collectionGrid, collectionGridExcepcional, collectionGridEstendidas],
                    selectedCountElem: that.ui.disciplinasSelecionadas,
                    fetchData: {
                        id_produto: that.model.get('id_produto'),
                        id_campanhacomercial: that.model.get('id_campanhacomercial'),
                        id_campanhapontualidade: that.model.get('id_campanhapontualidade')
                    }
                }
            });

            that.gridDisciplinasEstendidas.show(gridEstendida);

            loaded();
        });

        if (this.model.get('id_meiopagamento')) {
            this.ui.meioPagamento.val(this.model.get('id_meiopagamento'));
        }

        /**
         * Se o usuario salvar a forma de pagamento como Cartão de crédito vai habilitar o link de pagamento da loja
         */
        if (this.model.get('id_meiopagamento') == 1) {
            this.ui.linkPagamento.removeClass('hide');
        }

        //Instancias das Views
        var viewContratoInformacoes = new ViewContratoInformacoes();
        var viewMensalidadeInformacoes = new ViewMensalidadeInformacoes();
        var viewInformacoesSuperior = new ViewInformacoesSuperior();

        //Mostrando na tela as Views Instânciadas
        that.regiao3.show(viewContratoInformacoes);
        that.regiao4.show(viewMensalidadeInformacoes);
        that.regiao5.show(viewInformacoesSuperior);

    },
    actionSave: function () {
        var that = this;

        var nu_mindisciplinas = this.model.get('nu_mindisciplinas');
        var nu_maxdisciplinas = this.model.get('nu_maxdisciplinas');
        var nu_padraoselecionadas = collectionDisciplinasSelecionadas.filter(function (item) {
            return !item.get('bl_obrigatorio');
        }).length;

        /**
         * Faz a Verificação no momento do save para garantir que não será selecionado menos do minimo e mais do que
         * máximo de disciplinas selecionadas.
         */
        if (nu_padraoselecionadas < nu_mindisciplinas) {
            // Valida se possui o numero minimo de disciplinas selecionadas
            new PNotify({
                type: 'error',
                title: 'Atenção',
                text: 'A quantidade mínima de disciplinas não obrigatórias por período é de ' + nu_mindisciplinas
            });
        } else if (nu_padraoselecionadas > nu_maxdisciplinas) {
            // Valida se possui o numero de disciplinas selecionadas nao excedo o maximo
            new PNotify({
                type: 'error',
                title: 'Atenção',
                text: 'A quantidade máxima de disciplinas não obrigatórias por período é de ' + nu_maxdisciplinas
            });
        } else if (collectionGridEstendidas.length && !collectionDisciplinasSelecionadas.where({id_tipooferta: 2}).length) {
            // GII-7046
            // Quando salvar sem selecionar uma disciplina de Oferta Estendida, o sistema deve exibir uma mensagem
            bootbox.confirm({
                message: 'Nenhuma disciplina da oferta estendida foi selecionada. Você deseja selecionar alguma disciplina?',
                size: 'small',
                buttons: {
                    confirm: {
                        label: 'Sim',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Não'
                    }
                },
                callback: function (response) {
                    if (response) {
                        $('html, body').animate({scrollTop: $('#wrapper-disciplinas-estendidas').offset().top - 80});
                    } else {
                        that.saveDialog();
                    }
                }
            });
        } else {
            this.saveDialog();
        }
    },
    actionCancel: function () {
        //telaVendas.informacoesRegion.currentView.mostraTelaInformacoes();
    },
    saveDialog: function () {
        var that = this;

        // Template para o modal de confirmação;
        var template = '<p>' +
            '<div class="col-md-12">' +
            '<div class="col-md-3"><img width="100px" class="img-responsive" src="/images/assets/modal_grade.png" alt=""></div>' +
            '<div class="col-md-9">' +
            '<p>Prezado(a) ' + this.model.get('st_nomecompleto') + '</p>' +
            '<p>Você selecionou <b>' + this.model.get('nu_disciplinasselecionadas') + ' disciplinas</b> para cursar o seu próximo semestre letivo, por favor confirme os valores abaixo: </p>' +
            '</div>' +
            '</div>' +
            '<p>' +
            '<div class="col-md-12" style="padding-top: 75px">' +
            '<br><br>' +
            '<p><b>Valor do Contrato:</b> R$ ' + this.model.get('nu_valorcontratomostrar') + '</p>' +
            '<p><b>Valor da Mensalidade:</b> R$ ' + this.model.get('nu_valormensalidademostrar') + '</p>' +
            '<p><b>Valor da Mensalidade com Pontualidade: </b>R$ ' + this.model.get('nu_valormensalidadepontualidademostrar') + '</p>' +
            '</div>';

        //modal customizada
        var dialog = bootbox.confirm({
            title: 'Confirmação de Valores',
            message: template,
            callback: function (result) {
                if (result) {
                    // Atualiza a model global
                    that.model.set(that.model.toJSON());

                    //Caso tudo ok, salva.

                    $.ajax({
                        dataType: 'json',
                        type: 'post',
                        url: '/grade-disciplina/salvar-grade',
                        async: true,
                        data: {
                            disciplinasSelecionadas: collectionDisciplinasSelecionadas.toJSON(),
                            modelo: that.model.toJSON()
                        },
                        beforeSend: loading,
                        success: function (data) {
                            new PNotify(data);

                            if (that.model.get('id_meiopagamento') == 1) {
                                that.ui.linkPagamento.removeClass('hide');
                            } else {
                                that.ui.linkPagamento.addClass('hide');
                            }

                            $('#grid-disciplinas').html('<h1>' + data.text + '<br/><small>Você será redirecionado...</small></h1>');

                            // Redirecionar para a Home
                            setTimeout(function () {
                                window.location.href = window.location.href.replace('grade-disciplina', '');
                            }, 2000);
                        },
                        complete: loaded
                    })
                }
            }
        });
        //Inicializa o modal
        dialog.init();
    },
    changeMeioPagamento: function () {
        model.set({'id_meiopagamento': this.ui.meioPagamento.val()});
    },
    gridError: function (region, bl_showmessage) {
        if (bl_showmessage == 'undefined' || parseInt(bl_showmessage) > 0) {
            new PNotify({
                title: 'Erro',
                text: 'Não foi possível carregar a grade.',
                type: 'error'
            });
        }
        $(region.el).find('.alert').html('Nenhuma oferta disponível.');
    }
});

/**
 * ITEMVIEWS, o model Events de cada ItemView renderiza os elementos na tela caso haja uma alteração na modelo.
 */
var ViewInformacoesSuperior = Marionette.ItemView.extend({
    template: '#template-informacoes-superior',
    tagName: 'div',
    model: model,
    ui: {},
    events: {},
    initialize: function () {
        return this;
    },
    modelEvents: {
        change: function () {
            this.render()
        }

    }
});

var ViewMensalidadeInformacoes = Marionette.ItemView.extend({
    template: '#template-mensalidade-informacoes',
    tagName: 'div',
    model: model,
    ui: {},
    events: {},
    initialize: function () {
        return this;
    },
    modelEvents: {
        change: function () {
            this.render()
        }

    }
});

var ViewContratoInformacoes = Marionette.ItemView.extend({
    template: '#template-contrato-informacoes',
    tagName: 'div',
    model: model,
    initialize: function () {
        return this;
    },
    modelEvents: {
        change: function () {
            this.render()
        }

    }
});


$(document).ready(function () {
    if ($('#grid-disciplinas').length) {
        /**
         * Inicialmente, busca as informações na action retorna informações, depois seta a url da modelo para a action
         * calcula valores, pois o componente de grid sempre faz um fetch nessa action quando um elemento é marcado
         * e desmarcado, para não criar uma outra modelo essa foi a melhor forma.
         *
         * @type {string}
         */
        model.url = '/grade-disciplina/retorna-informacoes';
        model.fetch({
            beforeSend: loading(),
            success: function (response) {
                model.url = '/grade-disciplina/calcula-valores/';
                (new LayoutGrid({
                    el: '.grid',
                    model: this.model
                })).render();
            },
            complete: loaded()
        })
    }
});
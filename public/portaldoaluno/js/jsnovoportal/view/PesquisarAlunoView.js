/**
 * Created by Débora Castro on 20/11/14.
 * <debora.castro@unyleya.com.br
 */
var url = '';
var str = '';
var aluno = false;
var grade = false;
var viewModal = false;

var graduacao = false;

var modalPesquisaGradeAluno = null;
var tableA = null;
var tableB = null;
var templateItemGradeAlunoColaborador = null;
var templatePesquisarGradeAlunoView = null;
var templatePesquisarGradeAlunoPRRView = null;
var templateItemAluno = null;
var dadosAluno = null;
var templateTable = null;

this.verificaGraduacao();

// variáveis para a correta chamada dos components da view
if(graduacao) {
    modalPesquisaGradeAluno = "#modalPesquisaGradeAlunoGraduacao";
    tableA = ".tableGradeAGraduacao";
    tableB = ".tableGradeBGraduacao";
    templateItemGradeAlunoColaborador = "#templete-item-grade-aluno-graduacao";
    templatePesquisarGradeAlunoView = "#tabela-retorno-pesquisa-grade-aluno-graduacao";
    templatePesquisarGradeAlunoPRRView = "#tabela-retorno-pesquisa-grade-alunob-graduacao";
    templateItemAluno = "#template-item-aluno-graduacao";
    dadosAluno = ".dadosDoAlunoGraduacao";
    templateTable = '<div class="title-block clearfix">' +
        '<h3 class="h4-body-title uny class="col-md-12"><%=st_nomecompleto%></h3><div class="title-seperator"></div></div>' +
        '<div class="col-md-4">' +
        '<label class="label-control">Curso:</label>  <%=st_projetopedagogico %><br/>' +
        '<label class="label-control">Polo:</label>  <%=st_entidadematriz %>' +
        '</div>' +
        '<div class="col-md-4">' +
        '<label class="label-control">Data de Matrícula:</label>  <%= dt_inicio %><br/>' +
        '<label class="label-control">Turma: </label>  <%=st_turma %>' +
        '</div>' +
        '<div class="col-md-3"><label class="label-control">Situação:</label>  <%=st_situacao %> <br/>' +
        '<label class="label-control"> Evolução:</label>  <%=st_evolucao %>' +
        '</div>';
} else {
    modalPesquisaGradeAluno = "#modalPesquisaGradeAluno";
    tableA = ".tableGradeA";
    tableB = ".tableGradeB";
    templateItemGradeAlunoColaborador = "#templete-item-grade-aluno";
    templatePesquisarGradeAlunoView = "#tabela-retorno-pesquisa-grade-aluno";
    templatePesquisarGradeAlunoPRRView = "#tabela-retorno-pesquisa-grade-aluno";
    templateItemAluno = "#template-item-aluno";
    dadosAluno = ".dadosDoAluno";
    templateTable = ' <div class="title-block clearfix">' +
        '<h3 class="h4-body-title uny class="col-md-12"><%=st_nomecompleto%></h3><div class="title-seperator"></div></div>' +
        '<div class="col-md-4">' +
        '<label class="label-control">Curso:</label>  <%=st_projetopedagogico %>' +
        '</div>' +
        '<div class="col-md-4">' +
        '<label class="label-control">Data de Matrícula:</label>  <%= dt_inicio %><br/>' +
        '<label class="label-control">Turma: </label>  <%=st_turma %>' +
        '</div>' +
        '<div class="col-md-3"><label class="label-control">Situação:</label>  <%=st_situacao %> <br/>' +
        '<label class="label-control"> Evolução:</label>  <%=st_evolucao %>' +
        '</div>';
}

var modalAlunoPesquisaView = Marionette.CompositeView.extend({
    template: _.template(templateTable),
    model: VwMatricula,
    onRender: function () {
        $(modalPesquisaGradeAluno).removeClass('hide');
        this.gradeMatriculaNormal(modalPesquisaGradeAluno, tableA);
        this.gradeMatriculaPrr(modalPesquisaGradeAluno, tableB);
          return this;
    },
    gradeMatriculaNormal: function(modalPesquisa, table){
        var collectionGradeAluno = new GradeCurricularAlunoCollection();
        collectionGradeAluno.url = collectionGradeAluno.url+'?id_matricula='+this.model.get('id_matricula')+'&id_categoriasala=1';
        collectionGradeAluno.fetch({
            success: function (collection) {
                if (collection.length) {
                    $grade = new PesquisarGradeAlunoView({
                        el: $(modalPesquisa).find(table),
                        collection: collection
                    });
                    $grade.render();
                }
            }
        });
    },
    gradeMatriculaPrr: function(modalPesquisa, table){
        var collectionGradeAlunoB = new GradeCurricularAlunoCollection();
        collectionGradeAlunoB.url = collectionGradeAlunoB.url+'?id_matricula='+this.model.get('id_matricula')+'&id_categoriasala=2';
        collectionGradeAlunoB.fetch({
            success: function (collection) {
                if (collection.length) {
                    grade = new PesquisarGradeAlunoPRRView({
                        el: $(modalPesquisa).find(table),
                        collection: collection
                    });
                    grade.render();
                }
            }
        });
    }
});

var ItemGradeAlunoColaborador = Marionette.ItemView.extend({
    tagName: 'tr',
    template: templateItemGradeAlunoColaborador,
    onRender: function(){
    },
    events: {
    }
});

var PesquisarGradeAlunoView = Marionette.CompositeView.extend({
    template: templatePesquisarGradeAlunoView,
    childView: ItemGradeAlunoColaborador,
    childViewContainer: 'tbody',
    onRender: function () {

    }
});

var PesquisarGradeAlunoPRRView = Marionette.CompositeView.extend({
    template: templatePesquisarGradeAlunoPRRView,
    childView: ItemGradeAlunoColaborador,
    childViewContainer: 'tbody',
    onRender: function () {

    }
});

var ItemAlunoColaborador = Marionette.ItemView.extend({
    tagName: 'tr',
    template: templateItemAluno,
    onRender: function(){
        this.$el.css('cursor', 'pointer');
    },
    events: {
        'click .modalGrade': 'abreModalGrade'
    },
    abreModalGrade: function () {
        if(viewModal){
            viewModal.model = this.model.clone();
        }else{
            viewModal = new modalAlunoPesquisaView({
                el: $(modalPesquisaGradeAluno).find(dadosAluno),
                model: this.model.clone()
            });
        }
        viewModal.render();
    }
});

var alunoEmptyView = Marionette.ItemView.extend({
    template: '#template-aluno-lista-vazia',
    tagName: 'tr'
});

var PesquisarAlunoView = Marionette.CompositeView.extend({
    template: '#template-pesquisa-aluno',
    childView: ItemAlunoColaborador,
    childViewContainer: 'tbody',
    emptyView: alunoEmptyView,
    onRender: function () {

    },
    events: {
        'keyup #st_dadopesquisa': 'pesquisaAluno',
        'click .clearFilter': 'limparFiltro'
    },
    limparFiltro: function(){
        $('#st_dadopesquisa').val('');
        $('#table-resultados').addClass('hide');

    },
    pesquisaAluno: function () {
        $str = $('#st_dadopesquisa').val();

        if ($str.length > 7) {
            collectionMatricula.url = '';
            collectionMatricula.url = $url + '?st_dadopesquisa=' + $str;

            collectionMatricula.fetch({
                async: false,
                beforeSend: function () {
                    $(".cortina").show();
                    $('#st_dadopesquisa').attr('disabled', true);
                },
                success: function (collection) {
                    $('#table-resultados').removeClass('hide');

                    if (collection.length) {
                        $('#thead-result').removeClass('hide');

                        $aluno.collection = collection;
                    } else {
                        $('#thead-result').addClass('hide');
                    }
                },
                complete: function () {
                    $('#st_dadopesquisa').val($str);
                    $('#st_dadopesquisa').attr('disabled', false);
                    $('#st_dadopesquisa').focus();

                    $(".cortina").hide();
                }
            });
        } else {
            $('#table-resultados').addClass('hide');
        }

    }
});

if ($('#container-pesquisar-aluno').length) {
    var collectionMatricula = new PesquisarAlunoCollection();
    $url = collectionMatricula.url;
    collectionMatricula.fetch({
        beforeSend: function () {
            $(".cortina").show();
        },
        success: function (collection) {
            if (collection.length) {
                $aluno = new PesquisarAlunoView({
                    el: '#container-pesquisar-aluno',
                    collection: collection
                });
                $aluno.render();
                $('#table-resultados').addClass('hide');

            }
        },
        complete: function () {
            $(".cortina").hide();
        }
    });
}

function verificaGraduacao() {
    // 352 = entidade Graduação
    if (localStorage.portalid_entidade === '352') {
        graduacao = true;
    }
}
/**
 * Created by vinicius.avelino.alcantara on 16/10/15
 */

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             VARIAVEIS                             * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var mesesporextenso =  ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
var anoretorno = [];
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                             MODELS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * *                        COLLECTIONS                                * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */


var ItemExtratoProfessor = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#template-item-extrato'
});

var FormPesquisaExtratoView = Marionette.CompositeView.extend({
    template: '#template-extrato-pagamento',
    itemView: ItemExtratoProfessor,
    onRender: function()
    {
        this.popularMes();
        this.popularAno();
        var mes = this.$el.find('#id_mesextrato').val();
        var ano = this.$el.find('#id_anoextrato').val();
        this.pesquisa(mes,ano);
        return this;
    },

    popularMes: function()
    {
        collection = this.collection.toJSON();
        var MesModel = Backbone.Model.extend({
            defaults: {
                id: '',
                text: ''
            }
        });
        var MesesCollection = Backbone.Collection.extend({
            model: MesModel
        });

        var OptionMesView =  Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template('<%= text %>'),
            initialize: function() {
                this.$el.attr('value', this.model.id);
                this.$el.text(this.model.text);
                return this;
            }
        });

        var MesesCollectionView = Marionette.CollectionView.extend({
            itemView: OptionMesView,
            onRender: function () {
                this.$el.prepend('<option value="" selected>Selecione</option>');
                return this;
            }
        });

        var mesCollection = new MesesCollection();
        for(var y =0; y<12;y++) {
            mesCollection.add({
                id: y+1,
                text: mesesporextenso[y]
            });
        }

        var viewMeses = new MesesCollectionView({
            el: $('#id_mesextrato'),
            collection: mesCollection
        });
        viewMeses.render();
    },

    popularAno: function()
    {
        var that = this;
        var AnoModel = Backbone.Model.extend({
            defaults: {
                id: '',
                text: ''
            }
        });

        var AnoCollection = Backbone.Collection.extend({
        model: AnoModel
    });

        var OptionAnoView =  Marionette.ItemView.extend({
            tagName: 'option',
            template: _.template('<%= text %>'),
            initialize: function() {
                that.$el.attr('value', this.model.id);
                that.$el.text(this.model.text);
                return that;
            }
        });

        var AnosCollectionView = Marionette.CollectionView.extend({
            itemView: OptionAnoView,
            onRender: function () {
                $(this.el).prepend('<option value="" selected>Selecione</option>');
                return this;
            }
        });

        var anoCollection = new AnoCollection();
        var collection = this.collection.toJSON();
        for(var n = 0; n<this.collection.length;n++) {
            var data = new Date(collection[n]['st_encerramentofinanceiro']);
            var ano = data.getFullYear();
            var j = 0;
            if(ano>2000) {
                if (anoretorno.length) {
                    for (var i = 0; i < anoretorno.length; i++) {
                        if (ano != anoretorno[i]) {
                            j++;
                        }
                    }
                    if (j == anoretorno.length) {
                        anoretorno.push(ano);
                    }
                } else {
                    anoretorno.push(ano);
                }
            }
        }
        anoretorno.sort();
        for(var x = 0; x<anoretorno.length;x++) {
            anoCollection.add({
                id: anoretorno[x],
                text: anoretorno[x]
            });
        }

        var viewAnos = new AnosCollectionView({
            el: this.$el.find("#id_anoextrato"),
            collection: anoCollection
        });
        viewAnos.render();

    },

    pesquisa: function (e)
    {
        var that = this;
        var mes = that.$el.find('select[name="id_mesextrato"]').val();
        var ano = that.$el.find('select[name="id_anoextrato"]').val();
        if(mes != "" && ano != "")
        {
            that.extrato(mes,ano);
            that.render();
        }else{
            alert("Selecione um mês e um ano válido!");
        }
    },
    extrato: function(mes,ano)
    {
        var that = this;
        var columns = [];
        var modalbody = "<div class='modal-body'></div>";
        var pagos = 0;
        var i;
        var j;
        var valor;
        var paginacaoPrincipal;
        var grid;
        var ValorCollection = Backbone.Collection.extend({
            url: '',
            mode: "client" // page entirely on the client side
        });

        var DetalhesCollection = Backbone.Collection.extend({
            url: '',
            mode: "client" // page entirely on the client side
        });

        var collectionvalor = new ValorCollection();
        var collectiondetalhes = new DetalhesCollection();
        console.log(mes);
        console.log(ano);

        var cellInformacoes;

        if(mes != "" && ano != "")
        {

            columns = [
                {
                    name: "mesesporextenso[mes - 1]",
                    label: "Mês",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        render: function() {
                            this.$el.html(mesesporextenso[mes - 1]);
                            return this;
                        }
                    })
                },
                {
                    name: "ano",
                    label: "Ano",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        render: function() {
                            this.$el.html(ano);
                            return this;
                        }
                    })
                }
            ];
            collectionvalor.url = '/portal/extrato-pagamento/retornar-extrato-mes?mes='+mes+'&ano='+ano;
            collectionvalor.fetch({
                beforeSend: function () {
                    $(".cortina").show();
                },
                success: function (resultado) {
                    if (resultado.length) {
                        valor = resultado.toJSON();
                        pagos = valor[0]['valor'];
                        columns.push({
                            name: "pagos",
                            label: "Valor Total",
                            editable: false,
                            cell: Backgrid.Cell.extend({
                                render: function () {
                                    var texto = null;
                                    if (pagos.toString() !== '' ) {
                                        texto = 'R$ ' + pagos + ',00';
                                    }
                                    this.$el.html(texto);
                                    return this;
                                }
                            })
                        });
                    }

                    cellInformacoes = Backgrid.Cell.extend({
                        render: function () {
                            this.$el.html(_.template("<div class='btn-toolbar detalhes'><div class='btn'> <a class='btn  btn-success detalhes' title='Detalhes' href='javascript:void(0)'>" +
                                "<i class='icon icon-white icon-list-alt'></i> Detalhes</a></div></div>"));
                            return this;
                        },

                        events: {
                            "click .detalhes": "abrirDetalhamento"
                        },

                        abrirDetalhamento: function () {
                            collectiondetalhes.url = '/portal/extrato-pagamento/retornar-extrato-mes-completo?mes='+mes+'&ano='+ano;
                            collectiondetalhes.fetch({
                                beforeSend: function () {
                                    $(".cortina").show();
                                },
                                success:function(){
                                    var columnsd = [];
                                    columnsd = [
                                        {
                                            name: "id_encerramentosala",
                                            label: "ID",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_saladeaula",
                                            label: "Sala de Aula",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "nu_cargahoraria",
                                            label: "C.H",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_disciplina",
                                            label: "Disciplina",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "nu_alunos",
                                            label: "Alunos",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_encerramentocoordenador",
                                            label: "Pedagógico",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_encerramentopedagogico",
                                            label: "Financeiro",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "st_encerramentofinanceiro",
                                            label: "Pagamento",
                                            editable: false,
                                            cell: "string"
                                        },
                                        {
                                            name: "nu_valorpago",
                                            label: "Valor pago",
                                            editable: false,
                                            cell: "number"
                                        }
                                    ];
                                    var griddetalhamento = new Backgrid.Grid({
                                        columns: columnsd,
                                        collection: collectiondetalhes,
                                        className: 'table table-striped table-bordered table-hover table-detalhamento'
                                    });
                                    // $('#extrato').empty().append(griddetalhamento.render().el);
                                    //$('#extrato').show();
                                    $('#modal-body').empty().append(griddetalhamento.render().el);
                                    $('#modal-detalhamento').append('#modal-body');
                                    $('#modal-detalhamento').modal('show');
                                },
                                error: function()
                                {
                                    alert("Erro ao buscar detalhamento!");
                                },
                                complete: function () {
                                    $(".cortina").hide();
                                }
                            });
                        },
                        fecharModal: function () {
                            $('#close').modal('hide');
                        }
                    });


                    columns.push({
                        name: "informacoes",
                        label: "Informações",
                        editable: false,
                        cell: cellInformacoes
                    });

                },
                error: function()
                {
                    alert("Erro ao buscar valor do pagamento!1");
                },
                complete: function () {
                    $(".cortina").hide();
                }
            });
            grid = new Backgrid.Grid({
                columns: columns,
                collection: collectionvalor,
                className: 'table table-striped table-bordered table-hover'
            });
            $('#extrato').empty().append(grid.render().el);
            $('#extrato').render();
            $('#extrato').show();
        }else
        {
            alert("Selecione um mês e um ano válido!");
        }
    },

    events: {
        "click .pesquisaExtrato": "pesquisa"
    }

});

if ($('#template-extrato-professor').length){
    var collectionExtrato = new ExtratoPagamentoCollection();
    collectionExtrato.fetch({
        beforeSend: function () {
            $(".cortina").show();
        },
        success: function (collection) {
            if (collection.length) {
                var viewPesquisa = new FormPesquisaExtratoView({
                    el: '#template-extrato-professor',
                    collection: collection
                });
                viewPesquisa.render();
            }
        },
        complete: function () {
            $(".cortina").hide();
        }
    })
}


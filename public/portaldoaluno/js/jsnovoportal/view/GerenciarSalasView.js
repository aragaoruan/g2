/**
 * Created by paulo.silva on 22/07/14.
 */

function addHandler(obj, evnt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evnt.replace(/^on/, ''), handler, false);
    } else {
        if (obj[evnt]) {
            var origHandler = obj[evnt];
            obj[evnt] = function (evt) {
                origHandler(evt);
                handler(evt);
            };
        } else {
            obj[evnt] = function (evt) {
                handler(evt);
            };
        }
    }
}
addHandler(window, 'onerror', function (error, url, num) {
    console.log('ERRO: ' + error.message);
    console.log('------ ' + error.filename + '(' + error.lineno + ')');
    return true;
});


var thatSalas;

var id_perfil = $('#id_perfil').val();
var models;

var GerenciarSalasView = Backbone.View.extend({
    el: '.gerenciar-salas',
    model: GerenciarSalasModel,
    perfil: null,
    page: 1,
    searching: false,
    text: null,
    lastText: null,
    state: 'perfillist',
    atualCount: null,
    timeout: null,
    events: {
        'click .filtro-geral-salas a': 'filtroSalas',
        'click .subfiltro-geral-salas a': 'subfiltroSalas',
        'click .filtro-geral-disciplinas a': 'filtroDisciplinasProjetos',
        'click .subfiltro-geral-disciplinas a': 'subfiltroDisciplinasProjetos',
        'click .acesso-sala': 'acessoSala',
        'click .botao-voltar-projetos a': 'voltar',
        'click .backgrid-filter .clearFilter': 'limparFiltro',
        //'click #js-bt-pesquisar-perfil': 'buscarPerfis',
        'keydown input#buscaDeProjetos': 'buscarPerfisAuto',
        'submit form#formBuscaDeProjetos': 'buscarPerfisSubmit'
    },
    initialize: function () {
        this.perfil = new CoordenadorCollection();
        _.bindAll(this, "render", "carregarSalas");
        this.render();
        var that = this;
        $(window).scroll(function (e) {
            that.pesquisarPerfilScroll(e, this);
        });
    },
    render: function (data, busca) {
        this.text = null;
        this.pesquisarPerfis(data, busca);
    },
    buscarPerfisSubmit: function (e) {
        if (typeof e != "undefined")
            e.preventDefault();

        this.searching = false;
        this.buscarPerfis(e);
    },

    buscarPerfis: function (e) {
        if (typeof e != "undefined")
            e.preventDefault();

        this.page = 1;

        this.text = $('#buscaDeProjetos').val();
        if (this.state == 'carregarSalas') {
            this.voltar();
        }
        this.pesquisarPerfis({append: false});
    },
    buscarPerfisAuto: function (e) {
        var that = this;
        this.searching = false;
        this.atualCount = 0;
        clearTimeout(this.timeout);

        if ((!$(e.target).val() || $(e.target).val().length >= 2) && !this.searching && e.keyCode !== 13) {
            this.timeout = setTimeout(function () {
                that.buscarPerfis();
            }, 2000);
        }
    },
    pesquisarPerfis: function (data, busca) {

        busca = typeof busca == 'undefined' ? true : false;

        thatSalas = this;

        data || (data = {append: false});

        if (busca) {
            if (this.lastText && this.text && this.lastText === this.text) {
                return false;
            }
            if (this.atualCount > 0 && this.atualCount < 20) {
                return false;
            }
        }
        // if(typeof this.lastText != "undefined"){
        //    if(this.lastText == this.text && this.atualCount > 0 && this.atualCount < 20){
        //        this.searching = false;
        //        return false;
        //    }
        // }

        data.text = this.text;
        this.lastText = this.text;

        if (typeof data.append == "undefined") data.append = false;

        var that = this;
        var contexto = $('.uny.gestao');

        //var perfil = new CoordenadorCollection();
        var perfil = this.perfil;
        if (id_perfil != 1) {
            perfil.url = '/portal/index/coordenador?json=true';
        } else {
            perfil.url = '/portal/index/professor?json=true';
        }

        var data_filter = $.extend({}, {
            filtro: $('.filtro-geral-disciplinas a.portfolio-selected').data('rel')
        }, $('.subfiltro-geral-disciplinas a.portfolio-selected').data('filter'));

        if (data != null) {
            var dataParse = $.param($.extend({}, data, data_filter));
        } else {
            var dataParse = 'page=1&count=20&' + $.param(data_filter);
        }

        var table_projetos = thatSalas.$('#projetos-table');
        var salasdeula = $('#salasdeaula-wrapper');
        var salaWrapper = [];

        perfil.fetch({
            data: dataParse,
            success: function (a, b, c) {

                that.searching = false;

                that.atualCount = perfil.length;


                perfil.each(function (model) {
                    //Lista da esquerda para cursos e disciplinas
                    var ppView = new ProjetoPedagogicoView({
                        model: model
                    });
                    ppView.render();
                    table_projetos.append(ppView.el);

                    //Lista da direita para salas
                    var salasView = new SalaDeAulaView({
                        model: model
                    });
                    salasView.render();
                    salaWrapper.push(salasView.el);

                });

                if ($('.sala-wrapper-filha').length && !data.append)
                    $('.sala-wrapper-filha').remove();

                salasdeula.append(salaWrapper);

            },
            complete: function () {
                contexto.removeClass('cortina-loader');
                that.searching = false;
            },
            beforeSend: function () {
                that.searching = true;

                if (!data.append || that.page == 1)
                    table_projetos.empty();


                contexto.addClass('cortina-loader');
            }
        });

    },

    voltar: function () {
        $(".info-salas").hide();
        $('.uny.gestao').show();
        $('.botao-voltar-projetos').hide();
        $('.filtro-geral-disciplinas').show().parent().removeClass('col-md-7').addClass('col-md-8');
        $('.subfiltro-geral-disciplinas').show();
        this.state = 'perfillist';
    },
    limparFiltro: function (ct) {
        $(ct.currentTarget).prev().val('').change();
    },

    pesquisarPerfilScroll: function (e, t) {
        var that = this;
        var markerPosition = $("#position-marker").offset().top;

        var scrollTop = $(t).scrollTop() + $(t).height();
        if (scrollTop >= markerPosition && !that.searching && this.state == 'perfillist') {
            //console.log(markerPosition, scrollTop);
            that.page = that.page + 1;
            that.searching = true;
            that.pesquisarPerfis({page: that.page, append: true});
        } // if scroll
    },

    carregarSalas: function (ct, model) {

        this.state = 'carregarSalas';

        var rel = null;

        var carregarLinha = Backgrid.Cell.extend({
            render: function () {
                var id_saladeaula = this.model.get("id_saladeaula");
                var id_disciplina = this.model.get("id_disciplina");
                var id_sistema = this.model.get('id_sistema');
                this.$el.html("<div class='btn-group' data-id-sala='" + id_saladeaula + "' data-id-disciplina='" + id_disciplina + "' data-id-sistema='" + id_sistema + "'>" +
                    "<button class='btn btn-default uny acesso-sala'>Entrar</button>" +
                    "<button class='btn btn-default uny acesso-sala gestao-sala'>Gestão</button></div>");
                return this;
            }
        });

        var st_saladeaula = Backgrid.Cell.extend({
            render: function () {
                var st_saladeaula = this.model.get('st_saladeaula');
                this.$el.html("<div class='st_saladeaula'>" + st_saladeaula + "</div>");
                return this;
            }
        });

        var filtroGeral = $('.filtro-geral-salas a.portfolio-selected');
        var subfiltroGeral = $('.subfiltro-geral-salas a.portfolio-selected');

        if (id_perfil != 1) {
            rel = model.get('id_projetopedagogico');
        } else {
            rel = model.get('id_disciplina');
        }

        data = $.extend({}, {
            id_projetopedagogico: rel,
            id_status: filtroGeral.data('rel')
        }, subfiltroGeral.data('filter'));

        //Catch dos elementos com os devidos atributos
        var elemRenderDad = $(".info-salas[data-rel=" + rel + "]");
        var elemRender = $(".info-salas[data-rel=" + rel + "] .lista-salas");

        $('.info-salas, .info-salas-geral').hide();
        $('.coordenador-table tr').removeClass('ativo');

        if ($(".info-salas").data('rel') != '') {
            $(".info-salas[data-rel='" + rel + "']").addClass('visivel').show();

            var pai_tabela = $('.uny.gestao');
            pai_tabela.hide();
            $('.botao-voltar-projetos').show();
            $('.filtro-geral-disciplinas').hide().parent().removeClass('col-md-8').addClass('col-md-7');
            $('.subfiltro-geral-disciplinas').hide();

            //Definição das colunas que irão aparecer no backgrid
            var columns = [
                {
                    name: "st_saladeaula",
                    label: "Sala",
                    cell: st_saladeaula,
                    editable: false
                },
                {
                    name: "st_tutor",
                    label: "Tutor",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_abertura",
                    label: "Abertura",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_encerramentoextensao",
                    label: "Término",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        render: function () {
                            var st_encerramento = this.model.get('st_encerramento') || '-';
                            var st_encerramentoextensao = this.model.get('st_encerramentoextensao') || '-';

                            var html = '';
                            if (st_encerramentoextensao != '-' && st_encerramentoextensao != st_encerramento) {
                                html = '<div style="color: #49AFCD; white-space: nowrap;">' +
                                    st_encerramentoextensao +
                                    ' <div style="color: #333333; display: inline-block;">' +
                                    '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANVJREFUeNpiTEtLY4ACNiAGcUKA2BIqdhyI1wDxLCD+BRJggUpIA/EWIDZgQAX2UJwMxD5A/JQJavJ2NMV/gVgPiB2gfAOoGjYmqDN00UxmBmJOKIYBkJo0kJPCGbCDk1jEwkE2mKEJvgbiCCC2xaLBjAmL4B0gfofmHBj4BdJwCk1QHYj34XDqBZCGZWiCK5GC+hea3DKQhrlAfBZJEOQcCaTIhAGQmrlMUFNAkbIDKlENxE+A2ANJ8Q6oml8wT78AYk8gToG6/wsU74OKeULVMAAEGABizSkAgk+tyAAAAABJRU5ErkJggg==" alt="(i)" data-toggle="popover" data-placement="left" data-content="Data de encerramento após extensão. Data original: ' + st_encerramento + '"/>' +
                                    '</div>' +
                                    '</div>';
                            } else {
                                html = st_encerramento
                            }

                            this.$el.html(html);
                            this.$el.find('img').popover({trigger: 'hover'});

                            return this;
                        }
                    })
                },
                {
                    name: "st_alocados",
                    label: "Alunos Alocados",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_semacesso",
                    label: "Alunos Sem Acesso",
                    cell: "string",
                    editable: false
                },
                {
                    name: "nu_interacoes",
                    label: "Novas Interações",
                    editable: false,
                    cell: Backgrid.Cell.extend({
                        render: function () {
                            var nu_interacoes = this.model.get('nu_interacoes');
                            var html = '';
                            if (nu_interacoes > 0) {
                                html = '<div style="color: #FF0000; font-weight: bold;">' + nu_interacoes + '</div>';
                            } else {
                                html = nu_interacoes
                            }
                            this.$el.html(html);
                            return this;
                        }
                    })
                },
                {
                    name: "acoes",
                    label: "Ações",
                    cell: carregarLinha,
                    editable: false
                }
            ];

            //Para perfis diferentes de: Observador Institucional e Suporte,
            //   a seguinte coluna não ficara visível: Tutor.
            if (id_perfil != PERFIL_PEDAGOGICO.OBSERVADOR_INSTITUCIONAL && id_perfil != PERFIL_PEDAGOGICO.SUPORTE) {
                for (var i = 0; i < columns.length; i++) {
                    if (columns[i].name == 'st_tutor') {
                        columns.splice(i, 1);
                    }
                }
            }

            //Para perfis diferentes de: Coordenador de Projeto, Observador Institucional, Suporte e Tutor,
            //   as seguintes colunas não ficaram visíveis: Alunos Alocados na Sala e Alunos sem acesso na Sala.
            if (id_perfil != PERFIL_PEDAGOGICO.COORDENADOR_PROJETO
                && id_perfil != PERFIL_PEDAGOGICO.OBSERVADOR_INSTITUCIONAL
                && id_perfil != PERFIL_PEDAGOGICO.SUPORTE
                && id_perfil != PERFIL_PEDAGOGICO.PROFESSOR) {
                for (var j = 0; j < columns.length; j++) {
                    if (columns[j].name == 'st_alocados') {
                        columns.splice(j, 1);
                    }
                    if (columns[j].name == 'st_semacesso') {
                        columns.splice(j, 1);
                    }
                }
            }

            //Para perfis diferentes de: Aluno, a seguinte coluna não ficaram visíveis: Novas Interações.
            var perfil = [
                PERFIL_PEDAGOGICO.COORDENADOR_PROJETO,
                PERFIL_PEDAGOGICO.OBSERVADOR_INSTITUCIONAL,
                PERFIL_PEDAGOGICO.SUPORTE,
                PERFIL_PEDAGOGICO.PROFESSOR
            ];
            if (perfil.indexOf(id_perfil) != -1) {
                // if (id_perfil != PERFIL_PEDAGOGICO.PROFESSOR) {
                for (var x = 0; x < columns.length; x++) {
                    if (columns[x].name == 'nu_interacoes') {
                        columns.splice(x, 1);
                    }
                }
            }

            // Inicialização do objeto BackGrid
            var perfil = new CoordenadorCollection();
            var grid = new Backgrid.Grid({
                className: 'backgrid table uny table-bordered listaDeSalas',
                columns: columns,
                collection: perfil
            });

            elemRender.empty().append(grid.render().el);

            perfil.fetch({
                type: 'GET',
                data: $.param(data),
                success: function () {
                    $('.buscaDeProjetosSalas', elemRenderDad).fastLiveFilter('.listaDeSalas tbody', {
                        selector: 'div.st_saladeaula'
                    });
                },
                complete: function () {
                    $('.cortina', elemRenderDad).fadeOut();
                },
                beforeSend: function () {
                    //Show da cortina de carregamento
                    $('.cortina', elemRenderDad).show();
                }
            });

        } else {
            return false;
        }
    },
    filtroSalas: function (ev) {
        var target = $(ev.currentTarget);

        $('.filtro-geral-salas a, .subfiltro-geral-salas a').removeClass('portfolio-selected');
        target.addClass('portfolio-selected');

        target.closest('.filtro-grupo').find('.subfiltro-geral-salas li:first a').addClass('portfolio-selected');

        thatSalas.carregarSalas(null, models);
    },
    subfiltroSalas: function (ev) {
        $('.subfiltro-geral-salas a').removeClass('portfolio-selected');
        $(ev.currentTarget).addClass('portfolio-selected');

        thatSalas.carregarSalas(null, models);
    },
    filtroDisciplinasProjetos: function (ev) {
        var target = $(ev.currentTarget);

        this.page = 1;

        $('.filtro-geral-disciplinas a, .subfiltro-geral-disciplinas a').removeClass('portfolio-selected');
        target.addClass('portfolio-selected');

        target.closest('.filtro-grupo').find('.subfiltro-geral-disciplinas li:first a').addClass('portfolio-selected');

        var data = {
            'json': true
        };

        thatSalas.render(data, false);
    },
    subfiltroDisciplinasProjetos: function (ev) {
        $('.subfiltro-geral-disciplinas a').removeClass('portfolio-selected');
        $(ev.currentTarget).addClass('portfolio-selected');

        this.page = 1;

        var data = {
            'json': true
        };

        thatSalas.render(data, false);
    },
    acessoSala: function (ev) {

        var id_saladeaula = $(ev.currentTarget).parent().data('id-sala');
        var id_disciplina = $(ev.currentTarget).parent().data('id-disciplina');
        var id_sistema = $(ev.currentTarget).parent().data('id-sistema');

        $('#frm_id_disciplina').attr("value", id_disciplina);
        $('#frm_id_saladeaula').attr("value", id_saladeaula);

        if (id_sistema == 15) {
            $('#frmAcessoSala').attr("target", id_saladeaula);
        }

        if ($(ev.currentTarget).hasClass('gestao-sala')) {
            $('#frmAcessoSala').attr('action', '/portal/sala-de-aula/gestao');
        }

        $('#frmAcessoSala').submit();
    }
});

var ProjetoPedagogicoView = Backbone.View.extend({
    tagName: 'tr',
    className: 'menu-item',
    events: {
        'click .mostrar-projeto': 'carregarSalasAcesso'
    },
    initialize: function () {
        this.template = _.template($('#projetos-pedagogicos').html());
        _.bindAll(this, "render");
    },
    render: function () {
        this.$el.append(this.template(this.model.toJSON()));
    },
    carregarSalasAcesso: function (ev) {
        models = this.model;
        thatSalas.carregarSalas(ev, this.model);
    }
});

var SalaDeAulaView = Backbone.View.extend({
    tagName: 'div',
    className: 'sala-wrapper-filha',
    initialize: function () {
        this.template = _.template($('#salasdeaula').html());
        _.bindAll(this, "render");
    },
    render: function () {
        this.$el.append(this.template(this.model.toJSON()));
    }
});

$(document).ready(function () {
    if ($('.gerenciar-salas').length > 0) {
        var app = new GerenciarSalasView();
    }
});

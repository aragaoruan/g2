var mensagens = new MensagemCollection();

var that;
var nao_lidas;
var lidas;
var content = $('.content-msg');

var MensagemView = Backbone.View.extend({
    el: '#mensagens',
    collection: mensagens,
    events: {
        "click .voltar.acao": "voltar",
        "click .toggle-mensagem": 'toggleMensagem',
        "click .todas-mensagens": 'showTodasAsMensagens',
        "click .nao-lidas-mensagens": 'showNaoLidasMensagens',
        "click .outras-mensagens": 'showOutrasMensagens'
    },
    initialize: function () {
        this.template = _.template($("#mensagens-content").html());
        _.bindAll(this, "render", "voltar");
        that = this;
    },
    render: function () {
        that.$el.find('.content-msg').html(this.template);
        nao_lidas = that.$('.nao-lidas .lista-msg');
        lidas = that.$('.todas .lista-msg');
        this.collection.fetch({
            success: function (resp) {
                var count = 0;
                mensagens.each(function (mensagem) {
                    var msgView = new MensagemUnicaView({
                        model: mensagem
                    });
                    msgView.render();
                    if (mensagem.get('id_evolucao') == 42) {
                        nao_lidas.append(msgView.el);
                        count += 1;
                    } else {
                        lidas.append(msgView.el);
                    }
                });

                that.$el.find('.accordion .title.active + .desc').show();
                that.$el.find('.accordion .title.active .open-icon').addClass('close-icon');

                if (!content.hasClass('filtro'))
                    that.countMsg(count);

                if (lidas.is(':empty')) {
                    lidas.empty().append('<li class="no-msg">Você não possui mensagens lidas.</li>');
                }

                if (nao_lidas.is(':empty')) {
                    nao_lidas.empty().append('<li class="no-msg">Parabéns, todas as suas mensagens foram lidas. :)</li>');
                }

                return this;
            },
            complete: function () {

            }
        });
    },
    countMsg: function (count) {
        if (count != 0)
            $('.login .badge').show().html(count);
        else
            $('.login .badge').hide();
    },
    showNaoLidasMensagens: function (e) {
        this.voltar();
        that.toggleClassItem($(e.currentTarget));
        that.$('.nao-lidas.box-msg').show();
        that.$('.todas.box-msg').hide();
    },
    showTodasAsMensagens: function (e) {
        that.toggleClassItem($(e.currentTarget));
        this.voltar();
    },
    voltar: function () {
        that.$('.nao-lidas.box-msg').show();
        that.$('.todas.box-msg').show();
        mensagemRouter.navigate("", {trigger: true});
    },
    showOutrasMensagens: function (e) {
        that.toggleClassItem($(e.currentTarget));
        mensagemRouter.navigate('m/filtro/importante', {trigger: true});
    },
    toggleClassItem: function (e) {
        if (e != undefined) {
            $('.menu-lateral-msg a').removeClass('active');
            e.addClass('active');
        }
    }
});

var MensagemUnicaView = Backbone.View.extend({
    tagName: 'li',
    events: {
        "click .show-mensagem": "abrirMensagem",
        "click .voltar.acao": "voltar",
        "click .marcar-nao-lida": "setNaoLida"
    },
    initialize: function () {
        this.template = _.template($('#mensagem-unica-pendentes-content').html());
        _.bindAll(this, "render", "abrirMensagem");
    },
    render: function () {
        this.$el.append(this.template(this.model.toJSON()));
    },
    abrirMensagem: function (e) {
        var id_enviomensagem = this.model.get('id_enviomensagem');
        var id_matricula = this.model.get('id_matricula');
        this.model.set({
            id_evolucao: 43
        });
        //console.log(this.model); return false;
        this.model.save(null, {
            success: function (data) {
                if (data) {
                    mensagemRouter.navigate('m/' + id_enviomensagem +'/mat/'+id_matricula, {trigger: true});
                }
            },
            error: function () {
                //console.log(data);
                //console.log('ERROR');
            }
        });
    },
    setNaoLida: function () {
        this.model.set({
            id_evolucao: 42
        });

        var elemen = $(this.el);

        elemen.find('.marcar-nao-lida').removeClass('marcar-nao-lida').addClass('icon-envelope');
        if (nao_lidas.children('.no-msg').length) {
            nao_lidas.empty().append(elemen);
        } else {
            nao_lidas.append(elemen);
        }

        if (lidas.children().length == 0) {
            lidas.append('<li class="no-msg">Você possui mensagens não lidas.</li>')
        }

        this.model.save(null, {
            success: function (data) {
                //console.log('Salvo com sucesso')
            },
            error: function () {
                //console.log('Error')
            }
        });
    }
});

var MensagemShowView = Backbone.View.extend({
    tagName: 'div',
    tagClass: '.show-msg',
    render: function () {
        this.template = _.template($('#mensagem-unica-content').html());
        $('.mensagens .acoes').show();
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

var MensagemRouter = Backbone.Router.extend({
    routes: {
        "m/:id/mat/:idmat": "visualizarMessagem",
        "": "index",
        "m/filtro/:filter": "visualizarMensagensImportantes"
    },
    index: function () {
        content.removeClass('filtro');
        app.render();
        that.toggleClassItem($('.todas-mensagens'));
        $('.mensagens .acoes').hide();
    },
    visualizarMessagem: function (id, idmat) {
        content.removeClass('filtro');
        var msgShow = new MensagemShowView();

        if (mensagens.length != 0) {
            var obj = mensagens.find(function (model) {
                return (model.get('id_enviomensagem') == id && model.get('id_matricula') == idmat);
            });
            msgShow.model = obj;
            msgShow.render();
            $('.content-msg').empty().html(msgShow.el);
        } else {
            var menCo = new MensagemCollection();
            var msgShowRest = new MensagemShowView();
            menCo.fetch({
                data: {
                    id: id
                },
                success: function () {
                    menCo.each(function (mensagem) {
                        msgShow.model = mensagem;
                        msgShow.render();
                        $('.content-msg').empty().html(msgShow.el);

                        mensagem.set({
                            id_evolucao: 43
                        });

                        mensagem.save(null, {
                            success: function (data) {
                                //console.log(data);
                            },
                            error: function (e) {
                                //console.log(e);
                            }
                        });
                    }, this);
                    return this;
                },
                complete: function () {

                }
            });
        }
    },
    visualizarMensagensImportantes: function (filter) {
        content.addClass('filtro');
        var obj = new MensagemView();
        var orig_url = obj.collection.url;
        obj.collection.url += '&filtro=' + filter;
        obj.render();
        obj.collection.url = orig_url
    }
});


if ($('#mensagens').length > 0) {
    var app = new MensagemView({collection: mensagens});
    var mensagemRouter = new MensagemRouter();
    Backbone.history.start();
}



$.ajaxSetup({async: false});
/**
 * Created by neemias on 09/07/15.
 */
var viewSecretariaSelecionadas;
var Produto = Backbone.Model.extend({});

var ServicosCollection = Backbone.Collection.extend({
    model: Produto,
    url: "/portal/secretaria/lista-servicos"
});


/************************ ItemView *********************/
var RequerimentoItemView = Marionette.ItemView.extend({
    model: Produto,
    tagName: 'tr',
    template: '#requerimento-item',
    ui: {
        check_select: 'input[name="check_select"]'
    },
    selecionaModelo: function () {
        if (this.ui.check_select.is(":checked")) {
            viewSecretariaSelecionadas.collection.add(this.model);
        } else {
            viewSecretariaSelecionadas.collection.remove(this.model);
        }
    },
    events: {
        'change input[name="check_select"]': 'selecionaModelo'
    }
});

var RequerimentoSelecionadoItemView = Marionette.ItemView.extend({
    model: Produto,
    tagName: 'tr',
    template: '#requerimento-item-selecionados'
});

/************************ Composite *********************/
var SecretariaComposite = Marionette.CompositeView.extend({
    collection: new ServicosCollection(),
    childView: RequerimentoItemView,
    childViewContainer: 'tbody',
    template: '#requerimento-content'
});

var SecretariaSelecionadosComposite = Marionette.CompositeView.extend({
        childView: RequerimentoSelecionadoItemView,
        childViewContainer: 'tbody',
        initialize: function () {
            this.collection = new ServicosCollection();
            viewSecretariaSelecionadas = this;
            return this;
        },
        template: '#requerimento-selecionados',
        enviaLoja: function () {
            if (this.collection.length) {
                var servicoselecionados = this.collection.toJSON();

                var elem = $('#formRequerimentoDeclaracao');
                $.each(servicoselecionados, function (key, value) {
                    elem.append('<input type="hidden" name="produtos[' + key + '][id_produto]" value="' + value.id_produto + '"/>');
                    elem.append('<input type="hidden" name="produtos[' + key + '][nu_quantidade]" value="1" />');
                    elem.append('<input type="hidden" name="produtos[' + key + '][id_matricula]" value="'+$('#id_matricula').val()+'" />');
                });

                elem.submit();
                setTimeout(function () {
                    window.location.href = '/portal/secretaria/requerimento';
                }, 5000); // in milliseconds
            }
        },
        events: {
            'click #finalizarRequerimento': 'enviaLoja'
        }
    })
    ;


/************************ LayoutView *****************/
var RequerimentoLayoutView = Marionette.LayoutView.extend({
    template: '#requerimento-layout',
    regions: {
        listaServicos: '#requerimento-wrapper',
        listaServicosSelecionados: '#requerimento-wrapper-selecionados'
    },
    initialize: function () {
    }
});

$(document).ready(function () {
    if ($('#container-secretaria').length) {

        /*
         * Funcionalidade que vetifica se existe algum checkbox selecionado e habilita/desabilita o botão
         * */
        $(function () {
            $(".idProdutoCheckBox").change(function () {
                var countSelected = $(".idProdutoCheckBox:checked").length;
                var $btnDeletar = $('#finalizarCompra');
                $btnDeletar.prop("disabled", countSelected == 0);
            });
        });

        var layout = new RequerimentoLayoutView({
            el: '#container-secretaria'
        });

        layout.render();

        var collection = new ServicosCollection();
        collection.fetch({
            success: function () {
                layout.listaServicos.show(new SecretariaComposite({
                    collection: collection
                }));
                viewSecretariaSelecionadas = new SecretariaSelecionadosComposite();
                layout.listaServicosSelecionados.show(viewSecretariaSelecionadas);
            }
        });

    }
});




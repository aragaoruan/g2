
var DadosBasicosAlunoView = Backbone.View.extend({
    el: $('#container-principal'),
    model: DadosBasicosAlunoModel,
    events: {
    },
    initialize: function() {
        this.render();
    },
    render: function() {

        var variables = this.model.toJSON();
        var temp;
        temp = _.template($('#template-dados-basicos-aluno').html(), variables);
//        $('#container-alocar').show();
        this.$el.html(temp);
        return this;
    }
});

$(document).ready(function() {
    if ($('.container-principal').length > 0) {
    var app = new DadosBasicosAlunoModel();
    
    app.fetch({
                success: function(model, response, options) {
                    var	cardView = new DadosBasicosAlunoView({
//                            model: textoModel,
                            childViewTagName: 'div'
//                            el: $('#container-form-projeto-tab-texto')
                        }
                    );
                    cardView.render();
                }
            });
    }
});

var carregarLinha = Backgrid.Cell.extend({
    events: {
        "click #visualizar": "tramiteVisualizar"
    },
    render: function () {
        this.$el.html("<button id='visualizar' class='button small button-main'>Visualizar</button>");
        return this;

    },
    /**
     * Adicona traminte quando clicar em Visualizar
     */
    tramiteVisualizar: function () {
        var idUsuarioOriginal = $('#idUsuarioOriginal').val();
        var ocorrencias = [{
            id_ocorrencia: this.model.get('id_ocorrencia')
        }];

        $.ajax({
            url: '/portal/ocorrencia/aguardando-resposta-tramite',
            dataType: 'json',
            async: false,
            data: {
                ocorrencias: ocorrencias,
                nomeBotao: ' visualizou a',
                idUsuarioOriginal: idUsuarioOriginal
            },
            success: function (data) {
                localStorage.setItem('visualizarOcorrencia', true);
                window.location.href = "/portal/c-atencao/ocorrencia/id_ocorrencia/" + this.model.get('id_ocorrencia');
            }.bind(this)
        });
    }
});

var columns = [
    {
        name: "id_ocorrencia",
        label: "Número",
        editable: false,
        cell: "string"
    },
    {
        name: "st_titulo",
        label: "Título",
        cell: "string",
        editable: false
    },
    {
        name: "st_assuntoco",
        label: "Assunto",
        cell: "string",
        editable: false
    },
    {
        label: "Ação",
        cell: carregarLinha,
        editable: false
    }
];

var ModalOcorrencia = function () {
    var ocorrencias = [];
    var idUsuarioOriginal = $('#idUsuarioOriginal').val();
    /**
     * função que confirma que o aluno esta ciente das ocorrencias em aberto.
     */
    this.confirmarCiencia = function () {
        $.ajax({
            url: '/portal/ocorrencia/aguardando-resposta-tramite',
            dataType: 'json',
            async: false,
            data: {
                ocorrencias: ocorrencias,
                nomeBotao: 'tomou ciência da',
                idUsuarioOriginal: idUsuarioOriginal
            },
            success: function (data) {
                localStorage.setItem('confirmarCiencia', true);
            }
        });
    };

    /**
     * abre o modal na tela principal caso tenha ocorrencia com evolução
     * Aguardando Interesado
     */
    this.modalOcorrenciaPrincipal = function () {

        if (!localStorage.getItem('visualizarOcorrencia')) {
            if (!localStorage.getItem('confirmarCiencia')) {
                $.ajax({
                    url: '/portal/ocorrencia/ocorrencia-aguardando-interessado',
                    dataType: 'json',
                    async: false,
                    data: {id_matricula: $('#matriculaAluno').val()},
                    success: function (data) {
                        if (data !== null && data.length) {
                            ocorrencias = data;
                            var grid = new Backgrid.Grid({
                                className: 'backgrid table uny table-bordered',
                                columns: columns,
                                collection: new Backbone.Collection(data)
                            });

                            $('#regiao-grid-ocorrencia').append(grid.render().el);
                            $("#modalOcorrenciaPrincipal").modal();
                        }
                    }
                });
            }

        }
    };

};

$(document).ready(function () {
    var modalOcorrencia = new ModalOcorrencia();

    $(function () {
        modalOcorrencia.modalOcorrenciaPrincipal();
    });

    $('#confirmar-ciencia').click(function () {
        modalOcorrencia.confirmarCiencia();
    });
});

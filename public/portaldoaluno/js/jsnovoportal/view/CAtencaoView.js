var OcorrenciaView = Backbone.View.extend({
    el: $('.ocorrencia'),
//    model: OcorrenciaModel,
    events: {
        'click .btnEncerrar': 'encerrarOcorrencia'
    },
    initialize: function () {
        if ($('.ocorrencia').length) {
            var carregarLinha = Backgrid.Cell.extend({
                render: function () {
                    this.$el.html(
                        "<a " +
                        "class='button small button-main' " +
                        "href='/portal/c-atencao/ocorrencia/id_ocorrencia/"
                        + this.model.get('id_ocorrencia') + "'>Visualizar</a>");
                    return this;
                }
            })
            var columns = [
                {
                    name: "st_cadastro",
                    label: "Data de criação",
                    editable: false,
                    cell: "html"

                },
                {
                    name: "id_ocorrencia_parse",
                    label: "Ocorrência",
                    editable: false,
                    cell: "html"

                },
                {
                    name: "st_titulo",
                    label: "Título",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_evolucao",
                    label: "Evolução",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_situacao",
                    label: "Situação",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_categoriaocorrencia",
                    label: "Tipo",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_assuntocopai",
                    label: "Assunto",
                    cell: "string",
                    editable: false
                },
                {
                    name: "st_ultimotramite",
                    label: "Último trâmite",
                    cell: "string",
                    editable: false
                },
                {
                    label: "Ação",
                    cell: carregarLinha,
                    editable: false
                }
            ]

            // Initialize a new Grid instance
            var grid = new Backgrid.Grid({
                className: 'backgrid table uny table-bordered',
                columns: columns,
                collection: ocorrencias
            });

            // Render the grid and attach the root to your HTML document
            $('.content_ocorrencia').append(grid.render().el);

            ocorrencias.fetch({reset: true});
        }
    },
    encerrarOcorrencia: function (ev) {
        $.ajax({
            url: '/portal/c-atencao/alterar-ocorrencia',
            type: 'GET',
            data: {
                id_ocorrencia: $('#id_ocorrencia').val(),
                id_evolucao: EVOLUCAO.TB_OCORRENCIA.ENCERRADO_PELO_INTERESSADO,
                id_situacao: SITUACAO.TB_OCORRENCIA.ENCERRADA
            },
            dataType: 'html',
            success: function (data) {
                $('.info-box').hide();
                $('.uny-box').html(data);
                $('html, body').animate({
                    scrollTop: $("#mensagem-uny").offset().top
                }).delay('3000').queue(function () {
                    location.reload();
                });
            },
            beforeSend: function () {
                $('.cortina').show();
            },
            complete: function () {
                $('.cortina').hide();
            }
        });
    }
});

(function () {
    var HtmlCell = Backgrid.HtmlCell = Backgrid.Cell.extend({

        /** @property */
        className: 'html-cell',

        initialize: function () {
            Backgrid.Cell.prototype.initialize.apply(this, arguments);
        },

        render: function () {
            this.$el.empty();
            var rawValue = this.model.get(this.column.get('name'));
            var formattedValue = this.formatter.fromRaw(rawValue, this.model);
            this.$el.append(formattedValue);
            this.delegateEvents();
            return this;
        }
    })
}).call(this);

$(document).ready(function () {
    var app = new OcorrenciaView();
});
/**
 * Created by paulo.silva on 22/07/14.
 */

function addHandler(obj, evnt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evnt.replace(/^on/, ''), handler, false);
    } else {
        if (obj[evnt]) {
            var origHandler = obj[evnt];
            obj[evnt] = function (evt) {
                origHandler(evt);
                handler(evt);
            };
        } else {
            obj[evnt] = function (evt) {
                handler(evt);
            };
        }
    }
}

addHandler(window, 'onerror', function (error, url, num) {
    console.log('ERRO: ' + error.message);
    console.log('------ ' + error.filename + '(' + error.lineno + ')');
    return true;
});


function setLocalVar(c_name, value) {
    if (typeof(Storage) !== "undefined") {
        localStorage['portal' + c_name] = value;
    }
}

function getLocalVar(c_name) {
    if (typeof(Storage) !== "undefined") return localStorage['portal' + c_name];
    return null;
}

function unsetLocalVar(c_name) {
    setLocalVar(c_name, "");
}

$(document).ready(function () {

    unsetLocalVar('id_entidade');
    unsetLocalVar('id_perfil');

    $('#frmSessao').hide();
    $('#frmSessao').hide();

    $('#btnCancelarKick').click(function () {
        $('#frmLogin').show();
        $('#frmSessao').hide();
        $('#lb-senha').hide();
        $.ajax({
            url: "/portal/login/cancelarlogin",
            type: 'POST',
            dataType: 'json',
            data: {},
            beforeSend: function () {

            },
            success: function (data) {
                window.location.href = "";
            },
            complete: function (data) {

            },
            error: function () {

            }
        });
    });

    $('#btnKick').click(function () {
        $.ajax({
            url: "/portal/login/validar",
            type: 'POST',
            dataType: 'json',
            data: {
                login: $("#nomusuario").val(),
                senha: $("#dessenha").val(),
                hash: true
            },
            beforeSend: function () {

            },
            success: function (data) {
                window.location.href = data['redirect'];
            },
            complete: function (data) {

            },
            error: function () {

            }
        });
    });

    $('#frmLogin').submit(function () {
        $.ajax({
            url: "/portal/login/validar",
            type: 'POST',
            dataType: 'json',
            data: {
                login: $("#nomusuario").val(),
                senha: $("#dessenha").val(),
                hash: false
            },
            beforeSend: function () {

            },
            success: function (data) {

                if (data['senha'] === false) {
                    $('#lb-senha').html('Usuário e/ou senha incorretos.').show();
                }
                if (data['flag'] === true) {
                    $('#frmLogin').hide();
                    $('#frmSessao').show();
                }
                if (data['flag'] === false) {
                    window.location.href = data['redirect'];
                }

            },
            complete: function () {
            },
            error: function (data) {

            }

        });
        return false;
    });

    $('#recuperar-senha').bind('click', function (e) {
        e.preventDefault();

        $('#frmLogin').hide();
        $('#frmPasswd').show();

        return false;
    })


    $('#cancelar-recuperacao').bind('click', function (e) {
        e.preventDefault();

        $('#frmPasswd').hide();
        $('#frmLogin').show();

        return false;
    })

    $('#btnRecuperar').click(function (e) {

        e.preventDefault();

        $('#lb-error').hide();
        $('#lb-success').hide();
        $('#login').css({"borderColor": ""});
        $('#cpf').css({"borderColor": ""});
        $('#email').css({"borderColor": ""});

        var selectedCount = 0;

        if (!$('#email').val()) {
            $('#email').css({"borderColor": "red"});
        } else {
            selectedCount++;
        }

        if ($('#login').val()) {
            selectedCount++;
        }

        if ($('#cpf').val()) {
            selectedCount++;
        }
        if (selectedCount < 2) {
            $('#lb-error').html("É Necessário Informar o Email e outro Campo.");
            $('#lb-error').show();
            return;
        }
        $.ajax({
            url: $('#urlPost').val(),
            data: $('#frmPasswd').serialize() + "&json=true&ajax=true",
            dataType: 'json',
            method: "POST",
            beforeSend: function () {
                $("#btnRecuperar").val('Enviando...').attr('disabled', 'disabled');
            }
        })
            .done(function (data) {
                if (data.tipo == 1) {
                    $('#lb-success').html(data.mensagem[0]);
                    $('#lb-success').show();
                } else {
                    $('#lb-error').html(data.mensagem[0]);
                    $('#lb-error').show();
                }
            })
            .fail(function (error) {
                $('#lb-error').html("Erro ao Validar Dados. " + error.responseText);
                $('#lb-error').show();
            })
            .always(function () {
                $("#btnRecuperar").val('Recuperar Senha').removeAttr('disabled');
            });
        return false;
    });

});
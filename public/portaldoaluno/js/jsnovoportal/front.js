function addHandler(obj, evnt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evnt.replace(/^on/, ''), handler, false);
    } else {
        if (obj[evnt]) {
            var origHandler = obj[evnt];
            obj[evnt] = function (evt) {
                origHandler(evt);
                handler(evt);
            };
        } else {
            obj[evnt] = function (evt) {
                handler(evt);
            };
        }
    }
}

addHandler(window, 'onerror', function (error, url, num) {
    console.log('ERRO: ' + error.message);
    console.log('------ ' + error.filename + '(' + error.lineno + ')');
    return true;
});


function setLocalVar(c_name, value) {
    if (typeof(Storage) !== "undefined") {
        localStorage['portal' + c_name] = value;
    }
}

function getLocalVar(c_name) {
    if (typeof(Storage) !== "undefined") return localStorage['portal' + c_name];
    return null;
}

function unsetLocalVar(c_name) {
    setLocalVar(c_name, "");
}

$(document).ready(function () {
    var body = $('body');

    if ($(".photo").length > 0) {
        $(".photo").click(function (e) {
            $('.perfil').stop();
            $('.perfil').fadeToggle();
            $('.arrow-down-perfil i').toggleClass('icon-chevron-sign-down').toggleClass('icon-chevron-sign-up');
            e.stopPropagation();
        });

        $("html").click(function (e) {
            if ($('.perfil').is(':visible')) {
                $('.perfil').stop();
                $('.perfil').fadeOut(0);
                $('.arrow-down-perfil i').toggleClass('icon-chevron-sign-down').toggleClass('icon-chevron-sign-up');
            }

        });
    }

    if ($('.tooltip').length > 0) {
        $('.tooltip').tooltip({
            placement: "bottom"
        });
    }

    if ($('.mensagens-nao-lidas').length > 0) {
        $('.mensagens-nao-lidas .icon-remove').click(function () {
            $(this).parent().hide('fast', function () {
                if ($('.mensagens-nao-lidas').children('.itens:visible').length == 0) {
                    $('.mensagens-nao-lidas').hide()
                }
            })
        })
    }

    if ($('.side-navigation.uny.aluno').length > 0) {
        $('.side-navigation li.menu-item').hover(function (e) {
            $('.side-navigation li.menu-item').stop();
            $('.mensagens-nao-lidas').hide();
//            var heightMenu = $('.side-navigation.uny.aluno').height();
            $('.side-navigation.uny li.menu-item').removeClass('ativo');
            $(this).addClass('ativo');
            $('.info-salas, .info-salas-geral').hide();
            var rel = $('a', this).attr('data-rel');
            if ($(".info-salas[data-rel='" + rel + "']")) {
                $(".info-salas[data-rel='" + rel + "']").addClass('visivel').stop(true, true).fadeIn();
            }
            e.stopPropagation();
        }, function (e) {
            $('.side-navigation li.menu-item').stop();
            if (!$(e.relatedTarget).hasClass('menu-item')) {
                $('.info-salas').hide();
                $('.info-salas-geral').show();
                $('.mensagens-nao-lidas').show();
            }
            e.stopPropagation();
        })

        $('.acessosaladeaula').click(function () {
            if ($(this).hasClass('nao-iniciada')) {
                $('.uny-box').empty().html('<div class="info-box uny-mine yellow"><i class="icon-warning-sign"></i><div class="msg">Esta sala ainda não iniciou. Por favor, selecione outra.</div><a href="javascript:void()" class="info-box-remove"><i class="icon-remove"></i></a></div>').show();
            } else if ($(this).hasClass('fechada-final')) {
                $('.uny-box').empty().html('<div class="info-box uny-mine red"><i class="icon-warning-sign"></i><div class="msg">Esta sala não pode ser acessada. Por favor, selecione outra.</div><a href="javascript:void()" class="info-box-remove"><i class="icon-remove"></i></a></div>').show();
            } else {
                var rel = $(this).attr('data-rel');
                $(".info-salas[data-rel='" + rel + "'] form").submit();
            }
        })
    }

    if ($('.perfil-img-wrapper img').length) {
        $('.perfil-img-wrapper img').click(function () {
            window.open('/portal/usuario/upload-foto', "_blank", 'location=yes,height=800,width=810,scrollbars=yes,status=yes')
        })
    }

    if ($('.image-preview-wrapper').length) {
        jcrop_api = null;

        $('#imagem-aluno-file').change(function (e) {
            lerImagem(this);
        });

        $('#form-imagem-upload').submit(function (e) {
            checkCoords();
        });
    }

    if ($("#main_frame").length) {
        var iframe = $("#main_frame");
        iframe.height(iframe.height() + (2 * iframe.offset().top) + 60);
    }

    if ($('#buscaDeProjetos').length) {
        //Foco direcionado para o botao de busca
        $('#buscaDeProjetos').focus();
    }

    /*
     * Ações para a gestão da sala de aula
     * */
    if ($('.gestao-principal').length > 0) {
        $('#bt_alunos').click(function () {
            $('.acoes-wrapper').hide();
            $('#tbl_alunos').toggle();
        });
    }


    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    //Ações para a página de ocorrência
    if ($('.ocorrencia').length > 0) {


        //Verifica se existe ocorrência com mesmo assunto e subassunto
        var verificaAssuntoSubAssunto = function () {
            var cb_id_assuntoco = $('#cb_id_assuntoco');
            var cb_id_assuntopai = $('#cb_id_assuntopai');

            if (cb_id_assuntoco.val() !== '' && cb_id_assuntopai.val() !== '') {
                var data = {'cb_id_assuntoco': cb_id_assuntoco.val(), 'cb_id_assuntopai': cb_id_assuntopai.val()};

                $.ajax({
                    url: '/portal/C-atencao/lista-ocorrencias',
                    type: 'POST',
                    data: data,
                    success: function (dados) {
                        if (dados !== 'null') {
                            var data = JSON.parse(dados);
                            var assuntoSubassunto = false;
                            var html = '';

                            _.map(data, function (value) {

                                //se for diferente de encerrada
                                if (value.id_situacao !== 99) {
                                    assuntoSubassunto = true;

                                    html += '<tr>' +
                                        '<td>' + value.id_ocorrencia + '</td>' +
                                        '<td>' + value.st_titulo + '</td>' +
                                        '<td><a href="/portal/c-atencao/ocorrencia/id_ocorrencia/' + value.id_ocorrencia +
                                        '" class="button small button-main">VISUALIZAR</a></td>' +
                                        '</tr>';
                                }

                                if (assuntoSubassunto) {

                                    $('#btnsalvar').prop('disabled', 'true');
                                    $('#btnsalvar').css('cursor', 'not-allowed');
                                    $('#btnsalvar').attr('title', 'Já existe ocorrência em andamento.');

                                    $('#ocorrencias-similares-modal').modal();
                                    $('#ocorrencias-similares-modal table tbody').html(html);
                                }

                            })
                        } else {
                            //resetar botao salvar
                            $('#btnsalvar').prop('disabled', '');
                            $('#btnsalvar').css('cursor', 'pointer');
                            $('#btnsalvar').attr('title', '');
                        }
                        return true;
                    }
                });
            }
        };

        //Ao mudar o assunto do pai, alteraremos o assunto do filho
        $('#cb_id_assuntopai').change(function () {
            var select = $('option:selected', this);
            if (select.val() != 0) {
                var dados =
                    {
                        id_assuntocopai: select.val(),
                        ajax: true,
                        json: true
                    };

                $.ajax({
                    url: "/portal/C-atencao/retorna-combo-assuntoco",
                    type: 'POST',
                    data: dados,
                    success: function (dados) {
                        $('#cb_id_assuntoco').empty().prepend('<option value="">Selecione um subassunto...</option>' + dados);
                    }
                })
            }
        });

        //Carregar o texto pré-formatado da ocorrência
        $('#cb_id_assuntoco').change(function () {
            verificaAssuntoSubAssunto();

            if ($("option:selected", this).val() != 0) {
                var dados =
                    {
                        id_assuntoco: $("option:selected", this).val(),
                        ajax: true,
                        json: true
                    };
                $.ajax({
                    url: "/portal/C-atencao/retorna-descricao-assuntoco",
                    type: 'POST',
                    data: dados,
                    success: function (dados) {
                        if (dados.length)
                            $('#st_ocorrencia').empty().prepend(dados);
                    }
                })
            }
        })


        $("#frmOcorrencia").validate({
            messages: {
                st_titulo: {
                    required: 'Preencha o título da ocorrência',
                    minlength: 'Por favor, digite no mínimo 6 caracteres.'
                },
                cb_id_categoriaocorrencia: {
                    required: 'Selecione um tipo de ocorrência'
                },
                cb_id_assuntopai: {
                    required: 'Selecione um assunto'
                },
                cb_id_saladeaula: {
                    required: 'Selecione uma sala de aula'
                },
                st_ocorrencia: {
                    required: 'A mensagem não pode ser vazia'
                }
            },
            submitHandler: function (form) {
                $(form).find('[type="submit"]')
                    .attr({
                        disabled: 'disabled',
                        value: 'Gerando Ocorrência...'
                    })
                    .css({
                        cursor: 'wait',
                        opacity: 0.6
                    });
                form.submit();
            }
        });

        $("#frmJustificaReabrir").validate({
            messages: {
                st_tramite: {
                    required: 'A mensagem não pode ser vazia',
                    minlength: 'Por favor, digite no mínimo 15 caracteres.'
                }
            }
        });
    }

    if ($('.login .badge').is(':empty'))
        $('.login .badge').hide();

    if ($("#frmTramite").length > 0) {
        $("#frmTramite").validate({
            messages: {
                st_tramite: {required: 'Preencha a descrição da interação'}
            }
        });
    }

    //Ação para montar o menu principal horizontal
    if ($('.navigation.uny').length > 0) {
        var pars = {
            ajax: true,
            json: true
        };

        //verifica se não existe este elemento na dom (este elemento só existe na tela de seleção de entidade e perfil)
        if (!$("#lista-entidades-perfil").length) {
            //se este elemento não existir ele vai buscar os menus do usuario
            var url = "/portal/menu/nav";

            var identidade = getLocalVar('id_entidade');
            var idperfil = getLocalVar('id_perfil');
            if (identidade && idperfil)
                url = url + "/e/" + identidade + "/p/" + idperfil;

            $.ajax({
                url: url,
                type: 'POST',
                data: pars,
                success: function (dados) {
                    $('.navigation.uny, #sticky-navigation.uny ul').empty().prepend(dados);
                }
            });
        } else {
            //se o elemento existir, significa que estamos na tela de seleção de perfil, então limpamos o menu
            $('.navigation.uny, #sticky-navigation.uny ul').empty();
        }
    }

    $('.mobile-menu-button').click(function () {
        $(this).toggleClass("uny");
    });

    if ($('.perfispedagogico').length > 0) {
        $('.modal, .perfispedagogico').on('click', '.perfilAluno', function () {
            var formSelecionaPerfil = $('form#selecionarPerfil');

            $('#id_perfil', formSelecionaPerfil).val($(this).data('idperfil'))
            $('#id_perfilpedagogico', formSelecionaPerfil).val($(this).data('idperfilpedagogico'))
            $('#id_projetopedagogico', formSelecionaPerfil).val($(this).data('idprojetopedagogico'))
            $('#id_matricula', formSelecionaPerfil).val($(this).data('idmatricula'))
            $('#id_entidade', formSelecionaPerfil).val($(this).data('identidade'))

            setLocalVar('id_perfil', $(this).data('idperfil'));
            setLocalVar('id_entidade', $(this).data('identidade'));

            formSelecionaPerfil.submit();

        });

        $('.modal-target').click(function () {
            var htmlCopiado = $(this).next().clone().removeClass('hide');
            $('.modal-body').empty().html(htmlCopiado);
        });

        //custom scroll
        $(".feature-content.uny").mCustomScrollbar({
            scrollButtons: {
                /*scroll buttons*/
                enable: true, /*scroll buttons support: boolean*/
                scrollType: "continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                scrollSpeed: "auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                scrollAmount: 40 /*scroll buttons pixels scroll amount: integer (pixels)*/
            },
            theme: "dark-thin"
        });

    }

    if ($('.selecao-salas').length > 0) {
        $('.selecao-salas .salas .sala-item').click(function () {
            if (!$('.status .icon-circle', this).hasClass('nao-iniciada')) {
                $('#id_disciplina').attr('value', $(this).data('id-disciplina'));
                $('#id_saladeaula').attr('value', $(this).data('id-saladeaula'));
                $('#id_categoriasala').attr('value', $(this).data('id-categoriasala'));
                $('form#form-mudanca-sala').submit();
            }
        })
    }

    if ($('.dropdown-toggle').length) {
        $('.dropdown-toggle').dropdown();
    }

    if ($("#frmLogin").length) {
        $("#frmLogin").validate({
            rules: {
                nomusuario: "required",
                dessenha: "required"
            },
            messages: {
                nomusuario: "Este campo é necessário",
                dessenha: "Este campo é necessário"
            }
        })
    }


    if ($("#frmTrocarSenha").length) {
        $("#frmTrocarSenha").validate({
            rules: {
                st_senha: "required",
                st_senha_nova: "required",
                st_senha_nova_confirma: {
                    equalTo: "#st_senha_nova"
                }
            },
            messages: {
                st_senha: "Este campo é necessário",
                st_senha_nova: "Este campo é necessário",
                st_senha_nova_confirma: {
                    equalTo: "As senhas não são as mesmas"
                }
            }
        });
    }


    var wrapper_pai = $('.wrapper-pai');
    if (wrapper_pai.length) {
        var wrapper_encerramento = $('.encerramento-wrapper');
        var encerramento_box = $('.encerramento-box');
        var btn_sincronizar = $('#bt_sincronizar');
        var btn_encerramento = $('.bt_encerramentosalva');
        var btn_tcc = $('.bt_encerramentosalva.tcc');
        var id_saladeaula = btn_encerramento.data('id-saladeaula');
        var btn_liberar = $('#bt_liberatcc');
        var wrapper_liberacao = $('.liberar-wrapper');

        btn_encerramento.click(function () {
            $('.acoes-wrapper').hide();
            wrapper_encerramento.toggle();

            var dados = {
                id_saladeaula: id_saladeaula
            }

            var options = {
                url: "/portal/sala-de-aula/encerrar",
                type: 'POST',
                data: dados,
                success: function (data) {
                    wrapper_encerramento.append(data);
                }
            }

            if ($(this).hasClass('tcc'))
                options.url = '/portal/sala-de-aula/encerrar-tcc';

            if (wrapper_encerramento.children().length == 0) {
                //Dispara o AJAX para a requisição de encerramento e encerramento tcc
                $.ajax(options)
            }
        });

        var dados_alunos = {
            id_liberar: 1,
            id_saladeaula: id_saladeaula
        };

        wrapper_pai.on('click', '.nav-encerramento a', function () {
            $(this).tab('show');
        });

        wrapper_encerramento.on('click', '#repasse-tab', function () {
            if (!$('table.liberacao.encerramento').children().length) {
                carregarAlunos(dados_alunos, 'encerramento');
            }
        });

        wrapper_encerramento.on('click', '#tramite-tab', function () {
            var dados_tramites = {
                id_saladeaula: $('#id_saladeaula').val(),
                id_liberar: 1,
                id_tipotramite: TIPO_TRAMITE.MATRICULA.TCC
            };

            if (!$('table.tramite.encerramento').children().length) {
                carregarTramites(dados_tramites, 'encerramento');
            }
        });

        wrapper_encerramento.on('click', '#cb_dt_liberado a', function () {
            dados_alunos.id_liberar = $(this).data('rel');
            $('#cb_dt_liberado a').removeClass('portfolio-selected');
            $(this).addClass('portfolio-selected');

            carregarAlunos(dados_alunos, 'encerramento');
        });

        wrapper_pai.on('click', '#btnsalvarencerramento', function (e) {
            $('#frmEncerramento').validate({
                rules: {
                    st_justificativaacima: {required: true, minlength: 15},
                    st_justificativaabaixo: {required: true, minlength: 15},
                    st_pontosnegativos: {required: true, minlength: 15},
                    st_pontospositivos: {required: true, minlength: 15},
                    st_pontosmelhorar: {required: true, minlength: 15},
                    st_pontosresgate: {required: true, minlength: 15},
                    st_conclusoesfinais: {required: true, minlength: 15}
                },
                submitHandler: function (form) {
                    var options = {
                        url: "/portal/sala-de-aula/salvarencerramento",
                        type: 'get',
                        data: $(form).serialize(),
                        success: function (data) {
                            $('.cortina-loader.pai').hide();
                            var json = JSON.parse(data);

                            if (json.type == 'success') {
                                $('.encerramento-box').addClass('green');
                            } else {
                                $('.encerramento-box').addClass('yellow');
                            }

                            encerramento_box.show();
                            $('.encerramento-box .msg').html(json.text);
//                            $('html, body').scrollTo('#encerramento-wrapper');
                        },
                        beforeSend: function () {
                            $('.cortina-loader.pai').show();
                        }
                    };

                    $.ajax(options);

                }
            })
        });

        wrapper_pai.on('click', '.repassar_professor', function () {

            var button = $(this);

            button.hide();

            var id_sala = $(this).data('id-sala');
            var id_tiposaladeaula = $(this).data('id-tipo-sala');
            var id_tipodisciplina = $(this).data('id-tipo-disc');

            if (id_tiposaladeaula == 1 || id_tipodisciplina == 2) {
                var dados = $('#frmEncerramentoRepassar').serialize();
            } else {
                var dados = {
                    id_liberar: $('#id_liberar').val(),
                    id_saladeaula: id_saladeaula,
                    id_tiposaladeaula: id_tiposaladeaula,
                    id_tipodisciplina: id_tipodisciplina
                }
            }

            $.ajax({
                url: '/portal/sala-de-aula/repassarprofessor',
                dataType: 'json',
                data: dados,
                success: function (data) {
                    if (data.tipo != 1) {
                        $('.encerramento-box').removeClass('green').addClass('yellow');
                    } else if (data.tipo == 1) {
                        carregarAlunos(dados_alunos, 'encerramento');
                        $('.encerramento-box').removeClass('yellow').addClass('green');
                    }
                    $('.encerramento-box .msg').html(data.text);
                    button.show();
                },
                complete: function () {
                    $('.cortina-loader.pai').hide();
                    button.show();
                    encerramento_box.show();
                },
                beforeSend: function () {
                    $('.cortina-loader.pai').show();
                }
            });
        });

        wrapper_pai.on('click', '#marcartodos', function () {
            if ($(this).is(':checked')) {
                $(".liberacao input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $(".liberacao input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });

        $(document).on("keypress", ".nu_notaparametro", function (e) {
            var tecla = (window.event) ? event.keyCode : e.which;
            if ((tecla > 47 && tecla < 58)) {
                return true;
            } else {
                if (tecla === 8 || tecla === 0) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        if (btn_sincronizar.length) {
            var wrapper_sinc = $('.sincronizar-wrapper');
            btn_sincronizar.click(function () {
                $('.acoes-wrapper').hide();
                var dados =
                    {
                        id_saladeaula: $(this).data('id-sala'),
                        st_codsistemacurso: $(this).data('id-codsistema')
                    };
                if (wrapper_sinc.children().length == 0) {
                    $.ajax({
                        url: "/portal/sala-de-aula/abrirsincronizar",
                        data: dados,
                        success: function (data) {
                            $('.sincronizar-wrapper').html(data);
                        },
                        complete: function () {
                            $('.cortina-loader').hide();
                        },
                        beforeSend: function () {
                            $('.cortina-loader').show();
                        }
                    })
                }
                wrapper_sinc.toggle();
            });

            wrapper_sinc.on('click', '#btnSincronizar', function () {
                var dados = $('#frmSincronizar').serialize();
                $.ajax({
                    url: '/portal/sala-de-aula/sincronizar',
                    dataType: 'json',
                    type: 'post',
                    data: dados,
                    success: function (data) {
                        if (data.tipo != 1) {
                            $('.encerramento-box').removeClass('green').addClass('yellow');
                        } else if (data.tipo == 1) {
                            $('.encerramento-box').removeClass('yellow').addClass('green');
                            //TODO: Corrigir segundo request e atualização da lista de alunos a sincronizar.
                            $.ajax({
                                url: "/portal/sala-de-aula/abrirsincronizar",
                                data: dados,
                                success: function (data) {
                                    $('.sincronizar-wrapper').html(data);
                                },
                                complete: function () {
                                    $('.cortina-loader').hide();
                                },
                                beforeSend: function () {
                                    $('.cortina-loader').show();
                                }
                            })
                        }
                        $('.encerramento-box').show();
                        $('.encerramento-box .msg').html(data.text);
                    },
                    complete: function () {
                        $('.cortina-loader').hide();
                    },
                    beforeSend: function () {
                        $('.cortina-loader').show();
                    }
                })
            });

            wrapper_sinc.on('click', '#marcartodos', function () {
                var checked = $(this).prop('checked');
                $('.sincronizacao input[type=checkbox]').not(':disabled').each(function () {
                    $(this).prop('checked', checked);
                });
            });
        }

        $(document).on('click', '.table-toggle', function (e) {
            //e.preventDefault();
            var table = $(this).closest('.table-responsive').find('table');
            table.toggle();
        });

        if (btn_tcc.length) {
            wrapper_encerramento.on('click', '.lancarnota', function (e) {
                e.preventDefault();
                var elem = $(this);

                $('#myModal .modal-title').empty();
                $('#myModal .modal-body').empty();
                $('#myModal').modal('show');
                var options = {
                    url: "/portal/sala-de-aula/lancar-nota-tcc",
                    type: 'get',
                    data: {
                        id_saladeaula: elem.data('id-saladeaula'),
                        id_matricula: elem.data('id-matricula')
                    },
                    success: function (data) {
                        $('#myModal .modal-title').html('Lançar Nota do TCC');
                        $('#myModal .modal-body').html(data);

                        if (elem.data('view-only')) {
                            $('#myModal .modal-title').html('Informações');
                            $('#myModal').find('#btFormCancelar').remove();
                            $('#myModal .modal-body')
                                .find('.div_linha')
                                .next('div').html('<button class="button small button-main pull-right" type="button" data-dismiss="modal">Voltar</button>')
                                .end().remove();
                        } else {
                            $('#myModal .modal-title').html('Informações');
                            $('#myModal .modal-body').find('.div_linha').hide().end();
                            $('#myModal .modal-body').find('.div_linha').next().find('.bt_salvartcc').hide().end();
                        }
                        $('.cortina-loader').hide();
                    },
                    beforeSend: function () {
                        $('.cortina-loader').show();
                    }
                };

                $.ajax(options);
            });

            body.on('change', '#id_situacaotcc', function () {
                if ($('#id_situacaotcc').val() == SITUACAO.TB_ALOCACAO.DEVOLVIDO_ALUNO) {
                    $('#div_motivodevolucao').show();
                    $('#div_btn_atualizar').show();
                    $('#myModal .modal-title').html('Informações');
                    $('#myModal .modal-body').find('.div_linha').hide().end();
                    $('#myModal .modal-body').find('.div_linha').next().find('.bt_salvartcc').hide().end();

                } else if ($('#id_situacaotcc').val() == SITUACAO.TB_ALOCACAO.APROVADO) {
                    $('#div_motivodevolucao').hide();
                    $('#div_btn_atualizar').hide();
                    bootbox.confirm({
                        message: 'Deseja avaliar o trabalho do aluno?',
                        size: 'small',
                        buttons: {
                            confirm: {
                                label: 'OK'
                            },
                            cancel: {
                                label: 'Cancelar'
                            }
                        },
                        callback: function (response) {
                            if (response) {
                                $('#myModal .modal-title').html('Informações');
                                $('#myModal .modal-body').find('.div_linha').show().end();
                                $('#myModal .modal-body').find('.div_linha').next().find('.bt_salvartcc').show().end();
                            } else {
                                $('#myModal .modal-title').html('Informações');
                                $('#myModal .modal-body').find('.div_linha').hide().end();
                                $('#myModal .modal-body').find('.div_linha').next().find('.bt_salvartcc').hide().end();
                                $('#id_situacaotcc').val('');
                            }
                        }
                    });

                } else {
                    $('#div_motivodevolucao').hide();
                    $('#div_btn_atualizar').hide();
                    $('#myModal .modal-title').html('Informações');
                    $('#myModal .modal-body').find('.div_linha').hide().end();
                    $('#myModal .modal-body').find('.div_linha').next().find('.bt_salvartcc').hide().end();
                }
            });

            body.on('click', '#btn_atualizar_situacao', function () {
                $('#btn_atualizar_situacao').attr('disabled', 'disabled');
                bootbox.confirm({
                    message: 'Deseja devolver o trabalho do aluno para ajustes e reenvio?',
                    size: 'small',
                    buttons: {
                        confirm: {
                            label: 'Ok',
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: 'Cancelar'
                        }
                    },
                    callback: function (response) {
                        if (response) {
                            $.ajax({
                                url: "/portal/sala-de-aula/devolver-tcc",
                                type: 'post',
                                data: {
                                    'st_motivodevolucao': $('#st_motivodevolucao').val(),
                                    'id_situacaotcc': $('#id_situacaotcc').val(),
                                    'id_alocacao': $('#id_alocacao_devolucao').val(),
                                    'id_matricula': $('#id_matricula').val(),
                                    'id_saladeaula': $('#id_saladeaula').val(),
                                    'id_avaliacaoaluno': $('#id_avaliacaoaluno').val()
                                },
                                success: function (data) {
                                    if (data.tipo == 1) {
                                        $('.encerramento-box.tcc').removeClass('yellow').addClass('green');
                                        carregarAlunos(dados_alunos, 'encerramento');
                                        setTimeout(function () {
                                            $('#myModal').modal('hide');
                                        }, 6000);
                                    } else {
                                        $('.encerramento-box.tcc').removeClass('green').addClass('yellow');
                                        $('#btn_atualizar_situacao').removeAttr('disabled');
                                    }
                                    $('.encerramento-box.tcc').show().children('.msg').html(data.text);
                                },
                                beforeSend: function () {
                                    $('.cortina.tcc').show();
                                },
                                complete: function () {
                                    $('.cortina.tcc').hide();
                                }
                            });
                        }
                    }
                });
            });

            var box_tcc = $('.encerramento-box.tcc');
            body.on('click', '.bt_salvartcc', function () {
                $('#bt_salvartcc').attr('disabled', 'disabled');
                $('.cortina.tcc').show();
                if ($('#nu_notaparametro'))
                    var options = {
                        url: "/portal/sala-de-aula/salvar-nota-tcc",
                        type: 'post',
                        data: $("#formTccNotaUpload").serialize(),
                        success: function (data) {
                            if (data.tipo == 1) {
                                $('.encerramento-box.tcc').removeClass('yellow').addClass('green');
                                $('.encerramento-box.tcc').focus();
                                carregarAlunos(dados_alunos, 'encerramento');
                                setTimeout(function () {
                                    $('#myModal').modal('hide');
                                }, 6000);
                            } else {
                                $('.encerramento-box.tcc').focus();
                                $('.encerramento-box.tcc').removeClass('green').addClass('yellow');
                                $('.nu_notaparametro').focus();
                                setTimeout(function () {
                                    $('.encerramento-box.tcc').hide();
                                }, 6000);
                                $('#bt_salvartcc').removeAttr('disabled');
                            }
                            $('.encerramento-box.tcc').show().children('.msg').html(data.text);
                        },
                        beforeSend: function () {
                            $('.cortina.tcc').show();
                        },
                        complete: function () {
                            $('.cortina.tcc').hide();
                        }
                    };
                $.ajax(options);
            });

            body.on('change', '.nu_notaparametro', function () {
                var total_nota = $(this).val();
                if (0 <= parseInt(total_nota) && parseInt(total_nota) <= 100) {
                    $('#total_nota').html(total_nota);
                }
            });

            body.on('click', '#btFormCancelar', function () {
                var botao = $(this);
                var form_canc = $('#frmCancelamentoTcc');
                if (!form_canc.length) {
                    $.get('/portal/sala-de-aula/cancelarliberacaotcc',
                        {
                            id_matricula: $('#id_matricula').val(),
                            id_saladeaula: $('#id_saladeaula').val(),
                            id_avaliacaoaluno: $('#id_avaliacaoaluno').val()
                        },
                        function (response) {
                            $($.parseHTML(response)).insertAfter(botao);
                        });
                } else {
                    form_canc.toggle();
                }
            });

            body.on('click', '#btCancelarTcc', function () {
                $('#frmCancelamentoTcc').validate({
                    rules: {
                        st_justificativa: {required: true}
                    },
                    submitHandler: function (form) {
                        var form = $(form).serialize();
                        var options = {
                            url: "/portal/sala-de-aula/cancelarliberacaotcc",
                            type: 'post',
                            dataType: 'json',
                            data: form,
                            success: function (data) {
                                if (data.tipo == 1) {
                                    $('.encerramento-box.tcc').addClass('green');
                                    carregarAlunos(dados_alunos, 'encerramento');

                                    $.ajax({
                                        url: "/portal/sala-de-aula/lancar-nota-tcc",
                                        type: 'get',
                                        data: form,
                                        success: function (data) {
                                            $('#myModal .modal-body').html(data);
                                        },
                                        beforeSend: function () {
                                            $('.cortina-loader.tcc').show();
                                        },
                                        complete: function () {
                                            $('.cortina-loader.tcc').hide();
                                        }
                                    });
                                } else {
                                    $('.encerramento-box.tcc').addClass('yellow');
                                }

                                $('.encerramento-box.tcc').show().children('.msg').html(data.text);
                            },
                            beforeSend: function () {
                                $('.cortina-loader.tcc').show();
                            },
                            complete: function () {
                                $('.cortina-loader.tcc').hide();
                            }
                        };
                        $.ajax(options);
                    }
                });
            });
        }

        if (btn_liberar.length) {
            var btn_salvarliberacao = $('.btn_salvarliberacao');
            $('#bt_liberatcc').click(function (event, param) {
                var options = {
                    url: "/portal/sala-de-aula/liberar-tcc",
                    data: {
                        id_saladeaula: $(this).data('id-sala')
                    },
                    type: 'get',
                    success: function (data) {
                        wrapper_liberacao.toggle();
                        $('.content-liberacao').html(data);
                    },
                    beforeSend: function () {
                        $('.cortina-loader').show();
                    },
                    complete: function () {
                        $('.cortina-loader').hide();
                    }
                };

                $('.acoes-wrapper').hide();

                if ($('.content-liberacao').children().length && param == undefined) {
                    wrapper_liberacao.toggle();
                } else {
                    $.ajax(options);
                }

            });


            wrapper_liberacao.on('click', '.btn_salvarliberacao', function () {
                $("#frmLiberar_tcc").validate({
                    submitHandler: function (form) {
                        var options = {
                            url: "/portal/sala-de-aula/salvarliberacaotcc",
                            dataType: 'html',
                            type: 'post',
                            data: $(form).serialize(),
                            success: function (data) {
                                var json = JSON.parse(data);
                                if (json.type == 'success') {
                                    $('.encerramento-box').addClass('green');
                                } else {
                                    $('.encerramento-box').addClass('yellow');
                                }

                                encerramento_box.show();
                                $('.encerramento-box .msg').html(json.text);

                                $('#bt_liberatcc').trigger('click', ['custom']);
                            },
                            beforeSend: function () {
                                $('.cortina-loader').show();
                            },
                            complete: function () {
                                $('.cortina-loader').hide();
                            }
                        };
                        $.ajax(options);
                    }
                });
            })
        }
    }

    if ($('#formEnviarTCC').length > 0) {
        body.on('paste', '#st_tituloavaliacao', function (e) {
            e.preventDefault();
        });

        //ACAO DE SALVAR O TCC
        var enviar_box_tcc = $('.enviar-tcc-box');
        body.on('submit', '#formEnviarTCC', function (e) {
            e.preventDefault();
            var options = {
                url: "/portal/avaliacao/upload-tcc",
                type: 'post',
                dataType: 'json',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.type == 'success') {
                        enviar_box_tcc.removeClass('yellow').addClass('green');

                        window.setTimeout(function () {
                            $('#modalTcc').modal('hide');
                        }, 3000);

                        $('.tcc-box').hide();

                    } else {
                        enviar_box_tcc.removeClass('green').addClass('yellow');
                    }
                    enviar_box_tcc.show().children('.msg').html(data.mensagem);
                },
                beforeSend: function () {
                    $('.cortina.tcc').show();
                },
                complete: function () {
                    $('.cortina.tcc').hide();
                }
            };
            $.ajax(options);
        });
    }

    body.on('click', '.info-box-remove', function () {
        $(this).parent().hide();
    });


    /*
     *
     * Ativação do tutorial
     * */
    if ($('.tutorial').length) {
        $.ajax({
            url: "/portal/index/retornar-tutorial",
            type: 'POST',
            success: function (response) {
                if (response == 'false') {
                    $('.cycle-slideshow').cycle('destroy');
                    $('.tutorial').hide();
                } else {
                    $('.tutorial').show();
                }
            }
        });

        $('.cycle-slideshow').on('cycle-finished', function (event, opts) {
            $('.close-wrapper').show();
            $.ajax({
                url: "/portal/index/processar-tutorial",
                type: 'POST',
                success: function (response) {
                },
                complete: function () {
                }
            });
        });

        $('.cycle-close').click(function () {
            $('.cycle-slideshow').cycle('destroy');
            $('.tutorial').hide();
        });
    }
});

function carregarAlunos(dados, classe) {
    $.ajax({
        url: "/portal/sala-de-aula/listaalunosencerramento",
        data: dados,
        success: function (data) {
            $('.cortina-loader.pai').hide();
            $('table.liberacao.' + classe).html(data);

        },
        beforeSend: function () {
            $('.cortina-loader.pai').show();
        }
    })
}

function carregarTramites(dados, classe) {
    $.ajax({
        url: "/portal/sala-de-aula/tramites-encerramento-tcc",
        data: dados,
        success: function (data) {
            $('.cortina-loader.pai').hide();
            $('table.tramite.' + classe).html(data);
        },
        beforeSend: function () {
            $('.cortina-loader.pai').show();
        }
    })
}

function lerImagem(input) {
    var file = $(input);
    var image_preview = $('#image-preview');
    boundx = null;
    boundy = null;

    // Grab some information about the preview pane
    preview = $('#image-preview-wrapper');
    pcnt = $('#image-preview-thumb-wrapper');
    pimg = $('#image-preview-thumb-wrapper img');
    var img_wrapper = $('.image-preview-wrapper');

    xsize = pcnt.width();
    ysize = pcnt.height();

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result;

            image.onload = function () {
                // access image size here
                image_preview.attr('style', '');
                if (this.width > '1024') {
                    if (jcrop_api != null) {
                        jcrop_api.destroy();
                    }

                    file.replaceWith(file.val('').clone(true));
                    $('#image-preview, #image-preview-thumb-wrapper img').show().attr('src', '');

                    alert('Sua foto é muito grande, por favor, selecione outra imagem');

                } else {
                    $('#image-preview, #image-preview-thumb-wrapper img').show().attr('src', image.src);
                    if (this.height > img_wrapper.height()) {
                        img_wrapper.height(image_preview.height());
                    } else {
                        img_wrapper.height('');
                    }

//                    $('.image-preview-wrapper-in').width(image_preview.width());

                    if (jcrop_api != null)
                        jcrop_api.destroy();

                    setTimeout(function () {
                        $('#image-preview').Jcrop({
                            onChange: updatePreview,
                            onSelect: updateCoords,
                            aspectRatio: 1,
                            addClass: 'centered'
                        }, function () {
                            // Use the API to get the real image size
                            var bounds = this.getBounds();
                            boundx = bounds[0];
                            boundy = bounds[1];
                            // Store the API in the jcrop_api variable

                            jcrop_api = this;

                            // Move the preview into the jcrop container for css positioning
                            preview.appendTo(jcrop_api.ui.holder);
                        });
                    }, 1000);
                }
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function updatePreview(c) {
    if (parseInt(c.w) > 0) {
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        pimg.css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
    }
}

function updateCoords(c) {
    $('#x').val(c.x);
    $('#x2').val(c.x2);
    $('#y').val(c.y);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
}

function checkCoords() {
    if (!parseInt($('#w').val())) {
        alert('Selecione uma imagem e depois selecione a área que deseja selecionar');
    } else {
        return true;
    }
}

function mensagem_retorno(tipo, mensagem) {
    var retorno = '';
    if (tipo == 1) {
        retorno = '<i class="icon-ok-sign"></i>';
    } else {
        retorno = '<i class="icon-warning-sign"></i>';
    }
    retorno += '<div class="msg">';
    retorno += mensagem + ' </div>';
    retorno += '<a href="javascript:void(0)" class="info-box-remove"><i class="icon-remove"></i></a>';
    //msg-retorno

    $('.msg-retorno').html(retorno);
    $('.msg-retorno').removeClass('hide');
}

if (typeof stickyMenu != 'function') {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 200) {
            $('#header').addClass('sticky-header');
            $('.sticky-navigation,#to-top-button').fadeIn();
        }
        else {
            $('#header').removeClass('sticky-header');
            $('.sticky-navigation,#to-top-button').fadeOut();
        }
    });
}


function abrirLinkMoodle(id_sala, id_disciplina, id_aluno) {

    var objeto = {id_disciplina: id_disciplina, id_saladeaula: id_sala, id_aluno: id_aluno};

    var $alunosBox = $('.alunos-box');
    var $cortinaLoader = $('.cortina-loader');

    $.ajax({
        dataType: 'json',
        type: 'post',
        url: '/default/vw-encerramento-salas/retornar-link-relatorio',
        data: {
            to: 'VwEncerramentoCoordenadorTO',
            obj: objeto
        },
        beforeSend: function () {
            $cortinaLoader.show();
        },
        success: function (dataResponse) {

            if (dataResponse.tipo == 0) {
                $alunosBox.addClass('yellow').show()
                    .find('.msg').html(dataResponse.mensagem);
                $(window).scrollTop($alunosBox.offset().top - 40);
            } else {
                var win = window.open(dataResponse.mensagem, '_blank');
                if (win) {
                    win.focus();
                } else {
                    $alunosBox.addClass('yellow').show()
                        .find('.msg').html('Desbloqueie o pop-up do navegador!');
                }
            }
        },
        complete: function () {
            $cortinaLoader.hide();
        }
    });
}

/**
 * Evnia o formulário da biblioteca virtual
 */
function abrirBibliotecaVirtual() {
    $("#form-pearson").submit();
    setInterval(function () {
        window.history.go(-1);
    }, 3000);
};

/* Na tela de confirmação de endereço no agendamento,
 * faz com que o combobox de cidades atualize as opções
 * de acordo com o estado selecionado no combobox do UF.
 */


/**
 * Metodos para tratar o loading no portal
 */
function loading() {
    $('.cortina-loader').show();
};

function loaded() {
    $('.cortina-loader').hide();
};


$(document).ready(function () {
    $('#combo-uf').on('change', function () {
        $.getJSON('/default/pessoa/get-municipio-by-uf/sg_uf/' + $('#combo-uf').val(), function (response) {
            var options = '<option value="">--</option>';

            $.each(response, function (index, obj) {
                options += '<option value="' + obj.id_municipio + '">' + obj.st_nomemunicipio + '</option>';
            });

            $('#combo-municipio').html(options);
        });
    });

    //verifica se tem o formulário de redirecionamento para a pearson
    if ($("#form-pearson").length) {
        abrirBibliotecaVirtual();
    }
});

/**
 * Tratamento das bandeiras dos cartões, pois os id's do banco e do plugin utilizado para definir a bandeira
 * não corresponde.
 *
 * @param id_card
 * @returns {*}
 */
function codigoTipoCartao(id_card) {
    var card = null;

    switch (id_card) {
        //ID 5 no plugin corresponde a bandeira ELO e no banco o ELO tem ID 4
        case 5:
            card = 4; // ID correspondente ao nosso banco de dados para a bandeira ELO
            break;
        //ID 4 no plugin corresponde a bandeira DINNERS e no banco de dados DINERS tem o ID 5
        case 4:
            card = 5; // ID correspondente ao nosso banco de dados para a bandeira DINERS
            break;
        default:
            card = id_card;
            break;
    }

    return card;
}

/**
 * Função para validação do cartão de crédito e seta a bandeira do mesmo conforme a entrada do usuário.
 * @returns {Boolean}
 */
function verificaCartao() {
    var retorno;
    $('#nu_cartao').validateCreditCard(function (result) {
        $('#div_cartao').addClass('has-error');
        $('#div_cartao').removeClass('has-success');
        $('#img_MASTERCARD').addClass('imgopacity');
        $('#img_Elo').addClass('imgopacity');
        $('#img_Amex').addClass('imgopacity');
        $('#img_VISA').addClass('imgopacity');
        $('#img_Diners').addClass('imgopacity');
        $('#img_Hipercard').addClass('imgopacity');

        if (result.card_type != null) {
            cartao = '#img_' + result.card_type.name;
            $(cartao).removeClass('imgopacity');
            $(cartao).addClass('has-error');
            $('#div_cartao').addClass('has-success');
            $('#div_cartao').removeClass('has-error');
            $('#nu_cartaoUtilizado').val(codigoTipoCartao(result.card_type.id_card));

            retorno = true;
        } else {
            $('#div_cartao').addClass('has-error');
            $('#div_cartao').removeClass('has-success');
            $('#img_MASTERCARD').addClass('imgopacity');
            $('#img_Elo').addClass('imgopacity');
            $('#img_Amex').addClass('imgopacity');
            $('#img_VISA').addClass('imgopacity');
            $('#img_Diners').addClass('imgopacity');
            $('#img_Hipercard').addClass('imgopacity');
            retorno = false;
        }

    }, {
        accept: ['VISA', 'MASTERCARD', 'Amex', 'Elo', 'Diners', 'Hipercard']
    });

    $('#nu_cartaoRecorrente').validateCreditCard(function (result) {
        $('#div_cartaoRecorrente').addClass('has-error');
        $('#div_cartaoRecorrente').removeClass('has-success');
        $('#img_MASTERCARD').addClass('imgopacity');
        $('#img_Elo').addClass('imgopacity');
        $('#img_Amex').addClass('imgopacity');
        $('#img_VISA').addClass('imgopacity');
        $('#img_Diners').addClass('imgopacity');
        $('#img_Hipercard').addClass('imgopacity');

        if (result.card_type != null) {
            cartao = '#img_' + result.card_type.name;
            $(cartao).removeClass('imgopacity');
            $(cartao).addClass('has-error');
            $('#div_cartaoRecorrente').addClass('has-success');
            $('#div_cartaoRecorrente').removeClass('has-error');
            $('#nu_cartaoUtilizadoRecorrente').val(codigoTipoCartao(result.card_type.id_card));

            retorno = true;
        } else {
            $('#div_cartaoRecorrente').addClass('has-error');
            $('#div_cartaoRecorrente').removeClass('has-success');
            $('#img_MASTERCARD').addClass('imgopacity');
            $('#img_Elo').addClass('imgopacity');
            $('#img_Amex').addClass('imgopacity');
            $('#img_VISA').addClass('imgopacity');
            $('#img_Diners').addClass('imgopacity');
            $('#img_Hipercard').addClass('imgopacity');
            retorno = false;
        }

    }, {
        accept: ['VISA', 'MASTERCARD', 'Amex', 'Elo', 'Diners', 'Hipercard']
    });

    if (AMBIENTE != 'producao') {
        retorno = true;
    }

    return retorno;
}

// Calcula o tamanho exato disponível para exibição do iFrame de tutorial e seta width e height.

$(document).ready(function setTutorialIframeSize() {
    var height = $(document).outerHeight() - $('.top_wrapper').outerHeight() - $('.footer-portal').outerHeight();
    var width = $(document).outerWidth();
    $('#iframe-tutorial').height(height).width(width);
});
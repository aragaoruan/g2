/**
 * Created by felipe.pastor on 11/02/14.
 */

var OcorrenciaCollection = Backbone.Collection.extend({
//    model: OcorrenciaModel,
    url: "/portal/C-atencao/lista-ocorrencias",
    parse: function (response) {
        _.map(response,function (value,index) {
            if(value.id_evolucao === 30) {
                response[index].id_ocorrencia_parse = value.id_ocorrencia + ' <span style="font-size: 16px;color:#FF0000" class="icon-exclamation-sign"></span>';
            }else{
                response[index].id_ocorrencia_parse = value.id_ocorrencia;
            }
        });
        return response;
    }
});

var ocorrencias = new OcorrenciaCollection();
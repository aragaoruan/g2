var ExtratoColaboradorCollection = Backbone.Collection.extend({
    model: ExtratoColaboradorModel,
    url: '/portal/financeiro/retornar-extrato',
    total: 0,
    parse: function (response, options) {
        var that = this;
        $.each(response, function (i, model) {
            var valor = model.nu_valor;
            valor = valor.replace("+", "");
            valor = valor.replace(".", "");
            valor = valor.replace(",", ".");
            that.total += (parseFloat(valor));
        });
        return response;
    }
});
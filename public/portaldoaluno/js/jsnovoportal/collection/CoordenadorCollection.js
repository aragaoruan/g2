/**
 * Created by felipe.pastor on 11/02/14.
 */

var CoordenadorCollection = Backbone.Collection.extend({
    model: GerenciarSalasModel,
    url: "/portal/index/pesquisasalascoordenadorprojeto",
    state: {
        pageSize: 10000
    },
    mode: "client"
});
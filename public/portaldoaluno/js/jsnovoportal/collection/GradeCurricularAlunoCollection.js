/**
 * Created by Débora Castro on 27/11/14.
 * <debora.castro@unyleya.com.br
 */
var GradeCurricularAlunoCollection = Backbone.Collection.extend({
    model: VwGradeNota,
    url: '/portal/matricula/pesquisa-grade-aluno-perfil'
});
/**
 * Created by felipe.pastor on 11/02/14.
 */


var MensagemCollection = Backbone.Collection.extend({
    model: VwEnviarMensagemAluno,
    url: "/api/mensagem-portal?json=true",
    currentStatus: function (status) {
        return _(this.filter(function (data) {
            return data.get("id_evolucao") == status;
        }));
    }
});


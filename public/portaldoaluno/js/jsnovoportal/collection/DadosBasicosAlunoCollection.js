/**
 * Created by Débora Castro 24/04/2014 <debora.castro@unyleya.com.br>
 */

var DadosBasicosAlunoCollection = Backbone.Collection.extend({
    model: DadosBasicosAlunoModel,
    url: "/portal/avaliacao/agendamento"
});

var dadosbasicos = new DadosBasicosAlunoCollection();
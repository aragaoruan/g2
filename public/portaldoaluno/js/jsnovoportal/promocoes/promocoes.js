$(document).ready(function () {
    $('.promocoes').click(function (element) {
        getPromocao($(element.currentTarget).data("idpromocao"))
    });

    if ((typeof id_primeiraCampanha != "undefined") && id_primeiraCampanha) {
        getPromocao(id_primeiraCampanha)
    }

    function getPromocao(id_campanha) {
        $.post('/portal/promocoes/promocao', 'id_campanha=' + id_campanha, function (data) {
            $('#box-promocao').html(data);
        });
    }
});
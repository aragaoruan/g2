$(document).ready(function() {
    var body = $('body');
    if ($('#formEnviarTCC').length > 0) {
        body.on('paste', '#st_tituloavaliacao', function (e) {
            e.preventDefault();
        });

        var enviar_box_tcc = $('.enviar-tcc-box');
        var hide_save = $('.hide-save');
        body.on('submit', '#formEnviarTCC', function (e) {
            e.preventDefault();
            var options = {
                url: "/portal/avaliacao/upload-tcc",
                type: 'post',
                dataType: 'json',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.type == 'success') {
                        enviar_box_tcc.removeClass('yellow').addClass('green');
                        hide_save.hide();
                        $('.titletcc ').text($('#st_tituloavaliacao').val());
                    } else {
                        enviar_box_tcc.removeClass('green').addClass('yellow');
                    }
                    enviar_box_tcc.show().children('.msg').html(data.mensagem);
                },
                beforeSend: function () {
                    $('.cortina.tcc').show();
                },
                complete: function () {
                    $('.cortina.tcc').hide();
                }
            };
            $.ajax(options);
        });
    }
    body.on('click', '.info-box-remove', function () {
        $(this).parent().hide();
    });
});
/*
 Função para gravar Cookie no cliente
 */
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}


/**
 * Funcoes de loader
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2015-01-26
 */
function loading() {
    $('.cortina-loader').show();
}
function loaded() {
    $('.cortina-loader').hide();
}

/*
 * Novas interações para o novo portal
 *
 * */

$(document).ready(function () {

})
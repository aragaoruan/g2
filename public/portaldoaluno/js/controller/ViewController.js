
var ViewController = {};

ViewController.viewChange = function(url,data,onlyRedirect){
	if(onlyRedirect == true){
		window.location.href = url+(typeof(data) == 'string' ? '?'+data : '');
	}else{
		var options	= {
				dataType	: 'html',
				async		: false,
				url			: url,
				data		: data
		};
		var request = $.ajax(options);
		request
		.done(function(html){
			$('#conteudo').html(html);
		})
		.fail(function(xhr){
			$('#conteudo').html("Erro do Servidor!");
		});
	}
};

/* 
 * Funções js do Primeiro Acesso
 */

$(document).ready(function () {
    $('#pa-aba-endereco-residencial').click(function () {
        $('#pa-aba-endereco-residencial').addClass('active');
        $('#pa-conteudo-endereco-residencial').removeClass('hidden');
        $('#pa-aba-endereco-correspondencia').removeClass('active');
        $('#pa-conteudo-endereco-correspondencia').addClass('hidden');
    });

    $('#pa-aba-endereco-correspondencia').click(function () {
        $('#pa-aba-endereco-correspondencia').addClass('active');
        $('#pa-conteudo-endereco-correspondencia').removeClass('hidden');
        $('#pa-aba-endereco-residencial').removeClass('active');
        $('#pa-conteudo-endereco-residencial').addClass('hidden');
    });

    $('#abas-endereco a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#motivo').keyup(function () {
        if ($('#motivo').prop('value').trim().length > 0) {
            if (verificarPreenchimento()) {
                $('#btnAlterarDadosContato').removeClass('hidden');
                $('#btnAlterarDadosCadastrais').removeClass('hidden');
            }
        } else {
            $('#btnAlterarDadosContato').addClass('hidden');
            $('#btnAlterarDadosCadastrais').addClass('hidden');
        }
    });

    $('#pa-btn-corrigir').click(exibirAlteracoes);

    $('#btnCancelar').click(exibirAlteracoes);

    $('#btnAlterarDadosContato').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/portal/primeiro-acesso/salvar-alteracoes-dados-contato',
            data: $('#pa-alteracoes-dados-contato').serialize(),
            dataType: 'json',
            success: function (data) {
                alert(data.mensagem);
                exibirAlteracoes();
                window.location = window.location;
            },
            beforeSend: function () {
                $('.cortina.cortina-loader').show();
            },
            complete: function () {
                $('.cortina.cortina-loader').hide();
            }
        });
    });

    $('#btnAlterarDadosCadastrais').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/portal/primeiro-acesso/salvar-alteracoes-dados-cadastrais',
            data: $('#pa-alteracoes-dados-cadastrais').serialize(),
            dataType: 'json',
            beforeSend: function () {
                $('.cortina.cortina-loader').show();
            },
            complete: function () {
                $('.cortina.cortina-loader').hide();
            }
        })
    });

    $('#pa-aceitacao-contrato-btn-prosseguir').click(function () {
        if (!$('#pa-aceitacao-contrato-checkbox').prop('checked')) {
            alert('Até o contrato ser lido e aceito você não poderá prosseguir.');
            return false;
        }
        return true;
    });

    $('#sg_uf').change(
        function () {

            $.ajax({
                url: '/default/util/retorna-municipios-json',
                data: 'sg_uf=' + $('#sg_uf').prop('value'),
                dataType: 'json'
            }).done(function (data) {
                var options = "";
                $.each(data.mensagem, function (key, value) {
                    options += '<option value="' + value[0] + '">' + value[1] + '</option>';
                });
                $("#id_municipio").html(options);
            });
        }
    );

    $('#sg_ufcorrespondencia').change(
        function () {

            $.ajax({
                url: '/default/util/retorna-municipios-json',
                data: 'sg_uf=' + $('#sg_ufcorrespondencia').prop('value'),
                dataType: 'json'
            }).done(function (data) {
                var options = "";
                $.each(data.mensagem, function (key, value) {
                    options += '<option value="' + value[0] + '">' + value[1] + '</option>';
                });
                $("#id_municipiocorrespondencia").html(options);
            });
        }
    );

    $('#st_cep').mask('99.999-999');
    $('#st_cepcorrespondencia').mask('99.999-999');
    $('#nu_ddd').mask('(99)');
    $('#nu_telefone').mask('99999-999?9');
    $('#nu_dddalternativo').mask('(99)');
    $('#nu_telefonealternativo').mask('99999-999?9');

    $('#dt_nascimento').mask('99/99/9999');
    $('#dt_dataexpedicao').mask('99/99/9999');
    $('#st_cpf').mask('999.999.999-99');
});

exibirAlteracoes = function () {
    var alteracoesContainer = $('#pa-alteracoes-container');
    if (alteracoesContainer.hasClass('hidden')) {
        alteracoesContainer.show(500);
        alteracoesContainer.removeClass('hidden');
    } else {
        alteracoesContainer.hide(500);
        alteracoesContainer.addClass('hidden');
    }
    return false;
};

verificarPreenchimento = function () {

    ok = false;
    $(".frm-alteracoes").find('input').each(function () {
        if ($(this).prop('value') != '' && $(this).prop('id') != 'motivo' && $(this).prop('id') != 'etapa') {
            ok = true;
        }
    });

    $(".frm-alteracoes").find('select').each(function () {
        if ($(this).prop('value') != '' && $(this).prop('id') != 'motivo' && $(this).prop('id') != 'etapa') {
            ok = true;
        }
    });
    return ok;
};

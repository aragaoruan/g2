/**
 * variável 'planoPag' instanciada em financeiro/index.phtml
 */
$.getScript('/portal/financeiro/js-plano-pagamento');

document.onreadystatechange = function() {
    if ($('#tab-plano-pagamento').length > 0) {

        if (document.readyState === 'complete') {
            $.getScript(planoPag.urlG2 + "/js/backbone/models/vw-matricula.js?_=$.now()");

            var gridContainer = $("#tab-parcelas");
            renderParcelas = function() {
                gridContainer.html('<div class="text-right" style="padding-top: 10px;" id="div-mudar-cartao-recorrente"> Clique <a id="abrir-tela-mudar-cartao-recorrente" style="cursor: pointer;">AQUI</a> para informar os dados do Cartão de Crédito.</div><br/><br/>');
                $('#abrir-tela-mudar-cartao-recorrente').click(function(){
                    window.open(planoPag.urlLoja+'/loja/pagamento/mudar-cartao-recorrente?venda='+$('#id_venda').val())
                });
                that = this;
                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: '/default/matricula/retornar-responsavel-financeiro',
                    data: 'id_matricula=' + planoPag.id_matricula,
                    success: function(data) {
                        $.ajaxSetup({'async': false});
                        if (data.type === 'success') {
                            for (var i in data.mensagem) {
                                that.gerarGridParcelas(data.mensagem[i].id_usuariolancamento, data.mensagem[i].st_usuariolancamento, 0);
                            }
                        } else {
                            gridContainer.append(data.text);
                            $.pnotify(data);
                        }
                        $.ajaxSetup({'async': true});
                    },
                    complete: function() {
                    }
                });
            };

            gerarGridParcelas = function(id_usuariolancamento, st_usuariolancamento, bl_original) {
                var VwResumoFinanceiro = Backbone.Model.extend({});
                var DataGrid = Backbone.PageableCollection.extend({
                    url: '/default/matricula',
                    state: {
                        pageSize: 12
                    },
                    mode: "client", // page entirely on the client side
                    valorParcelasAPagar: function() {
                        var valorAPagar = 0;
                        this.fullCollection.each(function(i, o) {
                            if (!i.get('bl_quitado')) {
                                valorAPagar += parseFloat(i.get('nu_valor'));
                            }
                        });

                        return valorAPagar;
                    },
                    valorParcelasEmAtraso: function() {
                        var valorAtrasados = 0;
                        this.fullCollection.each(function(i, o) {
                            if (!i.get('bl_quitado')) {
                                var dt_vencimento = moment(i.get('dt_vencimento'), "DD/MM/YYYY");

                                var hoje = moment();
                                var diferenca = hoje.diff(dt_vencimento, 'days');
                                if (diferenca > 0) {
                                    valorAtrasados += parseFloat(i.get('nu_valor'));
                                }
                            }
                        });
                        return valorAtrasados;
                    }
                });

                // recupara a url e os parametros passados
                var url = '/default/matricula/retornar-vw-resumo-financeiro';
                var dataResponse;
                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: url,
                    data: 'id_matricula=' + planoPag.id_matricula + '&id_usuariolancamento=' + id_usuariolancamento + '&bl_original=' + bl_original,
                    beforeSend: function() {
                    },
                    success: function(data) {
                        dataResponse = data;
                    },
                    complete: function() {
                        if (dataResponse.type === 'success') {
                            var dataGrid = new DataGrid(dataResponse.mensagem);
                            var objTemp = dataResponse.mensagem[0];
                            if(objTemp.get('bl_cartaovencendo')){
                                $('#div-mudar-cartao-recorrente').show()
                            }else{
                                $('#div-mudar-cartao-recorrente').hide()
                            }

                            if (objTemp != undefined && objTemp && objTemp.get('bl_recorrente') && objTemp.get('id_vendamatricula') == objTemp.get('id_venda')) {
                                $('#lnk-trocar-cartao-recorrente').show()
                                id_venda_trocar_cartao_recorrente = objTemp.get('id_venda');
                            }

                            //Colunas comuns a grid de parcelas originais e atuais
                            var columns = [{
                                    name: "nu_ordemparcela",
                                    label: "Nº Parcelas",
                                    editable: false,
                                    cell: "string"
                                },
                                {
                                    name: "nu_valor",
                                    label: "Valor",
                                    editable: false,
                                    cell: "string"
                                },
                                {
                                    name: "st_meiopagamento",
                                    label: "Meio de Pagamento",
                                    editable: false,
                                    cell: "string"
                                },
                                {
                                    name: "st_situacao",
                                    label: "Status",
                                    editable: false,
                                    cell: Backgrid.StringCell.extend({
                                        className: 'situacao-cell'
                                    })
                                },
                                {
                                    name: "dt_vencimento",
                                    label: "Vencimento",
                                    editable: false,
                                    cell: "string"
                                },
                                {
                                    name: "dt_quitado",
                                    label: "Data da Baixa",
                                    editable: false,
                                    cell: "string"
                                },
                                {
                                    name: "st_link",
                                    label: "Recibo/2ª Via",
                                    editable: false,
                                    cell: Backgrid.UriCell.extend({
                                        render: function() {
                                            if (this.model.get('bl_quitado')) {
                                                this.$el.html('<a href="' + this.model.get('st_link') + '" target="_blank"><i class="icon icon-file"></i></a>');
                                            } else {
                                                if (this.model.get('id_meiopagamento') == 2) { //Se for boletos
                                                    this.$el.html('<a href="' + planoPag.urlLoja + '/loja/pagamento/boleto/id_lancamento/' + this.model.get('id_lancamento') + '" target="_blank"><i class="icon icon-print"></i></a>');
                                                }
                                            }
                                            return this;
                                        }
                                    })
                                }
                            ];

                            var valorAPagar = 0;
                            var valorAtrasados = 0;

                            // Suppose you want to highlight the entire row when an editable field is focused
                            var RowGrid = Backgrid.Row.extend({
                                initialize: function(options) {
                                    RowGrid.__super__.initialize.apply(this, arguments);
                                }
                            });

                            //verifica se o json retornou algum resultado
                            if (dataGrid.length > 0) {
                                //monta a grid
                                var pageableGrid = new Backgrid.Grid({
                                    row: RowGrid,
                                    columns: columns,
                                    collection: dataGrid,
                                    className: 'backgrid backgrid-selectall table table-bordered table-hover table_imcube'
                                });
                                //renderiza o grid
                                if (!bl_original) {
                                    gridContainer.append('<div class="row titulo-grid-parcelas"><div class="span12"><h6 class="span7">' + st_usuariolancamento + '</h6><div class="pull-right btn-group"></div>');
                                    gridContainer.append('<input type="hidden" id="id_venda" name="id_venda" value="' + dataResponse.mensagem[0].get('id_venda') + '"/>');
                                } else {
                                    gridContainer.append('<div class="row titulo-grid-parcelas"><div class="span12"><h6 class="span7">' + st_usuariolancamento + '</h6>');
                                }

                                gridContainer.append(pageableGrid.render().$el);

                                if (!bl_original) {
                                    $(pageableGrid.render().$el).after(
                                            '<table class="backgrid backgrid-selectall table table-bordered table-hover table_imcube"><thead>' +
                                            '<tr><th>Valores a pagar: R$ ' + dataGrid.valorParcelasAPagar().toFixed(2) + '</th></tr>' +
                                            '<tr><th>Valores vencidos: R$ ' + dataGrid.valorParcelasEmAtraso().toFixed(2) + '</th></tr>' +
                                            '</thead></table>'
                                            );
                                }

                                //Colorem o status
                                $('.situacao-cell').each(function() {
                                    if ($(this).text() === 'Pago') {
                                        $(this).addClass('pago');
                                        //} else if ($(this).text() === 'Pendente') {
                                        //    $(this).addClass('pendente');
                                    } else if ($(this).text() === 'Vencido') {
                                        $(this).addClass('vencido');
                                    }
                                });

                                //configura o paginator
                                var paginator = new Backgrid.Extension.Paginator({
                                    collection: dataGrid
                                });
                                //renderiza o paginator
                                gridContainer.append(paginator.render().$el);
                                dataGrid.fetch({reset: true});
                                gridContainer.append('</div></div>');
                            } else {
                                gridContainer.append('Sem resultado para exibição!');
                            }
                        } else {
                            gridContainer.html('');
                            gridContainer.append(dataResponse.text);
                            $.pnotify(dataResponse);
                        }
                    }
                });
            };
            renderParcelas();
        }
    }
};
/**
 * Classe que mostra o texto carregando na tela do usuário
 */
var Carregando = {};

/**
 * Método estatico que mostra o carregando
 */
Carregando.iniciar = function(){
	
	$(document).ajaxStart(function(){
		$("#carregando-tela, #icone_carregando").show();
		
	}).ajaxStop(function(){
		$("#carregando-tela, #icone_carregando").hide();
	}).ajaxError(function(e, jqxhr, settings, exception){
		//$("#carregando-tela, #icone_carregando").hide();
		Mensageiro.mostrar("Erro no Servidor!",0);
	});
};

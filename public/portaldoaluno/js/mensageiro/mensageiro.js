/**
 * Classe de Notificações de Mensagens
 */
var Mensageiro = {};

/**
 * Método Estatico que mostra uma Determinada Mensagem
 */
Mensageiro.mostrar = function(mensagem,tipo,tempo){
	
	//Removendo Classes
	$('#panel-mensageiro').removeClass('mensageiro-sucesso');
	$('#panel-mensageiro').removeClass('mensageiro-aviso');
	$('#panel-mensageiro').removeClass('mensageiro-erro');
	
	//Fechando por se caso já estiver aberto.
	$('#mensageiro').slideUp('fast',
			function(){
				$('#mensagem-mensageiro').html('');
				$('#tipo-mensagem').html('');
				var mensagem_tipo = '';
				switch(tipo){
				case 1:
					mensagem_tipo = 'Sucesso:';
					$('#panel-mensageiro').addClass('mensageiro-sucesso');
					break;
				case 2:
					mensagem_tipo = 'Aviso:';
					$('#panel-mensageiro').addClass('mensageiro-aviso');
					break;
				case 0:
					mensagem_tipo = 'Erro:';
					$('#panel-mensageiro').addClass('mensageiro-erro');
					break;
				}
			
				var delayMensagem = 0;
				
				if(tempo != undefined){
					delayMensagem = parseInt(tempo);
				}else{
					var calculoEspera = mensagem.length/0.4 * 10;
					if(calculoEspera < 500){
						delayMensagem = 500;
					}else if(calculoEspera > 7000){
						delayMensagem = 7000;
					}else{
						delayMensagem = calculoEspera;
					}
				}
				$('#mensageiro').slideDown('fast');
				$('#mensagem-mensageiro').html(mensagem);
				$('#tipo-mensagem').html(mensagem_tipo);
				$('#panel-mensageiro').slideDown('slow').delay(delayMensagem).slideUp('slow',
						function(){
					$('#mensageiro').slideUp('fast');
				});
	});
	
}
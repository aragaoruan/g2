$(document).ready(function(){
var Grade = {
		
	capturaGrids: function(){
		$(".gridModulo").each(function(indice, tabela){
			Grade.dadosGrid($(tabela).attr("id"));
		});
	},	
		
	/**
	 * Alterando os módulos e aplicando o efeito de slideUp e slideDown
	 */
	dadosGrid: function(idGrid){
		var dadosGrid = eval(($('#' + idGrid + 'DadosHidden').attr('value')));
		$("#" + idGrid).jqGrid({
			datatype: "local",
			height: "100%",
		   	colNames:['Disciplina','Início', 'Término', 'Carga Horária', 'Professor'],
		   	colModel:[
		   		{name:'st_disciplina',index:'st_disciplina',width:155},
		   		{name:'dt_abertura',index:'dt_abertura',width:80, align:"center"},
		   		{name:'dt_encerramento',index:'dt_encerramento', align:"center", width:80},
		   		{name:'nu_cargahoraria',index:'nu_cargahoraria', width:85, align:"center",sorttype:"float"},
		   		{name:'st_professortitular',index:'st_professortitular',width:155}
		   	],
		   	caption: $('#' + idGrid + 'ModuloHidden').attr('value')
		});
		
		for(var i=0; i<= dadosGrid.length;i++){
			$("#" + idGrid).jqGrid('addRowData', i+1, dadosGrid[i]);
		}
	}
};

Grade.capturaGrids();
	

/*
 * Configurações dos painéis....
 * 
 * */	
// panels
$('.dragbox').each(function(){
	$(this).hover(function(){
		$(this).find('h2').addClass('collapse');
	}, function(){
		$(this).find('h2').removeClass('collapse');
	})
	.find('h2').hover(function(){
		$(this).find('.configure').css('visibility', 'visible');
	}, function(){
		$(this).find('.configure').css('visibility', 'hidden');
	})
	.click(function(){
		$(this).siblings('.dragbox-content').toggle();
	})
	.end()
	.find('.configure').css('visibility', 'hidden');
});

$('.column').sortable({
	connectWith: '.column',
	handle: 'h2',
	cursor: 'move',
	placeholder: 'placeholder',
	forcePlaceholderSize: true,
	opacity: 0.4,
	stop: function(event, ui){
		$(ui.item).find('h2').click();
		var sortorder='';
		$('.column').each(function(){
			var itemorder = $(this).sortable('toArray');
			var columnId  = $(this).attr('id');
			if(itemorder.length>0) {
				for(x=0;x<itemorder.length;x++) {
					sortorder+=columnId+'[]='+itemorder[x]+'&';
				}
			}
		});
	}
}).disableSelection();

/**
 * Ativando hover nos botões
 */
$('#dialog_link, ul.icons li').hover(
    function() { $(this).addClass('ui-state-hover'); },
    function() { $(this).removeClass('ui-state-hover'); }
);
});
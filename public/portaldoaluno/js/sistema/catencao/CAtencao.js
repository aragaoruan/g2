var CAtencao = function() {

    var self = this;

    /**
     * Listando ocorrencias iniciais
     */
    this.changeInicial = function() {
        var dados =
                {
                    id_situacao: $("select#cb_id_situacao option:selected").val(),
//							id_evolucao: $("select#cb_id_evolucao option:selected").val(),
                    ajax: true,
                    json: true
                };
        self.carregarOcorrencias(dados);


    };

    /**
     * Alterando as ocorrencias pela evolucao
     */
    this.changeEvolucao = function() {
        $('#cb_id_evolucao').change(function() {
            var dados =
                    {
                        id_situacao: $("select#cb_id_situacao option:selected").val(),
                        id_evolucao: $("select#cb_id_evolucao option:selected").val(),
                        ajax: true,
                        json: true
                    };
            self.carregarOcorrencias(dados);

        });
    };

    /**
     * Alterando as ocorrencias pela situacao
     */
    this.changeSituacao = function() {
        $('#cb_id_situacao').change(function() {
            var dados =
                    {
                        id_situacao: $("select#cb_id_situacao option:selected").val(),
                        id_evolucao: $("select#cb_id_evolucao option:selected").val(),
                        ajax: true,
                        json: true
                    };
            self.carregarOcorrencias(dados);

        });
    };
//ate aqui ok (eu acho)
    /**
     * Carregando as ocorrencias
     */
    this.carregarOcorrencias = function(dados) {
        $('#idConteudoOcorrencias').slideUp('slow', function() {
            $('#idConteudoOcorrencias').slideDown('slow');
            Portal.Ajax.loadHTML('containerOcorrencias', config.baseUrlModulo + '/C-atencao/lista-ocorrencias', dados);
        });
    };

    /**
     * chamando nova ocorrencia
     */
    this.novaOcorrencia = function() {
        $('#idConteudoOcorrencias').slideUp('slow', function() {
            Portal.Ajax.loadHTML('conteudo', config.baseUrlModulo + '/C-atencao/nova-ocorrencia', '');
        });
    };

    /**
     * Buscando assuntos relacionados ao assunto pai
     */
    this.changeAssuntoPai = function() {
        $('#cb_id_assuntopai').change(function() {
            if ($("select#cb_id_assuntopai option:selected").attr("value") != 0) {
                var dados =
                        {
                            id_assuntocopai: $("select#cb_id_assuntopai option:selected").val(),
                            ajax: true,
                            json: true
                        };
                self.carregarAssunto(dados);
            }
        }).trigger('change');
    };

    this.carregarAssunto = function(dados) {
        Portal.Ajax.loadJSON(config.baseUrlModulo + '/C-atencao/retorna-combo-assuntoco', dados, self.resultCarregarAssunto);
    };

    /**
     * Carregando o combo de assuntos de acordo com o assunto pai selecionado
     */
    this.resultCarregarAssunto = function(source) {
        $('select#cb_id_assuntoco option').remove();
        if (source != "null") {
            $('#cb_id_assuntoco').append('<option value="0" selected="selected">Selecione</option>');
            $('#cb_id_assuntoco').append(source);
        } else
            $('#cb_id_assuntoco').append('<option value="0" selected="selected">Nenhum assunto encontrado</option>');

    };


    /**
     * Combo de assunto procura texto cadastrado para o mesmo
     */
    this.changeAssunto = function() {
        $('#cb_id_assuntoco').change(function() {
            if ($("select#cb_id_assuntoco option:selected").attr("value") != 0) {
                var dados =
                        {
                            id_assuntoco: $("select#cb_id_assuntoco option:selected").val(),
                            ajax: true,
                            json: true
                        };
                self.carregarDescricao(dados);
            }
        }).trigger('change');
    };

    this.carregarDescricao = function(dados) {
        Portal.Ajax.loadJSON(config.baseUrlModulo + '/C-atencao/retorna-descricao-assuntoco', dados, self.resultCarregarDescricao);
    };

    /**
     * Preenche o st_ocorrencia com o texto pre-definido do assunto
     */
    this.resultCarregarDescricao = function(source) {
        $('#st_ocorrencia').val('');
        if (source != null) {
            $('#st_ocorrencia').val(source.mensagem[0].st_texto);
        }

    };


    /**
     * Mostra tela com detalhes da ocorrência e suas interações
     */
    this.detalhesOcorrencia = function(id_ocorrencia) {
        $('#idConteudoOcorrencias').slideUp('slow', function() {
            $("#lgdOcorrencias").text('Ocorrência nº: ' + id_ocorrencia);
            $('#idConteudoOcorrencias').slideDown('slow');
            var dados =
                    {
                        id_situacao: $("select#cb_id_situacao option:selected").val(),
                        id_evolucao: $("select#cb_id_evolucao option:selected").val(),
                        id_ocorrencia: id_ocorrencia,
                        ajax: true,
                        json: true
                    };
            Portal.Ajax.loadHTML('content_ocorrencia', config.baseUrlModulo + '/C-atencao/detalharocorrencia', dados);
        });
    };

    this.alterarOcorrencia = function(situacao, evolucao) {
        $.ajax({
            //url: config.baseUrlModulo + '/C-atencao/alterar-ocorrencia',
            url: '/portal/c-atencao/alterar-ocorrencia',
            dataType: 'json',
            async: false,
            data: {id_ocorrencia: $('#id_ocorrencia').val(),
                id_evolucao: evolucao,
                id_situacao: situacao},
            success: function(source) {
                if (source.tipo != 1) {
                    Mensageiro.mostrar(source.mensagem[0], 3);
                } else if (source.tipo == 1) {
                    var dados =
                            {
                                id_situacao: $("#id_situacao").val(),
                                id_evolucao: $("#id_evolucao").val(),
                                id_ocorrencia: $('#id_ocorrencia').val(),
                                ajax: true,
                                json: true
                            };

                    //Portal.Ajax.loadHTML('content_ocorrencia', config.baseUrlModulo + '/C-atencao/detalharocorrencia', dados);
                    Mensageiro.mostrar("Ocorrência alterada com sucesso!", 1, 1500);
                    window.location.reload();
                }
            }
        });
    };

    /*Reabertura de uma ocorrência pelo interessado 
     É nessária uma justificativa de pelo menos 15 caracteres
     */
    this.reabrirOcorrencia = function(situacao, evolucao) {
        $("#dialogFrmJustificativa").dialog("destroy");
        $("#dialogFrmJustificativa").dialog({
            autoOpen: true,
            height: 180,
            width: 400,
            modal: false,
            title: 'Justifique a reabertura da ocorrência',
            buttons: {
                "Reabrir": function() {
                    var pars = $('#frmJustificaReabrir').serialize() + '&id_ocorrencia=' + $('#id_ocorrencia').val() + '&id_evolucao=' + evolucao + '&id_situacao=' + situacao;
                    $.ajax({
                        url: config.baseUrlModulo + '/C-atencao/reabrir-ocorrencia',
                        dataType: 'json',
                        async: false,
                        data: pars,
                        success: function(source) {
                            if (source.tipo != 1) {
                                Mensageiro.mostrar(source.mensagem[0], 3);
                            } else if (source.tipo == 1) {
                                $("#dialogFrmJustificativa").dialog("close");
                                $("#dialogFrmJustificativa").dialog("destroy");
                                $("#dialogFrmJustificativa").remove();

                                var dados =
                                        {
                                            id_situacao: $("#id_situacao").val(),
                                            id_evolucao: $("#id_evolucao").val(),
                                            id_ocorrencia: $('#id_ocorrencia').val(),
                                            ajax: true,
                                            json: true
                                        };
                                //Portal.Ajax.loadHTML('content_ocorrencia', config.baseUrlModulo + '/C-atencao/detalharocorrencia', dados);
                                Mensageiro.mostrar("Ocorrência alterada com sucesso!", 1, 1500);
                                window.location.reload();
                            }
                        }
                    });

                },
                "Cancelar": function() {
                    $("#dialogFrmJustificativa").dialog("close");
                    $("#dialogFrmJustificativa").dialog("destroy");
                }
            }

        });
    };

};

$(document).ready(function() {

    var catencao = new CAtencao();

    // validate signup form on keyup and submit
    $('.jbutton').button();

    /**
     * Ativando hover nos botões
     */
    $('#dialog_link, ul.icons li').hover(
            function() {
                $(this).addClass('ui-state-hover');
            },
            function() {
                $(this).removeClass('ui-state-hover');
            }
    );

    $("#frmJustificaReabrir").validate({
        rules: {
            st_tramite: {required: true, minlength: 15}
        }
    });

    $('#btnnovotramite').click(function() {
        $('#novo_tramite').toggle();
    });

    $("#frmTramite").validate({
        rules: {
            st_tramite: {required: true, minlength: 15}
        }
    });

    $('#btnEncerrar').click(function() {
        catencao.alterarOcorrencia($('#constSituacaoEncerrada').val(), $('#constEvolEncerrPorInteressado').val());
    });

    $('#btnReabrir').click(function() {
        catencao.reabrirOcorrencia($('#constSituacaoPendente').val(), $('#constEvolReabPorInteressado').val());
    });

    $('#btnVoltar').click(function() {
        window.location.href = '/portal/c-atencao/index';
    });
});
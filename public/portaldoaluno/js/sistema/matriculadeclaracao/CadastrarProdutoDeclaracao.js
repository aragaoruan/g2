var CadastrarProdutoDeclaracao = function(){
	
	var self = this;
	
	/**
	 * Listener de click do botão de solicitar declaracao
	 */
	this.listenerClickSolicitarDeclaracao = function(){
		$('#btn_solicitardeclaracao').click(function(event){
			var arr_textosistema = new Array();
			$('input[type="checkbox"]:checked').each(function(){
				arr_textosistema.push($(this).val());
			});
			if(!arr_textosistema.length){
				Mensageiro.mostrar("Selecione Pelo menos uma Declaração!",2);
				return false;
			}
			var pars = {
					id_textosistema: arr_textosistema,
					ajax: true,
					json: true
			};
			Portal.Ajax.loadJSON(config.baseUrlModulo+'/matriculadeclaracao/cadastrarprodutodeclaracao',pars,self.resultCadastrarProdutoDeclaracao);
		});
	};
	
	/**
	 * Listener de click do botão de cancelar a solicitacao
	 */
	this.listenerClickCancelarSolicitacao = function(){
		$('#btn_cancelar').click(function(event){
			ViewController.viewChange(config.baseUrlModulo+'/matriculadeclaracao/entregadeclaracao');
		});
	};
	
	/**
	 * Result de cadastrar o produto da declaracao
	 */
	this.resultCadastrarProdutoDeclaracao = function(source,xhr){
		Mensageiro.mostrar(source.mensagem[0],source.tipo);
		if(source.tipo == 1){
			ViewController.viewChange(config.baseUrlModulo+'/matriculadeclaracao/entregadeclaracao');
		}
	};
};

//var Agendamento = function(){
//
//	var self = this;
//
//	/**
//	 * Listener do botao de agendar avaliacao
//	 */
//	this.agendarAvaliacaoListener = function(){
//		$('#btnAgendarAvaliacao').click( function(event) {
//			self.carregarFormularioAgendamento('item0_id');
//		});
//	};
//
//	/**
//	 * Listener de Change do combo de seleção de Módulo
//	 */
//	this.comboModuloChangeListener = function(){
//		$('#id_modulo').change( function(event){
//			$('#grid-avaliacao-nao-agendada').html('');
//			if($('#id_modulo option:selected').val() == ''){
//				$('#id_disciplina').html( '<option value="" >Selecione.</option>' );
//				return false;
//			}
//			$('#id_disciplina').html( '<option value="" >Buscando....<option>' );
//			var parsDis	= {id_modulo : $('#id_modulo option:selected').val(), json: true, ajax:true } ;
//			var pars	= {id_modulo : $('#id_modulo option:selected').val()} ;
//			Portal.Ajax.loadHTML( 'lista-disciplina', config.baseUrlModulo + '/Avaliacao/listar-avaliacao-agendamento', pars);
//			Portal.Ajax.loadJSON( config.baseUrlModulo + '/Avaliacao/listar-disciplinas', parsDis, self.resultCarregaComboDisciplina);
//			self.listenerClickAgendamento();
//		});
//	};
//
//	/**
//	 * Result de retornar as disciplinas do Combo de Disciplinas
//	 */
//	this.resultCarregaComboDisciplina = function(source, xhr){
//		if(source.tipo != 1){
//			Mensageiro.mostrar(source.mensagem[0],source.tipo);
//		}else{
//			$('#id_disciplina').html('<option value=""> Selecione</option> ');
//			jQuery.each( source.mensagem[0]['disciplinas'] , function( index, disciplina ){
//				$('#id_disciplina').append($( '<option value="'+ disciplina.id_disciplina +'">'+ disciplina.st_disciplinaexibicao +'</option>' ));
//			});
//		}
//	};
//
//	/**
//	 * Método que carrega o HTML do Formulario de Agendamento
//	 */
//	this.carregarFormularioAgendamento = function(id_container){
//		Portal.Ajax.loadHTML(id_container, config.baseUrlModulo +'/Avaliacao/cadastrar-agendamento');
//		self.listenerClickAgendamento();
//		self.botaoCancelarAgendamentoListener();
//		self.comboModuloChangeListener();
//		self.listenerComboDisciplinaChange();
//		self.listenerClickCadastrarAgendamento();
//	};
//
//	/**
//	 * Listener de Change do Combo de Disciplinas
//	 */
//	this.listenerComboDisciplinaChange = function(){
//		$('#id_disciplina').change(function(event){
//			if($('#id_disciplina option:selected').val() == '' ) {
//				return false;
//			}
//			var idModulo		= $('#id_modulo').val();
//			var idDisciplina	= $('#id_disciplina option:selected').val();
//			$('#grid-avaliacao-nao-agendada').html('');
//			self.listarDisplinasParaAgendamento(idModulo, idDisciplina);
//			self.listenerClickAgendamento();
//		});
//	};
//
//	/**
//	 * Retorna a Lista de Disciplinas por Agendamento
//	 */
//	this.listarDisplinasParaAgendamento = function(id_modulo,id_disciplina){
//		var pars	= {};
//		if( id_modulo != undefined ){
//			pars.id_modulo	= id_modulo;
//		}
//		if( id_disciplina != undefined ){
//			pars.id_disciplina	= id_disciplina;
//		}
//		Portal.Ajax.loadHTML('lista-disciplina', config.baseUrlModulo +'/Avaliacao/listar-avaliacao-agendamento', pars);
//	};
//
//	/**
//	 * Listener de Click de agendamento
//	 */
//	this.listenerClickAgendamento = function(){
//		$('.agendamento').click( function(event){
//			$('#dt_agendamento').datepicker('destroy');
//			self.limparFormularioAgendamento();
//			var arId							= $('.agendamento').attr('id').split(';');
//			var idAvaliacao						= arId[0];
//			var idAvaliacaoAplicacao			= arId[1];
//			var dtAplicacao						= $('input[name="dt_aplicacao['+ idAvaliacao +']['+ idAvaliacaoAplicacao +']"]');
//	 		var idAvaliacaoconjuntoreferencia	= $('input[name="id_avaliacaoconjuntoreferencia['+ idAvaliacao +']['+ idAvaliacaoAplicacao +']"]');
//			var blUnica							= $('input[name="bl_unica['+ idAvaliacao +']['+ idAvaliacaoAplicacao +']"]');
//			var diaSemana						= '';
//			diaSemana							= $('input[name="id_diasemana['+ idAvaliacao +']['+ idAvaliacaoAplicacao +']"]').val().split(';');
//			var semana					= new Array();
//			semana[0]	= '7';
//			semana[1]	= '1';
//			semana[2]	= '2';
//			semana[3]	= '3';
//			semana[4]	= '4';
//			semana[5]	= '5';
//			semana[6]	= '6';
//			$('#id_avaliacaoaplicacao').val(idAvaliacaoAplicacao);
//			$('#id_avaliacaoconjuntoreferencia').val($(idAvaliacaoconjuntoreferencia).val());
//			$('#id_avaliacao').val(idAvaliacao);
//			if($(blUnica).val() == 0) {
//				$('#dt_agendamento').val(jQuery.trim($(dtAplicacao).val()));
//				$("#btnAgendamento" ).hide();
//			} else {
//				$('#dt_agendamento').datepicker({
//				    minDate: 'today',
//					beforeShowDay: function(date){
//						if(jQuery.inArray(semana[date.getDay()],  diaSemana) != -1){
//							return [true];
//						}else{
//							return [false];
//						}
//					}
//				});
//			}
//			$('#dialog-agendamento').show();
//		});
//	};
//
//	/**
//	 * Método que limpa o formulario de agendamento
//	 */
//	this.limparFormularioAgendamento = function(){
//		$("#btnAgendamento").show();
//		$('#dt_agendamento').val('');
//		$('#id_avaliacaoaplicacao').val('');
//		$('#id_avaliacaoconjuntoreferencia').val('');
//		$('#id_avaliacao').val('');
//	};
//
//	/**
//	 * Listener do botão cancelar agendamento
//	 */
//	this.botaoCancelarAgendamentoListener = function(){
//		$('#btnCancelar').click( function(event) {
//			$('#dialog-agendamento').hide();
//		});
//	};
//
//	/**
//	 * Listener de click de cadastrar agendamento
//	 */
//	this.listenerClickCadastrarAgendamento = function(){
//		$('#btnCadastrarAgendamento').click(function(event){
//			if(!$('#dt_agendamento').val()){
//				Mensageiro.mostrar('Informe a Data do Agendamento.',2);
//				return false;
//			}
//			var id_avaliacao = $('#id_avaliacao').val();
//			var dt_agendamento = $('#dt_agendamento').val();
//			var id_avaliacaoaplicacao = $('#id_avaliacaoaplicacao').val();
//			var id_avaliacaoconjuntoreferencia = $('#id_avaliacaoconjuntoreferencia').val();
//			var pars = {id_avaliacao: id_avaliacao,
//						dt_agendamento: dt_agendamento,
//						id_avaliacaoaplicacao: id_avaliacaoaplicacao,
//						id_avaliacaoconjuntoreferencia: id_avaliacaoconjuntoreferencia,
//						json: true,
//						ajax:true};
//			Portal.Ajax.loadJSON(config.baseUrlModulo+'/Avaliacao/confirmar-agendamento',pars,self.resultCadastrarAgendamento);
//		});
//	};
//
//	/**
//	 * Resutl de Cadastrar Agendamento
//	 */
//	this.resultCadastrarAgendamento = function(source, xhr){
//		Mensageiro.mostrar(source.mensagem[0],source.tipo, 3000);
//		if(source.tipo == 1){
//			Portal.Ajax.loadHTML('pagina_conteudo', config.baseUrlModulo+'/Avaliacao/agendamento', 'ajax=true');
//		}
//	};
//};
/**
 * Classe responsável por geração de grid's.  
 */

Portal.Grid	=	function( idGrid, dados ) {
	this.idGrid	= idGrid;
	this.header	= new Array();
	this.dados	= dados;
	this.config	= {};
	
};

Portal.Grid.prototype.setCaption	= function( caption ){
	this.config.caption	= caption;
};

Portal.Grid.prototype.addColumn			= function( objHeader ) {
	this.header.push( objHeader );
	return this;
};

Portal.Grid.prototype.render	= function( idContainer ){
	var table		= jQuery( '<table></table>' ).attr( 'id', this.idGrid );
	var columnName	= new Array();
	var columnModel	= new Array();
	var header; 
	var row; 
	var rowId; 
	var index;
	
	for( index=0; index < this.header.length; index++ ){
		columnName.push( this.header[index].label );
		columnModel.push( this.header[index] );
	}
	
	this.config.colNames	= columnName;
	this.config.colModel	= columnModel;
	this.config.datatype	= 'local';

	
	
	
	jQuery("#"+ idContainer).html( table );
	jQuery(table).jqGrid( this.config );
	
	var trId = false;
	for(index=0; index < this.dados.length; index++ ){
		jQuery('#'+ this.idGrid ).jqGrid('addRowData', ((this.config.rowId == undefined) ? (index+1) : this.dados[index][this.config.rowId]) , this.dados[index] );
	}
};


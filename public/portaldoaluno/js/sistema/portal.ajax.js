Portal.Ajax	= {};

Portal.Ajax.loadAJAX	= function( options ){
	$.ajax( options );
};

/**
 * Gera uma requisição ajax com os dados retornados em JSON
 * url  		- endereço de destino
 * data 		- objeto contendo os parâmetros a serem enviados
 * onSuccess	- função de callback a ser utilizada na resposta do servidor
 * option		- OPCIONAL para sobreescrever as opçÕes do ajax. Ver função jQuery.ajax
 * 
 * @param string url  	- url de destino  
 * @param object data 	- parâmetros a serem enviados
 * @param function onSuccess	  	- função a ser executada após o retorno do servidor 
 * @param object option 		- parâmetros adicionais a serem passados para o ajax
 */
Portal.Ajax.loadJSON	= function( url, data, onSuccess, option ){
	var options	= {
			dataType	: 'json',
			async		: false,
			url			: url,
			data		: data,
			success		: onSuccess
	};
	
	jQuery.extend( options, option );
	options.dataType	= 'json';
	
	this.loadAJAX( options );
};


/**
 * Gera uma requisição ajax e insere o retorno da função em um container
 * idContainer	- id do elemento aonde será inserido o html
 * url			- endereço de onde será buscado o html
 * data			- objeto contendo os parâmentros a serem enviados
 * 
 * @param string idContainer
 * @param string url
 * @param data
 */
Portal.Ajax.loadHTML	= function( idContainer, url, data ){
	var options	= {
			dataType	: 'html',
			async		: false,
			url			: url,
			data		: data,
			success		: function( response ) {
				jQuery( '#'+ idContainer ).html( response );
			}
	};
	
	this.loadAJAX( options );
};
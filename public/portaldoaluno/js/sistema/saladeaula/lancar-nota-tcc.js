/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var gestao = new GestaoSala();

function cancelarTCC() {

    if(!$('#st_justificativa').val()){
        alert('O campo justificativa e obrigatorio');
        $('#st_justificativa').focus();
        return;
    }
    
    $('#frmCancelamentoTcc').ajaxSubmit({
        url: '/portal/sala-de-aula/cancelarliberacaotcc',
        async:false,
        dataType:'json',
        success: function(obj) {
            $('.modal').hide();
            Mensageiro.mostrar(obj.mensagem[0], obj.tipo, 5000);
            
            //Atualizando o conteudo da pagina.
            var dados =  $('#frmCancelamentoTcc').serialize();
            dados += '&ajax=false&json=true';
            Portal.Ajax.loadHTML('content_gestao', config.baseUrlModulo + '/sala-de-aula/lancar-nota-tcc', dados);
        }
    });
    
    
}

$(function() {
    $('#btFormCancelar').click(function() {
        $.get('/portal/sala-de-aula/cancelarliberacaotcc', {id_matricula: $('#id_matricula').val(), id_saladeaula: $('#id_saladeaula').val(), id_avaliacaoaluno: $('#id_avaliacaoaluno').val()}, 
            function(response) {
                modal('show', 'Justificativa de Cancelamento', response);
            });
    });
});

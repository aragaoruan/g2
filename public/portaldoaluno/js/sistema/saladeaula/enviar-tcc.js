/**
 * @author Rafael Bruno (RBD) <rafaelbruno.ti@gmail.com>
 * Bloco de scripts para a funcionalidade de envio de TCC do aluno
 */

$(document).ready(function () {

    $(function () {
        $('.jbutton').button();
        $(".numeric").numeric();

        $("#formTccNotaUpload").validate({
            rules: {
                st_tituloavaliacao: "required",
                arquivo_tcc: "required"
            }
        });

        $('#bt_salvartcc').click(function () {
            $('#formTccNotaUpload').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {

                    $('#progress_progress').attr('value', percentComplete).show();
                    $('#porcentagem_progress').html(percentComplete + '%').show();

                },
                success: function (mensageiro) {

                    if (null === mensageiro) {
                        $('#progress_progress').attr('value', '0').hide();
                        $('#porcentagem_progress').html('0%').hide();

                        JMensageiro.mostrar({
                            'tipo': JMensageiro.ERRO,
                            'mensagem': 'Ops, ocorreu um erro na requisição.'
                        }, null);
                        return false;
                    }


                    $('#progress_progress').attr('value', '100');
                    $('#porcentagem_progress').html('100%');


                    JMensageiro.mostrar(mensageiro, null);
                    //Atualizando o conteudo da pagina
                    window.location.reload();

                },
                error: function () {
                    $('#progress_progress').attr('value', '0').hide();
                    $('#porcentagem_progress').html('0%').hide();

                    JMensageiro.mostrar({
                        'tipo': JMensageiro.ERRO,
                        'mensagem': 'Ops, ocorreu um erro na requisiçãoo.'
                    }, null);
                },
                dataType: 'json',
                url: '/portal/avaliacao/upload-tcc',
                resetForm: false
            }).submit();
        });
    });

});
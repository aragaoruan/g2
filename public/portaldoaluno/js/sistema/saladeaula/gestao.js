var GestaoSala = function () {

    var self = this;

    this.abrirExtensao = function (id_saladeaula) {

        $("#dialogFrmExtensao").dialog({
            autoOpen: true,
            height: 150,
            width: 400,
            modal: false,
            title: 'Estender',
            buttons: {
                "Estender": function () {

                    var options = {
                        url: config.baseUrlModulo + "/sala-de-aula/extender",
                        dataType: 'json',
                        type: 'post',
                        success: function (JSMensageiro) {
                            Mensageiro.mostrar(JSMensageiro.mensagem[0], JSMensageiro.tipo);
                            if (JSMensageiro.tipo == 1) {
                                $("#dialogFrmExtensao").dialog("close");
                                $("#dialogFrmExtensao").dialog("destroy");
                                $("#dialogFrmExtensao").remove();
                                Portal.Ajax.loadHTML('conteudo', config.baseUrlModulo + '/sala-de-aula/gestao', { "id_saladeaula": id_saladeaula });
                            }
                        }
                    };
                    $('#frmExtensao').ajaxSubmit(options);

                },
                "Cancelar": function () {
                    $("#dialogFrmExtensao").dialog("close");
                }
            }, close: function (event, ui) {
            }

        });


    };

    this.abrirEncerramento = function (id_saladeaula, encerramento) {
        $('#item0').slideUp('slow', function () {
            $('#item0_id').slideDown('slow');
            var dados =
            {
                id_saladeaula: id_saladeaula,
                fla_encerramento: encerramento,
                ajax: false,
                json: true
            };
            Portal.Ajax.loadHTML('content_gestao', config.baseUrlModulo + '/sala-de-aula/encerrar', dados);
        });
    };

    this.abrirSincronizacao = function (id_saladeaula, st_codsistemacurso) {
        $('#item0').slideUp('slow', function () {
            $('#item0_id').slideDown('slow');
            var dados =
            {
                id_saladeaula: id_saladeaula,
                st_codsistemacurso: st_codsistemacurso,
                id_projetopedagogico: id_projetopedagogico,
                ajax: false,
                json: true
            };
            Portal.Ajax.loadHTML('content_gestao', config.baseUrlModulo + '/sala-de-aula/abrirsincronizar', dados);
        });
    };

    this.voltar = function (id_saladeaula) {
        $('#item0').slideUp('slow', function () {
            $('#item0_id').slideDown('slow');
            var dados =
            {
                id_saladeaula: id_saladeaula,
                ajax: false,
                json: true
            };
            Portal.Ajax.loadHTML('content_gestao', config.baseUrlModulo + '/sala-de-aula/gestao', dados);
        });
    };

    this.changeInicial = function (id_saladeaula) {
        var dados =
        {
            id_liberar: $("select#cb_dt_liberado option:selected").val(),
            id_saladeaula: id_saladeaula,
            fla_encerramento: $('#fla_encerramento').val(),
            ajax: true,
            json: true
        };
        self.carregarGridAlunos(dados);
    };

    this.changeLiberar = function (id_saladeaula) {
        $('#cb_dt_liberado').change(function () {
            var dados =
            {
                id_liberar: $("select#cb_dt_liberado option:selected").val(),
                id_saladeaula: id_saladeaula,
                fla_encerramento: $('#fla_encerramento').val(),
                ajax: true,
                json: true
            };
            self.carregarGridAlunos(dados);
        });
    };

    this.carregarGridAlunos = function (dados) {
        $('#idConteudoRepasse').slideUp('slow', function () {
            $('#idConteudoRepasse').slideDown('slow');
            Portal.Ajax.loadHTML('containerRepasse', config.baseUrlModulo + '/sala-de-aula/listaalunosencerramento', dados);
        });
    };


    this.marcartodos = function (name) {
        $("input[name='" + name + "[]']").each(function () {
            if (!this.checked) {
                $(this).attr("checked", "checked");
            } else {
                $(this).removeAttr("checked");
            }
        });
    };


    this.repassarprofessor = function (id_sala, id_tiposaladeaula, id_tipodisciplina) {
        var dados;
        if (id_tiposaladeaula == 1 || id_tipodisciplina == 2) {//SALA_PERMANENTE
            dados = $('#frmEcerramento').serialize();
        } else {
            dados = {
                id_liberar: $('#id_liberar').val(),
                id_sala: id_sala,
                id_tiposaladeaula: id_tiposaladeaula,
                id_tipodisciplina: id_tipodisciplina};

        }
        $.ajax({
            url: config.baseUrlModulo + '/sala-de-aula/repassarprofessor',
            dataType: 'json',
            async: false,
            data: dados,
            success: function (source) {
                JMensageiro.mostrar(source, null);
                if (source.tipo != 1) {
//					Mensageiro.mostrar(source.mensagem[0],3);
                } else if (source.tipo == 1) {
//					Mensageiro.mostrar(source.mensagem[0],1, 1500);
                    self.changeInicial(id_sala);
                }
            }
        });
    };


    this.sincronizar = function () {
        var dados;
        dados = $('#frmSincronizar').serialize();
        $.ajax({
            url: config.baseUrlModulo + '/sala-de-aula/sincronizar',
            dataType: 'json',
            async: false,
            type: 'post',
            data: dados,
            success: function (source) {
                if (source.tipo != 1) {
                    Mensageiro.mostrar(source.mensagem[0], 3);
                } else if (source.tipo == 1) {
                    Mensageiro.mostrar(source.mensagem[0], 1, 1500);
                    self.abrirSincronizacao($('#id_saladeaula').val(), $('#st_codsistemacurso').val());
                }
            }
        });
    };

    this.salvarLiberacao = function () {
        var dados;
        dados = $('#frmLiberar_tcc').serialize();
        $.ajax({
            url: config.baseUrlModulo + '/sala-de-aula/cancelarliberacaotcc',
            dataType: 'json',
            async: false,
            type: 'post',
            data: dados,
            success: function (source) {
                if (source.tipo != 1) {
                    //             Mensageiro.mostrar(source.mensagem[0],3);
                } else if (source.tipo == 1) {
                    //             Mensageiro.mostrar(source.mensagem[0],1, 1500);
                    /*
                     self.abrirSincronizacao($('#id_saladeaula').val(), $('#st_codsistemacurso').val());
                     */
                }
            }
        });
    };

    this.abrirSala = function (url) {
        window.location = url; // redirect
    }


};

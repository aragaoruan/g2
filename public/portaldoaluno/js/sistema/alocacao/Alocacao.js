var Alocacao = function(){

	var self = this;
	
	/**
	 * Alterando os módulos e aplicando o efeito de slideUp e slideDown
	 */
	this.changeModulo = function(){
		$('#cb_id_modulo').change(function () {
			if($("select#cb_id_modulo option:selected").attr("value") != 0){
				var str = $("select#cb_id_modulo option:selected").text() + " ";
				var dados = 
						{
							id_modulo: $("select#cb_id_modulo option:selected").val(),
							ajax: true,
							json: true
						};
				self.carregarDisciplinas(str,dados);
			}else{
				$('#idConteudoDisciplina').slideUp('slow');
            }
		}).trigger('change');
	};

	/**
	 * Carregando as disciplinas
	 */
	this.carregarDisciplinas = function(str, dados){
		$('#idConteudoDisciplina').slideUp('slow', function(){
			$("#lgdModulo").text(str);
			Portal.Ajax.loadJSON(config.baseUrlModulo + '/alocacao/disciplinas-por-modulo',dados,self.resultCarregarDisciplinas);
		});
	};
	
	/**
	 * Result de carregar Disciplinas que monta a grid de disciplinas pelo DOM
	 */
	this.resultCarregarDisciplinas = function(source){
		$("#containerDisciplinasAlocacao").html('');
		if(source.tipo == 1){
			$.each(source.mensagem, function(index, item){
				var divPrincipal = $("<div>").appendTo("#containerDisciplinasAlocacao");
				$('<hr style="clear:both;"/>').appendTo("#containerDisciplinasAlocacao");
				
				var divEsquerda = $("<div>").css("float", "left").appendTo(divPrincipal);
				var divDireita = $("<div>").css("float", "right").appendTo(divPrincipal);
				
				var imIcone = "Apto";
				var imgIcone = '';
				switch(item.st_status){
					case "Apto":
						imgIcone = 'blog_accept.png';
						
						var dadosSalaDeAula = {};
						dadosSalaDeAula.id_disciplina = item.id_disciplina;
						dadosSalaDeAula.ajax = true;
						dadosSalaDeAula.json = true;
						
						self.comboSalaDeAula(dadosSalaDeAula, divDireita);
						break;
					case "Inapto":
						imgIcone = 'blog_private.png';
						break;
					default:
						imgIcone = 'blog_compose.png';
						break;
				}
				var corSpan = '';
				if(item.st_status == 'Apto'){
					corSpan = '#008e00';
				}else if(item.st_status == 'Cursando'){
					corSpan = '#0033cc';
				}else{
					corSpan = '#f00';
				}
				
				var spanImagem = $("<span>").css("padding", "4px").appendTo(divEsquerda);
				var imagem = $('<img width="20" height="20" border="0" src="/layout/padrao/imagens/icones/' + imgIcone + '" />').appendTo(spanImagem);
				$('<span style="font-size: 14px">' + ' | ' + item.st_disciplina + ' | ' + '<span style="font-size: 13px; font-weight:bold; color: ' + 
						corSpan + ';">&nbsp;' + item.st_status + "&nbsp; </span>| </span>" ).appendTo(divEsquerda);
			});
			$('#idConteudoDisciplina').slideDown('slow');
		}else{
			$('#idConteudoDisciplina').slideUp('slow');
			Mensageiro.mostrar(source.mensagem[0],source.tipo);
		}
	};
	
	/**
	 * Buscando salas de aulas disponíveis
	 */
	this.comboSalaDeAula = function(dadosSalaDeAula, divDireita){
		if($("#disciplinasDisponiveis").text() != "0"){
			$.ajax({
				url: config.baseUrlModulo + '/alocacao/sala-de-aula',
				dataType: 'json',
				async: false,
				data: dadosSalaDeAula,
				success: function (rsp2){
					if(rsp2.tipo == 1){
						var selectSalaDeAula = $("<select>").attr("id", "cb_saladeaula" + dadosSalaDeAula.id_disciplina).attr("style", 'min-width: 300px; font-size:13px;');
						var optionSalaDeAula = $("<option>").attr("value", 0).text("Selecione...");
						optionSalaDeAula.appendTo(selectSalaDeAula);
						$.each(rsp2.mensagem, function(index2, item2){
							var optionSalaDeAula = $("<option>").attr("value", item2.id_saladeaula + "#" + item2.id_matriculadisciplina).text(item2.st_saladeaula);
							optionSalaDeAula.appendTo(selectSalaDeAula);
						});
						
						self.changeSalaDeAula(selectSalaDeAula);
						selectSalaDeAula.appendTo(divDireita);
					}
				}
			});
		}
	};
	
	/**
	 * Modificando as salas de aula
	 */
	this.changeSalaDeAula = function(selectSalaDeAula){
		selectSalaDeAula.change(function(){
			if($(this).attr('value') != 0){
				if(confirm("Deseja realmente alocar-se nesta sala de aula?")){
					var dadosAlocar = {};
					var dadosOption = $(this).attr('value').split("#");
					dadosAlocar.id_saladeaula = dadosOption[0];
					dadosAlocar.id_matriculadisciplina = dadosOption[1]; 
					dadosAlocar.ajax = true; 
					dadosAlocar.json = true; 
					self.alocar(dadosAlocar);
				}else{
					selectSalaDeAula[0].selectedIndex = 0;
				}
			}
		}).trigger('change');
	};
	
	/**
	 * Alocar em sala de aula
	 */
	this.alocar = function(dadosAlocar){
		$.ajax({
			url: config.baseUrlModulo + '/alocacao/alocar',
			dataType: 'json',
			async: false,
			data: dadosAlocar,
			success: function (rsp3){
				if(rsp3.tipo != 1){
					Mensageiro.mostrar(rsp3.mensagem[0],3);
				}else if(rsp3.tipo == 1){
					$('#disciplinasAlocadas').text(parseInt($('#disciplinasAlocadas').text()) + 1);
					$('#disciplinasDisponiveis').text(parseInt($('#disciplinasDisponiveis').text()) - 1);
					var str = $("select#cb_id_modulo option:selected").text() + " ";
					var dados = 
							{
								id_modulo: $("select#cb_id_modulo option:selected").val(),
								ajax: true,
								json: true
							};
					self.carregarDisciplinas(str,dados);
					Mensageiro.mostrar("Alocado com sucesso!",1);
				}
			}
		});
	};
};

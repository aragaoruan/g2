/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function modal(action, title, content, buttons) {

    var headerTitle = title ? title:'Title';
    var bodyContent = content ? content:'Content';
    var bodyButtons = buttons ? buttons: '';

    switch (action) {
        case 'show':
            $('.modal #title').html(headerTitle);
            $('.modal #content').html(bodyContent);
            $('.modal #buttons').html(bodyButtons);
            $('.modal').fadeIn(900);
            break;
    }
    
    
    $('.modal .close').click(function(){
       $('.modal').hide();
    });
    
    $('.modal #close').click(function(){
       $('.modal').hide(); 
    });
}
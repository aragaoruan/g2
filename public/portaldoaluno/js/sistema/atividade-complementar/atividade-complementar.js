$(document).ready(function () {
    if ($("#frmAtividade").length) {
        $("a[data-toggle='tooltip']").tooltip();


        $("#st_resumoatividade").on("keyup", function () {
            var currentLength = $(this).val().length;
            var maxLength = parseInt($(this).attr("maxlength"));
            $("#contador-resumo").text((maxLength - currentLength) + " / " + maxLength);
        });

        var validarCampoAnexo = function () {
            var fileName = $("input#anexo").val();
            if (fileName) {
                var _validFileExtensions = /(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i;
                if (!_validFileExtensions.exec(fileName)) {
                    $("#erro-anexo").show()
                        .html("Formato inválido do arquivo. Os formatos permitidos são: JPEG, PNG, DOC, DOCX e PDF.");
                    return false;
                }

                if ($("input#anexo")[0].files[0].size > 10485760) {
                    $("#erro-anexo").show()
                        .html("O tamanho do anexo excede os 10mb permitido pelo sistema.");
                    return false;
                }
            }
            $("#erro-anexo").hide();
            return true;
        }

        $("#anexo").on("change", validarCampoAnexo);


        $("#frmAtividade").validate({
            "messages": {
                "st_tituloatividade": {
                    "required": "Campo obrigatório não preenchido",
                    "maxlength": "A quantidade máxima é de 100 caractéres."
                },
                "id_tipoatividade": {
                    "required": "Campo obrigatório não preenchido"
                },
                "anexo": {
                    "required": "Campo obrigatório não preenchido"
                }
            },
            submitHandler: function (form) {
                if (validarCampoAnexo()) {
                    $(form).find("[type='submit']")
                        .attr({
                            disabled: "disabled",
                            value: "Enviando..."
                        })
                        .css({
                            cursor: "wait",
                            opacity: 0.6
                        });
                    form.submit();
                }
            }
        });

    }

    if ($("#atividade-complementar-index").length) {
        $(".btnShowResumo").on("click", function () {
            $("#modal-content").html($(this).data("content"));
        });

        $(".btn-remover").on("click", function () {
            var url = $(this).data("url");
            bootbox.confirm({
                message: "Deseja mesmo remover esta atividade?",
                size: "small",
                buttons: {
                    confirm: {
                        label: "Sim",
                        className: "btn-primary"
                    },
                    cancel: {
                        label: "Não"
                    }
                },
                callback: function (response) {
                    if (response) {
                        window.open(url, "_self");
                    }
                }
            });

        });
    }
});
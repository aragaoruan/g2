/**
 * Classe de Sele��o de Perfil
 */
var SelecaoPerfil = function () {

    var self = this;
    var _width = $('.grupo-perfil-entidade').width();
    var _heigth = $('.grupo-perfil-entidade').height();
    var ALUNO = 5;
    var ALUNO_INSTITUCIONAL = 6;
    var PROFESSOR = 1;

    /**
     * Listener de Click do Bot�o de Sele��o de Perfil Pedag�gico
     */
    this.btnPerilPedagogicoClickListener = function () {
        $('.btn-perfil-pedagogico').click(btnPerfilPedagogicoClickHandler);
    };

    /**
     * Handler de click do bot�o de perfil pedagogico
     * @private
     */
    var btnPerfilPedagogicoClickHandler = function (event) {
        var button = event.target;
        var arIds = event.target.id.split('-');
        var id_entidade = arIds[0];
        var id_perfilpedagogico = arIds[1];
        var id_perfilprofessor = arIds[2];
        if (id_perfilpedagogico == ALUNO || id_perfilpedagogico == ALUNO_INSTITUCIONAL) {
            $('.grupo-perfil-entidade').each(function (index, obj) {
                if ($(this).attr('id') != 'grupo-entidade-' + id_entidade) {
                    $(this).animate({width: 1, height: 1, opacity: 0}, 'slow', function () {
                        $(this).hide('slow');
                    });
                }
            });
            $('#grupo-entidade-' + id_entidade).find('.btn-perfil-pedagogico').fadeOut('fast');
            $('#grupo-entidade-' + id_entidade).animate({width: 950, height: '100%', left: 3}, 'slow', function () {
                $('.logo-selecao-perfil-entidade').animate({width: '180px', height: '180px'}, 'slow');
                $('#list-perf-' + id_entidade + '-' + id_perfilpedagogico).slideDown('slow').addClass('lista');
                $('#simulado-title').show();
                $('#simulado-link').show();
                $('.lb-nome-categoria').show();
                $('.btn-voltar').show();
            });
        } else/* if(id_perfilpedagogico == PROFESSOR)*/{
            var pars = {id_entidade: id_entidade, id_perfilpedagogico: id_perfilpedagogico, id_perfil: id_perfilprofessor, ajax: true, json: true};
            self.perfilSelecionado(pars);
        }
    };

    /**
     * Listener de Click do botao voltar
     */
    this.btnVoltarClickListener = function () {
        $('.btn-voltar').click(btnVoltarClickHandler);
    };

    /**
     * Handler de click do botao de voltar a sele��o de perfil
     * @private
     */
    var btnVoltarClickHandler = function (event) {
        $('.btn-voltar').hide();
        $('#simulado-title').hide();
        $('#simulado-link').hide();
        $('.lb-nome-categoria').hide();
        $('.list-perf').slideUp('slow');
        $('.grupo-perfil-entidade').animate({left: 0});
        $('.grupo-perfil-entidade').show('fast', function () {
            $('.logo-selecao-perfil-entidade').animate({width: '100px', height: '100px'}, 'slow');
        });
        $('.grupo-perfil-entidade').animate({width: 430, height: '100%', opacity: 100}, 'slow', function () {
            $('.btn-perfil-pedagogico').show();
        });
    };

    /**
     * Listener de Click de sele��o do perfil de aluno
     */
    this.perfilAlunoClickListener = function () {
        $('.perfil-aluno').click(function () {
            if ($(this).attr('id') === "simulado-link") {
                $('#simulado-open').submit();
                //window.open($(this).data('url'));
            } else {
                var arIds = $(this).attr('id').split('-');
                var id_perfil = arIds[0];
                var id_perfilpedagogico = arIds[1];
                var id_projetopedagogico = arIds[2];
                var id_matricula = arIds[3];
                var id_entidade = arIds[4];
                var pars = {id_entidade: id_entidade, id_perfil: id_perfil, id_perfilpedagogico: id_perfilpedagogico, id_projetopedagogico: id_projetopedagogico, id_matricula: id_matricula, ajax: true, json: true };
                self.perfilSelecionado(pars);
            }
        });
    };

    /**
     * M�todo de sele��o de perfil
     * @private
     */
    this.perfilSelecionado = function (pars) {
        Portal.Ajax.loadJSON(config.baseUrlModulo + "/login/selecionaperfilacesso", pars, function (source, xhr) {
            if (source.tipo == 1) {
                location.replace(config.baseUrlModulo + source.codigo);
            } else {
                Mensageiro.mostrar(source.mensagem[0], source.tipo);
            }
        });
    };
};
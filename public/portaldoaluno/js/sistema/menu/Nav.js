/**
 * Classe de Controle de Menu (Nav)
 */
var Nav = function(){
	var self = this;
	
	/**
	 * Método que monta as funcionalidades do menu
	 * @public
	 */
	this.montaFuncionalidadesMenu = function(){
		Nav.removeFuncionalidadesDinamicasMenu();
		var pars = {
				ajax: true,
				json: true
		};
        $.ajax({

        })
		Portal.Ajax.loadJSON(config.baseUrlModulo+'/menu/nav',pars,resultRetornarFuncionalidadesMenu);
	};
	
	/**
	 * Listener de click da funcionalidade do menu
	 */
	this.menuNavClickListener = function(){
		$('.link-menu-nav').click(function(event){
			event.preventDefault();
			var redirect = false;
			redirect = $(this).hasClass('change-redirect')
			ViewController.viewChange(event.target.id,{ajax:true},redirect);
		});

		$('.link-menu-nav-target-blank').click(function(event){
			event.preventDefault();
			window.open(event.target.id, '_blank');
		});
	};
	
	/**
	 * Resutl que retorna as funcionalidades do Menu
	 * @private 
	 */
	var resultRetornarFuncionalidadesMenu = function(source, xhr){
		if(source.tipo == 1){
			var html = funcionalidadesDinamicasMenuRecurssivo(source.mensagem.dados,source.mensagem.layout);
			$('#nav').prepend(html);
		}else{
			Mensageiro.mostrar(source.mensagem[0],source.tipo);
		}
	};
	
	/**
	 * Método que monta as funcionalidades do menu recurssivamente
	 * @private 
	 */
	var funcionalidadesDinamicasMenuRecurssivo = function(dados,layout){
		if($.isArray(dados)){
			var html = '';
			$.each(dados,function(key, itemMenu){
				html += "<li id='id_funcionalidadenav-"+itemMenu.id+"' class='funcionalidade-nav'>";
			    html += "<span class='icone_menu' style='padding:4px;'>";
			    html += "<img src='"+config.baseUrl+"/layout/"+layout+itemMenu.img+"' border=0 width='20' height='20' />";
			    html += "</span>";
			    var texto = itemMenu.texto;
			    
			    if(itemMenu.link && itemMenu.link != "#" ){
			    	if(itemMenu.target === '_blank'){
			    		html += "<span id='"+config.baseUrlModulo + itemMenu.link+"' class='link-menu-nav-target-blank change-"+itemMenu.type+"' style='padding-left:33px; cursor:pointer;'>"+texto+"</span>";
			    	} else {
			    		html += "<span id='"+config.baseUrlModulo + itemMenu.link+"' class='link-menu-nav change-"+itemMenu.type+"' style='padding-left:33px; cursor:pointer;'>"+texto+"</span>";
			    	}
				}else{
					html += "<span class='dir' style='padding-left:33px'>"+texto+"</span>";
				}
			    
				if($.isArray(itemMenu.filhos)){
					html += "<ul>";
					html += funcionalidadesDinamicasMenuRecurssivo(itemMenu.filhos, layout);
					html += "</ul>";
				}
			    html += "</li>";
			});
			return html;
		}
	};
	
	
};

/**
 * Método que recarrega o menu
 * @static
 */
Nav.refreshMenu = function(){
	var nav = new Nav();
	nav.montaFuncionalidadesMenu();
	nav.menuNavClickListener();
}

/**
 * Método estatico que remove as funcionalidades dinamicas do menu
 * @static
 */
Nav.removeFuncionalidadesDinamicasMenu = function(){
	$('.funcionalidade-nav').remove();
}

/**
 * Método estatico que remove determinada funcionalidade do menu
 * @param (int) id_funcionalidade
 * @static
 */
Nav.removeFuncionalidadeMenu = function(id_funcionalidade){
	$('#id_funcionalidadenav-'+id_funcionalidade).remove();
}
//Temporario - aham!
var self = this;
$(document).ready(function () {
    var GradeProfessor = {

        capturaGrids: function () {
            /*$(".gridProfessor").each(function(indice, tabela){
             GradeProfessor.dadosGrid($(tabela).attr("id"));
             });*/
        },


        /**
         * Alterando os módulos e aplicando o efeito de slideUp e slideDown
         */
        dadosGrid: function (idGrid) {
            var dadosGrid = eval(($('#' + idGrid + '-source').attr('value')));
            $("#" + idGrid).jqGrid({
                datatype: "local",
                height: "100%",
                width: "100%",
                colNames: ['Sala', 'Abertura', 'Encerramento', ' '],
                colModel: [
                    {name: 'st_saladeaula', index: 'st_saladeaula', width: 315},
                    {name: 'st_abertura', index: 'st_abertura', width: 110},
                    {name: 'st_encerramento', index: 'st_encerramento', width: 110},
                    {name: 'lb_sala', index: 'lb_sala', width: 100, align: "center"}
                ],
                caption: $('#' + idGrid + '-caption').attr('value')
            }).setGridState("hidden");

            for (var i = 0; i <= dadosGrid.length; i++) {
                $("#" + idGrid).jqGrid('addRowData', i + 1, dadosGrid[i]);
            }
        },
        /**
         * Listener de click para entrar na sala de aula
         */
        entrarListener: function () {
            $('.salaSelected').click(function () {
                var arParams = $(this).attr("id").split('-');
                var id_saladeaula = arParams[0];
                var id_disciplina = arParams[1];
                $('#frm_acesso_turma_id_disciplina').attr("value", id_disciplina);
                $('#id_saladeaula').attr("value", id_saladeaula);
                $('#frmAcessoSala').submit();
            });


            $('.salaGestao').click(function () {
                var arParams = $(this).attr("id").split('-');
                var dados = {
                    ajax: true,
                    json: true
                };
                dados.courseid = arParams[1];
                dados.id_saladeaula = arParams[0];

                Portal.Ajax.loadHTML('conteudo', config.baseUrlModulo + '/sala-de-aula/gestao', { "id_saladeaula": arParams[0], "id_disciplina": arParams[1] });
            });

        }
    };

    GradeProfessor.capturaGrids();
    GradeProfessor.entrarListener();


    /*
     * Configurações dos painéis....
     *
     * */
// panels
    $('.dragbox').each(function () {
        $(this).hover(function () {
            $(this).find('h2').addClass('collapse');
        }, function () {
            $(this).find('h2').removeClass('collapse');
        })
            .find('h2').hover(function () {
                $(this).find('.configure').css('visibility', 'visible');
            }, function () {
                $(this).find('.configure').css('visibility', 'visible');
            })
            .click(function () {

                $(this).siblings('.dragbox-content').toggle();

                $('#id_disponibilidade').click(function () {
                    $('.dragbox-content').attr('display', $('.dragbox-content').attr('display'));//display: block;
                    return false;
                });

            })
            .end()
            .find('.configure').css('visibility', 'hidden');
    });

    $('.column').sortable({
        connectWith: '.column',
        handle: 'h2',
        cursor: 'move',
        placeholder: 'placeholder',
        forcePlaceholderSize: true,
        opacity: 0.4,
        stop: function (event, ui) {
            $(ui.item).find('h2').click();
            var sortorder = '';
            $('.column').each(function () {
                var itemorder = $(this).sortable('toArray');
                var columnId = $(this).attr('id');
                if (itemorder.length > 0) {
                    for (x = 0; x < itemorder.length; x++) {
                        sortorder += columnId + '[]=' + itemorder[x] + '&';
                    }
                }
            });
        }
    });

    $('#id_disponibilidade_professor').change(function () {
        JMensageiro.redirecionar({'tipo': 'replace', 'url': config.baseUrlModulo + '/Index/professor?id_status=' + $(this).val()})
    });
    $('#id_disponibilidade_coordenador').change(function () {
        JMensageiro.redirecionar({'tipo': 'replace', 'url': config.baseUrlModulo + '/Index/coordenador?id_status=' + $(this).val()})
    });


    $('.HeaderButton').addClass('ui-state-hover');

    $('.HeaderButton').hover(
        function () {
            $(this).addClass('ui-state-hover');
        },
        function () {
            $(this).addClass('ui-state-hover');
        }

    );

    /**
     * Ativando hover nos botões
     */
    $('#dialog_link, ul.icons li').hover(
        function () {
            $(this).addClass('ui-state-hover');
        }
        //function() { $(this).removeClass('ui-state-hover'); }
    );

    $('.btProjetoPedagogico').click(function () {

        if ($('#ppSessao').val() == 'Limpar') {
            var url = config.baseUrlModulo + '/Index/limpaprojetopesquisa';
        } else {
            var url = config.baseUrlModulo + '/Index/setaprojetopesquisa';
        }
        $.ajax({

            url: url,
            dataType: 'json',
            async: false,
            data: {id_projetopedagogico: $(this).attr('id')},
            success: function (source) {
                if (source.retorno == "true") {
                    JMensageiro.redirecionar({'tipo': 'replace', 'url': config.baseUrlModulo + '/Index/' + source.metodo + '?id_status=' + $('select#id_disponibilidade_' + source.metodo + ' option:selected').val()})
                }
            }
        });
    });


    $('.ui-icon-circle-triangle-s').click(
        function () {
            $(this).removeClass('ui-icon-circle-triangle-s');
            $(this).addClass('ui-icon-circle-triangle-n');
            classboxes = $(this).attr('id')
//            $('.'+classboxes).each(function(){
//                $(this).show();
//            });
            console.log($('#st_perfil').val());
            if ($('#st_perfil').val() == 'Professor') {
                var perfil = 'professor';
                var url = config.baseUrlModulo + '/Index/pesquisasalasprofessorrprojeto';
            } else {
                var perfil = 'coordenador';
                var url = config.baseUrlModulo + '/Index/pesquisasalascoordenadorprojeto';
            }
            var id = $(this).attr('id').split('-');
            $.ajax({
                url: url,
                dataType: 'html',
                async: true,
                data: {
                    id_projetopedagogico: id[1],
                    id_status: $('select#id_disponibilidade_' + perfil + ' option:selected').val()
                },
                success: function (source) {
                    $('#salas-' + id[1]).html('teste');
                    $('#salas-' + id[1]).html(source);
                    $('#salas-' + id[1]).css('display', '');
                }
            });
        });
});
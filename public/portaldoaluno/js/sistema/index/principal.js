//Mudar nome da classe por algum melhor e mudar no index/principal.phtml

var SelecaoSala = function(){
	
	/**
	 * Render
	 */
	this.render = function(id_usuario){
		$('#accordion').accordion({autoHeight: false,navigation: true});
		$('.dragbox').each(function(){
			$(this).hover(function(){
				$(this).find('h2').addClass('collapse');
			}, function(){
				$(this).find('h2').removeClass('collapse');
			})
			.find('h2').hover(function(){
				$(this).find('.configure').css('visibility', 'visible');
			}, function(){
				$(this).find('.configure').css('visibility', 'hidden');
			})
			.click(function(){
				$(this).siblings('.dragbox-content').toggle();
				setCookie($(this).siblings('.dragbox-content').attr('id') + '_' + id_usuario,$(this).siblings('.dragbox-content').css('display'),365);
			})
			.end()
			.find('.configure').css('visibility', 'hidden');
		});
		
		$('.column').sortable({
			connectWith: '.column',
			handle: 'h2',
			cursor: 'move',
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			opacity: 0.4,
			stop: function(event, ui){
				$(ui.item).find('h2').click();
				var sortorder='';
				$('.column').each(function(){
					var itemorder = $(this).sortable('toArray');
					var columnId  = $(this).attr('id');
					if(itemorder.length>0) {
						for(x=0;x<itemorder.length;x++) {
							sortorder+=columnId+'[]='+itemorder[x]+'&';
						}
					}
				});
				setCookie('SortOrder_' + id_usuario,sortorder,365);
			}
		})
		.disableSelection();
		this.acessoSalaDeAulaListener();
	}
	
	/**
	 * Metodo que ativa o listener de click do acesso a sala de aula
	 */
	this.acessoSalaDeAulaListener = function(){
		$('.acessosaladeaula').click(function(event){
			var arValues = $(this).attr("id").split('#');
			var id_disciplina = arValues[0];
			var bl_abertura = arValues[1];
			var bl_encerramento = arValues[2];
                        var id_categoriasala = arValues[3];
			if(bl_abertura == false){
				Mensageiro.mostrar("A Sala de Aula ainda não esta Aberta.",2);
				return;
			}
			if(bl_encerramento == false){
				Mensageiro.mostrar("A Sala de Aula já esta encerrada.",2);
				return;
			}
			$("#frm_acesso_turma_id_disciplina").attr("value",id_disciplina);
			$("#frm_acesso_turma_id_categoriasala").attr("value",id_categoriasala);
			$('#frmAcessoTurma').submit();
		});
	}
};
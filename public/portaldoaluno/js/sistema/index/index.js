$(function() {

    var params = new Array(null, '');

    // validate signup form on keyup and submit
    $("#frmLogin").validate({
        rules: {
            nomusuario: "required",
            dessenha: "required"
        },
        submitHandler: function() {
            $('#frmLogin button[type=submit]', this).attr('disabled', 'disabled');

            /*
             * Verifica se na url contem a string url-direta, o que demonstra
             * que a url repassada provem de uma ferramnta para acesso direto a
             * uma determinada funcionalidade (RBD)
             */
            if (window.location.href.search('url-direta') > 0) {
                params = window.location.href.split(config.baseUrlModulo);
            }

            var options = {
                url: config.baseUrlModulo + "/login/validar" + params[1],
                dataType: 'html',
                type: 'post',
                success: function(JsonData) {
                    var obj = jQuery.parseJSON(JsonData);
                    if (obj.tipo === 1) {
                        $('#mensagem').hide();
                        
                        Mensageiro.mostrar(obj.mensagem[0], obj.tipo);
                        location.replace(config.baseUrlModulo + "/" + obj.codigo);

                    } else {
                        Mensageiro.mostrar(obj.mensagem[0], obj.tipo);
                        $('#frmLogin button[type=submit]', this).attr('disabled', '');
                    }
                }
            };

            $('#frmLogin').ajaxSubmit(options);

        }
    });
});
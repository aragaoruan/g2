<?php

zray_disable();
define('APPLICATION_ROOT', dirname(dirname(__FILE__)));
defined('APPLICATION_SYS')
|| define('APPLICATION_SYS', 'PORTAL_DO_ALUNO');
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));


defined('APPLICATION_REAL_PATH')
|| define('APPLICATION_REAL_PATH', realpath(dirname(__FILE__)));

defined('URL_APP')
|| define('URL_APP', $_SERVER['HTTP_HOST']);


// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/configs'),
    realpath(APPLICATION_PATH . '/ro'),
    realpath(APPLICATION_PATH . '/models'),
    realpath(APPLICATION_PATH . '/apps/portal/controllers'),
    realpath(APPLICATION_PATH . '/apps/portal/models'),
    realpath(APPLICATION_PATH . '/apps/portal/models/bo'),
    realpath(APPLICATION_PATH . '/models/to'),
    realpath(APPLICATION_PATH . '/models/bo'),
    realpath(APPLICATION_PATH . '/models/dao'),
    realpath(APPLICATION_PATH . '/models/orm'),
    realpath(APPLICATION_PATH . '/webservices/clients/sa'),
    realpath(APPLICATION_PATH . '/webservices/clients/actor'),
    realpath(APPLICATION_PATH . '/webservices/clients/moodle'),
    realpath(APPLICATION_PATH . '/webservices/clients'),
    realpath(APPLICATION_PATH . '/apps/loja/controllers'),
    realpath(APPLICATION_PATH . '/apps/loja/models'),
    realpath(APPLICATION_PATH . '/apps/loja/models/bo'),
    realpath(APPLICATION_PATH . '/webservices'),
    realpath(APPLICATION_PATH . '/webservices/clients'),
    realpath(APPLICATION_PATH . '/webservices/clients/sa'),
    realpath(APPLICATION_PATH . '/webservices/clients/moodle'),
    realpath(APPLICATION_PATH . '/webservices/clients/pagamento'),
    realpath(APPLICATION_PATH . '/webservices/clients/pagamento/Braspag/to'),
    realpath(APPLICATION_PATH . '/webservices/clients/Maximize'),
    get_include_path(),
)));


include('Zend/Loader/Autoloader.php');

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);


// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : Ead1_Ambiente::getAmbiente()));

/** Zend_Application */
require_once 'Zend/Application.php';

//TODO REFATORAR E TRATAR OS ERROS PARA O FLEX
try {
    // Create application, bootstrap, and run
    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application.ini'
    );

    /**
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * Modificar definicoes do application.ini especificas para o portal
     */
    $options = $application->getOptions();
    $options['resources']['frontController']['defaultModule'] = 'portal';
    $options['resources']['frontController']['prefixDefaultModule'] = TRUE;
    $options['resources']['layout']['layoutPath'] = APPLICATION_PATH . '/apps/portal/views/layouts';
    $application->setOptions($options);

    /**
     * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
     * Verificar se o sistema esta e manutencao (application.ini, var maintenance)
     */
    $inMaintenance = $options['maintenance'];
    if ($inMaintenance) {
        include(APPLICATION_PATH . '/views/scripts/index/manutencao.phtml');
        exit;
    }

    $application->bootstrap()->run();

} catch (Exception $exception) {

    echo 'Erro de aplicação. Erro: ' . $exception->getMessage();

}



function in_array (needle, haystack, argStrict) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true
    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '',
        strict = !! argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}

/*
Fun��o para gravar Cookie no cliente
*/

if(typeof window.setCookie != 'function') {

	function setCookie(c_name,value,exdays) {
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}

}

if(typeof window.carregaSelect != 'function') {
	function carregaSelect(idselect, iddestino, url, callback){

			var queryString = $(idselect).attr('id').toString() + '='+  $(idselect).val() ;
			var municipio = null;
			if($(idselect).val()!=''){
				$.post(url, queryString,
				   function(data){
					if(data.tipo == 1){
						
						$(iddestino).removeOption(/./); 
						if(data.mensagem.length>0){
							selecione = {"":"Selecione..."};
							$(iddestino).addOption(selecione, false); 
							for(var i=0; i<data.mensagem.length;i++) {
								
								$(iddestino).append('<option value="'+data.mensagem[i][0] +'">'+ data.mensagem[i][1]+'</option>');
								
							} //for (var k in data) {
						} else {
							data = {"":"Nenhum registro encontrado."};
							$(iddestino).addOption(data, false).focus(); 
						}
						
						
						 if(typeof callback != 'undefined'){
							 return callback($(iddestino));
						 }
						
						
					}
					
				   }, "json");
			} else {
				$(iddestino).removeOption(/./); 
			}//if($('#codufnaturalidade').val()>0){
		return true;
	}
}


EAD1 = {};
EAD1.Ajax	= {};
EAD1.Ajax.loadAJAX	= function( options ){
	$.ajax( options );
};

/**
 * Gera uma requisi��o ajax com os dados retornados em JSON
 * url  		- endere�o de destino
 * data 		- objeto contendo os par�metros a serem enviados
 * onSuccess	- fun��o de callback a ser utilizada na resposta do servidor
 * option		- OPCIONAL para sobreescrever as op��es do ajax. Ver fun��o jQuery.ajax
 * 
 * @param string url  	- url de destino  
 * @param object data 	- par�metros a serem enviados
 * @param function onSuccess	  	- fun��o a ser executada ap�s o retorno do servidor 
 * @param object option 		- par�metros adicionais a serem passados para o ajax
 */
EAD1.Ajax.loadJSON	= function(url, data, onSuccess, option ){
	
	if(data){
		data = data + '&isJson=true';
	} else {
		data = 'isJson=true';
	}
	
	var options	= {
			dataType	: 'json',
			async		: false,
			url			: url,
			data		: data,
			
			beforeSend: function(){
				$("#carregando").show();
		    },
		    complete: function(){
			    $("#carregando").show();
		    },
			
			success		: onSuccess
	};
	
	jQuery.extend( options, option );
	options.dataType	= 'json';
	
	this.loadAJAX( options );
};


/**
 * Gera uma requisi��o ajax e insere o retorno da fun��o em um container
 * idContainer	- id do elemento aonde ser� inserido o html
 * url			- endere�o de onde ser� buscado o html
 * data			- objeto contendo os par�mentros a serem enviados
 * 
 * @param string idContainer
 * @param string url
 * @param data
 */
EAD1.Ajax.loadHTML	= function(idContainer, url, data, method){
	
	if(typeof(method) =="undefined") { //test do parametro opcional
		method = 'post';
	}
	
	if(data){
		data = data + '&isAjax=true';
	} else {
		data = 'isAjax=true';
	}

	var options	= {
			dataType	: 'html',
			async		: false,
			url			: url,
			data		: data,
			type 		: method,
			beforeSend: function(){
				$("#carregando").show();
		    },
		    complete: function(){
			    $("#carregando").show();
		    },
			success		: function( response ) {
				jQuery( '#'+ idContainer ).html( response );
			}
	};
	
	this.loadAJAX( options );
};


/**
 * Classe de Notificações de Mensagens
 */
var Mensageiro = {};
	Mensageiro.ERRO 	= 0;
	Mensageiro.SUCESSO 	= 1;
	Mensageiro.AVISO 	= 2;
/**
 * Método Estatico que mostra uma Determinada Mensagem
 */
Mensageiro.mostrar = function(mensageiro, tipo){
	
	var titulo 		 = 'Erro';
	var stateclass   = 'ui-state-error';
	tipo 			 = (typeof tipo == 'undefined' || tipo == null) ? 'ALERTA' : tipo;
	
	switch(mensageiro.tipo){
		case Mensageiro.SUCESSO: 	titulo 	= 'Sucesso'; 	stateclass   = 'ui-state-highlight';  break;
		case Mensageiro.AVISO: 		titulo 	= 'Aviso'; 		stateclass   = null; break;
	}
	
	if(tipo == 'ALERTA'){
		$('body').append('<div id=\'mensageiro-entrega\'>'+ mensageiro.mensagem +'</div>');
		$( "#mensageiro-entrega" ).dialog({
				modal: true,
				title: titulo,
				draggable: false,
				close: function(event, ui) { 
					
					$(this).dialog("destroy");
					$("#mensageiro-entrega").remove();
					
					
				},
				buttons: {
					Ok: function() {
						$(this).dialog("destroy");
						$("#mensageiro-entrega").remove();
						if(typeof mensageiro.redirecionar != 'undefined'){
							Mensageiro.redirecionar(mensageiro.redirecionar);
						}
					}
				}
		}).prev().addClass(stateclass);
	}
		
}


/**
 * Método que executa o redirecionamento
 * @param propriedades - Propriedades pro Login
 * @param redirecionar - Redirecionar do Mensageiro que deu origem à requisição, deve conter o redirecionar
 */
Mensageiro.redirecionar = function(propriedades){
	/*
	Tipos:
		1 - replace: Substitui a pagina.
		2 - normal: abre uma pagina sobre a atual
	*/
	
	propriedades.tempo = propriedades.tempo ? propriedades.tempo*1000 : 0;
	propriedades.destino = propriedades.destino ? propriedades.destino : '_self';
	
	switch(propriedades.tipo){
		case 'replace': 
		
			var direcionando = function() {
					location.replace(propriedades.url);
				}
			//setTimeout(direcionando,propriedades.tempo);
			
		break;
		case 'ajax': 
			
			propriedades.parametros = propriedades.parametros ? propriedades.parametros : '';
			
			var direcionando = function() {	
					$.post(propriedades.url, propriedades.parametros,
					  function( data ) {
						  $(propriedades.destino).html(data);
					  }
					);
				};
		
		break;
		case 'close': 
			window.close();
			break;
		default: var direcionando = function() { 
			window.open(propriedades.url, propriedades.destino);		
		};
		break;
	}
	
	if(propriedades.tempo){
		setTimeout(direcionando,propriedades.tempo);
	} else {
		direcionando();
	};
	
}


/**
 * Método que abre uma janela de login
 * @param propriedades - Propriedades pro Login
 * @param redirecionar - Redirecionar do Mensageiro que deu origem à requisição, deve conter o redirecionar
 */
Mensageiro.janelalogin = function(propriedades, redirecionar){
	
	
	if($('#mensageiro-janelalogin').length == 0){
		
		$('body').append('<div id="mensageiro-janelalogin" title="Efetuar Login">'
				+'<form name="form-mensageiro-janelalogin" id="form-mensageiro-janelalogin">'
				+'<p style="text-align: right;"><label for="st_login" class="form">Usuário</label>'
				+'&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="st_login" id="st_login" class="text ui-widget-content ui-corner-all" val="" /></p>'
				+'<p style="text-align: right;"><label for="st_senha" class="form">Senha</label>'
				+'&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="st_senha" id="st_senha" value="" class="text ui-widget-content ui-corner-all" val="" /></p>'
				+'</form>'
				+'</div>');
		
	}
	
	$( "#mensageiro-janelalogin").dialog({
			modal: true,
			title: 'Login',
			draggable: false,
			autoOpen: true,
			close: function(event, ui) { 
				
				
			},
			buttons: {
				 'Recuperar Senha': function() {
					 if(typeof propriedades.urlesquecisenha != 'undefined'){
						 Mensageiro.janelaesquecisenha({ url: propriedades.urlesquecisenha }, redirecionar);
					 } else {
						 alert('Url para recuperar a senha não informada!');
					 }
				 }
				
				,'Acessar': function() {
					
					if(!$('#form-mensageiro-janelalogin #st_login').val()){
						var mensageiro = {
								tipo : 2,
								mensagem : 'O nome de usuário é obrigatório!',
						};
						
						Mensageiro.mostrar(mensageiro,'ALERTA');
						return false;
					}
					
					if(!$('#form-mensageiro-janelalogin #st_senha').val()){
						var mensageiro = {
								tipo : 2,
								mensagem : 'A senha é obrigatória!',
						};
						
						Mensageiro.mostrar(mensageiro,'ALERTA');
						return false;
					}
					
					
					$("#mensageiro-janelalogin").dialog("close");
					
					$.post(propriedades.url, $('#form-mensageiro-janelalogin').serialize(),
							 function(jsonMensageiro){
								   if(jsonMensageiro.tipo==Mensageiro.SUCESSO){
									   
									   jsonMensageiro.redirecionar = redirecionar;
									   
									    $("#mensageiro-janelalogin").dialog("destroy");
										$("#mensageiro-janelalogin").remove();
								   } else {
									   
									   $('#form-mensageiro-janelalogin #st_senha').val('');
									   Mensageiro.janelalogin({ url: propriedades.url }, redirecionar);
									   $("#mensageiro-janelalogin").dialog("show");
								   }
								   Mensageiro.mostrar(jsonMensageiro, null);
								   
							 }, "json");
					
				}
			}
	});
	
}



/**
 * Método que abre uma janela para recuperar a senha
 * @param propriedades - Propriedades pro Login
 * @param redirecionar - Redirecionar do Mensageiro que deu origem à requisição, deve conter o redirecionar
 */
Mensageiro.janelaesquecisenha = function(propriedades, redirecionar){
	
	
	if($('#mensageiro-janelaesquecisenha').length == 0){
		
		$('body').append('<div id="mensageiro-janelaesquecisenha" title="Efetuar Login">'
				+'<form name="form-mensageiro-janelaesquecisenha" id="form-mensageiro-janelaesquecisenha">'
				+'<p style="text-align: right;"><label for="st_cpf" class="form">CPF</label>'
				+'&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="st_cpf" id="st_cpf" class="text ui-widget-content ui-corner-all" val="" /></p>'
				+'<p style="text-align: right;"><label for="st_email" class="form">E-mail</label>'
				+'&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="st_email" id="st_email" value="" class="text ui-widget-content ui-corner-all" val="" /></p>'
				+'</form>'
				+'</div>');
		
	}
	
	$( "#mensageiro-janelaesquecisenha").dialog({
		modal: true,
		title: 'Recuperar Senha',
		draggable: false,
		autoOpen: true,
		close: function(event, ui) { 
			
			
		},
		buttons: {
			'Recuperar': function() {
				
				if(!$('#form-mensageiro-janelaesquecisenha #st_cpf').val() || !$('#form-mensageiro-janelaesquecisenha #st_email').val()){
					var mensageiro = {
							tipo : 2,
							mensagem : 'CPF e E-mail obrigatórios!',
					};
					Mensageiro.mostrar(mensageiro,'ALERTA');
					return false;
				}
				
				$("#mensageiro-janelaesquecisenha").dialog("close");
				
				$.post(propriedades.url, $('#form-mensageiro-janelaesquecisenha').serialize(),
						function(jsonMensageiro){
					if(jsonMensageiro.tipo==Mensageiro.SUCESSO){
						$("#mensageiro-janelaesquecisenha").dialog("destroy");
						$("#mensageiro-janelaesquecisenha").remove();
					} else {
						$("#mensageiro-janelaesquecisenha").dialog("show");
					}
					Mensageiro.mostrar(jsonMensageiro, null);
					
				}, "json");
				
			}
		}
	});
	
}



/*
 * Instâncias das libs do GULP
 *
 * */
var gulp = require('gulp'),
    minifyCSS = require('gulp-minify-css'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify'),
    uglifyjs = require('gulp-uglifyjs');

/*
 * Definição dos nomes finais dos arquivos compressados
 * */
var build_name_css = 'all_styles.min.css',
    name_css = 'all_styles.css',
    build_name_js = 'all_scripts.min.js';

/*
 * Variáveis para auxiliar as definições de pastas do PORTAL
 *
 * */
var folderSrcPortal = './public/portaldoaluno';
var folderDestPortalBuild = './public/portaldoaluno/build';


/*
 * Variáveis para auxiliar as definições de pastas do G2
 *
 * */
var folderSrcG2 = './public/gestor2';
var folderDestG2Build = './public/gestor2/build';

/*
 * Tarefas para concatenar o JS e CSS do G2
 *
 * */

gulp.task('minifyJsG2', function () {
    return gulp.src(folderSrcG2 + '/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(folderDestG2Build + '/js/min'))
});
gulp.task('concatJsG2', ['minifyJsG2'], function () {
    return gulp.src(
        [
            folderDestG2Build + '/bower_components/jquery/dist/jquery.min.js',
            folderDestG2Build + '/bower_components/bootstrap/docs/assets/js/bootstrap.min.js',
            folderDestG2Build + '/bower_components/bootbox/bootbox.js',
            folderDestG2Build + '/bower_components/underscore/underscore-min.js',
            folderDestG2Build + '/bower_components/backbone/backbone-min.js',
            folderDestG2Build + '/bower_components/marionette/lib/backbone.marionette.min.js',
            folderDestG2Build + '/bower_components/backbone-modelbinder/Backbone.ModelBinder.min.js',
            folderDestG2Build + '/bower_components/backbone-modelbinder/Backbone.CollectionBinder.min.js',
            folderDestG2Build + '/bower_components/backgrid/lib/backgrid.min.js',
            folderDestG2Build + '/bower_components/backbone-forms/distribution/backbone-forms.min.js',
            folderDestG2Build + '/bower_components/backbone-forms/src/editors/extra/list.js',
            folderDestG2Build + '/bower_components/lunr.js/lunr.min.js',
            folderDestG2Build + '/bower_components/backgrid-filter/backgrid-filter.min.js',
            folderDestG2Build + '/bower_components/backgrid-paginator/backgrid-paginator.min.js',
            folderDestG2Build + '/bower_components/backgrid-select-all/backgrid-select-all.min.js',
            folderDestG2Build + '/bower_components/backbone-pageable/lib/backbone-pageable.min.js',
            folderDestG2Build + '/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            folderDestG2Build + '/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js',
            folderDestG2Build + '/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
            folderDestG2Build + '/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js',
            folderDestG2Build + '/bower_components/jquery-cookie/jquery.cookie.js',
            folderDestG2Build + '/bower_components/jquery.hotkeys/jquery.hotkeys.js',
            folderDestG2Build + '/bower_components/jquery-numeric/dist/jquery-numeric.js',
            folderDestG2Build + '/bower_components/backbone-autocomplete/src/backbone.autocomplete-min.js',
            folderDestG2Build + '/bower_components/pnotify/jquery.pnotify.min.js',
            folderDestG2Build + '/bower_components/select2/select2.min.js',
            folderDestG2Build + '/bower_components/tinymce/tinymce.min.js',
            folderDestG2Build + '/bower_components/tinymce/jquery.tinymce.min.js',
            folderDestG2Build + '/bower_components/colpick-jQuery-Color-Picker/js/colpick.js',

            folderDestG2Build + '/js/jquery.jstree.js',
            folderDestG2Build + '/js/CollectionView.js',
            folderDestG2Build + '/js/app.js',
            folderDestG2Build + '/js/backbone/autocomplete.js',
            folderDestG2Build + '/js/backbone/views/Util/modal_basica.js',
            folderDestG2Build + '/js/comboboxview.js'
        ]
    )
        .pipe(concat(build_name_js))
        .pipe(gulp.dest(folderDestG2Build + '/js'))
});

gulp.task('minifyCssG2', ['concatCssG2'], function () {
    return gulp.src([folderDestG2Build + '/css/*.css'])
        .pipe(minifyCSS({
            keepSpecialComments: 0
        }))
        .pipe(rename(build_name_css))
        .pipe(gulp.dest(folderDestG2Build + '/css'));
});
gulp.task('concatCssG2', function () {
    return gulp.src(
        [
            folderSrcG2 + '/bower_components/bootstrap/docs/assets/css/bootstrap.css',
            folderSrcG2 + '/bower_components/bootstrap/docs/assets/css/bootstrap-responsive.css',
            folderSrcG2 + '/bower_components/backgrid/lib/backgrid.min.css',
            folderSrcG2 + '/bower_components/backgrid-paginator/backgrid-paginator.min.css',
            folderSrcG2 + '/bower_components/backgrid-filter/backgrid-filter.min.css',
            folderSrcG2 + '/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            folderSrcG2 + '/bower_components/pnotify/jquery.pnotify.default.css',
            folderSrcG2 + '/bower_components/select2/select2.css',
            folderSrcG2 + '/bower_components/colpick-jQuery-Color-Picker/css/colpick.css',

            folderSrcG2 + '/css/jquery_tree/themes/default/style.css',
            folderSrcG2 + '/css/select2/select2-bootstrap2.css',
            folderSrcG2 + '/css/auto-complete-pessoa.css',
            folderSrcG2 + '/css/style.css'
        ]
    )
        .pipe(concatCss(name_css))
        .pipe(gulp.dest(folderDestG2Build + '/css'))
});

/*
 * Tarefas para concatenar o JS e CSS do portal
 *
 * */
gulp.task('minifyJsPortal', function () {
    return gulp.src([
        folderSrcPortal + '/js/**/*.js',
        folderSrcG2 + '/js/utils/constants.js'
    ])
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(folderDestPortalBuild + '/js/min'));
});


gulp.task('concatJsPortal', ['minifyJsPortal'], function () {

    return gulp.src([
        folderDestPortalBuild + '/js/min/jsnovoportal/_jq.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/_jquery.placeholder.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/bootstrap.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/animationEnigne.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/arwa.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/ie-fixes.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.base64.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.carouFredSel-6.2.1-packed.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.cycle.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.cycle2.carousel.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.easytabs.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/pnotify.custom.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.eislideshow.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.isotope.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.prettyPhoto.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.fancybox.pack.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.themepunch.plugins.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.themepunch.revolution.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.tipsy.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jqueryvalidate/jquery.validate.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jqueryvalidate/localization/messages_ptbr.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jQuery.XDomainRequest.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/retina.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/timeago.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/jquery.mCustomScrollbar.concat.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/jquery.pajinate.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/jquery.fastLiveFilter.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/jquery.maskedinput.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/underscore.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/bootbox.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backbone.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backbone.marionette.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backgrid.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backbone-pageable.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backgrid-paginator.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backgrid-filter.min.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/moment-with-locales.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/lib/backgrid-moment-cell.js',
        folderDestPortalBuild + '/js/min/constants.js',
        folderDestPortalBuild + '/js/min/CollectionView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwUsuarioPerfilPedagogicoTOModel.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/GerenciarSalasModel.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwAlunosAgendamento.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/AvaliacaoAgendamento.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwAvaliacaoAgendamento.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwEnviarMensagemAluno.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwAvaliacaoAplicacao.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwMunicipioAplicacao.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/GerenciarSalasModel.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/ExtratoColaboradorModel.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwAvaliacaoAluno.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwGradeNota.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwMatricula.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwDisciplinasAgendamento.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/model/VwHistoricoAgendamento.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/CAtencaoCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/GerenciarSalasCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/ProfessorCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/CoordenadorCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/MensagemCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/CoordenadorPrincipalCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/ProfessorPrincipalCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/ExtratoColaboradorCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/GradeCurricularAlunoCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/PesquisarAlunoCollection.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/collection/GridCollectionView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/CAtencaoView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/GerenciarSalasView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/MensagemView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/AgendamentoView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/ExtratoColaboradorView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/ModalOcorrenciaPrincipalView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/SecretariaView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/PesquisarAlunoView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/view/GradeDisciplinaView.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/front.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/financeiro/financeiro.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/promocoes/promocoes.js',
        folderDestPortalBuild + '/js/min/jsnovoportal/financeiro/plano-pagamento.js',
        folderDestPortalBuild + '/js/sistema/atividade-complementar/atividade-complementar.js',
        folderDestPortalBuild + '/js/min/primeiro-acesso/main.js'
    ])
        .pipe(plumber())
        .pipe(concat(build_name_js))
        .pipe(gulp.dest(folderDestPortalBuild + '/js'))
});

gulp.task('minifyCssPortal', function () {
    return gulp.src([folderSrcPortal + '/css/cssnovoportal/**/*.css'])
        .pipe(plumber())
        .pipe(minifyCSS({
            rebase: false,
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest(folderDestPortalBuild + '/css/min'));
});
gulp.task('concatCssPortal', ['minifyCssPortal'], function () {
    return gulp.src(
        [
            folderDestPortalBuild + '/css/min/bootstrap.min.css',
            folderDestPortalBuild + '/css/min/normalize.css',
            folderDestPortalBuild + '/css/min/font-awesome-ie7.min.css',
            folderDestPortalBuild + '/css/min/font-awesome.css',
            folderDestPortalBuild + '/css/min/revolution_settings.css',
            folderDestPortalBuild + '/css/min/isotop_animation.css',
            folderDestPortalBuild + '/css/min/_color-chooser.css',
            folderDestPortalBuild + '/css/min/_colorpicker.css',
            folderDestPortalBuild + '/css/min/pnotify.custom.min.css',
            folderDestPortalBuild + '/css/min/style.css',
            folderDestPortalBuild + '/css/min/responsive.css',
            folderDestPortalBuild + '/css/min/backgrid-paginator.min.css',
            folderDestPortalBuild + '/css/min/backgrid-filter.min.css',
            folderDestPortalBuild + '/css/min/jquery.mCustomScrollbar.css',
        ]
    )
        .pipe(concat(build_name_css))
        .pipe(gulp.dest(folderDestPortalBuild + '/css'))
});

/*
 * Definição das tarefas separadas caso queira executar apenas em um ambiente
 *
 * */
gulp.task('portal', ['concatJsPortal', 'concatCssPortal']);
gulp.task('g2', ['concatJsG2', 'concatCssG2']);

/*
 * Junção das tarefas para executar em todas aplicações
 *
 * */
gulp.task('default', ['portal']);
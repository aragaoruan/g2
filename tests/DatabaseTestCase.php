<?php

/**
 *
 * Classe base para os testes de Entidades.
 *
 */
class DatabaseTestCase extends \ApplicationTestCase
{

    protected $tool;
    protected $em;
    private $entityList;
    private $viewList;
    private $fnList;
    private $spList;
    private $data;
    private $entitysToView = array();
    private $configs;
    private $modelTestCase;
    private $metas;

    public function appBootstrap()
    {
        $bootstrap = $this->front->getParam('bootstrap');
        $this->em = $bootstrap->getResource('doctrine')->getEntityManager();
    }

    public function __construct()
    {
        parent::__construct();

        $this->appBootstrap();
        $this->configs = $this->application->bootstrap()->getOption('phpunit');

        $this->modelTestCase = new ModelTestCase();

        $entityList = realpath(APPLICATION_PATH . '/../tests/xml') . '/entity-list.xml';
        $this->entityList = new \Zend_Config_Xml($entityList);

        $viewList = realpath(APPLICATION_PATH . '/../tests/xml') . '/view-list.xml';
        $this->viewList = new \Zend_Config_Xml($viewList);

        $fnList = realpath(APPLICATION_PATH . '/../tests/xml') . '/fn-list.xml';
        $this->fnList = new \Zend_Config_Xml($fnList);

        $spList = realpath(APPLICATION_PATH . '/../tests/xml') . '/sp-list.xml';
        $this->spList = new \Zend_Config_Xml($spList);

        $dataPath = realpath(APPLICATION_PATH . '/../tests/xml') . '/data.xml';
        $this->data = new SimpleXMLElement(file_get_contents($dataPath));

        $this->metas = $this->getClassMetas(APPLICATION_PATH . '/../library/G2/Entity', 'G2\Entity\\');

        $this->initCreateSchema();

        $this->generateDataMass($this->data);
    }

    /**
     * Esta função retorna os METAS (annotations) dos arquivos de entities
     * para que o banco possa ser criado para utilização nas rotinas de teste
     * Todos os arquivos .php do diretório Entity serão lidos e a partir disso
     * o Doctrine vai criar as tabelas.
     * @param string $path
     * @param string $namespace
     * @return array
     */
    public function getClassMetas($path, $namespace)
    {
        $metas = array();

        /**
         * Verifica se pelas configuracoes da tag [TESTING] do application.ini
         * se esta definido a geracao das entitys pelo entity list ou geral
         */
        if ($this->configs['database']['entitylist']) {
            $metas = $this->getEntitiesCustomList($namespace);
        } else {

            $exclude = $this->entityList->toArray();

            if ($handle = opendir($path)) {
                $cont = 0;
                while (false !== ($file = readdir($handle))) {
                    list($class) = explode('.', $file);

                    //Gera e mantem meta da entity
                    if (strstr($file, '.php')) {
                        //Verifica se a entity
                        $excluded = false;
                        $createOnly = false;
                        if (array_key_exists('exclude', $exclude) && array_key_exists($class, $exclude['exclude'])) {
                            $excluded = true;

                            if (array_key_exists('createOnly', $exclude['exclude'][$class]) && $exclude['exclude'][$class]['createOnly'] == 'true') {
                                $excluded = false;
                                $createOnly = true;
                            }
                        }

                        if (!$excluded) {
                            /**
                             * Verificando se a classe entity e uma View
                             * para que o doctrine nao crie a tabela por enquanto
                             */
                            $ref = new ReflectionClass($namespace . $class);
                            if (strstr($ref->getDocComment(), '@EntityView')) {
                                $this->addEntityToView($namespace . $class);
                            } else {
                                $obj = $this->em->getClassMetadata($namespace . $class);
                                $obj->createOnly = $createOnly;

                                $metas[] = $obj;
                            }
                        }
                    }
                    $cont++;
                }
            }
        }
        return $metas;
    }

    private function addEntityToView($class)
    {
        $this->entitysToView[] = $class;
    }

    /**
     * @description Método responsável por gerar as views a partir dos
     * scripts sql que estão na pasta /docs/vw_sql
     * @param array $files
     * @return boolean $return;
     * @throws Zend_Exception
     */
    private function generateViews($files = array())
    {
        // Seta a conexão
        $connection = $this->em->getConnection();

        // Remove todas as views existentes
        $connection->prepare(
            file_get_contents($this->getPathQuery() . DIRECTORY_SEPARATOR . 'querys/drop_all_views.sql')
        )->execute();

        //Seta o caminho para os scripts de vw
        $dirVw = $this->getPathQuery() . DIRECTORY_SEPARATOR . 'vw_sql';

        // Pega os valores setados anteriormente na variavel através do arquivo de xml
        $vws = $this->viewList->toArray();

        foreach ($vws['include'] as $k => $value) {
            try {

                $file = file_get_contents($dirVw . DIRECTORY_SEPARATOR . 'dbo.' . $value . '.View.sql');

                if (!$file)
                    throw new Zend_Exception('Erro ao ler o arquivo: dbo.' . $value . '.View.sql');

                $connection->prepare($file)->execute();

            } catch (\Doctrine\ORM\ORMException $orm) {
                throw new Zend_Exception($orm->getMessage());
            } catch (\Doctrine\DBAL\ConnectionException $c) {
                throw new Zend_Exception($c->getMessage());
            }
        }
    }

    /**
     * @description Método responsável por gerar as sps a partir dos
     * scripts sql que estão na pasta /docs/vw_sql
     * @param array $files
     * @return boolean $return;
     * @throws Zend_Exception
     */
    private function generateSp($files = array())
    {
        // Seta a conexão
        $connection = $this->em->getConnection();

        // Remove todas as views existentes
        $connection->prepare(
            file_get_contents($this->getPathQuery() . DIRECTORY_SEPARATOR . 'querys/drop_all_sps.sql')
        )->execute();

        //Seta o caminho para os scripts de vw
        $dirVw = $this->getPathQuery() . DIRECTORY_SEPARATOR . 'sp_sql';

        // Pega os valores setados anteriormente na variavel através do arquivo de xml
        $vws = $this->spList->toArray();

        foreach ($vws['include'] as $k => $value) {
            try {

                $file = file_get_contents($dirVw . DIRECTORY_SEPARATOR . 'dbo.' . $value . '.sql');

                if (!$file)
                    throw new Zend_Exception('Erro ao ler o arquivo: dbo.' . $value . '.sql');

                $connection->prepare($file)->execute();

            } catch (\Doctrine\ORM\ORMException $orm) {
                throw new Zend_Exception($orm->getMessage());
            } catch (\Doctrine\DBAL\ConnectionException $c) {
                throw new Zend_Exception($c->getMessage());
            }
        }
    }

    /**
     * @description Método responsável por gerar as functions a partir dos
     * scripts sql que estão na pasta /docs/vw_sql
     * @param array $files
     * @return boolean $return;
     * @throws Zend_Exception
     */
    private function generateFunctions($files = array())
    {
        // Seta a conexão
        $connection = $this->em->getConnection();

        // Remove todas as views existentes
        $connection->prepare(
            file_get_contents($this->getPathQuery() . DIRECTORY_SEPARATOR . 'querys/drop_all_functions.sql')
        )->execute();

        //Seta o caminho para os scripts de vw
        $dirVw = $this->getPathQuery() . DIRECTORY_SEPARATOR . 'fn_sql';

        // Pega os valores setados anteriormente na variavel através do arquivo de xml
        $vws = $this->fnList->toArray();

        foreach ($vws['include'] as $k => $value) {
            try {

                $file = file_get_contents($dirVw . DIRECTORY_SEPARATOR . 'dbo.' . $value . '.sql');

                if (!$file)
                    throw new Zend_Exception('Erro ao ler o arquivo: dbo.' . $value . '.sql');

                $connection->prepare($file)->execute();

            } catch (\Doctrine\ORM\ORMException $orm) {
                throw new Zend_Exception($orm->getMessage());
            } catch (\Doctrine\DBAL\ConnectionException $c) {
                throw new Zend_Exception($c->getMessage());
            }
        }
    }

    /**
     * @description Método responsável por gerar a massa de dados iniciais e dados posteriores por grupos ou individuais.
     * @param mixed $data
     * @return boolean $return;
     * @throws Zend_Exception
     */
    private function generateDataMass($data)
    {
        if ($this->configs['database']['createdatamass']) {
            try {
                foreach ($data as $group) {
                    if (isset($group['dependency']) && !empty($group['dependency']))
                        $this->generateMassAux((string)$group['dependency']);

                    foreach ($group->file as $file) {
                        if (isset($file['dependency']) && !empty($file['dependency']))
                            $this->generateMassAux((string)$file['dependency']);

                        $this->modelTestCase->loadFixtures((string)$file['name']);
                    }
                }
            } catch (\Doctrine\ORM\ORMException $orm) {
                throw new \Zend_Exception($orm->getMessage());
            } catch (\Doctrine\DBAL\ConnectionException $c) {
                throw new \Zend_Exception($c->getMessage());
            }
        }
    }

    private function generateMassAux($groups = null)
    {
        if ($groups !== null) {
            $groups = explode(".", $groups);
            foreach ($groups as $g) {
                if (empty($this->data->$g)) {
                    $fileDp = $this->modelTestCase->loadFixtures((string)$g);
                    if (!$fileDp)
                        throw new Zend_Exception('Nó ou arquivo ' . $g . ' inexistente, por favor verifique o nome das dependências');
                } else {
                    $this->generateDataMass($this->data->$g);
                }
            }
        }
    }

    /**
     * Método resposável por procurar o arquivo com a lista das entities a
     * serem testadas, este método testa entities especificas passadas
     * pelo arquivo
     * @param string $namespace
     * @return array
     */
    private function getEntitiesCustomList($namespace)
    {
        //Path do arquivo xml com a lista das entities a serem testadas
        $file = realpath(APPLICATION_PATH . '/../tests/xml') . '/entity-list.xml';
        $metas = array();
        //Verifica se existe o arquivo
        if (file_exists($file)) {
            $include = $this->entityList->toArray();

            if (array_key_exists('include', $include)) {
                foreach ($include['include'] as $entity) {
                    $metas[] = $this->em->getClassMetadata($namespace . $entity['className']);
                }
            }
        }
        return $metas;
    }

    /**
     * Cria a base de dados apartir das entitys
     */
    public function initCreateSchema()
    {
        $this->tool = new \Doctrine\ORM\Tools\SchemaTool($this->em);
        $filename = realpath(APPLICATION_PATH) . '/../tests/database.sql';

        //Vefica pelas configuracoes se deve gerar o banco de dados
        if ($this->configs['database']['create']) {
            // Drop e construção do banco de dados
            $this->tool->dropDatabase();

            /*
             * TODO: Não é a melhor maneira, verificar se o Doctrine consegue gerar de uma forma mais eficiente.
             * */
            $extra = '';

            foreach ($this->metas as $metas) {
                $table = $metas->table['name'];

//                foreach ($metas->getAssociationMappings() as $field) {
//                    $type = '';
//                    if (array_key_exists('joinColumns', $field) && $field['joinColumns'][0]['nullable'] == true) {
//
//                        switch ($field['type']) {
//                            case 1:
//                            case 2:
//                                $type = 'INT';
//                                break;
//                            default:
//                                $type = $field['type'];
//                                break;
//                        }
//
//                        $extra .= "ALTER TABLE $table ALTER COLUMN {$field['fieldName']} $type NULL;\r\n";
//                    }
//                }

                foreach ($metas->fieldMappings as $field) {
                    if ($field['nullable'] == true) {
                        switch ($field['type']) {
                            case 'string':
                                $length = $field['length'] ? $field['length'] : 255;
                                $type = 'VARCHAR(' . $length . ')';
                                break;
                            case 'boolean':
                                $type = 'BIT';
                                break;
                            case 'integer':
                                $type = 'INT';
                                break;
                            default:
                                $type = $field['type'];
                                break;

                        }

                        $extra .= "ALTER TABLE $table ALTER COLUMN {$field['fieldName']} $type NULL;\r\n";
                    }
                }

            }

            //Grava conteudo sql executado em banco em um arquivo fisico
            $sqls = '';
            if ($this->configs['database']['generatefile']) {
                $file = @fopen($filename, 'w');
                foreach ($this->tool->getCreateSchemaSql($this->metas) as $schemaSql) {
                    $sqls .= "$schemaSql\r\n";
                }

                @fputs($file, $sqls . $extra);
                @fclose($file);
            }

            if ($this->configs['database']['createfromsqlfile']){
                try {
                    $file = file($filename);
                    foreach ($file as $key => $line) {
                        $this->em->getConnection()->prepare($line)->execute();
                    }
                    @fclose($file);
                } catch (\Doctrine\ORM\ORMException $e){
                    \Zend_Debug::dump($e->getMessage());
                }
            } else {
                $this->tool->createSchema($this->metas);

                try {
                    foreach (explode(';', $extra) as $line) {
                        $this->em->getConnection()->prepare($line)->execute();
                    }
                } catch (\Doctrine\ORM\ORMException $e){
                    \Zend_Debug::dump($e->getMessage());
                }

            }

            //Drop e construção das fns
            $this->generateFunctions();

            //Drop e construção das views
            $this->generateViews();

            //Drop e construção das views
            $this->generateSp();

        }

    }

    /**
     * Metodo Test responsavel por gerar Proxies onde sao identificados
     * possiveis problemas na criacao de entities
     */
    public function testSchemaValidator()
    {
        try {
            $tool = new \Doctrine\ORM\Tools\SchemaValidator($this->em);
            $out = '';

            $count = 0;

            foreach ($this->metas as $metas) {
                if (!$metas->createOnly) {
                    $validate = $tool->validateClass($metas);

                    if (!empty($validate)) {
                        $count++;
                        foreach ($validate as $value) {
                            $out .= $metas->name . ': ' . $value . ' | ';
                        }
                    }
                }
            }

            $this->assertEquals(0, $count, 'Existem os seguintes erros: ' . $out);
        } catch (Doctrine\ORM\Mapping\MappingException $e) {
            throw $e;
        }
    }
}
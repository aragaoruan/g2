<?php

/**
 *
 * Classe base para os testes de Entidades.
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class ApplicationTestCase extends \PHPUnit_Framework_TestCase
{

    protected $application;
    protected $front;
    private $pathQuery;

    public function __construct()
    {
        $this->pathQuery = $path = APPLICATION_PATH . "/../docs";

        $this->application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        $this->application->bootstrap();

        $this->front = Zend_Controller_Front::getInstance();
        if ($this->front->getParam('bootstrap') === null) {
            $this->front->setParam('bootstrap', $this->application->getBootstrap());
        }
    }

    /**
     * @return mixed
     */
    public function getPathQuery()
    {
        return $this->pathQuery;
    }

    /**
     * @param mixed $pathQuery
     */
    public function setPathQuery($pathQuery)
    {
        $this->pathQuery = $pathQuery;
    }

}

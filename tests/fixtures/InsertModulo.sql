DELETE
FROM
	tb_modulo;

INSERT INTO dbo.tb_modulo (
	st_modulo,
	st_tituloexibicao,
	bl_ativo,
	st_descricao,
	id_situacao,
	id_projetopedagogico,
	id_moduloanterior
)
VALUES
	(
		'Único',
		'Único',
		1,
		'Descricao',
		33,
		2,
		NULL
	);
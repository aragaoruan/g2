DELETE
FROM
	tb_modulodisciplina;


SET IDENTITY_INSERT dbo.tb_modulodisciplina ON;

ALTER TABLE dbo.tb_modulodisciplina NOCHECK CONSTRAINT ALL;

ALTER TABLE dbo.tb_modulodisciplina ALTER COLUMN nu_ponderacaocalculada NUMERIC (10, 0) NULL;

ALTER TABLE dbo.tb_modulodisciplina ALTER COLUMN nu_ponderacaoaplicada NUMERIC (10, 0) NULL;

ALTER TABLE dbo.tb_modulodisciplina ALTER COLUMN id_usuarioponderacao INT NULL;

ALTER TABLE dbo.tb_modulodisciplina ALTER COLUMN dt_cadastroponderacao DATETIME NULL;

ALTER TABLE dbo.tb_modulodisciplina ALTER COLUMN nu_importancia INT NULL;

INSERT INTO tb_modulodisciplina (
	id_modulo,
	id_disciplina,
	id_serie,
	id_nivelensino,
	id_areaconhecimento,
	bl_ativo,
	nu_ordem,
	bl_obrigatoria,
	nu_ponderacaocalculada,
	nu_ponderacaoaplicada,
	id_usuarioponderacao,
	dt_cadastroponderacao,
	id_modulodisciplina,
	nu_importancia
)
VALUES
	(
		2,
		1,
		11,
		7,
		1,
		1,
		0,
		1,
		NULL,
		NULL,
		NULL,
		NULL,
		2,
		NULL
	);


SET IDENTITY_INSERT dbo.tb_modulodisciplina OFF;
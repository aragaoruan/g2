ALTER TABLE dbo.tb_perfil ALTER COLUMN id_entidadeclasse INT NULL;

ALTER TABLE dbo.tb_perfil ALTER COLUMN id_perfilpai INT NULL;

ALTER TABLE dbo.tb_perfil NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_perfil ON;

DELETE
FROM
	tb_perfil;

INSERT INTO tb_perfil (
	id_perfil,
	id_perfilpai,
	st_nomeperfil,
	bl_ativo,
	id_situacao,
	id_entidade,
	bl_padrao,
	id_entidadeclasse,
	dt_cadastro,
	id_usuariocadastro,
	id_perfilpedagogico,
	id_sistema
)
VALUES
	(
		4,
		NULL,
		'Aluno',
		1,
		-- boolean bl_ativo
		4,
		3,
		0,
		-- boolean bl_padrao
		NULL,
		'2012-03-22',
		19,
		5,
		4
	);


SET IDENTITY_INSERT dbo.tb_perfil OFF;
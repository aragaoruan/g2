DELETE
FROM
	tb_tipodestinatario;

SET IDENTITY_INSERT dbo.tb_tipodestinatario ON;

INSERT INTO tb_tipodestinatario (
	id_tipodestinatario,
	st_tipodestinatario
)
VALUES
	(1, 'Destinatário'),
	(2, 'CC'),
	(3, 'BCC');


SET IDENTITY_INSERT dbo.tb_tipodestinatario OFF;
ALTER TABLE dbo.tb_pessoa ALTER COLUMN id_entidade INT NULL;

ALTER TABLE dbo.tb_pessoa ALTER COLUMN id_municipio INT NULL;

ALTER TABLE dbo.tb_pessoa ALTER COLUMN id_usuariocadastro INT NULL;

ALTER TABLE dbo.tb_pessoa ALTER COLUMN st_passaporte INT NULL;

ALTER TABLE dbo.tb_pessoa ALTER COLUMN st_urlavatar VARCHAR (255) NULL;

ALTER TABLE dbo.tb_pessoa ALTER COLUMN st_cidade VARCHAR (255) NULL;

ALTER TABLE dbo.tb_pessoa NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_pessoa ON;

DELETE
FROM
	tb_pessoa;

INSERT INTO tb_pessoa (
	id_usuario,
	id_entidade,
	st_sexo,
	st_nomeexibicao,
	dt_nascimento,
	st_nomepai,
	st_nomemae,
	id_pais,
	sg_uf,
	id_municipio,
	dt_cadastro,
	id_usuariocadastro,
	st_passaporte,
	id_situacao,
	bl_ativo,
	id_estadocivil,
	st_urlavatar,
	st_cidade,
	bl_alterado
)
VALUES
	(
		1,
		3,
		'F',
		'Felipe Lemos Toffolo',
		'1986-02-28',
		'Pai',
		'Mãe',
		22,
		'MG',
		3146107,
		'2012-03-22',
		1,
		NULL,
		1,
		1,
		1,
		NULL,
		NULL,
		0
	),
	(
		19,
		3,
		'M',
		'VINICIUS SILVA',
		'1986-03-12',
		'Pai',
		'Mãe',
		22,
		'SP',
		3520103,
		'2012-03-27',
		5,
		NULL,
		1,
		1,
		1,
		NULL,
		NULL,
		0
	);


SET IDENTITY_INSERT dbo.tb_pessoa OFF;
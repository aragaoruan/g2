ALTER TABLE tb_contratoregra ALTER COLUMN id_modelocarteirinha INT NULL;

DELETE FROM tb_contratoregra;

SET IDENTITY_INSERT dbo.tb_contratoregra ON;

INSERT INTO tb_contratoregra (
	id_contratoregra,
	st_contratoregra,
	nu_contratoduracao,
	nu_mesesmulta,
	bl_proporcaomes,
	id_projetocontratoduracaotipo,
	id_entidade,
	id_usuariocadastro,
	nu_contratomultavalor,
	bl_renovarcontrato,
	nu_mesesmensalidade,
	bl_ativo,
	dt_cadastro,
	nu_tempoextensao,
	id_contratomodelo,
	id_extensaomodelo,
	id_tiporegracontrato
)
VALUES
	(
		1,
		'Contrato IBOPE',
		6,
		1,
		1,
		--boolean true
		2,
		3,
		1,
		'100.00',
		0,
		--boolean false
		0,
		1,
		--boolean true
		'2012-03-22',
		0,
		3,
		4,
		1
	);


SET IDENTITY_INSERT dbo.tb_contratoregra OFF
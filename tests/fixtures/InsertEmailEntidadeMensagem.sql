ALTER TABLE dbo.tb_emailentidademensagem NOCHECK CONSTRAINT ALL;

DELETE
FROM
	tb_emailentidademensagem;

INSERT INTO tb_emailentidademensagem (
	id_emailconfig,
	id_entidade,
	id_usuariocadastro,
	id_mensagempadrao,
	id_textosistema,
	bl_ativo,
	dt_cadastro
)
VALUES
	(1, 3, 19, 1, 5, 1, '2012-04-20');
ALTER TABLE dbo.tb_matricula NOCHECK CONSTRAINT ALL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_periodoletivo INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_vendaproduto INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_turma INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_matriculaorigem INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_mensagemrecuperacao INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_entidadematriz INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_entidadematricula INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_entidadematricula INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_situacaoagendamento INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_evolucaoagendamento INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_cancelamento INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_evolucaocertificacao INT NULL;

ALTER TABLE dbo.tb_matricula ALTER COLUMN id_matriculavinculada INT NULL;

DELETE
FROM
	tb_matricula;


SET IDENTITY_INSERT dbo.tb_matricula ON;

INSERT INTO tb_matricula (
	id_matricula,
	id_projetopedagogico,
	id_usuario,
	id_periodoletivo,
	dt_cadastro,
	id_usuariocadastro,
	dt_inicio,
	id_evolucao,
	id_situacao,
	bl_ativo,
	id_vendaproduto,
	id_entidadeatendimento,
	id_entidadematriz,
	id_entidadematricula,
	dt_concluinte,
	bl_institucional,
	id_turma,
	id_matriculaorigem,
	id_situacaoagendamento,
	dt_termino,
	id_evolucaoagendamento,
	id_mensagemrecuperacao,
	bl_documentacao,
	bl_academico
)
VALUES
	(
		23,
		2,
		1,
		NULL,
		'2012-03-26',
		1,
		'2012-03-26',
		6,
		50,
		1,
		NULL,
		3,
		3,
		3,
		NULL,
		1,
		NULL,
		NULL,
		120,
		NULL,
		119,
		NULL,
		0,
		0
	);


SET IDENTITY_INSERT dbo.tb_matricula OFF;
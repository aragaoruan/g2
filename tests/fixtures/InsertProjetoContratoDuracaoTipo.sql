SET IDENTITY_INSERT dbo.tb_projetocontratoduracaotipo ON;

DELETE FROM tb_projetocontratoduracaotipo;

INSERT INTO tb_projetocontratoduracaotipo
  (id_projetocontratoduracaotipo, st_projetocontratoduracaotipo, st_descricao)
  VALUES
  (2, 'Meses', 'Define a regra de duração de um contrato para um projeto pedagógico como MESES');

SET IDENTITY_INSERT dbo.tb_projetocontratoduracaotipo OFF;
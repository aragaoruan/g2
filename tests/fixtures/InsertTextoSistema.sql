ALTER TABLE dbo.tb_textosistema NOCHECK CONSTRAINT ALL;

SET IDENTITY_INSERT dbo.tb_textosistema ON;

DELETE FROM dbo.tb_textosistema;

INSERT INTO dbo.tb_textosistema (
  id_textosistema,
  id_usuario,
  id_entidade,
  id_textoexibicao,
  id_textocategoria,
  id_situacao,
  st_textosistema,
  st_texto,
  dt_inicio,
  dt_fim,
  dt_cadastro,
  id_orientacaotexto,
  bl_cabecalho)
VALUES (5, 1, 3, 2, 1, 60, 'Envio de Boleto Ibope',
        '<h1> Caro aluno, segue abaixo o link do seu boleto:</h1><h3>&nbsp;</h3><h3>#link_boleto#</h3><p>&nbsp;</p><p>&nbsp;</p>',
        '2012-03-21', '2012-03-21', '2012-03-23', 1, 0);

SET IDENTITY_INSERT dbo.tb_textosistema OFF;
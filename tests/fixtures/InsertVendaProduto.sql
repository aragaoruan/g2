ALTER TABLE dbo.tb_vendaproduto NOCHECK CONSTRAINT ALL;

ALTER TABLE dbo.tb_vendaproduto ALTER COLUMN id_avaliacaoaplicacao INT NULL;

ALTER TABLE dbo.tb_vendaproduto ALTER COLUMN id_matricula INT NULL;

ALTER TABLE dbo.tb_vendaproduto ALTER COLUMN id_tiposelecao INT NULL;

ALTER TABLE dbo.tb_vendaproduto ALTER COLUMN id_situacao INT NULL;

DELETE
FROM
	tb_vendaproduto;


SET IDENTITY_INSERT dbo.tb_vendaproduto ON;

INSERT INTO tb_vendaproduto (
	id_vendaproduto,
	id_produto,
	nu_desconto,
	id_venda,
	id_campanhacomercial,
	nu_valorliquido,
	nu_valorbruto,
	dt_cadastro,
	id_matricula,
	id_turma,
	id_produtocombo,
	id_avaliacaoaplicacao,
	dt_entrega
)
VALUES
	(
		9,
		2,
		'0.00',
		9,
		'',
		'4410.12',
		'7800.00',
		'2012-03-27',
		NULL,
		'',
		'',
		NULL,
		-- id_avaliacaoaplicacao
		''
	);


SET IDENTITY_INSERT dbo.tb_vendaproduto OFF;
ALTER TABLE tb_areaprojetopedagogico NOCHECK CONSTRAINT all;

DELETE
FROM
	tb_areaconhecimento;
DELETE
FROM
	tb_tipoareaconhecimento;
SET IDENTITY_INSERT dbo.tb_tipoareaconhecimento ON;
INSERT INTO [dbo].[tb_tipoareaconhecimento] (
	[id_tipoareaconhecimento],
	[st_tipoareaconhecimento],
	[st_descricao]
)
VALUES
	(
		'1',
		'Global',
		'Esse tipo de área serve como um agregador de áreas disciplinares'
	);
INSERT INTO [dbo].[tb_tipoareaconhecimento] (
	[id_tipoareaconhecimento],
	[st_tipoareaconhecimento],
	[st_descricao]
)
VALUES
	(
		'2',
		'Conhecimento',
		'Área básica correspondente a um nível de disciplina'
	);
INSERT INTO [dbo].[tb_tipoareaconhecimento] (
	[id_tipoareaconhecimento],
	[st_tipoareaconhecimento],
	[st_descricao]
)
VALUES
	(
		'3',
		'Disciplinar',
		'Área das subdivisões de uma disciplina'
	);
SET IDENTITY_INSERT dbo.tb_tipoareaconhecimento OFF;
SET IDENTITY_INSERT dbo.tb_areaconhecimento ON;
INSERT INTO [dbo].[tb_areaconhecimento] (
	[id_areaconhecimento],
	[id_situacao],
	[st_descricao],
	[bl_ativo],
	[st_areaconhecimento],
	[id_entidade],
	[dt_cadastro],
	[id_usuariocadastro],
	[id_areaconhecimentopai],
	[id_tipoareaconhecimento],
	[st_tituloexibicao],
	[bl_extras]
)
VALUES
	(
		'1',
		'6',
		'MBA',
		'0',
		'MBA',
		'3',
		'2012-03-22 14:53:55.9070000',
		'3',
		NULL,
		'1',
		'MBA',
		NULL
	);
SET IDENTITY_INSERT dbo.tb_areaconhecimento OFF;

DELETE FROM dbo.tb_areaprojetopedagogico;

INSERT INTO [dbo].[tb_areaprojetopedagogico] (
	[id_areaconhecimento],
	[id_projetopedagogico]
)
VALUES
	('1', '2');
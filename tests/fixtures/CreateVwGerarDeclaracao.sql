CREATE VIEW vw_gerardeclaracao as
  SELECT DISTINCT

    mt.id_matricula,
    us.id_usuario AS id_usuarioaluno,
    us.st_nomecompleto AS st_nomecompletoaluno,
    us.st_login AS st_loginaluno,
    ds.st_senha AS st_senhaaluno,
    us.st_cpf AS st_cpfaluno,
    id.st_rg AS st_rgaluno,
    id.st_orgaoexpeditor,
    id.dt_dataexpedicao AS dt_dataexpedicaoaluno,
    ps.st_nomemae AS st_filiacao,


    ps.dt_nascimento AS dt_nascimentoaluno,
    UPPER(SUBSTRING(st_nomemunicipio, 1, 1)) +
    LOWER(SUBSTRING(mn.st_nomemunicipio, 2, 499)) AS st_municipioaluno,
    uf.st_uf AS st_ufaluno,
    uf.sg_uf AS sg_ufaluno,
    ct.nu_ddd AS nu_dddaluno,
    ct.nu_telefone AS nu_telefonealuno,


    pp.st_tituloexibicao AS st_projeto,
    pp.nu_cargahoraria,
    ne.st_nivelensino,
    mt.dt_concluinte AS dt_concluintealuno,
    us_coordenador.st_nomecompleto AS st_coordenadorprojeto,


    fl.id_fundamentolegal AS id_fundamentolei,
    fl.nu_numero AS nu_lei,
    flr.id_fundamentolegal AS id_fundamentoresolucao,


    tur.st_turma,
    tur.dt_fim AS dt_terminoturma,


    flr.nu_numero AS nu_resolucao,
    flpr.id_fundamentolegal AS id_fundamentoparecer,
    flpr.nu_numero AS nu_parecer,
    flp.id_fundamentolegal AS id_fundamentoportaria,
    flp.nu_numero AS nu_portaria,

    et.id_entidade,
    et.st_nomeentidade,
    et.st_razaosocial,
    et.st_cnpj,
    et.st_urlimglogo,
    et.st_urlsite,
    pais.st_nacionalidade,
    ps.st_sexo AS sexo_aluno,
    av.st_tituloavaliacao AS st_titulomonografia,

    CONVERT(VARCHAR(2), DATEPART(DAY, GETDATE())) + ' de ' +

    CONVERT(VARCHAR(15), DATENAME(MONTH, GETDATE())) + ' de ' +

    CONVERT(VARCHAR(4), DATEPART(YEAR, GETDATE())) AS st_data,


    DAY(GETDATE()) AS st_dia,
    DATENAME(MONTH, GETDATE()) AS st_mes,
    YEAR(GETDATE()) AS st_ano,

    CONVERT(DATE,sa.dt_abertura, 103) AS dt_primeirasala,
    CONVERT(DATE,DATEADD(MONTH,13, sa.dt_abertura), 103) AS dt_previsaofim

  FROM tb_matricula mt


    JOIN tb_projetopedagogico AS pp
      ON pp.id_projetopedagogico = mt.id_projetopedagogico


    JOIN tb_entidade AS et
      ON et.id_entidade = mt.id_entidadematriz


    LEFT JOIN tb_entidadeendereco AS ete
      ON ete.id_entidade = et.id_entidade AND ete.bl_padrao = 1


    LEFT JOIN tb_endereco AS etee
      ON etee.id_endereco = ete.id_endereco


    JOIN tb_usuario AS us
      ON us.id_usuario = mt.id_usuario

    LEFT JOIN tb_dadosacesso AS ds ON ds.id_usuario = mt.id_usuario AND ds.id_entidade = mt.id_entidadeatendimento AND ds.bl_ativo = 1


    JOIN tb_pessoa AS ps
      ON ps.id_usuario = mt.id_usuario AND (ps.id_entidade = mt.id_entidadematricula OR ps.id_entidade = mt.id_entidadematriz)


    LEFT JOIN tb_municipio AS mn
      ON mn.id_municipio = ps.id_municipio


    LEFT JOIN tb_uf AS uf
      ON ps.sg_uf = uf.sg_uf


    LEFT JOIN tb_pais AS pais
      ON ps.id_pais = pais.id_pais


    LEFT JOIN tb_pessoaendereco AS pe
      ON mt.id_usuario = pe.id_usuario AND pe.bl_padrao = 1


    LEFT JOIN tb_endereco AS ed
      ON ed.id_endereco = pe.id_endereco


    LEFT JOIN tb_contatostelefonepessoa AS ctp
      ON ctp.id_usuario = mt.id_usuario AND ctp.id_entidade = mt.id_entidadeatendimento AND ctp.bl_padrao = 1


    LEFT JOIN tb_contatostelefone AS ct
      ON ct.id_telefone = ctp.id_telefone


    LEFT JOIN tb_documentoidentidade AS id
      ON id.id_usuario = mt.id_usuario


    LEFT JOIN tb_fundamentolegal AS fl
      ON fl.id_entidade = mt.id_entidadematriz AND fl.id_tipofundamentolegal = 1 AND fl.dt_publicacao <= GETDATE() AND fl.dt_vigencia >= GETDATE()


    LEFT JOIN tb_fundamentolegal AS flp
      ON flp.id_entidade = mt.id_entidadematriz AND flp.id_tipofundamentolegal = 2 AND flp.dt_publicacao <= GETDATE() AND flp.dt_vigencia >= GETDATE()


    LEFT JOIN tb_fundamentolegal AS flr
      ON flr.id_entidade = mt.id_entidadematriz AND flr.id_tipofundamentolegal = 3 AND flr.dt_publicacao <= GETDATE() AND flr.dt_vigencia >= GETDATE()


    LEFT JOIN tb_fundamentolegal AS flpr
      ON flpr.id_entidade = mt.id_entidadematriz AND flpr.id_tipofundamentolegal = 4 AND flpr.dt_publicacao <= GETDATE() AND flpr.dt_vigencia >= GETDATE()


    LEFT JOIN tb_projetopedagogicoserienivelensino AS psn
      ON psn.id_projetopedagogico = mt.id_projetopedagogico


    LEFT JOIN tb_turma AS tur
      ON tur.id_turma = mt.id_turma


    JOIN tb_nivelensino AS ne
      ON ne.id_nivelensino = psn.id_nivelensino

    LEFT JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = pp.id_projetopedagogico AND uper.bl_ativo = 1 AND uper.bl_titular = 1
    LEFT JOIN dbo.tb_usuario AS us_coordenador ON us_coordenador.id_usuario = uper.id_usuario


    OUTER APPLY (SELECT TOP 1 ts.dt_abertura FROM tb_matriculadisciplina tm
      JOIN tb_alocacao ta ON tm.id_matriculadisciplina = ta.id_matriculadisciplina
      JOIN tb_saladeaula ts ON ta.id_saladeaula = ts.id_saladeaula WHERE tm.id_matricula = mt.id_matricula ORDER BY ts.dt_abertura DESC) AS sa
    LEFT JOIN dbo.vw_avaliacaoaluno av ON(av.id_matricula = mt.id_matricula AND av.id_tipoavaliacao = 6  AND av.st_tituloavaliacao IS NOT NULL AND id_tipodisciplina = 2 AND av.bl_ativo = 1)
  END
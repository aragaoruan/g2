ALTER TABLE dbo.tb_textocategoria NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_textocategoria ON;

DELETE
FROM
	tb_textocategoria;

INSERT INTO tb_textocategoria (
	id_textocategoria,
	st_textocategoria,
	st_descricao,
	st_metodo
)
VALUES
	(
		1,
		'Matricula',
		'Texto para matricula',
		'gerarDeclaracao'
	);


SET IDENTITY_INSERT dbo.tb_textocategoria OFF;
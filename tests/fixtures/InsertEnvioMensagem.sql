
ALTER TABLE dbo.tb_enviomensagem NOCHECK CONSTRAINT ALL;
ALTER TABLE dbo.tb_enviomensagem ALTER COLUMN dt_tentativa DATETIME NULL;
ALTER TABLE dbo.tb_enviomensagem ALTER COLUMN id_sistema INT NULL;
ALTER TABLE dbo.tb_enviomensagem ALTER COLUMN dt_cadastro DATETIME NULL;
ALTER TABLE dbo.tb_enviomensagem ALTER COLUMN dt_envio DATETIME NULL;

ALTER TABLE dbo.tb_enviodestinatario ALTER COLUMN st_nome VARCHAR(100) NULL;
ALTER TABLE dbo.tb_enviodestinatario ALTER COLUMN id_evolucao INT NULL;
ALTER TABLE dbo.tb_enviodestinatario ALTER COLUMN nu_telefone VARCHAR(17) NULL;
ALTER TABLE dbo.tb_enviodestinatario ALTER COLUMN st_endereco VARCHAR(100) NULL;

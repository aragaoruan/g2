ALTER TABLE tb_entidade ALTER COLUMN id_entidadecadastro INT NULL;
ALTER TABLE tb_entidade ALTER COLUMN id_esquemaconfiguracao INT NULL;

DELETE FROM tb_entidade;

SET IDENTITY_INSERT dbo.tb_entidade ON;

INSERT INTO [dbo].[tb_entidade] (
	[id_entidade],
	[bl_ativo],
	[st_nomeentidade],
	[st_urlimglogo],
	[bl_acessasistema],
	[nu_cnpj],
	[id_situacao],
	[st_razaosocial],
	[nu_inscricaoestadual],
	[id_entidadecadastro],
	[st_urlsite],
	[dt_cadastro],
	[id_usuariocadastro],
	[st_cnpj],
	[st_wschave],
	[st_conversao],
	[st_urlportal],
	[st_siglaentidade],
	[str_urlimglogoentidade],
	[st_apelido],
	[id_uploadloja],
	[st_urllogoutportal]
)
VALUES
	(
		'1',
		'1',
		'EAD1',
		NULL,
		'1',
		'1',
		'2',
		'EAD1',
		'1',
		NULL,
		'www.ead1.com.br',
		'2012-03-21 21:48:22.0000000',
		'1',
		'11111111111111',
		'1',
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	);

INSERT INTO [dbo].[tb_entidade] (
	[id_entidade],
	[bl_ativo],
	[st_nomeentidade],
	[st_urlimglogo],
	[bl_acessasistema],
	[nu_cnpj],
	[id_situacao],
	[st_razaosocial],
	[nu_inscricaoestadual],
	[id_entidadecadastro],
	[st_urlsite],
	[dt_cadastro],
	[id_usuariocadastro],
	[st_cnpj],
	[st_wschave],
	[st_conversao],
	[st_urlportal],
	[st_siglaentidade],
	[str_urlimglogoentidade],
	[st_apelido],
	[id_uploadloja],
	[st_urllogoutportal]
)
VALUES
	(
		'2',
		'1',
		'Unyleya',
		'/upload/entidade/logo_(2).png',
		'0',
		NULL,
		'2',
		'Unyleya',
		'1',
		'1',
		'www.unyleya.com.br',
		'2012-03-22 09:57:42.6930000',
		'1',
		'11111111111111',
		'8060e79984130cb38b061765b75a26d5a76f4de0',
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	);

INSERT INTO [dbo].[tb_entidade] (
	[id_entidade],
	[bl_ativo],
	[st_nomeentidade],
	[st_urlimglogo],
	[bl_acessasistema],
	[nu_cnpj],
	[id_situacao],
	[st_razaosocial],
	[nu_inscricaoestadual],
	[id_entidadecadastro],
	[st_urlsite],
	[dt_cadastro],
	[id_usuariocadastro],
	[st_cnpj],
	[st_wschave],
	[st_conversao],
	[st_urlportal],
	[st_siglaentidade],
	[str_urlimglogoentidade],
	[st_apelido],
	[id_uploadloja],
	[st_urllogoutportal]
)
VALUES
	(
		'3',
		'1',
		'Ibope',
		'/upload/entidade/logo_header-menu-ibope.png.png',
		'0',
		NULL,
		'2',
		'Ibope',
		'2',
		'2',
		'http://www.tempodeconcurso.com.br',
		'2012-03-22 10:17:49.5370000',
		'1',
		'14019108000130',
		'40757a259b7ab11eb12d342730c8a1e8cad0248f',
		NULL,
		'http://portaltesting.ead1.net/index',
		'',
		'',
		'',
		NULL,
		'http://correiowebpreparese.com.br'
	);

SET IDENTITY_INSERT dbo.tb_entidade OFF;
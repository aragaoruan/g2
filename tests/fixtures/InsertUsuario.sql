ALTER TABLE tb_entidade NOCHECK CONSTRAINT ALL;

SET IDENTITY_INSERT dbo.tb_usuario ON;

DELETE FROM dbo.tb_usuario;

INSERT INTO dbo.tb_usuario
           (id_usuario,
           st_login,
           st_senha,
           st_nomecompleto,
           id_usuariopai,
           bl_ativo,
           st_cpf,
           id_registropessoa,
           bl_redefinicaosenha,
           id_titulacao)
     VALUES
           (1, 'Felipe','teste','Felipe Lemos Toffolo',1,1,'05515340795',4,0,1),
           (19, 'Felipe','teste','Felipe Lemos Toffolo234234',1,1,'05515340795',4,0,1);

SET IDENTITY_INSERT dbo.tb_usuario OFF;
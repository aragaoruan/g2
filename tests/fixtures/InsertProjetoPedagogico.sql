ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_tipoextracurricular INT NULL;

ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_medidatempoconclusao INT NULL;

ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_contratoregra INT NULL;

ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_livroregistroentidade INT NULL;

ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_horarioaula INT NULL;

ALTER TABLE dbo.tb_projetopedagogico ALTER COLUMN id_anexoementa INT NULL;

ALTER TABLE dbo.tb_projetopedagogico NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_projetopedagogico ON;

DELETE
FROM
	tb_projetopedagogico;

INSERT INTO tb_projetopedagogico (
	id_projetopedagogico,
	st_descricao,
	nu_idademinimainiciar,
	bl_ativo,
	nu_tempominimoconcluir,
	nu_idademinimaconcluir,
	nu_tempomaximoconcluir,
	nu_tempopreviconcluir,
	st_projetopedagogico,
	st_tituloexibicao,
	bl_autoalocaraluno,
	id_situacao,
	id_trilha,
	id_projetopedagogicoorigem,
	st_nomeversao,
	id_tipoextracurricular,
	nu_notamaxima,
	nu_percentualaprovacao,
	nu_valorprovafinal,
	id_entidadecadastro,
	bl_provafinal,
	bl_salasemprovafinal,
	st_objetivo,
	st_estruturacurricular,
	st_metodologiaavaliacao,
	st_certificacao,
	st_valorapresentacao,
	nu_cargahoraria,
	dt_cadastro,
	id_usuariocadastro,
	bl_disciplinaextracurricular,
	id_usuario,
	st_conteudoprogramatico,
	id_medidatempoconclusao,
	id_contratoregra,
	id_livroregistroentidade,
	nu_prazoagendamento,
	st_perfilprojeto,
	st_coordenacao,
	st_horario,
	st_publicoalvo,
	st_mercadotrabalho,
	bl_turma,
	bl_gradepronta,
	id_horarioaula,
	--dt_criagrade,
	id_usuariograde
)
VALUES
	(
		2,
		'',
		0,
		1,
		-- boolean bl_ativo
		0,
		0,
		0,
		0,
		'MBA Em Gestão Estratégica',
		'MBA Em Gestão Estratégica',
		1,
		-- boolean bl_autoalocaraluno
		7,
		2,
		2,
		NULL,
		NULL,
		10.00,
		70,
		0,
		3,
		1,
		-- boolean bl_provafinal
		1,
		-- boolean bl_salasemprovafinal
		'',
		'',
		'',
		'',
		'',
		420,
		'2012-03-22 00:00:00',
		3,
		1,
		-- boolean bl_disciplinaextracurricular
		1,
		'',
		NULL,
		NULL,
		NULL,
		0,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		-- boolean bl_turma
		1,
		-- boolean bl_gradepronta
		NULL,
		--null,
		19
	);


SET IDENTITY_INSERT dbo.tb_projetopedagogico OFF;
ALTER TABLE dbo.tb_produtoprojetopedagogico NOCHECK CONSTRAINT ALL;
ALTER TABLE dbo.tb_produtoprojetopedagogico ALTER COLUMN nu_cargahoraria NUMERIC NULL;
ALTER TABLE dbo.tb_produtoprojetopedagogico ALTER COLUMN st_disciplina VARCHAR NULL;

DELETE FROM tb_produtoprojetopedagogico;

SET IDENTITY_INSERT dbo.tb_produtoprojetopedagogico ON;

insert into tb_produtoprojetopedagogico 
(id_projetopedagogico,
id_produto,
id_entidade,
nu_tempoacesso,
id_turma,
id_produtoprojetopedagogico,
bl_indeterminado,
nu_cargahoraria,
st_disciplina)
values (
2,
2,
3,
'',
'',
2,
1, -- boolean bl_indeterminado
NULL,
NULL);

SET IDENTITY_INSERT dbo.tb_produtoprojetopedagogico OFF;
ALTER TABLE dbo.tb_projetopedagogicoserienivelensino NOCHECK CONSTRAINT ALL;

DELETE
FROM
	tb_projetopedagogicoserienivelensino;

INSERT INTO tb_projetopedagogicoserienivelensino (
	id_projetopedagogico,
	id_serie,
	id_nivelensino
)
VALUES
	(2, 11, 7)
ALTER TABLE tb_contratoregra NOCHECK CONSTRAINT all;

DELETE FROM tb_tiporegracontrato;

INSERT INTO [dbo].[tb_tiporegracontrato] ([id_tiporegracontrato], [st_tiporegracontrato], [bl_ativo]) VALUES ('1', 'Normal', '1');
INSERT INTO [dbo].[tb_tiporegracontrato] ([id_tiporegracontrato], [st_tiporegracontrato], [bl_ativo]) VALUES ('2', 'Free-Pass', '1');

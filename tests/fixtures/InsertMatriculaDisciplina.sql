
--SET IDENTITY_INSERT dbo.tb_matriculadisciplina ON;

ALTER TABLE dbo.tb_matriculadisciplina NOCHECK CONSTRAINT ALL;
ALTER TABLE dbo.tb_matriculadisciplina ALTER COLUMN nu_aprovafinal INT NULL;
ALTER TABLE dbo.tb_matriculadisciplina ALTER COLUMN dt_conclusao DATETIME NULL;

--SET IDENTITY_INSERT dbo.tb_matriculadisciplina OFF;
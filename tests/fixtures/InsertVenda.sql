ALTER TABLE dbo.tb_venda ALTER COLUMN id_contratoafiliado INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_prevenda INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_protocolo INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_cupom INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_campanhacomercialpremio INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_uploadcontrato INT NULL;

ALTER TABLE dbo.tb_venda ALTER COLUMN id_campanhapontualidade INT NULL;

ALTER TABLE dbo.tb_venda NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_venda ON;

DELETE
FROM
	tb_venda;

INSERT INTO tb_venda (
	id_venda,
	id_formapagamento,
	dt_cadastro,
	nu_descontoporcentagem,
	nu_descontovalor,
	nu_juros,
	id_campanhacomercial,
	id_evolucao,
	id_situacao,
	bl_ativo,
	id_usuariocadastro,
	nu_valorliquido,
	nu_valorbruto,
	id_entidade,
	id_usuario,
	id_tipocampanha,
	nu_parcelas,
	bl_contrato,
	nu_diamensalidade,
	id_origemvenda,
	dt_agendamento,
	dt_confirmacao,
	id_contratoafiliado,
	st_observacao,
	nu_valoratualizado,
	recorrente_orderid,
	id_ocorrencia,
	id_atendente,
	id_prevenda,
	id_protocolo,
	id_enderecoentrega,
	id_cupom
)
VALUES
	(
		9,
		1,
		'2012-03-27',
		'43.46000',
		'3390.00000',
		'0.00000',
		1,
		10,
		39,
		1,
		1,
		'4410.00',
		'7800.00',
		3,
		1,
		1,
		0,
		1,
		1,
		1,
		'',
		'2014-09-30',
		NULL,
		'',
		'0.00',
		'',
		'',
		1,
		NULL,
		NULL,
		62130,
		NULL
	);


SET IDENTITY_INSERT dbo.tb_venda OFF;
ALTER TABLE dbo.tb_endereco ALTER COLUMN st_estadoprovincia INT NULL;

ALTER TABLE dbo.tb_endereco ALTER COLUMN st_cidade INT NULL;

ALTER TABLE dbo.tb_endereco NOCHECK CONSTRAINT ALL;


SET IDENTITY_INSERT dbo.tb_endereco ON;

DELETE
FROM
	tb_endereco;

INSERT INTO tb_endereco (
	id_endereco,
	id_pais,
	sg_uf,
	id_municipio,
	id_tipoendereco,
	st_cep,
	st_endereco,
	st_bairro,
	st_complemento,
	nu_numero,
	st_estadoprovincia,
	st_cidade,
	bl_ativo
)
VALUES
	(
		62130,
		22,
		'MG',
		3147105,
		1,
		'35.660-000',
		'Rua Benedito Valadares',
		'Centro',
		'',
		'455',
		NULL,
		NULL,
		1
	);


SET IDENTITY_INSERT dbo.tb_endereco OFF;

DELETE
FROM
	tb_pessoaendereco;

ALTER TABLE dbo.tb_pessoaendereco NOCHECK CONSTRAINT ALL;

INSERT INTO tb_pessoaendereco (
	id_usuario,
	id_entidade,
	id_endereco,
	bl_padrao
)
VALUES
	(1, 3, 62130, 1);
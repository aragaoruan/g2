ALTER TABLE tb_usuario NOCHECK CONSTRAINT all;

DELETE FROM tb_registropessoa;

set IDENTITY_INSERT dbo.tb_registropessoa ON;

INSERT INTO tb_registropessoa (id_registropessoa, st_registropessoa) VALUES (4, 'Brasileiro Maior de Idade');

set IDENTITY_INSERT dbo.tb_registropessoa OFF;

DELETE FROM tb_titulacao;

set IDENTITY_INSERT dbo.tb_titulacao ON;

INSERT INTO tb_titulacao (id_titulacao, st_titulacao) VALUES (1, 'Especialista');

set IDENTITY_INSERT dbo.tb_titulacao OFF;
DELETE
FROM
	tb_nivelensino;

SET IDENTITY_INSERT dbo.tb_nivelensino ON;

INSERT INTO tb_nivelensino (
	id_nivelensino,
	st_nivelensino
)
VALUES
	(1, 'Ensino Superior'),
	(2, 'Ensino Médio'),
	(3, 'Ensino Fundamental'),
	(4, 'Educação Infantil'),
	(5, 'Técnico'),
	(6, 'Curso Livre'),
	(7, 'Pós-Graduação');


SET IDENTITY_INSERT dbo.tb_nivelensino OFF;
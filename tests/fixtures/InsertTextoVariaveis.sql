ALTER TABLE dbo.tb_textovariaveis NOCHECK CONSTRAINT ALL;

ALTER TABLE dbo.tb_textovariaveis ALTER COLUMN st_mascara VARCHAR (100) NULL;


SET IDENTITY_INSERT dbo.tb_textovariaveis ON;

DELETE
FROM
	tb_textovariaveis;

INSERT INTO tb_textovariaveis (
	id_textovariaveis,
	id_textocategoria,
	st_camposubstituir,
	st_textovariaveis,
	st_mascara,
	id_tipotextovariavel
)
VALUES
	(
		54,
		1,
		'st_nomecompletoaluno',
		'#nome_aluno#',
		NULL,
		1
	);


SET IDENTITY_INSERT dbo.tb_textovariaveis OFF;
DELETE FROM dbo.tb_contrato

SET IDENTITY_INSERT dbo.tb_contrato ON

INSERT INTO tb_contrato (
	id_contrato,
	bl_ativo,
	id_usuario,
	dt_cadastro,
	id_entidade,
	id_usuariocadastro,
	id_evolucao,
	id_situacao,
	id_venda,
	id_contratoregra,
	id_textosistema,
	dt_ativacao,
	dt_termino,
	nu_bolsa,
	nu_codintegracao
)
VALUES
	(
		9,
		1,
		19,
		'2012-03-27 15:20:39.963',
		3,
		1,
		4,
		44,
		9,
		1,
		3,
		'2012-03-27',
		'2012-09-27',
		100,
		NULL
	);


SET IDENTITY_INSERT dbo.tb_contrato OFF;
<?php

/**
 * Controller Test Case
 *
 * @author Paulo Silva
 * 
 */
class ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase {

    public function setUp() {

        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();

        global $application;
        $application->bootstrap();
        if ($this->getFrontController()->getParam('bootstrap') === null) {
            $this->getFrontController()->setParam('bootstrap', $application->getBootstrap());
        }

    }

    public function appBootstrap(){

        \Zend_Controller_Action_HelperBroker::addHelper(new Zend_Layout_Controller_Action_Helper_Layout());

        $this->getFrontController()->getInstance()->setParam('prefixDefaultModule', true);
        $this->getFrontController()->addModuleDirectory(APPLICATION_PATH . '/apps');

        $this->getFrontController()->setDefaultModule('default');
        $this->getFrontController()->setDefaultControllerName('index');

    }

    public function loginUser($usuario, $senha, $entidade, $perfil) {

        $this->request->setMethod('POST')
              ->setPost(array(
                  'username' => $usuario,
                  'password' => $senha
              ));
        $this->dispatch('/index/auth');
    }

    public function tearDown() {
        parent::tearDown();
    }

}

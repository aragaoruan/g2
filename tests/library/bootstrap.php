<?php 

$rootPath	= __DIR__ .'/../..';

set_include_path( $rootPath .'/library'. PATH_SEPARATOR .get_include_path() );
include('Zend/Loader/Autoloader.php');

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);


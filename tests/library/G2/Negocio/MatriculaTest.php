<?php

namespace G2\Negocio;

/**
 * CLasse para tests de funcionalidades ligadas a matriculas
 * @group Matricula
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class MatriculaTest extends \ModelTestCase
{

    /**
     * Teste da funcionalidade ativar matriculas do G2
     * neste teste foram utilizados recursos mais antigos como BO, TO, ORM e DAO
     * alem de populacao de banco com SQLs diretos, Storage Procedures e Views.
     *
     * @group Matricula
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function testAtivarMatricula()
    {

        //Chamadas de TO
        require_once APPLICATION_PATH . '/models/to/ContratoTO.php';
        require_once APPLICATION_PATH . '/models/to/VendaTO.php';
        require_once APPLICATION_PATH . '/models/to/VwGerarDeclaracaoTO.php';

        //Chamadas de BO
        require_once APPLICATION_PATH . '/models/bo/MatriculaBO.php';
        require_once APPLICATION_PATH . '/models/bo/ContratoBO.php';
        require_once APPLICATION_PATH . '/models/bo/TramiteBO.php';
        require_once APPLICATION_PATH . '/models/bo/MensagemBO.php';
        require_once APPLICATION_PATH . '/models/bo/SalaDeAulaBO.php';
        require_once APPLICATION_PATH . '/models/bo/TextoSistemaBO.php';
        require_once APPLICATION_PATH . '/models/bo/TextosBO.php';
        require_once APPLICATION_PATH . '/models/bo/EntidadeBO.php';

        //Chamadas de DAO
        require_once APPLICATION_PATH . '/models/dao/MatriculaDAO.php';
        require_once APPLICATION_PATH . '/models/dao/ProdutoDAO.php';
        require_once APPLICATION_PATH . '/models/dao/ProjetoPedagogicoDAO.php';
        require_once APPLICATION_PATH . '/models/dao/TramiteDAO.php';
        require_once APPLICATION_PATH . '/models/dao/VendaDAO.php';
        require_once APPLICATION_PATH . '/models/dao/MensagemDAO.php';
        require_once APPLICATION_PATH . '/models/dao/SalaDeAulaDAO.php';
        require_once APPLICATION_PATH . '/models/dao/TextosDAO.php';
        require_once APPLICATION_PATH . '/models/dao/EntidadeDAO.php';

        //Chamadas de ORM
        require_once APPLICATION_PATH . '/models/orm/ContratoORM.php';
        require_once APPLICATION_PATH . '/models/orm/ContratoRegraORM.php';
        require_once APPLICATION_PATH . '/models/orm/ContratoMatriculaORM.php';
        require_once APPLICATION_PATH . '/models/orm/VendaORM.php';
        require_once APPLICATION_PATH . '/models/orm/VendaProdutoORM.php';
        require_once APPLICATION_PATH . '/models/orm/MatriculaORM.php';
        require_once APPLICATION_PATH . '/models/orm/VendaProdutoNivelSerieORM.php';
        require_once APPLICATION_PATH . '/models/orm/ProjetoPedagogicoORM.php';
        require_once APPLICATION_PATH . '/models/orm/PerfilORM.php';
        require_once APPLICATION_PATH . '/models/orm/UsuarioPerfilEntidadeORM.php';
        require_once APPLICATION_PATH . '/models/orm/EmailEntidadeMensagemORM.php';
        require_once APPLICATION_PATH . '/models/orm/TextoSistemaORM.php';
        require_once APPLICATION_PATH . '/models/orm/TextoCategoriaORM.php';
        require_once APPLICATION_PATH . '/models/orm/VwGerarDeclaracaoORM.php';
        require_once APPLICATION_PATH . '/models/orm/TextoVariaveisORM.php';
        require_once APPLICATION_PATH . '/models/orm/MensagemORM.php';
        require_once APPLICATION_PATH . '/models/orm/EnvioMensagemORM.php';
        require_once APPLICATION_PATH . '/models/orm/EnvioDestinatarioORM.php';
        require_once APPLICATION_PATH . '/models/orm/EntidadeIntegracaoORM.php';

//        parent::loadFixtures('SpMatriculaDisciplina.sql');
//        parent::loadFixtures('CreateVwGerarDeclaracao.sql');

        $contratoTO = new \ContratoTO();
        $contratoTO->setId_contrato(9);

        $negocio = new \G2\Negocio\Matricula();

        $resultSave = $negocio->ativarMatricula($contratoTO);

        $this->assertEquals(1, $resultSave->getTipo(), $resultSave->getFirstMensagem());

    }

}

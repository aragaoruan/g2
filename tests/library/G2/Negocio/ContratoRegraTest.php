<?php

namespace G2\Negocio;

/**
 * Description of ContratoRegraTest
 *
 * @author elcioguimaraes@gmail.com
 */
class ContratoRegraTest extends \ModelTestCase
{

    /**
     * Funcao de teste exemplo
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function testSalvarRegra()
    {
        $negocio = new \G2\Negocio\Contrato();

        $contratoRegra = new \G2\Entity\ContratoRegra();
        $contratoRegra->setSt_contratoregra('Descrição da regra')
            ->setDt_cadastro(new \DateTime())
            ->setId_entidade(array('id_entidade' => 3))
            ->setId_extensaomodelo(1)
            ->setId_usuariocadastro(array('id_usuario' => 1))
            ->setBl_proporcaomes(true)
            ->setBl_renovarcontrato(true)
            ->setBl_ativo(true)
            ->setId_tiporegracontrato($this->em->getReference('G2\Entity\TipoRegraContrato', 1))
            ->setId_projetocontratoduracaotipo(array('id_projetocontratoduracaotipo' => 2));

        $resultSave = $negocio->salvarRegra($contratoRegra);

        $this->assertEquals(1, $resultSave->getTipo());
    }
}

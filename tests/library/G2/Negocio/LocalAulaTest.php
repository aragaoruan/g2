<?php

namespace G2\Negocio;

/**
 * Description of LocalAulaTest
 *
 * @author paulo.silva@unyleya.com.br
 */
class LocalAulaTest extends \ModelTestCase {

    /**
     * Funcao de teste exemplo
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function testConsigoInstanciarLocalAula() {
        $this->needsDB = false;
        $this->assertInstanceOf('G2\Entity\LocalAula', new \G2\Entity\LocalAula());
    }

}

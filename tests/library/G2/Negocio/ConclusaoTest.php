<?php

namespace G2\Negocio;

/**
 * Description of ConclusaoTest
 * @group Free
 * @author unyleya
 */
class ConclusaoTest extends \ModelTestCase
{
    public function testPesquisaAlunosConcluintesSucesso()
    {
        $negocio = new \G2\Negocio\Conclusao();

        $where = array(
            'st_nomecompleto' => '',
            'id_entidade' => 3, 
            'dt_concluinte_inicio' => '',
            'dt_concluinte_fim' => '',
            'id_projetopedagogico' => '',
            'bl_documentacao' => '',
            'id_evolucao' => '6',
            'id_evolucaocertificacao' => '',
            'bl_disciplinacomplementar' => false,
            'dt_certificado_inicio' => '',
            'dt_certificado_fim' => '',
            'dt_envio-aluno_inicio' => '',
            'dt_envio-aluno_fim' => '',
            'dt_envio_certificadora_inicio' => '',
            'dt_envio_certificadora_fim' => '',
            'dt_retorno_certificadora_inicio' => '',
            'dt_retorno_certificadora_fim' => '',
            'bl_academico' => ''
        );

        $result = $negocio->pesquisaAlunosConcluintes($where);

        $this->assertInternalType('array', $result);
    }
}

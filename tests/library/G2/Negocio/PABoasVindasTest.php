<?php

namespace G2\Negocio;

use G2\Entity\PABoasVindas as PABoasVindasEntity;

/**
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 */
class PABoasVindasTest extends \ModelTestCase {

    public function testInstanciarPABoasVindas() {
        $this->assertInstanceOf('G2\Entity\PABoasVindas', new PABoasVindasEntity());
    }

}
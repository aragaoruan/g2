<?php

/**
 * Teste do controller index
 */

namespace G2\Controller;

/**
 * @group Controllers
 * Teste do controlador Index
 * @author paulo.silva
 */
class IndexTest extends \ControllerTestCase
{

    public function testChamadaSemUrlDeveChamarModuloDefaultControllerIndexActionIndex()
    {
        $this->dispatch('/index');
        $this->assertModule('default');
        $this->assertController('index');
        $this->assertAction('login');
    }

    public function testChamandoRotaInexistenteDeveRetornarControladorErroActionErro()
    {
        $this->dispatch('/default/index');
        $this->assertModule('default');
        $this->assertController('index');
        $this->assertAction('login');
    }

    /**
     * Funcao de teste exemplo
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    /*    public function testeLoadingApiMatriculaAction() {

            $this->getRequest()
                ->setMethod('GET')
                ->setParam('chave', 'valor');
            $this->dispatch('/api/matricula/head');

            $this->assertController('matricula');
            $this->assertAction('head');

            //$this->assertEquals(!0, strpos($this->getResponse()->getBody(), 'mosca no restaurante'));
        }*/

}

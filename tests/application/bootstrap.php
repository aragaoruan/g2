<?php

// Define path to application directory
defined('APPLICATION_PATH')
   || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

//Define application sys
defined('APPLICATION_SYS')
    || define('APPLICATION_SYS', 'GESTOR');

// Define application environment
define('APPLICATION_ENV', 'testing');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../tests'),
    realpath(APPLICATION_PATH . '/models'),
    realpath(APPLICATION_PATH . '/models/bo'),
    realpath(APPLICATION_PATH . '/models/dao'),
    realpath(APPLICATION_PATH . '/models/to'),
    realpath(APPLICATION_PATH . '/models/orm'),
    realpath(APPLICATION_PATH . '/apps/portal/controllers'),
    get_include_path()
)));

include('Zend/Loader/Autoloader.php');
//Inicializando autoload na aplicacao
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

/** Zend_Application */
require_once 'Zend/Application.php';
// Create application, bootstrap, and run
$application = new Zend_Application(
   APPLICATION_ENV,
   APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap()->run();

clearstatcache();
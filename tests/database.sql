CREATE TABLE tb_nucleotm (id_nucleotm INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_entidade INT NOT NULL, id_entidadematriz INT NOT NULL, id_usuariocadastro INT NOT NULL, id_situacao INT, bl_ativo BIT NOT NULL, st_nucleotm NVARCHAR(150) NOT NULL, PRIMARY KEY (id_nucleotm))
CREATE TABLE tb_arquivoretornopacote (id_arquivoretornopacote INT IDENTITY NOT NULL, id_arquivoretornomaterial INT, id_pacote INT, id_itemdematerial INT, id_situacaopostagem INT, st_codrastreamento NVARCHAR(60) NOT NULL, dt_postagem DATETIME2(6), dt_devolucao DATETIME2(6), st_resultado NVARCHAR(200) NOT NULL, PRIMARY KEY (id_arquivoretornopacote))
CREATE INDEX IDX_D0BB03F2C303F3E8 ON tb_arquivoretornopacote (id_arquivoretornomaterial)
CREATE INDEX IDX_D0BB03F22416BAA7 ON tb_arquivoretornopacote (id_pacote)
CREATE INDEX IDX_D0BB03F248CEA880 ON tb_arquivoretornopacote (id_itemdematerial)
CREATE INDEX IDX_D0BB03F2EB341277 ON tb_arquivoretornopacote (id_situacaopostagem)
CREATE TABLE tb_projetopedagogicoserienivelensino (id_projetopedagogico INT NOT NULL, id_serie INT NOT NULL, id_nivelensino INT NOT NULL, PRIMARY KEY (id_projetopedagogico))
CREATE INDEX IDX_D47E7C5144BFD204 ON tb_projetopedagogicoserienivelensino (id_serie)
CREATE INDEX IDX_D47E7C516DA54E76 ON tb_projetopedagogicoserienivelensino (id_nivelensino)
CREATE TABLE tb_formadisponibilizacao (id_formadisponibilizacao INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), bl_ativo INT NOT NULL, st_formadisponibilizacao NVARCHAR(250) NOT NULL, PRIMARY KEY (id_formadisponibilizacao))
CREATE TABLE tb_sistema (id_sistema INT IDENTITY NOT NULL, st_sistema NVARCHAR(255) NOT NULL, st_conexao NVARCHAR(2000) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_sistema))
CREATE TABLE tb_entidadesistema (id_sistema INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_sistema, id_entidade))
CREATE INDEX IDX_F6FA05424F5F0211 ON tb_entidadesistema (id_sistema)
CREATE INDEX IDX_F6FA0542B3F20A47 ON tb_entidadesistema (id_entidade)
CREATE TABLE tb_disciplinaconteudo (id_disciplinaconteudo INT IDENTITY NOT NULL, id_disciplina INT NOT NULL, id_usuariocadastro INT NOT NULL, id_formatoarquivo INT NOT NULL, st_descricaoconteudo NVARCHAR(100), dt_cadastro DATETIME2(6), bl_ativo BIT, PRIMARY KEY (id_disciplinaconteudo))
CREATE INDEX IDX_5D127010D759AAF1 ON tb_disciplinaconteudo (id_disciplina)
CREATE INDEX IDX_5D1270102FF60C20 ON tb_disciplinaconteudo (id_usuariocadastro)
CREATE INDEX IDX_5D12701068C7A4BD ON tb_disciplinaconteudo (id_formatoarquivo)
CREATE TABLE tb_tipofuncionalidade (id_tipofuncionalidade INT IDENTITY NOT NULL, st_tipofuncionalidade NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipofuncionalidade))
CREATE TABLE tb_contratoresponsavel (id_contratoresponsavel INT IDENTITY NOT NULL, id_contrato INT, id_tipocontratoresponsavel INT, id_entidaderesponsavel INT, id_venda INT, id_usuario INT, nu_porcentagem INT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_contratoresponsavel))
CREATE INDEX IDX_C47DBFEB36B289B6 ON tb_contratoresponsavel (id_contrato)
CREATE INDEX IDX_C47DBFEB78B9BC56 ON tb_contratoresponsavel (id_tipocontratoresponsavel)
CREATE INDEX IDX_C47DBFEBE94BA3BB ON tb_contratoresponsavel (id_entidaderesponsavel)
CREATE INDEX IDX_C47DBFEB2BA0BD34 ON tb_contratoresponsavel (id_venda)
CREATE TABLE tb_formapagamentoproduto (id_produto INT NOT NULL, id_formapagamento INT NOT NULL, PRIMARY KEY (id_produto, id_formapagamento))
CREATE TABLE tb_categoriasala (id_categoriasala INT IDENTITY NOT NULL, st_categoriasala NVARCHAR(100) NOT NULL, PRIMARY KEY (id_categoriasala))
CREATE TABLE tb_prevendaproduto (id_prevendaproduto INT IDENTITY NOT NULL, id_produto INT, id_prevenda INT, PRIMARY KEY (id_prevendaproduto))
CREATE INDEX IDX_A3E4EC148231E0A7 ON tb_prevendaproduto (id_produto)
CREATE INDEX IDX_A3E4EC14B960F69E ON tb_prevendaproduto (id_prevenda)
CREATE TABLE tb_operacaolog (id_operacaolog INT IDENTITY NOT NULL, st_nomeoperacaolog NVARCHAR(10) NOT NULL, st_descricaoperacaolog NVARCHAR(255), PRIMARY KEY (id_operacaolog))
CREATE TABLE tb_tipodematerial (id_tipodematerial INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_entidade INT NOT NULL, st_tipodematerial NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipodematerial))
CREATE INDEX IDX_F00452B5C0694FE1 ON tb_tipodematerial (id_situacao)
CREATE INDEX IDX_F00452B5B3F20A47 ON tb_tipodematerial (id_entidade)
CREATE TABLE tb_alocacao (id_alocacao INT IDENTITY NOT NULL, id_saladeaula INT, id_evolucao INT, id_situacao INT, id_usuariocadastro INT, id_matriculadisciplina INT, dt_inicio DATE, dt_cadastro DATETIME2(6) NOT NULL, id_situacaotcc INT NOT NULL, id_categoriasala INT, bl_ativo BIT NOT NULL, bl_desalocar BIT NOT NULL, nu_diasextensao INT, PRIMARY KEY (id_alocacao))
CREATE INDEX IDX_42023FE15A094BB1 ON tb_alocacao (id_saladeaula)
CREATE INDEX IDX_42023FE130B15C09 ON tb_alocacao (id_evolucao)
CREATE INDEX IDX_42023FE1C0694FE1 ON tb_alocacao (id_situacao)
CREATE INDEX IDX_42023FE12FF60C20 ON tb_alocacao (id_usuariocadastro)
CREATE INDEX IDX_42023FE1C9D64318 ON tb_alocacao (id_matriculadisciplina)
CREATE TABLE tb_lancamento (id_lancamento INT IDENTITY NOT NULL, id_tipolancamento INT, id_meiopagamento INT, st_banco NVARCHAR(3), id_cartaoconfig INT, id_boletoconfig INT, id_entidadelancamento INT, nu_valor NUMERIC(30, 2) NOT NULL, nu_vencimento NUMERIC(30, 2), dt_cadastro DATETIME2(6) NOT NULL, id_usuariolancamento INT, id_entidade INT, id_usuariocadastro INT, bl_quitado BIT NOT NULL, dt_vencimento DATE, dt_quitado DATE, dt_emissao DATETIME2(6), dt_prevquitado DATETIME2(6), st_emissor NVARCHAR(255), st_coddocumento NVARCHAR(255), nu_juros NUMERIC(30, 2), nu_desconto NUMERIC(30, 2), nu_quitado NUMERIC(30, 5), nu_multa NUMERIC(30, 2), dt_atualizado DATETIME2(6), bl_ativo BIT NOT NULL, bl_original BIT NOT NULL, st_renegociacao NVARCHAR(10), id_lancamentooriginal INT, id_acordo INT, st_agencia NVARCHAR(10), st_numcheque NVARCHAR(10), st_nossonumero NVARCHAR(20), st_codconta NVARCHAR(10), id_codcoligada INT, st_statuslan NVARCHAR(1), dt_vencimentocheque DATE, nu_verificacao INT NOT NULL, bl_baixadofluxus BIT NOT NULL, nu_tentativabraspag INT NOT NULL, nu_cartao INT NOT NULL, st_retornoverificacao NVARCHAR(255), st_autorizacao NVARCHAR(255), st_ultimosdigitos NVARCHAR(4), bl_chequedevolvido BIT, PRIMARY KEY (id_lancamento))
CREATE INDEX IDX_DF0F502EF40710E8 ON tb_lancamento (id_tipolancamento)
CREATE INDEX IDX_DF0F502E9B119E27 ON tb_lancamento (id_meiopagamento)
CREATE INDEX IDX_DF0F502E13FE11CC ON tb_lancamento (st_banco)
CREATE INDEX IDX_DF0F502ECD869197 ON tb_lancamento (id_cartaoconfig)
CREATE INDEX IDX_DF0F502EB2D22ED8 ON tb_lancamento (id_boletoconfig)
CREATE INDEX IDX_DF0F502E230863F0 ON tb_lancamento (id_entidadelancamento)
ALTER TABLE tb_lancamento ADD CONSTRAINT DF_DF0F502E_4F5F5BA0 DEFAULT '0' FOR bl_original
ALTER TABLE tb_lancamento ADD CONSTRAINT DF_DF0F502E_4A3EE7AC DEFAULT '0' FOR bl_baixadofluxus
CREATE TABLE tb_matriculacertificacao (id_indicecertificado INT IDENTITY NOT NULL, id_entidade INT, id_usuarioenviocertificado INT, id_usuarioretornocertificadora INT, id_usuarioenvioaluno INT, id_usuariocadastro INT, id_matricula INT NOT NULL, nu_ano INT NOT NULL, dt_cadastro DATETIME2(6), dt_enviocertificado DATETIME2(6), dt_retornocertificadora DATETIME2(6), dt_envioaluno DATETIME2(6), bl_ativo BIT NOT NULL, st_siglaentidade NVARCHAR(200), st_codigoacompanhamento NVARCHAR(13), PRIMARY KEY (id_indicecertificado))
CREATE INDEX IDX_390664E4B3F20A47 ON tb_matriculacertificacao (id_entidade)
CREATE INDEX IDX_390664E4D7EB1E7A ON tb_matriculacertificacao (id_usuarioenviocertificado)
CREATE INDEX IDX_390664E476B4630 ON tb_matriculacertificacao (id_usuarioretornocertificadora)
CREATE INDEX IDX_390664E4592D74DF ON tb_matriculacertificacao (id_usuarioenvioaluno)
CREATE INDEX IDX_390664E42FF60C20 ON tb_matriculacertificacao (id_usuariocadastro)
CREATE TABLE tb_dashboardentidade (id_dashboardentidade INT IDENTITY NOT NULL, id_dashboard INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_dashboardentidade))
CREATE TABLE tb_situacao (id_situacao INT IDENTITY NOT NULL, st_situacao NVARCHAR(255) NOT NULL, st_tabela NVARCHAR(255) NOT NULL, st_campo NVARCHAR(255) NOT NULL, st_descricaosituacao VARCHAR(MAX) NOT NULL, PRIMARY KEY (id_situacao))
CREATE TABLE tb_cancelamento (id_cancelamento INT IDENTITY NOT NULL, id_evolucao INT, dt_solicitacao DATETIME2(6) NOT NULL, nu_diascancelamento INT NOT NULL, st_observacao NVARCHAR(255), st_observacaocalculo NVARCHAR(255), nu_cargahoraria INT, nu_cargahorariacursada INT, nu_valorbruto NUMERIC(10, 0), nu_valornegociado NUMERIC(10, 0), nu_valordiferenca NUMERIC(10, 0), nu_valorhoraaula NUMERIC(10, 0), nu_valormaterial NUMERIC(10, 0), nu_valorutilizadomaterial NUMERIC(10, 0), nu_valortotal NUMERIC(10, 0), nu_totalutilizado NUMERIC(10, 0), nu_valordevolucao NUMERIC(10, 0), bl_cartagerada BIT, bl_cancelamentofinalizado BIT, nu_valorcarta NUMERIC(10, 0), nu_valormultadevolucao NUMERIC(10, 0), nu_valormultacarta NUMERIC(10, 0), nu_multaporcentagemcarta NUMERIC(10, 0), nu_multaporcentagemdevolucao NUMERIC(10, 0), PRIMARY KEY (id_cancelamento))
CREATE INDEX IDX_D214AD2730B15C09 ON tb_cancelamento (id_evolucao)
CREATE TABLE tb_meiopagamento (id_meiopagamento INT IDENTITY NOT NULL, st_meiopagamento NVARCHAR(255) NOT NULL, st_descricao VARCHAR(MAX) NOT NULL, PRIMARY KEY (id_meiopagamento))
CREATE TABLE tb_concursonivelensino (id_concursonivelensino INT IDENTITY NOT NULL, id_concurso INT NOT NULL, id_nivelensino INT NOT NULL, PRIMARY KEY (id_concursonivelensino))
CREATE INDEX IDX_B7D6EF3228847173 ON tb_concursonivelensino (id_concurso)
CREATE INDEX IDX_B7D6EF326DA54E76 ON tb_concursonivelensino (id_nivelensino)
CREATE TABLE tb_categoriaendereco (id_categoriaendereco INT IDENTITY NOT NULL, st_categoriaendereco NVARCHAR(255) NOT NULL, PRIMARY KEY (id_categoriaendereco))
CREATE TABLE tb_avalagendamentoref (id_avalagendamentoref INT IDENTITY NOT NULL, id_avaliacaoagendamento INT, id_avaliacaoconjuntoreferencia INT, PRIMARY KEY (id_avalagendamentoref))
CREATE TABLE tb_cargahoraria (id_cargahoraria INT IDENTITY NOT NULL, nu_cargahoraria INT NOT NULL, PRIMARY KEY (id_cargahoraria))
CREATE TABLE tb_entidadeintegracao (id_entidadeintegracao INT IDENTITY NOT NULL, id_entidade INT, id_usuariocadastro INT, id_sistema INT NOT NULL, st_codsistema NVARCHAR(255) NOT NULL, dt_cadastro DATETIME2(6), st_codchave NVARCHAR(255) NOT NULL, st_codarea NVARCHAR(100), st_caminho NVARCHAR(255), nu_perfilsuporte NUMERIC(10, 0), nu_perfilprojeto NUMERIC(10, 0), nu_perfildisciplina NUMERIC(10, 0), nu_perfilobservador NUMERIC(10, 0), nu_perfilalunoobs NUMERIC(10, 0), nu_perfilalunoencerrado NUMERIC(10, 0), nu_contexto NUMERIC(10, 0), st_linkedserver NVARCHAR(20), PRIMARY KEY (id_entidadeintegracao))
CREATE TABLE tb_produtoprr (id_saladeaula INT NOT NULL, id_produto INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_saladeaula, id_produto, id_entidade))
CREATE TABLE tb_produto (id_produto INT IDENTITY NOT NULL, id_tipoproduto INT NOT NULL, id_modelovenda INT NOT NULL, id_situacao INT NOT NULL, id_usuariocadastro INT NOT NULL, id_entidade INT NOT NULL, st_produto NVARCHAR(250), bl_ativo BIT NOT NULL, bl_todasformas BIT NOT NULL, bl_todascampanhas BIT NOT NULL, bl_unico BIT NOT NULL, nu_gratuito INT NOT NULL, id_produtoimagempadrao INT, st_descricao NVARCHAR(255), st_observacoes NVARCHAR(255), st_informacoesadicionais NVARCHAR(255), st_estruturacurricular NVARCHAR(255), st_subtitulo NVARCHAR(300), st_slug NVARCHAR(200), bl_destaque BIT NOT NULL, bl_mostrarpreco BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, dt_atualizado DATETIME2(6) NOT NULL, dt_iniciopontosprom DATETIME2(6), dt_fimpontosprom DATETIME2(6), nu_pontos INT NOT NULL, nu_pontospromocional INT NOT NULL, nu_estoque INT NOT NULL, bl_todasentidades BIT NOT NULL, st_cargahoraria NVARCHAR(255) NOT NULL, PRIMARY KEY (id_produto))
CREATE INDEX IDX_237B9375283495BB ON tb_produto (id_tipoproduto)
CREATE INDEX IDX_237B9375E70E16A9 ON tb_produto (id_modelovenda)
CREATE INDEX IDX_237B9375C0694FE1 ON tb_produto (id_situacao)
CREATE INDEX IDX_237B93752FF60C20 ON tb_produto (id_usuariocadastro)
CREATE INDEX IDX_237B9375B3F20A47 ON tb_produto (id_entidade)
CREATE TABLE tb_areaconhecimento (id_areaconhecimento INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_tipoareaconhecimento INT, st_descricao NVARCHAR(2500) NOT NULL, bl_ativo BIT NOT NULL, st_areaconhecimento NVARCHAR(255) NOT NULL, id_entidade INT, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT, id_areaconhecimentopai INT, st_tituloexibicao VARCHAR(MAX), bl_extras BIT, st_imagemarea NVARCHAR(255), PRIMARY KEY (id_areaconhecimento))
CREATE INDEX IDX_C75A6310C0694FE1 ON tb_areaconhecimento (id_situacao)
CREATE INDEX IDX_C75A6310A9B4471E ON tb_areaconhecimento (id_tipoareaconhecimento)
CREATE TABLE tb_tipotelefone (id_tipotelefone INT IDENTITY NOT NULL, st_tipotelefone NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipotelefone))
CREATE TABLE tb_pa_dadoscontato (id_pa_dadoscontato INT IDENTITY NOT NULL, id_textomensagem INT, id_textoinstrucao INT, id_assuntoco INT, id_categoriaocorrencia INT, id_entidade INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_pa_dadoscontato))
CREATE INDEX IDX_E02AA49CB81EAF15 ON tb_pa_dadoscontato (id_textomensagem)
CREATE INDEX IDX_E02AA49CC88A814 ON tb_pa_dadoscontato (id_textoinstrucao)
CREATE INDEX IDX_E02AA49C9B9DCB8E ON tb_pa_dadoscontato (id_assuntoco)
CREATE INDEX IDX_E02AA49CCA9E9832 ON tb_pa_dadoscontato (id_categoriaocorrencia)
CREATE INDEX IDX_E02AA49CB3F20A47 ON tb_pa_dadoscontato (id_entidade)
CREATE INDEX IDX_E02AA49C2FF60C20 ON tb_pa_dadoscontato (id_usuariocadastro)
CREATE TABLE tb_notificacaoentidadeperfil (id_notificacaoentidadeperfil INT IDENTITY NOT NULL, id_notificacaoentidade INT, id_perfil INT, PRIMARY KEY (id_notificacaoentidadeperfil))
CREATE INDEX IDX_91527986316240E5 ON tb_notificacaoentidadeperfil (id_notificacaoentidade)
CREATE INDEX IDX_91527986B052C3AA ON tb_notificacaoentidadeperfil (id_perfil)
CREATE TABLE tb_ocorrencia (id_ocorrencia INT IDENTITY NOT NULL, id_matricula INT, id_evolucao INT, id_situacao INT, id_usuariointeressado INT, id_categoriaocorrencia INT, id_assuntoco INT, id_saladeaula INT, id_ocorrenciaoriginal INT, id_entidade INT, id_usuariocadastro INT, id_motivoocorrencia INT, dt_atendimento DATE, dt_cadastro DATETIME2(6), st_titulo NVARCHAR(255), st_codigorastreio NVARCHAR(255), st_ocorrencia NVARCHAR(255), id_venda INT, dt_cadastroocorrencia DATETIME2(6), PRIMARY KEY (id_ocorrencia))
CREATE INDEX IDX_65F23AF295EAA4A2 ON tb_ocorrencia (id_matricula)
CREATE INDEX IDX_65F23AF230B15C09 ON tb_ocorrencia (id_evolucao)
CREATE INDEX IDX_65F23AF2C0694FE1 ON tb_ocorrencia (id_situacao)
CREATE INDEX IDX_65F23AF2D892A01A ON tb_ocorrencia (id_usuariointeressado)
CREATE INDEX IDX_65F23AF2CA9E9832 ON tb_ocorrencia (id_categoriaocorrencia)
CREATE INDEX IDX_65F23AF29B9DCB8E ON tb_ocorrencia (id_assuntoco)
CREATE INDEX IDX_65F23AF25A094BB1 ON tb_ocorrencia (id_saladeaula)
CREATE INDEX IDX_65F23AF2F0B1D8EB ON tb_ocorrencia (id_ocorrenciaoriginal)
CREATE INDEX IDX_65F23AF2B3F20A47 ON tb_ocorrencia (id_entidade)
CREATE INDEX IDX_65F23AF22FF60C20 ON tb_ocorrencia (id_usuariocadastro)
CREATE INDEX IDX_65F23AF264964C31 ON tb_ocorrencia (id_motivoocorrencia)
CREATE TABLE tb_mensagempadrao (id_mensagempadrao INT IDENTITY NOT NULL, id_tipoenvio INT, st_mensagempadrao NVARCHAR(150) NOT NULL, st_default VARCHAR(MAX), PRIMARY KEY (id_mensagempadrao))
CREATE INDEX IDX_AD1C2331A2782CAC ON tb_mensagempadrao (id_tipoenvio)
CREATE TABLE tb_tipodeconta (id_tipodeconta INT IDENTITY NOT NULL, st_tipodeconta NVARCHAR(100) NOT NULL, PRIMARY KEY (id_tipodeconta))
CREATE TABLE tb_funcao (id_funcao INT IDENTITY NOT NULL, id_tipofuncao INT NOT NULL, st_funcao NVARCHAR(100), PRIMARY KEY (id_funcao))
CREATE INDEX IDX_D309E87147D007BE ON tb_funcao (id_tipofuncao)
CREATE TABLE tb_documentoidentidade (id_usuario INT NOT NULL, id_entidade INT NOT NULL, st_rg NVARCHAR(20) NOT NULL, st_orgaoexpeditor NVARCHAR(80), dt_dataexpedicao DATE, PRIMARY KEY (id_usuario, id_entidade))
CREATE TABLE tb_documentosutilizacao (id_documentosutilizacao INT IDENTITY NOT NULL, st_documentosutilizacao NVARCHAR(30) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_documentosutilizacao))
CREATE TABLE tb_tipotrilhafixa (id_tipotrilhafixa INT IDENTITY NOT NULL, id_tipotrilha INT NOT NULL, st_tipotrilhafixa NVARCHAR(255) NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, PRIMARY KEY (id_tipotrilhafixa))
CREATE INDEX IDX_41C8E2921B1F91F4 ON tb_tipotrilhafixa (id_tipotrilha)
CREATE TABLE tb_concursouf (id_concursouf INT IDENTITY NOT NULL, id_concurso INT NOT NULL, sg_uf NVARCHAR(2) NOT NULL, PRIMARY KEY (id_concursouf))
CREATE INDEX IDX_88467B7E28847173 ON tb_concursouf (id_concurso)
CREATE INDEX IDX_88467B7E4FD882EB ON tb_concursouf (sg_uf)
CREATE TABLE tb_entidadecartaoconfig (id_entidadefinanceiro INT NOT NULL, id_cartaoconfig INT NOT NULL, PRIMARY KEY (id_entidadefinanceiro, id_cartaoconfig))
CREATE INDEX IDX_C556964F51EFDBF ON tb_entidadecartaoconfig (id_entidadefinanceiro)
CREATE INDEX IDX_C556964FCD869197 ON tb_entidadecartaoconfig (id_cartaoconfig)
CREATE TABLE tb_premioproduto (id_premioproduto INT IDENTITY NOT NULL, id_usuariocadastro INT, id_produto INT NOT NULL, id_premio INT NOT NULL, id_campanhacomercial INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_premioproduto))
CREATE INDEX IDX_331FFAC62FF60C20 ON tb_premioproduto (id_usuariocadastro)
CREATE INDEX IDX_331FFAC68231E0A7 ON tb_premioproduto (id_produto)
CREATE INDEX IDX_331FFAC63A005945 ON tb_premioproduto (id_premio)
CREATE INDEX IDX_331FFAC616FD79A0 ON tb_premioproduto (id_campanhacomercial)
CREATE TABLE tb_esquemaconfiguracaoitem (id_esquemaconfiguracao INT NOT NULL, id_itemconfiguracao INT NOT NULL, st_valor NVARCHAR(200) NOT NULL, PRIMARY KEY (id_esquemaconfiguracao, id_itemconfiguracao))
CREATE INDEX IDX_40020F6978C3520B ON tb_esquemaconfiguracaoitem (id_esquemaconfiguracao)
CREATE INDEX IDX_40020F696B954C8B ON tb_esquemaconfiguracaoitem (id_itemconfiguracao)
CREATE TABLE tb_tipocalculoavaliacao (id_tipocalculoavaliacao INT IDENTITY NOT NULL, st_tipocalculoavaliacao NVARCHAR(100) NOT NULL, PRIMARY KEY (id_tipocalculoavaliacao))
CREATE TABLE tb_mensagemmotivacional (id_mensagemmotivacional INT IDENTITY NOT NULL, id_usuariocadastro INT, st_mensagemmotivacional VARCHAR(MAX) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_mensagemmotivacional))
CREATE INDEX IDX_DFFDC1762FF60C20 ON tb_mensagemmotivacional (id_usuariocadastro)
CREATE TABLE tb_entregamaterial (id_entregamaterial INT IDENTITY NOT NULL, dt_entrega DATETIME2(6), dt_devolucao DATETIME2(6), id_matricula INT NOT NULL, id_usuariocadastro INT NOT NULL, id_situacao INT NOT NULL, id_itemdematerial INT NOT NULL, id_pacote INT, bl_ativo BIT NOT NULL, id_matriculadisciplina INT NOT NULL, PRIMARY KEY (id_entregamaterial))
CREATE TABLE tb_tipoavaliacao (id_tipoavaliacao INT IDENTITY NOT NULL, st_tipoavaliacao NVARCHAR(200) NOT NULL, st_descricao NVARCHAR(300) NOT NULL, PRIMARY KEY (id_tipoavaliacao))
CREATE TABLE tb_avaliacaoagendamento (id_avaliacaoagendamento INT IDENTITY NOT NULL, id_avaliacaoaplicacao INT, id_matricula INT, id_situacao INT NOT NULL, id_usuariocadastro INT, id_avaliacao INT, id_usuario INT, id_entidade INT, dt_agendamento DATE, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, nu_presenca INT, id_tipodeavaliacao INT, bl_provaglobal INT, id_usuariolancamento INT, PRIMARY KEY (id_avaliacaoagendamento))
CREATE INDEX IDX_B82DDF509281E3AF ON tb_avaliacaoagendamento (id_avaliacaoaplicacao)
CREATE INDEX IDX_B82DDF5095EAA4A2 ON tb_avaliacaoagendamento (id_matricula)
CREATE INDEX IDX_B82DDF50C0694FE1 ON tb_avaliacaoagendamento (id_situacao)
CREATE INDEX IDX_B82DDF502FF60C20 ON tb_avaliacaoagendamento (id_usuariocadastro)
CREATE INDEX IDX_B82DDF50E92EFB05 ON tb_avaliacaoagendamento (id_avaliacao)
CREATE INDEX IDX_B82DDF50FCF8192D ON tb_avaliacaoagendamento (id_usuario)
CREATE INDEX IDX_B82DDF50B3F20A47 ON tb_avaliacaoagendamento (id_entidade)
CREATE TABLE tb_projetoentidade (id_projetoentidade INT IDENTITY NOT NULL, id_projetopedagogico INT NOT NULL, id_entidade INT NOT NULL, id_situacao INT NOT NULL, id_usuariocadastro INT NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, dt_inicio DATE NOT NULL, PRIMARY KEY (id_projetoentidade))
CREATE INDEX IDX_79FAAA845E2EAFF2 ON tb_projetoentidade (id_projetopedagogico)
CREATE INDEX IDX_79FAAA84B3F20A47 ON tb_projetoentidade (id_entidade)
CREATE INDEX IDX_79FAAA84C0694FE1 ON tb_projetoentidade (id_situacao)
CREATE INDEX IDX_79FAAA842FF60C20 ON tb_projetoentidade (id_usuariocadastro)
CREATE TABLE tb_localaula (id_localaula INT IDENTITY NOT NULL, id_catraca INT, st_localaula NVARCHAR(30) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, id_situacao INT NOT NULL, id_entidade INT NOT NULL, nu_maxalunos INT NOT NULL, PRIMARY KEY (id_localaula))
CREATE INDEX IDX_A8CB110CCA131A2C ON tb_localaula (id_catraca)
CREATE TABLE tb_perfilpermissaofuncionalidade (id_perfil INT NOT NULL, id_funcionalidade INT NOT NULL, id_permissao INT NOT NULL, PRIMARY KEY (id_perfil, id_funcionalidade, id_permissao))
CREATE TABLE tb_saladeaula (id_saladeaula INT IDENTITY NOT NULL, id_modalidadesaladeaula INT NOT NULL, id_tiposaladeaula INT NOT NULL, id_periodoletivo INT, id_situacao INT NOT NULL, id_categoriasala INT NOT NULL, id_usuariocancelamento INT, dt_cadastro DATETIME2(6) NOT NULL, st_saladeaula NVARCHAR(255) NOT NULL, bl_ativa BIT NOT NULL, id_usuariocadastro INT NOT NULL, dt_inicioinscricao DATE NOT NULL, dt_fiminscricao DATE, dt_abertura DATE, st_localizacao NVARCHAR(255), dt_encerramento DATE, id_entidade INT NOT NULL, bl_usardoperiodoletivo BIT NOT NULL, nu_maxalunos INT, nu_diasencerramento NUMERIC(10, 0), bl_semencerramento BIT NOT NULL, bl_todasentidades BIT, nu_diasaluno NUMERIC(10, 0), bl_semdiasaluno BIT NOT NULL, nu_diasextensao INT NOT NULL, st_pontosnegativos VARCHAR(MAX), st_pontospositivos VARCHAR(MAX), st_pontosmelhorar VARCHAR(MAX), st_pontosresgate VARCHAR(MAX), st_conclusoesfinais VARCHAR(MAX), st_justificativaacima VARCHAR(MAX), st_justificativaabaixo VARCHAR(MAX), dt_atualiza DATETIME2(6), dt_cancelamento DATETIME2(6), id_usuarioatualiza INT NOT NULL, PRIMARY KEY (id_saladeaula))
CREATE INDEX IDX_461F28F550A6D247 ON tb_saladeaula (id_modalidadesaladeaula)
CREATE INDEX IDX_461F28F56D176833 ON tb_saladeaula (id_tiposaladeaula)
CREATE INDEX IDX_461F28F5B3B7BA0A ON tb_saladeaula (id_periodoletivo)
CREATE INDEX IDX_461F28F5C0694FE1 ON tb_saladeaula (id_situacao)
CREATE INDEX IDX_461F28F5CA52AB42 ON tb_saladeaula (id_categoriasala)
CREATE INDEX IDX_461F28F59107A21B ON tb_saladeaula (id_usuariocancelamento)
CREATE TABLE tb_saladeaulaentidade (id_saladeaula INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_saladeaula, id_entidade))
CREATE TABLE tb_disciplinasaladeaula (id_disciplina INT NOT NULL, id_saladeaula INT NOT NULL, PRIMARY KEY (id_disciplina, id_saladeaula))
CREATE TABLE tb_cartacredito (id_cartacredito INT IDENTITY NOT NULL, id_usuariocadastro INT, id_situacao INT, id_usuario INT, id_cancelamento INT, id_entidade INT, nu_valororiginal NUMERIC(10, 0) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_cartacredito))
CREATE INDEX IDX_5DE4D7FE2FF60C20 ON tb_cartacredito (id_usuariocadastro)
CREATE INDEX IDX_5DE4D7FEC0694FE1 ON tb_cartacredito (id_situacao)
CREATE INDEX IDX_5DE4D7FEFCF8192D ON tb_cartacredito (id_usuario)
CREATE INDEX IDX_5DE4D7FE92BA0BD2 ON tb_cartacredito (id_cancelamento)
CREATE INDEX IDX_5DE4D7FEB3F20A47 ON tb_cartacredito (id_entidade)
CREATE TABLE tb_mensagemcobranca (id_mensagemcobranca INT IDENTITY NOT NULL, id_textosistema INT, id_entidade INT, nu_diasatraso INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, bl_antecedencia BIT NOT NULL, PRIMARY KEY (id_mensagemcobranca))
CREATE INDEX IDX_24166436136FA251 ON tb_mensagemcobranca (id_textosistema)
CREATE INDEX IDX_24166436B3F20A47 ON tb_mensagemcobranca (id_entidade)
CREATE TABLE tb_erro (id_erro INT IDENTITY NOT NULL, id_sistema INT, id_processo INT, id_usuario INT NOT NULL, id_entidade INT NOT NULL, st_mensagem NVARCHAR(255) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, st_origem NVARCHAR(255) NOT NULL, st_codigo NVARCHAR(255), bl_lido BIT NOT NULL, bl_corrigido BIT NOT NULL, PRIMARY KEY (id_erro))
CREATE INDEX IDX_E1BD01BF4F5F0211 ON tb_erro (id_sistema)
CREATE INDEX IDX_E1BD01BF463E54B8 ON tb_erro (id_processo)
ALTER TABLE tb_erro ADD CONSTRAINT DF_E1BD01BF_AE5A7FF7 DEFAULT '0' FOR bl_lido
ALTER TABLE tb_erro ADD CONSTRAINT DF_E1BD01BF_551C773F DEFAULT '0' FOR bl_corrigido
CREATE TABLE tb_venda (id_venda INT IDENTITY NOT NULL, id_formapagamento INT, id_campanhacomercial INT, id_campanhacomercialpremio INT, id_evolucao INT, id_situacao INT, id_usuario INT, id_atendente INT, id_tipocampanha INT, id_origemvenda INT, id_prevenda INT, id_protocolo INT, id_enderecoentrega INT, id_ocorrencia INT, id_uploadcontrato INT, id_cupom INT, id_campanhapontualidade INT, dt_cadastro DATETIME2(6), nu_descontoporcentagem NUMERIC(10, 5) NOT NULL, nu_descontovalor NUMERIC(10, 5) NOT NULL, nu_juros NUMERIC(10, 5) NOT NULL, bl_ativo BIT NOT NULL, id_usuariocadastro INT NOT NULL, nu_valorliquido NUMERIC(30, 2) NOT NULL, nu_valorbruto NUMERIC(30, 2) NOT NULL, id_entidade INT NOT NULL, nu_parcelas INT NOT NULL, bl_contrato BIT NOT NULL, nu_diamensalidade INT, dt_agendamento DATE, dt_confirmacao DATETIME2(6), id_contratoafiliado INT, st_observacao NVARCHAR(255), nu_valoratualizado NUMERIC(30, 2) NOT NULL, recorrente_orderid NVARCHAR(255) NOT NULL, nu_valorcartacredito NUMERIC(10, 0), PRIMARY KEY (id_venda))
CREATE INDEX IDX_AFCEACF4443FB97 ON tb_venda (id_formapagamento)
CREATE INDEX IDX_AFCEACF416FD79A0 ON tb_venda (id_campanhacomercial)
CREATE INDEX IDX_AFCEACF49D4E6E40 ON tb_venda (id_campanhacomercialpremio)
CREATE INDEX IDX_AFCEACF430B15C09 ON tb_venda (id_evolucao)
CREATE INDEX IDX_AFCEACF4C0694FE1 ON tb_venda (id_situacao)
CREATE INDEX IDX_AFCEACF4FCF8192D ON tb_venda (id_usuario)
CREATE INDEX IDX_AFCEACF4BAC6B6DF ON tb_venda (id_atendente)
CREATE INDEX IDX_AFCEACF4490DB0A9 ON tb_venda (id_tipocampanha)
CREATE INDEX IDX_AFCEACF4608A1C2 ON tb_venda (id_origemvenda)
CREATE INDEX IDX_AFCEACF4B960F69E ON tb_venda (id_prevenda)
CREATE INDEX IDX_AFCEACF4F098E264 ON tb_venda (id_protocolo)
CREATE INDEX IDX_AFCEACF4F36DE71B ON tb_venda (id_enderecoentrega)
CREATE INDEX IDX_AFCEACF479E459B6 ON tb_venda (id_ocorrencia)
CREATE INDEX IDX_AFCEACF4EA7023B6 ON tb_venda (id_uploadcontrato)
CREATE INDEX IDX_AFCEACF42F43E9C3 ON tb_venda (id_cupom)
CREATE INDEX IDX_AFCEACF47DCD6A89 ON tb_venda (id_campanhapontualidade)
CREATE TABLE tb_livroregistroentidade (id_livroregistroentidade INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_usuariocadastro INT NOT NULL, st_livroregistroentidade NVARCHAR(30) NOT NULL, nu_folhas NUMERIC(10, 0) NOT NULL, nu_registros NUMERIC(10, 0) NOT NULL, nu_registroinicial NUMERIC(10, 0) NOT NULL, PRIMARY KEY (id_livroregistroentidade))
CREATE TABLE tb_avaliacaoconjuntoreferencia (id_avaliacaoconjuntoreferencia INT IDENTITY NOT NULL, id_projetopedagogico INT, id_modulo INT, id_avaliacaoconjunto INT NOT NULL, id_saladeaula INT NOT NULL, dt_inicio DATE NOT NULL, dt_fim DATE, PRIMARY KEY (id_avaliacaoconjuntoreferencia))
CREATE INDEX IDX_68C497555E2EAFF2 ON tb_avaliacaoconjuntoreferencia (id_projetopedagogico)
CREATE INDEX IDX_68C49755CAC67ADB ON tb_avaliacaoconjuntoreferencia (id_modulo)
CREATE TABLE tb_areaprojetosala (id_areaprojetosala INT IDENTITY NOT NULL, id_projetopedagogico INT, id_nivelensino INT, id_saladeaula INT, id_areaconhecimento INT, nu_diasacesso INT, st_referencia NVARCHAR(200), PRIMARY KEY (id_areaprojetosala))
CREATE TABLE tb_tipotrilha (id_tipotrilha INT IDENTITY NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, st_tipotrilha NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipotrilha))
CREATE TABLE tb_lancamentovenda (id_venda INT NOT NULL, id_lancamento INT NOT NULL, bl_entrada BIT NOT NULL, nu_ordem INT, PRIMARY KEY (id_venda, id_lancamento))
CREATE INDEX IDX_7D25FC2E2BA0BD34 ON tb_lancamentovenda (id_venda)
CREATE INDEX IDX_7D25FC2EC319336A ON tb_lancamentovenda (id_lancamento)
CREATE TABLE tb_log (id_log INT IDENTITY NOT NULL, id_usuario INT, id_entidade INT, id_perfil INT, id_funcionalidade INT, id_operacao INT, dt_cadastro DATETIME2(6) NOT NULL, st_antes NVARCHAR(255) NOT NULL, st_depois NVARCHAR(255) NOT NULL, st_tabela NVARCHAR(255) NOT NULL, st_motivo NVARCHAR(255) NOT NULL, id_tabela INT NOT NULL, st_coluna NVARCHAR(255), PRIMARY KEY (id_log))
CREATE INDEX IDX_2193647AFCF8192D ON tb_log (id_usuario)
CREATE INDEX IDX_2193647AB3F20A47 ON tb_log (id_entidade)
CREATE INDEX IDX_2193647AB052C3AA ON tb_log (id_perfil)
CREATE INDEX IDX_2193647A5BC834DB ON tb_log (id_funcionalidade)
CREATE INDEX IDX_2193647A6391BFB6 ON tb_log (id_operacao)
CREATE TABLE tb_fundamentolegal (id_fundamentolegal INT IDENTITY NOT NULL, dt_publicacao DATE NOT NULL, dt_vigencia DATE, dt_cadastro DATETIME2(6) NOT NULL, id_situacao INT NOT NULL, id_usuariocadastro INT NOT NULL, id_tipofundamentolegal INT NOT NULL, id_entidade INT NOT NULL, id_nivelensino INT, bl_ativo BIT NOT NULL, nu_numero NVARCHAR(255) NOT NULL, st_orgaoexped NVARCHAR(255), PRIMARY KEY (id_fundamentolegal))
CREATE TABLE tb_acordorel (IDACORDO INT NOT NULL, IDLAN INT NOT NULL, CLASSIFICACAO INT NOT NULL, CODCOLIGADA INT NOT NULL, PRIMARY KEY (IDACORDO))
CREATE TABLE tb_projetopedagogicointegracao (id_projetointegracao INT IDENTITY NOT NULL, id_projetopedagogico INT NOT NULL, id_sistema INT, id_projetopedagogicosistema INT NOT NULL, PRIMARY KEY (id_projetointegracao))
CREATE INDEX IDX_F1E4C00A5E2EAFF2 ON tb_projetopedagogicointegracao (id_projetopedagogico)
CREATE INDEX IDX_F1E4C00A4F5F0211 ON tb_projetopedagogicointegracao (id_sistema)
CREATE TABLE tb_catraca (id_catraca INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), id_entidade INT NOT NULL, id_situacao INT NOT NULL, id_usuariocadastro INT NOT NULL, bl_ativo BIT NOT NULL, st_codigocatraca NVARCHAR(255), PRIMARY KEY (id_catraca))
CREATE TABLE tb_evolucao (id_evolucao INT IDENTITY NOT NULL, st_evolucao NVARCHAR(255) NOT NULL, st_tabela NVARCHAR(255) NOT NULL, st_campo NVARCHAR(255) NOT NULL, PRIMARY KEY (id_evolucao))
CREATE TABLE tb_enviomensagem (id_enviomensagem INT IDENTITY NOT NULL, id_mensagem INT, id_tipoenvio INT, id_evolucao INT, id_emailconfig INT, id_sistema INT, dt_cadastro DATETIME2(6) NOT NULL, dt_envio DATETIME2(6), dt_enviar DATETIME2(6) NOT NULL, dt_tentativa DATETIME2(6), PRIMARY KEY (id_enviomensagem))
CREATE INDEX IDX_EDACFD2FCE9EDE25 ON tb_enviomensagem (id_mensagem)
CREATE INDEX IDX_EDACFD2FA2782CAC ON tb_enviomensagem (id_tipoenvio)
CREATE INDEX IDX_EDACFD2F30B15C09 ON tb_enviomensagem (id_evolucao)
CREATE INDEX IDX_EDACFD2FDBED4F98 ON tb_enviomensagem (id_emailconfig)
CREATE INDEX IDX_EDACFD2F4F5F0211 ON tb_enviomensagem (id_sistema)
CREATE TABLE tb_pessoaendereco (id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_endereco INT NOT NULL, bl_padrao BIT, PRIMARY KEY (id_usuario))
CREATE INDEX IDX_8132E9C9B3F20A47 ON tb_pessoaendereco (id_entidade)
CREATE INDEX IDX_8132E9C9A83B3A9B ON tb_pessoaendereco (id_endereco)
CREATE TABLE tb_tipoprova (id_tipoprova INT IDENTITY NOT NULL, st_tipoprova NVARCHAR(255) NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, PRIMARY KEY (id_tipoprova))
CREATE TABLE tb_premio (id_premio INT IDENTITY NOT NULL, id_usuariocadastro INT, id_campanhacomercial INT NOT NULL, id_cupomcampanha INT NOT NULL, nu_comprasacima NUMERIC(10, 0), nu_compraunidade INT, nu_descontopremio NUMERIC(10, 0), id_tipodescontopremio INT, id_tipopremio INT, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, bl_compraacima BIT, bl_promocaocumulativa BIT, bl_compraunidade BIT, st_prefixocupompremio NVARCHAR(30), PRIMARY KEY (id_premio))
CREATE INDEX IDX_A1E0F5E42FF60C20 ON tb_premio (id_usuariocadastro)
CREATE INDEX IDX_A1E0F5E416FD79A0 ON tb_premio (id_campanhacomercial)
CREATE INDEX IDX_A1E0F5E4A5316601 ON tb_premio (id_cupomcampanha)
CREATE TABLE tb_situacaointegracao (id_situacaointegracao  INT IDENTITY NOT NULL, id_sistema INT, nu_status INT, st_situacao NVARCHAR(255) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_situacaointegracao ))
CREATE INDEX IDX_E8EA21474F5F0211 ON tb_situacaointegracao (id_sistema)
CREATE TABLE tb_turmaentidade (id_turmaentidade INT IDENTITY NOT NULL, id_usuariocadastro INT NOT NULL, id_turma INT, id_entidade INT, bl_ativo BIT, dt_cadastro DATETIME2(6), PRIMARY KEY (id_turmaentidade))
CREATE INDEX IDX_F52B178A2FF60C20 ON tb_turmaentidade (id_usuariocadastro)
CREATE INDEX IDX_F52B178AC5875896 ON tb_turmaentidade (id_turma)
CREATE INDEX IDX_F52B178AB3F20A47 ON tb_turmaentidade (id_entidade)
CREATE TABLE tb_turmaintegracao (id_turmaintegracao INT IDENTITY NOT NULL, id_turma INT NOT NULL, id_sistema INT, st_turmasistema NVARCHAR(255), PRIMARY KEY (id_turmaintegracao))
CREATE INDEX IDX_ED96AD7BC5875896 ON tb_turmaintegracao (id_turma)
CREATE INDEX IDX_ED96AD7B4F5F0211 ON tb_turmaintegracao (id_sistema)
CREATE TABLE tb_pa_boasvindas (id_pa_boasvindas INT IDENTITY NOT NULL, id_textosistema INT, id_entidade INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_pa_boasvindas))
CREATE INDEX IDX_6113D834136FA251 ON tb_pa_boasvindas (id_textosistema)
CREATE INDEX IDX_6113D834B3F20A47 ON tb_pa_boasvindas (id_entidade)
CREATE INDEX IDX_6113D8342FF60C20 ON tb_pa_boasvindas (id_usuariocadastro)
CREATE TABLE tb_vendaproduto (id_vendaproduto INT IDENTITY NOT NULL, id_produto INT, id_venda INT, id_tiposelecao INT, id_campanhacomercial INT, id_matricula INT, id_turma INT, id_produtocombo INT, id_situacao INT, dt_cadastro DATETIME2(6), dt_entrega DATETIME2(6), nu_desconto DOUBLE PRECISION, nu_valorliquido DOUBLE PRECISION, nu_valorbruto DOUBLE PRECISION, id_avaliacaoaplicacao INT, PRIMARY KEY (id_vendaproduto))
CREATE INDEX IDX_E16ED9618231E0A7 ON tb_vendaproduto (id_produto)
CREATE INDEX IDX_E16ED9612BA0BD34 ON tb_vendaproduto (id_venda)
CREATE INDEX IDX_E16ED961D70D9966 ON tb_vendaproduto (id_tiposelecao)
CREATE INDEX IDX_E16ED96116FD79A0 ON tb_vendaproduto (id_campanhacomercial)
CREATE INDEX IDX_E16ED96195EAA4A2 ON tb_vendaproduto (id_matricula)
CREATE INDEX IDX_E16ED961C5875896 ON tb_vendaproduto (id_turma)
CREATE INDEX IDX_E16ED9618958479A ON tb_vendaproduto (id_produtocombo)
CREATE INDEX IDX_E16ED961C0694FE1 ON tb_vendaproduto (id_situacao)
CREATE TABLE tb_entidaderesponsavellegal (id_entidaderesponsavellegal INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_tipoentidaderesponsavel INT NOT NULL, id_usuario INT NOT NULL, id_entidade INT NOT NULL, sg_uf NVARCHAR(2) NOT NULL, bl_padrao BIT NOT NULL, bl_ativo BIT NOT NULL, st_orgaoexpeditor NVARCHAR(10) NOT NULL, st_registro NVARCHAR(100) NOT NULL, PRIMARY KEY (id_entidaderesponsavellegal))
CREATE INDEX IDX_843DB550C0694FE1 ON tb_entidaderesponsavellegal (id_situacao)
CREATE INDEX IDX_843DB550F88F3E41 ON tb_entidaderesponsavellegal (id_tipoentidaderesponsavel)
CREATE INDEX IDX_843DB550FCF8192D ON tb_entidaderesponsavellegal (id_usuario)
CREATE INDEX IDX_843DB550B3F20A47 ON tb_entidaderesponsavellegal (id_entidade)
CREATE INDEX IDX_843DB5504FD882EB ON tb_entidaderesponsavellegal (sg_uf)
CREATE TABLE tb_motivo (id_motivo INT IDENTITY NOT NULL, id_entidadecadastro INT, id_usuariocadastro INT, id_situacao INT, bl_ativo BIT, st_motivo NVARCHAR(255), dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_motivo))
CREATE INDEX IDX_A42E212A43D3DAFE ON tb_motivo (id_entidadecadastro)
CREATE INDEX IDX_A42E212A2FF60C20 ON tb_motivo (id_usuariocadastro)
CREATE INDEX IDX_A42E212AC0694FE1 ON tb_motivo (id_situacao)
CREATE TABLE tb_planopagamento (id_planopagamento INT IDENTITY NOT NULL, id_produto INT NOT NULL, nu_parcelas INT NOT NULL, nu_valorparcela NUMERIC(10, 0) NOT NULL, nu_valorentrada NUMERIC(10, 0) NOT NULL, bl_padrao BIT NOT NULL, PRIMARY KEY (id_planopagamento))
CREATE TABLE tb_pa_dadoscadastrais (id_pa_dadoscadastrais INT IDENTITY NOT NULL, id_textomensagem INT, id_textoinstrucao INT, id_assuntoco INT, id_categoriaocorrencia INT, id_entidade INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_pa_dadoscadastrais))
CREATE INDEX IDX_AE3721C3B81EAF15 ON tb_pa_dadoscadastrais (id_textomensagem)
CREATE INDEX IDX_AE3721C3C88A814 ON tb_pa_dadoscadastrais (id_textoinstrucao)
CREATE INDEX IDX_AE3721C39B9DCB8E ON tb_pa_dadoscadastrais (id_assuntoco)
CREATE INDEX IDX_AE3721C3CA9E9832 ON tb_pa_dadoscadastrais (id_categoriaocorrencia)
CREATE INDEX IDX_AE3721C3B3F20A47 ON tb_pa_dadoscadastrais (id_entidade)
CREATE INDEX IDX_AE3721C32FF60C20 ON tb_pa_dadoscadastrais (id_usuariocadastro)
CREATE TABLE tb_produtoimagem (id_produtoimagem INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_upload INT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_produtoimagem))
CREATE TABLE tb_tipolivro (id_tipolivro INT IDENTITY NOT NULL, st_tipolivro NVARCHAR(20), PRIMARY KEY (id_tipolivro))
CREATE TABLE tb_prevenda (id_prevenda INT IDENTITY NOT NULL, id_formapagamento INT, id_meiopagamento INT, id_tipoendereco INT, id_nivelensino INT, id_situacao INT, id_usuarioatendimento INT, id_usuariodescarte INT, dt_nascimento DATE, dt_cadastro DATETIME2(6), id_usuario INT, id_entidade INT, nu_dddcelular INT, id_paisnascimento INT, nu_lancamentos INT, nu_ddicelular INT, nu_dddresidencial INT, nu_ddiresidencial INT, nu_dddcomercial INT, nu_ddicomercial INT, id_pais INT, bl_ativo BIT, id_municipio INT, id_municipionascimento INT NOT NULL, nu_juros INT, nu_desconto INT, nu_valorliquido INT, st_nomecompleto VARCHAR(MAX), st_email NVARCHAR(255), nu_telefonecelular NVARCHAR(8), nu_telefoneresidencial NVARCHAR(8), nu_telefonecomercial NVARCHAR(8), st_orgaoexpeditor NVARCHAR(80), st_instituicao VARCHAR(MAX), st_graduacao VARCHAR(MAX), st_localtrabalho VARCHAR(MAX), st_recomendacao VARCHAR(MAX), st_endereco VARCHAR(MAX), st_bairro NVARCHAR(255), st_complemento VARCHAR(MAX), nu_numero NVARCHAR(10), st_rg NVARCHAR(20), st_contato VARCHAR(MAX), st_cpf NVARCHAR(11), sg_uf NVARCHAR(2), st_cep NVARCHAR(8), st_sexo NVARCHAR(1), sg_ufnascimento NVARCHAR(2), dt_atendimento DATETIME2(6), dt_descarte DATETIME2(6), PRIMARY KEY (id_prevenda))
CREATE INDEX IDX_D11C0F15443FB97 ON tb_prevenda (id_formapagamento)
CREATE INDEX IDX_D11C0F159B119E27 ON tb_prevenda (id_meiopagamento)
CREATE INDEX IDX_D11C0F15BC9063A1 ON tb_prevenda (id_tipoendereco)
CREATE INDEX IDX_D11C0F156DA54E76 ON tb_prevenda (id_nivelensino)
CREATE INDEX IDX_D11C0F15C0694FE1 ON tb_prevenda (id_situacao)
CREATE INDEX IDX_D11C0F152CFC0759 ON tb_prevenda (id_usuarioatendimento)
CREATE INDEX IDX_D11C0F15FD7DED57 ON tb_prevenda (id_usuariodescarte)
CREATE TABLE tb_logacesso (id_logacesso INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_funcionalidade INT NOT NULL, id_perfil INT, id_usuario INT NOT NULL, id_saladeaula INT, id_entidade INT NOT NULL, PRIMARY KEY (id_logacesso))
CREATE TABLE tb_categoria (id_categoria INT IDENTITY NOT NULL, id_usuariocadastro INT NOT NULL, id_uploadimagem INT, id_categoriapai INT, id_situacao INT, id_entidadecadastro INT NOT NULL, st_nomeexibicao NVARCHAR(255) NOT NULL, st_categoria NVARCHAR(255) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT, PRIMARY KEY (id_categoria))
CREATE INDEX IDX_B427885B2FF60C20 ON tb_categoria (id_usuariocadastro)
CREATE INDEX IDX_B427885B9AB457F8 ON tb_categoria (id_uploadimagem)
CREATE INDEX IDX_B427885B952FF260 ON tb_categoria (id_categoriapai)
CREATE INDEX IDX_B427885BC0694FE1 ON tb_categoria (id_situacao)
CREATE TABLE tb_tipodisciplina (id_tipodisciplina INT IDENTITY NOT NULL, st_tipodisciplina NVARCHAR(255) NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, PRIMARY KEY (id_tipodisciplina))
CREATE TABLE tb_contatostelefonepessoa (id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_telefone INT NOT NULL, bl_padrao BIT, PRIMARY KEY (id_usuario, id_entidade, id_telefone))
CREATE TABLE tb_formatoarquivo (id_formatoarquivo INT NOT NULL, st_formatoarquivo NVARCHAR(4) NOT NULL, st_extensaoarquivo NVARCHAR(4) NOT NULL, PRIMARY KEY (id_formatoarquivo))
CREATE TABLE tb_formadiavencimento (id_formadiavencimento INT IDENTITY NOT NULL, id_formapagamento INT, nu_diavencimento INT, PRIMARY KEY (id_formadiavencimento))
CREATE INDEX IDX_C61A156E443FB97 ON tb_formadiavencimento (id_formapagamento)
CREATE TABLE tb_tipodestinatario (id_tipodestinatario INT IDENTITY NOT NULL, st_tipodestinatario NVARCHAR(20), PRIMARY KEY (id_tipodestinatario))
CREATE TABLE tb_itemconfiguracao (id_itemconfiguracao INT IDENTITY NOT NULL, st_itemconfiguracao NVARCHAR(100) NOT NULL, st_descricao NVARCHAR(255) NOT NULL, st_default NVARCHAR(200) NOT NULL, PRIMARY KEY (id_itemconfiguracao))
CREATE TABLE tb_arquivoretornomaterial (id_arquivoretornomaterial INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_usuariocadastro INT, id_situacao INT, id_upload INT, st_arquivoretornomaterial NVARCHAR(500) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_arquivoretornomaterial))
CREATE INDEX IDX_5914CF94B3F20A47 ON tb_arquivoretornomaterial (id_entidade)
CREATE INDEX IDX_5914CF942FF60C20 ON tb_arquivoretornomaterial (id_usuariocadastro)
CREATE INDEX IDX_5914CF94C0694FE1 ON tb_arquivoretornomaterial (id_situacao)
CREATE INDEX IDX_5914CF94318A53F2 ON tb_arquivoretornomaterial (id_upload)
CREATE TABLE tb_aplicadorprovaentidade (id_aplicadorprovaentidade INT IDENTITY NOT NULL, id_aplicadorprova INT, id_entidade INT, PRIMARY KEY (id_aplicadorprovaentidade))
CREATE INDEX IDX_8802BE6667F8C046 ON tb_aplicadorprovaentidade (id_aplicadorprova)
CREATE INDEX IDX_8802BE66B3F20A47 ON tb_aplicadorprovaentidade (id_entidade)
CREATE TABLE tb_assuntoco (id_assuntoco INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_assuntocopai INT, id_entidadecadastro INT, id_usuariocadastro INT, id_tipoocorrencia INT, id_textosistema INT, dt_cadastro DATETIME2(6) NOT NULL, bl_autodistribuicao BIT NOT NULL, bl_abertura BIT NOT NULL, bl_ativo BIT NOT NULL, bl_cancelamento BIT, bl_trancamento BIT, st_assuntoco NVARCHAR(200) NOT NULL, PRIMARY KEY (id_assuntoco))
CREATE INDEX IDX_E19FEDDFC0694FE1 ON tb_assuntoco (id_situacao)
CREATE INDEX IDX_E19FEDDFAE23E535 ON tb_assuntoco (id_assuntocopai)
CREATE INDEX IDX_E19FEDDF43D3DAFE ON tb_assuntoco (id_entidadecadastro)
CREATE INDEX IDX_E19FEDDF2FF60C20 ON tb_assuntoco (id_usuariocadastro)
CREATE INDEX IDX_E19FEDDF4EFA7A34 ON tb_assuntoco (id_tipoocorrencia)
CREATE INDEX IDX_E19FEDDF136FA251 ON tb_assuntoco (id_textosistema)
CREATE TABLE tb_boletoconfig (id_boletoconfig INT IDENTITY NOT NULL, id_contaentidade INT NOT NULL, id_entidadeendereco INT, nu_carteira NVARCHAR(6) NOT NULL, st_contacedente NVARCHAR(15), st_digitocedente NVARCHAR(2), PRIMARY KEY (id_boletoconfig))
CREATE INDEX IDX_F27C882D68B0E68E ON tb_boletoconfig (id_contaentidade)
CREATE INDEX IDX_F27C882D70F58862 ON tb_boletoconfig (id_entidadeendereco)
CREATE TABLE tb_formapagamentoparcela (id_formapagamentoparcela INT IDENTITY NOT NULL, id_meiopagamento INT, id_formapagamento INT NOT NULL, nu_parcelaquantidademin INT NOT NULL, nu_parcelaquantidademax INT NOT NULL, nu_juros NUMERIC(10, 0), nu_valormin NUMERIC(10, 0), PRIMARY KEY (id_formapagamentoparcela))
CREATE INDEX IDX_C8489A619B119E27 ON tb_formapagamentoparcela (id_meiopagamento)
CREATE TABLE tb_tipoformapagamentoparcela (id_tipoformapagamentoparcela INT IDENTITY NOT NULL, st_tipoformapagamentoparcela NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipoformapagamentoparcela))
CREATE TABLE tb_estadocivil (id_estadocivil INT IDENTITY NOT NULL, st_estadocivil NVARCHAR(255) NOT NULL, PRIMARY KEY (id_estadocivil))
CREATE TABLE tb_produtocombo (id_produtocombo INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_produtoitem INT NOT NULL, id_usuario INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, nu_descontoporcentagem NUMERIC(10, 0), PRIMARY KEY (id_produtocombo))
CREATE INDEX IDX_C9F6E16F8231E0A7 ON tb_produtocombo (id_produto)
CREATE INDEX IDX_C9F6E16FA3AE31BC ON tb_produtocombo (id_produtoitem)
CREATE INDEX IDX_C9F6E16FFCF8192D ON tb_produtocombo (id_usuario)
CREATE TABLE tb_trilha (id_trilha INT IDENTITY NOT NULL, nu_disciplinassimultaneasmai NUMERIC(10, 0), nu_disciplinassimultaneaspendencia NUMERIC(10, 0), nu_disciplinassimultaneasmen NUMERIC(10, 0), nu_disciplinaspendencia NUMERIC(10, 0), id_tipotrilhafixa INT NOT NULL, PRIMARY KEY (id_trilha))
CREATE INDEX IDX_8FC67E3B17B6B119 ON tb_trilha (id_tipotrilhafixa)
CREATE TABLE tb_itemdematerialpresencial (id_material INT IDENTITY NOT NULL, id_entidade INT, id_turma INT, id_disciplina INT, id_usuariocadastro INT, id_professor INT, id_tipomaterial INT, id_situacao INT, nu_paginas INT, nu_encontro INT, nu_qtdestoque INT, id_upload INT, dt_cadastro DATETIME2(6) NOT NULL, nu_valor NUMERIC(10, 0) NOT NULL, st_nomematerial NVARCHAR(255) NOT NULL, bl_ativo BIT, bl_portal BIT, PRIMARY KEY (id_material))
CREATE INDEX IDX_AB057D42B3F20A47 ON tb_itemdematerialpresencial (id_entidade)
CREATE INDEX IDX_AB057D42C5875896 ON tb_itemdematerialpresencial (id_turma)
CREATE INDEX IDX_AB057D42D759AAF1 ON tb_itemdematerialpresencial (id_disciplina)
CREATE INDEX IDX_AB057D422FF60C20 ON tb_itemdematerialpresencial (id_usuariocadastro)
CREATE INDEX IDX_AB057D42F9386BC4 ON tb_itemdematerialpresencial (id_professor)
CREATE INDEX IDX_AB057D4238CEC03A ON tb_itemdematerialpresencial (id_tipomaterial)
CREATE INDEX IDX_AB057D42C0694FE1 ON tb_itemdematerialpresencial (id_situacao)
CREATE TABLE tb_produtoarea (id_produtoarea INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_areaconhecimento INT NOT NULL, PRIMARY KEY (id_produtoarea))
CREATE INDEX IDX_1A8CBA208231E0A7 ON tb_produtoarea (id_produto)
CREATE INDEX IDX_1A8CBA20ED4F74A0 ON tb_produtoarea (id_areaconhecimento)
CREATE TABLE tb_enviodestinatario (id_enviodestinatario INT IDENTITY NOT NULL, id_tipodestinatario INT, id_enviomensagem INT, id_usuario INT, id_matricula INT, id_evolucao INT NOT NULL, nu_telefone INT, st_endereco NVARCHAR(100), st_nome NVARCHAR(100), id_sistema INT, PRIMARY KEY (id_enviodestinatario))
CREATE INDEX IDX_9E59D92B2F2A5850 ON tb_enviodestinatario (id_tipodestinatario)
CREATE INDEX IDX_9E59D92B203B551A ON tb_enviodestinatario (id_enviomensagem)
CREATE INDEX IDX_9E59D92BFCF8192D ON tb_enviodestinatario (id_usuario)
CREATE INDEX IDX_9E59D92B95EAA4A2 ON tb_enviodestinatario (id_matricula)
CREATE TABLE tb_avaliacao (id_avaliacao INT IDENTITY NOT NULL, id_tipoavaliacao INT, id_usuariocadastro INT, id_entidade INT, st_avaliacao NVARCHAR(100) NOT NULL, nu_valor INT, dt_cadastro DATETIME2(6) NOT NULL, id_situacao INT, st_descricao NVARCHAR(255), nu_quantquestoes INT, st_linkreferencia NVARCHAR(500), bl_recuperacao BIT, PRIMARY KEY (id_avaliacao))
CREATE INDEX IDX_932CDD542F3689EE ON tb_avaliacao (id_tipoavaliacao)
CREATE INDEX IDX_932CDD542FF60C20 ON tb_avaliacao (id_usuariocadastro)
CREATE INDEX IDX_932CDD54B3F20A47 ON tb_avaliacao (id_entidade)
CREATE TABLE tb_campanhadesconto (id_campanhadesconto INT IDENTITY NOT NULL, id_campanhacomercial INT NOT NULL, id_meiopagamento INT NOT NULL, nu_valormin NUMERIC(10, 0) NOT NULL, nu_valormax NUMERIC(10, 0) NOT NULL, nu_descontominimo NUMERIC(10, 0) NOT NULL, nu_descontomaximo NUMERIC(10, 0) NOT NULL, PRIMARY KEY (id_campanhadesconto))
CREATE INDEX IDX_1E9F1F3316FD79A0 ON tb_campanhadesconto (id_campanhacomercial)
CREATE INDEX IDX_1E9F1F339B119E27 ON tb_campanhadesconto (id_meiopagamento)
CREATE TABLE tb_notificacao (id_notificacao INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), st_notificacao NVARCHAR(50), PRIMARY KEY (id_notificacao))
CREATE TABLE tb_informacaoacademicapessoa (id_informacaoacademicapessoa INT IDENTITY NOT NULL, id_usuario INT, id_entidade INT, id_nivelensino INT, st_curso NVARCHAR(400) NOT NULL, st_nomeinstituicao NVARCHAR(400) NOT NULL, PRIMARY KEY (id_informacaoacademicapessoa))
CREATE INDEX IDX_D52299ECFCF8192D ON tb_informacaoacademicapessoa (id_usuario)
CREATE INDEX IDX_D52299ECB3F20A47 ON tb_informacaoacademicapessoa (id_entidade)
CREATE INDEX IDX_D52299EC6DA54E76 ON tb_informacaoacademicapessoa (id_nivelensino)
CREATE TABLE tb_tipocampo (id_tipocampo INT IDENTITY NOT NULL, st_tipocampo NVARCHAR(30) NOT NULL, PRIMARY KEY (id_tipocampo))
CREATE TABLE tb_tipolancamento (id_tipolancamento INT IDENTITY NOT NULL, st_tipolancamento NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipolancamento))
CREATE TABLE tb_carreira (id_carreira INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, st_carreira NVARCHAR(200) NOT NULL, PRIMARY KEY (id_carreira))
CREATE TABLE tb_pacote (id_pacote INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT, id_matricula INT, id_situacao INT, id_lotematerial INT, PRIMARY KEY (id_pacote))
CREATE TABLE tb_propriedadecamporelatorio (id_camporelatorio INT NOT NULL, id_tipopropriedadecamporel INT NOT NULL, st_valor NVARCHAR(255) NOT NULL, PRIMARY KEY (id_camporelatorio, id_tipopropriedadecamporel))
CREATE TABLE tb_nucleoco (id_nucleoco INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_tipoocorrencia INT, id_entidade INT NOT NULL, id_usuariocadastro INT NOT NULL, id_situacao INT, id_textonotificacao INT, nu_horasmeta INT, id_nucleofinalidade INT NOT NULL, bl_ativo BIT NOT NULL, bl_notificaatendente BIT NOT NULL, bl_notificaresponsavel BIT NOT NULL, st_nucleoco NVARCHAR(150) NOT NULL, st_corinteracao NVARCHAR(10), st_cordevolvida NVARCHAR(10), st_coratrasada NVARCHAR(10), st_cordefault NVARCHAR(10), PRIMARY KEY (id_nucleoco))
CREATE TABLE tb_produtoprojetopedagogico (id_produtoprojetopedagogico INT IDENTITY NOT NULL, id_projetopedagogico INT NOT NULL, id_produto INT NOT NULL, id_entidade INT NOT NULL, id_turma INT NOT NULL, nu_tempoacesso INT, bl_indeterminado BIT NOT NULL, nu_cargahoraria INT, st_disciplina NVARCHAR(255), PRIMARY KEY (id_produtoprojetopedagogico))
CREATE INDEX IDX_C5BCA3D35E2EAFF2 ON tb_produtoprojetopedagogico (id_projetopedagogico)
CREATE INDEX IDX_C5BCA3D38231E0A7 ON tb_produtoprojetopedagogico (id_produto)
CREATE INDEX IDX_C5BCA3D3B3F20A47 ON tb_produtoprojetopedagogico (id_entidade)
CREATE INDEX IDX_C5BCA3D3C5875896 ON tb_produtoprojetopedagogico (id_turma)
CREATE TABLE tb_horarioaula (id_horarioaula INT IDENTITY NOT NULL, id_turno INT, id_entidade INT, id_codhorarioacesso INT, st_codhorarioacesso NVARCHAR(100), st_horarioaula NVARCHAR(30) NOT NULL, hr_inicio DATETIME2(6) NOT NULL, hr_fim DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_horarioaula))
CREATE INDEX IDX_67E4BCB39122652 ON tb_horarioaula (id_turno)
CREATE INDEX IDX_67E4BCB3B3F20A47 ON tb_horarioaula (id_entidade)
CREATE TABLE tb_tipoconexaoemail (id_tipoconexaoemail INT IDENTITY NOT NULL, st_tipoconexaoemail NVARCHAR(20) NOT NULL, PRIMARY KEY (id_tipoconexaoemail))
CREATE TABLE tb_tramiteocorrencia (id_tramiteocorrencia INT IDENTITY NOT NULL, id_tramite INT NOT NULL, id_ocorrencia INT NOT NULL, bl_interessado BIT, PRIMARY KEY (id_tramiteocorrencia))
CREATE TABLE tb_areaconhecimentoserienivelensino (id_serie INT NOT NULL, id_areaconhecimento INT NOT NULL, id_nivelensino INT NOT NULL, PRIMARY KEY (id_serie))
CREATE TABLE tb_modulo (id_modulo INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_projetopedagogico INT, st_modulo NVARCHAR(255) NOT NULL, st_tituloexibicao NVARCHAR(255) NOT NULL, bl_ativo BIT NOT NULL, st_descricao NVARCHAR(500), id_moduloanterior INT, PRIMARY KEY (id_modulo))
CREATE INDEX IDX_5126D67AC0694FE1 ON tb_modulo (id_situacao)
CREATE INDEX IDX_5126D67A5E2EAFF2 ON tb_modulo (id_projetopedagogico)
CREATE TABLE tb_medidatempoconclusao (id_medidatempoconclusao INT IDENTITY NOT NULL, st_medidatempoconclusao NVARCHAR(50), PRIMARY KEY (id_medidatempoconclusao))
CREATE TABLE tb_livrocolecao (id_livrocolecao INT IDENTITY NOT NULL, id_entidadecadastro INT, id_usuariocadastro INT, st_livrocolecao NVARCHAR(200) NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_livrocolecao))
CREATE TABLE tb_itemgradehorariaturma (id_itemgradehorariaturma INT IDENTITY NOT NULL, id_itemgradehoraria INT NOT NULL, id_turma INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_itemgradehorariaturma))
CREATE INDEX IDX_DDA88509E323A720 ON tb_itemgradehorariaturma (id_itemgradehoraria)
CREATE INDEX IDX_DDA88509C5875896 ON tb_itemgradehorariaturma (id_turma)
CREATE TABLE tb_materialprojeto (id_materialprojeto INT IDENTITY NOT NULL, id_projetopedagogico INT NOT NULL, id_usuariocadastro INT, id_itemdematerial INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_materialprojeto))
CREATE INDEX IDX_5B36165F5E2EAFF2 ON tb_materialprojeto (id_projetopedagogico)
CREATE TABLE tb_processo (id_processo INT IDENTITY NOT NULL, st_processo NVARCHAR(50) NOT NULL, st_classe NVARCHAR(150) NOT NULL, PRIMARY KEY (id_processo))
CREATE TABLE tb_produtocarreira (id_produtocarreira INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_carreira INT NOT NULL, PRIMARY KEY (id_produtocarreira))
CREATE INDEX IDX_6229C6A28231E0A7 ON tb_produtocarreira (id_produto)
CREATE INDEX IDX_6229C6A2EFE671B4 ON tb_produtocarreira (id_carreira)
CREATE TABLE tb_titulacao (id_titulacao INT IDENTITY NOT NULL, st_titulacao NVARCHAR(200) NOT NULL, PRIMARY KEY (id_titulacao))
CREATE TABLE tb_tipoareaconhecimento (id_tipoareaconhecimento INT IDENTITY NOT NULL, st_tipoareaconhecimento NVARCHAR(100) NOT NULL, st_descricao NVARCHAR(300) NOT NULL, PRIMARY KEY (id_tipoareaconhecimento))
CREATE TABLE tb_tramiteagendamento (id_tramiteagendamento INT IDENTITY NOT NULL, id_tramite INT NOT NULL, id_avaliacaoagendamento INT NOT NULL, id_situacao INT NOT NULL, PRIMARY KEY (id_tramiteagendamento))
CREATE TABLE tb_textovariaveis (id_textovariaveis INT IDENTITY NOT NULL, id_textocategoria INT NOT NULL, id_tipotextovariavel INT, st_camposubstituir NVARCHAR(100), st_textovariaveis NVARCHAR(100) NOT NULL, st_mascara NVARCHAR(100), PRIMARY KEY (id_textovariaveis))
CREATE INDEX IDX_430962BE88A1ED7 ON tb_textovariaveis (id_textocategoria)
CREATE INDEX IDX_430962BFF3097B2 ON tb_textovariaveis (id_tipotextovariavel)
CREATE TABLE tb_produtotextosistema (id_produtotextosistema INT IDENTITY NOT NULL, id_textosistema INT NOT NULL, id_entidade INT NOT NULL, id_produto INT NOT NULL, id_upload INT NOT NULL, id_formadisponibilizacao INT NOT NULL, PRIMARY KEY (id_produtotextosistema))
CREATE TABLE tb_areaentidade (id_areaconhecimento INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_areaconhecimento, id_entidade))
CREATE TABLE tb_categoriaocorrencia (id_categoriaocorrencia INT IDENTITY NOT NULL, id_situacao INT, id_tipoocorrencia INT NOT NULL, id_usuariocadastro INT, id_entidadecadastro INT, st_categoriaocorrencia NVARCHAR(250) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_categoriaocorrencia))
CREATE INDEX IDX_679E0675C0694FE1 ON tb_categoriaocorrencia (id_situacao)
CREATE INDEX IDX_679E06754EFA7A34 ON tb_categoriaocorrencia (id_tipoocorrencia)
CREATE INDEX IDX_679E06752FF60C20 ON tb_categoriaocorrencia (id_usuariocadastro)
CREATE INDEX IDX_679E067543D3DAFE ON tb_categoriaocorrencia (id_entidadecadastro)
CREATE TABLE tb_entregadeclaracao (id_entregadeclaracao INT IDENTITY NOT NULL, id_situacao INT, id_vendaproduto INT, dt_solicitacao DATETIME2(6), dt_envio DATETIME2(6), dt_entrega DATETIME2(6), dt_cadastro DATETIME2(6) NOT NULL, id_matricula INT NOT NULL, id_venda INT, id_textosistema INT NOT NULL, id_usuariocadastro INT NOT NULL, PRIMARY KEY (id_entregadeclaracao))
CREATE INDEX IDX_C0A6DFC2C0694FE1 ON tb_entregadeclaracao (id_situacao)
CREATE INDEX IDX_C0A6DFC2A1C07F94 ON tb_entregadeclaracao (id_vendaproduto)
CREATE TABLE tb_cartaoconfig (id_cartaoconfig INT IDENTITY NOT NULL, id_cartaobandeira INT NOT NULL, id_cartaooperadora INT NOT NULL, id_sistema INT NOT NULL, id_contaentidade INT NOT NULL, st_gateway NVARCHAR(50), st_contratooperadora NVARCHAR(50), PRIMARY KEY (id_cartaoconfig))
CREATE INDEX IDX_8D283762797090DD ON tb_cartaoconfig (id_cartaobandeira)
CREATE INDEX IDX_8D283762F4810C88 ON tb_cartaoconfig (id_cartaooperadora)
CREATE INDEX IDX_8D2837624F5F0211 ON tb_cartaoconfig (id_sistema)
CREATE TABLE tb_regrapagamento (id_regrapagamento INT IDENTITY NOT NULL, id_tiporegrapagamento INT, id_entidade INT, id_areaconhecimento INT, id_projetopedagogico INT, id_professor INT, id_cargahoraria INT, id_tipodisciplina INT, nu_valor DOUBLE PRECISION NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_regrapagamento))
CREATE INDEX IDX_BEC9D7ACF91098FE ON tb_regrapagamento (id_tiporegrapagamento)
CREATE INDEX IDX_BEC9D7ACB3F20A47 ON tb_regrapagamento (id_entidade)
CREATE INDEX IDX_BEC9D7ACED4F74A0 ON tb_regrapagamento (id_areaconhecimento)
CREATE INDEX IDX_BEC9D7AC5E2EAFF2 ON tb_regrapagamento (id_projetopedagogico)
CREATE INDEX IDX_BEC9D7ACF9386BC4 ON tb_regrapagamento (id_professor)
CREATE INDEX IDX_BEC9D7ACD8F1FC2A ON tb_regrapagamento (id_cargahoraria)
CREATE INDEX IDX_BEC9D7ACE0478973 ON tb_regrapagamento (id_tipodisciplina)
CREATE TABLE tb_tipocalculojuros (id_tipocalculojuros INT IDENTITY NOT NULL, st_tipocalculojuros NVARCHAR(100) NOT NULL, PRIMARY KEY (id_tipocalculojuros))
CREATE TABLE tb_tipocampanha (id_tipocampanha INT IDENTITY NOT NULL, st_tipocampanha NVARCHAR(250), bl_ativo BIT NOT NULL, PRIMARY KEY (id_tipocampanha))
CREATE TABLE tb_produtovalor (id_produtovalor INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_usuariocadastro INT NOT NULL, id_tipoprodutovalor INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, dt_termino DATETIME2(6) NOT NULL, dt_inicio DATETIME2(6) NOT NULL, nu_valor NUMERIC(10, 0) NOT NULL, nu_valormensal NUMERIC(10, 0) NOT NULL, nu_basepropor NUMERIC(10, 0) NOT NULL, PRIMARY KEY (id_produtovalor))
CREATE INDEX IDX_5643F60D8231E0A7 ON tb_produtovalor (id_produto)
CREATE INDEX IDX_5643F60D2FF60C20 ON tb_produtovalor (id_usuariocadastro)
CREATE INDEX IDX_5643F60DBD3FEC5D ON tb_produtovalor (id_tipoprodutovalor)
CREATE TABLE tb_transacaofinanceira (id_transacaofinanceira  INT IDENTITY NOT NULL, id_usuariocadastro INT, id_venda INT, un_transacaofinanceira UNIQUEIDENTIFIER NOT NULL, st_codtransacaogateway NVARCHAR(100) NOT NULL, st_codtransacaooperadora NVARCHAR(100) NOT NULL, nu_status INT, dt_cadastro DATETIME2(6) NOT NULL, id_cartaobandeira INT, id_cartaooperadora INT, nu_verificacao INT, un_orderid UNIQUEIDENTIFIER NOT NULL, st_mensagem VARCHAR(MAX) NOT NULL, id_situacaointegracao INT, st_titularcartao NVARCHAR(255), st_ultimosdigitos NVARCHAR(4), nu_parcelas INT NOT NULL, nu_valorcielo NUMERIC(10, 2) NOT NULL, PRIMARY KEY (id_transacaofinanceira ))
CREATE INDEX IDX_DE3D9D2FF60C20 ON tb_transacaofinanceira (id_usuariocadastro)
CREATE INDEX IDX_DE3D9D2BA0BD34 ON tb_transacaofinanceira (id_venda)
CREATE TABLE tb_usuarioperfilentidadereferencia (id_perfilreferencia INT IDENTITY NOT NULL, id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_perfil INT NOT NULL, id_areaconhecimento INT, id_projetopedagogico INT, id_saladeaula INT, id_disciplina INT, id_livro INT, id_titulacao INT, bl_ativo BIT NOT NULL, bl_titular BIT NOT NULL, bl_autor BIT, nu_porcentagem NUMERIC(10, 0), dt_inicio DATETIME2(6) NOT NULL, dt_fim DATETIME2(6) NOT NULL, bl_desativarmoodle BIT, PRIMARY KEY (id_perfilreferencia))
CREATE INDEX IDX_41431867FCF8192D ON tb_usuarioperfilentidadereferencia (id_usuario)
CREATE INDEX IDX_41431867B3F20A47 ON tb_usuarioperfilentidadereferencia (id_entidade)
CREATE INDEX IDX_41431867B052C3AA ON tb_usuarioperfilentidadereferencia (id_perfil)
CREATE INDEX IDX_41431867ED4F74A0 ON tb_usuarioperfilentidadereferencia (id_areaconhecimento)
CREATE INDEX IDX_414318675E2EAFF2 ON tb_usuarioperfilentidadereferencia (id_projetopedagogico)
CREATE INDEX IDX_414318675A094BB1 ON tb_usuarioperfilentidadereferencia (id_saladeaula)
CREATE INDEX IDX_41431867D759AAF1 ON tb_usuarioperfilentidadereferencia (id_disciplina)
CREATE INDEX IDX_41431867A233E7B7 ON tb_usuarioperfilentidadereferencia (id_livro)
CREATE INDEX IDX_41431867733DB0F6 ON tb_usuarioperfilentidadereferencia (id_titulacao)
CREATE TABLE tb_textoexibicao (id_textoexibicao INT IDENTITY NOT NULL, st_textoexibicao NVARCHAR(200) NOT NULL, st_descricao NVARCHAR(300) NOT NULL, PRIMARY KEY (id_textoexibicao))
CREATE TABLE tb_contrato (id_contrato INT IDENTITY NOT NULL, id_evolucao INT NOT NULL, id_situacao INT NOT NULL, id_venda INT, id_contratoregra INT, dt_ativacao DATE, dt_termino DATE, dt_cadastro DATETIME2(6) NOT NULL, id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_usuariocadastro INT NOT NULL, id_textosistema INT, nu_codintegracao INT, bl_ativo BIT NOT NULL, nu_bolsa NUMERIC(10, 0), PRIMARY KEY (id_contrato))
CREATE INDEX IDX_5ECE703D30B15C09 ON tb_contrato (id_evolucao)
CREATE INDEX IDX_5ECE703DC0694FE1 ON tb_contrato (id_situacao)
CREATE INDEX IDX_5ECE703D2BA0BD34 ON tb_contrato (id_venda)
CREATE INDEX IDX_5ECE703D63FC6B92 ON tb_contrato (id_contratoregra)
CREATE TABLE tb_projetocontratoduracaotipo (id_projetocontratoduracaotipo INT IDENTITY NOT NULL, st_projetocontratoduracaotipo NVARCHAR(255) NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, PRIMARY KEY (id_projetocontratoduracaotipo))
CREATE TABLE tb_horariodiasemana (id_diasemana INT NOT NULL, id_horarioaula INT NOT NULL, PRIMARY KEY (id_diasemana, id_horarioaula))
CREATE INDEX IDX_1CBC338F18209A9C ON tb_horariodiasemana (id_diasemana)
CREATE INDEX IDX_1CBC338F16492F59 ON tb_horariodiasemana (id_horarioaula)
CREATE TABLE tb_entregadocumentos (id_entregadocumentos INT IDENTITY NOT NULL, id_documentos INT, id_matricula INT, id_situacao INT, id_contratoresponsavel INT, id_usuario INT NOT NULL, id_entidade INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, PRIMARY KEY (id_entregadocumentos))
CREATE INDEX IDX_C944D3CABB32A9E1 ON tb_entregadocumentos (id_documentos)
CREATE INDEX IDX_C944D3CA95EAA4A2 ON tb_entregadocumentos (id_matricula)
CREATE INDEX IDX_C944D3CAC0694FE1 ON tb_entregadocumentos (id_situacao)
CREATE INDEX IDX_C944D3CA697D21AC ON tb_entregadocumentos (id_contratoresponsavel)
CREATE TABLE tb_tipodocumento (id_tipodocumento INT IDENTITY NOT NULL, st_tipodocumento NVARCHAR(30) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_tipodocumento))
CREATE TABLE tb_pessoa (id_usuario INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_pais INT, id_municipio INT, id_situacao INT NOT NULL, id_estadocivil INT NOT NULL, st_sexo NVARCHAR(1), st_nomeexibicao NVARCHAR(255), dt_nascimento DATE, st_nomepai NVARCHAR(255), st_nomemae NVARCHAR(100), sg_uf NVARCHAR(2), dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT, st_passaporte NVARCHAR(255), bl_ativo BIT NOT NULL, bl_alterado BIT NOT NULL, st_urlavatar VARCHAR(MAX), st_cidade VARCHAR(MAX), st_identificacao NVARCHAR(255), PRIMARY KEY (id_usuario))
CREATE INDEX IDX_A108B2CEB3F20A47 ON tb_pessoa (id_entidade)
CREATE INDEX IDX_A108B2CEF57D32FD ON tb_pessoa (id_pais)
CREATE INDEX IDX_A108B2CE7EAD49C7 ON tb_pessoa (id_municipio)
CREATE INDEX IDX_A108B2CEC0694FE1 ON tb_pessoa (id_situacao)
CREATE INDEX IDX_A108B2CE40C6C314 ON tb_pessoa (id_estadocivil)
CREATE TABLE tb_tiporegracontrato (id_tiporegracontrato INT NOT NULL, bl_ativo BIT NOT NULL, st_tiporegracontrato NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tiporegracontrato))
CREATE TABLE tb_usuariodevice (id_usuariodevice INT IDENTITY NOT NULL, id_usuario INT, st_key NVARCHAR(250) NOT NULL, st_infodevice NVARCHAR(250) NOT NULL, dt_cadastro DATETIME2(6), PRIMARY KEY (id_usuariodevice))
CREATE INDEX IDX_ED76495CFCF8192D ON tb_usuariodevice (id_usuario)
CREATE TABLE tb_usuariosessao (id_usuario INT NOT NULL, dt_sessao DATETIME2(6), st_hash NVARCHAR(155), PRIMARY KEY (id_usuario))
CREATE TABLE tb_emailentidademensagem (id_entidade INT NOT NULL, id_mensagempadrao INT NOT NULL, id_emailconfig INT NOT NULL, id_usuariocadastro INT NOT NULL, id_textosistema INT NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_entidade, id_mensagempadrao))
CREATE INDEX IDX_13590668DBED4F98 ON tb_emailentidademensagem (id_emailconfig)
CREATE INDEX IDX_135906682FF60C20 ON tb_emailentidademensagem (id_usuariocadastro)
CREATE INDEX IDX_13590668136FA251 ON tb_emailentidademensagem (id_textosistema)
CREATE TABLE tb_concurso (id_concurso INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_situacao INT, st_concurso NVARCHAR(255) NOT NULL, st_slug NVARCHAR(200), st_imagem NVARCHAR(255), dt_cadastro DATETIME2(6) NOT NULL, st_orgao NVARCHAR(200), dt_prova DATE, dt_inicioinscricao DATE, dt_fiminscricao DATE, nu_vagas INT, st_banca NVARCHAR(200) NOT NULL, PRIMARY KEY (id_concurso))
CREATE INDEX IDX_40F888F8B3F20A47 ON tb_concurso (id_entidade)
CREATE INDEX IDX_40F888F8C0694FE1 ON tb_concurso (id_situacao)
CREATE TABLE tb_upload (id_upload INT IDENTITY NOT NULL, st_upload NVARCHAR(255) NOT NULL, PRIMARY KEY (id_upload))
CREATE TABLE tb_produtolivro (id_produto INT NOT NULL, id_entidade INT NOT NULL, id_livro INT NOT NULL, PRIMARY KEY (id_produto))
CREATE INDEX IDX_347C77A2B3F20A47 ON tb_produtolivro (id_entidade)
CREATE INDEX IDX_347C77A2A233E7B7 ON tb_produtolivro (id_livro)
CREATE TABLE tb_entidadefinanceiro (id_entidadefinanceiro INT IDENTITY NOT NULL, id_textosistemarecibo INT NOT NULL, id_textoavisoatraso INT NOT NULL, id_reciboconsolidado INT NOT NULL, id_textochequesdevolvidos INT NOT NULL, id_entidade INT, bl_automatricula BIT NOT NULL, bl_protocolo BIT NOT NULL, st_linkloja NVARCHAR(255), nu_avisoatraso INT, nu_diaspagamentoentrada INT, nu_multacomcarta NUMERIC(10, 0), nu_multasemcarta NUMERIC(10, 0), PRIMARY KEY (id_entidadefinanceiro))
CREATE INDEX IDX_6208FCD5F2B9C522 ON tb_entidadefinanceiro (id_textosistemarecibo)
CREATE INDEX IDX_6208FCD5D74876C1 ON tb_entidadefinanceiro (id_textoavisoatraso)
CREATE INDEX IDX_6208FCD5666AC2A9 ON tb_entidadefinanceiro (id_reciboconsolidado)
CREATE INDEX IDX_6208FCD5CD640913 ON tb_entidadefinanceiro (id_textochequesdevolvidos)
CREATE TABLE tb_entidadeboletoconfig (id_boletoconfig INT NOT NULL, id_entidadefinanceiro INT NOT NULL, PRIMARY KEY (id_boletoconfig, id_entidadefinanceiro))
CREATE TABLE tb_turma (id_turma INT IDENTITY NOT NULL, id_usuariocadastro INT, id_situacao INT, id_evolucao INT, id_entidadecadastro INT, dt_inicioinscricao DATE, dt_fiminscricao DATE, dt_inicio DATE, dt_fim DATE, dt_cadastro DATETIME2(6), nu_maxalunos INT, nu_maxfreepass INT, bl_ativo BIT, st_turma NVARCHAR(150), st_codigo NVARCHAR(20), st_tituloexibicao NVARCHAR(200), PRIMARY KEY (id_turma))
CREATE INDEX IDX_41E949562FF60C20 ON tb_turma (id_usuariocadastro)
CREATE INDEX IDX_41E94956C0694FE1 ON tb_turma (id_situacao)
CREATE INDEX IDX_41E9495630B15C09 ON tb_turma (id_evolucao)
CREATE INDEX IDX_41E9495643D3DAFE ON tb_turma (id_entidadecadastro)
CREATE TABLE tb_itemgradehoraria (id_itemgradehoraria INT IDENTITY NOT NULL, id_gradehoraria INT, id_unidade INT, id_entidadecadastro INT, id_turno INT, id_turma INT, id_projetopedagogico INT, id_diasemana INT, id_professor INT, id_disciplina INT, dt_diasemana DATE NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6), st_observacao NVARCHAR(255), nu_encontro INT, PRIMARY KEY (id_itemgradehoraria))
CREATE INDEX IDX_C936B0909DBD6D4B ON tb_itemgradehoraria (id_gradehoraria)
CREATE INDEX IDX_C936B0909A6508A3 ON tb_itemgradehoraria (id_unidade)
CREATE INDEX IDX_C936B09043D3DAFE ON tb_itemgradehoraria (id_entidadecadastro)
CREATE INDEX IDX_C936B0909122652 ON tb_itemgradehoraria (id_turno)
CREATE INDEX IDX_C936B090C5875896 ON tb_itemgradehoraria (id_turma)
CREATE INDEX IDX_C936B0905E2EAFF2 ON tb_itemgradehoraria (id_projetopedagogico)
CREATE INDEX IDX_C936B09018209A9C ON tb_itemgradehoraria (id_diasemana)
CREATE INDEX IDX_C936B090F9386BC4 ON tb_itemgradehoraria (id_professor)
CREATE INDEX IDX_C936B090D759AAF1 ON tb_itemgradehoraria (id_disciplina)
CREATE TABLE tb_comissaolancamento (id_comissaolancamento INT IDENTITY NOT NULL, id_usuario INT, id_lancamento INT, id_vendaproduto INT, id_lancamentopagamento INT, id_disciplina INT, id_entidade INT, bl_adiantamento BIT, dt_cadastro DATETIME2(6), id_perfilpedagogico INT, bl_autorizado BIT, PRIMARY KEY (id_comissaolancamento))
CREATE INDEX IDX_5BB08FB5FCF8192D ON tb_comissaolancamento (id_usuario)
CREATE INDEX IDX_5BB08FB5C319336A ON tb_comissaolancamento (id_lancamento)
CREATE INDEX IDX_5BB08FB5A1C07F94 ON tb_comissaolancamento (id_vendaproduto)
CREATE INDEX IDX_5BB08FB5992AA1B5 ON tb_comissaolancamento (id_lancamentopagamento)
CREATE INDEX IDX_5BB08FB5D759AAF1 ON tb_comissaolancamento (id_disciplina)
CREATE INDEX IDX_5BB08FB5B3F20A47 ON tb_comissaolancamento (id_entidade)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT DF_5BB08FB5_35F3E13D DEFAULT '0' FOR bl_autorizado
CREATE TABLE tb_contatostelefone (id_telefone INT IDENTITY NOT NULL, id_tipotelefone INT, nu_ddi INT, nu_telefone NVARCHAR(15), st_descricao VARCHAR(MAX), nu_ddd NVARCHAR(3), PRIMARY KEY (id_telefone))
CREATE INDEX IDX_4ECEE777654256CE ON tb_contatostelefone (id_tipotelefone)
CREATE TABLE tb_contaentidade (id_contaentidade INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_tipodeconta INT NOT NULL, st_banco NVARCHAR(3), st_digitoagencia NVARCHAR(2), st_agencia NVARCHAR(20), st_conta NVARCHAR(10), bl_padrao BIT NOT NULL, st_digitoconta NVARCHAR(2), PRIMARY KEY (id_contaentidade))
CREATE INDEX IDX_A5274EBBB3F20A47 ON tb_contaentidade (id_entidade)
CREATE INDEX IDX_A5274EBB404C14D3 ON tb_contaentidade (id_tipodeconta)
CREATE INDEX IDX_A5274EBB13FE11CC ON tb_contaentidade (st_banco)
CREATE TABLE tb_avaliacaoconjuntodisciplina (id_avaliacaoconjunto INT NOT NULL, id_tipodisciplina INT NOT NULL, PRIMARY KEY (id_avaliacaoconjunto, id_tipodisciplina))
CREATE TABLE tb_camporelatorio (id_camporelatorio INT IDENTITY NOT NULL, id_relatorio INT, st_camporelatorio NVARCHAR(30) NOT NULL, st_titulocampo NVARCHAR(30) NOT NULL, bl_filtro BIT NOT NULL, bl_exibido BIT NOT NULL, nu_ordem INT, bl_obrigatorio BIT NOT NULL, PRIMARY KEY (id_camporelatorio))
CREATE TABLE tb_modelocarteirinhaentidade (id_modelocarteirinhaentidade INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_modelocarteirinha INT NOT NULL, PRIMARY KEY (id_modelocarteirinhaentidade))
CREATE INDEX IDX_E7C711A8B3F20A47 ON tb_modelocarteirinhaentidade (id_entidade)
CREATE INDEX IDX_E7C711A87565DE0 ON tb_modelocarteirinhaentidade (id_modelocarteirinha)
CREATE TABLE tb_matriculadisciplina (id_matriculadisciplina INT IDENTITY NOT NULL, id_matriculaoriginal INT, id_evolucao INT NOT NULL, id_situacao INT NOT NULL, id_matricula INT NOT NULL, id_disciplina INT NOT NULL, bl_obrigatorio BIT NOT NULL, nu_aprovafinal INT, dt_conclusao DATE, id_pacote INT, PRIMARY KEY (id_matriculadisciplina))
CREATE INDEX IDX_64D6DD5FDAC7F78D ON tb_matriculadisciplina (id_matriculaoriginal)
CREATE TABLE tb_origemvenda (id_origemvenda INT IDENTITY NOT NULL, st_origemvenda NVARCHAR(25) NOT NULL, PRIMARY KEY (id_origemvenda))
CREATE TABLE tb_pedidointegracao (id_pedidointegracao  INT IDENTITY NOT NULL, id_venda INT, id_sistema INT, id_situacaointegracao INT, dt_envio DATETIME2(6) NOT NULL, dt_inicio DATETIME2(6) NOT NULL, dt_termino DATETIME2(6) NOT NULL, dt_proximopagamento DATETIME2(6) NOT NULL, dt_vencimentocartao DATETIME2(6) NOT NULL, dt_proximatentativa DATETIME2(6) NOT NULL, nu_intervalo INT, nu_diavencimento INT, nu_tentativas INT, nu_tentativasfeitas INT, nu_valor INT, nu_bandeiracartao INT, st_mensagem NVARCHAR(255) NOT NULL, st_orderid NVARCHAR(50) NOT NULL, dt_mudancavencimento DATETIME2(6) NOT NULL, PRIMARY KEY (id_pedidointegracao ))
CREATE INDEX IDX_C95062912BA0BD34 ON tb_pedidointegracao (id_venda)
CREATE INDEX IDX_C95062914F5F0211 ON tb_pedidointegracao (id_sistema)
CREATE TABLE tb_matricula (id_matricula INT IDENTITY NOT NULL, id_projetopedagogico INT, id_usuario INT, id_usuariocadastro INT, id_evolucao INT, id_matriculaorigem INT, id_situacaoagendamento INT, id_situacao INT, id_turma INT, id_cancelamento INT, id_evolucaocertificacao INT, id_matriculavinculada INT, dt_concluinte DATE, dt_termino DATE, dt_cadastro DATETIME2(6), dt_inicio DATETIME2(6) NOT NULL, id_periodoletivo INT, id_vendaproduto INT, id_entidadeatendimento INT NOT NULL, id_entidadematriz INT, id_entidadematricula INT, bl_ativo BIT NOT NULL, bl_institucional BIT NOT NULL, id_mensagemrecuperacao INT, id_evolucaoagendamento INT, bl_documentacao BIT NOT NULL, st_codcertificacao NVARCHAR(200), dt_ultimoacesso DATETIME2(6), bl_academico BIT NOT NULL, PRIMARY KEY (id_matricula))
CREATE INDEX IDX_EFE882F35E2EAFF2 ON tb_matricula (id_projetopedagogico)
CREATE INDEX IDX_EFE882F3FCF8192D ON tb_matricula (id_usuario)
CREATE INDEX IDX_EFE882F32FF60C20 ON tb_matricula (id_usuariocadastro)
CREATE INDEX IDX_EFE882F330B15C09 ON tb_matricula (id_evolucao)
CREATE INDEX IDX_EFE882F378241A5C ON tb_matricula (id_matriculaorigem)
CREATE INDEX IDX_EFE882F31C03E728 ON tb_matricula (id_situacaoagendamento)
CREATE INDEX IDX_EFE882F3C0694FE1 ON tb_matricula (id_situacao)
CREATE INDEX IDX_EFE882F3C5875896 ON tb_matricula (id_turma)
CREATE INDEX IDX_EFE882F392BA0BD2 ON tb_matricula (id_cancelamento)
CREATE INDEX IDX_EFE882F3442A8246 ON tb_matricula (id_evolucaocertificacao)
CREATE INDEX IDX_EFE882F3C3201BE6 ON tb_matricula (id_matriculavinculada)
ALTER TABLE tb_matricula ADD CONSTRAINT DF_EFE882F3_E543DF3C DEFAULT '1' FOR bl_ativo
ALTER TABLE tb_matricula ADD CONSTRAINT DF_EFE882F3_914F4756 DEFAULT '0' FOR bl_institucional
ALTER TABLE tb_matricula ADD CONSTRAINT DF_EFE882F3_19093A88 DEFAULT '0' FOR bl_documentacao
ALTER TABLE tb_matricula ADD CONSTRAINT DF_EFE882F3_4AF0B681 DEFAULT '0' FOR bl_academico
CREATE TABLE tb_gradehoraria (id_gradehoraria INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_entidade INT NOT NULL, st_nomegradehoraria NVARCHAR(100) NOT NULL, dt_iniciogradehoraria DATETIME2(6) NOT NULL, dt_fimgradehoraria DATETIME2(6), bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_sincronizado BIT NOT NULL, PRIMARY KEY (id_gradehoraria))
CREATE INDEX IDX_DD13CBBEC0694FE1 ON tb_gradehoraria (id_situacao)
CREATE INDEX IDX_DD13CBBEB3F20A47 ON tb_gradehoraria (id_entidade)
CREATE TABLE tb_formapagamentoaplicacao (id_formapagamentoaplicacao INT IDENTITY NOT NULL, st_formapagamentoaplicacao NVARCHAR(255) NOT NULL, PRIMARY KEY (id_formapagamentoaplicacao))
CREATE TABLE tb_funcionalidade (id_funcionalidade INT IDENTITY NOT NULL, id_funcionalidadepai INT, id_situacao INT NOT NULL, id_tipofuncionalidade INT, id_sistema INT NOT NULL, st_funcionalidade NVARCHAR(100) NOT NULL, bl_ativo BIT NOT NULL, st_classeflex NVARCHAR(300), st_urlicone NVARCHAR(1000), nu_ordem INT, st_classeflexbotao INT, bl_pesquisa BIT NOT NULL, bl_lista BIT NOT NULL, st_ajuda VARCHAR(MAX), st_target NVARCHAR(100), st_urlcaminho NVARCHAR(50), bl_relatorio BIT, st_urlcaminhoedicao NVARCHAR(50), bl_delete BIT NOT NULL, PRIMARY KEY (id_funcionalidade))
CREATE INDEX IDX_DB667504C7151A7 ON tb_funcionalidade (id_funcionalidadepai)
CREATE INDEX IDX_DB66750C0694FE1 ON tb_funcionalidade (id_situacao)
CREATE INDEX IDX_DB667504A6F2802 ON tb_funcionalidade (id_tipofuncionalidade)
CREATE INDEX IDX_DB667504F5F0211 ON tb_funcionalidade (id_sistema)
CREATE TABLE tb_relatorio (id_relatorio INT IDENTITY NOT NULL, id_tipoorigemrelatorio INT NOT NULL, id_funcionalidade INT NOT NULL, id_usuario INT NOT NULL, id_entidade INT, st_relatorio NVARCHAR(250) NOT NULL, st_origem VARCHAR(MAX), bl_geral BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, st_descricao VARCHAR(MAX) NOT NULL, PRIMARY KEY (id_relatorio))
CREATE INDEX IDX_848E807994A212BF ON tb_relatorio (id_tipoorigemrelatorio)
CREATE INDEX IDX_848E80795BC834DB ON tb_relatorio (id_funcionalidade)
CREATE INDEX IDX_848E8079FCF8192D ON tb_relatorio (id_usuario)
CREATE INDEX IDX_848E8079B3F20A47 ON tb_relatorio (id_entidade)
CREATE TABLE tb_tramite (id_tramite INT IDENTITY NOT NULL, id_tipotramite INT, id_usuario INT, id_entidade INT, id_upload INT NOT NULL, dt_cadastro DATETIME2(6), bl_visivel BIT, st_tramite NVARCHAR(255), PRIMARY KEY (id_tramite))
CREATE INDEX IDX_E0E5F587EBAAF349 ON tb_tramite (id_tipotramite)
CREATE INDEX IDX_E0E5F587FCF8192D ON tb_tramite (id_usuario)
CREATE INDEX IDX_E0E5F587B3F20A47 ON tb_tramite (id_entidade)
CREATE INDEX IDX_E0E5F587318A53F2 ON tb_tramite (id_upload)
CREATE TABLE tb_tipocontratoresponsavel (id_tipocontratoresponsavel INT NOT NULL, st_tipocontratoresponsavel NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipocontratoresponsavel))
CREATE TABLE tb_tramiteentregamaterial (id_tramiteentregamaterial INT IDENTITY NOT NULL, id_tramite INT NOT NULL, id_entregamaterial INT NOT NULL, PRIMARY KEY (id_tramiteentregamaterial))
CREATE INDEX IDX_D036B3EE41AF8655 ON tb_tramiteentregamaterial (id_tramite)
CREATE INDEX IDX_D036B3EEFA72F2BF ON tb_tramiteentregamaterial (id_entregamaterial)
CREATE TABLE tb_vendacartacredito (id_vendacartacredito INT IDENTITY NOT NULL, id_cartacredito INT, id_venda INT, nu_valorutilizado NUMERIC(10, 0) NOT NULL, PRIMARY KEY (id_vendacartacredito))
CREATE INDEX IDX_DF0C8AB21D4A710B ON tb_vendacartacredito (id_cartacredito)
CREATE INDEX IDX_DF0C8AB22BA0BD34 ON tb_vendacartacredito (id_venda)
CREATE TABLE tb_textocategoria (id_textocategoria INT IDENTITY NOT NULL, st_textocategoria NVARCHAR(100) NOT NULL, st_descricao NVARCHAR(200), st_metodo NVARCHAR(100), PRIMARY KEY (id_textocategoria))
CREATE TABLE tb_textoexibicaocategoria (id_textocategoria INT NOT NULL, id_textoexibicao INT NOT NULL, PRIMARY KEY (id_textocategoria, id_textoexibicao))
CREATE INDEX IDX_4AD0030E88A1ED7 ON tb_textoexibicaocategoria (id_textocategoria)
CREATE INDEX IDX_4AD003065A8E33F ON tb_textoexibicaocategoria (id_textoexibicao)
CREATE TABLE tb_ocorrenciaresponsavel (id_ocorrenciaresponsavel INT IDENTITY NOT NULL, id_usuario INT, dt_cadastro DATETIME2(6), id_ocorrencia INT, bl_ativo BIT, PRIMARY KEY (id_ocorrenciaresponsavel))
CREATE INDEX IDX_6F168CD9FCF8192D ON tb_ocorrenciaresponsavel (id_usuario)
CREATE TABLE tb_avaliacaoaluno (id_avaliacaoaluno INT IDENTITY NOT NULL, id_matricula INT, id_avaliacao INT, id_avaliacaoagendamento INT, id_usuariocadastro INT, id_situacao INT NOT NULL, id_usuarioalteracao INT, id_avaliacaoconjuntoreferencia INT, st_nota INT, dt_cadastro DATETIME2(6) NOT NULL, id_sistemaimportacao INT, bl_ativo BIT NOT NULL, dt_avaliacao DATE, id_usuariorevisor INT, id_avaliacaoalunoorigem INT, id_upload INT, st_tituloavaliacao NVARCHAR(500), dt_defesa DATE, id_tiponota INT NOT NULL, st_justificativa NVARCHAR(2000), dt_alteracao DATETIME2(6), PRIMARY KEY (id_avaliacaoaluno))
CREATE INDEX IDX_155F1F1295EAA4A2 ON tb_avaliacaoaluno (id_matricula)
CREATE INDEX IDX_155F1F12E92EFB05 ON tb_avaliacaoaluno (id_avaliacao)
CREATE INDEX IDX_155F1F1250380BFD ON tb_avaliacaoaluno (id_avaliacaoagendamento)
CREATE INDEX IDX_155F1F122FF60C20 ON tb_avaliacaoaluno (id_usuariocadastro)
CREATE INDEX IDX_155F1F12C0694FE1 ON tb_avaliacaoaluno (id_situacao)
CREATE INDEX IDX_155F1F12A432CCDB ON tb_avaliacaoaluno (id_usuarioalteracao)
CREATE TABLE tb_campanhacategorias (id_campanhacategoris INT IDENTITY NOT NULL, id_categoria INT NOT NULL, id_campanhacomercial INT NOT NULL, PRIMARY KEY (id_campanhacategoris))
CREATE INDEX IDX_ABA0E020CE25AE0A ON tb_campanhacategorias (id_categoria)
CREATE INDEX IDX_ABA0E02016FD79A0 ON tb_campanhacategorias (id_campanhacomercial)
CREATE TABLE tb_cargoconcurso (id_cargoconcurso INT IDENTITY NOT NULL, id_concurso INT, nu_salario NUMERIC(30, 2), st_cargoconcurso NVARCHAR(200) NOT NULL, PRIMARY KEY (id_cargoconcurso))
CREATE INDEX IDX_77D751E428847173 ON tb_cargoconcurso (id_concurso)
CREATE TABLE tb_textosistema (id_textosistema INT IDENTITY NOT NULL, id_usuario INT, id_entidade INT, id_textoexibicao INT, id_textocategoria INT, id_situacao INT NOT NULL, id_orientacaotexto INT NOT NULL, st_textosistema NVARCHAR(200) NOT NULL, st_texto VARCHAR(MAX) NOT NULL, dt_inicio DATE NOT NULL, dt_fim DATE NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_cabecalho BIT, PRIMARY KEY (id_textosistema))
CREATE INDEX IDX_53C104A4FCF8192D ON tb_textosistema (id_usuario)
CREATE INDEX IDX_53C104A4B3F20A47 ON tb_textosistema (id_entidade)
CREATE INDEX IDX_53C104A465A8E33F ON tb_textosistema (id_textoexibicao)
CREATE INDEX IDX_53C104A4E88A1ED7 ON tb_textosistema (id_textocategoria)
CREATE INDEX IDX_53C104A4C0694FE1 ON tb_textosistema (id_situacao)
CREATE INDEX IDX_53C104A4EAA41207 ON tb_textosistema (id_orientacaotexto)
CREATE TABLE tb_documentos (id_documentos INT IDENTITY NOT NULL, id_tipodocumento INT, id_situacao INT, id_entidade INT, id_documentosobrigatoriedade INT, id_documentoscategoria INT, bl_digitaliza BIT NOT NULL, st_documentos NVARCHAR(30) NOT NULL, bl_obrigatorio BIT NOT NULL, nu_quantidade INT NOT NULL, id_usuariocadastro INT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_documentos))
CREATE INDEX IDX_A724CAA5F09CE00B ON tb_documentos (id_tipodocumento)
CREATE INDEX IDX_A724CAA5C0694FE1 ON tb_documentos (id_situacao)
CREATE INDEX IDX_A724CAA5B3F20A47 ON tb_documentos (id_entidade)
CREATE INDEX IDX_A724CAA5D357A3AE ON tb_documentos (id_documentosobrigatoriedade)
CREATE INDEX IDX_A724CAA599AE2CEF ON tb_documentos (id_documentoscategoria)
CREATE TABLE tb_tiposelecao (id_tiposelecao INT NOT NULL, st_tiposelecao NVARCHAR(200) NOT NULL, PRIMARY KEY (id_tiposelecao))
CREATE TABLE tb_autorizacaoentrada (id_autorizacaoentrada INT IDENTITY NOT NULL, id_usuario INT NOT NULL, id_usuariocadastro INT NOT NULL, id_textosistema INT NOT NULL, id_entidade INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_autorizacaoentrada))
CREATE INDEX IDX_F72B3B9CFCF8192D ON tb_autorizacaoentrada (id_usuario)
CREATE INDEX IDX_F72B3B9C2FF60C20 ON tb_autorizacaoentrada (id_usuariocadastro)
CREATE INDEX IDX_F72B3B9C136FA251 ON tb_autorizacaoentrada (id_textosistema)
CREATE INDEX IDX_F72B3B9CB3F20A47 ON tb_autorizacaoentrada (id_entidade)
CREATE TABLE tb_enviocobranca (id_enviocobranca INT IDENTITY NOT NULL, id_mensagemcobranca INT, id_mensagem INT, id_lancamento INT, PRIMARY KEY (id_enviocobranca))
CREATE INDEX IDX_46C989BAE037386 ON tb_enviocobranca (id_mensagemcobranca)
CREATE INDEX IDX_46C989BACE9EDE25 ON tb_enviocobranca (id_mensagem)
CREATE INDEX IDX_46C989BAC319336A ON tb_enviocobranca (id_lancamento)
CREATE TABLE tb_meiopagamentointegracao (id_meiopagamentointegracao NVARCHAR(255) NOT NULL, id_cartaobandeira INT, id_sistema INT NOT NULL, id_usuariocadastro INT NOT NULL, id_meiopagamento INT NOT NULL, id_entidade INT, st_codsistema NVARCHAR(30) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, st_codcontacaixa NVARCHAR(255), PRIMARY KEY (id_meiopagamentointegracao))
CREATE TABLE tb_tipotextovariavel (id_tipotextovariavel INT IDENTITY NOT NULL, st_tipotextovariavel NVARCHAR(50) NOT NULL, PRIMARY KEY (id_tipotextovariavel))
CREATE TABLE tb_documentoscategoria (id_documentoscategoria INT IDENTITY NOT NULL, st_documentoscategoria NVARCHAR(30) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_documentoscategoria))
CREATE TABLE tb_documentosprojetopedagogico (id_documentos INT NOT NULL, id_projetopedagogico INT NOT NULL, PRIMARY KEY (id_documentos, id_projetopedagogico))
CREATE TABLE tb_categoriaentidade (id_categoria INT NOT NULL, id_entidade INT NOT NULL, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_categoria, id_entidade))
CREATE INDEX IDX_AF72368D2FF60C20 ON tb_categoriaentidade (id_usuariocadastro)
CREATE TABLE tb_diasemana (id_diasemana INT IDENTITY NOT NULL, st_diasemana NVARCHAR(30) NOT NULL, st_abreviatura NVARCHAR(30) NOT NULL, PRIMARY KEY (id_diasemana))
CREATE TABLE tb_cartaobandeira (id_cartaobandeira INT IDENTITY NOT NULL, st_cartaobandeira NVARCHAR(150) NOT NULL, PRIMARY KEY (id_cartaobandeira))
CREATE TABLE tb_contatosemail (id_email INT IDENTITY NOT NULL, st_email VARCHAR(MAX), PRIMARY KEY (id_email))
CREATE TABLE tb_turno (id_turno INT IDENTITY NOT NULL, st_turno NVARCHAR(255) NOT NULL, PRIMARY KEY (id_turno))
CREATE TABLE tb_campanhaformapagamento (id_formapagamento INT NOT NULL, id_campanhacomercial INT NOT NULL, PRIMARY KEY (id_formapagamento))
CREATE TABLE tb_orientacaotexto (id_orientacaotexto INT IDENTITY NOT NULL, st_orientacaotexto NVARCHAR(15) NOT NULL, PRIMARY KEY (id_orientacaotexto))
CREATE TABLE tb_perfil (id_perfil INT IDENTITY NOT NULL, id_perfilpai INT, id_situacao INT NOT NULL, id_entidade INT NOT NULL, id_entidadeclasse INT, id_usuariocadastro INT NOT NULL, id_perfilpedagogico INT, id_sistema INT NOT NULL, st_nomeperfil NVARCHAR(100) NOT NULL, bl_ativo BIT NOT NULL, bl_padrao BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_perfil))
CREATE INDEX IDX_2BB26F0B52655E0B ON tb_perfil (id_perfilpai)
CREATE INDEX IDX_2BB26F0BC0694FE1 ON tb_perfil (id_situacao)
CREATE INDEX IDX_2BB26F0BB3F20A47 ON tb_perfil (id_entidade)
CREATE INDEX IDX_2BB26F0B3C1D0386 ON tb_perfil (id_entidadeclasse)
CREATE INDEX IDX_2BB26F0B2FF60C20 ON tb_perfil (id_usuariocadastro)
CREATE INDEX IDX_2BB26F0BEA87A5D ON tb_perfil (id_perfilpedagogico)
CREATE INDEX IDX_2BB26F0B4F5F0211 ON tb_perfil (id_sistema)
CREATE TABLE tb_areaprojetopedagogico (id_areaconhecimento INT NOT NULL, id_projetopedagogico INT NOT NULL, PRIMARY KEY (id_areaconhecimento, id_projetopedagogico))
CREATE INDEX IDX_FAAA0166ED4F74A0 ON tb_areaprojetopedagogico (id_areaconhecimento)
CREATE INDEX IDX_FAAA01665E2EAFF2 ON tb_areaprojetopedagogico (id_projetopedagogico)
CREATE TABLE tb_municipio (id_municipio INT IDENTITY NOT NULL, sg_uf NVARCHAR(2), st_nomemunicipio NVARCHAR(255), nu_codigoibge NUMERIC(10, 0), PRIMARY KEY (id_municipio))
CREATE INDEX IDX_4AF6F964FD882EB ON tb_municipio (sg_uf)
CREATE TABLE tb_serie (id_serie INT IDENTITY NOT NULL, st_serie NVARCHAR(255) NOT NULL, id_serieanterior INT, PRIMARY KEY (id_serie))
CREATE TABLE tb_serienivelensino (id_serie INT NOT NULL, id_nivelensino INT NOT NULL, PRIMARY KEY (id_serie, id_nivelensino))
CREATE INDEX IDX_8E957B9C44BFD204 ON tb_serienivelensino (id_serie)
CREATE INDEX IDX_8E957B9C6DA54E76 ON tb_serienivelensino (id_nivelensino)
CREATE TABLE tb_entidadefuncionalidade (id_entidade INT NOT NULL, id_funcionalidade INT NOT NULL, id_situacao INT NOT NULL, bl_ativo BIT NOT NULL, st_label NVARCHAR(255), bl_obrigatorio BIT NOT NULL, bl_visivel BIT, nu_ordem INT, PRIMARY KEY (id_entidade, id_funcionalidade, id_situacao))
CREATE INDEX IDX_8C4DE6BFC0694FE1 ON tb_entidadefuncionalidade (id_situacao)
CREATE TABLE tb_tramitematricula (id_tramitematricula INT IDENTITY NOT NULL, id_tramite INT, id_matricula INT, PRIMARY KEY (id_tramitematricula))
CREATE INDEX IDX_E7BCFD1F41AF8655 ON tb_tramitematricula (id_tramite)
CREATE INDEX IDX_E7BCFD1F95EAA4A2 ON tb_tramitematricula (id_matricula)
CREATE TABLE tb_perfilpedagogico (id_perfilpedagogico INT IDENTITY NOT NULL, st_perfilpedagogico NVARCHAR(30) NOT NULL, PRIMARY KEY (id_perfilpedagogico))
CREATE TABLE tb_avaliacaoconjunto (id_avaliacaoconjunto INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_tipoprova INT, id_situacao INT, id_tipocalculoavaliacao INT, id_usuariocadastro INT, id_entidade INT, st_avaliacaoconjunto NVARCHAR(100) NOT NULL, PRIMARY KEY (id_avaliacaoconjunto))
CREATE TABLE tb_produtoentidade (id_produtoentidade INT IDENTITY NOT NULL, id_entidade INT, id_produto INT, PRIMARY KEY (id_produtoentidade))
CREATE INDEX IDX_3E3DBD51B3F20A47 ON tb_produtoentidade (id_entidade)
CREATE INDEX IDX_3E3DBD518231E0A7 ON tb_produtoentidade (id_produto)
CREATE TABLE tb_tipopessoa (id_tipopessoa INT IDENTITY NOT NULL, st_tipopessoa NVARCHAR(20) NOT NULL, PRIMARY KEY (id_tipopessoa))
CREATE TABLE tb_documentosobrigatoriedade (id_documentosobrigatoriedade INT IDENTITY NOT NULL, st_documentosobrigatoriedade NVARCHAR(30) NOT NULL, id_usuariocadastro INT NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_documentosobrigatoriedade))
CREATE TABLE tb_cartaooperadora (id_cartaooperadora INT IDENTITY NOT NULL, st_cartaooperadora NVARCHAR(50) NOT NULL, PRIMARY KEY (id_cartaooperadora))
CREATE TABLE tb_contratomatricula (id_contrato INT NOT NULL, id_matricula INT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_contrato, id_matricula))
ALTER TABLE tb_contratomatricula ADD CONSTRAINT DF_CC5723C1_E543DF3C DEFAULT '1' FOR bl_ativo
CREATE TABLE tb_itemativacaoentidade (id_itemativacaoentidade INT IDENTITY NOT NULL, id_itemativacao INT NOT NULL, id_entidade INT NOT NULL, id_situacao INT, id_usuarioatualizacao INT, dt_atualizacao DATETIME2(6) NOT NULL, PRIMARY KEY (id_itemativacaoentidade))
CREATE TABLE tb_sistemaentidademensagem (id_sistemaentidademensagem INT IDENTITY NOT NULL, id_mensagempadrao INT NOT NULL, id_textosistema INT NOT NULL, id_usuariocadastro INT NOT NULL, id_entidade INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_sistemaentidademensagem))
CREATE INDEX IDX_A758364DFB6270BA ON tb_sistemaentidademensagem (id_mensagempadrao)
CREATE INDEX IDX_A758364D136FA251 ON tb_sistemaentidademensagem (id_textosistema)
CREATE INDEX IDX_A758364D2FF60C20 ON tb_sistemaentidademensagem (id_usuariocadastro)
CREATE INDEX IDX_A758364DB3F20A47 ON tb_sistemaentidademensagem (id_entidade)
CREATE TABLE tb_campanhacomercial (id_campanhacomercial INT IDENTITY NOT NULL, id_entidade INT NOT NULL, id_situacao INT NOT NULL, id_tipodesconto INT, id_usuariocadastro INT NOT NULL, id_tipocampanha INT NOT NULL, id_categoriacampanha INT NOT NULL, id_premio INT NOT NULL, st_campanhacomercial NVARCHAR(255) NOT NULL, bl_aplicardesconto BIT, bl_ativo BIT NOT NULL, bl_disponibilidade BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, dt_fim DATETIME2(6), dt_inicio DATETIME2(6) NOT NULL, nu_disponibilidade INT, st_descricao NVARCHAR(2500), bl_todosprodutos BIT, bl_todasformas BIT, nu_valordesconto NUMERIC(10, 0) NOT NULL, nu_diasprazo INT, PRIMARY KEY (id_campanhacomercial))
CREATE INDEX IDX_DDB6DF3BB3F20A47 ON tb_campanhacomercial (id_entidade)
CREATE INDEX IDX_DDB6DF3BC0694FE1 ON tb_campanhacomercial (id_situacao)
CREATE INDEX IDX_DDB6DF3B480B1473 ON tb_campanhacomercial (id_tipodesconto)
CREATE INDEX IDX_DDB6DF3B2FF60C20 ON tb_campanhacomercial (id_usuariocadastro)
CREATE INDEX IDX_DDB6DF3B490DB0A9 ON tb_campanhacomercial (id_tipocampanha)
CREATE INDEX IDX_DDB6DF3B8A6D73C2 ON tb_campanhacomercial (id_categoriacampanha)
CREATE INDEX IDX_DDB6DF3B3A005945 ON tb_campanhacomercial (id_premio)
CREATE TABLE tb_pa_mensageminformativa (id_pa_mensageminformativa INT IDENTITY NOT NULL, id_textomensagem INT, id_textoemail INT, id_entidade INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_pa_mensageminformativa))
CREATE INDEX IDX_E9D7F883B81EAF15 ON tb_pa_mensageminformativa (id_textomensagem)
CREATE INDEX IDX_E9D7F88310D64153 ON tb_pa_mensageminformativa (id_textoemail)
CREATE INDEX IDX_E9D7F883B3F20A47 ON tb_pa_mensageminformativa (id_entidade)
CREATE INDEX IDX_E9D7F8832FF60C20 ON tb_pa_mensageminformativa (id_usuariocadastro)
CREATE TABLE tb_tipodesconto (id_tipodesconto INT IDENTITY NOT NULL, st_tipodesconto NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipodesconto))
CREATE TABLE tb_motivoentidade (id_motivoentidade INT IDENTITY NOT NULL, id_usuariocadastro INT, id_motivo INT, id_entidade INT, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_motivoentidade))
CREATE INDEX IDX_5A54F7742FF60C20 ON tb_motivoentidade (id_usuariocadastro)
CREATE INDEX IDX_5A54F7743FCE8D8B ON tb_motivoentidade (id_motivo)
CREATE INDEX IDX_5A54F774B3F20A47 ON tb_motivoentidade (id_entidade)
CREATE TABLE tb_tipoocorrencia (id_tipoocorrencia INT IDENTITY NOT NULL, st_tipoocorrencia NVARCHAR(200) NOT NULL, PRIMARY KEY (id_tipoocorrencia))
CREATE TABLE tb_avaliacaoaplicacaorelacao (id_avaliacaoaplicacao INT NOT NULL, id_avaliacao INT NOT NULL, PRIMARY KEY (id_avaliacaoaplicacao, id_avaliacao))
CREATE TABLE tb_registropessoa (id_registropessoa INT IDENTITY NOT NULL, st_registropessoa NVARCHAR(255) NOT NULL, PRIMARY KEY (id_registropessoa))
CREATE TABLE tb_nivelensino (id_nivelensino INT IDENTITY NOT NULL, st_nivelensino NVARCHAR(255) NOT NULL, PRIMARY KEY (id_nivelensino))
CREATE TABLE tb_tramiteliberatcc (id_tramiteliberatcc INT IDENTITY NOT NULL, id_tramite INT NOT NULL, id_alocacao INT NOT NULL, id_situacaotcc INT NOT NULL, PRIMARY KEY (id_tramiteliberatcc))
CREATE TABLE tb_produtouf (id_produtouf INT IDENTITY NOT NULL, id_produto INT NOT NULL, sg_uf NVARCHAR(2) NOT NULL, PRIMARY KEY (id_produtouf))
CREATE INDEX IDX_8C0E37178231E0A7 ON tb_produtouf (id_produto)
CREATE INDEX IDX_8C0E37174FD882EB ON tb_produtouf (sg_uf)
CREATE TABLE tb_usuariointegracao (id_usuariointegracao INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_sistema INT NOT NULL, id_usuariocadastro INT NOT NULL, id_usuario INT NOT NULL, id_entidade INT, st_codusuario NVARCHAR(255) NOT NULL, st_senhaintegrada NVARCHAR(255), st_loginintegrado NVARCHAR(255), bl_encerrado BIT NOT NULL, PRIMARY KEY (id_usuariointegracao))
CREATE TABLE tb_entidadeemail (id_entidadeemail INT IDENTITY NOT NULL, id_emailconfig INT, id_entidade INT NOT NULL, st_email NVARCHAR(200), PRIMARY KEY (id_entidadeemail))
CREATE INDEX IDX_D6C3EAB9DBED4F98 ON tb_entidadeemail (id_emailconfig)
CREATE TABLE tb_modelogradenotas (id_modelogradenotas INT NOT NULL, st_modelogradenotas NVARCHAR(100), PRIMARY KEY (id_modelogradenotas))
CREATE TABLE tb_entidadeclasse (id_entidadeclasse INT IDENTITY NOT NULL, st_entidadeclasse NVARCHAR(255) NOT NULL, PRIMARY KEY (id_entidadeclasse))
CREATE TABLE tb_encerramentosala (id_encerramentosala INT IDENTITY NOT NULL, id_saladeaula INT, id_usuariocoordenador INT, id_usuariopedagogico INT, id_usuarioprofessor INT, id_usuariofinanceiro INT, id_usuariorecusa INT, dt_encerramentoprofessor DATETIME2(6), dt_encerramentocoordenador DATETIME2(6), dt_encerramentopedagogico DATETIME2(6), dt_encerramentofinanceiro DATETIME2(6), dt_recusa DATETIME2(6), st_motivorecusa VARCHAR(MAX), PRIMARY KEY (id_encerramentosala))
CREATE INDEX IDX_DC16D7445A094BB1 ON tb_encerramentosala (id_saladeaula)
CREATE INDEX IDX_DC16D7446D436514 ON tb_encerramentosala (id_usuariocoordenador)
CREATE INDEX IDX_DC16D744337E47CB ON tb_encerramentosala (id_usuariopedagogico)
CREATE INDEX IDX_DC16D7445C8635CB ON tb_encerramentosala (id_usuarioprofessor)
CREATE INDEX IDX_DC16D74475150EEA ON tb_encerramentosala (id_usuariofinanceiro)
CREATE INDEX IDX_DC16D744CA3A2B1E ON tb_encerramentosala (id_usuariorecusa)
CREATE TABLE tb_matriculaintegracao (id_matriculaintegracao INT IDENTITY NOT NULL, id_matricula INT NOT NULL, id_sistema INT, id_matriculasistema INT NOT NULL, bl_encerrado BIT NOT NULL, PRIMARY KEY (id_matriculaintegracao))
CREATE INDEX IDX_434B59D695EAA4A2 ON tb_matriculaintegracao (id_matricula)
CREATE INDEX IDX_434B59D64F5F0211 ON tb_matriculaintegracao (id_sistema)
CREATE TABLE tb_tipoprodutovalor (id_tipoprodutovalor INT IDENTITY NOT NULL, st_descricaovalor NVARCHAR(250), st_descricao NVARCHAR(250), PRIMARY KEY (id_tipoprodutovalor))
CREATE TABLE tb_logcancelamento (id_logcancelamento INT IDENTITY NOT NULL, id_matricula INT, id_cancelamento INT, id_usuariocadastro INT, id_evolucao INT, id_evolucaocancelamento INT, id_situacao INT, dt_cadastro DATETIME2(6), nu_valorproporcionalcursado NUMERIC(10, 0), nu_valordevolucao NUMERIC(10, 0), nu_percentualmulta NUMERIC(10, 0), nu_cargahorariautilizada INT, nu_valormaterialutilizado NUMERIC(10, 0), st_motivo NVARCHAR(1000), bl_cartacredito BIT, PRIMARY KEY (id_logcancelamento))
CREATE INDEX IDX_201D578595EAA4A2 ON tb_logcancelamento (id_matricula)
CREATE INDEX IDX_201D578592BA0BD2 ON tb_logcancelamento (id_cancelamento)
CREATE INDEX IDX_201D57852FF60C20 ON tb_logcancelamento (id_usuariocadastro)
CREATE INDEX IDX_201D578530B15C09 ON tb_logcancelamento (id_evolucao)
CREATE INDEX IDX_201D578511732B07 ON tb_logcancelamento (id_evolucaocancelamento)
CREATE INDEX IDX_201D5785C0694FE1 ON tb_logcancelamento (id_situacao)
CREATE TABLE tb_concursoproduto (id_concursoproduto INT IDENTITY NOT NULL, id_concurso INT NOT NULL, id_produto INT NOT NULL, PRIMARY KEY (id_concursoproduto))
CREATE INDEX IDX_1A8EE87D28847173 ON tb_concursoproduto (id_concurso)
CREATE INDEX IDX_1A8EE87D8231E0A7 ON tb_concursoproduto (id_produto)
CREATE TABLE tb_dashboard (id_dashboard INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_funcionalidade INT, st_dashboard NVARCHAR(250) NOT NULL, st_descricao NVARCHAR(255), st_origem NVARCHAR(255), st_xtipo NVARCHAR(255), st_xtitulo NVARCHAR(255), st_ytipo NVARCHAR(255), st_ytitulo NVARCHAR(255), st_campolabel NVARCHAR(255), st_tipochart NVARCHAR(255), bl_ativo BIT NOT NULL, PRIMARY KEY (id_dashboard))
CREATE INDEX IDX_A6A3658EC0694FE1 ON tb_dashboard (id_situacao)
CREATE INDEX IDX_A6A3658E5BC834DB ON tb_dashboard (id_funcionalidade)
CREATE TABLE tb_turmaturno (id_turmaturno INT IDENTITY NOT NULL, id_turno INT, dt_cadastro DATETIME2(6), id_turma INT, id_usuariocadastro INT, bl_ativo BIT, PRIMARY KEY (id_turmaturno))
CREATE INDEX IDX_F989DFD59122652 ON tb_turmaturno (id_turno)
CREATE TABLE tb_tipotramite (id_tipotramite INT NOT NULL, id_categoriatramite INT NOT NULL, st_tipotramite NVARCHAR(30) NOT NULL, PRIMARY KEY (id_tipotramite))
CREATE TABLE tb_avaliacaoaplicacao (id_avaliacaoaplicacao INT IDENTITY NOT NULL, id_aplicadorprova INT, id_endereco INT, nu_maxaplicacao INT, id_horarioaula INT, id_usuariocadastro INT NOT NULL, dt_aplicacao DATE, dt_alteracaolimite DATE NOT NULL, dt_antecedenciaminima DATE NOT NULL, bl_unica BIT NOT NULL, dt_cadastro DATETIME2(6), bl_ativo BIT NOT NULL, PRIMARY KEY (id_avaliacaoaplicacao))
CREATE TABLE tb_declaracaoaluno (id_declaracao INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), id_usuario INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_declaracao))
CREATE TABLE tb_ocorrenciaacompanhante (id_ocorrenciaacompanhante INT IDENTITY NOT NULL, id_ocorrencia INT NOT NULL, id_usuario INT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_ocorrenciaacompanhante))
CREATE TABLE tb_entidadeendereco (id_entidadeendereco INT IDENTITY NOT NULL, id_endereco INT NOT NULL, id_entidade INT NOT NULL, bl_padrao BIT NOT NULL, PRIMARY KEY (id_entidadeendereco))
CREATE INDEX IDX_5AE09FD2A83B3A9B ON tb_entidadeendereco (id_endereco)
CREATE TABLE tb_campodashboard (id_campodashboard INT IDENTITY NOT NULL, id_dashboard INT NOT NULL, st_campodashboard NVARCHAR(250) NOT NULL, st_titulocampo NVARCHAR(250) NOT NULL, PRIMARY KEY (id_campodashboard))
CREATE TABLE tb_interface (id_interface INT IDENTITY NOT NULL, st_interface NVARCHAR(30) NOT NULL, PRIMARY KEY (id_interface))
CREATE TABLE tb_dadosacesso (id_dadosacesso INT IDENTITY NOT NULL, id_usuario INT, id_usuariocadastro INT, id_entidade INT, dt_cadastro DATETIME2(6), bl_ativo BIT, st_login NVARCHAR(50), st_senha NVARCHAR(50), PRIMARY KEY (id_dadosacesso))
CREATE INDEX IDX_97BE9BAAFCF8192D ON tb_dadosacesso (id_usuario)
CREATE INDEX IDX_97BE9BAA2FF60C20 ON tb_dadosacesso (id_usuariocadastro)
CREATE INDEX IDX_97BE9BAAB3F20A47 ON tb_dadosacesso (id_entidade)
CREATE TABLE tb_avaliacaoconjuntorelacao (id_avaliacaoconjuntorelacao INT IDENTITY NOT NULL, id_avaliacao INT NOT NULL, id_avaliacaorecupera INT, id_avaliacaoconjunto INT NOT NULL, PRIMARY KEY (id_avaliacaoconjuntorelacao))
CREATE TABLE tb_parametrotcc (id_parametrotcc INT IDENTITY NOT NULL, id_projetopedagogico INT NOT NULL, st_parametrotcc NVARCHAR(200) NOT NULL, nu_peso INT, bl_ativo BIT NOT NULL, PRIMARY KEY (id_parametrotcc))
CREATE TABLE tb_mensagem (id_mensagem INT IDENTITY NOT NULL, id_situacao INT, id_projetopedagogico INT, id_areaconhecimento INT, id_turma INT, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, id_entidade INT NOT NULL, id_funcionalidade INT, st_mensagem VARCHAR(MAX) NOT NULL, st_texto VARCHAR(MAX) NOT NULL, bl_importante BIT NOT NULL, st_caminhoanexo VARCHAR(MAX), PRIMARY KEY (id_mensagem))
CREATE INDEX IDX_A6E227AEC0694FE1 ON tb_mensagem (id_situacao)
CREATE INDEX IDX_A6E227AE5E2EAFF2 ON tb_mensagem (id_projetopedagogico)
CREATE INDEX IDX_A6E227AEED4F74A0 ON tb_mensagem (id_areaconhecimento)
CREATE INDEX IDX_A6E227AEC5875896 ON tb_mensagem (id_turma)
CREATE TABLE tb_modalidadesaladeaula (id_modalidadesaladeaula INT IDENTITY NOT NULL, st_modalidadesaladeaula VARCHAR(MAX) NOT NULL, PRIMARY KEY (id_modalidadesaladeaula))
CREATE TABLE tb_executaclasses (id_campo INT NOT NULL, bl_executa INT, PRIMARY KEY (id_campo))
CREATE TABLE tb_tiposaladeaula (id_tiposaladeaula INT IDENTITY NOT NULL, st_tiposaladeaula NVARCHAR(255) NOT NULL, st_descricao NVARCHAR(2500) NOT NULL, PRIMARY KEY (id_tiposaladeaula))
CREATE TABLE tb_cupom (id_cupom INT IDENTITY NOT NULL, id_usuariocadastro INT NOT NULL, id_tipodesconto INT NOT NULL, id_campanhacomercial INT NOT NULL, st_codigocupom NVARCHAR(22) NOT NULL, st_prefixo NVARCHAR(11) NOT NULL, st_complemento NVARCHAR(11) NOT NULL, nu_desconto NUMERIC(10, 0) NOT NULL, dt_inicio DATETIME2(6) NOT NULL, dt_fim DATETIME2(6) NOT NULL, bl_ativacao BIT NOT NULL, bl_unico BIT NOT NULL, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_cupom))
CREATE INDEX IDX_AB2DF8032FF60C20 ON tb_cupom (id_usuariocadastro)
CREATE INDEX IDX_AB2DF803480B1473 ON tb_cupom (id_tipodesconto)
CREATE INDEX IDX_AB2DF80316FD79A0 ON tb_cupom (id_campanhacomercial)
CREATE TABLE tb_entidade (id_entidade INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_entidadecadastro INT, id_usuariocadastro INT, id_esquemaconfiguracao INT, bl_ativo BIT, st_siglaentidade NVARCHAR(20), st_nomeentidade NVARCHAR(2500), st_urlimglogo NVARCHAR(1000), bl_acessasistema NVARCHAR(255) NOT NULL, nu_cnpj NVARCHAR(15), st_razaosocial NVARCHAR(800) NOT NULL, nu_inscricaoestadual NVARCHAR(2500), st_urlsite NVARCHAR(2500), dt_cadastro DATETIME2(6) NOT NULL, st_cnpj NVARCHAR(15), st_wschave NVARCHAR(40), st_conversao NVARCHAR(255), st_urlportal NVARCHAR(2500), str_urlimglogoentidade NVARCHAR(1000), st_apelido NVARCHAR(15), id_uploadloja INT, st_urllogoutportal NVARCHAR(7), st_urlimgapp NVARCHAR(255), PRIMARY KEY (id_entidade))
CREATE INDEX IDX_DB8EF3CCC0694FE1 ON tb_entidade (id_situacao)
CREATE INDEX IDX_DB8EF3CC43D3DAFE ON tb_entidade (id_entidadecadastro)
CREATE INDEX IDX_DB8EF3CC2FF60C20 ON tb_entidade (id_usuariocadastro)
CREATE INDEX IDX_DB8EF3CC78C3520B ON tb_entidade (id_esquemaconfiguracao)
CREATE TABLE tb_holdingfiliada (id_holdingfiliada INT IDENTITY NOT NULL, id_holding INT, id_entidade INT, dt_cadastro DATETIME2(6) NOT NULL, id_situacao BIT NOT NULL, id_usuariocadastro BIT NOT NULL, PRIMARY KEY (id_holdingfiliada))
CREATE INDEX IDX_2DCC4AD185227166 ON tb_holdingfiliada (id_holding)
CREATE TABLE tb_tipoproduto (id_tipoproduto INT IDENTITY NOT NULL, st_tabelarelacao NVARCHAR(255), st_tipoproduto NVARCHAR(255), PRIMARY KEY (id_tipoproduto))
CREATE TABLE tb_tiporegrapagamento (id_tiporegrapagamento INT IDENTITY NOT NULL, st_tiporegrapagamento NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tiporegrapagamento))
CREATE TABLE tb_livro (id_livro INT IDENTITY NOT NULL, id_entidadecadastro INT NOT NULL, id_usuariocadastro INT NOT NULL, id_situacao INT NOT NULL, id_tipolivro INT NOT NULL, id_livrocolecao INT NOT NULL, st_livro NVARCHAR(250) NOT NULL, st_isbn NVARCHAR(50), st_codigocontrole NVARCHAR(50), bl_ativo BIT NOT NULL, st_edicao NVARCHAR(50), nu_anolancamento INT, nu_pagina INT, nu_peso INT, PRIMARY KEY (id_livro))
CREATE INDEX IDX_265DF67743D3DAFE ON tb_livro (id_entidadecadastro)
CREATE INDEX IDX_265DF6772FF60C20 ON tb_livro (id_usuariocadastro)
CREATE INDEX IDX_265DF677C0694FE1 ON tb_livro (id_situacao)
CREATE INDEX IDX_265DF6779B89BDFE ON tb_livro (id_tipolivro)
CREATE INDEX IDX_265DF6778AA0B36E ON tb_livro (id_livrocolecao)
CREATE TABLE tb_holding (id_holding INT IDENTITY NOT NULL, id_situacao INT, dt_cadastro DATETIME2(6) NOT NULL, st_holding NVARCHAR(100) NOT NULL, id_usuariocadastro BIT NOT NULL, PRIMARY KEY (id_holding))
CREATE INDEX IDX_246802B4C0694FE1 ON tb_holding (id_situacao)
CREATE TABLE tb_formameiopagamento (id_formapagamento INT NOT NULL, id_meiopagamento INT NOT NULL, id_tipodivisaofinanceira INT NOT NULL, PRIMARY KEY (id_formapagamento, id_meiopagamento))
CREATE TABLE tb_tag (id_tag INT IDENTITY NOT NULL, id_usuariocadastro INT, st_tag NVARCHAR(100) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_tag))
CREATE INDEX IDX_AD25BB3C2FF60C20 ON tb_tag (id_usuariocadastro)
CREATE TABLE tb_itemdematerial (id_itemdematerial INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_tipodematerial INT NOT NULL, id_usuariocadastro INT NOT NULL, id_upload INT NOT NULL, id_professor INT NOT NULL, id_entidade INT, st_itemdematerial NVARCHAR(255) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, nu_qtdepaginas INT, nu_peso INT, nu_valor NUMERIC(10, 0), bl_portal BIT, bl_ativo BIT, nu_encontro INT, nu_qtdestoque INT, PRIMARY KEY (id_itemdematerial))
CREATE INDEX IDX_1EB0FB0BC0694FE1 ON tb_itemdematerial (id_situacao)
CREATE INDEX IDX_1EB0FB0BA67A013E ON tb_itemdematerial (id_tipodematerial)
CREATE INDEX IDX_1EB0FB0B2FF60C20 ON tb_itemdematerial (id_usuariocadastro)
CREATE INDEX IDX_1EB0FB0B318A53F2 ON tb_itemdematerial (id_upload)
CREATE INDEX IDX_1EB0FB0BF9386BC4 ON tb_itemdematerial (id_professor)
CREATE INDEX IDX_1EB0FB0BB3F20A47 ON tb_itemdematerial (id_entidade)
CREATE TABLE tb_contatosemailpessoaperfil (id_perfil INT NOT NULL, id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_email INT NOT NULL, bl_padrao BIT NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_perfil))
CREATE TABLE tb_aplicadorprova (id_aplicadorprova INT IDENTITY NOT NULL, id_entidadeaplicador INT, id_usuarioaplicador INT, id_entidadecadastro INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, st_aplicadorprova NVARCHAR(200) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_aplicadorprova))
CREATE INDEX IDX_318693CD6898C988 ON tb_aplicadorprova (id_entidadeaplicador)
CREATE INDEX IDX_318693CD99F138D ON tb_aplicadorprova (id_usuarioaplicador)
CREATE INDEX IDX_318693CD43D3DAFE ON tb_aplicadorprova (id_entidadecadastro)
CREATE INDEX IDX_318693CD2FF60C20 ON tb_aplicadorprova (id_usuariocadastro)
CREATE TABLE tb_operacaorelatorio (id_operacaorelatorio INT IDENTITY NOT NULL, st_operacaorelatorio NVARCHAR(30) NOT NULL, PRIMARY KEY (id_operacaorelatorio))
CREATE TABLE tb_emailconfig (id_emailconfig INT IDENTITY NOT NULL, id_tipoconexaoemailsaida INT, id_tipoconexaoemailentrada INT, id_usuariocadastro INT, id_entidade INT, id_servidoremailentrada INT, st_smtp NVARCHAR(100), st_pop NVARCHAR(100), st_imap NVARCHAR(100), bl_autenticacaosegura BIT, bt_autenticacaosegura BIT, st_conta NVARCHAR(100), bl_ativo BIT, dt_cadastro DATETIME2(6) NOT NULL, nu_portaentrada INT, nu_portasaida INT, st_usuario NVARCHAR(50), st_senha NVARCHAR(20), st_titulo NVARCHAR(200), PRIMARY KEY (id_emailconfig))
CREATE INDEX IDX_AA40DC7212FF81C9 ON tb_emailconfig (id_tipoconexaoemailsaida)
CREATE INDEX IDX_AA40DC72E220608D ON tb_emailconfig (id_tipoconexaoemailentrada)
CREATE INDEX IDX_AA40DC722FF60C20 ON tb_emailconfig (id_usuariocadastro)
CREATE INDEX IDX_AA40DC72B3F20A47 ON tb_emailconfig (id_entidade)
CREATE INDEX IDX_AA40DC7223D8EC4C ON tb_emailconfig (id_servidoremailentrada)
CREATE TABLE tb_servidoremail (id_servidoremail INT IDENTITY NOT NULL, st_servidoremail NVARCHAR(20) NOT NULL, PRIMARY KEY (id_servidoremail))
CREATE TABLE tb_periodoletivo (id_periodoletivo INT IDENTITY NOT NULL, st_periodoletivo NVARCHAR(255) NOT NULL, dt_inicioinscricao DATETIME2(6) NOT NULL, dt_fiminscricao DATETIME2(6) NOT NULL, dt_abertura DATETIME2(6) NOT NULL, dt_encerramento DATETIME2(6) NOT NULL, id_entidade INT NOT NULL, id_usuariocadastro INT NOT NULL, PRIMARY KEY (id_periodoletivo))
CREATE TABLE tb_arquivoretorno (id_arquivoretorno INT IDENTITY NOT NULL, id_usuariocadastro INT, id_entidade INT, st_arquivoretorno NVARCHAR(250), dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, st_banco NVARCHAR(3), PRIMARY KEY (id_arquivoretorno))
CREATE INDEX IDX_DB1C5C6D2FF60C20 ON tb_arquivoretorno (id_usuariocadastro)
CREATE INDEX IDX_DB1C5C6DB3F20A47 ON tb_arquivoretorno (id_entidade)
CREATE TABLE tb_smsentidademensagem (id_smsentidademensagem INT IDENTITY NOT NULL, id_mensagempadrao INT NOT NULL, id_textosistema INT NOT NULL, id_usuariocadastro INT NOT NULL, id_sistema INT NOT NULL, id_entidade INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_smsentidademensagem))
CREATE INDEX IDX_D0550C68FB6270BA ON tb_smsentidademensagem (id_mensagempadrao)
CREATE INDEX IDX_D0550C68136FA251 ON tb_smsentidademensagem (id_textosistema)
CREATE INDEX IDX_D0550C682FF60C20 ON tb_smsentidademensagem (id_usuariocadastro)
CREATE INDEX IDX_D0550C684F5F0211 ON tb_smsentidademensagem (id_sistema)
CREATE INDEX IDX_D0550C68B3F20A47 ON tb_smsentidademensagem (id_entidade)
CREATE TABLE tb_saladeaulaintegracao (id_saladeaulaintegracao INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), id_saladeaula INT, id_usuariocadastro INT, id_sistema INT NOT NULL, bl_conteudo BIT, bl_visivel BIT NOT NULL, st_codsistemacurso NVARCHAR(30) NOT NULL, st_codsistemasala NVARCHAR(30), st_codsistemareferencia NVARCHAR(30), st_retornows NVARCHAR(255), PRIMARY KEY (id_saladeaulaintegracao))
CREATE TABLE tb_modelovenda (id_modelovenda INT IDENTITY NOT NULL, st_modelovenda NVARCHAR(255), PRIMARY KEY (id_modelovenda))
CREATE TABLE tb_endereco (id_endereco INT IDENTITY NOT NULL, id_pais INT NOT NULL, id_municipio INT NOT NULL, id_tipoendereco INT NOT NULL, sg_uf NVARCHAR(2), st_cep NVARCHAR(12), st_endereco VARCHAR(MAX), st_bairro NVARCHAR(255), st_complemento NVARCHAR(500), nu_numero NVARCHAR(30), st_estadoprovincia NVARCHAR(255), st_cidade NVARCHAR(255), bl_ativo BIT NOT NULL, PRIMARY KEY (id_endereco))
CREATE INDEX IDX_C047C310F57D32FD ON tb_endereco (id_pais)
CREATE INDEX IDX_C047C3107EAD49C7 ON tb_endereco (id_municipio)
CREATE INDEX IDX_C047C310BC9063A1 ON tb_endereco (id_tipoendereco)
CREATE TABLE tb_pais (id_pais INT IDENTITY NOT NULL, st_nomepais NVARCHAR(100) NOT NULL, st_nacionalidade NVARCHAR(150), PRIMARY KEY (id_pais))
CREATE TABLE tb_protocolo (id_protocolo INT IDENTITY NOT NULL, id_usuariocadastro INT, id_entidade INT, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_protocolo))
CREATE TABLE tb_assuntoentidadeco (id_assuntoentidadeco INT IDENTITY NOT NULL, id_assuntoco INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_assuntoentidadeco))
CREATE INDEX IDX_530FFA2C9B9DCB8E ON tb_assuntoentidadeco (id_assuntoco)
CREATE INDEX IDX_530FFA2CB3F20A47 ON tb_assuntoentidadeco (id_entidade)
CREATE TABLE tb_disciplina (id_disciplina INT IDENTITY NOT NULL, id_situacao INT NOT NULL, sg_uf NVARCHAR(2), st_disciplina NVARCHAR(255) NOT NULL, st_descricao VARCHAR(MAX), nu_identificador NVARCHAR(255), nu_cargahoraria NUMERIC(10, 0) NOT NULL, nu_creditos NUMERIC(10, 0), nu_codigoparceiro NVARCHAR(255), bl_ativa BIT NOT NULL, bl_compartilhargrupo BIT NOT NULL, st_tituloexibicao NVARCHAR(255), id_tipodisciplina INT NOT NULL, st_apelido NVARCHAR(15), id_entidade INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, st_identificador NVARCHAR(30), st_ementacertificado VARCHAR(MAX), bl_provamontada BIT, nu_repeticao INT, PRIMARY KEY (id_disciplina))
CREATE INDEX IDX_CB4FC9B5C0694FE1 ON tb_disciplina (id_situacao)
CREATE INDEX IDX_CB4FC9B54FD882EB ON tb_disciplina (sg_uf)
CREATE TABLE tb_formapagamentoaplicacaorelacao (id_formapagamento INT NOT NULL, id_formapagamentoaplicacao INT NOT NULL, PRIMARY KEY (id_formapagamento, id_formapagamentoaplicacao))
CREATE TABLE tb_documentosutilizacaorelacao (id_documentos INT NOT NULL, id_documentosutilizacao INT NOT NULL, PRIMARY KEY (id_documentos, id_documentosutilizacao))
CREATE TABLE tb_motivocancelamento (id_motivocancelamento INT IDENTITY NOT NULL, id_cancelamento INT, id_motivo INT, id_usuariocadastro INT, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT, PRIMARY KEY (id_motivocancelamento))
CREATE INDEX IDX_ECB3FDAD92BA0BD2 ON tb_motivocancelamento (id_cancelamento)
CREATE INDEX IDX_ECB3FDAD3FCE8D8B ON tb_motivocancelamento (id_motivo)
CREATE INDEX IDX_ECB3FDAD2FF60C20 ON tb_motivocancelamento (id_usuariocadastro)
CREATE TABLE tb_banco (st_banco NVARCHAR(3) NOT NULL, id_banco INT NOT NULL, st_nomebanco NVARCHAR(255) NOT NULL, PRIMARY KEY (st_banco))
CREATE TABLE tb_tipofuncao (id_tipofuncao INT IDENTITY NOT NULL, st_tipofuncao NVARCHAR(150) NOT NULL, PRIMARY KEY (id_tipofuncao))
CREATE TABLE tb_tipoentidaderesponsavellegal (id_tipoentidaderesponsavel INT IDENTITY NOT NULL, PRIMARY KEY (id_tipoentidaderesponsavel))
CREATE TABLE tb_itemdematerialdisciplina (id_itemdematerialdisciplina INT IDENTITY NOT NULL, id_disciplina INT NOT NULL, id_itemdematerial INT, PRIMARY KEY (id_itemdematerialdisciplina))
CREATE INDEX IDX_7F87BDA2D759AAF1 ON tb_itemdematerialdisciplina (id_disciplina)
CREATE TABLE tb_nucleofuncionariotm (id_nucleofuncionariotm INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_funcao INT, id_usuario INT, id_nucleotm INT, id_usuariocadastro INT, id_situacao INT, bl_ativo BIT NOT NULL, PRIMARY KEY (id_nucleofuncionariotm))
CREATE TABLE tb_modulodisciplina (id_modulodisciplina INT IDENTITY NOT NULL, id_modulo INT NOT NULL, id_disciplina INT NOT NULL, id_serie INT NOT NULL, id_nivelensino INT NOT NULL, id_areaconhecimento INT, id_usuarioponderacao INT, nu_ordem INT, nu_importancia INT, bl_ativo BIT NOT NULL, bl_obrigatoria BIT NOT NULL, nu_ponderacaocalculada NUMERIC(10, 0), nu_ponderacaoaplicada NUMERIC(10, 0), dt_cadastroponderacao DATETIME2(6), nu_cargahoraria INT, PRIMARY KEY (id_modulodisciplina))
CREATE INDEX IDX_4E1E8E16CAC67ADB ON tb_modulodisciplina (id_modulo)
CREATE INDEX IDX_4E1E8E16D759AAF1 ON tb_modulodisciplina (id_disciplina)
CREATE INDEX IDX_4E1E8E1644BFD204 ON tb_modulodisciplina (id_serie)
CREATE INDEX IDX_4E1E8E166DA54E76 ON tb_modulodisciplina (id_nivelensino)
CREATE INDEX IDX_4E1E8E16ED4F74A0 ON tb_modulodisciplina (id_areaconhecimento)
CREATE INDEX IDX_4E1E8E16AADAB48A ON tb_modulodisciplina (id_usuarioponderacao)
CREATE TABLE tb_usuarioperfilentidade (id_usuario INT NOT NULL, id_entidade INT NOT NULL, id_perfil INT NOT NULL, id_situacao INT, dt_inicio DATE NOT NULL, dt_termino DATE, dt_cadastro DATETIME2(6), id_entidadeidentificacao INT, bl_ativo BIT NOT NULL, bl_tutorial BIT, st_token NVARCHAR(32), PRIMARY KEY (id_usuario, id_entidade, id_perfil))
CREATE INDEX IDX_696D871FC0694FE1 ON tb_usuarioperfilentidade (id_situacao)
CREATE TABLE tb_esquemaconfiguracao (id_esquemaconfiguracao INT IDENTITY NOT NULL, id_usuariocadastro INT, id_usuarioatualizacao INT, st_esquemaconfiguracao NVARCHAR(200) NOT NULL, dt_cadastro DATETIME2(6), dt_atualizacao DATETIME2(6), PRIMARY KEY (id_esquemaconfiguracao))
CREATE INDEX IDX_D5C3CC4C2FF60C20 ON tb_esquemaconfiguracao (id_usuariocadastro)
CREATE INDEX IDX_D5C3CC4C83CB061 ON tb_esquemaconfiguracao (id_usuarioatualizacao)
CREATE TABLE tb_projetopedagogico (id_projetopedagogico INT IDENTITY NOT NULL, id_situacao INT NOT NULL, id_trilha INT NOT NULL, id_projetopedagogicoorigem INT, id_medidatempoconclusao INT, id_contratoregra INT, id_livroregistroentidade INT, id_anexoementa INT NOT NULL, st_descricao VARCHAR(MAX), nu_idademinimainiciar INT, bl_ativo BIT NOT NULL, nu_tempominimoconcluir INT, nu_idademinimaconcluir INT, nu_tempomaximoconcluir INT, nu_tempopreviconcluir INT, st_projetopedagogico NVARCHAR(255) NOT NULL, st_tituloexibicao NVARCHAR(255), bl_autoalocaraluno BIT NOT NULL, st_nomeversao NVARCHAR(255), id_tipoextracurricular INT, nu_notamaxima INT, nu_percentualaprovacao INT, nu_valorprovafinal INT, id_entidadecadastro INT NOT NULL, bl_provafinal BIT NOT NULL, bl_salasemprovafinal BIT NOT NULL, st_objetivo VARCHAR(MAX), st_estruturacurricular VARCHAR(MAX), st_metodologiaavaliacao VARCHAR(MAX), st_certificacao VARCHAR(MAX), st_apelido VARCHAR(MAX), st_valorapresentacao VARCHAR(MAX), nu_cargahoraria INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, bl_disciplinaextracurricular BIT NOT NULL, id_usuario INT NOT NULL, st_conteudoprogramatico VARCHAR(MAX) NOT NULL, nu_prazoagendamento INT, st_perfilprojeto VARCHAR(MAX), st_coordenacao VARCHAR(MAX), st_horario VARCHAR(MAX), st_publicoalvo VARCHAR(MAX), st_mercadotrabalho VARCHAR(MAX), bl_turma BIT NOT NULL, bl_gradepronta BIT, dt_criagrade DATETIME2(6), id_usuariograde INT, id_horarioaula INT, bl_disciplinacomplementar BIT, PRIMARY KEY (id_projetopedagogico))
CREATE INDEX IDX_95650969C0694FE1 ON tb_projetopedagogico (id_situacao)
CREATE INDEX IDX_956509691426D29A ON tb_projetopedagogico (id_trilha)
CREATE INDEX IDX_9565096967AF234A ON tb_projetopedagogico (id_projetopedagogicoorigem)
CREATE INDEX IDX_95650969616C5294 ON tb_projetopedagogico (id_medidatempoconclusao)
CREATE INDEX IDX_9565096963FC6B92 ON tb_projetopedagogico (id_contratoregra)
CREATE INDEX IDX_956509692CABD3E0 ON tb_projetopedagogico (id_livroregistroentidade)
CREATE INDEX IDX_956509693F9252D0 ON tb_projetopedagogico (id_anexoementa)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT DF_95650969_F067B6A6 DEFAULT '0' FOR bl_disciplinacomplementar
CREATE TABLE tb_modelocarteirinha (id_modelocarteirinha INT IDENTITY NOT NULL, id_situacao INT, st_modelocarteirinha NVARCHAR(200) NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, bl_ativo BIT NOT NULL, PRIMARY KEY (id_modelocarteirinha))
CREATE INDEX IDX_CC1DFB7BC0694FE1 ON tb_modelocarteirinha (id_situacao)
CREATE TABLE tb_produtointegracao (id_produtointegracao INT IDENTITY NOT NULL, id_produto INT NOT NULL, id_sistema INT NOT NULL, nu_codprodutosistema NVARCHAR(10) NOT NULL, PRIMARY KEY (id_produtointegracao))
CREATE INDEX IDX_B959304F8231E0A7 ON tb_produtointegracao (id_produto)
CREATE INDEX IDX_B959304F4F5F0211 ON tb_produtointegracao (id_sistema)
CREATE TABLE tb_turmaprojeto (id_turmaprojeto INT IDENTITY NOT NULL, id_usuariocadastro INT, id_turma INT, id_projetopedagogico INT, bl_ativo BIT, dt_cadastro DATETIME2(6), PRIMARY KEY (id_turmaprojeto))
CREATE INDEX IDX_CE6B804B2FF60C20 ON tb_turmaprojeto (id_usuariocadastro)
CREATE INDEX IDX_CE6B804BC5875896 ON tb_turmaprojeto (id_turma)
CREATE INDEX IDX_CE6B804B5E2EAFF2 ON tb_turmaprojeto (id_projetopedagogico)
CREATE TABLE tb_respostaautorizacao (id_respostaautorizacao INT IDENTITY NOT NULL, st_codigoresposta NVARCHAR(50) NOT NULL, st_descricao NVARCHAR(50) NOT NULL, id_usuariocadastro INT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_respostaautorizacao))
CREATE TABLE tb_entidaderelacao (id_entidade INT NOT NULL, id_entidadepai INT NOT NULL, id_usuariocadastro INT NOT NULL, id_entidadeclasse INT, PRIMARY KEY (id_entidade, id_entidadepai, id_usuariocadastro))
CREATE INDEX IDX_EDB877CA3C1D0386 ON tb_entidaderelacao (id_entidadeclasse)
CREATE TABLE tb_campanhaproduto (id_campanhacomercial INT NOT NULL, id_produto INT NOT NULL, PRIMARY KEY (id_campanhacomercial, id_produto))
CREATE TABLE tb_livroentidade (id_livroentidade INT IDENTITY NOT NULL, id_livro INT NOT NULL, id_entidade INT NOT NULL, PRIMARY KEY (id_livroentidade))
CREATE INDEX IDX_BC83B035A233E7B7 ON tb_livroentidade (id_livro)
CREATE INDEX IDX_BC83B035B3F20A47 ON tb_livroentidade (id_entidade)
CREATE TABLE tb_configuracaoentidade (id_configuracaoentidade INT IDENTITY NOT NULL, id_manualaluno INT, id_tabelapreco INT, id_assuntoduplicidade INT, id_entidade INT NOT NULL, id_textosistemafichac INT, id_modelogradenotas INT NOT NULL, nu_maxextensao INT NOT NULL, nu_encerramentoprofessor INT NOT NULL, id_categoriaocorrencia INT, id_textosemsala INT, nu_horasencontro INT, id_assuntoresgate INT, id_textoresgate INT, id_assuntorecuperacao INT, bl_bloqueiadisciplina BIT NOT NULL, bl_encerrasemacesso BIT NOT NULL, bl_emailunico BIT, bl_tutorial BIT, st_informacoestecnicas VARCHAR(MAX), bl_primeiroacesso BIT, bl_professordisciplina BIT, st_campotextogooglecredito VARCHAR(MAX), st_corprimarialoja NVARCHAR(7), st_corsecundarialoja NVARCHAR(7), st_corterciarialoja NVARCHAR(7), st_campotextogoogleboleto VARCHAR(MAX), st_corentidade NVARCHAR(15), bl_acessoapp BIT NOT NULL, id_assuntorequerimento NVARCHAR(15), bl_loginsimultaneo BIT, PRIMARY KEY (id_configuracaoentidade))
CREATE INDEX IDX_A1ED20E755FD03C5 ON tb_configuracaoentidade (id_manualaluno)
CREATE INDEX IDX_A1ED20E7834E0A09 ON tb_configuracaoentidade (id_tabelapreco)
CREATE INDEX IDX_A1ED20E73DC88C24 ON tb_configuracaoentidade (id_assuntoduplicidade)
CREATE TABLE tb_formapagamento (id_formapagamento INT IDENTITY NOT NULL, id_situacao INT, id_tipoformapagamentoparcela INT, id_tipocalculojuros INT, dt_cadastro DATETIME2(6) NOT NULL, id_usuariocadastro INT NOT NULL, id_entidade INT, nu_maxparcelas INT NOT NULL, bl_todosprodutos BIT NOT NULL, nu_entradavalormin NUMERIC(10, 0), nu_entradavalormax NUMERIC(10, 0), nu_valormin NUMERIC(10, 0), nu_valormax NUMERIC(10, 0), nu_valorminparcela NUMERIC(10, 0), nu_multa NUMERIC(10, 0), nu_juros NUMERIC(10, 0), nu_jurosmax NUMERIC(10, 0), nu_jurosmin NUMERIC(10, 0), st_formapagamento NVARCHAR(255) NOT NULL, st_descricao VARCHAR(MAX) NOT NULL, PRIMARY KEY (id_formapagamento))
CREATE INDEX IDX_523DA81CC0694FE1 ON tb_formapagamento (id_situacao)
CREATE INDEX IDX_523DA81C2AA7EA ON tb_formapagamento (id_tipoformapagamentoparcela)
CREATE INDEX IDX_523DA81C311E715F ON tb_formapagamento (id_tipocalculojuros)
CREATE TABLE tb_tipoenvio (id_tipoenvio INT IDENTITY NOT NULL, st_tipoenvio NVARCHAR(100) NOT NULL, st_descricao NVARCHAR(100), PRIMARY KEY (id_tipoenvio))
CREATE TABLE tb_vendaprodutonivelserie (id_vendaproduto INT NOT NULL, PRIMARY KEY (id_vendaproduto))
CREATE TABLE tb_categoriacampanha (id_categoriacampanha INT IDENTITY NOT NULL, st_categoriacampanha NVARCHAR(50) NOT NULL, PRIMARY KEY (id_categoriacampanha))
CREATE TABLE tb_lotematerial (id_lotematerial INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, PRIMARY KEY (id_lotematerial))
CREATE TABLE tb_transacaolancamento (id_transacaofinanceira  INT NOT NULL, id_lancamento  INT NOT NULL, PRIMARY KEY (id_transacaofinanceira , id_lancamento ))
CREATE TABLE tb_turmahorario (id_turmahorario INT IDENTITY NOT NULL, dt_cadastro DATETIME2(6), id_turma INT NOT NULL, id_horarioaula INT NOT NULL, id_usuariocadastro INT, bl_ativo BIT, PRIMARY KEY (id_turmahorario))
CREATE TABLE tb_uf (sg_uf NVARCHAR(2) NOT NULL, st_uf NVARCHAR(100) NOT NULL, nu_codigocapital NUMERIC(10, 0), nu_codigoibge NUMERIC(10, 0), st_nomeregiao NVARCHAR(30), PRIMARY KEY (sg_uf))
CREATE TABLE tb_contratoregra (id_contratoregra INT IDENTITY NOT NULL, id_projetocontratoduracaotipo INT, id_entidade INT, id_usuariocadastro INT, id_modelocarteirinha INT, id_tiporegracontrato INT, st_contratoregra NVARCHAR(100) NOT NULL, nu_contratoduracao INT, nu_mesesmulta INT, bl_proporcaomes BIT NOT NULL, nu_contratomultavalor NUMERIC(10, 0), bl_renovarcontrato BIT NOT NULL, nu_mesesmensalidade INT, bl_ativo BIT NOT NULL, dt_cadastro DATETIME2(6) NOT NULL, nu_tempoextensao INT, id_contratomodelo INT, id_extensaomodelo INT, PRIMARY KEY (id_contratoregra))
CREATE INDEX IDX_AE6BC3A73A71990D ON tb_contratoregra (id_projetocontratoduracaotipo)
CREATE INDEX IDX_AE6BC3A7B3F20A47 ON tb_contratoregra (id_entidade)
CREATE INDEX IDX_AE6BC3A72FF60C20 ON tb_contratoregra (id_usuariocadastro)
CREATE INDEX IDX_AE6BC3A77565DE0 ON tb_contratoregra (id_modelocarteirinha)
CREATE INDEX IDX_AE6BC3A7BE123958 ON tb_contratoregra (id_tiporegracontrato)
CREATE TABLE tb_disciplinaserienivelensino (id_serie INT NOT NULL, id_disciplina INT NOT NULL, id_nivelensino INT NOT NULL, PRIMARY KEY (id_serie, id_disciplina, id_nivelensino))
CREATE TABLE tb_motivoocorrencia (id_motivoocorrencia INT NOT NULL, st_motivoocorrencia NVARCHAR(100), PRIMARY KEY (id_motivoocorrencia))
CREATE TABLE tb_tipoorigemrelatorio (id_tipoorigemrelatorio INT IDENTITY NOT NULL, st_tipoorigemrelatorio NVARCHAR(100) NOT NULL, PRIMARY KEY (id_tipoorigemrelatorio))
CREATE TABLE tb_tipoendereco (id_tipoendereco INT IDENTITY NOT NULL, id_categoriaendereco INT NOT NULL, st_tipoendereco NVARCHAR(255) NOT NULL, PRIMARY KEY (id_tipoendereco))
CREATE INDEX IDX_FC3EC5547FF0A0CA ON tb_tipoendereco (id_categoriaendereco)
CREATE TABLE tb_notificacaoentidade (id_notificacaoentidade INT IDENTITY NOT NULL, id_notificacao INT, id_entidade INT, dt_cadastro DATETIME2(6), bl_enviaparaemail BIT, nu_percentuallimitealunosala INT, PRIMARY KEY (id_notificacaoentidade))
CREATE INDEX IDX_9C62DEA242DF5E71 ON tb_notificacaoentidade (id_notificacao)
CREATE INDEX IDX_9C62DEA2B3F20A47 ON tb_notificacaoentidade (id_entidade)
CREATE TABLE tb_usuario (id_usuario INT IDENTITY NOT NULL, id_registropessoa INT, id_usuariopai INT, id_titulacao INT, st_login NVARCHAR(40) NOT NULL, st_senha NVARCHAR(32), st_nomecompleto NVARCHAR(300) NOT NULL, st_cpf NVARCHAR(11), bl_ativo BIT, bl_redefinicaosenha BIT, PRIMARY KEY (id_usuario))
CREATE INDEX IDX_5DB26AFFDF4FBDC ON tb_usuario (id_registropessoa)
CREATE INDEX IDX_5DB26AFFFBCB3421 ON tb_usuario (id_usuariopai)
CREATE INDEX IDX_5DB26AFF733DB0F6 ON tb_usuario (id_titulacao)
ALTER TABLE tb_arquivoretornopacote ADD CONSTRAINT FK_D0BB03F2C303F3E8 FOREIGN KEY (id_arquivoretornomaterial) REFERENCES tb_arquivoretornomaterial (id_arquivoretornomaterial)
ALTER TABLE tb_arquivoretornopacote ADD CONSTRAINT FK_D0BB03F22416BAA7 FOREIGN KEY (id_pacote) REFERENCES tb_pacote (id_pacote)
ALTER TABLE tb_arquivoretornopacote ADD CONSTRAINT FK_D0BB03F248CEA880 FOREIGN KEY (id_itemdematerial) REFERENCES tb_itemdematerial (id_itemdematerial)
ALTER TABLE tb_arquivoretornopacote ADD CONSTRAINT FK_D0BB03F2EB341277 FOREIGN KEY (id_situacaopostagem) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_projetopedagogicoserienivelensino ADD CONSTRAINT FK_D47E7C515E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_projetopedagogicoserienivelensino ADD CONSTRAINT FK_D47E7C5144BFD204 FOREIGN KEY (id_serie) REFERENCES tb_serie (id_serie)
ALTER TABLE tb_projetopedagogicoserienivelensino ADD CONSTRAINT FK_D47E7C516DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_entidadesistema ADD CONSTRAINT FK_F6FA05424F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_entidadesistema ADD CONSTRAINT FK_F6FA0542B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_disciplinaconteudo ADD CONSTRAINT FK_5D127010D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_disciplinaconteudo ADD CONSTRAINT FK_5D1270102FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_disciplinaconteudo ADD CONSTRAINT FK_5D12701068C7A4BD FOREIGN KEY (id_formatoarquivo) REFERENCES tb_formatoarquivo (id_formatoarquivo)
ALTER TABLE tb_contratoresponsavel ADD CONSTRAINT FK_C47DBFEB36B289B6 FOREIGN KEY (id_contrato) REFERENCES tb_contrato (id_contrato)
ALTER TABLE tb_contratoresponsavel ADD CONSTRAINT FK_C47DBFEB78B9BC56 FOREIGN KEY (id_tipocontratoresponsavel) REFERENCES tb_tipocontratoresponsavel (id_tipocontratoresponsavel)
ALTER TABLE tb_contratoresponsavel ADD CONSTRAINT FK_C47DBFEBE94BA3BB FOREIGN KEY (id_entidaderesponsavel) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_contratoresponsavel ADD CONSTRAINT FK_C47DBFEB2BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_prevendaproduto ADD CONSTRAINT FK_A3E4EC148231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_prevendaproduto ADD CONSTRAINT FK_A3E4EC14B960F69E FOREIGN KEY (id_prevenda) REFERENCES tb_prevenda (id_prevenda)
ALTER TABLE tb_tipodematerial ADD CONSTRAINT FK_F00452B5C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_tipodematerial ADD CONSTRAINT FK_F00452B5B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_alocacao ADD CONSTRAINT FK_42023FE15A094BB1 FOREIGN KEY (id_saladeaula) REFERENCES tb_saladeaula (id_saladeaula)
ALTER TABLE tb_alocacao ADD CONSTRAINT FK_42023FE130B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_alocacao ADD CONSTRAINT FK_42023FE1C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_alocacao ADD CONSTRAINT FK_42023FE12FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_alocacao ADD CONSTRAINT FK_42023FE1C9D64318 FOREIGN KEY (id_matriculadisciplina) REFERENCES tb_matriculadisciplina (id_matriculadisciplina)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502EF40710E8 FOREIGN KEY (id_tipolancamento) REFERENCES tb_tipolancamento (id_tipolancamento)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502E9B119E27 FOREIGN KEY (id_meiopagamento) REFERENCES tb_meiopagamento (id_meiopagamento)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502E13FE11CC FOREIGN KEY (st_banco) REFERENCES tb_banco (st_banco)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502ECD869197 FOREIGN KEY (id_cartaoconfig) REFERENCES tb_cartaoconfig (id_cartaoconfig)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502EB2D22ED8 FOREIGN KEY (id_boletoconfig) REFERENCES tb_boletoconfig (id_boletoconfig)
ALTER TABLE tb_lancamento ADD CONSTRAINT FK_DF0F502E230863F0 FOREIGN KEY (id_entidadelancamento) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_matriculacertificacao ADD CONSTRAINT FK_390664E4B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_matriculacertificacao ADD CONSTRAINT FK_390664E4D7EB1E7A FOREIGN KEY (id_usuarioenviocertificado) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matriculacertificacao ADD CONSTRAINT FK_390664E476B4630 FOREIGN KEY (id_usuarioretornocertificadora) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matriculacertificacao ADD CONSTRAINT FK_390664E4592D74DF FOREIGN KEY (id_usuarioenvioaluno) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matriculacertificacao ADD CONSTRAINT FK_390664E42FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_cancelamento ADD CONSTRAINT FK_D214AD2730B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_concursonivelensino ADD CONSTRAINT FK_B7D6EF3228847173 FOREIGN KEY (id_concurso) REFERENCES tb_concurso (id_concurso)
ALTER TABLE tb_concursonivelensino ADD CONSTRAINT FK_B7D6EF326DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_produto ADD CONSTRAINT FK_237B9375283495BB FOREIGN KEY (id_tipoproduto) REFERENCES tb_tipoproduto (id_tipoproduto)
ALTER TABLE tb_produto ADD CONSTRAINT FK_237B9375E70E16A9 FOREIGN KEY (id_modelovenda) REFERENCES tb_modelovenda (id_modelovenda)
ALTER TABLE tb_produto ADD CONSTRAINT FK_237B9375C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_produto ADD CONSTRAINT FK_237B93752FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_produto ADD CONSTRAINT FK_237B9375B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_areaconhecimento ADD CONSTRAINT FK_C75A6310C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_areaconhecimento ADD CONSTRAINT FK_C75A6310A9B4471E FOREIGN KEY (id_tipoareaconhecimento) REFERENCES tb_tipoareaconhecimento (id_tipoareaconhecimento)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49CB81EAF15 FOREIGN KEY (id_textomensagem) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49CC88A814 FOREIGN KEY (id_textoinstrucao) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49C9B9DCB8E FOREIGN KEY (id_assuntoco) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49CCA9E9832 FOREIGN KEY (id_categoriaocorrencia) REFERENCES tb_categoriaocorrencia (id_categoriaocorrencia)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49CB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pa_dadoscontato ADD CONSTRAINT FK_E02AA49C2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_notificacaoentidadeperfil ADD CONSTRAINT FK_91527986316240E5 FOREIGN KEY (id_notificacaoentidade) REFERENCES tb_notificacaoentidade (id_notificacaoentidade)
ALTER TABLE tb_notificacaoentidadeperfil ADD CONSTRAINT FK_91527986B052C3AA FOREIGN KEY (id_perfil) REFERENCES tb_perfil (id_perfil)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF295EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF230B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF2C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF2D892A01A FOREIGN KEY (id_usuariointeressado) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF2CA9E9832 FOREIGN KEY (id_categoriaocorrencia) REFERENCES tb_categoriaocorrencia (id_categoriaocorrencia)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF29B9DCB8E FOREIGN KEY (id_assuntoco) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF25A094BB1 FOREIGN KEY (id_saladeaula) REFERENCES tb_saladeaula (id_saladeaula)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF2F0B1D8EB FOREIGN KEY (id_ocorrenciaoriginal) REFERENCES tb_ocorrencia (id_ocorrencia)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF2B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF22FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_ocorrencia ADD CONSTRAINT FK_65F23AF264964C31 FOREIGN KEY (id_motivoocorrencia) REFERENCES tb_motivoocorrencia (id_motivoocorrencia)
ALTER TABLE tb_mensagempadrao ADD CONSTRAINT FK_AD1C2331A2782CAC FOREIGN KEY (id_tipoenvio) REFERENCES tb_tipoenvio (id_tipoenvio)
ALTER TABLE tb_funcao ADD CONSTRAINT FK_D309E87147D007BE FOREIGN KEY (id_tipofuncao) REFERENCES tb_tipofuncao (id_tipofuncao)
ALTER TABLE tb_tipotrilhafixa ADD CONSTRAINT FK_41C8E2921B1F91F4 FOREIGN KEY (id_tipotrilha) REFERENCES tb_tipotrilha (id_tipotrilha)
ALTER TABLE tb_concursouf ADD CONSTRAINT FK_88467B7E28847173 FOREIGN KEY (id_concurso) REFERENCES tb_concurso (id_concurso)
ALTER TABLE tb_concursouf ADD CONSTRAINT FK_88467B7E4FD882EB FOREIGN KEY (sg_uf) REFERENCES tb_uf (sg_uf)
ALTER TABLE tb_entidadecartaoconfig ADD CONSTRAINT FK_C556964F51EFDBF FOREIGN KEY (id_entidadefinanceiro) REFERENCES tb_entidadefinanceiro (id_entidadefinanceiro)
ALTER TABLE tb_entidadecartaoconfig ADD CONSTRAINT FK_C556964FCD869197 FOREIGN KEY (id_cartaoconfig) REFERENCES tb_cartaoconfig (id_cartaoconfig)
ALTER TABLE tb_premioproduto ADD CONSTRAINT FK_331FFAC62FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_premioproduto ADD CONSTRAINT FK_331FFAC68231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_premioproduto ADD CONSTRAINT FK_331FFAC63A005945 FOREIGN KEY (id_premio) REFERENCES tb_premio (id_premio)
ALTER TABLE tb_premioproduto ADD CONSTRAINT FK_331FFAC616FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_esquemaconfiguracaoitem ADD CONSTRAINT FK_40020F6978C3520B FOREIGN KEY (id_esquemaconfiguracao) REFERENCES tb_esquemaconfiguracao (id_esquemaconfiguracao)
ALTER TABLE tb_esquemaconfiguracaoitem ADD CONSTRAINT FK_40020F696B954C8B FOREIGN KEY (id_itemconfiguracao) REFERENCES tb_itemconfiguracao (id_itemconfiguracao)
ALTER TABLE tb_mensagemmotivacional ADD CONSTRAINT FK_DFFDC1762FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF509281E3AF FOREIGN KEY (id_avaliacaoaplicacao) REFERENCES tb_avaliacaoaplicacao (id_avaliacaoaplicacao)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF5095EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF50C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF502FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF50E92EFB05 FOREIGN KEY (id_avaliacao) REFERENCES tb_avaliacao (id_avaliacao)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF50FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacaoagendamento ADD CONSTRAINT FK_B82DDF50B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_projetoentidade ADD CONSTRAINT FK_79FAAA845E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_projetoentidade ADD CONSTRAINT FK_79FAAA84B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_projetoentidade ADD CONSTRAINT FK_79FAAA84C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_projetoentidade ADD CONSTRAINT FK_79FAAA842FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_localaula ADD CONSTRAINT FK_A8CB110CCA131A2C FOREIGN KEY (id_catraca) REFERENCES tb_catraca (id_catraca)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F550A6D247 FOREIGN KEY (id_modalidadesaladeaula) REFERENCES tb_modalidadesaladeaula (id_modalidadesaladeaula)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F56D176833 FOREIGN KEY (id_tiposaladeaula) REFERENCES tb_tiposaladeaula (id_tiposaladeaula)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F5B3B7BA0A FOREIGN KEY (id_periodoletivo) REFERENCES tb_periodoletivo (id_periodoletivo)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F5C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F5CA52AB42 FOREIGN KEY (id_categoriasala) REFERENCES tb_categoriasala (id_categoriasala)
ALTER TABLE tb_saladeaula ADD CONSTRAINT FK_461F28F59107A21B FOREIGN KEY (id_usuariocancelamento) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_cartacredito ADD CONSTRAINT FK_5DE4D7FE2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_cartacredito ADD CONSTRAINT FK_5DE4D7FEC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_cartacredito ADD CONSTRAINT FK_5DE4D7FEFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_cartacredito ADD CONSTRAINT FK_5DE4D7FE92BA0BD2 FOREIGN KEY (id_cancelamento) REFERENCES tb_cancelamento (id_cancelamento)
ALTER TABLE tb_cartacredito ADD CONSTRAINT FK_5DE4D7FEB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_mensagemcobranca ADD CONSTRAINT FK_24166436136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_mensagemcobranca ADD CONSTRAINT FK_24166436B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_erro ADD CONSTRAINT FK_E1BD01BF4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_erro ADD CONSTRAINT FK_E1BD01BF463E54B8 FOREIGN KEY (id_processo) REFERENCES tb_processo (id_processo)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4443FB97 FOREIGN KEY (id_formapagamento) REFERENCES tb_formapagamento (id_formapagamento)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF416FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF49D4E6E40 FOREIGN KEY (id_campanhacomercialpremio) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF430B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4BAC6B6DF FOREIGN KEY (id_atendente) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4490DB0A9 FOREIGN KEY (id_tipocampanha) REFERENCES tb_tipocampanha (id_tipocampanha)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4608A1C2 FOREIGN KEY (id_origemvenda) REFERENCES tb_origemvenda (id_origemvenda)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4B960F69E FOREIGN KEY (id_prevenda) REFERENCES tb_prevenda (id_prevenda)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4F098E264 FOREIGN KEY (id_protocolo) REFERENCES tb_protocolo (id_protocolo)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4F36DE71B FOREIGN KEY (id_enderecoentrega) REFERENCES tb_endereco (id_endereco)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF479E459B6 FOREIGN KEY (id_ocorrencia) REFERENCES tb_ocorrencia (id_ocorrencia)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF4EA7023B6 FOREIGN KEY (id_uploadcontrato) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF42F43E9C3 FOREIGN KEY (id_cupom) REFERENCES tb_cupom (id_cupom)
ALTER TABLE tb_venda ADD CONSTRAINT FK_AFCEACF47DCD6A89 FOREIGN KEY (id_campanhapontualidade) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_avaliacaoconjuntoreferencia ADD CONSTRAINT FK_68C497555E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_avaliacaoconjuntoreferencia ADD CONSTRAINT FK_68C49755CAC67ADB FOREIGN KEY (id_modulo) REFERENCES tb_modulo (id_modulo)
ALTER TABLE tb_lancamentovenda ADD CONSTRAINT FK_7D25FC2E2BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_lancamentovenda ADD CONSTRAINT FK_7D25FC2EC319336A FOREIGN KEY (id_lancamento) REFERENCES tb_lancamento (id_lancamento)
ALTER TABLE tb_log ADD CONSTRAINT FK_2193647AFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_log ADD CONSTRAINT FK_2193647AB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_log ADD CONSTRAINT FK_2193647AB052C3AA FOREIGN KEY (id_perfil) REFERENCES tb_perfil (id_perfil)
ALTER TABLE tb_log ADD CONSTRAINT FK_2193647A5BC834DB FOREIGN KEY (id_funcionalidade) REFERENCES tb_funcionalidade (id_funcionalidade)
ALTER TABLE tb_log ADD CONSTRAINT FK_2193647A6391BFB6 FOREIGN KEY (id_operacao) REFERENCES tb_operacaolog (id_operacaolog)
ALTER TABLE tb_projetopedagogicointegracao ADD CONSTRAINT FK_F1E4C00A5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_projetopedagogicointegracao ADD CONSTRAINT FK_F1E4C00A4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_evolucao ADD CONSTRAINT FK_58CDA58230B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_enviomensagem ADD CONSTRAINT FK_EDACFD2FCE9EDE25 FOREIGN KEY (id_mensagem) REFERENCES tb_mensagem (id_mensagem)
ALTER TABLE tb_enviomensagem ADD CONSTRAINT FK_EDACFD2FA2782CAC FOREIGN KEY (id_tipoenvio) REFERENCES tb_tipoenvio (id_tipoenvio)
ALTER TABLE tb_enviomensagem ADD CONSTRAINT FK_EDACFD2F30B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_enviomensagem ADD CONSTRAINT FK_EDACFD2FDBED4F98 FOREIGN KEY (id_emailconfig) REFERENCES tb_emailconfig (id_emailconfig)
ALTER TABLE tb_enviomensagem ADD CONSTRAINT FK_EDACFD2F4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_pessoaendereco ADD CONSTRAINT FK_8132E9C9B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pessoaendereco ADD CONSTRAINT FK_8132E9C9A83B3A9B FOREIGN KEY (id_endereco) REFERENCES tb_endereco (id_endereco)
ALTER TABLE tb_premio ADD CONSTRAINT FK_A1E0F5E42FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_premio ADD CONSTRAINT FK_A1E0F5E416FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_premio ADD CONSTRAINT FK_A1E0F5E4A5316601 FOREIGN KEY (id_cupomcampanha) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_situacaointegracao ADD CONSTRAINT FK_E8EA21474F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_turmaentidade ADD CONSTRAINT FK_F52B178A2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_turmaentidade ADD CONSTRAINT FK_F52B178AC5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_turmaentidade ADD CONSTRAINT FK_F52B178AB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_turmaintegracao ADD CONSTRAINT FK_ED96AD7BC5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_turmaintegracao ADD CONSTRAINT FK_ED96AD7B4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_pa_boasvindas ADD CONSTRAINT FK_6113D834136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_boasvindas ADD CONSTRAINT FK_6113D834B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pa_boasvindas ADD CONSTRAINT FK_6113D8342FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED9618231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED9612BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED961D70D9966 FOREIGN KEY (id_tiposelecao) REFERENCES tb_tiposelecao (id_tiposelecao)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED96116FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED96195EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED961C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED9618958479A FOREIGN KEY (id_produtocombo) REFERENCES tb_produtocombo (id_produtocombo)
ALTER TABLE tb_vendaproduto ADD CONSTRAINT FK_E16ED961C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_entidaderesponsavellegal ADD CONSTRAINT FK_843DB550C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_entidaderesponsavellegal ADD CONSTRAINT FK_843DB550F88F3E41 FOREIGN KEY (id_tipoentidaderesponsavel) REFERENCES tb_tipoentidaderesponsavellegal (id_tipoentidaderesponsavel)
ALTER TABLE tb_entidaderesponsavellegal ADD CONSTRAINT FK_843DB550FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_entidaderesponsavellegal ADD CONSTRAINT FK_843DB550B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_entidaderesponsavellegal ADD CONSTRAINT FK_843DB5504FD882EB FOREIGN KEY (sg_uf) REFERENCES tb_uf (sg_uf)
ALTER TABLE tb_motivo ADD CONSTRAINT FK_A42E212A43D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_motivo ADD CONSTRAINT FK_A42E212A2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_motivo ADD CONSTRAINT FK_A42E212AC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C3B81EAF15 FOREIGN KEY (id_textomensagem) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C3C88A814 FOREIGN KEY (id_textoinstrucao) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C39B9DCB8E FOREIGN KEY (id_assuntoco) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C3CA9E9832 FOREIGN KEY (id_categoriaocorrencia) REFERENCES tb_categoriaocorrencia (id_categoriaocorrencia)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C3B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pa_dadoscadastrais ADD CONSTRAINT FK_AE3721C32FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F15443FB97 FOREIGN KEY (id_formapagamento) REFERENCES tb_formapagamento (id_formapagamento)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F159B119E27 FOREIGN KEY (id_meiopagamento) REFERENCES tb_meiopagamento (id_meiopagamento)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F15BC9063A1 FOREIGN KEY (id_tipoendereco) REFERENCES tb_tipoendereco (id_tipoendereco)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F156DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F15C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F152CFC0759 FOREIGN KEY (id_usuarioatendimento) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_prevenda ADD CONSTRAINT FK_D11C0F15FD7DED57 FOREIGN KEY (id_usuariodescarte) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_categoria ADD CONSTRAINT FK_B427885B2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_categoria ADD CONSTRAINT FK_B427885B9AB457F8 FOREIGN KEY (id_uploadimagem) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_categoria ADD CONSTRAINT FK_B427885B952FF260 FOREIGN KEY (id_categoriapai) REFERENCES tb_categoria (id_categoria)
ALTER TABLE tb_categoria ADD CONSTRAINT FK_B427885BC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_formadiavencimento ADD CONSTRAINT FK_C61A156E443FB97 FOREIGN KEY (id_formapagamento) REFERENCES tb_formapagamento (id_formapagamento)
ALTER TABLE tb_arquivoretornomaterial ADD CONSTRAINT FK_5914CF94B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_arquivoretornomaterial ADD CONSTRAINT FK_5914CF942FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_arquivoretornomaterial ADD CONSTRAINT FK_5914CF94C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_arquivoretornomaterial ADD CONSTRAINT FK_5914CF94318A53F2 FOREIGN KEY (id_upload) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_aplicadorprovaentidade ADD CONSTRAINT FK_8802BE6667F8C046 FOREIGN KEY (id_aplicadorprova) REFERENCES tb_aplicadorprova (id_aplicadorprova)
ALTER TABLE tb_aplicadorprovaentidade ADD CONSTRAINT FK_8802BE66B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDFC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDFAE23E535 FOREIGN KEY (id_assuntocopai) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDF43D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDF2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDF4EFA7A34 FOREIGN KEY (id_tipoocorrencia) REFERENCES tb_tipoocorrencia (id_tipoocorrencia)
ALTER TABLE tb_assuntoco ADD CONSTRAINT FK_E19FEDDF136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_boletoconfig ADD CONSTRAINT FK_F27C882D68B0E68E FOREIGN KEY (id_contaentidade) REFERENCES tb_contaentidade (id_contaentidade)
ALTER TABLE tb_boletoconfig ADD CONSTRAINT FK_F27C882D70F58862 FOREIGN KEY (id_entidadeendereco) REFERENCES tb_entidadeendereco (id_entidadeendereco)
ALTER TABLE tb_formapagamentoparcela ADD CONSTRAINT FK_C8489A619B119E27 FOREIGN KEY (id_meiopagamento) REFERENCES tb_meiopagamento (id_meiopagamento)
ALTER TABLE tb_produtocombo ADD CONSTRAINT FK_C9F6E16F8231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtocombo ADD CONSTRAINT FK_C9F6E16FA3AE31BC FOREIGN KEY (id_produtoitem) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtocombo ADD CONSTRAINT FK_C9F6E16FFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D42B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D42C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D42D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D422FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D42F9386BC4 FOREIGN KEY (id_professor) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D4238CEC03A FOREIGN KEY (id_tipomaterial) REFERENCES tb_tipodematerial (id_tipodematerial)
ALTER TABLE tb_itemdematerialpresencial ADD CONSTRAINT FK_AB057D42C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_produtoarea ADD CONSTRAINT FK_1A8CBA208231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtoarea ADD CONSTRAINT FK_1A8CBA20ED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_enviodestinatario ADD CONSTRAINT FK_9E59D92B2F2A5850 FOREIGN KEY (id_tipodestinatario) REFERENCES tb_tipodestinatario (id_tipodestinatario)
ALTER TABLE tb_enviodestinatario ADD CONSTRAINT FK_9E59D92B203B551A FOREIGN KEY (id_enviomensagem) REFERENCES tb_enviomensagem (id_enviomensagem)
ALTER TABLE tb_enviodestinatario ADD CONSTRAINT FK_9E59D92BFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_enviodestinatario ADD CONSTRAINT FK_9E59D92B95EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_avaliacao ADD CONSTRAINT FK_932CDD542F3689EE FOREIGN KEY (id_tipoavaliacao) REFERENCES tb_tipoavaliacao (id_tipoavaliacao)
ALTER TABLE tb_avaliacao ADD CONSTRAINT FK_932CDD542FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacao ADD CONSTRAINT FK_932CDD54B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_campanhadesconto ADD CONSTRAINT FK_1E9F1F3316FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_campanhadesconto ADD CONSTRAINT FK_1E9F1F339B119E27 FOREIGN KEY (id_meiopagamento) REFERENCES tb_meiopagamento (id_meiopagamento)
ALTER TABLE tb_informacaoacademicapessoa ADD CONSTRAINT FK_D52299ECFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_informacaoacademicapessoa ADD CONSTRAINT FK_D52299ECB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_informacaoacademicapessoa ADD CONSTRAINT FK_D52299EC6DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_produtoprojetopedagogico ADD CONSTRAINT FK_C5BCA3D35E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_produtoprojetopedagogico ADD CONSTRAINT FK_C5BCA3D38231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtoprojetopedagogico ADD CONSTRAINT FK_C5BCA3D3B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_produtoprojetopedagogico ADD CONSTRAINT FK_C5BCA3D3C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_horarioaula ADD CONSTRAINT FK_67E4BCB39122652 FOREIGN KEY (id_turno) REFERENCES tb_turno (id_turno)
ALTER TABLE tb_horarioaula ADD CONSTRAINT FK_67E4BCB3B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_modulo ADD CONSTRAINT FK_5126D67AC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_modulo ADD CONSTRAINT FK_5126D67A5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_itemgradehorariaturma ADD CONSTRAINT FK_DDA88509E323A720 FOREIGN KEY (id_itemgradehoraria) REFERENCES tb_itemgradehoraria (id_itemgradehoraria)
ALTER TABLE tb_itemgradehorariaturma ADD CONSTRAINT FK_DDA88509C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_materialprojeto ADD CONSTRAINT FK_5B36165F5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_produtocarreira ADD CONSTRAINT FK_6229C6A28231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtocarreira ADD CONSTRAINT FK_6229C6A2EFE671B4 FOREIGN KEY (id_carreira) REFERENCES tb_carreira (id_carreira)
ALTER TABLE tb_textovariaveis ADD CONSTRAINT FK_430962BE88A1ED7 FOREIGN KEY (id_textocategoria) REFERENCES tb_textocategoria (id_textocategoria)
ALTER TABLE tb_textovariaveis ADD CONSTRAINT FK_430962BFF3097B2 FOREIGN KEY (id_tipotextovariavel) REFERENCES tb_tipotextovariavel (id_tipotextovariavel)
ALTER TABLE tb_categoriaocorrencia ADD CONSTRAINT FK_679E0675C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_categoriaocorrencia ADD CONSTRAINT FK_679E06754EFA7A34 FOREIGN KEY (id_tipoocorrencia) REFERENCES tb_tipoocorrencia (id_tipoocorrencia)
ALTER TABLE tb_categoriaocorrencia ADD CONSTRAINT FK_679E06752FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_categoriaocorrencia ADD CONSTRAINT FK_679E067543D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_entregadeclaracao ADD CONSTRAINT FK_C0A6DFC2C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_entregadeclaracao ADD CONSTRAINT FK_C0A6DFC2A1C07F94 FOREIGN KEY (id_vendaproduto) REFERENCES tb_vendaproduto (id_vendaproduto)
ALTER TABLE tb_cartaoconfig ADD CONSTRAINT FK_8D283762797090DD FOREIGN KEY (id_cartaobandeira) REFERENCES tb_cartaobandeira (id_cartaobandeira)
ALTER TABLE tb_cartaoconfig ADD CONSTRAINT FK_8D283762F4810C88 FOREIGN KEY (id_cartaooperadora) REFERENCES tb_cartaooperadora (id_cartaooperadora)
ALTER TABLE tb_cartaoconfig ADD CONSTRAINT FK_8D2837624F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACF91098FE FOREIGN KEY (id_tiporegrapagamento) REFERENCES tb_tiporegrapagamento (id_tiporegrapagamento)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7AC5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACF9386BC4 FOREIGN KEY (id_professor) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACD8F1FC2A FOREIGN KEY (id_cargahoraria) REFERENCES tb_cargahoraria (id_cargahoraria)
ALTER TABLE tb_regrapagamento ADD CONSTRAINT FK_BEC9D7ACE0478973 FOREIGN KEY (id_tipodisciplina) REFERENCES tb_tipodisciplina (id_tipodisciplina)
ALTER TABLE tb_produtovalor ADD CONSTRAINT FK_5643F60D8231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtovalor ADD CONSTRAINT FK_5643F60D2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_produtovalor ADD CONSTRAINT FK_5643F60DBD3FEC5D FOREIGN KEY (id_tipoprodutovalor) REFERENCES tb_tipoprodutovalor (id_tipoprodutovalor)
ALTER TABLE tb_transacaofinanceira ADD CONSTRAINT FK_DE3D9D2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_transacaofinanceira ADD CONSTRAINT FK_DE3D9D2BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867B052C3AA FOREIGN KEY (id_perfil) REFERENCES tb_perfil (id_perfil)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867ED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_414318675E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_414318675A094BB1 FOREIGN KEY (id_saladeaula) REFERENCES tb_saladeaula (id_saladeaula)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867A233E7B7 FOREIGN KEY (id_livro) REFERENCES tb_livro (id_livro)
ALTER TABLE tb_usuarioperfilentidadereferencia ADD CONSTRAINT FK_41431867733DB0F6 FOREIGN KEY (id_titulacao) REFERENCES tb_titulacao (id_titulacao)
ALTER TABLE tb_contrato ADD CONSTRAINT FK_5ECE703D30B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_contrato ADD CONSTRAINT FK_5ECE703DC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_contrato ADD CONSTRAINT FK_5ECE703D2BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_contrato ADD CONSTRAINT FK_5ECE703D63FC6B92 FOREIGN KEY (id_contratoregra) REFERENCES tb_contratoregra (id_contratoregra)
ALTER TABLE tb_horariodiasemana ADD CONSTRAINT FK_1CBC338F18209A9C FOREIGN KEY (id_diasemana) REFERENCES tb_diasemana (id_diasemana)
ALTER TABLE tb_horariodiasemana ADD CONSTRAINT FK_1CBC338F16492F59 FOREIGN KEY (id_horarioaula) REFERENCES tb_horarioaula (id_horarioaula)
ALTER TABLE tb_entregadocumentos ADD CONSTRAINT FK_C944D3CABB32A9E1 FOREIGN KEY (id_documentos) REFERENCES tb_documentos (id_documentos)
ALTER TABLE tb_entregadocumentos ADD CONSTRAINT FK_C944D3CA95EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_entregadocumentos ADD CONSTRAINT FK_C944D3CAC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_entregadocumentos ADD CONSTRAINT FK_C944D3CA697D21AC FOREIGN KEY (id_contratoresponsavel) REFERENCES tb_contratoresponsavel (id_contratoresponsavel)
ALTER TABLE tb_pessoa ADD CONSTRAINT FK_A108B2CEB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pessoa ADD CONSTRAINT FK_A108B2CEF57D32FD FOREIGN KEY (id_pais) REFERENCES tb_pais (id_pais)
ALTER TABLE tb_pessoa ADD CONSTRAINT FK_A108B2CE7EAD49C7 FOREIGN KEY (id_municipio) REFERENCES tb_municipio (id_municipio)
ALTER TABLE tb_pessoa ADD CONSTRAINT FK_A108B2CEC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_pessoa ADD CONSTRAINT FK_A108B2CE40C6C314 FOREIGN KEY (id_estadocivil) REFERENCES tb_estadocivil (id_estadocivil)
ALTER TABLE tb_usuariodevice ADD CONSTRAINT FK_ED76495CFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_usuariosessao ADD CONSTRAINT FK_7292677FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_emailentidademensagem ADD CONSTRAINT FK_13590668DBED4F98 FOREIGN KEY (id_emailconfig) REFERENCES tb_emailconfig (id_emailconfig)
ALTER TABLE tb_emailentidademensagem ADD CONSTRAINT FK_135906682FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_emailentidademensagem ADD CONSTRAINT FK_13590668136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_concurso ADD CONSTRAINT FK_40F888F8B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_concurso ADD CONSTRAINT FK_40F888F8C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_produtolivro ADD CONSTRAINT FK_347C77A2B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_produtolivro ADD CONSTRAINT FK_347C77A2A233E7B7 FOREIGN KEY (id_livro) REFERENCES tb_livro (id_livro)
ALTER TABLE tb_entidadefinanceiro ADD CONSTRAINT FK_6208FCD5F2B9C522 FOREIGN KEY (id_textosistemarecibo) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_entidadefinanceiro ADD CONSTRAINT FK_6208FCD5D74876C1 FOREIGN KEY (id_textoavisoatraso) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_entidadefinanceiro ADD CONSTRAINT FK_6208FCD5666AC2A9 FOREIGN KEY (id_reciboconsolidado) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_entidadefinanceiro ADD CONSTRAINT FK_6208FCD5CD640913 FOREIGN KEY (id_textochequesdevolvidos) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_turma ADD CONSTRAINT FK_41E949562FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_turma ADD CONSTRAINT FK_41E94956C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_turma ADD CONSTRAINT FK_41E9495630B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_turma ADD CONSTRAINT FK_41E9495643D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B0909DBD6D4B FOREIGN KEY (id_gradehoraria) REFERENCES tb_gradehoraria (id_gradehoraria)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B0909A6508A3 FOREIGN KEY (id_unidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B09043D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B0909122652 FOREIGN KEY (id_turno) REFERENCES tb_turno (id_turno)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B090C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B0905E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B09018209A9C FOREIGN KEY (id_diasemana) REFERENCES tb_diasemana (id_diasemana)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B090F9386BC4 FOREIGN KEY (id_professor) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemgradehoraria ADD CONSTRAINT FK_C936B090D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5C319336A FOREIGN KEY (id_lancamento) REFERENCES tb_lancamento (id_lancamento)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5A1C07F94 FOREIGN KEY (id_vendaproduto) REFERENCES tb_vendaproduto (id_vendaproduto)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5992AA1B5 FOREIGN KEY (id_lancamentopagamento) REFERENCES tb_lancamento (id_lancamento)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_comissaolancamento ADD CONSTRAINT FK_5BB08FB5B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_contatostelefone ADD CONSTRAINT FK_4ECEE777654256CE FOREIGN KEY (id_tipotelefone) REFERENCES tb_tipotelefone (id_tipotelefone)
ALTER TABLE tb_contaentidade ADD CONSTRAINT FK_A5274EBBB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_contaentidade ADD CONSTRAINT FK_A5274EBB404C14D3 FOREIGN KEY (id_tipodeconta) REFERENCES tb_tipodeconta (id_tipodeconta)
ALTER TABLE tb_contaentidade ADD CONSTRAINT FK_A5274EBB13FE11CC FOREIGN KEY (st_banco) REFERENCES tb_banco (st_banco)
ALTER TABLE tb_modelocarteirinhaentidade ADD CONSTRAINT FK_E7C711A8B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_modelocarteirinhaentidade ADD CONSTRAINT FK_E7C711A87565DE0 FOREIGN KEY (id_modelocarteirinha) REFERENCES tb_modelocarteirinha (id_modelocarteirinha)
ALTER TABLE tb_matriculadisciplina ADD CONSTRAINT FK_64D6DD5FDAC7F78D FOREIGN KEY (id_matriculaoriginal) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_pedidointegracao ADD CONSTRAINT FK_C95062912BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_pedidointegracao ADD CONSTRAINT FK_C95062914F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F35E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F3FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F32FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F330B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F378241A5C FOREIGN KEY (id_matriculaorigem) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F31C03E728 FOREIGN KEY (id_situacaoagendamento) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F3C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F3C5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F392BA0BD2 FOREIGN KEY (id_cancelamento) REFERENCES tb_cancelamento (id_cancelamento)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F3442A8246 FOREIGN KEY (id_evolucaocertificacao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_matricula ADD CONSTRAINT FK_EFE882F3C3201BE6 FOREIGN KEY (id_matriculavinculada) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_gradehoraria ADD CONSTRAINT FK_DD13CBBEC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_gradehoraria ADD CONSTRAINT FK_DD13CBBEB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_funcionalidade ADD CONSTRAINT FK_DB667504C7151A7 FOREIGN KEY (id_funcionalidadepai) REFERENCES tb_funcionalidade (id_funcionalidade)
ALTER TABLE tb_funcionalidade ADD CONSTRAINT FK_DB66750C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_funcionalidade ADD CONSTRAINT FK_DB667504A6F2802 FOREIGN KEY (id_tipofuncionalidade) REFERENCES tb_tipofuncionalidade (id_tipofuncionalidade)
ALTER TABLE tb_funcionalidade ADD CONSTRAINT FK_DB667504F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_relatorio ADD CONSTRAINT FK_848E807994A212BF FOREIGN KEY (id_tipoorigemrelatorio) REFERENCES tb_tipoorigemrelatorio (id_tipoorigemrelatorio)
ALTER TABLE tb_relatorio ADD CONSTRAINT FK_848E80795BC834DB FOREIGN KEY (id_funcionalidade) REFERENCES tb_funcionalidade (id_funcionalidade)
ALTER TABLE tb_relatorio ADD CONSTRAINT FK_848E8079FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_relatorio ADD CONSTRAINT FK_848E8079B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_tramite ADD CONSTRAINT FK_E0E5F587EBAAF349 FOREIGN KEY (id_tipotramite) REFERENCES tb_tipotramite (id_tipotramite)
ALTER TABLE tb_tramite ADD CONSTRAINT FK_E0E5F587FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_tramite ADD CONSTRAINT FK_E0E5F587B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_tramite ADD CONSTRAINT FK_E0E5F587318A53F2 FOREIGN KEY (id_upload) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_tramiteentregamaterial ADD CONSTRAINT FK_D036B3EE41AF8655 FOREIGN KEY (id_tramite) REFERENCES tb_tramite (id_tramite)
ALTER TABLE tb_tramiteentregamaterial ADD CONSTRAINT FK_D036B3EEFA72F2BF FOREIGN KEY (id_entregamaterial) REFERENCES tb_entregamaterial (id_entregamaterial)
ALTER TABLE tb_vendacartacredito ADD CONSTRAINT FK_DF0C8AB21D4A710B FOREIGN KEY (id_cartacredito) REFERENCES tb_cartacredito (id_cartacredito)
ALTER TABLE tb_vendacartacredito ADD CONSTRAINT FK_DF0C8AB22BA0BD34 FOREIGN KEY (id_venda) REFERENCES tb_venda (id_venda)
ALTER TABLE tb_textoexibicaocategoria ADD CONSTRAINT FK_4AD0030E88A1ED7 FOREIGN KEY (id_textocategoria) REFERENCES tb_textocategoria (id_textocategoria)
ALTER TABLE tb_textoexibicaocategoria ADD CONSTRAINT FK_4AD003065A8E33F FOREIGN KEY (id_textoexibicao) REFERENCES tb_textoexibicao (id_textoexibicao)
ALTER TABLE tb_ocorrenciaresponsavel ADD CONSTRAINT FK_6F168CD9FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F1295EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F12E92EFB05 FOREIGN KEY (id_avaliacao) REFERENCES tb_avaliacao (id_avaliacao)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F1250380BFD FOREIGN KEY (id_avaliacaoagendamento) REFERENCES tb_avaliacaoagendamento (id_avaliacaoagendamento)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F122FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F12C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_avaliacaoaluno ADD CONSTRAINT FK_155F1F12A432CCDB FOREIGN KEY (id_usuarioalteracao) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_campanhacategorias ADD CONSTRAINT FK_ABA0E020CE25AE0A FOREIGN KEY (id_categoria) REFERENCES tb_categoria (id_categoria)
ALTER TABLE tb_campanhacategorias ADD CONSTRAINT FK_ABA0E02016FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_cargoconcurso ADD CONSTRAINT FK_77D751E428847173 FOREIGN KEY (id_concurso) REFERENCES tb_concurso (id_concurso)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A4FCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A4B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A465A8E33F FOREIGN KEY (id_textoexibicao) REFERENCES tb_textoexibicao (id_textoexibicao)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A4E88A1ED7 FOREIGN KEY (id_textocategoria) REFERENCES tb_textocategoria (id_textocategoria)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A4C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_textosistema ADD CONSTRAINT FK_53C104A4EAA41207 FOREIGN KEY (id_orientacaotexto) REFERENCES tb_orientacaotexto (id_orientacaotexto)
ALTER TABLE tb_documentos ADD CONSTRAINT FK_A724CAA5F09CE00B FOREIGN KEY (id_tipodocumento) REFERENCES tb_tipodocumento (id_tipodocumento)
ALTER TABLE tb_documentos ADD CONSTRAINT FK_A724CAA5C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_documentos ADD CONSTRAINT FK_A724CAA5B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_documentos ADD CONSTRAINT FK_A724CAA5D357A3AE FOREIGN KEY (id_documentosobrigatoriedade) REFERENCES tb_documentosobrigatoriedade (id_documentosobrigatoriedade)
ALTER TABLE tb_documentos ADD CONSTRAINT FK_A724CAA599AE2CEF FOREIGN KEY (id_documentoscategoria) REFERENCES tb_documentoscategoria (id_documentoscategoria)
ALTER TABLE tb_autorizacaoentrada ADD CONSTRAINT FK_F72B3B9CFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_autorizacaoentrada ADD CONSTRAINT FK_F72B3B9C2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_autorizacaoentrada ADD CONSTRAINT FK_F72B3B9C136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_autorizacaoentrada ADD CONSTRAINT FK_F72B3B9CB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_enviocobranca ADD CONSTRAINT FK_46C989BAE037386 FOREIGN KEY (id_mensagemcobranca) REFERENCES tb_mensagemcobranca (id_mensagemcobranca)
ALTER TABLE tb_enviocobranca ADD CONSTRAINT FK_46C989BACE9EDE25 FOREIGN KEY (id_mensagem) REFERENCES tb_mensagem (id_mensagem)
ALTER TABLE tb_enviocobranca ADD CONSTRAINT FK_46C989BAC319336A FOREIGN KEY (id_lancamento) REFERENCES tb_lancamento (id_lancamento)
ALTER TABLE tb_categoriaentidade ADD CONSTRAINT FK_AF72368D2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0B52655E0B FOREIGN KEY (id_perfilpai) REFERENCES tb_perfil (id_perfil)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0BC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0BB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0B3C1D0386 FOREIGN KEY (id_entidadeclasse) REFERENCES tb_entidadeclasse (id_entidadeclasse)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0B2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0BEA87A5D FOREIGN KEY (id_perfilpedagogico) REFERENCES tb_perfilpedagogico (id_perfilpedagogico)
ALTER TABLE tb_perfil ADD CONSTRAINT FK_2BB26F0B4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_areaprojetopedagogico ADD CONSTRAINT FK_FAAA0166ED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_areaprojetopedagogico ADD CONSTRAINT FK_FAAA01665E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_municipio ADD CONSTRAINT FK_4AF6F964FD882EB FOREIGN KEY (sg_uf) REFERENCES tb_uf (sg_uf)
ALTER TABLE tb_serienivelensino ADD CONSTRAINT FK_8E957B9C44BFD204 FOREIGN KEY (id_serie) REFERENCES tb_serie (id_serie)
ALTER TABLE tb_serienivelensino ADD CONSTRAINT FK_8E957B9C6DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_entidadefuncionalidade ADD CONSTRAINT FK_8C4DE6BFC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_tramitematricula ADD CONSTRAINT FK_E7BCFD1F41AF8655 FOREIGN KEY (id_tramite) REFERENCES tb_tramite (id_tramite)
ALTER TABLE tb_tramitematricula ADD CONSTRAINT FK_E7BCFD1F95EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_produtoentidade ADD CONSTRAINT FK_3E3DBD51B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_produtoentidade ADD CONSTRAINT FK_3E3DBD518231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_sistemaentidademensagem ADD CONSTRAINT FK_A758364DFB6270BA FOREIGN KEY (id_mensagempadrao) REFERENCES tb_mensagempadrao (id_mensagempadrao)
ALTER TABLE tb_sistemaentidademensagem ADD CONSTRAINT FK_A758364D136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_sistemaentidademensagem ADD CONSTRAINT FK_A758364D2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_sistemaentidademensagem ADD CONSTRAINT FK_A758364DB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3BB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3BC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3B480B1473 FOREIGN KEY (id_tipodesconto) REFERENCES tb_tipodesconto (id_tipodesconto)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3B2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3B490DB0A9 FOREIGN KEY (id_tipocampanha) REFERENCES tb_tipocampanha (id_tipocampanha)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3B8A6D73C2 FOREIGN KEY (id_categoriacampanha) REFERENCES tb_categoriacampanha (id_categoriacampanha)
ALTER TABLE tb_campanhacomercial ADD CONSTRAINT FK_DDB6DF3B3A005945 FOREIGN KEY (id_premio) REFERENCES tb_premio (id_premio)
ALTER TABLE tb_pa_mensageminformativa ADD CONSTRAINT FK_E9D7F883B81EAF15 FOREIGN KEY (id_textomensagem) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_mensageminformativa ADD CONSTRAINT FK_E9D7F88310D64153 FOREIGN KEY (id_textoemail) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_pa_mensageminformativa ADD CONSTRAINT FK_E9D7F883B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_pa_mensageminformativa ADD CONSTRAINT FK_E9D7F8832FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_motivoentidade ADD CONSTRAINT FK_5A54F7742FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_motivoentidade ADD CONSTRAINT FK_5A54F7743FCE8D8B FOREIGN KEY (id_motivo) REFERENCES tb_motivo (id_motivo)
ALTER TABLE tb_motivoentidade ADD CONSTRAINT FK_5A54F774B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_produtouf ADD CONSTRAINT FK_8C0E37178231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtouf ADD CONSTRAINT FK_8C0E37174FD882EB FOREIGN KEY (sg_uf) REFERENCES tb_uf (sg_uf)
ALTER TABLE tb_entidadeemail ADD CONSTRAINT FK_D6C3EAB9DBED4F98 FOREIGN KEY (id_emailconfig) REFERENCES tb_emailconfig (id_emailconfig)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D7445A094BB1 FOREIGN KEY (id_saladeaula) REFERENCES tb_saladeaula (id_saladeaula)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D7446D436514 FOREIGN KEY (id_usuariocoordenador) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D744337E47CB FOREIGN KEY (id_usuariopedagogico) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D7445C8635CB FOREIGN KEY (id_usuarioprofessor) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D74475150EEA FOREIGN KEY (id_usuariofinanceiro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_encerramentosala ADD CONSTRAINT FK_DC16D744CA3A2B1E FOREIGN KEY (id_usuariorecusa) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_matriculaintegracao ADD CONSTRAINT FK_434B59D695EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_matriculaintegracao ADD CONSTRAINT FK_434B59D64F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D578595EAA4A2 FOREIGN KEY (id_matricula) REFERENCES tb_matricula (id_matricula)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D578592BA0BD2 FOREIGN KEY (id_cancelamento) REFERENCES tb_cancelamento (id_cancelamento)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D57852FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D578530B15C09 FOREIGN KEY (id_evolucao) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D578511732B07 FOREIGN KEY (id_evolucaocancelamento) REFERENCES tb_evolucao (id_evolucao)
ALTER TABLE tb_logcancelamento ADD CONSTRAINT FK_201D5785C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_concursoproduto ADD CONSTRAINT FK_1A8EE87D28847173 FOREIGN KEY (id_concurso) REFERENCES tb_concurso (id_concurso)
ALTER TABLE tb_concursoproduto ADD CONSTRAINT FK_1A8EE87D8231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_dashboard ADD CONSTRAINT FK_A6A3658EC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_dashboard ADD CONSTRAINT FK_A6A3658E5BC834DB FOREIGN KEY (id_funcionalidade) REFERENCES tb_funcionalidade (id_funcionalidade)
ALTER TABLE tb_turmaturno ADD CONSTRAINT FK_F989DFD59122652 FOREIGN KEY (id_turno) REFERENCES tb_turno (id_turno)
ALTER TABLE tb_entidadeendereco ADD CONSTRAINT FK_5AE09FD2A83B3A9B FOREIGN KEY (id_endereco) REFERENCES tb_endereco (id_endereco)
ALTER TABLE tb_dadosacesso ADD CONSTRAINT FK_97BE9BAAFCF8192D FOREIGN KEY (id_usuario) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_dadosacesso ADD CONSTRAINT FK_97BE9BAA2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_dadosacesso ADD CONSTRAINT FK_97BE9BAAB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_mensagem ADD CONSTRAINT FK_A6E227AEC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_mensagem ADD CONSTRAINT FK_A6E227AE5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_mensagem ADD CONSTRAINT FK_A6E227AEED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_mensagem ADD CONSTRAINT FK_A6E227AEC5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_cupom ADD CONSTRAINT FK_AB2DF8032FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_cupom ADD CONSTRAINT FK_AB2DF803480B1473 FOREIGN KEY (id_tipodesconto) REFERENCES tb_tipodesconto (id_tipodesconto)
ALTER TABLE tb_cupom ADD CONSTRAINT FK_AB2DF80316FD79A0 FOREIGN KEY (id_campanhacomercial) REFERENCES tb_campanhacomercial (id_campanhacomercial)
ALTER TABLE tb_entidade ADD CONSTRAINT FK_DB8EF3CCC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_entidade ADD CONSTRAINT FK_DB8EF3CC43D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_entidade ADD CONSTRAINT FK_DB8EF3CC2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_entidade ADD CONSTRAINT FK_DB8EF3CC78C3520B FOREIGN KEY (id_esquemaconfiguracao) REFERENCES tb_esquemaconfiguracao (id_esquemaconfiguracao)
ALTER TABLE tb_holdingfiliada ADD CONSTRAINT FK_2DCC4AD185227166 FOREIGN KEY (id_holding) REFERENCES tb_holding (id_holding)
ALTER TABLE tb_livro ADD CONSTRAINT FK_265DF67743D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_livro ADD CONSTRAINT FK_265DF6772FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_livro ADD CONSTRAINT FK_265DF677C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_livro ADD CONSTRAINT FK_265DF6779B89BDFE FOREIGN KEY (id_tipolivro) REFERENCES tb_tipolivro (id_tipolivro)
ALTER TABLE tb_livro ADD CONSTRAINT FK_265DF6778AA0B36E FOREIGN KEY (id_livrocolecao) REFERENCES tb_livrocolecao (id_livrocolecao)
ALTER TABLE tb_holding ADD CONSTRAINT FK_246802B4C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_tag ADD CONSTRAINT FK_AD25BB3C2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0BC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0BA67A013E FOREIGN KEY (id_tipodematerial) REFERENCES tb_tipodematerial (id_tipodematerial)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0B2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0B318A53F2 FOREIGN KEY (id_upload) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0BF9386BC4 FOREIGN KEY (id_professor) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerial ADD CONSTRAINT FK_1EB0FB0BB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_aplicadorprova ADD CONSTRAINT FK_318693CD6898C988 FOREIGN KEY (id_entidadeaplicador) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_aplicadorprova ADD CONSTRAINT FK_318693CD99F138D FOREIGN KEY (id_usuarioaplicador) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_aplicadorprova ADD CONSTRAINT FK_318693CD43D3DAFE FOREIGN KEY (id_entidadecadastro) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_aplicadorprova ADD CONSTRAINT FK_318693CD2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_emailconfig ADD CONSTRAINT FK_AA40DC7212FF81C9 FOREIGN KEY (id_tipoconexaoemailsaida) REFERENCES tb_tipoconexaoemail (id_tipoconexaoemail)
ALTER TABLE tb_emailconfig ADD CONSTRAINT FK_AA40DC72E220608D FOREIGN KEY (id_tipoconexaoemailentrada) REFERENCES tb_tipoconexaoemail (id_tipoconexaoemail)
ALTER TABLE tb_emailconfig ADD CONSTRAINT FK_AA40DC722FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_emailconfig ADD CONSTRAINT FK_AA40DC72B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_emailconfig ADD CONSTRAINT FK_AA40DC7223D8EC4C FOREIGN KEY (id_servidoremailentrada) REFERENCES tb_servidoremail (id_servidoremail)
ALTER TABLE tb_arquivoretorno ADD CONSTRAINT FK_DB1C5C6D2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_arquivoretorno ADD CONSTRAINT FK_DB1C5C6DB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_smsentidademensagem ADD CONSTRAINT FK_D0550C68FB6270BA FOREIGN KEY (id_mensagempadrao) REFERENCES tb_mensagempadrao (id_mensagempadrao)
ALTER TABLE tb_smsentidademensagem ADD CONSTRAINT FK_D0550C68136FA251 FOREIGN KEY (id_textosistema) REFERENCES tb_textosistema (id_textosistema)
ALTER TABLE tb_smsentidademensagem ADD CONSTRAINT FK_D0550C682FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_smsentidademensagem ADD CONSTRAINT FK_D0550C684F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_smsentidademensagem ADD CONSTRAINT FK_D0550C68B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_endereco ADD CONSTRAINT FK_C047C310F57D32FD FOREIGN KEY (id_pais) REFERENCES tb_pais (id_pais)
ALTER TABLE tb_endereco ADD CONSTRAINT FK_C047C3107EAD49C7 FOREIGN KEY (id_municipio) REFERENCES tb_municipio (id_municipio)
ALTER TABLE tb_endereco ADD CONSTRAINT FK_C047C310BC9063A1 FOREIGN KEY (id_tipoendereco) REFERENCES tb_tipoendereco (id_tipoendereco)
ALTER TABLE tb_assuntoentidadeco ADD CONSTRAINT FK_530FFA2C9B9DCB8E FOREIGN KEY (id_assuntoco) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_assuntoentidadeco ADD CONSTRAINT FK_530FFA2CB3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_disciplina ADD CONSTRAINT FK_CB4FC9B5C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_disciplina ADD CONSTRAINT FK_CB4FC9B54FD882EB FOREIGN KEY (sg_uf) REFERENCES tb_uf (sg_uf)
ALTER TABLE tb_motivocancelamento ADD CONSTRAINT FK_ECB3FDAD92BA0BD2 FOREIGN KEY (id_cancelamento) REFERENCES tb_cancelamento (id_cancelamento)
ALTER TABLE tb_motivocancelamento ADD CONSTRAINT FK_ECB3FDAD3FCE8D8B FOREIGN KEY (id_motivo) REFERENCES tb_motivo (id_motivo)
ALTER TABLE tb_motivocancelamento ADD CONSTRAINT FK_ECB3FDAD2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_itemdematerialdisciplina ADD CONSTRAINT FK_7F87BDA2D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E16CAC67ADB FOREIGN KEY (id_modulo) REFERENCES tb_modulo (id_modulo)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E16D759AAF1 FOREIGN KEY (id_disciplina) REFERENCES tb_disciplina (id_disciplina)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E1644BFD204 FOREIGN KEY (id_serie) REFERENCES tb_serie (id_serie)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E166DA54E76 FOREIGN KEY (id_nivelensino) REFERENCES tb_nivelensino (id_nivelensino)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E16ED4F74A0 FOREIGN KEY (id_areaconhecimento) REFERENCES tb_areaconhecimento (id_areaconhecimento)
ALTER TABLE tb_modulodisciplina ADD CONSTRAINT FK_4E1E8E16AADAB48A FOREIGN KEY (id_usuarioponderacao) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_usuarioperfilentidade ADD CONSTRAINT FK_696D871FC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_esquemaconfiguracao ADD CONSTRAINT FK_D5C3CC4C2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_esquemaconfiguracao ADD CONSTRAINT FK_D5C3CC4C83CB061 FOREIGN KEY (id_usuarioatualizacao) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_95650969C0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_956509691426D29A FOREIGN KEY (id_trilha) REFERENCES tb_trilha (id_trilha)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_9565096967AF234A FOREIGN KEY (id_projetopedagogicoorigem) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_95650969616C5294 FOREIGN KEY (id_medidatempoconclusao) REFERENCES tb_medidatempoconclusao (id_medidatempoconclusao)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_9565096963FC6B92 FOREIGN KEY (id_contratoregra) REFERENCES tb_contratoregra (id_contratoregra)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_956509692CABD3E0 FOREIGN KEY (id_livroregistroentidade) REFERENCES tb_livroregistroentidade (id_livroregistroentidade)
ALTER TABLE tb_projetopedagogico ADD CONSTRAINT FK_956509693F9252D0 FOREIGN KEY (id_anexoementa) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_modelocarteirinha ADD CONSTRAINT FK_CC1DFB7BC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_produtointegracao ADD CONSTRAINT FK_B959304F8231E0A7 FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
ALTER TABLE tb_produtointegracao ADD CONSTRAINT FK_B959304F4F5F0211 FOREIGN KEY (id_sistema) REFERENCES tb_sistema (id_sistema)
ALTER TABLE tb_turmaprojeto ADD CONSTRAINT FK_CE6B804B2FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_turmaprojeto ADD CONSTRAINT FK_CE6B804BC5875896 FOREIGN KEY (id_turma) REFERENCES tb_turma (id_turma)
ALTER TABLE tb_turmaprojeto ADD CONSTRAINT FK_CE6B804B5E2EAFF2 FOREIGN KEY (id_projetopedagogico) REFERENCES tb_projetopedagogico (id_projetopedagogico)
ALTER TABLE tb_entidaderelacao ADD CONSTRAINT FK_EDB877CA3C1D0386 FOREIGN KEY (id_entidadeclasse) REFERENCES tb_entidadeclasse (id_entidadeclasse)
ALTER TABLE tb_livroentidade ADD CONSTRAINT FK_BC83B035A233E7B7 FOREIGN KEY (id_livro) REFERENCES tb_livro (id_livro)
ALTER TABLE tb_livroentidade ADD CONSTRAINT FK_BC83B035B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_configuracaoentidade ADD CONSTRAINT FK_A1ED20E755FD03C5 FOREIGN KEY (id_manualaluno) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_configuracaoentidade ADD CONSTRAINT FK_A1ED20E7834E0A09 FOREIGN KEY (id_tabelapreco) REFERENCES tb_upload (id_upload)
ALTER TABLE tb_configuracaoentidade ADD CONSTRAINT FK_A1ED20E73DC88C24 FOREIGN KEY (id_assuntoduplicidade) REFERENCES tb_assuntoco (id_assuntoco)
ALTER TABLE tb_formapagamento ADD CONSTRAINT FK_523DA81CC0694FE1 FOREIGN KEY (id_situacao) REFERENCES tb_situacao (id_situacao)
ALTER TABLE tb_formapagamento ADD CONSTRAINT FK_523DA81C2AA7EA FOREIGN KEY (id_tipoformapagamentoparcela) REFERENCES tb_tipoformapagamentoparcela (id_tipoformapagamentoparcela)
ALTER TABLE tb_formapagamento ADD CONSTRAINT FK_523DA81C311E715F FOREIGN KEY (id_tipocalculojuros) REFERENCES tb_tipocalculojuros (id_tipocalculojuros)
ALTER TABLE tb_vendaprodutonivelserie ADD CONSTRAINT FK_9ACCBC11A1C07F94 FOREIGN KEY (id_vendaproduto) REFERENCES tb_vendaproduto (id_vendaproduto)
ALTER TABLE tb_contratoregra ADD CONSTRAINT FK_AE6BC3A73A71990D FOREIGN KEY (id_projetocontratoduracaotipo) REFERENCES tb_projetocontratoduracaotipo (id_projetocontratoduracaotipo)
ALTER TABLE tb_contratoregra ADD CONSTRAINT FK_AE6BC3A7B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_contratoregra ADD CONSTRAINT FK_AE6BC3A72FF60C20 FOREIGN KEY (id_usuariocadastro) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_contratoregra ADD CONSTRAINT FK_AE6BC3A77565DE0 FOREIGN KEY (id_modelocarteirinha) REFERENCES tb_modelocarteirinha (id_modelocarteirinha)
ALTER TABLE tb_contratoregra ADD CONSTRAINT FK_AE6BC3A7BE123958 FOREIGN KEY (id_tiporegracontrato) REFERENCES tb_tiporegracontrato (id_tiporegracontrato)
ALTER TABLE tb_tipoendereco ADD CONSTRAINT FK_FC3EC5547FF0A0CA FOREIGN KEY (id_categoriaendereco) REFERENCES tb_categoriaendereco (id_categoriaendereco)
ALTER TABLE tb_notificacaoentidade ADD CONSTRAINT FK_9C62DEA242DF5E71 FOREIGN KEY (id_notificacao) REFERENCES tb_notificacao (id_notificacao)
ALTER TABLE tb_notificacaoentidade ADD CONSTRAINT FK_9C62DEA2B3F20A47 FOREIGN KEY (id_entidade) REFERENCES tb_entidade (id_entidade)
ALTER TABLE tb_usuario ADD CONSTRAINT FK_5DB26AFFDF4FBDC FOREIGN KEY (id_registropessoa) REFERENCES tb_registropessoa (id_registropessoa)
ALTER TABLE tb_usuario ADD CONSTRAINT FK_5DB26AFFFBCB3421 FOREIGN KEY (id_usuariopai) REFERENCES tb_usuario (id_usuario)
ALTER TABLE tb_usuario ADD CONSTRAINT FK_5DB26AFF733DB0F6 FOREIGN KEY (id_titulacao) REFERENCES tb_titulacao (id_titulacao)
ALTER TABLE tb_nucleotm ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_arquivoretornopacote ALTER COLUMN dt_postagem datetime NULL;
ALTER TABLE tb_arquivoretornopacote ALTER COLUMN dt_devolucao datetime NULL;
ALTER TABLE tb_formadisponibilizacao ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_disciplinaconteudo ALTER COLUMN st_descricaoconteudo VARCHAR(100) NULL;
ALTER TABLE tb_disciplinaconteudo ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_disciplinaconteudo ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_contratoresponsavel ALTER COLUMN id_usuario INT NULL;
ALTER TABLE tb_operacaolog ALTER COLUMN st_descricaoperacaolog VARCHAR(255) NULL;
ALTER TABLE tb_alocacao ALTER COLUMN dt_inicio date NULL;
ALTER TABLE tb_alocacao ALTER COLUMN id_categoriasala INT NULL;
ALTER TABLE tb_alocacao ALTER COLUMN nu_diasextensao INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN nu_vencimento decimal NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_usuariolancamento INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_vencimento date NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_quitado date NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_emissao datetime2 NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_prevquitado datetime2 NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_emissor VARCHAR(255) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_coddocumento VARCHAR(255) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN nu_juros decimal NULL;
ALTER TABLE tb_lancamento ALTER COLUMN nu_desconto decimal NULL;
ALTER TABLE tb_lancamento ALTER COLUMN nu_quitado decimal NULL;
ALTER TABLE tb_lancamento ALTER COLUMN nu_multa decimal NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_atualizado datetime2 NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_renegociacao VARCHAR(10) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_lancamentooriginal INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_acordo INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_agencia VARCHAR(10) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_numcheque VARCHAR(10) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_nossonumero VARCHAR(20) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_codconta VARCHAR(10) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN id_codcoligada INT NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_statuslan VARCHAR(1) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN dt_vencimentocheque date NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_retornoverificacao VARCHAR(255) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_autorizacao VARCHAR(255) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN st_ultimosdigitos VARCHAR(4) NULL;
ALTER TABLE tb_lancamento ALTER COLUMN bl_chequedevolvido BIT NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN dt_enviocertificado datetime NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN dt_retornocertificadora datetime NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN dt_envioaluno datetime NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN st_siglaentidade VARCHAR(200) NULL;
ALTER TABLE tb_matriculacertificacao ALTER COLUMN st_codigoacompanhamento VARCHAR(13) NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN st_observacao VARCHAR(255) NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN st_observacaocalculo VARCHAR(255) NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_cargahoraria INT NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_cargahorariacursada INT NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valorbruto decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valornegociado decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valordiferenca decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valorhoraaula decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valormaterial decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valorutilizadomaterial decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valortotal decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_totalutilizado decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valordevolucao decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN bl_cartagerada BIT NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN bl_cancelamentofinalizado BIT NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valorcarta decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valormultadevolucao decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_valormultacarta decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_multaporcentagemcarta decimal NULL;
ALTER TABLE tb_cancelamento ALTER COLUMN nu_multaporcentagemdevolucao decimal NULL;
ALTER TABLE tb_avalagendamentoref ALTER COLUMN id_avaliacaoagendamento INT NULL;
ALTER TABLE tb_avalagendamentoref ALTER COLUMN id_avaliacaoconjuntoreferencia INT NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN st_codarea VARCHAR(100) NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN st_caminho VARCHAR(255) NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfilsuporte decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfilprojeto decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfildisciplina decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfilobservador decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfilalunoobs decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_perfilalunoencerrado decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN nu_contexto decimal NULL;
ALTER TABLE tb_entidadeintegracao ALTER COLUMN st_linkedserver VARCHAR(20) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_produto VARCHAR(250) NULL;
ALTER TABLE tb_produto ALTER COLUMN id_produtoimagempadrao INT NULL;
ALTER TABLE tb_produto ALTER COLUMN st_descricao VARCHAR(255) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_observacoes VARCHAR(255) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_informacoesadicionais VARCHAR(255) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_estruturacurricular VARCHAR(255) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_subtitulo VARCHAR(300) NULL;
ALTER TABLE tb_produto ALTER COLUMN st_slug VARCHAR(200) NULL;
ALTER TABLE tb_produto ALTER COLUMN dt_iniciopontosprom datetime2 NULL;
ALTER TABLE tb_produto ALTER COLUMN dt_fimpontosprom datetime2 NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN id_areaconhecimentopai INT NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN st_tituloexibicao text NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN bl_extras BIT NULL;
ALTER TABLE tb_areaconhecimento ALTER COLUMN st_imagemarea VARCHAR(255) NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN dt_atendimento date NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN st_titulo VARCHAR(255) NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN st_codigorastreio VARCHAR(255) NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN st_ocorrencia VARCHAR(255) NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN id_venda INT NULL;
ALTER TABLE tb_ocorrencia ALTER COLUMN dt_cadastroocorrencia datetime2 NULL;
ALTER TABLE tb_mensagempadrao ALTER COLUMN st_default text NULL;
ALTER TABLE tb_funcao ALTER COLUMN st_funcao VARCHAR(100) NULL;
ALTER TABLE tb_documentoidentidade ALTER COLUMN st_orgaoexpeditor VARCHAR(80) NULL;
ALTER TABLE tb_documentoidentidade ALTER COLUMN dt_dataexpedicao date NULL;
ALTER TABLE tb_entregamaterial ALTER COLUMN dt_entrega datetime2 NULL;
ALTER TABLE tb_entregamaterial ALTER COLUMN dt_devolucao datetime NULL;
ALTER TABLE tb_entregamaterial ALTER COLUMN id_pacote INT NULL;
ALTER TABLE tb_avaliacaoagendamento ALTER COLUMN dt_agendamento date NULL;
ALTER TABLE tb_avaliacaoagendamento ALTER COLUMN nu_presenca INT NULL;
ALTER TABLE tb_avaliacaoagendamento ALTER COLUMN id_tipodeavaliacao INT NULL;
ALTER TABLE tb_avaliacaoagendamento ALTER COLUMN bl_provaglobal INT NULL;
ALTER TABLE tb_avaliacaoagendamento ALTER COLUMN id_usuariolancamento INT NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN dt_fiminscricao date NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN dt_abertura date NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_localizacao VARCHAR(255) NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN dt_encerramento date NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN nu_maxalunos INT NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN nu_diasencerramento decimal NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN bl_todasentidades BIT NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN nu_diasaluno decimal NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_pontosnegativos text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_pontospositivos text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_pontosmelhorar text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_pontosresgate text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_conclusoesfinais text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_justificativaacima text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN st_justificativaabaixo text NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN dt_atualiza datetime2 NULL;
ALTER TABLE tb_saladeaula ALTER COLUMN dt_cancelamento datetime NULL;
ALTER TABLE tb_erro ALTER COLUMN st_codigo VARCHAR(255) NULL;
ALTER TABLE tb_venda ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_venda ALTER COLUMN nu_diamensalidade INT NULL;
ALTER TABLE tb_venda ALTER COLUMN dt_agendamento date NULL;
ALTER TABLE tb_venda ALTER COLUMN dt_confirmacao datetime2 NULL;
ALTER TABLE tb_venda ALTER COLUMN id_contratoafiliado INT NULL;
ALTER TABLE tb_venda ALTER COLUMN st_observacao VARCHAR(255) NULL;
ALTER TABLE tb_venda ALTER COLUMN nu_valorcartacredito decimal NULL;
ALTER TABLE tb_avaliacaoconjuntoreferencia ALTER COLUMN dt_fim date NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN id_projetopedagogico INT NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN id_nivelensino INT NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN id_saladeaula INT NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN id_areaconhecimento INT NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN nu_diasacesso INT NULL;
ALTER TABLE tb_areaprojetosala ALTER COLUMN st_referencia VARCHAR(200) NULL;
ALTER TABLE tb_lancamentovenda ALTER COLUMN nu_ordem INT NULL;
ALTER TABLE tb_log ALTER COLUMN st_coluna VARCHAR(255) NULL;
ALTER TABLE tb_fundamentolegal ALTER COLUMN dt_vigencia date NULL;
ALTER TABLE tb_fundamentolegal ALTER COLUMN id_nivelensino INT NULL;
ALTER TABLE tb_fundamentolegal ALTER COLUMN st_orgaoexped VARCHAR(255) NULL;
ALTER TABLE tb_catraca ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_catraca ALTER COLUMN st_codigocatraca VARCHAR(255) NULL;
ALTER TABLE tb_enviomensagem ALTER COLUMN dt_envio datetime2 NULL;
ALTER TABLE tb_enviomensagem ALTER COLUMN dt_tentativa datetime2 NULL;
ALTER TABLE tb_pessoaendereco ALTER COLUMN bl_padrao BIT NULL;
ALTER TABLE tb_premio ALTER COLUMN nu_comprasacima decimal NULL;
ALTER TABLE tb_premio ALTER COLUMN nu_compraunidade INT NULL;
ALTER TABLE tb_premio ALTER COLUMN nu_descontopremio decimal NULL;
ALTER TABLE tb_premio ALTER COLUMN id_tipodescontopremio INT NULL;
ALTER TABLE tb_premio ALTER COLUMN id_tipopremio INT NULL;
ALTER TABLE tb_premio ALTER COLUMN bl_compraacima BIT NULL;
ALTER TABLE tb_premio ALTER COLUMN bl_promocaocumulativa BIT NULL;
ALTER TABLE tb_premio ALTER COLUMN bl_compraunidade BIT NULL;
ALTER TABLE tb_premio ALTER COLUMN st_prefixocupompremio VARCHAR(30) NULL;
ALTER TABLE tb_situacaointegracao ALTER COLUMN nu_status INT NULL;
ALTER TABLE tb_turmaentidade ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_turmaentidade ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_turmaintegracao ALTER COLUMN st_turmasistema VARCHAR(255) NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN dt_entrega datetime2 NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN nu_desconto float NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN nu_valorliquido float NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN nu_valorbruto float NULL;
ALTER TABLE tb_vendaproduto ALTER COLUMN id_avaliacaoaplicacao INT NULL;
ALTER TABLE tb_motivo ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_motivo ALTER COLUMN st_motivo VARCHAR(255) NULL;
ALTER TABLE tb_tipolivro ALTER COLUMN st_tipolivro VARCHAR(20) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN dt_nascimento date NULL;
ALTER TABLE tb_prevenda ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_prevenda ALTER COLUMN id_usuario INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_dddcelular INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN id_paisnascimento INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_lancamentos INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_ddicelular INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_dddresidencial INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_ddiresidencial INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_dddcomercial INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_ddicomercial INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN id_pais INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN id_municipio INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_juros INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_desconto INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_valorliquido INT NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_nomecompleto text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_email VARCHAR(255) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_telefonecelular VARCHAR(8) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_telefoneresidencial VARCHAR(8) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_telefonecomercial VARCHAR(8) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_orgaoexpeditor VARCHAR(80) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_instituicao text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_graduacao text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_localtrabalho text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_recomendacao text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_endereco text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_bairro VARCHAR(255) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_complemento text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN nu_numero VARCHAR(10) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_rg VARCHAR(20) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_contato text NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_cpf VARCHAR(11) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN sg_uf VARCHAR(2) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_cep VARCHAR(8) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN st_sexo VARCHAR(1) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN sg_ufnascimento VARCHAR(2) NULL;
ALTER TABLE tb_prevenda ALTER COLUMN dt_atendimento datetime2 NULL;
ALTER TABLE tb_prevenda ALTER COLUMN dt_descarte datetime2 NULL;
ALTER TABLE tb_logacesso ALTER COLUMN id_perfil INT NULL;
ALTER TABLE tb_logacesso ALTER COLUMN id_saladeaula INT NULL;
ALTER TABLE tb_categoria ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_contatostelefonepessoa ALTER COLUMN bl_padrao BIT NULL;
ALTER TABLE tb_formadiavencimento ALTER COLUMN nu_diavencimento INT NULL;
ALTER TABLE tb_tipodestinatario ALTER COLUMN st_tipodestinatario VARCHAR(20) NULL;
ALTER TABLE tb_assuntoco ALTER COLUMN bl_cancelamento BIT NULL;
ALTER TABLE tb_assuntoco ALTER COLUMN bl_trancamento BIT NULL;
ALTER TABLE tb_boletoconfig ALTER COLUMN st_contacedente VARCHAR(15) NULL;
ALTER TABLE tb_boletoconfig ALTER COLUMN st_digitocedente VARCHAR(2) NULL;
ALTER TABLE tb_formapagamentoparcela ALTER COLUMN nu_juros decimal NULL;
ALTER TABLE tb_formapagamentoparcela ALTER COLUMN nu_valormin decimal NULL;
ALTER TABLE tb_produtocombo ALTER COLUMN nu_descontoporcentagem decimal NULL;
ALTER TABLE tb_trilha ALTER COLUMN nu_disciplinassimultaneasmai decimal NULL;
ALTER TABLE tb_trilha ALTER COLUMN nu_disciplinassimultaneaspendencia decimal NULL;
ALTER TABLE tb_trilha ALTER COLUMN nu_disciplinassimultaneasmen decimal NULL;
ALTER TABLE tb_trilha ALTER COLUMN nu_disciplinaspendencia decimal NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN nu_paginas INT NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN nu_encontro INT NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN nu_qtdestoque INT NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN id_upload INT NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_itemdematerialpresencial ALTER COLUMN bl_portal BIT NULL;
ALTER TABLE tb_enviodestinatario ALTER COLUMN nu_telefone INT NULL;
ALTER TABLE tb_enviodestinatario ALTER COLUMN st_endereco VARCHAR(100) NULL;
ALTER TABLE tb_enviodestinatario ALTER COLUMN st_nome VARCHAR(100) NULL;
ALTER TABLE tb_enviodestinatario ALTER COLUMN id_sistema INT NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN nu_valor INT NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN st_descricao VARCHAR(255) NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN nu_quantquestoes INT NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN st_linkreferencia VARCHAR(500) NULL;
ALTER TABLE tb_avaliacao ALTER COLUMN bl_recuperacao BIT NULL;
ALTER TABLE tb_notificacao ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_notificacao ALTER COLUMN st_notificacao VARCHAR(50) NULL;
ALTER TABLE tb_pacote ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_pacote ALTER COLUMN id_matricula INT NULL;
ALTER TABLE tb_pacote ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_pacote ALTER COLUMN id_lotematerial INT NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN id_tipoocorrencia INT NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN id_textonotificacao INT NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN nu_horasmeta INT NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN st_corinteracao VARCHAR(10) NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN st_cordevolvida VARCHAR(10) NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN st_coratrasada VARCHAR(10) NULL;
ALTER TABLE tb_nucleoco ALTER COLUMN st_cordefault VARCHAR(10) NULL;
ALTER TABLE tb_produtoprojetopedagogico ALTER COLUMN nu_tempoacesso INT NULL;
ALTER TABLE tb_produtoprojetopedagogico ALTER COLUMN nu_cargahoraria INT NULL;
ALTER TABLE tb_produtoprojetopedagogico ALTER COLUMN st_disciplina VARCHAR(255) NULL;
ALTER TABLE tb_horarioaula ALTER COLUMN id_codhorarioacesso INT NULL;
ALTER TABLE tb_horarioaula ALTER COLUMN st_codhorarioacesso VARCHAR(100) NULL;
ALTER TABLE tb_tramiteocorrencia ALTER COLUMN bl_interessado BIT NULL;
ALTER TABLE tb_modulo ALTER COLUMN st_descricao VARCHAR(500) NULL;
ALTER TABLE tb_modulo ALTER COLUMN id_moduloanterior INT NULL;
ALTER TABLE tb_medidatempoconclusao ALTER COLUMN st_medidatempoconclusao VARCHAR(50) NULL;
ALTER TABLE tb_livrocolecao ALTER COLUMN id_entidadecadastro INT NULL;
ALTER TABLE tb_livrocolecao ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_materialprojeto ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_materialprojeto ALTER COLUMN id_itemdematerial INT NULL;
ALTER TABLE tb_textovariaveis ALTER COLUMN st_camposubstituir VARCHAR(100) NULL;
ALTER TABLE tb_textovariaveis ALTER COLUMN st_mascara VARCHAR(100) NULL;
ALTER TABLE tb_entregadeclaracao ALTER COLUMN dt_solicitacao datetime2 NULL;
ALTER TABLE tb_entregadeclaracao ALTER COLUMN dt_envio datetime2 NULL;
ALTER TABLE tb_entregadeclaracao ALTER COLUMN dt_entrega datetime2 NULL;
ALTER TABLE tb_entregadeclaracao ALTER COLUMN id_venda INT NULL;
ALTER TABLE tb_cartaoconfig ALTER COLUMN st_gateway VARCHAR(50) NULL;
ALTER TABLE tb_cartaoconfig ALTER COLUMN st_contratooperadora VARCHAR(50) NULL;
ALTER TABLE tb_tipocampanha ALTER COLUMN st_tipocampanha VARCHAR(250) NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN nu_status INT NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN id_cartaobandeira INT NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN id_cartaooperadora INT NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN nu_verificacao INT NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN id_situacaointegracao INT NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN st_titularcartao VARCHAR(255) NULL;
ALTER TABLE tb_transacaofinanceira ALTER COLUMN st_ultimosdigitos VARCHAR(4) NULL;
ALTER TABLE tb_usuarioperfilentidadereferencia ALTER COLUMN bl_autor BIT NULL;
ALTER TABLE tb_usuarioperfilentidadereferencia ALTER COLUMN nu_porcentagem decimal NULL;
ALTER TABLE tb_usuarioperfilentidadereferencia ALTER COLUMN bl_desativarmoodle BIT NULL;
ALTER TABLE tb_contrato ALTER COLUMN dt_ativacao date NULL;
ALTER TABLE tb_contrato ALTER COLUMN dt_termino date NULL;
ALTER TABLE tb_contrato ALTER COLUMN id_textosistema INT NULL;
ALTER TABLE tb_contrato ALTER COLUMN nu_codintegracao INT NULL;
ALTER TABLE tb_contrato ALTER COLUMN nu_bolsa decimal NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_sexo VARCHAR(1) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_nomeexibicao VARCHAR(255) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN dt_nascimento date NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_nomepai VARCHAR(255) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_nomemae VARCHAR(100) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN sg_uf VARCHAR(2) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_passaporte VARCHAR(255) NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_urlavatar text NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_cidade text NULL;
ALTER TABLE tb_pessoa ALTER COLUMN st_identificacao VARCHAR(255) NULL;
ALTER TABLE tb_usuariodevice ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_usuariosessao ALTER COLUMN dt_sessao datetime NULL;
ALTER TABLE tb_usuariosessao ALTER COLUMN st_hash VARCHAR(155) NULL;
ALTER TABLE tb_concurso ALTER COLUMN st_slug VARCHAR(200) NULL;
ALTER TABLE tb_concurso ALTER COLUMN st_imagem VARCHAR(255) NULL;
ALTER TABLE tb_concurso ALTER COLUMN st_orgao VARCHAR(200) NULL;
ALTER TABLE tb_concurso ALTER COLUMN dt_prova date NULL;
ALTER TABLE tb_concurso ALTER COLUMN dt_inicioinscricao date NULL;
ALTER TABLE tb_concurso ALTER COLUMN dt_fiminscricao date NULL;
ALTER TABLE tb_concurso ALTER COLUMN nu_vagas INT NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN st_linkloja VARCHAR(255) NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN nu_avisoatraso INT NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN nu_diaspagamentoentrada INT NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN nu_multacomcarta decimal NULL;
ALTER TABLE tb_entidadefinanceiro ALTER COLUMN nu_multasemcarta decimal NULL;
ALTER TABLE tb_turma ALTER COLUMN dt_inicioinscricao date NULL;
ALTER TABLE tb_turma ALTER COLUMN dt_fiminscricao date NULL;
ALTER TABLE tb_turma ALTER COLUMN dt_inicio date NULL;
ALTER TABLE tb_turma ALTER COLUMN dt_fim date NULL;
ALTER TABLE tb_turma ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_turma ALTER COLUMN nu_maxalunos INT NULL;
ALTER TABLE tb_turma ALTER COLUMN nu_maxfreepass INT NULL;
ALTER TABLE tb_turma ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_turma ALTER COLUMN st_turma VARCHAR(150) NULL;
ALTER TABLE tb_turma ALTER COLUMN st_codigo VARCHAR(20) NULL;
ALTER TABLE tb_turma ALTER COLUMN st_tituloexibicao VARCHAR(200) NULL;
ALTER TABLE tb_itemgradehoraria ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_itemgradehoraria ALTER COLUMN st_observacao VARCHAR(255) NULL;
ALTER TABLE tb_itemgradehoraria ALTER COLUMN nu_encontro INT NULL;
ALTER TABLE tb_comissaolancamento ALTER COLUMN bl_adiantamento BIT NULL;
ALTER TABLE tb_comissaolancamento ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_comissaolancamento ALTER COLUMN id_perfilpedagogico INT NULL;
ALTER TABLE tb_comissaolancamento ALTER COLUMN bl_autorizado BIT NULL;
ALTER TABLE tb_contatostelefone ALTER COLUMN nu_ddi INT NULL;
ALTER TABLE tb_contatostelefone ALTER COLUMN nu_telefone VARCHAR(15) NULL;
ALTER TABLE tb_contatostelefone ALTER COLUMN st_descricao text NULL;
ALTER TABLE tb_contatostelefone ALTER COLUMN nu_ddd VARCHAR(3) NULL;
ALTER TABLE tb_contaentidade ALTER COLUMN st_digitoagencia VARCHAR(2) NULL;
ALTER TABLE tb_contaentidade ALTER COLUMN st_agencia VARCHAR(20) NULL;
ALTER TABLE tb_contaentidade ALTER COLUMN st_conta VARCHAR(10) NULL;
ALTER TABLE tb_contaentidade ALTER COLUMN st_digitoconta VARCHAR(2) NULL;
ALTER TABLE tb_camporelatorio ALTER COLUMN id_relatorio INT NULL;
ALTER TABLE tb_camporelatorio ALTER COLUMN nu_ordem INT NULL;
ALTER TABLE tb_matriculadisciplina ALTER COLUMN nu_aprovafinal INT NULL;
ALTER TABLE tb_matriculadisciplina ALTER COLUMN dt_conclusao date NULL;
ALTER TABLE tb_matriculadisciplina ALTER COLUMN id_pacote INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN id_situacaointegracao INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_intervalo INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_diavencimento INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_tentativas INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_tentativasfeitas INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_valor INT NULL;
ALTER TABLE tb_pedidointegracao ALTER COLUMN nu_bandeiracartao INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN dt_concluinte date NULL;
ALTER TABLE tb_matricula ALTER COLUMN dt_termino date NULL;
ALTER TABLE tb_matricula ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_periodoletivo INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_vendaproduto INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_entidadematriz INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_entidadematricula INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_mensagemrecuperacao INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN id_evolucaoagendamento INT NULL;
ALTER TABLE tb_matricula ALTER COLUMN st_codcertificacao VARCHAR(200) NULL;
ALTER TABLE tb_matricula ALTER COLUMN dt_ultimoacesso datetime2 NULL;
ALTER TABLE tb_gradehoraria ALTER COLUMN dt_fimgradehoraria datetime NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_classeflex VARCHAR(300) NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_urlicone VARCHAR(1000) NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN nu_ordem INT NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_classeflexbotao INT NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_ajuda text NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_target VARCHAR(100) NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_urlcaminho VARCHAR(50) NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN bl_relatorio BIT NULL;
ALTER TABLE tb_funcionalidade ALTER COLUMN st_urlcaminhoedicao VARCHAR(50) NULL;
ALTER TABLE tb_relatorio ALTER COLUMN st_origem text NULL;
ALTER TABLE tb_tramite ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_tramite ALTER COLUMN bl_visivel BIT NULL;
ALTER TABLE tb_tramite ALTER COLUMN st_tramite VARCHAR(255) NULL;
ALTER TABLE tb_textocategoria ALTER COLUMN st_descricao VARCHAR(200) NULL;
ALTER TABLE tb_textocategoria ALTER COLUMN st_metodo VARCHAR(100) NULL;
ALTER TABLE tb_ocorrenciaresponsavel ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_ocorrenciaresponsavel ALTER COLUMN id_ocorrencia INT NULL;
ALTER TABLE tb_ocorrenciaresponsavel ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN id_avaliacaoconjuntoreferencia INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN st_nota INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN id_sistemaimportacao INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN dt_avaliacao date NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN id_usuariorevisor INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN id_avaliacaoalunoorigem INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN id_upload INT NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN st_tituloavaliacao VARCHAR(500) NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN dt_defesa date NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN st_justificativa VARCHAR(2000) NULL;
ALTER TABLE tb_avaliacaoaluno ALTER COLUMN dt_alteracao datetime NULL;
ALTER TABLE tb_cargoconcurso ALTER COLUMN nu_salario decimal NULL;
ALTER TABLE tb_textosistema ALTER COLUMN bl_cabecalho BIT NULL;
ALTER TABLE tb_meiopagamentointegracao ALTER COLUMN id_cartaobandeira INT NULL;
ALTER TABLE tb_meiopagamentointegracao ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_meiopagamentointegracao ALTER COLUMN st_codcontacaixa VARCHAR(255) NULL;
ALTER TABLE tb_contatosemail ALTER COLUMN st_email text NULL;
ALTER TABLE tb_municipio ALTER COLUMN st_nomemunicipio VARCHAR(255) NULL;
ALTER TABLE tb_municipio ALTER COLUMN nu_codigoibge decimal NULL;
ALTER TABLE tb_serie ALTER COLUMN id_serieanterior INT NULL;
ALTER TABLE tb_entidadefuncionalidade ALTER COLUMN st_label VARCHAR(255) NULL;
ALTER TABLE tb_entidadefuncionalidade ALTER COLUMN bl_visivel BIT NULL;
ALTER TABLE tb_entidadefuncionalidade ALTER COLUMN nu_ordem INT NULL;
ALTER TABLE tb_avaliacaoconjunto ALTER COLUMN id_tipoprova INT NULL;
ALTER TABLE tb_avaliacaoconjunto ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_avaliacaoconjunto ALTER COLUMN id_tipocalculoavaliacao INT NULL;
ALTER TABLE tb_avaliacaoconjunto ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_avaliacaoconjunto ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_itemativacaoentidade ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_itemativacaoentidade ALTER COLUMN id_usuarioatualizacao INT NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN bl_aplicardesconto BIT NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN dt_fim datetime2 NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN nu_disponibilidade INT NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN st_descricao VARCHAR(2500) NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN bl_todosprodutos BIT NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN bl_todasformas BIT NULL;
ALTER TABLE tb_campanhacomercial ALTER COLUMN nu_diasprazo INT NULL;
ALTER TABLE tb_usuariointegracao ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_usuariointegracao ALTER COLUMN st_senhaintegrada VARCHAR(255) NULL;
ALTER TABLE tb_usuariointegracao ALTER COLUMN st_loginintegrado VARCHAR(255) NULL;
ALTER TABLE tb_entidadeemail ALTER COLUMN st_email VARCHAR(200) NULL;
ALTER TABLE tb_modelogradenotas ALTER COLUMN st_modelogradenotas VARCHAR(100) NULL;
ALTER TABLE tb_entidadeclasse ALTER COLUMN st_entidadeclasse VARCHAR(255) NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN dt_encerramentoprofessor datetime2 NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN dt_encerramentocoordenador datetime2 NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN dt_encerramentopedagogico datetime2 NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN dt_encerramentofinanceiro datetime2 NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN dt_recusa datetime2 NULL;
ALTER TABLE tb_encerramentosala ALTER COLUMN st_motivorecusa VARCHAR(8000) NULL;
ALTER TABLE tb_tipoprodutovalor ALTER COLUMN st_descricaovalor VARCHAR(250) NULL;
ALTER TABLE tb_tipoprodutovalor ALTER COLUMN st_descricao VARCHAR(250) NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN nu_valorproporcionalcursado decimal NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN nu_valordevolucao decimal NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN nu_percentualmulta decimal NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN nu_cargahorariautilizada INT NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN nu_valormaterialutilizado decimal NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN st_motivo VARCHAR(1000) NULL;
ALTER TABLE tb_logcancelamento ALTER COLUMN bl_cartacredito BIT NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_descricao VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_origem VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_xtipo VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_xtitulo VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_ytipo VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_ytitulo VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_campolabel VARCHAR(255) NULL;
ALTER TABLE tb_dashboard ALTER COLUMN st_tipochart VARCHAR(255) NULL;
ALTER TABLE tb_turmaturno ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_turmaturno ALTER COLUMN id_turma INT NULL;
ALTER TABLE tb_turmaturno ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_turmaturno ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN id_aplicadorprova INT NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN id_endereco INT NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN nu_maxaplicacao INT NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN id_horarioaula INT NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN dt_aplicacao date NULL;
ALTER TABLE tb_avaliacaoaplicacao ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_declaracaoaluno ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_dadosacesso ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_dadosacesso ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_dadosacesso ALTER COLUMN st_login VARCHAR(50) NULL;
ALTER TABLE tb_dadosacesso ALTER COLUMN st_senha VARCHAR(50) NULL;
ALTER TABLE tb_avaliacaoconjuntorelacao ALTER COLUMN id_avaliacaorecupera INT NULL;
ALTER TABLE tb_parametrotcc ALTER COLUMN nu_peso INT NULL;
ALTER TABLE tb_mensagem ALTER COLUMN id_funcionalidade INT NULL;
ALTER TABLE tb_mensagem ALTER COLUMN st_caminhoanexo VARCHAR(8000) NULL;
ALTER TABLE tb_executaclasses ALTER COLUMN bl_executa INT NULL;
ALTER TABLE tb_entidade ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_siglaentidade VARCHAR(20) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_nomeentidade VARCHAR(2500) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_urlimglogo VARCHAR(1000) NULL;
ALTER TABLE tb_entidade ALTER COLUMN nu_cnpj VARCHAR(15) NULL;
ALTER TABLE tb_entidade ALTER COLUMN nu_inscricaoestadual VARCHAR(2500) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_urlsite VARCHAR(2500) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_cnpj VARCHAR(15) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_wschave VARCHAR(40) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_conversao VARCHAR(255) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_urlportal VARCHAR(2500) NULL;
ALTER TABLE tb_entidade ALTER COLUMN str_urlimglogoentidade VARCHAR(1000) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_apelido VARCHAR(15) NULL;
ALTER TABLE tb_entidade ALTER COLUMN id_uploadloja INT NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_urllogoutportal VARCHAR(7) NULL;
ALTER TABLE tb_entidade ALTER COLUMN st_urlimgapp VARCHAR(255) NULL;
ALTER TABLE tb_holdingfiliada ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_tipoproduto ALTER COLUMN st_tabelarelacao VARCHAR(255) NULL;
ALTER TABLE tb_tipoproduto ALTER COLUMN st_tipoproduto VARCHAR(255) NULL;
ALTER TABLE tb_livro ALTER COLUMN st_isbn VARCHAR(50) NULL;
ALTER TABLE tb_livro ALTER COLUMN st_codigocontrole VARCHAR(50) NULL;
ALTER TABLE tb_livro ALTER COLUMN st_edicao VARCHAR(50) NULL;
ALTER TABLE tb_livro ALTER COLUMN nu_anolancamento INT NULL;
ALTER TABLE tb_livro ALTER COLUMN nu_pagina INT NULL;
ALTER TABLE tb_livro ALTER COLUMN nu_peso INT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN nu_qtdepaginas INT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN nu_peso INT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN nu_valor decimal NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN bl_portal BIT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN nu_encontro INT NULL;
ALTER TABLE tb_itemdematerial ALTER COLUMN nu_qtdestoque INT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_smtp VARCHAR(100) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_pop VARCHAR(100) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_imap VARCHAR(100) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN bl_autenticacaosegura BIT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN bt_autenticacaosegura BIT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_conta VARCHAR(100) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN nu_portaentrada INT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN nu_portasaida INT NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_usuario VARCHAR(50) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_senha VARCHAR(20) NULL;
ALTER TABLE tb_emailconfig ALTER COLUMN st_titulo VARCHAR(200) NULL;
ALTER TABLE tb_arquivoretorno ALTER COLUMN st_arquivoretorno VARCHAR(250) NULL;
ALTER TABLE tb_arquivoretorno ALTER COLUMN st_banco VARCHAR(3) NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN id_saladeaula INT NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN bl_conteudo BIT NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN st_codsistemasala VARCHAR(30) NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN st_codsistemareferencia VARCHAR(30) NULL;
ALTER TABLE tb_saladeaulaintegracao ALTER COLUMN st_retornows VARCHAR(255) NULL;
ALTER TABLE tb_modelovenda ALTER COLUMN st_modelovenda VARCHAR(255) NULL;
ALTER TABLE tb_endereco ALTER COLUMN sg_uf VARCHAR(2) NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_cep VARCHAR(12) NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_endereco text NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_bairro VARCHAR(255) NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_complemento VARCHAR(500) NULL;
ALTER TABLE tb_endereco ALTER COLUMN nu_numero VARCHAR(30) NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_estadoprovincia VARCHAR(255) NULL;
ALTER TABLE tb_endereco ALTER COLUMN st_cidade VARCHAR(255) NULL;
ALTER TABLE tb_pais ALTER COLUMN st_nacionalidade VARCHAR(150) NULL;
ALTER TABLE tb_protocolo ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_protocolo ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_disciplina ALTER COLUMN st_descricao text NULL;
ALTER TABLE tb_disciplina ALTER COLUMN nu_identificador VARCHAR(255) NULL;
ALTER TABLE tb_disciplina ALTER COLUMN nu_creditos decimal NULL;
ALTER TABLE tb_disciplina ALTER COLUMN nu_codigoparceiro VARCHAR(255) NULL;
ALTER TABLE tb_disciplina ALTER COLUMN st_tituloexibicao VARCHAR(255) NULL;
ALTER TABLE tb_disciplina ALTER COLUMN st_apelido VARCHAR(15) NULL;
ALTER TABLE tb_disciplina ALTER COLUMN st_identificador VARCHAR(30) NULL;
ALTER TABLE tb_disciplina ALTER COLUMN st_ementacertificado text NULL;
ALTER TABLE tb_disciplina ALTER COLUMN bl_provamontada BIT NULL;
ALTER TABLE tb_disciplina ALTER COLUMN nu_repeticao INT NULL;
ALTER TABLE tb_motivocancelamento ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_itemdematerialdisciplina ALTER COLUMN id_itemdematerial INT NULL;
ALTER TABLE tb_nucleofuncionariotm ALTER COLUMN id_funcao INT NULL;
ALTER TABLE tb_nucleofuncionariotm ALTER COLUMN id_usuario INT NULL;
ALTER TABLE tb_nucleofuncionariotm ALTER COLUMN id_nucleotm INT NULL;
ALTER TABLE tb_nucleofuncionariotm ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_nucleofuncionariotm ALTER COLUMN id_situacao INT NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN nu_ordem INT NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN nu_importancia INT NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN nu_ponderacaocalculada decimal NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN nu_ponderacaoaplicada decimal NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN dt_cadastroponderacao datetime2 NULL;
ALTER TABLE tb_modulodisciplina ALTER COLUMN nu_cargahoraria INT NULL;
ALTER TABLE tb_usuarioperfilentidade ALTER COLUMN dt_termino date NULL;
ALTER TABLE tb_usuarioperfilentidade ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_usuarioperfilentidade ALTER COLUMN id_entidadeidentificacao INT NULL;
ALTER TABLE tb_usuarioperfilentidade ALTER COLUMN bl_tutorial BIT NULL;
ALTER TABLE tb_usuarioperfilentidade ALTER COLUMN st_token VARCHAR(32) NULL;
ALTER TABLE tb_esquemaconfiguracao ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_esquemaconfiguracao ALTER COLUMN dt_atualizacao datetime NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_descricao text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_idademinimainiciar INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_tempominimoconcluir INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_idademinimaconcluir INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_tempomaximoconcluir INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_tempopreviconcluir INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_tituloexibicao VARCHAR(255) NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_nomeversao VARCHAR(255) NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN id_tipoextracurricular INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_notamaxima INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_percentualaprovacao INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_valorprovafinal INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_objetivo text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_estruturacurricular text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_metodologiaavaliacao text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_certificacao text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_apelido text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_valorapresentacao text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN nu_prazoagendamento INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_perfilprojeto text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_coordenacao text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_horario text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_publicoalvo text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN st_mercadotrabalho text NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN bl_gradepronta BIT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN dt_criagrade datetime2 NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN id_usuariograde INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN id_horarioaula INT NULL;
ALTER TABLE tb_projetopedagogico ALTER COLUMN bl_disciplinacomplementar BIT NULL;
ALTER TABLE tb_turmaprojeto ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_turmaprojeto ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_textosistemafichac INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_categoriaocorrencia INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_textosemsala INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN nu_horasencontro INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_assuntoresgate INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_textoresgate INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_assuntorecuperacao INT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN bl_emailunico BIT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN bl_tutorial BIT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_informacoestecnicas VARCHAR(8000) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN bl_primeiroacesso BIT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN bl_professordisciplina BIT NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_campotextogooglecredito VARCHAR(8000) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_corprimarialoja VARCHAR(7) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_corsecundarialoja VARCHAR(7) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_corterciarialoja VARCHAR(7) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_campotextogoogleboleto VARCHAR(8000) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN st_corentidade VARCHAR(15) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN id_assuntorequerimento VARCHAR(15) NULL;
ALTER TABLE tb_configuracaoentidade ALTER COLUMN bl_loginsimultaneo BIT NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN id_entidade INT NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_entradavalormin decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_entradavalormax decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_valormin decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_valormax decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_valorminparcela decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_multa decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_juros decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_jurosmax decimal NULL;
ALTER TABLE tb_formapagamento ALTER COLUMN nu_jurosmin decimal NULL;
ALTER TABLE tb_tipoenvio ALTER COLUMN st_descricao VARCHAR(100) NULL;
ALTER TABLE tb_turmahorario ALTER COLUMN dt_cadastro datetime2 NULL;
ALTER TABLE tb_turmahorario ALTER COLUMN id_usuariocadastro INT NULL;
ALTER TABLE tb_turmahorario ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_uf ALTER COLUMN nu_codigocapital decimal NULL;
ALTER TABLE tb_uf ALTER COLUMN nu_codigoibge decimal NULL;
ALTER TABLE tb_uf ALTER COLUMN st_nomeregiao VARCHAR(30) NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN nu_contratoduracao INT NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN nu_mesesmulta INT NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN nu_contratomultavalor decimal NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN nu_mesesmensalidade INT NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN nu_tempoextensao INT NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN id_contratomodelo INT NULL;
ALTER TABLE tb_contratoregra ALTER COLUMN id_extensaomodelo INT NULL;
ALTER TABLE tb_motivoocorrencia ALTER COLUMN st_motivoocorrencia VARCHAR(100) NULL;
ALTER TABLE tb_notificacaoentidade ALTER COLUMN dt_cadastro datetime NULL;
ALTER TABLE tb_notificacaoentidade ALTER COLUMN bl_enviaparaemail BIT NULL;
ALTER TABLE tb_notificacaoentidade ALTER COLUMN nu_percentuallimitealunosala INT NULL;
ALTER TABLE tb_usuario ALTER COLUMN st_senha VARCHAR(32) NULL;
ALTER TABLE tb_usuario ALTER COLUMN st_cpf VARCHAR(11) NULL;
ALTER TABLE tb_usuario ALTER COLUMN bl_ativo BIT NULL;
ALTER TABLE tb_usuario ALTER COLUMN bl_redefinicaosenha BIT NULL;

<?php

/**
 *
 * Classe base para os testes de Entidades.
 *
 */
class ModelTestCase extends \ApplicationTestCase
{

    protected $em;

    private $pathVw;

    public function __construct()
    {
        parent::__construct();

        $bootstrap = $this->front->getParam('bootstrap');
        $this->em = $bootstrap->getResource('doctrine')->getEntityManager();

        $this->pathVw = $path = APPLICATION_PATH . "/../tests/fixtures";
    }

    /**
     * Metodo que recebe um arquivo pelo qual ira executar uma instrucao
     * de SQL direta.
     * @param mixed $fixture
     * @throws Zend_Exception
     * @return boolean
     */
    public function loadFixtures($fixture = null)
    {
        try {
            var_dump('loading fixture' . (!empty($fixture)) ? (': ' . $fixture) : 's');

            $path = APPLICATION_PATH . "/../tests/fixtures";
            if (!empty($fixture) && is_file($path . DIRECTORY_SEPARATOR . $fixture)) {
                $sql = file_get_contents($path . DIRECTORY_SEPARATOR . $fixture);

                if (!$sql)
                    return false;

                return $this->em->getConnection()->prepare($sql)->execute();
            } else {
                chdir($path);
                if ($handle = opendir($path)) {
                    while (false !== ($file = readdir($handle))) {
                        if (strstr($file, '.sql')) {
                            $sql = file_get_contents($file);
                            $stmt = $this->em->getConnection()->prepare($sql);
                        }
                    }
                    $stmt->execute();
                }
            }
        } catch (\Doctrine\ORM\ORMException $orm) {
            throw new Zend_Exception($orm->getMessage());
        } catch (\Doctrine\DBAL\ConnectionException $c) {
            throw new Zend_Exception($c->getMessage());
        }

        return true;
    }

    public function beginTransaction()
    {
        $stmt = $this->em->getConnection()->prepare('BEGIN TRANSACTION');
        $stmt->execute();
    }

    public function rolback()
    {
        $stmt = $this->em->getConnection()->prepare('ROLLBACK');
        $stmt->execute();
    }

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

}

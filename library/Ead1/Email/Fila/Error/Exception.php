<?php
/**
 * Classe de exceção de Erro da fila de email
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class Ead1_Email_Fila_Error_Exception extends Zend_Exception{}
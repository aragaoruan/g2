<?php
/**
 * Representa uma mensagem a ser salva/enviada pelo robô de e-mail
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 */

class Ead1_Email_Fila_Mensagem 
{
	const TIPO_ENVIO	= 	3;
	
	protected $idEntidade;
	protected $idUsuarioCadastro;
	protected $idEmailConfig;
	
	/**
	 * @var array
	 */
	protected $_destinatarios	= array();
	
	/**
	 * 
	 * @var string
	 */
	protected $_assunto;
	
	/**
	 * Texto do e-mail
	 * Pode ser tanto em html quanto em texto puro 
	 * @var string
	 */
	protected $_texto;
	
	
	/**
	 * Cria uma nova mensagem para ser adicionada na fila de mensagens. 
	 * @param $idEntidade 		 - id_entidade que irá criar a mensagem
	 * @param $idUsuarioCadastro - id_usuarii que irá cria a mensagem 
	 */
	public function __construct( $idEntidade, $idUsuarioCadastro ){
		$this->idEntidade			= $idEntidade;
		$this->idUsuarioCadastro	= $idUsuarioCadastro;
	}

	/**
	 * Carrega a mensagem a partir de uma mensagem padrão
	 * O array de parâmetros deve ser disponibilizado de acordo com a categoria ( id_textocategoria ) do texto a ser gerado
	 * Caso a categoria seja MATRICULA 	= 1 deve ser informado o índice id_matricula 
	 * Caso a categoria seja CONTRATO 	= 2 deve ser informado o índice id_contrato 
	 * Caso a categoria seja MATRICULA 	= 3 deve ser informado o índice id_matricula
	 * 
	 * Provê Fluent Interface
	 * @param int $idMensagemPadrao
	 * @param array $arParametros
	 * @param array $variaveisSubstituicao
	 * @throws Exception
	 */
	public function carregaMensagemPadrao( $idMensagemPadrao, $arParametros, array $variaveisSubstituicao = NULL ){
		if( is_null( $arParametros ) ){
			throw new Exception( 'Para criar uma nova mensagem a partir de uma mensagem padrão é necessário informar o parâmetro $arParametros' );
		}
		
		$emailBO		= new EmailBO();
		$textosBO		= new TextosBO();
		$mensageiro		= $emailBO->retornarConfEmailMensagemPadrao( $this->idEntidade, $idMensagemPadrao );
		
		if( $mensageiro->getTipo() != Ead1_IMensageiro::SUCESSO ){
			$mensagem			= $mensageiro->getMensagem();
			throw new Exception( $mensagem[0] );
		}
		$mensagemROW			= $mensageiro->getMensagem();
		$mensagemROW			= $mensagemROW[0];
		$this->idEmailConfig	= $mensagemROW->id_emailconfig;
		
		if( !is_null( $variaveisSubstituicao ) ) {
			foreach( $variaveisSubstituicao as $campo => $valor ){
				
				if(method_exists($textosBO,'addVariavelSubstuicao')){
					$textosBO->addVariavelSubstuicao( $campo, $valor );
				} else {
					$mensagemROW->st_texto = $this->substituirVariavelValor($mensagemROW->st_texto, $campo, $valor);
				}

			} 
		}
		
		if(method_exists($textosBO,'gerar')){
			$this->_texto	= $textosBO->gerar( $mensagemROW->id_textosistema, $arParametros, TextosBO::HTML );
		} else {
			$this->_texto	= $mensagemROW->st_texto;
		}
		
		return $this;
	}
	
	/**
	 * Troca uma String tida como variável no texto por um valor determinado
	 * @see Mensagem::carregaMensagemPadrao()
	 * @param String $texto
	 * @param String $campo
	 * @param String $valor
	 * @return String
	 */
	public function substituirVariavelValor($texto, $campo, $valor){
		$texto = str_replace($campo, $valor, $texto);
		return $texto;
	}
	
	/**
	 * Adiciona um destinatário a mensagem
	 * Provides Fluent Interface
	 * 
	 * @param string $enderecoEmailDestinatario
	 * @param string $nomeDestinatario
	 * @param string $idUsuarioDestinatario
	 * @return Ead1_Email_Fila_Mensagem
	 */
	public function addDestinatario( $enderecoEmailDestinatario, $nomeDestinatario, $idUsuarioDestinatario = null  ){

		$arArgumentos	= array( 'st_endereco'	=> $enderecoEmailDestinatario 
								//,'st_nome'		=> $nomeDestinatario 
								,'id_tipoenvio'	=> self::TIPO_ENVIO 
								,'dt_cadastro'	=> date( 'Y-m-d H:i:s' ) 
								,'id_usuario'	=> $idUsuarioDestinatario );
								
		$this->_destinatarios[]	= new EnvioMensagemTO( $arArgumentos );								
		
		return $this;
	}
	
	/**
	 * Retorna um array de EnvioMensagemTO
	 * @return array
	 */
	public function getDestinatarios() {
		return $this->_destinatarios;
	}
	
	/**
	 * Transforma a mensagem em um MensagemTO
	 * O id_funcionalidade não é fornecido. 
	 * 
	 * S
	 * @return MensagemTO
	 */
	public function toMensagemTO() {
		
		$mensagemTO	= new MensagemTO();
		$mensagemTO->id_entidade		= $this->idEntidade;
		$mensagemTO->id_usuariocadastro = $this->idUsuarioCadastro;
		$mensagemTO->dt_cadastro		= date( 'Y-m-d H:i:s' );
		$mensagemTO->st_mensagem		= $this->_assunto;
		$mensagemTO->st_texto			= $this->_texto;
		return $mensagemTO;
	}
	
	/**
	 * Verifica se a mensagem possui destinatários inseridos
	 * @return bool
	 */
	public function possuiDestinatarios(){
		return count( $this->_destinatarios ) >= 1 ? true : false;
	}
	
	/**
	 * Provê Fluent Interface
	 * @param string $assunto
	 * @return Ead1_Email_Fila_Mensagem
	 */
	public function setAssunto( $assunto ){
		$this->_assunto	= $assunto;
		return $this;
	}
	
	/**
	 * Provê Fluent Interface
	 * @param string $texto
	 * @return Ead1_Email_Fila_Mensagem
	 */
	public function setTexto( $texto ){
		$this->_texto	= $texto;
		return $this;
	}
	
	public function getAssunto(){
		return$this->_assunto;
	}
	
	public function getTexto(){
		return$this->_texto;
	}
	
	public function getIdEntidade(){
		return $this->idEntidade;
	}
	
	public function getIdUsuarioCadastro(){
		return $this->idUsuarioCadastro;
	}
	
	public function getIdEmailConfig(){
		return $this->idEmailConfig;
	}
}
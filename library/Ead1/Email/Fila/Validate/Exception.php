<?php
/**
 * Classe de exceção de validação da fila de email
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class Ead1_Email_Fila_Validate_Exception extends Zend_Exception{}
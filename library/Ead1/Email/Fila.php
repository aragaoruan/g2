<?php
/**
 * Classe responsável por enfileirar os e-mails a serem enviados pelo sistema
 * @author Arthur Cláudio de Almeida Pereira
 *
 */
class Ead1_Email_Fila
{
	/**
	 * 
	 * @var array
	 */
	private $_queue; 
	
	public function __construct() {
		$this->_queue				= array();
	}
	
	/**
	 * @param Ead1_Email_Fila_Mensagem $mensagem
	 * @param unknown_type $idFuncionalidade
	 * @return Ead1_Email_Fila
	 */
	public function addMensagem( Ead1_Email_Fila_Mensagem $mensagem, $idFuncionalidade ) {
		$this->_queue[ $idFuncionalidade ][]	= $mensagem;
		return $this;
	} 
	
	
	/**
	 * 
	 * @param bool $enviarAgora
	 * @return string
	 */
	public function salvar( $enviarAgora = false ){
		if( count( $this->_queue ) < 1 ) {
			THROW new Ead1_Email_Fila_Validate_Exception( 'Para salvar os e-mails é necessário ao menos uma mensagem adicionada na fila' );
		}
		$emailDAO			= new EmailDAO();
		$emailDAO->beginTransaction();
		$envioMensagemTO	= '';
		try {
			foreach ( $this->_queue as $idFuncionalidade => $arfilaMensagem ) {
				$dataEnvio	= '';
				$enviado	= false;
				foreach( $arfilaMensagem as $filaMensagem ) {
					if( $filaMensagem->possuiDestinatarios() === false ) {
						THROW new Ead1_Email_Fila_Error_Exception('Não foi encontrado nenhum destinatário para mensagem: '. $filaMensagem->getAssunto() );
					}
					$mensagemTO						= $filaMensagem->toMensagemTO();
					$filaMensagem->setAssunto($mensagemTO->st_mensagem);
					$destinatarios					= $filaMensagem->getDestinatarios();
					$mensagemTO->id_funcionalidade	= $idFuncionalidade;
					$idMensagem						= $emailDAO->salvarMensagem( $mensagemTO ) ;
					$mailer							= new Ead1_Email_Zend( $filaMensagem->getIdEmailConfig() );
					if( $mailer->enviar( $filaMensagem, $idFuncionalidade ) == true ){
						$dataEnvio	= date( 'Y-m-d H:i:s' );
						$enviado	= true;
					}
					foreach( $destinatarios as $envioMensagemTO ){
						$envioMensagemTO->id_mensagem	= $idMensagem;
						$envioMensagemTO->dt_envio		= $dataEnvio;
						$envioMensagemTO->bl_enviado	= $enviado;
						$emailDAO->salvarEnvioMensagem( $envioMensagemTO );
					}
				}
			}
			$emailDAO->commit();
			$status	= true;
		}catch(Zend_Exception $e) {
			$emailDAO->rollBack();
			$status	= false;
			THROW $e;
		}
		return $status;
	}
	
	//@todo implementar desacoplamento de envio do metodo salvar
	private function _enviar() {
		
		if( $this->_mailer === null ) {
			throw new ErrorException( 'Para enviar o e-mail é necessário informar a classe enviadora de e-mails' );
		}
		
		//return $this->_mailer->send();
	}
}
<?php
/**
 * Classe abstrata para envios de e-mails
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 */

abstract class Ead1_Email_Abstract
{
	/**
	 * Armazena as configurações de envio de e-mail por funcionalidade
	 * @var int
	 */
	private  $_entidadeConf	= array();
	protected $_mailer;
	protected $_endRemetente;
	protected $_nomRemetente;
	
	/**
	 * @param $idEntidade
	 * @param $mailer
	 */
	public function __construct (  $idEmailConfig = null ) {
		if( !is_null( $idEmailConfig ) ) {
			$this->_configure( $idEmailConfig );		
		}
		$this->_defaultConfigure($idEmailConfig);
	}
	
	/**
	 * Realiza a configuração padrão para envio de e-mail
	 */
	private function _defaultConfigure($idEmailConfig) {
		$arConfig						= Ead1_Ambiente::email()->toArray();
		$this->_entidadeConf['default']	= new EmailConfiguracaoTO( $arConfig['mail'] );
		$this->_configure($idEmailConfig);
	}
	
	/**
	 * Busca os dados de configuração de e-mail e preenche o array de configurações por funcionalidade
	 * @param int $idEmailConfig
	 */
	private function _configure( $idEmailConfig ) {
		$emailDAO				= new EmailDAO();
		$dadosConfROWSET		= $emailDAO->retornarDadosConfEmail( $idEmailConfig );
		if( $dadosConfROWSET->count() > 1 ) {
			$arConfiguracaoEmail	= '';
			Zend_Debug::dump($dadosConfROWSET->current());exit;
			$this->_endRemetente	= $dadosConfROWSET->current()->st_conta;
			$this->_nomRemetente	= $dadosConfROWSET->current()->st_nomeentidade;
	
			foreach( $dadosConfROWSET as $dadosConfROW ) {
				
				$arConfiguracaoEmail['st_conta']				= $dadosConfROW->st_conta;
				$arConfiguracaoEmail['st_imap']					= $dadosConfROW->st_imap;
				$arConfiguracaoEmail['st_smtp']					= $dadosConfROW->st_smtp;
				$arConfiguracaoEmail['st_pop']					= $dadosConfROW->st_pop;
				$arConfiguracaoEmail['bl_autenticacaosegura']	= $dadosConfROW->bl_autenticacaosegura;
				$arConfiguracaoEmail['nu_portaentrada']			= $dadosConfROW->nu_portaentrada;
				$arConfiguracaoEmail['nu_portasaida']			= $dadosConfROW->nu_portasaida;
				$arConfiguracaoEmail['st_usuario']				= $dadosConfROW->st_usuario;
				$arConfiguracaoEmail['st_senha']				= $dadosConfROW->st_senha;
				
				$this->_entidadeConf[ $dadosConfROW->id_funcionalidade ]	= new EmailConfiguracaoTO( $arConfiguracaoEmail );
			}
		}
	}
	
	/**
	 * Retorna as configurações de e-mail para a funcionalidade
	 * @param int $idFuncionalidade
	 * @return EmailConfiguracaoTO
	 */
	protected function _getConfiguracaoEmailFuncionalidade( $idFuncionalidade = null  ){
		
		if( array_key_exists( $idFuncionalidade, $this->_entidadeConf ) === false || empty( $idFuncionalidade ) ) {
			$emailConfiguracaoTO	= $this->_entidadeConf['default'];
			$this->_endRemetente	= $emailConfiguracaoTO->st_conta;
			$this->_nomRemetente	= $emailConfiguracaoTO->st_nomeentidade;
			return $this->_entidadeConf['default'];	
		}
		return $this->_entidadeConf[ $idFuncionalidade ];
	} 
	
	/**
	 * 
	 * @param Ead1_Email_Fila_Mensagem $mensagem
	 * @return bool
	 */
	public function enviar( Ead1_Email_Fila_Mensagem $mensagem, $idFuncionalidade ){
		$this->_setMessage( $mensagem );
		return $this->_enviar( $mensagem, $idFuncionalidade );
	}
	
	abstract protected function _setMessage( Ead1_Email_Fila_Mensagem $mensagem );
	
	abstract protected function _enviar( Ead1_Email_Fila_Mensagem $mensagem, $idFuncionalidade );
}

<?php
/**
 * Classe de envio de e-mail utilizando a biblioteca Zend_Mail
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeidapereira@gmail.com >
 *
 */
class Ead1_Email_Zend extends Ead1_Email_Abstract {
	
	/**
	 * @param int $idEmailConfig
	 */
	public function __construct( $idEmailConfig ){
		parent::__construct( $idEmailConfig );
		$this->_mailer	= new Zend_Mail( 'UTF-8' );
	}
			
	/**
	 * @todo implementar outra forma de transport
	 * @todo implementar forma de envio através de conexão segura( ssl / tls )
	 * @todo implementar log de erro de envio de e-mail
	 * @param int $idFuncionalidade
	 * @return bool
	 */		
	protected function _enviar( Ead1_Email_Fila_Mensagem $mensagem, $idFuncionalidade ) {
		
		$emailConfTO	= $this->_getConfiguracaoEmailFuncionalidade( $idFuncionalidade );
		
		if( !empty( $emailConfTO->st_smtp ) ) {
			$arOptions	= array( 'username' => $emailConfTO->st_usuario, 'password' => $emailConfTO->st_senha, 'auth' => 'plain' );
			
			if( $emailConfTO->bl_autenticacaosegura == 1 ) {
				$arOptions['auth']	= 'login';
			}
			
			$transport	= new Zend_Mail_Transport_Smtp( $emailConfTO->st_smtp, $arOptions );
		}
		
		$this->_mailer->setFrom( $this->_endRemetente, $this->_nomRemetente );
		$this->_mailer->send( $transport );
		
		return true;
	}
	
	/**
	 * Configura a mensagem a ser enviada
	 * Seta o assunto e corpo da mensagem
	 * Adiciona os destinatários da mensagem
	 * @param Ead1_Email_Fila_Mensagem $mensagem
	 */
	protected function _setMessage( Ead1_Email_Fila_Mensagem $mensagem ){
		$this->_mailer->setSubject( $mensagem->getAssunto() );
		$this->_mailer->setBodyHtml( $mensagem->getTexto() );
		
		$destinatarios	= $mensagem->getDestinatarios();
		
		foreach( $destinatarios as $envioMensagemTO ){
			$this->_mailer->addTo( $envioMensagemTO->st_endereco, $envioMensagemTO->st_nome );
		}
	}
	
}


	
	
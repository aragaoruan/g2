<?php

/**
 * Classe cliente do serviço de pagamento via carão de crédito provido pela Ead1Shop - Central de Pagamento
 * @author Arthur Cláudio de Almeida Pereira 
 *
 */
class Ead1_Service_PagamentoCartao extends Zend_Rest_Client {
	
	/**
	 * Url do serviço de pagamento de e-mail
	 * @var string
	 */
	protected $_url	= 'https://admescolar.websiteseguro.com/Ead1Shop/public/api/Cartao';
	
	public function __construct( $uri = null ) {
		
		if( is_null( $uri ) ) {
			$uri	= $this->_url;
		}
		
		parent::__construct( $uri );
		self::getHttpClient()->setHeaders( 'Accept-Charset', 'utf-8' )
								->setConfig( array( 'timeout' => 30) );
	}
	
	/**
	 * Realiza a comunição com o servico de pagamento
	 * @param PagamentoCartaoTO $pagamentoCartaoTO
	 * @return bool
	 */
	public function realizarPagamento( PagamentoCartaoTO $pagamentoCartaoTO ){
		
		if( empty( $pagamentoCartaoTO->ano_validade )  ) {
			throw new InvalidArgumentException( 'Informe o ano de validade do cartão de crédito para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->mes_validade )  ) {
			throw new InvalidArgumentException( 'Informe o mês de validade do cartão de crédito para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->cod_seguranca )  ) {
			throw new InvalidArgumentException( 'Informe o código de segurança do cartão de crédito para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->num_cartao1 ) || empty( $pagamentoCartaoTO->num_cartao2 ) || empty( $pagamentoCartaoTO->num_cartao3 ) || empty( $pagamentoCartaoTO->num_cartao4 ) ) {
			throw new InvalidArgumentException( 'Informe o número do cartão de crédito para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->id_venda ) ) {
			throw new InvalidArgumentException( 'Informe o código da venda para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->id_cartaobandeira ) ){
			throw new InvalidArgumentException( 'Informe o código da bandeira do cartão para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->valor_total ) ){
			throw new InvalidArgumentException( 'Informe o valor total para pagamento no cartão para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->st_portadorcartao ) ){
			throw new InvalidArgumentException( 'Informe o nome do portador do cartão.' );
		}
		
		if( empty( $pagamentoCartaoTO->quantidade_parcela ) ){
			throw new InvalidArgumentException( 'Informe a quantidade de parcelas para pagamento no cartão para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->num_afiliacao ) ){
			throw new InvalidArgumentException( 'Informe o número de afiliação (contrato) com a operadora para pagamento no cartão para o serviço de pagamento' );
		}
		
		if( empty( $pagamentoCartaoTO->num_gateway ) ){
			throw new InvalidArgumentException( 'Informe o número de gateway (contrato) com a Locaweb para pagamento no cartão para o serviço de pagamento' );
		}
		
		$params	= array(
						'ano_validade'			=>	str_pad( $pagamentoCartaoTO->ano_validade, 2, 0, STR_PAD_LEFT ) 
					   ,'mes_validade'			=>	str_pad( $pagamentoCartaoTO->mes_validade, 2, 0, STR_PAD_LEFT ) 
					   ,'cod_seguranca'			=>	$pagamentoCartaoTO->cod_seguranca
					   ,'num_cartao1'			=>	$pagamentoCartaoTO->num_cartao1
					   ,'num_cartao2'			=>	$pagamentoCartaoTO->num_cartao2
					   ,'num_cartao3'			=>	$pagamentoCartaoTO->num_cartao3
					   ,'num_cartao4'			=>	$pagamentoCartaoTO->num_cartao4
					   ,'id_pagamento'			=>	$pagamentoCartaoTO->id_venda
					   ,'valor_total'			=>	$pagamentoCartaoTO->valor_total
					   ,'nome_portador_cartao'	=>	$pagamentoCartaoTO->st_portadorcartao
					   ,'quantidade_parcela'	=>	$pagamentoCartaoTO->quantidade_parcela
					   ,'num_afiliacao'			=>	$pagamentoCartaoTO->num_afiliacao
					   ,'num_gateway'			=>	$pagamentoCartaoTO->num_gateway
					   ,'ambiente'				=>	'teste'
					   ,'id_bandeiracartao'		=>	$pagamentoCartaoTO->id_cartaobandeira
					   ,'nom_pessoa'			=>	$pagamentoCartaoTO->st_portadorcartao
					   ,'num_dddtelefone'		=>	''
					   ,'num_telefone'			=>	''
					   ,'email'					=>	''
						);
		
								
		$httpResponse	= $this->restPost( $this->_uri->getPath(), $params );
		$simpleXML		= new SimpleXMLElement( $httpResponse->getBody() );
		
		if( $httpResponse->isError() ) {
			throw new ErrorException( 'Não foi possível ao realizar o pagamento com o gateway '. $simpleXML->erro->mensagem );
		}
		
		$pagamentoCartaoTO->tid	= (string) $simpleXML->tid;
		return true;
	}
}
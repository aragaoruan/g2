<?php
/**
 * 
 * Classe com utilidades
 * @author Elcio Mauro Guimarães
 *
 */
class Ead1_Util {
	
	/**
	 * Converte um objeto para array
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param unknown_type $object
	 * @return Ambigous <NULL, array, multitype:NULL >
	 */
	public function object_to_array($object){
		$new=NULL;
		if(is_object($object)){
			$object=(array)$object;
		}
		
		//return array($object);
		
		if(is_array($object)){
			$new=array();
			foreach($object as $key => $val) {
				$key=preg_replace("/^\\0(.*)\\0/","",$key);
				$key=str_replace("key_","",$key);
				$new[$key] = self::object_to_array($val);
			}
		}else{
			$new = $object;
		}
		return $new;
	}

    /**
     * MÃ©todo que pega um Objeto e converte em Array
     * @param object
     */
    public static function objectToArray(  $object){
        $new=NULL;

        if(is_object($object)){
            $object=(array)$object;
            foreach($object as $key => $val) {
                if(is_object($val)){
                    $object[$key] = self::objectToArray($val);
                }
                if(is_string($val)){
                    if(trim($val)==''){
                        unset($object[$key]);
                    } else {
                        $object[$key] = trim($val);
                    }
                }
            }

        }

        if(!$object && $object !== '0'){
            return null;
        }

        if(is_array($object)){
            $new=array();
            foreach($object as $key => $val) {
                $key=preg_replace("/^\\0(.*)\\0/","",$key);
                $key=str_replace("key_","",$key);
                $new[$key] = self::objectToArray($val);
            }
        }else{
            $new = $object;
        }
        return $new;
    }

	/**
	 *  Método para Listar as Unidades Federativas
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param UfTO $to
	 * @return Ead1_Mensageiro
	 */
	public static function listarUF(UfTO $to){
	
		try {
			$orm = new UfORM();
			$dados = $orm->consulta($to, false, false, array('st_uf'));
			return new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro ao retornar as UFs.', Ead1_IMensageiro::ERRO);
		}
	
	}

	/**
	 *  Método para Listar os Países
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param PaisTO $to
	 * @return Ead1_Mensageiro
	 */
	public static function listarPais(PaisTO $to){
	
		try {
			$orm = new PaisORM();
			$dados = $orm->consulta($to, false, false, array('st_nomepais'));
			return new Ead1_Mensageiro($dados, Ead1_IMensageiro::SUCESSO);
		} catch (Exception $e) {
			return new Ead1_Mensageiro('Erro ao retornar os Países.', Ead1_IMensageiro::ERRO);
		}
	
	}
	
	
	/**
	 * Método que retorna os Municípios
	 * @param MunicipioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function retornarMunicipios(MunicipioTO $to){
	
		try {
	
			$tbMunicipio = new MunicipioORM();
			return new Ead1_Mensageiro($tbMunicipio->consulta($to, false, false, 'st_nomemunicipio'), Ead1_IMensageiro::SUCESSO);
	
		} catch (Exception $e) {
			return new Ead1_Mensageiro("Erro ao retornar os Municípios", Ead1_IMensageiro::ERRO);
		}
	
	
	}
	
	/**
	 * Verifica a existencia de uma URL
	 * @param string $url
	 * @return boolean
	 */
	public static function url_exists($url) {
		
		$headers = @get_headers($url);
		if(strpos($headers[0],'200')===false){ 
			return false;
		}
		return true;
		
	}
	
	
	/**
	 * Envia o Json mas antes dispara os Headers do http retornando o log do firephp firebug
	 * @param unknown_type $controller
	 * @param unknown_type $data
	 * na Action chamar Ead1_Util::json($this, $dados);
	 */
	public static function json($controller, $data){
		Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
		return $controller->getHelper('json')->sendJson($data);
	}
	
}
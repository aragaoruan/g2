<?php

/**
 * Classe que Gera XLS
 * @author Eduardo Romão - ejushiro@gmail.com
 * Data: 29/03/2011
 */
class Ead1_GeradorXLS
{

    /**
     * Metodo Construtor
     * @param array $arrXLSHeaderTO
     * @param array $arrTO
     * @param XLSConfigurationTO $XLSConfigurationTO
     * @return string $html
     */
    public function __construct(array $arrXLSHeaderTO = null, array $arrTO = null, XLSConfigurationTO $XLSConfigurationTO = null)
    {
        if (is_null($XLSConfigurationTO)) {
            $XLSConfigurationTO = new XLSConfigurationTO();
        }
        $this->setaPars($arrXLSHeaderTO, $arrTO, $XLSConfigurationTO);
        //Retornar o XLS com Header...
    }

    /**
     * Variavel que contem o Array dos Tos de Header
     * @var array
     */
    private $arrXLSHeaderTO;

    /**
     * Variavel que contem os TO's do Conteudo do Corpo da Tabela
     * @var array
     */
    private $arrTO;

    /**
     * Variavel que contem o To de Configuração do XLS
     * @var XLSConfigurationTO
     */
    private $XLSConfigurationTO;

    /**
     * @return XLSConfigurationTO $XLSConfigurationTO
     */
    public function getXLSConfigurationTO()
    {
        return $this->XLSConfigurationTO;
    }

    /**
     * @param XLSConfigurationTO $XLSConfigurationTO
     */
    public function setXLSConfigurationTO($XLSConfigurationTO)
    {
        $this->XLSConfigurationTO = $XLSConfigurationTO;
    }

    /**
     * @return the $arrXLSHeaderTO
     */
    public function getArrXLSHeaderTO()
    {
        return $this->arrXLSHeaderTO;
    }

    /**
     * @param array $arrXLSHeaderTO
     */
    public function setArrXLSHeaderTO($arrXLSHeaderTO)
    {
        $this->arrXLSHeaderTO = $arrXLSHeaderTO;
    }

    /**
     * @return the $arrTO
     */
    public function getArrTO()
    {
        return $this->arrTO;
    }

    /**
     * @param array $arrTO
     */
    public function setArrTO($arrTO)
    {
        $this->arrTO = $arrTO;
    }

    /**
     * Metodo que gera o XLS ja com o Header
     */
    public function geraXLS($arrXLSHeaderTO, $arrTO = null, XLSConfigurationTO $XLSConfigurationTO, $converterEnconding = true)
    {
        try {
            $this->setaPars($arrXLSHeaderTO, $arrTO, $XLSConfigurationTO);
            $this->validaPars();
            $xls = $this->montaHTML();
            header('Content-type: application/x-msexcel; charset=ISO-8859-1');
            header('Content-Disposition: attachment; filename="' . $this->getXLSConfigurationTO()->getFilename() . '.xls"');
            header("Content-Description: Gerador de XLS By: Eduardo Romão - ejushiro@gmail.com Data: 29/03/2011");

            if($converterEnconding){
                echo mb_convert_encoding($xls,'ISO-8859-1','auto');
            }else{
                echo $xls;
            }
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que gera o HTML sem o Header
     */
    public function geraHTML(array $arrXLSHeaderTO, array $arrTO, XLSConfigurationTO $XLSConfigurationTO, $encode = false)
    {
        try {
            $this->setaPars($arrXLSHeaderTO, $arrTO, $XLSConfigurationTO);
            $this->validaPars();
            $html = $this->montaHTML();
            if ($encode) {
                $html = utf8_encode($html);
            }
            return $html;
        } catch (Zend_Exception $e) {
            THROW new Zend_Exception($e->getMessage());
        }
    }

    /**
     * Metodo que monta o HTML
     * @return string $html
     */
    private function montaHTML()
    {
        $html = '';
        //Styles
        $titleStyle = $this->montaTitleStyle();
        $bodyStyle = $this->montaBodyStyle();
        $footerStyle = $this->montaFooterStyle();
        $headerStyle = $this->montaHeaderStyle();
        $tableStyle = $this->montaTableStyle();
        //Fim Styles
//        $html .= "<!DOCTYPE html><html><head></head><body>";
        $html .= "<style>.num {
                      mso-number-format:'\@';
                    }</style><table>";
        $html .= "<thead>";
        if ($this->getXLSConfigurationTO()->getTitle()) {
            $html .= "<tr>";
            $html .= "<th colspan='" . count($this->getArrXLSHeaderTO()) . "' style = 'border:solid #000; border-collapse:collapse; border-width:thin; {$titleStyle}'>" . $this->getXLSConfigurationTO()->getTitle() . "</th>";
            $html .= "</tr>";
        }
        $html .= "<tr>";
        foreach ($this->getArrXLSHeaderTO() as $XLSHeaderTO) {
            $strHeader = $XLSHeaderTO->getSt_header();
            if ($XLSHeaderTO->getSt_par() == 'st_cpf')
                $strHeader = 'NUMERO_CPF';
            $html .= "<th style='border:solid #000; border-collapse:collapse; border-width:thin; {$headerStyle}'>" . $strHeader . "</th>";
        }
        $html .= "</tr>";
        $html .= "</thead>";

        if ($this->getArrTO()) {
            $html .= "<tbody style='border:solid #000; border-collapse:collapse; border-width:thin; {$bodyStyle}'>";
            $cor = 2;
            $color = 'getStripedBgColor2';
            foreach ($this->getArrTO() as $to) {
                $html .= "<tr>";
                foreach ($this->getArrXLSHeaderTO() as $XLSHeaderTO) {

                    $arrPars = $to->toArray();
                    $keys = array_keys($arrPars);
                    $par = $XLSHeaderTO->getSt_par();
                    if (in_array($par, $keys)) {


                        if (empty($to->$par) || strtolower($to->$par) == 'null') {
                            $to->$par = '';
                        }
                        if ($to->$par instanceof Zend_Date) {
                            $to->$par = $to->$par->toString('dd/MM/yyyy');
                        }

                        if(substr($par, 0, 3)=='nu_' && is_float($to->$par)){
                            $nao = array("nu_numero","nu_parcelas", "nu_parcela", "nu_celular", "nu_telefone");
                            if(!in_array($par, $nao)){
                                $to->$par = number_format($to->$par, 2,',','');
                            }
                        }

                        /**
                         * @history AC-27016
                         * @update 29/06/2015
                         * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
                         * @description Transformar o number CPF pra STRING no XLS, pra evitar remover os zeros no comeco
                         */
                        if ($par == 'st_cpf') {
                            $to->$par = "&#8203;{$to->$par}&nbsp;";
                        }

                        /**
                         * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
                         * @description Mascara para o CEP
                         */
                        if ($par == 'st_cep') {
                            $to->$par = preg_replace('/\D/', '', $to->$par);
                            $to->$par = $this->applyMask($to->$par, '#####-###');
                        }

                        /**
                         * @author Rafael Leite <rafael.leite@unyleya.com.br>
                         * @description Forçar formatação como texto
                         */
                        if (substr($par, 0, 3) == 'st_') {
                            $bodyStyle .= 'mso-number-format:"\@";';
                        }

                        $html .= "<td style='{$bodyStyle} border:solid #000; border-collapse:collapse; border-width:thin; background-color: {$this->getXLSConfigurationTO()->$color()};' class=''   >";
                        $html .= utf8_decode($to->$par);
                        $html .= "</td>";
                        $bodyStyle = '';
                    } else {
                        $html .= "<td style='{$bodyStyle} border:solid #000; border-collapse:collapse; border-width:thin; background-color: {$this->getXLSConfigurationTO()->$color()};'  class='' >";
                        $html .= "</td>";
                    }

                };
                $html .= "</tr>";
//                if ($cor == 1) {
//                    if ($this->getXLSConfigurationTO()->getStriped()) {
//                        $color = 'getStripedBgColor2';
//                    } else {
//                        $color = 'getStripedBgColor1';
//                    }
//                    $cor = 2;
//                } else {
//                    if ($this->getXLSConfigurationTO()->getStriped()) {
//                        $color = 'getStripedBgColor1';
//                    } else {
//                        $color = 'getStripedBgColor1';
//                    }
//                    $cor = 1;
//                }
            } //exit;
            $html .= "</tbody>";

        } else {
            $html .= "<tbody style='{$bodyStyle}'>";
            $html .= "<tr>";
            $html .= "<td colspan='" . count($this->getArrXLSHeaderTO()) . "' style = 'border:solid #000; border-collapse:collapse; border-width:thin;'>";
            $html .= "Nenhum Registro Encontrado!";
            $html .= "</td>";
            $html .= "</tr>";
            $html .= "</tbody>";
        }
        if ($this->getXLSConfigurationTO()->getFooter()) {
            $html .= "<tfoot>";
            $html .= "<tr>";
            $html .= "<td colspan='" . count($this->getArrXLSHeaderTO()) . "' style = 'border:solid #000; border-collapse:collapse; border-width:thin; {$footerStyle}'>";
//            $html .= utf8_decode($this->getXLSConfigurationTO()->getFooter());
            $html .= $this->getXLSConfigurationTO()->getFooter();
            $html .= "</td>";
            $html .= "</tr>";
            $html .= "</tfoot>";
        }
        $html .= "</table>";
//        $html .= "</body></html>";
        return $html;
    }

    /**
     * Metodo que valida os parametros
     * @param array $conteudo
     * @param XLSConfigurationTO $configurationTO
     * @throws Zend_Exception
     * @return boolean
     */
    private function validaPars()
    {
        if (!$this->getArrXLSHeaderTO()) {
            THROW new Zend_Exception('O Array de Header não pode vir vazio!');
            return false;
        }
        foreach ($this->getArrXLSHeaderTO() as $index => $XLSHeaderTO) {
            if (!($XLSHeaderTO instanceof XLSHeaderTO)) {
                THROW new Zend_Exception('O Array de XLSHeaderTO  contem itens que não são XLSHeaderTO');
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo que seta os parametros
     * @param array $arrXLSHeaderTO
     * @param array $arrTO
     * @param XLSConfigurationTO $XLSConfigurationTO
     */
    private function setaPars($arrXLSHeaderTO, $arrTO = null, XLSConfigurationTO $XLSConfigurationTO)
    {
        $this->setArrXLSHeaderTO($arrXLSHeaderTO);
        $this->setArrTO($arrTO);
        $this->setXLSConfigurationTO($XLSConfigurationTO);
    }

    /**
     * Metodo que gera o Style do Titulo
     * @param boolean $comStyle
     * @return string $style
     */
    private function montaTitleStyle($comStyle = false)
    {
        $style = '';
        if ($comStyle) {
            $style .= "style = '";
        }
        if ($this->getXLSConfigurationTO()->getTitleBgColor()) {
            $style .= "background-color: {$this->getXLSConfigurationTO()->getTitleBgColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTitleFontColor()) {
            $style .= "color: {$this->getXLSConfigurationTO()->getTitleFontColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTitleFontSize()) {
            $style .= "font-size: {$this->getXLSConfigurationTO()->getTitleFontSize()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTitleTextAlign()) {
            $style .= "text-align: {$this->getXLSConfigurationTO()->getTitleTextAlign()}; ";
        }
        if ($comStyle) {
            $style .= "'";
        }
        return $style;
    }

    /**
     * Metodo que gera o Style do Footer
     * @param boolean $comStyle
     * @return string $style
     */
    private function montaFooterStyle($comStyle = false)
    {
        $style = '';
        if ($comStyle) {
            $style .= "style = '";
        }
        if ($this->getXLSConfigurationTO()->getFooterBgColor()) {
            $style .= "background-color: {$this->getXLSConfigurationTO()->getFooterBgColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getFooterFontColor()) {
            $style .= "color: {$this->getXLSConfigurationTO()->getFooterFontColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getFooterFontSize()) {
            $style .= "font-size: {$this->getXLSConfigurationTO()->getFooterFontSize()}; ";
        }
        if ($this->getXLSConfigurationTO()->getFooterTextAlign()) {
            $style .= "text-align: {$this->getXLSConfigurationTO()->getFooterTextAlign()}; ";
        }
        if ($comStyle) {
            $style .= "'";
        }
        return $style;
    }

    /**
     * Metodo que gera o Style do Table
     * @param boolean $comStyle
     * @return string $style
     */
    private function montaTableStyle($comStyle = false)
    {
        $style = '';
        if ($comStyle) {
            $style .= "style = '";
        }
        if ($this->getXLSConfigurationTO()->getTableBgColor()) {
            $style .= "background-color: {$this->getXLSConfigurationTO()->getTableBgColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableFontColor()) {
            $style .= "color: {$this->getXLSConfigurationTO()->getTableFontColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableFontSize()) {
            $style .= "font-size: {$this->getXLSConfigurationTO()->getTableFontSize()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableHeight()) {
            $style .= "height: {$this->getXLSConfigurationTO()->getTableHeight()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableMaxHeight()) {
            $style .= "max-height: {$this->getXLSConfigurationTO()->getTableMaxHeight()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableMinHeight()) {
            $style .= "min-height: {$this->getXLSConfigurationTO()->getTableMinHeight()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableMaxWidth()) {
            $style .= "max-width: {$this->getXLSConfigurationTO()->getTableMaxWidth()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableMinWidth()) {
            $style .= "min-width: {$this->getXLSConfigurationTO()->getTableMinWidth()}; ";
        }
        if ($this->getXLSConfigurationTO()->getTableWidth()) {
            $style .= "width: {$this->getXLSConfigurationTO()->getTableWidth()}; ";
        }
        if ($comStyle) {
            $style .= "'";
        }
        return $style;
    }

    /**
     * Metodo que gera o Style do Header
     * @param boolean $comStyle
     * @return string $style
     */
    private function montaHeaderStyle($comStyle = false)
    {
        $style = '';
        if ($comStyle) {
            $style .= "style = '";
        }
        if ($this->getXLSConfigurationTO()->getHeaderBgColor()) {
            $style .= "background-color: {$this->getXLSConfigurationTO()->getHeaderBgColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getHeaderFontColor()) {
            $style .= "color: {$this->getXLSConfigurationTO()->getHeaderFontColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getHeaderFontSize()) {
            $style .= "font-size: {$this->getXLSConfigurationTO()->getHeaderFontSize()}; ";
        }
        if ($this->getXLSConfigurationTO()->getHeaderTextAlign()) {
            $style .= "text-align: {$this->getXLSConfigurationTO()->getHeaderTextAlign()}; ";
        }
        if ($comStyle) {
            $style .= "'";
        }
        return $style;
    }

    /**
     * Metodo que gera o Style do Body
     * @param boolean $comStyle
     * @return string $style
     */
    private function montaBodyStyle($comStyle = false)
    {
        $style = '';
        if ($comStyle) {
            $style .= "style = '";
        }
        if ($this->getXLSConfigurationTO()->getBodyBgColor()) {
            $style .= "background-color: {$this->getXLSConfigurationTO()->getBodyBgColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getBodyFontColor()) {
            $style .= "color: {$this->getXLSConfigurationTO()->getBodyFontColor()}; ";
        }
        if ($this->getXLSConfigurationTO()->getBodyFontSize()) {
            $style .= "font-size: {$this->getXLSConfigurationTO()->getBodyFontSize()}; ";
        }
        if ($this->getXLSConfigurationTO()->getBodyTextAlign()) {
            $style .= "text-align: {$this->getXLSConfigurationTO()->getBodyTextAlign()}; ";
        }
        if ($comStyle) {
            $style .= "'";
        }
        return $style;
    }

    /**
     * Gera do HTML um PDF
     * @param  $arrXLSHeaderTO
     * @param  $arrTO
     * @param  XLSConfigurationTO $xlsConfig
     */
    public function gerarHTML2PDF($arrXLSHeaderTO, $arrTO = null, XLSConfigurationTO $xlsConfig)
    {
        require_once(APPLICATION_PATH . '/../dompdf/dompdf_config.inc.php');
        $this->ajax();
        $xls = new Ead1_GeradorXLS();
        $xls->geraHTML($arrXLSHeaderTO, $arrTO, $xlsConfig);
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader('DOMPDF_autoload', '');
        $dompdf = new DOMPDF();
        $dompdf->load_html($this->montaHTMLCabecalho($xlsConfig));
        $dompdf->set_paper($xlsConfig->getTipoPapel(), $xlsConfig->getOrientacao());
        $dompdf->render();
        $dompdf->stream($xlsConfig->getFilename() . ".pdf");
    }

    /**
     * Montar o cabeÃ§alho HTML dinÃ¢mico
     * @param XLSConfigurationTO $xlsConfig
     * @todo mudar o caminho da imagem
     * @return View
     */
    public function montaHTMLCabecalho(XLSConfigurationTO $xlsConfig)
    {
        $entidadeTO = new EntidadeTO();
        $entidadeTO->setId_entidade($entidadeTO->getSessao()->id_entidade);
        $bo = new EntidadeBO();
        $retornoEntidade = $bo->retornaEntidade($entidadeTO);
        $retornoEnt = $retornoEntidade->getMensagem();
        $retornoEmail = $bo->retornaContatosEntidade($entidadeTO);
        $retornoStEmail = $retornoEmail->getMensagem();
        $retornoEndereco = $bo->retornaEnderecoEntidade($entidadeTO);
        $retornoStEndereco = $retornoEndereco->getMensagem();
        $view = new Zend_View();
        $view->nomeEntidade = (is_object($retornoEnt[0])) ? $retornoEnt[0]->st_nomeentidade : NULL;
        //mudar o caminho da imagem
        $view->logoEntidade = (is_object($retornoEnt[0])) ? $retornoEnt[0]->st_urlimglogo : 'http://gestor2.localhost.com/img/nao_encontrada.jpg';
        $view->email = (is_object($retornoStEmail[0])) ? $retornoStEmail[0]->st_email : "E-mail não informado";
        $view->endereco = (is_object($retornoStEndereco[0])) ? $retornoStEndereco[0]->st_endereco : "Endereço não informado";
        $view->html = $xlsConfig->getHtml();
        $view->addScriptPath(APPLICATION_PATH . "/apps/gestor2/views/scripts/geradorArquivos/");
        return $view->render('montaHTMLCabecalho.phtml');
    }

    /**
     * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
     * @date 2015-09-16
     * @desciption Aplica mascara a uma string, use # para o replacement
     * @param mixed $string
     * @param string $mask
     * @param string $default
     * @return string
     */
    public function applyMask($string, $mask, $default = '') {
        if ($string) {
            for ($i = 0; $i < strlen($string); $i++) {
                $mask[strpos($mask, '#')] = $string[$i];
            }
            return $mask;
        }

        return $default;
    }

}

<?php
/**
 * Classe Singleton para controle de Transações utilizando SingleTon
 * @author edermariano
 *
 */
class Ead1_Transacao{
	
	/**
	 * Atributo estático que contám a instancia do objeto Ead1_Transacao
	 * @var Ead1_Transacao
	 */
	public static $instancia;
	
	/**
	 * Variavel que diz se a transação está aberta
	 * @var boolean
	 */
	public $transacao;
	
	/**
	 * Variavel que contem a quantidade de transações abertas
	 * @var int
	 */
	public $qtdTransacao;
	
	/**
	 * Método construtor privado para impedir instancia da classe Ead1_Transacao sem utilização do Singleton
	 */
	private function __construct(){}
	
	/**
	 * Metodo que Verifica se já Existe uma instancia da classe
	 * @return object $instancia 
	 */
	public static function getInstancia(){
		if(self::$instancia === null){
			self::$instancia = new Ead1_Transacao();
		}
		return self::$instancia;
	}
}
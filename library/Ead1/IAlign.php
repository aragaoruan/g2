<?php
/**
 * Classe de Interface de Alinhamento
 * @author Eduardo Romão - ejushiro@gmail.com
 */
interface Ead1_IAlign{
	
	const TEXT_ALIGN_CENTER = 'center';
	const TEXT_ALIGN_INHERIT = 'inherit';
	const TEXT_ALIGN_JUSTIFY = 'justify';
	const TEXT_ALIGN_LEFT = 'left';
	const TEXT_ALIGN_RIGHT = 'right';
	
}

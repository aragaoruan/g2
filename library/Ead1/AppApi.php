<?php


/**
 * Classe com implementações padrões para a API do aplicativo mobile
 *
 */
class Ead1_AppApi extends \Zend_Controller_Action// não tem porque estender da Zend_Rest_Controller se não mapeamos os métodos default do zend para rest
{
    private $auth;
    public $token;

    /**
     * @var \G2\Utils\User
     */
    private $user;

    /**
     * Directions permitidas
     * @var array
     */
    protected $directions = array('ASC', 'DESC');

    /**
     * @return \G2\Utils\User
     */
    public function getuser()
    {
        return $this->user;
    }

    public function json(array $data)
    {
//        header('Content-Type: text/html; charset=utf-8');
//        \Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
        $this->_helper->json($data);
    }

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->auth = new \G2\Entity\AuthToken();

        try {

            //verifica primeiro se a requisição não é do tipo options
            if (!$this->getRequest()->isOptions()) {
                switch ($this->getRequest()->getActionName()) {
                    case "create":
                        if (!$this->getRequest()->isPost()) {
                            throw new Exception("Método inválido, por favor faça a requisição usando POST");
                        }
                        break;
                    case "update":
                        if (!$this->getRequest()->isPut()) {
                            throw new Exception("Método inválido, por favor faça a requisição usando PUT");
                        }
                        break;
                    case "delete":
                        if (!$this->getRequest()->isDelete()) {
                            throw new Exception("Método inválido, por favor faça a requisição usando DELETE");
                        }
                        break;
                }
            }

            $headers = $this->getHeaders();
            $params = $this->getRequest()->getParams();
            $freecontrollers = array('auth', 'login');

            if (isset($headers['public_token']) && $headers['public_token'] == $this->auth->getPublicToken()) {
                if (!in_array($params['controller'], $freecontrollers) && isset($headers['user_token']) && !$this->getRequest()->isOptions()) {
                    $user = JWT::decode($headers['user_token'], $this->auth->getSecretToken(), array('HS256'));
                    $this->user = new \G2\Utils\User();
                    $this->user->setuser_name($user->user_name);
                    $this->user->setuser_id($user->user_id);
                    $this->user->setuser_avatar($user->user_avatar);
                    $this->user->setlogin($user->login);


                    $this->user->setentidade_id((isset($user->entidade_id) ? $user->entidade_id : null));
                } else if (!in_array($params['controller'], $freecontrollers)) {
                    throw new Zend_Exception('Token de acesso inválido.');
                }
            } else {
                //TODO: EXCLUIR IF EM PRODUÇÂO
                if (!$this->getRequest()->isOptions()) {
                    throw new Zend_Exception('Acesso não autorizado.');
                }
                exit;
            }

        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(400);
            $error = new \G2\Utils\Error(\G2\Utils\Error::ERROR, $e->getMessage(), 400);
            $this->_helper->json($error->toArray());

        }
        return true;
    }

    /**
     * Função que formata os índices do array de Headers para minúsculo
     * @return array
     */
    private function getHeaders(){
        $headers = getallheaders();
        $arrHeader=[];
        foreach ($headers as $key=> $header) {
            $arrHeader[strtolower($key)] = $header;
        }
        return $arrHeader;
    }
}

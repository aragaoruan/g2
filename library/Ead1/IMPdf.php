<?php

require_once APPLICATION_PATH .'/../library/MPDF/mpdf.php' ;

class Ead1_IMPdf {
    protected $_mpdf;

    public function __construct($options = null){
        $this->_mpdf = new mPDF($options);
        $this->configuraPapel();
    }

    /**
     * Configura o tamanho e orientação do papel
     *
     * O tamanho do papel pode ser 'A4', 'letter' , 'legal'
     * Tipos de orientação:  'P' : 'DEFAULT Portrait'( retrato ) ou 'L' : 'Landscape' ( paisagem )
     *
     * @param string $tamanho
     * @param string $orientacao
     * @return Ead1_MPdf
     */
    public function configuraPapel( $tamanho = 'A4', $orientacao = 'P' ){
        $this->_mpdf->_setPageSize($tamanho, $orientacao);
        return $this;
    }

    /**
     * Carrega uma string HTML
     * @param string $html
     * @return Ead1_Pdf
     */
    public function carregarHTML( $html ) {
        //echo "carregarHTML";
        $this->_mpdf->WriteHTML($html);
        return $this;
    }

    /**
     * Substitui a tag </pagina> gerada pelo componente RichTextEditor para gerção de conteúdos dinâmicos
     * para a tag específica utilizada pela biblioteca DOMPDF
     * para finalizar o término de uma página e ínicio de outra página.
     *
     * @param string $texto
     * @return string
     */
    public  function inserirQuebraPagina( ) {
        $this->_mpdf->addPage();
    }


    /**
     * Gera o PDF com o nome informados.
     * Caso seja informado um caminho de diretório válido será salvo o arquivo
     * ao invés de gerar a opção download.
     * @todo implementar ação de salvar o PDF
     * @param string $nomeArquivo
     * @param string $caminhoArquivo - OPCIONAL -  caminho absoluto para salvar o pdf
     * @param string $destination - Default: 'I' - Pode assumir os seguintes valores:
     *      I: Envia o arquivo ao browser, utiliza o plugin do navegador se disponivel, o nome do arquivo é utilizado quando selecionado o "Salvar Como"
     *      D: Envia o arquivo ao browser e força o download com o nome informado.
     *      F: Salva o arquivo com o nome informado no servidor
     *      S: retorna o documento como uma string, o nome do arquivo é ignorado.
     * @return void
     */
    public function gerarPdf( $nomeArquivo, $caminhoArquivo = false, $destination = 'I' ) {

        if ($caminhoArquivo && !is_dir($caminhoArquivo)) {
            mkdir($caminhoArquivo, 0777, true);
        }

        if (empty($nomeArquivo)) {
            $nomeArquivo = 'default';
        }

        $nomeArquivo = str_replace('.pdf', '', $nomeArquivo) . '.pdf';
        $nomeArquivo = ($caminhoArquivo !== false ? $caminhoArquivo . DIRECTORY_SEPARATOR . $nomeArquivo : $nomeArquivo);

        $this->_mpdf->Output($nomeArquivo, $destination);
        return $nomeArquivo;
    }

    public function ImageBoleto($image, &$html, $nameVariable){

        $this->_mpdf->$nameVariable = file_get_contents($image);
        $html = str_replace('http://loja.ead1.net'.str_replace('loja', '', $image), 'var:'.$nameVariable, $html);

    }

}

<?php
/**
 * Classe de Interface de Cores
  * @author Eduardo Romão - ejushiro@gmail.com
 */
interface Ead1_IColor{
	
	const AZUL = '#0000ff';
	const AZUL_CLARO = '#00bff3';
	const AMARELO = '#ffff00';
	const MARROM = '#603913';
	const VERMELHO = '#ff0000';
	const ROSA = '#ff00ff';
	const VERDE = '#00ff00';
	const VERDE_LIMAO = '#50f300';
	const VERDE_ESCURO = '#005826';
	const PRETO = '#000000';
	const CINZA = '#acacac';
	const BRANCO = '#ffffff';
	const LARANJA = '#f26522';
	const ROXO = '#630460';
	
}
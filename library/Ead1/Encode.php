<?php

/**
 * Classe que tra unifica o processo de encode e decode
 * @author eduardoromao
 */
class Ead1_Encode
{

    const ASCII = 'Ascii';
    const ASCIIU = 'ASCII';
    const UTF8 = 'UTF-8';
    const ISO88591 = 'ISO-8859-1';
    const APPLICATION_GESTOR2 = 'GESTOR';
    const APPLICATION_PORTAL = 'PORTAL_DO_ALUNO';

    /**
     * Método de encode por aplicação
     * @param String $texto
     * @return String $textoCodificado
     */
    public static function applicationEncode($texto)
    {
        $textoCodificado = '';
        switch (APPLICATION_SYS) {
            case self::APPLICATION_GESTOR2:
                $textoCodificado = self::encode($texto, self::UTF8);
                break;
            case self::APPLICATION_PORTAL:
                $textoCodificado = self::encode($texto, self::UTF8);
                break;
            default:
                $textoCodificado = self::encode($texto, self::UTF8);
                break;
        }
        return $textoCodificado;
    }

    /**
     * Método que seta o tipo de encode da string
     * @param String $texto
     * @param String $encode
     * @param Boolean $forceEncode
     * @return String $textoCodificado
     */
    public static function encode($texto, $encode, $forceEncode = false)
    {
        $textoCodificado = '';
        if ($texto) {
            $texto = (string)$texto; //\Zend_Debug::dump(mb_detect_encoding($texto),__CLASS__.'('.__LINE__.')');
            switch (mb_detect_encoding($texto)) {
                case self::UTF8:
                    switch ($encode) {
                        case self::UTF8:
                            switch (PHP_OS) {
                                case 'Darwin':
                                    $textoCodificado = $texto; //Fixed Encoding for MacOS
                                    break;
                                case 'Linux':
                                    $textoCodificado = $texto;
                                    break;
                                case 'WINNT':
                                    $textoCodificado = utf8_encode($texto);
                                    break;
                                default:
                                    $textoCodificado = mb_convert_encoding($texto, $encode, self::UTF8);
                                    break;
                            }
                            break;
                        case self::ASCII:
                            $textoCodificado = mb_convert_encoding($texto, $encode, self::ASCII);
                            break;
                        case self::ISO88591:
                            $textoCodificado = $texto;
                            break;
                        default:
                            $textoCodificado = mb_convert_encoding($texto, $encode, mb_detect_encoding($texto));
                            break;
                    }
                    break;
                case self::ASCII:
                case self::ASCIIU:
                    switch ($encode) {
                        case self::ASCII:
                            $textoCodificado = $texto;
                            break;
                        default:
                            $textoCodificado = mb_convert_encoding($texto, $encode, self::ASCII);
                            break;
                    }
                    break;
                case self::ISO88591:
                    switch ($encode) {
                        case self::ISO88591:
                            $textoCodificado = $texto;
                            break;
                        default:
                            $textoCodificado = utf8_decode($texto);
                            break;
                    }
                    break;
                default:
                    $textoCodificado = utf8_encode($texto);
                    break;
            }
        }

        return $textoCodificado;
    }


}

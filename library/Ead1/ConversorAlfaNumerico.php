<?php
/**
 * Classe que converte Numeros Inteiros Normais para letras - vice-versa
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class Ead1_ConversorAlfaNumerico{
	
	public function __construct(){
		$this->_dataCollection = $this->dataCollection();
	}
	
	private $_dataCollection = array();
	
	/**
	 * Variavel que contem o Valor do Numero Normal
	 * @var int
	 */
	private $normal = 0;
		
	/**
	 * @return the $normal
	 */
	public function getNormal() {
		return $this->normal;
	}
	
	/**
	 * Variavel que contem o Valor da Letra
	 * @var String
	 */
	private $letra = "";

	/**
	 * @return the $letra
	 */
	public function getLetra() {
		return $this->letra;
	}
	
	/**
	 * Método que retorna os numeros de 1 a 26 | a a z
	 * @param Mixed $numero
	 * @return Ead1_ContadorAlfaNumerico
	 */
	public function converteNumero($numero){
		if(is_string($numero)){
			if(!in_array($numero, $this->_dataCollection)){
				$this->normal = 0;
				$this->letra = "";
			}else{
				$this->letra = $numero;
				$this->normal = array_search($numero, $this->_dataCollection);
			}
		}elseif(is_int($numero)){
			if(!array_key_exists($numero, $this->_dataCollection)){
				$this->normal = 0;
				$this->letra = "";
			}else{
				$this->letra = $this->_dataCollection[$numero];
				$this->normal = $numero;
			}
		}else{
			$this->normal = 0;
			$this->letra = "";
		}
		return $this;
	}
	
	/**
	 * Método que subtrai um valor de um numero
	 * @param Mixed $numero
	 * @return Ead1_ContadorAlfaNumerico
	 */
	public function subNumber($numero = 1){
		if(!$this->normal){
			return $this;
		}
		$normal = $this->normal;
		$letra = $this->letra;
		$numero = $this->converteNumero($numero)->normal;
		$valor = ($normal-$numero);
		return $this->converteNumero($valor);
	}
	
	/**
	 * Método que soma um valor de um numero
	 * @param Mixed $numero
	 * @return Ead1_ContadorAlfaNumerico
	 */
	public function sumNumber($numero = 1){
		$normal = $this->normal;
		$letra = $this->letra;
		$numero = $this->converteNumero($numero)->normal;
		$valor = ($normal+$numero);
		if($valor > count($this->_dataCollection)){
			$valor = count($this->_dataCollection);
		}
		return $this->converteNumero($valor);
	}
	
	/**
	 * Método que soma um valor ++
	 * @return Ead1_ContadorAlfaNumerico
	 */
	public function plusNumber(){
		$this->normal++;
		if($this->normal > count($this->_dataCollection)){
			$this->normal = count($this->_dataCollection);
		}
		return $this->converteNumero($this->normal);
	}
	
	/**
	 * Método que seta o numero novamente para 0
	 * @return Ead1_ContadorAlfaNumerico
	 */
	public function resetNumber(){
		$this->normal = 0;
		return $this->converteNumero($this->normal);
	}
	
	/**
	 * Metodo que retorna um array associativo com os numeros e letras
	 * @return $dataCollection
	 */
	private function dataCollection(){
		$dataCollection = array(
			 0 => ""
			,1 => "a"
			,2 => "b"
			,3 => "c"
			,4 => "d"
			,5 => "e"
			,6 => "f"
			,7 => "g"
			,8 => "h"
			,9 => "i"
			,10 => "j"
			,11 => "k"
			,12 => "l"
			,13 => "m"
			,14 => "n"
			,15 => "o"
			,16 => "p"
			,17 => "q"
			,18 => "r"
			,19 => "s"
			,20 => "t"
			,21 => "u"
			,22 => "v"
			,23 => "w"
			,24 => "x"
			,25 => "y"
			,26 => "z"
			);
		return $dataCollection;
	}
}
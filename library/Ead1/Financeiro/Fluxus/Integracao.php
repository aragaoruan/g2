<?php

namespace Ead1\Financeiro\Fluxus;

use Doctrine\Common\Util\Debug;
use G2\Constante\Sistema;

class Integracao {

    /**
     * @var
     */
    protected $integracao;

    /**
     * Essas duas constantes são usadas para diferenciar os 2 cenários possíveis no momento de fazer um cancelamento.
     * o CANCELAMENTO, representa o cenário onde só é necessário cancelar as parcelas.
     * o ACORDO, representa o cenário onde é necessário cancelar as parcelas existentes e criar novas. com o id_acordo
     */
    const CANCELAMENTO = 2;
    const ACORDO = 3;

    public function __construct(){

        $sistema = new \SistemaTO();
        $sistema->setId_sistema(Sistema::FLUXUS_WS);
        $sistema->fetch(false,true,true);

        $this->integracao = $sistema;

    }

    /**
     * @param array $dados
     * @return \Ead1_Mensageiro
     */
    public function integrarParcelaGestorFluxus(array $dados){
        //\Doctrine\Common\Util\Debug::dump($this->integracao->getst_conexao());die;
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/inserirParcela');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());

        if ($arrayReturn['_sxml']['status'] == "true" ){
            return new \Ead1_Mensageiro($arrayReturn['_sxml']['result'], \Ead1_IMensageiro::SUCESSO );
        }else{
            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $dados
     * @return mixed
     */
    public function listarParcelaFluxusG2(array $dados = array()){

        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/listarParcelaG2');
        $httpClient = $client->getHttpClient();
        $httpClient->setConfig(array('timeout' => '120'));

        $client->setHttpClient($httpClient);
        $client->token($this->integracao->getst_chaveacesso())->parametros($dados);
        $client->maxResults($dados['maxResults']);

        $return = $client->post();
        $arrayReturn = \Ead1_Util::objectToArray($return);
        return $arrayReturn['_sxml'];

    }

    /**
     * @param array $dados
     * @return mixed
     */
    public function retornaAcordo(array $dados){
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/retornaAcordo');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());
        return ($arrayReturn['_sxml']);
    }

    /**
     * @param array $dados
     * @return mixed
     */
    public function atualizarFlagFluxus(array $dados){
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/atualizarStatus');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());
        return ($arrayReturn['_sxml']);
    }

    /**
     * @param array $dados
     * @return mixed
     */
    public function baixarParcelaFluxus(array $dados){

        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/baixarParcela');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());
        return ($arrayReturn['_sxml']);

    }

    /**
     * @param array $dados
     */
    public function inserirVenda(array $dados) {
        //try{
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/inserirMovimento');

        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $return = $client->post();
        $arrayReturn = \Ead1_Util::objectToArray($return);

        if ($arrayReturn['_sxml']['status'] == "true" ){
            return new \Ead1_Mensageiro($arrayReturn['_sxml']['result'], \Ead1_IMensageiro::SUCESSO );
        }else{
            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);
        }

        /*}catch (\Exception $message){
            \Zend_Debug::dump($client->getHttpClient()->getLastResponse(),__CLASS__.'('.__LINE__.')');
            \Zend_Debug::dump($message,__CLASS__.'('.__LINE__.')');exit;
        }*/
    }

    /**
     * @param array $dados
     */
    public function inserirVendaLancamento(array $dados) {
        try{
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/inserirMovimentoLancamento');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());

        if ($arrayReturn['_sxml']['status'] == "true" ){
            return new \Ead1_Mensageiro($arrayReturn['_sxml']['result'], \Ead1_IMensageiro::SUCESSO );
        }else{
            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);
        }
        }catch (\Exception $message){
            \Zend_Debug::dump($client->getHttpClient()->getLastResponse(),__CLASS__.'('.__LINE__.')');
            \Zend_Debug::dump($message,__CLASS__.'('.__LINE__.')');exit;
        }
    }

    /**
     * @param array $dados
     */
    public function inserirProdutoVenda(array $dados) {
        //try{
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/inserirItem');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());

        if ($arrayReturn['_sxml']['status'] == "true" ){
            return new \Ead1_Mensageiro($arrayReturn['_sxml']['result'], \Ead1_IMensageiro::SUCESSO );
        }else{
            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);
        }

   /*     }catch (\Exception $message){
            \Zend_Debug::dump($client->getHttpClient()->getLastResponse(),__CLASS__.'('.__LINE__.')');
            \Zend_Debug::dump($message,__CLASS__.'('.__LINE__.')');exit;
        }*/
    }

    public function sincronizaCadastroPessoa(array $dados){
        //try{
        $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/integrarUsuario');
        $client->token($this->integracao->getst_chaveacesso())->parametros(($dados));
        $arrayReturn = \Ead1_Util::objectToArray($client->post());
        if ($arrayReturn['_sxml']['status'] == "true" ){
            return new \Ead1_Mensageiro($arrayReturn['_sxml']['result'], \Ead1_IMensageiro::SUCESSO );
        }else{
            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * @param array $params array(
            'idlan' => valor obrigatorio, pode ser tanto integer quanto array , caso tenha mais de um idlan para poder
     *                  ser informado informa conforme o exemplo. Exemplo idlan => array(1989856, 1989857, 1989858)
     *      'codcoligada' =>  valor obrigatorio
     *      'status' => valor obrigatorio, valores possiveis,
     *                      2 - Cancelamento de parcela,
     *                      3 - baixar parcela por acordo
     *
     *          Params para o acordo | status 3
     *
     *      'valordesconto' => se não informado, é considerado o valor 0.0000
     *      'valoracrescimo' => se não informado, é considerado o valor 0.0000
     *      'taxajuros' => se não informado, é considerado o valor 0.0000
     *      'primeirovencimento' => se não informado é considerado a data corrente
     *                              data do vencimento da primeira parcela do acordo gerado
     *      'qtdparcelas' => se não informado é considerado o valor 1
     *                       quantidade de parcelas geradas pelo acordo
     * )
     * @return \Ead1_Mensageiro
     *      status => true|false
     *      retorno =>
     *              IDLAN
     *              CODCOLIGADAO
     *              IDACORDO => caso o status informado for 3 esse valor virá preenchido
     *              DATACANCELAMENTO => caso o status informado for 3 esse valor virá preenchido
     */
    public function processoCancelamento(array $params) {

        try {

            $client = new \Zend_Rest_Client($this->integracao->getst_conexao().'/index.php/processoCancelamento');

            $client->token($this->integracao->getst_chaveacesso())->parametros($params);
            $arrayReturn = \Ead1_Util::objectToArray($client->post());

            if ($arrayReturn['_sxml']['status'] == "true" ) {
                return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::SUCESSO );
            }

            return new \Ead1_Mensageiro($arrayReturn['_sxml'], \Ead1_IMensageiro::ERRO);

        } catch (\Exception $message) {
            return new \Ead1_Mensageiro(array(
                'response' => $client->getHttpClient()->getLastResponse(),
                'message' => $message->getMessage()
            ), \Ead1_IMensageiro::ERRO);
        }
    }
}


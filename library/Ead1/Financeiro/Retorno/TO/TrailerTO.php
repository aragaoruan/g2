<?php

/**
 * Classe que padroniza a nomeclatura de retorno do Trailer do Arquivo
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class Ead1_Financeiro_Retorno_TO_TrailerTO extends Ead1_TO_Dinamico {

	public $st_banco;
	public $st_nomebanco;
	public $st_agencia;
	public $st_digitoagencia;
	public $st_conta;
	public $st_digitoconta;
	public $dt_arquivo;
	public $st_padraoarquivo;
	
	/**
	 * @return the $st_padraoarquivo
	 */
	public function getSt_padraoarquivo() {
		return $this->st_padraoarquivo;
	}

	/**
	 * @param field_type $st_padraoarquivo
	 */
	public function setSt_padraoarquivo($st_padraoarquivo) {
		$this->st_padraoarquivo = $st_padraoarquivo;
	}

	/**
	 * @return the $st_banco
	 */
	public function getSt_banco() {
		return $this->st_banco;
	}

	/**
	 * @return the $st_nomebanco
	 */
	public function getSt_nomebanco() {
		return $this->st_nomebanco;
	}

	/**
	 * @return the $st_agencia
	 */
	public function getSt_agencia() {
		return $this->st_agencia;
	}

	/**
	 * @return the $st_digitoagencia
	 */
	public function getSt_digitoagencia() {
		return $this->st_digitoagencia;
	}

	/**
	 * @return the $st_conta
	 */
	public function getSt_conta() {
		return $this->st_conta;
	}

	/**
	 * @return the $st_digitoconta
	 */
	public function getSt_digitoconta() {
		return $this->st_digitoconta;
	}

	/**
	 * @return the $dt_arquivo
	 */
	public function getDt_arquivo() {
		return $this->dt_arquivo;
	}

	/**
	 * @param field_type $st_banco
	 */
	public function setSt_banco($st_banco) {
		$this->st_banco = $st_banco;
	}

	/**
	 * @param field_type $st_nomebanco
	 */
	public function setSt_nomebanco($st_nomebanco) {
		$this->st_nomebanco = trim($st_nomebanco);
	}

	/**
	 * @param field_type $st_agencia
	 */
	public function setSt_agencia($st_agencia) {
		$this->st_agencia = $st_agencia;
	}

	/**
	 * @param field_type $st_digitoagencia
	 */
	public function setSt_digitoagencia($st_digitoagencia) {
		$this->st_digitoagencia = $st_digitoagencia;
	}

	/**
	 * @param field_type $st_conta
	 */
	public function setSt_conta($st_conta) {
		$this->st_conta = $st_conta;
	}

	/**
	 * @param field_type $st_digitoconta
	 */
	public function setSt_digitoconta($st_digitoconta) {
		$this->st_digitoconta = $st_digitoconta;
	}

	/**
	 * @param field_type $dt_arquivo
	 */
	public function setDt_arquivo($dt_arquivo) {
		$this->dt_arquivo = $dt_arquivo;
	}

	
	
}

?>
<?php

/**
 * Classe que padroniza a nomeclatura de retorno do Detalhe do Arquivo
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class Ead1_Financeiro_Retorno_TO_DetalheTO extends Ead1_TO_Dinamico {

	public $st_nossonumero;
	public $nu_valor;
	public $dt_ocorrencia;
    public $nu_juros;
    public $nu_descontos;
    public $st_carteira;
    public $nu_tarifa;
    public $st_documento;
    public $dt_quitado;
    public $nu_valornominal;

    /**
     * @param mixed $st_documento
     */
    public function setSt_documento($st_documento)
    {
        $this->st_documento = $st_documento;
    }

    /**
     * @return mixed
     */
    public function getSt_documento()
    {
        return $this->st_documento;
    }

    /**
     * @param mixed $nu_tarifa
     */
    public function setNu_tarifa($nu_tarifa)
    {
        $this->nu_tarifa = $nu_tarifa;
    }

    /**
     * @return mixed
     */
    public function getNu_tarifa()
    {
        return $this->nu_tarifa;
    }

    /**
     * @param mixed $st_carteira
     */
    public function setSt_carteira($st_carteira)
    {
        $this->st_carteira = $st_carteira;
    }

    /**
     * @return mixed
     */
    public function getSt_carteira()
    {
        return $this->st_carteira;
    }


    /**
     * @param mixed $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return mixed
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @param mixed $nu_descontos
     */
    public function setNu_descontos($nu_descontos)
    {
        $this->nu_descontos = $nu_descontos;
    }

    /**
     * @return mixed
     */
    public function getNu_descontos()
    {
        return $this->nu_descontos;
    }
	

	/**
	 * @return the $st_nossonumero
	 */
	public function getSt_nossonumero() {
		return $this->st_nossonumero;
	}

	/**
	 * @return the $nu_valor
	 */
	public function getNu_valor() {
		return $this->nu_valor;
	}

	/**
	 * @return the $dt_ocorrencia
	 */
	public function getDt_ocorrencia() {
		return $this->dt_ocorrencia;
	}



	/**
	 * @param field_type $st_nossonumero
	 */
	public function setSt_nossonumero($st_nossonumero) {
		$this->st_nossonumero = trim($st_nossonumero);
	}

	/**
	 * @param field_type $nu_valor
	 */
	public function setNu_valor($nu_valor) {
		$this->nu_valor = $nu_valor;
	}

	/**
	 * @param field_type $dt_ocorrencia
	 */
	public function setDt_ocorrencia($dt_ocorrencia) {
		$this->dt_ocorrencia = $dt_ocorrencia;
	}

    /**
     * @param mixed $dt_quitado
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @return mixed
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param mixed $nu_valornominal
     */
    public function setNu_valornominal($nu_valornominal)
    {
        $this->nu_valornominal = $nu_valornominal;
    }

    /**
     * @return mixed
     */
    public function getNu_valornominal()
    {
        return $this->nu_valornominal;
    }

	
}

?>
<?php

// require_once("Ead1_Financeiro_Retorno_CNAB240.php");
// require_once("Ead1_Financeiro_Retorno_CNAB400_Conv6.php");
// require_once("Ead1_Financeiro_Retorno_CNAB400_Conv7.php");
// require_once("Ead1_Financeiro_Retorno_CNAB400_Bradesco.php");

/**Classe que identifica o tipo de arquivo de retorno sendo carregado e instancia a classe
* específica para leitura do mesmo.
* @copyright GPLv2
* @package ArquivoRetornoTitulosBancarios
* @author Manoel Campos da Silva Filho. http://manoelcampos.com/contato
* @version 0.4
*/
class Ead1_Financeiro_Retorno_Factory  {
  /**Instancia um objeto de uma das sub-classes de Ead1_Financeiro_Retorno_Base,
  * com base no tipo do arquivo de retorno indicado por $fileName
  * @param string fileName Nome do arquivo de retorno a ser identificado
  * para poder instancia a classe específica para leitura do mesmo.
  * @param string $aoProcessarLinhaFunctionName @see Ead1_Financeiro_Retorno_Base
  * @return Ead1_Financeiro_Retorno_Base Retorna um objeto de uma das sub-classes de Ead1_Financeiro_Retorno_Base.
  */
  static function getRetorno($fileName, $aoProcessarLinhaFunctionName = null) {
    if($fileName == "")
      throw new Exception("Informe o nome do arquivo de retorno.");
      
    $arq = fopen($fileName, "r");
    if($arq) {
       //Lê o header do arquivo
       $linha=fgets($arq, 500);
       if($linha) {
          $len = strlen($linha);
          if($len >= 240 and $len <= 242)
             return new Ead1_Financeiro_Retorno_CNAB240($fileName, $aoProcessarLinhaFunctionName);
          else if($len >= 400 and $len <= 402) {
             if(strstr($linha, "BRADESCO"))
               return new Ead1_Financeiro_Retorno_CNAB400_Bradesco($fileName, $aoProcessarLinhaFunctionName);
          
             //Lê o primeiro registro detalhe
             $linha=fgets($arq, 500);
       		 if($linha) {
                switch ($linha[0]) {
                  case Ead1_Financeiro_Retorno_CNAB400_Conv6::DETALHE:
                    return new Ead1_Financeiro_Retorno_CNAB400_Conv6($fileName, $aoProcessarLinhaFunctionName);
                  break;
                  case Ead1_Financeiro_Retorno_CNAB400_Conv7::DETALHE:
                    return new Ead1_Financeiro_Retorno_CNAB400_Conv7($fileName, $aoProcessarLinhaFunctionName);
                  break;
                  default:
                    throw new Exception("Tipo de registro detalhe desconhecido: " . $linha[0]);   
                  break;
                }
             }
             else throw new Exception("Tipo de arquivo de retorno não identificado. Não foi possível ler um registro detalhe.");
          }
          else throw new Exception("Tipo de arquivo de retorno não identificado. Total de colunas do header: $len");
       } 
       else throw new Exception("Tipo de arquivo de retorno não identificado. Não foi possível ler o header do arquivo.");
       
       fclose($arq);
    }
    else throw new Exception("Não foi possível abrir o arquivo \"$fileName\".");
  }
}
?>

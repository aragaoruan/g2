<?php

class Ead1_LabelFuncionalidade
{

    public static function getLabel ($id_funcionaldidade)
    {
        $sessao = new Zend_Session_Namespace('labelFuncionalidade');
        $funcionalidade = $sessao->funcionalidades[$id_funcionaldidade];

        $labelFuncionalidade = "";
        if (isset($funcionalidade)) {
            if (isset($funcionalidade['label'])) {
                $mensageiro = new Ead1_Mensageiro();
                if ($mensageiro->getSu()) {
                    $labelFuncionalidade = $funcionalidade['label'] . "({$funcionalidade['funcionalidade']})";
                } else {
                    $labelFuncionalidade = $funcionalidade['label'];
                }
            } else {
                $labelFuncionalidade = $funcionalidade['funcionalidade'];
            }
        } else {
            $labelFuncionalidade = "ERRO LABEL $id_funcionaldidade";
        }
        return utf8_encode($labelFuncionalidade);
    }

}


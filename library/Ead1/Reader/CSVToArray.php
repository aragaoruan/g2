<?php

/**
 * Classe para carregar arquivos CSV
 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
 * @package package_name
 *
 */
class Ead1_Reader_CSVToArray {
	

	private $_data;
	private $_tempFile;
	
	
	public function __construct($tempFile) {
		$this->converteArquivo($tempFile);
	}
	
	/**
	 * Converte o arquivo em Array
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param file $tempFile
	 * @return array
	 */
	private function converteArquivo($tempFile){
		
		$this->_tempFile = $tempFile;
		$lines 			= file($tempFile);		
		$this->_data 	= array();
		$raderKey 		= array();
		$x = 0;
		
		foreach ($lines as $line_num => $line) {
			if($line_num==0){
				$raderKey = explode(';', $line);
				continue;
			}
			
			if(!trim($line)){
				continue;	
			}

			$data = explode(';', trim($line));
			if(count($raderKey)!=count($data)){
				$this->_data = null;
				break;
			}
			
			foreach($data as $key => $value){
				if(trim($value)){
					$this->_data[$x][trim($raderKey[$key])] = trim($value);
				}
			}
			$x++;
		}
		
	}
	

	/**
	 * Retorna os dados do arquivo
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @return Ead1_Mensageiro
	 */
	public function getData(){
		
		if($this->_data){
			return new Ead1_Mensageiro($this->_data, Ead1_IMensageiro::SUCESSO);
		} else {
			return new Ead1_Mensageiro('Erro no arquivo de Importação.', Ead1_IMensageiro::ERRO);
		}
		
	}
	

}
<?php
/**
 * Estratégia de leitura para arquivos com extensão CSV
 * @author Arthur Cláudio de Almeida Pereira
 * @todo refatorar para melhor atender. 
 */
class Ead1_Reader_CSVStrategy2 implements Iterator {
	
	private $_handler;
	private $_index;
	private $_data 	;
	private $_isValid 	= true;
	private $_cabecalho;
	private $_tempFile;
	
	
	public function __construct( $tempFile ) {
		$this->_tempFile = $tempFile; 
	}
	
	public function current()
    {
    	if( is_null( $this->_data ) || $this->_data === 0 ){
    		$this->next();
    	}
        return $this->_data;
    }

    public function key()
    {
        return $this->_index;
    }

    public function next()
    {
    	$linha			= fgetcsv( $this->_handler, 0, ';' );
		$this->_isValid = ( bool ) $linha;
		if(is_array($linha))
		{
			$this->_data 	=  array_combine( $this->_cabecalho, $linha );
		}
		unset($linha);
    }

    //TODO ver como resetar um RESOURCE
    public function rewind()
    {
    	$this->_handler 	= fopen( $this->_tempFile, 'r' );
    	$this->_cabecalho 	= fgetcsv( $this->_handler, 0, ';' );
    }

    public function valid()
    {
        return $this->_isValid; 
    }
}
<?php
class Ead1_Reader_CSVStrategy {

	public function __construct( $tempFile ) {
		$this->_readFile( $tempFile );
	}
	
	private function _readFile( $tempFile ) {
		$handler	= fopen( $tempFile, 'r' );
		$cabecalho	= fgetcsv( $handler, 0, ';' );
		$data		= array();
		while( $linha	= fgetcsv( $handler, 0, ';' ) ){
			if( array_sum($linha) !== 0 ){
				$data[] =  array_combine( $cabecalho, $linha );
			}
		}
		$this->_data	= $data;		
	}
	
	public function getData() {
		return $this->_data;
	}
}
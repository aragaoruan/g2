<?php
/**
 * Estratégia de leitura para arquivos com extensão XLS
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeidapereira@gmail.com>
 *
 */
class Ead1_Reader_XLSStrategy extends ArrayObject {
	
	public function __construct( $filePath ) {
		$excelReader	= new Ead1_Reader_Excel( $filePath );
		$arrTOs 		= array();
		$arrTOs 		= $excelReader->getArrayData();
		parent::__construct( $arrTOs );
	}
}
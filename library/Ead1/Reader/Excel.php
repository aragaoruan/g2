<?php
require_once APPLICATION_PATH .'/../library/Ead1/Reader/lib/excelreader.php';
/**
 * Classe para Leitura de Arquivos xls
 * @since 15/08/2011 
 * @author Eduardo RomÃ£o - ejushiro@gmail.com
 */
class Ead1_Reader_Excel {
	
	/**
	 * Contem a Instancia do leitor de Xls
	 * @var Spreadsheet_Excel_Reader
	 */
	private $_reader;
	
	/**
	 * Metodo Construtor
	 */
	public function __construct( $arquivo ){
		if( file_exists($arquivo) === false || is_readable($arquivo) === false ){
			THROW new Zend_Exception('Arquivo nÃ£o encontrado no caminho: '.$arquivo);
		}
		
		$this->_reader = new Spreadsheet_Excel_Reader( $arquivo );
	}
	
	/**
	 * Metodo que encapsula os dados do XLS em um TO
	 * @param Ead1_TO_Dinamico $to
	 * @return array $arrTOs
	 */
	public function encapsularDados( Ead1_TO_Dinamico $to ){
		$className 		= get_class( $to );
		$totalColumns	= $this->_reader->colcount();
		$totalRows		= $this->_reader->rowcount();
		$arrXLSParams 	= array();
		for($posicaoColuna = 1; $posicaoColuna <= $totalColumns; $posicaoColuna++){
			$nomeColuna	= $this->_reader->val( 1, $posicaoColuna );
			if(!empty( $nomeColuna )){
				$arrXLSParams[$posicaoColuna] =	 $nomeColuna;
			}
		}
		$this->validaPars( $to, $arrXLSParams );
		$arrTOs = array();
		for($row = 2; $row <= $totalRows; $row++){
			$cloneTO = new $className;
			foreach( $arrXLSParams as $posicaoColuna => $nomeColuna ){
				$cloneTO->$nomeColuna 	= $this->_reader->val( $row, $posicaoColuna );
			}
			$arrTOs[] = $cloneTO;
		}
		return $arrTOs;
	}
	
	/**
	 * Retorna os dados da planilha em um formato array
	 * @return array $dados
	 */
	public function getArrayData(){
		$totalColumns	= $this->_reader->colcount();
		$totalRows		= $this->_reader->rowcount();
		$cabecalhoXLS 	= array();
		
		for( $posicaoColuna = 1; $posicaoColuna <= $totalColumns; $posicaoColuna++ 	){
			$nomeColuna	= $this->_reader->val( 1, $posicaoColuna );
			if(!empty( $nomeColuna )){
				$cabecalhoXLS[$posicaoColuna] =	 $nomeColuna;
			}
		}
		$dados		= array();
		for( $linha = 2; $linha <= $totalRows; $linha++){
			$dadosLinha	= array();
			foreach( $cabecalhoXLS as $posicaoColuna => $nomeColuna ){
				$dadosLinha[] 	= $this->_reader->val( $linha, $posicaoColuna );
			}
			
			if( array_sum( $dadosLinha ) !== 0 ){
				$dados[] = array_combine( $cabecalhoXLS, $dadosLinha);
			}
		}
		return $dados;
	}
	
	/**
	 * Metodo estatico que valida os parametros do XLS com o do TO
	 * @param Ead1_TO_Dinamico $to
	 * @param array $xlsPars
	 * @throws Zend_Exception
	 * @return Boolean
	 */
	private function validaPars(Ead1_TO_Dinamico $to, $xlsPars){
		$params = $to->toArray();
		foreach ($xlsPars as $par){
			if(!array_key_exists($par, $params)){
				THROW new Zend_Exception('O Parametro: '.$par.' do XLS nÃ£o se encontra no TO!');
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Metodo estatico que retorna a instancia do Objeto
	 * @return Ead1_Reader_Excel
	 */
	public static function getInstance(){
		if(self::$instance === null){
			self::$instance = new Ead1_Reader_Excel();
		}
		return self::$instance;
	}
	
	/**
	 * Metodo estatico que reseta a instancia da classe
	 * @return Ead1_Reader_Excel
	 */
	public static function resetInstance(){
		self::$instance = new Ead1_Reader_Excel();
		return  self::$instance;
	}
	
	/**
	 * Metodo que retorna a classe de leitura de arquivos xls
	 * @return Spreadsheet_Excel_Reader
	 */
	public static function getReader(){
		return self::getInstance()->_reader;
	}
	
	/**
	 * Metodo que le o XLS
	 * @param String $filename | file.xls
	 */
	public static function read($filename){
		if( file_exists($filename) === false || is_readable($filename) === false ){
			THROW new Zend_Exception('Arquivo nÃ£o encontrado no caminho: '.$filename);
		}
		self::getInstance()->_reader->read($filename);
		return self::$instance;
	}
	
	/**
	 * Metodo que retorna o numero de colunas
	 * @param int $sheet
	 * @return int
	 */
	public static function colCount($sheet = 0){
		$reader	= self::getInstance()->_reader;
		Zend_Debug::dump( $reader );
		Zend_Debug::dump( $reader->colcount($sheet) );
		return $reader->colcount($sheet);
	}
	
	/**
	 * Metodo que retorna o numero de linhas
	 * @param int $sheet
	 * @return int
	 */
	public static function rowCount($sheet = 0){
		return self::getInstance()->_reader->rowcount($sheet);
	}
	
	/**
	 * Metodo que retorna um array com os valores de uma coluna
	 * @param Mixed $col | 1 ou "A"
	 * @return array
	 */
	public static function getCol($col){
		return self::getInstance()->_reader->getCol($col);
	}
	
	/**
	 * Metodo que retorna o formato do dado em determinada linha e coluna
	 * @param int $row
	 * @param Mixed $col | 1 ou "A"
	 * @param int $sheet
	 * @return String
	 */
	public static function getFormat($row, $col, $sheet = 0){
		return self::getInstance()->_reader->format($row, $col,$sheet);
	}
	
	/**
	 * Metodo que retorna o indice do formato do dado em determinada linha e coluna
	 * @param int $row
	 * @param Mixed $col | 1 ou "A"
	 * @param int $sheet
	 * @return int
	 */
	public static function getFormatIndex($row, $col, $sheet = 0){
		return self::getInstance()->_reader->formatIndex($row, $col,$sheet);
	}
	
	/**
	 * Retorna um valor de uma linha e coluna que existe ou retorna vazio
	 * @param int $row
	 * @param Mixed $col | 1 ou "A" 
	 * @return Mixed
	 */
	public static function getVal($row,$col,$sheet = 0){
		return self::getInstance()->_reader->val($row, $col, $sheet);
	}
	
	/**
	 * Retorna a data que contem a leitura do XLS
	 */
	public static function getData(){
		return self::getInstance()->_reader->data;
	}
}
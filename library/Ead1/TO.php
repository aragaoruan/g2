<?php

/**
 * SuperClasse abstrata do TO
 * @author eder.mariano
 *
 */
abstract class Ead1_TO
{

    /**
     * Atributo que contém as propriedades para montar o array da DAO
     * @var array
     */
    private $chavesDao;

    public function __autoload($name)
    {
        echo "Want to load $name.\n";
        throw new MissingException("Unable to load $name.");
    }

    /**
     * Método que seta os atributos se existentes na classe instanciada e se for passado um array no construtor
     * Para TOs dinâmicos veja Ead1_TO_Dinamico
     * @see Ead1_TO_Dinamico
     * @param array $dados
     */
    public function __construct(array $dados = null)
    {
        if (!is_null($dados)) {
            $arPropriedadeTO = get_object_vars($this);
            foreach ($dados as $propriedade => $valor) {
                if (array_key_exists($propriedade, $arPropriedadeTO) !== false) {
                    $this->$propriedade = $valor;
                }
            }
        }
    }

    /**
     * Retorna as propriedades públicas do objeto
     * @return $array
     */
    public function toArray()
    {
        $arChaves = get_object_vars($this);
        unset($arChaves['chavesDao']);
        return $arChaves;
    }

    /**
     * Transforma o TO em um array pronto para o Backbone
     * @param boolean $formatDate Set format date para d/m/y h:m:s
     * @return Ambigous <multitype:, string, unknown>
     */
    public function toBackboneArray($formatDate = true)
    {
        $arChaves = get_object_vars($this);
        unset($arChaves['chavesDao']);
        foreach ($arChaves as $chave => $valor) {
            if (!$formatDate) {
                if (substr($chave, 0, 3) == 'dt_') {
                    $arChaves[$chave] = $this->trataDataBancoBR($valor);
                }
            } else {
                if ($valor instanceof Zend_Date) {
                    $arChaves[$chave] = $valor->toString("dd/MM/yyyy HH:mm:ss");
                } elseif ($valor instanceof DateTime) {
                    $arChaves[$chave] = $valor->format("d/m/Y H:i:s");
                } elseif (substr($chave, 0, 3) == 'dt_' && is_string($valor)) {
                    $data = new Zend_Date($valor, Zend_Date::ISO_8601);
                    $arChaves[$chave] = $data->toString("dd/MM/yyyy HH:mm:ss");
                } elseif (is_bool($valor)) {
                    $arChaves[$chave] = ($valor ? 1 : 0);
                } else {
                    switch (strtolower(substr($chave, 0, 3))) {
                        case 'st_':
//                            $arChaves[$chave] = Ead1_Encode::encode($valor, Ead1_Encode::UTF8, TRUE);
                            $arChaves[$chave] = $valor;
                        break;
                    }
                }
            }
        }
        return $arChaves;
    }

    /**
     * Método que retorna as propriedades publicas setadas como array para situações de UPDATE
     * @param bool $aceitaNull parametro para caso queira atualizar algo para NULL
     * @return $array
     */
    public function toArrayUpdate($aceitaNull = false, $chavesNaoRequeridas = null)
    {
        $this->tipaTODAO();
        $arRetorno = array();
        if (is_array($chavesNaoRequeridas)) {
            foreach ($this->toArray() as $atributo => $valor) {
                if (!in_array($atributo, $chavesNaoRequeridas)) {
                    if (isset($valor) || $aceitaNull) {
                        $arRetorno[$atributo] = $valor;
                    }
                }
            }
        } else {
            foreach ($this->toArray() as $atributo => $valor) {
                if (((isset($valor) && $this->isValidAttr($atributo, $valor)) === true) || $aceitaNull) {
                    $arRetorno[$atributo] = $valor;
                }
            }
        }

        if (array_key_exists('dt_cadastro', $arRetorno)) {
            unset($arRetorno['dt_cadastro']);
        }

        return $arRetorno;
    }

    /**
     * Verifica se o valor informado na proprieda é válido
     * @param string $atributo - atributo da classe
     * @param string $valor - valor do atributo da classe
     * @return bool
     */
    public function isValidAttr($atributo, $valor)
    {
        $arDados = explode('_', $atributo);
        $tipo = $arDados[0];
        $retorno = true;
        switch ($tipo) {
            case 'nu':
                if (strtolower($valor) == 'nan') {
                    $retorno = false;
                };
                break;
        }

        return $retorno;
    }

    /**
     * Método que retorna o array de dados necessários para a DAO de acordo com um método que encapsulou através do addChaveDao as propriedades
     *    Para que este método funcione, é necessário que o método chaves esteja implementado no seu TO
     *
     * Outra possibilidade é enviar um array de chaves que você não deseja que seja formada no array da saída.
     *
     * @return Array
     */
    public function toArrayDAO($metodo = null)
    {
        $this->tipaTODAO();
        if (is_array($metodo)) {
            foreach ($this->toArray() as $atributo => $valor) {
                if (!in_array($atributo, $metodo)) {
                    $arAtributos[$atributo] = $valor;
                }
            }
            return $arAtributos;
        } else {
            if ($metodo) {
                $this->$metodo();
            } else {
                return $this->toArray();
            }

            foreach ($this->chavesDao as $propriedade) {
                $arFinal[$propriedade] = $this->$propriedade;
            }
            return $arFinal;
        }
    }

    /**
     * Método que retorna atributos para inserção
     */
    public function toArrayInsert()
    {
        $clone = clone $this;
        $clone->tipaTODAO();
        $arFinal = array();
        $arAtributos = array_keys(get_class_vars(get_class($clone)));

        foreach ($clone->toArray() as $atributo => $valor) {
            if (isset($valor) && $clone->isValidAttr($atributo, $valor)) {
                if (in_array($atributo, $arAtributos)) {
                    $arFinal[$atributo] = $valor;
                }
            }
        }
        if (array_key_exists('dt_cadastro', $arAtributos)) {
            unset($arFinal['dt_cadastro']);
            //    $arFinal['dt_cadastro'] = new Zend_Date();
        }

        return $arFinal;
    }

    /**
     * Método que retorna string do array.
     * @param array $ar
     */
    public function toStringArray($arDados)
    {
        foreach ($arDados as $chave => $valor) {
            if ($valor === null) {
                $valor = "null";
            }
            $str[] = "$chave = $valor";
        }
        return implode(', ', $str);
    }

    /**
     * Método que zera a varíavel chavesDao
     */
    public function limpaChavesDao()
    {
        $this->chavesDao = array();
    }

    /**
     * Método que adciona as chaves no atributo chavesDAO
     * @param string $chave
     */
    public function addChaveDao($chave)
    {
        $this->chavesDao[] = $chave;
    }

    /**
     * Método que retorna a sessão
     * @return Zend_Session_Namespace
     */
    public function getSessao()
    {
        return new Zend_Session_Namespace('geral');
    }

    /**
     * Método que retorna os dados da sessão
     * return array
     */
    public function getValoresSessao()
    {
        $sessao = new Zend_Session_Namespace('geral');
        return $sessao->getIterator();
    }

    /**
     * Método que trata a data do Banco para o formato Brasileiro
     * @param Zend_Date $dataGeral
     * @return String
     */
    public function trataDataBancoBR($dataGeral)
    {
        $data = null;
        if ($dataGeral instanceof DateTime) {
            return $dataGeral->format('Y-m-d H:i:s');
        } else {
            $data = new Zend_Date(new Zend_Date($dataGeral), Zend_Date::ISO_8601);
            return $data->toString('yyyy-MM-dd HH:mm:ss');
        }
    }

    /**
     * Método que converte data string para Zend_Date
     * @param String $data
     * @return Zend_Date
     * @see PortalCAtencaoBO::retornaInteracoesOcorrencia()
     */
    public function trataDataBancoObjeto($data)
    {
        $objDate = '';

        if ($data instanceof DateTime) {
            $data = $data->format('Ymd H:i:s');
        }

        $data = new Zend_Date($data, Zend_Date::ISO_8601);

        return $data;
    }

    /**
     * Converte um objeto DateTime para Zend_Date
     * @param DateTime $datetime
     * @return Zend_Date
     */
    protected function _convertDateTimeToZendDate(DateTime $datetime)
    {
        return new Zend_Date($datetime->format(DateTime::ISO8601), Zend_Date::ISO_8601);
    }

    /**
     * Método que recarrega o TO com os tratamentos dos SETs
     */
    public function reloadTO()
    {
        $atributos = get_object_vars($this);
        $metodosObjeto = get_class_methods(get_class($this));

        foreach ($atributos as $atributo => $valor) {
            $metodo = "set" . ucfirst($atributo);
            if (in_array($metodo, $metodosObjeto) && $this->$atributo) {
                $this->$metodo($this->$atributo);
            }
        }

        return $this;
    }

    /**
     * Metodo Que Tipa o To Para a DAO
     * @author Eduardo Romão - ejushiro@gmail.com
     * @return Ead1_TO
     */
    public function tipaTODAO()
    {
        $pars = $this->toArray();
        if (empty($pars)) {
            return $this;
        }
        foreach ($pars as $chave => $par) {
            $expl = null;
            $ini = null;
            $set = null;
            $get = null;
            $convertedData = null;

            $expl = explode('_', $chave);
            $ini = $expl[0];
            $chaveMetodo = ucwords($chave);
            $get = 'get' . $chaveMetodo;
            $set = 'set' . $chaveMetodo;
            switch ($ini) {
                case 'id' :
                    if ($this->$chave == 0) {
                        $this->$set(null);
                    }
                    break;
                case 'nu' :
                    if (is_numeric($this->$get())) {
                        $this->$set($this->$get());
                        if (is_nan($this->$get())) {
                            $this->$set(null);
                        }
                    } else {
                        $this->$set(null);
                    }
                    break;
                case 'dt' :
                    if (!is_null($this->$get()) && !empty($this->$chave) && $this->$get()) {
                        $convertedData = $this->trataDataBancoBR($this->$get());
                        $this->$set($convertedData);
                    } else {
                        $this->$set(null);
                    }
                    break;
                case 'bl' :
                    if (!is_null($this->$get())) {
                        if ($this->is_boolean($this->$get())) {
                            $this->$set(1);
                        } else {
                            $this->$set(0);
                        }
                                        };
                    break;
                case 'st' :
                    if (!is_null($this->$get())) {
                        $this->$set(Ead1_Encode::encode($this->$get(), Ead1_Encode::ISO88591, TRUE));
                    }
                    break;
            }
            unset($expl);
            unset($ini);
            unset($set);
            unset($get);
            unset($convertedData);
        }
        return $this;
    }

    /**
     * Transforma os atributos do TO no formato necessário para o FLEX
     * @param $debug
     * @param $utf8JaCodificado
     * @param force - por default, força a conversao do campo julgando seu prefixo
     */
    public function tipaTOFlex($debug = false, $utf8JaCodificado = false , $force = true)
    {
        $arPropriedadeClasse = $this->toArray();

        foreach ($arPropriedadeClasse as $nomePropriedadeClasse => $valorPropriedadeClasse) {
            $expl = explode('_', $nomePropriedadeClasse);
            $tipoValor = $expl[0];

            switch ($tipoValor) {
                case 'st' :
                    if (!is_null($valorPropriedadeClasse)) {
                        $this->$nomePropriedadeClasse = ($utf8JaCodificado ? $valorPropriedadeClasse : Ead1_Encode::applicationEncode($valorPropriedadeClasse));
                    }
                    break;
                case 'nu' :
                    $this->$nomePropriedadeClasse = $force ? (float)$valorPropriedadeClasse : $valorPropriedadeClasse;
                    break;
                case 'ob' :
                    $this->$nomePropriedadeClasse = (object)$valorPropriedadeClasse;
                    break;
                case 'id' :
                    $this->$nomePropriedadeClasse = (int)$valorPropriedadeClasse;
                    break;
                case 'dt' :
                    if ($this->$nomePropriedadeClasse == null || $this->$nomePropriedadeClasse == '') {
                        $this->$nomePropriedadeClasse = null;
                    }
                    if (!is_null($this->$nomePropriedadeClasse)) {
                        if (!($this->$nomePropriedadeClasse instanceof DateTime)) {
                            if (strlen($this->$nomePropriedadeClasse) > 20) {
                                $this->$nomePropriedadeClasse = new DateTime(substr($this->$nomePropriedadeClasse, 0, 20));
                                $this->$nomePropriedadeClasse = $this->_convertDateTimeToZendDate($this->$nomePropriedadeClasse);
                            } else {
                                $this->$nomePropriedadeClasse = new Zend_Date($this->$nomePropriedadeClasse);
                            }
                        } else {
                            $this->$nomePropriedadeClasse = $this->_convertDateTimeToZendDate($this->$nomePropriedadeClasse);
                        }

                    }
                    break;
                case 'DATA' :
                    if ($this->$nomePropriedadeClasse == 'null') {
                        $this->$nomePropriedadeClasse = null;
                    }
                    if (!is_null($this->$nomePropriedadeClasse)) {
                        if (!($this->$nomePropriedadeClasse instanceof DateTime)) {
                            if (strlen($this->$nomePropriedadeClasse) > 20) {
                                $this->$nomePropriedadeClasse = new DateTime(substr($this->$nomePropriedadeClasse, 0, 20));
                                $this->$nomePropriedadeClasse = $this->_convertDateTimeToZendDate($this->$nomePropriedadeClasse);
                            } else {
                                $this->$nomePropriedadeClasse = new Zend_Date($this->$nomePropriedadeClasse);
                            }
                        } else {
                            $this->$nomePropriedadeClasse = $this->_convertDateTimeToZendDate($this->$nomePropriedadeClasse);
                        }
                    }
                    break;
                case 'bl' :
                    if (!empty($this->$nomePropriedadeClasse)) {
                        if ($this->is_boolean($this->$nomePropriedadeClasse)) {
                            $this->$nomePropriedadeClasse = true;
                        } else {
                            $this->$nomePropriedadeClasse = false;
                        }
                    } else {
                        $this->$nomePropriedadeClasse = false;
                    }
                    break;
                case 'hr' :
                    if (!empty($this->$nomePropriedadeClasse)) {
                        if (is_string($this->$nomePropriedadeClasse)) {
                            $date = new Zend_Date($this->$nomePropriedadeClasse, Zend_Date::ISO_8601);
                            $this->$nomePropriedadeClasse = $date;
                        }

                        if ($this->$nomePropriedadeClasse instanceof DateTime) {
                            $this->$nomePropriedadeClasse = $this->_convertDateTimeToZendDate($this->$nomePropriedadeClasse);
                        }
                    }
                    break;
                default:
                    continue;
            }
        }
        return $this;
    }

    /**
     * Metodo Que Tipa e limpa o TO para consultas
     * @author Eduardo Romão - ejushiro@gmail.com
     * @return Ead1_TO
     */
    public function tipaTOConsulta($exceptions = array())
    {
        $pars = $this->toArray();
        if (empty($pars)) {
            return $this;
        }
        $exceptions[] = 'bl_ativo';
        $exceptions[] = 'bl_ativa';
        $exceptions[] = 'bl_prioritario';
        $exceptions[] = 'bl_redefinicaosenha';
        $exceptions[] = 'bl_padrao';
        $exceptions[] = 'bl_acessasistema';
         //Chave bl_acessasistema, para corrigir sempre setando valor null

        foreach ($pars as $chave => $par) {

            $expl = null;
            $ini = null;
            $set = null;
            $get = null;
            $convertedData = null;

            $expl = explode('_', $chave);
            $ini = $expl[0];
            $chaveMetodo = ucwords($chave);
            $get = 'get' . $chaveMetodo;
            $set = 'set' . $chaveMetodo;
            switch ($ini) {
                case 'id' :
                    if ($this->$chave == 0) {
                        $this->$set(null);
                    }
                    break;
                case 'dt' :
                    if (!is_null($this->$get()) && !empty($this->$chave)) {
                        $convertedData = $this->trataDataBancoBR($this->$get());
                        $this->$set($convertedData);
                    }
                    break;
                case 'hr' :
                    if (!is_null($this->$get())) {
                        if ($this->$get()) {
                            $convertedData = $this->trataDataBancoBR($this->$get());
                            $this->$set($convertedData);
                        } else {
                            $this->$set(null);
                        }
                    }
                    break;
                case 'bl' :
                    if (!is_null($this->$get())) {
                        if (!in_array($chave, $exceptions)) {
                            $this->$set(null);
                        }
                    }
                    break;
                case 'nu' :
                    if (!is_null($this->$get())) {
                        if ($this->$get() == 0) {
                            $this->$set(null);
                        }
                    }
                    break;
                case 'st' :
                    if ($chave !== "St_classeflex") {
                        $bo = new Ead1_BO();
                        if (!is_null($this->$get())) {
                            $this->$set($bo->substituirCaracteresEspeciais($this->$get()));
                        }
                    }

                    break;
            }
            unset($expl);
            unset($ini);
            unset($set);
            unset($get);
            unset($convertedData);
        }
        return $this;
    }

    /**
     * Retorna se o valor é booleano e verdadeiro
     * Para ser considerado booleano os possíveis valores são:
     * String 'true'
     * String '1'
     * int 1
     * Boolean true
     * @param mixed $bool
     * @return boolean
     */
    public static function is_boolean($bool = false)
    {
        $bool = strtolower(trim($bool));
        return ($bool === 'true' || $bool == 1 || $bool === true) ? true : false;
    }

    /**
     *
     * Monta os dados no formato do IN da pesquisa
     * @param array $retorno Retorno de uma constulta
     * @param string $campo o campo da consulta que deseja colocar no formato do IN
     * @return string retorna os dados no formato ex.: 4,7,9,10
     */
    public function montaDadosIN(Array $retorno, $campo)
    {
        if (!empty($retorno)) {
            foreach ($retorno as $index => $value) {
                $entidade[] = $value[$campo];
            }
            return implode(',', $entidade);
        } else {
            return '';
        }
    }
}

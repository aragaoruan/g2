<?php
/**
 * Classe padrão para retorno de mensagens do arquivo mensagens.ead1
 * @author eder.mariano
 *
 */
class Ead1_TO_Mensagem extends Ead1_TO{
	
	/**
	 * Atributo que contém 
	 * @var string
	 */
	public $codigo;
	
	/**
	 * Atributo que contém o texto da mensagem
	 * @var string
	 */
	public $mensagem;
	
	/**
	 * Método construtor que seta a mensagem e verifica se existe alguma mensagem diferente para a entidade
	 *  atual
	 * @param unknown_type $codigoMensagem
	 */
	public function __construct($codigoMensagem){
		
		$sessaoArquivo = 'geral';
		
		$sessao = new Zend_Session_Namespace('geral');
		
		if(isset($sessao->codentidade)){
			$sessaoArquivo = $sessao->codentidade;
		}
		$config = new Zend_Config_Ini('mensagens.ead1',$sessaoArquivo);
		$this->codigo = $codigoMensagem;
		$this->mensagem = $config->$codigoMensagem;
	}
	
	/**
	 * @return the $codigo
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @param $codigo the $codigo to set
	 */
	public function setCodigo($codigo) {
		$this->codigo = $codigo;
	}

	/**
	 * @return the $mensagem
	 */
	public function getMensagem() {
		return $this->mensagem;
	}

	/**
	 * @param $mensagem the $mensagem to set
	 */
	public function setMensagem($mensagem) {
		$this->mensagem = $mensagem;
	}
}
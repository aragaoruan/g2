<?php

/**
 * Classe para trabalhar com TOs dinamicos
 *    TOs dinamicos são os que os atributos ou métodos não estão definidos, ou seja,
 *        podem ser criados em tempo de execução.
 * @author edermariano
 *
 */
abstract class Ead1_TO_Dinamico extends Ead1_TO
{

    /**
     * Método que possui lógica para criar atributos dinâmicamente
     * @param array $dados
     */
    public function __construct(array $dados = null)
    {
        if ($dados) {
            foreach ($dados as $propriedade => $valor) {
                if (!empty($propriedade)) {
                    $propriedade = trim($propriedade);
                    $this->$propriedade = $valor;
                }
            }
        }
    }


    /**
     * Método que possui lógica para criar atributos dinâmicamente
     * @param array $dados
     */
    public function montaToDinamico(array $dados = null)
    {
        if ($dados) {
            foreach ($dados as $propriedade => $valor) {
                if (!empty($propriedade)) {
                    $propriedade = trim($propriedade);
                    $metodo = 'set' . $propriedade;
                    if (method_exists($this, $metodo)) {
                        $this->$metodo($valor);
                    }
                }
            }
        }
    }

    /**
     * Método que encapsula apenas os dados da instância atual do TO
     *    Se vier um TO de view ou array com todas as propriedades do TO da instância, é possível preencher o TO da instância só com as
     *        propriedade pertinentes!
     * @param mixed $dados
     */
    public function encapsularDadosInstanciaTO($dados)
    {
        $propriedadesTO = array_keys(get_object_vars($this));
        if ($dados instanceof Ead1_TO || is_array($dados)) {
            foreach ($dados as $propriedade => $valor) {
                if (in_array($propriedade, $propriedadesTO)) {
                    $this->$propriedade = $valor;
                }
            }
        }

        return $this;
    }


    /*
     * Método mágico para retornar os valores de atributos que não possuem get/set e utilizam me'todos como tipoTOConsulta
     */
    public function __call($nomeMetodo, $arParametros)
    {

        $tipoMetodo = strtolower(substr($nomeMetodo, 0, 3));
        $nomeAtributo = strtolower(substr($nomeMetodo, 3));
        switch ($tipoMetodo) {
            case 'get' :
                if (isset($this->$nomeAtributo)) {
                    return $this->$nomeAtributo;
                }
                break;
            case 'set' :
                if (isset($this->$nomeAtributo)) {
                    $this->$nomeAtributo = $arParametros[0];
                }
                break;

        }

    }

    /**
     * Método que seta os atributos
     * @param array $dados
     */
    public function setAtributos(array $dados = null)
    {
        $this->__construct($dados);
        return $this;
    }

    /**
     * Método que seta os atributos de um clone do objeto original
     * @param array $dados
     */
    public function setAtributosClone(array $dados = null)
    {
        $clone = clone $this;
        $clone->__construct($dados);
        return $clone;
    }

    /**
     * Método que encapsula um array de dados em um array de objetos dinâmicos
     * @param mixed array|Zend_Db_Table_Rowset $dados array de dados
     * @param Ead1_TO_Dinamico $objetoTo
     * @param $utf8JaCodificado
     * @param force - por default, força a conversao do campo julgando seu prefixo
     * @return array
     */
    public static function encapsularTo($dados, Ead1_TO_Dinamico $objetoTo, $apenasUm = false, $mesmoTO = false, $utf8JaCodificado = false, $force = true)
    {
        if ($dados instanceof Zend_Db_Table_Rowset) {
            $dados = $dados->toArray();
        }
        $retorno = array();
        if (!empty($dados)) {
            foreach ($dados as $objetoAtual) {
                if ($mesmoTO && $apenasUm) {
                    $objetoTo->__construct($objetoAtual);
                    array_push($retorno, $objetoTo->tipaTOFlex(null, $utf8JaCodificado, $force));
                } else {
                    array_push($retorno, $objetoTo->setAtributosClone($objetoAtual)->tipaTOFlex(null, $utf8JaCodificado, $force));
                }
            }
            if ($apenasUm === true) {
                return $retorno[0];
            }
        }
        return $retorno;
    }

    /**
     * Metodo que compara e mescla os TO's
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param Ead1_TO_Dinamico $toPredominante
     * @param Ead1_TO_Dinamico $toSecundario
     * @param boolean $mescla
     * @param boolean $aceitaNull
     * @return Ead1_TO_Dinamico
     */
    public static function mergeTO(Ead1_TO_Dinamico $toPredominante, Ead1_TO_Dinamico $toSecundario, $mescla = false, $aceitaNull = false)
    {
        if ($aceitaNull) {
            $parsSecundario = $toSecundario->toArray();
        } else {
            $parsSecundario = $toSecundario->toArrayUpdate(false);
        }
        $parsPredominante = $toPredominante->toArray();
        if ($mescla) {
            $arrTemp = array();
            foreach ($parsPredominante as $par => $valor) {
                if (array_key_exists($par, $parsSecundario)) {
                    $arrTemp[$par] = $valor ? $valor : $parsSecundario[$par];
                } else {
                    $arrTemp[$par] = $valor;
                }
            }
            foreach ($parsSecundario as $par => $valor) {
                if (array_key_exists($par, $parsPredominante)) {
                    $arrTemp[$par] = $parsPredominante[$par] ? $parsPredominante[$par] : $valor;
                } else {
                    $arrTemp[$par] = $valor;
                }
            }
            $parsPredominante = $arrTemp;
            foreach ($parsPredominante as $par => $valor) {
                $toPredominante->$par = $valor;
            }
            return $toPredominante;
        } else {
            foreach ($parsSecundario as $par => $valor) {
                if (array_key_exists($par, $toPredominante->toArray())) {
                    $toPredominante->$par = $parsPredominante[$par] ? $parsPredominante[$par] : $valor;
                }
            }
            return $toPredominante;
        }
    }

    /**
     * Método que pega o TO e realiza a consulta com o seu ORM correspondente
     * @param bool $soPk realiza a pesquisa pela chave primária
     * @param bool $soUm retorna apenas um registro
     * @param bool $referenciaTOOrifinal seta o TO da instancia com os dados dessa pesquisa
     * @param bool $pesquisa define se essa consulta será normal ou uma consulta de pesquisa
     * @param bool $and define se na consulta de pesquisa será utilizando AND ou OR
     * @param mixed $orderBy define se na consulta haverá uma ordernação
     * @return Ead1_TO_Dinamico|false
     */
    public function fetch($soPk = false, $soUm = false, $referenciaTOOrifinal = false, $pesquisa = false, $and = false, $orderBy = null)
    {
        $classeTO = get_class($this);
        $classeORM = substr($classeTO, 0, -2) . 'ORM';
        if (class_exists($classeORM)) {
            $orm = new $classeORM();
            $where = ($pesquisa ? $orm->montarWhereView($this, $soPk, $and) : $orm->montarWhere($this, $soPk));
            return Ead1_TO_Dinamico::encapsularTo($orm->fetchAll($where, $orderBy), $this, $soUm, $referenciaTOOrifinal);
        } else {
            throw new Zend_Exception('ORM inexistente para consulta! ' . $classeTO);
            return false;
        }
    }
}
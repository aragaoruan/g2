<?php
/**
 * To que deverá ser pai de todos os TOs de pesquisa
 * @author edermariano
 *
 */
class Ead1_TO_Pesquisar extends Ead1_TO_Dinamico{
	
	/**
	 * Atributo que contém a classe flex que será renderizada por exemplo numa situação de edição
	 * @var String
	 */
	public $st_classeflex;
	
	/**
	 * @return the $stClasseFlex
	 */
	public function getSt_classeflex() {
		return $this->st_classeflex;
	}

	/**
	 * @param $stClasseFlex the $stClasseFlex to set
	 */
	public function setSt_classeflex($stClasseFlex) {
		$this->st_classeflex = $stClasseFlex;
	}
	
	/**
	 * Transforma as datas que retornam no formato ZendDate para o formato de string
	 * @param array $dados recebe o array de retorno do método dao
	 * @param boolean $hora por padrão não vem a hora na string
	 * @param boolean $ptBr por padrão já muda a data para o formato PTBR 
	 * @return array com os campos dt no formato de string
	 */
	public static function transformaZendDateToString(array $dados, $hora = false, $ptBr = true){
		foreach($dados as $index => $value){
			foreach($value as $ind => $campos){
				$result = explode("_", $ind);
				if($result[0] == "dt"){
					if(is_object($campos)){
						$arrayDate = get_object_vars($campos);
						$dataHora = explode(" ",$arrayDate['date']);
						$data = $dataHora[0];
						$hora = $dataHora[1];
						if($data == '1900-01-01'){
							$data = NULL;
						}
						if($ptBr === true && $data != NULL){
							$anoMesDia = explode("-",$data);
							$data = $anoMesDia[2]."/".$anoMesDia[1]."/".$anoMesDia[0];
						}
						if($hora === true && $data != NULL){
							$data = $data." ".$hora;
						}
						$dados[$index][$ind] = $data;
					}
				}
			}
		}
		return $dados;
	}
}
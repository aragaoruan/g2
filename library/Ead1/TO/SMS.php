<?php

/**
 * Class EAD1_TO_SMS
 * Classe TO para envio de sms
 */
//class Ead1_TO_SMS extends Ead1_TO_Dinamico
class Ead1_TO_SMS extends Ead1_TO_Dinamico
{
    const SENDSMS = 'sendsms';

    private $login;
    private $senha;
    private $action;
    private $urlApi;
    private $sendTo;
    private $mensagem;

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return EAD1_TO_SMS
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return EAD1_TO_SMS
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMensagem()
    {
        $message = urlencode(htmlspecialchars($this->mensagem));
        return $message;
    }

    /**
     * @param mixed $mensagem
     * @return EAD1_TO_SMS
     */
    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSendTo()
    {
        $number = preg_replace("/^(?=55)(\d){2}(.*)/", '${2}', $this->sendTo);
        return $number;
    }

    /**
     * @param mixed $sendTo
     * @return EAD1_TO_SMS
     */
    public function setSendTo($sendTo)
    {
        $this->sendTo = $sendTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     * @return EAD1_TO_SMS
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlApi()
    {
        return $this->urlApi;
    }

    /**
     * @param mixed $urlApi
     * @return EAD1_TO_SMS
     */
    public function setUrlApi($urlApi)
    {
        $this->urlApi = $urlApi;
        return $this;
    }

}
<?php
/**
 * To que deverá ser pai de todos os TOs de relatórios
 * @author edermariano
 *
 */
class Ead1_TO_Relatorio extends Ead1_TO_Dinamico{
	
	protected $filtros;
	protected $labels;
	protected $dados;
	protected $configuracoes;
	protected $paginacao;
	protected $formato;
	
	/**
	 * @return the $filtros
	 */
	public function getFiltros() {
		return $this->filtros;
	}

	/**
	 * @return the $labels
	 */
	public function getLabels() {
		return $this->labels;
	}

	/**
	 * @return the $dados
	 */
	public function getDados() {
		return $this->dados;
	}

	/**
	 * @return the $configuracoes
	 */
	public function getConfiguracoes() {
		return $this->configuracoes;
	}

	/**
	 * @return the $paginacao
	 */
	public function getPaginacao() {
		return $this->paginacao;
	}

	/**
	 * @return the $formato
	 */
	public function getFormato() {
		return $this->formato;
	}

	/**
	 * @param $filtros the $filtros to set
	 */
	public function setFiltros($filtros) {
		$this->filtros = $filtros;
	}

	/**
	 * @param $labels the $labels to set
	 */
	public function setLabels($labels) {
		$this->labels = $labels;
	}

	/**
	 * @param $dados the $dados to set
	 */
	public function setDados($dados) {
		$this->dados = $dados;
	}

	/**
	 * @param $configuracoes the $configuracoes to set
	 */
	public function setConfiguracoes($configuracoes) {
		$this->configuracoes = $configuracoes;
	}

	/**
	 * @param $paginacao the $paginacao to set
	 */
	public function setPaginacao($paginacao) {
		$this->paginacao = $paginacao;
	}

	/**
	 * @param $formato the $formato to set
	 */
	public function setFormato($formato) {
		$this->formato = $formato;
	}
}
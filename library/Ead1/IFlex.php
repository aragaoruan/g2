<?php
/**
 * Interface com constantes de elementos do Flex
 * @author edermariano
 *
 */
interface Ead1_IFlex{
	const DropDownList 	= 'br.com.ead1.flexlib.controls.DropDownList';
	const CheckBox 		= 'br.com.ead1.flexlib.controls.CheckBox';
	const DateField 	= 'br.com.ead1.flexlib.controls.DateField';
	const TextInput 	= 'br.com.ead1.flexlib.controls.TextInput';
}
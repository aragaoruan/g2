<?php
/**
 * Classe que converte Numeros Inteiros Normais para Numeros Romanos
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class Ead1_ConversorRomano{
	
	public function __construct(){
		$this->dataCollection = $this->dataCollection();
	}
	
	private $dataCollection;

	/**
	 * Variavel que contem o Valor do Numero Normal
	 * @var int
	 */
	private $normal = 0;
		
	/**
	 * @return the $normal
	 */
	public function getNormal() {
		return $this->normal;
	}
	
	/**
	 * Variavel que contem o Valor do Numero Romano
	 * @var String
	 */
	private $romano = 0;

	/**
	 * @return the $romano
	 */
	public function getRomano() {
		return $this->romano;
	}
	
	/**
	 * Método que retorna os numeros de 1 a 100 | I a C
	 * Enter description here ...
	 * @param Mixed $numero
	 * @return Ead1_ContadorRomano
	 */
	public function converteNumero($numero){
		if(is_string($numero)){
			$this->normal = array_search($numero, $this->dataCollection);
			$this->romano = $numero;
		}elseif(is_int($numero)){
			$this->romano = $this->dataCollection[$numero];
			$this->normal = $numero;
		}else{
			$this->normal = 0;
			$this->romano = 0;
		}
		return $this;
	}
	
	/**
	 * Método que subtrai um valor de um numero
	 * @param Mixed $numero
	 * @return Ead1_ContadorRomano
	 */
	public function subNumber($numero = 1){
		if(!$this->normal){
			return $this;
		}
		$normal = $this->normal;
		$romano = $this->romano;
		$numero = $this->converteNumero($numero)->normal;
		$valor = ($normal-$numero);
		return $this->converteNumero($valor);
	}
	
	/**
	 * Método que soma um valor de um numero
	 * @param Mixed $numero
	 * @return Ead1_ContadorRomano
	 */
	public function sumNumber($numero = 1){
		$normal = $this->normal;
		$romano = $this->romano;
		$numero = $this->converteNumero($numero)->normal;
		$valor = ($normal+$numero);
		if($valor > 100){
			$valor = 100;
		}
		return $this->converteNumero($valor);
	}
	
	/**
	 * Método que soma um valor ++
	 * @return Ead1_ContadorRomano
	 */
	public function plusNumber(){
		$this->normal++;
		if($this->normal > 100){
			$this->normal = 100;
		}
		return $this->converteNumero($this->normal);
	}
	
	/**
	 * Método que seta o numero novamente para 0
	 * @return Ead1_ContadorRomano
	 */
	public function resetNumber(){
		$this->normal = 0;
		return $this->converteNumero($this->normal);
	}
	
	/**
	 * Metodo que retorna um array associativo com os numeros romanos
	 * @return $dataCollection
	 */
	private function dataCollection(){
		$dataCollection = array(
			0 =>"0",
			1 =>"I",
			2 =>"II",
			3 =>"III",
			4 =>"IV",
			5 =>"V",
			6 =>"VI",
			7 =>"VII",
			8 =>"VIII",
			9 =>"IX",
			10 =>"X",
			11 =>"XI",
			12 =>"XII",
			13 =>"XIII",
			14 =>"XIV",
			15 =>"XV",
			16 =>"XVI",
			17 =>"XVII",
			18 =>"XVIII",
			19 =>"XIX",
			20 =>"XX",
			21 =>"XXI",
			22 =>"XXII",
			23 =>"XXIII",
			24 =>"XXIV",
			25 =>"XXV",
			26 =>"XXVI",
			27 =>"XXVII",
			28 =>"XXVIII",
			29 =>"XXIX",
			30 =>"XXX",
			31 =>"XXXI",
			32 =>"XXXII",
			33 =>"XXXIII",
			34 =>"XXXIV",
			35 =>"XXXV",
			36 =>"XXXVI",
			37 =>"XXXVII",
			38 =>"XXXVIII",
			39 =>"XXXIX",
			40 =>"XL",
			41 =>"XLI",
			42 =>"XLII",
			43 =>"XLIII",
			44 =>"XLIV",
			45 =>"XLV",
			46 =>"XLVI",
			47 =>"XLVII",
			48 =>"XLVIII",
			49 =>"XLIX",
			50 =>"L",
			51 =>"LI",
			52 =>"LII",
			53 =>"LIII",
			54 =>"LIV",
			55 =>"LV",
			56 =>"LVI",
			57 =>"LVII",
			58 =>"LVIII",
			59 =>"LIX",
			60 =>"LX",
			61 =>"LXI",
			62 =>"LXII",
			63 =>"LXIII",
			64 =>"LXIV",
			65 =>"LXV",
			66 =>"LXVI",
			67 =>"LXVII",
			68 =>"LXVIII",
			69 =>"LXIX",
			70 =>"LXX",
			71 =>"LXXI",
			72 =>"LXXII",
			73 =>"LXXIII",
			74 =>"LXXIV",
			75 =>"LXXV",
			76 =>"LXXVI",
			77 =>"LXXVII",
			78 =>"LXXVIII",
			79 =>"LXXIX",
			80 =>"LXXX",
			81 =>"LXXXI",
			82 =>"LXXXII",
			83 =>"LXXXIII",
			84 =>"LXXXIV",
			85 =>"LXXXV",
			86 =>"LXXXVI",
			87 =>"LXXXVII",
			88 =>"LXXXVIII",
			89 =>"LXXXIX",
			90 =>"XC",
			91 =>"XCI",
			92 =>"XCII",
			93 =>"XCIII",
			94 =>"XCIV",
			95 =>"XCV",
			96 =>"XCVI",
			97 =>"XCVII",
			98 =>"XCVIII",
			99 =>"XCIX",
			100 =>"C");
		return $dataCollection;
	}

}
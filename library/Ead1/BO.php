<?php

/**
 * SuperClasse que possui as funcionalidades do BO
 * @author edermariano
 *
 */
use Ead1\Doctrine\EntitySerializer;

class Ead1_BO
{

    /**
     * Atributo que contém os dados de sessão
     * @var Zend_Session_Namespace
     */
    public $sessao;

    /**
     * Atributo que contém o código da entidade do usuário logado
     * @var int
     */
    private $id_entidade;

    /**
     * Atributo com a instância do objeto Ead1_Mensageiro
     * @var Ead1_Mensageiro
     */
    protected $mensageiro;

    /**
     * @var Doctrine
     */
    private $doctrineContainer;

    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    public $em;

    /**
     * @description Instancia da negocio
     * @var \G2\Negocio\Negocio $negocio
     */
    public $negocio;

    /**
     * Método construtor da classe
     */
    public function __construct()
    {
        $this->negocio = new G2\Negocio\Negocio();

        $this->sessao = new Zend_Session_Namespace('geral');

        $this->mensageiro = new Ead1_Mensageiro();

        if (isset($this->sessao->id_entidade)) {
            $this->id_entidade = $this->sessao->id_entidade;
        }

        try {
            $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->em = $bootstrap->getResource('doctrine')->getEntityManager();
        } catch (Exception $ex) {
            Throw(new Exception("Doctrine não encontrado na inicialização do projeto"));
        }
    }

    /**
     * Método que valida o texto do login e outros
     * @param String $login
     * @return Booelan
     */
    public function validaLoginUsuario($login)
    {
        $validacao = new Zend_Validate_Regex(array('pattern' => '(^[a-zA-Z0-9\._]*$)'));
        return $validacao->isValid($login);
    }

    /**
     * Determine if supplied string is a valid GUID
     *
     * @param string $md5 String to validate
     * @return boolean
     */
    public static function isValidMd5($md5)
    {
        return !empty($md5) && preg_match('/^[a-f0-9]{32}$/', $md5);
    }

    /**
     * Função para verificar se o CPF é válido
     * @param int $cpf
     * @return boolean
     * @deprecated
     * @see \G2\Utils\FormValidator::cpf
     */
    public function validaCPF($cpf)
    {
        if (!is_numeric($cpf)) {
            return false;
        } else {
            if (($cpf == '11111111111') || ($cpf == '22222222222') || ($cpf ==
                    '33333333333') || ($cpf == '44444444444') || ($cpf == '55555555555') ||
                ($cpf == '66666666666') || ($cpf == '77777777777') || ($cpf ==
                    '88888888888') || ($cpf == '99999999999') || ($cpf == '00000000000')
            ) {
                return false;
            } else {
                $dv_informado = substr($cpf, 9, 2);
                for ($i = 0; $i <= 8; $i++) {
                    $digito[$i] = substr($cpf, $i, 1);
                }
                $posicao = 10;
                $soma = 0;
                for ($i = 0; $i <= 8; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[9] = $soma % 11;
                if ($digito[9] < 2) {
                    $digito[9] = 0;
                } else {
                    $digito[9] = 11 - $digito[9];
                }
                $posicao = 11;
                $soma = 0;
                for ($i = 0; $i <= 9; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[10] = $soma % 11;
                if ($digito[10] < 2) {
                    $digito[10] = 0;
                } else {
                    $digito[10] = 11 - $digito[10];
                }
                $dv = $digito[9] * 10 + $digito[10];
                if ($dv != $dv_informado) {
                    return false;
                } else
                    return true;
            }
        }
    }

    /**
     * Metodo que valida CNPJ
     * @param int
     * @return boolean
     * @deprecated
     * @see \G2\Utils\FormValidator::cnpj
     */
    public function validaCNPJ($cnpj)
    {

        $cnpj = $this->somenteNumero($cnpj);
        if (!$cnpj) {
            return false;
        }
        if (strlen($cnpj) != 18) {
            return false;
        }
        $soma1 = ($cnpj[0] * 5) + ($cnpj[1] * 4) + ($cnpj[3] * 3) +
            ($cnpj[4] * 2) + ($cnpj[5] * 9) + ($cnpj[7] * 8) + ($cnpj[8] *
                7) + ($cnpj[9] * 6) + ($cnpj[11] * 5) + ($cnpj[12] * 4) +
            ($cnpj[13] * 3) + ($cnpj[14] * 2);
        $resto = $soma1 % 11;
        $digito1 = $resto < 2 ? 0 : 11 - $resto;
        $soma2 = ($cnpj[0] * 6) + ($cnpj[1] * 5) + ($cnpj[3] * 4) +
            ($cnpj[4] * 3) + ($cnpj[5] * 2) + ($cnpj[7] * 9) + ($cnpj[8] *
                8) + ($cnpj[9] * 7) + ($cnpj[11] * 6) + ($cnpj[12] * 5) +
            ($cnpj[13] * 4) + ($cnpj[14] * 3) + ($cnpj[16] * 2);
        $resto = $soma2 % 11;
        $digito2 = $resto < 2 ? 0 : 11 - $resto;
        return (($cnpj[16] == $digito1) && ($cnpj[17] == $digito2));
    }

    /**
     *
     * @param unknown_type $titulo
     */
    public function validaTituloEleitor($titulo)
    {

        if (strlen(trim($titulo)) < 12) {
            $titulo = "0" . $titulo;
        }
        $ultDigito = strlen($titulo);
        if ($titulo == "000000000000") {
            return false;
        }
        $d1 = settype(substr_replace($titulo, $ultDigito, -11, 1), 'integer');
        $d2 = settype(substr_replace($titulo, $ultDigito, -10, 1), 'integer');
        $d3 = settype(substr_replace($titulo, $ultDigito, -9, 1), 'integer');
        $d4 = settype(substr_replace($titulo, $ultDigito, -8, 1), 'integer');
        $d5 = settype(substr_replace($titulo, $ultDigito, -7, 1), 'integer');
        $d6 = settype(substr_replace($titulo, $ultDigito, -6, 1), 'integer');
        $d7 = settype(substr_replace($titulo, $ultDigito, -5, 1), 'integer');
        $d8 = settype(substr_replace($titulo, $ultDigito, -4, 1), 'integer');
        $d9 = settype(substr_replace($titulo, $ultDigito, -3, 1), 'integer');
        $d10 = settype(substr_replace($titulo, $ultDigito, -2, 1), 'integer');
        $d11 = settype(substr_replace($titulo, $ultDigito, -1, 1), 'integer');
        $d12 = settype(substr_replace($titulo, $ultDigito, 1), 'integer');
        $dv1 = ($d1 * 2) + ($d2 * 3) + ($d3 * 4) + ($d4 * 5) + ($d5 * 6) +
            ($d6 * 7) + ($d7 * 8) + ($d8 * 9);
        $dv1 = $dv1 % 11;
        if ($dv1 == 10) {
            $dv1 = 0;
        }
        $dv2 = ($d9 * 7) + ($d10 * 8) + ($dv1 * 9);
        $dv2 = $dv2 % 11;
        if ($dv2 == 10) {
            $dv2 = 0;
        }
        if (($d11 == $dv1) && ($d12 == $dv2)) {
            if ((($d9 > 0) && ($d10 > 0)) && (($d9 < 29) && ($d10 < 29))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Metodo que valida CEP
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return boolean
     * @deprecated
     * @see \G2\Utils\FormValidator::cep
     */
    public function validaCep($cep)
    {

        //		$cep = trim($cep);
        //		$r = "/^([0-9]{2})\.?([0-9]{3})-?([0-9]{3})$/";
        //		if(!preg_match($r,$cep)){
        //			return false;
        //		}else{
        //			return true;
        //		}
        if (strlen(
                $cep) != 8
        ) {
            return false;
        }
        for ($i = 0; $i < 8; $i++) {
            $digito = ord($cep[$i]);
            if (($digito < 48) || ($digito > 57)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Limpa - e . do CEP
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return int
     */
    public function limpaCep($cep)
    {

        $cep = trim($cep);
        $r = "/^([0-9]{2})\.?([0-9]{3})-?([0-9]{3})$/";
        preg_match($r, $cep, $novoCep);
        $novoCep[0] = '';
        $cepFinal = implode('', $novoCep);
        return $cepFinal;
    }

    /**
     * Método para validar número de telefone
     * @param $numero
     * @return bool
     * @throws Zend_Exception
     * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
     * @deprecated
     * @see \G2\Utils\FormValidator::telefone
     */
    public function validaTelefone($numero)
    {
        $nvalidator = new Zend_Validate_Digits();
        $validator = new Zend_Validate_StringLength(array('max' => 9));
        if($nvalidator->isValid($numero) && $validator->isValid($numero))
        {
            return true;
        }
        return false;
    }

    /**
     * Metodo que Valida Email
     * @param string
     * @return boolean
     * @deprecated
     * @see \G2\Utils\FormValidator::email
     */
    public function validaEmail($email)
    {

        $validate = new Zend_Validate_EmailAddress();
        return $validate->isValid($email);
    }

    /**
     * Método para validar e-mail do usuário
     * @param $email
     * @return bool
     * @throws Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function validaEmailAvancado($email)
    {
        try {
            if ($email) {
                $validator = new Zend_Validate_EmailAddress(array(
                    'allow' => Zend_Validate_Hostname::ALLOW_DNS,
                    'mx' => true,
                    'deep' => true
                ));

                $valid = $validator->isValid($email);
                if (!$valid) {
                    return false;
                } else {
                    return true;
                }

//            } else {
//                return false;
            }
        } catch (Exception $e) {
            throw new Zend_Exception("Erro ao tentar validar e-mail. " . $e->getMessage());
        }
    }

    /**
     * Metodo que Valida Dominio
     * @param string
     * @return bollean
     */
    public function validaDominio($dominio)
    {

        $validate = new Zend_Validate_Hostname();
        return $validate->isValid($dominio);
    }

    /**
     * Função para verificar Data Válida
     * @author Eder Lamar
     * @param String | Zend_Date $data
     * @return boolean
     */
    public function validaData($data)
    {

        $zdata = new Zend_Date($data);
        $validate = new Zend_Validate_Date();
        return $validate->isValid($zdata);
    }

    /**
     * Função para Comparar se a data de termino é menor que a data de inicio
     * Retorna true caso a primeira data seja menor que a segunda e true no caso contrário
     * @author Eder Lamar
     * @param String | Zend_Date $datinicio (data inicial)
     * @param String | Zend_Date $dattermino (data final)
     * @return boolean
     */
    public function comparaData($dataInicial, $dataFinal)
    {
        $zdataInicial = new Zend_Date($dataInicial);
        $zdataFinal = new Zend_Date($dataFinal);
        return $zdataFinal->isLater($zdataInicial);
    }

    /**
     * Método que retorna a diferença em dias das datas
     * @param Zend_Date $dataInicio
     * @param Zend_Date $dataTermino
     * @return int
     */
    public function diferencaDias($dataInicio, $dataTermino)
    {
        $inicio = new Zend_Date($dataInicio, 'YYYY-MM-dd');
        $termino = new Zend_Date($dataTermino, 'YYYY-MM-dd');
        return ceil(($termino->sub($inicio)->toValue()) / 86400);
    }

// 	/**
// 	 * Metodo que compara se a $horaFim é maior que a $horaInicio.
// 	 * @author Eduardo Romão - ejushiro@gmail.com
// 	 * @param string $horaInicio
// 	 * @param string $horaFim
// 	 * @return boolean
// 	 *
// 	 * Adaptado para limitar os períodos também a 4h no máximo
// 	 */
// 	public function comparaHora( $horaInicio, $horaFim ){
// 		$horaInicio = str_replace( ':', '', $horaInicio );
// 		$horaFim = str_replace( ':', '', $horaFim );
// 		if( $horaInicio > $horaFim ){
// 			return false;
// 		}
// 		return true;
// 	}

    /**
     * Método para comparar as horas de forma correta
     * @author: Paulo Silva <paulo.silva@unyleya.com.br>
     * @param string $horaInicio
     * @param string $horaFim
     * @return Integer|Boolean
     */
    public function comparaHora($horaInicio, $horaFim)
    {

        $timeInicio = strtotime($horaInicio);
        $timeFinal = strtotime($horaFim);

        $diferenca = ($timeFinal - $timeInicio);

        if ($diferenca < 0) {
            return -1;
        } elseif (($diferenca / 3600) > 4) {
            return -2;
        }
        return true;
    }

    /**
     * Função para Converter Data para o Formato do Banco
     * Time - serve para indicar se uma data deve ser convertida ate o limite de hoje 23:59
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string $data
     * @param boolean $time
     * @return string $dataBanco
     */
    public function converteDataBanco($data, $time = false, $withTime = false)
    {

        if ($data) {
            $data = new Zend_Date($data);
            $data = $data->toString('y-M-d');
            if ($withTime) {
                if (!$time) {
                    $data .= ' 00:00';
                } else {
                    $data .= ' 23:59';
                }
            }
        }
        return $data;
    }

    /**
     * Função para Converter Data do banco para Data do Cliente
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string $data
     * @return string $dataCliente
     */
    public function converteDataCliente($data)
    {
        $date = new Zend_Date($data);
        return $date->toString('dd/MM/Y');
    }

    /**
     * Metodo que formata um valor para um formato
     * @param Number $valor
     * @param String $format
     * @return String
     */
    public static function formataValor($valor, $format = 'R$ #,##0.00')
    {
        return Zend_Locale_Format::toNumber($valor, array('number_format' => $format));
    }

    /**
     * Exibe o número por extenso
     * @param unknown_type $valor
     * @param unknown_type $format
     * @return string
     */
    public function formataValorExtenso($valor)
    {

        $valor = number_format($valor, 2, '', '');
        if ($valor > 0) {
            return ucfirst(Ead1_Financeiro_GExtenso::moeda($valor));
        } else {
            return 'Zero real';
        }
    }

    /**
     * Metodo que retira a formatação
     * @param Number $valor
     * @return int
     */
    public function desFormataValor($valor)
    {

        $valor = str_replace('R$ ', '', $valor);
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '', $valor);

        return $valor;
    }

    /**
     * Executa o ceil com casas decimais.
     * @param float $value
     * @param int $precision
     * @return number
     */
    public static function ceiling($value, $precision = 0)
    {
        return ceil($value * pow(10, $precision)) / pow(10, $precision);
    }

    /**
     * Método que transforma um numero em Float
     * @param Mixed $valor
     * @param int $precisao
     * @return float
     */
    public function toFloat($valor, $precisao = 2)
    {
        $locale = new Zend_Locale('de_AT');
        return Zend_Locale_Format::toNumber($valor, array('precision' => $precisao,
                'locale' => $locale)
        );
    }

    /**
     * Metodo que converte um valor para o formato de Porcentagem
     * @param Number $valor
     * @return String
     */
    public function formataValorPorcentagem($valor)
    {
        return $this->formataValor($valor, '#,##% 0.00%');
    }

    /**
     * Função para Converter valor para o tipo money
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param int $moeda
     * @return int $moeda
     */
    public function converteMoeda($moeda)
    {

        if (strpos($moeda, '.')) {
            $moeda = str_replace('.', '', $moeda);
        }
        if (!strpos($moeda, ',')) {
            $moeda .= ',00';
        }
        return $moeda;
    }

    /**
     * Seta o link do boleto.
     * @author João Gabriel - jgvasconcelos16@gmail.com
     * @param int $idLancamento
     * @return string
     */
    public function setaLink($idLancamento)
    {
        $url = Ead1_Ambiente::geral()->applicationUrl . '/Pagamento/boleto/' . $idLancamento;
        return '<a href="' . $url . '"> Enviar Boleto </a>';
    }

    /**
     * Valida Senha
     * Minimo de Caracteres 6
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return boolean
     */
    public function validaSenha($senha)
    {

        if (strlen(trim($senha)) < 6) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Valida Login
     * Minimo de Caracteres 6
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return boolean
     */
    public function validaLogin($login)
    {

        if (strlen(trim($login)) < 6) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Valida Nome Completo
     * Minimo de Caracteres 6
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return boolean
     * @deprecated
     * @see \G2\Utils\FormValidator::nomeSobrenome
     */
    public function validaNomeCompleto($nomcompleto)
    {

        if (strlen(trim($nomcompleto)) < 6) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Valida número`
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param $int
     * @return boolean
     */
    public function validaNumero($numero, $maxLen = false)
    {
        if (!is_numeric($numero)) {
            return false;
        }

        if ($maxLen && strlen($numero) > $maxLen) {
            return false;
        }

        return true;
    }

    /**
     * Deixa somente numeros
     * @author Eduardo Romão - ejushiro@gmail.com
     * @param string
     * @return int
     */
    public function somenteNumero($numero)
    {

        $filtro = new Zend_Filter_Digits();
        $novoNumero = $filtro->filter($numero);
        return $novoNumero;
    }

    /**
     * @author Lucky Spadille <spadille.lucky@gmail.com>
     * website: http://my.opera.com/spadille/blog/
     * @param string $dir_upload directory path, where store file upload
     * @param string $option override|rename
     * return array An array store file name
     */
    public function uploadImagem($dir_upload, $option = 'override')
    {

        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->setDestination($dir_upload);
        $files = $upload->getFileInfo();
        $arrFileName = array();
        $countFileUpload = count($files);
        if ($countFileUpload > 0) {
            $i = 1;
            foreach ($files as $file => $info) {
                if ($info['name'] != '') {
                    $validator = new Zend_Validate_File_Exists($dir_upload);
                    if ($option == 'rename') {
                        if ($validator->isValid($info['name'])) {
                            $file_name = $upload->getFileName(
                                $info['name']);
                            preg_match("/\.([^\.]+)$/", $file_name, $matches);
                            $file_ext = $matches[1];
                            if ($countFileUpload > 1) {
                                $file_name = time() . '_' . $i . '.' . $file_ext;
                            } else {
                                $file_name = time() . '.' . $file_ext;
                            }
                            $arrFileName[$file] = $file_name;
                            $upload->addFilter('Rename', $dir_upload . '/' . $file_name);
                            $i++;
                        }
                    } else {
                        $arrFileName[$file] = $info['name'];
                    }
                    $upload->receive($file);
                }
            }
        }
        return $arrFileName;
    }

    /**
     * Metodo que gera letras e numeros aleatoriamente
     * @param int $qtdCaracteres
     * @param boolean $semLetras
     * @param boolean $semNumeros
     * @return string
     */
    public function geradorHash($qtdCaracteres, $semLetras = false, $semNumeros = false)
    {

        $str = 'abcdefghijklmnopqrstuvwxyz';
        $numeros = '0123456789';
        $hash = '';
        if (!$semNumeros) {
            $hash .= $numeros;
        }
        if (!$semLetras) {
            $hash .= $str;
        }
        $variavel = '';
        $tamanhoHash = strlen($hash) - 1;

        for ($i = 0; $i < $qtdCaracteres; $i++) {
            $variavel .= $hash[mt_rand(0, $tamanhoHash)];
        }
        return $variavel;
    }

    /**
     * Metodo que retorna Endereço, Estado, Cidade e Bairro pelo CEP
     * @param EnderecoTO $eTO
     * @return Ead1_Mensageiro
     */
    public function cepService(EnderecoTO $eTO)
    {

        $cep = $eTO->getSt_cep();
        $cep = $this->limpaCep($cep);
        if (!$this->validaCep($cep)) {
            return new Ead1_Mensageiro('CEP Inválido!', Ead1_IMensageiro::AVISO);
        }
        $webservice_url = 'http://webservice.kinghost.net/web_cep.php';
        $webservice_query = array(
            'auth' => '3122116928bb5f19cdead9865faef878', //Chave de autenticação do WebService - Consultar seu painel de controle
            'formato' => 'query_string', //Valores possíveis: xml, query_string ou javascript
            'cep' => $cep); //CEP que será pesquisado
        //Forma URL
        $webservice_url .= '?';
        foreach ($webservice_query as $get_key => $get_value) {
            $webservice_url .= $get_key . '=' . urlencode($get_value) . '&';
        }
        parse_str(file_get_contents($webservice_url), $resultado);
        switch ($resultado['resultado']) {
            case 2 :
                $eTO->setSg_uf($resultado['uf']);
                $eTO->setSt_cidade(
                    mb_strtoupper(utf8_encode($resultado['cidade']), "UTF-8"));
                break;
            case 1 :
                $eTO->setSg_uf($resultado['uf']);
                $eTO->setSt_cidade(
                    mb_strtoupper(utf8_encode($resultado['cidade']), "UTF-8"));
                $eTO->setSt_bairro(utf8_encode($resultado['bairro']));
                $eTO->setSt_endereco(
                    utf8_encode($resultado['tipo_logradouro']) . " " . utf8_encode(
                        $resultado['logradouro']));
                break;
            default :
                return new Ead1_Mensageiro('Nenhum CEP Encontrado!', Ead1_IMensageiro::AVISO);
                break;
        }
        return new Ead1_Mensageiro($eTO, Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Método que transforma o retorno da DAO, em um array com label e value, que hoje é utilizado nos métodos de pesquisa
     * @param Zend_Db_Table_Rowset_Abstract | Array $retorno Retorno da DAO o objeto
     * @param String $label contém o label do campo ex.: st_nomepessoa
     * @param String $valor contém o value do campo ex.: id_usuario
     * @author Dimas Sulz <dimassulz@gmail.com>
     * @return Array
     */
    public function retornarCombosPesquisa($retorno, $label, $valor)
    {

        $arr = array();
        if (!is_array($retorno)) {
            $retorno = $retorno->toArray();
        }
        foreach ($retorno as $index => $values) {
            $arr[$index]['label'] = $values[$label];
            $arr[$index]['valor'] = $values[$valor];
        }
        return $arr;
    }

    /**
     * Método que trata variáveis através de métodos de tratamento
     *
     * @param mixed $variavel
     * @param string $metodoTratamento
     * @return mixed
     */
    public function trataVariavel($variavel, $metodoTratamento = null)
    {

        if ($metodoTratamento) {
            if (in_array($metodoTratamento, get_class_methods(get_class($this)))) {
                $dado = $this->$metodoTratamento($variavel);
            } else {
                throw new Zend_Exception(
                    "Método $metodoTratamento, não encontrado!");
                $dado = $variavel;
            }
        } else {
            $dado = $variavel;
        }
        return $dado;
    }

    /**
     * Método que verifica os campos obrigatórios
     * @param Ead1_TO $to
     * @param array $arCamposNaoObrigatorios
     * @throws Zend_Validate_Exception
     */
    public function verificaCamposObrigatorios(Ead1_TO_Dinamico $to, array $arCamposNaoObrigatorios = array())
    {
        foreach ($to->toArray() as $campo => $valor) {
            if (!in_array($campo, $arCamposNaoObrigatorios) && empty($valor)) {
                throw new Zend_Exception("Por favor preencha o campo: " . $campo);
            }
        }
    }

    /**
     * Metodo que muda a string para o formato de CPF
     * @param String $cpf
     * @return String $cpf
     */
    public function setaMascaraCPF($cpf)
    {

        $cpf = $this->somenteNumero($cpf);
        if (strlen($cpf) == 11) {
            $cpf = substr($cpf, 0, 3) . '.' . substr($cpf, 3, 3) . '.' . substr($cpf, 6, 3) . '-' . substr($cpf, -2);
        }
        return $cpf;
    }

    /**
     * Metodo que muda a string para o formato do CEP
     * @param String $cep
     * @return String $cep
     */
    public function setaMascaraCEP($cep)
    {

        $cep = $this->somenteNumero($cep);
        if (strlen($cep) == 8) {
            $cep = substr($cep, 0, strlen($cep) - 3) . '-' . substr($cep, -3, 3);
        }
        return $cep;
    }

    /**
     * Metodo que muda a string para o formato do CNPJ
     * @param String $cnpj
     * @return String $cnpj
     */
    public function setaMascaraCNPJ($cnpj)
    {

        $cnpj = $this->somenteNumero($cnpj);
        if (strlen($cnpj) == 14) {
            $cnpj = substr($cnpj, 0, 2) . '.' . substr($cnpj, 2, 3) . '.' .
                substr($cnpj, 5, 3) . '/' . substr($cnpj, 8, 4) . '-' . substr(
                    $cnpj, 12, 2);
        }
        return $cnpj;
    }

    /**
     * Metodo que muda a string para o formato do Telefone
     * @param String $tel
     * @return String $tel
     */
    public function setaMascaraTelefone($tel)
    {

        $tel = $this->somenteNumero($tel);
        if (strlen($tel) == 10) {
            $tel = substr($tel, 0, 2) . ' ' . substr($tel, 2, 4) . '.' .
                substr($tel, 6, 4);
        }
        return $tel;
    }

    /**
     * Metodo que retorna o ultimo dia do Mes
     * @param Mixed
     * @return String $lastDay
     */
    public function getLastDay($date = null)
    {
        if (!$date || is_string($date)) {
            $date = new Zend_Date($date, Zend_Date::ISO_8601);
        }
        $lastDay = '';
        switch (get_class($date)) {
            case 'Zend_Date':
                $lastDay = $date->get(Zend_Date::MONTH_DAYS);
                break;
        }
        return $lastDay;
    }

    /**
     * Metodo que retorna a idade de uma pessoa (anos)
     * @param Zend_Date $dt_nascimento
     * @param Zend_Date $dt_comparacao
     * @return Int $idade
     */
    public function retornaIdade(Zend_Date $dt_nascimento, Zend_Date $dt_comparacao = null)
    {
        if (!$dt_comparacao) {
            $dt_comparacao = new Zend_Date(null, Zend_Date::ISO_8601);
        }
        $anoComparacao = $dt_comparacao->get(Zend_Date::YEAR);
        $anoNascimento = $dt_nascimento->get(Zend_Date::YEAR);
        $idade = $anoComparacao - $anoNascimento;
        return $idade;
    }

    /**
     * Realiza o upload básico do cliente para o servidor.
     * Salva um arquivo no caminho informado
     * A pasta pública de upload é a public/upload
     * @todo realizar a validação de acordo com os tipos recebidos no TO
     * @todo realizar a verificação de substituição/renomear arquivo
     * @todo implementar novo adapter???
     * @todo verificar forma de permissão de criação de pasta
     * @param ArquivoTO $arquivoTO
     * @return Ead1_Mensageiro
     */
    public function uploadBasico(\ArquivoTO $arquivoTO)
    {

        $this->mensageiro = new Ead1_Mensageiro();
        try {

            if (empty($arquivoTO->st_caminhodiretorio)) {
                throw new ErrorException('Informe o diretório para salvar o arquivo');
            }

            if (empty($arquivoTO->st_nomearquivo)) {
                throw new ErrorException('Informe o nome do arquivo');
            }

            if (empty($arquivoTO->st_extensaoarquivo)) {
                throw new ErrorException('Informe a extensão do arquivo');
            }

            if (empty($arquivoTO->st_conteudoarquivo) && empty($arquivoTO->ar_arquivo)) {
                throw new ErrorException('Informe o conteúdo do arquivo a ser salvo');
            }


            $diretorioGeralUpload = Ead1_Ambiente::geral()->uploadFolder;
            $urlGeralUpload = Ead1_Ambiente::geral()->uploadUrl;
            $caminhoDiretorio = $arquivoTO->st_caminhodiretorio;

            if ($caminhoDiretorio[0] == "/" || $caminhoDiretorio[0] == '\\') {
                $caminhoDiretorio[0] = '';
            }

            $posicaoUltimoCaracter = strlen($caminhoDiretorio) - 1;

            if ($caminhoDiretorio[$posicaoUltimoCaracter] == "/" || $caminhoDiretorio[$posicaoUltimoCaracter] == '\\') {
                $caminhoDiretorio[$posicaoUltimoCaracter] = '';
            }
            $caminhoDiretorio = trim($caminhoDiretorio);

            $diretorioUpload = $diretorioGeralUpload . DIRECTORY_SEPARATOR . $caminhoDiretorio . DIRECTORY_SEPARATOR;
            $nomeLimpo = $this->substituirCaracteresEspeciais($arquivoTO->st_nomearquivo);
            $nomeLimpo = $this->_substituirEspacosEmBranco(rtrim($nomeLimpo));

            $nomeLimpoUpload = $nomeLimpo . '.' . $arquivoTO->getSt_extensaoarquivo();

            if (!is_dir($diretorioUpload)) {
                if (mkdir($diretorioUpload, 0777, true) === false) {
                    throw new ErrorException('Não foi possível criar o diretório informado.');
                }
            }

            $caminhoCompleto = $diretorioUpload . $nomeLimpoUpload;

            if (file_exists($caminhoCompleto)) {
                $caminhoCompletoNovo = $diretorioUpload . date('d_m_Y_H_i_s_u') . '_' . $nomeLimpoUpload;
                rename($caminhoCompleto, $caminhoCompletoNovo);
            }


            if (!$arquivoTO->getAr_arquivo()) {

                $handle = fopen($caminhoCompleto, 'w+');
                $bytes = fwrite($handle, $arquivoTO->st_conteudoarquivo);
                fclose($handle);

                if ($bytes == false) {
                    throw new ErrorException(' Erro sao salvar o arquivo');
                }
            } else {

                $arquivo = $arquivoTO->getAr_arquivo();
                $adapter = new Zend_File_Transfer_Adapter_Http();
// 				$adapter->addFilter('Rename', $nomeLimpo.'.'.$arquivoTO->getSt_extensaoarquivo());
                $adapter->addFilter('Rename', array('target' => $nomeLimpo . '.' . $arquivoTO->getSt_extensaoarquivo(), 'overwrite' => true));
                $adapter->setDestination(realpath($diretorioUpload));

                if (!$adapter->receive()) {
                    throw new ErrorException(implode("\n", $adapter->getMessages()));
                }
            }

            $urlArquivo = $urlGeralUpload . '/' . $caminhoDiretorio . '/' . $nomeLimpo . '.' . $arquivoTO->getSt_extensaoarquivo();

            $arquivoTO->setSt_nomearquivo($nomeLimpo);
            $arquivoTO->setst_caminhodiretorio($caminhoCompleto);
            $arquivoTO->setst_url(Ead1_Ambiente::geral()->baseUrlUpload . '/' . $caminhoDiretorio . '/' . $nomeLimpo . '.' . $arquivoTO->getSt_extensaoarquivo());


            /**
             * Envia o arquivo retorno para suas rotinas
             * //TODO Adicionar na BO do arquivo de retorno, como foi feito no caso do TCCBO::salvarNotaTCC
             */
            switch (strtoupper($arquivoTO->getSt_extensaoarquivo())) {
                case 'RET':

                    /*$aBO = new ArquivoRetornoBO();
                    $mebo = $aBO->processarArquivoRetorno($arquivoTO);
                    if ($mebo->getTipo() != Ead1_IMensageiro::SUCESSO) {
                        throw new Zend_Exception(' Erro sao salvar o arquivo retorno.');
                    }*/

                    break;
                default:
                    ;
                    break;
            }

            $this->mensageiro->setMensageiro('Arquivo salvo com sucesso', Ead1_IMensageiro::SUCESSO);
            $this->mensageiro->addMensagem($arquivoTO);
        } catch (Exception $exc) {
            $this->mensageiro->setMensageiro($exc->getMessage(), Ead1_IMensageiro::ERRO);
        }
        return $this->mensageiro;
    }

    /**
     * Limpa os caracteres especias de uma string
     * @param string $texto
     * @return strin
     */
    public function substituirCaracteresEspeciais($texto)
    {

        $arCaracterAcentuado = array('á', 'ã', 'à', 'â', 'ä', 'ç', 'é', 'ë', 'è', 'ê',
            'í', 'ï', 'ì', 'î', 'ó', 'ö', 'ô', 'õ', 'ò', 'ú', 'ü', 'û', 'ù', 'ñ',
            'ý', 'ÿ', 'Á', 'Ã', 'À', 'Â', 'Ä', 'Ç', 'É', 'Ë', 'È', 'Ê',
            'Í', 'Ï', 'Ì', 'Î', 'Ó', 'Ö', 'Ô', 'Õ', 'Ò', 'Ú', 'Ü', 'Û', 'Ù', 'Ñ',
            'Ý', 'Ÿ');
        $arCaracterReposicao = array('a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e',
            'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
            'n', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E',
            'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U',
            'N', 'Y', 'Y');

        return str_replace($arCaracterAcentuado, $arCaracterReposicao, $texto);
    }

    /**
     * Limpa os caracteres especias de uma string
     * @param string $texto
     * @return strin
     */
    public function substituirCaracteresEspeciaisAtualizado($texto)
    {

        return preg_replace('/[`´¨^~#?*@$%&\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $texto));
    }

    /**
     * Substitui os espaços em branco de uma string pelo caractere informado
     * O caractere utilizado como padrão é o underscore(_)
     * @param string $texto
     * @param string $caracter OPCIONAL
     * @return string
     */
    protected function _substituirEspacosEmBranco($texto, $caracter = '_')
    {
        return str_replace(' ', $caracter, $texto);
    }

    /**
     * Método que gera o link do WebService para o sistema em questão
     */
    public static function geradorLinkWebService($idSistema)
    {
        $to = new SistemaTO();
        switch ($idSistema) {
            case SistemaTO::SISTEMA_AVALIACAO:
                $key = $to->getSessao()->id_entidade;
                $sha1hash = sha1($key . 'webService' . date('YmdH'));
                $linkHash = Ead1_Ambiente::ws()->wsUrlSistemaAvaliacao . "/key/$key/auth/$sha1hash";
                break;
        }

        return $linkHash;
    }

    public static function parse($str)
    {
        return Ead1_BO::iteraXML(new SimpleXmlIterator($str, null));
    }

    public static function iteraXML($iter)
    {
        $arr = array();
        foreach ($iter as $key => $val) {
            $arr[$key][] = ($iter->hasChildren() ? Ead1_BO::iteraXML($val) : strval($val));
        }
        return $arr;
    }

    public static function arrayUtfEncode($array)
    {

        $dados = array();

        $count = count(array_keys($array));
        $key = array_keys($array);
        $values = array_values($array);
        for ($y = 0; $y < $count; $y++) {

            if (is_array($values[$y])) {
                $dados[$key[$y]] = self::arrayUtfEncode($values[$y]);
            } else {
                $dados[$key[$y]] = utf8_encode($values[$y]);
// 				Zend_Debug::dump($key[$y].' - '.utf8_encode($values[$y]),__CLASS__.'('.__LINE__.')');
            }
        }

        return $dados;
    }

    public static function my_com_create_guid()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime() * 10000); //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = chr(123)// "{"
                . substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12)
                . chr(125); // "}"
            return $uuid;
        }
    }

    /**
     *
     * Monta os dados no formato do IN da pesquisa
     * @param array $retorno Retorno de uma constulta
     * @param string $campo o campo da consulta que deseja colocar no formato do IN
     * @return string retorna os dados no formato ex.: 4,7,9,10
     */
    public function montaDadosIN(Array $retorno, $campo)
    {
        if (!empty($retorno)) {
            foreach ($retorno as $index => $value) {
                $entidade[] = $value[$campo];
            }
            return implode(',', $entidade);
        } else {
            return '';
        }
    }

    /**
     * Retorna instância da classe EntitySerializer
     * @return \Ead1\Doctrine\EntitySerializer
     */
    public function entitySerialize($object)
    {
        $serialize = new EntitySerializer($this->em);
        return $serialize->entityToArray($object);
    }

    /**
     * Serialize an entity to an array
     * @param The entity $entity
     * @return array
     */
    public function toArrayEntity($entity)
    {
        $serialize = new EntitySerializer($this->em);
        return $serialize->toArray($entity);
    }

    /**
     * converte o resultado da pesquisa em array de este for um objeto, do contrário
     * retorna um array vazio
     *
     * @param object $result
     * @return array
     */
    public static function resultToArrayIfObjectOrReturnEmptyArray($result)
    {
        return is_object($result)
               ? $result->toArray()
               : array();
    }

    /**
     * Convert an entity to a JSON object
     *
     * @param The entity $entity
     * @return string
     */
    public function toJsonEntity($entity)
    {
        $serialize = new EntitySerializer($this->em);
        return $serialize->toJson($entity);
    }

    /**
     * Calcula a data futura
     * @param int $qtdDias Quantidade de dias a calcular
     * @return Obj Date
     */
    public function getDataFutura($qtdDias)
    {
        if (!empty($qtdDias)) {
            $date = new Zend_Date(null, Zend_Date::ISO_8601);
            $date->add($qtdDias, Zend_Date::DAY);
            return $date;
        } else {
            return null;
        }
    }

    /**
     * Metodo que usa constantes da utils para retornar data por extenso em formato Brasileiro
     * @param $dataPtBr
     * @return string
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function formataDataExtensoPtBr($dataPtBr)
    {
        $date = new Zend_Date($dataPtBr);
        return $date->toString('dd') . ' de ' . \G2\Constante\Utils::getMonthsExtense($date->toString('M')) . ' de ' . $date->toString('yyyy');
    }


    /**
     * Limpa texto para cabeçalho e rodapé usado na declaração (PDF)
     * @param $html
     * @return mixed
     */
    public function limpaHtmlCabecalhoRodape($html){
        // Remover quebras de linha
        $html_string = preg_replace("/[\n|\r]/", '', $html);
        preg_match("#<body>(.*?)</body>#", $html_string, $results);
        if(is_array($results) && array_key_exists(1, $results))
            return $results[1];
        else
            return $html;

    }

    public function setaMascaraCpfCnpj($cpf_cnpj){
        if (strlen($cpf_cnpj) == 14) {
            $cpf_cnpj = $this->setaMascaraCNPJ($cpf_cnpj);
        }else if (strlen($cpf_cnpj) == 11) {
            $cpf_cnpj = $this->setaMascaraCPF($cpf_cnpj);
        }
        return $cpf_cnpj;
    }
}

<?php

/**
 *
 * Classe com implementações gerais para a camada de Controller
 * @author eder.mariano
 *
 */
class Ead1_Controller extends Zend_Controller_Action
{

    /**
     * Desabilitando o Layout no geral
     * @see Zend_Controller_Action::init()
     */
    protected $doctrineContainer;
    protected $em;

    public function init()
    {
        if ($this->_getParam('controller') == 'index' && $this->_getParam('action') == 'index' && APPLICATION_SYS == 'PORTAL_DO_ALUNO') {
            $this->_redirect('/portal');
        } elseif ($this->_getParam('controller') == 'index' && $this->_getParam('action') == 'index' && APPLICATION_SYS == 'UNICO') {
            $this->_redirect('/unico');
        } elseif ($this->_getParam('controller') == 'index' && $this->_getParam('action') == 'index' && APPLICATION_SYS == 'LOJA') {
            $this->_redirect('/loja');
        } else {
            $config = Ead1_Ambiente::geral();
            $this->view->flexFolder = $config->flexFolder;
            $bootBaseUrl = $this->getInvokeArg('bootstrap')->getOption('baseUrl');
            $this->view->baseUrl = ($bootBaseUrl ? $bootBaseUrl : $config->baseUrl);
            $this->_helper->layout->disableLayout();
        }
    }

    /**
     * Valida se o acesso ao robo esta vindo de fora dos nossos servidores
     * @throws Zend_Exception
     */
    protected function validarPermissaoExecRobo()
    {
        $serverParams = $this->getRequest()->getServer();
        //IP dos servidores de dev e de prod
        $arrAccepted = ["54.207.54.35", "54.94.216.213"]; //TODO, melhorar isso pegar esse ip de algum outro lugar
        //verifica se o ip que tentando executar não tem permissão e se a variável de ambiente não é a de dev
        if (!in_array($serverParams['REMOTE_ADDR'], $arrAccepted)
            && $serverParams["APPLICATION_ENVIRONMENT"] !== "desenvolvimento"
        ) {
            //se não responder aos critérios acima para a execução
            throw new Zend_Exception("Permissão negada.");
        }
    }

    public function setupDoctrine()
    {
        $this->doctrineContainer = Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    /**
     * Método que desabilita as renderizações de PHTML. Utilizado para requisições AJAX.
     */
    public function ajax()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    /**
     * Método que habilita as renderizações do PHTML.
     */
    public function disableAjax()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->enableLayout();
    }

    /**
     * Método que gera combos HTML
     * @param $dados
     * @param $selected
     * @return array
     */
    public function gerarCombo($dados, $selected = null)
    {
        $html = "";
        if (is_array($dados)) {
            foreach ($dados as $valor) {
                if (isset($valor['value'])) {
                    $selectedTexto = ($selected == $valor['value'] ? "selected=\"selected\"" : "");
                    $html .= "<option value='" . $valor['value'] . "' " . $selectedTexto . ">" . $valor['label'] . "</option>\n";
                } else {
                    if (isset($valor['valor'])) {
                        $selectedTexto = ($selected == $valor['valor'] ? "selected=\"selected\"" : "");
                        $html .= "<option value='" . $valor['valor'] . "' " . $selectedTexto . ">" . $valor['label'] . "</option>\n";
                    }
                }
            }
        }
        return $html;
    }

    /**
     * Método que prepara a resposta para a saída Ajax, JSON ou Normal
     * @param mixed $dados
     */
    public function resposta($dados = null)
    {
        if ($dados) {
            if ($this->_getParam('ajax') == true) {
                $this->ajax();
                if ($this->_getParam('json') == true) {
                    $dados = Zend_Json::encode($dados);
                }
                $this->_response->appendBody($dados);
            } else {
                $this->view->dados = $dados;
            }
        }
    }

    /**
     * Limpa o array de parametros
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     * @param array $params
     * @return array
     */
    public function limpaGetAllParams(array $params)
    {
        $notAllowed = ["module", "controller", "action", "page", "per_page", "total_pages", "total_entries", "{}"];
        foreach ($params as $key => $value) {
            if (in_array($key, $notAllowed)) {
                unset($params[$key]);
            }
        }
        return is_array($params) ? $params : null;
    }

    /**
     * Seta o caminho do Layout para a pasta do Modulo em Questão
     * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
     */
    public function setLayoutPath()
    {
        $moduleDir = Zend_Controller_Front::getInstance()->getModuleDirectory();
        $this->_helper->layout()->setLayoutPath($moduleDir . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "layouts");
    }

    public function json($data)
    {
        \Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
        $this->_helper->json($data);
    }

    /**
     * @param $string
     * @return string
     */
    public function camelToDashed($string)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
    }

    /**
     *  retorna as configuraçãoes da aplicação
     *
     * @param string $key
     * @return Zend_config|string
     */
    public function config($key = null)
    {
        $config = $this->getFrontController()
            ->getParam('bootstrap')
            ->getApplication()
            ->getOptions();

        $config = new Zend_Config($config);

        return $key ? $config->get($key) : $config;
    }
}

<?php
/**
 * Super Classe DAO
 * @author edermariano
 *
 */
class Ead1_DAO{
	
    
	/**
	 * Atributo que contém a exceção
	 * @var Exception
	 */
	public $excecao;
	
	public $log = false;
	
	/**
	 * Método que retorna o adaptador padrão do banco de dados
	 */
	public function db(){
		return Zend_Db_Table_Abstract::getDefaultAdapter();
	}
	
	
	/**
	 * Método para debugar o caminho das transações
	 * @author Elcio Mauro Guimarães - elcioguimaraes@gmail.com
	 * @param unknown_type $tran
	 */
	private function _logTransaction($tran = 'Abre'){
		$transacao = Ead1_Transacao::getInstancia();
		echo '<hr>'.$tran.' Transações Abertas atéagora '.$transacao->qtdTransacao.' =>';
		$aqui = debug_backtrace();
		for($x=0;$x<=3;$x++){
			echo $aqui[$x]['file'].' ('.$aqui[$x]['line'].') <br>' ;
		}
	}
	
	
	/**
	 * Método que inicializa a transação
	 */
	public function beginTransaction(){
		
		if($this->log)
			$this->_logTransaction();
		
		$transacao = Ead1_Transacao::getInstancia();
		if(!Ead1_TO::is_boolean($transacao->transacao)){
			$this->db()->beginTransaction();
			$transacao->transacao = true;
		}
		$transacao->qtdTransacao++;
	}
	
	/**
	 * Método que verifica se a transação está ativa ou não.
	 * Deverá ser invocado, todas as vezes que se fizer obrigatória a presença de uma transação.
	 * Exemplo em qualquer inserção, atualização e delete.
	 */
	public function transacaoAtiva(){
		if(!$this->transacao){
			throw new Exception('É necessário possuir uma transação ativa para este processo.',0);
			Zend_Debug::dump('É necessário possuir uma transação ativa para este processo.');
			exit;
		}
		return $this->transacao;
	}
	
	/**
	 * Método para commit de transações
	 */
	public function commit(){
		
		if($this->log)
			$this->_logTransaction('Fecha');
		
		$transacao = Ead1_Transacao::getInstancia();
		if($transacao->qtdTransacao == 1){
			$this->db()->commit();
			$transacao->transacao = false;
			$transacao->qtdTransacao = 0;
		}else{
			$transacao->qtdTransacao--;
		}
	}
	
	/**
	 * Método para rollBack de transações
	 */
	public function rollBack(){

		if($this->log)
			$this->_logTransaction('RollBack');

		$transacao = Ead1_Transacao::getInstancia();
		if($transacao->transacao){
			$this->db()->rollBack();
			$transacao->transacao = false;
			$transacao->qtdTransacao = 0;			
		}
	}
	
	/**
	 * Método que mostra quantas transações estão ativas.
	 * @return number
	 */
	public function getQtdTransacao(){
		
		$transacao = Ead1_Transacao::getInstancia();
		return $transacao->qtdTransacao;
		
	}
}
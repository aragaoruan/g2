<?php
/**
 * Classe de Mensagem que salva e Envia as Mensagens e seus destinatários
 * @author eduardoromao
 * @todo Desenvolvimento Interrompido por Mudanças na Modelagem e Regras de Negócio do Sistema
 */
class Ead1_Mail_Message{
	
	/**
	 * Construtor
	 */
	public function __construct(){
		$this->_bcc = array();
		$this->_cc = array();
		$this->_dt_envio = new Zend_Date(null);
	}
	
	/**
	 * Contem a Data para Envio da Mensagem 
	 * @var Zend_Date
	 */
	private $_dt_envio;
	
	/**
	 * Contem o Assunto da Mensagem 
	 * @var String
	 */
	private $_subject;
	
	/**
	 * Contem a Mensagem a Ser Enviada
	 * @var String
	 */
	private $_text;
	
	/**
	 * Contem os Destinatarios
	 * @var array
	 */
	private $_to;
	
	/**
	 * Contem os destinatarios que receberao a Copia da Mensagem
	 * @var array
	 */
	private $_cc;
	
	/**
	 * Contem os destinatarios que receberao a Copia Oculta da Mensagem
	 * @var array
	 */
	private $_bcc;
	
	/**
	 * @return the $_text
	 */
	public function getText() {
		return $this->_text;
	}

	/**
	 * @param String $_text
	 * @return Ead1_Mail_Message
	 */
	public function setText($_text) {
		$this->_text = $_text;
		return $this;
	}

	/**
	 * @return the $_subject
	 */
	public function getSubject() {
		return $this->_subject;
	}

	/**
	 * @param String $_subject
	 * @return Ead1_Mail_Message
	 */
	public function setSubject($_subject) {
		$this->_subject = $_subject;
		return $this;
	}

	/**
	 * Método que valida a Mensagem
	 * @param function $validateFuntion
	 * @return Ead1_Mail_Message
	 */
	public function validate($validateFuntion = NULL){
		if($validateFuntion){
			$validateFuntion($this);
		}else{
			$this->_validateTo();
		}
		return $this;
	}
	
	/**
	 * Método privado que valida o Destinatário
	 * @throws Ead1_Mail_Exception
	 */
	private function _validateTo(){
		if(empty($this->getTo())){
			THROW new Ead1_Mail_Exception('Nenhum Destinatário Informado');
		}
		foreach ($this->getTo() as $to){
			if(!array_key_exists('id_usuario', $to) || 
				(!array_key_exists('st_email', $to) || !array_key_exists('st_nome', $to)) ){
				THROW new Ead1_Mail_Exception('Parametros de Destinatários Inválidos.');
			}else{
				if(array_key_exists('st_email', $to)){
					$validate = new Zend_Validate_EmailAddress();
					if(!$validate->isValid($to['st_email'])){
						THROW new Ead1_Mail_Exception('Email do Destinatário Inválido.');
					}
				}
			}
		}
	}
	
	/**
	 * Método privado que valida o Destinatário de Cópia
	 * @throws Ead1_Mail_Exception
	 */
	private function _validateCc(){
		foreach ($this->getCc() as $cc){
			if(!array_key_exists('id_usuario', $cc) || 
				(!array_key_exists('st_email', $cc) || !array_key_exists('st_nome', $cc)) ){
				THROW new Ead1_Mail_Exception('Parametros de Destinatários de Cópia Inválidos.');
			}else{
				if(array_key_exists('st_email', $cc)){
					$validate = new Zend_Validate_EmailAddress();
					if(!$validate->isValid($cc['st_email'])){
						THROW new Ead1_Mail_Exception('Email do Destinatário de Cópia Inválido.');
					}
				}
			}
		}
	}
	
	/**
	 * Método privado que valida o Destinatário de Cópia Oculta
	 * @throws Ead1_Mail_Exception
	 */
	private function _validateBcc(){
		foreach ($this->getBcc() as $bcc){
			if(!array_key_exists('id_usuario', $bcc) || 
				(!array_key_exists('st_email', $bcc) || !array_key_exists('st_nome', $bcc)) ){
				THROW new Ead1_Mail_Exception('Parametros de Destinatários Cópia Oculta Inválidos.');
			}else{
				if(array_key_exists('st_email', $bcc)){
					$validate = new Zend_Validate_EmailAddress();
					if(!$validate->isValid($bcc['st_email'])){
						THROW new Ead1_Mail_Exception('Email do Destinatário de Cópia Oculta Inválido.');
					}
				}
			}
		}
	}
	
	/**
	 * Adiciona o destinatario da cópia oculta
	 * @param array $data
	 * @return Ead1_Mail_Message
	 * Ex:
	 * array('id_usuario'=> 1,)
	 * array('st_email' => 'bcc@email.com','st_nome','Bcc Nome')
	 */
	public function addBcc(array $data){
		$this->_bcc[] = $data;
		return $this;
	}
	
	/**
	 * Método que retorna os Destinatarios das Cópias Ocultas
	 * @return array
	 */
	public function getBcc($index = null){
		if(is_null($index)){
			return $this->_bcc;
		}else{
			return $this->_bcc[$index];
		}
	}
	
	/**
	 * Método remove os Destinatarios das Cópias Ocultas
	 * @return Ead1_Mail_Message
	 */
	public function removeBcc($index = null){
		if(is_null($index)){
			$this->_bcc = array();
		}else{
			unset($this->_bcc[$index]);
		}
		return $this;
	}
	
	/**
	 * Método remove os Destinatarios das Cópias
	 * @return Ead1_Mail_Message
	 */
	public function removeCc($index = null){
		if(is_null($index)){
			$this->_cc = array();
		}else{
			unset($this->_cc[$index]);
		}
		return $this;
	}
	
	/**
	 * Método remove os Destinatarios
	 * @return Ead1_Mail_Message
	 */
	public function removeTo($index = null){
		if(is_null($index)){
			$this->_to = array();
		}else{
			unset($this->_to[$index]);
		}
		return $this;
	}
	
	/**
	 * Método que retorna os Destinatarios das Cópias
	 * @return array
	 */
	public function getCc($index = null){
		if(is_null($index)){
			return $this->_cc;
		}else{
			return $this->_cc[$index];
		}
	}
	
	/**
	 * Método que retorna os Destinatarios das Cópias
	 * @return array
	 */
	public function getTo($index = null){
		if(is_null($index)){
			return $this->_to;
		}else{
			return $this->_to[$index];
		}
	}
	
	/**
	 * Adiciona o destinatario da cópia
	 * @param array $data
	 * @return Ead1_Mail_Message
	 * Ex:
	 * array('id_usuario'=> 1,)
	 * array('st_email' => 'cc@email.com','st_nome','Cc Nome')
	 */
	public function addCc(array $data){
		$this->_cc[] = $data;
		return $this;
	}
	
	/**
	 * Adiciona os destinatarios principais
	 * @param array $data
	 * @return Ead1_Mail_Message
	 * Ex:
	 * array('id_usuario'=> 1,)
	 * array('st_email' => 'destinatario@email.com','st_nome','Destinatario Nome')
	 */
	public function addTo(array $data){
		$this->_to[] = $data;
		return $this;
	}
	
	/**
	 * @return the $_dt_envio
	 */
	public function getDt_envio() {
		return $this->_dt_envio;
	}

	/**
	 * @param Zend_Date $_dt_envio
	 * @return Ead1_Mail_Message
	 */
	public function setDt_envio($_dt_envio) {
		$this->_dt_envio = $_dt_envio;
		return $this;
	}
	
	/**
	 * Método que salva a Mensagem
	 * @param Ead1_Mail_Message $message
	 * @return $id_mensagem
	 */
	public static function save(Ead1_Mail_Message $message){
	}
	
}
<?php
/**
 * Adapter de Abstração de dados de configuração de email
 * @author eduardoromao
 * -Feita para ser alterada de acordo com a necessidade
 */
class Ead1_Mail_Connection_Factory implements Ead1_Mail_Interfaces_ConnectionFactory{
	
	/**
	 * Método que retorna os dados de configuração do banco de dados
	 * @param Mixed $config - Parametro para configuração do Retorno das Configurações de Email
	 * @return Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	public function getDbConfig($config){
		try{
			$adapter = Zend_Db_Table::getDefaultAdapter();
//			$select = $adapter
//						->select()
//							->from(Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG,'*')
//							->join("tb_entidade", 
//							"tb_entidade.id_entidade = ".Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG.".id_entidade",
//							"st_nomeentidade")
//							->where(Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG.'.bl_ativo = 1')
//							->where($config);
			$select = $adapter
						->select()
							->from(Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG,'*')
							->join("tb_entidade", 
							"tb_entidade.id_entidade = ".Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG.".id_entidade",
							"st_nomeentidade")
							->where(Ead1_Mail_Interfaces_ConnectionFactory::TABLE_EMAIL_CONFIG.'.bl_ativo = 1')
							->where($config);
			$data = $adapter->fetchRow($select);
			if(empty($data)){
				THROW new Ead1_Mail_Exception("Nenhuma Configuração de Email Encontrada");
			}
			$data['st_nomeentidade'] = utf8_decode($data['st_nomeentidade']);
		}catch (Ead1_Mail_Exception $e){
			THROW $e;
		}catch (Zend_Db_Exception $e){
			THROW new Ead1_Mail_Exception('Erro ao Pegar Configurações do Email',null,$e);
		}
		return self::encapsulaConfigData($data);
	}
	
	/**
	 * Método que retorna os dados de configuração do sistema
	 * @return Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	public function getSistemConfig(){
		return self::encapsulaConfigData(Ead1_Ambiente::email()->toArray());
	}

	/**
	 * Método que encapsula a data para o objeto
	 * @return Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	public static function encapsulaConfigData(array $data){
		return Ead1_Mail_Config_Factory::getFactoryAdapter()->adaptConfigObjectData($data);
	}
}
<?php
/**
 * Classe que encapsula configuracoes de email
 * @author eduardoromao
 */
class Ead1_Mail_Connection_Adapter_ConfigObject{
	
	/**
	 * Id da configuracao de email
	 * @var int
	 */
	protected $id_emailconfig;
	
	/**
	 * id da instituição da configuração do email
	 * @var int
	 */
	protected $id_entidade;
	
	/**
	 * Tipo de conexão de email de entrada (IMAP/POP3) 
	 * (SSL, TLS, ETC)
	 * @var int
	 */
	protected $id_tipoconexaoemailentrada;
	
	/**
	 * Tipo de conexão de email de saida (SMTP)
	 * (SSL, TLS, ETC)
	 * @var int
	 */
	protected $id_tipoconexaoemailsaida;
	
	/**
	 * Tipo de servidor de entrada 
	 * (IMAP/POP3) 
	 * @var int
	 */
	protected $id_tiposervidoremailentrada;
	
	/**
	 * Host de email de entrada (IMAP/POP3)
	 * @var String
	 */
	protected $st_hostentrada;
	
	/**
	 * Host de email de saida (SMTP)
	 * @var String
	 */
	protected $st_hostsaida;
	
	/**
	 * Usuário (user/username) - email@host.com
	 * @var String
	 */
	protected $st_usuario;
	
	/**
	 * Senha (password)
	 * @var String
	 */
	protected $st_senha;
	
	/**
	 * Autenticacao segura
	 * @var int - (1/0)
	 */
	protected $bl_autenticacaosegura;
	
	/**
	 * Porta de Entrada (25/995)
	 * @var int
	 */
	protected $nu_portaentrada;
	
	/**
	 * Porta de Saida (22/110)
	 * @var int
	 */
	protected $nu_portasaida;
	
	/**
	 * Quem esta mandando o Email - Gestor 2
	 * @var String
	 */
	protected $st_from;
	
	/**
	 * @return the $id_emailconfig
	 */
	public function getId_emailconfig() {
		return $this->id_emailconfig;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_tipoconexaoemailentrada
	 */
	public function getId_tipoconexaoemailentrada() {
		return $this->id_tipoconexaoemailentrada;
	}

	/**
	 * @return the $id_tipoconexaoemailsaida
	 */
	public function getId_tipoconexaoemailsaida() {
		return $this->id_tipoconexaoemailsaida;
	}

	/**
	 * @return the $id_tiposervidoremailentrada
	 */
	public function getId_tiposervidoremailentrada() {
		return $this->id_tiposervidoremailentrada;
	}

	/**
	 * @return the $st_hostentrada
	 */
	public function getSt_hostentrada() {
		return $this->st_hostentrada;
	}

	/**
	 * @return the $st_hostsaida
	 */
	public function getSt_hostsaida() {
		return $this->st_hostsaida;
	}

	/**
	 * @return the $st_usuario
	 */
	public function getSt_usuario() {
		return $this->st_usuario;
	}

	/**
	 * @return the $st_senha
	 */
	public function getSt_senha() {
		return $this->st_senha;
	}

	/**
	 * @return the $bl_autenticacaosegura
	 */
	public function getBl_autenticacaosegura() {
		return $this->bl_autenticacaosegura;
	}

	/**
	 * @return the $nu_portaentrada
	 */
	public function getNu_portaentrada() {
		return $this->nu_portaentrada;
	}

	/**
	 * @return the $nu_portasaida
	 */
	public function getNu_portasaida() {
		return $this->nu_portasaida;
	}

	/**
	 * @param int $id_emailconfig
	 */
	public function setId_emailconfig($id_emailconfig) {
		$this->id_emailconfig = $id_emailconfig;
	}

	/**
	 * @param int $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
	}

	/**
	 * @param int $id_tipoconexaoemailentrada
	 */
	public function setId_tipoconexaoemailentrada($id_tipoconexaoemailentrada) {
		$this->id_tipoconexaoemailentrada = $id_tipoconexaoemailentrada;
	}

	/**
	 * @param int $id_tipoconexaoemailsaida
	 */
	public function setId_tipoconexaoemailsaida($id_tipoconexaoemailsaida) {
		$this->id_tipoconexaoemailsaida = $id_tipoconexaoemailsaida;
	}

	/**
	 * @param int $id_tiposervidoremailentrada
	 */
	public function setId_tiposervidoremailentrada($id_tiposervidoremailentrada) {
		$this->id_tiposervidoremailentrada = $id_tiposervidoremailentrada;
	}

	/**
	 * @param String $st_hostentrada
	 */
	public function setSt_hostentrada($st_hostentrada) {
		$this->st_hostentrada = $st_hostentrada;
	}

	/**
	 * @param String $st_hostsaida
	 */
	public function setSt_hostsaida($st_hostsaida) {
		$this->st_hostsaida = $st_hostsaida;
	}

	/**
	 * @param String $st_usuario
	 */
	public function setSt_usuario($st_usuario) {
		$this->st_usuario = $st_usuario;
	}

	/**
	 * @param String $st_senha
	 */
	public function setSt_senha($st_senha) {
		$this->st_senha = $st_senha;
	}

	/**
	 * @param int $bl_autenticacaosegura
	 */
	public function setBl_autenticacaosegura($bl_autenticacaosegura) {
		$this->bl_autenticacaosegura = $bl_autenticacaosegura;
	}

	/**
	 * @param int $nu_portaentrada
	 */
	public function setNu_portaentrada($nu_portaentrada) {
		$this->nu_portaentrada = $nu_portaentrada;
	}

	/**
	 * @param int $nu_portasaida
	 */
	public function setNu_portasaida($nu_portasaida) {
		$this->nu_portasaida = $nu_portasaida;
	}
	/**
	 * @return the $st_from
	 */
	public function getSt_from() {
		return $this->st_from;
	}

	/**
	 * @param String $st_from
	 */
	public function setSt_from($st_from) {
		$this->st_from = $st_from;
	}

}
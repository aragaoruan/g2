<?php
/**
 * Classe Adapter de Factory Object
 * @author eduardoromao
 */
class Ead1_Mail_Connection_Adapter_Factory implements Ead1_Mail_Interfaces_ConnectionAdapterFactory{
	
	/**
	 * Método que adapta o Objeto
	 * @param array $data
	 * @return Ead1_Mail_Connection_Adapter_ConfigObject 
	 */
	public static function adaptConfigObjectData(array $data){
		self::resetConfigObjectInstance();
		self::getConfigObject()->setId_emailconfig(isset($data['id_emailconfig']) ? $data['id_emailconfig'] : 0);
		self::getConfigObject()->setId_tipoconexaoemailentrada($data['id_tipoconexaoemailentrada']);
		self::getConfigObject()->setId_tipoconexaoemailsaida($data['id_tipoconexaoemailsaida']);
		self::getConfigObject()->setSt_hostsaida($data['st_smtp']);
		self::getConfigObject()->setId_tiposervidoremailentrada($data['id_servidoremailentrada'] == Ead1_Mail_Connection_Factory::SERVER_IMAP ? $data['st_imap'] : $data['st_pop']);
		self::getConfigObject()->setId_tiposervidoremailentrada($data['id_servidoremailentrada']);
		self::getConfigObject()->setSt_usuario($data['st_conta']);
		self::getConfigObject()->setSt_senha($data['st_senha']);
		self::getConfigObject()->setSt_from(isset($data['st_nomeentidade']) ? $data['st_nomeentidade'] : "Email Automático");
		self::getConfigObject()->setBl_autenticacaosegura($data['bl_autenticacaosegura']);
		self::getConfigObject()->setNu_portasaida(isset($data['nu_portasaida']) ? $data['nu_portasaida'] : NULL);
		self::getConfigObject()->setNu_portaentrada(isset($data['nu_portaentrada']) ? $data['nu_portaentrada'] : NULL);
		return self::getConfigObject();
	}
	
	/**
	 * Contem o Objeto a ser configurado
	 * @var Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	private static $_configObject;
	
	/**
	 * Reseta a instancia do objeto de configuracao
	 */
	public static function resetConfigObjectInstance(){
		self::$_configObject = new Ead1_Mail_Connection_Adapter_ConfigObject();
	}
	
	/**
	 * Valida se a Instancia já foi Criada
	 * @return Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	public static function getConfigObject(){
		if(!self::$_configObject){
			self::$_configObject = new Ead1_Mail_Connection_Adapter_ConfigObject();
		}
		return self::$_configObject;
	}
}
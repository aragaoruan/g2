<?php
/**
 * Interface de Factory de Conexão
 * @author eduardoromao
 */
interface Ead1_Mail_Interfaces_ConnectionFactory{
	
	/**
	 * Constante da tabela de configuracao de email
	 * @var String
	 */
	const TABLE_EMAIL_CONFIG = 'tb_emailconfig';
	
	/**
	 * Constantes de tipo de conexão de email
	 */
	const CONNECTION_SSL = 1;
	const CONNECTION_TLS = 2;
	const CONNECTION_STARTTLS = 3;
	
	/**
	 * Constantes de tipo de servidor de envio de email de entrada
	 */
	const SERVER_POP3 = 1;
	const SERVER_IMAP = 2;
	
	
	/**
	 * Método que retorna os dados de configuração do banco de dados
	 * @param int $id_emailconfig
	 * @return Ead1_Mail_Connection_Adapter_Object
	 */
	public function getDbConfig($id_emailconfig);
	
	/**
	 * Método que retorna os dados de configuração do sistema
	 * @return Ead1_Mail_Connection_Adapter_Object
	 */
	public function getSistemConfig();
	
	/**
	 * Método que encapsula a data para o objeto de Configuração
	 * @param array $data
	 */
	public static function encapsulaConfigData(array $data);
}
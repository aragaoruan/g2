<?php
/**
 * Interface de Dapter de Factory de Conexão
 * @author eduardoromao
 */
interface Ead1_Mail_Interfaces_ConnectionAdapterFactory{

	/**
	 * Método que adapta o Objeto
	 * @param array $data
	 */
	public static function adaptConfigObjectData(array $data);
	
	/**
	 * Reseta a instancia do objeto de configuracao
	 */
	public static function resetConfigObjectInstance();
	
	/**
	 * Valida se a Instancia já foi Criada
	 * @return Ead1_Mail_Connection_Adapter_Object
	 */
	public static function getConfigObject();
}
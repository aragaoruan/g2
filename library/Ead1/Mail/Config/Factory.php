<?php
/**
 * Classe de criação de configuração de mensagens
 * @author eduardoromao
 */
class Ead1_Mail_Config_Factory{

	/**
	 * Contruct
	 * @param Mixed $config - Parametro para configuração do Retorno das Configurações de Email
	 */
	public function __construct($config = null){
		if(is_null($config)){
			$this->_config = self::getFactoryConnection()->getSistemConfig();
		}else{
			$this->_config = self::getFactoryConnection()->getDbConfig($config);
		}
	}

	/**
	 * Método que reseta os factorys Defaults
	 */
	public static function resetDefaults(){
		self::$_factoryAdapter = new Ead1_Mail_Connection_Adapter_Factory();
		self::$_factoryConnection = new Ead1_Mail_Connection_Factory();
	}

    /**
	 * Contem o adapter de ancapsulamento do Objeto
	 * @var Ead1_Mail_Connection_Adapter_Factory
	 */
	private static $_factoryAdapter;

    /**
	 * Método que seta o adapter do factory
	 * @param Ead1_Mail_Interfaces_ConnectionAdapterFactory $adapter
	 */
	public static function setFactoryAdapter(Ead1_Mail_Interfaces_ConnectionAdapterFactory $adapter){
		self::$_factoryAdapter = $adapter;
	}

    /**
     * Método que retorna o factory adapter
	 * @return Ead1_Mail_Connection_Adapter_Factory
	 */
	public static function getFactoryAdapter(){
		if(!self::$_factoryAdapter){
			self::$_factoryAdapter = new Ead1_Mail_Connection_Adapter_Factory();
		}
		return self::$_factoryAdapter;
	}

    /**
	 * Contem a factory de conexão a ser usada
	 * @var Ead1_Mail_Connection_Factory
	 */
	private static $_factoryConnection;

    /**
	 * Método que seta a factory de connexão
	 * @param Ead1_Mail_Interfaces_ConnectionFactory $conn
	 */
	public static function setFactoryConnection(Ead1_Mail_Interfaces_ConnectionFactory $conn){
		self::$_factoryConnection = $conn;
	}

	/**
     * Método que retorna o factory da conexão
	 * @return Ead1_Mail_Connection_Factory
	 */
	public static function getFactoryConnection(){
		if(!self::$_factoryConnection){
			self::$_factoryConnection = new Ead1_Mail_Connection_Factory();
		}
		return self::$_factoryConnection;
	}

	/**
	 * Método que gera as configurações padrão de Email do Sistema (email.ead1)
	 * @return Ead1_Mail_Config_Factory
	 */
	public function geraConfiguracaoPadraoSistema(){
		$this->__construct();
		return $this;
	}

    /**
	 * Método que gera as configurações padrão de Email da entidade
	 * @param string|int|array $config - Parametro para configuração do Retorno das Configurações de Email
	 * @return Ead1_Mail_Config_Factory
	 */
	public function geraConfiguracaoPadraoEntidade($config){
		$this->__construct($config);
		return $this;
	}

    /**
	 * Variavel que contem a configuração de transporte de envio de mensagens
	 * @var Ead1_Mail_Connection_Adapter_ConfigObject
	 */
	private $_config;

    /**
	 * Variavel de Configuração de Transporte de Email
	 * @var Zend_Mail_Transport_Smtp
	 */
	private $_transport;

    /**
	 * Método que retorna a configuração de transporte de envio de email
	 * @return Zend_Mail_Transport_Smtp
	 */
	public function getConfiguracaoSaida(){
		$this->montaConfiguracaoSaida($this->_config);
		return $this->_transport;
	}

    /**
	 * Método que retorna o Mail Com a Configuração de Transporte
	 * @param Zend_Mail $mail
	 */
	public function sendMail(Zend_Mail $mail){
		$transport = $this->montaConfiguracaoSaida($this->_config)->getConfiguracaoSaida();
		$mail->setReplyTo($this->_config->getSt_usuario());
		$mail->setFrom($this->_config->getSt_usuario(),$this->_config->getSt_from());
		$mail->setReturnPath($this->_config->getSt_usuario());
		$transport->send($mail);
	}

    /**
	 * Variavel de configuração de Captação de Email
	 * @var Zend_Mail_Storage_Pop3|Zend_Mail_Storage_Imap
	 */
	private $_storage;

    /**
	 * Método que retorna a configuração de Storage de email
	 * @return Zend_Mail_Storage_Pop3|Zend_Mail_Storage_Imap
	 */
	public function getConfiguracaoEntrada(){
		$this->montaConfiguracaoEntrada($this->_config);
		return $this->_storage;
	}

    /**
	 * Método que gera a configuração para transporte
	 * @param Ead1_Mail_Connection_Adapter_ConfigObject $object
	 * @return array
	 */
	protected function transportConfigFactory(Ead1_Mail_Connection_Adapter_ConfigObject $object){
		$config = array();
        if($object->getNu_portasaida()){
            $config['port'] = $object->getNu_portasaida();
        }
        if($object->getBl_autenticacaosegura()== true){
            $config['auth'] = 'login';
            $config['username'] = $object->getSt_usuario();
            $config['password'] = $object->getSt_senha();
            switch ($object->getId_tipoconexaoemailsaida()){
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_SSL:
                    $config['ssl'] = 'ssl';
					break;
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_TLS:
					$config['ssl'] = 'tls';
					break;
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_STARTTLS:
					$config['ssl'] = 'STARTTLS';
					break;
			}
		}

		return $config;

	}

    /**
	 * Método que gera a configuração para transporte
	 * @param Ead1_Mail_Connection_Adapter_ConfigObject $object
	 * @return array
	 */
	protected function storageConfigFactory(Ead1_Mail_Connection_Adapter_ConfigObject $object){
		$config = array();
		$config['host'] = $object->getSt_hostentrada();
		$config['user'] = $object->getSt_usuario();
		$config['password'] = $object->getSt_senha();
		if($object->getNu_portaentrada()){
			$config['port'] = $object->getNu_portaentrada();
		}
		if($object->getBl_autenticacaosegura()){
			switch ($object->getId_tipoconexaoemailentrada()){
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_SSL:
					$config['ssl'] = 'SSL';
					break;
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_TLS:
					$config['ssl'] = 'TLS';
					break;
				case Ead1_Mail_Interfaces_ConnectionFactory::CONNECTION_STARTTLS:
					$config['ssl'] = 'STARTTLS';
					break;
			}
		}
		return $config;
	}

    /**
	 * Método que monta as Configurações de Transporte (Saida)
	 * @param Ead1_Mail_Connection_Adapter_ConfigObject $object
	 * @return Ead1_Mail_Config_Factory
	 */
	public function montaConfiguracaoSaida(Ead1_Mail_Connection_Adapter_ConfigObject $object){
		$config = $this->transportConfigFactory($object);
		$this->_transport = new Zend_Mail_Transport_Smtp($object->getSt_hostsaida(),$config);
		return $this;
	}

    /**
	 * Método que monta as Configurações de Storage (Entrada)
	 * @param Ead1_Mail_Connection_Adapter_ConfigObject $object
	 */
	public function montaConfiguracaoEntrada(Ead1_Mail_Connection_Adapter_ConfigObject $object){
		$config = $this->storageConfigFactory($object);
		switch ($object->getId_tiposervidoremailentrada()){
			case Ead1_Mail_Interfaces_ConnectionFactory::SERVER_POP3:
				$this->_storage = new Zend_Mail_Storage_Pop3($config);
				break;
			case Ead1_Mail_Interfaces_ConnectionFactory::SERVER_IMAP:
				$this->_storage = new Zend_Mail_Storage_Imap($config);
				break;
			default:
				THROW new Ead1_Mail_Exception("Servidor de Email de Entrada não Identificado.");
				break;
		}
	}
}

<?php
/**
 * Classe de Exception de Ead1 Mail
 * @author eduardoromao
 */
class Ead1_Mail_Exception extends Zend_Mail_Exception{}
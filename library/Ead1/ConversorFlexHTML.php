<?php
/**
 * Classe responsável para tratar os textos formatados pelos componentes FLEX 
 * e converte-lôs no formato HTML
 * 
 * @author Arthur Cláudio de Almeida Pereira < arthur.almeida@ead1.com.br >
 */

class Ead1_ConversorFlexHTML {
	
	private $_flexExpr	= array(
								'/<font(.*?)(size="(\d+)")(.*?)>/im'	=>	'<font$1style="font-size: $3px;">'
								,'/<\/*?textformat.*?>/im'	=>	''
								);
	
	public function toHTML( $string ) {
		foreach( $this->_flexExpr as $pattern => $replacement ) {
			$string	=  preg_replace( $pattern, $replacement, $string );
		}
		return $string;
	}
}
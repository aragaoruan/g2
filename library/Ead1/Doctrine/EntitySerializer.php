<?php

namespace Ead1\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata,
    Doctrine\Common\Util\Inflector,
    Doctrine\ORM\EntityManager,
    Exception;

class EntitySerializer
{

    /**
     * @var EntityManager
     */
    protected $_em;

    /**
     * @var int
     */
    protected $_recursionDepth = 0;

    /**
     * @var int
     */
    protected $_maxRecursionDepth = 0;

    public function __construct ($em)
    {
        $this->setEntityManager($em);
    }

    /**
     *
     * @return EntityManager
     */
    public function getEntityManager ()
    {
        return $this->_em;
    }

    public function setEntityManager (EntityManager $em)
    {
        $this->_em = $em;

        return $this;
    }

    protected function _serializeEntity ($entity)
    {
        $className = get_class($entity);
        $metadata = $this->_em->getClassMetadata($className);

        $data = array();

        foreach ($metadata->fieldMappings as $field => $mapping) {
            $value = $metadata->reflFields[$field]->getValue($entity);
            $field = Inflector::tableize($field);
            if ($value instanceof \DateTime) {
                // We cast DateTime to array to keep consistency with array result
                $data[$field] = (array) $value;
            } elseif (is_object($value)) {
                $data[$field] = (string) $value;
            } else {
                $data[$field] = $value;
            }
        }

        foreach ($metadata->associationMappings as $field => $mapping) {
            $key = Inflector::tableize($field);
            if ($mapping['isCascadeDetach']) {
                $data[$key] = $metadata->reflFields[$field]->getValue($entity);
                if (null !== $data[$key]) {
                    $data[$key] = $this->_serializeEntity($data[$key]);
                }
            } elseif ($mapping['isOwningSide'] && $mapping['type'] & ClassMetadata::TO_ONE) {
                if (null !== $metadata->reflFields[$field]->getValue($entity)) {
                    if ($this->_recursionDepth < $this->_maxRecursionDepth) {
                        $this->_recursionDepth++;
                        $data[$key] = $this->_serializeEntity(
                                $metadata->reflFields[$field]
                                        ->getValue($entity)
                        );
                        $this->_recursionDepth--;
                    } else {
                        $data[$key] = $this->getEntityManager()
                                ->getUnitOfWork()
                                ->getEntityIdentifier(
                                $metadata->reflFields[$field]
                                ->getValue($entity)
                        );
                    }
                } else {
                    // In some case the relationship may not exist, but we want
                    // to know about it
                    $data[$key] = null;
                }
            }
        }

        return $data;
    }

    protected function _deserializeEntity (array $dice  , $entity)
    {
        $className = get_class($entity);
        $metadata = $this->_em->getClassMetadata($className);

        foreach ($metadata->fieldMappings as $field => $mapping) {
            if (array_key_exists($field, $dice) && !is_null($dice[$field]) && $dice[$field] !== ''){
                if ($mapping['type'] == "datetime" || $mapping['type'] == "datetime2"){
                    if ($dice[$field] instanceof \DateTime) {
                        $metadata->reflFields[$field]->setValue($entity, $dice[$field]);
                    }else if ($dice[$field] instanceof \Zend_Date){
                        $metadata->reflFields[$field]->setValue($entity, $dice[$field]->toString('yyyy-MM-dd HH:mm:ss'));
                    }else{
                            $date = new \Zend_Date( new \Zend_Date($dice[$field]), \Zend_Date::ISO_8601 );
                            $metadata->reflFields[$field]->setValue($entity, new \DateTime($date->toString('yyyy-MM-dd HH:mm:ss')));
                    }
                }else{
                    $metadata->reflFields[$field]->setValue($entity, $dice[$field]);
                }
            }
        }

        foreach ($metadata->associationMappings as $field => $mapping) {
            if ($mapping['isOwningSide'] && $mapping['type'] & ClassMetadata::TO_ONE) {
                if (!empty($dice[$field])){
                    $metadata->reflFields[$field]->setValue($entity, $this->_em->getReference($mapping['targetEntity'], $dice[$field]));
                }
            }else if ($mapping['type'] & ClassMetadata::MANY_TO_MANY){
                if (!empty($dice[$field])){
                    $collection = array();
                    foreach ($dice[$field] as $data){
                        $collection[] = $this->_em->getRepository($mapping['targetEntity'])->findOneBy($data);
                    }
                    $metadata->reflFields[$field]->setValue($entity,$collection);
                }
            }
        }

        return $entity;
    }

    public function arrayToEntity(array $params, $entity){
        return $this->_deserializeEntity($params, $entity);
    }

    /**
     * Serialize an entity to an array
     *
     * @param The entity $entity
     * @return array
     */
    public function toArray ($entity)
    {
        return $this->_serializeEntity($entity);
    }

    /**
     * Convert an entity to a JSON object
     *
     * @param The entity $entity
     * @return string
     */
    public function toJson ($entity)
    {
        return json_encode($this->toArray($entity));
    }

    /**
     * Convert an entity to XML representation
     *
     * @param The entity $entity
     * @throws Exception
     */
    public function toXml ($entity)
    {
        throw new Exception('Not yet implemented');
    }

    /**
     * Set the maximum recursion depth
     *
     * @param   int     $maxRecursionDepth
     * @return  void
     */
    public function setMaxRecursionDepth ($maxRecursionDepth)
    {
        $this->_maxRecursionDepth = $maxRecursionDepth;
    }

    /**
     * Get the maximum recursion depth
     *
     * @return  int
     */
    public function getMaxRecursionDepth ()
    {
        return $this->_maxRecursionDepth;
    }

    /**
     * Traverses the object and converts the entity to array
     * @param object $object
     * @return mixed array
     */
    public function entityToArray ($object)
    {
        $arrReturn = array();
        if ($object) {
            foreach ($object as $dataRow) {
                $arrReturn[] = $this->toArray($dataRow);
            }
        }
        return $arrReturn;
    }

}

<?php

namespace Ead1;

/**
 * Classe para encapsular os dados de endereço.
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package Ead1
 */
class Filtro
{

    /**
     * Atributo que diz qual o label do filtro da pesquisa
     * @var String
     */
    public $st_label;

    /**
     * Atributo que diz qual o tipo do filtro da pesquisa
     * @var String
     */
    public $st_tipo;

    /**
     * Atributo que diz qual o valor do filtro da pesquisa
     * @var *
     */
    public $valor = array();

    /**
     * Atributo que diz qual a propriedade da pesquisa
     * @var *
     */
    public $st_propriedade;

    /**
     * @return the $st_label
     */

    /**
     * @return the $st_propriedade
     */
    public function getSt_propriedade ()
    {
        return $this->st_propriedade;
    }

    /**
     * @param $st_propriedade the $st_propriedade to set
     */
    public function setSt_propriedade ($st_propriedade)
    {
        $this->st_propriedade = $st_propriedade;
    }

    public function getSt_label ()
    {
        return $this->st_label;
    }

    /**
     * @return the $st_tipo
     */
    public function getSt_tipo ()
    {
        return $this->st_tipo;
    }

    /**
     * @return the $valor
     */
    public function getValor ()
    {
        return $this->valor;
    }

    /**
     * @param $st_label the $st_label to set
     */
    public function setSt_label ($st_label)
    {
        $this->st_label = $st_label;
    }

    /**
     * @param $st_tipo the $st_tipo to set
     */
    public function setSt_tipo ($st_tipo)
    {
        $this->st_tipo = $st_tipo;
    }

    /**
     * @param $valor the $valor to set
     */
    public function setValor ($valor)
    {
        $this->valor = $valor;
    }

}
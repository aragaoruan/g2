<?php

namespace Ead1;

interface Api {

	/**
	 * Método responsável por retornar todos os dados da Model/Collection/Consulta
	 */
	public function indexAction();

	/**
	 * Método responsável por retornar um item da Model/Collection/Consulta baseado no ID
	 */
	public function getAction();

	/**
	 * Método responsável por inserir dados na Model
	 */
	public function createAction();

	/**
	 * Método responsável por retornar atualizar os dados na Model
	 */
	public function updateAction();

	/**
	 * Método responsável por excluir os dados da Model/Collection/Consulta
	 */
	public function deleteAction();

	/**
	 * Método responsável por retornar todos os dados da Model/Collection/Consulta por página
	 */
	public function pagingAction();


}
<?php
/**
 * Classe de valores para transporte de valores
 * @author Arthur Cláudio de Almeida Pereira
 */
class Ead1_Boleto_Parameter extends Ead1_TO_Dinamico {
	
	/**
	 * Código de controle interno do estabelecimente
	 * O código deve possuir até 8 caracteres
	 * @var int
	 */
	public $nosso_numero		= '';
	
	/**
	 *  
	 * @var int
	 */
	public $numero_documento	= '';
	
	/**
	 * Data de vencimento do boleto
	 * @var String
	 */
	public $data_vencimento		= '';
	
	/**
	 * Data de emissão de um boleto
	 * @var String
	 */
	public $data_documento		= '';
	
	/**
	 * Data de processamento do boleto
	 * @var String
	 */
	public $data_processamento	= '';
	
	/**
	 * Valor total do boleto
	 * @var int
	 */
	public $valor_boleto		= '';
	
	/**
	 * Nome do cliente 
	 * @var String
	 */
	public $sacado				= '';
	
	/**
	 * Endereço do cliente
	 * @var String
	 */
	public $endereco1			= '';
	
	/**
	 * Complemento do endereço do cliente
	 * @var String
	 */
	public $endereco2			= '';
	
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $demonstrativo1		= '';
	
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $demonstrativo2		= '';
	
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $demonstrativo3		= '';
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $demonstrativo4		= '';
	
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $instrucoes1			= '';
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $instrucoes2			= '';
	/**
	 * Informação para o cliente
	 * @var String
	 */
	public $instrucoes3			= '';
	
	
	
	public $quantidade			= '';
	public $valor_unitario		= '';
	public $aceite				= '';
	public $especie				= 'R$';
	public $especie_doc			= '';
	
	/**
	 * Número da agência sem dígito
	 * @var int
	 */
	public $agencia				= '';
	
	/**
	 * Número da conta sem dígito
	 * @var unknown_type
	 */
	public $conta				= '';
	
	/**
	 * Código da carteira para emissão de boleto
	 * Os valores podem ser 175, 164, 104, 109, 178 ou 157
	 * @var int
	 */
	public $carteira			= '';
	
	/**
	 * Nome da empresa ( entidade ) que está emitindo o boleto
	 * @var String
	 */
	public $identificacao		= '';
	
	/**
	 * CPF / CNPJ da empresa/pessoa que está emitindo o boleto
	 * @var unknown_type
	 */
	public $cpf_cnpj			= '';
	
	/**
	 * Endereço da empresa/pessoa que está emitindo o boleto 
	 * @var String
	 */
	public $endereco			= '';
	
	/**
	 * Cidade e UF da empresa que está emitindo o boleto
	 * Deve ser informado no formato Cidade / Estado
	 * @var String
	 */
	public $cidade_uf			= ''; 
	
	/**
	 * Razão social da empresa que está emitindo o boleto
	 * @var String
	 */
	public $cedente				= '';
}

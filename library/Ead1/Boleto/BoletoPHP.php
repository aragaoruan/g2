<?php

class Ead1_Boleto_BoletoPHP extends Ead1_Boleto_Abstract
{

    const BOLETO_ITAU = '341';
    const BOLETO_BB = '001';
    const BOLETO_SANTANDER = '033';
    const BOLETO_BRADESCO = '237';
    const BOLETO_CEF = '104';
    const BOLETO_REAL = '356';


    /**
     * Realiza o mapeamento entre os arquivos da biblioteca BoletoPHP
     * e os bancos inseridos no bd.
     * @var array
     */
    private $_arMapeamentoBoleto = array(
        self::BOLETO_ITAU => 'boleto_itau',
        self::BOLETO_BB => 'boleto_bb',
        self::BOLETO_SANTANDER => 'boleto_santander_banespa',
        self::BOLETO_BRADESCO => 'boleto_bradesco',
        self::BOLETO_CEF => 'boleto_cef',
        self::BOLETO_REAL => 'boleto_real'
    );

    /**
     * Gera o boleto para impressão
     * @param $stBanco
     * @throws ErrorException
     */
    public function emitirBoleto($stBanco)
    {

        if ($this->checkDadosBoleto($stBanco)) {

            $caminhoArquivo = APPLICATION_PATH . '/../public/loja/boletophp/' . $this->_arMapeamentoBoleto[$stBanco] . '.php';

            if (file_exists($caminhoArquivo) === false) {
                throw new ErrorException('Erro ao buscar o arquivo para gerar o boleto a partir da BoletoPHP. Caminho: ' . $caminhoArquivo);
            }

            $dadosboleto = $this->_parameter->toArray();
            include $caminhoArquivo;
        }
    }


    /**
     * Verifica os dados do boleto
     * @param $stBanco
     * @return string
     * @throws ErrorException
     */
    private function checkDadosBoleto($stBanco)
    {

        if (empty($stBanco)) {
            throw new InvalidArgumentException('Informe o st_banco para emissão de boletos a partir da BoletoPHP');
        }

        if (array_key_exists($stBanco, $this->_arMapeamentoBoleto) === false) {
            throw new UnexpectedValueException('O st_banco informado não está inserido no array de boletos para emissão a partir da BoletoPHP');
        }

        return true;
    }


    /**
     * Método para retornar os dados do boleto
     * @param $stBanco
     * @return array
     * @throws ErrorException
     */
    public function retornarDadosBoleto($stBanco)
    {

        //verificações padrão
        if ($this->checkDadosBoleto($stBanco)) {

            //pega o nome do arquivo para gerar o boleto e troca o nome para funcoes_[nomedobanco]
            $caminhoArquivo = APPLICATION_PATH . '/../public/loja/boletophp/include/' . str_replace('boleto', 'funcoes', $this->_arMapeamentoBoleto[$stBanco]) . '.php';

            //verifica se o arquivo existe
            if (!file_exists($caminhoArquivo)) {
                throw new ErrorException('Erro ao buscar o arquivo para gerar o boleto a partir da BoletoPHP. Caminho: ' . $caminhoArquivo);
            }

            //pega os parametros e transforma em um array
            $dadosboleto = $this->_parameter->toArray();

            /*
             * inclue o arquivo de funções que gera os dados essenciais para o boleto
             * Esse arquivo atribui os valores na mesma variavel que foi setado os parametros
            */
            include $caminhoArquivo;

            //retorna a variavel com os novos valores
            return $dadosboleto;

        }
    }

}
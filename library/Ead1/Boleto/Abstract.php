<?php

/**
 * Classe de abstração para implementação de bibliotecas de geração de boletos
 * @author Arthur Cláudio de Almeida Pereira <arthur.almeida@ead1.com.br>
 */

abstract class Ead1_Boleto_Abstract 
{
	/**
	 * Armazena os parâmetros enviados ao boleto
	 * @var Ead1_Boleto_Parameter
	 */
	protected $_parameter;

	public function __construct( Ead1_Boleto_Parameter $parameter ) {
		$this->_parameter	= $parameter;
		$this->_validarCamposMininosObrigatorios();
	}
	
	private function _validarCamposMininosObrigatorios() {
		$camposObrigatorios	= array( 'nosso_numero', 'numero_documento', 'data_vencimento', 'data_processamento', 'valor_boleto'
									, 'sacado', 'agencia', 'conta', 'carteira', 'identificacao', 'cpf_cnpj', 'endereco'
									, 'cidade_uf', 'cedente' );
									
		$arAtributos		= $this->_parameter->toArray();
		foreach ( $arAtributos as $nomeAtributo => $valorAtributo ){
			if( in_array( $nomeAtributo, $camposObrigatorios ) ) {
				if( empty( $valorAtributo ) ) {
					throw new InvalidArgumentException( 'O parâmetro '. $nomeAtributo .' é obrigatório para geração do boleto' );
				}
			}
		}
	}
	
}
<?php

/**
 * @SWG\Definition(@SWG\Xml(name="Ead1_Mensageiro"))
 * @author Elcio Guimarães
 */
class Ead1_Mensageiro
{
    /**
     * Atributo que contém a URL do sistema
     * @SWG\Property(format="string")
     * @var mixed
     */
    public $urlfull;

    /**
     * Atributo que contém algo que será a resposta para a requisição
     * @SWG\Property(format="string")
     * @var mixed
     */
    public $mensagem;

    /**
     * Atributo que contém algo que será a resposta para a requisição na Thread
     * @SWG\Property(format="string")
     * @var mixed
     */
    public $dynMensagem;
    /**
     * Atributo que contém o tipo da resposta
     * @SWG\Property(format="integer", example="1")
     * @var Ead1_IMensageiro
     */
    public $tipo;
    /**
     * Atributo que contém um código opcional para a resposta da requisição
     * @SWG\Property(format="string")
     * @var mixed
     */
    public $codigo;
    /**
     * Atributo que contém um código opcional para a resposta da requisição
     * @SWG\Property(format="boolean")
     * @var boolean
     */
    public $logado;
    /**
     * Atributo que diz se o usuário logado é ou não super-usuário no sistema
     * @SWG\Property(format="string")
     * @var boolean
     */
    public $su;
    /**
     * ID do Objeto para indentificação no Backbone
     * @SWG\Property(format="integer")
     * @var int
     */
    public $id;
    /**
     * Tipo de retorno para o pnotify, constantes encontradas na \Ead1_IMensageiro
     * @SWG\Property(format="string", example="sucesso")
     * @var string
     */
    public $type;
    /**
     * Texto de retorno, normalmente uma mensagem de retorno do pnotify
     * @SWG\Property(format="string", example="sucesso")
     * @var string
     */
    public $text;
    /**
     * Título para a janela do pnotify
     * @SWG\Property(format="string", example="Sucesso")
     * @var string
     */
    public $title;

    /**
     * Método construtor da classe
     *
     * @param array $objeto
     * @param Ead1_IMensageiro $tipo
     * @param mixed $codigo
     */
    public function __construct($objeto = null, $tipo = null, $codigo = null)
    {
        $this->urlfull = Ead1_Ambiente::geral()->applicationUrl;

        if ($objeto instanceof Ead1_Mensageiro) {
            $tipo = $objeto->getTipo();
            $codigo = $objeto->getCodigo();
            $objeto = $objeto->getMensagem();
        }

        $this->setMensagem($objeto);
        $this->setTipo($tipo);
        $this->setCodigo($codigo);

        $this->setLogado();
        $this->setSu();
        $this->notificaErroEmail();
    }

    /**
     * Método que verifica se o usuário autenticado é su ou não
     */
    public static function su()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $config = new Zend_Config_Ini('../application/configs/su.ead1', 'producao');
            $superUsuarios = $config->su->toArray();

            $idUsuario = Zend_Auth::getInstance()->getIdentity();
            $existe = array_search($idUsuario, $superUsuarios);
            return ($existe === false ? false : true);
        } else {
            return false;
        }
    }

    /**
     * @return the $mensagem
     */
    public function getCurrent()
    {
        if (is_array($this->dynMensagem)) {
            $retorno = $this->dynMensagem[0];
            if (count($this->dynMensagem) == 1) {
                $this->dynMensagem = false;
            } else {
                array_shift($this->dynMensagem);
            }
        } elseif ($this->dynMensagem === null) {
            if (is_array($this->mensagem)) {
                $this->dynMensagem = array_values($this->mensagem);
            } else {
                $this->dynMensagem = $this->mensagem;
            }
            $retorno = $this->getCurrent();
        } else {
            $retorno = false;
        }
        return $retorno;
    }

    /**
     * Método que adciona um item ao array
     * @param mixed $mensagem the $mensagem to set
     * @param mixed $chave the $chave to set
     */
    public function addMensagem($mensagem = null, $chave = null)
    {
        if ($mensagem) {
            if ($chave) {
                $this->mensagem[$chave] = $mensagem;
            } else {
                $this->mensagem[] = $mensagem;
            }
        }
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Método que limpa todas as propriedades do objeto
     */
    public function limparTudo()
    {
        $this->limpar();
        $this->su = false;
        $this->logado = null;
    }

    /**
     * Método que limpa as propriedades mensagem, tipo e código
     */
    public function limpar()
    {
        $this->mensagem = null;
        $this->tipo = null;
        $this->codigo = null;

        return $this;
    }

    /**
     * Seta todas as propriedades novamente
     * @param mixed $mensagem
     * @param mixed $tipo
     * @param mixed $codigo
     * @return Ead1_Mensageiro
     */
    public function setMensageiro($mensagem = null, $tipo = null, $codigo = null)
    {
        $this->__construct($mensagem, $tipo, $codigo);
        return $this;
    }

    /**
     * @return the $mensagem
     */
    public function getMensagem($chave = null)
    {
        if ($chave) {
            return $this->mensagem[$chave];
        } else {
            return $this->mensagem;
        }
    }

    /**
     * @param $mensagem the $mensagem to set
     */
    public function setMensagem($mensagem)
    {

        if (is_array($mensagem)) {
            $this->mensagem = $mensagem;
        } else {
            $this->mensagem = array($mensagem);
        }
        return $this;
    }

    /**
     * @return the $tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param $tipo the $tipo to set
     */
    public function setTipo($tipo)
    {
        if (is_numeric($tipo)) {
            $this->tipo = $tipo;
        } else {
                $this->tipo = Ead1_IMensageiro::SUCESSO;
        }

        switch ($this->getTipo()) {
            case Ead1_IMensageiro::SUCESSO:
                $this->type = Ead1_IMensageiro::TYPE_SUCESSO;
                $this->title = Ead1_IMensageiro::TITLE_SUCESSO;
                $this->text = (is_string($this->getFirstMensagem()) ? $this->getFirstMensagem() : Ead1_IMensageiro::MENSAGEM_SUCESSO);
                break;
            case Ead1_IMensageiro::AVISO:
                $this->type = Ead1_IMensageiro::TYPE_AVISO;
                $this->title = Ead1_IMensageiro::TITLE_AVISO;
                $this->text = (is_string($this->getFirstMensagem()) ? $this->getFirstMensagem() : Ead1_IMensageiro::MENSAGEM_AVISO);
                break;
            case Ead1_IMensageiro::ERRO:
                $this->type = Ead1_IMensageiro::TYPE_ERRO;
                $this->title = Ead1_IMensageiro::TITLE_ERRO;
                $this->text = (is_string($this->getFirstMensagem()) ? $this->getFirstMensagem() : Ead1_IMensageiro::MENSAGEM_ERRO);
                break;
        }

        return $this;
    }

    /**
     * @return the $codigo
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param $codigo the $codigo to set
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * Método que envia um e-mail ao desenvolvedor notificando o erro gerado
     *
     */
    public function notificaErroEmail($enviar = false)
    {

        if (($this->getTipo() == Ead1_IMensageiro::ERRO && ($this->getCodigo() instanceof Exception)) || $enviar) {

            try {

                $to = new EntidadeTO();

                if ($to->getSessao()->id_entidade) {
                    $to->setId_entidade($to->getSessao()->id_entidade);
                    $to->fetch(true, true, true);
                }

                $factory = new Ead1_Mail_Config_Factory();
                $mail = new Zend_Mail('UTF-8');
                $mail->addTo('elciomgdf@gmail.com', 'Elcio Guimarães');

                if (Ead1_Ambiente::getAmbiente() == 'producao') {
                    $mail->addTo('felipe.toffolo@gmail.com', 'Toff');
                }

                $erro = 'Prezado desenvolvedor<br><br>Ocorreu um erro no Gestor 2. Abaixo estão as informações para que possamos corrigi-lo da melhor forma.<br><br>';
                $erro .= 'Entidade: <strong>' . $to->getSessao()->id_entidade . ' - ' . $to->getSt_nomeentidade() . '</strong><br>';
                $erro .= 'Usuário: <strong>' . $to->getSessao()->id_usuario . ' - ' . $to->getSessao()->nomeUsuario . '</strong><br>';
                $erro .= 'Erro do Mensageiro: <strong>' . $this->getFirstMensagem() . '</strong><br><br>';

                $e = $this->getCodigo();
                if ($e instanceof Exception) {
                    $erro .= 'Erro: <strong>' . $e->getMessage() . '</strong><br>Arquivo: <strong>' . $e->getFile() . '(' . $e->getLine() . ')</strong><br><br>';
                    $erro .= str_replace('#', '<br>#', $e->getTraceAsString());
                } else {
                    $erro .= 'Erro: <strong>' . var_export($e, true) . '</strong><br><br>';

                    $erro .= '<h2>Trace</h2>';

                    $trace = debug_backtrace();
                    if (!empty($trace)) {
                        foreach ($trace as $linha) {

                            $erro .= 'Trace: <strong>' . $linha['file'] . ' (' . $linha['line'] . ')</strong><br>';
                            if ($linha['object'] instanceof Ead1_Mensageiro) {
                                if (is_array($linha['object']->getFirstMensagem())) {
                                    $erro .= 'Mensageiro: <strong>' . var_export($linha['object']->getFirstMensagem(), true) . '</strong><br>';
                                } else {
                                    $erro .= 'Mensageiro: <strong>' . $linha['object']->getFirstMensagem() . '</strong><br>';
                                }
                            }
                            $erro .= '<br><br>';

                        }
                    }
                }
                $mail->setBodyHtml($erro);
                $mail->setSubject(strtoupper(Ead1_Ambiente::getAmbiente()) . ': Erro no Gestor 2');
                $factory->sendMail($mail);


            } catch (Exception $e) {
                return true;
            }

        }
    }

    /**
     * Método que retorna o primeiro indice do array de mensagem
     * @return Mixed $mensagem
     */
    public function getFirstMensagem()
    {
        $mensagem = null;
        if (!empty($this->mensagem)) {
            $mensagem = isset($this->mensagem[0]) ? $this->mensagem[0] : $this->mensagem;
        }
        return $mensagem;
    }

    /**
     * Método que converte o objeto em json
     */
    public function toJson($var = null)
    {
        return json_encode(get_object_vars(($var ? $var : $this)));
    }

    /**
     * Método que coloca todos os objetos como array até o primeiro nível
     */
    public function toArrayAll()
    {
        $mensagemArray = $this->__toArray($this->getMensagem());
        return new Ead1_Mensageiro($mensagemArray, $this->getTipo(), $this->getCodigo());
    }

    /**
     * Método que transforma recursivamente os elementos de uma mensagem em array
     * @param mixed $var
     */
    private function __toArray($var)
    {
        if (is_array($var) || is_object($var)) {
            if ($var instanceOf Ead1_TO) {
                $arDefinitivo = $var->toArrayDAO();
            } else {
                if (is_object($var)) {
                    $arNovo = $this->toArray($var);
                } elseif (is_array($var)) {
                    $arNovo = $var;
                }
                $arDefinitivo = array();
                if (is_array($arNovo)) {
                    foreach ($arNovo as $chave => $valor) {
                        $arDefinitivo[$chave] = $this->__toArray($valor);
                    }
                } else {
                    $arDefinitivo = $var;
                }
            }
            return $arDefinitivo;
        } else {
            return $var;
        }

    }

    /**
     * Método que converte o objeto em array
     */
    public function toArray($var = null)
    {
        return get_object_vars(($var ? $var : $this));
    }

    /**
     * @return the $logado
     */
    public function getLogado()
    {
        return $this->logado;
    }

    /**
     * Método que verifica se o usuário está ou não logado
     */
    public function setLogado()
    {
        $this->logado = Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * @return the $su
     */
    public function getSu()
    {
        return $this->su;
    }

    /**
     * Método que verifica se o usuário é ou não super usuário e seta o atributo su com true ou false
     */
    public function setSu()
    {
        if ($this->logado) {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/su.ead1', 'producao');
            $superUsuarios = $config->su->toArray();

            $idUsuario = Zend_Auth::getInstance()->getIdentity();
            $existe = array_search($idUsuario, $superUsuarios);
            $this->su = ($existe === false ? false : true);
        } else {
            $this->su = false;
        }
    }

    /**
     * Retorna o objeto pai
     * @return Ead1_Mensageiro
     */
    public function toMensageiro()
    {
        return new Ead1_Mensageiro($this);
    }

    /**
     * Método que subtrai o conteudo do mensageiro e de acordo com o tipo lança excessões
     * @param $exception - 0 Erro, 2 Aviso, 3 Todas
     * @return Ead1_Mensageiro
     */
    public function subtractMensageiro($exception = 3)
    {
        switch ($this->tipo) {
            case Ead1_IMensageiro::AVISO:
                if ($exception == 3 || $exception == Ead1_IMensageiro::AVISO) {
                    THROW new Zend_Validate_Exception($this->getFirstMensagem());
                }
                break;
            case Ead1_IMensageiro::ERRO:
                if ($exception == 3 || $exception == Ead1_IMensageiro::ERRO) {
                    if ($this->getCodigo()) {
                        THROW new Zend_Exception($this->getCodigo());
                    } else {
                        THROW new Zend_Exception($this->getFirstMensagem());
                    }
                }
                break;
        }
        return $this;
    }
}

<?php
/**
 * Super RO
 * @author edermariano
 *
 */
class Ead1_RO{
	
	/**
	 * Array
	 * @var Array
	 */
	public $arrayRetorno = array();
	
	/**
	 * Método que retorna os países
	 * @return Ead1_Mensageiro
	 */
	public function pais(){
		$tbPais = new PaisORM();
		return new Ead1_Mensageiro($tbPais->consulta(new PaisTO()));		
	}
	
	/**
	 * Método que lista todas as UFs
	 * @return Ead1_Mensageiro
	 */
	public function uf(){
		$tbUf = new UfORM();
		return new Ead1_Mensageiro($tbUf->consulta(new UfTO()));
	}
	
	/**
	 * Método que lista todos os municípios da UF
	 * @param MunicipioTO $to
	 * @return Ead1_Mensageiro
	 */
	public function municipios(MunicipioTO $to){
		if(!$to->getNu_codigoibge()){
			$to->nu_codigoibge = null;
		}
		
		$tbMunicipio = new MunicipioORM();
		return new Ead1_Mensageiro($tbMunicipio->consulta($to), Ead1_IMensageiro::SUCESSO,$tbMunicipio->montarWhere($to));
	}
	
	/**
	 * Método que define o tipo de telefone
	 * @return Ead1_Mensageiro
	 */
	public function tipoTelefone(){
		$tbTipoTelefone = new TipoTelefoneORM();
		return new Ead1_Mensageiro($tbTipoTelefone->consulta(new TipoTelefoneTO()));
	}
	
	/**
	 * Método que define o tipo de redes sociais
	 * @return Ead1_Mensageiro
	 */
	public function tipoRedeSocial(){
		$tbTipoRedeSocial = new TipoRedeSocialORM();
		return new Ead1_Mensageiro($tbTipoRedeSocial->consulta(new TipoRedeSocialTO()));
	}
	
	/**
	 * 
	 * RO para consulta de bancos
	 * @param string $order com o campo da tabela de banco, para fazer a ordernação
	 * @throws Zend_Exception
	 * @return Ead1_Mensageiro
	 */
	public function banco($order = null){
		try{
			$tbBanco = new BancoORM();
			if( $order !== null ){
				$order .= " ASC";
				$retornoBanco = $tbBanco->fetchAll( $tbBanco->select()->order( $order ) )->toArray();
				if( !$retornoBanco ){
					throw new Zend_Exception("Erro ao retornar dados bancários ORDER!");
				}
				foreach($retornoBanco as $arBanco){
					$to = new BancoTO();
					$to->setId_banco($arBanco['id_banco']);
					$to->setSt_nomebanco($arBanco['st_nomebanco']);
					$to->setSt_banco($arBanco['st_banco']);
					$arrTO[] = $to;
				}
				return new Ead1_Mensageiro($arrTO, Ead1_IMensageiro::SUCESSO );
			}else{
				$bancos = $tbBanco->consulta(new BancoTO());
				if(!$bancos){
					return new Ead1_Mensageiro("Nenhum Registro de Banco encontrado!",Ead1_IMensageiro::AVISO);
				}
				return new Ead1_Mensageiro($bancos, Ead1_IMensageiro::SUCESSO );
			}
		}catch(Zend_Exception $e){
			return new Ead1_Mensageiro("Erro ao retornar o(s) dado(s) bancário(s)!",Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * RO para consulta de tipos de contas bancárias
	 * @return Ead1_Mensageiro
	 */
	public function tipoConta(){
		$tbBanco = new TipoDeContaORM();
		return new Ead1_Mensageiro($tbBanco->consulta(new TipoDeContaTO()));
	}
	
	/**
	 * Método que retorna os tipos Sanguíneos
	 * @return Ead1_Mensageiro
	 */
	public function tipoSanguineo(){
		$tbTipoSanguineo = new TipoSanguineoORM();
		return new Ead1_Mensageiro($tbTipoSanguineo->consulta(new TipoSanguineoTO()));
	}
	
	/**
	 * Método que retorna as Etnias
	 * @return Ead1_Mensageiro
	 */
	public function etnia(){
		$tbEtnia = new EtniaORM();
		return new Ead1_Mensageiro($tbEtnia->consulta(new EtniaTO()));
	}
	
	/**
	 * Método que retorna os Estados Civis
	 * @return Ead1_Mensageiro
	 */
	public function estadoCivil(){
		$tbEstadoCivil = new EstadoCivilORM();
		return new Ead1_Mensageiro($tbEstadoCivil->consulta(new EstadoCivilTO()));
	}
	
	/**
	 * Método que retorna o situacao
	 * @param SituacaoTO de preferencia com $st_tabela preenchido
	 * @return Ead1_Mensageiro
	 */
	public function situacao(SituacaoTO $sTO){
		$tbSituacao = new SituacaoORM();
		return new Ead1_Mensageiro($tbSituacao->consulta($sTO));
	}
	
	/**
	 * Método que retorna o tipo de trilha
	 * @return Ead1_Mensageiro
	 */
	public function tipoTrilha() {
		$tbTipoTrilha = new TipoTrilhaORM();
		return new Ead1_Mensageiro($tbTipoTrilha->consulta(new TipoTrilhaTO()));
	}
	
	/**
	 * Metodo que retorna a Classe da Entidade
	 * @param EntidadeRelacaoTO $erTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaClasseDaEntidade(EntidadeRelacaoTO  $erTO){
		$ro = new EntidadeRO();
		return $ro->retornaClasseDaEntidade($erTO);
	}
	
	/**
	 * Metodo que retorna os tipos de Endereço de Pessoa
	 * @return Ead1_Mensageiro
	 */
	public function tipoEndereco(TipoEnderecoTO $teTO){
		$orm = new TipoEnderecoORM();
		$teTO->setId_categoriaendereco($teTO->getId_categoriaendereco());
		return new Ead1_Mensageiro($orm->consulta($teTO));
	}
	
	/**
	 * Metodo que retorna os níveis de ensino
	 * @param Boolean $pesquisa
	 * @param String $consulta
	 * @return Ead1_Mensageiro
	 */
	public function nivelEnsino($pesquisa = false, $consulta = NULL){
		$neTO = new NivelEnsinoTO();
		$pesqBO = new PesquisarBO();
		return $pesqBO->retornaNivelEnsino($neTO,$pesquisa,$consulta);
	}
	
	/**
	 * Método que retorna séries de um nível de ensino.
	 * @param VwSerieNivelEnsinoTO $vsneTO
	 * @return Ead1_Mensageiro
	 */
	public function serieNivelEnsino(VwSerieNivelEnsinoTO $vsneTO) {
		$orm = new VwSerieNivelEnsinoORM();
		return new Ead1_Mensageiro($orm->consulta($vsneTO));
	}
	
	/**
	 * Método que retorna séries
	 * @param SerieTO $sTO
	 * @return Ead1_Mensageiro
	 */
	public function cbSerie(SerieTO $sTO){
		$orm = new SerieORM();
		return new Ead1_Mensageiro($orm->consulta($sTO));
	}
	
	/**
	 * Metodo que retorna as áreas de atuação
	 * @return Ead1_Mensageiro
	 */
	public function areaAtuacao(){
		$orm = new AreaAtuacaoORM();
		return new Ead1_Mensageiro($orm->consulta(new AreaAtuacaoTO()));
	}
	
	/**
	 * Método que retorna os tipos de registros para alimentar o combo
	 */
	public function tipoRegistro(){
		$orm = new RegistroPessoaORM();
		return new Ead1_Mensageiro($orm->consulta(new RegistroPessoaTO()));
	}
	
	/**
	 * Método que retorna as entidade filhas
	 * @param VwEntidadeClasseTO $ecTO
	 * @return Ead1_Mensageiro
	 * @todo criar a pesquisa de entidade filhas, que ainda não está certa essa busca, entidade classe verificar se é ele mesmo 20/12/2010
	 */
	public function entidadeFilhas(VwEntidadeClasseTO $ecTO = null){
		$bo = new PesquisarBO();
		if(!$ecTO){
			$ecTO = new VwEntidadeClasseTO();
		}
		return $bo->pesquisarEntidadeFilha($ecTO);
	}
	
	
	/**
	 * Método que retorna as entidade filhas vinculadas a um projeto
	 * @param VwProjetoEntidadeTO $ecTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarVwProjetoEntidadeProjeto(VwProjetoEntidadeTO $peTO = null){
		
		$bo = new ProjetoPedagogicoBO();
		if(!$peTO){
			$peTO = new VwProjetoEntidadeTO();
		}

// 		return $bo->pesquisarEntidadeFilha($peTO);
		return $bo->retornarVwProjetoEntidadeProjeto($peTO);
		
	}
	
	/**
	 * Retorna as áreas do conhecimento
	 * @param AreaConhecimentoTO $acTO
	 * @return Ead1_Mensageiro
	 */
	public function areaCombo(AreaConhecimentoTO $acTO = null){
		$bo = new AreaBO();
		if(!$acTO){
			$acTO = new AreaConhecimentoTO();
		}
		return $bo->retornaArea($acTO);
	}
	
	/**
	 * Retorna os emails, utilizado no combo de e-mail padrão
	 * @param UsuarioTO $uTO
	 * @return Ead1_Mensageiro
	 */
	public function comboEmail(UsuarioTO $uTO){
		$bo = new PessoaBO();
		return new Ead1_Mensageiro($bo->retornaEmails($uTO));
	}
	
	/**
	 * Retorna o(s) telefone(s), utilizado no combo de telefone padrão
	 * @param UsuarioTO $uTO
	 * @return Ead1_Mensageiro
	 */
	public function comboTelefone(UsuarioTO $uTO){
		$bo = new PessoaBO();
		return new Ead1_Mensageiro($bo->retornaTelefones($uTO));
	}
	
	/**
	 * Retorna as áreas do conhecimento, junto como nível de ensino e série
	 * @return Ead1_Mensageiro
	 */
	public function areaSerieNivel(){
		$orm = new VwAreaConhecimentoNivelSerieORM();
		$to = new VwAreaConhecimentoNivelSerieTO();
		return new Ead1_Mensageiro($orm->consulta($to));
	}
	
	/**
	 * Retorna a pessoa para o combo de auto complete
	 * @param VwPesquisarProfessorTO $ppTO
	 * @return Ead1_Mensageiro
	 */
	public function retornarProfessorAutoComplete(VwPesquisarProfessorTO $ppTO){
		$bo = new PesquisarBO();
		return $bo->retornaProfessor($ppTO);
	}
	
	/**
	 * Método que retorna o Endereço com base no CEP informado
	 */
	public function retornarEnderecoCEP(EnderecoTO $eTO){
		$bo = new Ead1_BO();
		return $bo->cepService($eTO);
	}
	
	/**
	 * Método que retorna os sistemas.
	 */
	public function retornarSistema(SistemaTO $sTO){
		$orm = new SistemaORM();
		return new Ead1_Mensageiro($orm->consulta($sTO));
	}
	
	/**
	 * Método que retorna um array de evoluções.
	 */
	public function retornarEvolucao(EvolucaoTO $eTO){
		try{
			$orm = new EvolucaoORM();
			$resultado = $orm->consulta($eTO);
			
			if($resultado){
				return new Ead1_Mensageiro($resultado);				
			}else{
				return new Ead1_Mensageiro('Nenhum Registro de Evolução Encontrado!',Ead1_IMensageiro::AVISO);
			}
		}catch (Zend_Exception $e){
			return new Ead1_Mensageiro('Erro ao Retornar Evolução!',Ead1_IMensageiro::ERRO,$e->getMessage());
		}
	}
	
	/**
	 * Metodo que retorna a medida do tempo de conclusão do projeto pedagógico.
	 * @return Ead1_Mensageiro
	 */
	public function medidaTempoConclusao(){
		$orm = new MedidaTempoConclusaoORM();
		return new Ead1_Mensageiro($orm->consulta(new MedidaTempoConclusaoTO()));
	}
	
	/**
	 * Retorno de combo perfil de usuario
	 * @param UsuarioTO $uTO
	 * @return Ead1_Mensageiro
	 */
	public function retornaUsuarioPerfil(UsuarioTO $uTO){
		$bo = new PerfilBO();
		return $bo->retornaUsuarioPerfil($uTO);
	}
	
	/**
	 * Método que retorna variáveis do TO passado.
	 * @return Array $variaveis;
	 */
	public function retornarVariaveisTO($to){
		if(class_exists($to)) {
			$reflection = new ReflectionClass(new $to());
			$variaveis 	= $reflection->getProperties();
			$i = 0;
			foreach ($variaveis as $propriedade) {
				$atributos[$i] = $propriedade->getName();
				$i++;
			}
			return new Ead1_Mensageiro($atributos);
		} else {
			return new Ead1_Mensageiro("Classe $to não existe!", Ead1_IMensageiro::ERRO);
		}
	}
	
	
	/**
	 * Realiza o upload de arquivos
	 * Pega o arquivo informado e salva na pasta informada. 
	 * @param ArquivoTO $arquivoTO
	 * @return Ead1_Mensageiro
	 */
	public function uploadBasico( ArquivoTO $arquivoTO ) {
		$ead1BO	= new Ead1_BO();
		return $ead1BO->uploadBasico( $arquivoTO, false ); 	
	}
	
	public function retornarTeste(){
		return new Ead1_Mensageiro("Teste Rodrigues!");	
	}
}
<?php

namespace Ead1\SMS;
/**
 * Class Ead1_SMS
 * Classe para envio de sms
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class SMS extends \WsBO
{

    /**
     * Método para enviar mensgem sms individual
     * @param \Ead1_TO_SMS $smsTo
     * @return mixed
     * @throws \Exception
     */
    public static function send(\Ead1_TO_SMS $smsTo)
    {
        try {
            //valida os dados
            if (self::validaDados($smsTo)) {
                //pega a url que foi setado na classe
                $url = $smsTo->getUrlApi();
                //monta os parametros da url
                $url .= "?action={$smsTo->getAction()}&lgn={$smsTo->getLogin()}&pwd={$smsTo->getSenha()}&msg={$smsTo->getMensagem()}&numbers={$smsTo->getSendTo()}";
                //inicia o curl
                $cUrl = curl_init($url);
                curl_setopt_array($cUrl, array(
                    CURLOPT_HEADER => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_RETURNTRANSFER => true
                ));
                $response = curl_exec($cUrl);
                curl_close($cUrl);
                //pega a resposta e decodifica o json
                $decoded = json_decode($response);
                return $decoded;
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Método para verificar os dados passados
     * @param \Ead1_TO_SMS $smsTo
     * @return bool
     * @throws \Exception
     */
    private static function validaDados(\Ead1_TO_SMS $smsTo)
    {
        try {
            //verifica se foi passado a url da api
            if (!$smsTo->getUrlApi()) {
                throw new \Exception("Url da API do SMS não informada, verifique as configurações de integração da entidade.");
            }
            if (!$smsTo->getAction()) {
                throw new \Exception("Ação para execução do serviço de SMS não informada, verifique os parametros passados.");
            }
            if (!$smsTo->getLogin()) {
                throw new \Exception("Login para autenticação no serviço não informado, verifique as configurações de integração da entidade.");
            }
            if (!$smsTo->getSenha()) {
                throw new \Exception("Senha para autenticação no serviço não informado, verifique as configurações de integração da entidade.");
            }
            if (!$smsTo->getMensagem()) {
                throw new \Exception("Mensagem para envio de SMS não informada, verifique os parametros passados.");
            }
            if (!$smsTo->getSendTo()) {
                throw new \Exception("Destinatário não informado, verifique os parametros passados.");
            }
            return true;
        } catch (\Exception $e) {
            throw new \Exception("Erro nos parametros para enviar SMS. " . $e->getMessage());
        }
    }

}
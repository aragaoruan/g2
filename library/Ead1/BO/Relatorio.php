<?php
/**
 * 
 * @author edermariano
 *
 */
class Ead1_BO_Relatorio extends Ead1_BO{
	
	const EAD1_MENSAGEIRO = 'Ead1_Mensageiro';
	const MATRIZ = 'array';
	const TEXTO = 'string';
	
	private $tipoRetorno;
	private $tipoPaginacao;
	private $formatoSaida;
	private $dadosPaginacao;
	
	/**
	 * Método construtor para o gerador de relatório
	 * @param mixed $labels
	 * @param mixed $dados
	 * @param mixed $paginacao
	 * @param mixed $formato
	 * @param mixed $filtros
	 */
	public function __construct($labels = null, $dados = null, $paginacao = null, $formato = EAD1_MENSAGEIRO, $filtros = null){
		$to = new Ead1_TO_Relatorio();
		$to->setLabels($labels);
		$to->setDados($dados);
		$to->setFiltros($filtros);
		$to->setLabels($labels);
		$to->setLabels($labels);
	}
	
	/**
	 * Método que escolhe o tipo de retorno
	 */
	public function setTipoRetorno($tipoRetorno = EAD1_MENSAGEIRO){
		$this->tipoRetorno = $tipoRetorno;
	}
	
	/**
	 * Método que gera o relatório com base em filtros preenchidos de pesquisa
	 * 	Os dados deverão vir filtrados por uma consulta prévia com os filtros setados
	 * 	Os Labels deverão vir setados
	 * 	Os filtros são opicionais e servem apenas para a exibição no relatório
	 * 
	 * OBS.: Não é realizado filtro por aqui.
	 * 	
	 * @param String|Array|Zend_Db_Table_Rowset $labels
	 * @param String|Array|Zend_Db_Table_Rowset $dados
	 * @param String|Array|Zend_Db_Table_Rowset $filtros
	 */
	public function gerarRelatorio($labels, $dados, $filtros = null){
		if($dados instanceof Zend_Db_Table_Rowset){
			$this->zendDbRowset($dados);
		}else if(is_array($dados)){
			$this->zendDbRowset($dados);			
		}
	}
	
	/**
	 * 
	 * @param $zdtr
	 */
	private function zendDbRowset(Zend_Db_Table_Rowset $zdtr){
		
	}
}
<?php
/**
 * Classe de Interface de variaveis para substituição nos textos do sistema
 * @author Eduardo Romão - ejushiro@gmail.com
 */
interface Ead1_ITextosVar{
		const NOME_ALUNO = '/#nome_aluno#/';
		const SEXO_ALUNO = '/#sexo_aluno#/';
		const MATRICULA_ALUNO = '/#matricula_aluno#/';
		const NACIONALIDADE_ALUNO = '/#nacionalidade_aluno#/';
		const RG_ALUNO = '/#rg_aluno#/';
		const CPF_ALUNO = '/#cpf_aluno#/';
		const DATA_NASCIMENTO_ALUNO = '/#data_nascimento_aluno#/';
		const NATURALIDADE_ALUNO = '/#naturalidade_aluno#/';
		const ORGAO_EXPEDIDOR_ALUNO = '/#orgao_expedidor_aluno#/';
		const UF_NATURALIDADE_ALUNO = '/#uf_naturalidade_aluno#/';
		const UF_ORGAO_EXPEDIDOR_ALUNO = '/#uf_orgao_expedidor_aluno#/';
		const PROJETOPEDAGOGICO_ALUNO = '/#projetopedagogico_aluno#/';
		const GRID_HISTORICO_ALUNO = '/#grid_historico_aluno#/';
		const GRID_APROVEITAMENTO_ALUNO = '/#grid_aproveitamento_aluno#/';
		const PORTARIA = '/#portaria#/';
		const LEI = '/#lei#/';
		const RESOLUCAO = '/#resolucao#/';
		const PARECER = '/#parecer#/';
		const ID_CONTRATO = '/#id_contrato#/';
		const TELEFONE_ALUNO = '/#telefone_aluno#/';
		const DATA_HOJE = '/#data_hoje#/';
		const EMAIL_ALUNO = '/#email_aluno#/';
		const ENDERECO_ALUNO = '/#endereco_aluno#/'; 
		const DISCIPLINA_ALUNO = '/#disciplina_aluno#/';
		const DATA_INICIO_DISCIPLINA = '/#data_inicio_disciplina#/';
		const NOME_ENTIDADE = '/#nome_entidade#/';
		const CNPJ_ENTIDADE = '/#cnpj_entidade#/';
		const ENDERECO_ENTIDADE = '/#endereco_entidade#/';
		const RAZAO_SOCIAL_ENTIDADE = '/#razao_social_entidade#/';
		const LOGO_ENTIDADE = '/#logo_entidade#/';
		const TELEFONE_ENTIDADE = '/#telefone_entidade#/';
		const EMAIL_ENTIDADE = '/#email_entidade#/';
		const SITE_ENTIDADE = '/#site_entidade#/';
		
}
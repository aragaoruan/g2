<?php
/**
 * Classe para geração de QRCode usando a api do Google
 * @author Eder Lamar
 *
 */
class Ead1_Qrcode_Gerador{
	
	/**
	 * Atributo que contém a informação do QR-Code
	 * @var string
	 */
	private $data;
	
	/**
	 * Criando código com metadata de link
	 * @param string $url link
	 */
	public function link($url){
		if (preg_match('/^http:\/\//', $url) || preg_match('/^https:\/\//', $url)){
			$this->data = $url;
		}else{
			$this->data = "http://".$url;
		}
	}
	
	/**
	 * Criando dado e registrando o metadata
	 * @param string $title
	 * @param string $url
	 */
	public function bookmark($title, $url){
		$this->data = "MEBKM:TITLE:".$title.";URL:".$url.";;";
	}
	
	/**
	 * Gerar QR Code através de texto
	 * @param string $text
	 */
	public function text($text){
		$this->data = $text;
	}
	
	/**
	 * Gerador de QR Code com metadata para SMS
	 * @param string $phone telefone
	 * @param string $text texto
	 */
	public function sms($phone, $text){
		$this->data = "SMSTO:".$phone.":".$text;
	}
	
	/**
	 * Gerador de QR Code com número de telefone
	 * @param string $phone número de telefone
	 */
	public function phone_number($phone){
		$this->data = "TEL:".$phone;
	}
	
	/**
	 * Gerador de QR Code com informações de contato
	 * @param string $nome Nome do contato
	 * @param string $endereco endereço
	 * @param string $telefone telefone
	 * @param string $email email
	 */
	public function contact_info($nome, $endereco, $telefone, $email){
		$this->data = "MECARD:N:".$nome.";ADR:".$endereco.";TEL:".$telefone.";EMAIL:".$email.";;";
	}
	
	/**
	 * Gerador de QR-Code com metadado para mensagem de e-mail
	 * @param string $email e-mail
	 * @param string $assunto Assunto do e-mail
	 * @param string $texto texto do e-mail
	 */
	public function email($email, $assunto, $texto){
		$this->data = "MATMSG:TO:".$email.";SUB:".$assunto.";BODY:".$texto.";;";
	}
	
	/**
	 * Gerador de QR-Code com metadado para posição geográfica
	 * @param string $lat Latitude
	 * @param string $lon Longitude
	 * @param string $height Altitude
	 */
	public function geo($lat, $lon, $height){
		$this->data = "GEO:".$lat.",".$lon.",".$height;
	}
	
	/**
	 * Gerador de QR-Code com metadado para configuração de WiFi
	 * @param string $tipo Tipo
	 * @param string $ssid SSID
	 * @param string $senha Senha
	 */
	public function wifi($tipo, $ssid, $senha){
		$this->data = "WIFI:T:".$tipo.";S".$ssid.";".$senha.";;";
	}
	
	/**
	 * Gerador de QR-Code com metadado para configuração de i-appli
	 * @param string $adf
	 * @param string $cmd
	 * @param string $param
	 */
	public function iappli($adf, $cmd, $param){
		$param_str = "";
		foreach($param as $val){
			$param_str .= "PARAM:".$val["name"].",".$val["value"].";";
		}
		$this->data = "LAPL:ADFURL:".$adf.";CMD:".$cmd.";".$param_str.";";
	}
	
	/**
	 * Gerador de QR-Code com metadado para criação de gif, jpg ou smf ou MFi do Toruca
	 * @param string $type tipo
	 * @param string $size tamanho
	 * @param string $content conteúdo
	 */
	public function content($type, $size, $content){
		$this->data = "CNTS:TYPE:".$type.";LNG:".$size.";BODY:".$content.";;";
	}
	
	/**
	 * Pegar a imagem
	 * @param string $size tamanho
	 * @param string $EC_level nível de erro
	 * @param string $margin margem
	 */
	public function getImage($size = 150, $EC_level = 'L', $margin = '0'){
		$ch = curl_init();
		$this->data = urlencode($this->data); 
		curl_setopt($ch, CURLOPT_URL, 'http://chart.apis.google.com/chart');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'chs='.$size.'x'.$size.'&cht=qr&chld='.$EC_level.'|'.$margin.'&chl='.$this->data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	
	/**
	 * Método que retorna o link da imagem
	 * @param string $size tamanho
	 * @param string $EC_level nível de erro
	 * @param string $margin margem
	 */
	public function getLink($size = 150, $EC_level = 'L', $margin = '0'){
		$this->data = urlencode($this->data); 
		return 'http://chart.apis.google.com/chart?chs='.$size.'x'.$size.'&cht=qr&chld='.$EC_level.'|'.$margin.'&chl='.$this->data;
	}
	
	/**
	 * Método para baixar a imagem
	 * @param string $file nome do arquivo
	 */
	public function download_image($file){
		
		header('Content-Description: File Transfer');
		header('Content-Type: image/png');
		header('Content-Disposition: attachment; filename=QRcode.png');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		echo $file;
	}
}
?>
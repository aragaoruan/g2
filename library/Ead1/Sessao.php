<?php

/**
 * Classe de Controle de Sessao
 * @author eduardoromao
 */
class Ead1_Sessao
{

    /**
     * Método que inicia a sessao do Usuario
     */
    public static function sessionStart()
    {
        Zend_Session::start();
    }

    /**
     * Método que destroi a sessao do usuario
     */
    public static function sessionDestroy()
    {
        Zend_Session::namespaceUnset('usuario');
        Zend_Session::namespaceUnset('projetopedagogico');
        Zend_Session::namespaceUnset('entidade');
        Zend_Session::namespaceUnset('perfil');
        Zend_Session::namespaceUnset('moodle');
        Zend_Session::namespaceUnset('carrinho');
        Zend_Session::namespaceUnset('geral');
        Zend_Session::namespaceUnset('mensagem');
        Zend_Session::namespaceUnset('menu');
        Zend_Session::namespaceUnset('salas');
    }

    /**
     * Método que gera os dados de Sessão do Carrinho de Compras para Loja
     * @param array $carrinho
     */
    public static function setSessionCarrinho($carrinho)
    {

        $ead1_carrinho = new Ead1_Sessao_Carrinho($carrinho);
        $p_carrinho = new Zend_Session_Namespace('carrinho');
        foreach ($carrinho as $chave => $valor) {
            if (method_exists($ead1_carrinho, 'set' . $chave)) {
                $set = 'set' . $chave;
                $get = 'get' . $chave;
                $ead1_carrinho->{$set}($valor);
                $p_carrinho->{$chave} = $ead1_carrinho->{$get}();
            } else {
                $p_carrinho->{$chave} = $valor;
            }
        }
    }

    /**
     * Método Estatico que retorna a sessao do Carrinho de Compras para Loja
     * @return Ead1_Sessao_Carrinho
     */
    public static function getSessaoCarrinho()
    {
        self::sessionStart();
        return new Ead1_Sessao_Carrinho(Zend_Session::namespaceGet('carrinho'));
    }


    /**
     * Método que gera os dados de Sessão de Usuário para o Portal do Aluno
     * @param array $usuario
     */
    public static function setSessionUsuario($usuario)
    {
        $p_usuario = new Zend_Session_Namespace('usuario');
        foreach ($usuario as $chave => $valor) {
            $p_usuario->$chave = $valor;
        }
    }

    /**
     * Método que gera os dados de Sessão de Entidade para o Portal do Aluno
     * @param array $entidade
     */
    public static function setSessionEntidade($entidade)
    {
        $p_entidade = new Zend_Session_Namespace('entidade');
        foreach ($entidade as $chave => $valor) {
            $p_entidade->$chave = $valor;
        }

        if (file_exists(APPLICATION_REAL_PATH . '/layout/' . $p_entidade->id_entidade . '/layout.ok')):
            $p_entidade->st_layout = $p_entidade->id_entidade;
        else:
            $p_entidade->st_layout = 'padrao';
        endif;
    }

    /**
     * Método que gera os dados de Sessão de perfil para o Portal do Aluno
     * @param array $perfil
     */
    public static function setSessionPerfil($perfil)
    {
        $p_perfil = new Zend_Session_Namespace('perfil');
        foreach ($perfil as $chave => $valor) {
            $p_perfil->{$chave} = $valor;
        }

    }

    /**
     * Método que gera os dados de Sessão de projeto pedagógico para o Portal do Aluno
     * @param array $projeto
     */
    public static function setSessionProjetoPedagogico($projeto)
    {
        $p_projetopedagogico = new Zend_Session_Namespace('projetopedagogico');
        foreach ($projeto as $chave => $valor) {
            $p_projetopedagogico->{$chave} = $valor;
        }
    }

    /**
     * Método que gera os dados de Sessão gerais para o Portal do Aluno
     * @param array $dados
     */
    public static function setSessionGeral($dados)
    {
        $geral = new Zend_Session_Namespace('geral');
        foreach ($dados as $chave => $valor) {
            //if (!isset($geral->$chave)) {
                $geral->$chave = $valor;
            //}
        }
    }


    /**
     * Método que gera os dados de Sessão de mensagem para o Portal do Aluno
     * @param array $perfil
     */
    public static function setSessionMensagem($item)
    {
        $m_msg = new Zend_Session_Namespace('mensagem');
        foreach ($item as $k => $valor) {
            $m_msg->{$k} = $valor;
        }
    }

    /**
     * Método que gera os dados de Sessão de menu para o Portal do Aluno
     * @param array $usuario
     */
    public static function setSessionMenu($dados)
    {
        $p_menu = new Zend_Session_Namespace('menu');
        foreach ($dados as $chave => $valor) {
            $p_menu->$chave = $valor;
        }
    }

    /**
     * Método que gera os dados de Sessão de salas do Portal do Aluno
     * @param array $dados
     */
    public static function setSessionSalas($dados)
    {
        $p_menu = new Zend_Session_Namespace('salas');
        foreach ($dados as $chave => $valor) {
            $p_menu->$chave = $valor;
        }
    }


    /**
     * Método Estatico que retorna a sessao de usuario
     * @return Ead1_Sessao_Usuario
     */
    public static function getSessaoUsuario()
    {
        self::sessionStart();
        return new Ead1_Sessao_Usuario(Zend_Session::namespaceGet('usuario'));
    }

    /**
     * Método Estatico que retorna a sessao de entidade
     * @return Ead1_Sessao_Entidade
     */
    public static function getSessaoEntidade()
    {
        self::sessionStart();
        return new Ead1_Sessao_Entidade(Zend_Session::namespaceGet('entidade'));
    }

    /**
     * Método Estatico que retorna a sessao de perfil
     * @return Ead1_Sessao_Perfil
     */
    public static function getSessaoPerfil()
    {
        self::sessionStart();
        return new Ead1_Sessao_Perfil(Zend_Session::namespaceGet('perfil'));
    }

    /**
     * Método Estatico que retorna a sessao de projeto pedagogico
     * @return Ead1_Sessao_ProjetoPedagogico
     */
    public static function getSessaoProjetoPedagogico()
    {
        self::sessionStart();
        return new Ead1_Sessao_ProjetoPedagogico(Zend_Session::namespaceGet('projetopedagogico'));
    }

    /**
     * Método Estatico que retorna a sessao geral
     */
    public static function getSessaoGeral()
    {
        self::sessionStart();
        return new Zend_Session_Namespace('geral');
    }

    /**
     * @return Ead1_Sessao_Geral
     */
    public static function getObjetoSessaoGeral()
    {
        self::sessionStart();
        return new Ead1_Sessao_Geral(Zend_Session::namespaceGet('geral'));
    }

    /**
     * Método que gera os dados de Sessão de mensagem para o Portal do Aluno
     */
    public static function getSessionMensagem()
    {
        self::sessionStart();
        return new Ead1_Sessao_Mensagem(Zend_Session::namespaceGet('mensagem'));
    }

    /**
     * Método que gera os dados de Sessão de menu para o Portal do Aluno
     */
    public static function getSessionMenu()
    {
        self::sessionStart();
        return new Ead1_Sessao_Menu(Zend_Session::namespaceGet('menu'));
    }

    /**
     * Método que gera os dados de Sessão de salas de aula do Portal do Aluno
     */
    public static function getSessionSalas()
    {
        self::sessionStart();
        return new Ead1_Sessao_Salas(Zend_Session::namespaceGet('salas'));
    }
}
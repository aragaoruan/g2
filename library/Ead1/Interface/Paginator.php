<?php
/**
 * Classe de Interface de Paginator
 * @author Eduardo Romão - ejushiro@gmail.com
 */
class Ead1_Interface_Paginator{
	
	/**
	 * 
	 * Variaveis
	 * 
	 */
	private $_firstPageInRange;
	private $_lastPageInRange;
	private $_currentItemCount;
	private $_totalItemCount;
	private $_firstItemNumber;
	private $_lastItemNumber;
	private $_pageCount;
	private $_itemCountPerPage;
	private $_first;
	private $_current;
	private $_last;
	private $_pagesInRange;
	
	/**
	 * Construtor
	 * @param Mixed $object
	 */
	public function __construct($object = null){
		if($object){
			if(is_object($object)){
				$this->objectToPaginator($object);
			}
		}
	}
	
	/**
	 * Método que transforma um array para Ead1_Interface_Paginator
	 * @param array $arr
	 * @return Ead1_Interface_Paginator
	 */
	protected function objectToPaginator($object){
		$vars = get_object_vars($object);
		if($vars){
			foreach ($vars as $var => $value) {
				Switch($var){
					case 'pageCount':
						$this->setPageCount($value);
						break;
					case 'itemCountPerPage':
						$this->setItemCountPerPage($value);
						break;
					case 'first':
						$this->setFirst($value);
						break;
					case 'current':
						$this->setCurrent($value);
						break;
					case 'last':
						$this->setLast($value);
						break;
					case 'pagesInRange':
						$this->setPagesInRange($value);
						break;
					case 'firstPageInRange':
						$this->setFirstPageInRange($value);
						break;
					case 'lastPageInRange':
						$this->setLastPageInRange($value);
						break;
					case 'currentItemCount':
						$this->setCurrentItemCount($value);
						break;
					case 'totalItemCount':
						$this->setTotalItemCount($value);
						break;
					case 'firstItemNumber':
						$this->setFirstItemNumber($value);
						break;
					case 'lastItemNumber':
						$this->setLastItemNumber($value);
						break;
				}
			}
		}
	}
	/**
	 * @return the $_firstPageInRange
	 */
	public function getFirstPageInRange() {
		return $this->_firstPageInRange;
	}

	/**
	 * @return the $_lastPageInRange
	 */
	public function getLastPageInRange() {
		return $this->_lastPageInRange;
	}

	/**
	 * @return the $_currentItemCount
	 */
	public function getCurrentItemCount() {
		return $this->_currentItemCount;
	}

	/**
	 * @return the $_totalItemCount
	 */
	public function getTotalItemCount() {
		return $this->_totalItemCount;
	}

	/**
	 * @return the $_firstItemNumber
	 */
	public function getFirstItemNumber() {
		return $this->_firstItemNumber;
	}

	/**
	 * @return the $_lastItemNumber
	 */
	public function getLastItemNumber() {
		return $this->_lastItemNumber;
	}

	/**
	 * @return the $_pageCount
	 */
	public function getPageCount() {
		return $this->_pageCount;
	}

	/**
	 * @return the $_itemCountPerPage
	 */
	public function getItemCountPerPage() {
		return $this->_itemCountPerPage;
	}

	/**
	 * @return the $_first
	 */
	public function getFirst() {
		return $this->_first;
	}

	/**
	 * @return the $_current
	 */
	public function getCurrent() {
		return $this->_current;
	}

	/**
	 * @return the $_last
	 */
	public function getLast() {
		return $this->_last;
	}

	/**
	 * @return the $_pagesInRange
	 */
	public function getPagesInRange() {
		return $this->_pagesInRange;
	}

	/**
	 * @param field_type $_firstPageInRange
	 */
	public function setFirstPageInRange($_firstPageInRange) {
		$this->_firstPageInRange = $_firstPageInRange;
	}

	/**
	 * @param field_type $_lastPageInRange
	 */
	public function setLastPageInRange($_lastPageInRange) {
		$this->_lastPageInRange = $_lastPageInRange;
	}

	/**
	 * @param field_type $_currentItemCount
	 */
	public function setCurrentItemCount($_currentItemCount) {
		$this->_currentItemCount = $_currentItemCount;
	}

	/**
	 * @param field_type $_totalItemCount
	 */
	public function setTotalItemCount($_totalItemCount) {
		$this->_totalItemCount = $_totalItemCount;
	}

	/**
	 * @param field_type $_firstItemNumber
	 */
	public function setFirstItemNumber($_firstItemNumber) {
		$this->_firstItemNumber = $_firstItemNumber;
	}

	/**
	 * @param field_type $_lastItemNumber
	 */
	public function setLastItemNumber($_lastItemNumber) {
		$this->_lastItemNumber = $_lastItemNumber;
	}

	/**
	 * @param field_type $_pageCount
	 */
	public function setPageCount($_pageCount) {
		$this->_pageCount = $_pageCount;
	}

	/**
	 * @param field_type $_itemCountPerPage
	 */
	public function setItemCountPerPage($_itemCountPerPage) {
		$this->_itemCountPerPage = $_itemCountPerPage;
	}

	/**
	 * @param field_type $_first
	 */
	public function setFirst($_first) {
		$this->_first = $_first;
	}

	/**
	 * @param field_type $_current
	 */
	public function setCurrent($_current) {
		$this->_current = $_current;
	}

	/**
	 * @param field_type $_last
	 */
	public function setLast($_last) {
		$this->_last = $_last;
	}

	/**
	 * @param field_type $_pagesInRange
	 */
	public function setPagesInRange($_pagesInRange) {
		$this->_pagesInRange = $_pagesInRange;
	}

	
}
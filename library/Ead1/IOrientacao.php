<?php
/**
 * 
 * Classe que contem a orientação do papel
 * @author DIMAS SULZ
 *
 */
interface Ead1_IOrientacao{
	const PAISAGEM = 'landscape';
	const RETRATO = 'portrait';
}
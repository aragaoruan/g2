<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Unyleya
 *
 * @package    wsunyleya
 * @copyright  2016 Denise Xavier - Unyleya BR (http://unyleya.com.br/)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");
require_once($CFG->libdir . "/gdlib.php");

class local_wsunyleya_external extends external_api {

    /****************************************************
     * Example template - hello world
     ***************************************************/
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function hello_world_parameters() {
        return new external_function_parameters(
            array('welcomemessage' => new external_value(PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, '))
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function hello_world($welcomemessage = 'Hello world, ') {
        global $USER;

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::hello_world_parameters(),
            array('welcomemessage' => $welcomemessage));

        //Context validation
        //OPTIONAL but in most web service it should present
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

        return $params['welcomemessage'] . $USER->firstname ;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function hello_world_returns() {
        return new external_value(PARAM_TEXT, 'The welcome message + user first name');
    }


    /**
     * Upload de imagem para o Perfil
     */

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function upload_profileimage_parameters() {
        return new external_function_parameters(
            array(
                'base64_file_string' => new external_value(PARAM_TEXT, 'base64 string from image file', VALUE_REQUIRED),
                'file_name' => new external_value(PARAM_TEXT, 'file name', VALUE_REQUIRED),
                'user_id'   => new external_value(PARAM_INT, 'Moodle id user"', VALUE_REQUIRED)
            )
        );
    }


    /**
     * @param $base64_file_string
     * @param $file_name
     * @param $user_id
     * @return string
     */
    public static function upload_profileimage($base64_file_string, $file_name, $user_id) {
        global $DB;

        self::validate_parameters(self::upload_profileimage_parameters(),
            array(
                'base64_file_string' => $base64_file_string,
                'file_name' => $file_name,
                'user_id' => $user_id
            ));
        $context = context_user::instance($user_id);

        $newfile = realpath(sys_get_temp_dir()).'/'.$file_name;
        $ifp = fopen($newfile, "wb");
        $data = explode(',', $base64_file_string);
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        $newpicture = process_new_icon($context, 'user', 'icon', 0, $newfile);
        $tipo = 0;
        if($newpicture){
            $tipo = 1;
            $DB->set_field('user', 'picture', $newpicture, array('id' => $user_id));
        }

        return array(
            'tipo'=> $tipo,
            'image_id'=> $newpicture,
            'user_id'=> $user_id,
            'mensagem' => $newpicture ? 'Imagem salva com sucesso' : 'Não foi possível salvar a Imagem');

    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function upload_profileimage_returns() {

        return new external_single_structure(
            array(
                'tipo' => new external_value(PARAM_INT, '1 para sucesso, 0 para erro'),
                'image_id' => new external_value(PARAM_INT, 'ID da Imagem'),
                'user_id' => new external_value(PARAM_INT, 'ID do Usuário'),
                'mensagem' => new external_value(PARAM_TEXT, 'Mensagem para sucesso, nada para erro'),
            )
        );

    }
    /**
     * Fim Upload de imagem para o Perfil
     */


    /*******************************************************************************
     * Return recent actitivies of course - moodle
     *******************************************************************************/

    /**
     * Recent activity WS
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function recent_activity_parameters() {

        return new external_function_parameters(
            array('course_id' => new external_value(PARAM_INT, 'Moodle id course"', VALUE_REQUIRED) ,
                  'user_id'   => new external_value(PARAM_INT, 'Moodle id user"', VALUE_REQUIRED) )
        );
    }

    /**
     * Returns Recent Activities of course
     * @param int $course_id
     * @param int $user_id
     * @return array recent activity
     */
    public function recent_activity($course_id, $user_id) {
        global $DB;
        global $CFG;
        require_once($CFG->dirroot.'/course/lib.php');

        $timestart = self::get_timestart($user_id, $course_id);




        //////////////////////// buscando detalhes dos modulos //////////////////////////////////////
        $modinfo = get_fast_modinfo($course_id);
        $sections = array();
        foreach ($modinfo->get_section_info_all() as $i => $section) {
            if (!empty($section->uservisible)) {
                $sections[$i] = $section;
            }
        }
        $activities = array();
        $index = 0;

        foreach ($sections as $sectionnum => $section) {

            $activity = new stdClass();
            $activity->type = 'section';
            if ($section->section > 0) {
                $activity->name = get_section_name($course_id, $section);
            } else {
                $activity->name = '';
            }

            $activity->visible = $section->visible;
            $activities[$index++] = $activity;

            if (empty($modinfo->sections[$sectionnum])) {
                continue;
            }

            foreach ($modinfo->sections[$sectionnum] as $cmid) {
                $cm = $modinfo->cms[$cmid];

                if (!$cm->uservisible) {
                    continue;
                }

                if (!empty($filter) and $cm->modname != $filter) {
                    continue;
                }

                if (!empty($filter_modid) and $cmid != $filter_modid) {
                    continue;
                }

                $libfile = "$CFG->dirroot/mod/$cm->modname/lib.php";

                if (file_exists($libfile)) {
                    require_once($libfile);


                    $get_recent_mod_activity = $cm->modname."_get_recent_mod_activity";

                    if (function_exists($get_recent_mod_activity)) {
                        $activity = new stdClass();
                        $activity->type    = 'activity';
                        $activity->cmid    = $cmid;
                        $activity->fonte    = $get_recent_mod_activity;
                        $activities[$index++] = $activity;

                        $get_recent_mod_activity($activities, $index, $timestart, $course_id, $cmid);

                    }
                }
            }
        }
        //////////////////////// fim buscando detalhes dos modulos //////////////////////////////////////

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::recent_activity_parameters(),
            array('user_id' => $user_id ,
                  'course_id' => $course_id) );

        //Context validation
        //OPTIONAL but in most web service it should present
        $contextUser = context_user::instance($user_id);
        self::validate_context($contextUser);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $contextUser)) {
            throw new moodle_exception('cannotviewprofile');
        }

        $course = $DB->get_record('course', array('id'=>$course_id), '*', MUST_EXIST);

        $time_since = userdate($timestart);
        $context = context_course::instance($course_id);
        //Capability checking about view blocl recent activity
        $canviewdeleted = has_capability('block/recent_activity:viewdeletemodule', $context);
        $canviewupdated = has_capability('block/recent_activity:viewaddupdatemodule', $context);
        if (!$canviewdeleted && !$canviewupdated) {
            return;
        }


        $changelist = array();
        // The following query will retrieve the latest action for each course module in the specified course.
        // Also the query filters out the modules that were created and then deleted during the given interval.
        $sql = "SELECT
                    cmid, MIN(action) AS minaction, MAX(action) AS maxaction, MAX(modname) AS modname
                FROM {block_recent_activity}
                WHERE timecreated > ? AND courseid = ?
                GROUP BY cmid
                ORDER BY MAX(timecreated) ASC";
        $params = array($timestart, $course->id);
        $logs = $DB->get_records_sql($sql, $params);



        if (isset($logs[0])) {
            // If special record for this course and cmid=0 is present, migrate logs.
            $class = new recent_activity();
            $class::migrate_logs_ws($course);
            $logs = $DB->get_records_sql($sql, $params);
        }

//        var_dump($logs);


        if ($logs) {
            $modinfo = get_fast_modinfo($course);
            foreach ($logs as $log) {

                // We used aggregate functions since constants CM_CREATED, CM_UPDATED and CM_DELETED have ascending order (0,1,2).
                $wasdeleted = ($log->maxaction == block_recent_activity_observer::CM_DELETED);
                $wascreated = ($log->minaction == block_recent_activity_observer::CM_CREATED);


                if ($wasdeleted && $wascreated) {
                    // Activity was created and deleted within this interval. Do not show it.
                    continue;
                } else if ($wasdeleted && $canviewdeleted) {
                    if (plugin_supports('mod', $log->modname, FEATURE_NO_VIEW_LINK, false)) {

                        // Better to call cm_info::has_view() because it can be dynamic.
                        // But there is no instance of cm_info now.
                        continue;
                    }
                    // Unfortunately we do not know if the mod was visible.
                    $modnames = get_module_types_names();
                    $changelist[$log->cmid] = array('action' => 'delete mod',
                        'module' => (object)array(
                            'modname' => $log->modname,
                            'modfullname' => isset($modnames[$log->modname]) ? $modnames[$log->modname] : $log->modname
                        ));

                } else if (!$wasdeleted && isset($modinfo->cms[$log->cmid]) && $canviewupdated) {
                    // Module was either added or updated during this interval and it currently exists.
                    // If module was both added and updated show only "add" action.
                    $cm = $modinfo->cms[$log->cmid];

//                    var_dump($cm);


                    //return $cm;
                    if ($cm->has_view() && $cm->uservisible) {

                        $changelist[$log->cmid] = array(
                            'action' => $wascreated ? 'add' : 'update',
                            'module_id' => $cm->id ,//$cm
                            'module_name' =>$cm->name,
                            'module_added' => $cm->added,
                            'module_modname' => $cm->modname,
                            'module_url' => $cm->url->get_path(),
                            'since' => $time_since,
                            'extratext'=>''
                        );

                        if($activities){
//                            var_dump($activities);

                            foreach($activities as $a){
                                if($log->cmid == $a->cmid){
                                    switch($a->type){
                                        case "forum": $changelist[$log->cmid]['extratext'] = $a->content->subject; break;

                                    }

                                }
                            }
                        }

                    }
                }
            }
        }

        return $changelist;
    }


    /**
     * Get the timestart like on Moodle
     * @param $user_id
     * @param $course_id
     * @return float
     */
    public function get_timestart($user_id, $course_id) {
            global $USER;
                $timestart = round(time() - COURSE_MAX_RECENT_PERIOD, -2); // better db caching for guests - 100 seconds

                if (!isguestuser($user_id)) {
                    if (!empty($USER->lastcourseaccess[$course_id])) {
                        if ($USER->lastcourseaccess[$course_id] > $this->timestart) {
                            $this->timestart = $USER->lastcourseaccess[$course_id];
                        }
                    }
                }
            return $timestart;
    }

    /**
     * Description of return recent activity
     * @return external_description
     */
    public static function recent_activity_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'action'  => new external_value(PARAM_TEXT, 'Action'),
                    'module_id' =>  new external_value(PARAM_INT, 'Id') ,
                    'module_name' => new external_value(PARAM_TEXT, 'Modulo name'),
                    'module_added' => new external_value(PARAM_ALPHANUM, 'Time added module'),
                    'module_modname' => new external_value(PARAM_TEXT, 'Type of module'),
                    'module_url' => new external_value(PARAM_URL, 'URL to access module'),
                    'extratext' => new external_value(PARAM_TEXT, 'Texto que pode vir a mais'),
                    'since' => new external_value(PARAM_TEXT, 'Date since action')
                )
            )
        );
    }


    /*******************************************************************************
     * Return recent interactions of course - moodle
     *******************************************************************************/

    /**
     * Recent interactions WS
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function recent_interactions_parameters()
    {

        return new external_function_parameters(
            array('course_id' => new external_value(PARAM_INT, 'Moodle id course"', VALUE_REQUIRED),
                'user_id' => new external_value(PARAM_INT, 'Moodle id user"', VALUE_REQUIRED))
        );
    }

    /**
     * Returns Recent Interactions of course
     * @param int $course_id
     * @param int $user_id
     * @return array recent interactions
     */
    public function recent_interactions($course_id, $user_id)
    {
        global $DB;
        global $CFG;
        require_once($CFG->dirroot . '/course/lib.php');

        $timestart = self::get_timestart($user_id, $course_id);

        //////////////////////// buscando detalhes dos modulos //////////////////////////////////////
        $modinfo = get_fast_modinfo($course_id);

        $sections = array();
        foreach ($modinfo->get_section_info_all() as $i => $section) {
            if (!empty($section->uservisible)) {
                $sections[$i] = $section;
            }
        }
        $activities = array();
        $index = 0;

        foreach ($sections as $sectionnum => $section) {
            $activity = new stdClass();
            $activity->type = 'section';
            if ($section->section > 0) {
                $activity->name = get_section_name($course_id, $section);
            } else {
                $activity->name = '';
            }

            $activity->visible = $section->visible;
            $activities[$index++] = $activity;

            if (empty($modinfo->sections[$sectionnum])) {
                continue;
            }

            foreach ($modinfo->sections[$sectionnum] as $cmid) {
                $cm = $modinfo->cms[$cmid];

                if (!$cm->uservisible) {
                    continue;
                }

                if (!empty($filter) and $cm->modname != $filter) {
                    continue;
                }

                if (!empty($filter_modid) and $cmid != $filter_modid) {
                    continue;
                }

                $libfile = "$CFG->dirroot/mod/$cm->modname/lib.php";

                if (file_exists($libfile)) {
                    require_once($libfile);


                    $get_recent_mod_activity = $cm->modname . "_get_recent_mod_activity";

                    if (function_exists($get_recent_mod_activity)) {
                        $activity = new stdClass();
                        $activity->type = $cm->modname;
                        $activity->cmid = $cmid;
                        $activity->fonte = $get_recent_mod_activity;
                        $activities[$index++] = $activity;

                        $get_recent_mod_activity($activities, $index, $timestart, $course_id, $cmid);
                    }
                }
            }
        }
        //////////////////////// fim buscando detalhes dos modulos //////////////////////////////////////


        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::recent_activity_parameters(),
            array('user_id' => $user_id,
                'course_id' => $course_id));

        //Context validation
        //OPTIONAL but in most web service it should present
        $contextUser = context_user::instance($user_id);
        self::validate_context($contextUser);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $contextUser)) {
            throw new moodle_exception('cannotviewprofile');
        }

        $course = $DB->get_record('course', array('id' => $course_id), '*', MUST_EXIST);

        $context = context_course::instance($course_id);

        //Capability checking about view blocl recent activity
        $canviewdeleted = has_capability('block/recent_activity:viewdeletemodule', $context);
        $canviewupdated = has_capability('block/recent_activity:viewaddupdatemodule', $context);
        if (!$canviewdeleted && !$canviewupdated) {
            return;
        }

        $changelist = array();
        // The following query will retrieve the latest action for each course module in the specified course.
        // Also the query filters out the modules that were created and then deleted during the given interval.
        $sql = "SELECT * FROM {block_recent_activity} WHERE courseid = ? GROUP BY cmid";
        $params = array($course->id);
        $logs = $DB->get_records_sql($sql, $params);

        if (isset($logs[0])) {
            // If special record for this course and cmid=0 is present, migrate logs.
            $class = new recent_activity();
            $class::migrate_logs_ws($course);
            $logs = $DB->get_records_sql($sql, $params);
        }

        if ($logs) {
            $modinfo = get_fast_modinfo($course);
            foreach ($logs as $log) {

                // We used aggregate functions since constants CM_CREATED, CM_UPDATED and CM_DELETED have ascending order (0,1,2).
                $wasdeleted = ($log->maxaction == block_recent_activity_observer::CM_DELETED);
                $wascreated = ($log->minaction == block_recent_activity_observer::CM_CREATED);

                if (!$wasdeleted && isset($modinfo->cms[$log->cmid]) && $canviewupdated) {
                    // Module was either added or updated during this interval and it currently exists.
                    // If module was both added and updated show only "add" action.
                    $cm = $modinfo->cms[$log->cmid];

                    if ($cm->has_view() && $cm->uservisible) {

                        $changelist[$log->cmid] = array(
                            'action' => $wascreated ? 'add' : 'update',
                            'module_id' => $cm->id,
                            'module_name' => $cm->name,
                            'module_added' => $cm->added,
                            'module_modname' => $cm->modname,
                            'module_url' => $cm->url->get_path(),
                            'unread' => 0
                        );

                        if ($activities) {

                            foreach ($activities as $a) {
                                if ($log->cmid == $a->cmid) {

                                    switch ($a->type) {
                                        case "forum":
                                            $id = $cm->id;
                                            $coursemodule = get_coursemodule_from_id('forum', $id);

                                            $now = round(time(), -2);
                                            $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 360);
//                                            var_dump(date('d/m/Y', 1299446702));die();
                                            $params = array();
                                            $groupmode = groups_get_activity_groupmode($cm);
                                            $currentgroup = groups_get_activity_group($cm);

                                            if ($groupmode) {
                                                $modcontext = context_module::instance($id);

                                                if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
                                                    if ($currentgroup) {
                                                        $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                                                        $params['currentgroup'] = $currentgroup;
                                                    } else {
                                                        $groupselect = "";
                                                    }

                                                } else {
                                                    //separate groups without access all
                                                    if ($currentgroup) {
                                                        $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                                                        $params['currentgroup'] = $currentgroup;
                                                    } else {
                                                        $groupselect = "AND d.groupid = -1";
                                                    }
                                                }
                                            } else {
                                                $groupselect = "";
                                            }

                                            if (!empty($CFG->forum_enabletimedposts)) {
                                                $timedsql = "AND d.timestart < :now1 AND (d.timeend = 0 OR d.timeend > :now2)";
                                                $params['now1'] = $now;
                                                $params['now2'] = $now;
                                            } else {
                                                $timedsql = "";
                                            }

                                            $sql = "SELECT d.id, COUNT(p.id) AS unread
                                                        FROM {forum_discussions} d
                                                           JOIN {forum_posts} p     ON p.discussion = d.id
                                                           LEFT JOIN {forum_read} r ON (r.postid = p.id AND r.userid = $user_id)
                                                        WHERE d.forum = {$coursemodule->instance}
                                                           AND p.modified >= :cutoffdate AND r.id is NULL
                                                           $groupselect
                                                           $timedsql
                                                        GROUP BY d.id";
                                            $params['cutoffdate'] = $cutoffdate;

                                            $unreads = 0;
                                            if ($arrunreads = $DB->get_records_sql($sql, $params)) {
                                                foreach ($arrunreads as $unread) {
                                                    $unreads += $unread->unread;
                                                }
                                            }

                                            $changelist[$log->cmid]['unread'] = $unreads;
                                            break;

                                        case "assign":

                                            $currentgroup = groups_get_activity_group($cm, true);

                                            list($esql, $params) = get_enrolled_sql(context_module::instance($cm->id), 'mod/assign:submit', $currentgroup, true);

                                            $id = $cm->id;
                                            $coursemodule = get_coursemodule_from_id('assign', $id);

                                            $params['assignid'] = $coursemodule->instance;
                                            $params['submitted'] = ASSIGN_SUBMISSION_STATUS_SUBMITTED;

                                            $sql = 'SELECT COUNT(s.userid)
                                                       FROM {assign_submission} S
                                                       LEFT JOIN {assign_grades} g ON
                                                            S.assignment = g.assignment AND
                                                            S.userid = g.userid AND
                                                            g.attemptnumber = S.attemptnumber
                                                       JOIN(' . $esql . ') e ON e.id = S.userid
                                                       WHERE
                                                            S.latest = 1 AND
                                                            S.assignment = :assignid AND
                                                            S.timemodified IS NOT NULL AND
                                                            S.status = :submitted AND
                                                            (S.timemodified >= g.timemodified OR g.timemodified IS NULL OR g.grade IS NULL)';
                                            $unreads = $DB->count_records_sql($sql, $params);
                                            $changelist[$log->cmid]['unread'] = $unreads;
                                            break;
                                    }

                                }
                            }
                        }

                    }
                }
            }
        }

        return $changelist;
    }

    /**
     * Description of return recent interactions
     * @return external_description
     */
    public static function recent_interactions_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'action' => new external_value(PARAM_TEXT, 'Action'),
                    'module_id' => new external_value(PARAM_INT, 'Id'),
                    'module_name' => new external_value(PARAM_TEXT, 'Modulo name'),
                    'module_added' => new external_value(PARAM_ALPHANUM, 'Time added module'),
                    'module_modname' => new external_value(PARAM_TEXT, 'Type of module'),
                    'unread' => new external_value(PARAM_INT, 'Interactions unread')
                )
            )
        );
    }

}

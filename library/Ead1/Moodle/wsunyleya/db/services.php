<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin for UNYLEYA
 *
 * @package    localwsunyleya
 * @copyright  2016 Denise Xavier
 * @update     2017 Rafael Leite
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_wsunyleya_hello_world' => array(
                'classname'   => 'local_wsunyleya_external',
                'methodname'  => 'hello_world',
                'classpath'   => 'local/wsunyleya/externallib.php',
                'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
                'type'        => 'read',
        ),
        'local_wsunyleya_upload_profileimage' => array(
                'classname'   => 'local_wsunyleya_external',
                'methodname'  => 'upload_profileimage',
                'classpath'   => 'local/wsunyleya/externallib.php',
                'description' => 'Faz o upload de uma imagem para o perfil do usuário',
                'type'        => 'write',
        ),
        'local_wsunyleya_recent_activity' => array(
            'classname' => 'local_wsunyleya_external',
            'methodname' => 'recent_activity',
            'classpath' => 'local/wsunyleya/externallib.php',
            'description' => 'Retorna array de atividades recentes do moodle',
            'type' => 'read',
        ),
    'local_wsunyleya_recent_interactions' => array(
        'classname' => 'local_wsunyleya_external',
        'methodname' => 'recent_interactions',
        'classpath' => 'local/wsunyleya/externallib.php',
        'description' => 'Retorna array de interações recentes do moodle',
        'type' => 'read',
        )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Unyleya Moodle Web Services ' => array(
            'functions' => array('local_wsunyleya_hello_world', 'local_wsunyleya_recent_activity',
                'local_wsunyleya_upload_profileimage', 'local_wsunyleya_recent_interactions'),
                'restrictedusers' => 0,
                'enabled'=>1,
        )
);

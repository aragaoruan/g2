<?php

/**
 *
 * Classe que captura informações gerais de ambiente
 * @author Eder Lamar Mariano
 *
 */
class Ead1_Ambiente
{

    const AMBIENTE_PRODUCAO = 'producao';
    const AMBIENTE_HOMOLOGACAO = 'homologacao';
    const AMBIENTE_DESENVOLVIMENTO = 'desenvolvimento';


    /**
     * Método que captura o ambiente para arquivos ini
     * @return string
     */
    public static function getAmbiente()
    {
        if (!isset($_SERVER['SERVER_NAME'])) {
            return 'desenvolvimento';
        }
        switch ($_SERVER["SERVER_NAME"]) {
            case "g2.ead1.net":
            case "gestor.ead1.net":
            case "g2release.ead1.net":
            case "portalrelease.ead1.net":
            case "lojarelease.ead1.net":
            case "www.g2.ead1.net":
            case "www.g2s.unyleya.com.br":
            case "g2s.unyleya.com.br":
            case "www.gestor.ead1.net":
            case "gestor2.ead1.net":
            case "www.gestor2.ead1.net":
            case "aluno.ead1.net":
            case "www.aluno.ead1.net":
            case "www.portal.ead1.net":
            case "portal.ead1.net":
            case "www.portal.ead1.com.br":
            case "portal.ead1.com.br":
            case "www.unico.ead1.net":
            case "unico.ead1.net":
            case "www.loja.ead1.net":
            case "loja.ead1.net":
            case "www.loja.ead1.com.br":
            case "loja.ead1.com.br":
            case "www.library.ead1.net":
            case "library.ead1.net":
                return "producao";
            case "g2h.ead1.net":
            case "www.g2h.ead1.net":
            case "www.portalh.ead1.net":
            case "portalh.ead1.net":
            case "www.unicoh.ead1.net":
            case "unicoh.ead1.net":
            case "www.lojah.ead1.net":
            case "lojah.ead1.net":
            case "www.g2lite.ead1.com.br":
            case "g2lite.ead1.com.br":
            case "www.g2liteh.ead1.net":
            case "g2liteh.ead1.net":
            case "www.libraryh.ead1.net":
            case "libraryh.ead1.net":
                return "homologacao";
            case "g2d.ead1.net":
            case "www.g2d.ead1.net":
                return "dev";
            case "gestor2.itto.com.br":
            case "itto.com.br":
            case "www.itto.com.br":
                return "teste";
            default:
                return "desenvolvimento";
        }
    }

    /**
     * Método que captura e retorna o arquivo ini do DB
     */
    public static function db()
    {
        return new Zend_Config_Ini(Zend_Controller_Front::getInstance()->getModuleDirectory() . "/config/database.ead1", Ead1_Ambiente::getAmbiente());
    }

    /**
     * Método que captura e retorna o arquivo ini das configurações gerais do sistema
     */
    public static function geral()
    {
        $modulo = APPLICATION_PATH . '/apps/default';
        return new Zend_Config_Ini($modulo . "/config/geral.ead1", Ead1_Ambiente::getAmbiente());
    }

    /**
     * Método que captura e retorna o arquivo ini das configurações da aplicação
     */
    public static function application()
    {
        return new Zend_Config_Ini(Zend_Controller_Front::getInstance()->getModuleDirectory() . "/config/application.ini", Ead1_Ambiente::getAmbiente());
    }

    /**
     * Método que captura e retorna o arquivo ini dos super usuários da aplicação
     */
    public static function su()
    {
        return new Zend_Config_Ini(Zend_Controller_Front::getInstance()->getModuleDirectory() . "/config/su.ead1", Ead1_Ambiente::getAmbiente());
    }

    /**
     * Método que captura e retorna o arquivo ead1 de configuração do e-mail
     * @return Zend_Config_Ini
     */
    public static function email()
    {
        return new Zend_Config_Ini(Zend_Controller_Front::getInstance()->getModuleDirectory() . "/config/email.ead1", Ead1_Ambiente::getAmbiente());
    }

    /**
     * Método que captura e retorna o arquivo ead1 de configuração do e-mail
     * @return Zend_Config_Ini
     */
    public static function ws()
    {
        return new Zend_Config_Ini(Zend_Controller_Front::getInstance()->getModuleDirectory() . "/config/ws.ead1", Ead1_Ambiente::getAmbiente());
    }
}
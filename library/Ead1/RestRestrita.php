<?php
/**
 *
 * Classe com implementações que tentam evitar o uso indevido das Controllers Rest do Zend sem Sessão
 * @author Elcio Mauro Guimarães
 * @date 17/12/2014
 *
 * */
class Ead1_RestRestrita extends \Zend_Rest_Controller
{
    const RESPONSE_CODE_SUCCESS       = 200;
    const RESPONSE_CODE_BAD_REQUEST   = 400;

    protected $sessao;

    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->sessao = \Ead1_Sessao::getObjetoSessaoGeral();
        if ($this->sessao->id_usuario == false) {
            $this->getResponse()->setHttpResponseCode(self::RESPONSE_CODE_BAD_REQUEST);
            $this->_helper->json(new \Ead1_Mensageiro("Você não está logado no sistema, favor efetuar o login", \Ead1_IMensageiro::ERRO));
            exit;
        }
    }

    public function indexAction()
    {
    }

    public function getAction()
    {
    }

    public function postAction()
    {
    }

    public function putAction()
    {
    }

    public function deleteAction()
    {
    }

    public function headAction()
    {
    }

    public function optionsAction()
    {
    }
}
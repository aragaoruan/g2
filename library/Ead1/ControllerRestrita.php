<?php

/**
 *
 * Classe com implementações que tentam evitar o uso indevido das Controllers normais do Zend sem Sessão
 * @author Elcio Mauro Guimarães
 * @date 16/12/2014
 *
 */
class Ead1_ControllerRestrita extends Ead1_Controller {


    public function init() {
        parent::init();

        $sessao = \Ead1_Sessao::getObjetoSessaoGeral();
        if($sessao->id_usuario==false){

            if ($this->getRequest()->isXmlHttpRequest()) {
                $this->getResponse()->setHttpResponseCode(400);
                $this->_helper->json(new \Ead1_Mensageiro("Você não está logado no sistema, favor efetuar o login", \Ead1_IMensageiro::ERRO));
            } else {
                $this->_helper->redirector('index', 'index');
            }
        }
    }

    protected function prepareArrayFromRequest($params)
    {
        $result = array();
        foreach ($params as $param) {
            $result[$param] = $this->getRequest()->getParam($param);
        }
        return $result;
    }

    protected function response($body, $httpCode)
    {
        $this->getResponse()->setBody(Zend_Json::encode($body));
        $this->getResponse()->setHttpResponseCode($httpCode);
    }

    protected function responseSuccess($message)
    {
        return $this->response(array(
            'retorno' => 'Salvo!',
            'mensagem' => $message,
            'type' => 'success',
        ), 200);
    }

    protected function responseNotModified($message)
    {
        return $this->response(array(
            'retorno' => 'Erro!',
            'mensagem' => $message,
            'type' => 'error',
        ), 304);
    }

}

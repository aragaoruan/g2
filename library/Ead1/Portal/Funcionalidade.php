<?php
/**
 * Classe de encapsulamento e negocios de funcionalidades do portal
 * @author Eduardo Romão
 */
class Ead1_Portal_Funcionalidade{
	
	/**
	 * Retorna o Label da funcionalidade
	 * de acordo com o id_funcionalidade e o id_entidade
	 * @param Int $id_funcionalidade
	 * @return String
	 */
	public static function getLabel($id_funcionalidade){
		$label = 'Erro Label!';
		$orm = new VwEntidadeFuncionalidadeORM();
		$vwFuncionalidadeTO = $orm->consulta(new VwEntidadeFuncionalidadeTO(
											array('id_entidade' => Ead1_Sessao::getSessaoEntidade()->id_entidade,'id_funcionalidade' => $id_funcionalidade))
											,false,true);
		if($vwFuncionalidadeTO){
			$label = $vwFuncionalidadeTO->st_label ? 
													$vwFuncionalidadeTO->st_label : 
													$vwFuncionalidadeTO->st_funcionalidade;
		}
		return $label;
	}
	
	
	
	public static function verificaAcesso($id_funcionalidade){
		if(Ead1_Sessao::getSessaoPerfil()->id_perfil && Ead1_Sessao::getSessaoEntidade()->id_entidade){
			
			$to = new VwPerfilEntidadeFuncionalidadeTO();
			$to->setId_entidade(Ead1_Sessao::getSessaoEntidade()->id_entidade);
			$to->setId_perfil(Ead1_Sessao::getSessaoPerfil()->id_perfil);
			$to->setId_funcionalidade($id_funcionalidade);
			$to->fetch(false, true, true);
			if($to->bl_ativo){
				return true;
			}
			
			return false;
			
		} else {
			return false;
		}
	}
	
}
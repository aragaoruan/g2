<?php
/**
 * Classe para interações do componente de relatórios
 * @author edermariano
 *
 */
class Ead1_Relatorio{
	
	const DB = 'db';
	const WS = 'ws';
	const INI = 'ini';
	const XLS = 'xls';
	const CSV = 'csv';
	
	private $provider;
	private $adapter;
	private $host;
	private $porta;
	private $usuario;
	private $senha;
	private $banco;
	
	/**
	 * Método que seta as configurações iniciais do componente de relatórios
	 * @param String $provider tipo de provider a ser utilizado para a pesquisa
	 */
	public function __construct($provider = self::DB){
		$this->setAdapter(Zend_Db_Table::getDefaultAdapter());
		$this->setProvider($provider);
	}
	
	public function setAdapter(Zend_Db_Adapter_Abstract $adapter){
		$this->adapter = $adapter;
	}

	public function getAdapter(){
		return $this->adapter;
	}
	
	public function setProvider($provider){
		$this->provider = $provider;
	}
	
	public function getProvider($provider){
		$this->provider = $provider;
	}
	
	/**
	 * Método que cria a instancia do ORM
	 */
	public function orm(){
		
	}
	
	public function __call($funcao, $dados){
		switch(substr($funcao, 0, 3)){
			case 'set':
				$atributo = strtolower(substr($funcao, 3, strlen($funcao)));
				$this->$atributo = $dados[0];
				break;
			case 'get':
				$atributo = strtolower(substr($funcao, 3, strlen($funcao)));
				return $this->$atributo;
				break;
		}
	}
}
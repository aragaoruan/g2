<?php

/**
 * Interface para constantes do mensageiro
 *
 */
interface Ead1_IMensageiro
{

    const ERRO = 0;
    const SUCESSO = 1;
    const AVISO = 2;
    const EmailUtilizado = 3;

    const MENSAGEM_ERRO = 'Erro ao executar a operação!';
    const MENSAGEM_SUCESSO = 'Operação executada com sucesso!';
    const MENSAGEM_AVISO = 'Aviso para operação!';
    const MENSAGEM_NADA_ENCONTRADO = 'Nenhum registro encontrado!';
    const MENSAGEM_NOME_DUPLICADO = 'Já existe um item cadastrado com esse nome!';

    const TYPE_ERRO = 'error';
    const TYPE_SUCESSO = 'success';
    const TYPE_AVISO = 'warning';

    const TITLE_ERRO = 'Erro';
    const TITLE_SUCESSO = 'Sucesso';
    const TITLE_AVISO = 'Aviso';

}
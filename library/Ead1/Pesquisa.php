<?php

namespace Ead1;

/**
 * Classe para encapsular os dados de endereço.
 * @author Dimas Sulz <dimassulz@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @package Ead1
 */
class Pesquisa
{

    /**
     * Atributo que diz qual o valor do filtro da pesquisa
     * @var Array
     */
    public $filtros = array();

    /**
     * Atributo que diz qual o Método da RO
     * @var String
     */
    public $metodo;

    /**
     * Atributo que qual o tipo da pesquisa
     * @var String
     */
    public $tipo;

    /**
     * Atributo que contém os arrays com os campos da Grid
     * @var Array
     */
    public $camposGrid;

    /**
     * Contém o id_funcionalidade da pesquisa atual
     * @var int
     */
    public $id_funcionalidade;

    /**
     *
     * @var array 
     */
    public $funcionalidade = array();

    /**
     * Atributo que diz qual o tipo de to que vai enviar
     * @var String
     */
    public $to_envia;

    /**
     * Retorna string to_envia
     * @return String Nome da TO Envia
     */
    public function getTo_envia ()
    {
        return $this->to_envia;
    }

    /**
     * Seta string do to_envia
     * @param string $to_envia String com o nome da To envia
     */
    public function setTo_envia ($to_envia)
    {
        $this->to_envia = $to_envia;
    }

    /**
     * 
     * @return array
     */
    public function getFuncionalidade ()
    {
        return $this->funcionalidade;
    }

    /**
     * 
     * @param array $funcionalidade
     */
    public function setFuncionalidade (array $funcionalidade)
    {
        $this->funcionalidade = $funcionalidade;
    }

    /**
     * @return the $id_funcionalidade
     */
    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @param $id_funcionalidade the $id_funcionalidade to set
     */
    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
    }

    /**
     * @return the $tipo
     */
    public function getTipo ()
    {
        return $this->tipo;
    }

    /**
     * @param $tipo the $tipo to set
     */
    public function setTipo ($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return the $metodo
     */
    public function getMetodo ()
    {
        return $this->metodo;
    }

    /**
     * @param $metodo the $metodo to set
     */
    public function setMetodo ($metodo)
    {
        $this->metodo = $metodo;
    }

    /**
     * @return the $filtros
     */
    public function getFiltros ()
    {
        return $this->filtros;
    }

    /**
     * @param $filtros the $filtros to set
     */
    public function setFiltros ($filtros)
    {
        $this->filtros = $filtros;
    }

    /**
     * @return the $camposGrid
     */
    public function getCamposGrid ()
    {
        return $this->camposGrid;
    }

    /**
     * @param $camposGrid the $camposGrid to set
     */
    public function setCamposGrid ($camposGrid)
    {
        $this->camposGrid = $camposGrid;
    }

}
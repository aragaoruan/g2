<?php
/**
 * Interface para PAGAMENTO via cartão de crédito
 * Deve organizar o Fluxo permitindo assim o Factory
 * @author Elcio Mauro Guimarães
 * @since 03/07/2012
 */
interface Ead1_IPagamentoCartao {
	
	const BRASPAG 				= 'braspag';
	const BRASPAG_RECORRENTE 	= 'braspag_recorrente';
	const LOCAWEB 				= 'NAO_IMPLEMENTADA';
    const PAGAR_ME               = 'pagar_me';
	
	
	/**
	 * Solicita uma autorização de pagamento
	 * @param array $arDadosCartao - Array com dados do Cartão que será convertido no TO correspondente
	 * @return Ead1_Mensageiro
	 */
	public function autorizar(array $arDadosCartao);
	
	/**
	 * Verifica uma solicitação de pagamento 
	 */
	public function verificar(TransacaoFinanceiraTO $to);
	
	/**
	 * Solicita um cancelamento de pagamento 
	 */
	public function cancelar(TransacaoFinanceiraTO $to, VendaTO $venda);
	
	/**
	 * Solicita o estorno de um pagamento 
	 */
	public function estornar();
	
	
	
}
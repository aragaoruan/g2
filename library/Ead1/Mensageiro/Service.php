<?php
/**
 * 
 * Classe que trata para um tipo Mensageiro a resposta do serviço consumido 
 * @author Eder Lamar
 */
class Ead1_Mensageiro_Service extends Ead1_Mensageiro implements Ead1_IMensageiro{
	
	/**
	 * Atributo que contém o XML
	 * @var mixed
	 */
	private $resultService;
	
	/**
	 * 
	 * Construtor da classe
	 * @param mixed $resultService xml da resposta do serviço
	 */
	public function __construct($resultService = null){
		parent::__construct();
		$this->setResultService($resultService);
		$this->trataStatus();
		$this->trataMensagem();
	}
	
	/**
	 * Método que seta o resultService
	 * @param mixed $resultService
	 */
	public function setResultService($resultService = null){
		$this->resultService = $resultService;
		return $this;
	}
	
	/**
	 * Método que trata o status da mensagem
	 */
	private function trataStatus(){
		if(isset($this->resultService->status)){
			switch($this->resultService->status[0]){
				case 'success':
					$this->setTipo(Ead1_IMensageiro::SUCESSO);
					break;
				default:
					$this->setTipo(Ead1_IMensageiro::ERRO);
			}
		}
	}
	
	/**
	 * Método que trata as mensagens do Serviço
	 */
	private function trataMensagem(){
		if(isset($this->resultService->response)){
			if(isset($this->resultService->response->message)){
				if(is_array($this->resultService->response->message)){
					$resposta = false;
					$mensagem = true;
					$i = 0;
					while($mensagem){
						$chave = "key_$i";
						if(isset($this->resultService->response->message->$chave)){
							if(isset($this->resultService->response->message->$chave->mensagem)){
								$arMensagens[] = $this->resultService->response->message->$chave->mensagem;		
							}else{
								$arMensagens[] = $this->resultService->response->message->$chave;		
							}
						}else{
							$mensagem = false;
						}
						$i++;
					}
					
					if($arMensagens){
						$resposta = implode(", ", $arMensagens) . ".";	
					}
				}else{
					if(isset($this->resultService->response->message->mensagem)){
						$resposta = $this->resultService->response->message->mensagem;
					}else{
						$resposta = (string) $this->resultService->response->message;				
					}
				}
				$this->setMensagem($resposta);
			}else{
				$this->setMensagem($this->resultService->response);
			}
		}
		return $this;
	}
	
	/**
	 * Método que converte XML em Array
	 * @param mixed $xml
	 */
	public static function getArrayXML($xml){
		$i = 0;
		$arMensagem = array();
		$mensagem = true;
		while($mensagem){
			$chave = "key_$i";
			if(isset($xml->$chave)){
				$arMensagem[] = $xml->$chave; 
			}else{
				$mensagem = false;
			}
			$i++;
		}
		
		return $arMensagem;
	}
}
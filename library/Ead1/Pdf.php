<?php
require_once APPLICATION_PATH . '/../library/dompdf/dompdf_config.inc.php';

/**
 * @package Ead1
 * Wrapper da biblioteca de emissão de PDF
 * @author Arthur Cláudio de Almeida Pereira
 *
 */
class Ead1_Pdf
{

    /**
     *
     * @var DOMPDF
     */
    protected $_dompdf;

    public function __construct()
    {
        Zend_Loader_Autoloader::getInstance()->pushAutoloader('DOMPDF_autoload', '');
        $this->_dompdf = new DOMPDF(array('enable_remote' => true));
        $this->configuraPapel();
    }

    /**
     * Carrega uma string HTML
     * @param string $html
     * @return Ead1_Pdf
     */
    public function carregarHTML($html)
    {
        //echo "carregarHTML";
        $html = $this->substituiQuebraPagina($html);
        $html = iconv("utf-8", "iso-8859-1//IGNORE", $html);
        //  echo "substituiQuebraPagina";
        $this->_dompdf->load_html($html);
        return $this;
    }

    /**
     * Substitui a tag </pagina> gerada pelo componente RichTextEditor para gerção de conteúdos dinâmicos
     * para a tag específica utilizada pela biblioteca DOMPDF
     * para finalizar o término de uma página e ínicio de outra página.
     *
     * @param string $texto
     * @return string
     */
    private function substituiQuebraPagina($texto)
    {
        $texto = str_replace("<!-- pagebreak -->", '</font></P><div style="page-break-before: always;"></div>', $texto);
        return preg_replace('/&lt;\/pagina&gt;.*?<\/P.*?>/', '</font></P><div style="page-break-before: always;"></div>', $texto);
    }

    /**
     * Configura o tamanho e orientação do papel
     *
     * O tamanho do papel pode ser 'a4', 'letter' , 'legal'
     * @link CPDF_Adapter::$PAPER_SIZES
     *
     * Tipos de orientação:  'portrait'( retrato ) ou 'landscape' ( paisagem )
     *
     * @param string $tamanho
     * @param string $orientacao
     * @return Ead1_Pdf
     */
    public function configuraPapel($tamanho = 'a4', $orientacao = 'portrait')
    {
        //echo "configuraPapel";
        $this->_dompdf->set_paper($tamanho, $orientacao);
        return $this;
    }

    /**
     * Gera o PDF com o nome informados.
     * Caso seja informado um caminho de diretório válido será salvo o arquivo
     * ao invés de gerar a opção download.
     * @todo implementar ação de salvar o PDF
     * @param string $nomeArquivo
     * @param string $caminhoArquivo - OPCIONAL -  caminho absoluto para salvar o pdf
     * @return Boolean
     */
    public function gerarPdf($nomeArquivo, $caminhoArquivo = false)
    {
        $this->_dompdf->render();
        if (!$caminhoArquivo) {
            $this->_dompdf->stream($nomeArquivo);
            return true;
        } else {
            return file_put_contents($caminhoArquivo . $nomeArquivo, $this->_dompdf->output());
        }
    }

    /**
     * @description Esta funcao define o header do html como um arquivo PDF e o proprio navegador conseguira ler o arquivo sem precisar baixar
     */
    public function showPdf() {
        $this->_dompdf->render();
        header("Content-type:application/pdf");
        echo $this->_dompdf->output();
        exit;
    }
}

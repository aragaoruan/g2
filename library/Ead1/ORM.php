<?php

/**
 * Classe para implementações de Mapeamento Objeto Relacional das tabelas para a EAD1
 *
 */
class Ead1_ORM extends Zend_Db_Table_Abstract
{

    /**
     *
     * @var string
     */
    public $_schema;

    /**
     * campos de uma consulta
     * @var array
     */
    private $_campos;

    /**
     * Método que chamará além do construtor do pai o info
     * @param mixed $config array para o construct pai
     */
//	public function __construct($config = array()){
//		parent::__construct($config);
//		$this->info();
//	}

    /**
     * Método que seta os campos para serem usados no fetchAll com offset
     * @param array $campos
     */
    public function setCampos(array $campos)
    {
        $this->_campos = $campos;
    }

    /**
     * Método que seta os campos para serem usados no fetchAll com offset
     * @param String $campos
     */
    public function addCampos($campo)
    {
        $this->_campos[] = $campo;
    }

    /**
     * Método que realiza sobrescrita dos commits e coloca a regra de identidade igual ao id_usuario na sessão para commits
     */
    public function commit()
    {
        try {
            $sessao = new Zend_Session_Namespace('geral');
            if (Zend_Auth::getInstance()->hasIdentity()) {
                if ($sessao->id_usuario == Zend_Auth::getInstance()->getIdentity()) {
                    $this->_db->commit();
                } else {
                    $this->_db->rollBack();
                }
            } else {
                $this->_db->commit();
            }
        } catch (Zend_Db_Exception $e) {
            $this->database_log($e);
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Método que sobrescreve o insert para gerar logs automaticamente
     * @param boolean $transacao por padrão obriga o uso de transações
     * @param array $dados
     * @return mixed|null
     * @throws Exception
     * @throws Zend_Db_Exception
     */
    public function insert(array $dados, $transacao = true)
    {
        $id = null;
        try {
            if ($transacao) {

                $transacao = Ead1_Transacao::getInstancia();
                if ($transacao->transacao) {
                    //$this->validaInsert($dados);
                    $id = parent::insert($dados);
                } else {
                    $mensagem = 'Para realizar inserção é necessário possuir transação ativa.';
                    throw new Exception($mensagem, 0);
                }
            } else {
                //$this->validaInsert($dados);
                $id = parent::insert($dados);
            }
        } catch (Zend_Db_Exception $e) {
            $this->database_log($e, $dados);
            throw $e;
        } catch (Exception $e) {
            $this->database_log($e, $dados);
            throw $e;
        }
        return $id;
    }

    /**
     * Método que sobrescreve o update para gerar logs automaticamente
     * @param array $dados
     * @todo Gerar log de eventos
     * @return int números de linhas autalizadas
     */
    public function update(array $dados, $where, $verificapk = false)
    {
        $retorno = null;

        if (!$where) {
            throw new Zend_Exception("Obrigatório informar alguma cláusula where para o Update.");
        }

        if ($verificapk) {
            if (is_array($this->_primary)) {
                foreach ($this->_primary as $pk) {
                    if (is_array($where)) {
                        if (!in_array($pk, array_keys($where))) {
                            throw new Exception('Chave Obrigatória para o Update na ' . get_class($this), 0);
                        }
                    } else {
                        if (strpos($where, $pk) === false) {
                            throw new Exception('Chave Obrigatória para o Update na ' . get_class($this), 0);
                        }
                    }
                }
            } else {
                if (is_array($where)) {
                    if (!in_array($this->_primary, array_keys($where))) {
                        throw new Exception('Chave Obrigatória para o Update na ' . get_class($this), 0);
                    }
                } else {
                    if (strpos($where, $this->_primary) === false) {
                        throw new Exception('Chave Obrigatória para o Update na ' . get_class($this), 0);
                    }
                }
            }
        }

        try {
            //$this->validaUpdate($dados);
            $retorno = parent::update($dados, $where);
        } catch (Exception $e) {
            $this->database_log($e, $dados);
            throw $e;
        }

        return $retorno;
    }

    /**
     * Método que sobrescreve o delete para gerar logs automaticamente
     * @param array $dados
     * @return int número de linhas deletadas
     */
    public function delete($where)
    {
        $transacao = Ead1_Transacao::getInstancia();
        if ($transacao->transacao) {
            try {
                return parent::delete($where);
//			$this->log();
            } catch (Zend_Db_Exception $e) {
                $this->database_log($e);
            }
        } else {
            $mensagem = 'Para deletar dados é necessário possuir transação ativa.';
            throw new Exception($mensagem, 0);
            Zend_Debug::dump($mensagem);
            exit;
        }
    }

    /**
     * Fetches all rows.
     *
     * Honors the Zend_Db_Adapter fetch mode.
     *
     * @param string|array|Zend_Db_Table_Select $where OPTIONAL An SQL WHERE clause or Zend_Db_Table_Select object.
     * @param string|array $order OPTIONAL An SQL ORDER clause.
     * @param int $count OPTIONAL An SQL LIMIT count.
     * @param int $offset OPTIONAL An SQL LIMIT offset.
     * @return Zend_Db_Table_Rowset_Abstract The row results per the Zend_Db_Adapter fetch mode.
     */
    public function fetchAll($where = null, $order = null, $count = null, $offset = null)
    {
        try {
            if ($offset) {
                if ($where instanceof Zend_Db_Table_Select) {
                    throw new Zend_Exception("Problemas para consulta Zen_Db_Table_Select usando offset.", 0);
                } elseif (!$count) {
                    throw new Zend_Exception("Problemas para consulta usando offset não existe um valor limite.", 0);
                } else {
                    $partida = ($count * ($offset - 1)) + 1;
                    $registros = $count * $offset;
                    $pk = (is_string($this->_primary) ? $this->_primary : array_pop(array_reverse($this->_primary)));
                    $campos = ($this->_campos ? implode(', ', $this->_campos) : "*");
                    $whereClausula = ($where ? " WHERE $where " : "");

                    $orderClausula = ($order ? " ORDER BY $order" : "");

                    $query = "WITH OrderedOrders AS
					(
					   SELECT cast(ROW_NUMBER() OVER (ORDER BY $pk) as int) AS 'linha',
					   $campos
					   FROM $this->_schema.$this->_name $whereClausula
					)
					SELECT *
					FROM OrderedOrders
					WHERE linha BETWEEN $partida AND $registros $orderClausula";
                    return $this->getDefaultAdapter()->query($query)->fetchAll();
                }
            } else {
                return parent::fetchAll($where, $order, $count, $offset);
            }
        } catch (Zend_Db_Exception $e) {
            $this->database_log($e);
        }
    }

    /**
     * Método que registra o log
     * @param array $dados
     */
    private function log($dados)
    {
        /*$sessao = new Zend_Session_Namespace('geral');

        $tbLog = new $tbLog();
        $tbLog->insert();*/
    }


    /**
     * Método que retorna uma consulta para a tabela em questão com os dados do TO
     * @param Ead1_TO $to
     */
    public function consulta(Ead1_TO $toOriginal, $soPk = false, $row = false, $order = null)
    {
        $to = clone $toOriginal;
        $to->tipaTODAO();
        $classe = substr(get_class($this), 0, -3) . 'TO';
        if ($to instanceof $classe) {
            try {
                $consulta = $this->montarWhere($to, $soPk);

                if ($row) {
                    $resultado = $this->fetchRow($consulta);
                    if ($resultado) {
                        $arResultado[] = $resultado->toArray();
                        $dados = Ead1_TO_Dinamico::encapsularTo($arResultado, new $classe());
                        $dados = $dados[0];
                    }
                } else {
                    if (is_null($order)) {
                        $retorno = $this->fetchAll($consulta);
                            if(isset($retorno) && !empty($retorno)){
                                $dados = Ead1_TO_Dinamico::encapsularTo($retorno->toArray(), new $classe());
                            }
                    } else {
                        $dados = Ead1_TO_Dinamico::encapsularTo($this->fetchAll($consulta, $order)->toArray(), new $classe());
                    }
                }
            } catch (Zend_Db_Exception $e) {
                $this->database_log($e);
            }
            return (!isset($dados) || empty($dados) ? false : $dados);
        } else {
            throw new Exception("A classe TO $classe não reflete a tabela em questão.", 1);
        }
    }


    /**
     * Método que retorna uma consulta para a tabela em questão com os dados do TO mas selecionando os campos do array
     * @param Ead1_TO $toOriginal
     * @param array $arrCampos
     * @param boolean $soPk
     * @param boolean $row
     * @param array $order
     * @throws Exception
     * @return array
     */
    public function consultaCampos(Ead1_TO $toOriginal, array $arrCampos = null, $soPk = false, $row = false, array $order = null)
    {
        $to = clone $toOriginal;
        $to->tipaTODAO();
        $classe = substr(get_class($this), 0, -3) . 'TO';
        if (is_a($to, $classe)) {

            $select = $this->select();
            if ($arrCampos)
                $select->from($this, $arrCampos);
            $where = $this->montarWhere($to, $soPk);
            if ($where)
                $select->where($where);
            if ($order)
                $select->order($order);

            try {
                if ($row)
                    $dados = $this->fetchRow($select);
                else
                    $dados = $this->fetchAll($select);

                if ($dados)
                    $dados = $dados->toArray();
            } catch (Zend_Db_Exception $e) {
                $this->database_log($e);
            }
            return (!isset($dados) || empty($dados) ? false : $dados);
        } else {
            throw new Exception("A classe TO $to não reflete a tabela em questão.", 1);
        }
    }

    /**
     * Método que retorna a consulta WHERE como string com base no TO
     * @param Ead1_TO $to
     * @param bool|false $soPk
     * @return bool|null|string
     * @throws Exception
     */
    public function montarWhere(Ead1_TO $to, $soPk = false)
    {
        $cloneTO = clone $to;
        $cloneTO->tipaTOConsulta();
        $classe = substr(get_class($this), 0, -3) . 'TO';

        $wheresopk = array();
        if ($cloneTO instanceof $classe) {
            //pega o nome dos campos baseado na classe
            $camposTabela = get_class_vars($classe);
            $camposTabelaNames = array_keys($camposTabela);
//            $camposTabela = $this->getDefaultAdapter()->describeTable($this->_name);
            $camposTo = $cloneTO->toArrayUpdate();
            $where = false;
            if (!empty($camposTo)) {
                foreach ($camposTo as $campo => $valor) {

                    $tiposTexto = array('st_', 'dt_', 'sg_');//seta os prefixos que serão entendidos como texto
                    //cria um array com o prefixo para consulta como like
                    $tiposTextoLike = array('st_');

                    //verifica se o nome do campo existe no array dos campos
                    if (array_key_exists($campo, $camposTabela)) {

                        //verifica se a consulta corresponde aos parametros de consulta por pk se a pk não é um array e se é só uma pk
                        if ($soPk && (!is_array($this->_primary) && count($this->_primary) == 1)) {

                            //monta a string da consulta
                            $where .= ($where ? " AND " : "");

                            //verifica se o campo corresponde ao prefixo de texto
                            if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTexto)) {


                                if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTextoLike)) {
                                    $where .= " $campo like '%$valor%'";
                                } else {
                                    $where .= " $campo = '$valor'";
                                }
                            } else {
                                $where .= " $campo = $valor";
                            }
                        } elseif ($soPk && $this->_primary && !$where) { // Adicionado para voltar a funcionar o fetch por VW
                            if (is_array($this->_primary)) {
                                foreach ($this->_primary as $chave) {
                                    $metodo = 'get' . $chave;
                                    if (!in_array(" $chave = '" . $cloneTO->$metodo() . "'", $wheresopk)) {
                                        $where .= " $chave = '" . $cloneTO->$metodo() . "'";
                                        $wheresopk[] = " $chave = '" . $cloneTO->$metodo() . "'";
                                    }
                                }
                            } else {
                                $metodo = 'get' . $this->_primary;
                                if (!in_array(" $this->_primary = '" . $cloneTO->$metodo() . "'", $wheresopk)) {
                                    $where .= " $this->_primary = '" . $cloneTO->$metodo() . "'";
                                    $wheresopk[] = " $this->_primary = '" . $cloneTO->$metodo() . "'";
                                }
                            }
                        } elseif (!$soPk) {
                            $where .= ($where ? " AND " : "");
                            if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTexto)) {
                                if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTextoLike)) {
                                    $where .= " $campo like '%$valor%'";
                                } else {
                                    $where .= " $campo = '$valor'";
                                }
                            } else {
                                $where .= " $campo = $valor";
                            }
                        }

                    }
                }
            }
            return ($where ? $where : null);
        } else {
            throw new Exception("A classe TO $cloneTO não reflete a tabela em questão.", 1);
        }

    }

    /**
     * Método que retorna a consulta WHERE para uma pesquisa como string com base nos dados setados no TO
     * @param Ead1_TO $to
     * @param bool|false $soPk
     * @param bool|false $and
     * @return null|string
     */
    public function montarWherePesquisa(Ead1_TO $to, $soPk = false, $and = false)
    {
        $clone = clone $to;
        $clone->tipaTOConsulta();
        //pega o nome dos campos baseado na classe
        $camposTabela = get_class_vars(get_class($to));
        $camposTabelaNames = array_keys($camposTabela);
//        $camposTabela = $this->getDefaultAdapter()->describeTable($this->_name);
        $camposTo = $clone->toArrayUpdate();
        $where = array();
        foreach ($camposTo as $campo => $valor) {
            if (array_key_exists($campo, $camposTabela)) {
                $tiposTexto = array('st_', 'sg_');//seta os prefixos que serão entendidos como texto

                if ($soPk && (!is_array($this->_primary) && count($this->_primary) == 1)) {
                    //verifica se o campo corresponde ao prefixo de texto
                    if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTexto)) {
                        $where[] = "$campo like '%$valor%'";
                    } else {
                        $where[] = "$campo = $valor";
                    }
                } elseif (!$soPk) {
                    if (in_array(substr($camposTabelaNames[array_search($campo, $camposTabelaNames)], 0, 3), $tiposTexto)) {
                        $where[] = "$campo like '%$valor%'";
                    } else {
                        $where[] = "$campo = $valor";
                    }
                }
            }
        }
        $orAnd = ($and ? ' AND ' : ' OR ');
        return ($where ? implode($orAnd, $where) : NULL);
    }

    /**
     * Metodo que Cria consulta para views
     * @param Ead1_TO $to
     * @param array $pks
     * @param boolean $and
     * @param boolean $soPK
     */
    public function montarWhereView(Ead1_TO $to, $pks = false, $and = false, $soPK = false)
    {
        $to->tipaTOConsulta();
        $camposTO = $to->toArrayUpdate();
        $wherePK = array();
        $where = array();
        foreach ($camposTO as $campo => $valor) {
            $ini = explode('_', $campo);
            $ini = $ini[0];
            switch ($ini) {
                case 'st':
                    $where[] = " $campo LIKE '%$valor%' ";
                    break;
                case 'sg':
                    $where[] = " $campo = '$valor' ";
                    break;
                case 'dt':
                    $where[] = " $campo = '$valor' ";
                    break;
                case 'hr':
                    $where[] = " $campo = '$valor' ";
                    break;
                case 'id':
                    if (!$pks || empty($pks)) {
                        $where[] = " $campo = $valor ";
                    } else {
                        if (array_search($campo, $pks) !== false) {
                            $wherePK[] = " $campo = $valor";
                        } else {
                            $where[] = " $campo = $valor";
                        }
                    }
                    break;
                case 'bl':
                    $where[] = " $campo = $valor ";
                    break;
                default:
                    $where[] = " $campo = $valor ";
                    break;
            }
        }
        $orAnd = ($and ? ' AND ' : ' OR ');
        if ($soPK) {
            if (count($wherePK) == 1) {
                $consulta = $wherePK[0];
            } elseif (count($wherePK) == 0) {
                $consulta = null;
            } else {
                $consulta = implode($orAnd, $wherePK);
            }
        } else {
            $merged = array_merge($where, $wherePK);
            if (count($merged) == 1) {
                $consulta = $merged[0];
            } elseif (count($merged) == 0) {
                $consulta = null;
            } else {
                $consulta = implode($orAnd, $merged);
            }
        }
        return $consulta ? $consulta : null;
    }

    /**
     * Método que retorna o último
     * Atualizado, add foreach para corrigir o bug das chaves do array
     */
    public function getUltimo($where = null)
    {
        try {
            if (is_array($this->_primary)) {
                foreach ($this->_primary as $_primary) {
                    $orderBy = $_primary;
                    break;
                }
            } else {
                $orderBy = $this->_primary;
            }
            return $this->fetchRow($where, "$orderBy DESC");
        } catch (Zend_Db_Exception $e) {
            $this->database_log($e);
        }
    }

    /**
     * Método que valida os dados para insert
     */
    private function validaInsert(array $dados)
    {
        if (empty($dados)) {
            throw new Zend_Exception("INSERT vazio para a tabela " . $this->_name . ".");
        } else {
            $this->colunasValidas($dados);
            $this->camposObrigatorios($dados);
        }
    }

    /**
     * Método que valida os dados para insert
     */
    private function validaUpdate(array $dados)
    {

        if (empty($dados)) {
            throw new Zend_Exception('UPDATE vazio para a tabela ' . $this->_name);
        }

        $this->colunasValidas($dados);
        $this->camposObrigatorios($dados, true);
    }

    /**
     * Método que verifica colunas válidas
     * @param array $dados array de dados
     * @throws Zend_Exception
     * @return bool
     */
    protected function colunasValidas(array $dados)
    {
        $chavesInsert = array_keys($dados);
        $colunasInexistentes = array_diff($chavesInsert, $this->_cols);

        if (!empty($colunasInexistentes)) {
            $msg = 'O(s) campo(s) (' . implode(', ', $colunasInexistentes) . ') não estão presentes na tabela: ' . $this->_name;
            throw new Zend_Exception($msg);
        }

        return true;
    }

    /**
     * Verifica campos obrigatórios para o INSERT
     * @param array $dados
     */
    protected function camposObrigatorios(array $dados, $update = false)
    {
        foreach ($this->_metadata as $campo => $describe) {
            $this->validaCampoTipo($campo, $describe, $dados);
            if ($describe["NULLABLE"] === false && $describe["DEFAULT"] === NULL && $describe["IDENTITY"] === false) {
                if (isset($dados[$campo])) {
                    if ($dados[$campo] === null) {
                        throw new Zend_Exception("O campo (" . $campo . ") não pode ser NULL na tabela (" . $this->_name . ")");
                    }
                } else {
                    if (!$update) {
                        throw new Zend_Exception("O campo (" . $campo . ") não pode ser NULL na tabela (" . $this->_name . ")");
                    }
                }
            }
        }
    }

    /**
     * Método que valida se os tipos de dados e quantidade de caracteres estão de acordo
     *
     * @param String $campo
     * @param array $describe
     * @param array $dados
     */
    protected function validaCampoTipo($campo, $describe, $dados)
    {
        if (isset($dados[$campo])) {
            switch ($describe["DATA_TYPE"]) {
                case "int":
                    $classe = new Zend_Validate_Int();
                    if (!$classe->isValid($dados[$campo])) {
                        throw new Zend_Exception("O campo $campo precisa ser inteiro, na tabela " . $this->_name . ". (" . $dados[$campo] . ") não é um valor válido.");
                    }
                    break;
                case "bit":
                    $classe = new Zend_Validate_Int();
                    if (!$classe->isValid($dados[$campo])) {
                        throw new Zend_Exception("O campo $campo precisa ser bit, na tabela " . $this->_name . ". (" . $dados[$campo] . ") não é um valor válido.");
                    }
                    break;
                case "numeric":
                    $classe = new Zend_Validate_Float(array('locale' => 'en'));
                    if (!$classe->isValid($dados[$campo])) {
                        throw new Zend_Exception("O campo $campo precisa ser número, na tabela " . $this->_name . ". (" . $dados[$campo] . ") não é um valor válido.");
                    }
                    break;
                case "datetime":
                    if (is_string($dados[$campo])) {
                        $dataAtual = new Zend_Date($dados[$campo]);
                    } else {
                        $dataAtual = $dados[$campo];
                    }
                    $classe = new Zend_Validate_Date();
                    if (!$classe->isValid($dataAtual)) {
                        throw new Zend_Exception("O campo $campo precisa ser data, na tabela " . $this->_name . ". (" . $dados[$campo] . ") não é um valor válido.");
                    }
                    break;

                case "char":
                case "varchar":

                    if (strlen($dados[$campo]) > $describe['LENGTH']) {
                        throw new Zend_Exception("O campo $campo pode ter até " . $describe['LENGTH'] . " caractere(s), " . strlen($dados[$campo]) . " foram fornecido(s), na tabela " . $this->_name . ". (" . $dados[$campo] . ") não é um valor válido.");
                    }

                    break;

            }
        }
    }

    /**
     * Método que retorna a instancia do objeto
     * @param Ead1_ORM $instancia
     * @return Ead1_ORM
     */
    public static function instance(Ead1_ORM $instancia)
    {
        return $instancia;
    }


    /**
     * Metodo que grava no log, erros de banco de dados
     * @param Zend_Db_Exception $e
     * @param array $data
     */
    private function database_log(Zend_Db_Exception $e, $data = array())
    {
        $path = APPLICATION_PATH . '/../docs/logs/';
        $file = 'database.log';

        $log = (date('Y-m-d H:i:s') . ' in ' . $e->getFile() . ':(' . $e->getLine() . ') - ' . $e->getMessage());
        if ($data && is_array($data)) {
            $log .= " WITH PARAMS ('";
            $log .= implode("', '", $data);
            $log .= "')";
        }
        $log .= "\n\n";

        if (@filesize($path . $file) > 1713015) {
            rename($path . $file, $path . 'database-' . date('d-m-Y-His') . '.log');
        }

        file_put_contents($path . $file, $log, FILE_APPEND);
        //chmod($path . $file, 0777);

        \G2\Utils\LogSistema::enviarLogNewRelic($log);
    }


}

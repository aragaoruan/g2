<?php

/**
 * Created by PhpStorm.
 * User: felipe.pastor
 * Date: 09/04/14
 * Time: 14:57
 */
class Ead1_Sessao_Menu
{

    public $bl_tcc;

    public function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * Transforma o Objeto em um Array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
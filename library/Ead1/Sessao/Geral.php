<?php

class Ead1_Sessao_Geral
{

    public $id_usuario;
    public $id_entidade;
    public $st_nomecompleto;
    public $nomeUsuario;
    public $id_perfil;
    public $st_nomeperfil;
    public $st_versao;

    public function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $this->$key = $value;
                    if ($key == "nomeUsuario")
                        $this->st_nomecompleto = $value;

                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getkey()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return mixed
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return mixed
     */
    public function getnomeUsuario()
    {
        return $this->nomeUsuario;
    }

    /**
     * @return mixed
     */
    public function getid_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @return mixed
     */
    public function getst_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @return mixed
     */
    public function getSt_versao()
    {
        return $this->st_versao;
    }

    /**
     * Transforma o Objeto em um Array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
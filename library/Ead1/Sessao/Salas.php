<?php

class Ead1_Sessao_Salas
{

    private $ar_salas;

    public function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * @param mixed $ar_salas
     */
    public function setAr_salas($ar_salas)
    {
        $this->ar_salas = $ar_salas;
    }

    /**
     * @return mixed
     */
    public function getAr_salas()
    {
        return $this->ar_salas;
    }

    /**
     * Transforma o Objeto em um Array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
<?php

class Ead1_Sessao_Entidade
{

    public $id_entidade;
    public $st_nomeentidade;
    public $st_razaosocial;
    public $st_layout;
    public $bl_header;
    public $bl_nav;
    public $bl_content;
    public $bl_footer;
    public $bl_acessoapp;

    public function __construct($data = null)
    {
        $this->bl_content = true;
        $this->bl_footer = true;
        $this->bl_header = true;
        $this->bl_nav = true;
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $this->$key = $value;
                }
            }
        }
        $this->st_layout = $this->st_layout ? $this->st_layout : 'padrao';
    }

    /**
     * Transforma o Objeto em um Array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

}
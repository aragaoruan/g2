<?php
class Ead1_Sessao_ProjetoPedagogico{

	public function __construct($data = null){
		if(!is_null($data) && is_array($data)){
			foreach ($data as $key => $value){
				if(array_key_exists($key, get_object_vars($this))){
					$metodo = 'set'.$key;
					$this->{$metodo}($value);
				}
			}
		}
	}

	public $id_projetopedagogico;
	public $st_tituloexibicao;
	public $id_trilha;
	public $id_matricula;
	public $id_saladeaula;
	public $st_saladeaula;
	public $bl_bloqueiadisciplina;
    public $bl_atendimentovirtual;
    public $st_conexao;
    public $st_chaveacesso;

    /**
     * @return mixed
     */
    public function getSt_conexao()
    {
        return $this->st_conexao;
    }

    /**
     * @param mixed $st_conexao
     */
    public function setSt_conexao($st_conexao)
    {
        $this->st_conexao = $st_conexao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_chaveacesso()
    {
        return $this->st_chaveacesso;
    }

    /**
     * @param mixed $st_chaveacesso
     */
    public function setSt_chaveacesso($st_chaveacesso)
    {
        $this->st_chaveacesso = $st_chaveacesso;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBl_atendimentovirtual()
    {
        return $this->bl_atendimentovirtual;
    }

    /**
     * @param mixed $bl_atendimentovirtual
     */
    public function setBl_atendimentovirtual($bl_atendimentovirtual)
    {
        $this->bl_atendimentovirtual = $bl_atendimentovirtual;
        return $this;
    }

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $id_trilha
	 */
	public function getId_trilha() {
		return $this->id_trilha;
	}

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @return the $id_saladeaula
	 */
	public function getId_saladeaula() {
		return $this->id_saladeaula;
	}

	/**
	 * @return the $st_saladeaula
	 */
	public function getSt_saladeaula() {
		return $this->st_saladeaula;
	}

	/**
	 * @return the $bl_bloqueiadisciplina
	 */
	public function getBl_bloqueiadisciplina() {
		return $this->bl_bloqueiadisciplina;
	}

	/**
	 * @param field_type $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
	}

	/**
	 * @param field_type $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
	}

	/**
	 * @param field_type $id_trilha
	 */
	public function setId_trilha($id_trilha) {
		$this->id_trilha = $id_trilha;
	}

	/**
	 * @param field_type $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
	}

	/**
	 * @param field_type $id_saladeaula
	 */
	public function setId_saladeaula($id_saladeaula) {
		$this->id_saladeaula = $id_saladeaula;
	}

	/**
	 * @param field_type $st_saladeaula
	 */
	public function setSt_saladeaula($st_saladeaula) {
		$this->st_saladeaula = $st_saladeaula;
	}

	/**
	 * @param field_type $bl_bloqueiadisciplina
	 */
	public function setBl_bloqueiadisciplina($bl_bloqueiadisciplina) {
		$this->bl_bloqueiadisciplina = $bl_bloqueiadisciplina;
	}

	/**
	 * Transforma o Objeto em um Array
	 * @return array
	 */
	public function toArray(){
		return get_object_vars($this);
	}
}

<?php

class Ead1_Sessao_Mensagem
{

    public function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $this->$key = $value;
                }
            }
        }
    }

    public $nr_mensagem;
    public $nr_mensagem_nao_lidas;
    public $bl_mensagem;


    /**
     * Transforma o Objeto em um Array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
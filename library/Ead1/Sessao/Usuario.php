<?php
class Ead1_Sessao_Usuario{

	public function __construct($data = null){
		if(!is_null($data) && is_array($data)){
			foreach ($data as $key => $value){
				if(array_key_exists($key, get_object_vars($this))){
					$this->$key = $value;
				}
			}
		}
	}

	public $id_usuario;
	public $id_entidade;
	public $st_nomecompleto;
	public $st_nomeexibicao;
	public $st_login;
	public $st_senha;
	public $st_senhaentidade;
	public $st_urlavatar;
	public $dt_nascimento;
	public $st_gravalog;
	public $st_cpf;
    public $id_usuariooriginal = 0;

	/**
	 * Transforma o Objeto em um Array
	 * @return array
	 */
	public function toArray(){
		return get_object_vars($this);
	}
}

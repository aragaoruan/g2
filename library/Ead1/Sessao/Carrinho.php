<?php

class Ead1_Sessao_Carrinho
{

    public function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, get_object_vars($this))) {
                    $metodo = 'set' . $key;
                    $this->{$metodo}($value);
                }
            }
        }
    }

    public $id_usuario;
    public $st_nomecompleto;
    public $id_entidade;
    public $st_nomeentidade;
    public $st_urlimglogo;
    public $id_sistema;
    public $st_codchave;
    public $st_vendaexterna;
    public $st_urlvendaexterna;
    public $st_urlconfirmacao;
    public $st_email;
    public $st_cpf;
    public $produtos;
    public $produtos_gratuitos;
    public $produtos_pagos;
    public $cadastro;
    public $id_enderecoentrega;
    public $id_venda;
    public $st_codigocupom;
    public $id_cupom;
    public $id_modelovenda;
    public $id_campanhacomercial;
    public $id_matricula;
    public $id_campanhacomercialpremio;
    public $utm_source;
    public $utm_medium;
    public $utm_campaign;
    public $utm_term;
    public $utm_content;
    public $origem_organica;


    /**
     * @return the $id_modelovenda
     */
    public function getid_modelovenda()
    {
        return $this->id_modelovenda;
    }

    /**
     * @return the $id_campanhacomercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return the $id_campanhacomercialpremio
     */
    public function getId_campanhacomercialpremio()
    {
        return $this->id_campanhacomercialpremio;
    }

    /**
     * @param field_type $id_modelovenda
     */
    public function setid_modelovenda($id_modelovenda)
    {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    /**
     * @param field_type $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = (int)$id_campanhacomercial;
        return $this;
    }

    /**
     * @param field_type $id_campanhacomercialpremio
     */
    public function setId_campanhacomercialpremio($id_campanhacomercialpremio)
    {
        $this->id_campanhacomercialpremio = $id_campanhacomercialpremio;
        return $this;
    }

    /**
     * @return the $id_cupom
     */
    public function getId_cupom()
    {
        return $this->id_cupom;
    }

    /**
     * @param field_type $id_cupom
     */
    public function setId_cupom($id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @return the $st_codigocupom
     */
    public function getSt_codigocupom()
    {
        return $this->st_codigocupom;
    }

    /**
     * @param field_type $st_codigocupom
     */
    public function setSt_codigocupom($st_codigocupom)
    {
        $this->st_codigocupom = $st_codigocupom;
        return $this;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param field_type $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return the $id_enderecoentrega
     */
    public function getId_enderecoentrega()
    {
        return $this->id_enderecoentrega;
    }

    /**
     * @param field_type $id_enderecoentrega
     */
    public function setId_enderecoentrega($id_enderecoentrega)
    {
        $this->id_enderecoentrega = $id_enderecoentrega;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return the $st_email
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param field_type $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return the $id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param field_type $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return the $st_urlimglogo
     */
    public function getSt_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    /**
     * @return the $st_codchave
     */
    public function getSt_codchave()
    {
        return $this->st_codchave;
    }

    /**
     * @return the $st_vendaexterna
     */
    public function getSt_vendaexterna()
    {
        return $this->st_vendaexterna;
    }

    /**
     * @return the $st_urlvendaexterna
     */
    public function getSt_urlvendaexterna()
    {
        return $this->st_urlvendaexterna;
    }

    /**
     * @return the $st_urlconfirmacao
     */
    public function getSt_urlconfirmacao()
    {
        return $this->st_urlconfirmacao;
    }

    /**
     * @return the $produtos
     */
    public function getProdutos()
    {
        return $this->produtos;
    }

    /**
     * @return the $produtos_gratuitos
     */
    public function getProdutos_gratuitos()
    {
        return $this->produtos_gratuitos;
    }

    /**
     * @return the $produtos_pagos
     */
    public function getProdutos_pagos()
    {
        return $this->produtos_pagos;
    }

    /**
     * @return the $cadastro
     */
    public function getCadastro()
    {
        return $this->cadastro;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @param field_type $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param field_type $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param field_type $st_urlimglogo
     */
    public function setSt_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
    }

    /**
     * @param field_type $st_codchave
     */
    public function setSt_codchave($st_codchave)
    {
        $this->st_codchave = $st_codchave;
    }

    /**
     * @param field_type $st_vendaexterna
     */
    public function setSt_vendaexterna($st_vendaexterna)
    {
        $this->st_vendaexterna = $st_vendaexterna;
    }

    /**
     * @param field_type $st_urlvendaexterna
     */
    public function setSt_urlvendaexterna($st_urlvendaexterna)
    {
        $this->st_urlvendaexterna = $st_urlvendaexterna;
    }

    /**
     * @param field_type $st_urlconfirmacao
     */
    public function setSt_urlconfirmacao($st_urlconfirmacao)
    {
        $this->st_urlconfirmacao = $st_urlconfirmacao;
    }

    /**
     * @param field_type $produtos
     */
    public function setProdutos(array $produtos)
    {

        foreach ($produtos as $key => $value) {
            $this->produtos[$key] = $value;
        }

    }

    /**
     * @param field_type $produtos_gratuitos
     */
    public function setProdutos_gratuitos(array $produtos_gratuitos)
    {

        foreach ($produtos_gratuitos as $key => $value) {
            $this->produtos_gratuitos[$key] = $value;
        }

    }

    /**
     * @param field_type $produtos_pagos
     */
    public function setProdutos_pagos(array $produtos_pagos)
    {

        foreach ($produtos_pagos as $key => $value) {
            $this->produtos_pagos[$key] = $value;
        }

    }

    /**
     * @param field_type $cadastro
     */
    public function setCadastro(array $cadastro)
    {
        foreach ($cadastro as $key => $value) {
            $this->cadastro[$key] = $value;
        }

    }

    /**
     * @return mixed
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param mixed $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return mixed
     */
    public function getUtm_source()
    {
        return $this->utm_source;
    }

    /**
     * @param mixed $utm_source
     */
    public function setUtm_source($utm_source)
    {
        $this->utm_source = $utm_source;
    }

    /**
     * @return mixed
     */
    public function getUtm_medium()
    {
        return $this->utm_medium;
    }

    /**
     * @param mixed $utm_medium
     */
    public function setUtm_medium($utm_medium)
    {
        $this->utm_medium = $utm_medium;
    }

    /**
     * @return mixed
     */
    public function getUtm_campaign()
    {
        return $this->utm_campaign;
    }

    /**
     * @param mixed $utm_campaign
     */
    public function setUtm_campaign($utm_campaign)
    {
        $this->utm_campaign = $utm_campaign;
    }

    /**
     * @return mixed
     */
    public function getUtm_term()
    {
        return $this->utm_term;
    }

    /**
     * @param mixed $utm_term
     */
    public function setUtm_term($utm_term)
    {
        $this->utm_term = $utm_term;
    }

    /**
     * @return mixed
     */
    public function getUtm_content()
    {
        return $this->utm_content;
    }

    /**
     * @param mixed $utm_content
     */
    public function setUtm_content($utm_content)
    {
        $this->utm_content = $utm_content;
    }

    /**
     * @return mixed
     */
    public function getOrigem_organica()
    {
        return $this->origem_organica;
    }

    /**
     * @param mixed $origem_organica
     */
    public function setOrigem_organica($origem_organica)
    {
        $this->origem_organica = $origem_organica;
    }


}

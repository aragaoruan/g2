<?php
class Ead1_Sessao_Perfil{
	
	public function __construct($data = null){
		if(!is_null($data) && is_array($data)){
			foreach ($data as $key => $value){
				if(array_key_exists($key, get_object_vars($this))){
					$metodo = 'set'.$key;
					$this->{$metodo}($value);
				}
			}
		}
		$this->ar_funcionalidades = $this->ar_funcionalidades ? $this->ar_funcionalidades : array();
	}
	
	public $id_perfil;
	public $st_nomeperfil;
	public $st_perfilpedagogico;
	public $id_perfilpedagogico;
	public $dt_inicio;
	public $dt_termino;
	public $ar_funcionalidades;
    public $id_projetopedagogicopesquisa;
	
	/**
	 * @return the $id_perfil
	 */
	public function getId_perfil() {
		return $this->id_perfil;
	}

	/**
	 * @return the $st_nomeperfil
	 */
	public function getSt_nomeperfil() {
		return $this->st_nomeperfil;
	}

	/**
	 * @return the $st_perfilpedagogico
	 */
	public function getSt_perfilpedagogico() {
		return $this->st_perfilpedagogico;
	}

	/**
	 * @return the $id_perfilpedagogico
	 */
	public function getId_perfilpedagogico() {
		return $this->id_perfilpedagogico;
	}

	/**
	 * @return the $dt_inicio
	 */
	public function getDt_inicio() {
		return $this->dt_inicio;
	}

	/**
	 * @return the $dt_termino
	 */
	public function getDt_termino() {
		return $this->dt_termino;
	}

	/**
	 * @return the $ar_funcionalidades
	 */
	public function getAr_funcionalidades() {
		return $this->ar_funcionalidades;
	}

	/**
	 * @param field_type $id_perfil
	 */
	public function setId_perfil($id_perfil) {
		$this->id_perfil = $id_perfil;
	}

	/**
	 * @param field_type $st_nomeperfil
	 */
	public function setSt_nomeperfil($st_nomeperfil) {
		$this->st_nomeperfil = $st_nomeperfil;
	}

	/**
	 * @param field_type $st_perfilpedagogico
	 */
	public function setSt_perfilpedagogico($st_perfilpedagogico) {
		$this->st_perfilpedagogico = $st_perfilpedagogico;
	}

	/**
	 * @param field_type $id_perfilpedagogico
	 */
	public function setId_perfilpedagogico($id_perfilpedagogico) {
		$this->id_perfilpedagogico = $id_perfilpedagogico;
	}

	/**
	 * @param field_type $dt_inicio
	 */
	public function setDt_inicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
	}

	/**
	 * @param field_type $dt_termino
	 */
	public function setDt_termino($dt_termino) {
		$this->dt_termino = $dt_termino;
	}

	/**
	 * @param multitype: $ar_funcionalidades
	 */
	public function setAr_funcionalidades($ar_funcionalidades) {
		$this->ar_funcionalidades = $ar_funcionalidades;
	}

	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Aluno
	 * @return Boolean
	 */
	public function is_aluno(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO ? true : false;
	}
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de AlunoInstitucional
	 * @return Boolean
	 */
	public function is_alunoInstitucional(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::ALUNO_INSTITUCIONAL ? true : false;
	}
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Professor
	 * @return Boolean
	 */
	public function is_professor(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::PROFESSOR ? true : false;
	}
	
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Coordenador de Projeto Pedagogico
	 * @return boolean
	 */
	public function is_coordenadorProjetoPedagogico(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::COORDENADOR_DE_PROJETO_PEDAGOGICO ? true : false;
	}
	
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Coordenador de disciplina
	 * @return boolean
	 */
	public function is_coordenadorDisciplina(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::COORDENADOR_DE_DISCIPLINA ? true : false;
	}
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Obervador Institucional
	 * @return boolean
	 */
	public function is_observadorInstitucional(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::COORDENADOR_DE_AREA ? true : false;
	}
	
	/**
	 * Verifica se a Pessoa Esta logada com o Perfil Pedagógico de Suporte
	 * @return boolean
	 */
	public function is_suporte(){
		return $this->id_perfilpedagogico == PerfilPedagogicoTO::SUPORTE ? true : false;
	}

	/**
	 * Transforma o Objeto em um Array
	 * @return array
	 */
	public function toArray(){
		return get_object_vars($this);
	}

    /**
     * @param mixed $id_projetopedagogicopesquisa
     */
    public function setId_projetopedagogicopesquisa($id_projetopedagogicopesquisa)
    {
        $this->id_projetopedagogicopesquisa = $id_projetopedagogicopesquisa;
    }

    /**
     * @return mixed
     */
    public function getId_projetopedagogicopesquisa()
    {
        return $this->id_projetopedagogicopesquisa;
    }


	
}
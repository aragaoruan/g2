<?php

Class MenuItem
{

    var $caption;
    var $children;
    var $link;
    var $icon;
    var $id_tipo_funcionalidade;
    var $classeFlexBotao;
    var $bl_pesquisa;
    var $id_situacao;
    var $id_funcionalidade;
    var $url;
    var $url_edicao;
    var $bl_relatorio;
    var $migrada;
    var $bl_delete;

    public function __construct ($obj = null)
    {
        if (!$obj) {
            $this->caption = 'Item de menu';
            $this->link = '#';
            $this->classFlexBotao = '';
            $this->children = null;
            $this->id_tipo_funcionalidade = 1;
            $this->bl_pesquisa = false;
            $this->bl_relatorio = false;
            $this->id_situacao = 3;
            $this->id_funcionalidade = 0;
            $this->url = '/';
            $this->url_edicao = '/';
            $this->bl_delete = true;
            $this->st_ajuda = '';
            return $this;
        }
        $array_classeflexbotao = explode('.', $obj->st_classeflexbotao);
        $this->classeFlexBotao = $array_classeflexbotao[count($array_classeflexbotao) - 1];

        $this->bl_pesquisa = $obj->bl_pesquisa;
        $this->bl_relatorio = $obj->bl_relatorio;

        $this->caption = $obj->st_funcionalidade;
        $this->link = $obj->st_classeflex;
        $this->id_tipo_funcionalidade = $obj->id_tipofuncionalidade;
        $this->id_situacao = $obj->id_situacao;
        $this->url = $obj->st_urlcaminho;
        $this->id_funcionalidade = $obj->id_funcionalidade;
        
        $this->url_edicao = $obj->st_urlcaminhoedicao;

        $this->migrada = ($this->id_situacao === 123) ? true:false;
        $this->bl_delete = $obj->bl_delete;

        $this->st_ajuda = $obj->st_ajuda;

        if (count($obj->filhos)) {
            foreach ($obj->filhos as $subItem) {
                $this->children[] = new MenuItem($subItem);
            }
        }
    }

    public function getJson ()
    {
        $output = array('caption' => $this->caption, 'link' => $this->link, 'children' => $this->children);
        return json_encode($ouput);
    }

    public function render ()
    {
        switch ($this->id_tipo_funcionalidade) {
            case '1':
                echo '<li class="area"><a class="crumb" href="#s' . $this->id_funcionalidade . '" id="link_menu_' . $this->id_funcionalidade . '">' . $this->caption . '</a>';
                echo '<span id="s' . $this->id_funcionalidade . '"></span>';
                break;
            case '2':
                echo '<li class="subarea"><a href="#" class="crumb section-header" id="link_menu_' . $this->id_funcionalidade . '">' . $this->caption . '</a>';
                break;
            case '3':
                if ($this->bl_pesquisa)
                        echo '<li class="sub"><a id="link_menu_' . $this->id_funcionalidade . '" class="crumb" data-urledicao="'. $this->url_edicao. '" data-url="' . $this->url . '" data-hasmigrated="'. (($this->id_situacao===123) ? 'true':'false') .'" data-classeflex="' . $this->classeFlexBotao . '"  href="#pesquisa/' . $this->classeFlexBotao . '"  >' . $this->caption . '</a>';
                elseif ($this->id_situacao == 123)
                    if ($this->bl_relatorio)
                        echo '<li class="sub"><a id="link_menu_' . $this->id_funcionalidade . '" class="crumb" data-urledicao="'. $this->url_edicao. '" data-url="' . $this->url . '" data-hasmigrated="'. (($this->id_situacao===123) ? 'true':'false').'"  data-relatorio="true" href="#relatorio/' . $this->id_funcionalidade . '" >' . $this->caption . '</a>';
                    else
                        echo '<li class="sub"><a id="link_menu_' . $this->id_funcionalidade . '" class="crumb" data-urledicao="'. $this->url_edicao. '" data-url="' . $this->url . '" data-hasmigrated="'. (($this->id_situacao===123) ? 'true':'false').'"  href="#' . $this->url . '" >' . $this->caption . '</a>';
                else
                        echo '<li class="sub"><a id="link_menu_' . $this->id_funcionalidade . '" class="crumb" data-urledicao="'. $this->url_edicao. '" data-url="' . $this->url . '" data-hasmigrated="'. (($this->id_situacao===123) ? 'true':'false') .'" data-classeflex="' . $this->link . '" href="#flex/' . $this->link . '" >' . $this->caption . '</a>';
                break;
        }

        if (count($this->children) && $this->id_tipo_funcionalidade < 3) {

            echo '<ul class="' . ($this->id_tipo_funcionalidade == 1 ? 'subs' : 'submenu') . ' ">';
            foreach ($this->children as $child) {
                echo $child->render();
            }
            echo '</ul>';
        }
        echo '</li>';
    }

}
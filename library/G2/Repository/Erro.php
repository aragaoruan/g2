<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;
use \G2\Entity\Erro as ErroEntity;

class Erro extends EntityRepository {

    public function save(ErroEntity $erro){
        try {
            if (is_null($erro->getProcesso())){
                $erro->setProcesso($this->_em->getReference('G2\Entity\Processo', 1));
            }

            $this->_em->persist($erro);
            $this->_em->flush();

            return $erro;
        } catch (\PDOException $exception) {
            return $exception;
        }
    }
} 
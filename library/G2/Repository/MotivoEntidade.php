<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class MotivoEntidade para Repository para Motivos
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-03-12
 *
 * @package G2\Repository
 */
class MotivoEntidade extends EntityRepository
{
    public function retornarMotivosCancelamento(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('me')
                ->select(
                    'mt.id_motivo',
                    'mt.st_motivo',
                    'sit.id_situacao',
                    'mt.dt_cadastro',
                    'mt.bl_ativo'
                )
                ->join('me.id_motivo', 'mt')
                ->join('mt.id_situacao', 'sit')
                ->where('mt.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            $query->andWhere('me.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidadecadastro']);
            $query->orderBy('mt.st_motivo', 'ASC');
            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository para Alocacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@@unyleya.com.br>
 */
class Alocacao extends EntityRepository
{

    /**
     * Retorna Alocacoes de um ou mais projetos pedagogicos
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarAlocacoesSalasNaoIniciadasPorProjetosPedagogicos(array $ids_projetospedagogicos, $params)
    {
        try {

            $sql = 'SELECT al.*, sda.id_entidadeintegracao FROM tb_alocacao al
                    JOIN tb_matriculadisciplina md ON (al.id_matriculadisciplina = md.id_matriculadisciplina)
                    JOIN tb_matricula mt ON (md.id_matricula = mt.id_matricula)
                    JOIN tb_saladeaula sda ON (sda.id_saladeaula = al.id_saladeaula)
                    WHERE 1=1 AND mt.id_projetopedagogico IN(' . implode(',', $ids_projetospedagogicos) . ')
                    AND sda.dt_abertura >= getDate()';

            // Ignorar alocacoes com APROVEITAMENTO DE DISCIPLINA
            $sql .= ' AND md.id_situacao != ' . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA;

            if (array_key_exists('id_entidade', $params)) {
                $sql .= ' AND sda.id_entidade = ' . $params['id_entidade'];
                unset($params['id_entidade']);
            }

            foreach ($params as $key => $value) {
                $sql .= ' AND al.' . $key . ' = ' . $value;
            }


            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }
    }

    /**
     * @param array $where
     * @return array|null
     */
    public function retornarAlocacoesSalas($where = array())
    {
        try {
            $sql = "
            SELECT
                sa.id_saladeaula,
                sa.id_entidade,
                mt.id_matricula,
                mt.id_evolucao,
                sa.id_entidadeintegracao
            FROM
                tb_saladeaula AS sa
                JOIN tb_alocacao AS al ON al.id_saladeaula = sa.id_saladeaula
                JOIN tb_matriculadisciplina AS md ON md.id_matriculadisciplina = al.id_matriculadisciplina
                JOIN tb_matricula AS mt ON mt.id_matricula = md.id_matricula
            WHERE 1 = 1
            ";

            if (is_array($where) && $where) {
                foreach ($where as $attr => $val) {
                    if (strstr($attr, '.')) {
                        $sql .= " AND {$attr} = '{$val}'";
                    } else {
                        $sql .= " AND al.{$attr} = '{$val}'";
                    }
                }
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            return null;
        }
    }

}

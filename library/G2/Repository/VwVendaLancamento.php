<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/05/14
 * Time: 14:39
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;
use Ead1\Doctrine\EntitySerializer;
use G2\Constante\Sistema;


class VwVendaLancamento extends EntityRepository
{

    protected function montaArrayParams($object)
    {

        if (!is_object($object))
            throw new \Exception('Parâmetro informado deve ser do tipo objeto.');

        $entitySerializer = new EntitySerializer($this->_em);
        $params = array();
        foreach ($entitySerializer->toArray($object) as $key => $value) {
            if (!empty($value))
                $params[$key] = $value;
        }

        return $params;
    }

    public function listarlancamentosCartaoRecorrentePendente(array $params = array())
    {

        $connection = $this->_em->getConnection();
        $sel = "";
        if (!empty($params['recorrente_orderid'])) {
            $sel .= " AND vw.recorrente_orderid = :recorrente_orderid";
        }

        if (!empty($params['id_venda'])) {
            $sel .= " AND vw.id_venda = :id_venda";
        }

        if (!empty($params['id_sistemacobranca'])) {
            $sel .= " AND vw.id_sistemacobranca = :id_sistemacobranca";
        } else {
            $sel .= " AND ( vw.id_sistemacobranca = 30 or vw.id_sistemacobranca is null )";
        }

        if (!empty($params['nu_verificacao'])) {
            $sel .= " AND vw.nu_verificacao between :nu_verificacao AND :nu_verificacaomax";
        } else {
            $sel .= " AND vw.nu_verificacao between 0 AND 30 ";
        }

        if (!empty($params['id_lancamento'])) {
            $sel .= " AND vw.id_lancamento = " . $params['id_lancamento'];
        } else {
            $sel .= "
            AND NOT EXISTS (SELECT tf.id_venda FROM tb_transacaofinanceira AS tf WHERE tf.id_venda = vw.id_venda AND dateadd(HOUR,8,tf.dt_cadastro) > GETDATE())
            AND vw.id_lancamento not in (select id_lancamento from tb_lancamento where id_lancamento = vw.id_lancamento AND dateadd(HOUR,4,dt_atualizado) > GETDATE()) 
            ";
        }

        $params['top'] = 300;


        if (!empty($params['top'])) {
            $top = " top " . $params['top'];
        } else {
            $top = "";
        }

        $sql = "SELECT $top vw.* FROM vw_vendalancamento vw
                  WHERE vw.dt_prevquitado is null
                    AND vw.dt_quitado is null
                    AND vw.bl_quitado = 0
                    AND vw.nu_ordem is not null
                    AND vw.recorrente_orderid is not null
                    AND vw.dt_vencimento <= ( GETDATE() + 5 )
                    AND vw.id_meiopagamento = :idMeioPagamento
                    AND vw.dt_vencimento > (getdate() - 730)

                    AND ((vw.id_sistemacobranca IS NULL and vw.bl_entrada = 0) or vw.id_sistemacobranca IS NOT NULL)

                    AND vw.id_entidade NOT IN (12)
                    
                    AND substring(vw.recorrente_orderid, (len(vw.recorrente_orderid) -2), 3) != 'G1U'
                    
                    AND vw.bl_original = 1
                    
                    {$sel}
                 order by vw.dt_vencimento DESC";


        $statement = $connection->prepare($sql);

        $statement->bindValue('idMeioPagamento', \MeioPagamentoTO::RECORRENTE);

        if (!empty($params['recorrente_orderid'])) {
            $statement->bindValue('recorrente_orderid', $params['recorrente_orderid']);
        }

        if (!empty($params['id_venda'])) {
            $statement->bindValue('id_venda', $params['id_venda']);
        }

        if (!empty($params['id_sistemacobranca'])) {
            $statement->bindValue('id_sistemacobranca', $params['id_sistemacobranca']);
        }

        if (!empty($params['nu_verificacao'])) {
            $statement->bindValue('nu_verificacao', $params['nu_verificacao']);
            $statement->bindValue('nu_verificacaomax', $params['nu_verificacao'] + 5);
        }

        $statement->execute();
        $results = $statement->fetchAll();

        $entitySerializer = new EntitySerializer($this->_em);

        foreach ($results as &$result) {
            $result = $entitySerializer->arrayToEntity($result, new \G2\Entity\VwVendaLancamento());
        }

        return $results;
    }


    /**
     * Retorna o somatório de lançamentos pagos de acordo com o id da matrícula
     * @param $idMatricula
     * @return array
     * @throws \Exception
     */
    public function getTotalLancamentosPagosPorMatricula($matriculas)
    {

        try {

            if (!$matriculas) {
                throw new \Exception("Id Matrícula não informado");
            }
            $qb = $this->_em->createQuery("SELECT SUM(vl.nu_quitado) AS nu_valor, mt.id_venda
                                            FROM \G2\Entity\VwResumoFinanceiro vl
                                            JOIN \G2\Entity\VwMatricula mt WITH vl.id_venda = mt.id_venda
                                            WHERE vl.bl_quitado = :bl_quitado
                                            AND mt.id_matricula IN(" . implode(",", $matriculas) . ")
                                            GROUP BY mt.id_venda")
                ->setParameters(["bl_quitado" => true]);
            $valor = $qb->getOneOrNullResult();
            return $valor;

        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /**
     * Retorna os lançamentos pendentes no plano de pagamento recorrente
     * @return array
     * @throws \Exception
     */
    public function listarLancamentosCartaoRecorrenteACobrar()
    {

        try {

            $sql = "select vw.id_venda, vw.id_lancamento, vw.id_usuariolancamento, vw.st_nomecompleto, eci.st_valor, l.dt_notificacao
                    , (getdate() - cast(eci.st_valor as int)) as dt_limite_notificacao, vw.nu_ordem
                    , vw.dt_emissao, vw.dt_vencimento, vw.dt_quitado, vw.nu_valor 
                    from vw_vendalancamento as vw  join tb_lancamento as l on (l.id_lancamento = vw.id_lancamento)
                    join tb_entidade as e on vw.id_entidade = e.id_entidade
                    join tb_esquemaconfiguracaoitem as eci on e.id_esquemaconfiguracao = eci.id_esquemaconfiguracao and eci.id_itemconfiguracao = 34
                    where vw.id_meiopagamento = 11 
                    and vw.id_evolucao = 9 
                    and vw.bl_quitado = 0 
                    and eci.st_valor is not null
                    and vw.dt_quitado is null 
                    and vw.dt_vencimento < getdate() 
                    and (l.dt_notificacao is null or l.dt_notificacao < getdate() - cast(eci.st_valor as int))
                    and l.bl_cancelamento = 0
                    and l.id_acordo is null
                    and vw.bl_ativo = 1
                    order by vw.id_venda, vw.nu_ordem";
            $statment = $this->_em->getConnection()->prepare($sql);
            $statment->execute();
            $results = $statment->fetchAll();

            return $results;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Salva a data de notificação para controle interno de quando voltar a notificar
     * @param $id_lancamento
     * @throws \Exception
     */
    public function salvarDataNotificacaoRecorrente($id_lancamento)
    {
        try {
            if ($id_lancamento) {
                $query = $this->_em->getConnection()->prepare("update tb_lancamento set dt_notificacao = getdate() WHERE id_lancamento = :id_lancamento");
                $query->bindValue("id_lancamento", $id_lancamento);
                $query->execute();
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Este método serve para recuperar o valor do plano vigente e as parcelas que restam nele a serem pagas
     * @param int $id_venda
     * @return Array
     */
    public function recuperaPlanoEValorRestante($id_venda){
        $connection = $this->_em->getConnection();
        $statement = $connection->prepare(
            "select 
                        (select sum(nu_valor) as valor_total 
                          from vw_vendalancamento 
                            where id_venda = $id_venda and id_transacaoexterna is null and bl_quitado = 0 group by id_venda) as valor_total,
                        (select count(nu_valor) as quantidade 
                          from vw_vendalancamento 
                            where id_venda = $id_venda and id_transacaoexterna is null and bl_quitado = 0  group by id_venda) as quantidade,
                        * 
                      from vw_vendalancamento where id_venda = $id_venda and id_transacaoexterna is null and bl_quitado = 0 order by nu_ordem asc");
        $statement->execute();
        return $statement->fetchAll();
    }
}

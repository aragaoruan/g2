<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe respository responsavel por realizar ações mais complexas a respeito de Perguntas Frequentes
 * Class PerguntasFrequentes
 * @package G2\Repository
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class PerguntasFrequentes extends EntityRepository
{

    /**
     * Retorna os dados da pesquisa do esquema de perguntas
     * @param array $arrColumns
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return object
     * @throws \Zend_Exception
     */
    public function retornarPesquisa(array $arrColumns, array $params = [], $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $qb = $this->createQueryBuilder('es')
                ->select($arrColumns)
                ->where('es.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);

            //todo arrumar a forma de fazer o like, fiz da forma feia
            if (array_key_exists('st_esquemapergunta', $params) && !empty($params['st_esquemapergunta'])) {
                $qb->andWhere("es.st_esquemapergunta LIKE '%{$params['st_esquemapergunta']}%'");
            }


            if (!$orderBy)
                $orderBy = array('st_esquemapergunta' => 'asc');


            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($qb, $orderBy, $limit, $offset, 'es.id_esquemapergunta');

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados da pesquisa. " . $e->getMessage());
        }
    }

    /**
     * Método para retornar dados da pesquisa das perguntas.
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarPesquisaPerguntas(array $params = array())
    {
        try {
            $qb = $this->createQueryBuilder('p')
                ->select(array(
                    'ep.id_esquemapergunta',
                    'ep.st_esquemapergunta',
                    'cp.id_categoriapergunta',
                    'cp.st_categoriapergunta',
                    'p.id_pergunta',
                    'p.st_pergunta',
                    'p.st_resposta'
                ))
                ->innerJoin('p.id_categoriapergunta', 'cp')
                ->innerJoin('cp.id_esquemapergunta', 'ep')
                ->where('p.bl_ativo = :bl_ativo')
                ->andWhere('ep.bl_ativo = :bl_ativo')
                ->andWhere('cp.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);


            if (isset($params['id_esquemapergunta']) && !empty($params['id_esquemapergunta'])) {
                $qb->andWhere('ep.id_esquemapergunta = :id_esquemapergunta')
                    ->setParameter('id_esquemapergunta', $params['id_esquemapergunta']);
            }

            if (isset($params['st_busca']) && !empty($params['st_busca'])) {
                $st_busca = "%" . trim($params['st_busca']) . "%";
                $arrLike[] = 'cp.st_categoriapergunta LIKE :busca';
                $arrLike[] = 'p.st_pergunta LIKE :busca';
                $arrLike[] = 'p.st_resposta LIKE :busca';

                $qb->andWhere(implode(' OR ', $arrLike))
                    ->setParameter('busca', $st_busca);
            }

            $qb->orderBy('cp.st_categoriapergunta', 'asc');

            return $qb->getQuery()->getArrayResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar perguntas. " . $e->getMessage());
        }
    }


}
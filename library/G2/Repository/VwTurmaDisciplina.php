<?php
namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class Disciplina para Repository para Disciplina
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-04-14
 *
 * @package G2\Repository
 */
class VwTurmaDisciplina extends EntityRepository {

    /**
     * Retorna as disciplinas por turmas
     * @param array $params
     * @param $id_entidade
     */
    public function retornaDisciplinasInTurmas($params)
    {
        try {
            $in = ' WHERE id_turma IN ('.implode(',', $params['id']).')';
            $sql = "SELECT DISTINCT vw.id_disciplina, vw.st_disciplina FROM vw_turmadisciplina as vw ".$in."ORDER BY st_disciplina ASC";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {

        }
    }

}
<?php
/**
 * Repository para Entrega de Documentos e visualização dos mesmos.
 * @author Janilson Mendes <janilson.silva@unyleya.com.br>
 *
 * @package G2\Repository
 */

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

class VwEntregaDocumento extends EntityRepository
{
    public function findVwEntregaDocumentos(array $params, array $order = null)
    {
        $query = $this->createQueryBuilder('vw')
                      ->select();

        if (isset($params['id_entidade'])) {
            if (is_array($params['id_entidade'])) {
                $query->andWhere($query->expr()->in('vw.id_entidade', $params['id_entidade']));
            } else {
                $query->andWhere('vw.id_entidade = :id_entidade')
                      ->setParameter('id_entidade', $params['id_entidade']);
            }
        }

        if (isset($params['id_usuario'])) {
            $query->andWhere('vw.id_usuario = :id_usuario')
                  ->setParameter('id_usuario', $params['id_usuario']);
        }

        if (null != $order) {
            foreach ($order as $k => $v) {
                $query->orderBy("vw.{$k}", $v);
            }
        }

        return $query->getQuery()->getArrayResult();
    }
}

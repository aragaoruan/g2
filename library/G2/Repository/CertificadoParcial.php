<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 23/04/18
 * Time: 13:42
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CertificadoParcial
 * @package G2\Repository
 */
class CertificadoParcial extends EntityRepository
{
    public function retornaCertificadoParcial($idEntidade)
    {
        try {
            $sql = "SELECT DISTINCT
                      cp.st_certificadoparcial,
                      cp.id_certificadoparcial,
                      en.id_entidade
                    FROM tb_entidaderelacao AS en
                      JOIN tb_certificadoparcial AS cp ON
                                                  cp.id_entidadecadastro = en.id_entidade
                                                  OR
                                                  cp.id_entidadecadastro = en.id_entidadepai
                                                  AND en.id_entidadepai <> 2
                    WHERE en.id_entidade = {$idEntidade}";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar Certificado Parcial.' . $ex->getMessage());
        }
    }
}

<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class Vwatendente  para Repository para Atendente
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-03-25
 *
 * @package G2\Repository
 */
class VwAtendente extends EntityRepository
{
    /**
     * Retorna dados de motivo
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarAtendente(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $query = $this->createQueryBuilder('atendente')
                ->select()
                ->where('atendente.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            if ($params) {
                if (isset($params['st_nomecompleto']) && $params['st_nomecompleto'] != '') {
                    $query->andWhere("atendente.st_nomecompleto LIKE '%{$params['st_nomecompleto']}%'");
                }
                if (isset($params['st_cpf']) && $params['st_cpf'] != '') {
                    $query->orWhere("atendente.st_cpf LIKE '%{$params['st_cpf']}%'");
                }
                if (isset($params['id_entidadematriz']) && $params['id_entidadematriz'] != '') {
                    $query->andWhere('atendente.id_entidadematriz = :id_entidadematriz')
                        ->setParameter('id_entidadematriz', $params['id_entidadematriz']);
                }
                if (isset($params['id_situacao']) && $params['id_situacao'] != '') {
                    $query->andWhere('atendente.id_situacao = :id_situacao')
                        ->setParameter('id_situacao', $params['id_situacao']);
                }
                if (isset($params['id_nucleotm']) && $params['id_nucleotm'] != '') {
                    $query->andWhere('atendente.id_nucleotm = :id_nucleotm')
                        ->setParameter('id_nucleotm', $params['id_nucleotm']);
                }
            }


            if (!$orderBy)
                $orderBy = array(
                    'st_nomecompleto' => 'asc',
                    'st_nucleotm' => 'asc'
                );


            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'atendente.id_nucleofuncionariotm');


//            $query->orderBy('atendente.st_nomecompleto', 'ASC');
//            $query->addOrderBy('atendente.st_nucleotm', 'ASC');
//            $result = $query->getQuery()->getResult();
//            \Zend_Debug::dump(count($result));
//            \Zend_Debug::dump($query->getQuery());
//            exit;
//            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}

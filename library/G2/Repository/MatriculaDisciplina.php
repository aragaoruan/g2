<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class Matricula
 *
 * @package G2\Repository
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class MatriculaDisciplina extends EntityRepository
{
    /*
     * Retorna dados das disciplinas pela matricula do aluno
     * @param array $params
     * @throws \Zend_Exception
     * @return array
     */
    public function retornarMatriculaDisciplinaByMatricula(array $params = [])
    {
        try {
            $qb = $this->createQueryBuilder('md')
                ->select(array(
                    'md.id_matriculadisciplina',
                    'md.id_matricula',
                    'md.id_evolucao',
                    'md.id_situacao',
                    'disc.id_disciplina',
                    'disc.nu_cargahoraria',
                    'disc.bl_disciplinasemestre'
                ))
                ->join('G2\Entity\Disciplina', 'disc', 'WITH', '
                    disc.id_disciplina = md.id_disciplina
                    ')
                ->andWhere('md.id_matricula = :id_matricula')
                ->setParameter('id_matricula', $params['id_matricula']);

            return $qb->getQuery()->getResult();


        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula disciplina. " . $e->getMessage());
        }
    }

    /**
     * realiza contagem de disciplinas com dependências
     *
     * @param $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function countDisciplinasDependentes($param, $attrToCount)
    {
        try {
            $sql = "SELECT 
                      count(*) as counter
                    FROM 
                      tb_discmatriculadiscorigem
                    WHERE 
                      $attrToCount = " . $param;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll()[0]['counter'];
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function getDadosDiscOrigem($id_matriculadisciplina)
    {
        try {
            $sql = "SELECT
                      do.id_disciplinaorigem, dmdo.id_matriculadisciplina, do.st_disciplina, do.nu_cargahoraria, do.st_instituicao
                    FROM
                      tb_discmatriculadiscorigem AS dmdo
                      JOIN tb_disciplinaorigem AS do ON dmdo.id_disciplinaorigem = do.id_disciplinaorigem
                    WHERE
                      dmdo.id_matriculadisciplina = " . $id_matriculadisciplina;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function getDadosDisciplinaByOrigem($id_disciplinaorigem)
    {
        try {
            $sql = "SELECT
                      md.id_disciplina, dmdo.id_matriculadisciplina, disc.st_disciplina, disc.nu_cargahoraria
                    FROM
                      tb_discmatriculadiscorigem AS dmdo
                      JOIN tb_matriculadisciplina AS md ON dmdo.id_matriculadisciplina = md.id_matriculadisciplina
                      JOIN tb_disciplina AS disc ON md.id_disciplina = disc.id_disciplina
                    WHERE
                      dmdo.id_disciplinaorigem = " . $id_disciplinaorigem;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}

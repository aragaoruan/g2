<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository for EntidadeIntegracao
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2015-10-29
 */
class EntidadeIntegracao extends EntityRepository
{

    /**
     * @param int $id_sistema
     * @param string $url
     * @return array G2\Entity\EntidadeIntegracao
     */
    public function retornarEntidadeIntegracaoPorUrl($id_sistema, $url)
    {
        $query = $this->createQueryBuilder('tb')
            ->where("tb.id_sistema = :id_sistema")
            ->andWhere("tb.st_codsistema LIKE :st_codsistema")
            ->setParameters(array(
                'id_sistema' => $id_sistema,
                'st_codsistema' => "%{$url}%"
            ));
        return $query->getQuery()->getResult();
    }

}
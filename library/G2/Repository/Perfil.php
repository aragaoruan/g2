<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para Perfil
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @since 2015-05-12
 */
class Perfil extends EntityRepository
{

    /**
     * Busca dados da VwPerfilEntidadeRelacao por busca em parte do nome do perfil, e tambem por outros
     * parametros correspondentes as colunas existentes na vw_perfilentidaderelacao
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwPerfilEntidadeRelacaoParteNomePerfil(array $params = array())
    {
        try {

            $query = $this->createQueryBuilder('vw');

            if (array_key_exists('limit', $params)){
                $query->setMaxResults($params['limit']);
            }

            if (array_key_exists('st_nomeperfil', $params)){
                $query->where("vw.st_nomeperfil like '%".$params['st_nomeperfil']."%'");
            }

            //retirando parametro que nao podera mais ser utilizado no filtro
            unset($params['st_nomeperfil'], $params['limit']);

            foreach ($params as $key => $value){
                $query->andWhere("vw.".$key." = :".$key);
                $query->setParameter('id_entidade', $params[$key]);
            }

            return $query->getQuery()->getResult();

        } catch (\Doctrine\ORM\ORMException $ex) {
            throw new \Zend_Exception('Erro ao tentar executar a consulta.' . $ex->getMessage());
        }
    }

}

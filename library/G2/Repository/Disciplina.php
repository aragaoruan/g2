<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class Disciplina para Repository para Disciplina
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-18
 *
 * @package G2\Repository
 */
class Disciplina extends EntityRepository
{
    /**
     * Retorna dados das disciplinas
     * @param array $params
     * @throws \Zend_Exception
     * @return array
     */
    public function retornarDisciplinasRecursivaIMP(array $params = [])
    {
        try {
            $arrWhere = [];

            //verifica se o id_entidade foi passado
            if (isset($params['id_entidade'])) {
                //instancia o Negocio de Entidade
                $entidadeNegocio = new \G2\Negocio\Entidade();
                //busca a entidade
                $resultEntidade = $entidadeNegocio->findEntidade($params['id_entidade']);
                //verifica se tem resultado
                if ($resultEntidade) {
                    //começa a montar a string da busca
                    $str = "(";
                    //atribui os valores do resultado a variavel
                    $id_entidade = $resultEntidade->getId_entidade();
                    $id_entidadecadastro = $resultEntidade->getId_entidadecadastro() ? $resultEntidade->getId_entidadecadastro()->getId_entidade() : NULL;

                    //concatena a string o id_entidade
                    if ($id_entidade) {
                        $str .= "dc.id_entidade = {$id_entidade}";
                    }

                    //verifica se tem id_entidadecadastroe se ele é igual a 19, se for atribui o valor concatenando
                    if ($id_entidadecadastro && $id_entidadecadastro == 19) {
                        $str .= " or dc.id_entidade = {$id_entidadecadastro}";
                    }
                    //concatena a string fechando o parentese
                    $str .= ")";
                    //atribui a string no array
                    $arrWhere[] = $str;
                } else {
                    throw new \Exception("Entidade não encontrada.");
                }

            } else {
                throw new \Exception("id_entidade não informado.");
            }

            $selin = '';
            if (isset($params['com_professores']) && $params['com_professores']) {
                $selin = ' and dc.id_disciplina in (SELECT vw.id_disciplina FROM vw_usuarioperfilentidadereferencia as vw WHERE ( vw.id_perfil = 204 AND vw.id_disciplina = dc.id_disciplina ))';
                unset($params['com_professores']);
            }


            //verifica os outros parametros, percorrendo eles e montando posições no array com a string
            if ($params) {
                foreach ($params as $key => $value) {
                    if ($key != 'id_entidade') {
                        $arrWhere[] = "dc.{$key}='{$value}'";
                    }
                }
            }
            //faz o implode montando uma string para where
            $where = " WHERE " . implode(' AND ', $arrWhere);

            $sql = "SELECT
                    dc.*
                FROM
                    tb_disciplina AS dc
                join tb_entidade e on e.id_entidade = dc.id_entidade
                  {$where}
                  {$selin}
                ORDER BY
                    dc.st_disciplina ASC";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de disciplina. " . $e->getMessage());
        }
    }
} 
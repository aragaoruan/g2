<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class VwProfessorSalaDeAulaLote
 * @package G2\Repository
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwProfessorSalaDeAulaLote extends EntityRepository
{
    /**
     * Retorna pesquisa de professores por parametros
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function pesquisaProfessores(array $params)
    {
        try {
            $qb = $this->createQueryBuilder('vw')
                ->select()->distinct()
                ->where('vw.id_entidade = :id_entidade');

            $qb->setParameter('id_entidade', $params['id_entidade']);

            if($params['bl_professores_indefinidos'] == 'true'){
                $qb->andWhere('vw.st_nomecompleto IS NULL');
            }

            if(array_key_exists('st_saladeaula', $params) && $params['st_saladeaula'] != ''){
                $qb->andWhere('vw.st_saladeaula LIKE :st_saladeaula')
                    ->setParameter('st_saladeaula', "%{$params['st_saladeaula']}%");
            }

            if(array_key_exists('id_disciplina', $params) && (int)$params['id_disciplina']){
                $qb->andWhere('vw.id_disciplina = :id_disciplina')
                    ->setParameter('id_disciplina', $params['id_disciplina']);
            }

            if(array_key_exists('id_usuario', $params) && (int)$params['id_usuario']){
                $qb->andWhere('vw.id_usuario = :id_usuario')
                    ->setParameter('id_usuario', $params['id_usuario']);
            }

            if (array_key_exists('dt_aberturainicio', $params) and array_key_exists('dt_aberturafim', $params)) {
                if (($params['dt_aberturainicio']) != '' and $params['dt_aberturafim'] != '') {

                    $params['dt_aberturainicio'] = new \Zend_Date($params['dt_aberturainicio']);
                    $params['dt_aberturafim'] = new \Zend_Date($params['dt_aberturafim']);

                    $qb->andWhere('vw.dt_abertura BETWEEN :ai AND :at')
                        ->setParameter('ai', $params['dt_aberturainicio']->toString('Y-M-d 00:00:00'))
                        ->setParameter('at', $params['dt_aberturafim']->toString('Y-M-d 23:59:59'));

                }
            }

            if (array_key_exists('dt_encerramentoinicio', $params) and array_key_exists('dt_encerramentofim', $params)) {
                if (($params['dt_encerramentoinicio']) != '' and $params['dt_encerramentofim'] != '') {

                    $params['dt_encerramentoinicio'] = new \Zend_Date($params['dt_encerramentoinicio']);
                    $params['dt_encerramentofim'] = new \Zend_Date($params['dt_encerramentofim']);

                    $qb->andWhere('vw.dt_encerramento BETWEEN :ei AND :et')
                        ->setParameter('ei', $params['dt_encerramentoinicio']->toString('Y-M-d 00:00:00'))
                        ->setParameter('et', $params['dt_encerramentofim']->toString('Y-M-d 23:59:59'));

                }
            }
            return $qb->getQuery()->getResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao efetuar pesquisa:' . $ex->getMessage());
        }
    }

    /**
     * Desativa titularidade do perfil de uma sala de aula da entidade
     * @see \G2\Negocio\Professor::desativaTitulares
     * @param $id_saladeaula
     * @param $id_perfil
     * @param $id_entidade
     * @return bool
     * @throws \Zend_Exception
     */
    public function desativaTitulares($id_saladeaula, $id_perfil, $id_entidade)
    {
        try {

            if (!$id_saladeaula || !$id_perfil)
                throw new \Zend_Exception('O Perfil e a Sala de Aula são obrigatórios');

            $sql = "UPDATE tb_usuarioperfilentidadereferencia
                            SET bl_titular = 0
                            WHERE bl_titular = 1
                            AND bl_ativo = 1
                            AND id_perfil = ".$id_perfil."
                            AND id_saladeaula = ".$id_saladeaula."
                            AND id_entidade = ".$id_entidade;


            $stmt = $this->_em->getConnection()->prepare($sql);
            return $stmt->execute();

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao setar professores para não titulares  - ' . $ex->getMessage());
        }
    }
}

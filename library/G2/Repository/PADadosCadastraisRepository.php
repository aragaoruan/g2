<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \Ead1\Doctrine\EntitySerializer;

class PADadosCadastraisRepository extends EntityRepository {

    private $sessao;

    /**
     * @return \G2\Entity\PADadosContato
     */
    public function findByEntidadeSessao() {
        $this->sessao = new \Zend_Session_Namespace('geral');
        return $this->findOneBy(array('id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * @param integer $id
     * @return \G2\Entity\PADadosContato
     */
    public function findByEntidade($id) {
        return $this->findBy(array('id_entidade' => $id));
    }

    public function save($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = new \G2\Entity\PADadosCadastrais();
        $entidadeNegocio = new \G2\Negocio\Entidade();
        $textoSistemaNegocio = new \G2\Negocio\TextoSistema();

        if (!($data['id_entidade'])) {
            $data['id_entidade'] = $this->sessao->id_entidade;
        }

        if (!($data['id_usuariocadastro'])) {
            $data['id_usuariocadastro'] = $this->sessao->id_usuario;
        }

        $textoCategoria = $this->getEntityManager()->getReference('\G2\Entity\TextoCategoria', \G2\Constante\TextoCategoria::PRIMEIRO_ACESSO);
        $situacao = $this->getEntityManager()->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_TEXTOSISTEMA_ATIVO);
        $textoExibicao = $this->getEntityManager()->getReference('\G2\Entity\TextoExibicao', \G2\Constante\TextoExibicao::SISTEMA);
        $orientacaoTexto = $this->getEntityManager()->getReference('\G2\Entity\OrientacaoTexto', \G2\Constante\OrientacaoTexto::RETRATO);
        $entidade = $this->getEntityManager()->getReference('\G2\Entity\Entidade', $data['id_entidade']);

        $textoMensagem = new \G2\Entity\TextoSistema();
        $textoMensagem->setId_textocategoria($textoCategoria);
        $textoMensagem->setId_situacao($situacao);
        $textoMensagem->setId_textoexibicao($textoExibicao);
        $textoMensagem->setId_orientacaotexto($orientacaoTexto);
        $textoMensagem->setId_usuario($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $textoMensagem->setId_entidade($entidade);
        $textoMensagem->setSt_texto($data['st_textomensagem']);
        $textoMensagem->setSt_textosistema('Primeiro Acesso - Mensagem Dados Cadastrais - ' . $entidade->getSt_nomeentidade());
        $textoMensagem->setDt_cadastro(new \DateTime());
        $textoMensagem->setDt_inicio(new \DateTime());

        $textoInstrucao = new \G2\Entity\TextoSistema();
        $textoInstrucao->setId_textocategoria($textoCategoria);
        $textoInstrucao->setId_situacao($situacao);
        $textoInstrucao->setId_textoexibicao($textoExibicao);
        $textoInstrucao->setId_orientacaotexto($orientacaoTexto);
        $textoInstrucao->setId_usuario($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $textoInstrucao->setId_entidade($entidade);
        $textoInstrucao->setSt_texto($data['st_textoinstrucao']);
        $textoInstrucao->setSt_textosistema('Primeiro Acesso - Instrução Dados Cadastrais - ' . $entidade->getSt_nomeentidade());
        $textoInstrucao->setDt_cadastro(new \DateTime());
        $textoInstrucao->setDt_inicio(new \DateTime());

        $this->getEntityManager()->persist($textoMensagem);
        $this->getEntityManager()->persist($textoInstrucao);

        $assuntoCO = $this->getEntityManager()->getReference('\G2\Entity\AssuntoCo', $data['id_assuntoco']);
        $categoriaOcorrencia = $this->getEntityManager()->getReference('\G2\Entity\CategoriaOcorrencia', $data['id_categoriaocorrencia']);

        $etapa->setId_textomensagem($textoMensagem);
        $etapa->setId_textoinstrucao($textoInstrucao);
        $etapa->setId_assuntoco($assuntoCO);
        $etapa->setId_categoriaocorrencia($categoriaOcorrencia);
        $etapa->setId_usuariocadastro($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $etapa->setId_entidade($entidade);
        $etapa->setDt_cadastro(new \DateTime());

        $this->getEntityManager()->persist($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

    public function update($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = $this->getEntityManager()->getReference('\G2\Entity\PADadosCadastrais', $data['id_pa_dadoscadastrais']);

        $etapa->setId_assuntoco($this->getEntityManager()->getReference('\G2\Entity\AssuntoCo', $data['id_assuntoco']));
        $etapa->setId_categoriaocorrencia($this->getEntityManager()->getReference('\G2\Entity\CategoriaOcorrencia', $data['id_categoriaocorrencia']));
        $etapa->getId_textomensagem()->setSt_texto($data['st_textomensagem']);
        $etapa->getId_textoinstrucao()->setSt_texto($data['st_textoinstrucao']);

        $this->getEntityManager()->merge($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

}
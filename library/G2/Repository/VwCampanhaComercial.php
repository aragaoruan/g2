<?php
/**
 * Repository para Campanhas comerciais
 * @author Elcio Guimarães
 * @since 2017-10-09
 *
 * @package G2\Repository
 */

namespace G2\Repository;

use Doctrine\Common\Util\Debug;
use \Doctrine\ORM\EntityRepository;

class VwCampanhaComercial extends EntityRepository
{

    /**
     * Retorna 1 registro da vw_campanhacomercial
     * @param $id_entidade
     * @param $id_campanhacomercial
     * @param $returnEntity
     * @return array|int
     * @throws \Exception
     */
    public function retornaVwCampanhaComercial($params, $id_entidade, $returnEntity)
    {

        try {

            if (!$id_entidade) throw new \Exception("A Entidade é obrigatória");

            $query = $this->createQueryBuilder('vw')
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $id_entidade);

            if (!empty($params['dt_inicio'])) {
                $query
                    ->andWhere("vw.dt_inicio >= :dt_inicio")
                    ->setParameter('dt_inicio', $params['dt_inicio']);
            }

            if (!empty($params['id_campanhacomercial'])) {
                $query
                    ->andWhere("vw.id_campanhacomercial = :id_campanhacomercial")
                    ->setParameter('id_campanhacomercial', $params['id_campanhacomercial']);
            }

            if (!empty($params['dt_fim'])) {
                $query
                    ->andWhere("vw.dt_fim <= :dt_fim")
                    ->setParameter('dt_fim', $params['dt_fim']);
            }

            if (isset($params['bl_portaldoaluno']) && $params['bl_portaldoaluno'] !== '') {
                $query
                    ->andWhere("vw.bl_portaldoaluno = :bl_portaldoaluno")
                    ->setParameter('bl_portaldoaluno', $params['bl_portaldoaluno']);
            }

            if (isset($params['bl_mobile']) && $params['bl_mobile'] !== '') {
                $query
                    ->andWhere("vw.bl_mobile = :bl_mobile")
                    ->setParameter('bl_mobile', $params['bl_mobile']);
            }

            if (isset($params['bl_vigente']) && $params['bl_vigente'] !== '') {
                $query
                    ->andWhere("vw.bl_vigente = :bl_vigente")
                    ->setParameter('bl_vigente', $params['bl_vigente']);
            }

            if (isset($params['id_situacao']) && $params['id_situacao'] !== '') {
                $query
                    ->andWhere("vw.id_situacao = :id_situacao")
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            if (isset($params['bl_visualizado']) && $params['bl_visualizado'] !== '') {
                $query
                    ->andWhere("vw.bl_visualizado = :bl_visualizado")
                    ->setParameter('bl_visualizado', $params['bl_visualizado']);
            }

            if (!empty($params['id_matricula'])) {
                $query
                    ->andWhere("vw.id_matricula = :id_matricula")
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            $query->setMaxResults(1);


            if ($returnEntity) {
                $results = $query->getQuery()->getSingleResult();
            } else {
                $results = $query->getQuery()->getArrayResult();
                if ($results) $results = $results[0];
            }

            return $results;

        } catch (\Exception $ex) {
            throw $ex;
        }

    }


    /**
     * Retorna as campanhas comerciais
     * @param $params
     * @param $id_entidade
     * @param bool $returnEntities
     * @return array
     * @throws \Exception
     */
    public function listarCampanhas($params, $id_entidade, $returnEntities = true)
    {
        try {

            if (!$id_entidade) throw new \Exception("A Entidade é obrigatória");

            $query = $this->createQueryBuilder('vw')
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $id_entidade);

            if (!empty($params['dt_inicio'])) {
                $query
                    ->andWhere("vw.dt_inicio >= :dt_inicio")
                    ->setParameter('dt_inicio', $params['dt_inicio']);
            }

            if (!empty($params['id_campanhacomercial'])) {
                $query
                    ->andWhere("vw.id_campanhacomercial = :id_campanhacomercial")
                    ->setParameter('id_campanhacomercial', $params['id_campanhacomercial']);
            }

            if (!empty($params['dt_fim'])) {
                $query
                    ->andWhere("vw.dt_fim <= :dt_fim")
                    ->setParameter('dt_fim', $params['dt_fim']);
            }

            if (isset($params['bl_portaldoaluno']) && $params['bl_portaldoaluno'] !== '') {
                $query
                    ->andWhere("vw.bl_portaldoaluno = :bl_portaldoaluno")
                    ->setParameter('bl_portaldoaluno', $params['bl_portaldoaluno']);
            }

            if (isset($params['bl_mobile']) && $params['bl_mobile'] !== '') {
                $query
                    ->andWhere("vw.bl_mobile = :bl_mobile")
                    ->setParameter('bl_mobile', $params['bl_mobile']);
            }

            if (isset($params['bl_vigente']) && $params['bl_vigente'] !== '') {
                $query
                    ->andWhere("vw.bl_vigente = :bl_vigente")
                    ->setParameter('bl_vigente', $params['bl_vigente']);
            }

            if (isset($params['id_situacao']) && $params['id_situacao'] !== '') {
                $query
                    ->andWhere("vw.id_situacao = :id_situacao")
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            if (isset($params['bl_visualizado']) && $params['bl_visualizado'] !== '') {
                $query
                    ->andWhere("vw.bl_visualizado = :bl_visualizado")
                    ->setParameter('bl_visualizado', $params['bl_visualizado']);
            }

            if (!empty($params['id_matricula'])) {
                $query
                    ->andWhere("vw.id_matricula = :id_matricula")
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            if (!empty($params['order']) && is_array($params['order'])) {

                foreach ($params['order'] as $index => $value) {
                    $query->addOrderBy('vw.' . str_replace('vw.', '', $index), $value);
                }

            } else {
                $query->orderBy(
                    !empty($params['order']) ? 'vw.' . $params['order'] : 'vw.bl_visualizado',
                    !empty($params['direction']) ? $params['direction'] : 'DESC');
            }

            if ($returnEntities) {
                $results = $query->getQuery()->getResult();
            } else {
                $results = $query->getQuery()->getArrayResult();
                if (!empty($params['columns'])) {
                    $newResults = array();
                    foreach ($results as $line => $result) {
                        foreach ($result as $key => $value) {
                            if (in_array($key, $params['columns'])) {
                                $newResults[$line][$key] = $value;
                            }
                        }
                    }
                    $results = $newResults;
                } else {
                    return $results;
                }
            }

            return $results;

        } catch (\Exception $ex) {
            throw $ex;
        }
    }


    public function retornarQuantidadeCampanha($params, $id_entidade, $returnEntities = true)
    {
        try {

            if (!$id_entidade) throw new \Exception("A Entidade é obrigatória");

            $query = $this->createQueryBuilder('vw')->select('count(vw.id_campanhacomercial) as total')
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $id_entidade);

            if (!empty($params['dt_inicio'])) {
                $query
                    ->andWhere("vw.dt_inicio >= :dt_inicio")
                    ->setParameter('dt_inicio', $params['dt_inicio']);
            }

            if (!empty($params['dt_fim'])) {
                $query
                    ->andWhere("vw.dt_fim <= :dt_fim")
                    ->setParameter('dt_fim', $params['dt_fim']);
            }

            if (isset($params['bl_portaldoaluno']) && $params['bl_portaldoaluno'] !== '') {
                $query
                    ->andWhere("vw.bl_portaldoaluno = :bl_portaldoaluno")
                    ->setParameter('bl_portaldoaluno', $params['bl_portaldoaluno']);
            }

            if (isset($params['bl_mobile']) && $params['bl_mobile'] !== '') {
                $query
                    ->andWhere("vw.bl_mobile = :bl_mobile")
                    ->setParameter('bl_mobile', $params['bl_mobile']);
            }

            if (isset($params['bl_vigente']) && $params['bl_vigente'] !== '') {
                $query
                    ->andWhere("vw.bl_vigente = :bl_vigente")
                    ->setParameter('bl_vigente', $params['bl_vigente']);
            }

            if (isset($params['id_situacao']) && $params['id_situacao'] !== '') {
                $query
                    ->andWhere("vw.id_situacao = :id_situacao")
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            if (isset($params['bl_visualizado']) && $params['bl_visualizado'] !== '') {
                $query
                    ->andWhere("vw.bl_visualizado = :bl_visualizado")
                    ->setParameter('bl_visualizado', $params['bl_visualizado']);
            }

            if (!empty($params['id_matricula'])) {
                $query
                    ->andWhere("vw.id_matricula = :id_matricula")
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            $result = $query->getQuery()->getSingleResult();

            $result = $result ? $result : array('total' => 0);

            return $result;

        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}

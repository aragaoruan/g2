<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/05/14
 * Time: 14:39
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;
use Ead1\Doctrine\EntitySerializer;


class VwTurmaProjetoVenda extends EntityRepository
{
    /**
     * @param $params
     * @return array
     * @throws \Zend_Exception
     * Executa a consulta a vw_turmaprojetovenda, retornando as vendas de acordo com os parâmetros passados
     */
    public function getVwTurmaProjetoVenda($params)
    {
        try {
            $where = ' where 1 = 1';

            $orderBy = ' order by pp.st_projetopedagogico';

            $sql = '
            select distinct v.id_entidade,
  ent.st_nomeentidade,
  pp.id_projetopedagogico,
  pp.st_projetopedagogico,
  p.id_produto,
  p.st_produto,
  t.st_turma,
  t.id_turma,
  u.id_usuario,
  u.st_nomecompleto,
  u.st_cpf,
  mat.id_matricula,
  v.id_venda,
  CONVERT(VARCHAR(10), v.dt_cadastro , 103) as dt_cadastro ,
  mat.id_situacao as id_situacaomatricula,
  sit.st_situacao as st_situacaomatricula,
  mat.id_evolucao as id_evolucaomatricula,
  evolM.st_evolucao as st_evolucaomatricula,
  v.nu_valorliquido
FROM tb_venda as v
  JOIN tb_vendaproduto as vp on v.id_venda = vp.id_venda and v.bl_ativo = 1
  JOIN tb_produto as p on p.id_produto = vp.id_produto and p.id_tipoproduto = 1 and p.bl_ativo = 1
  JOIN tb_produtoprojetopedagogico as ppp on p.id_produto = ppp.id_produto
  JOIN tb_projetopedagogico as pp on pp.id_projetopedagogico = ppp.id_projetopedagogico
  JOIN tb_turmaprojeto as tpp on tpp.id_projetopedagogico = pp.id_projetopedagogico and tpp.id_turma = ppp.id_turma
  JOIN  tb_turma as t on t.id_turma = tpp.id_turma and t.bl_ativo = 1 and t.id_entidadecadastro = v.id_entidade
  JOIN tb_usuario as u on u.id_usuario = v.id_usuario
  JOIN tb_matricula as mat on mat.id_matricula = vp.id_matricula and mat.bl_ativo = 1 and mat.id_situacao = 50 and mat.id_turma = t.id_turma
  JOIN tb_situacao as sit on sit.id_situacao = mat.id_situacao
  JOIN tb_entidade as ent on v.id_entidade = ent.id_entidade
  JOIN tb_evolucao as evolM on evolM.id_evolucao = mat.id_evolucao
            ';

            if (array_key_exists('id_entidade', $params) && $params['id_entidade']) {

                $where = $where . " and v.id_entidade  in ( {$params['id_entidade']} )";

            }
            if (array_key_exists('id_projeto', $params) && $params['id_projeto'] && $params['id_projeto'] != 0) {
                $where = $where . " and pp.id_projetopedagogico = {$params['id_projeto']}";
            }
            if (array_key_exists('id_turma-produto', $params) && $params['id_turma-produto']) {
                $where = $where . " and t.id_turma = {$params['id_turma-produto']}";
            }
            if (array_key_exists('dt_mes_filtro', $params) && array_key_exists('dt_ano_filtro', $params) && $params['dt_mes_filtro'] && $params['dt_ano_filtro']) {
                $data1 = new \Zend_Date();
                $data1->setDate(01);
                $data1->setMonth((int)$params['dt_mes_filtro']);
                $data1->setYear((int)$params['dt_ano_filtro']);
                $data2 = new \Zend_Date();
                $data2->setDate($data2->get(\Zend_Date::MONTH_DAYS));
                $data2->setMonth((int)$params['dt_mes_filtro']);
                $data2->setYear((int)$params['dt_ano_filtro']);
                $where = $where . " and CAST(v.dt_cadastro as date) between '" . $data1->toString('Y-M-d') . "' and '" . $data2->toString('Y-M-d') . "'";
            }

            $consulta = $sql . $where. $orderBy;
            $query = $this->_em->getConnection()->executeQuery($consulta);
            return $query->fetchAll();

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados da matricula.' . $ex->getMessage());
        }

    }
}
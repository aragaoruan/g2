<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class Reposytory para Repository para Holding
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 08/06/2015
 *
 * @package G2\Repository
 */
class Holding extends EntityRepository
{
    /**
     * Retorna dados de motivo
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarHolding(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('holding')
                ->select(array(
                    'holding.id_holding',
                    'holding.st_holding',
                    'holding.dt_cadastro',
                    'situacao.st_situacao'
                ))
                ->join('holding.id_situacao', 'situacao')
                ->where('1 = 1');
            if ($params) {
                if (isset($params['st_holding']) && $params['st_holding'] != '') {
                    $query->andWhere("holding.st_holding LIKE '%{$params['st_holding']}%'");
                }

                if (isset($params['id_situacao']) && $params['id_situacao'] != '') {
                    $query->andWhere('holding.id_situacao = :id_situacao')
                        ->setParameter('id_situacao', $params['id_situacao']);
                }
            }
            $query->orderBy('holding.st_holding', 'ASC');
            $result = $query->getQuery()->getResult();
            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */

    public function retornaHoldingEntidade(array $params = array())
    {
        try {
            $where = ' where 1=1 ';
            $sql = 'SELECT DISTINCT et.id_entidade FROM dbo.tb_holdingfiliada AS hf
 JOIN dbo.tb_holdingfiliada AS hf2 ON hf2.id_holding = hf.id_holding AND hf.id_entidade != hf2.id_entidade
 JOIN dbo.tb_entidade AS et ON hf2.id_entidade = et.id_entidade
             ';

            if (isset($params['id_entidade'])) {
                $where = 'and hf.id_entidade =' . $params['id_entidade'];
            }
            $sql = $sql . $where;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Repository que retorna todas as entidades de uma mesma holding, aceitando parametros.
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaEntidadesHoldingsParameters(array $params = array())
    {

        try {
            $where = ' where 1=1 ';
            $sql = 'SELECT DISTINCT * FROM vw_entidadesmesmaholdingparams';
            if (array_key_exists('bl_cartacompartilhada', $params) && isset($params['bl_cartacompartilhada'])) {
                $where .= ' and bl_cartacompartilhada =' . $params['bl_cartacompartilhada'];
            }
            if (array_key_exists('id_entidade', $params) && isset($params['id_entidade'])) {
                $where .= ' and id_entidade =' . $params['id_entidade'];
            }
            $sql = $sql . $where;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function getHoldingsByEntidade($id_entidade = null)
    {
        try {
            if (!$id_entidade) {
                throw new \Exception('Informe o ID da Entidade');
            }

            $sql = "
                SELECT
                    *
                FROM
                  tb_holding AS hd
                  JOIN tb_holdingfiliada AS hf ON hf.id_holding = hd.id_holding
                WHERE 
                  hf.id_entidade = {$id_entidade}
                ORDER BY hd.st_holding ASC
            ";

            $query = $this->_em->getConnection()->executeQuery($sql);

            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}

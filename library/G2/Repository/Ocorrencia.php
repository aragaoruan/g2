<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;
use G2\G2Entity;
use G2\Entity\AssuntoCo;
use G2\Entity\OcorrenciaResponsavel;
use G2\Entity\Usuario;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

/**
 * Repository para Ocorrencia
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 08/07/2015
 */
class Ocorrencia extends EntityRepository
{

    /**
     * Pesquisa ocorrencia por parametros
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function pesquisarOcorrencia(array $params = array())
    {
        try {

            $sql = "
                    SELECT DISTINCT
                    oc.id_ocorrencia,
                    oc.st_titulo,
                    oc.dt_cadastro,
                    ev.st_evolucao,
                    oc.id_venda,
                    oc.st_codigorastreio
                    from tb_ocorrencia as oc
                        JOIN tb_venda as vd on vd.id_venda = oc.id_venda
                        JOIN tb_configuracaoentidade as ce on ce.id_assuntorequerimento = oc.id_assuntoco
                        JOIN tb_evolucao as ev on ev.id_evolucao = oc.id_evolucao
                    WHERE 1 = 1 ";


            if (!empty($params['id_entidade'])) {
                $id = $params['id_entidade'];
                $sql .= " AND oc.id_entidade = {$id}";
            }
            if (!empty($params['id_ocorrencia'])) {
                $id_ocorrencia = $params['id_ocorrencia'];
                $sql .= " AND oc.id_ocorrencia = {$id_ocorrencia}";
            }

            if (!empty($params['id_usuario'])) {
                $idusuario = $params['id_usuario'];
                $sql .= " AND oc.id_usuariointeressado = {$idusuario}";
            }

            $sql .= " ORDER BY oc.dt_cadastro DESC";

            $result = $this->_em->getConnection()->executeQuery($sql);
            $result = $result->fetchAll();

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método para retornar os anexos vinculados a ocorrencia
     * @param array $params array(chave=>valor)
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarAnexosOcorrencia(array $params)
    {
        try {
            if ((array_key_exists('id_ocorrencia', $params) && !empty($params['id_ocorrencia']))
                || (array_key_exists('id_upload', $params) && !empty($params['id_upload']))
            ) {
                $qb = $this->createQueryBuilder('vw')
                    ->select('vw.id_upload, vw.st_upload, vw.st_tramite')
                    ->andWhere('vw.id_upload is not null');


                if (array_key_exists('id_ocorrencia', $params) && !empty($params['id_ocorrencia'])) {
                    $qb->andWhere('vw.id_ocorrencia = :id_ocorrencia')
                        ->setParameter('id_ocorrencia', $params['id_ocorrencia']);
                }

                if (array_key_exists('id_upload', $params) && !empty($params['id_upload'])) {
                    $qb->andWhere('vw.id_upload = :id_upload')
                        ->setParameter('id_upload', $params['id_upload']);
                }

                return $qb->getQuery()->getArrayResult();
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar os anexos da ocorrencia. " . $e->getMessage());
        }
    }

    /**
     * Retorna ocorrencias dos alunos para o aplicativo
     * @param $idAluno
     * @param array $idEntidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarOcorrenciasAluno($idAluno, array $idEntidade, $idOcorrencia = null, $order = 'dt_ultimaacao', $direction = 'DESC')
    {
        try {

            if (!$idAluno) {
                throw new \Exception("ID do aluno não informado");
            }

            if (!$idEntidade) {
                throw new \Exception("ID da entidade não informado");
            }


            $qb = $this->createQueryBuilder('m')
                ->where('m.id_usuariointeressado = :id_user')
                ->andWhere("m.id_entidade in(" . implode(',', $idEntidade) . ")")
//                ->andWhere('m.id_evolucao != :evolucao')
                ->setParameter('id_user', $idAluno)
//                ->setParameter('id_entidade', $idEntidade)
//                ->setParameter('evolucao', \G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO)
                ->orderBy('m.' . $order, $direction);

            if ($idOcorrencia) {
                $qb->andWhere('m.id_ocorrencia = :id_ocorrencia');
                $qb->setParameter('id_ocorrencia', $idOcorrencia);
            }

            $ocorrencias = $qb->getQuery()->getArrayResult();
            if ($ocorrencias) {
                foreach ($ocorrencias as $chave => $ocorrencia) {
                    $ocorrencias[$chave]['group'] = $ocorrencias[$chave]['dt_ultimaacao']->format('Y-m') . '-01';
                }
            }
            return $ocorrencias;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar as ocorrencias do aluno. " . $e->getMessage());
        }

    }


    /**
     * Função responsável por buscar no bancos os atendentes responsável por assunto
     *
     * @param $idEntidade
     * @param $tipoServico
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Zend_Exception
     */
    public function retornaAtendentesOcorrencia($idEntidade, $tipoServico)
    {
        try {

            $sqlNucleo = "( SELECT TOP 1 " . $tipoServico;
            $sqlNucleo .= " FROM tb_nucleoco ";
            $sqlNucleo .= "WHERE id_entidade = " . $idEntidade . " AND bl_ativo = 1 AND id_tipoocorrencia = 1 )";

            $sqlAtendente = "SELECT DISTINCT usr.id_usuario, usr.st_nomecompleto";
            $sqlAtendente .= " FROM tb_nucleopessoaco AS ncP ";
            $sqlAtendente .= " JOIN tb_assuntoco AS ac ON ncP.id_assuntoco = ac.id_assuntoco ";
            $sqlAtendente .= " JOIN tb_usuario AS usr ON ncP.id_usuario = usr.id_usuario ";
            $sqlAtendente .= " JOIN tb_nucleoco AS nc ON ncP.id_nucleoco = nc.id_nucleoco ";
            $sqlAtendente .= " WHERE nc.id_entidade = " . $idEntidade . " AND ac.bl_ativo = 1 ";
            $sqlAtendente .= " AND ac.id_assuntoco = " . $sqlNucleo;

            $resultAtendente = $this->_em->getConnection()->executeQuery($sqlAtendente);
            $resultAtendente = $resultAtendente->fetchAll();

            return $resultAtendente;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar os atendetes. " . $e->getMessage());
        }
    }

    /**
     * Função que retorna matriculas canceladas
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarMatriculasCanceladas()
    {
        try {
            $sql = "SELECT DISTINCT
              oc.id_matricula
            FROM tb_matricula as ma
            JOIN tb_ocorrencia oc ON ma.id_matricula = oc.id_matricula AND oc.id_situacao <> :id_situacao
            WHERE ma.id_evolucao = :id_evolucao";

            $statement = $this->_em->getConnection()->prepare($sql);
            $statement->bindValue('id_situacao', \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA);
            $statement->bindValue('id_evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO);
            $statement->execute();
            return $statement->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar Matriculas" . $e->getMessage());
        }
    }
    /**
     * Função que retorna as ocorrencia que estao com evoluçao Agurdando interesado
     * @param $id_matricula
     * @return array
     * @throws \Zend_Exception
     */
    public function ocorrenciaAguardandoInteresado($id_matricula)
    {
        try {

            if ($id_matricula) {
                $sql = "SELECT
                          oc.id_assuntoco,
                          oc.id_ocorrencia,
                          oc.st_titulo,
                          a2.st_assuntoco
                        FROM tb_ocorrencia AS oc
                        INNER JOIN tb_assuntoco a on oc.id_assuntoco = a.id_assuntoco
                        INNER JOIN tb_assuntoco a2 on a2.id_assuntoco = a.id_assuntocopai
                        WHERE id_matricula = :id_matricula AND id_evolucao = :id_evolucao";

                $stmt = $this->_em->getConnection()->prepare($sql);
                $stmt->bindValue('id_matricula', $id_matricula);
                $stmt->bindValue('id_evolucao', \G2\Constante\Evolucao::AGUARDANDO_INTERESSADO);
                $stmt->execute();
                return $stmt->fetchAll();
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar ocorrencia. " . $e->getMessage());
        }

    }

    /**
     * Função responsável por buscar os dados atuais da ocorrência, esse método é chamado em
     * Negocio/Ocorrencia::gerarMensagemTramiteEncaminharOcorrencia()
     * através dele recupero algumas variaveis para compor o texto do tramite da demanda GII-9400
     *
     * @param $idOcorrencia
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Zend_Exception
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function retornaDadosOcorrenciaAtual($idOcorrencia)
    {
        try {
            $sql = "SELECT oc.id_ocorrencia, a.st_assuntoco AS subassunto, aPai.st_assuntoco AS assunto, u.st_nomecompleto";
            $sql .= " FROM tb_ocorrencia oc JOIN tb_assuntoco a ON oc.id_assuntoco = a.id_assuntoco";
            $sql .= " JOIN tb_assuntoco aPai ON a.id_assuntocopai = aPai.id_assuntoco ";
            $sql .= " LEFT JOIN tb_ocorrenciaresponsavel oRes ON oc.id_ocorrencia = oRes.id_ocorrencia";
            $sql .= " LEFT JOIN tb_usuario u ON oRes.id_usuario = u.id_usuario";
            $sql .= " WHERE oc.id_ocorrencia = " . $idOcorrencia . " ORDER BY oRes.dt_cadastro DESC";

            $result = $this->_em->getConnection()->executeQuery($sql);
            $result = $result->fetch();
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da ocorrência atual. Descrição: " . $e->getMessage());
        }
    }

    /**
     * Função responsável por buscar os dados novos da ocorrência, esse método é chamado em
     * Negocio/Ocorrencia::gerarMensagemTramiteEncaminharOcorrencia()
     * através dele recupero algumas variaveis para compor o texto do tramite da demanda GII-9400
     *
     * @param $idAssunto
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Zend_Exception
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function retornaDadosOcorrenciaNova($idAssunto)
    {
        try {
            $sql = " SELECT assunto.st_assuntoco AS subassunto, assuntoPai.st_assuntoco AS assunto ";
            $sql .= " FROM tb_assuntoco AS assunto JOIN tb_assuntoco AS assuntoPai ON assunto.id_assuntocopai = assuntoPai.id_assuntoco ";
            $sql .= " WHERE assunto.id_assuntoco = " . $idAssunto;

            $result = $this->_em->getConnection()->executeQuery($sql);
            $result = $result->fetch();

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da ocorrência nova. Descrição: " . $e->getMessage());
        }
    }
}

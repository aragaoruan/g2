<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04/05/2015
 * Time: 13:51
 */

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository para Cancelamento
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-05-04
 */
class Cancelamento extends EntityRepository
{
    public function retornaValorMateriais(array $params)
    {
        $where = ' WHERE 1=1 AND i.bl_ativo = 1 ';

        if (array_key_exists('id_entidadeatendimento', $params)) {
            $where = $where . ' AND mat.id_entidadeatendimento = ' . $params['id_entidadeatendimento'];
        }

        if (array_key_exists('id_matricula', $params)) {
            $where = $where . ' AND mat.id_matricula = ' . $params['id_matricula'];
        }

        if (array_key_exists('id_situacao', $params)) {
            $where = $where . ' AND em.id_situacao = ' . $params['id_situacao'];
        }

        $groupby = ' GROUP BY mat.id_matricula';

        $sql = 'SELECT
  mat.id_matricula,
  SUM(i.nu_valor) AS nu_valortotal
FROM dbo.tb_itemdematerial AS i
  JOIN dbo.tb_itemdematerialdisciplina AS imd ON imd.id_itemdematerial = i.id_itemdematerial
  JOIN dbo.tb_itemdematerialturma AS imt ON imt.id_material = i.id_itemdematerial
  JOIN dbo.tb_turma AS tur ON tur.id_turma = imt.id_turma
  JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = imd.id_disciplina
  JOIN dbo.tb_matriculadisciplina AS md ON md.id_disciplina = dis.id_disciplina
  JOIN dbo.tb_matricula AS mat ON mat.id_matricula = md.id_matricula AND mat.id_turma = imt.id_turma
  JOIN dbo.tb_tipodematerial AS tmat ON tmat.id_tipodematerial = i.id_tipodematerial
  LEFT JOIN dbo.tb_upload AS up ON up.id_upload = i.id_upload
  LEFT JOIN dbo.tb_entregamaterial AS em ON em.id_itemdematerial = i.id_itemdematerial AND em.id_matriculadisciplina = md.id_matriculadisciplina AND mat.id_matricula = em.id_matricula
  LEFT JOIN dbo.tb_situacao AS sit ON sit.id_situacao = em.id_situacao ';


        $consulta = $sql . $where . $groupby;


        $query = $this->_em->getConnection()->executeQuery($consulta);
        return $query->fetch();

    }

    public function retornaCargaHorariaOfertada(array $params)
    {
        $where = ' AND 1=1';

        $groupby = ' GROUP BY mt.id_matricula;';

        if (array_key_exists('id_matricula', $params)) {
            $where = $where . ' AND mt.id_matricula = ' . $params['id_matricula'];
        }

        $sql = "SELECT  mt.id_matricula ,
        SUM(ct.nu_cargasofertadas) AS nu_somacargaofertada
FROM    tb_matricula AS mt
        JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
        JOIN dbo.tb_cancelamento AS cc ON cc.id_cancelamento = mt.id_cancelamento
        CROSS APPLY ( SELECT    igh.id_turma ,
                                igh.id_unidade ,
                                igh.id_disciplina ,
                                ( MAX(igh.nu_encontro) OVER ( PARTITION BY igh.id_disciplina,
                                                              igh.id_turma,
                                                              igh.id_unidade )
                                  * et.nu_horasencontro ) AS nu_cargasofertadas
                      FROM      dbo.tb_itemgradehoraria AS igh
                                JOIN dbo.tb_configuracaoentidade AS et ON et.id_entidade = igh.id_unidade
                                JOIN dbo.tb_turno AS tn ON tn.id_turno = igh.id_turno
                      WHERE     igh.id_turma = mt.id_turma
                                AND igh.id_unidade = mt.id_entidadeatendimento
                                AND cc.dt_solicitacao <= CAST(CAST(igh.dt_diasemana AS VARCHAR)
                                + ' ' + CAST(tn.hr_inicio AS VARCHAR) AS DATETIME2)
                    ) AS ct
WHERE   md.id_disciplina = ct.id_disciplina
";

        $consulta = $sql . $where . $groupby;


        $query = $this->_em->getConnection()->executeQuery($consulta);
        return $query->fetch();
    }
}
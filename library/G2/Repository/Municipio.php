<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class Municipio extends EntityRepository
{

    public function findMunicipiosLike($sg_uf, $st_nomemunicipio, $limit)
    {
        $qb = $this->createQueryBuilder("m");
        $qb->select('m')
            ->where('m.sg_uf = :sg_uf')
            ->andWhere($qb->expr()->like('m.st_nomemunicipio', $qb->expr()->literal("%$st_nomemunicipio%")))
            ->setParameter('sg_uf', $sg_uf)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }
}
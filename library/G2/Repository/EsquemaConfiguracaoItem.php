<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 11/08/2015
 * Time: 16:44
 */

namespace G2\Repository;

use Doctrine\Common\Util\Debug;
use \Doctrine\ORM\EntityRepository;

/**
 * Class EsquemaConfiguracaoItem
 * @package G2\Repository
 */
class EsquemaConfiguracaoItem extends EntityRepository
{
    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaEsquemaConfiguracaoItem(array $params = array())
    {
        try {


            $qb = $this->createQueryBuilder('eci')
                ->select('eci, ec, ic')
                ->leftjoin('eci.id_esquemaconfiguracao', 'ec')
                ->leftjoin('eci.id_itemconfiguracao', 'ic');


            if ($params) {
                if (array_key_exists('id_esquemaconfiguracao', $params) && !empty($params['id_esquemaconfiguracao'])) {
                    $qb->andWhere('eci.id_esquemaconfiguracao = :id_esquemaconfiguracao')
                        ->setParameter('id_esquemaconfiguracao', $params['id_esquemaconfiguracao']);
                }

            }
            $results = $qb->getQuery()->getResult();
            $arrayResults = array();
            if ($results) {
                foreach ($results as $result) {
                    $arrayResults[] = array(
                        'id_esquemaconfiguracao' => array(
                            'id_esquemaconfiguracao' => $result->getid_esquemaconfiguracao()->getid_esquemaconfiguracao(),
                            'st_esquemaconfiguracao' => $result->getid_esquemaconfiguracao()->getst_esquemaconfiguracao(),
                        ),
                        'id_itemconfiguracao' => array(
                            'id_itemconfiguracao' => $result->getid_itemconfiguracao()->getid_itemconfiguracao(),
                            'st_itemconfiguracao' => $result->getid_itemconfiguracao()->getst_itemconfiguracao(),
                            'st_descricao' => $result->getid_itemconfiguracao()->getst_descricao(),
                            'st_default' => $result->getid_itemconfiguracao()->getst_default(),
                        ),
                        'st_valor' => $result->getst_valor()
                    );
                }
            }

            return $arrayResults;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return array|bool
     * @throws \Zend_Exception
     */
    public function retornaEsquemaConfiguracaoEntidade(array $params = array())
    {

        try {

            $sql = 'SELECT ec.*, ic.*, eci.st_valor, e.st_nomeentidade, e.id_entidade
                        FROM tb_entidade e
                            INNER JOIN tb_esquemaconfiguracao ec ON e.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
                            INNER JOIN tb_esquemaconfiguracaoitem eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
                            INNER JOIN tb_itemconfiguracao ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao
                            INNER JOIN tb_configuracaoentidade ce ON ce.id_entidade = e.id_entidade';

            if (array_key_exists('id_entidade', $params)) {
                $sql .= ' AND e.id_entidade = ' . $params['id_entidade'];
                unset($params['id_entidade']);
            }

            if (array_key_exists('id_esquemaconfiguracao', $params)) {
                $sql .= ' AND eci.id_esquemaconfiguracao = ' . $params['id_esquemaconfiguracao'];
                unset($params['id_esquemaconfiguracao']);
            }

            if (array_key_exists('id_itemconfiguracao', $params)) {
                $sql .= ' AND eci.id_itemconfiguracao = ' . $params['id_itemconfiguracao'];
                unset($params['id_itemconfiguracao']);
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }

    }
}

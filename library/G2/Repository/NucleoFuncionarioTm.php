<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 31-03-2015
 * Time: 11:36
 */

namespace G2\Repository;
use Doctrine\ORM\EntityRepository;
/**
 * Class NucleoTm  para Repository para Nucleo Funcionários
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-03-31
 *
 * @package G2\Repository
 */

class NucleoFuncionarioTm extends EntityRepository{

} 
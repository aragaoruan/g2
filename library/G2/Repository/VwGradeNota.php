<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class VwGradeNota  para Repository para VwGradeNota
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-03-25
 *
 * @package G2\Repository
 */
class VwGradeNota extends EntityRepository
{
    /**
     * Retorna a grade horaria considerando disciplinas insentas
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function returnGradeWithInsentos(array $params = array(), $orderBy = null)
    {
        try {
            $query = $this->createQueryBuilder('vw')
                ->select()
                ->where('1=1');

            if ($params) {

                if (isset($params['id_matricula']) && $params['id_matricula']) {
                    $query->andWhere("vw.id_matricula = :id_matricula")
                        ->setParameter('id_matricula', $params['id_matricula']);
                }

                if (isset($params['id_categoriasala']) && $params['id_categoriasala']
                    && array_key_exists('id_situacao', $params) && $params['id_situacao']
                ) {

                    $query->andWhere("vw.id_categoriasala = :id_categoriasala or vw.id_situacao =:id_situacao")
                        ->setParameter('id_categoriasala', $params['id_categoriasala'])
                        ->setParameter('id_situacao', $params['id_situacao']);

                } elseif (isset($params['id_categoriasala']) && $params['id_categoriasala']
                    && !array_key_exists('id_situacao', $params)
                ) {
                    $query->andWhere("vw.id_categoriasala = :id_categoriasala")
                        ->setParameter('id_categoriasala', $params['id_categoriasala']);
                }
            }
            if ($orderBy) {
                $count = 0;
                foreach ($orderBy as $key => $ob) {
                    if ($count == 0) {
                        $query->orderBy('vw.' . $key, $ob);
                        $count++;
                    } else {
                        $query->addOrderBy('vw.' . $key, $ob);
                    }
                }
            }

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


    /**
     * Método criado para substituir a query que fazia na \G2\Negocio\Avaliacao::concluirMatricula() linha 256 adiante
     * Ao invés de consultar os dados nessa vw e verificar se retornou dados, agora faz um count na query que é
     * mais rápido
     * @param $idMatriculaDisciplina
     * @return int
     * @throws \Exception
     */
    public function verificaSalaNaoEncerrada($idMatriculaDisciplina, $idMatricula)
    {
        try {

            $query = $this->createQueryBuilder("vw")
                ->select("count(vw.id_matriculadisciplina)")
                ->where("vw.id_matriculadisciplina = :id_matriculadisciplina")
                ->andWhere("vw.dt_encerramento >= :dt_encerramento")
                ->andWhere("vw.id_categoriasala = :id_categoriasala")
                ->andWhere("vw.id_matricula = :id_matricula")
                ->setParameter("id_matriculadisciplina", $idMatriculaDisciplina)
                ->setParameter("dt_encerramento", date('Y-m-d'))
                ->setParameter("id_categoriasala", \G2\Constante\CategoriaSala::PRR)
                ->setParameter("id_matricula", $idMatricula);

            return (int)$query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }


}

<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use G2\Constante\Evolucao;
use G2\Constante\Situacao;

/**
 * Class GradeHoraria para Repository para Grade Horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-03
 *
 * @package G2\Repository
 */
class GradeHoraria extends EntityRepository
{
    /**
     * Retorna dados de grade horaria
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarPesquisaGrade(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $query = $this->createQueryBuilder('gh')
                ->select(array(
                    'gh.id_gradehoraria',
                    'gh.st_nomegradehoraria',
                    'gh.dt_iniciogradehoraria',
                    'gh.dt_fimgradehoraria',
                    'gh.dt_cadastro',
                    'gh.bl_ativo',
                    's.id_situacao',
                    's.st_situacao'
                ))
                ->join('gh.id_situacao', 's')
                ->where('gh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);


            if (isset($params['st_nomegradehoraria']) && !empty($params['st_nomegradehoraria'])) {
                $query->andWhere('gh.st_nomegradehoraria LIKE :st_nomegradehoraria')
                    ->setParameter('st_nomegradehoraria', '%' . $params['st_nomegradehoraria'] . '%');
            }

            if (isset($params['dt_iniciogradehoraria']) && !empty($params['dt_iniciogradehoraria'])) {
                $query->andWhere('gh.dt_iniciogradehoraria >= :dt_iniciogradehoraria')
                    ->setParameter('dt_iniciogradehoraria', $params['dt_iniciogradehoraria']);
            }

            if (isset($params['dt_fimgradehoraria']) && !empty($params['dt_fimgradehoraria'])) {
                $query->andWhere('gh.dt_fimgradehoraria <= :dt_fimgradehoraria')
                    ->setParameter('dt_fimgradehoraria', $params['dt_fimgradehoraria']);
            }

            if (isset($params['id_situacao']) && !empty($params['id_situacao'])) {
                $query->andWhere('gh.id_situacao <= :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            if (isset($params['id_entidade']) && !empty($params['id_entidade'])) {
                $query->andWhere('gh.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }

            if (isset($params['id_projetopedagogico']) && !empty($params['id_projetopedagogico'])) {
                $query->andWhere('gh.id_projetopedagogico = :id_projetopedagogico')
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
            }

            if (!$orderBy)
                $orderBy = array('id_gradehoraria' => 'ASC');

            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'gh.id_gradehoraria');

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Verifica Se o Local de Aula (sala) dessa unidade e turno, já possui algum Professor.
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function verificaLocalAulaUnidadeTurnoGrade(array $params)
    {
        try {
            $query = $this->createQueryBuilder('igh')
                ->select(array(
                    'igh.id_itemgradehoraria',
                    'u.id_usuario',
                    'u.st_nomecompleto',
                    'la.id_localaula',
                    'la.st_localaula'
                ))
                ->join('igh.id_gradehoraria', 'gh')
                ->join('igh.id_unidade', 'e')
                ->join('igh.id_turno', 't')
                ->join('igh.id_localaula', 'la')
                ->join('igh.id_diasemana', 'ds')
                ->join('igh.id_professor', 'u');

            if ($params) {
                $query->where('gh.bl_ativo = :bl_ativo')
                    ->andWhere('igh.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', true)
                    ->andWhere('e.id_entidade = :id_unidade')
                    ->setParameter('id_unidade', $params['id_unidade'])
                    ->andWhere('gh.id_gradehoraria = :id_gradehoraria')
                    ->setParameter('id_gradehoraria', $params['id_gradehoraria'])
                    ->andWhere('t.id_turno = :id_turno')
                    ->setParameter('id_turno', $params['id_turno'])
                    ->andWhere('la.id_localaula = :id_localaula')
                    ->setParameter('id_localaula', $params['id_localaula'])
                    ->andWhere('ds.id_diasemana = :id_diasemana')
                    ->setParameter('id_diasemana', $params['id_diasemana']);

                if (isset($params['id_itemgradehoraria'])) {
                    $query->andWhere('igh.id_itemgradehoraria NOT IN (:id_itemgradehoraria)')
                        ->setParameter('id_itemgradehoraria', $params['id_itemgradehoraria']);
                }

            }
            if ($query->getQuery()->getResult())
                return $query->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar validação 1. " . $e->getMessage());
        }
    }

    /**
     * Verifica se Já existe na grade aquele Professor, naquele Dia e Turno.
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function verificaProfessorDiaTurno(array $params)
    {
        try {

            $query = $this->createQueryBuilder('igh')
                ->select(array(
                    'igh.id_itemgradehoraria',
                    'u.id_usuario',
                    'u.st_nomecompleto',
                    't.id_turno',
                    't.st_turno',
                    'e.id_entidade',
                    'e.st_nomeentidade',
                    'ds.id_diasemana',
                    'ds.st_diasemana',
                    'gh.st_nomegradehoraria',
                    'gh.id_gradehoraria',
                    'ta.st_turno as st_turnoaula'
                ))
                ->join('igh.id_unidade', 'e')
                ->join('igh.id_gradehoraria', 'gh')
                ->join('igh.id_turno', 't')
                ->join('igh.id_professor', 'u')
                ->join('igh.id_diasemana', 'ds')
                ->join('igh.id_turnoaula', 'ta')
                ->join('igh.id_tipoaula', 'tipoaula')
                ->where('gh.bl_ativo = :bl_ativo')
                ->andWhere('igh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);

            if ($params) {
//                $query->andWhere('gh.id_gradehoraria = :id_gradehoraria')
//                    ->setParameter('id_gradehoraria', $params['id_gradehoraria']);
                $query->andWhere('t.id_turno = :id_turno')
                    ->setParameter('id_turno', $params['id_turno']);
                $query->andWhere('ds.id_diasemana = :id_diasemana')
                    ->setParameter('id_diasemana', $params['id_diasemana']);
                $query->andWhere('u.id_usuario = :id_professor')
                    ->setParameter('id_professor', $params['id_professor']);
                $query->andWhere('igh.dt_diasemana BETWEEN :dt_inicio AND :dt_fim')
                    ->setParameter('dt_inicio', $params['dt_inicio'])
                    ->setParameter('dt_fim', $params['dt_fim']);
                $query->andWhere('ta.id_turno = :id_turnoaula')
                    ->setParameter('id_turnoaula', $params['id_turnoaula']);
                $query->andWhere('tipoaula.id_tipoaula = :id_tipoaula')
                    ->setParameter('id_tipoaula', $params['id_tipoaula']);

                if (isset($params['id_itemgradehoraria']) && !empty($params['id_itemgradehoraria'])) {
                    $query->andWhere('igh.id_itemgradehoraria NOT IN (:id_itemgradehoraria)')
                        ->setParameter('id_itemgradehoraria', $params['id_itemgradehoraria']);
                }
            }
            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar validação 2. " . $e->getMessage());
        }
    }

    /**
     * Verifica se a Turma já está tendo aula naquele Dia e Turno.
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function verificaTurmaDiaTurno(array $params)
    {
        try {
            $query = $this->createQueryBuilder('igh')
                ->select(array(
                    'tm.id_turma',
                    'tm.st_turma',
                    'tn.id_turno',
                    'tn.st_turno',
                    'e.id_entidade',
                    'e.st_nomeentidade',
                    'd.id_disciplina',
                    'd.st_disciplina',
                    'ds.id_diasemana',
                    'ds.st_diasemana',
                    'ta.st_turno as st_turnoaula',

                ))
                ->join('igh.id_turma', 'tm')
                ->join('igh.id_gradehoraria', 'gh')
                ->join('igh.id_turno', 'tn')
                ->join('igh.id_unidade', 'e')
                ->join('igh.id_disciplina', 'd')
                ->join('igh.id_diasemana', 'ds')
                ->join('igh.id_turnoaula', 'ta')
                ->join('igh.id_tipoaula', 'tipoaula')
                ->where('gh.bl_ativo = :bl_ativo')
                ->andWhere('igh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);


            if ($params) {
                $query->andWhere('gh.id_gradehoraria = :id_gradehoraria')
                    ->setParameter('id_gradehoraria', $params['id_gradehoraria']);
                $query->andWhere('tn.id_turno = :id_turno')
                    ->setParameter('id_turno', $params['id_turno']);
                $query->andWhere('ds.id_diasemana = :id_diasemana')
                    ->setParameter('id_diasemana', $params['id_diasemana']);
                $query->andWhere('igh.dt_diasemana = :dt_diasemana')
                    ->setParameter('dt_diasemana', $params['dt_diasemana']);
                $query->andWhere('tm.id_turma = :id_turma')
                    ->setParameter('id_turma', $params['id_turma']);
                $query->andWhere('ta.id_turno = :id_turnoaula')
                    ->setParameter('id_turnoaula', $params['id_turnoaula']);
                $query->andWhere('tipoaula.id_tipoaula = :id_tipoaula')
                    ->setParameter('id_tipoaula', $params['id_tipoaula']);;

                if (isset($params['id_itemgradehoraria'])) {
                    $query->andWhere('igh.id_itemgradehoraria NOT IN(:id_itemgradehoraria)')
                        ->setParameter('id_itemgradehoraria', $params['id_itemgradehoraria']);
                }
            }
            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar validação 3. " . $e->getMessage());
        }
    }

    /**
     * Retorna lista de professores buscando os itens da grade horaria
     * @param array $params
     * @throws \Zend_Exception
     * @return mixed
     */
    public function retornarProfessoresItens(array $params = null)
    {
        try {
            $query = $this->createQueryBuilder('igh')
                ->select('
                    IDENTITY(igh.id_gradehoraria) as id_gradehoraria,
                    IDENTITY(igh.id_unidade) as id_unidade,
                    u.id_usuario,
                    u.st_nomecompleto,
                    e.st_nomeentidade,
                    gh.dt_iniciogradehoraria,
                    gh.dt_fimgradehoraria')
                ->distinct()
                ->join('igh.id_gradehoraria', 'gh')
                ->join('igh.id_professor', 'u')
                ->join('igh.id_entidadecadastro', 'e')
                ->where('igh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);

            if (isset($params['id_gradehoraria']) && !empty($params['id_gradehoraria'])) {
                $query->andWhere('igh.id_gradehoraria = :id_gradehoraria')
                    ->setParameter('id_gradehoraria', $params['id_gradehoraria']);
            }
            if (isset($params['id_professor']) && !empty($params['id_professor'])) {
                $query->andWhere('igh.id_professor = :id_professor')
                    ->setParameter('id_professor', $params['id_professor']);
            }

            if (isset($params['dt_inicial']) && !empty($params['dt_inicial'])) {
                $query->andWhere('gh.dt_iniciogradehoraria >= :dt_inicial')
                    ->setParameter('dt_inicial', $params['dt_inicial']);
            }

            if (isset($params['dt_termino']) && !empty($params['dt_termino'])) {
                $query->andWhere('gh.dt_fimgradehoraria <= :dt_termino')
                    ->setParameter('dt_termino', $params['dt_termino']);
            }


            $query->orderBy('u.st_nomecompleto', 'asc');

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar GradeHoraria::retornarProfessoresItens(). " . $e->getMessage());
        }
    }

    /**
     * Retorna dados para relatorio de grade horaria
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarDadosRelatorioGrade(array $params)
    {
        try {

            $query = $this->createQueryBuilder('igh')
                ->select(array(
                    'igh.id_itemgradehoraria',
                    'igh.dt_diasemana',
                    'igh.st_observacao',
                    'igh.nu_encontro',
                    'gh.id_gradehoraria',
                    'gh.st_nomegradehoraria',
                    'gh.dt_iniciogradehoraria',
                    'gh.dt_fimgradehoraria',
                    'pp.id_projetopedagogico',
                    'pp.st_apelido',
                    'pp.st_projetopedagogico',
                    'e.id_entidade',
                    'e.st_nomeentidade',
                    'e.st_urlimglogo',
                    't.id_turno',
                    't.st_turno',
                    'tm.id_turma',
                    'tm.st_turma',
                    'tm.dt_inicio AS dt_inicioturma',
                    'tm.dt_fim as dt_fimturma',
                    'tm.st_codigo as st_codigoturma',
                    'ds.id_diasemana',
                    'ds.st_diasemana',
                    'us.id_usuario',
                    'us.st_nomecompleto',
                    'dp.id_disciplina',
                    'dp.st_disciplina',
                    'dp.nu_cargahoraria',
                    'ta.st_turno as st_turnoaula',
                    'tpa.st_tipoaula',
                    'tpa.id_tipoaula'
                ))
                ->join('igh.id_gradehoraria', 'gh')
                ->join('igh.id_unidade', 'e')
                ->join('igh.id_projetopedagogico', 'pp')
                ->join('igh.id_turno', 't')
                ->join('igh.id_turma', 'tm')
                ->join('igh.id_diasemana', 'ds')
                ->join('igh.id_professor', 'us')
                ->join('igh.id_disciplina', 'dp')
                ->join('igh.id_turnoaula', 'ta')
                ->leftJoin('igh.id_tipoaula', 'tpa');


            //parametros fixos da consulta
            $query->where('igh.bl_ativo = :bl_ativo')
                ->andWhere('gh.bl_ativo = :bl_ativo')
                ->andWhere('e.bl_ativo = :bl_ativo')
                ->andWhere('pp.bl_ativo = :bl_ativo')
                ->andWhere('tm.bl_ativo = :bl_ativo')
                ->andWhere('tm.id_evolucao = ' . Evolucao::TB_TURMA_CONFIRMADO)
                ->andWhere('tm.id_situacao = ' . Situacao::TB_TURMA_ATIVA)
                ->andWhere('gh.id_situacao in(' . Situacao::TB_GRADEHORARIA_ATIVO . ',' . Situacao::TB_GRADEHORARIA_CONFIRMADO . ')')
                ->setParameter('bl_ativo', true);

            //monta os parametros dinamicos
            if (isset($params['id_entidade'])) {
                $query->andWhere('e.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }

            if (isset($params['id_gradehoraria'])) {
                $query->andWhere('gh.id_gradehoraria IN(' . $params['id_gradehoraria'] . ')');
            }

            if (isset($params['id_professor'])) {
                $query->andWhere('us.id_professor = :id_professor')
                    ->setParameter('id_professor', $params['id_professor']);
            }

            if (isset($params['id_projetopedagogico'])) {
                $query->andWhere('pp.id_projetopedagogico = :id_projetopedagogico')
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
            }

            if (isset($params['dt_inicial']) && !empty($params['dt_inicial'])) {
                $query->andWhere('gh.dt_iniciogradehoraria >= :dt_inicio')
                    ->setParameter('dt_inicio', $params['dt_inicial'] . ' 00:00:00');
            }

            if (isset($params['dt_termino']) && !empty($params['dt_termino'])) {
                $query->andWhere('gh.dt_fimgradehoraria <= :dt_termino')
                    ->setParameter('dt_termino', $params['dt_termino'] . ' 23:59:59');
            }

            $query->orderBy('igh.id_turno', 'asc');
            $query->addOrderBy('igh.dt_diasemana', 'asc');

            return $query->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar GradeHoraria::retornarProfessoresItens(). " . $e->getMessage());
        }
    }

    public function retornarPesquisaItemGradeHoraria($params)
    {
        try {
            $query = $this->createQueryBuilder('igh')
                ->select(array(
                    'gh.id_gradehoraria',
                    'gh.st_nomegradehoraria',
                    'gh.dt_iniciogradehoraria',
                    'gh.dt_fimgradehoraria',
                    'gh.dt_cadastro',
                    'gh.bl_ativo',
                    's.id_situacao',
                    's.st_situacao',
                    'igh.id_itemgradehoraria',
                    'u.st_nomeentidade as st_unidades'
                ))
                ->join('igh.id_gradehoraria', 'gh')
                ->join('gh.id_situacao', 's')
                ->join('igh.id_unidade', 'u')
                ->where('gh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1)
                //SITUAÇAO
                ->andWhere('gh.id_situacao = :situacao')
                ->setParameter('situacao', Situacao::TB_GRADEHORARIA_ATIVO);

            if (isset($params['dt_iniciogradehoraria']) && !empty($params['dt_iniciogradehoraria'])) {
                $query->andWhere('gh.dt_iniciogradehoraria >= :dt_iniciogradehoraria')
                    ->setParameter('dt_iniciogradehoraria', $params['dt_iniciogradehoraria']);
            }

            if (isset($params['dt_fimgradehoraria']) && !empty($params['dt_fimgradehoraria'])) {
                $query->andWhere('gh.dt_fimgradehoraria <= :dt_fimgradehoraria')
                    ->setParameter('dt_fimgradehoraria', $params['dt_fimgradehoraria']);
            }

            if (isset($params['id_unidade']) && !empty($params['id_unidade'])) {
                $query->andWhere('igh.id_unidade = :id_unidade')
                    ->setParameter('id_unidade', $params['id_unidade']);
            }

            return $query->getQuery()->getArrayResult();


        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna a grade horaria considerando os parametros dos itens da grade horaria
     *
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaRelatorioControleTurmaGradeHoraria($params)
    {
        try {
            $query = $this->createQueryBuilder('gh')
                ->select(array(
                    'gh.id_gradehoraria',
                    'gh.st_nomegradehoraria',
                    'gh.dt_iniciogradehoraria',
                    'gh.dt_fimgradehoraria'
//                    'u.id_entidade',
//                    'en.st_endereco',
//                    'u.st_nomeentidade as st_unidades'
                ))
                ->leftJoin('gh.id_gradehoraria', 'igh')
//                ->join('gh.id_situacao', 's')
//                ->join('igh.id_unidade', 'u')
                ->where('gh.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1)
                //SITUAÇAO
                ->andWhere('gh.id_situacao = :situacao')
                ->setParameter('situacao', Situacao::TB_GRADEHORARIA_ATIVO);

            if (isset($params['id_unidade']) && !empty($params['id_unidade'])) {
                $query->andWhere('igh.id_unidade = :id_unidade')
                    ->setParameter('id_unidade', $params['id_unidade']);
            }

            if (isset($params['id_projetopedagogico']) && !empty($params['id_projetopedagogico'])) {
                $query->andWhere('igh.id_projetopedagogico = :id_projetopedagogico')
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
            }

            if (isset($params['id_turma']) && !empty($params['id_turma'])) {
                $query->andWhere('igh.id_turma = :id_turma')
                    ->setParameter('id_turma', $params['id_turma']);
            }

            $query->groupBy(' igh.id_gradehoraria, gh.st_nomegradehoraria, gh.dt_iniciogradehoraria, gh.dt_fimgradehoraria');
            return $query->getQuery()->getArrayResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
} 
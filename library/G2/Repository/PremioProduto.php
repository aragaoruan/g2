<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class PremioProduto para Repository para PremioProduto
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-06-25
 *
 * @package G2\Repository
 */
class PremioProduto extends EntityRepository
{

    /**
     * Retorna dados de motivo
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaPremioProduto(array $params = array(), $orderBy = null, $limit = 0, $offset = 0){
        try{
            $query = $this->createQueryBuilder('pp')
                ->select(
                    'p.id_produto as id',
                    'p.id_produto',
                    'p.st_produto',
                    'tp.id_tipoproduto',
                    'tp.st_tipoproduto'
                )
                ->join('pp.id_produto','p')
                ->join('p.id_tipoproduto', 'tp')
                ->where('pp.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            if($params){
                if (isset($params['id_premio'])) {
                    $query->andWhere("pp.id_premio = :id_premio")
                        ->setParameter('id_premio',$params['id_premio']);
                }
                if (isset($params['id_campanhacomercial'])) {
                    $query->andWhere('pp.id_campanhacomercial = :id_campanhacomercial')
                        ->setParameter('id_campanhacomercial', $params['id_campanhacomercial']);
                }
            }

            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'pp.id_produto');

        }catch(\Exception $e){
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
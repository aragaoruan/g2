<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para ComissaoLancamento
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-05-15
 */
class ComissaoLancamento extends EntityRepository
{

    /**
     * Retorna dados da Vw
     * @param array $where array chave=>valor para consulta
     * @return array Resultado da Query
     * @throws \Zend_Exception
     */
    public function retornarDadosVwComissaoReceber(array $where)
    {
        try {
            $sql = $this->montaStringSql('vw_comissaoreceber', $where);
            $query = $this->_em->getConnection()->executeQuery($sql, $where);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados de vw_comissaoreceber.' . $ex->getMessage());
        }
    }

    /**
     * Monta os parametros na string sql
     * @param string $from Nome da tabela ou view
     * @param array $where Array chave=>valor
     * @param string $columnsReturn Nome das colunas separado por virgula
     * @return string Retorna string sql SELECT :colunas FROM :tabela WHERE parametro = :parametro
     */
    private function montaStringSql($from, array $where = array(), $columnsReturn = "*")
    {
        //monta a string do select até o FROm
        $sql = "SELECT ";
        $sql .= $columnsReturn;
        $sql .= " FROM " . $from;
        //verifica se passou o where e monta a condição da consulta
        if ($where) {
            $sql .= " WHERE 1=1 ";
            foreach ($where as $key => $value) {
                $sql .= " AND ";
                $sql .= $key . " = :" . $key;
            }
        }

        return $sql;
    }

    /**
     * Retorna Dados de comissao lancamento dos coordenadores de disciplina
     * @param integer $idUsuario
     * @param integer $idEntidade
     * @param string $dt_inicio
     * @param string $dt_termino
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaExtratoCoordenadorDisciplina($idUsuario, $idEntidade, $dt_inicio, $dt_termino)
    {
        try {
            $sql = "SELECT * FROM fn_extratocomissaodireitos ({$idUsuario},{$idEntidade},'{$dt_inicio}','{$dt_termino}')";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber dos coordenadores de disciplina.' . $ex->getMessage());
        }
    }

    /**
     * Retorna Dados de comissao lancamento dos coordenadores de projeto
     * @param integer $idUsuario
     * @param integer $idEntidade
     * @param datetime $dt_inicio
     * @param datetime $dt_termino
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaExtratoCoordenadorProjeto($idUsuario, $idEntidade, $dt_inicio, $dt_termino)
    {
        try {
            $sql = "select * from fn_extratocomissaodireitos_coordenador({$idUsuario},{$idEntidade},'{$dt_inicio}','{$dt_termino}') ORDER BY st_projetopedagogico";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber dos coordenadores de projeto.' . $ex->getMessage());
        }
    }

    /**
     * Busca dados da comissão receber baseado no id_usuario e array com id das vendas
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaComissaoLancamentoParaPagamento(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('cl')
                ->where('cl.id_lancamentopagamento IS NULL');

            if ($params['id_usuario']) {
                $query->andWhere('cl.id_usuario = :id_usuario')
                    ->setParameter('id_usuario', $params['id_usuario']);
            }
            if (isset($params['vendas'])) {
                $query->andWhere("cl.id_vendaproduto IN(" . implode(',', $params['vendas']) . ")");
            }
            return $query->getQuery()->getResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber.' . $ex->getMessage());
        }
    }

    /**
     * Retorna dados de Usuarios Comissionados
     * @param array $where
     * @param array $orderBy
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarUsuariosComissionados(array $where = array(), array $orderBy = array())
    {
        try {

            $qb = $this->createQueryBuilder('vw')
                ->select();

            //monta o where
            if ($where) {
//                $where['st_nomecompleto'] = 'kak';
                if (!empty($where['st_cpf'])) {
                    $qb->andWhere('vw.st_cpf = :st_cpf')
                        ->setParameter('st_cpf', $where['st_cpf']);
                }
                if (!empty($where['st_nomecompleto'])) {
                    $qb->andWhere('vw.st_nomecompleto LIKE :st_nomecompleto')
                        ->setParameter('st_nomecompleto', "%" . $where['st_nomecompleto'] . "%");
                }
                if (!empty($where['id_entidade'])) {
                    $qb->andWhere('vw.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $where['id_entidade']);
                }
                if (!empty($where['id_usuario'])) {
                    $qb->andWhere('vw.id_usuario = :id_usuario')
                        ->setParameter('id_usuario', $where['id_usuario']);
                }
                if (!empty($where['id_funcao'])) {
                    $qb->andWhere('vw.id_funcao = :id_funcao')
                        ->setParameter('id_funcao', $where['id_funcao']);
                }
            }

            //monta o order by
            if ($orderBy) {
                $arrOrder = array();
                foreach ($orderBy as $column => $order) {
                    $arrOrder[] = " {$column} {$order} ";
                    $qb->addOrderBy("vw." . $column, $order);
                }
            }


            return $qb->getQuery()->getArrayResult();

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber.' . $ex->getMessage());
        }

    }

    /**
     * Retorna dados para aba de autorizações
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaAutorizacoesPagamento($params = array())
    {
        try {
            $where = ' WHERE ps.id_usuario = cr.id_usuario AND cr.id_entidade = ps.id_entidade';
            $where2 = ' WHERE 1=1';
            $whereOuter = 'AND 1=1';
            $orderby = ' order by nu_valor desc';
            if ($params['id_entidade']) {
                $where2 .= ' and id_entidade = ' . $params['id_entidade'];
            }

            if ($params['dt_inicial'] && $params['dt_final']) {
                $where .= " AND cr.dt_confirmacao <= '" . $params['dt_final'] . "'";
            }

            if ($params['dt_inicial'] && $params['dt_final']) {
                $whereOuter .= " AND lc.dt_vencimento <= '" . $params['dt_final'] . "'";
            }


            if ($params['st_cpf']) {
                $where2 .= " AND st_cpf = '" . $params['st_cpf'] . "'";
            }

            if ($params['st_nomecompleto']) {
                $where2 .= " AND ps.st_nomeexibicao like '%" . $params['st_nomecompleto'] . "%'";
            }


            $GROUPBY = ' GROUP BY  cr.id_usuario ,
                                cr.st_funcao,
                                cr.id_funcao,
                                cr.st_cpf
                    ) AS crs ';

            $outerapply = 'OUTER APPLY ( SELECT   cl.id_usuario,
                                         SUM(ISNULL(lc.nu_valor,0)) AS nu_soma
                              FROM      dbo.tb_comissaolancamento AS cl
                                        JOIN dbo.tb_lancamento AS lc ON cl.id_lancamento = lc.id_lancamento
                                        JOIN dbo.tb_disciplina AS dc ON cl.id_disciplina = dc.id_disciplina
                                        WHERE lc.id_entidade = ps.id_entidade
                                        AND cl.bl_adiantamento = 1
                                        AND cl.id_usuario =ps.id_usuario AND lc.bl_quitado = 0 ' . $whereOuter . ' GROUP BY cl.id_usuario) AS lca ';

            $sql = 'SELECT  crs.id_usuario ,
                    ps.st_nomeexibicao ,
                    ( crs.nu_valor - ISNULL(lca.nu_soma,0) ) AS nu_valor ,
                    crs.st_funcao,
                    crs.id_funcao,
                    crs.st_cpf
                    FROM    tb_pessoa AS ps
                    CROSS APPLY ( SELECT    SUM(cr.nu_comissaoreceber) AS nu_valor ,
                                cr.id_usuario ,
                                cr.st_funcao,
                                cr.id_funcao,
                                cr.st_cpf
                      FROM      dbo.vw_comissaoreceber AS cr';

            $stringSql = $sql . $where . $GROUPBY . $outerapply . $where2 . $orderby;

            $query = $this->_em->getConnection()->executeQuery($stringSql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber.' . $e->getMessage());
        }
    }

    /**
     * Busca os projetos pedagogicos vinculados as disciplinas para o extrato
     * @param integer $idDisciplina
     * @param integer $idUsuario
     * @param integer $idEntidade
     * @param date $dtInicio
     * @param date $dtFim
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaProjetoDetalheExtrato($idDisciplina, $idUsuario, $idEntidade, $dtInicio, $dtFim)
    {
        try {

            $sql = "SELECT
                      crs.id_usuario,
                      crs.st_projetopedagogico,
                      crs.id_projetopedagogico,
                      crs.id_disciplina,
                      crs.nu_valor,
                      crs.nu_valorvenda,
                      crs.nu_ponderacaoaplicada,
                      crs.nu_porcentagem
                    FROM tb_pessoa AS ps
                    CROSS APPLY (
                      SELECT
                        sum(cr.nu_comissaoreceber) AS nu_valor,
                        cr.id_usuario,
                        cr.st_projetopedagogico,
                        cr.id_projetopedagogico,
                        cr.id_disciplina,
                        SUM(cr.nu_valorliquido) as nu_valorvenda,
                        cr.nu_ponderacaoaplicada,
                        cr.nu_porcentagem
                        
                      FROM dbo.vw_comissaoreceber AS cr
                      WHERE ps.id_usuario = cr.id_usuario
                      AND ps.id_entidade = cr.id_entidade
                      AND dt_confirmacao BETWEEN '{$dtInicio}' AND '{$dtFim}'
                      AND cr.id_disciplina = {$idDisciplina}
                      GROUP BY cr.id_usuario, 
                      cr.st_projetopedagogico, 
                      cr.id_projetopedagogico, 
                      cr.id_disciplina, 
                      cr.nu_ponderacaoaplicada,
                      cr.nu_porcentagem) AS crs
                    WHERE ps.id_usuario = {$idUsuario}
                    AND id_entidade = {$idEntidade}";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar projeto para detalhe do extrato. " . $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVendaProdutos($params = array())
    {
        try {
            $where = ' WHERE 1=1';

            if ($params['id_entidade']) {
                $where .= ' and id_entidade = ' . $params['id_entidade'];
            }

            if ($params['id_usuario']) {
                $where .= ' and id_usuario = ' . $params['id_usuario'];
            }

            if ($params['dt_final']) {
                $where .= " AND dt_confirmacao <= '" . $params['dt_final'] . "'";
            }
            $sql = 'SELECT * from vw_comissaoreceber';

            $stringSql = $sql . $where;
            $query = $this->_em->getConnection()->executeQuery($stringSql);
            return $query->fetchAll();
        } catch
        (\Exception $e) {
            throw new \Zend_Exception('Erro ao buscar dados de comissao receber.' . $e->getMessage());
        }
    }

    /**
     * Método para montar o insert e processar os dados para autorizações de pagamento.
     * @param array $paramsBuscaAutorizacoes parametros para retornar a consulta das autorizações
     * @param $id_lancamento
     * @return bool
     * @throws \Zend_Exception
     */
    public function processarAutorizacoesdePagamentoLote(array $paramsBuscaAutorizacoes, $id_lancamento)
    {
        try {

            $sqlSelect = "SELECT
                id_usuario,
                {$id_lancamento} ,
                id_vendaproduto,
                0,
                id_disciplina,
                id_entidade,
                id_funcao,
                1
            FROM
                vw_comissaoreceber
            WHERE
                id_entidade = {$paramsBuscaAutorizacoes['id_entidade']}
            AND id_usuario =  {$paramsBuscaAutorizacoes['id_usuario']}
            AND dt_confirmacao <= '{$paramsBuscaAutorizacoes['dt_final']}'";

            $sqlInsert = "INSERT INTO dbo.tb_comissaolancamento (
                                id_usuario,
                                id_lancamento,
                                id_vendaproduto,
                                bl_adiantamento,
                                id_disciplina,
                                id_entidade,
                                id_perfilpedagogico,
                                bl_autorizado
                            )
                                {$sqlSelect}";


            $query = $this->_em->getConnection()->prepare($sqlInsert);
            return $query->execute();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao processar autorizações de pagamentos em lote. " . $e->getMessage());
        }
    }

    /**
     * Método para retornar resumo de pagamento efetuados ao colaborador
     * @param array $params array de parametros chave => valor
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaListaPagamentosEfetuados(array $params)
    {
        try {
            $qb = $this->createQueryBuilder('cl')
                ->select(array(
                    'us.id_usuario',
                    'lc.id_lancamento',
                    'lc.nu_valor',
                    'lc.dt_quitado',
                    'dc.id_disciplina',
                    'dc.st_disciplina'
                ))
                ->distinct()
                ->join('cl.id_lancamento', 'lc')
                ->join('cl.id_usuario', 'us')
                ->join('cl.id_disciplina', 'dc');

            $qb->where('lc.bl_ativo = :bl_ativo')
                ->andWhere('dc.bl_ativa = :bl_ativo')
                ->andWhere('lc.bl_quitado = :bl_ativo')
                ->setParameter('bl_ativo', true);

            if ($params) {
                if (array_key_exists('id_usuario', $params) && !empty($params['id_usuario'])) {
                    $qb->andWhere('us.id_usuario = :id_usuario')
                        ->setParameter('id_usuario', $params['id_usuario']);
                }

                if (
                    (array_key_exists('dt_inicio', $params) && !empty($params['dt_inicio']))
                    && (array_key_exists('dt_termino', $params) && !empty($params['dt_termino']))
                ) {
                    $qb->andWhere('lc.dt_quitado BETWEEN :dt_inicio AND :dt_termino')
                        ->setParameter('dt_inicio', $params['dt_inicio'])
                        ->setParameter('dt_termino', $params['dt_termino']);
                }

                if (array_key_exists('id_entidade', $params) && !empty($params['id_entidade'])) {
                    $qb->andWhere('cl.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $params['id_entidade']);
                }
            }
            $qb->orderBy('lc.dt_quitado', 'desc');

            return $qb->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar pagamentos efetuados. " . $e->getMessage());
        }
    }

    /**
     * Retorna os registros que estão marcados como autorizados
     * @param array $params chave=>valor
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarLancamentosAutorizados(array $params = [])
    {
        try {

            //monta o query builder com os relacionamentos necessarios
            $qb = $this->createQueryBuilder('cl')
                ->distinct()
                ->select(array(
                    'us.id_usuario',
                    'us.st_nomecompleto',
                    'us.st_cpf',
                    'lc.id_lancamento',
                    'lc.nu_valor'
                ))
                ->join('cl.id_lancamento', 'lc')
                ->join('cl.id_usuario', 'us')
                ->join('cl.id_vendaproduto', 'vp')
                ->where('cl.bl_adiantamento = :bl_adiantamento')
                ->setParameter('bl_adiantamento', 0)
                ->andWhere('cl.bl_autorizado = :bl_autorizado')
                ->setParameter('bl_autorizado', 1)
                ->andWhere('lc.bl_quitado = :bl_quitado')
                ->setParameter('bl_quitado', 0);

            //verifica se foi passado parametros
            if ($params) {
                if (array_key_exists('id_entidade', $params) && !empty($params['id_entidade'])) {
                    $qb->andWhere('cl.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $params['id_entidade']);
                }
                if (array_key_exists('st_cpf', $params) && !empty($params['st_cpf'])) {
                    $qb->andWhere('us.st_cpf = :st_cpf')
                        ->setParameter('st_cpf', $params['st_cpf']);
                }
                if (array_key_exists('st_nomecompleto', $params) && !empty($params['st_nomecompleto'])) {
                    $qb->andWhere('us.st_nomecompleto LIKE :st_nomecompleto')
                        ->setParameter('st_nomecompleto', "%{$params['st_nomecompleto']}%");
                }
            }

            $qb->orderBy('us.st_nomecompleto', 'desc');

            return $qb->getQuery()->getArrayResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar lançamentos autorizados. " . $e->getMessage());
        }
    }
}

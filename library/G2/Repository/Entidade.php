<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository for Entidade
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> - 2014-12-01
 * @since 2013-12-09
 */
class Entidade extends EntityRepository
{

    /**
     * Retorna pesquisa de entidades
     * @param array $params parametros de pesquisa
     * @return array|bool
     * @throws \Zend_Exception
     */
    public function pesquisaEntidade(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('vw')
                ->where('1=1');
            // Percorre os parametros monta a clausula e seta o valor

            $where = array();
            if (isset($params['st_razaosocial']) && !empty($params['st_razaosocial'])) {
                $where[] = "vw.st_razaosocial LIKE '%" . $params['st_razaosocial'] . "%'";
            }
            if (isset($params['st_nomeentidade']) && !empty($params['st_nomeentidade'])) {
                $where[] = "vw.st_nomeentidade LIKE '%" . $params['st_nomeentidade'] . "%'";
            }
            if (isset($params['st_cnpj']) && !empty($params['st_cnpj'])) {
                $where[] = "vw.st_cnpj LIKE '%" . $params['st_cnpj'] . "%'";
            }
            if (isset($params['id_entidade']) && !empty($params['id_entidade'])) {
                $where[] = "vw.id_entidade LIKE '%" . $params['id_entidade'] . "%'";
            }
            if (isset($params['id_situacao']) && !empty($params['id_situacao'])) {
                $where[] = "vw.id_situacao LIKE '%" . $params['id_situacao'] . "%'";
            }
            if ($where) {
                $query->andWhere(implode(" OR ", $where));
            }
            //Ordena a consulta
            $query->orderBY('vw.st_razaosocial', 'ASC');
            //Executa a query
            return $query->getQuery()->getResult();
        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna dados da vw_entidaderecursivaid
     * @param integer $idEntidade
     * @return mixed
     * @throws Zend_Exception
     */
    public function retornarEntidadesRecursivaById($idEntidade)
    {
        try {
            $query = $this->createQueryBuilder('vw')
                ->distinct()
                ->select(array('vw.id_entidade', 'vw.st_nomeentidade'))
                ->where('vw.nu_entidadepai = :id_entidade')
                ->orWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $idEntidade)
                ->orderBy('vw.st_nomeentidade', 'ASC');

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new Zend_Exception($e->getMessage());
        }
    }

}

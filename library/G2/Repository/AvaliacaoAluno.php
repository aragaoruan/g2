<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AvaliacaoAluno
 * @package G2\Repository
 */
class AvaliacaoAluno extends EntityRepository
{


    /**
     * Se o aluno tiver nota de APROVEITAMENTO para essa avaliação, a nota do Moodle NÃO pode sobrescreve-la
     * @param $id_matricula
     * @param $id_avaliacao
     * @param $id_avaliacaoconjuntoreferencia
     * @return int
     * @throws \Exception
     */
    public function verificarAlunoNotaAproveitamento($id_matricula, $id_avaliacao, $id_avaliacaoconjuntoreferencia)
    {
        try {
            $query = $this->createQueryBuilder("aa")
                ->select("count(aa.id_matricula)")
                ->where("aa.id_matricula = :id_matricula")
                ->andWhere("aa.bl_ativo = :bl_ativo")
                ->andWhere("aa.id_avaliacao = :id_avaliacao")
                ->andWhere("aa.id_avaliacaoconjuntoreferencia = :id_avaliacaoconjuntoreferencia")
                ->andWhere("aa.id_tiponota = :id_tiponota")
                ->setParameter("id_matricula", $id_matricula)
                ->setParameter("id_avaliacao", $id_avaliacao)
                ->setParameter("id_avaliacaoconjuntoreferencia", $id_avaliacaoconjuntoreferencia)
                ->setParameter("id_tiponota", \G2\Constante\TipoNota::APROVEITAMENTO)
                ->setParameter("bl_ativo", true);

            return (int)$query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }

}

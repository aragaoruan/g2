<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class VwGradeNota  para Repository para Pessoa
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2017-03-07
 *
 * @package G2\Repository
 */
class Pessoa extends EntityRepository
{
    /**
     * Retorna as pessoas para serem sincronizadas com outros sistemas
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function returnPessoasSync($params = array(), $or = array())
    {
        try {
            if ($params || $or) {
                $query = $this->createQueryBuilder('p')
                    ->select()
                    ->where('1=1');

                if ($params) {
                    foreach ($params as $key => $value) {
                        $query->andWhere('p.' . $key . ' = :' . $key);
                        $query->setParameter($key, $value);
                    }
                }

                if ($or) {
                    $andOR = array();
                    foreach ($or as $key => $value) {
                        array_push($andOR, 'p.' . $key . ' = :' . $key);
                        $query->setParameter($key, $value);
                    }
                    $query->andWhere(implode(' or ', $andOR));
                }

                return $query->getQuery()->getResult();
            } else {
                return array('mensagem' => 'É obrigatório pelomenos um parâmetro ou um sistema.');
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
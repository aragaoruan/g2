<?php
namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para Aproveitamento
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2016-01-27
 */
class Aproveitamento extends EntityRepository
{

    /**
     * Retorna as disciplinas com seus modulos relacionados.
     * @param $params
     * @return array
     * @throws \Zend_Exception
     */
    public function findModulosDisciplinasAproveitamento($params)
    {
        try {
            $where = ' where 1=1';

            if (array_key_exists('id_matricula', $params)) {
                $where .= ' AND m.id_matricula= ' . $params['id_matricula'];
            }

            $sql = 'SELECT mo.*, md.* FROM tb_matricula  AS m
JOIN dbo.vw_modulodisciplina AS mo ON mo.id_projetopedagogico = m.id_projetopedagogico
JOIN dbo.tb_matriculadisciplina AS md ON md.id_matricula = m.id_matricula AND md.id_disciplina = mo.id_disciplina
' . $where;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param int $id_matriculadisciplina
     * @return array
     * @throws \Zend_Exception
     */
    public function getDisciplinasOriginais($id_matriculadisciplina)
    {
        try {
            $sql = "
            SELECT 
                  md.id_matricula,
                  md.id_matriculadisciplina,
                  dmdo.id_discmatriculadiscorigem,
                  do.st_disciplina,
                  do.id_disciplinaorigem,
                  do.st_disciplina,
                  do.nu_cargahoraria,
                  do.st_instituicao
            FROM 
                tb_matriculadisciplina AS md
                JOIN tb_discmatriculadiscorigem AS dmdo ON dmdo.id_matriculadisciplina = md.id_matriculadisciplina
                JOIN tb_disciplinaorigem AS do ON do.id_disciplinaorigem = dmdo.id_disciplinaorigem
            WHERE 
                md.id_matriculadisciplina = {$id_matriculadisciplina}
              ";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

}
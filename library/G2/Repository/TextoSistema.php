<?php
/**
 * Class Repository for Texto Sistema
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Repository;


class TextoSistema extends \Doctrine\ORM\EntityRepository
{

    public function findTextoSistemaProdutoDeclaracao(array $params)
    {
        try {

            $sql = "SELECT DISTINCT
                        tp.id_projetopedagogico,
                        tp.st_projetopedagogico
                    FROM
                        vw_turmaprojetopedagogico as tp
                    join tb_turma as t on t.id_turma = tp.id_turma and t.bl_ativo = 1
                    WHERE
                        tp.bl_ativo = 1
                    and t.id_situacao = " . \G2\Constante\Situacao::TB_TURMA_ATIVA . "
                    and t.id_evolucao = " . \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO . "
                    AND (t.dt_fim >= (GETDATE() - 10) OR t.dt_fim IS NULL)";


            if ($entidades) {
                $sql .= " AND tp.id_entidadecadastro in(" . implode(',', $entidades) . ") ";
            }

            //Ordena a consulta
            $sql .= ' ORDER BY tp.st_projetopedagogico ASC';
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar projetos de forma recursiva. " . $e->getMessage());
        }

    }

}
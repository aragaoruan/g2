<?php
/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 19/05/14
 * Time: 10:09
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class UsuarioPerfilEntidadeReferencia extends EntityRepository
{
    /**
     * Retorna Disciplinas dos Colaboradores
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaDisciplinas(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('uper')
                ->select('d.id_disciplina', 'd.st_disciplina')
                ->join('uper.id_disciplina', 'd')
                ->where('uper.id_disciplina IS NOT NULL');

            if ($params) {
                if ($params['id_entidade']) {
                    $query->andWhere('uper.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $params['id_entidade']);
                }
                if ($params['id_usuario']) {
                    $query->andWhere('uper.id_usuario = :id_usuario')
                        ->setParameter('id_usuario', $params['id_usuario']);
                }
            }
            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar disciplinas ' . $e->getMessage());
        }
    }
} 
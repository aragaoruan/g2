<?php


namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class VwMatriculaAlocacao
 * @package G2\Repository
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class VwMatriculaAlocacao extends EntityRepository
{
    /*
     * Método que retorna as matriculas de uma determinada sala de aula
     *
     * @funcionalidade: Prorrogação de Sala de Aula
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaAlunosProjetoPedagogico(array $params)
    {
        try {
            $qb = $this->createQueryBuilder('ma')
                ->select()
                ->where(' 1 = 1 ');
            if ($params['id_saladeaula']) {
                $qb->andWhere('ma.id_saladeaula = :id_saladeaula')
                    ->setParameter('id_saladeaula', $params['id_saladeaula']);
            }
            if ($params['dt_inicio'] && $params['dt_termino']) {
                $qb->andWhere('ma.dt_cadastro BETWEEN :dt_inicio AND :dt_termino')
                    ->setParameter('dt_inicio', $params['dt_inicio'])
                    ->setParameter('dt_termino', $params['dt_termino']);
            }

            return $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao gerar as matriculas. " . $e->getMessage());
        }
    }
}
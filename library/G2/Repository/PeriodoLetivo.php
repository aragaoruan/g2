<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository PeriodoLetivo
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2016-02-02
 */
class PeriodoLetivo extends EntityRepository
{

    /**
     * @description Retorna as proximas 5 Ofertas de Sala (tb_periodoletivo) a partir de uma Data, filtrando por Entidade e Tipo de Oferta
     * @param string $dt_abertura
     * @param int $id_entidade
     * @param int $id_tipooferta
     * @return \G2\Entity\VwPeriodoLetivoEntidade[]|null
     */
    public function findProximasOfertasPorData($dt_abertura = null, $id_entidade = null, $id_tipooferta = \G2\Constante\TipoOferta::PADRAO)
    {
        $negocio = new \G2\Negocio\PeriodoLetivo();

        $dt_abertura = $dt_abertura ? $negocio->converterData($dt_abertura, 'Y-m-d') : date('Y-m-d');
        $id_entidade = $id_entidade ? $id_entidade : $negocio->sessao->id_entidade;

        $query = $this
            ->_em->getRepository('\G2\Entity\VwPeriodoLetivoEntidade')
            ->createQueryBuilder('pl')
            ->where('pl.dt_abertura >= :dt_abertura')
            ->andWhere('pl.id_entidade = :id_entidade')
            ->andWhere('pl.id_tipooferta = :id_tipooferta')
            ->setParameters(array(
                'dt_abertura'   => $dt_abertura,
                'id_entidade'   => $id_entidade,
                'id_tipooferta' => $id_tipooferta
            ))
            ->orderBy('pl.dt_abertura', 'ASC')
            ->setMaxResults(5);

        return $query->getQuery()->getResult();
    }

}

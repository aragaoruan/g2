<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class TransacaoFinanceira extends EntityRepository {

    public function retornaTransacaoFinanceiraLancamento($parametros) {
        try {
            $sql = 'SELECT *
                    FROM tb_transacaofinanceira tf
                      JOIN tb_transacaolancamento tl ON tf.id_transacaofinanceira = tl.id_transacaofinanceira
                    WHERE id_statustransacao in ( 187, 188, 189 )
                        AND id_lancamento = '. $parametros['id_lancamento'];

            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();
            return $result;

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao executar a consulta. ' . $ex->getMessage());
        }
    }
}

<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use G2\Constante\Situacao;

/**
 * Repository for Prevenda
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-06
 */
class PrevendaProduto extends EntityRepository
{

    /**
     * Get PrevendaProduto Join Entity Prevenda
     * @param array $params Params from query
     * @return mixed/array Return object Doctrine
     * @throws Zend_Exception
     */
    public function getPrevendaProdutoJoinPrevenda(array $params)
    {
        try {

            $query = $this->createQueryBuilder('pvp')
                ->join('pvp.id_prevenda', 'pv')
                ->where('1=1');
            //Set Parametrer from query
            if (isset($params['id_entidade'])) {
                $query->andWhere('pv.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }
            if (isset($params['id_situacao'])) {
                $query->andWhere('pv.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }

            if (!empty($params['dt_inicio'])) {
                $query->andWhere('pv.dt_cadastro >= :dt_inicio')
                    ->setParameter('dt_inicio', $params['dt_inicio'] . ' 00:00:00');
            }

            if (!empty($params['dt_fim'])) {
                $query->andWhere('pv.dt_cadastro <= :dt_fim')
                    ->setParameter('dt_fim', $params['dt_fim'] . ' 23:59:59');
            }

            if (!empty($params['id_usuarioatendimento'])) {
                $query->andWhere('pv.id_usuarioatendimento = :id_usuarioatendimento')
                    ->setParameter('id_usuarioatendimento', $params['id_usuarioatendimento']);
            }

            return $query->getQuery()->getResult();

        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Get PrevendaProduto Join Entity Prevenda
     * @param array $params Params from query
     * @return mixed/array Return object Doctrine
     * @throws Zend_Exception
     */
    public function getPreVendas(array $params)
    {
        try {
            $sql = "
                    SELECT 
                        pvp.id_prevendaproduto ,
                        pvp.id_produto,
                        pv.id_prevenda,
                        pv.id_usuario, 
                        pv.id_entidade, 
                        pv.st_nomecompleto, 
                        pv.st_cpf,
                        pv.st_email,
                        pv.nu_telefonecelular,
                        pv.nu_dddcelular,
                        pv.nu_ddicelular,
                        pv.nu_telefoneresidencial,
                        pv.nu_ddiresidencial,
                        pv.nu_dddresidencial,
                        pv.nu_telefonecomercial,
                        pv.nu_dddcomercial,
                        pv.nu_ddicomercial,
                        pv.st_orgaoexpeditor,
                        pv.id_pais,
                        pv.sg_uf,
                        pv.id_municipio,
                        pv.st_cep,
                        pv.st_endereco,
                        pv.st_bairro,
                        pv.st_complemento, 
                        pv.nu_numero,
                        pv.st_sexo,
                        pv.dt_nascimento,
                        pv.id_paisnascimento,
                        pv.sg_ufnascimento,
                        pv.id_municipionascimento,
                        pv.dt_cadastro,
                        pv.bl_ativo,
                        pv.st_rg,
                        pv.id_tipoendereco,
                        pv.st_contato,
                        pv.id_nivelensino,
                        pv.id_usuarioatendimento,
                        pv.id_usuariodescarte,
                        pv.dt_atendimento,
                        pv.dt_descarte,
                        pv.id_situacao,
                        a.st_nomecompleto AS st_nomecompletoatendente,
                        interacao.dt_ultimainteracao,
                        pp.st_produto,
                        s.st_situacao
                    FROM tb_prevendaproduto pvp
                    JOIN tb_prevenda pv ON pvp.id_prevenda = pv.id_prevenda
                    JOIN tb_produto pp ON pvp.id_produto = pp.id_produto
                    JOIN tb_situacao s ON s.id_situacao = pv.id_situacao
                    LEFT JOIN vw_atendentes a ON pv.id_usuarioatendimento = a.id_usuario 
                          AND pv.id_entidade = a.id_entidade
                    OUTER APPLY (
                        SELECT MAX(dt_cadastro) AS dt_ultimainteracao 
                            FROM tb_tramite t
                            JOIN tb_tramiteprevenda tpv ON t.id_tramite = tpv.id_tramite
                            WHERE tpv.id_prevenda = pv.id_prevenda
                    ) AS interacao WHERE 1 = 1";


            if (!empty($params['id_entidade'])) {
                $sql .= ' AND pv.id_entidade = ' . $params['id_entidade'];
            }

            if (!empty($params['id_situacao'])) {
                $sql .= ' AND pv.id_situacao = ' . $params['id_situacao'];
            }

            if (!empty($params['busca']) && $params['busca'] == 'atendente') {
                $sql .= ' AND pv.id_situacao != ' . Situacao::TB_PREVENDA_PENDENTE;
            }

            if (!empty($params['st_nomecompleto'])) {
                $sql .= ' AND pv.st_nomecompleto like \'%' . $params['st_nomecompleto'] . '%\'';
            }

            if (!empty($params['dt_inicio'])) {
                $sql .= ' AND pv.dt_cadastro >= \'' . $params['dt_inicio'] . ' 00:00:00\'';
            }

            if (!empty($params['dt_fim'])) {
                $sql .= ' AND pv.dt_cadastro <= \'' . $params['dt_fim'] . ' 23:59:59\'';
            }

            if (!empty($params['id_usuarioatendimento'])) {
                $sql .= ' AND pv.id_usuarioatendimento = ' . $params['id_usuarioatendimento'];
            }

            if (!empty($params['dt_ultimainteracao'])) {
                $sql .= " AND CAST(interacao.dt_ultimainteracao AS DATE) = '" . $params['dt_ultimainteracao'] . "'";
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna os tramites de uma Pré-venda
     * @param $id_prevenda
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarInteracoesPrevenda($id_prevenda)
    {
        try {
            $sql = "
                SELECT
                    t.id_tramite ,
                    t.id_tipotramite,
                    t.id_usuario,
                    t.id_entidade, 
                    t.st_tramite,
                    t.dt_cadastro,
                    t.bl_visivel,
                    tpv.id_prevenda,
                    pv.st_nomecompleto,
                    pv.st_cpf,
                    p.st_produto,
                    vw_p.st_nomecompleto AS st_nomeatendente
                FROM tb_tramite t
                    JOIN tb_tramiteprevenda tpv ON t.id_tramite = tpv.id_tramite
                    JOIN tb_prevenda pv ON tpv.id_prevenda = pv.id_prevenda
                    JOIN tb_prevendaproduto pvp ON pvp.id_prevenda = pv.id_prevenda
                    JOIN tb_produto p ON pvp.id_produto = p.id_produto
                    JOIN vw_pessoa vw_p ON vw_p.id_usuario = t.id_usuario AND vw_p.id_entidade = pv.id_entidade
                WHERE tpv.id_prevenda = " . $id_prevenda . "
                ORDER BY t.dt_cadastro";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new \Zend_Exception($e->getMessage());
        }
    }

}

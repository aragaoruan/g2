<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \G2\Entity\TotvsVwIntegraGestorFluxus AS TotvsVwIntegraGestorFluxusEntity;
use \Ead1\Doctrine\EntitySerializer;


class TotvsVwIntegraGestorFluxus extends EntityRepository {

    protected function montaArrayParams($object){

        if (!is_object($object))
            throw new \Exception('Parâmetro informado deve ser do tipo objeto.');

        $entitySerializer = new EntitySerializer($this->_em);
        $params = array();
        foreach ($entitySerializer->toArray($object) as $key => $value){
            if (!empty($value))
                $params[$key] = $value;
        }

        return $params;
    }

    public function getLancamentosVendaIntegracao(TotvsVwIntegraGestorFluxusEntity $vw_integracao){
        return  $this->findBy($this->montaArrayParams($vw_integracao), array('id_venda' => 'DESC', 'datavencimento' => 'ASC'));
    }
}
<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository for VwPesquisarPessoa
 * @author Kayo Silva <kayo.silva@example.com>
 * @since 2013-10-17
 */
class VwPesquisarPessoa extends EntityRepository
{

    /**
     * Retorna Pesquisa de passando ou não parametros e/ou colunudas desejadas
     * @param array $params Parametros de pesquisa
     * @param array $arrColumns Array com colunas desejadas
     * @return mixed array
     * @throws Zend_Exception
     */
    public function pesquisaPessoa(array $params = array(), array $arrColumns = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        $pesquisaBO = new \PesquisarBO();
        try {

            $query = $this->createQueryBuilder('vw')
                ->select($arrColumns)
                ->distinct();

            // GII- 9412 - Tratando CPF para adicionar somente números quando pesquisado CPF
            if (isset($params['st_cpf']) && !empty($params['st_cpf'])) {
                $params['st_cpf'] = $pesquisaBO->somenteNumero($params['st_cpf']);
            }

            //monta o or com os parametros passados
            $this->montarCriterioOr($query, $params);

            if (isset($params['st_nomecompleto']) && !empty($params['st_nomecompleto'])) {
                $query->setParameter('st_nomecompleto', $params['st_nomecompleto'] . '%');
            }

            if (isset($params['st_email']) && !empty($params['st_email'])) {
                $query->setParameter('st_email', $params['st_email'] . '%');
            }

            if (isset($params['st_cpf']) && !empty($params['st_cpf'])) {
                $query->setParameter('st_cpf', $pesquisaBO->somenteNumero($params['st_cpf']) . '%');
            }

            if (isset($params['id_situacao']) && !empty($params['id_situacao'])) {
                $query->andWhere('vw.id_situacao IN(' . $params['id_situacao'] . ')');
            }

            /** Caso seja busca por entidades na mesma holding, monta a query com o array
             * com os múltiplos valores de entidades. Caso contrário, monta a busca normal,
             * passando apenas o ID da entidade única. */

            if (isset($params['entidadesMesmaHolding']) && is_array($params['id_entidade'])) {
                if (isset($params['id_entidade']) && $params['id_entidade']) {
                    $query->andWhere('vw.id_entidade IN(:entidades)')
                        ->setParameter('entidades', $params['id_entidade'], \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
                }
            } else {
                if (isset($params['id_entidade']) && $params['id_entidade']) {
                    $query->andWhere('vw.id_entidade IN(' . $params['id_entidade'] . ')');
                }
            }

            if (isset($params['allEntidades']) && $params['allEntidades']) {
                $limit = false;
            }

            if (!$orderBy) {
                $orderBy = array('st_nomecompleto' => 'asc');
            }

            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'vw.id_usuario');
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }


    /**
     * Monta os criterios or na query
     * @param array $params
     * @param $query
     */
    private function montarCriterioOr(&$query, array $params = [])
    {

        $pesquisaBO = new \PesquisarBO();

        $arrColumns = ["st_nomecompleto", "st_email", "st_cpf"];
        $where = [];
        if ($params) {
            foreach ($params as $key => $value) {

                if (in_array($key, $arrColumns) && $value) {
                    $where[] = "vw.{$key} LIKE :{$key} ";
                    continue;
                }

                if ($key === "id_usuario" && !empty($value)) {
                    $criteria = "vw.id_usuario LIKE '" . $params['id_usuario'] . "%'";

                    // GII- 9412 - Tratando ID para adicionar somente números quando pesquisado id_ou CPF do usuário
                    if (is_numeric($value)) {
                        $criteria = "vw.id_usuario = " . $pesquisaBO->somenteNumero($value);
                    }

                    $where[] = $criteria;
                }
            }
        }

        if ($where) {
            $query->andWhere(implode(" OR ", $where));
        }

    }

}

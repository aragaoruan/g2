<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

class EsquemaConfiguracao extends EntityRepository
{
    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaEsquemaConfiguracao(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {

            $qb = $this->createQueryBuilder('ec')
                ->select('ec.id_esquemaconfiguracao as id, ec.id_esquemaconfiguracao, ec.st_esquemaconfiguracao')
                ->distinct();


//            $where = ' where 1=1 ';
//            $sql = 'SELECT DISTINCT * FROM dbo.tb_esquemaconfiguracao AS ec
//             ';

            if (isset($params['st_esquemaconfiguracao'])) {
                $qb->andWhere("ec.st_esquemaconfiguracao  like '%" . $params['st_esquemaconfiguracao'] . "%'");
//                $where .= "and ec.st_esquemaconfiguracao  like '%" . $params['st_esquemaconfiguracao']."%'";
            }
//            $sql = $sql . $where;
//            $query = $this->_em->getConnection()->executeQuery($sql);
//            return $query->fetchAll();


            if (!$orderBy)
                $orderBy = array('st_esquemaconfiguracao' => 'asc');

            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($qb, $orderBy, $limit, $offset, 'ec.id_esquemaconfiguracao');

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
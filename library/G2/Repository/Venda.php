<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para Cupom
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-06-30
 */
class Venda extends EntityRepository
{

    /**
     * Retorna vendas de produtos do modelo assinatura
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVendaProdutoAssinatura(array $params = array())
    {
        try {
            $sql = "SELECT DISTINCT
                        vd.id_usuario,
                        vwp.st_nomecompleto,
                        vwp.st_cpf,
                        vd.id_venda,
                        vd.id_situacao,
                        st.st_situacao,
                        vd.id_evolucao,
                        evo.st_evolucao,
                        vd.id_entidade,
                        vdp.id_vendaproduto,
                        vdp.id_produto,
                        pd.st_produto
                    FROM tb_venda AS vd
                    JOIN tb_vendaproduto AS vdp ON vd.id_venda = vdp.id_venda
                    JOIN tb_produto AS pd ON pd.id_produto = vdp.id_produto and pd.id_modelovenda = 2
                    JOIN vw_pessoa AS vwp ON vwp.id_usuario = vd.id_usuario
                    JOIN tb_situacao AS st ON st.id_situacao = vd.id_situacao
                    JOIN tb_evolucao AS evo ON evo.id_evolucao = vd.id_evolucao
                    WHERE vd.bl_ativo = 1 AND pd.bl_ativo = 1 AND (vd.id_situacao != 46 AND (vd.id_evolucao = 10 OR vd.id_evolucao = 8)) ";

            if (isset($params['id_entidade'])) {
                $sql .= " AND vd.id_entidade = {$params['id_entidade']} ";
            }
            if (isset($params['id_evolucao'])) {
                $sql .= " AND vd.id_evolucao = {$params['id_evolucao']} ";
            }
            if (isset($params['id_situacao'])) {
                $sql .= " AND vd.id_situacao = {$params['id_situacao']} ";
            }
            if (isset($params['st_nomecompleto']) || isset($params['st_cpf'])) {
                $arrOR = array();
                if (isset($params['st_nomecompleto'])) {
                    $arrOR[] = "vwp.st_nomecompleto LIKE '%{$params['st_nomecompleto']}%' ";
                }
                if (isset($params['st_cpf'])) {
                    $arrOR[] = "vwp.st_cpf LIKE '%{$params['st_cpf']}%' ";
                }
                $sql .= " AND (" . implode(' OR ', $arrOR) . ")";
            }
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao consultar Venda de Produtos Assinatura' . $ex->getMessage());
        }
    }

    public function retornaSpMatriculas($parametros)
    {
        try {
            $sql = 'exec sp_matricula_turma @id_entidadepai =' . $parametros['id_pai'] . ',@dt_datainformada ="' . $parametros['dt_datainformada'] . '"';
            if ($parametros['id_entidade'] != '') {
                $sql = $sql . ', @id_entidade=' . $parametros['id_entidade'];
            }
            if ($parametros['id_turma'] != '') {
                $sql = $sql . ', @id_turma=' . $parametros['id_turma'];
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();
            return $result;

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao executar a consulta. ' . $ex->getMessage());
        }
    }

    public function retornaVendasToHubspot()
    {
        try {
            $sql = 'SELECT vd.id_venda ,
       vd.id_formapagamento ,
       vd.dt_cadastro ,
       vd.id_entidade ,
       vd.id_usuario ,
       vd.dt_atualizado,
       ent.st_nomeentidade,
       prod.id_produto,
	   prod.st_produto,
       pes.st_nomecompleto ,
       pes.st_sexo ,
       pes.dt_nascimento ,
       pes.st_estadocivil ,
       pes.st_email ,
       pes.nu_telefone ,
       pes.nu_ddd ,
       pes.nu_ddi ,
       pes.st_rg ,
       pes.st_endereco ,
       pes.st_cep ,
       pes.st_bairro ,
       pes.st_complemento ,
       pes.nu_numero ,
       pes.st_estadoprovincia ,
       pes.st_cidade ,
       pes.sg_uf ,
       pes.st_nomepais ,
       pes.st_nomemunicipio ,
       pes.st_identificacao FROM tb_venda AS vd
JOIN tb_entidade AS ent ON ent.id_entidade = vd.id_entidade
JOIN dbo.tb_entidadeintegracao AS ei ON ei.id_entidade = ent.id_entidade
JOIN dbo.tb_sistema AS sis ON sis.id_sistema = ei.id_sistema
LEFT JOIN dbo.vw_pessoa AS pes ON pes.id_usuario = vd.id_usuario AND pes.id_entidade = ei.id_entidade
LEFT JOIN dbo.tb_vendaproduto AS vp ON vp.id_venda = vd.id_venda
LEFT JOIN dbo.tb_produto AS prod ON prod.id_produto = vp.id_produto
WHERE vd.dt_atualizado >= DATEADD(HOUR, -1, GETDATE()) AND sis.id_sistema = ' . \G2\Constante\Sistema::HUBSPOT;
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar vendas ' . $e->getMessage());
        }
    }

    /**
     * @param int $id_venda
     * @throws \Exception $e
     */
    public function deletarTransacaoLancamento($id_venda)
    {
        try {
            //TODO: Tive vários problemas executando com as classes ao inves dos nomes das tabelas. Verificar e usar com a classe.
            $query = $this->_em->getConnection()->prepare("DELETE FROM tb_transacaolancamento WHERE id_lancamento IN (SELECT lv.id_lancamento FROM tb_lancamentovenda lv where lv.id_venda = :id_venda)");
            $query->bindValue("id_venda", $id_venda);
            $query->execute();
        } catch (\Exception $e) {
            throw new \Zend_Exception('222Erro ao excluir as transações do lançamento ' . $e->getMessage());
        }
    }

    /**
     * @param $params ('dt_inicio', 'dt_fim', 'id_entidade')
     * @return array
     * @throws \Exception
     */
    public function executaRelatorioPreparaSe($params, $datasEntre = true)
    {
        try {

            if (empty($params['dt_inicio'])) {
                throw new \Exception('Data de início da confirmação da venda não foi informado.');
            }

            if (empty($params['dt_fim']) && $datasEntre) {
                throw new \Exception('Data término da confirmação da venda não foi informado.');
            }

            if (empty($params['id_entidade'])) {
                throw new \Exception('ID da entidade não foi informado.');
            }

            $sql = "SELECT
                        COUNT(rel.confirmacao_comum) AS confirmacao_comum ,
                        COUNT(rel.confirmacao_cb) AS confirmacao_cb,

                        COUNT(rel.cancelamento_comum) AS cancelamento_comum,
                        COUNT(rel.cancelamento_cb) AS cancelamento_cb,

                        SUM(somatoriaconf_comum) AS valor_confirmacao_comum,
                        SUM(somatoriaconf_cb) AS valor_confirmacao_cb

                    FROM
                        (
                                SELECT
                                    COUNT(v.id_venda) AS confirmacao_comum,
                                    NULL AS confirmacao_cb,
                                    NULL AS cancelamento_comum,
                                    NULL AS cancelamento_cb,
                                    v.id_entidade,
                                    v.dt_confirmacao,
                                    NULL AS somatoriaconf_cb,
                                    NULL AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
                                    JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2
                                    JOIN tb_contrato AS c ON c.id_venda = v.id_venda
                                    JOIN tb_contratomatricula AS cm ON cm.id_contrato = c.id_contrato
                                    JOIN tb_matricula AS m ON m.id_matricula = cm.id_matricula
                                WHERE v.id_cupom IS NULL
                                GROUP BY v.id_entidade, v.dt_confirmacao

                            UNION

                                SELECT
                                    NULL AS confirmacao_comum,
                                    COUNT(v.id_venda) AS confirmacao_cb,
                                    NULL AS cancelamento_comum,
                                    NULL AS cancelamento_cb,
                                    v.id_entidade,
                                    v.dt_confirmacao,
                                    NULL AS somatoriaconf_cb,
                                    NULL AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
                                    JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2

                                WHERE  v.id_cupom IS NOT NULL
                                GROUP BY v.id_entidade, v.dt_confirmacao


                            UNION
                                SELECT
                                    NULL AS confirmacao_comum,
                                    NULL AS confirmacao_cb,
                                    COUNT(v.id_venda) AS cancelamento_comum,
                                    NULL AS cancelamento_cb,
                                    v.id_entidade,
                                    t.dt_cadastro AS dt_confirmacao,
                                    NULL AS somatoriaconf_cb,
                                    NULL AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN tb_contrato AS c ON c.id_venda = v.id_venda
                                    JOIN tb_contratomatricula AS cm ON cm.id_contrato = c.id_contrato
                                    JOIN tb_matricula AS m ON m.id_matricula = cm.id_matricula
                                    JOIN tb_tramitematricula AS tm ON tm.id_matricula = m.id_matricula AND m.id_evolucao = 27
                                    JOIN tb_tramite AS t ON t.id_tramite = tm.id_tramite AND bl_visivel = 0

                                WHERE v.id_cupom IS NULL
                                GROUP BY v.id_entidade, t.dt_cadastro

                            UNION
                                SELECT
                                    NULL AS confirmacao_comum,
                                    NULL AS confirmacao_cb,
                                    NULL AS cancelamento_comum,
                                    COUNT(v.id_venda) AS cancelamento_cb,
                                    v.id_entidade,
                                    t.dt_cadastro AS dt_confirmacao,
                                    NULL AS somatoriaconf_cb,
                                    NULL AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
                                    JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2
                                    JOIN tb_contrato AS c ON c.id_venda = v.id_venda
                                    JOIN tb_contratomatricula AS cm ON cm.id_contrato = c.id_contrato
                                    JOIN tb_matricula AS m ON m.id_matricula = cm.id_matricula
                                    JOIN tb_tramitematricula AS tm ON tm.id_matricula = m.id_matricula AND m.id_evolucao = 27
                                    JOIN tb_tramite AS t ON t.id_tramite = tm.id_tramite AND bl_visivel = 0
                                WHERE v.id_cupom IS NOT NULL
                                GROUP BY v.id_entidade, t.dt_cadastro

                            UNION

                                SELECT
                                    NULL AS confirmacao_comum,
                                    NULL AS confirmacao_cb,
                                    NULL AS cancelamento_comum,
                                    NULL AS cancelamento_cb,
                                    v.id_entidade,
                                    v.dt_confirmacao AS dt_confirmacao,
                                    NULL AS somatoriaconf_cb,
                                    SUM(l.nu_valor) AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
                                    JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2
                                    JOIN tb_contrato AS c ON c.id_venda = v.id_venda
                                    JOIN tb_contratomatricula AS cm ON cm.id_contrato = c.id_contrato
                                    JOIN tb_matricula AS m ON m.id_matricula = cm.id_matricula
                                    JOIN tb_lancamentovenda AS lv ON lv.id_venda = v.id_venda
                                    JOIN tb_lancamento AS l ON l.id_lancamento = lv.id_lancamento

                                WHERE v.id_cupom IS NULL
                                GROUP BY v.id_entidade, v.dt_confirmacao

                            UNION

                                SELECT
                                    NULL AS confirmacao_comum,
                                    NULL AS confirmacao_cb,
                                    NULL AS cancelamento_comum,
                                    NULL AS cancelamento_cb,
                                    v.id_entidade,
                                    v.dt_confirmacao AS dt_confirmacao,
                                    SUM(l.nu_valor) AS somatoriaconf_cb,
                                    NULL AS somatoriaconf_comum,
                                    NULL AS resultadoconf_comum,
                                    NULL AS resultadoconf_cb
                                FROM tb_venda v
                                    JOIN dbo.tb_vendaproduto AS vp ON v.id_venda = vp.id_venda
                                    JOIN dbo.tb_produto AS pd ON pd.id_produto = vp.id_produto AND pd.id_modelovenda = 2
                                    JOIN tb_contrato AS c ON c.id_venda = v.id_venda
                                    JOIN tb_contratomatricula AS cm ON cm.id_contrato = c.id_contrato
                                    JOIN tb_matricula AS m ON m.id_matricula = cm.id_matricula
                                    JOIN tb_lancamentovenda AS lv ON lv.id_venda = v.id_venda
                                    JOIN tb_lancamento AS l ON l.id_lancamento = lv.id_lancamento

                                WHERE v.id_cupom IS NOT NULL
                                GROUP BY v.id_entidade, v.dt_confirmacao

                        ) AS rel

                    WHERE rel.id_entidade = :id_entidade " . ($datasEntre ? " AND rel.dt_confirmacao between :dt_inicio AND :dt_fim" : " AND rel.dt_confirmacao < :dt_inicio"); // '2015-05-01';


            $statement = $this->_em->getConnection()->prepare($sql);

            $statement->bindValue('dt_inicio', $params['dt_inicio']);
            if ($datasEntre) {
                $statement->bindValue('dt_fim', $params['dt_fim']);
            }
            $statement->bindValue('id_entidade', $params['id_entidade']);

            $statement->execute();
            $results = $statement->fetchAll();

            return $results;
        } catch (\Exception $message) {
            throw $message;
        }

    }

    /**
     * @param $idVenda
     * @param $idAcordo
     * @return mixed
     * @throws \Exception
     */
    public function acordo($idVenda, $idAcordo)
    {
        try {
            $sqlQuery = sprintf('SELECT COUNT(lan.id_lancamento) AS nu_parcela,
                            SUM(lan.nu_valor + lan.nu_juros - lan.nu_desconto) AS vl_pagamento
                       FROM tb_lancamentovenda AS lan_ven
                 INNER JOIN tb_lancamento AS lan ON lan_ven.id_lancamento = lan.id_lancamento
                      WHERE lan_ven.id_venda = %d
                        AND lan.id_acordo = %d
                        AND lan.id_acordo IS NOT NULL
                        AND lan.bl_ativo = 1
                   GROUP BY lan.id_acordo'
                , $idVenda
                , $idAcordo
            );

            return $this->_em->getConnection()
                ->executeQuery($sqlQuery)
                ->fetch();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function lancamentosAcordo($idVenda, $idAcordo)
    {
        try {
            $sqlQuery = 'SELECT lan.id_lancamento
                       FROM tb_lancamentovenda AS lan_ven
                 INNER JOIN tb_lancamento AS lan ON lan_ven.id_lancamento = lan.id_lancamento
                      WHERE lan_ven.id_venda = :idVenda
                        AND lan.id_acordo = :idAcordo
                        AND lan.dt_quitado IS NULL
                        AND lan.bl_quitado = 0
                        AND lan.bl_ativo = 1';

            $stmt = $this->_em->getConnection()->prepare($sqlQuery);
            $stmt->bindValue('idVenda', $idVenda);
            $stmt->bindValue('idAcordo', $idAcordo);
            $stmt->execute();

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Monta uma query sql para consultar os dados da venda recebimento, esse método foi criado para adicionar um critério
     * discutido entre a Patricia e o DEV, esse critério consiste em consultar as vendas porém as que estiverem com
     * o id_formapagamentoaplicacao igual a 2, estas devem ser retornadas apenas se existir um lançamento de entrada quitado.
     * Isso visa solucionar o problema reportado na issue GII-7806, onde algumas vendas vindas do site estão sendo confirmadas
     * sem lançamentos
     * @param array $where
     * @param array $order
     * @param integer $limit
     * @return array
     * @throws \Exception
     */
    public function retornarDadosVwVendasRecebimentos(array $where = array(), array $order = array(), $limit = null)
    {
        try {
            $prefixo = "vw";

            /*
             * Explicando o case da query
             *  (CASE  {$prefixo}.id_formapagamentoaplicacao --verifica se a forma de pagamento aplicação é Venda Site
             *      WHEN 2 THEN
             *          -- retorna se o lançamento de entrada (left's join's) esta quitado
             *          ISNULL(lc.bl_quitado, 0)
             *      ELSE --para todos as outras formas de pagamento aplicação retorna 1 (true)
             *          1
             *  END)
             */

            $sql = "SELECT";
            if ($limit) {
                $sql .= " TOP {$limit} ";
            }
            $sql .= "
                        {$prefixo}.*
                    FROM
                        vw_vendasrecebimento AS  {$prefixo}
                    LEFT JOIN tb_lancamentovenda AS lcv ON lcv.id_venda =  {$prefixo}.id_venda AND lcv.bl_entrada = 1
                    LEFT JOIN tb_lancamento AS lc ON lc.id_lancamento = lcv.id_lancamento
                    WHERE
                    (CASE  {$prefixo}.id_formapagamentoaplicacao
                        WHEN 2 THEN
                                ISNULL(lc.bl_quitado, 0)	
                        ELSE
                            1
                    END) = 1";

            //verifica se existe alguma coisa no where
            if (is_array($where) && $where) {
                $arrCriterio = [];
                foreach ($where as $coluna => $criterio) {
                    //checa se tem algo no parametro
                    if (is_null($criterio) || $criterio == "") {
                        continue;
                    }

                    //verifica se o criterio é um array e seta o in
                    if (is_array($criterio)) {
                        $arrCriterio[] = $prefixo . "." . $coluna . " IN(" . implode($criterio, ',') . ")";
                        continue;
                    }

                    //para todos os outros
                    $arrCriterio[] = $prefixo . "." . $coluna . " = '" . $criterio . "'";
                }
                //concatena os critérios
                $sql .= " AND " . implode($arrCriterio, ' AND ');
            }

            //verifica se o order foi passado
            if (is_array($order) && $order) {
                $arrOrder = [];
                foreach ($order as $coluna => $criterio) {
                    if (!$criterio) {
                        $criterio = "ASC";
                    }
                    $arrOrder[] = $prefixo . "." . $coluna . " " . $criterio;

                }
                $sql .= " ORDER BY " . implode($arrOrder, ', ');
            }

            $stmt = $this->_em->getConnection()->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $id_matricula
     * @return bool|array
     * @throws \Zend_Exception
     */
    public function retornarUltimaVendaConfirmada($id_matricula)
    {
        $query = sprintf('
            SELECT TOP 1 ven.*
            FROM tb_matricula AS mt
            JOIN tb_vendaproduto AS ven_pro ON mt.id_matricula = ven_pro.id_matricula
            JOIN tb_venda AS ven ON ven_pro.id_venda = ven.id_venda
            WHERE mt.id_matricula = %d 
                AND ven.id_evolucao = %d
                AND ven.id_situacao = %d
                AND ven.bl_ativo = 1
            ORDER BY ven.dt_cadastro DESC
            ',
            $id_matricula,
            \G2\Constante\Evolucao::TB_VENDA_CONFIRMADA,
            \G2\Constante\Situacao::TB_VENDA_ATIVO
        );

        try {
            $results = $this->_em
                ->getConnection()
                ->executeQuery($query)
                ->fetch();

            return $results;
        } catch (\Exception $e) {
            return false;
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/11/13
 * Time: 11:41
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class LancamentoVenda extends EntityRepository
{
    public function retornaUltimoNuOrdemLancamentoVenda($id_venda)
    {
        $query = $this->createQueryBuilder("lv")
            ->select(" MAX(lv.nu_ordem) + 1 AS nu_ordem ")
            ->where("lv.id_venda = :idVenda")
            ->setParameter('idVenda', $id_venda);

        return $query->getQuery()->getSingleResult();
    }

    /**
     * @param int $id_venda
     * @throws \Zend_Exception $e
     */
    public function deletarLancamentoVenda($id_venda)
    {
        try {
            $query = $this->_em->createQuery("DELETE FROM '\G2\Entity\LancamentoVenda' tv WHERE tv.id_venda = :id");
            $query->setParameter('id', $id_venda);
            $query->execute();
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao excluir os lançamentos da venda ' . $e->getMessage());
        }
    }

    /**
     * @param array $where
     * @throws \Zend_Exception $e
     */
    public function retornaLancamentosPorIdVenda($params)
    {
        try {
            $qb = $this->createQueryBuilder('lc')
                ->select(array(
                    'SUM(l.nu_quitado)'
                ))
                ->join('lc.id_lancamento', 'l')
                ->where('lc.id_venda = :id_venda')
                ->setParameter('id_venda', $params['id_venda']);

            if (array_key_exists('bl_ativo', $params) && !empty($params['bl_ativo'])) {
                $qb->andWhere('l.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo']);
            }

            if (array_key_exists('bl_quitado', $params) && !empty($params['bl_quitado'])) {
                $qb->andWhere('l.bl_quitado = :bl_quitado')
                    ->setParameter('bl_quitado', $params['bl_quitado']);
            }

            return $qb->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao somar os lançamentos já quitados da venda.' . $e->getMessage());
        }
    }

}
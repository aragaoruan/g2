<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para Produto
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-10-03
 */
class Produto extends EntityRepository
{

    /**
     * Pesquisa produto por parametros
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function pesquisarProduto(array $params = array())
    {
        try {
            $query = $this->createQueryBuilder('vw')
                ->where('1=1');
            //seta os parametros
            if ($params) {
                if (isset($params['st_produto'])) {
                    $query->andWhere("vw.st_produto LIKE :st_produto")
                        ->setParameter('st_produto', '%'.$params['st_produto'].'%');
                }

                if (isset($params['id_situacao'])) {
                    $query->andWhere("vw.id_situacao = :id_situacao")
                        ->setParameter('id_situacao', $params['id_situacao']);
                }

                if (isset($params['id_entidade']) && $params['id_entidade'] != null) {
                    $query->andWhere('vw.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $params['id_entidade']);
                }
                if (isset($params['id_tipoproduto'])) {
                    $query->andWhere('vw.id_tipoproduto = :id_tipoproduto')
                        ->setParameter('id_tipoproduto', $params['id_tipoproduto']);
                }
                if (isset($params['id_produto'])) {
                    /*
                     * verifica se o parametro id_produto é um array,
                     * se for transforma em uma string para fazer a consulta utilizando o IN do SQL
                    */
                    if (is_array($params['id_produto'])) {
                        $arr = array();
                        foreach($params['id_produto'] as $value){
                            array_push($arr, intval($value));
                        }

                        $query->andWhere("vw.id_produto IN (:id_produto)")
                            ->setParameter('id_produto',$arr);

                    } else {
                        $query->andWhere('vw.id_produto = :id_produto')
                            ->setParameter('id_produto', $params['id_produto']);
                    }
                }
            }

            //Ordena a consulta
            $query->orderBY('vw.st_produto', 'ASC');

            //Executa a query
            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws \Zend_Exception
     */
    public function retornaProdutosCategorias(array $params = array())
    {
        try {

            $where = "WHERE 1=1 ";

            if (isset($params['id_entidade'])) {
                $where .= " AND pd.id_entidade = '{$params['id_entidade']}'";
            }

            if (isset($params['categoria'])) {
                $where .= " AND catp.id_categoria IN(".implode(',', $params['categoria']).")";
            }

            if (isset($params['bl_destaque'])) {
                $where .= " AND pd.bl_destaque = {$params['bl_destaque']}";
            }

            $sql = " select pd.*, catp.id_categoria, catp.st_nomeexibicao, catp.st_categoria from vw_categoriaproduto catp
                 join vw_produto pd on pd.id_produto = catp.id_produto
                {$where}
                  ORDER BY pd.id_produto asc";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
            exit;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Consulta dados em VwProdutoCompartilhado
     * @param array $where
     * @return array
     * @throws \Zend_Exception
     */
    //@todo = Trocar isso por um view de verdade
    public function retornarVwProdutoCompartilhado(array $where = array())
    {
        try {
            $arrWhere = array();
            $arrWherePe = array();
            $arrWhereEntidade = array();

            $id_produto = null;

            if (!empty($where['id_selected'])) {
                $id_produto = $where['id_selected'];
                unset($where['id_selected']);
            }

            $id_tipoproduto = null;

            //se exitir "id_tipoproduto" separa do where
            if (!empty($where['id_tipoproduto'])) {
                $id_tipoproduto = $where['id_tipoproduto'];
                unset($where['id_tipoproduto']);
            }


            //verifica se a variavel $where tem valor
            if ($where) {
                //percorre os valores e monta os where's
                $i = 0;
                foreach ($where as $key => $value) {
                    if ($value) {
                        if ($key == 'st_produto') {
                            $arrWhere[$i] = "vw.{$key} LIKE '%{$value}%'";
                            $arrWherePe[$i] = "vw.{$key} LIKE '%{$value}%'";
                            $arrWhereEntidade[$i] = "vw.{$key} LIKE '%{$value}%'";
                        } else {
                            $arrWhere[$i] = "vw.{$key} = '{$value}'";
                            $arrWherePe[$i] = "vw.{$key} = '{$value}'";
                            $arrWhereEntidade[$i] = "vw.{$key} = '{$value}'";
                            if ($key == 'id_entidade') {
                                $arrWherePe[$i] = "pe.{$key} = '{$value}'";
                                $arrWhereEntidade[$i] = "er.id_entidade = '{$value}'";
                            }
                            if ($key == 'id_situacao') {
                                $arrWhereEntidade[$i] = "vw.id_situacao = '{$value}'";
                            }
                        }
                        $i++;
                    }
                }
            }

            $strWhere = $arrWhere ? 'WHERE '.implode(" AND ", $arrWhere) : NULL;
            $strWherePe = $arrWherePe ? 'WHERE '.implode(" AND ", $arrWherePe) : NULL;
            $strWhereEntidade = $arrWhereEntidade ? 'WHERE '.implode(" AND ", $arrWhereEntidade) : NULL;

            if ($id_produto) {
                $strWhere .= ($strWhere ? ' OR' : '') . ' vw.id_produto = ' . $id_produto;
            }

            $sql = "SELECT * FROM (SELECT DISTINCT	vw.* FROM vw_entidaderecursivaid er
                        JOIN vw_produto vw	ON vw.id_entidade = er.id_entidadepai AND vw.bl_todasentidades = 1
                        {$strWhereEntidade}
                    UNION
                    SELECT vw.* FROM vw_produto vw
                        {$strWhere}
                    UNION
                    SELECT vw.* FROM vw_produto AS vw
                        JOIN tb_produtoentidade AS pe ON pe.id_produto = vw.id_produto and vw.bl_todasentidades = 0
                       {$strWherePe} ) as q ";

            //se foi passa o id_tipoproduto coloca como WHERE principal
            if (!empty($id_tipoproduto)) {
                $sql .= " WHERE q.id_tipoproduto = " . $id_tipoproduto;
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function listarProdutoWs(array $parametros, array $entidade_id = [])
    {
        try {


            if (!empty($parametros['limit'])) {
                $limit = "TOP ".$parametros['limit'];
            } else {
                $limit = "TOP 100";
            }


            $sql = "SELECT DISTINCT {$limit} vw.id_produto,
                vw.st_produto,
                vw.bl_destaque,
                vw.id_entidade,
                vw.st_nomeentidade,
                vw.st_descricao,
                vw.dt_cadastro,
                vw.st_evolucao,
                vw.id_evolucao,
                vw.nu_valor,
                vw.bl_mostrarpreco,
                vw.id_projetopedagogico,
                vw.nu_cargahoraria,
                vw.dt_inicio,
                vw.st_turno,
                vw.st_disciplina,
                vw.st_diassemana
                FROM vw_consultaproduto AS vw
                WHERE 1 = 1 ";


            if (!empty($parametros['id_entidade'])) {
                $id = $parametros['id_entidade'];
                $sql .= "AND vw.id_entidade = {$id}";
            }

            //TODO: realizar implementação caso venha um array de categorias
            if (!empty($parametros['category'])) {
                $cat = $parametros['category'];
                $sql .= "AND vw.id_categoria = {$cat}";
            }

            if (!empty($parametros['id_concurso'])) {
                $conc = $parametros['id_concurso'];

                $sql = "SELECT
                        DISTINCT {$limit}
                        vw.id_produto,
                        vw.st_produto,
                        vw.bl_destaque,
                        vw.id_entidade,
                        vw.st_nomeentidade,
                        vw.st_descricao,
                        vw.st_concurso,
                        vw.dt_cadastro,
                        vw.id_projetopedagogico,
                        vw.nu_cargahoraria,
                        vw.dt_inicio,
                        vw.st_turno,
                        vw.st_disciplina,
                        vw.st_diassemana
                        FROM vw_consultaproduto AS vw
                        WHERE vw.id_concurso = {$conc}";

            }


            if (!empty($parametros['bl_destaque']) && $parametros['bl_destaque'] == '1') {
                $bl_destaque = 1;
                $sql .= " AND vw.bl_destaque = {$bl_destaque}";
            }

            $sql .= " ORDER BY vw.dt_cadastro DESC";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function retornaProdutoCategoriasEntidadeParams(array $params)
    {


        $sql = "SELECT DISTINCT
                        cp.id_produto,
                        cp.st_produto
                FROM    vw_categoriaproduto AS cp
                        JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto
                                                     AND pd.bl_ativo = 1
                        JOIN dbo.tb_produtoentidade AS pe ON pe.id_produto = pd.id_produto
                WHERE 1=1";
                    if ($params['id_entidadecadastro']) {
                        $id_entidadecadastro = $params['id_entidadecadastro'];
                        $sql .= " AND cp.id_entidadecadastro = $id_entidadecadastro";
                    }
                    if ($params['id_categoria']) {
                        $id_categoria = $params['id_categoria'];
                        $sql .= " AND cp.id_entidadecadastro = $id_categoria";
                    }

        $sql .= "
                UNION
                    SELECT DISTINCT
                            cp.id_produto,
                            cp.st_produto
                    FROM    rel.vw_ecommerce AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto AND pd.bl_ativo = 1 AND pd.bl_todasentidades = 1
                            JOIN dbo.vw_entidaderecursivaid AS er ON er.nu_entidadepai = pd.id_entidade
                    WHERE 1=1";
                        if ($params['id_entidadecadastro']) {
                            $id_entidadecadastro = $params['id_entidadecadastro'];
                            $sql .= " AND cp.id_entidade = $id_entidadecadastro";
                        }
                        if ($params['id_entidadecadastro']) {
                            $id_entidadepai = $params['id_entidadecadastro'];
                            $sql .= " AND cp.id_entidadepai = $id_entidadepai";
                        }
                        if ($params['id_categoria']) {
                            $id_categoria = $params['id_categoria'];
                            $sql .= " AND cp.id_categoria = $id_categoria";
                        }
               $sql .="
                    UNION
                    SELECT DISTINCT
                            cp.id_produto,
                            cp.st_produto
                    FROM    vw_categoriaproduto AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto AND pd.bl_ativo = 1 AND pd.bl_todasentidades = 1
                    WHERE 1=1";
                        if ($params['id_entidadecadastro']) {
                            $id_entidadecadastro = $params['id_entidadecadastro'];
                            $sql .= " AND cp.id_entidadecadastro = $id_entidadecadastro";
                        }
                        if ($params['id_entidadecadastro']) {
                            $id_entidadepai = $params['id_entidadecadastro'];
                            $sql .= " AND cp.id_entidadecadastro = $id_entidadepai";
                        }
                        if ($params['id_categoria']) {
                            $id_categoria = $params['id_categoria'];
                            $sql .= " AND cp.id_categoria = $id_categoria";
                        }

                    $sql .= " ORDER BY cp.st_produto";

        $query = $this->_em->getConnection()->executeQuery($sql);
        return $query->fetchAll();
    }

    public  function  retornaProdutoLike($params){
        try {
            $query = $this->createQueryBuilder('vw')
                ->select(array(
                    'vw.id_produto as id',
                    'vw.id_produto',
                    'vw.st_produto',
                    'vw.st_tipoproduto'
                ))
                ->where('vw.bl_ativo = :bl_ativo')
                ->andWhere('vw.id_situacao = :id_situacao')
                ->setParameter('id_situacao', \G2\Constante\Situacao::TB_PRODUTO_ATIVO)
                ->setParameter('bl_ativo', true);
            //seta os parametros
            if ($params) {
                if (isset($params['st_produto'])) {
                    $query->andWhere("vw.st_produto LIKE :st_produto")
                        ->setParameter('st_produto', '%'.$params['st_produto'].'%');
                }
                if (isset($params['id_entidade'])) {
                    $query->andWhere('vw.id_entidade = :id_entidade')
                        ->setParameter('id_entidade', $params['id_entidade']);
                }
            }
            //Ordena a consulta
            $query->orderBY('vw.st_produto', 'ASC');


            //Executa a query
            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

}

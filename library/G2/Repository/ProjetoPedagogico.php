<?php
namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para ProjetoPedagogico
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-05
 */
class ProjetoPedagogico extends EntityRepository
{

    /**
     * Retorna todos os projetos pedagogicos que tiver turma vinculada de acordo com o array de id das entidades
     * @param array $entidades array com id das entidades array(1,2,...,n)
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaTurmaProjetoPedagogicoEntidadeRecursiva(array $entidades = [])
    {
        try {


            $sql = "SELECT DISTINCT
                        tp.id_projetopedagogico,
                        tp.st_projetopedagogico
                    FROM
                        vw_turmaprojetopedagogico as tp
                    join tb_turma as t on t.id_turma = tp.id_turma and t.bl_ativo = 1
                    WHERE
                        tp.bl_ativo = 1
                    and t.id_situacao = " . \G2\Constante\Situacao::TB_TURMA_ATIVA . "
                    and t.id_evolucao = " . \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO . "
                    AND (t.dt_fim >= (GETDATE() - 10) OR t.dt_fim IS NULL)";


            if ($entidades) {
                $sql .= " AND tp.id_entidadecadastro in(" . implode(',', $entidades) . ") ";
            }

            //Ordena a consulta
            $sql .= ' ORDER BY tp.st_projetopedagogico ASC';
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar projetos de forma recursiva. " . $e->getMessage());
        }
    }

    /**
     * Retorna lista de entidades que contenham turma vinculadas de acordo com o projeto pedagogico
     * @param array $entidades
     * @param integer $idProjeto
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarEntidadeComTurmaByProjeto(array $entidades, $idProjeto)
    {
        try {
            $sql = "SELECT DISTINCT
                        vw.id_entidade,
                        vw.st_nomeentidade
                    FROM
                        vw_projetoentidade vw
                    JOIN tb_turmaentidade te ON te.id_entidade = vw.id_entidade
                    AND te.bl_ativo = 1
                    WHERE
                        vw.id_projetopedagogico = {$idProjeto}
                    AND vw.id_entidade IN (" . implode(',', $entidades) . ")
                    ORDER BY vw.st_nomeentidade ASC;";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar Entidades com turma pelo projeto. " . $e->getMessage());
        }
    }

    /**
     * Retorna as Turmas vinculadas ao projeto e entidade informada
     * @param integer $idProjeto
     * @param integer $idEntidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarTurmaByProjetoUnidade($idProjeto, $idEntidade, $idTurno)
    {
        try {

            $sql = "SELECT
                        DISTINCT
                        t.id_turma,
                        t.st_turma
                    FROM
                        vw_turmaprojetopedagogico AS vw
                    JOIN tb_turma AS t ON t.id_turma = vw.id_turma
                    AND t.bl_ativo = 1
                    WHERE
                        vw.bl_ativo = 1
                    AND t.id_situacao = " . \G2\Constante\Situacao::TB_TURMA_ATIVA . "
                    AND t.id_evolucao = " . \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO . "
                    AND (
                        t.dt_fim >= (GETDATE() - 10)
                        OR t.dt_fim IS NULL
                    )
                    and vw.id_entidadecadastro = {$idEntidade}
                    and vw.id_projetopedagogico = {$idProjeto}
                    and vw.id_turno = {$idTurno}
                    ORDER BY t.st_turma ASC";

//            $sql = "SELECT DISTINCT
//                        t.id_turma,
//                        t.st_turma
//                    FROM
//                        vw_projetoentidade vw
//                    JOIN tb_turmaentidade te ON te.id_entidade = vw.id_entidade AND te.bl_ativo = 1
//                    JOIN tb_turma t ON t.id_turma = te.id_turma AND t.bl_ativo = 1
//                    WHERE
//                        vw.bl_ativo = 1
//                    AND vw.id_projetopedagogico = {$idProjeto}
//                    AND t.id_situacao = " . \G2\Constante\Situacao::TB_TURMA_ATIVA . "
//                    AND t.id_evolucao = " . \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO . "
//                    AND (t.dt_fim >= (GETDATE() - 10) OR t.dt_fim IS NULL)
//                    AND vw.id_entidade = {$idEntidade}
//                    ORDER BY t.st_turma ASC";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar Turmas. " . $e->getMessage());
        }
    }

    /**
     * Retorna as Disciplinas vinculadas ao projeto e unidade
     * @param $idProjeto
     * @param $idEntidade
     * @throws \Zend_Exception
     * @return array
     */
    public function retornarDisciplinasProjetoUnidade($idProjeto, $idEntidade, $idTurma)
    {
        try {

            $sql = "SELECT DISTINCT
               d.id_disciplina,
                d.st_disciplina,
                mdc.nu_cargahoraria,
                ISNULL(
                (SELECT
					eci.st_valor
				FROM tb_entidade e
						INNER JOIN tb_esquemaconfiguracao ec ON e.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
						INNER JOIN tb_esquemaconfiguracaoitem eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
						INNER JOIN tb_itemconfiguracao ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao
				 AND e.id_entidade = pe.id_entidade AND eci.id_itemconfiguracao = " . \G2\Constante\ItemConfiguracao::HORAS_ENCONTRO . "
				)
                , 0) AS nu_cargahorariaencontro,
                ISNULL(
                mdc.nu_cargahoraria / (SELECT
                                            eci.st_valor
                                        FROM tb_entidade e
                                                INNER JOIN tb_esquemaconfiguracao ec ON e.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
                                                INNER JOIN tb_esquemaconfiguracaoitem eci ON eci.id_esquemaconfiguracao = ec.id_esquemaconfiguracao
                                                INNER JOIN tb_itemconfiguracao ic ON ic.id_itemconfiguracao = eci.id_itemconfiguracao
                                         AND e.id_entidade = pe.id_entidade AND eci.id_itemconfiguracao = " . \G2\Constante\ItemConfiguracao::HORAS_ENCONTRO . ")
                , 0) AS nu_totalencontros,
                  (SELECT ISNULL(max(igh.nu_encontro),0)
   FROM tb_itemgradehoraria AS igh
   WHERE igh.id_disciplina = d.id_disciplina AND igh.id_unidade = pe.id_entidade AND
         igh.id_projetopedagogico = pp.id_projetopedagogico AND igh.id_turma = t.id_turma AND igh.bl_ativo =
                                                                                              1)                AS nu_encontros
            FROM
                tb_projetoentidade AS pe
            JOIN tb_projetopedagogico AS pp ON pp.id_projetopedagogico = pe.id_projetopedagogico AND pp.bl_ativo = 1
            JOIN tb_modulo AS md ON pp.id_projetopedagogico = md.id_projetopedagogico AND md.bl_ativo = 1
            JOIN tb_modulodisciplina AS mdc ON mdc.id_modulo = md.id_modulo AND mdc.bl_ativo = 1
            JOIN tb_disciplina AS d ON mdc.id_disciplina = d.id_disciplina AND d.bl_ativa = 1
            JOIN tb_turmaentidade AS te ON te.id_entidade = pe.id_entidade AND te.bl_ativo = 1
            JOIN tb_turma t ON t.id_turma = te.id_turma AND bl_ativa = 1
            WHERE
                pe.bl_ativo = 1
            AND d.id_situacao = " . \G2\Constante\Situacao::TB_DISCIPLINA_ATIVA . "
            AND pe.id_situacao = " . \G2\Constante\Situacao::TB_PROJETOENTIDADE_ATIVO . "
            AND pp.id_situacao = " . \G2\Constante\Situacao::TB_PROJETOPEDAGOGICO_ATIVO . "
            AND md.id_situacao = " . \G2\Constante\Situacao::TB_MODULO_ATIVO . "
            and t.id_situacao = " . \G2\Constante\Situacao::TB_TURMA_ATIVA . "
            and t.id_evolucao = " . \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO . "
            and pe.id_entidade = {$idEntidade}
            and pe.id_projetopedagogico = {$idProjeto}
            and t.id_turma = {$idTurma}
            ORDER BY d.st_disciplina ASC";
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar Disciplinas. " . $e->getMessage());
        }
    }

    public function retornaProjetosEmComumEntreSalas(array $params, $id_saladeaula1, $id_saladeaula2)
    {

        $where = '';

        if (array_key_exists('id_entidade', $params)) {
            $where .= ' AND id_entidade = ' . $params['id_entidade'];
        }

        $sql = 'SELECT
	              pp.id_projetopedagogico, pp.st_projetopedagogico,
	              COUNT(DISTINCT al.id_matriculadisciplina ) as nu_alocados
                FROM tb_projetopedagogico pp
                JOIN tb_areaprojetopedagogico app on pp.id_projetopedagogico = app.id_projetopedagogico
                JOIN tb_areaprojetosala aps on aps.id_projetopedagogico = pp.id_projetopedagogico
                JOIN tb_matricula as mt on mt.id_projetopedagogico  = pp.id_projetopedagogico
                JOIN tb_matriculadisciplina as md on md.id_matricula = mt.id_matricula
                JOIN tb_alocacao as al on md.id_matriculadisciplina = al.id_matriculadisciplina and al.bl_ativo = 1
                JOIN tb_saladeaula sa2 on sa2.id_saladeaula = aps.id_saladeaula
                WHERE al.id_saladeaula = ' . $id_saladeaula1 . '
                AND sa2.id_saladeaula = ' . $id_saladeaula2 . ' ' . $where . '
                GROUP BY pp.id_projetopedagogico, pp.st_projetopedagogico';

        $query = $this->_em->getConnection()->executeQuery($sql);
        return $query->fetchAll();
    }

    /**
     * @param $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaProjetosEntidades($params)
    {
        try {
            $query = $this->createQueryBuilder('pp')
                ->select(
                    'pp.id_projetopedagogico',
                    'pp.st_projetopedagogico',
                    'pp.st_nomeentidade'

                )
                ->where('pp.bl_ativo = :bl_ativo')
                ->andWhere('pp.id_entidade IN (' . $params['id_entidade'] . ')')
                ->setParameter('bl_ativo', 1)
                ->orderBy('pp.st_projetopedagogico', 'ASC');

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retornar turmas sem repetição para a tela de produto
     * @param integer $idEntidade
     * @param integer $idProjetoPedagogico
     * @return array
     * @throws \Zend_Exception
     */

    public function retornarProjetosPedagogicosEntidadeProduto($idEntidade, $idProjetoPedagogico)
    {
        $query = $this->createQueryBuilder('pp')
            ->select(
                't.id_turma',
                't.st_turma'
            )
            ->join("G2\\Entity\\TurmaProjeto", 'tp', 'WITH', 'tp.id_projetopedagogico = pp.id_projetopedagogico')
            ->join("G2\\Entity\\Turma", 't', 'WITH', 'tp.id_turma = t.id_turma')
            ->join("G2\\Entity\\TurmaEntidade", 'te', 'WITH', 'te.id_turma = t.id_turma')
            ->join("G2\\Entity\\TurmaTurno", 'tt', 'WITH', 't.id_turma = tt.id_turma')
            ->join("G2\\Entity\\Turno", 'tur', 'WITH', 'tur.id_turno = tt.id_turno')
            ->where('pp.id_projetopedagogico = :id_projetopedagogico')
            ->andWhere('t.id_situacao = :id_situacao')
            ->andWhere('te.id_entidade = :id_entidade')
            ->orderBy('t.st_turma', 'ASC')
            ->setParameters([
                'id_projetopedagogico' => $idProjetoPedagogico,
                'id_entidade' => $idEntidade,
                'id_situacao' => \G2\Constante\Situacao::TB_TURMA_ATIVA
            ]);

        return $query->getQuery()->getResult();
    }

    public function returnTurmaSalaByProjeto($params)
    {
        try {
            $sql = "SELECT DISTINCT  ts.id_salaturma  ,vw.st_turma, vw.id_turma 
        FROM vw_turmaprojetopedagogico as vw
        LEFT JOIN tb_areaprojetosala as aps  ON vw.id_projetopedagogico = aps.id_projetopedagogico
        LEFT JOIN tb_turmasala as ts ON aps.id_saladeaula = ts.id_saladeaula AND vw.id_turma = ts.id_turma
        WHERE vw.bl_ativo = 1";

            if (array_key_exists('id_projetopedagogico', $params) && $params['id_projetopedagogico']) {
                $sql .= " AND vw.id_projetopedagogico = " . $params['id_projetopedagogico'];
            }else{
                throw new \Zend_Exception('É necessário um projeto pedagógico.');
            }

            if (array_key_exists('id_saladeaula', $params) && $params['id_saladeaula']) {
                $sql .= " AND aps.id_saladeaula = " . $params['id_saladeaula'];
            }

            if (array_key_exists('id_entidadecadastro', $params) && $params['id_entidadecadastro']) {
                $sql .= " AND vw.id_entidadecadastro in (" . implode(",", $params['id_entidadecadastro']) . ")";
            }

            $sql.= " ORDER BY id_turma";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }


    }

    /**Função que retorna o st_projetopedagogico passando o id_matricula
     * @param $idMatricula
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function retornarStProjeto($idMatricula)
    {
        if ($idMatricula) {
            $query = $this->createQueryBuilder('pp')
                ->select('pp.st_projetopedagogico')
                ->join(
                    'G2\Entity\Matricula',
                    'ma',
                    'WITH',
                    'pp.id_projetopedagogico = ma.id_projetopedagogico')
                ->where('ma.id_matricula = :id_matricula')
                ->setParameter('id_matricula', $idMatricula);

            return $query->getQuery()->getSingleResult();
        }
    }

}

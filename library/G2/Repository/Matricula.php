<?php

namespace G2\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\AST\OrderByClause;

/**
 * Class Matricula
 *
 * @package G2\Repository
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Matricula extends EntityRepository
{
    /**
     * Retorna dados da matricula join produto projeto pedagogico
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaMatriculaValida(array $params)
    {
        try {
            $sql = "SELECT
                    vwm.id_situacao,
                    vwm.st_situacao,
                    vwm.id_evolucao,
                    vwm.st_evolucao
                FROM tb_produtoprojetopedagogico AS ppp
                JOIN vw_matricula AS vwm ON vwm.id_projetopedagogico = ppp.id_projetopedagogico
                WHERE vwm.bl_ativo = 1
                   AND (vwm.dt_inicio <= GETDATE() AND vwm.dt_termino >= GETDATE())
	               AND ((vwm.dt_inicioturma <= GETDATE() OR vwm.dt_inicioturma IS NULL) AND (vwm.dt_terminoturma >= GETDATE() OR vwm.dt_terminoturma IS NULL)) ";

            if ($params) {
                if ($params['id_produto']) {
                    $sql .= " and ppp.id_produto = {$params['id_produto']}";
                }
                if ($params['id_entidade']) {
                    $sql .= " and ppp.id_entidade = {$params['id_entidade']}";
                }
                if ($params['id_usuario']) {
                    $sql .= " and vwm.id_usuario = {$params['id_usuario']}";
                }
            }
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetch();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados da matricula.' . $ex->getMessage());
        }
    }

    /**
     * @param integer $idUsuario
     * @param integer $idMatricula
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaMatriculaAssinatura($idMatricula)
    {
        try {
            $sql = "SELECT
                        mt.id_matricula,
                        mt.id_usuario,
                        mt.st_nomecompleto,
                        mt.st_projetopedagogico,
                        pd.st_produto,
                        mt.id_situacao,
                        mt.st_situacao,
                        mt.id_evolucao,
                        mt.st_evolucao,
                        vp.id_produto,
                        pd.id_situacao AS id_situacaoproduto,
                        pd.st_situacao AS st_situacaoproduto,
                        pd.id_modelovenda,
                        pd.st_modelovenda,
                        mt.dt_terminomatricula
                    FROM vw_matricula mt
                    JOIN tb_vendaproduto vp	ON vp.id_vendaproduto = mt.id_vendaproduto
                    JOIN vw_produto pd	ON vp.id_produto = pd.id_produto
                    WHERE
                    mt.bl_ativo = 1
                    AND pd.bl_ativo = 1
                    AND pd.id_modelovenda = 2
                    AND mt.dt_inicio <= GETDATE()
                    AND (mt.dt_termino >= GETDATE() OR mt.dt_termino IS NULL)
                    AND (mt.dt_inicioturma <= GETDATE() OR mt.dt_inicioturma IS NULL)
                    AND (mt.dt_terminoturma >= GETDATE() OR mt.dt_terminoturma IS NULL)
                    AND (mt.dt_terminomatricula >= GETDATE() OR mt.dt_terminomatricula IS NULL)";

            if ($idMatricula) {
                $sql .= " AND mt.id_matricula = {$idMatricula}";
            }
            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetch();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados da matricula.' . $ex->getMessage());
        }
    }


    /**
     * O método usado no cadastro de projeto pedagógico, sincroniza as disciplinas da grade dos alunos com a grade do projeto pedagógico
     * @param int $id_projetopedagogico
     * @return bool
     * @throws \Zend_Exception
     */
    public function corrigirMatriculaDisciplina($id_projetopedagogico)
    {
        $this->_em->beginTransaction();

        try {
            if (!$id_projetopedagogico) {
                throw new \Zend_Exception('O Projeto Pedagógico é obrigatório');
            }

            // Adicionar na grade do aluno as novas disciplinas adicionadas no projeto
            $sql = "
              INSERT INTO tb_matriculadisciplina (
                    id_matricula, 
                    id_evolucao,
                    id_disciplina,
                    id_situacao,
                    nu_aprovafinal,
                    bl_obrigatorio,
                    id_matriculaoriginal
                )
                SELECT 
                    mt.id_matricula,
                    " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO . ", 
                    modi.id_disciplina, 
                    " . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_PENDENTE . ",
                    NULL,
                    modi.bl_obrigatoria,
                    mt.id_matricula
                FROM 
                    tb_matricula AS mt
                    JOIN tb_modulo AS mo ON mt.id_projetopedagogico = mo.id_projetopedagogico AND mo.bl_ativo = 1
                    JOIN tb_modulodisciplina AS modi 
                        ON modi.id_modulo = mo.id_modulo AND modi.bl_ativo = 1
                            AND modi.id_disciplina NOT IN (SELECT id_disciplina FROM tb_matriculadisciplina WHERE id_matricula = mt.id_matricula)
                WHERE 
                    mt.id_projetopedagogico = {$id_projetopedagogico}
            ";

            $stmt = $this->_em->getConnection()->prepare($sql);
            $stmt->execute();

            // Evoluções de Matrícula que terão as disciplinas removidas da grade dos alunos caso sejam removidas do projeto
            $evolucoes = array(
                \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO,
                \G2\Constante\Evolucao::TB_MATRICULA_TRANCADO,
                \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO,
                \G2\Constante\Evolucao::TB_MATRICULA_BLOQUEADO
            );

            // Sincronizar obrigatoriedade da disciplina da grade dos alunos com a do projeto
            $sql = "
                UPDATE
                    tb_matriculadisciplina
                SET
                    bl_obrigatorio = modi.bl_obrigatoria
                FROM 
                    tb_matriculadisciplina AS md
                    JOIN tb_matricula AS mt ON mt.id_matricula = md.id_matricula
                    JOIN tb_modulo AS mo ON mo.id_projetopedagogico = mt.id_projetopedagogico AND mo.bl_ativo = 1
                    JOIN tb_modulodisciplina AS modi ON modi.id_modulo = mo.id_modulo AND modi.id_disciplina = md.id_disciplina AND modi.bl_ativo = 1
                WHERE 
                    md.bl_obrigatorio != modi.bl_obrigatoria
                    AND mt.id_evolucao IN (" . implode(',', $evolucoes) . ")
                    AND mt.id_projetopedagogico = {$id_projetopedagogico}
            ";

            $stmt = $this->_em->getConnection()->prepare($sql);
            $stmt->execute();

            // Buscar as disciplinas que foram removidas do projeto pedagógoico e ainda estão na grade dos alunos
            $sql = "
                SELECT DISTINCT
                    md.id_matriculadisciplina,
                    al.id_alocacao
                FROM 
                    tb_matricula AS mt
                    JOIN tb_matriculadisciplina AS md ON md.id_matricula = mt.id_matricula
                    LEFT JOIN tb_alocacao AS al ON al.id_matriculadisciplina = md.id_matriculadisciplina
                    OUTER APPLY (
                        SELECT TOP 1 modi2.id_modulodisciplina
                        FROM 
                            tb_modulo AS mo
                            JOIN tb_modulodisciplina AS modi2 
                              ON modi2.id_modulo = mo.id_modulo 
                                AND modi2.id_disciplina = md.id_disciplina 
                                AND modi2.bl_ativo = 1
                        WHERE
                            mo.bl_ativo = 1
                            AND mo.id_projetopedagogico = mt.id_projetopedagogico
                    ) AS modi
                WHERE 
                    modi.id_modulodisciplina IS NULL
                    AND mt.id_evolucao IN (" . implode(',', $evolucoes) . ")
                    AND mt.id_projetopedagogico = {$id_projetopedagogico}
            ";

            $query = $this->_em->getConnection()->executeQuery($sql);
            $response = $query->fetchAll();

            if ($response) {
                // Variáveis que armazenarão os IDS dos registros que serão removidos
                $idsMatDisciplina = array();
                $idsAlocacao = array();

                foreach ($response as $item) {
                    $idsMatDisciplina[] = $item['id_matriculadisciplina'];

                    if ($item['id_alocacao']) {
                        $idsAlocacao[] = $item['id_alocacao'];
                    }
                }

                // Converter para String
                $idsMatDisciplina = implode(',', $idsMatDisciplina);
                $idsAlocacao = implode(',', $idsAlocacao);

                /**
                 * As queries abaixo foram estão explícitas para que a execução tenha uma melhor performance
                 */

                // Remover dependências da Alocação
                if ($idsAlocacao) {
                    // Remover todos os registros de "Encerramento Alocação" em uma única query
                    $sql = "DELETE FROM tb_encerramentoalocacao WHERE id_alocacao IN({$idsAlocacao})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();

                    // Remover todos os registros de "Alocação Integração" em uma única query
                    $sql = "DELETE FROM tb_alocacaointegracao WHERE id_alocacao IN({$idsAlocacao})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();

                    // Remover todos as Alocações em uma única query
                    $sql = "DELETE FROM tb_alocacao WHERE id_alocacao IN({$idsAlocacao})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();
                }

                // Remover dependências e as disiplinas da grade do aluno
                if ($idsMatDisciplina) {
                    // Remover todos os registros de "Entrega Material" em uma única query
                    $sql = "DELETE FROM tb_entregamaterial WHERE id_matriculadisciplina IN({$idsMatDisciplina})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();

                    // Remover todos os registros de "Disciplina Matrícula / Disciplina Origem" em uma única query
                    $sql = "DELETE FROM tb_discmatriculadiscorigem WHERE id_matriculadisciplina IN({$idsMatDisciplina})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();

                    // Remover todos os registros de "Matrícula Disciplina" em uma única query
                    $sql = "DELETE FROM tb_matriculadisciplina WHERE id_matriculadisciplina IN({$idsMatDisciplina})";
                    $stmt = $this->_em->getConnection()->prepare($sql);
                    $stmt->execute();
                }
            }

            $this->_em->commit();

            return true;
        } catch (\Exception $e) {
            $this->_em->rollback();
            echo $e->getMessage(); exit;
//            throw new \Zend_Exception('Erro ao corrigir a Matrícula nas Disciplinas - ' . $e->getMessage());
        }
    }


    /**
     * Método para retornar alunos vinculados a uma turma para impressão da carteirinha
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarUsuariosTurmaCarteirinha(array $params)
    {
        try {
            $qb = $this->createQueryBuilder('mt')
                ->select(array(
                    'mt.id_usuario',
                    'mt.st_nomecompleto',
                    'mt.st_identificacao',
                    'mt.st_urlavatar',
                    'mt.st_codigo',
                    'mt.id_turma',
                    'eec.st_valor',
                    'ppd.nu_cargahoraria',
                ))->distinct()
                ->leftJoin('G2\Entity\VwEntidadeEsquemaConfiguracao', 'eec', 'WITH', '
                    eec.id_entidade = mt.id_entidadematricula
                    AND eec.st_valor > 0
                    AND eec.id_itemconfiguracao = 3
                    ')
                ->join('G2\Entity\ProdutoProjetoPedagogico', 'ppd', 'WITH', '
                    ppd.id_entidade = mt.id_entidadematricula
                    AND ppd.id_projetopedagogico = mt.id_projetopedagogico
                    AND mt.id_turma = ppd.id_turma
                    ')
                //só busca matriculas que estão associadas a uma turma
                ->andWhere("mt.st_codigo IS NOT NULL OR mt.st_codigo = ''")
                //código da carteirinha do aluno, deve trazer somente os alunos que não tem cód cadastrado
                ->andWhere("mt.st_identificacao IS NULL OR mt.st_identificacao = ''")
                //o contrato regra de ser preenchidos
                ->andWhere('mt.id_contratoregra IS NOT NULL')
                //o contrato regra de ser preenchidos
                ->andWhere('ppd.nu_cargahoraria IS NOT NULL')
                ->andWhere('eec.st_valor IS NOT NULL')
                ->andWhere('((ppd.nu_cargahoraria) / (eec.st_valor)) > :qt_encontros')
                ->setParameter('qt_encontros', \G2\Constante\Matricula::NU_MINIMODEENCONTROS);

            //verifica o status da matricula por padrão deve ser true
            if (isset($params['bl_ativo'])) {
                $qb->andWhere('mt.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo']);
            }

            //verifica a evolução da matricula do aluno, por padrão deve ser cursando
            if (isset($params['id_evolucao'])) {
                $qb->andWhere('mt.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }

            //verifica a entidade de matricula
            if (isset($params['id_entidadematricula'])) {
                $qb->andWhere('mt.id_entidadematricula = :id_entidadematricula')
                    ->setParameter('id_entidadematricula', $params['id_entidadematricula']);
            }

            //Verifica o código da turma
            if (isset($params['id_turma'])) {
                $qb->andWhere('mt.id_turma = :id_turma')
                    ->setParameter('id_turma', $params['id_turma']);
            }

            //verifica se o tipo de regra de contrato, se for contrato regra FREEPASS não deve ser gerado
            if (isset($params['id_NotTiporegracontrato'])) {
                $qb->andWhere('mt.id_tiporegracontrato != :id_NotTiporegracontrato')
                    ->setParameter('id_NotTiporegracontrato', $params['id_NotTiporegracontrato']);
            }

            //determina o período de datas da matricula
            if (isset($params['dt_matricula_inicial']) && isset($params['dt_matricula_fim'])) {
                $qb->andWhere('mt.dt_cadastro BETWEEN :dt_inicio AND :dt_termino')
                    ->setParameter('dt_inicio', $params['dt_matricula_inicial'])
                    ->setParameter('dt_termino', $params['dt_matricula_fim']);
            }

            //Verifica o paramentro para trazer com ou sem foto, por padrão deve sempre trazer com foto
            if (isset($params['st_urlavatar']) && !empty($params['st_urlavatar'])) {
                $qb->andWhere("mt.st_urlavatar IS NOT NULL OR mt.st_urlavatar != ''");
            } else {
                $qb->andWhere("mt.st_urlavatar IS NULL OR mt.st_urlavatar = ''");
            }

            //ordena pelo nome e pela turma
            $qb->orderBy('mt.st_nomecompleto', 'ASC');

            return $qb->getQuery()->getResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao consultar alunos de uma turma - ' . $ex->getMessage());
        }
    }

    /**
     * Retorna cursos vinculados ao aluno
     * ESTE MÉTODO É UTILIZADO PARA RETORNAR OS CURSOS PARA O APLICATIVO MOBILE
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarCursosAlunos(array $params = [])
    {

        try {

            if (array_key_exists('id_evolucao', $params) && !empty($params['id_evolucao']) && is_array($params['id_evolucao'])) {
                $params['id_evolucao'] = implode(",", $params['id_evolucao']);
            }

            //monta a query
            $sql = 'SELECT DISTINCT
                vwm.dt_inicioturma,
                vwm.st_projetopedagogico,
                pp.st_tituloexibicao,
                vwm.id_projetopedagogico,
                vwm.id_entidadematricula,
                vwm.st_entidadematricula,
                vwm.id_matricula,
                vwm.st_descricao,
                vwm.st_imagem,
                e.st_urlimglogo,
                e.st_urlimgapp,
                e.st_urlsite,
                ac.st_imagemarea,
                ce.bl_acessoapp,
                pp.bl_atendimentovirtual,
                sist.st_caminho AS st_conexao,
                sist.st_codchave AS st_chaveacesso
            FROM vw_matricula AS vwm
            JOIN tb_entidade AS e ON vwm.id_entidadematricula = e.id_entidade
            JOIN tb_projetopedagogico AS pp ON vwm.id_projetopedagogico = pp.id_projetopedagogico
            JOIN tb_configuracaoentidade AS ce ON e.id_entidade = ce.id_entidade
            JOIN tb_areaconhecimento AS ac ON ac.id_areaconhecimento = vwm.id_areaconhecimento AND ac.bl_ativo = 1
            JOIN tb_compartilhadados AS ec ON ec.id_entidadedestino = vwm.id_entidadematricula
            AND ec.id_tipodados = ' . \G2\Constante\TipoDados::LOGIN . '
            LEFT JOIN tb_entidadeintegracao AS sist ON sist.id_sistema = 33 AND sist.id_entidade = vwm.id_entidadematricula
            WHERE vwm.id_usuario = :id_usuario
            AND vwm.bl_ativo = :bl_ativo
            AND vwm.id_evolucao IN(' . $params['id_evolucao'] . ')
            AND vwm.id_situacao = :id_situacao
            AND ce.bl_acessoapp = :bl_acessoapp
            AND ec.id_entidadeorigem = :id_entidade
            AND (vwm.dt_termino IS NULL
                  OR vwm.dt_termino > GETDATE())';


            if (array_key_exists("id_matricula", $params) && !empty($params['id_matricula'])) {
                $sql .= ' AND vwm.id_matricula = :id_matricula';
            }

            if (array_key_exists("bl_novoportal", $params)) {
                $params['bl_novoportal'] = $params['bl_novoportal'] ? true : false;
                $sql .= ' AND pp.bl_novoportal = :bl_novoportal';
            }

            $sql .= ' ORDER BY vwm.st_entidadematricula ASC, vwm.st_projetopedagogico ASC';
            unset($params['id_evolucao']);
            //retorna o fetch
            return $this->_em->getConnection()->fetchAll($sql, $params);

        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar cursos vinculados ao aluno. ' . $e->getMessage());
        }
    }

    /**
     * Método para retornar alunos vinculados a uma turma para impressão da carteirinha
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarUsuariosSemMatriculaVinculada()
    {
        try {
            $sql = "SELECT DISTINCT
                        vd.id_usuario,
                        us.st_nomecompleto,
                        vd.id_venda,
                        vd.id_entidade,
                        mp.st_meiopagamento,
                        ct.id_contrato,
                        e.st_evolucao,
                        pd.st_produto,
                        ct.dt_cadastro
                    FROM dbo.tb_venda AS vd
                    JOIN dbo.tb_vendaproduto AS vp ON vd.id_venda = vp.id_venda
                    JOIN dbo.tb_produto AS pd  ON vp.id_produto = pd.id_produto  AND pd.id_tipoproduto = 1
                    JOIN dbo.tb_produtoprojetopedagogico AS pp ON pp.id_produto = pd.id_produto
                    JOIN dbo.tb_evolucao AS e ON e.id_evolucao = vd.id_evolucao
                    JOIN dbo.tb_contrato AS ct  ON ct.id_venda = vd.id_venda
                    JOIN dbo.tb_lancamentovenda AS lv ON lv.id_venda = ct.id_venda
                    JOIN dbo.tb_lancamento AS l ON l.id_lancamento = lv.id_lancamento
                    JOIN dbo.tb_meiopagamento AS mp ON mp.id_meiopagamento = l.id_meiopagamento
                      AND ct.id_contrato NOT IN (SELECT
                        id_contrato
                      FROM dbo.tb_contratomatricula)
                    JOIN tb_usuario AS us ON us.id_usuario = vd.id_usuario
                    WHERE vd.bl_ativo = 1
                    AND vd.dt_cadastro  >= DATEADD(DAY, -1, GETDATE())
                    AND  vd.id_evolucao IN(10,44)
                      ";

            //Ordena a consulta
            $sql .= ' ORDER BY us.st_nomecompleto ASC';
            $query = $this->_em->getConnection()->executeQuery($sql);
            $query = $query->fetchAll();
            return $query;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao gerar as matriculas sem vinculo. " . $e->getMessage());
        }
    }

    /*
     * Método que retorna os alunos vunculados ao um projeto pedagogico
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaAlunosProjetoPedagogico(array $params)
    {
        $qb = $this->createQueryBuilder('ma')
            ->select()
            ->where('ma.bl_ativo = :bl_ativo')
            ->setParameter('bl_ativo', '1');
        if (isset($params['id_projetopedagogico'])) {
            $qb->andWhere('ma.id_projetopedagogico = :id_projetopedagogico')
                ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
        }
        if (isset($params['id_disciplina'])) {
            $qb->andWhere('ma.id_disciplina = :id_disciplina')
                ->setParameter('id_disciplina', $params['id_disciplina']);
        }
        if (isset($params['id_saladeaula'])) {
            $qb->andWhere('ma.id_saladeaula = :id_saladeaula')
                ->setParameter('id_saladeaula', $params['id_saladeaula']);
        }
        if ($params['dt_inicio'] && $params['dt_termino']) {
            $qb->andWhere('ma.dt_cadastro BETWEEN :dt_inicio AND :dt_termino')
                ->setParameter('dt_inicio', $params['dt_inicio'])
                ->setParameter('dt_termino', $params['dt_termino']);
        }
        if ($params['termino_matricula']) {
            $qb->andWhere('ma.dt_terminomatricula is not null');
        }

        $qb->orderBy('ma.st_nomecompleto', 'ASC');


        return $qb->getQuery()->getResult();

    }

    /**
     * Retorna as matriculas que tem renovação para ser finalizadas.
     * author: helder silva <helder.silva@unyleya.com.br>
     *
     * @param int $id_venda
     * @return array
     * @throws \Exception
     */
    public function retornaMatriculasRenovacaoFinalizar($id_venda = null)
    {
        $query = sprintf('SELECT *
                            FROM tb_matricula AS mat
                      INNER JOIN tb_vendaproduto AS ven_pro ON mat.id_matricula = ven_pro.id_matricula
                      INNER JOIN tb_venda AS ven ON ven_pro.id_venda = ven.id_venda
                           WHERE ven.bl_renovacao = 1
                             AND ven.id_evolucao = %d
                             AND ven.id_situacao = %d
                             AND ven.bl_ativo = 1
                             AND EXISTS (
                                SELECT id_meiopagamento
                                  FROM vw_vendalancamento
                                 WHERE id_venda = ven.id_venda)'
            , \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO
            , \G2\Constante\Situacao::TB_VENDA_PENDENTE
        );

        /**
         * @history GII-8758
         * Se possuir o ID Venda, não deverá verificar a data de limite da renovação
         */
        if ($id_venda) {
            $query .= " AND ven.id_venda = $id_venda ";
        } else {
            $query .= " AND ven.dt_limiterenovacao < GETDATE() ";
        }

        try {
            return $this->_em
                ->getConnection()
                ->executeQuery($query)
                ->fetchAll();

        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Retorna a data limite para renovacao de uma matrícula
     *
     * @param $id_matricula
     * @return mixed
     * @throws \Exception
     */
    public function retornaDataLimiteRenovacao($id_matricula)
    {
        try {
            if (!$id_matricula) {
                throw new \Exception(
                    'O id_matricula deve ser passado para a pesquisa da data de limite de renoção ser realizada'
                );
            }

            $sql = "SELECT TOP 1 v.*, mat.*
                      FROM tb_matricula AS mat
			            JOIN tb_vendaproduto AS vp ON vp.id_matricula = mat.id_matricula
			            JOIN tb_venda AS v ON v.id_venda = vp.id_venda
			            JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = vp.id_produto
			          WHERE mat.id_matricula = {$id_matricula} 
			            AND v.bl_renovacao = 1 
			            AND v.id_evolucao = 9 
			            AND v.bl_ativo = 1
			          ORDER BY v.id_venda DESC";

            $query = $this->_em->getConnection()->executeQuery($sql);

            return $query->fetch();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function retornaVendaMatricula($params)
    {
        try {
            $sql = "SELECT * FROM tb_matricula AS mt
                    JOIN tb_vendaproduto AS vp ON mt.id_matricula = vp.id_matricula
                    JOIN tb_venda AS v ON vp.id_venda = v.id_venda WHERE 1 = 1 ";

            if (array_key_exists('id_matricula', $params) && $params['id_matricula']) {
                $sql .= " AND mt.id_matricula = {$params['id_matricula']}";
            };

            if (array_key_exists('bl_renovacao', $params) && $params['bl_renovacao']) {
                $sql .= " AND v.bl_renovacao = {$params['bl_renovacao']}";
            }

            $orderBy = ' ORDER BY v.dt_cadastro DESC';

            $query = $this->_em->getConnection()->executeQuery($sql . $orderBy);
            return $query->fetch();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Verifica se a matricula é de uma entidade com linha de negocio 2 (Graduação)
     * IMPORTANTE: valida o id_entidadematricula da matricula.
     * @param $id_matricula
     * @return bool
     * @throws \Exceptionn
     */
    public function isMatriculaGraduacao($id_matricula)
    {
        try {

            $sql = "SELECT eci.st_valor FROM tb_matricula AS mat
                        JOIN tb_entidade AS e ON e.id_entidade = mat.id_entidadematricula
                        JOIN tb_esquemaconfiguracaoitem AS eci ON eci.id_esquemaconfiguracao = e.id_esquemaconfiguracao
                        JOIN tb_itemconfiguracao as ic ON eci.id_itemconfiguracao = ic.id_itemconfiguracao AND ic.id_itemconfiguracao = 22
                        WHERE mat.id_matricula = {$id_matricula} ";

            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();

            $return = false;

            if ($result && is_array($result) && array_key_exists('st_valor', $result[0])) {
                if ($result[0]['st_valor'] == \G2\Constante\LinhaDeNegocio::GRADUACAO) {
                    $return = true;
                }
            }

            return $return;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Retorna o próximo semestre para matriculas disciplina
     * @param $id_matricula
     * @return int
     */
    public function proximoSemestre($id_matricula)
    {
        try {

            $sql = "SELECT TOP 1 ISNULL(v.nu_semestre, 0) + 1 AS nu_nextsemestre
                          FROM tb_venda AS v
								  JOIN tb_vendaproduto AS pv ON v.id_venda = pv.id_venda AND pv.id_matricula = {$id_matricula}
								  JOIN tb_produtoprojetopedagogico AS ppp ON ppp.id_produto = pv.id_produto

								  WHERE v.bl_ativo = 1
								     		AND v.id_situacao = 39
								     		AND v.id_evolucao = 10
                              ORDER BY v.id_venda DESC";
            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();

            $return = 1;
            if (is_array($result) && array_key_exists(0, $result) && array_key_exists('nu_nextsemestre', $result[0])) {
                $return = $result[0]['nu_nextsemestre'];
            }

            return $return;
        } catch (\Exception $ex) {
            return 0;
        }
    }

    /**
     * Retorna uma coluna da consulta de acordo com o valor passado via parametro
     * @param $id_matricula
     * @param $str_key
     * @return bool|mixed|string
     */
    public function retornaVariaveisVendaConfirmada($id_matricula, $str_key)
    {
        try {
            $sql = "SELECT TOP 1 v.nu_valorbruto, v.nu_valorliquido
                    , (SELECT vle.nu_valor FROM vw_vendalancamento vle WHERE vle.bl_entrada = 1 AND vle.id_venda = v.id_venda) AS nu_valorentrada
                    , (SELECT vlp.nu_valor FROM vw_vendalancamento vlp WHERE vlp.bl_entrada = 0 AND vlp.nu_ordem = 2 AND vlp.id_venda = v.id_venda) AS nu_valormensalidade
                    , (SELECT count(lan.id_lancamento) FROM tb_lancamento as lan
                                                        JOIN tb_lancamentovenda AS lv ON lv.id_venda = v.id_venda
                                                                        AND lan.id_lancamento = lv.id_lancamento ) AS nu_parcelas

                    FROM tb_vendaproduto AS vp
                    JOIN tb_venda AS v ON v.id_venda = vp.id_venda
                        AND vp.id_matricula = {$id_matricula} AND v.id_evolucao = 10 AND v.bl_ativo = 1
                    JOIN tb_produtoprojetopedagogico AS ppp ON vp.id_produto = ppp.id_produto
                    ORDER BY v.id_venda DESC";
            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetch();
            $return = '';

            if (is_array($result) && !empty($result) && array_key_exists($str_key, $result)) {
                $return = $result[$str_key];
            }
            return $return;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Retorna nu_semestre da ultima venda ativa da matricula (produto projeto pedagogico)
     * @param $id_matricula
     * @return bool
     */
    public function retornaSemestreUltimaVendaMatricula($id_matricula)
    {
        try {
            $sql = "SELECT TOP 1 v.id_venda, v.nu_semestre
                        FROM tb_vendaproduto AS vp
                        JOIN tb_venda AS v ON v.id_venda = vp.id_venda
                            AND vp.id_matricula = {$id_matricula} AND v.id_evolucao = 10 AND v.bl_ativo = 1
                        JOIN tb_produtoprojetopedagogico AS ppp ON vp.id_produto = ppp.id_produto

                        ORDER BY v.id_venda DESC";
            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();
            return $result[0]['nu_semestre'];
        } catch (\Exception $e) {
            return false;
        }
    }


    public function sumMatriculaDisciplinaAprovada($dados)
    {
        try {
            $sql = "SELECT SUM(nu_aprovafinal) as total , count(id_matriculadisciplina) AS nu_aprovadas
                        FROM tb_matriculadisciplina
                            WHERE id_matricula = {$dados['id_matricula']}
                            AND nu_semestre =  {$dados['nu_semestre']}
                            AND id_evolucao in (  " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO . " , " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO . " )
                            AND id_situacao =  {$dados['id_situacao']}
                            AND id_situacao != " . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA . "

                            ";
            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();
            if (is_array($result) && !empty($result) && array_key_exists('nu_aprovadas', $result[0])) {
                return $result[0];
            } else
                return false;
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     * Método para retornar a grid de disciplinas baseadas na matricula da renovação
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarGridDisciplinasByMatriculaRenovacao($id_matricula)
    {
        try {

            $sql = "select * from (select top 1 tb_venda.id_venda from tb_matricula
                    join tb_vendaproduto on tb_matricula.id_matricula = tb_vendaproduto.id_matricula
                    join tb_venda on tb_vendaproduto.id_venda = tb_venda.id_venda
                    join tb_projetopedagogico on tb_matricula.id_projetopedagogico = tb_projetopedagogico.id_projetopedagogico
                    where tb_venda.bl_renovacao = 1 and tb_matricula.id_matricula = {$id_matricula}
                    ORDER BY  tb_venda.id_venda DESC) as consulta1
                    join tb_prematriculadisciplina on consulta1.id_venda = tb_prematriculadisciplina.id_venda
                    join tb_disciplina on tb_prematriculadisciplina.id_disciplina = tb_disciplina.id_disciplina
                    ";

            $query = $this->_em->getConnection()->executeQuery($sql);
            $query = $query->fetchAll();
            return $query;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao gerar as disciplinas. " . $e->getMessage());
        }
    }

    /**
     * Retorna disciplinas para isenção
     *
     * @param $id_matricula
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Zend_Exception
     */
    public function retornaDisciplinasIsencao($id_matricula)
    {
        try {
            $sql = "select
                      md.id_matricula,
                      md.id_matriculadisciplina,
                      ds.nu_cargahoraria,
                      ds.st_disciplina
                    from
                      tb_matriculadisciplina as md
                      LEFT JOIN tb_disciplina as ds ON ds.id_disciplina = md.id_disciplina
                    where
                      md.id_matricula = $id_matricula ";

            $query = $this->_em->getConnection()->executeQuery($sql);
            $query = $query->fetchAll();
            return $query;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao gerar declaração: " . $e->getMessage());
        }
    }

    /**
     * Retorna os dados para variavel complexa GRIDHISTORICO - CERTIFICADO
     * @param $id_matricula
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Exception
     */
    public function retornarGridHistoricoEscolar($id_matricula)
    {
        try {
            $script = ' SELECT st_tituloexibicaodisciplina
                     ,  st_disciplina
                     , nu_cargahoraria
                     , nu_notafinal
                     , st_evolucao
                     , st_projetopedagogico
                     , st_semestre = CASE WHEN dt_inicio IS NULL THEN \' -- \'
                                        WHEN MONTH(dt_inicio) > 6 THEN  CONVERT ( VARCHAR , YEAR(dt_inicio)  , 4) +  \'.2  \'
                                        ELSE CONVERT ( VARCHAR , YEAR(dt_inicio)  , 4) +  \'.1 \'
                                        END

                    , (SELECT SUM (nu_cargahoraria) FROM vw_gradenota WHERE id_matricula = ' . $id_matricula . ' GROUP BY id_matricula )AS nu_totalcargahoraria
                                        FROM vw_gradenota
                                        WHERE id_matricula = ' . $id_matricula . '

                                        ORDER BY id_matriculadisciplina ';
            $query = $this->_em->getConnection()->executeQuery($script);
            $query = $query->fetchAll();
            return $query;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao gerar gridHistoricoEscolar. " . $e->getMessage());
        }
    }

    public function retornaTiposAvaliacao($id_matricula)
    {
        try {
            $script = ' SELECT DISTINCT id_tipoavaliacao = CASE WHEN aa.id_tipoavaliacao IS NULL THEN 5
												 ELSE aa.id_tipoavaliacao
												 END ,
			aa.st_tipoavaliacao AS st_labeltipo
			 FROM dbo.vw_avaliacaoaluno_simples AS aa
			WHERE aa.id_matricula= ' . $id_matricula . ' AND  aa.id_tipoavaliacao IN  (1,2,4,5,6)';
            $query = $this->_em->getConnection()->executeQuery($script);
            $query = $query->fetchAll();
            return $query;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao buscar tipos de avaliação. " . $e->getMessage());
        }
    }

    /**
     * @param int $id_usuario
     * @param int $id_projetopedagogico
     * @return null|int
     */
    public function retornarMatriculaAtivaProjeto($id_usuario, $id_projetopedagogico)
    {
        $id_matricula = null;

        if ($id_usuario && $id_projetopedagogico) {
            $dql = 'SELECT mt.id_matricula
                        FROM \G2\Entity\Matricula AS mt
                        JOIN \G2\Entity\MatriculaDisciplina AS md WITH md.id_matricula = mt.id_matricula
                        JOIN \G2\Entity\DisciplinaSalaDeAula AS ds WITH ds.id_disciplina = md.id_disciplina
                        WHERE mt.id_evolucao = ' . \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO . '
                          AND mt.id_usuario = ' . $id_usuario . '
                          AND mt.id_projetopedagogico = ' . $id_projetopedagogico;

            $matricula = $this->_em->createQuery($dql)->getResult();

            if ($matricula) {
                $id_matricula = $matricula[0]['id_matricula'];
            }
        }

        return $id_matricula;
    }

    /**
     * retorna o gênero do aluno pela matricula
     *
     * @param $id_matricula
     * @return array|\Doctrine\DBAL\Driver\Statement
     * @throws \Exception
     */
    public function retornaGenero($id_matricula)
    {
        try {
            $script = 'SELECT TOP 1
                         st_sexo
                       FROM
                         tb_pessoa AS pe
                       JOIN
                         tb_matricula AS mat ON pe.id_usuario = mat.id_usuario
                       WHERE
                         mat.id_matricula = ' . $id_matricula;

            $query = $this->_em->getConnection()->executeQuery($script);
            $query = $query->fetchAll();

            return $query[0]['st_sexo'];
        } catch (\Exception $e) {
            throw new \Exception("Erro ao buscar dados de matrícula: " . $e->getMessage());
        }
    }

    /**
     * Verifica se o aluno tem uma renovação feita e ainda não efetivada.
     * @param $id_matricula
     * @return bool|int
     */
    public function verificaRenovacaoAtiva($id_matricula)
    {
        try {
            if ($id_matricula) {
                $sql = "SELECT _ven.id_venda
                             , _ven.dt_limiterenovacao
                            FROM tb_venda _ven
                              INNER JOIN tb_vendaproduto _ven_pro ON _ven.id_venda = _ven_pro.id_venda
                            WHERE _ven.id_evolucao = " . \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO . "
                                  AND _ven.dt_limiterenovacao > '" . date('Y-m-d') . "'
                                  AND _ven.bl_renovacao = 1
                                  AND _ven.bl_ativo = 1
                                  AND _ven.id_situacao = " . \G2\Constante\Situacao::TB_VENDA_PENDENTE . "
                                  AND _ven_pro.id_matricula = {$id_matricula}
                            ORDER BY _ven.id_venda DESC
                        ";
                $result = $this->_em->getConnection()->executeQuery($sql)->fetch();
                return $result;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Retorna os alunos que estao cursando em turmas que ja foram encerradas
     * do esquema de configuracao IMP.
     * @return array|bool
     */
    public function retornarAlunosTurmasEncerradas($max_execute)
    {

        if (!$max_execute) {
            throw new \Exception('Nenhum limite foi passado para consultar os alunos concluintes de turmas encerradas!');
        }

        try {
            $sql = "select top " . $max_execute . " m.id_matricula,
                          u.st_nomecompleto,
                          m.id_entidadematricula,
                          e.st_nomeentidade,
                          t.id_turma,
                          t.st_turma,
                          t.dt_fim
                        from tb_matricula as m
                          join tb_usuario as u on u.id_usuario = m.id_usuario
                          join tb_turma as t on t.id_turma = m.id_turma
                          join tb_entidade as e on m.id_entidadematricula = e.id_entidade
                          join tb_esquemaconfiguracao as ec on ec.id_esquemaconfiguracao = e.id_esquemaconfiguracao
                        where t.dt_fim < getdate()
                              and e.id_esquemaconfiguracao =" . \G2\Constante\EsquemaConfiguracao::IMP . "
                              and m.id_evolucao = " . \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO . "
                              and m.bl_institucional = 0";
            $result = $this->_em->getConnection()->executeQuery($sql)->fetchAll();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**Função que retorna dados para a criação da url para ver log no moodle
     * @param $id_saladeaula
     * @param $id_usuario
     * @return bool|mixed
     */
    public function retornaParametroMoodle($id_saladeaula, $id_usuario, $id_entidade = null)
    {
        try {

            if ($id_saladeaula && $id_usuario) {
                $sql = "SELECT
                          ei.st_codsistema,
                          si.st_codsistemacurso,
                          us.st_codusuario,
                          us.st_loginintegrado,
                          us.st_senhaintegrada,
                          sa.id_entidadeintegracao,
                          sa.id_entidade
                        FROM tb_saladeaula AS sa
                          INNER JOIN tb_entidadeintegracao AS ei ON sa.id_entidadeintegracao = ei.id_entidadeintegracao
                          INNER JOIN tb_saladeaulaintegracao AS si ON sa.id_saladeaula = si.id_saladeaula
                          INNER JOIN tb_usuariointegracao AS us ON us.id_usuario = {$id_usuario}
                        WHERE sa.id_saladeaula = {$id_saladeaula}";

                if ($id_entidade) {
                    $sql .= "AND us.id_entidade = {$id_entidade}";
                }

                return $this->_em->getConnection()->executeQuery($sql)->fetch();

            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * Retorna os dados da vw_alunogradeintegracaopos para atender a demanda GII-9176
     * O método foi criado aqui para utilizar o doctrine e seus caches
     * @param array|null $where
     * @return array
     * @throws \Exception
     */
    public function retornarVwAlunoGradeIntegracaoPos(array $where = null)
    {
        try {

            $qb = $this->createQueryBuilder('vw');
            if ($where) {
                foreach ($where as $key => $criterio) {
                    $qb->andWhere("vw." . $criterio);
                }

            }

            foreach ([
                         'vw.nu_ordenacao' => Criteria::ASC,
                         'vw.dt_abertura' => Criteria::ASC,
                         'vw.id_projetopedagogico' => Criteria::ASC,
                         'vw.id_modulo' => Criteria::ASC,
                         'vw.id_disciplina' => Criteria::ASC
                     ] as $column => $criterio) {
                $qb->addOrderBy($column, $criterio);

            }

            return $qb->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /**
     * Verifica se a matrícula do aluno tem o status de concluinte ou certificado
     * @param $idMatricula
     * @return int
     */
    public function verificarMatriculaConcluinteOuCursando($idMatricula)
    {
        try {
            $query = $this->createQueryBuilder("mt")
                ->select("count(mt.id_matricula)")
                ->andWhere("mt.id_matricula = :id_matricula")
                ->andWhere("mt.id_evolucao = :concluinte OR mt.id_evolucao = :certificado")
                ->setParameter("concluinte", \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE)
                ->setParameter("certificado", \G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO)
                ->setParameter("id_matricula", $idMatricula);

            return (int)$query->getQuery()->getSingleScalarResult();

        } catch (\Exception $e) {

        }
    }

    /**
     * Função que retorna maticulas com evolução cursando a partir do id_usuario.
     * feature GII-9563
     * @param $idUsuario
     * @return array|bool
     * @throws \Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculas($idUsuario, $idEntidade)
    {
        try {
            if ($idUsuario && $idEntidade) {
                $query = $this->createQueryBuilder("mt")
                    ->select("mt.id_matricula, pp.st_projetopedagogico , ev.st_evolucao, en.st_nomeentidade , ev.id_evolucao")
                    ->innerJoin(
                        '\G2\Entity\ProjetoPedagogico',
                        'pp',
                        'WITH',
                        'mt.id_projetopedagogico = pp.id_projetopedagogico AND pp.bl_disciplinacomplementar = 0'
                    )
                    ->innerJoin(
                        '\G2\Entity\Evolucao',
                        'ev',
                        'WITH',
                        'mt.id_evolucao = ev.id_evolucao'
                    )
                    ->innerJoin(
                        '\G2\Entity\Entidade',
                        'en',
                        'WITH',
                        'mt.id_entidadematricula = en.id_entidade'
                    )
                    ->andWhere("mt.id_usuario = :id_usuario")
                    ->andWhere("mt.id_evolucao IN 
                            (:id_evolucaoCursando, :id_evolucaoConcluinte, :id_evolucaoCertificado ) ")
                    ->andWhere("mt.id_entidadematricula = :id_entidade")
                    ->setParameter("id_usuario", $idUsuario)
                    ->setParameter("id_entidade", $idEntidade)
                    ->setParameter("id_evolucaoCursando", \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO)
                    ->setParameter("id_evolucaoConcluinte", \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE)
                    ->setParameter("id_evolucaoCertificado", \G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO);

                $result = $query->getQuery()->getArrayResult();

                return $result;

            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /** Função que retorna matricula complementar com evolução cursando e concluinte passando id_usuario.
     * feature GII-9563
     * @param $idUsuario
     * @param $idEntidade
     * @return array|bool
     * @throws \Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornaMatriculaComplementar($idUsuario, $idEntidade)
    {
        try {
            if ($idUsuario && $idEntidade) {
                $query = $this->createQueryBuilder("mt")
                    ->select("mt.id_matricula, pp.st_projetopedagogico")
                    ->innerJoin(
                        '\G2\Entity\MatriculaDisciplina',
                        'md',
                        'WITH',
                        'mt.id_matricula = md.id_matricula'
                    )
                    ->innerJoin(
                        '\G2\Entity\ProjetoPedagogico',
                        'pp',
                        'WITH',
                        'mt.id_projetopedagogico = pp.id_projetopedagogico AND pp.bl_disciplinacomplementar = 1'
                    )
                    ->andWhere("mt.id_usuario = :id_usuario")
                    ->andWhere("mt.id_evolucao IN ( :id_evolucaoCursando, :id_evolucaoConcluinte )")
                    ->andWhere("mt.id_entidadematricula = :id_entidade")
                    ->setParameter("id_usuario", $idUsuario)
                    ->setParameter("id_entidade", $idEntidade)
                    ->setParameter("id_evolucaoCursando", \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO)
                    ->setParameter("id_evolucaoConcluinte", \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE);

                $result = $query->getQuery()->getArrayResult();

                return $result;

            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /**Função que retorna matricula complementar vinculada passando id_matricula
     * feature GII-9563
     * @param $idMatricula
     * @param $idEntidade
     * @return array|bool
     * @throws \Exception
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculaComplementarVinculada($idMatricula, $idEntidade)
    {
        try {
            if ($idMatricula && $idEntidade) {

                $sql = "SELECT
                              mt.id_matricula,
                              pp.st_projetopedagogico,
                              FORMAT(mt.dt_matriculavinculada, 'dd/MM/yyyy') AS dt_matriculavinculada
                            FROM tb_matricula mt
                              INNER JOIN tb_matriculadisciplina md ON mt.id_matricula = md.id_matriculaoriginal
                              INNER JOIN tb_projetopedagogico pp ON mt.id_projetopedagogico = pp.id_projetopedagogico 
                              AND pp.bl_disciplinacomplementar = 1
                            WHERE mt.id_matriculavinculada = {$idMatricula} 
                            AND mt.id_entidadematricula = {$idEntidade}";

                $query = $this->_em->getConnection()->executeQuery($sql);
                return $query->fetchAll();

            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /**
     * Verifica as matriculas/vendas que tem contrato na tabela
     * @param $id_entidade
     * @param $evolucoes
     * @return array
     * @throws \Exception
     */
    public function retornaMatriculasByEntidadeEvolucao($id_entidade, $evolucoes)
    {
        try {
            //faz a consulta com os parâmetros passados
            $sql = "SELECT v.id_venda FROM tb_matricula m 
                        JOIN tb_vendaproduto vp ON vp.id_vendaproduto = m.id_vendaproduto
                        JOIN tb_venda v ON v.id_venda = vp.id_venda
                        JOIN tb_contrato c ON c.id_venda = v.id_venda
                        JOIN tb_contratoregra cr ON cr.id_contratoregra = c.id_contratoregra
                      WHERE v.bl_ativo = 1
                        AND m.bl_ativo = 1
                        AND m.id_entidadematricula = " . $id_entidade . "
                        AND m.id_evolucao IN (" . $evolucoes . ")";

            $result = $this->_em->getConnection()->executeQuery($sql)->fetchAll();
            return $result;

        } catch (\Exception $e) {
            throw  $e;
        }
    }

}

<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
//use \G2\Entity\PABoasVindas AS PABoasVindasEntity;
use \Ead1\Doctrine\EntitySerializer;

class PAMensagemInformativaRepository extends EntityRepository {

    private $sessao;

    /**
     * @return \G2\Entity\PAMensagemInformativa
     */
    public function findByEntidadeSessao() {
        $this->sessao = new \Zend_Session_Namespace('geral');
        return $this->findOneBy(array('id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * @param integer $id
     * @return \G2\Entity\PAMensagemInformativa
     */
    public function findByEntidade($id) {
        return $this->findBy(array('id_entidade' => $id));
    }

    public function save($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = new \G2\Entity\PAMensagemInformativa();
        $entidadeNegocio = new \G2\Negocio\Entidade();
        $textoSistemaNegocio = new \G2\Negocio\TextoSistema();

        if (!isset($data['id_entidade'])) {
            $data['id_entidade'] = $this->sessao->id_entidade;
        }

        if (!isset($data['id_usuariocadastro'])) {
            $data['id_usuariocadastro'] = $this->sessao->id_usuario;
        }

        //TextoSistema da Mensagem
        $textoCategoria = $this->getEntityManager()->getReference('\G2\Entity\TextoCategoria', \G2\Constante\TextoCategoria::PRIMEIRO_ACESSO);
        $situacao = $this->getEntityManager()->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_TEXTOSISTEMA_ATIVO);
        $textoExibicao = $this->getEntityManager()->getReference('\G2\Entity\TextoExibicao', \G2\Constante\TextoExibicao::SISTEMA);
        $orientacaoTexto = $this->getEntityManager()->getReference('\G2\Entity\OrientacaoTexto', \G2\Constante\OrientacaoTexto::RETRATO);
        $entidade = $this->getEntityManager()->getReference('\G2\Entity\Entidade', $data['id_entidade']);

        $textoMensagem = new \G2\Entity\TextoSistema();
        $textoMensagem->setId_textocategoria($textoCategoria);
        $textoMensagem->setId_situacao($situacao);
        $textoMensagem->setId_textoexibicao($textoExibicao);
        $textoMensagem->setId_orientacaotexto($orientacaoTexto);
        $textoMensagem->setId_usuario($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $textoMensagem->setId_entidade($entidade);
        $textoMensagem->setSt_texto($data['st_textomensagem']);
        $textoMensagem->setSt_textosistema('Primeiro Acesso - Mensagem Informativa - ' . $entidade->getSt_nomeentidade());
        $textoMensagem->setDt_cadastro(new \DateTime());
        $textoMensagem->setDt_inicio(new \DateTime());

        //TextoSistema do Email
        $textoEmail = new \G2\Entity\TextoSistema();
        $textoEmail->setId_textocategoria($textoCategoria);
        $textoEmail->setId_situacao($situacao);
        $textoEmail->setId_textoexibicao($textoExibicao);
        $textoEmail->setId_orientacaotexto($orientacaoTexto);
        $textoEmail->setId_usuario($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $textoEmail->setId_entidade($entidade);
        $textoEmail->setSt_texto($data['st_textoemail']);
        $textoEmail->setSt_textosistema('Primeiro Acesso - Texto do Email - ' . $entidade->getSt_nomeentidade());
        $textoEmail->setDt_cadastro(new \DateTime());
        $textoEmail->setDt_inicio(new \DateTime());

        $this->getEntityManager()->persist($textoMensagem);
        $this->getEntityManager()->persist($textoEmail);

        $etapa->setId_textomensagem($textoMensagem);
        $etapa->setId_textoemail($textoEmail);
        $etapa->setId_usuariocadastro($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $etapa->setId_entidade($entidade);
        $etapa->setDt_cadastro(new \DateTime());

        $this->getEntityManager()->persist($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

    public function update($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = $this->getEntityManager()->getReference('\G2\Entity\PAMensagemInformativa', $data['id']);

        $etapa->getId_textomensagem()->setSt_texto($data['st_textomensagem']);
        $etapa->getId_textoemail()->setSt_texto($data['st_textoemail']);

        $this->getEntityManager()->merge($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

}

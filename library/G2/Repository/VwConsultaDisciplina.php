<?php
/**
 * Class Consulta Disciplina para Repository Consulta Disciplina
 * @author Rafael Leie <rafael.leite@unyleya.com.br>
 * @since 2016-09-30
 *
 * @package G2\Repository
 */
namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class VwConsultaDisciplina extends EntityRepository
{
    /**
     * Retorna dados do relatorio
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function pesquisaResultadoConsultaDisciplina($params)
    {
        try {
            $query = $this->createQueryBuilder('cd')
                ->distinct()
                ->select(array(
                    'cd.id_projetopedagogico',
                    'cd.st_projetopedagogico',
                    'cd.id_disciplina',
                    'cd.st_disciplina',
                    'cd.st_tituloexibicao',
                    'cd.nu_cargahoraria',
                    'cd.st_formatoarquivo',
                    'cd.id_disciplinaconteudo',
                    'cd.st_descricaoconteudo',
                    'cd.dt_cadastro'
                ));

            //BUSCA PELO PROJETO PEDAGOGICO SELECIONADO
            if (isset($params['id_projetopedagogico']) && !empty($params['id_projetopedagogico'])) {
                $query->andWhere('cd.id_projetopedagogico = :id_projetopedagogico')
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
            }

            //BUSCA PELA DISCIPLINA SELECIONADA
            if (isset($params['id_disciplina']) && !empty($params['id_disciplina'])) {
                $query->andWhere('cd.id_disciplina = :id_disciplina')
                    ->setParameter('id_disciplina', $params['id_disciplina']);
            }

            //BUSCA PELO FORMATO DE ARQUIVO SELECIONADO
            if (isset($params['id_formatoarquivo']) && !empty($params['id_formatoarquivo'])) {
                $query->andWhere('cd.id_formatoarquivo = :id_formatoarquivo')
                    ->setParameter('id_formatoarquivo', $params['id_formatoarquivo']);
            }

            //BUSCA PELO PERIODO DA DATA DO CADASTRO DO CONTEUDO
            if (isset($params['dt_inicial_cadastro_conteudo']) &&
                isset($params['dt_termino_cadastro_conteudo']) &&
                !empty($params['dt_inicial_cadastro_conteudo']) &&
                !empty($params['dt_termino_cadastro_conteudo'])
            ) {
                $query->andWhere('cd.dt_cadastro BETWEEN :dt_inicial_cadastro_conteudo AND :dt_termino_cadastro_conteudo')
                    ->setParameter('dt_inicial_cadastro_conteudo', $params['dt_inicial_cadastro_conteudo'])
                    ->setParameter('dt_termino_cadastro_conteudo', $params['dt_termino_cadastro_conteudo']);
            }

            $query->orderBy('cd.st_disciplina', 'asc');

            return $query->getQuery()->getArrayResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

}
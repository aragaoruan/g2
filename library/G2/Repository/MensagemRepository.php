<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para Mensagem
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MensagemRepository extends EntityRepository
{

    /**
     * Retorna lista de mensagens pendentes de serem entregues aos devices
     * dentro das entidades que utilizam aplicativo portal do aluno
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function retornaMensagensEnvioNotificacaoApp(array $whereAnd = [], array $whereLike = [], array $whereIn = [])
    {

        $sql = 'SELECT DISTINCT vw.id_enviomensagem, vw.st_mensagem, ud.st_key FROM dbo.vw_enviarmensagemaluno vw
                JOIN dbo.tb_configuracaoentidade ce ON ce.id_entidade = vw.id_entidade
                JOIN dbo.tb_usuariodevice ud ON ud.id_usuario = vw.id_usuario
                WHERE vw.bl_entrega = 0 AND ce.bl_acessoapp = 1 ';

        foreach ($whereAnd as $key => $value) {
            $sql .= ' AND vw.' . $key . ' = ' . $value;
        }

        foreach ($whereIn as $key => $value) {
            $sql .= ' AND vw.' . $key . ' IN (' . implode(',', $value) . ')';
        }

        $conn = $this->_em->getConnection()->executeQuery($sql);
        return $conn->fetchAll();
    }


    /**
     * Persiste na tb_enviodestinatario os destinatarios que receberao as mensagens
     * @param string $sqlDestinatorios SQL com os critérios e campos para serem inseridos como destinatarios
     * @return bool
     * @throws \Zend_Exception
     */
    public function inserirMensagemDestinatario($sqlDestinatorios)
    {
        try {
            $sql = "INSERT INTO dbo.tb_enviodestinatario
                    (id_tipodestinatario ,
                    id_enviomensagem ,
                    id_usuario ,
                    st_endereco ,
                    st_nome ,
                    id_matricula ,
                    nu_telefone ,
                    id_evolucao)
                    {$sqlDestinatorios}";

            $stmt = $this->_em->getConnection()->prepare($sql);
            return $stmt->execute();

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao definir destinatários. - ' . $ex->getMessage());
        }
    }

}
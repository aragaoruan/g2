<?php
/**
 * Class Categoria para Repository para Relatorio Comercial Ecommerce
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 30/04/2015
 *
 * @package G2\Repository
 */
namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class Categoria extends EntityRepository
{
    /**
     * Retorna as todas as categorias vinculadas aos produtos que não muitas
     * vezes não estão vinculadas as entidades,
     * query utilizada no relatório ecommerce
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaCategoriasProdutoEntidadeParams($params)
    {
        try {
            $sql = '';
            if($params['todos']=='true'){
                $sql = "
                    SELECT DISTINCT
                            cp.st_categoria ,
                            cp.id_categoria
                    FROM    vw_categoriaproduto AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto
                                                         AND pd.bl_ativo = 1
                                                         AND pd.bl_todasentidades = 1
                            JOIN dbo.vw_entidaderecursivaid AS er ON er.nu_entidadepai = pd.id_entidade
                    WHERE   er.id_entidadepai = " . $params['id_entidadepai'];
            }else {
                $sql = "SELECT DISTINCT
                            cp.st_categoria ,
                            cp.id_categoria
                    FROM    vw_categoriaproduto AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto
                                                         AND pd.bl_ativo = 1
                            JOIN dbo.tb_produtoentidade AS pe ON pe.id_produto = pd.id_produto
                    WHERE   pe.id_entidade = "  . $params['id_entidade'] .  "
                    UNION
                    SELECT DISTINCT
                            cp.st_categoria ,
                            cp.id_categoria
                    FROM    vw_categoriaproduto AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto
                                                         AND pd.bl_ativo = 1
                                                         AND pd.bl_todasentidades = 1
                            JOIN dbo.vw_entidaderecursivaid AS er ON er.nu_entidadepai = pd.id_entidade
                    WHERE   er.id_entidade = " . $params['id_entidade'] .  "
                    UNION
                    SELECT DISTINCT
                            cp.st_categoria ,
                            cp.id_categoria
                    FROM    vw_categoriaproduto AS cp
                            JOIN dbo.tb_produto AS pd ON pd.id_produto = cp.id_produto
                                                         AND pd.bl_ativo = 1
                                                         AND pd.bl_todasentidades = 1
                    WHERE   pd.id_entidade = " . $params['id_entidade'] . "
                    ORDER BY st_categoria";
            }
            $query = $this->_em->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

}
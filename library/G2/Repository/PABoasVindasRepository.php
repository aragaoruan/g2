<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
//use \G2\Entity\PABoasVindas AS PABoasVindasEntity;
use \Ead1\Doctrine\EntitySerializer;

class PABoasVindasRepository extends EntityRepository {

    private $sessao;

//    public function __construct($em, Mapping\ClassMetadata $class) {
//        parent::__construct($em, $class);
//        $this->sessao = new \Zend_Session_Namespace('geral');
//    }

    /**
     * @return \G2\Entity\PABoasVindas
     */
    public function findByEntidadeSessao() {
        $this->sessao = new \Zend_Session_Namespace('geral');
        return $this->findOneBy(array('id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * @param integer $id
     * @return \G2\Entity\PABoasVindas
     */
    public function findByEntidade($id) {
        return $this->findBy(array('id_entidade' => $id));
    }

    public function save($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = new \G2\Entity\PABoasVindas();
        $entidadeNegocio = new \G2\Negocio\Entidade();
        $textoSistemaNegocio = new \G2\Negocio\TextoSistema();

        if (!isset($data['id_entidade'])) {
            $data['id_entidade'] = $this->sessao->id_entidade;
        }

        if (!isset($data['id_usuariocadastro'])) {
            $data['id_usuariocadastro'] = $this->sessao->id_usuario;
        }

        $textoCategoria = $this->getEntityManager()->getReference('\G2\Entity\TextoCategoria', \G2\Constante\TextoCategoria::PRIMEIRO_ACESSO);
        $situacao = $this->getEntityManager()->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_TEXTOSISTEMA_ATIVO);
        $textoExibicao = $this->getEntityManager()->getReference('\G2\Entity\TextoExibicao', \G2\Constante\TextoExibicao::SISTEMA);
        $orientacaoTexto = $this->getEntityManager()->getReference('\G2\Entity\OrientacaoTexto', \G2\Constante\OrientacaoTexto::RETRATO);
        $entidade = $this->getEntityManager()->getReference('\G2\Entity\Entidade', $data['id_entidade']);

        $textoSistema = new \G2\Entity\TextoSistema();
        $textoSistema->setId_textocategoria($textoCategoria);
        $textoSistema->setId_situacao($situacao);
        $textoSistema->setId_textoexibicao($textoExibicao);
        $textoSistema->setId_orientacaotexto($orientacaoTexto);
        $textoSistema->setId_usuario($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $textoSistema->setId_entidade($entidade);
        $textoSistema->setSt_texto($data['st_texto']);
        $textoSistema->setSt_textosistema('Primeiro Acesso - Boas Vindas - ' . $entidade->getSt_nomeentidade());
        $textoSistema->setDt_cadastro(new \DateTime());
        $textoSistema->setDt_inicio(new \DateTime());

        $this->getEntityManager()->persist($textoSistema);

        $etapa->setId_textosistema($textoSistema);
        $etapa->setId_usuariocadastro($this->getEntityManager()->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']));
        $etapa->setId_entidade($entidade);
        $etapa->setDt_cadastro(new \DateTime());

        $this->getEntityManager()->persist($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

    public function update($data) {
        $this->sessao = new \Zend_Session_Namespace('geral');
        $etapa = $this->getEntityManager()->getReference('\G2\Entity\PABoasVindas', $data['id_pa_boasvindas']);

        $etapa->getId_textosistema()->setSt_texto($data['st_texto']);

        $this->getEntityManager()->merge($etapa);
        $this->getEntityManager()->flush();

        return $etapa;
    }

}

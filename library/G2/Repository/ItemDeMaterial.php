<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ItemDeMaterial
 * @package G2\Repository
 * @author helder.silva <kayo.silva@unyleya.com.br>
 */
class ItemDeMaterial extends EntityRepository
{


    public function retornaPesquisaItemDeMaterialPresencial($params)
    {
//        \Zend_Debug::dump($params);exit;
        try {

            $where = 'WHERE 1=1';

            if (array_key_exists('id_entidade', $params))
                $where = $where . ' AND im.id_entidade = ' . $params['id_entidade'];

            if (array_key_exists('id_turma', $params))
                $where = $where . ' AND im.imt.id_turma IN (' . implode(',', $params['id_turma']) . ')';

            if (array_key_exists('idi.id_disciplina', $params))
                $where = $where . ' AND im.id_disciplina = ' . $params['id_disciplina'];

            if (array_key_exists('id_professor', $params))
                $where = $where . ' AND im.id_professor = ' . $params['id_professor'];

            if (array_key_exists('nu_encontro', $params))
                $where = $where . ' AND im.nu_encontro = ' . $params['nu_encontro'];

            if (array_key_exists('id_tipomaterial', $params))
                $where = $where . ' AND im.id_tipodematerial = ' . $params['id_tipomaterial'];

            if (array_key_exists('id_situacao', $params))
                $where = $where . ' AND im.id_situacao = ' . $params['id_situacao'];

            if (array_key_exists('st_nomematerial', $params))
                $where = $where . " AND im.st_itemdematerial = '" . $params['st_nomematerial'] . "'";

            if (array_key_exists('nu_valor', $params))
                $where = $where . ' AND im.nu_valor = ' . $params['nu_valor'] . '';

            if (array_key_exists('nu_paginas', $params))
                $where = $where . ' AND im.nu_qtdepaginas = ' . $params['nu_paginas'];

            if (array_key_exists('bl_portal', $params)) {
                if ($params == TRUE) {
                    $where = $where . ' AND im.bl_portal = 1';
                } else {
                    $where = $where . ' AND im.bl_portal = 0';
                }
            }

            $where = $where . ' AND im.bl_ativo = 1;';


            $sql = "SELECT im.*,mat.st_nomeentidade as st_itemdematerial, dis.st_disciplina as st_disciplina, us.st_nomecompleto as st_professor, tip.st_tipodematerial as st_tipo, sit.st_situacao as st_situacao,  id_turmas
                  FROM tb_itemdematerial as im
                JOIN tb_itemdematerialdisciplina AS idi on idi.id_itemdematerial = im.id_itemdematerial
                JOIN tb_entidade AS mat on mat.id_entidade = im.id_entidade
                JOIN tb_disciplina AS dis on dis.id_disciplina = idi.id_disciplina
                JOIN tb_usuario AS us on us.id_usuario = im.id_professor
                JOIN tb_tipodematerial AS tip ON tip.id_tipodematerial = im.id_tipodematerial
                JOIN tb_situacao AS sit ON sit.id_situacao = im.id_situacao
                CROSS APPLY (SELECT CAST(it.id_turma AS VARCHAR)+', ' FROM tb_itemdematerialturma as it where it.id_material = im.id_itemdematerial
				ORDER BY it.id_turma FOR XML PATH('')) D (id_turmas) ";
            $sql = $sql . $where;

//            \Zend_Debug::dump($sql);exit;

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetch();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados da matricula.' . $ex->getMessage());
        }
    }

    public function retornaItemUsuario($params)
    {
        try {
            $where = 'WHERE 1=1 and i.bl_ativo = 1 ';

            if (array_key_exists('id_usuario', $params))
                $where = $where . ' AND mat.id_usuario = ' . $params['id_usuario'];

            if (array_key_exists('id_entidade', $params))
                $where = $where . ' AND mat.id_entidadeatendimento = ' . $params['id_entidade'];

            if (array_key_exists('id_itemdematerial', $params))
                $where = $where . ' AND i.id_itemdematerial = ' . $params['id_itemdematerial'];

            if (array_key_exists('id_matricula', $params))
                $where = $where . ' AND mat.id_matricula = ' . $params['id_matricula'];


            $sql = 'SELECT i.id_itemdematerial ,
        i.st_itemdematerial ,
        i.id_situacao ,
        i.id_tipodematerial ,
        i.dt_cadastro ,
        i.id_usuariocadastro ,
        i.nu_qtdepaginas ,
        i.nu_peso ,
        i.id_upload ,
        i.nu_valor ,
        i.bl_portal ,
        i.nu_encontro ,
        i.id_professor ,
        i.bl_ativo ,
        i.id_entidade,
        i.nu_qtdestoque,
        mat.id_matricula ,
        dis.id_disciplina ,
        dis.st_disciplina ,
		md.id_matriculadisciplina,
		tur.st_turma,
		tmat.st_tipodematerial,
		up.st_upload,
		em.id_entregamaterial,
		em.id_situacao,
		sit.st_situacao
         FROM dbo.tb_itemdematerial AS i
 JOIN dbo.tb_itemdematerialdisciplina AS imd ON imd.id_itemdematerial = i.id_itemdematerial
 JOIN dbo.tb_itemdematerialturma AS imt ON imt.id_material = i.id_itemdematerial
 JOIN dbo.tb_turma AS tur ON tur.id_turma = imt.id_turma
 JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = imd.id_disciplina
 JOIN dbo.tb_matriculadisciplina AS md ON md.id_disciplina = dis.id_disciplina
 JOIN dbo.tb_matricula AS mat ON mat.id_matricula = md.id_matricula AND mat.id_turma = imt.id_turma
 JOIN dbo.tb_tipodematerial AS tmat ON tmat.id_tipodematerial = i.id_tipodematerial
 LEFT JOIN dbo.tb_upload AS up ON up.id_upload = i.id_upload
 LEFT JOIN dbo.tb_entregamaterial AS em ON em.id_itemdematerial = i.id_itemdematerial AND em.id_matriculadisciplina = md.id_matriculadisciplina AND mat.id_matricula = em.id_matricula
 LEFT JOIN dbo.tb_situacao AS sit ON sit.id_situacao = em.id_situacao ';

            $sql = $sql . $where. ' ORDER BY id_itemdematerial';

//            \Zend_Debug::dump($sql);exit;

            $query = $this->_em->getConnection()->executeQuery($sql);

            return $query->fetchAll();
        } catch (\Exception $e) {

        }
    }

    public function retornaTramite($params)
    {
        try {
            $where = 'WHERE 1=1';

            if (array_key_exists('id_usuario', $params))
                $where = $where . ' AND mat.id_usuario = ' . $params['id_usuario'];

            if (array_key_exists('id_entidade', $params))
                $where = $where . ' AND mat.id_entidadeatendimento = ' . $params['id_entidade'];

            $where = $where . 'AND bl_visivel = 1';

            $sql = ' SELECT i.id_itemdematerial ,
        i.st_itemdematerial ,
        i.dt_cadastro ,
        i.nu_encontro ,
        i.id_professor ,
        i.id_entidade,
		em.id_entregamaterial,
		tramite.id_tramite ,
        tramite.id_usuario ,
        tramite.st_tramite ,
        CONVERT(VARCHAR, tramite.dt_cadastro, 103) AS dt_tramite ,
        tramite.bl_visivel,
        u.st_nomecompleto
         FROM dbo.tb_itemdematerial AS i
 JOIN dbo.tb_itemdematerialdisciplina AS imd ON imd.id_itemdematerial = i.id_itemdematerial
 JOIN dbo.tb_itemdematerialturma AS imt ON imt.id_material = i.id_itemdematerial
 JOIN dbo.tb_disciplina AS dis ON dis.id_disciplina = imd.id_disciplina
 JOIN dbo.tb_matriculadisciplina AS md ON md.id_disciplina = dis.id_disciplina
 JOIN dbo.tb_matricula AS mat ON mat.id_matricula = md.id_matricula AND mat.id_turma = imt.id_turma
 LEFT JOIN dbo.tb_entregamaterial AS em ON em.id_itemdematerial = i.id_itemdematerial AND em.id_matriculadisciplina = md.id_matriculadisciplina AND mat.id_matricula = em.id_matricula
 JOIN dbo.tb_tramiteentregamaterial AS trentrega ON trentrega.id_entregamaterial = em.id_entregamaterial
 JOIN dbo.tb_tramite AS tramite ON tramite.id_tramite = trentrega.id_tramite
 JOIN dbo.tb_usuario AS u ON u.id_usuario = tramite.id_usuario ';

            $sql = $sql . $where. '  ORDER BY tramite.id_tramite DESC';

            $query = $this->_em->getConnection()->executeQuery($sql);

            return $query->fetchAll();
        } catch (\Exception $e) {

        }
    }

}
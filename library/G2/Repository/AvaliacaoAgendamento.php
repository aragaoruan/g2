<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository para AvaliacaoAgendamento
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2016-01-27
 */
class AvaliacaoAgendamento extends EntityRepository
{


    /**
     * Busca histórico de tramites de lancamentos de presença para um agendamento
     * @param $id_avaliacaoagendamento
     * @return array
     * @throws \Zend_Exception
     */
    public function findHistoricoLancamentoPresenca($id_avaliacaoagendamento)
    {
        try {

            $sql = 'SELECT tr.id_tramite
                    , CONVERT(VARCHAR, tr.dt_cadastro, 103) AS dt_cadastro
                    , tr.st_tramite
                    , u.st_nomecompleto
                    , sit.st_situacao
                        FROM tb_tramite AS tr
                        JOIN tb_tramiteagendamento AS ta ON ta.id_tramite = tr.id_tramite
                        JOIN tb_usuario AS u ON u.id_usuario = tr.id_usuario
                        JOIN tb_situacao AS sit ON sit.id_situacao = ta.id_situacao
                              WHERE tr.id_tipotramite = 23 AND ta.id_avaliacaoagendamento = ' . $id_avaliacaoagendamento;
            $query = $this->_em->getConnection()->executeQuery($sql);

            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param int $id_matricula
     * @return \G2\Entity\VwAvaliacaoAgendamento|null
     */
    public function retornarUltimoAgendamento($id_matricula)
    {
        $query = $this->_em->getRepository('G2\Entity\VwAvaliacaoAgendamento')
            ->createQueryBuilder("vw")
            ->where('vw.id_matricula = :id_matricula')
            ->andWhere('vw.id_situacao = :id_situacao')
            ->andWhere('vw.id_avaliacaoaplicacao IS NOT NULL')
            ->setParameters(array(
                'id_matricula' => $id_matricula,
                'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO
            ))
            ->orderBy('vw.id_avaliacaoaplicacao', 'DESC')
            ->setMaxResults(1);

        $result = $query->getQuery()->getOneOrNullResult();

        return $result;
    }

    /**
     * Método para verificar se o aluno tem presença, isso substitui o find que fazia em \G2\Negocio\Avaliacao::verificaProvaFinal()
     * @param $id_matricula
     * @return int
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function verificarPossuiPresenca($id_matricula)
    {

        $query = $this->createQueryBuilder("vw")
            ->select("count(vw.id_matricula)")
            ->where("vw.id_matricula = :id_matricula")
            ->andWhere("vw.nu_presenca = :nu_presenca")
            ->setParameter("id_matricula", $id_matricula)
            ->setParameter("nu_presenca", true);

        return (int)$query->getQuery()->getSingleScalarResult();

    }


    /**
     * Faz o count na vw_gradenota com os parametros para verificar se o cara tá aprovado no tcc
     * Substitui o select que fazia para verificar se retornava resultado
     * @param $id_matricula
     * @return int
     * @throws \Exception
     */
    public function verificarTccComDataDefesaeAprovado($id_matricula)
    {
        try {
            $query = $this->createQueryBuilder("vw")
                ->select("count(vw.id_matriculadisciplina)")
                ->where("vw.id_matricula = :id_matricula")
                ->andWhere("vw.id_tipodisciplina = :id_tipodisciplina")
                ->andWhere("vw.id_evolucao = :id_evolucao")
                ->andWhere("vw.dt_defesa IS NOT NULL")
                ->setParameter("id_matricula", $id_matricula)
                ->setParameter("id_tipodisciplina", \G2\Constante\TipoDisciplina::TCC)
                ->setParameter("id_evolucao", \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO);

            return (int)$query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
}

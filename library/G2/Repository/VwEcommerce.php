<?php
/**
 * Class GradeHoraria para Repository para Grade Horaria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-03
 *
 * @package G2\Repository
 */
namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class VwEcommerce extends EntityRepository
{
    /**
     * Retorna dados do relatorio
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function pesquisaRelatorioEcommerce($params)
    {
        try {
            $query = $this->createQueryBuilder('ec')
                ->distinct()
                ->select(array(
                    'ec.st_nomeentidade',
                    'ec.st_nomecompleto',
                    'ec.st_produto',
                    'ec.nu_valortabela',
                    'ec.nu_valorcupom',
                    'ec.nu_valornegociado',
                    'ec.st_campanhacomercial',
                    'ec.st_codigocupom',
                    'ec.st_nomeatendente',
                    'ec.id_venda',
                    'ec.st_evolucao',
                    'ec.st_status',
                    'ec.st_meiopagamento',
                    'ec.nu_parcelas',
                    'ec.dt_cadastroshow',
                    'ec.dt_confirmacaoshow',
                    'ec.st_categorias',
                    'ec.st_cpf',
                    'ec.dt_nascimento',
                    'ec.st_email',
                    'ec.st_telefone',
                    'ec.st_endereco',
                    'ec.sg_uf',
                    'ec.st_cidade',
                    'ec.st_areaconhecimento',
                    'ec.st_bairro',
                    'ec.st_cep'
                ));
            //BUSCA PELA ENTIDADE SELECIONADA
            if (isset($params['id_entidade']) && !empty($params['id_entidade'])) {
                $query->andWhere('ec.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }

            if (isset($params['id_entidadepai']) && !empty($params['id_entidadepai'])) {
                $query->andWhere('ec.id_entidadepai = :id_entidadepai')
                    ->setParameter('id_entidadepai', $params['id_entidadepai']);
            }


            //BUSCA PELA CATEGORIA  SELECIONADA
            if (isset($params['id_categoria']) && !empty($params['id_categoria'])) {
                $query->andWhere('ec.id_categoria = :id_categoria')
                    ->setParameter('id_categoria', $params['id_categoria']);
            }

            //BUSCA PELA PRODUTO SELECIONADA
            if (isset($params['id_produto']) && !empty($params['id_produto'])) {
                $query->andWhere('ec.id_produto = :id_produto')
                    ->setParameter('id_produto', $params['id_produto']);
            }

            //BUSCA PELA EVOLUCAO SELECIONADA
            if (isset($params['id_evolucao']) && !empty($params['id_evolucao'])) {
                $query->andWhere('ec.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }

            //BUSCA PELO MEIO DE PAGAMENTO SELECIONADA
            if (isset($params['id_meiopagamento']) && !empty($params['id_meiopagamento'])) {
                $query->andWhere('ec.id_meiopagamento = :id_meiopagamento')
                    ->setParameter('id_meiopagamento', $params['id_meiopagamento']);
            }

            //BUSCA PELO UF SELECIONADA
            if (isset($params['sg_uf']) && !empty($params['sg_uf'])) {
                $query->andWhere('ec.sg_uf = :sg_uf')
                    ->setParameter('sg_uf', $params['sg_uf']);
            }

            //BUSCA PELO PERIODO DA DATA DA COMPRA
            if (isset($params['dt_inicial_comprar']) &&
                isset($params['dt_termino_comprar']) &&
                !empty($params['dt_inicial_comprar']) &&
                !empty($params['dt_termino_comprar'])
            ) {
                $query->andWhere('ec.dt_cadastro BETWEEN :dt_inicial_comprar AND :dt_termino_comprar')
                    ->setParameter('dt_inicial_comprar', $params['dt_inicial_comprar'])
                    ->setParameter('dt_termino_comprar', $params['dt_termino_comprar']);
            }

            //BUSCA PELO PERIODO DA DATA DA CONFIRMACAO
            if (isset($params['dt_inicial_confirmacao']) &&
                isset($params['dt_termino_confirmacao']) &&
                !empty($params['dt_inicial_confirmacao']) &&
                !empty($params['dt_termino_confirmacao'])
            ) {
                $query->andWhere('ec.dt_confirmacao BETWEEN :dt_inicial_confirmacao AND :dt_termino_confirmacao')
                    ->setParameter('dt_inicial_confirmacao', $params['dt_inicial_confirmacao'])
                    ->setParameter('dt_termino_confirmacao', $params['dt_termino_confirmacao']);
            }
            $query->orderBy('ec.st_nomecompleto', 'asc');

            return $query->getQuery()->getArrayResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

    public function categoriasSemRelacionamentoEntidade()
    {
        try {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->distinct()
                ->select('vwCp.id_categoria', 'vwCp.st_categoria')
                ->from('\G2\Entity\VwCategoriaProduto', 'vwCp')
                ->where('vwCp.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);

            //BUSCA PELA ENTIDADE SELECIONADA
            if (isset($params['id_entidadecadastro']) && !empty($params['id_entidadecadastro'])) {
                $query->andWhere('vwCp.id_entidadecadastro = :id_entidadecadastro')
                    ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
            }
            $query->orderBy('vwCp.st_categoria', 'asc');

            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

} 
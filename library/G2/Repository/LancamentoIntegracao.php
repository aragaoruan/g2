<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \G2\Entity\TotvsVwIntegraGestorFluxus AS TotvsVwIntegraGestorFluxusEntity;
use \Ead1\Doctrine\EntitySerializer;

class LancamentoIntegracao extends EntityRepository {

    protected function montaArrayParams($object){

        if (!is_object($object))
            throw new \Exception('Parâmetro informado deve ser do tipo objeto.');

        $entitySerializer = new EntitySerializer($this->_em);
        $params = array();
        foreach ($entitySerializer->toArray($object) as $key => $value){
            if (!empty($value))
                $params[$key] = $value;
        }

        return $params;
    }

    public function listarLancamentosBaixarFluxus(array $dados = array())
    {
        $stmt = null;
        $connection = $this->_em->getConnection();

        $strQuery ="SELECT st_codlancamento ,
                           CASE WHEN l.id_codcoligada IS NOT NULL
                             THEN l.id_codcoligada
                             ELSE ei.st_codsistema
                           END AS st_codsistema,
                           l.nu_quitado,
                           l.dt_quitado,
                           l.id_lancamento
                      FROM tb_lancamentointegracao li
                INNER JOIN tb_lancamento l ON  li.id_lancamento = l.id_lancamento
                INNER JOIN tb_lancamentovenda lv ON lv.id_lancamento = l.id_lancamento
                INNER JOIN tb_venda v ON lv.id_venda = v.id_venda
                INNER JOIN tb_entidadeintegracao ei ON ei.id_entidade = v.id_entidade
                       AND ei.id_sistema = :id_sistema
                     WHERE l.bl_baixadofluxus = 1
                       AND l.nu_quitado IS NOT NULL
                       AND l.dt_quitado IS NOT NULL
                       AND l.bl_quitado = 1";

        if (! empty($dados['id_lancamento'])) {
            $stmt = $connection->prepare($strQuery .= ' AND l.id_lancamento = :id_lancamento');
            $stmt->bindValue('id_lancamento', $dados['id_lancamento'], \PDO::PARAM_INT);
        } else {
            $stmt = $connection->prepare($strQuery);
        }

        $stmt->bindValue('id_sistema', \G2\Constante\Sistema::FLUXUS, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}

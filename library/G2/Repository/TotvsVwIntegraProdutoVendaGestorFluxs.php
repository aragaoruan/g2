<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \Ead1\Doctrine\EntitySerializer;

class TotvsVwIntegraProdutoVendaGestorFluxs extends EntityRepository {

    protected function montaArrayParams($object){

        if (!is_object($object))
            throw new \Exception('Parâmetro informado deve ser do tipo objeto.');

        $entitySerializer = new EntitySerializer($this->_em);
        $params = array();
        foreach ($entitySerializer->toArray($object) as $key => $value){
            if (!empty($value))
                $params[$key] = $value;
        }

        return $params;
    }

}
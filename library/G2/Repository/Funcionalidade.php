<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository para Funcionalidade
 */
class Funcionalidade extends EntityRepository {

    public function retornarMenuNovoPortal($idEntidade) {
        try {
            $sql = "SELECT * 
                  FROM tb_perfil AS pf
                  JOIN tb_perfilfuncionalidade AS ppf ON ppf.id_perfil = pf.id_perfil 
                                                     AND ppf.bl_ativo = 1
                  JOIN tb_entidadefuncionalidade AS ef ON ef.id_entidade = pf.id_entidade 
                                                      AND ef.bl_visivel = 1 
                                                      AND ef.bl_ativo = 1
                  JOIN tb_funcionalidade AS fc ON fc.id_funcionalidade = ppf.id_funcionalidade 
                                              AND fc.id_funcionalidade = ef.id_funcionalidade 
                                              AND fc.bl_ativo = 1 
                                              AND fc.id_sistema = ".\G2\Constante\Sistema::PORTAL_ALUNO."
                WHERE pf.id_entidade = {$idEntidade} 
                  AND pf.id_perfilpedagogico = ".\G2\Constante\PerfilPedagogico::ALUNO." 
                  AND pf.bl_ativo = 1 
                  AND fc.id_situacao = ".\G2\Constante\Situacao::TB_FUNCIONALIDADE_NOVO_PORTAL . "
                ORDER BY fc.nu_ordem";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }

    }
}

<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \Ead1\Doctrine\EntitySerializer;


/**
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @date 25/06/2014
 */
class VwLancamentoAssinatura extends EntityRepository {

    /**
     * Método que retorna os Lançamentos a serem gerados para assinatura
     * @param array $params
     * @return multitype:
     */
    public function listarLancamentosAssinaturaPendentes(array $params = array()){

        $query = $this->createQueryBuilder("vw")
            ->select(" vw ")
            ->orderBy("vw.id_entidade, vw.id_venda");
        return $query->getQuery()->getResult();
        
    }
    
    

} 
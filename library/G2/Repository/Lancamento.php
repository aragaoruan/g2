<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;
use \G2\Entity\TotvsVwIntegraGestorFluxus AS TotvsVwIntegraGestorFluxusEntity;
use \Ead1\Doctrine\EntitySerializer;

class Lancamento  extends EntityRepository {

    protected function montaArrayParams($object){

        if (!is_object($object))
            throw new \Exception('Parâmetro informado deve ser do tipo objeto.');

        $entitySerializer = new EntitySerializer($this->_em);
        $params = array();
        foreach ($entitySerializer->toArray($object) as $key => $value){
            if (!empty($value))
                $params[$key] = $value;
        }

        return $params;
    }

    /**
     * @param string $where
     * @throws \Zend_Exception $e
     */
    public function deletarLancamento($where)
    {
        try{
            $query = $this->createQuery("DELETE FROM '\G2\Entity\Lancamento' WHERE " . $where);
            $query->execute();
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao excluir os lançamentos da venda ' . $e->getMessage());
        }
    }

    /**
     * Método que recupera as informações do pagamento com cartão recorrente
     * @param integer $id_venda
     * @return Array
     */
    public function recuperarInfoCartaoRecorrente($id_venda){
        try {
            if (empty($id_venda)) {
                throw new \Exception('Informe o número da venda');
            }

            $query = "
                select DISTINCT 
                    l.id_lancamento, 
                    format(l.dt_vencimento, 'dd/MM/yyyy') as dt_vencimento,
                    l.nu_valor, 
                    l.nu_assinatura,
                    l.bl_quitado,
                    l.nu_cobranca,
                    u.st_nomecompleto,
                    lv.bl_entrada, 
                CASE
                    when (lv.nu_ordem = 1 and lv.bl_entrada = 1) then 'Entrada'
                    else CAST(lv.nu_ordem as varchar)
                end as nu_ordem
                    , 	
                    tf.st_ultimosdigitos,
                    tf.st_titularcartao,
                CASE 
                    when (bl_quitado = 1) then 'Pago'
                    when (bl_quitado = 0 and l.dt_vencimento < GETDATE()) then 'Vencido'
                    else 'Pendente'
                end as situacao,
                pdi.nu_tentativas
                from tb_lancamentovenda lv
                    join tb_lancamento l on lv.id_lancamento = l.id_lancamento 
                    left join tb_transacaolancamento tl on tl.id_lancamento = l.id_lancamento
	                left join tb_transacaofinanceira tf on tf.id_venda = lv.id_venda and tf.id_transacaofinanceira = tl.id_transacaofinanceira
	                left join tb_pedidointegracao pdi on pdi.id_venda = lv.id_venda
                    join tb_usuario u on u.id_usuario = l.id_usuariocadastro
                where lv.id_venda = $id_venda  and l.id_meiopagamento = 11
                order by l.id_lancamento asc
                ";

            $stmt =  $this->_em->getConnection()->prepare($query);

            $stmt->execute();
        }catch (Exception $e){
            return $e->getMessage();
        }
        return $stmt->fetchAll();
    }

}

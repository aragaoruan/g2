<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository para Cupom
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-21
 */
class Cupom extends EntityRepository
{

    /**
     * Retorna Dados do Cupom
     * @param array $params
     * @return \G2\Entity\VwCupomCampanhaProduto
     * @throws \Zend_Exception
     */
    public function retornarCupomCampanhaProdutoWs (array $params)
    {
        try {

            //Cria o query builder
            //Passa como parametro para dt_inicio e dt_fim a data do sistema para verificar a validade do cupom
            $query = $this->createQueryBuilder('vw')
                    ->where('1=1');
            //verifica se o parametro st_codigocupom veio e seta ele
            if (isset($params['st_codigocupom']) && !empty($params['st_codigocupom'])) {
                $query->andWhere("vw.st_codigocupom = :st_codigocupom")
                        ->setParameter("st_codigocupom", $params['st_codigocupom']);
            }
            //verifica se veio um parametro produtos e se é um array
            if (isset($params['produtos']) && is_array($params['produtos'])) {
                //implode o array colocando "," e atribuindo diretamente na where
                $query->andWhere("vw.id_produto IN(" . implode(',', $params['produtos']) . ")");
            }
            return $query->getQuery()->getResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

}

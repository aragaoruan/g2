<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Repository for LogCancelamento
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class LogCancelamento extends EntityRepository
{
    public function getLogCancelamento (array $params)
    {
        try {
            $qb = $this->createQueryBuilder("log")
                ->where('1=1');
            if (isset($params['dt_inicio']) && isset($params['dt_fim'])) {
                $qb->andWhere('log.dt_cadastro BETWEEN :dt_inicio AND :dt_fim')
                    ->setParameter('dt_inicio',$params['dt_inicio'])
                    ->setParameter('dt_fim', $params['dt_fim']);
            } else if (!isset($params['dt_fim']) && isset($params['dt_inicio'])) {
                $qb->andWhere('log.dt_cadastro >= :dt_inicio')
                    ->setParameter('dt_inicio',$params['dt_inicio']);
            } else if (isset($params['dt_fim']) && !isset($params['dt_inicio'])) {
                $qb->andWhere('log.dt_cadastro <= :dt_fim')
                    ->setParameter('dt_fim',$params['dt_fim']);
            };
            if(isset($params['id_matricula'])){
                $qb->andWhere('log.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', $params['id_matricula']);
            };
            $qb->orderBy('log.dt_cadastro', 'DESC');
            return $qb->getQuery()->getResult();
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

}
<?php

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class ModuloDisciplina extends EntityRepository
{

    /**
     * Recupera somatario de uma determinada coluna pelo id do projeto pedagogico
     * @param integer $id
     * @param string $coluna
     * @return array Coluna com o valor da soma
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function recuperaSomatorioPonderacaoByProjeto($id, $coluna)
    {
        try {
            //verifica se passou o id != vazio
            if (!$id) {
                throw new \Exception('Id do projeto não encontrado.');
            }
            //monta o query builder
            $query = $this->createQueryBuilder('vw')
                ->select("SUM(vw.{$coluna}) as {$coluna}")
                ->where('vw.id_projetopedagogico = :id')
                ->setParameter('id', $id);
            //retorna
            return $query->getQuery()->getSingleResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex);
        }
    }

    /**
     * Método para retornar a caraga horaria de uma disciplina dentro de um projeto pedagogico
     * @param int $idDisciplina
     * @param int $idProjeto
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @return mixed | array
     */
    public function retornaCargaHorariaDisciplinaNoProjeto($idDisciplina, $idProjeto)
    {
        try {
            //monta a query selecionando somente a coluna necessária
            $qb = $this->createQueryBuilder('md')
                ->select('md.nu_cargahoraria')
                ->join('md.id_modulo', 'm')
                ->where('m.id_projetopedagogico = :projeto')
                ->andWhere('md.id_disciplina = :disciplina')
                ->andWhere('md.bl_ativo = 1');


            //seta os parametros
            if ($idDisciplina && $idProjeto) {
                $qb->setParameters(array(
                    'projeto' => $idProjeto,
                    'disciplina' => $idDisciplina
                ));
            }
            //retorna o resultado
            return $qb->getQuery()->getSingleResult();
        } catch (\Exception $e) {
            throw new \Exception("Erro ao consultar carga horária da disciplina no projeto. " . $e->getMessage());
        }

    }

}

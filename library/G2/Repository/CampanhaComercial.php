<?php

namespace G2\Repository;

use Doctrine\ORM\AbstractQuery;
use \Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Internal\Hydration\ArrayHydrator;

/**
 * Repository para CampanhaComercial
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-07-14
 */
class CampanhaComercial extends EntityRepository
{

    /**
     * Retorna os dados da vw_campanhaprodutocategoria feita para atender ao site do IMP
     * @param array $params
     * @param $idEntidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVwCampanhaProdutoCategoria(array $params, $idEntidade)
    {
        try {

            $qbuilder = $this->createQueryBuilder('vw')->where('1 = 1');

            if (!empty($params['id_produto'])) {
                $qbuilder->andWhere('vw.id_produto = :id_produto')
                    ->setParameter('id_produto', $params['id_produto']);
            }

            if (!empty($params['bl_ativo'])) {
                $qbuilder->andWhere('vw.bl_ativo = 1')
                    ->andWhere('vw.id_situacao = 41');
            }

            if (!empty($params['id_campanhacomercial'])) {
                $qbuilder->andWhere('vw.id_campanhacomercial = :id_campanhacomercial')
                    ->setParameter('id_campanhacomercial', $params['id_campanhacomercial']);
            }
            if (!empty($params['id_finalidadecampanha'])) {
                $qbuilder->andWhere('vw.id_finalidadecampanha = :id_finalidadecampanha')
                    ->setParameter('id_finalidadecampanha', $params['id_finalidadecampanha']);
            }

            $qbuilder->andWhere('vw.id_entidade = :id_entidade')->setParameter('id_entidade', $idEntidade);

            if (!empty($params['bl_recente'])) {

                if (empty($params['id_produto'])) {
                    throw new \Exception("Para recuperar a campanha mais recente você precisa informar o id_produto.");
                }

                $qbuilder->andWhere('vw.bl_vigente = 1')
                    ->andWhere('vw.bl_ativo = 1')
                    ->andWhere('vw.id_situacao = 41')->orderBy('vw.dt_fim', 'desc')->setMaxResults(1);

                return $qbuilder->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
            }

            $qbuilder->orderBy('vw.st_campanhacomercial', 'asc');

            return $qbuilder->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Retorna os dados da vw_campanhapremioproduto de acordo com a entidade e o array de produtos
     * @param array $produtosId array de produtos
     * @param $idEntidade id da entidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaDadosVwCampanhaPremioProduto(array $produtosId, $idEntidade)
    {
        try {
            $qb = $this->createQueryBuilder('vw')
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $idEntidade)
                ->andWhere('vw.id_produto in(' . implode(',', $produtosId) . ')')
                ->orderBy('vw.id_campanhacomercial', 'asc');

            return $qb->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar dados da vw_campanhapremioproduto. " . $e->getMessage());
        }
    }

    public function retornaCampanhaComercialCupom(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {

            $query = $this->createQueryBuilder('cc')
                ->select(array(
                    'cc.id_campanhacomercial',
                    'cc.st_campanhacomercial'
                ))
                ->where('cc.bl_ativo = :bl_ativo')
                ->andWhere('cc.id_categoriacampanha = :id_categoriacampanha')
                ->setParameter('id_categoriacampanha', \G2\Constante\CategoriaCampanha::CUPOM)
                ->setParameter('bl_ativo', true);

            if (isset($params['entidade']) && !empty($params['entidade'])) {
                $query->andWhere('cc.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['entidade']);
            }

            if (isset($params['dt_atual']) && !empty($params['dt_atual'])) {
                $query->andWhere('cc.dt_fim >= :dt_atual')
                    ->setParameter('dt_atual', $params['dt_atual']);
            }

            if (!$orderBy)
                $orderBy = array('st_campanhacomercial' => 'ASC');


            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'cc.id_campanhacomercial');
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

    /**
     * Retorna as campanhas comerciais
     * @param array $params
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return object
     * @throws \Zend_Exception
     */
    public function retornaCampanhaCustomizada(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            $query = $this->createQueryBuilder('cc')
                ->select()
                ->where('cc.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true)
                ->andWhere('cc.id_finalidadecampanha = :id_finalidadecampanha')
                ->setParameter('id_finalidadecampanha', 1);

            if (isset($params['id_entidade']) && !empty($params['id_entidade'])) {
                $ar_entidades = array($params['id_entidade']);

                $ng_entidade = new \G2\Negocio\Entidade();

                /** @var \G2\Entity\Entidade $entidade */
                $id_esquemaconfiguracao = $ng_entidade->retornaEsquemaConfiguracao($params['id_entidade']);

                // Se for graduação, trazer campanhas da entidade pai$relacoes
                if ($id_esquemaconfiguracao == \G2\Constante\EsquemaConfiguracao::GRADUACAO) {
                    $entidadepai = $ng_entidade->retornaEntidadePai($params['id_entidade']);

                    if ($entidadepai instanceof \G2\Entity\Entidade) {
                        $ar_entidades[] = $entidadepai->getId_entidade();
                    }
                }

                $query->andWhere('cc.id_entidade IN (:id_entidade)')
                    ->setParameter('id_entidade', $ar_entidades);
            }

            if (isset($params['id_tipocampanha']) && !empty($params['id_tipocampanha'])) {
                $query->andWhere('cc.id_tipocampanha = :id_tipocampanha')
                    ->setParameter('id_tipocampanha', $params['id_tipocampanha']);
            }

            if (isset($params['id_tipodesconto']) && !empty($params['id_tipodesconto'])) {
                $query->andWhere('cc.id_tipodesconto = :id_tipodesconto')
                    ->setParameter('id_tipodesconto', $params['id_tipodesconto']);
            }

            if (array_key_exists('bl_ativo', $params)) {
                $query->andWhere('cc.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo'] ?: 0);
            }

            //Verifica se tem vigência, se sim, vai buscar as campanhas que estão de em vigência
            if (!empty($params['vigencia'])) {
                $query->andWhere('cc.dt_fim >= :dt_atual')
                    ->andWhere('cc.dt_inicio <= :dt_atual')
                    ->setParameter('dt_atual', date('Y-m-d'));
            }

            if (!$orderBy) {
                $orderBy = array('st_campanhacomercial' => 'ASC');
            }

            // Se passar o ID da campanha selecionada, ela será retornada INDEPENDENTE de qualquer condição
            if (!empty($params['id_campanhaselecionada'])) {
                $query->orWhere('cc.id_campanhacomercial = :id_campanhacomercial')
                    ->setParameter('id_campanhacomercial', $params['id_campanhaselecionada']);
            }

            $pesquisaBO = new \PesquisarBO();

            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'cc.id_campanhacomercial');
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


}

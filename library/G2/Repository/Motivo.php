<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class Motivo para Repository para Motivos
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-03-02
 *
 * @package G2\Repository
 */
class Motivo extends EntityRepository
{
    /**
     * Retorna dados de motivo
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarMotivos(array $params = array(), $orderBy = null, $limit = 0, $offset = 0){
        try{
            $query = $this->createQueryBuilder('mt')
                ->select(
                    'mt.id_motivo',
                    'mt.st_motivo',
                    'mt.dt_cadastro',
                    's.id_situacao',
                    's.st_situacao'
                )
                ->join('mt.id_situacao','s')
                ->where('mt.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            if($params){
                if (isset($params['st_motivo'])) {
                    $query->andWhere("mt.st_motivo LIKE :st_motivo")
                        ->setParameter('st_motivo', '%' . $params['st_motivo'] . '%');
                }
                if (isset($params['id_entidadecadastro'])) {
                    $query->andWhere('mt.id_entidadecadastro = :id_entidadecadastro')
                        ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
                }
                if (isset($params['id_situacao'])) {
                    $query->andWhere('mt.id_situacao = :id_situacao')
                        ->setParameter('id_situacao', $params['id_situacao']);
                }
            }

            if (!$orderBy)
                $orderBy = array('id_motivo' => 'ASC');

            $pesquisaBO = new \PesquisarBO();
            return $pesquisaBO->preparaPaginacaoOpicional($query, $orderBy, $limit, $offset, 'mt.id_motivo');

        }catch(\Exception $e){
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
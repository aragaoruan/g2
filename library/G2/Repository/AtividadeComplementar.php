<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

class AtividadeComplementar extends EntityRepository
{
    private $arrWhere = [
        "st_nomecompleto" => [
            "criteria" => "LIKE",
            "alias" => "u",
            "column" => "st_nomecompleto"
        ],
        "st_cpf" => [
            "criteria" => "=",
            "alias" => "u",
            "column" => "st_cpf"
        ],
        "id_projetopedagogico" => [
            "criteria" => "=",
            "alias" => "pp",
            "column" => "id_projetopedagogico"
        ],
        "id_situacao" => [
            "criteria" => "=",
            "alias" => "atividade",
            "column" => "id_situacao"
        ],
        "id_tipoatividade" => [
            "criteria" => "=",
            "alias" => "atividade",
            "column" => "id_tipoatividade"
        ],
        "id_entidade" => [
            "criteria" => "=",
            "alias" => "atividade",
            "column" => "id_entidadecadastro"
        ],
        "id_matricula" => [
            "criteria" => "=",
            "alias" => "mat",
            "column" => "id_matricula"
        ]
    ];

    /**
     * Seta os parametros na consulta
     * @param \Doctrine\ORM\QueryBuilder $query
     * @param array $params
     */
    private function setParametros(\Doctrine\ORM\QueryBuilder &$query, array $params = [])
    {

        foreach ($params as $coluna => $valor) {
            if (!$valor) {
                continue;
            }
            $attributes = (object)$this->arrWhere[$coluna];

            if ($attributes->criteria === "LIKE") {
                $valor = "%$valor%";
            }

            $where = $attributes->alias . "." . $attributes->column . " " . $attributes->criteria . " :" . $coluna;
            $query->andWhere($where)
                ->setParameter($coluna, $valor);

        }

    }


    /**
     * Método para retornar a pesquisa das atividades complementares
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornarPesquisaAtividade(array $params = [])
    {
        try {

            /** @var  \Doctrine\ORM\QueryBuilder $query */
            $query = $this->createQueryBuilder("atividade")
                ->select(array(
                    "u.id_usuario",
                    "u.st_nomecompleto",
                    "u.st_cpf",
                    "mat.id_matricula",
                    "pp.id_projetopedagogico",
                    "pp.st_projetopedagogico",
                    "pp.nu_horasatividades",
                    "evo.id_evolucao",
                    "evo.st_evolucao",
                    "s.id_situacao",
                    "s.st_situacao",
                    "atividade.id_atividadecomplementar",
                    "atividade.st_tituloatividade",
                    "atividade.st_resumoatividade",
                    "atividade.st_observacaoavaliador",
                    "atividade.nu_horasconvalidada",
                    "atividade.st_caminhoarquivo",
                    "atividade.bl_ativo",
                    "atividade.dt_analisado",
                    "ec.id_entidade AS id_entidadecadastro",
                    "ta.id_tipoatividadecomplementar AS id_tipoatividade",
                    "up.id_upload",
                    "usanalise.id_usuario AS id_usuarioanalise"
                ))
                ->join("atividade.id_matricula", "mat")
                ->join("mat.id_evolucao", "evo")
                ->join("mat.id_usuario", "u")
                ->join("mat.id_projetopedagogico", "pp")
                ->join("atividade.id_situacao", "s")
                ->join("atividade.id_entidadecadastro", "ec")
                ->join("atividade.id_tipoatividade", "ta")
                ->join("atividade.id_upload", "up")
                ->leftJoin("atividade.id_usuarioanalise", "usanalise")
                ->where("atividade.bl_ativo = :bl_ativo")
                ->setParameter("bl_ativo", true);

            $this->setParametros($query, $params);

            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw $e;
        }
    }
}

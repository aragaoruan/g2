<?php

namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

/**
 * Class NucleoTm  para Repository para Nucleo
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-03-25
 *
 * @package G2\Repository
 */
class NucleoTm extends EntityRepository
{
    public function retornaNucleoFuncionarioTm(array $params = null)
    {
        $query = $this->createQueryBuilder('n')
            ->select()
            ->where('n.id_entidadematriz = :id_entidadematriz')
            ->setParameter('id_entidadematriz', $params['id_entidade'])
            ->andWhere('n.bl_ativo = :bl_ativo')
            ->setParameter('bl_ativo', true)
            ->andWhere('n.id_situacao = :id_situacao')
            ->setParameter('id_situacao', 76);


        $query->orderBy('n.st_nucleotm', 'ASC');

        $result = $query->getQuery()->getResult();
        return $result;
    }

    /**
     * Método para retornar a pesquisa da funcionalidade
     * @param array $arrColumns
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return object
     */
    public function retornarPesquisa(array $arrColumns, array $params = [], $orderBy = null, $limit = 0, $offset = 0)
    {

        //faz o parse das colunas que quero retornar
        foreach ($arrColumns as $i => $arrColumn) {
            if ($arrColumn == 'ntm.st_situacao')
                $arrColumns[$i] = 'st.st_situacao';
        }

        $qb = $this->createQueryBuilder('ntm')
            ->select(array('ntm.id_nucleotm', 'ntm.st_nucleotm', 'st.st_situacao'))
            ->join('ntm.id_situacao', 'st')
            ->where('ntm.bl_ativo = :bl_ativo')
            ->setParameter('bl_ativo', true);


        if (isset($params['id_nucleotm']) && !empty($params['id_nucleotm'])) {
            $qb->andWhere('ntm.id_nucleotm = :id_nucleotm')
                ->setParameter('id_nucleotm', $params['id_nucleotm']);
        }


        //todo arrumar a forma de fazer o like, fiz da forma feia
        if (array_key_exists('st_nucleotm', $params) && !empty($params['st_nucleotm'])) {
            $qb->andWhere("ntm.st_nucleotm LIKE '%{$params['st_nucleotm']}%'");
        }

        if (isset($params['id_situacao']) && !empty($params['id_situacao'])) {
            $qb->andWhere('ntm.id_situacao= :id_situacao')
                ->setParameter('id_situacao', $params['id_situacao']);
        }

        if (isset($params['id_entidadematriz']) && !empty($params['id_entidadematriz'])) {
            $qb->andWhere('ntm.id_entidadematriz= :id_entidadematriz')
                ->setParameter('id_entidadematriz', $params['id_entidadematriz']);
        }

        if (!$orderBy)
            $orderBy = array('st_nucleotm' => 'asc');


        $pesquisaBO = new \PesquisarBO();
        return $pesquisaBO->preparaPaginacaoOpicional($qb, $orderBy, $limit, $offset, 'ntm.id_nucleotm');

    }
}
<?php

/**
 * Classe repository para Turma e seus relacionamentos
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */

namespace G2\Repository;

use Doctrine\ORM\EntityRepository;

class Turma extends EntityRepository
{

    /**
     * Retorna daddos do turno baseado em um projeto.
     * @param integer $idProjeto
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarTurnoByProjeto($idProjeto)
    {
        try {
            $sql = "select DISTINCT tn.id_turno, tn.st_turno, tup.id_projetopedagogico from tb_turma AS tu
                JOIN dbo.tb_turmaturno AS tt ON tt.id_turma = tu.id_turma
                JOIN dbo.tb_turno AS tn ON tn.id_turno = tt.id_turno
                join dbo.tb_turmaprojeto AS tup on tup.id_turma = tu.id_turma
                where tup.id_projetopedagogico = {$idProjeto}";

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar turnos do projeto. " . $e->getMessage());
        }
    }

    /**
     * Retornar dias da semana de uma turma
     * @param integer $idTurma
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarDiaSemanaByTurma($idTurma, $idDiaSemana = null, $idTurno = null)
    {
        try {
            $sql = "select DISTINCT ds.id_diasemana, ds.st_diasemana, ha.hr_inicio, ha.hr_fim, ta.id_tipoaula, ta.st_tipoaula, tur.id_turno, tur.st_turno from tb_turma AS tu
                JOIN dbo.tb_turmahorario AS th ON th.id_turma = tu.id_turma and th.bl_ativo = 1
                JOIN dbo.tb_horarioaula AS ha ON ha.id_horarioaula = th.id_horarioaula
                JOIN dbo.tb_horariodiasemana AS hds ON hds.id_horarioaula = ha.id_horarioaula
                JOIN dbo.tb_diasemana AS ds ON ds.id_diasemana = hds.id_diasemana
                JOIN dbo.tb_tipoaula AS ta ON ta.id_tipoaula = ha.id_tipoaula
                JOIN dbo.tb_turno AS tur ON tur.id_turno = ha.id_turno
                where tu.id_turma = {$idTurma}";

            if($idDiaSemana){
                $sql .= ' and ds.id_diasemana = '.$idDiaSemana;
            }

            if($idTurno){
                $sql .= ' and tur.id_turno = '.$idTurno;
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dias da semana de uma turma. " . $e->getMessage());
        }
    }
}
<?php
/**
 * Class Repository for Sala de Aula.
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

namespace G2\Repository;


/**
 * Class SalaDeAula
 * @package G2\Repository
 */
class SalaDeAula extends \Doctrine\ORM\EntityRepository
{


    /**
     * Metodo resposavel por retornar VwSalaDeAulaQuantitativos com salas de aula
     * que ainda nao foram iniciadas.
     *
     * @param array $params
     * @param null $stSalaDeAulaLike
     * @param array $order
     * @param null $limit
     * @param null $next
     * @param null $offset
     * @return array
     */
    public function retornaVwSalaDeAulaQuantitativosSalasNaoIniciadas(array $params, $stSalaDeAulaLike = null, $order = array(), $limit = null, $next = null, $offset = null)
    {

        $query = $this->createQueryBuilder('vw')
            ->where('vw.dt_abertura >= :dt_abertura')
            ->setParameter('dt_abertura', new \DateTime());

        if ($stSalaDeAulaLike) {
            $query->andWhere('vw.st_saladeaula like :st_saladeaula')
                ->setParameter('st_saladeaula', "%{$stSalaDeAulaLike}%");
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        if ($order) {
            foreach ($order as $key => $direction) {
                $query->orderBy('vw.' . $key, $direction);
            }
        }

        foreach ($params as $key => $value) {
            $query->andWhere('vw.' . $key . ' = :' . $key)
                ->setParameter($key, $value);
        }

        return $query->getQuery()->getResult();
    }

    public function retornarDisciplinasProfessorPortal(array $params, $order = null, $limit = null, $offset = 0)
    {
        $query = $this->createQueryBuilder('vw')->select('vw.id_disciplina, vw.st_disciplina')->distinct();

        foreach ($params as $attr_with_signal => $value) {
            $comparators = array('=', '<', '>');
            $last_char = substr(trim($attr_with_signal), -1);

            if (trim($attr_with_signal) == 'st_disciplina') {
                $query->andWhere('vw.st_disciplina like :st_disciplina')
                    ->setParameter('st_disciplina', "%{$value}%");
            } elseif (!in_array($last_char, $comparators)) {
                $attr_with_signal .= ' = ';
                $query->andWhere("vw.{$attr_with_signal} {$value}");
            } else {
                $query->andWhere("vw.{$attr_with_signal} {$value}");
            }
        }

        // ORDER parameter
        if (is_array($order) && $order) {
            $first = true;
            foreach ($order as $attr => $sort) {
                if ($first) {
                    $query->orderBy("vw.{$attr}", $sort);
                    $first = false;
                } else {
                    $query->addOrderBy("vw.{$attr}", $sort);
                }
            }
        }

        // LIMIT and OFFSET parameter
        if ($limit) {
            $query
                ->setMaxResults($limit)
                ->setFirstResult($offset);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Retorna as salas de aula vinculada ao projeto pedagógico e a disciplina
     *
     * @funcionalidades: Prorrogaçap Sala de Aula
     * @param array $array
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retonaSalaAulaProrrogacaoSala(array $array)
    {
        try {
            $sql = "
                SELECT DISTINCT
                    sa.id_saladeaula,
                    sa.st_saladeaula
                FROM tb_saladeaula AS sa
                    JOIN tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
                    JOIN tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
                    JOIN tb_saladeaulaentidade AS se ON sa.id_entidade = se.id_entidade
                WHERE 1=1
        ";

            if (isset($array['id_projetopedagogico'])) {
                $sql .= 'AND aps.id_projetopedagogico = ' . $array['id_projetopedagogico'];
            }

            if (isset($array['id_disciplina'])) {
                $sql .= 'AND ds.id_disciplina = ' . $array['id_disciplina'];
            }

            if (isset($array['id_entidade'])) {
                $sql .= 'AND se.id_entidade = ' . $array['id_entidade'];
            }

            $query = $this->_em->getConnection()->executeQuery($sql);
            return $query->fetchAll();

        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao buscar dados da matricula.' . $ex->getMessage());
        }
    }


    /**
     * @param int $id_usuario
     * @param int $id_disciplina
     * @param int $id_saladeaula
     * @return array
     */
    public function getStatusAlunoSala($id_usuario, $id_disciplina, $id_saladeaula, $id_projetopedagogico = null)
    {
        $dql = "SELECT
                    mt.id_usuario,
                    mt.id_matricula,
                    ds.id_saladeaula,
                    md.id_disciplina,
                    md.id_evolucao,
                    ev.st_evolucao,";
        //GII-8401
        $dql .= " (
	                CASE
                        WHEN eec.st_valor = " . \G2\Constante\LinhaDeNegocio::GRADUACAO . "
                        AND al.id_alocacao IS NOT NULL
                        AND mn.bl_aprovado = 0
                        AND mn.st_notarecuperacao IS NULL THEN
                            1
                    ELSE
                           0
                    END
                    ) AS bl_emprovafinal
                    ";
        //GII-8401 END

        $dql .= " FROM G2\Entity\VwMatricula AS mt
                JOIN G2\Entity\MatriculaDisciplina AS md WITH md.id_matricula = mt.id_matricula
                JOIN G2\Entity\DisciplinaSalaDeAula AS ds WITH ds.id_disciplina = md.id_disciplina
                JOIN G2\Entity\Evolucao AS ev WITH ev.id_evolucao = md.id_evolucao";

        //GII-8401
        $dql .= ' JOIN G2\Entity\VwEntidadeEsquemaConfiguracao AS eec WITH eec.id_entidade = mt.id_entidadematricula 
                AND eec.id_itemconfiguracao = ' . \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO . '
                LEFT JOIN G2\Entity\Alocacao AS al WITH al.id_matriculadisciplina = md.id_matriculadisciplina AND al.bl_ativo = 1
                LEFT JOIN G2\Entity\EncerramentoAlocacao AS ea WITH ea.id_alocacao = al.id_alocacao
                LEFT JOIN G2\Entity\VwMatriculaNota AS mn WITH md.id_matriculadisciplina = mn.id_matriculadisciplina 
                AND mn.id_alocacao = al.id_alocacao';
        //GII-8401 END
        $dql .= ' WHERE mt.id_evolucao = ' . \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO . '  
                    AND mt.id_usuario = ' . $id_usuario . ' 
                    AND ds.id_disciplina = ' . $id_disciplina . ' 
                    AND ds.id_saladeaula = ' . $id_saladeaula;

        if ($id_projetopedagogico) {
            $dql .= ' AND mt.id_projetopedagogico = ' . $id_projetopedagogico;
        }


        $matricula = $this->_em->createQuery($dql)->getResult();

        if ($matricula) {
            $matricula = array_shift($matricula);

            $id_evolucao = $matricula['id_evolucao'];
            $st_status = $matricula['st_evolucao'];

            //GII-8401
            if ($matricula['bl_emprovafinal']) {
                $id_evolucao = \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO;
                $st_status = 'Em Prova Final';
            }
            //GII-8401 END

            if ($id_evolucao == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO) {
                $st_status = 'Não Cursado';
            }
        } else {
            $id_evolucao = 0;
            $st_status = 'Não Cursado';
        }

        return array(
            'id_evolucao' => $id_evolucao,
            'st_status' => $st_status
        );
    }

    /**
     * @param int $id_matricula
     * @return null|string
     */
    public function retornarDataEncerramentoUltimaSala($id_matricula)
    {
        $dql = '
          SELECT
            vw.dt_encerramento
          FROM
            G2\Entity\VwGradeNota AS vw
            JOIN \G2\Entity\SalaDeAula AS sa WITH sa.id_saladeaula = vw.id_saladeaula
            JOIN \G2\Entity\PeriodoLetivo AS pl WITH pl.id_periodoletivo = sa.id_periodoletivo
              AND pl.id_tipooferta = :id_tipooferta
          WHERE
            vw.id_matricula = :id_matricula
          ORDER BY
            vw.dt_encerramento DESC
        ';

        $query = $this->_em->createQuery($dql);
        $query->setParameters(array(
            'id_tipooferta' => \G2\Constante\TipoOferta::PADRAO,
            'id_matricula' => $id_matricula
        ));

        $result = $query->setMaxResults(1)->getOneOrNullResult();

        if ($result) {
            return \G2\Utils\Helper::converterData($result['dt_encerramento'], 'Y-m-d');
        }

        return null;
    }
}

<?php
/**
 * Repository para Relatório Alunos a Recuperar
 * @author Rafael Leie <rafael.leite@unyleya.com.br>
 * @since 2017-02-03
 *
 * @update 01/11/2017
 * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
 * @package G2\Repository
 */
namespace G2\Repository;

use \Doctrine\ORM\EntityRepository;

class VwAlunosRecuperar extends EntityRepository
{
    /**
     * Retorna Alunos a Recuperar
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function pesquisaResultadoAlunosRecuperar($params, $entidade, $idUsuarios)
    {
        try {
            $query = $this->createQueryBuilder('vw')
                // Filtrar entidade
                ->where('vw.id_entidadematricula = :id_entidadematricula')
                ->setParameter('id_entidadematricula', $entidade);
            /**
             * Configurando parametros PERSONALIZADOS
             */

            /**
             * Colocando o parametro para trazer de acordo com a nota
             * Diferenciando pelos tipos Resgate ou Recuperação
             */
            if ($params["id_tipo_servico"] != "todos") {
                if ($params["id_tipo_servico"] === "resgate") {
                    $query->andWhere("vw.id_situacaotiposervico = :id_situacaotiposervico")
                        ->setParameter('id_situacaotiposervico', 210);
                } else {
                    $query->andWhere("vw.id_situacaotiposervico = :id_situacaotiposervico")
                        ->setParameter('id_situacaotiposervico', 211);
                }
            }

            if (isset($params['dt_inicioturma_min']) && $params['dt_inicioturma_min']
                    && isset($params['dt_inicioturma_max']) && $params['dt_inicioturma_max']) {
                $query
                    ->andWhere("vw.dt_inicioturma BETWEEN :dt_inicioturma_min AND :dt_inicioturma_max")
                    ->setParameter('dt_inicioturma_min', $params['dt_inicioturma_min'])
                    ->setParameter('dt_inicioturma_max', $params['dt_inicioturma_max']);
            }

            if (isset($params['id_areaconhecimento']) && $params['id_areaconhecimento']) {
                $query
                    ->andWhere("vw.id_areaconhecimento = :id_areaconhecimento")
                    ->setParameter('id_areaconhecimento', $params['id_areaconhecimento']);
            }

            if (isset($params['id_projetopedagogico']) && $params['id_projetopedagogico']) {
                $query
                    ->andWhere("vw.id_projetopedagogico = :id_projetopedagogico")
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
            }

            if (isset($params['id_disciplina']) && $params['id_disciplina']) {
                $query
                    ->andWhere("vw.id_disciplina = :id_disciplina")
                    ->setParameter('id_disciplina', $params['id_disciplina']);
            }

            if (isset($params['id_evolucao']) && $params['id_evolucao']) {
                $query
                    ->andWhere($query->expr()->in("vw.id_evolucao", $params['id_evolucao']));
            }

            //condição para nota ead:caso ele tenha nota EAD
            //  tcc: caso ele não tenha nota ead mas tenha nota final
            //  metodologia: caso ele tenha nota final, tenha nota EAD e não tenha nota TCC
            if (isset($params['id_notaead']) && $params['id_notaead']) {
                $query->andWhere("(vw.st_notaead IS NOT NULL AND vw.st_avaliacaoead IS NOT NULL AND vw.st_avaliacaofinal IS NOT NULL AND vw.st_notaead <= ".$params['id_notaead'].")
                                    OR (vw.st_avaliacaotcc IS NOT NULL AND vw.id_tipodisciplina = " . \G2\Constante\TipoDisciplina::TCC . ")
                                    OR (vw.st_notaead IS NOT NULL AND vw.st_avaliacaoead IS NOT NULL AND vw.st_avaliacaofinal IS NULL )
                                ");

            }else{
                $query->andWhere("(vw.st_avaliacaotcc IS NOT NULL and vw.id_tipodisciplina = " . \G2\Constante\TipoDisciplina::TCC . " )
                                    OR (vw.st_notaead IS NOT NULL AND vw.st_avaliacaoead IS NOT NULL AND vw.st_avaliacaofinal IS NULL )
                                ");
            }

            if ($idUsuarios) {
                $query->andWhere("vw.id_usuario NOT IN " . $idUsuarios);
            }

            $query->orderBy('vw.st_nomecompleto', 'ASC');
            $results = $query->getQuery()->getResult();

            return $results;

        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }
    }
}

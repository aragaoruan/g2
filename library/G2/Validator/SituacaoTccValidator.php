<?php

namespace G2\Validator;

use G2\Entity\Alocacao;

class SituacaoTccValidator
{
    const INVALID_DATA_TYPES = 'invalid';
    const INVALID_ARGUMENTS = 'error';

    private $validator;

    public function __construct()
    {
        $this->validator = new \Zend_Validate();
    }

    protected $_messages = array(
        self::INVALID_DATA_TYPES => 'Argumentos ou tipos de dados inválidos na Atualização da Situação do TCC',
        self::INVALID_ARGUMENTS => 'Argumentos inválidos na Atualização da Situação do TCC',
    );


    /**
     * @param $parametrosDeBusca
     * @param $novaSituacaoTcc
     * @return bool
     * @throws \Zend_Validate_Exception
     */
    public function isValid($parametrosDeBusca, $novaSituacaoTcc)
    {
        if (!is_array($parametrosDeBusca))
            throw new \Zend_Validate_Exception($this->_messages[self::INVALID_ARGUMENTS]);

        try {
            $isValid = (new \Zend_Validate_Int())->isValid($novaSituacaoTcc) && $novaSituacaoTcc !== 0
                && (new \Zend_Validate_NotEmpty())->isValid($parametrosDeBusca);

            foreach ($parametrosDeBusca as $parametro => $valor) {
                $isValid = $isValid
                    && (new \Zend_Validate_InArray($this->getPrivatePropertiesFrom(Alocacao::class)))
                        ->isValid($parametro);
            }

            if (!$isValid) {
                throw new \Zend_Validate_Exception($this->_messages[self::INVALID_DATA_TYPES]);
            }
            return true;
        } catch (\Zend_Validate_Exception $e) {
            throw new \Zend_Validate_Exception($e->getMessage());
        }
    }

    private function getPrivatePropertiesFrom($class)
    {
        $result = array();
        $reflect = new \ReflectionClass($class);
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

        foreach ($properties as $property) {
            array_push($result, $property->getName());
        }
        return $result;
    }
}

<?php

namespace G2\Constante;

/**
 * Class TipoPremio valores padr�es para Tipo Premio, utilizado no cadastro de campanhas premios
 * @package G2\Constante
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-07-20
 */
class TipoPremio
{
    const PRODUTO = 1;
    const CUPOM = 2;

    /**
     * Retorna array com os nomes dos tipos de premio
     * @return array
     */
    public static function getArray()
    {
        return array(
            self::PRODUTO => "Produto",
            self::CUPOM => "Cupom"
        );
    }

    /**
     * Retorna o tipo do premio segundo o parametro passado
     * @param integer $tipo n�mero ou id do tipo
     * @return string
     */
    public static function getTipo($tipo)
    {
        $premioArray = self::getArray();
        return $premioArray[$tipo];
    }
}
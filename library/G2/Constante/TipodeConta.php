<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de CONTA
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class TipodeConta {

    const CONTACORRENTE = 3;

}

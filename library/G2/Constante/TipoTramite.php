<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de trâmite
 * @author Densie Xavier <denise.xavier@unyleya.com.br>
 */
class TipoTramite
{
    const MATRICULA_SITUACAO = 1;
    const MATRICULA_EVOLUCAO = 2;
    const EVOLUCAO_APROVEITAMENTO = 3;
    const MATERIAL_ENTREGA = 4;
    const VENDA_SITUACAO = 5;
    const EVOLUCAO_VENDA = 6;
    const VENDA_LANCAMENTOS = 7;
    const VENDA_MANUAL = 8;
    const MATRICULA_MANUAL = 9;
    const MATERIAL_MANUAL = 10;
    const MATRICULA_TRANSFERENCIA = 11;
    const OCORRENCIA_AUTOMATICO = 12;
    const OCORRENCIA_MANUAL = 13;
    const AGENDAMENTO_SITUACAO = 14;
    const MATRICULA_TCC = 15;
    const DOCUMENTACAO = 16;
    const GERACAO_CONTRATO = 17;
    const ADD_PRODUTO_VENDA = 18;
    const REMOVENDO_PRODUTO_VENDA = 19;
    const ALTERANDO_ATENDENTE_VENDA = 20;
    const ALTERANDO_LANCAMENTO = 21;
    const MATRICULA_AUTOMATICO = 22;
    const AGENDAMENTO_LANCAMENTO_FREQUENCIA = 23;
    const CARTEIRINHA = 24;
    const ALTERANDO_DISCIPLINA_GRADE = 27;
    const MATRICULA_TRANCAMENTO_DISCIPLINA = 28;
    const MATRICULA_TRANSFERENCIA_TURMA = 29;
    const MATRICULA_COLACAO_GRAU = 30;
    const SALADEAULA_SITUACAO_TCC = 31;
    const VENDA_AUTOMATICA = 33;
    
    const VENDA_TERMO_RESPONSABILIDADE = 34;
    const MATRICULA_TERMO_RESPONSABILIDADE = 35;
}

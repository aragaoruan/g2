<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de telefone
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoTelefone
{

    const Residencial = 1;
    const Comercial = 2;
    const Celular = 3;

}

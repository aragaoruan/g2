<?php

namespace G2\Constante;

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2015-11-20
 */

class TipoSalaDeAula {

    const PERMANENTE = 1;
    const PERIODICA = 2;

}
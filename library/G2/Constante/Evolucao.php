<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de evolucao
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Evolucao
{

    /* Constantes tb_ocorrencia */
    const AGUARDANDO_ATENDIMENTO = 28;
    const SENDO_ATENDIDO = 29;
    const AGUARDANDO_INTERESSADO = 30;
    const AGUARDANDDO_ATENDENTE = 31;
    const INTERESSADO_INTERAGIU = 32;
    const AGUARDANDO_DOCUMENTACAO = 33;
    const AGUARDANDO_PAGAMENTO = 34;
    const AGUARDANDO_OUTROS_SETORES = 35;
    const ENCERRAMENTO_REALIZADO_INTERESSADO = 36;
    const REABERTURA_REALIZADA_INTERESSADO = 37;
    const ENCERRAMENTO_REALIZADO_ATENDENTE = 38;
    const DEVOLVIDO_DISTRIBUICAO = 39;
    const PENDENCIA_SOLUCIONADA = 87;
    const PAGAMENTO_REALIZADO = 88;

    /* Constantes tb_venda */
    const TB_VENDA_AGUARDANDO_NEGOCIACAO = 7;
    const TB_VENDA_CANCELADA = 8;
    const TB_VENDA_AGUARDANDO_RECEBIMENTO = 9;
    const TB_VENDA_CONFIRMADA = 10;
    const TB_VENDA_NEGOCIADA = 17;
    const TB_VENDA_EM_ANDAMENTO = 18;
    const TB_VENDA_EM_PROCESSAMENTO = 44;

    /* Constantes tb_venda */
    const TB_ENVIODESTINATARIO_NAO_LIDA = 42;
    const TB_ENVIODESTINATARIO_LIDA = 43;

    /* Constantes tb_enviomensagem */
    const TB_ENVIOMENSAGEM_ENVIADO = 22;
    const TB_ENVIOMENSAGEM_NAO_ENVIADO = 23;
    const TB_ENVIOMENSAGEM_ERRO_AO_ENVIAR = 24;
    const TB_ENVIOMENSAGEM_NAO_LIDA = 42;
    const TB_ENVIOMENSAGEM_LIDA = 43;

    /*Constantes tb_motivocancelamento*/
    const TB_MOTIVOCANCELANENTO_NORMAL = 48;
    const TB_MOTIVOCANCELANENTO_PEDIDO_CANCELAMENTO = 49;
    const TB_MOTIVOCANCELANENTO_PROCESSO_CANCELAMENTO = 50;
    const TB_MOTIVOCANCELANENTO_FINALIZADO = 51;
    const TB_MOTIVOCANCELANENTO_DESISTIU = 52;


    /**
     * @return array
     */

    /*Constantes para tb_matricula*/
    const TB_MATRICULA_AGUARDANDOCONFIRMACAO = 5;
    const TB_MATRICULA_CURSANDO = 6;
    const TB_MATRICULA_CERTIFICADO = 16;
    const TB_MATRICULA_CONCLUINTE = 15;
    const TB_MATRICULA_TRANSFERIDO = 20;
    const TB_MATRICULA_TRANCADO = 21;
    const TB_MATRICULA_SEM_RENOVACAO = 25;
    const TB_MATRICULA_RETORNADO = 26;
    const TB_MATRICULA_CANCELADO = 27;
    const TB_MATRICULA_BLOQUEADO = 40;
    const TB_MATRICULA_ANULADO = 41;
    const TB_MATRICULA_TRANSFERIDO_DE_ENTIDADE = 47;
    const TB_MATRICULA_DECURSO_DE_PRAZO = 69;
    const TB_MATRICULA_FORMADO = 81;
    const TB_MATRICULA_DIPLOMADO = 82;

    //Evolucoes para o campo evolucao certificado
    const TB_MATRICULA_CERT_NAO_APTO = 60;
    const TB_MATRICULA_CERT_APTO_GERAR = 61;
    const TB_MATRICULA_CERT_GERADO = 62;
    const TB_MATRICULA_CERT_ENVIADO_CERTIFICADORA = 63;
    const TB_MATRICULA_CERT_RETORNADO_CERTIFICADORA = 64;
    const TB_MATRICULA_CERT_ENVIADO_ALUNO = 65;


    /*Constantes para tb_turma*/
    const TB_TURMA_PREVISTO = 45;
    const TB_TURMA_CONFIRMADO = 46;
    const TB_TURMA_FINALIZADA = 53;
    const TB_TURMA_CANCELADA = 54;

    /*Constantes para tb_contrato*/
    const TB_CONTRATO_AGUARDANDOCONFIRMACAO = 1;
    const TB_CONTRATO_CANCELADO = 2;
    const TB_CONTRATO_AGUARDANDOATIVACAO = 3;
    const TB_CONTRATO_CONFIRMADO = 4;

    //Constantes para tb_matriculadisciplina
    const TB_MATRICULADISCIPLINA_NAO_ALOCADO = 11;
    const TB_MATRICULADISCIPLINA_APROVADO = 12;
    const TB_MATRICULADISCIPLINA_CURSANDO = 13;
    const TB_MATRICULADISCIPLINA_INSATISFATORIO = 19;
    const TB_MATRICULADISCIPLINA_TRANCADA = 70;

    //Constantes para tb_minhapasta
    const TB_MINHAPASTA_ADICIONOU = 66;
    const TB_MINHAPASTA_INATIVO = 67;


    //Evolucoes MATRICULA DIPLOMACAO
    const TB_MATRICULA_DIPLO_APTO_DIPLOMAR = 71;
    const TB_MATRICULA_DIPLO_DIPLOMA_GERADO = 72;
    const TB_MATRICULA_DIPLO_ENVIADO_ASSINATURA = 73;
    const TB_MATRICULA_DIPLO_RETORNADO_ASSINATURA = 74;
    const TB_MATRICULA_DIPLO_ENVIADO_SECRETARIA = 75;
    const TB_MATRICULA_DIPLO_ENVIADO_REGISTRO = 76;
    const TB_MATRICULA_DIPLO_RETORNADO_REGISTRO = 77;
    const TB_MATRICULA_DIPLO_ENVIADO_POLO_ALUNO = 78;
    const TB_MATRICULA_DIPLO_ENTREGUE_ALUNO = 79;
    const TB_MATRICULA_DIPLO_HISTORICO_GERADO = 80;
    const TB_MATRICULA_DIPLO_EQUIVALENCIA_GERADO = 83;
    const TB_MATRICULA_DIPLO_SEGUNDA_VIA_DIPLOMA_GERADO = 84;
    const TB_MATRICULA_DIPLO_SEGUNDA_VIA_HISTORICO_GERADO = 85;
    const TB_MATRICULA_DIPLO_SEGUNDA_VIA_EQUIVALENCIA_GERADO = 86;

    /**
     * @return array
     */
    public static function getArrayOcorrencia()
    {
        return array(
            Evolucao::AGUARDANDO_ATENDIMENTO => 'Aguardando Atendimento',
            Evolucao::SENDO_ATENDIDO => 'Sendo Atendido',
            Evolucao::AGUARDANDO_INTERESSADO => 'Aguardando Interessado',
            Evolucao::AGUARDANDDO_ATENDENTE => 'Aguardando Atendente',
            Evolucao::INTERESSADO_INTERAGIU => 'Interessado Interagiu',
            Evolucao::AGUARDANDO_DOCUMENTACAO => 'Aguardando Documentação',
            Evolucao::AGUARDANDO_PAGAMENTO => 'Aguardando Pagamento',
            Evolucao::AGUARDANDO_OUTROS_SETORES => 'Aguardando Outros Setores',
            Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO => 'Encerramento realizado pelo Interessado',
            Evolucao::REABERTURA_REALIZADA_INTERESSADO => 'Reabertura realizada pelo Interessado',
            Evolucao::ENCERRAMENTO_REALIZADO_ATENDENTE => 'Encerramento realizado pelo Atendente',
            Evolucao::DEVOLVIDO_DISTRIBUICAO => 'Devolvido para distribuição',
            Evolucao::PENDENCIA_SOLUCIONADA => 'Pendência Solucionada',
            Evolucao::PAGAMENTO_REALIZADO => 'Pagamento Realizado'
        );
    }

    /**
     * @return array
     */
    public static function getArrayVenda()
    {

        return array(
            Evolucao::TB_VENDA_AGUARDANDO_NEGOCIACAO => 'Aguardando Negociação',
            Evolucao::TB_VENDA_CANCELADA => 'Venda Cancelada',
            Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO => 'Aguardando Recebimento',
            Evolucao::TB_VENDA_CONFIRMADA => 'Confirmada',
            Evolucao::TB_VENDA_NEGOCIADA => 'Negociada',
            Evolucao::TB_VENDA_EM_ANDAMENTO => 'Em Andamento',
            Evolucao::TB_VENDA_EM_PROCESSAMENTO => 'Em Processamento'

        );
    }

    /**
     * @return array
     */
    public static function getArrayEnvioMensagem()
    {
        return array(
            self::TB_ENVIOMENSAGEM_ENVIADO => 'Enviado',
            self::TB_ENVIOMENSAGEM_NAO_ENVIADO => 'Não Enviado',
            self::TB_ENVIOMENSAGEM_ERRO_AO_ENVIAR => 'Erro ao Enviar',
            self::TB_ENVIOMENSAGEM_NAO_LIDA => 'Não Lida',
            self::TB_ENVIOMENSAGEM_LIDA => 'Lida',
        );
    }

    /**
     * @return array
     */
    public static function getArrayCertificado()
    {
        return array(
            Evolucao::TB_MATRICULA_CERT_NAO_APTO => 'Não Apto',
            Evolucao::TB_MATRICULA_CERT_APTO_GERAR => 'Apto a Gerar Certificado',
            Evolucao::TB_MATRICULA_CERT_GERADO => 'Certificado Gerado',
            Evolucao::TB_MATRICULA_CERT_ENVIADO_CERTIFICADORA => 'Enviado para Assinatura',
            Evolucao::TB_MATRICULA_CERT_RETORNADO_CERTIFICADORA => 'Retornado da Assinatura',
            Evolucao::TB_MATRICULA_CERT_ENVIADO_ALUNO => 'Enviado ao Aluno',
        );
    }

    /**
     * @return array
     */
    public static function getArrayMatricula()
    {
        return array(
            Evolucao::TB_MATRICULA_AGUARDANDOCONFIRMACAO => 'Aguardando Confirmação',
            Evolucao::TB_MATRICULA_CURSANDO => 'Cursando',
            Evolucao::TB_MATRICULA_CERTIFICADO => 'Certificado',
            Evolucao::TB_MATRICULA_CONCLUINTE => 'Concluinte',
            Evolucao::TB_MATRICULA_TRANSFERIDO => 'Transferido',
            Evolucao::TB_MATRICULA_TRANCADO => 'Trancado',
            Evolucao::TB_MATRICULA_SEM_RENOVACAO => 'Sem Renovação',
            Evolucao::TB_MATRICULA_RETORNADO => 'Retornado',
            Evolucao::TB_MATRICULA_CANCELADO => 'Cancelado',
            Evolucao::TB_MATRICULA_BLOQUEADO => 'Bloqueado',
            Evolucao::TB_MATRICULA_ANULADO => 'Anulado',
            Evolucao::TB_MATRICULA_TRANSFERIDO_DE_ENTIDADE => 'Transferido de Entidade'
        );
    }


    /**
     * @return array
     */
    public static function getArraySituacoesMatriculasAtivas()
    {
        return array(
            self::TB_MATRICULA_CURSANDO,
            self::TB_MATRICULA_BLOQUEADO,
            self::TB_MATRICULA_CONCLUINTE,
            self::TB_MATRICULA_TRANCADO
        );
    }


    /**
     * @return array
     */
    public static function getArrayDiplomacao()
    {
        return array(
            Evolucao::TB_MATRICULA_DIPLO_APTO_DIPLOMAR => 'Apto a Diplomar',
            Evolucao::TB_MATRICULA_DIPLO_DIPLOMA_GERADO => 'Diploma Gerado',
            Evolucao::TB_MATRICULA_DIPLO_ENVIADO_ASSINATURA => 'Enviado para Assinatura',
            Evolucao::TB_MATRICULA_DIPLO_RETORNADO_ASSINATURA => 'Retorno da Assinatura',
            Evolucao::TB_MATRICULA_DIPLO_ENVIADO_SECRETARIA => 'Enviado para Secretaria Geral',
            Evolucao::TB_MATRICULA_DIPLO_ENVIADO_REGISTRO => 'Enviado para Registro',
            Evolucao::TB_MATRICULA_DIPLO_RETORNADO_REGISTRO => 'Retornado do Registro',
            Evolucao::TB_MATRICULA_DIPLO_ENVIADO_POLO_ALUNO => 'Enviado ao Polo do Aluno',
            Evolucao::TB_MATRICULA_DIPLO_ENTREGUE_ALUNO => 'Entregue ao Aluno',
            Evolucao::TB_MATRICULA_DIPLO_HISTORICO_GERADO => 'Histórico Gerado',
            Evolucao::TB_MATRICULA_DIPLO_EQUIVALENCIA_GERADO => 'Quadro de Equivalência Gerado',
            Evolucao::TB_MATRICULA_DIPLO_SEGUNDA_VIA_DIPLOMA_GERADO => '2° Via do Diploma Gerado',
            Evolucao::TB_MATRICULA_DIPLO_SEGUNDA_VIA_HISTORICO_GERADO => '2° Via do Histórico Gerado',
            Evolucao::TB_MATRICULA_DIPLO_SEGUNDA_VIA_EQUIVALENCIA_GERADO => '2° Via do Quadro de Equivalência Gerado'
        );
    }
}

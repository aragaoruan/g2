<?php
/**
 * Classe contendo variaveis e retornos estaticos com tipos de situacao
 *
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Constante;


class Lancamento
{
    const PAGAMENTO_QUITADO = 1;
    const PAGAMENTO_NAO_QUITADO = 0;
}
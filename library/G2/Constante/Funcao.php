<?php

namespace G2\Constante;

/**
 * Classe contendo constantes de Funcoes
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Funcao {

    const ATENDENTE_CM = 1;
    const GERENTE = 2;
    const DISTRIBUIDOR = 3;
    const PROTOCOLADOR = 4;
    const REPRESENTANTE = 5;
    const PROFESSOR = 6;
    const COORDENADOR = 7;
    const ATENDENTE = 8;
    const RESPONSAVEL = 9;
    const ATENDENTE_SETOR = 10;

}

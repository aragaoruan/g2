<?php
/**
 * Classe contendo id das forma de Pagamento Aplicação
 *
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Constante;


class FormaPagamentoAplicacao
{
    const VENDA_INTERNA = 1;
    const VENDA_SITE = 2;
    const PRE_VENDA_INTERNA = 3;
    const PRE_VENDA_SITE = 4;
    const VENDA_CICLICA = 5;
    const VENDA_AUTOMATICA = 6;
}
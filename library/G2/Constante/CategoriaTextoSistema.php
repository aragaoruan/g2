<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 20/04/18
 * Time: 11:36
 */

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com exibições dos textos do sistema fixos
 *
 */
class CategoriaTextoSistema
{
    const MATRICULA = 1;
    const CONTRATO = 2;
    const DECLARACAO = 3;
    const CERTIFICADO = 4;
    const DADOS_PESSOAIS = 5;
    const VENDA = 6;
    const OCORRENCIA = 7;
    const ASSUNTO = 8;
    const PRIMEIRO_ACESSO = 9;
    const AGENDAMENTO = 10;
    const ENTIDADE_LOJA = 11;
    const PLANO_PAGAMENTO = 12;
    const RESPOSTA_CARTAO_CREDITO = 14;
    const CABECALHO = 15;
    const RODAPE = 16;


}

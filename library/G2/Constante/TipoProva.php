<?php

namespace G2\Constante;

class TipoProva
{
    const PROJETO_PEDAGOGICO = 1;
    const SALA_DE_AULA = 2;

    public static function getArray()
    {
        return array(
            self::PROJETO_PEDAGOGICO => 'Prova por Projeto Pedagógico',
            self::SALA_DE_AULA       => 'Prova por Sala de Aula'
        );
    }
}
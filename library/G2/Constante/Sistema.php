<?php

namespace G2\Constante;

class Sistema
{

    const GESTOR = 1;
    const ACTOR = 2;
    const FLUXUS = 3;
    const PORTAL_ALUNO = 4;
    const SISTEMA_AVALIACAO = 5;
    const MOODLE = 6;

    // @history GII-7241
    // CARTAO e BRASPAG iguais para não quebrar funcionalidades que já usam o alias CARTAO
    const CARTAO = 7;
    const BRASPAG = 7;

    const LOCAWEB = 8;
    const WORDPRESS_ECOMMERCE = 9;

    // @history GII-7241
    // CARTAO e BRASPAG iguais para não quebrar funcionalidades que já usam o alias CARTAO
    const CARTAO_RECORRENTE = 10;
    const BRASPAG_RECORRENTE = 10;

    const AMAIS = 11;
    const HENRY7 = 12;
    const LEYA_BOOKSTORE = 13;
    const BLACKBOARD = 15;
    const BRASPAG_LANCAMENTO = 19;
    const PONTOSOFT = 20;
    const HUBSPOT = 22;
    const PEARSON = 23;
    const FLUXUS_WS = 24;
    const FACEBOOK = 26;
    const GOOGLE = 27;
    const LOCASMS = 25;
    const JIRA  = 28;
    const ALUMNUS = 29;
    const PAGARME = 30;
    const BRASPAG_NOVO_RECORRENTE_CONSULTA = 31;
    const MAXIMIZE = 32;
    const HIBOT = 33;

    public static function arraySistemas($key = NULL){

        $array = array(
            \G2\Constante\Sistema::GESTOR => 'Gestor 2',
            \G2\Constante\Sistema::ACTOR => 'Actor',
            \G2\Constante\Sistema::FLUXUS => 'Fluxus',
            \G2\Constante\Sistema::PORTAL_ALUNO => 'Portal do Aluno',
            \G2\Constante\Sistema::SISTEMA_AVALIACAO => 'Sistema de Avaliação',
            \G2\Constante\Sistema::MOODLE => 'Moodle',
            \G2\Constante\Sistema::BRASPAG => 'Braspag',
            \G2\Constante\Sistema::LOCAWEB => 'Locaweb',
            \G2\Constante\Sistema::WORDPRESS_ECOMMERCE => 'Wordpress Ecommerce',
            \G2\Constante\Sistema::BRASPAG_RECORRENTE => 'Braspag Recorrente',
            \G2\Constante\Sistema::AMAIS => 'A+',
            \G2\Constante\Sistema::HENRY7 => 'Henry7',
            \G2\Constante\Sistema::LEYA_BOOKSTORE => 'LEYA Bookstore',
            \G2\Constante\Sistema::BLACKBOARD => 'Blackboard',
            \G2\Constante\Sistema::BRASPAG_LANCAMENTO => 'Braspag Lançamento',
            \G2\Constante\Sistema::PONTOSOFT => 'Ponto Soft',
            \G2\Constante\Sistema::HUBSPOT => 'Hubspot',
            \G2\Constante\Sistema::PEARSON => 'Pearson',
            \G2\Constante\Sistema::FLUXUS_WS => 'Fluxus WS',
            \G2\Constante\Sistema::FACEBOOK => 'Facebook',
            \G2\Constante\Sistema::GOOGLE => 'Google',
            \G2\Constante\Sistema::LOCASMS => 'LocaSMS',
            \G2\Constante\Sistema::JIRA => 'Jira',
            \G2\Constante\Sistema::MAXIMIZE => 'Maximize - Fábrica de Provas',
            \G2\Constante\Sistema::HIBOT => 'HiBot'
        );

        if ($key){
            return $array[$key];
        }

        return $array;
    }
}

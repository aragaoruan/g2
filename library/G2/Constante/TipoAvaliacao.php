<?php

namespace G2\Constante;

/**
 * Classe de constantes para tb_tipoavaliacao
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class TipoAvaliacao {

    const ATIVIDADE_PRESENCIAL = 1;
    const AVALIACAO_RECUPERACAO = 2;
    const AVALIACAO_SIMULADA = 3;
    const AVALIACAO_PRESENCIAL = 4;
    const ATIVIDADE_EAD = 5;
    const TCC = 6;
    const CONCEITUAL = 7;

    public static function getArray(){
        return array(
            self::ATIVIDADE_PRESENCIAL => 'Atividades Presenciais',
            self::AVALIACAO_RECUPERACAO => 'Avliação de Recuperação',
            self::AVALIACAO_SIMULADA => 'Avaliação Simulada',
            self::AVALIACAO_PRESENCIAL => 'Avaliação Presencial',
            self::ATIVIDADE_EAD => 'Atividades à Distância',
            self::TCC => 'TCC',
            self::CONCEITUAL => 'Conceitual',
        );
    }

}

<?php
/**
 * Classe contendo constantes com as possíveis strings
 * retornadas no campo st_status da vw_gradenota.
 *
 * User: Marcus Pereira
 * Date: 14/03/2016
 * Time: 15:24
 */

namespace G2\Constante;

class StatusGradeNota
{
    const CREDITO_CONCEDIDO = 'Crédito Concedido';
    const SATISFATORIO = 'Satisfatório';
    const INSATISFATORIO = 'Insatisfatório';
    const APROVEITAMENTO_DE_DISCIPLINA = 'Aproveitamento de Disciplina';
    const NAO_ENCERRADO = 'Não encerrado';
    const SEM_STATUS = '-';
}
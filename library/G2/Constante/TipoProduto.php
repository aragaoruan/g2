<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de produtos
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class TipoProduto {

    const PROJETO_PEDAGOGICO = 1;
    const TAXA = 2;
    const DECLARACOES = 3;
    const MATERIAL = 4;
    const AVALIACAO = 5;
    const LIVRO = 6;
    const COMBO = 7;
    const PRR = 8;
    
    public static function getArray(){
        return array(
            TipoProduto::PROJETO_PEDAGOGICO => 'Projeto Pedagógico',
            TipoProduto::TAXA => 'Taxa',
            TipoProduto::DECLARACOES => 'Declarações',
            TipoProduto::MATERIAL => 'Material',
            TipoProduto::AVALIACAO => 'Avaliação',
            TipoProduto::LIVRO => 'Livro',
            TipoProduto::COMBO => 'Combo',
            TipoProduto::PRR => 'PRR',
        );
    }

}
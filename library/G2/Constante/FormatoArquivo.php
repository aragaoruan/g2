<?php
/**
 * Created by PhpStorm.
 * User: reinaldo.pereira
 * Date: 18/06/2018
 * Time: 10:19
 *
 * Classe contendo os formatos de arquivo.
 */

namespace G2\Constante;

class FormatoArquivo
{
    const PDF = 1;
    const ZIP = 2;
}

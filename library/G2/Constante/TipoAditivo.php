<?php

namespace G2\Constante;

/**
 * Constantes para tipos de aditivos
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class TipoAditivo
{

    const RENOVACAO = 1;
    const TRANSFERENCIA = 2;
}
<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de aula
 * @author helder Silva <hlelder.silva@unyleya.com.br>
 */
class TipoAula
{

    const REGULAR = 1;
    const MONITORIA = 2;
    const TUTORIA= 3;

}

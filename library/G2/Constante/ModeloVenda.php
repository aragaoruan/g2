<?php

namespace G2\Constante;

/**
 * Classe contendo constantes de ModeloVenda (tb_modelovenda)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class ModeloVenda {

    const PADRAO = 1;
    const ASSINATURA = 2;
    const CREDITO = 3;

    public static function getArray(){
        return array(
            self::PADRAO => 'Padrão',
            self::ASSINATURA => 'Assinatura',
            self::CREDITO => 'Crédito'
        );
    }
}

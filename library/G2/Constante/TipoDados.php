<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de dados
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoDados
{

    const PESSOAS = 1;
    const LOGIN = 2;
    const HIERARQUIA_ENTIDADES = 3;

    public static function getArrayTipoDados($id = null)
    {
        $array = array(
            self::PESSOAS => 'Pessoas',
            self::LOGIN => 'Login',
            self::HIERARQUIA_ENTIDADES => 'Hierarquia Entidades'
        );
        if ($id == null)
            return $array;
        else
            return $array[$id];
    }

}

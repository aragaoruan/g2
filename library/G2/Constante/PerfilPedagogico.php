<?php

namespace G2\Constante;

class PerfilPedagogico {
    const PROFESSOR = 1;
    const COORDENADOR_DE_PROJETO = 2;
    const OBSERVADOR_INSTITUCIONAL = 3;
    const COORDENADOR_DE_DICIPLINA = 4;
    const ALUNO = 5;
    const ALUNO_INSTITUCIONAL = 6;
    const SUPORTE = 7;
    const PROFESSOR_AUTOR_LIVRO = 8;
    const TITULAR_DE_CERTIFICACAO = 9;
}
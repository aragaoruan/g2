<?php

namespace G2\Constante;

/**
 * Class IssueTypes
 * @package G2\Constante
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class IssueTypes
{
    const ERROS = 1;
    const NOVO_RECURSO = 2;
    const TAREFA = 3;
    const MELHORIA = 4;
    const SUBTAREFA = 5;
    const EPIC = 6;
    const STORY = 7;
    const TAREFA_8 = 8;
    const CONTEUDO = 10000;
    const SITE = 10001;
    const DEMANDA = 10100;
    const ERRO = 10101;
    const ALTERACAO_DE_PECA_EXISTENTE = 10102;
    const MELHORIA_10103 = 10103;
    const NOVA_FUNCIONALIDADE = 10104;
    const TAREFA_10105 = 10105;
    const SUBTAREFA_10107 = 10107;
    const CONTEUDO_COMPLETO = 10108;
    const CONTEUDO_PDF = 10109;
    const CONTEUDO_HTML = 10110;
    const VIDEO = 10111;
    const SALA_DE_AULA_MOODLE = 10112;
    const SALA_DE_AULA_BLACKBOARD = 10113;
    const REVISAO_DE_PROVA = 10114;
    const CONTEUDO_PDF_REFORMULACAO = 10200;
    const CONTEUDO_HTML_REFORMULACAO = 10201;
    const SOLICITACAO = 10302;
    const SUBTAREFA_10300 = 10300;
    const PROBLEMA = 10301;
    const REUNIAO = 10303;
    const DOCUMENTACAO = 10304;
    const INCIDENTE = 10305;
    const MUDANCA = 10306;
    const MUDANCA_EMERGENCIAL = 10307;
    const BANCO_DE_DADOS = 10308;
    const TECNICA = 10309;
    const ALERTA = 10310;


    /**
     * Retorna o nome do tipo por ID
     * @param $id
     * @return mixed
     */
    public static function getIssueType($id)
    {
        $arrTypes = self::getArrayIssueTypes();
        return $arrTypes[$id];
    }

    /**
     * Retorna o array com todos os tipos de issue
     * @return array
     */
    public static function getArrayIssueTypes()
    {
        return array(
            1 => "Erros",
            2 => "Novo Recurso",
            3 => "Tarefa",
            4 => "Melhoria",
            5 => "Subtarefa",
            6 => "Epic",
            7 => "Story",
            8 => "Tarefa",
            10000 => "Conteúdo",
            10001 => "Site",
            10100 => "Demanda",
            10101 => "Erro",
            10102 => "Alteração de peça existente",
            10103 => "Melhoria",
            10104 => "Nova funcionalidade",
            10105 => "Tarefa",
            10107 => "Subtarefa",
            10108 => "Conteúdo Completo",
            10109 => "Conteúdo PDF",
            10110 => "Conteúdo HTML",
            10111 => "Vídeo",
            10112 => "Sala de Aula Moodle",
            10113 => "Sala de Aula Blackboard",
            10114 => "Revisão de Prova",
            10200 => "Conteúdo PDF - Reformulação",
            10201 => "Conteúdo HTML - Reformulação",
            10300 => "Sub-tarefa",
            10301 => "Problema",
            10302 => "Solicitação",
            10303 => "Reunião",
            10304 => "Documentação",
            10305 => "Incidente",
            10306 => "Mudança",
            10307 => "Mudança Emergencial",
            10308 => "Banco de Dados",
            10309 => "Técnica",
            10310 => "Alerta",

        );
    }
}
<?php
/**
 * Classe contendo id das forma de disponibilização
 *
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Constante;


class FormaDisponibilizacao
{
    const EnvioPelosCorreios = 1;
    const BuscaNaInstituição = 2;
    const EmissaoOnline = 3;
}
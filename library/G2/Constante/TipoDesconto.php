<?php

namespace G2\Constante;


/**
 * Classe contendo variaveis e retornos estaticos com tipos de desconto
 *
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoDesconto
{
    const VALOR = 1;
    const PORCENTAGEM = 2;

}
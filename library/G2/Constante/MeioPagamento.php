<?php

namespace G2\Constante;

/**
 * Classe contendo constantes de Meio de Pagamentos
 *
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MeioPagamento
{

    const CARTAO_CREDITO = 1;
    const BOLETO = 2;
    const DINHEIRO = 3;
    const CHEQUE = 4;
    const EMPENHO = 5;
    const DEPOSITO_BANCARIO = 6;
    const CARTAO_DEBITO = 7;
    const TRANSFERENCIA = 8;
    const PERMUTA = 9;
    const BOLSA = 10;
    const CARTAO_RECORRENTE = 11;
    const ESTORNO_CARTAO = 12;
    const CARTA_CREDITO = 13;
    const CREDIARIO_CARTAO_CREDITO_VISA = 14;
    const CREDIARIO_CARTAO_CREDITO_MASTER_CARD = 15;

    public static function getArray()
    {
        return array(
            \G2\Constante\MeioPagamento::CARTAO_CREDITO => 'Cartão de Crédito',
            \G2\Constante\MeioPagamento::BOLETO => 'Boleto',
            \G2\Constante\MeioPagamento::DINHEIRO => 'Dinheiro',
            \G2\Constante\MeioPagamento::CHEQUE => 'Cheque',
            \G2\Constante\MeioPagamento::EMPENHO => 'Empenho',
            \G2\Constante\MeioPagamento::DEPOSITO_BANCARIO => 'Depósito Bancário',
            \G2\Constante\MeioPagamento::CARTAO_DEBITO => 'Cartão de Débito',
            \G2\Constante\MeioPagamento::TRANSFERENCIA => 'Transferência',
            \G2\Constante\MeioPagamento::PERMUTA => 'Permuta',
            \G2\Constante\MeioPagamento::BOLSA => 'Bolsa',
            \G2\Constante\MeioPagamento::CARTAO_RECORRENTE => 'Cartão Recorrente',
            \G2\Constante\MeioPagamento::ESTORNO_CARTAO => 'Estorno de Cartão',
            \G2\Constante\MeioPagamento::CARTA_CREDITO => 'Carta de Crédito',
            \G2\Constante\MeioPagamento::CREDIARIO_CARTAO_CREDITO_VISA => 'Crediário Cartão Crédito - Visa',
            \G2\Constante\MeioPagamento::CREDIARIO_CARTAO_CREDITO_MASTER_CARD => 'Crediário Cartão Crédito - Master Card',
        );
    }

    public static function getMeiosPagamentoPrrArray()
    {
        return array(
            \G2\Constante\MeioPagamento::CARTAO_CREDITO => 'Cartão de Crédito',
            \G2\Constante\MeioPagamento::BOLETO => 'Boleto',
        );
    }

    public static function getMeioPagamento($id_meiopagamento)
    {
        $meios = self::getArray();
        return $meios[$id_meiopagamento];
    }

    /**
     * Retorna todos os meios de pagamento que teoricamente são de cartão
     * @return array
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public static function getMeioPagamentosCartao()
    {
        return array(
            self::CARTAO_CREDITO,
            self::CARTAO_DEBITO,
            self::CARTAO_RECORRENTE,
//            self::CREDIARIO_CARTAO_CREDITO_VISA,
//            self::CREDIARIO_CARTAO_CREDITO_MASTER_CARD,
        );
    }

    public static function getMeiosPagamentoTransferenciaArray()
    {
        return array(
            \G2\Constante\MeioPagamento::BOLETO => 'Boleto',
            \G2\Constante\MeioPagamento::CARTAO_CREDITO => 'Cartão de Crédito',
            \G2\Constante\MeioPagamento::CARTAO_DEBITO => 'Cartão de Débito',
            \G2\Constante\MeioPagamento::CHEQUE => 'Cheque',
        );
    }
}

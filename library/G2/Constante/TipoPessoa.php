<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de pessoa
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class TipoPessoa {

    const FISICA = 1;
    const JURIDICA = 2;

    public static function getArrayTipoPessoa($idTipoPessoa = null)
    {
        $array =  array(
            1 => 'Pessoa Física',
            2 => 'Pessoa Jurídica'
            );
        if($idTipoPessoa == null)
                return $array;
        else
            return $array[$idTipoPessoa];
    }

}

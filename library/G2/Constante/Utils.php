<?php
/**
 * Classe contendo constantes utils para utilizacao no G2
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

namespace G2\Constante;

class Utils
{

    public static $minExtenseMonths = array(
        1 => 'jan',
        2 => 'fev',
        3 => 'mar',
        4 => 'abr',
        5 => 'mai',
        6 => 'jun',
        7 => 'jul',
        8 => 'ago',
        9 => 'set',
        10 => 'out',
        11 => 'nov',
        12 => 'dez');

    public static $extenseMonths = array(
        1 => 'janeiro',
        2 => 'fevereiro',
        3 => 'março',
        4 => 'abril',
        5 => 'maio',
        6 => 'junho',
        7 => 'julho',
        8 => 'agosto',
        9 => 'setembro',
        10 => 'outubro',
        11 => 'novembro',
        12 => 'dezembro'
    );

    public static $tipoImagem = array(
        'image/jpeg' => 'jpg',
        'image/pjpeg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
    );

    /**
     * Constantes para os tipos de avisos que serão de apoio para o portal e app
     */
    const TIPO_AVISO_MENSAGEM   = "mensagem";
    const TIPO_AVISO_ENVIO_TCC  = "envioTcc";
    const TIPO_AVISO_AGENDAMENTO = 'agendamento';

    public static $tinYMceHtmlTagsAllowed =
        '<p><a><ul><li><ol><i><b><strong><i><br><span><strong><em><h1><h2><h3><h4><h5><img><table><tr><td><th><tbody><thead>';

    static public function getMinMonthsExtense($numberMonth)
    {
        if ($numberMonth) {
            return \G2\Constante\Utils::$minExtenseMonths[$numberMonth];
        } else {
            return self::$minExtenseMonths;
        }
    }

    static public function getMonthsExtense($numberMonth)
    {
        if ($numberMonth) {
            return \G2\Constante\Utils::$extenseMonths[$numberMonth];
        } else {
            return self::$extenseMonths;
        }
    }

    /**
     * @param $cpf
     * @return mixed
     */
    public static function limpaCpfCnpj($cpf)
    {
        $chars = array('.', '-', '/', ' ');
        $cpf_final = str_replace($chars, '', $cpf);

        if (strlen($cpf_final) == 10)
            $cpf_final = '0' . $cpf_final;

        return $cpf_final;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Denise
 * Date: 17/03/2016
 * Time: 16:18
 */

namespace G2\Constante;

/**
 * Classe contendo constantes de Esquemas de Configuracao
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class EsquemaConfiguracao
{

    const Graduacao_AVM = 1;
    const Pos_Graduacao = 2;
    const IMP = 3;
    const TdC = 4;
    const Defaul_t = 5;
    const Pos_Presencial = 6;
    const Pos_Graduacao_UCAM = 7;
    const Corporativo = 8;
    const Graduacao = 9;
    const Faculdade_Pos_Graduacao = 10;

    // Atualizando nomes das constantes
    const GRADUACAO = 1;
    const POS_GRADUACAO = 2;
    const TDC = 4;
    const PADRAO = 5;
    const POS_PRESENCIAL = 6;
    const POS_GRADUACAO_UCAM = 7;
    const CORPORATIVO = 8;
    const ESCOLA_TECNICA_SEMESTRAL = 9;
    const ESCOLA_TECNICA_PADRAO_POS = 10;

}

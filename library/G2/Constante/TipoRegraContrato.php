<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de regras de contrato
 *
 * @author Helder Sivla <helder.silva@unyleya.com.br>
 */
class TipoRegraContrato {

    const TB_TIPO_REGRA_CONTRATO_NORMAL = 1;
    const TB_TIPO_REGRA_CONTRATO_FREE_PASS= 2;

}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipo de aditivos da venda
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class VendaAditivo
{
    const RENOVACAO = 1;
    const TRANSFERENCIA = 2;
}


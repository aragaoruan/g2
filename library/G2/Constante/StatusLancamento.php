<?php

namespace G2\Constante;


/**
 * Class StatusLancamento
 * @package G2\Constante
 * Classe com as constantes para definir o status dos lançamentos para o portal e para o app
 */
class StatusLancamento
{
    const PAGO = 1;
    const A_PAGAR = 2;
    const ATRASADO = 3;

    const PROCESSING = 'processing';
    const AUTHORIZED = 'authorized';
    const PAID = 'paid';
    const REFUNDED = 'refunded';
    const WAITIN_PAYMENT = 'waiting_payment';
    const PEDING_REFUND = 'pending_refund';
    const REFUSED = 'refused';

    public static function getArrayStatusLancamento()
    {
        return array(
            self::PAGO => "Pago",
            self::A_PAGAR => "A pagar",
            self::ATRASADO => "Atrasado"
        );
    }


    public static function getStatusLancamento($status)
    {
        $arr = self::getArrayStatusLancamento();
        return $arr[$status];
    }
}
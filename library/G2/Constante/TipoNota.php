<?php

namespace G2\Constante;

class TipoNota
{
    const NORMAL = 1;
    const APROVEITAMENTO = 2;

    public static function getArray()
    {
        return array(
            self::NORMAL         => 'Normal',
            self::APROVEITAMENTO => 'Crédito Parcial'
        );
    }
}
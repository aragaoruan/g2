<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de regras de pagamentos
 *
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class TipoRegraPagamento {

    const ENTIDADE = 1;
    const AREA_CONHECIMENTO = 2;
    const PROJETO_PEDAGOGICO = 3;
    const PROFESSOR = 4;
    const PROFESSOR_PROJETO = 5;

}

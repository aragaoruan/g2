<?php

namespace G2\Constante;

class CartaoBandeira
{
    const VISA = 1;
    const MASTERCARD = 2;
    const AMEX = 3;
    const ELO = 4;
    const DINERS = 5;
}

<?php

namespace G2\Constante;

/**
 * tb_modelocarteirinha
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

class ModeloCarteirinha
{

    const PLATINUM = 1;
    const OUTROS = 2;
    const GRADUACAO = 3;

    public static function getArray()
    {
        return array(
            self::PLATINUM  => 'Platinum',
            self::OUTROS    => 'Outros',
            self::GRADUACAO => 'Graduação'
        );
    }

}

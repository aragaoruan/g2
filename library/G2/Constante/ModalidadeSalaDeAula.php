<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos da tabela tb_modadelidadesaladeaula
 * @author Denise Xavier
 */
class ModalidadeSalaDeAula{

    const PRESENCIAL = 1;
    const DISTANCIA = 2;

}

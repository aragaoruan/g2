<?php

namespace G2\Constante;

/**
 * Classe contendo constates de Tipos de Lancamento
 * 
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class TipoLancamento {
    
    const CREDITO = 1;
    const LANCAMENTO = 2;
    
    public static function getArray(){
        return array(
            \G2\Constante\TipoLancamento::CREDITO => 'Crédito',
            \G2\Constante\TipoLancamento::LANCAMENTO => 'Lancamento',
        );
    }
}

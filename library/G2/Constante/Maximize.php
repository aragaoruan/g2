<?php
/**
 * Constantes para valores padrões do webservice MAXIMIZE
 * @author Denise Xavier <denise.xavier07@gmail.com>
 */

namespace G2\Constante;


class Maximize
{
    /** Listagem de provas */
    const ENDPOINT_LISTA_PROVAS = '/c/prova/listar';

    /** Cadastro de salas */
    const ENDPOINT_CADASTRA_SALA = '/c/sala/salvar';

    /** Salvar usuário */
    const ENDPOINT_SALVA_USUARIO = '/c/usuario/salvar';

    /** Agendamento de Salas */
    const ENDPOINT_AGENDAR_SALA_APLICACAO = '/c/aplicacao/agendar-sala';

    /** Gerar a chave de acesso */
    const ENDPOINT_GERAR_CHAVE_ACESSO = '/c/login/gerarChaveDeAcesso?codigoAluno=';

    /** Autenticar usuário */
    const ENDPOINT_AUTENTICAR_USUARIO = '/c/login/autenticar?chaveDeAcesso=';

}

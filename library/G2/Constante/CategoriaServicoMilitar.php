<?php

namespace G2\Constante;

/**
 * Classe contendo constantes de Categoria do servico militar
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class CategoriaServicoMilitar
{

    const SERVIU_EXERCITO_MARINHA_AERONAUTICA = 1;
    const SERVIU_TIRO_DE_GUERRA = 2;
    const DISPENSADO = 3;


    public static function getArray()
    {
        return array(
            self::SERVIU_EXERCITO_MARINHA_AERONAUTICA => 'SERVIU EXERCITO, MARINHA OU AERONAUTICA',
            self::SERVIU_TIRO_DE_GUERRA => 'TIRO DE GUERRA',
            self::DISPENSADO => 'DISPENSADO',
        );
    }

}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de campanhas
 *
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class FinalidadeCampanha
{

    const VENDA = 1;
    const TRANSFERÊNCIA_CURSO = 2;

}
<?php

namespace G2\Constante;

/**
 * Classe de constantes para tb_tipoavaliacao
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class TipoDisciplina {

    const PADRAO = 1;
    const TCC = 2;
    const AMBIENTACAO = 3;

    public static function getArray(){
        return array(
            self::PADRAO => 'Tipo padrão de Disciplina',
            self::TCC => 'Disciplina de trabalho de conclusão de curso',
            self::AMBIENTACAO => 'Sala de Aula de Ambientação',
        );
    }

}
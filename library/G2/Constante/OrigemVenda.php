<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de origem de vendas
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class OrigemVenda {

    const CONTRATO = 1;
    const SITE = 2;
    const INTERNO = 3;

    public static function getArray() {
        return array(
            OrigemVenda::CONTRATO => 'Contrato',
            OrigemVenda::SITE => 'Site',
            OrigemVenda::INTERNO => 'Interno'
        );
    }

}
<?php

namespace G2\Constante;

class Permissao
{

    const MATRICULA_EVOLUCAO_BLOQUEAR = 14;
    const MATRICULA_EVOLUCAO_TRANCAR = 15;
    const MATRICULA_EVOLUCAO_CANCELAR = 16;
    const MATRICULA_EVOLUCAO_ANULAR = 17;
    const MATRICULA_EVOLUCAO_TRANSFERIR_ENTIDADE = 18;
    const MATRICULA_EVOLUCAO_LIBERAR_ACESSO = 19;
    const MATRICULA_EVOLUCAO_DECURSO_DE_PRAZO = 37;

    /**
     * Retorna todas as permissões cadastradas
     * @return array
     */
    public static function getArray()
    {
        return array(
            self::MATRICULA_EVOLUCAO_BLOQUEAR            => 'Bloquear',
            self::MATRICULA_EVOLUCAO_TRANCAR             => 'Trancar',
            self::MATRICULA_EVOLUCAO_CANCELAR            => 'Cancelar',
            self::MATRICULA_EVOLUCAO_ANULAR              => 'Anular',
            self::MATRICULA_EVOLUCAO_TRANSFERIR_ENTIDADE => 'Transferir de Entidade',
            self::MATRICULA_EVOLUCAO_LIBERAR_ACESSO      => 'Liberar Acesso',
            self::MATRICULA_EVOLUCAO_DECURSO_DE_PRAZO    => 'Decurso de Prazo'
        );
    }

    /**
     * Retorna somente as permissões relacionadas à evolução da matricula
     * @return array
     */
    public static function getArrayMatriculaEvolucao()
    {
        return array(
            self::MATRICULA_EVOLUCAO_BLOQUEAR            => 'Bloquear',
            self::MATRICULA_EVOLUCAO_TRANCAR             => 'Trancar',
            self::MATRICULA_EVOLUCAO_CANCELAR            => 'Cancelar',
            self::MATRICULA_EVOLUCAO_ANULAR              => 'Anular',
            self::MATRICULA_EVOLUCAO_TRANSFERIR_ENTIDADE => 'Transferir de Entidade',
            self::MATRICULA_EVOLUCAO_LIBERAR_ACESSO      => 'Liberar Acesso',
            self::MATRICULA_EVOLUCAO_DECURSO_DE_PRAZO    => 'Decurso de Prazo'
        );
    }
}
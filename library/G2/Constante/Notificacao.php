<?php
/**
 * Created by PhpStorm.
 * User: UNYLEYA
 * Date: 21/05/2015
 * Time: 15:06
 */
/**
 * Classe contendo constantes de Notificacoes
 *
 * @author Débora Castro
 */

namespace G2\Constante;

class Notificacao {

    const NOTIFICACAO_SALAS_LIMITE_EXCEDIDO = 1;
    const CONTRATO_SEM_MATRICULA_VINCULADA = 2;

}

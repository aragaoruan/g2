<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com categorias dos textos sistemas
 *
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class TextoCategoria {

    const MATRICULA = 1;
    const CONTRATO = 2;
    const DECLARACOES = 3;
    const CERTIFICADO = 4;
    const DADOS_PESSOAIS = 5;
    const VENDA = 6;
    const OCORRENCIAS = 7;
    const ASSUNTO = 8;
    const PRIMEIRO_ACESSO = 9;
    const AGENDAMENTO = 10;
    const ENTIDADE_LOJA = 11;
    const PLANO_PAGAMENTO = 12;

    const CABECALHO = 15;
    const RODAPE = 16;

}

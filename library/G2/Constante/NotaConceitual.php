<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 08/03/18
 * Time: 15:25
 */

namespace G2\Constante;

class NotaConceitual
{
    const NAO_CONCLUIDO = 1;
    const CONCLUIDO = 2;
}

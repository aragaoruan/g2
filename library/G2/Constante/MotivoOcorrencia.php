<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com motivos de ocorrencia
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MotivoOcorrencia {

    const RESGATE = 1;
    const CANCELAMENTO_TRANCAMENTO = 2;

    public static function getArray()
    {
        return array(
            1 => 'Resgate',
            2 => 'Cancelamento / Trancamento',
        );
    }

}

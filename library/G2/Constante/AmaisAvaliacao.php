<?php
/**
 * Constantes para valores padrões do webservice Amais Avaliações
 *
 * @author    Neemias Santos <neemiassantos.16@gmail.com>
 */

namespace G2\Constante;


class AmaisAvaliacao
{
    /**
     * URL para integração via web services utilizando o padrão SOAP
     */
    const URL = 'http://temposim.amaisavaliacoes.com.br/amais/servico/habilitacaoAluno';

    /**
     * WSDL da interface
     */
    const URLWSDL = 'http://temposim.amaisavaliacoes.com.br/amais/servico/habilitacaoAluno?WSDL';
}
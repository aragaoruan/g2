<?php

namespace G2\Constante;

/**
 * Classe contendo constantes de Meio de Pagamentos da Graduação
 * @author Denise Xavier <denie.xavier@unyleya.com.br>
 */
class MeioPagamentoGraduacao
{

    const CARTAO_CREDITO = 1;
    const BOLETO = 2;


    public static function getArray()
    {
        return array(
            \G2\Constante\MeioPagamento::CARTAO_CREDITO => 'Cartão de Crédito',
            \G2\Constante\MeioPagamento::BOLETO => 'Boleto',
        );
    }

    public static function getMeioPagamento($id_meiopagamento){
        $meios = self::getArray();
        return $meios[$id_meiopagamento];
    }

}

<?php

namespace G2\Constante;

class PagarmePaymentMethod {

    const CREDIT_CARD = 'credit_card';
    const BOLETO = 'boleto';

}
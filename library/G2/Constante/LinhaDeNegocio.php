<?php
/**
 * Created by PhpStorm.
 * User: Denise
 * Date: 17/03/2016
 * Time: 16:18
 */

namespace G2\Constante;

/**
 * Classe contendo constantes de Linha De Negocio
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class LinhaDeNegocio
{

    const CONCURSO = 1;
    const GRADUACAO = 2;
    const POS_GRADUACAO = 3;

}
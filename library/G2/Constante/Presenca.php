<?php

namespace G2\Constante;

class Presenca
{
    const AUSENTE = 0;
    const PRESENTE = 1;
}
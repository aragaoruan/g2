<?php

namespace G2\Constante;

class TipoOferta
{

    const PADRAO = 1;
    const ESTENDIDA = 2;

    public static function getArray()
    {
        return array(
            self::PADRAO    => 'Padrão',
            self::ESTENDIDA => 'Estendida'
        );
    }

}
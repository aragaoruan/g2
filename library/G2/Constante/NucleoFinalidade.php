<?php

namespace G2\Constante;


/**
 * Class NucleoFinalidade
 * @package G2\Constante
 */
class NucleoFinalidade
{
    const ACADEMICO = 1;
    const FINANCEIRO = 2;
    const TECNOLOGIA = 3;
    const COMERCIAL = 4;
    const CONTABIL = 5;
}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com exibições dos textos do sistema
 *
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class TextoExibicao {

    const IMPRESSO = 1;
    const EMAIL = 2;
    const SISTEMA = 3;
    const SMS = 4;

}
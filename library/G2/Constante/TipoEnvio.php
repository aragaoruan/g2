<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de envio
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class TipoEnvio {
    
    const EMAIL = 3;
    const HTML = 4;
    const SMS = 5;
    
}

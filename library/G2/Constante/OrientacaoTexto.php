<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com orientacao dos textos sistemas
 *
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class OrientacaoTexto {

    const RETRATO = 1;
    const PAISAGEM = 2;

}

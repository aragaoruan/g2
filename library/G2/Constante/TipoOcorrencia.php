<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de ocorrencia
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class TipoOcorrencia {

    const ALUNO = 1;
    const TUTOR = 2;
    const INTERNO = 3;

    public static function getArrayTipoOcorrencia($idTipoOcorrencia = null)
    {
        $array = array(
            \G2\Constante\TipoOcorrencia::ALUNO => 'Aluno',
            \G2\Constante\TipoOcorrencia::TUTOR => 'Tutor',
            \G2\Constante\TipoOcorrencia::INTERNO => 'Interno'
        );

        if ($idTipoOcorrencia == null) {
            return $array;
        } else {
            return $array[$idTipoOcorrencia];
        }
    }

}

<?php

namespace G2\Constante;

class TipoFuncionalidade {
    const PERFIL_MENU_PRINCIPAL = 1;
    const PERFIL_MENU_LATERAL = 2;
    const PERFIL_SUB_MENU_LATERAL = 3;
    const ABAS = 4;
    const GRUPOS = 5;
    const BOTAO_ADICIONAR_MENU = 6;
    const FUNCIONALIDADE_INTERNA = 7;
    const BOTAO = 8;
    const DASHBOARD = 9;
}
<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos da tabela mensagem padrao
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MensagemPadrao {

    const MATRICULA = 1;
    const DADOS_ACESSO = 2;
    const ENVIO_BOLETO = 3;
    const ENVIO_CARTAO_CREDITO = 4;
    const CONFIRMACAO_VENDA = 5;
    const EMAIL_COBRANCA = 6;
    const ENVIO_EBOOK = 7;
    const LIBERACAO_PUBLICACAO_TCC = 8;
    const RECIBO_TCC = 9;
    const SMS = 11;
    const AGENDAMENTO = 12;
    const ALERTA_IMPOSTO_RENDA = 13;
    const ALERTA_HORARIO = 14;
    const ALERTA_MANUTENCAO = 15;
    const ALERTA_REGRESSO = 16;
    const AVISO_VENCIMENTO_CARTAO_RECORRENTE = 25;
    const AGENDAMENTO_LIBERADO_FINAL = 33;
    const AGENDAMENTO_LIBERADO_RECUPERACAO = 34;
    const EMAIL_RENOVACAO_MATRICULA = 38;
    const ENVIO_CARTAO_CREDITO_ACORDO = 40;
    const LANCAMENTO_NOTA_PROVA_FINAL_POS = 51;
}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com exibições das categorias dos tramites
 *
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class CategoriaTramite
{
    const MATRICULA = 1;
    const MATERIAL = 2;
    const VENDA = 3;
    const OCORRENCIA = 4;
    const AGENDAMENTO = 5;
    const SALA_DE_AULA = 6;
}
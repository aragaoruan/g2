<?php

namespace G2\Constante;

class GrauAcademico
{
    const SUPERIOR_DE_TECNOLOGIA = 1;
    const BACHARELADO = 2;
    const LICENCIATURA = 3;
    const ESPECIALIZACAO = 4;
    const MESTRADO = 5;
    const DOUTORADO = 6;
}
<?php

namespace G2\Constante;

/**
 * Constantes para tabela situacoes AvaliacaoAgendamento
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AvaliacaoAgendamento
{

    const PRESENCA_PRESENTE = 178;
    const PRESENCA_AUSENTE = 179;
}
<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de endereco
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoEndereco
{

    const Residencial = 1;
    const Comercial = 2;
    const Matriz = 3;
    const Filial = 4;
    const Correspondência = 5;
    const Aplicador_de_prova = 6;


}

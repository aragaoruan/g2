<?php
/**
 * Constantes para valores padrões do webservice Amais Avaliações
 *
 * @author    Neemias Santos <neemiassantos.16@gmail.com>
 */

namespace G2\Constante;


class BraspagV2
{
    /**
     * URL da API da Braspag para consultas
     */
    const URL_API_BRASPAG_QUERY = 'https://apiquery.braspag.com.br/v2/';

    /**
     * URL da API da Braspag para transações
     */
    const URL_API_BRASPAG = 'https://api.braspag.com.br/v2/';
}

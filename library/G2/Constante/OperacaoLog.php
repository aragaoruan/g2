<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com as opera��es do log
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class OperacaoLog
{
    const UPDATE = 1;
    const INSERT = 2;
    const DELETE = 3;

    /**
     * @return array
     */
    public static function getArrayOperacao()
    {
        return array(
            self::UPDATE => 'update',
            self::INSERT => 'insert',
            self::DELETE => 'delete'
        );
    }

    /**
     * Retorna o nome da opera��o
     * @param $operacao
     * @return string
     */
    public static function getNomeOperacao($operacao)
    {
        $arrOperacao = self::getArrayOperacao();
        return $arrOperacao[$operacao];
    }
}
<?php

namespace G2\Constante;

/**
 * Classe
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class MensagemSistema
{

    const FORM_CAMPO_OBRIGATORIO = 'Campo obrigatório';
    const SALVAR_SUCESSO = 'Dado(s) salvo(s) com sucesso';
    const SALVAR_ERRO = 'Erro ao tentar salvar os dados';
    const SALVAR_ALERTA = 'Dados salvos com pendencias';
    const OCORRENCIA_ALTERACAO_SITUACAO_TEXTO = 'situação da ocorrencia antes [%s] foi alterada para [%s] ';
    const OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO = 'evolução da ocorrencia antes [%s] foi alterada para [%s] ';

    const OCORRENCIA_ALTERACAO_ASSUNTO_ENCAMINHAR = ' encaminhada do assunto [%s] e subassunto [%s] para assunto [%s] e subassunto [%s], do atendente [%s] para o atendente [%s]';
    const OCORRENCIA_CRIACAO_TEXTO = "Ocorrencias criadas com sucesso!";

    const SEM_ENTIDADE_SESSAO = 'Não existe entidade da sessão, faça o login e tente novamente!';


    const ERRO_CRIAR_LANCAMENTO = 'Não foi possível criar os lançamentos!';
    const SUCESSO_RESETAR_TRANSACAO_FINANCEIRA = 'Venda e transação financeira resetadas com sucesso.';
    const ERRO_RESETAR_TRANSACAO_FINANCEIRA = 'Não foi possível resetar a venda e a transação financeira.';

    const INFORME_ID_VENDA = 'Informe o id_venda para realizar o pagamento.';
    const VENDA_NAO_ENCONTRADA = 'Venda não encontrada.';

    const PAGAMENTO_NAO_AUTORIZADO = 'Pagamento não Autorizado!';
    const PAGAMENTO_AUTORIZADO = 'Pagamento efetuado com sucesso!';

    const ERRO_PRODUTO_CARTAO_RECORRENTE = 'Prezado usuário, para realizar pagamento com o cartão recorrente é necessário que o produto seja uma assinatura.';

    const LANCAMENTO_NAO_ENCONTRADO = 'Lançamento não encontrado.';
    const INFORME_PARAMETRO_ID = "Parâmetro Id não informado.";

    const IMPOSSIVEL_RECUPERAR_FUNCIONALIDADES = 'Não foi possível recuperar as funcionalidades.';
    const IMPOSSIVEL_RECUPERAR_FUNCIONALIDADE_PAI = 'Não foi possível recuperar a funcionalidade pai.';
    const SELECIONE_FUNC_PAI_CAD_FILHO = 'Selecione a funcionalidade pai para cadastrar a funcionalidade filha!';
    const IMPOSSIVEL_REMOVER_FUNC = 'Não foi possível remover as funcionalidades devido a um erro no banco de dados!';
    const NAO_ARRAY = 'O Parametro Recebido não é um Array!';
    const FUNC_SALVAS = 'Funcionalidades Salvas com Sucesso!';
    const NAO_OBJ_FUNCTO = 'O Objeto não é uma Instância da Classe FuncionalidadeTO!';
    const ERRO_SALVAR_FUNC = 'Erro ao Salvar Funcionalidades!';

    const SEM_MATRICULA = 'Matricula não foi encontrada!';


    /*
     * Constantes de mensagens para o método de log do esquema de configuração
     */
    const ID_ESQUEMA_VAZIO = "ID do esquema de configuração vazio.";

}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com tipos de campanhas
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoCampanha {

    const VENDAS = 1;
    const PRODUTOS = 2;
    const PONTUALIDADE = 3;

}
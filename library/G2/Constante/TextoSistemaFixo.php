<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com exibições dos textos do sistema fixos
 *
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class TextoSistemaFixo
{

    const HISTORICO_ESCOLAR_DEFINITIVO = 1487;
    const DIPLOMA_GRADUACAO = 1488;

}

<?php

namespace G2\Constante;

/**
 * Classe contendo variaveis e retornos estaticos com exibições dos textos do sistema
 *
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class CategoriaCampanha
{

    const CUPOM = 1;
    const PROMOCAO = 2;
    const PRAZO = 3;
    const PREMIO = 4;
}
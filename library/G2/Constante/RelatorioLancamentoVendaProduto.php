<?php

namespace G2\Constante;

class RelatorioLancamentoVendaProduto
{
    const RELATORIO_LANCAMENTO_VENDA_PRODUTO = 1;
    const RELATORIO_LANCAMENTO_VENDA_PRODUTO_POR_ATENDENTE = 2;
}
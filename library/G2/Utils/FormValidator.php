<?php

namespace G2\Utils;

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-07-26
 */
class FormValidator
{

    /**
     * @param string $str
     * @param int $min
     * @param int $max
     * @return bool
     */
    public static function tamanho($str, $min = 0, $max = INF)
    {
        $length = strlen($str);
        return $length >= $min && $length <= $max;
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function cpf($str)
    {
        // Remover todos os caracteres NÃO-NUMÉRICOS
        $str = \G2\Utils\Helper::unmask($str, '\D');

        // Verificar o tamanho
        if (!self::tamanho($str, 11, 11)) {
            return false;
        }

        // Verificar se foi informada uma sequência de digitos repetidos. Ex: 11111111111
        if (preg_match('/(\d)\1{10}/', $str)) {
            return false;
        }

        // Fazer o cálculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $str{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($str{$c} != $d) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function cnpj($str)
    {
        // Remover todos os caracteres NÃO-NUMÉRICOS
        $str = \G2\Utils\Helper::unmask($str, '\D');

        // Verificar o tamanho
        if (!self::tamanho($str, 14, 14)) {
            return false;
        }

        // Verificar se foi informada uma sequência de digitos repetidos. Ex: 11111111111
        if (preg_match('/(\d)\1{13}/', $str)) {
            return false;
        }

        // Validar primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $str{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($str{12} != ($resto < 2 ? 0 : 11 - $resto)) {
            return false;
        }

        // Validar segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $str{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $str{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function email($str)
    {
        return filter_var($str, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function url($str)
    {
        return filter_var($str, FILTER_VALIDATE_URL);
    }

    /**
     * @param string $str
     * @return bool
     */
    public static function cep($str)
    {
        // Remover todos os caracteres NÃO-NUMÉRICOS
        $str = \G2\Utils\Helper::unmask($str, '\D');

        // Verificar o tamanho
        return self::tamanho($str, 8, 8);
    }

    /**
     * @param string $str
     * @param int $digits
     * @return bool
     */
    public static function telefone($str, $digits = 8)
    {
        // Remover todos os caracteres NÃO-NUMÉRICOS
        $str = \G2\Utils\Helper::unmask($str, '\D');

        // Verificar o tamanho
        return self::tamanho($str, $digits, $digits);
    }

    /**
     * @param string $str
     * @param int $length
     * @return bool
     */
    public static function nomeSobrenome($str, $length = 2)
    {
        $str = trim($str);

        // Verificar o tamanho mínimo
        if (!self::tamanho($str, 6)) {
            return false;
        }

        // Devidir a string em array
        $arr = array_filter(explode(' ', $str));

        // A quantidade de nomes deve ser maior ou igual ao $length
        return count($arr) >= $length;
    }
}
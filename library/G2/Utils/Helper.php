<?php

namespace G2\Utils;

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2017-07-26
 */
class Helper
{

    /**
     * @param string $str
     * @param string $mask
     * @return string
     */
    public static function mask($str, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($str[$k]))
                    $maskared .= $str[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * @param string $str
     * @param string $replace
     * @return string
     */
    public static function unmask($str, $replace = '\D')
    {
        return preg_replace("/" . $replace . "/", '', $str);
    }

    /**
     * @description Converte uma data recebida (seja string, DateTime ou Zend_Date) em um formato específico (String), ou retornando o objeto DateTime / Zend_Date
     * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
     * @date 2016-02-02
     * @param string|\DateTime|\Zend_Date $dt
     * @param string $type_or_format (Opcoes: String(formato) / DateTime / Zend_Date)
     * @return string|\DateTime|\Zend_Date
     */
    public static function converterData($dt, $type_or_format = 'Y-m-d')
    {
        if (!$dt) {
            return $dt;
        }

        // Garante que o $dt a principio seja uma STRING no formato Y-m-d
        if ($dt instanceof \DateTime) {
            $dt = $dt->format('Y-m-d H:i:s');
        } else if ($dt instanceof \Zend_Date) {
            $dt = $dt->toString('YYYY-MM-dd HH:mm:ss');
        } else {
            if (!empty($dt['date'])) {
                $dt = $dt['date'];
            }
            $date = substr($dt, 0, 10);
            $time = substr($dt, 11);
            $date = implode('-', array_reverse(explode('/', $date)));
            $dt = trim($date . ' ' . $time);
        }

        switch (strtolower($type_or_format)) {
            case 'datetime' :
                $dt = new \DateTime($dt);
                break;
            case 'zend_date' :
                $dt = new \Zend_Date($dt, \Zend_Date::ISO_8601);
                break;
            default:
                $dt = date($type_or_format, strtotime($dt));
                break;
        }

        return $dt;
    }

    /**
     * Convert float number to date (H:i)
     * @param float|int $value
     * @return string
     */
    public static function convertNumberToHours($value)
    {
        return sprintf('%02d:%02d', (int)$value, fmod($value, 1) * 60);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function removeSpecialChars($str)
    {
        // Ter certeza que será uma string
        $str = ($str ?: '') . '';

        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => ' ', // Literally a single quote
            '/[“”«»„]/u' => ' ', // Double quote
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );

        return preg_replace(array_keys($utf8), array_values($utf8), $str);
    }

}

<?php

namespace G2\Utils;

class FiltroImportacao implements \PHPExcel_Reader_IReadFilter
{
    private $_startColumn;
    private $_endColumn;
    private $_startRow;
    private $_endRow;

    public function __construct($startColumn, $endColumn, $startRow = 1, $endRow = 1000)
    {
        $this->_startColumn = $startColumn;
        $this->_endColumn = $endColumn;
        $this->_startRow = $startRow;
        $this->_endRow = $endRow;
    }


    public function readCell($column, $row, $worksheetName = '')
    {
        if ($row >= $this->_startRow && $row <= $this->_endRow) {
            if (in_array(\PHPExcel_Cell::columnIndexFromString($column), range($this->_startColumn, $this->_endColumn))) {
                return true;
            }
        }
        return false;
    }
}

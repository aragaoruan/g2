<?php

namespace G2\Utils;

/**
 * Classe criada para controlar os dados do Usuario usado no Token Rest
 * @SWG\Definition(required={"user_id", "user_name", "login"}, @SWG\Xml(name="User"))
 * @author Elcio Guimarães
 */
class User
{

    /**
     * @var integer - ID do Usuário
     */
    private $user_id;

    /**
     * @var string - Nome completo do Usuário
     */
    private $user_name;

    /**
     * @var string - URL do avatar
     */
    private $user_avatar;

    /**
     * @var string - E-mail do usuário
     */
    private $user_email;

    /**
     * @var string - Cpf do usuário
     */
    private $user_cpf;


    /**
     * @var string - Nome de usuário
     */
    private $login;

    /**
     * @var integer - ID da Entidade
     */
    private $entidade_id;

    /**
     * @var integer - ID do usuário que está logando como aluno
     */
    private $id_usuariooriginal = null;

    /**
     * @var string - Nome do usuário que está logando como aluno
     */
    private $st_usuariooriginal = null;

    /**
     * @var boolean - Informa se ta sendo acessado como aluno ou não
     */
    private $bl_acessarcomo = false;

    /**
     * @return string
     */
    public function getst_usuariooriginal()
    {
        return $this->st_usuariooriginal;
    }

    /**
     * @param string $st_usuariooriginal
     */
    public function setst_usuariooriginal($st_usuariooriginal)
    {
        $this->st_usuariooriginal = $st_usuariooriginal;
        return $this;

    }



    /**
     * @return int
     */
    public function getid_usuariooriginal()
    {
        return $this->id_usuariooriginal;
    }

    /**
     * @param int $id_usuariooriginal
     */
    public function setid_usuariooriginal($id_usuariooriginal)
    {
        $this->id_usuariooriginal = $id_usuariooriginal;
        return $this;

    }

    /**
     * @return boolean
     */
    public function getbl_acessarcomo()
    {
        return $this->bl_acessarcomo;
    }

    /**
     * @param boolean $bl_acessarcomo
     */
    public function setbl_acessarcomo($bl_acessarcomo)
    {
        $this->bl_acessarcomo = $bl_acessarcomo;
        return $this;

    }

    /**
     * @return int
     */
    public function getuser_id()
    {
        return $this->user_id;
    }


    /**
     * @param int $user_id
     * @return $this
     */
    public function setuser_id($user_id)
    {
        $this->user_id = $user_id;
        return $this;

    }

    /**
     * @return string
     */
    public function getuser_name()
    {
        return $this->user_name;
    }

    /**
     * @param string $user_name
     * @return $this
     */
    public function setuser_name($user_name)
    {
        $this->user_name = $user_name;
        return $this;

    }

    /**
     * @return string
     */
    public function getuser_avatar()
    {
        return $this->user_avatar;
    }

    /**
     * @param string $user_avatar
     * @return $this
     */
    public function setuser_avatar($user_avatar)
    {
        $this->user_avatar = $user_avatar;
        return $this;

    }

    /**
     * @return string
     */
    public function getlogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return $this
     */
    public function setlogin($login)
    {
        $this->login = $login;
        return $this;

    }

    /**
     * @param $entidade_id
     * @return $this
     */
    public function setentidade_id($entidade_id)
    {
        $this->entidade_id = $entidade_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getentidade_id()
    {
        return $this->entidade_id;
    }



    /**
     * @return string
     */
    public function getuser_email()
    {
        return $this->user_email;
    }

    /**
     * @param string $user_email
     */
    public function setuser_email($user_email)
    {
        $this->user_email = $user_email;
        return $this;

    }

    /**
     * @return string
     */
    public function getuser_cpf()
    {
        return $this->user_cpf;
    }

    /**
     * @param string $user_cpf
     */
    public function setuser_cpf($user_cpf)
    {
        $this->user_cpf = $user_cpf;
        return $this;

    }

    /**
     * Conevrte para Array
     * @return array
     */
    public function toArray()
    {
        $array = array();
        $vars = get_class_vars(get_class($this));
        foreach ($vars as $key => $var) {
            $method = 'get' . $key;
            $array[$key] = $this->$method();
        }
        return $array;
    }

}

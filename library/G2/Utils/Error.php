<?php

namespace G2\Utils;

/**
 * @SWG\Definition(required={"type", "message"}, @SWG\Xml(name="Error"))
 * @author Elcio Guimarães
 */
class Error
{

    const WARNING   = 'warning';
    const ERROR     = 'error';

    /**
     * @SWG\Property(format="string", example="error")
     * @var string - Tipo do Erro, se espera error ou warning
     */
    private $type = self::ERROR;


    /**
     * @SWG\Property(format="string", example="Sucesso")
     * @var string - String com o título
     */
    private $title = 'Sucesso';

    /**
     * @SWG\Property(format="string", example="Ocorreu um erro na requisição")
     * @var string - String com as mensagem do erro
     */
    private $message = "Ocorreu um erro na requisição";


    /**
     * @SWG\Property(format="integer",example="404")
     * @var integer - Status HTTP 400, 404, 201 etc
     */
    private $status;


    /**
     * Padroniza as mensagens de erro rest
     * @param string $type
     * @param string $message
     * @param int $status
     */
    public function __construct($type=self::ERROR, $message='' ,$status=400){

        if($type)       $this->settype($type);
        if($message)    $this->setmessage($message);
        if($status)     $this->setstatus($status);

    }


    /**
     * @return string
     */
    public function gettype()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function settype($type)
    {
        $this->type = $type;

        switch ($type) {
            case self::ERROR:
                $this->settitle('Erro');
                break;
            case self::WARNING:
                $this->settitle('Atenção');
                break;
            default:
                $this->title = 'Sucesso';
        }

        return $this;

    }

    /**
     * @return string
     */
    public function getmessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setmessage($message)
    {
        $this->message = $message;
        return $this;

    }

    /**
     * @return int
     */
    public function getstatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setstatus($status)
    {
        $this->status = $status;
        return $this;

    }

    /**
     * @return string
     */
    public function gettitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function settitle($title)
    {
        $this->title = $title;
        return $this;

    }



    /**
     * Conevrte para Array
     * @return array
     */
    public function toArray(){
        return array(
            'type'=>$this->gettype(),
            'title' => $this->gettitle(),
            'message'=>$this->getmessage(),
            'status'=>$this->getstatus()
        );
    }


}

<?php

namespace G2\Utils;

use Ead1\Doctrine\EntitySerializer;
use G2\Negocio\Perfil;

/**
 * Class LogSistema para fazer as verificaoes nas entities e montar os dados para gerar criar o job para criar o log
 * @package G2\Utils
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-08-04
 */
final class LogSistema
{

    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    /**
     * @var mixed
     */
    private $entity, $entityName, $metas, $changes;


    public function __construct()
    {
        //pega as instancias do doctrine e do bootstrap
        $bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->em = $bootstrap->getResource('doctrine')->getEntityManager();
    }


    /**
     * Verifica se a entity tem o annontation @EntityLog, isso indica que e para armazenar log dessa entity
     * @return bool
     */
    private function verificaEntityLog()
    {
        $ref = new \ReflectionClass($this->entityName);
        if (strstr($ref->getDocComment(), '@EntityLog')) {//verifica se tem esse annotation
            return true;
        } else {
            return false;
        }
    }

    /**
     * Seta a entity com os novos dados
     * @param $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        //seta alguns parametros para reutilizacao
        $this->entityName = get_class($entity);
        $this->metas = $this->em->getClassMetadata($this->entityName);
        $this->entity = $entity;

        if ($this->verificaEntityLog()) {
            $this->setChanges($entity);
        }
        return $this;
    }

    /**
     * Retorna o nome da tabela da classe
     * @return string
     */
    private function getTableName()
    {
        return $this->metas->getTableName();
    }

    /**
     * Retorna o nome da chave primaria
     * @return mixed
     */
    private function getPkName()
    {
        $identifierFields = $this->metas->getIdentifier();
        if (count($identifierFields) > 0 && count($identifierFields) == 1) {
            return $identifierFields[0];
        } else {
            return $identifierFields;
        }


    }

    /**
     * identifica as alteracoes
     * @param $entity
     */
    private function setChanges($entity)
    {
        $uow = $this->em->getUnitOfWork();
        $uow->computeChangeSets();
        $changes = $uow->getEntityChangeSet($entity);
        $pkName = $this->getPkName();//busca o nome das chaves primarias

        //verifica se é uma array
        if (is_array($pkName)) {
            $arrValues = null;//inicia uma variavel com valores nulo para verificar depois
            //percorre as chaves primarias
            foreach ($pkName as $name) {
                //pega a string do nome do método getNomemetodo ou getNome_metodo
                $strGet = $this->getNomeMetodo($this->entity, $name, 'get');
                //verifica se os nomes são um relacionamento
                if (array_key_exists($name, $this->metas->getAssociationMappings())) {
                    //serializa os relacionamentos e percorre os valores
                    foreach ($this->toArray($this->entity->{$strGet}()) as $key => $value) {
                        //verifica se é o campo
                        if ($name == $key) {
                            $arrValues .= $value;//concatena os valores
                        }
                    }

                } else {//senão for um relacionamento concatena os valores
                    $arrValues .= $this->entity->{$strGet}();
                }
            }

            //verifica se os valores concatenados são null e seta como novo registro
            if (is_null($arrValues)) {
                $this->changes = null;
            } else {//senão seta como alteração

                $this->changes = $changes;
            }

        } else {
            $strGet = $this->getNomeMetodo($this->entity, $pkName, 'get');
            if (is_null($this->entity->{$strGet}())) {
                $this->changes = null;
            } else {
                $this->changes = $changes;
            }
        }
    }

    /**
     * Monta o nome do método get
     * @param $entity
     * @param $nome
     * @return mixed|string
     */
    private function getNomeMetodo($entity, $nome, $prefix)
    {
        //monta uma string com o nome do metodo da chave primaria
        $strGet = $prefix . ucfirst($nome);
        //verifica se o método não existe, pois há classes que o nome dos get's não seguem o padrão de getPrefixo_Sufixo()
        if (!method_exists($entity, $strGet)) {
            $strGet = str_replace("_", "", $strGet);//remove o underline montando um método com a string
        }
        return $strGet;
    }

    /**
     * retorna as alteracoes
     * @return mixed
     * @throws \Zend_Exception
     */
    private function getChanges()
    {
        try {

            $arrChanges = array();
            if ($this->changes) {
                $oldEntity = clone $this->entity;
//                $oldEntity = new $this->entityName;
                //percorre o array de alteracoes

                foreach ($this->changes as $key => $row) {
                    //percorre o array com as alteracoes
                    foreach ($row as $i => $dados) {
                        //verifica se e a chave com indice 0 que e o valor antigo do campo
                        if ($i == 0) {
                            //verifica se o indice do array e um relacionamento
                            if (array_key_exists($key, $this->metas->getAssociationMappings())) {
                                if ($dados) {
                                    $oldEntity->{ucfirst($this->getNomeMetodo($oldEntity, $key, 'set'))}($dados);
                                }
                            } else {//seta os valores que nao sao relacionamento
                                if ($dados) {
                                    $oldEntity->{ucfirst($this->getNomeMetodo($oldEntity, $key, 'set'))}($dados);
                                }
                            }
                        }
                    }
                }

                $arrChanges = $this->serializaArrayEntity($this->toArray($oldEntity));
            }
            return $arrChanges;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao identificar as alterações. " . $e->getMessage());
        }
    }

    /**
     *
     * @param array $entityArr
     * @return array
     */
    private function serializaArrayEntity(array $entityArr)
    {
        if ($entityArr) {
            //percorre o array
            foreach ($entityArr as $key => $dados) {
                //verifica se a chave e um relacionamento
                if (array_key_exists($key, $this->metas->getAssociationMappings())) {
                    //percorre o relacionamento
                    if ($dados) {
                        foreach ($dados as $valor) {
                            if ($valor) {//verifica o valor e seta substituindo o valor
                                $entityArr[$key] = $valor;
                            }
                        }
                    }
                } elseif (substr($key, 0, 2) == 'dt' && !($dados instanceof \DateTime) && is_array($dados)) {
                    $entityArr[$key] = $dados['date'];
                }
            }
        }
        return $entityArr;
    }

    /**
     * Retorna um array com os dados que foram salvos na entity
     * @return array
     */
    private function getNewEntityAsArray($entity)
    {
        //transforma a entity em array
        $entityArr = $this->toArray($entity);
        return $this->serializaArrayEntity($entityArr);
    }

    /**
     * @throws \Zend_Exception
     */
    public function criarLog()
    {
//        try {
        if ($this->verificaEntityLog()) {
            $changes = $this->getChanges();
            $newData = $this->getNewEntityAsArray($this->entity);
            $this->criarJob($newData, $changes);
        }
//        } catch (\Exception $e) {
//            throw new \Zend_Exception("Erro ao criar log. " . $e->getMessage());
//        }
    }

    /**
     * @param array $depois
     * @param array $antes
     * @throws \Zend_Exception
     */
    private function criarJob(array $depois, array $antes = [])
    {
        try {
            $sessao = $this->getSessaoDadosGerais();
            $funcionalidadeSessao = new \Zend_Session_Namespace('funcionalidade_atual');

            $paramsLog = array(
                'id_funcionalidade' => $funcionalidadeSessao ? $funcionalidadeSessao->id_funcionalidade : null,
                'id_entidade' => $sessao['id_entidade'],
                'id_usuario' => $sessao['id_usuario'],
                'id_perfil' => $sessao['id_perfil'],
                'st_tabela' => $this->getTableName(),
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'st_pk' => $this->getPkName(),
                'antes' => $antes,
                'depois' => $depois
            );

            //cria o job
            $negocio = new \G2\Negocio\Negocio();
            $schedule_time = date('Y-m-d H:i:s', time() + 10);
            $negocio->createHttpJob(
                \Ead1_Ambiente::geral()->st_url_gestor2 . '/robo/zend-job-log-sistema', $paramsLog, array(
                'name' => 'Log do sistema.',
                'schedule_time' => $schedule_time,
                'queue_name' => 'EntityLog'
            ),
                \G2\Constante\Sistema::GESTOR
            );
        } catch (\Exception $e) {
//            throw new \Zend_Exception("Erro ao tentar criar JOB no servidor. " . $e->getMessage());
//            return true;
        }
    }

    /**
     * Converte a entity para um array
     * @param $entity
     * @return array
     */
    private function toArray($entity)
    {
        $serialize = new EntitySerializer($this->em);
        return $serialize->toArray($entity);
    }


    /**
     * Método para retornar os dados que estão na sessão geral do sistema
     * @return array
     */
    private function getSessaoDadosGerais()
    {

        $sessao = new \Zend_Session_Namespace('geral');
        $sessaoCarrinho = \Ead1_Sessao::getSessaoCarrinho();

        $arrSessao = array(
            'id_entidade' => null,
            'id_usuario' => null,
            'id_perfil' => null
        );
        if ($sessao) {
            if (isset($sessao->id_entidade) && !empty($sessao->id_entidade))
                $arrSessao['id_entidade'] = $sessao->id_entidade;

            if (isset($sessao->id_usuario) && !empty($sessao->id_usuario))
                $arrSessao['id_usuario'] = $sessao->id_usuario;

            if (isset($sessao->id_perfil) && !empty($sessao->id_perfil))
                $arrSessao['id_perfil'] = $sessao->id_perfil;

        }


        //verifica se existe a sessão do carrinho e se na sessão do carrinho existe o id_sistema e se é a loja
        if ($sessaoCarrinho && $sessaoCarrinho->getId_sistema() == \G2\Constante\Sistema::WORDPRESS_ECOMMERCE) {

            //instancia a negocio do perfil
            $perfilNegocio = new Perfil();
            //busca o perfil do aluno no sistema do porta na entidade e como perfil pedagogico aluno
            $perfil = $perfilNegocio->retornarPerfilProfessor(array(
                'id_sistema' => \G2\Constante\Sistema::PORTAL_ALUNO,
                'id_entidade' => $arrSessao['id_entidade'],
                'id_perfilpedagogico' => \PerfilPedagogicoTO::ALUNO,

            ));

            //verifica se retornou dados
            if ($perfil) {
                //se retornou mais de uma posição no array
                if (count($perfil) > 1) {
                    //pega a posição zero do array
                    $arrSessao['id_perfil'] = $perfil[0]->getId_perfil();

                } else {
                    //pega o valor da entity
                    $arrSessao['id_perfil'] = $perfil->getId_perfil();
                }

            } else {
                //se não encontrou nenhum perfil seta esse como padrão
                $arrSessao['id_perfil'] = \PerfilTO::Aluno;
            }
        }


        return $arrSessao;

    }

    /**
     * Escreve o log de erro no arquivo de log do servidor
     * @param $exception
     * @param bool $saveFila
     * @throws \Exception
     */
    public function escreverErroArquivoLog($exception, $saveFile = true)
    {
        try {
            $stringLog = (date('Y-m-d H:i:s') . ' in ' . $exception->getFile() . ':(' . $exception->getLine() . ') - ' . $exception->getMessage());


            if ($this->entity && is_object($this->entity)) {
                $arr = $this->toArray($this->entity);
                $stringLog .= " WITH PARAMS ('";
                $stringLog .= @implode("', '", $arr);
                $stringLog .= "')";
                $stringLog .= " JSON => " . json_encode($arr);
            }

            $stringLog .= "\n\n";

            if ($saveFile) {
                $path = APPLICATION_PATH . '/../docs/logs/';
                $file = 'database.log';

                if (!is_dir($path)) {
                    throw new \Exception("Diretório de log não encontrado, verifique as configurações.");
                }

                if (!file_exists(realpath($path . $file))) {
                    throw  new \Exception("Arquivo de log de banco não encontrado, verifique as configurações.");
                }

                if (@filesize($path . $file) > 1713015) {
                    rename($path . $file, $path . 'database-' . date('d-m-Y-His') . '.log');
                }

                file_put_contents($path . $file, $stringLog, FILE_APPEND);
            }

            self::enviarLogNewRelic($stringLog);

        } catch (\Exception $e) {
            //não gera uma exception pois a ação de salvar os logs não pode interferir no processo
            self::enviarLogNewRelic("Houve um erro ao tentar escrever no arquivo database.log. " . $e->getMessage());
        }
    }

    /**
     * Envia o log para o New Relic
     * @param $trace
     * @throws \Exception
     */
    public static function enviarLogNewRelic($trace)
    {
        try {
            if (!extension_loaded('newrelic')) {
                throw new \Exception("Extensão do NewRelic não encontrada");
            }
            newrelic_capture_params(true);
            newrelic_notice_error($trace);
        } catch (\Exception $e) {
            //não gera uma exception pois a ação de salvar os logs não pode interferir no processo
        }
    }

}

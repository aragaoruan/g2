<?php

namespace G2\Utils;

/**
 * @SWG\Definition(@SWG\Xml(name="Pagination"))
 * @author Elcio Guimarães
 */
class Pagination
{

    /**
     * @SWG\Property(format="integer", example="0")
     * @var int - Quantidade total de regitros encontrados na tabela baseado na consulta atual
     */
    private $total = 0;

    /**
     * @SWG\Property(format="integer", example="0")
     * @var int - Total de registros retornados na página atual
     */
    private $totalThisPage = 0;

    /**
     * @SWG\Property(format="integer", example="0")
     * @var int - Quantidade de registros por página
     */
    private $perPage = 0;

    /**
     * @SWG\Property(format="integer", example="0")
     * @var int - Página atual
     */
    private $page = 0;

    /**
     * @SWG\Property(format="integer", example="0")
     * @var int - Total de páginas encontradas
     */
    private $totalPages = 0;

    /**
     * @var array - Registros
     */
    private $data = array();

    /**
     * @return int
     */
    public function gettotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function settotal($total)
    {
        $this->total = $total;
        return $this;

    }

    /**
     * @return int
     */
    public function gettotalThisPage()
    {
        return $this->totalThisPage;
    }

    /**
     * @param int $totalThisPage
     */
    public function settotalThisPage($totalThisPage)
    {
        $this->totalThisPage = $totalThisPage;
        return $this;

    }

    /**
     * @return int
     */
    public function getperPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setperPage($perPage)
    {
        $this->perPage = $perPage;
        return $this;

    }

    /**
     * @return int
     */
    public function getpage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setpage($page)
    {
        $this->page = $page;
        return $this;

    }

    /**
     * @return int
     */
    public function gettotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @param int $totalPages
     */
    public function settotalPages($totalPages)
    {
        $this->totalPages = $totalPages;
        return $this;

    }

    /**
     * @return array
     */
    public function getdata()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setdata($data)
    {
        $this->data = $data;
        return $this;

    }


    /**
     * Conevrte para Array
     * @return array
     */
    public function toArray(){
        $array = array();
        $vars = get_class_vars(get_class($this));
        foreach($vars as $key => $var){
            $method = 'get'.$key;
            $array[$key] = $this->$method();
        }
        return $array;
    }


}
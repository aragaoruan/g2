<?php

namespace G2\Events;

use G2\G2Entity;
use G2\Negocio\MensagemPortal;
use G2\Negocio\Negocio;
use MensagemBO;

class SituacaoTccChanged extends G2EventManager
{
    private $entity;
    private $negocio;
    private $mensagemBO;

    public function __construct(G2Entity $entity, Negocio $negocio, MensagemBO $mensagemBO)
    {
        parent::__construct();

        $this->entity = $entity;
        $this->negocio = $negocio;
        $this->mensagemBO = $mensagemBO;
    }

    /**
     * Função que processa o evento
     * @param \Zend_EventManager_Event $event
     */
    public function handle(\Zend_EventManager_Event $event)
    {
        if ($this->negocio->situacaoTccHasMensagemPadrao($event->getParam('situacaoTcc')))
            $this->sendEmailNotifications($event);

        if ($this->negocio->situacaoTccHasMensagemPortal($event->getParam('situacaoTcc')))
            $this->sendMessagesNotifications($event, new MensagemPortal);
    }

    /**
     * Retorna a TO pro envio de mensagens de email
     * @param $parametrosDeBusca
     * @return mixed
     */
    private function getData($parametrosDeBusca)
    {
        return $this->negocio->entityToTO(
            $this->negocio->findOneBy($this->entity,$parametrosDeBusca)
        );

    }

    /**
     * Envia as notificações de Email
     * @param \Zend_EventManager_Event $event
     */
    private function sendEmailNotifications(\Zend_EventManager_Event $event)
    {
        if ($vwAlunosSalaTO = $this->getData($event->getParam('parametrosDeBusca'))){
            $vwAlunosSalaTO->setId_situacaotcc($event->getParam('situacaoTcc'));
        }

        $this->mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(
            $this->negocio->getMensagemPadraoSituacaoTcc($event->getParam('situacaoTcc')),
            $vwAlunosSalaTO,
            \TipoEnvioTO::EMAIL,
            true
        );
    }

    /**
     * Envia as notificações do portal
     * @param \Zend_EventManager_Event $event
     * @param MensagemPortal $mensagemPortal
     */
    private function sendMessagesNotifications(\Zend_EventManager_Event $event, MensagemPortal $mensagemPortal)
    {
        $mensagemPortal->salvarMensagemPortal($this->getMensagemPortalData($event->getParams()));
    }

    /**
     * Prepara o Array de dados pro envio da mensagem do portal
     * @param $eventParams
     * @return mixed
     */
    private function getMensagemPortalData($eventParams)
    {
        $data = $this->negocio->getMensagemPortal($eventParams['situacaoTcc']);

        if (isset($eventParams['parametrosDeBusca']['id_matriculadisciplina'])) {
            $idMatriculaDisciplina = $eventParams['parametrosDeBusca']['id_matriculadisciplina'];
        } elseif (isset($eventParams['parametrosDeBusca']['id_alocacao'])) {
            $idMatriculaDisciplina = $this->negocio->findAlocacao($eventParams['parametrosDeBusca']['id_alocacao'])
                                        ->getId_matriculadisciplina();
        } else {
            $idMatriculaDisciplina = null;
        }

        $matriculaDisciplinaEN = $this->negocio->findOneBy('\G2\Entity\MatriculaDisciplina',
        ['id_matriculadisciplina' => $idMatriculaDisciplina]);
        $data['id_matricula'] =   $matriculaDisciplinaEN->getId_matricula();
        $data['dt_enviar'] = (new \DateTime('tomorrow'))->format('Y-m-d H:i:s');
        return $data;
    }
}

<?php

namespace G2\Events;

use G2\Entity\EncerramentoAlocacao;
use G2\Entity\EncerramentoSala;
use G2\Entity\Matricula;
use G2\Entity\SalaDeAula;
use G2\Entity\TipoTramite;
use G2\Entity\Tramite;
use G2\Entity\Usuario;
use G2\G2Entity;
use G2\Negocio\Negocio;
use Zend_EventManager_Event;

class TccDevolvido extends G2EventManager
{
    private $entity;
    private $negocio;

    public function __construct(G2Entity $entity, Negocio $negocio)
    {
        parent::__construct();

        $this->entity = $entity;
        $this->negocio = $negocio;
    }

    protected final function handle(Zend_EventManager_Event $event)
    {
        if ($this->salvarTramite($event->getParams()))
            $this->reverterRepasse($event->getParam('id_encerramentosala'), $event->getParam('id_alocacao'));
    }

    /**
     * Exclui as datas e os Ids do Coordenador e Pedagógico
     * @param $idEncerramentoSala
     * @param $idAlocacao
     * @return mixed
     */
    private function reverterRepasse($idEncerramentoSala, $idAlocacao)
    {

        $encerramentoSala = $this->negocio->findOneBy(EncerramentoSala::class,
            ['id_encerramentosala' => $idEncerramentoSala]);
        $encerramentoSala->setId_usuariocoordenador(null)
                         ->setId_usuariopedagogico(null)
                         ->setDt_encerramentocoordenador(null)
                         ->setDt_encerramentocoordenador(null);
        if ($this->negocio->save($encerramentoSala)) {
            $encerramentoAlocacao = $this->negocio->findOneBy(EncerramentoAlocacao::class,
                ['id_alocacao' => $idAlocacao]);
            if ($encerramentoAlocacao){
                $this->negocio->delete($encerramentoAlocacao);
            }
        }
    }

    private function salvarTramite($params)
    {
        $tipotramiteEN = $this->negocio->findOneBy(TipoTramite::class,
            ['id_tipotramite' => 15]);
        $usuarioEN = $this->negocio->findOneBy(Usuario::class,
            ['id_usuario' => $this->negocio->sessao->id_usuario]);

        $enTramite = new \G2\Entity\Tramite();
        $enTramite->setDt_cadastro(new \DateTime())
                               ->setId_tipotramite($tipotramiteEN)
                               ->setId_usuario($usuarioEN)
                               ->setSt_tramite('TCC Devolvido ao Tutor: ' . $params['motivo-devolucao'])
                               ->setBl_visivel(true);

        if ($this->negocio->save($enTramite))

            return $this->relacionarTramite($enTramite->getId_tramite(), $params['id_saladeaula'],
                $params['id_usuario'], $params['id_matricula']);
    }

    private function relacionarTramite($idTramite, $idSalaDeAula, $idUsuario, $idMatricula)
    {
        $tramiteEN = $this->negocio->findOneBy(Tramite::class, ['id_tramite' => $idTramite]);
        $matriculaEN = $this->negocio->findOneBy(Matricula::class, ['id_matricula' => $idMatricula]);
        $salaDeAulaEN = $this->negocio->findOneBy(SalaDeAula::class, ['id_saladeaula' => $idSalaDeAula]);
        $usuario = $this->negocio->findOneBy(Usuario::class, ['id_usuario' => $idUsuario]);

        $enTramiteMatricula = new \G2\Entity\TramiteMatricula();
        $enTramiteMatricula->setId_tramite($tramiteEN)
            ->setId_matricula($matriculaEN);
        $this->negocio->save($enTramiteMatricula);

        $enTramiteSala = new \G2\Entity\TramiteSalaDeAula();
        $enTramiteSala->setId_tramite($tramiteEN)
            ->setId_saladeaula($salaDeAulaEN)
            ->setId_usuario($usuario);

        return $this->negocio->save($enTramiteSala);
    }
}

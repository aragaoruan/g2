<?php

namespace G2\Events;

use Zend_EventManager_Event;
use Zend_EventManager_EventCollection;
use Zend_EventManager_EventManager;

abstract class G2EventManager
{
    protected $events;

    public function __construct()
    {
        $this->events()->attach('dispatch', function ($event) {
            $this->handle($event);
        });
    }

    protected abstract function handle(Zend_EventManager_Event $event);

    public function events(Zend_EventManager_EventCollection $events = null)
    {
        if (null !== $events) {
            $this->events = $events;
        } elseif (null === $this->events) {
            $this->events = new Zend_EventManager_EventManager(__CLASS__);
        }
        return $this->events;
    }

    public function dispatch($params)
    {
        $this->events()->trigger(__FUNCTION__, $this, $params);
    }
}

<?php

namespace G2;

use G2\Negocio\Negocio;
use G2\Utils\Pagination;

/**
 * Classe para facilitar o manuseio das Entities. Aqui poderemos ter métodos auxiliares que normalmente estariam
 * na Negocio. Mas caso não tenhamos uma Negocio da Entity, podemos usar aqui.
 * @author Elcio Mauro Guimarães
 *
 */
abstract class G2Entity
{


    /**
     * Classe para a partir de um Array de dados $chave => $valor
     * Sejam setados todos os campos da Entity
     *
     * @param array $dados
     */
    public function montarEntity(array $dados = null)
    {
        if ($dados) {
            foreach ($dados as $propriedade => $valor) {
                if (!empty($propriedade) && !empty($valor)) {
                    $metodo = 'set' . trim($propriedade);
                    if (method_exists($this, $metodo) && !empty($valor)) {
                        $this->$metodo($valor);
                    }
                }
            }

            $serialize = new \Ead1\Doctrine\EntitySerializer(\Zend_Registry::get('doctrine')->getEntityManager());
            $serialize->arrayToEntity($this->toArray(), $this);
        }
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        $em = \Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('doctrine')->getEntityManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(new FirebugDoctrineLogger());
        return $em;
    }

    /**
     *
     * Busca a Entity pelo ID
     * @param $id
     * @param $returnEntity usado caso queira pegar a entity para setar em algum insert ou upload
     * @return bool | G2Entity
     */
    public function find($id, $returnEntity = false)
    {
        $temp = $this->getEm()->getRepository(get_class($this))->find($id);
        if (!$temp) {
            return false;
        }
        $this->selfSet($temp);
        if ($returnEntity) {
            return $temp;
        }
    }

    /**
     * Busca uma Entity
     * @param array $array
     * @param $returnEntity usado caso queira pegar a entity para setar em algum insert ou upload
     * @param $orderBy array com campo e direção: array('dt_cadastro'=>'DESC')
     */
    public function findOneBy(array $array, $returnEntity = false, array $orderBy = null)
    {
        if (!$array)
            return false;

        $class_name = get_class($this);
        $temp = $this->getEm()->getRepository($class_name)->findOneBy($array, $orderBy);
        if ($temp) {
            $this->selfSet($temp);
        }
        if ($returnEntity) {
            return $temp;
        }
    }


    /**
     * @param array $array
     * @param array $order - array('name' => 'ASC')
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @return multitype:unknown
     */
    public function findBy(array $array, $order = null, $limit = null, $offset = 0, $returnEntity = true)
    {

        $result = array();
        $class_name = get_class($this);
        $temp = $this->getEm()->getRepository($class_name)->findBy($array, $order, $limit, $offset);
        if ($temp) {
            foreach ($temp as $entity) {
//				$ent = new $class_name();
//				$ent->selfSet($entity);
                if ($returnEntity) {
                    $result[] = $entity;
                } else {
                    $result[] = $entity->toBackboneArray();
                }

            }
        }
        return $result;
    }

    /**
     * Este método trata os tipos de campos adicionando LIKE para textos
     * @param array $array
     * @param null $order
     * @param null $limit
     * @param int $offset
     * @return array
     */
    public function findByCustom(array $array, $order = null, $limit = null, $offset = 0, $returnEntity = true)
    {

        try {
            $result = array();
            $class_name = get_class($this);

            $qb = $this->getEm()->getRepository($class_name)->createQueryBuilder('o')->where('1=1');
            if ($array) {
                foreach ($array as $chave => $valor) {
                    switch (substr($chave, 0, 2)) {
                        case "st":
                            $qb->andWhere('o.' . $chave . ' like :' . $chave);
                            $qb->setParameter($chave, '%' . $valor . '%');
                            break;
                        default:
                            if ($valor === null) {
                                $qb->andWhere('o.' . $chave . ' is null');
                            } else {
                                $qb->andWhere('o.' . $chave . ' = :' . $chave);
                                $qb->setParameter($chave, $valor);
                            }
                    }

                }
            }


            if ($order) {
                foreach ($order as $sort => $ordem) {
                    $qb->orderBy('o.' . $sort, $ordem);
                }
            }


            if ($limit)
                $qb->setMaxResults($limit);

            if ($offset)
                $qb->setFirstResult($offset);

            if (!$returnEntity) {
                $temp = $qb->getQuery()->getArrayResult();
            } else {
                $temp = $qb->getQuery()->getResult();
            }

            if ($temp) {
                foreach ($temp as $entity) {
                    $result[] = $entity;
                }
            }

            return $result;

        } catch (\Exception $e) {
            throw $e;
        }

    }


    /**
     * Método que busca na Entity paginando
     * @param array $array
     * @param null $order
     * @param int $page
     * @param int $per_page
     * @param bool|true $returnEntity
     * @return Pagination
     * @throws \Exception
     */
    public function paging(array $array, $order = null, $page = 1, $per_page = 25, $returnEntity = true)
    {

        try {
            $ne = new Negocio();
            return $ne->paging(get_class($this), $array, $order, $page, $per_page, $returnEntity);
        } catch (\Exception $e) {
            throw $e;
        }

    }


    /**
     * Seta o Objeto ATUAL com os dados vindos do banco
     * @param G2Entity $entity
     * @internal param array $dados
     */
    public function selfSet(G2Entity $entity)
    {
        $metodos = get_class_methods($entity);
        foreach ($metodos as $metodo) {
            if (substr($metodo, 0, 3) == 'set') {
                $get = "get" . substr($metodo, 3);
                if ($entity->$get() !== null && $entity->$get() !== '') {
                    if (substr(strtolower($metodo), 0, 6) == 'setdt_' || substr(strtolower($metodo), 0, 5) == 'setdt') {
                        if (is_string($entity->$get()) && $entity->$get()) {
                            $this->$metodo(new \DateTime($entity->$get()));
                        } else {
                            $this->$metodo($entity->$get());
                        }
                    } else {
                        $this->$metodo($entity->$get());
                    }
                }
            }
        }
    }

    /**
     * Converte a Entity em um Array
     * Talvez ainda precise de tratamento para datas
     * Alterado para o _toArray() abaixo por ter sido sobrescrito nas Entities
     * @return array
     */
    public function toArray()
    {
        return $this->entityToArray();
    }


    /**
     * Converte a Entity em um Array
     * Talvez ainda precise de tratamento para datas
     * Textos, etc
     * @return NULL
     */
    public final function entityToArray()
    {
        $array = null;
        $arChaves = get_class_methods($this);
        if ($arChaves) {
            foreach ($arChaves as $metodo) {
                if (strtolower(substr($metodo, 0, 3)) == 'get') {
                    $chave = strtolower(substr($metodo, 3));
                    if ($this->$metodo() instanceof \G2\G2Entity) {
                        $array[$chave] = $this->$metodo()->entityToArray();
                    } elseif ($this->$metodo() instanceof \DateTime) {
                        $array[$chave] = $this->$metodo()->format('d/m/Y H:i:s');
                    } else {
                        $array[$chave] = $this->$metodo();
                    }
                }
            }
        }
        return $array;
    }


    /**
     * @param string $dateTimeFormat
     * @param string $zendDateFormat
     * @return null
     * @throws \Zend_Exception
     */
    public function toBackboneArray($dateTimeFormat = 'd/m/Y H:i:s', $zendDateFormat = 'dd/MM/yyyy HH:mm:ss')
    {

        $dateTimeFormat = ($dateTimeFormat ? $dateTimeFormat : 'd/m/Y H:i:s');
        $zendDateFormat = ($zendDateFormat ? $zendDateFormat : 'dd/MM/yyyy HH:mm:ss');

        $em = \Zend_Registry::get('doctrine')->getEntityManager();
        $array = null;
        $arChaves = get_class_methods($this);
        if ($arChaves) {

            foreach ($arChaves as $metodo) {

                if ($metodo == "getEm") continue;

                if (strtolower(substr($metodo, 0, 3)) == 'get') {
                    $chave = strtolower(substr($metodo, 3));
                    if ($this->$metodo() instanceof \G2\G2Entity) {
                        $metodoPK = 'get' . $em->getClassMetadata(get_class($this->$metodo()))->getSingleIdentifierFieldName();
                        $array[$chave] = $this->$metodo()->$metodoPK();
                    } elseif ($this->$metodo() instanceof \DateTime) {
                        $array[$chave] = $this->$metodo()->format($dateTimeFormat);
                    } elseif ($this->$metodo() instanceof \Zend_Date) {
                        $array[$chave] = $this->$metodo()->toString($zendDateFormat);
                    } else {
                        switch (strtolower(substr($metodo, 0, 6))) {
                            case 'getdt_':
                                if ($this->$metodo()) {
                                    $date = new \Zend_Date($this->$metodo(), \Zend_Date::ISO_8601);
                                    $array[$chave] = $date->toString($zendDateFormat);
                                } else {
                                    $array[$chave] = null;
                                }
                                break;
                            case 'getbl_':

                                if ($this->$metodo() === true || $this->$metodo() === 'true' || (int)$this->$metodo() === 1) {
                                    $array[$chave] = true;
                                } else if ($this->$metodo() === false || $this->$metodo() === 'false' || $this->$metodo() === 0 || ((int)$this->$metodo() == 0 && $this->$metodo() != null)) {
                                    $array[$chave] = false;
                                } else {
                                    $array[$chave] = null;
                                }
                                break;

                            default:
                                $array[$chave] = $this->$metodo();
                                break;
                        }
                    }
                }
            }
        }
        return $array;

    }

}

<?php

namespace G2;

/**
 * O objetivo desta classe e reunir todas as formas de envio de mensagens
 * do sistema em uma unica classe que possa oferecer metodos estaticos que 
 * facilitem sua utilizacao, alem de ser o passo inicial para abandonar a
 * utilizacao das BOs.
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class G2Mensagem {

    /**
     * Metodo responsavel por fazer a chamada de metodos de BO enquanto ainda
     * utilizados
     * @param array $data
     * @return type
     */
    public static function gerarMensagemEnvioCartao(array $data)
    {
        $vendaTO = new \VendaTO();
        $mensagemBO = new \MensagemBO();

        if ($data['id_meiopagamento'] == \G2\Constante\MeioPagamento::CARTAO_CREDITO) {
            $vendaTO->setId_venda($data['id_venda']);
            $vendaTO->fetch(FALSE, TRUE, TRUE);

            return $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(
                            \G2\Constante\MensagemPadrao::ENVIO_CARTAO_CREDITO, $vendaTO, \G2\Constante\TipoEnvio::EMAIL);
        }
    }

    /**
     * Metodo responsavel por fazer a chamada de metodo gerarMensagemEnvioBoleto 
     * de BO enquanto ainda utilizados
     * @param array $data
     * @return type
     */
    public static function gerarMensagemEnvioBoleto(array $data)
    {
        $vendaTO = new \VendaTO();
        $mensagemBO = new \MensagemBO();

        if ($data['id_meiopagamento'] == \G2\Constante\MeioPagamento::BOLETO) {
            $vendaTO->setId_venda($data['id_venda']);
            $vendaTO->fetch(FALSE, TRUE, TRUE);

            return $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(
                            \G2\Constante\MensagemPadrao::ENVIO_BOLETO, $vendaTO, \G2\Constante\TipoEnvio::EMAIL);
        }
    }

}

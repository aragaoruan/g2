<?php

namespace G2\Plugins;

/**
 * Plugin responsavel por verificar a requisicao para aplicar validacoes
 * necessaria antes de permitir o usuario visualizar ou baixar os arquivos
 * do diretorio upload.
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class DownloadSeguro extends \Zend_Controller_Plugin_Abstract {

    public function dispatchLoopStartup(\Zend_Controller_Request_Abstract $request)
    {

        //Verifica se esta sendo feita a tentativa de acesso ao diretorio upload
        if ($request->getControllerName() == 'upload') {
            
            //Verifica se o usuario esta autenticado na pagina
            if (\Zend_Auth::getInstance()->hasIdentity() || \Ead1_Sessao::getSessaoUsuario()->id_usuario) {
                $path = realpath(APPLICATION_PATH . '/../') . '/public';
                $dirFile = \Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();

                //Verifica se o arquivo informado existe no diretorio
                if (is_file($path . $dirFile)) {
                    $arquivo = $path . $dirFile;
                    switch (strtolower(substr(strrchr(basename($arquivo), "."), 1))) { // verifica a extensão do arquivo para pegar o tipo
                        case "pdf": $tipo = "application/pdf";
                            break;
                        case "exe": $tipo = "application/octet-stream";
                            break;
                        case "zip": $tipo = "application/zip";
                            break;
                        case "doc": $tipo = "application/msword";
                            break;
                        case "docx": $tipo = "application/msword";
                            break;
                        case "xls": $tipo = "application/vnd.ms-excel";
                            break;
                        case "xlsx": $tipo = "application/vnd.ms-excel";
                            break;
                        case "ppt": $tipo = "application/vnd.ms-powerpoint";
                            break;
                        case "pptx": $tipo = "application/vnd.ms-powerpoint";
                            break;
                        case "gif": $tipo = "image/gif";
                            break;
                        case "png": $tipo = "image/png";
                            break;
                        case "jpg": $tipo = "image/jpg";
                            break;
                        case "php": // deixar vazio por seurança
                        case "htm": // deixar vazio por seurança
                        case "html": // deixar vazio por seurança
                    }
                    header("Content-Type: " . $tipo); // informa o tipo do arquivo ao navegador
                    header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
                    header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
                    readfile($arquivo);
                    exit;
                } else {
                    
                    //Gera uma mensagem e aplica o layout error.phtml
                    \Zend_Registry::set('messageNotFound', '<h3><center>Arquivo especificado não foi encontrado</center></h3>');
                    $layout = \Zend_Layout::getMvcInstance();
                    $layout->setLayout('error');
                }
            } else {
                
                //Seta para o usuario qual a pagina a ser acessada
                $request->setControllerName('index');
                $request->setActionName('index');
                
                //Verificando se a URL provem do portal do aluno para poder redirecionar o usuario para o login do portal
                if(stripos($_SERVER['HTTP_HOST'], 'portal') || stripos($_SERVER['HTTP_HOST'], 'aluno')){
                    $request->setModuleName('portal');
                }
            }
        }
    }

}

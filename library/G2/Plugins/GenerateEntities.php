<?php

namespace G2\Plugins;

/**
 * Plugin responsável por verificar todas as entidades e gerar um arquivo unico minificado
 *
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class GenerateEntities extends \Zend_Controller_Plugin_Abstract
{

   /* protected $doctrineContainer;
    protected $em;

    public function preDispatch()
    {
        $this->doctrineContainer = \Zend_Registry::get('doctrine');
        $this->em = $this->doctrineContainer->getEntityManager();
    }

    public function dispatchLoopStartup(\Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'default') {
            $dirEntity = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "G2" . DIRECTORY_SEPARATOR . "Entity";
            $dirJS = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "gestor2" . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR . "backbone" . DIRECTORY_SEPARATOR . "models" . DIRECTORY_SEPARATOR . "cache";

            if ($handle = opendir($dirEntity)) {
                $createJs = false;
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $baseName = basename($dirJS . DIRECTORY_SEPARATOR . $file, ".php");
                        $arquivoJSgerado = $baseName . '.js';
                        $modificacaoEntity = filemtime($dirEntity . DIRECTORY_SEPARATOR . $file);

                        if (!file_exists($arquivoJSgerado) && $modificacaoEntity != filemtime($arquivoJSgerado)) {
                            $this->generateJs($dirJS, $dirEntity, $handle);
                        }
                    }
                }
                closedir($handle);
            }
        }
    }

    public function generateJs($dirJS, $dirEntity, $handle)
    {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $baseName = basename($dirJS . DIRECTORY_SEPARATOR . $file, ".php");
                $arquivoJSgerado = $baseName . '.js';
                $modificacaoEntity = filemtime($dirEntity . DIRECTORY_SEPARATOR . $file);

                if (!file_exists($arquivoJSgerado) && $modificacaoEntity != filemtime($arquivoJSgerado)) {
                    $this->generateJs($dirJS, $dirEntity, $handle);
                }
            }
        }
        closedir($handle);
    }*/
}

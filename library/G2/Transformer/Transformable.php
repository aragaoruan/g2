<?php

namespace G2\Transformer;

/**
 * Class Transformable
 * @package G2\Transformer
 * @property $transformer
 */
abstract class Transformable
{
    /**
     * @var array
     */
    protected $transformer;

    /**
     * Transformable constructor.
     */
    public function __construct()
    {
        $this->transformer = array();
    }

    /**
     * @return mixed
     */
    public function getTransformer()
    {
        return $this->transformer;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

}

<?php

namespace G2\Transformer;

interface ArrayTransformerInterface
{
    function iterate(array $entities);
}

<?php

namespace G2\Transformer\Cancelamento;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;

/**
 * Class MatriculaCancelamentoTransformer
 * @package G2\Transformer\Cancelamento
 */
class MatriculaCancelamentoTransformer extends ArrayTransformer
{

    private $totalCh;
    private $dtReferencia;
    public function __construct(array $entities)
    {
        if ($entities) {
            $this->totalCh = 0;
            foreach ($entities as $entity) {
                //TODO COLCOLAR A VERIFICAÇÂO PARA RETORNAR SOMENTE OS BL_CANCELAMENTO = TRUE
                if ($entity->getBl_cancelamento() === true || $entity->getBl_cancelamento() === null) {
                    if ($entity->getNu_cargahoraria()) {
                        $this->totalCh += $entity->getNu_cargahoraria();
                    }
                }

                if ($this->dtReferencia == '' ) {
                    $this->dtReferencia = Helper::converterData($entity->getDt_abertura(), "Y-m-d");
                }
                if (strtotime($this->dtReferencia) < strtotime(
                        Helper::converterData($entity->getDt_abertura(), "Y-m-d")))
                {
                    $this->dtReferencia = Helper::converterData($entity->getDt_abertura(), "Y-m-d");
                }

            }
        }
        parent::__construct($entities);
    }

    /**
     * @param G2Entity $entity
     * @return array
     */
    function transform(G2Entity $entity)
    {

        return [
            "id_matricula" => $entity->getId_matricula(),
            "id_projeto" => $entity->getId_projetopedagogico(),
            "st_projeto" => $entity->getSt_tituloexibicao(),
            "id_evolucao" => $entity->getId_evolucao(),
            "st_evolucao" => $entity->getSt_evolucao(),
            "st_aceite" => Helper::converterData($entity->getDt_aceitecontrato(), "d/m/Y"),
            "nu_cargahorariaprojeto" => $entity->getnu_cargahorariaprojeto(),
            "nu_valortotalcurso" => $entity->getnu_valortotalcurso(),
            "nu_chdisponibilizada" => $this->totalCh,
            "id_venda" => $entity->getId_venda(),
            "dt_cancelamento" => Helper::converterData($entity->getDt_cancelamento(), "d/m/Y"),
            "dt_matricula" => Helper::converterData($entity->getDt_matricula(), "d/m/Y"),
            "dt_referencia" => Helper::converterData($this->dtReferencia, "d/m/Y"),
            "disciplinas" => [
                "id_matricula" => $entity->getId_matricula(),
                "id_disciplina" => $entity->getId_disciplina(),
                "st_disciplina" => $entity->getSt_disciplina(),
                "dt_abertura" => Helper::converterData($entity->getDt_abertura(), "d/m/Y"),
                "dt_encerramento" => Helper::converterData($entity->getDt_encerramento(), "d/m/Y"),
                "nu_cargahoraria" => $entity->getNu_cargahoraria(),
                "bl_cancelamento" => $entity->getBl_cancelamento(),
                "id_projeto" => $entity->getId_projetopedagogico(),
            ]
        ];
    }


}

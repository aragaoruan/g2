<?php

namespace G2\Transformer\Disciplina;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;

class DisciplinaTransformer extends ArrayTransformer
{

    /**
     * @param G2Entity $entity
     * @return array
     */
    function transform(G2Entity $entity)
    {
        return [
            'id_disciplina' => $entity->getId_disciplina(),
            'st_disciplina' => $entity->getSt_disciplina(),
            'st_descricao' => strip_tags($entity->getSt_descricao(), \G2\Constante\Utils::$tinYMceHtmlTagsAllowed),
            'nu_identificador' => $entity->getNu_identificador(),
            'nu_cargahoraria' => $entity->getNu_cargahoraria(),
            'nu_creditos' => $entity->getNu_creditos(),
            'nu_codigoparceiro' => $entity->getNu_codigoparceiro(),
            'bl_ativa' => $entity->getBl_ativa(),
            'bl_compartilhargrupo' => $entity->getBl_compartilhargrupo(),
            'id_situacao' => $entity->getId_situacao()->getId_situacao(),
            'st_tituloexibicao' => $entity->getSt_tituloexibicao(),
            'id_tipodisciplina' => $entity->getId_tipodisciplina(),
            'id_entidade' => $entity->getId_entidade(),
            'dt_cadastro' => $entity->getDt_cadastro(),
            'id_usuariocadastro' => $entity->getId_usuariocadastro(),
            'st_identificador' => $entity->getSt_identificador(),
            'st_imagem' => $entity->getst_imagem(),
            'st_ementacertificado' => strip_tags($entity->getSt_ementacertificado(), \G2\Constante\Utils::$tinYMceHtmlTagsAllowed),
            'bl_provamontada' => $entity->getBl_provamontada(),
            'bl_disciplinasemestre' => $entity->getBl_disciplinasemestre(),
            'nu_repeticao' => $entity->getNu_repeticao(),
            'sg_uf' => $entity->getSg_uf() ? $entity->getSg_uf()->getSg_uf() : null,
            'st_apelido' => $entity->getSt_apelido(),
            'id_grupodisciplina' => $entity->getId_grupodisciplina()
                ? $entity->getId_grupodisciplina()->getId_grupodisciplina()
                : null,
            'st_codprovaintegracao' => $entity->getSt_codprovaintegracao()
                ? $entity->getSt_codprovaintegracao()
                : null,
            'st_provaintegracao' => $entity->getSt_provaintegracao()
                ? $entity->getSt_provaintegracao()
                : null,
            'st_codprovarecuperacaointegracao' => $entity->getSt_codprovarecuperacaointegracao()
                ? $entity->getSt_codprovarecuperacaointegracao()
                : null,
            'st_provarecuperacaointegracao' => $entity->getSt_provarecuperacaointegracao()
                ? $entity->getSt_provarecuperacaointegracao()
                : null,
            'bl_coeficienterendimento' => $entity->getBl_coeficienterendimento(),
            'bl_cargahorariaintegralizada' => $entity->getBl_cargahorariaintegralizada()
        ];
    }
}

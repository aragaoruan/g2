<?php

namespace G2\Transformer\Maximize;

use G2\G2Entity;
use G2\Transformer\EntityTransformer;
use G2\Utils\Helper;

class VwPesquisarPessoaTransformer extends EntityTransformer
{

    public function transform(G2Entity $entity)
    {
        return [
            'codigoSistemaOrigem' => (string)$entity->getId_usuario(),
            'nome' => $entity->getSt_nomecompleto(),
            'login' => null,
            'nomeMae' => $entity->getSt_nomemae(),
            'matricula' => null,
            'entidade' => $entity->getId_entidade(),
            'email' => $entity->getSt_email(),
            'senha' => null,
            'cidade' => $entity->getSt_nomemunicipio(),
            'uf' => $entity->getSg_uf(),
            'sexo' => null,
            'endereco' => null,
            'dataNascimento' => Helper::converterData($entity->getDt_nascimento(), 'd/m/Y'),
            'telefone' => $entity->getSt_telefone(),
            'cpf' => $entity->getSt_cpf(),
            'rg' => null,
            'cep' => null,
            'isExcluido' => false,
            'inicioVigencia' => date('d/m/Y'),
            'fimVigencia' => null,
        ];
    }
}

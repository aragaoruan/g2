<?php

namespace G2\Transformer\Maximize;

use G2\G2Entity;
use G2\Transformer\EntityTransformer;

class NotasTransformer extends EntityTransformer
{

    function transform(G2Entity $entity)
    {
        $result = [
            'id_matricula' => $entity->getId_matricula(),
            'id_avaliacao' => $entity->getId_avaliacao(),
        ];

        $this->shouldNotHaveNullKey(
            $result,
            'id_avaliacaoconjuntoreferencia',
            $entity->getId_avaliacaoconjuntoreferencia());

        return $result;
    }

    private function shouldNotHaveNullKey(&$result, $key, $value)
    {
        !is_null($value) ? $result[$key] = $value : false;
    }
}

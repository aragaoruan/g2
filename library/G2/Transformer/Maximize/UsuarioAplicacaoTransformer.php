<?php

namespace G2\Transformer\Maximize;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;

class UsuarioAplicacaoTransformer extends ArrayTransformer
{

    /**
     * Método transform que deve ser implementado nas subclasses
     * @param G2Entity $entity
     * @return mixed
     */
    function transform(G2Entity $entity)
    {
        return [
            'codigoSalaSistemaOrigem' => (string)$entity->getId_aplicadorprova(),
            'dataAplicacao' => Helper::converterData($entity->getDt_aplicacao(), 'd/m/Y')
                . ' ' . Helper::converterData($entity->getHr_inicio(), 'H:i:s'),
            'isExcluido' => false,
            'provaUsuario' => [
                'codigoUsuarioSistemaOrigem' => (string)$entity->getId_usuario(),
                'codProva' => (int)$entity->getCod_prova(),
                'isExcluido' => false,
                'idAvaliacaoAgendamento' => (int)$entity->getId_avaliacaoagendamento()
            ]
        ];
    }
}

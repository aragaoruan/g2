<?php

namespace G2\Transformer\Maximize;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;

class UsuariosIntegracaoTransformer extends ArrayTransformer
{

    function transform(G2Entity $entity)
    {
        return [
            "id_usuario" => $entity->getId_usuario(),
            "id_entidade" => $entity->getId_entidade()
        ];
    }
}

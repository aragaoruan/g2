<?php
namespace G2\Transformer\Assunto;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;

class RecuperacaoResgateTransformer extends ArrayTransformer
{
    /**
     * @param G2Entity $entity
     * @return array
     */
    public function transform(G2Entity $entity)
    {
        return [
            'id_assuntoco' => $entity->getId_assuntoco(),
            'bl_recuperacao_resgate' => $entity->getBl_recuperacao_resgate(),
            'st_assuntoco' => $entity->getSt_assuntoco(),
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 18/09/2017
 * Time: 13:45
 */

namespace G2\Transformer\ColacaoGrau;


use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;

class AlunoColacaoTransformer extends ArrayTransformer
{
    /**
     * @var int
     */
    private $count;

    public function __construct(array $entities)
    {
        $this->count = 0;
        parent::__construct($entities);

    }

    function transform(G2Entity $entity)
    {
        $this->count = ++$this->count;
        return [
            'nu_numeracao' => $this->count,
            'id_projetopedagogico' => $entity->getId_projetopedagogico(),
            'st_projetopedagogico' => $entity->getSt_projetopedagogico(),
            'id_matricula' => $entity->getId_matricula(),
            'st_nomecompleto' => $entity->getSt_nomecompleto(),
            'st_cpf' => Helper::mask($entity->getSt_cpf(), '###.###.###-##'),
            'dt_concluinte' => $entity->getDt_concluinte() ?
                Helper::converterData($entity->getDt_concluinte(), 'd/m/Y') : null,
            'dt_colacao' => $entity->getDt_colacao() ?
                Helper::converterData($entity->getDt_colacao(), 'd/m/Y') : null,
            'bl_presenca' => !is_null($entity->getBl_presenca()) ? $entity->getBl_presenca() : 'null',
            'id_matriculacolacao' => $entity->getId_matriculacolacao(),
            'bl_documentacao' => $entity->getBl_documentacao(),
            'st_statusdocumentacao' => $entity->getSt_statusdocumentacao()
        ];

    }
}

<?php

namespace G2\Transformer\MatriculaDiplomacao;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;

class MatriculaDiplomacaoTransformer extends ArrayTransformer
{

    /**
     * @param G2Entity $entity
     * @return array
     */
    public function transform(G2Entity $entity)
    {
        $transformed = $entity->toBackboneArray();

        // Ajustar datas
        foreach ($transformed as $attr => $val) {
            if (substr($attr, 0, 3) === 'dt_') {
                $transformed[$attr] = Helper::converterData($val, 'd/m/Y');
            }
        }

        return $transformed;
    }


}

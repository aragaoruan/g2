<?php

namespace G2\Transformer;

use G2\G2Entity;

/**
 * Class ArrayTransformer
 * @package G2\Transformer
 * @property transformer
 */
abstract class ArrayTransformer extends Transformable implements TransformerInterface, ArrayTransformerInterface
{
    use Mergeable;

    public function __construct(array $entities)
    {
        parent::__construct();
        $this->iterate($entities);
        return $this;
    }

    public function iterate(array $entities)
    {
        foreach ($entities as $entity) {
            array_push($this->transformer, $this->transform($entity));
        }
    }

    abstract function transform(G2Entity $entity);
}

<?php

namespace G2\Transformer;

use G2\G2Entity;

interface TransformerInterface
{
    public function transform(G2Entity $entity);
}

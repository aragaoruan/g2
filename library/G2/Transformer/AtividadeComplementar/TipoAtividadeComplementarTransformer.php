<?php

namespace G2\Transformer\AtividadeComplementar;

use G2\G2Entity;
use G2\Transformer\ArrayTransformer;

/**
 * Class TipoAtividadeComplementarTransformer
 * @package G2\Transformer\AtividadeComplementar
 */
class TipoAtividadeComplementarTransformer extends ArrayTransformer
{

    /**
     * @param G2Entity $entity
     * @return array
     */
    function transform(G2Entity $entity)
    {
        return [
            "id_tipoatividadecomplementar" => $entity->getId_tipoatividadecomplementar(),
            "st_tipoatividadecomplementar" => $entity->getSt_tipoatividadecomplementar(),
            "dt_cadastro" => $entity->getDt_cadastro(),
            "bl_ativo" => $entity->isBl_ativo(),
            "id_usuariocadastro" => $entity->getId_usuariocadastro() ? $entity
                ->getId_usuariocadastro()->getId_usuario() : null,
            "id_entidadecadastro" => $entity->getId_entidadecadastro() ? $entity
                ->getId_entidadecadastro()->getId_entidade() : null,

        ];
    }
}

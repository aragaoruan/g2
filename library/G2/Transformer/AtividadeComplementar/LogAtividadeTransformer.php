<?php

namespace G2\Transformer\AtividadeComplementar;

use G2\Entity\OperacaoLog;
use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;


/**
 * Class LogAtividadeTransformer
 * @package G2\Transformer\AtividadeComplementar
 */
class LogAtividadeTransformer extends ArrayTransformer
{

    /**
     * @param G2Entity $entity
     * @return array
     */
    public function transform(G2Entity $entity)
    {
        $acao = $this->getAcao($entity);
        return [
            "id_log" => $entity->getId_log(),
            "id_usuario" => $entity->getid_usuario()->getId_usuario(),
            "st_usuario" => $entity->getid_usuario()->getSt_nomecompleto(),
            "dt_cadastro" => Helper::converterData($entity->getdt_cadastro(), "d/m/Y H:i:s"),
            "id_operacao" => $entity->getid_operacao()->getId_operacaolog(),
            "st_operacao" => $entity->getid_operacao()->getSt_descricaoperacaolog(),
            "acao" => $acao
        ];
    }

    /**
     * Formata o texto para retorno
     * @param $entity
     * @return string
     */
    private function getAcao($entity)
    {
        $antes = $entity->getst_antes();
        $depois = $entity->getst_depois();

        switch ($entity->getst_coluna()) {
            case "id_situacao":
                $situacaoAntes = $entity->getEm()->find(\G2\Entity\Situacao::class, $antes);
                $situacaoDepois = $entity->getEm()->find(\G2\Entity\Situacao::class, $depois);

                $acao = "Status alterado " . ($situacaoAntes ? "de " . $situacaoAntes->getSt_situacao() : "")
                    . " para " . $situacaoDepois->getSt_situacao();

                break;
            case "st_tituloatividade":
                $acao = "Título da Atividade alterado " . ($antes ? "de " . $antes : "")
                    . " para " . $depois;
                break;
            case "nu_horasconvalidada":
                $acao = "QTD de Horas alterado de " . $antes
                    . " para " . $depois;
                break;
            case "st_observacaoavaliador":
                $acao = "Informações Adicionais alterado " . ($antes ? "de " . $antes : "")
                    . " para " . $depois;
                break;
            default:
                $acao = "";
                break;
        }
        return $acao;
    }

}

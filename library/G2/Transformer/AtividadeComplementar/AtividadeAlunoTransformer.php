<?php

namespace G2\Transformer\AtividadeComplementar;

use G2\Constante\Situacao;
use G2\G2Entity;
use G2\Transformer\ArrayTransformer;
use G2\Utils\Helper;

/**
 * Class AtividadeAlunoTransformer
 * @package G2\Transformer\AtividadeComplementar
 */
class AtividadeAlunoTransformer extends ArrayTransformer
{

    /**
     * @param G2Entity $entity
     * @return array
     */
    function transform(G2Entity $entity)
    {
        $tipoAtividade = $entity->getid_tipoatividade();
        $situacao = $entity->getId_situacao();

        $nuHoras = $entity->getNu_horasconvalidada();

        //quando as horas não forem validadas força o valor para zero horas
        if ($situacao->getId_situacao() == Situacao::TB_ATIVIDADECOMPLEMENTAR_INDEFERIDO) {
            $nuHoras = "0";
        }

        //verifica se a situação do registro permite que ele seja removido
        $remove = false;
        if ($situacao->getId_situacao() == Situacao::TB_ATIVIDADECOMPLEMENTAR_ENVIADO_PARA_ANALISE) {
            $remove = true;
        }

        return [
            'id_atividadecomplementar' => $entity->getId_atividadecomplementar(),
            'st_tituloatividade' => $entity->getst_tituloatividade(),
            'st_tipoatividade' => $tipoAtividade->getSt_tipoatividadecomplementar(),
            'nu_horasconvalidada' => $nuHoras,
            'id_situacao' => $situacao->getId_situacao(),
            'st_situacao' => $situacao->getSt_situacao(),
            'st_resumo' => nl2br($entity->getSt_resumoatividade()),
            'url_arquivo' => \Ead1_Ambiente::geral()->st_url_portal . "/" . $entity->getSt_caminhoarquivo(),
            'remove' => $remove
        ];

    }


}

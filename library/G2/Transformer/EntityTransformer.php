<?php

namespace G2\Transformer;

use G2\G2Entity;

abstract class EntityTransformer extends Transformable implements TransformerInterface, EntityTransformerInterface
{
    public function __construct(G2Entity $entity)
    {
        $this->transformer = $this->transform($entity);
        return $this;
    }

    abstract function transform(G2Entity $entity);
}

<?php

namespace G2\Transformer;

trait Mergeable
{
    private $key;

    protected function merge($key)
    {
        $this->key = $key;
    }

    /**
     * TODO: Reduzir esse método
     * @param array $params
     * @return array
     */
    protected function on(array $params)
    {
        $aux = array();
        $transformerAux = $this->transformer;
        foreach ($transformerAux as $transformer) {
            $current = $transformer[$this->key];
            unset($transformer[$this->key]);
            foreach (array_keys($transformer) as $key) {
                if (!in_array($key, $params)) {
                    unset($transformer[$key]);
                }
            }

            $auxKeys = $aux;
            for ($i = 0; $i < count($auxKeys); $i++) {
                unset($auxKeys[$i][$this->key]);
            }

            if (!in_array($transformer, $auxKeys)) {
                $transformer[$this->key] = array();
                array_push($aux, $transformer);
            }

            $k = array_search($transformer, $aux);
            array_push($aux[$k][$this->key], $current);
        }

        $this->transformer = $aux;
    }

    /**
     * @param $method
     * @param $arguments
     * @return $this
     */
    public function __call($method, $arguments)
    {
        call_user_func_array(array($this, $method), $arguments);
        return $this;
    }
}

<?php

namespace G2\Transformer;

use G2\G2Entity;

/**
 * Classe abstrata para formatar dados e tipos de uma G2\Entity
 * Class Transformer
 * @package G2\Transformer
 */
abstract class Transformer
{

    /**
     * Variável pode assumir um Array, uma G2\Entity
     * @var $transformer
     */
    protected $transformer;

    /**
     * Chama o transform e retorna ela própria para métodos fluentes
     * Transformer constructor.
     * @param G2Entity $entity
     */
    public function __construct(G2Entity $entity)
    {
        $this->transform($entity);
        return $this;
    }

    /**
     * Método transform que deve ser implementado nas subclasses
     * @param G2Entity $entity
     * @return mixed
     */
    abstract function transform(G2Entity $entity);

    /**
     * Retorna a Entity transformada
     * @return mixed
     */
    public function transformer()
    {
        return $this->transformer;
    }

}

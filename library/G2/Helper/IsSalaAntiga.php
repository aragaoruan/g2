<?php

class G2_Helper_IsSalaAntiga extends Zend_Controller_Action_Helper_Abstract
{
    protected $situacoesTccAntigas = array('Apto', 'Não apto');

    public function getIsSalaAntiga($situacaoTcc)
    {
        return in_array($situacaoTcc, $this->situacoesTccAntigas);
    }

    public function direct($situacaoTcc)
    {
        return $this->getIsSalaAntiga($situacaoTcc);
    }
}

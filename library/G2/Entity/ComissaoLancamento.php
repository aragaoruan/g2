<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\ComissaoLancamento")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_comissaolancamento")
 * @author Kayo Silva <kayo.silva@unyelaya.com.br>
 * @since 2014-05-12
 */
class ComissaoLancamento
{

    /**
     * @var integer $id_comissaolancamento
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_comissaolancamento", type="integer", nullable=false, length=4)
     */
    private $id_comissaolancamento;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Lancamento $id_lancamento
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumn(name="id_lancamento", nullable=true, referencedColumnName="id_lancamento")
     */
    private $id_lancamento;

    /**
     * @var VendaProduto $id_vendaproduto
     * @ManyToOne(targetEntity="VendaProduto")
     * @JoinColumn(name="id_vendaproduto", nullable=true, referencedColumnName="id_vendaproduto")
     */
    private $id_vendaproduto;

    /**
     * @var Lancamento $id_lancamentopagamento
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumn(name="id_lancamentopagamento", nullable=true, referencedColumnName="id_lancamento")
     */
    private $id_lancamentopagamento;

    /**
     * @var boolean $bl_adiantamento
     * @Column(name="bl_adiantamento", type="boolean", nullable=true, length=1)
     */
    private $bl_adiantamento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", nullable=true, referencedColumnName="id_disciplina")
     */
    private $id_disciplina;


    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;


    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", nullable=true, type="integer")
     */
    private $id_perfilpedagogico;

    /**
     * @var boolean $bl_autorizado
     * @Column(name="bl_autorizado", nullable=true, type="boolean", options={"default"=0})
     */
    private $bl_autorizado;


    /**
     * @param integer $id_perfilpedagogico
     * @return $this
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }


    /**
     * @return integer
     */
    public function getId_comissaolancamento()
    {
        return $this->id_comissaolancamento;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return \G2\Entity\Lancamento
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @return \G2\Entity\VendaProduto
     */
    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @return \G2\Entity\Lancamento
     */
    public function getId_lancamentopagamento()
    {
        return $this->id_lancamentopagamento;
    }

    /**
     * @return boolean
     */
    public function getBl_adiantamento()
    {
        return $this->bl_adiantamento;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param integer $id_comissaolancamento
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_comissaolancamento($id_comissaolancamento)
    {
        $this->id_comissaolancamento = $id_comissaolancamento;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param \G2\Entity\Lancamento $id_lancamento
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_lancamento(Lancamento $id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }

    /**
     * @param \G2\Entity\VendaProduto $id_vendaproduto
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_vendaproduto(VendaProduto $id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    /**
     * @param \G2\Entity\Lancamento $id_lancamentopagamento
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_lancamentopagamento(Lancamento $id_lancamentopagamento)
    {
        $this->id_lancamentopagamento = $id_lancamentopagamento;
        return $this;
    }

    /**
     * @param boolean $bl_adiantamento
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setBl_adiantamento($bl_adiantamento)
    {
        $this->bl_adiantamento = $bl_adiantamento;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param \G2\Entity\Disciplina $id_disciplina
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_disciplina(Disciplina $id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return \G2\Entity\Disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param Entidade $id_entidade
     * @return \G2\Entity\ComissaoLancamento
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param boolean $bl_autorizado
     * @return $this
     */
    public function setBl_autorizado($bl_autorizado)
    {
        $this->bl_autorizado = $bl_autorizado;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_autorizado()
    {
        return $this->bl_autorizado;
    }

    /**
     * Retorna array com par chave valor dos atributos da entity
     * @return array chave valor dos atributos
     */
    public function toArray()
    {
        return array(
            'id_comissaolancamento' => $this->getId_comissaolancamento(),
            'id_usuario' => $this->getId_usuario()->getId_usuario(),
            'id_lancamento' => $this->getId_lancamento()->getIdLancamento(),
            'id_vendaproduto' => $this->getId_vendaproduto()->getId_vendaproduto(),
            'id_lancamentopagamento' => $this->getId_lancamentopagamento()->getIdLancamento(),
            'dt_cadastro' => $this->getDt_cadastro(),
            'bl_adiantamento' => $this->getBl_adiantamento(),
            'id_disciplina' => $this->getId_disciplina()->getId_disciplina(),
            'id_entidade' => $this->getId_entidade()->getId_entidade(),
            'id_perfilpedagogico' => $this->getId_perfilpedagogico(),
            'bl_autorizado' => $this->getBl_autorizado()
        );
    }

}

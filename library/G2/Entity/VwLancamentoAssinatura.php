<?php

namespace G2\Entity;

use G2\G2Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity(repositoryClass="G2\Repository\VwLancamentoAssinatura")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="vw_lancamento_assinatura")
 */
class VwLancamentoAssinatura extends G2Entity {

    /**
     * @var integer $id_venda
     * @Id
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var integer $id_produto
     * @Id
     * @Column(name="id_produto", type="integer")
     */
    private $id_produto;

    /**
     * @Column(name="dt_vencimento", type="datetime2", nullable=true)
     */
    private $dt_vencimento;

    /**
     * @var decimal $nu_valoranterior
     * @Column(name="nu_valoranterior", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valoranterior;

    /**
     * @var decimal $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valormensal;

    /**
     * @var decimal $nu_valordesconto
     * @Column(name="nu_valordesconto", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valordesconto;

    /**
     * @var decimal $nu_valorlancamento
     * @Column(name="nu_valorlancamento", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valorlancamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     * */
    private $id_entidade;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     * */
    private $id_usuario;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     * */
    private $id_matricula;

    /**
     * @Column(name="dt_confirmacao", type="datetime2", nullable=true)
     */
    private $dt_confirmacao;


    /**
     * @var integer $nu_orderm
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @var integer $id_lancamentoanterior
     * @Column(name="id_lancamentoanterior", type="integer")
     * */
    private $id_lancamentoanterior;

    /**
     * @var integer $id_cartaoconfig
     * @Column(name="id_cartaoconfig", type="integer")
     * */
    private $id_cartaoconfig;

    /**
     * @var integer $id_entidadelancamento
     * @Column(name="id_entidadelancamento", type="integer")
     * */
    private $id_entidadelancamento;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer")
     * */
    private $id_meiopagamento;

    /**
     * @var integer $id_tipolancamento
     * @Column(name="id_tipolancamento", type="integer")
     * */
    private $id_tipolancamento;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer")
     * */
    private $id_usuariocadastro;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer")
     * */
    private $id_usuariolancamento;


    /**
     * @var integer $nu_cobranca
     * @Column(name="nu_cobranca", type="integer", nullable=true)
     */
    private $nu_cobranca;

    /**
     * @var integer $nu_assinatura
     * @Column(name="nu_assinatura", type="integer", nullable=true)
     */
    private $nu_assinatura;

    /**
     * @return int
     */
    public function getNu_cobranca()
    {
        return $this->nu_cobranca;
    }

    /**
     * @param int $nu_cobranca
     */
    public function setNu_cobranca($nu_cobranca)
    {
        $this->nu_cobranca = $nu_cobranca;
        return $this;

    }

    /**
     * @return int
     */
    public function getNu_assinatura()
    {
        return $this->nu_assinatura;
    }

    /**
     * @param int $nu_assinatura
     */
    public function setNu_assinatura($nu_assinatura)
    {
        $this->nu_assinatura = $nu_assinatura;
        return $this;

    }

	/**
	 * @return the $id_matricula
	 */
	public function getId_matricula() {
		return $this->id_matricula;
	}

	/**
	 * @param number $id_matricula
	 */
	public function setId_matricula($id_matricula) {
		$this->id_matricula = $id_matricula;
		return $this;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $nu_valoranterior
	 */
	public function getNu_valoranterior() {
		return $this->nu_valoranterior;
	}

	/**
	 * @return the $nu_valormensal
	 */
	public function getNu_valormensal() {
		return $this->nu_valormensal;
	}

	/**
	 * @return the $nu_valordesconto
	 */
	public function getNu_valordesconto() {
		return $this->nu_valordesconto;
	}

	/**
	 * @return the $nu_ordem
	 */
	public function getNu_ordem() {
		return $this->nu_ordem;
	}

	/**
	 * @return the $id_lancamentoanterior
	 */
	public function getId_lancamentoanterior() {
		return $this->id_lancamentoanterior;
	}

	/**
	 * @return the $id_cartaoconfig
	 */
	public function getId_cartaoconfig() {
		return $this->id_cartaoconfig;
	}

	/**
	 * @return the $id_entidadelancamento
	 */
	public function getId_entidadelancamento() {
		return $this->id_entidadelancamento;
	}

	/**
	 * @return the $id_meiopagamento
	 */
	public function getId_meiopagamento() {
		return $this->id_meiopagamento;
	}

	/**
	 * @return the $id_tipolancamento
	 */
	public function getId_tipolancamento() {
		return $this->id_tipolancamento;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $id_usuariolancamento
	 */
	public function getId_usuariolancamento() {
		return $this->id_usuariolancamento;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param \G2\Entity\decimal $nu_valoranterior
	 */
	public function setNu_valoranterior($nu_valoranterior) {
		$this->nu_valoranterior = $nu_valoranterior;
		return $this;
	}

	/**
	 * @param \G2\Entity\decimal $nu_valormensal
	 */
	public function setNu_valormensal($nu_valormensal) {
		$this->nu_valormensal = $nu_valormensal;
		return $this;
	}

	/**
	 * @param \G2\Entity\decimal $nu_valordesconto
	 */
	public function setNu_valordesconto($nu_valordesconto) {
		$this->nu_valordesconto = $nu_valordesconto;
		return $this;
	}

	/**
	 * @param number $nu_ordem
	 */
	public function setNu_ordem($nu_ordem) {
		$this->nu_ordem = $nu_ordem;
		return $this;
	}

	/**
	 * @param number $id_lancamentoanterior
	 */
	public function setId_lancamentoanterior($id_lancamentoanterior) {
		$this->id_lancamentoanterior = $id_lancamentoanterior;
		return $this;
	}

	/**
	 * @param number $id_cartaoconfig
	 */
	public function setId_cartaoconfig($id_cartaoconfig) {
		$this->id_cartaoconfig = $id_cartaoconfig;
		return $this;
	}

	/**
	 * @param number $id_entidadelancamento
	 */
	public function setId_entidadelancamento($id_entidadelancamento) {
		$this->id_entidadelancamento = $id_entidadelancamento;
		return $this;
	}

	/**
	 * @param number $id_meiopagamento
	 */
	public function setId_meiopagamento($id_meiopagamento) {
		$this->id_meiopagamento = $id_meiopagamento;
		return $this;
	}

	/**
	 * @param number $id_tipolancamento
	 */
	public function setId_tipolancamento($id_tipolancamento) {
		$this->id_tipolancamento = $id_tipolancamento;
		return $this;
	}

	/**
	 * @param number $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
		return $this;
	}

	/**
	 * @param number $id_usuariolancamento
	 */
	public function setId_usuariolancamento($id_usuariolancamento) {
		$this->id_usuariolancamento = $id_usuariolancamento;
		return $this;
	}

	/**
	 * @return the $id_venda
	 */
	public function getId_venda() {
		return $this->id_venda;
	}

	/**
	 * @return the $dt_vencimento
	 */
	public function getDt_vencimento() {
		return $this->dt_vencimento;
	}

	/**
	 * @return the $nu_valorlancamento
	 */
	public function getNu_valorlancamento() {
		return $this->nu_valorlancamento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $dt_confirmacao
	 */
	public function getDt_confirmacao() {
		return $this->dt_confirmacao;
	}

	/**
	 * @param number $id_venda
	 */
	public function setId_venda($id_venda) {
		$this->id_venda = $id_venda;
		return $this;
	}

	/**
	 * @param field_type $dt_vencimento
	 */
	public function setDt_vencimento($dt_vencimento) {
		$this->dt_vencimento = $dt_vencimento;
		return $this;
	}

	/**
	 * @param \G2\Entity\decimal $nu_valorlancamento
	 */
	public function setNu_valorlancamento($nu_valorlancamento) {
		$this->nu_valorlancamento = $nu_valorlancamento;
		return $this;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @param number $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	/**
	 * @param field_type $dt_confirmacao
	 */
	public function setDt_confirmacao($dt_confirmacao) {
		$this->dt_confirmacao = $dt_confirmacao;
		return $this;
	}



}

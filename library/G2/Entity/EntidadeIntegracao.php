<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeintegracao")
 * @Entity(repositoryClass="G2\Repository\EntidadeIntegracao")
 * @HasLifecycleCallbacks
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class EntidadeIntegracao extends G2Entity
{

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(format="integer")
     */
    private $id_entidadeintegracao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false)
     */
    private $id_sistema;

    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=false)
     */
    private $st_codsistema;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var string $st_codchave
     * @Column(name="st_codchave", type="string", nullable=false)
     */
    private $st_codchave;

    /**
     * @var string $st_codarea
     * @Column(name="st_codarea", type="string", nullable=true, length=100)
     */
    private $st_codarea;

    /**
     * @var string $st_caminho
     * @Column(name="st_caminho", type="string", nullable=true)
     */
    private $st_caminho;

    /**
     * @var decimal $nu_perfilsuporte
     * @Column(name="nu_perfilsuporte", type="decimal", nullable=true)
     */
    private $nu_perfilsuporte;

    /**
     * @var decimal $nu_perfilprojeto
     * @Column(name="nu_perfilprojeto", type="decimal", nullable=true)
     */
    private $nu_perfilprojeto;

    /**
     * @var decimal $nu_perfildisciplina
     * @Column(name="nu_perfildisciplina", type="decimal", nullable=true)
     */
    private $nu_perfildisciplina;

    /**
     * @var decimal $nu_perfilobservador
     * @Column(name="nu_perfilobservador", type="decimal", nullable=true)
     */
    private $nu_perfilobservador;

    /**
     * @var decimal $nu_perfilalunoobs
     * @Column(name="nu_perfilalunoobs", type="decimal", nullable=true)
     */
    private $nu_perfilalunoobs;

    /**
     * @var decimal $nu_perfilalunoencerrado
     * @Column(name="nu_perfilalunoencerrado", type="decimal", nullable=true)
     */
    private $nu_perfilalunoencerrado;

    /**
     * @var decimal $nu_contexto
     * @Column(name="nu_contexto", type="decimal", nullable=true)
     */
    private $nu_contexto;

    /**
     * @var string $st_linkedserver
     * @Column(name="st_linkedserver", type="string", nullable=true, length=20)
     */
    private $st_linkedserver;

    /**
     * @var string $st_codchavewsuny
     * @Column(name="st_codchavewsuny", type="string", nullable=true)
     */
    private $st_codchavewsuny;

    /**
     * @var Recebedor $id_recebedor
     * @ManyToOne(targetEntity="Recebedor")
     * @JoinColumn(name="id_recebedor", nullable=false, referencedColumnName="id_recebedor")
     */
    private $id_recebedor;


    /**
     * @var boolean
     * @Column(type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var Situacao $id_recebedor
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $st_titulo;

    /** * @var boolean $bl_criarsala
     * @Column(name="bl_criarsala", type="boolean", nullable=true, length=1) */

    private $bl_criarsala;

    /**
     * @return bool
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return Situacao
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return string
     */
    public function getst_titulo()
    {
        return $this->st_titulo;
    }

    /**
     * @param string $st_titulo
     */
    public function setst_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
    }


    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_codchave()
    {
        return $this->st_codchave;
    }

    public function setSt_codchave($st_codchave)
    {
        $this->st_codchave = $st_codchave;
        return $this;
    }

    public function getSt_codarea()
    {
        return $this->st_codarea;
    }

    public function setSt_codarea($st_codarea)
    {
        $this->st_codarea = $st_codarea;
        return $this;
    }

    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
        return $this;
    }

    public function getNu_perfilsuporte()
    {
        return $this->nu_perfilsuporte;
    }

    public function setNu_perfilsuporte($nu_perfilsuporte)
    {
        $this->nu_perfilsuporte = $nu_perfilsuporte;
        return $this;
    }

    public function getNu_perfilprojeto()
    {
        return $this->nu_perfilprojeto;
    }

    public function setNu_perfilprojeto($nu_perfilprojeto)
    {
        $this->nu_perfilprojeto = $nu_perfilprojeto;
        return $this;
    }

    public function getNu_perfildisciplina()
    {
        return $this->nu_perfildisciplina;
    }

    public function setNu_perfildisciplina($nu_perfildisciplina)
    {
        $this->nu_perfildisciplina = $nu_perfildisciplina;
        return $this;
    }

    public function getNu_perfilobservador()
    {
        return $this->nu_perfilobservador;
    }

    public function setNu_perfilobservador($nu_perfilobservador)
    {
        $this->nu_perfilobservador = $nu_perfilobservador;
        return $this;
    }

    public function getNu_perfilalunoobs()
    {
        return $this->nu_perfilalunoobs;
    }

    public function setNu_perfilalunoobs($nu_perfilalunoobs)
    {
        $this->nu_perfilalunoobs = $nu_perfilalunoobs;
        return $this;
    }

    public function getNu_perfilalunoencerrado()
    {
        return $this->nu_perfilalunoencerrado;
    }

    public function setNu_perfilalunoencerrado($nu_perfilalunoencerrado)
    {
        $this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
        return $this;
    }

    public function getNu_contexto()
    {
        return $this->nu_contexto;
    }

    public function setNu_contexto($nu_contexto)
    {
        $this->nu_contexto = $nu_contexto;
        return $this;
    }

    public function getSt_linkedserver()
    {
        return $this->st_linkedserver;
    }

    public function setSt_linkedserver($st_linkedserver)
    {
        $this->st_linkedserver = $st_linkedserver;
        return $this;
    }

    public function getSt_codchavewsuny()
    {
        return $this->st_codchavewsuny;
    }

    public function setSt_codchavewsuny($st_codchavewsuny)
    {
        $this->st_codchavewsuny = $st_codchavewsuny;
    }


    /**
     * @PrePersist
     */
    public function prePersist()
    {
        $this->setDt_cadastro(new \DateTime());
        return $this;
    }

    /**
     * @return Recebedor
     */
    public function getId_recebedor()
    {
        return $this->id_recebedor;
    }

    /**
     * @param Recebedor $id_recebedor
     */
    public function setId_recebedor($id_recebedor)
    {
        $this->id_recebedor = $id_recebedor;
    }

    /**
     * @return bool
     */
    public function getBl_criarsala()
    {
        return $this->bl_criarsala;
    }

    /**
     * @param bool $bl_criarsala
     */
    public function setBl_criarsala($bl_criarsala)
    {
        $this->bl_criarsala = $bl_criarsala;
    }

}

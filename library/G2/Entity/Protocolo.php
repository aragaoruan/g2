<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 18/11/13
 * Time: 15:00
 */

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_protocolo")
 */
class Protocolo
{

    /**
     * @var integer $id_protocolo
     * @Column(name="id_protocolo", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     * @Id
     */
    private $id_protocolo;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var mixed $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @return int
     */
    public function getid_protocolo()
    {
        return $this->id_protocolo;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdProtocolo()
    {
        return $this->id_protocolo;
    }

    /**
     * @param int $id_protocolo
     * @return Protocolo
     */
    public function setid_protocolo($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
        return $this;
    }

    /**
     * @param int $id_protocolo
     * @deprecated
     */
    public function setIdProtocolo($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return Protocolo
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param int $id_usuariocadastro
     * @deprecated
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return Protocolo
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param int $id_entidade
     * @deprecated
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return Protocolo
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @deprecated
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }
} 
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_arquivoretornomaterial")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class ArquivoRetornoMaterial {


    CONST ATIVO = 161;
    CONST INATIVO = 162;
    /**
     * @Id
     * @var integer $id_arquivoretornomaterial
     * @Column(name="id_arquivoretornomaterial", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_arquivoretornomaterial;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;


    /**
     * @var string $st_arquivoretornomaterial
     * @Column(name="st_arquivoretornomaterial", type="string",  nullable=false, length=500)
     */
    private $st_arquivoretornomaterial;

    /**
     * @var datetime $dt_cadastro
     * @Column(type="datetime", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;


    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var Upload $id_upload
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", referencedColumnName="id_upload")
     */
    private $id_upload;

    public function getId_arquivoretornomaterial()
    {
        return $this->id_arquivoretornomaterial;
    }

    public function setId_arquivoretornomaterial($id_arquivoretornomaterial)
    {
        $this->id_arquivoretornomaterial = $id_arquivoretornomaterial;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getSt_arquivoretornomaterial()
    {
        return $this->st_arquivoretornomaterial;
    }

    public function setSt_arquivoretornomaterial($st_arquivoretornomaterial)
    {
        $this->st_arquivoretornomaterial = $st_arquivoretornomaterial;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getId_upload()
    {
        return $this->id_upload;
    }

    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
    }





}

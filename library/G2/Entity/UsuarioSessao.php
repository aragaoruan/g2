<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuariosessao")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class UsuarioSessao{
    
    /**
     * Entity Usuario
     * @Id
     * @var integer $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var datetime2 $dt_sessao
     * @Column(name="dt_sessao", type="datetime", nullable=true, length=8)
     * 
     */
    private $dt_sessao;

    /**
     * @var string $st_hash
     * @Column(name="st_hash", type="string", nullable=true, length=155)
     */
    private $st_hash;

    /**
     * @return datetime2 dt_sessao
     */
    public function getDt_sessao() {
        return $this->dt_sessao;
    }

    /**
     * @param dt_sessao
     */
    public function setDt_sessao($dt_sessao) {
        $this->dt_sessao = $dt_sessao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string st_hash
     */
    public function getSt_hash() {
        return $this->st_hash;
    }

    /**
     * @param st_hash
     */
    public function setSt_hash($st_hash) {
        $this->st_hash = $st_hash;
        return $this;
    }

}

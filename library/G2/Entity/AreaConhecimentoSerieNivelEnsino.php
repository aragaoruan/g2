<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_areaconhecimentoserienivelensino")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AreaConhecimentoSerieNivelEnsino
{

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_serie;

    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=false)
     */
    private $id_nivelensino;

    public function __construct ()
    {
        $this->id_areaconhecimento = new ArrayCollection();
    }

    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
    }

    public function getId_serie ()
    {
        return $this->id_serie;
    }

}
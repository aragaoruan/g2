<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmaprojeto")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class TurmaProjeto
{

    /**
     * @Id
     * @var integer $id_turmaprojeto
     * @Column(name="id_turmaprojeto", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turmaprojeto;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var integer $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;


    public function getId_turmaprojeto(){
        return $this->id_turmaprojeto;
    }

    public function getId_usuariocadastro(){
        return $this->id_usuariocadastro;
    }

    public function getId_turma(){
        return $this->id_turma;
    }

    public function getId_projetopedagogico(){
        return $this->id_projetopedagogico;
    }

    public function getBl_ativo(){
        return $this->bl_ativo;
    }

    public function getDt_cadastro(){
        return $this->dt_cadastro;
    }


    public function setId_turmaprojeto($id_turmaprojeto){
        $this->id_turmaprojeto = $id_turmaprojeto;
    }

    public function setId_usuariocadastro($id_usuariocadastro){
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setId_turma($id_turma){
        $this->id_turma = $id_turma;
    }

    public function setId_projetopedagogico($id_projetopedagogico){
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setBl_ativo($bl_ativo){
        $this->bl_ativo = $bl_ativo;
    }

    public function setDt_cadastro($dt_cadastro){
        $this->dt_cadastro = $dt_cadastro;
    }

}
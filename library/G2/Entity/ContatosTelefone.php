<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_contatostelefone")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ContatosTelefone
{

    /**
     * @var integer $id_telefone
     * @Column(name="id_telefone", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_telefone;

    /**
     * @var TipoTelefone $id_tipotelefone
     * @ManyToOne(targetEntity="TipoTelefone")
     * @JoinColumn(name="id_tipotelefone", nullable=true, referencedColumnName="id_tipotelefone")
     */
    private $id_tipotelefone;

    /**
     * @var integer $nu_ddi
     * @Column(name="nu_ddi", type="integer", nullable=true)
     */
    private $nu_ddi;

    /**
     * @var string $nu_telefone
     * @Column(name="nu_telefone", type="string", nullable=true, length=15)
     */
    private $nu_telefone;

    /**
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=true)
     */
    private $st_descricao;

    /**
     * @var string $nu_ddd
     * @Column(name="nu_ddd", type="string", nullable=true, length=3)
     */
    private $nu_ddd;


    /**
     * @return integer
     */
    public function getId_telefone()
    {
        return $this->id_telefone;
    }

    /**
     *
     * @param integer $id_telefone
     * @return \G2\Entity\ContatosTelefone
     */
    public function setId_telefone($id_telefone)
    {
        $this->id_telefone = $id_telefone;
        return $this;
    }

    /**
     * @return \G2\Entity\TipoTelefone
     */
    public function getId_tipotelefone()
    {
        return $this->id_tipotelefone;
    }

    /**
     * @param \G2\Entity\TipoTelefone $id_tipotelefone
     * @return \G2\Entity\ContatosTelefone
     */
    public function setId_tipotelefone(TipoTelefone $id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_ddi()
    {
        return $this->nu_ddi;
    }

    /**
     * @param integer $nu_ddi
     * @return \G2\Entity\ContatosTelefone
     */
    public function setNu_ddi($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
        return $this;
    }

    /**
     * @return string
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     *
     * @param string $nu_telefone
     * @return \G2\Entity\ContatosTelefone
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return \G2\Entity\ContatosTelefone
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    /**
     * @param integer $nu_ddd
     * @return \G2\Entity\ContatosTelefone
     */
    public function setNu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

}

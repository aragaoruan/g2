<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoprodutovalor")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class TipoProdutoValor extends G2Entity
{

    /**
     *
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoprodutovalor;

    /**
     *
     * @var string $st_descricaovalor
     * @Column(name="st_descricaovalor", type="string", nullable=true, length=250) 
     */
    private $st_descricaovalor;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=250) 
     */
    private $st_descricao;

    public function getId_tipoprodutovalor() {
        return $this->id_tipoprodutovalor;
    }

    public function setId_tipoprodutovalor($id_tipoprodutovalor) {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    public function getSt_descricaovalor() {
        return $this->st_descricaovalor;
    }

    public function setSt_descricaovalor($st_descricaovalor) {
        $this->st_descricaovalor = $st_descricaovalor;
        return $this;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
        return $this;
    }


}
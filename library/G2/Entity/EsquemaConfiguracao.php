<?php

namespace G2\Entity;

/**
 * @Table(name="tb_esquemaconfiguracao")
 * @Entity(repositoryClass="\G2\Repository\EsquemaConfiguracao")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 * @HasLifecycleCallbacks
 * @EntityLog
 */
class EsquemaConfiguracao
{

    /**
     * @var integer $id_esquemaconfiguracao
     * @Column(name="id_esquemaconfiguracao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_esquemaconfiguracao;

    /**
     *
     * @var string $st_esquemaconfiguracao
     * @Column(name="st_esquemaconfiguracao", type="string", nullable=false, length=200)
     */
    private $st_esquemaconfiguracao;

    /**
     * @var Usuario
     *
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Usuario
     *
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioatualizacao", referencedColumnName="id_usuario")
     */
    private $id_usuarioatualizacao;

    /**
     * @var datetime $dt_atualizacao
     * @Column(name="dt_atualizacao", type="datetime", nullable=true, length=8)
     */
    private $dt_atualizacao;

    /**
     * @return int
     */
    public function getid_esquemaconfiguracao()
    {
        return $this->id_esquemaconfiguracao;
    }

    /**
     * @param int $id_esquemaconfiguracao
     */
    public function setid_esquemaconfiguracao($id_esquemaconfiguracao)
    {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_esquemaconfiguracao()
    {
        return $this->st_esquemaconfiguracao;
    }

    /**
     * @param string $st_esquemaconfiguracao
     */
    public function setst_esquemaconfiguracao($st_esquemaconfiguracao)
    {
        $this->st_esquemaconfiguracao = $st_esquemaconfiguracao;
        return $this;

    }


    /**
     * @return datetime
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }


    /**
     * @return datetime
     */
    public function getdt_atualizacao()
    {
        return $this->dt_atualizacao;
    }

    /**
     * @param datetime $dt_atualizacao
     */
    public function setdt_atualizacao($dt_atualizacao)
    {
        $this->dt_atualizacao = $dt_atualizacao;
        return $this;

    }

    /**
     * @return Usuario
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;

    }

    /**
     * @return Usuario
     */
    public function getid_usuarioatualizacao()
    {
        return $this->id_usuarioatualizacao;
    }

    /**
     * @param Usuario $id_usuarioatualizacao
     */
    public function setid_usuarioatualizacao($id_usuarioatualizacao)
    {
        $this->id_usuarioatualizacao = $id_usuarioatualizacao;
        return $this;

    }

    /**
     * @PrePersist
     */
    public function prePersist() {
        $this->setdt_cadastro(new \DateTime());
        $this->setdt_atualizacao(new \DateTime());
        return $this;
    }

    /**
     * @PreUpdate
     */
    public function preUpdate(){
        $this->setdt_atualizacao(new \DateTime());
        return $this;
    }

}
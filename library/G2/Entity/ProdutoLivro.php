<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtolivro")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class ProdutoLivro
{


    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @Id
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    /**
     *
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;
   
    /**
     *
     * @var integer $id_livro
     * @ManyToOne(targetEntity="Livro")
     * @JoinColumn(name="id_livro", nullable=false, referencedColumnName="id_livro")
     */
    private $id_livro;
    
    
	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_livro
	 */
	public function getId_livro() {
		return $this->id_livro;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @param number $id_livro
	 */
	public function setId_livro($id_livro) {
		$this->id_livro = $id_livro;
		return $this;
	}


}
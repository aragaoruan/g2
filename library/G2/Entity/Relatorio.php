<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_relatorio")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class Relatorio
{

    /**
     *
     * @var integer $id_relatorio
     * @Column(name="id_relatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_relatorio;

    /**
     * @var TipoOrigemRelatorio $id_tipoorigemrelatorio
     * @ManyToOne(targetEntity="TipoOrigemRelatorio")
     * @JoinColumn(name="id_tipoorigemrelatorio", nullable=false, referencedColumnName="id_tipoorigemrelatorio")
     */
    private $id_tipoorigemrelatorio;

    /**
     * @var Funcionalidade $id_funcionalidade
     * @ManyToOne(targetEntity="Funcionalidade")
     * @JoinColumn(name="id_funcionalidade", nullable=false, referencedColumnName="id_funcionalidade")
     */
    private $id_funcionalidade;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var string $st_relatorio
     * @Column(name="st_relatorio", type="string", length=250, nullable=false)
     */
    private $st_relatorio;

    /**
     *
     * @var text $st_origem
     * @Column(name="st_origem", type="text", nullable=true)
     */
    private $st_origem;

    /**
     *
     * @var boolean $bl_geral
     * @Column(name="bl_geral", type="boolean", nullable=false)
     */
    private $bl_geral;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=false)
     */
    private $st_descricao;

    public function getId_relatorio ()
    {
        return $this->id_relatorio;
    }

    public function setId_relatorio ($id_relatorio)
    {
        $this->id_relatorio = $id_relatorio;
        return $this;
    }

    public function getId_tipoorigemrelatorio ()
    {
        return $this->id_tipoorigemrelatorio;
    }

    public function setId_tipoorigemrelatorio ($id_tipoorigemrelatorio)
    {
        $this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
        return $this;
    }

    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_relatorio ()
    {
        return $this->st_relatorio;
    }

    public function setSt_relatorio ($st_relatorio)
    {
        $this->st_relatorio = $st_relatorio;
        return $this;
    }

    public function getSt_origem ()
    {
        return $this->st_origem;
    }

    public function setSt_origem ($st_origem)
    {
        $this->st_origem = $st_origem;
        return $this;
    }

    public function getBl_geral ()
    {
        return $this->bl_geral;
    }

    public function setBl_geral ($bl_geral)
    {
        $this->bl_geral = $bl_geral;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}

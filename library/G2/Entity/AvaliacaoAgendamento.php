<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoagendamento")
 * @Entity(repositoryClass="\G2\Repository\AvaliacaoAgendamento")
 * @EntityLog
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @update Denise Xavier <denise.xavier@unyleya.com.br> Add Repository
 */
class AvaliacaoAgendamento extends G2Entity
{

    //situações do agendamento
    const AGENDADO = 68;
    const REAGENDADO = 69;
    const CANCELADO = 70;
    const APTO = 120;
    const LIBERADO = 129;
    const ABONADO = 130;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @ManyToOne(targetEntity="AvaliacaoAplicacao")
     * @JoinColumn(name="id_avaliacaoaplicacao", referencedColumnName="id_avaliacaoaplicacao",nullable=true)
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Avaliacao $id_avaliacao
     * @ManyToOne(targetEntity="Avaliacao")
     * @JoinColumn(name="id_avaliacao", referencedColumnName="id_avaliacao")
     */
    private $id_avaliacao;

    /**
     * @Column(name="dt_agendamento", type="date", nullable=true)
     * @var date $dt_agendamento
     */
    private $dt_agendamento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2",  nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @Column(name="nu_presenca", type="integer", nullable=true)
     * @var integer $nu_presenca
     */
    private $nu_presenca;

    /**
     * @Column(name="id_tipodeavaliacao", type="integer", nullable=true)
     * @var integer $id_tipodeavaliacao
     */
    private $id_tipodeavaliacao;

    /**
     * @Column(name="bl_provaglobal", type="integer", nullable=true)
     * @var integer $bl_provaglobal
     */
    private $bl_provaglobal;
    /**
     * @Column(name="id_usuariolancamento", type="integer", nullable=true)
     * @var integer $id_usuariolancamento
     */
    private $id_usuariolancamento;

    /**
     * @Column(name="bl_automatico", type="boolean", nullable=false)
     * @var boolean $bl_automatico
     */
    private $bl_automatico;

    /**
     * @var date $dt_envioaviso
     * @Column(name="dt_envioaviso", type="date", nullable=true)
     */
    private $dt_envioaviso;

    /**
     * @var datetime2 $dt_sincronizado
     * @Column(name="dt_sincronizado", type="datetime2",  nullable=true, length=8)
     */
    private $dt_sincronizado;

    /**
     * @var boolean $bl_sincronizado
     * @Column(name="bl_sincronizado", type="boolean",  nullable=false, length=1)
     */
    private $bl_sincronizado = 0;

    function __construct()
    {
        $this->id_usuariocadastro = new Usuario();
        $this->id_usuario = new Usuario();
        $this->id_matricula = new Matricula();
        $this->id_entidade = new Entidade();
        $this->bl_automatico = false;
    }

    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getNu_presenca()
    {
        return $this->nu_presenca;
    }

    public function getId_tipodeavaliacao()
    {
        return $this->id_tipodeavaliacao;
    }

    public function getBl_provaglobal()
    {
        return $this->bl_provaglobal;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setNu_presenca($nu_presenca)
    {
        $this->nu_presenca = $nu_presenca;
        return $this;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao)
    {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
        return $this;
    }

    public function setBl_provaglobal($bl_provaglobal)
    {
        $this->bl_provaglobal = $bl_provaglobal;
        return $this;
    }

    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_automatico()
    {
        return $this->bl_automatico;
    }

    /**
     * @param boolean $bl_automatico
     * @return $this
     */
    public function setBl_automatico($bl_automatico)
    {
        $this->bl_automatico = $bl_automatico;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_sincronizado()
    {
        return $this->dt_sincronizado;
    }

    /**
     * @param datetime2 $dt_sincronizado
     * @return $this
     */
    public function setDt_sincronizado($dt_sincronizado)
    {
        $this->dt_sincronizado = $dt_sincronizado;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBl_sincronizado()
    {
        return $this->bl_sincronizado;
    }

    /**
     * @param bool $bl_sincronizado
     * @return $this
     */
    public function setBl_sincronizado($bl_sincronizado)
    {
        $this->bl_sincronizado = $bl_sincronizado;
        return $this;
    }

    public function getDt_envioaviso()
    {
        return $this->dt_envioaviso;
    }

    public function setDt_envioaviso($dt_envioaviso)
    {
        $this->dt_envioaviso = $dt_envioaviso;
    }


}

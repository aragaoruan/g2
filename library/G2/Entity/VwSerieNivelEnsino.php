<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_serienivelensino")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwSerieNivelEnsino
{

    /**
     * @var integer $id_serie
     * @Column(type="integer", nullable=false, name="id_serie")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_serie;

    /**
     *
     * @var string $id_serie 
     * @Column(type="string", length=255,nullable=false, name="st_serie")
     */
    private $st_serie;

    /**
     *
     * @var integer $id_serieanterior
     * @Column(type="integer", nullable=true, name="id_serieanterior")
     */
    private $id_serieanterior;

    /**
     *
     * @var integer $id_nivelensino
     * @Column(type="integer", nullable=false, name="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     *
     * @var string $st_nivelensino
     * @Column(type="string", nullable=false, length=255, name="st_nivelensino")
     */
    private $st_nivelensino;

    public function getId_serie ()
    {
        return $this->id_serie;
    }

    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    public function getSt_serie ()
    {
        return $this->st_serie;
    }

    public function setSt_serie ($st_serie)
    {
        $this->st_serie = $st_serie;
        return $this;
    }

    public function getId_serieanterior ()
    {
        return $this->id_serieanterior;
    }

    public function setId_serieanterior ($id_serieanterior)
    {
        $this->id_serieanterior = $id_serieanterior;
        return $this;
    }

    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    public function getSt_nivelensino ()
    {
        return $this->st_nivelensino;
    }

    public function setSt_nivelensino ($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    /**
     * @return array Array for attributes of the entity
     */
    public function _toArray ()
    {
        return array(
            'id_serie' => $this->id_serie,
            'st_serie' => $this->st_serie,
            'id_serieanterior' => $this->id_serieanterior,
            'id_nivelensino' => $this->id_nivelensino,
            'st_nivelensino' => $this->st_nivelensino
        );
    }

}
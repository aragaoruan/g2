<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_gradehoraria")
 * @Entity(repositoryClass="\G2\Repository\GradeHoraria")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class GradeHoraria
{

    /**
     * @var integer $id_gradehoraria
     * @Column(name="id_gradehoraria", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_gradehoraria;

    /**
     * @var string $st_nomegradehoraria
     * @Column(name="st_nomegradehoraria", type="string", nullable=false, length=100)
     */
    private $st_nomegradehoraria;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao", nullable=false)
     */
    private $id_situacao;

    /**
     * @var datetime $dt_iniciogradehoraria
     * @Column(name="dt_iniciogradehoraria", type="datetime", nullable=false)
     */
    private $dt_iniciogradehoraria;

    /**
     * @var datetime $dt_fimgradehoraria
     * @Column(name="dt_fimgradehoraria", type="datetime", nullable=true)
     */
    private $dt_fimgradehoraria;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8, options={"default":NULL})
     */
    private $dt_cadastro;

    /**
     * @var Entidade $id_situacao
     * @ManyToOne(targetEntity="Entidade", fetch="EAGER")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false )
     */
    private $id_entidade;

    /**
     * @var boolean $bl_sincronizado
     * @Column(name="bl_sincronizado", type="boolean", nullable=false, length=1)
     */
    private $bl_sincronizado;

    /**
     * @return boolean
     */
    public function getBl_sincronizado ()
    {
        return $this->bl_sincronizado;
    }

    /**
     * @param boolean $bl_sincronizado
     * @return $this
     */
    public function setBl_sincronizado ($bl_sincronizado)
    {
        $this->bl_sincronizado = $bl_sincronizado;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_gradehoraria ()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @return string
     */
    public function getSt_nomegradehoraria ()
    {
        return $this->st_nomegradehoraria;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @return datetime
     */
    public function getDt_iniciogradehoraria ()
    {
        return $this->dt_iniciogradehoraria;
    }

    /**
     * @return datetime
     */
    public function getDt_fimgradehoraria ()
    {
        return $this->dt_fimgradehoraria;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param integer $id_gradehoraria
     * @return $this
     */
    public function setId_gradehoraria ($id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

    /**
     * @param string $st_nomegradehoraria
     * @return $this
     */
    public function setSt_nomegradehoraria ($st_nomegradehoraria)
    {
        $this->st_nomegradehoraria = $st_nomegradehoraria;
        return $this;
    }

    /**
     * @param Situacao $id_situacao
     * @return $this
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param datetime $dt_iniciogradehoraria
     * @return $this
     */
    public function setDt_iniciogradehoraria ($dt_iniciogradehoraria)
    {
        $this->dt_iniciogradehoraria = $dt_iniciogradehoraria;
        return $this;
    }

    /**
     * @param datetime $dt_fimgradehoraria
     * @return $this
     */
    public function setDt_fimgradehoraria ($dt_fimgradehoraria)
    {
        $this->dt_fimgradehoraria = $dt_fimgradehoraria;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/06/2015
 * Time: 17:31
 */

namespace G2\Entity;

/**
 * @Table (name="tb_premio")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br> 2015-06-19
 */
class Premio
{
    /**
     * @Id
     * @var integer $id_premio
     * @Column(name="id_premio", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_premio;
    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var CampanhaComercial $id_campanhacomercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_campanhacomercial;

    /**
     * @var CampanhaComercial $id_cupomcampanha
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_cupomcampanha", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_cupomcampanha;

    /**
     * @var decimal $nu_comprasacima
     * @Column(name="nu_comprasacima", type="decimal", nullable=true, length=4)
     */
    private $nu_comprasacima;
    /**
     * @var integer $nu_compraunidade
     * @Column(name="nu_compraunidade", type="integer", nullable=true, length=4)
     */
    private $nu_compraunidade;
    /**
     * @var decimal $nu_descontopremio
     * @Column(name="nu_descontopremio", type="decimal", nullable=true, length=4)
     */
    private $nu_descontopremio;
    /**
     * @var integer $id_tipodescontopremio
     * @Column(name="id_tipodescontopremio", type="integer", nullable=true, length=4)
     */
    private $id_tipodescontopremio;
    /**
     * @var integer $id_tipopremio
     * @Column(name="id_tipopremio", type="integer", nullable=true, length=4)
     */
    private $id_tipopremio;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var boolean $bl_compraacima
     * @Column(name="bl_compraacima", type="boolean", nullable=true, length=1)
     */
    private $bl_compraacima;
    /**
     * @var boolean $bl_promocaocumulativa
     * @Column(name="bl_promocaocumulativa", type="boolean", nullable=true, length=1)
     */
    private $bl_promocaocumulativa;
    /**
     * @var boolean $bl_compraunidade
     * @Column(name="bl_compraunidade", type="boolean", nullable=true, length=1)
     */
    private $bl_compraunidade;
    /**
     * @var string $st_prefixocupompremio
     * @Column(name="st_prefixocupompremio", type="string", nullable=true, length=30)
     */
    private $st_prefixocupompremio;


    /**
     * @return integer id_premio
     */
    public function getId_premio()
    {
        return $this->id_premio;
    }

    /**
     * @param id_premio
     */
    public function setId_premio($id_premio)
    {
        $this->id_premio = $id_premio;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_campanhacomercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @return decimal nu_comprasacima
     */
    public function getNu_comprasacima()
    {
        return $this->nu_comprasacima;
    }

    /**
     * @param nu_comprasacima
     */
    public function setNu_comprasacima($nu_comprasacima)
    {
        $this->nu_comprasacima = $nu_comprasacima;
        return $this;
    }

    /**
     * @return integer nu_compraunidade
     */
    public function getNu_compraunidade()
    {
        return $this->nu_compraunidade;
    }

    /**
     * @param nu_compraunidade
     */
    public function setNu_compraunidade($nu_compraunidade)
    {
        $this->nu_compraunidade = $nu_compraunidade;
        return $this;
    }

    /**
     * @return decimal nu_descontopremio
     */
    public function getNu_descontopremio()
    {
        return $this->nu_descontopremio;
    }

    /**
     * @param nu_descontopremio
     */
    public function setNu_descontopremio($nu_descontopremio)
    {
        $this->nu_descontopremio = $nu_descontopremio;
        return $this;
    }

    /**
     * @return integer id_tipodescontopremio
     */
    public function getId_tipodescontopremio()
    {
        return $this->id_tipodescontopremio;
    }

    /**
     * @param id_tipodescontopremio
     */
    public function setId_tipodescontopremio($id_tipodescontopremio)
    {
        $this->id_tipodescontopremio = $id_tipodescontopremio;
        return $this;
    }

    /**
     * @return integer id_tipopremio
     */
    public function getId_tipopremio()
    {
        return $this->id_tipopremio;
    }

    /**
     * @param id_tipopremio
     */
    public function setId_tipopremio($id_tipopremio)
    {
        $this->id_tipopremio = $id_tipopremio;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return boolean bl_compraacima
     */
    public function getBl_compraacima()
    {
        return $this->bl_compraacima;
    }

    /**
     * @param bl_compraacima
     */
    public function setBl_compraacima($bl_compraacima)
    {
        $this->bl_compraacima = $bl_compraacima;
        return $this;
    }

    /**
     * @return boolean bl_compraunidade
     */
    public function getBl_compraunidade()
    {
        return $this->bl_compraunidade;
    }
    /**
     * @return boolean bl_promocaocumulativa
     */
    public function getBl_promocaocumulativa()
    {
        return $this->bl_promocaocumulativa;
    }

    /**
     * @param bl_compraunidade
     */
    public function setBl_compraunidade($bl_compraunidade)
    {
        $this->bl_compraunidade = $bl_compraunidade;
        return $this;
    }
    /**
     * @param bl_promocaocumulativa
     */
    public function setBl_promocaocumulativa($bl_promocaocumulativa)
    {
        $this->bl_promocaocumulativa = $bl_promocaocumulativa;
        return $this;
    }

    /**
     * @return string st_prefixocupompremio
     */
    public function getSt_prefixocupompremio()
    {
        return $this->st_prefixocupompremio;
    }

    /**
     * @param st_prefixocupompremio
     */
    public function setSt_prefixocupompremio($st_prefixocupompremio)
    {
        $this->st_prefixocupompremio = $st_prefixocupompremio;
        return $this;
    }

    /**
     * @param id_cupomcampanha
     */
    public function setId_cupomcampanha($id_cupomcampanha) {
        $this->id_cupomcampanha = $id_cupomcampanha;
        return $this;
    }

    /**
     * @return integer id_cupomcampanha
     */
    public function getId_cupomcampanha() {
        return $this->id_cupomcampanha;
    }
}
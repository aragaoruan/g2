<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\ManyToOne;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_holding")
 * @Entity(repositoryClass="\G2\Repository\Holding")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */


class Holding
{
    /**
     * @var integer $id_holding
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_holding", type="integer", nullable=false, length=4)
     */
    private $id_holding;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var string $st_holding
     * @Column(name="st_holding", type="string", nullable=false, length=100)
     */
    private $st_holding;
    /**
     * @var boolean $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="boolean", nullable=false, length=1)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_compartilharcarta
     * @Column(name="bl_compartilharcarta", type="boolean", nullable=false, length=1)
     */
    private $bl_compartilharcarta;

    /**
     * @return integer id_holding
     */
    public function getId_holding()
    {
        return $this->id_holding;
    }

    /**
     * @param id_holding
     */
    public function setId_holding($id_holding)
    {
        $this->id_holding = $id_holding;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return string st_holding
     */
    public function getSt_holding()
    {
        return $this->st_holding;
    }

    /**
     * @param st_holding
     */
    public function setSt_holding($st_holding)
    {
        $this->st_holding = $st_holding;
        return $this;
    }

    /**
     * @return string id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_compartilharcarta()
    {
        return $this->bl_compartilharcarta;
    }

    /**
     * @param boolean $bl_compartilhar
     */
    public function setBl_compartilharcarta($bl_compartilharcarta)
    {
        $this->bl_compartilharcarta = $bl_compartilharcarta;
    }


} 
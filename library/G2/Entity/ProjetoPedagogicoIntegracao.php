<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_projetopedagogicointegracao")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class ProjetoPedagogicoIntegracao
{

    /**
     * @Id
     * @var integer $id_projetointegracao
     * @Column(name="id_projetointegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_projetointegracao;

    /**
     * @var integer $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico", nullable=false)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_projetopedagogicosistema
     * @Column(name="id_projetopedagogicosistema", type="integer", nullable=false)
     */
    private $id_projetopedagogicosistema;

    /**
     * @var integer $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @return int
     */
    public function getId_projetointegracao()
    {
        return $this->id_projetointegracao;
    }

    /**
     * @param int $id_projetointegracao
     */
    public function setId_projetointegracao($id_projetointegracao)
    {
        $this->id_projetointegracao = $id_projetointegracao;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogicosistema()
    {
        return $this->id_projetopedagogicosistema;
    }

    /**
     * @param int $id_projetopedagogicosistema
     */
    public function setId_projetopedagogicosistema($id_projetopedagogicosistema)
    {
        $this->id_projetopedagogicosistema = $id_projetopedagogicosistema;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunossala")
 * @Entity
 * @EntityView
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 */
class VwAlunosSala extends G2Entity
{
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer")
     * @Id
     */
    private $id_alocacao;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer")
     */
    private $id_saladeaula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string")
     */
    private $st_email;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     * @Id
     */
    private $id_matricula;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer")
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer")
     * @Id
     */
    private $id_matriculadisciplina;

    /**
     * @var float $st_nota
     * @Column(name="st_nota", type="float")
     */
    private $st_nota;

    /**
     * @var integer $id_logacesso
     * @Column(name="id_logacesso", type="integer")
     */
    private $id_logacesso;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer")
     */
    private $id_sistema;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var string $st_urlacesso
     * @Column(name="st_urlacesso", type="string")
     */
    private $st_urlacesso;

    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer")
     */
    private $id_situacaotcc;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer")
     */
    private $id_upload;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_professor
     * @Column(name="id_professor", type="integer")
     */
    private $id_professor;

    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string")
     */
    private $st_professor;

    /**
     * @var integer $id_emailprofessor
     * @Column(name="id_emailprofessor", type="integer")
     */
    private $id_emailprofessor;

    /**
     * @var string $st_emailprofessor
     * @Column(name="st_emailprofessor", type="string")
     */
    private $st_emailprofessor;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer")
     */
    private $id_entidadematricula;
}

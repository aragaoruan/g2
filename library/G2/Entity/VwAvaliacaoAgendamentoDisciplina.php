<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoagendamentodisciplina")
 * @Entity
 * @EntityView
 */
class VwAvaliacaoAgendamentoDisciplina extends G2Entity
{
    /**
     * @Id
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false, length=4)
     *
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false, length=4)
     */
    private $id_avaliacao;

    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer", nullable=false, length=4)
     */
    private $id_tipodeavaliacao;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true, length=4)
     */
    private $bl_provaglobal;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_presenca
     * @Column(name="nu_presenca", type="integer", nullable=true, length=4)
     */
    private $nu_presenca;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false, length=4)
     */
    private $id_tipoavaliacao;

    /**
     * @var boolean $bl_automatico
     * @Column(name="bl_automatico", type="boolean", nullable=false, length=1)
     */
    private $bl_automatico;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    /**
     * @param int $id_avaliacaoagendamento
     * @return $this
     */
    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @param int $id_avaliacao
     * @return $this
     */
    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoprova()
    {
        return $this->id_tipoprova;
    }

    /**
     * @param int $id_tipoprova
     * @return $this
     */
    public function setId_tipoprova($id_tipoprova)
    {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodeavaliacao()
    {
        return $this->id_tipodeavaliacao;
    }

    /**
     * @param int $id_tipodeavaliacao
     * @return $this
     */
    public function setId_tipodeavaliacao($id_tipodeavaliacao)
    {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getBl_provaglobal()
    {
        return $this->bl_provaglobal;
    }

    /**
     * @param int $bl_provaglobal
     * @return $this
     */
    public function setBl_provaglobal($bl_provaglobal)
    {
        $this->bl_provaglobal = $bl_provaglobal;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param int $id_avaliacaoconjuntoreferencia
     * @return $this
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @param int $id_avaliacaoaplicacao
     * @return $this
     */
    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_presenca()
    {
        return $this->nu_presenca;
    }

    /**
     * @param int $nu_presenca
     * @return $this
     */
    public function setNu_presenca($nu_presenca)
    {
        $this->nu_presenca = $nu_presenca;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param int $id_tipoavaliacao
     * @return $this
     */
    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_automatico()
    {
        return $this->bl_automatico;
    }

    /**
     * @param bool $bl_automatico
     * @return $this
     */
    public function setBl_automatico($bl_automatico)
    {
        $this->bl_automatico = $bl_automatico;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }
}

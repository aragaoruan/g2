<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turma")
 * @Entity(repositoryClass="\G2\Repository\Turma")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Turma extends G2Entity
{

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turma;

    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true, length=3)
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=3)
     */
    private $dt_fiminscricao;

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=true, length=3)
     */
    private $dt_inicio;

    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true, length=3)
     */
    private $dt_fim;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var integer $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;

    /**
     * @var integer $nu_maxfreepass
     * @Column(name="nu_maxfreepass", type="integer", nullable=true, length=4)
     */
    private $nu_maxfreepass;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=150)
     */
    private $st_turma;

    /**
     * @var string $st_codigo
     * @Column(name="st_codigo", type="string", nullable=true, length=20)
     */
    private $st_codigo;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=200)
     */
    private $st_tituloexibicao;

    /**
     * @return string
     */
    public function getst_codigo()
    {
        return $this->st_codigo;
    }

    /**
     * @param string $st_codigo
     */
    public function setst_codigo($st_codigo)
    {
        $this->st_codigo = $st_codigo;
        return $this;
    }


    /**
     *
     * @return integer
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param $id_turma
     * @return $this
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param $dt_inicioinscricao
     * @return $this
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param $dt_fiminscricao
     * @return $this
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param $dt_inicio
     * @return $this
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param $dt_fim
     * @return $this
     */
    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param $id_evolucao
     */
    function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }


    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param $nu_maxalunos
     * @return $this
     */
    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param $st_turma
     * @return $this
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param $st_tituloexibicao
     * @return $this
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_maxfreepass()
    {
        return $this->nu_maxfreepass;
    }

    /**
     * @param int $nu_maxfreepass
     */
    public function setNu_maxfreepass($nu_maxfreepass)
    {
        $this->nu_maxfreepass = $nu_maxfreepass;
    }


}
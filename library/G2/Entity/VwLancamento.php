<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_lancamento")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwLancamento extends G2Entity {

    /**
     * Contem o id do lancamento
     * @var int
     * @Id
     * @Column(type="integer")
     */
    private $id_lancamento;

    /**
     * Contem o valor se o lançamento é uma entrada
     * @var boolean
     * @Column(type="boolean")
     */
    private $bl_entrada;

    /**
     * Contem o id da venda
     * @var int
     * @Column(type="integer", name="id_venda")
     */
    private $id_venda;

    /**
     * Contem o valor do lancamento
     * @var numeric
     * @Column(type="float")
     */
    private $nu_valor;

    /**
     * Contem o vencimento?
     * @var numeric
     * @Column(type="float")
     */
    private $nu_vencimento;

    /**
     * Contem o id do tipo de lancamento
     * @var int
     * @Column(type="integer")
     */
    private $id_tipolancamento;

    /**
     * Contem o nome do tipo de lancamento
     * @var string
     * @Column(type="string")
     */
    private $st_tipolancamento;

    /**
     * Contem a data de cadastro
     * @var zend_date
     * @Column(type="datetime2")
     */
    private $dt_cadastro;

    /**
     * Contem o id do meio de pagamento
     * @var int
     * @Column(type="integer")
     */
    private $id_meiopagamento;

    /**
     * Contem o nome do meio de pagamento
     * @var string
     * @Column(type="string")
     */
    private $st_meiopagamento;

    /**
     * Contem o id do usuario do lancamento
     * @var int
     * @Column(type="integer")
     */
    private $id_usuariolancamento;

    /**
     * Contem o id da entidade
     * @var int
     * @Column(type="integer")
     */
    private $id_entidade;

    /**
     * Contem o nome da entidade
     * @var string
     * @Column(type="string")
     */
    private $st_nomeentidade;

    /**
     * Contem o valor se foi pago
     * @var boolean
     * @Column(type="boolean")
     */
    private $bl_quitado;

    /**
     * Contem o nome se foi quitado
     * @var string
     * @Column(type="string")
     */
    private $st_quitado;

    /**
     * Contem a data de vencimento do lancamento
     * @var zend_date
     * @Column(type="datetime")
     */
    private $dt_vencimento;

    /**
     * Contem a data em que foi quitado
     * @var zend_date
     * @Column(type="datetime")
     */
    private $dt_quitado;

    /**
     * Contem a data em que foi emitido
     * @var zend_date
     * @Column(type="datetime")
     */
    private $dt_emissao;

    /**
     * Contem a data de previsão de quitamento
     * @var zend_date
     * @Column(type="datetime")
     */
    private $dt_prevquitado;

    /**
     * Contem o nome do emissor
     * @var string
     * @Column(type="string")
     */
    private $st_emissor;

    /**
     * Contem o numero do documento (cheque)
     * @var numeric
     * @Column(type="string")
     */
    private $st_coddocumento;

    /**
     * Contem o numero da ordem, entrada seria a primeira, seguidas das proximas (nu_ordem-1)
     * Enter description here ...
     * @var string
     * @Column(type="string")
     */
    private $st_nuentrada;

    /**
     * @return the $bl_entrada
     */
    public function getBl_entrada() {
        return $this->bl_entrada;
    }

    /**
     * @return the $id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @return the $id_lancamento
     */
    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @return the $nu_vencimento
     */
    public function getNu_vencimento() {
        return $this->nu_vencimento;
    }

    /**
     * @return the $id_tipolancamento
     */
    public function getId_tipolancamento() {
        return $this->id_tipolancamento;
    }

    /**
     * @return the $st_tipolancamento
     */
    public function getSt_tipolancamento() {
        return $this->st_tipolancamento;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return the $id_meiopagamento
     */
    public function getId_meiopagamento() {
        return $this->id_meiopagamento;
    }

    /**
     * @return the $st_meiopagamento
     */
    public function getSt_meiopagamento() {
        return $this->st_meiopagamento;
    }

    /**
     * @return the $id_usuariolancamento
     */
    public function getId_usuariolancamento() {
        return $this->id_usuariolancamento;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $st_nomeentidade
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @return the $bl_quitado
     */
    public function getBl_quitado() {
        return $this->bl_quitado;
    }

    /**
     * @return the $st_quitado
     */
    public function getSt_quitado() {
        return $this->st_quitado;
    }

    /**
     * @return the $dt_vencimento
     */
    public function getDt_vencimento() {
        return $this->dt_vencimento;
    }

    /**
     * @return the $dt_quitado
     */
    public function getDt_quitado() {
        return $this->dt_quitado;
    }

    /**
     * @return the $dt_emissao
     */
    public function getDt_emissao() {
        return $this->dt_emissao;
    }

    /**
     * @return the $dt_prevquitado
     */
    public function getDt_prevquitado() {
        return $this->dt_prevquitado;
    }

    /**
     * @return the $st_emissor
     */
    public function getSt_emissor() {
        return $this->st_emissor;
    }

    /**
     * @return the $st_coddocumento
     */
    public function getSt_coddocumento() {
        return $this->st_coddocumento;
    }

    /**
     * @return the $st_nuentrada
     */
    public function getSt_nuentrada() {
        return $this->st_nuentrada;
    }

    /**
     * @param boolean $bl_entrada
     */
    public function setBl_entrada($bl_entrada) {
        $this->bl_entrada = $bl_entrada;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

    /**
     * @param int $id_lancamento
     */
    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param numeric $nu_valor
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param numeric $nu_vencimento
     */
    public function setNu_vencimento($nu_vencimento) {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @param int $id_tipolancamento
     */
    public function setId_tipolancamento($id_tipolancamento) {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @param string $st_tipolancamento
     */
    public function setSt_tipolancamento($st_tipolancamento) {
        $this->st_tipolancamento = $st_tipolancamento;
    }

    /**
     * @param zend_date $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param int $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento) {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param string $st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento) {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @param int $id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento) {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @param boolean $bl_quitado
     */
    public function setBl_quitado($bl_quitado) {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param string $st_quitado
     */
    public function setSt_quitado($st_quitado) {
        $this->st_quitado = $st_quitado;
    }

    /**
     * @param zend_date $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento) {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param zend_date $dt_quitado
     */
    public function setDt_quitado($dt_quitado) {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param zend_date $dt_emissao
     */
    public function setDt_emissao($dt_emissao) {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @param zend_date $dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado) {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param string $st_emissor
     */
    public function setSt_emissor($st_emissor) {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @param numeric $st_coddocumento
     */
    public function setSt_coddocumento($st_coddocumento) {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @param string $st_nuentrada
     */
    public function setSt_nuentrada($st_nuentrada) {
        $this->st_nuentrada = $st_nuentrada;
    }

}

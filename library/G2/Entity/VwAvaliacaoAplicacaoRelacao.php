<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avaliacaoaplicacaorelacao")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAvaliacaoAplicacaoRelacao {

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacao;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false)
     */
    private $st_avaliacao;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false)
     */
    private $id_tipoavaliacao;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false)
     * @Id
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true)
     */
    private $id_aplicadorprova;

    /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer", nullable=true)
     */
    private $id_endereco;

    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", type="date", nullable=true)
     */
    private $dt_aplicacao;

    /**
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=true)
     */
    private $st_tipoavaliacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true)
     */
    private $st_situacao;

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    public function getId_tipoavaliacao() {
        return $this->id_tipoavaliacao;
    }

    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function getDt_aplicacao() {
        return $this->dt_aplicacao;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
    }

    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
    }

    public function setId_tipoavaliacao($id_tipoavaliacao) {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    public function setId_aplicadorprova($id_aplicadorprova) {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }

    public function setDt_aplicacao($dt_aplicacao) {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    public function getSt_tipoavaliacao() {
        return $this->st_tipoavaliacao;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function setSt_tipoavaliacao($st_tipoavaliacao) {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

}

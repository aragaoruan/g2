<?php

namespace G2\Entity;

use G2\Entity\Entidade;
use G2\Entity\Usuario;
use G2\Entity\TextoSistema;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pa_mensageminformativa")
 * @Entity(repositoryClass="G2\Repository\PAMensagemInformativaRepository")
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 */
class PAMensagemInformativa {

    /**
     * @Id
     * @GeneratedValue
     * @Column(name="id_pa_mensageminformativa", type="integer")
     */
    private $id_pa_mensageminformativa;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textomensagem", referencedColumnName="id_textosistema")
     */
    private $id_textomensagem;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textoemail", referencedColumnName="id_textosistema")
     */
    private $id_textoemail;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /** @Column(name="dt_cadastro", type="datetime2", nullable=false) */
    private $dt_cadastro;

    function __construct() {
        $this->id_usuariocadastro = new Usuario();
        $this->id_textomensagem = new TextoSistema();
        $this->id_textoemail = new TextoSistema();
        $this->id_entidade = new Entidade();
    }

    public function getId_pa_mensageminformativa() {
        return $this->id_pa_mensageminformativa;
    }

    public function getId_textomensagem() {
        return $this->id_textomensagem;
    }

    public function getId_textoemail() {
        return $this->id_textoemail;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_pa_mensageminformativa($id_pa_mensageminformativa) {
        $this->id_pa_mensageminformativa = $id_pa_mensageminformativa;
    }

    public function setId_textomensagem($id_textomensagem) {
        $this->id_textomensagem = $id_textomensagem;
    }

    public function setId_textoemail($id_textoemail) {
        $this->id_textoemail = $id_textoemail;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

}

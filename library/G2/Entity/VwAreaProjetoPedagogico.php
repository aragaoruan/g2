<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_areaprojetopedagogico")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAreaProjetoPedagogico
{

    /**
     *
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;
    
    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", name="id_projetopedagogico", nullable=false)
     */
    private $id_projetopedagogico;
    
    /**
     * @var integer $id_entidade
     * @Column(type="integer", name="id_entidade", nullable=false)
     */
    private $id_entidade;
    
    /**
     * @var string $st_areaconhecimento
     * @Column(type="string", length=255, name="st_areaconhecimento", nullable=false)
     */
    private $st_areaconhecimento;
    
    /**
     * @var string $st_descricao
     * @Column(type="string", length=2500, name="st_descricao", nullable=false)
     */
    private $st_descricao;
    
    /**
     * @var integer $id_modulo
     * @Column(type="integer", name="id_modulo", nullable=false)
     */
    private $id_modulo;
    
    
    /**
     * @var integer $id_moduloanterior
     * @Column(type="integer", name="id_moduloanterior", nullable=true)
     */
    private $id_moduloanterior;
    
    
    /**
     * @var string $st_modulo
     * @Column(type="string", length=255, name="st_modulo", nullable=false)
     */
    private $st_modulo;
    
    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(type="string", length=255, name="st_tituloexibicaomodulo", nullable=true)
     */
    private $st_tituloexibicaomodulo;
    
    /**
     * @var string $st_descricaoprojetopedagogico
     * @Column(type="string", length=2500, name="st_descricaoprojetopedagogico", nullable=true)
     */
    private $st_descricaoprojetopedagogico;
    
    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", length=255, name="st_projetopedagogico", nullable=false)
     */
    private $st_projetopedagogico;
    
    /**
     * @var string $st_tituloexibicaoprojetopedagogico
     * @Column(type="string", length=255, name="st_tituloexibicaoprojetopedagogico", nullable=true)
     */
    private $st_tituloexibicaoprojetopedagogico;
    
    
     /**
     * @var integer $id_entidadecadastro
     * @Column(type="integer", name="id_entidadecadastro", nullable=false)
     */
    private $id_entidadecadastro;
    
    
    /**
     * @var string $st_modulopai
     * @Column(type="string", length=255, name="st_modulopai", nullable=true)
     */
    private $st_modulopai;
    
    /**
     * @var string $st_tituloexibicaopai
     * @Column(type="string", length=255, name="st_tituloexibicaopai", nullable=true)
     */
    private $st_tituloexibicaopai;
    
    /**
     * @var string $st_disciplina
     * @Column(type="string", length=255, name="st_disciplina", nullable=false)
     */
    private $st_disciplina;
    
    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(type="string", length=255, name="st_tituloexibicaodisciplina", nullable=true)
     */
    private $st_tituloexibicaodisciplina;
    
    /**
     * @var integer $id_disciplina
     * @Column(type="integer", name="id_disciplina", nullable=false)
     */
    private $id_disciplina;
    
    /**
     * @var integer $id_entidadedisciplina
     * @Column(type="integer", name="id_entidadedisciplina", nullable=false)
     */
    private $id_entidadedisciplina;
    
    /**
     * @var integer $id_situacaodisciplina
     * @Column(type="integer", name="id_situacaodisciplina", nullable=false)
     */
    private $id_situacaodisciplina;
    
    /**
     * @var integer $id_nivelensino
     * @Column(type="integer", name="id_nivelensino", nullable=false)
     */
    private $id_nivelensino;
    
    /**
     * @var integer $id_serie
     * @Column(type="integer", name="id_serie", nullable=false)
     */
    private $id_serie;
    
    
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
    }

    public function getId_modulo() {
        return $this->id_modulo;
    }

    public function setId_modulo($id_modulo) {
        $this->id_modulo = $id_modulo;
    }

    public function getId_moduloanterior() {
        return $this->id_moduloanterior;
    }

    public function setId_moduloanterior($id_moduloanterior) {
        $this->id_moduloanterior = $id_moduloanterior;
    }

    public function getSt_modulo() {
        return $this->st_modulo;
    }

    public function setSt_modulo($st_modulo) {
        $this->st_modulo = $st_modulo;
    }

    public function getSt_tituloexibicaomodulo() {
        return $this->st_tituloexibicaomodulo;
    }

    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
    }

    public function getSt_descricaoprojetopedagogico() {
        return $this->st_descricaoprojetopedagogico;
    }

    public function setSt_descricaoprojetopedagogico($st_descricaoprojetopedagogico) {
        $this->st_descricaoprojetopedagogico = $st_descricaoprojetopedagogico;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getSt_tituloexibicaoprojetopedagogico() {
        return $this->st_tituloexibicaoprojetopedagogico;
    }

    public function setSt_tituloexibicaoprojetopedagogico($st_tituloexibicaoprojetopedagogico) {
        $this->st_tituloexibicaoprojetopedagogico = $st_tituloexibicaoprojetopedagogico;
    }

    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    public function getSt_modulopai() {
        return $this->st_modulopai;
    }

    public function setSt_modulopai($st_modulopai) {
        $this->st_modulopai = $st_modulopai;
    }

    public function getSt_tituloexibicaopai() {
        return $this->st_tituloexibicaopai;
    }

    public function setSt_tituloexibicaopai($st_tituloexibicaopai) {
        $this->st_tituloexibicaopai = $st_tituloexibicaopai;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_tituloexibicaodisciplina() {
        return $this->st_tituloexibicaodisciplina;
    }

    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function getId_entidadedisciplina() {
        return $this->id_entidadedisciplina;
    }

    public function setId_entidadedisciplina($id_entidadedisciplina) {
        $this->id_entidadedisciplina = $id_entidadedisciplina;
    }

    public function getId_situacaodisciplina() {
        return $this->id_situacaodisciplina;
    }

    public function setId_situacaodisciplina($id_situacaodisciplina) {
        $this->id_situacaodisciplina = $id_situacaodisciplina;
    }

    public function getId_nivelensino() {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino($id_nivelensino) {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function getId_serie() {
        return $this->id_serie;
    }

    public function setId_serie($id_serie) {
        $this->id_serie = $id_serie;
    }



}
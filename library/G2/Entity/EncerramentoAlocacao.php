<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_encerramentoalocacao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class EncerramentoAlocacao extends G2Entity
{

    /**
     * @Id
     * @var integer $id_encerramentosala
     * @Column(name="id_encerramentosala", type="integer", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_encerramentosala;

    /**
     * @Id
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_alocacao;

    /**
     * @return mixed
     */
    public function getid_encerramentosala()
    {
        return $this->id_encerramentosala;
    }

    /**
     * @param mixed $id_encerramentosala
     * @return EncerramentoAlocacao
     */
    public function setid_encerramentosala($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param mixed $id_alocacao
     * @return EncerramentoAlocacao
     */
    public function setid_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }


}

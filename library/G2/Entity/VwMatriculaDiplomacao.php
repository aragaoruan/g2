<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_matriculadiplomacao")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwMatriculaDiplomacao extends G2Entity
{

    /**
     * @var integer $id_matriculadiplomacao
     * @Column(name="id_matriculadiplomacao", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_matriculadiplomacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var integer $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string")
     */
    private $st_email;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_reconhecimento
     * @Column(name="st_reconhecimento", type="string")
     */
    private $st_reconhecimento;

    /**
     * @var integer $id_tiposelecao
     * @Column(name="id_tiposelecao", type="integer")
     */
    private $id_tiposelecao;

    /**
     * @var \DateTime $dt_ingresso
     * @Column(name="dt_ingresso", type="datetime2")
     */
    private $dt_ingresso;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer")
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string")
     */
    private $st_situacao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var \DateTime $dt_concluinte
     * @Column(name="dt_concluinte", type="datetime2")
     */
    private $dt_concluinte;

    /**
     * @var integer $id_entidadematriz
     * @Column(name="id_entidadematriz", type="integer")
     */
    private $id_entidadematriz;

    /**
     * @var string $st_entidadematriz
     * @Column(name="st_entidadematriz", type="string")
     */
    private $st_entidadematriz;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer")
     */
    private $id_entidadematricula;

    /**
     * @var string $st_entidadematricula
     * @Column(name="st_entidadematricula", type="string")
     */
    private $st_entidadematricula;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer")
     */
    private $id_entidadeatendimento;

    /**
     * @var string $st_entidadeatendimento
     * @Column(name="st_entidadeatendimento", type="string")
     */
    private $st_entidadeatendimento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var \DateTime $dt_termino
     * @Column(name="dt_termino", type="datetime2")
     */
    private $dt_termino;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @var \DateTime $dt_inicio
     * @Column(name="dt_inicio", type="datetime2")
     */
    private $dt_inicio;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer")
     */
    private $id_turma;

    /**
     * @var integer $id_turmaorigem
     * @Column(name="id_turmaorigem", type="integer")
     */
    private $id_turmaorigem;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string")
     */
    private $st_turma;

    /**
     * @var \DateTime $dt_inicioturma
     * @Column(name="dt_inicioturma", type="datetime2")
     */
    private $dt_inicioturma;

    /**
     * @var boolean $bl_institucional
     * @Column(name="bl_institucional", type="boolean", length=1)
     */
    private $bl_institucional;

    /**
     * @var \DateTime $dt_terminoturma
     * @Column(name="dt_terminoturma", type="datetime2")
     */
    private $dt_terminoturma;


    /**
     * @var date $dt_terminomatricula
     * @Column(type="date", nullable=true, name="dt_terminomatricula")
     */
    private $dt_terminomatricula;


    /**
     * @var boolean $bl_documentacao
     * @Column(name="bl_documentacao", type="boolean", length=1)
     */
    private $bl_documentacao;

    /**
     * @var boolean $bl_academico
     * @Column(name="bl_academico", type="boolean", length=1)
     */
    private $bl_academico;

    /**
     * @var string $st_statusdocumentacao
     * @Column(name="st_statusdocumentacao", type="string", nullable=true)
     */
    private $st_statusdocumentacao;

    /**
     * @var \DateTime $dt_colacao
     * @Column(name="dt_colacao", type="datetime")
     */
    private $dt_colacao;

    /**
     * @var boolean $bl_presenca
     * @Column(name="bl_presenca", type="boolean", length=1)
     */
    private $bl_presenca;

    /**
     * @var \DateTime $dt_aptodiplomar
     * @Column(name="dt_aptodiplomar", type="datetime")
     */
    private $dt_aptodiplomar;

    /**
     * @var integer $id_usuarioaptodiplomar
     * @Column(name="id_usuarioaptodiplomar", type="integer")
     */
    private $id_usuarioaptodiplomar;

    /**
     * @var \DateTime $dt_diplomagerado
     * @Column(name="dt_diplomagerado", type="datetime")
     */
    private $dt_diplomagerado;

    /**
     * @var integer $id_usuariodiplomagerado
     * @Column(name="id_usuariodiplomagerado", type="integer")
     */
    private $id_usuariodiplomagerado;

    /**
     * @var \DateTime $dt_historicogerado
     * @Column(name="dt_historicogerado", type="datetime")
     */
    private $dt_historicogerado;

    /**
     * @var integer $id_usuariohistoricogerado
     * @Column(name="id_usuariohistoricogerado", type="integer")
     */
    private $id_usuariohistoricogerado;

    /**
     * @var \DateTime $dt_equivalenciagerado
     * @Column(name="dt_equivalenciagerado", type="datetime")
     */
    private $dt_equivalenciagerado;

    /**
     * @var integer $id_usuarioequivalenciagerado
     * @Column(name="id_usuarioequivalenciagerado", type="integer")
     */
    private $id_usuarioequivalenciagerado;

    /**
     * @var \DateTime $dt_enviadoassinatura
     * @Column(name="dt_enviadoassinatura", type="datetime")
     */
    private $dt_enviadoassinatura;

    /**
     * @var integer $id_usuarioenviadoassinatura
     * @Column(name="id_usuarioenviadoassinatura", type="integer")
     */
    private $id_usuarioenviadoassinatura;

    /**
     * @var \DateTime $dt_retornadoassinatura
     * @Column(name="dt_retornadoassinatura", type="datetime")
     */
    private $dt_retornadoassinatura;

    /**
     * @var integer $id_usuarioretornadoassinatura
     * @Column(name="id_usuarioretornadoassinatura", type="integer")
     */
    private $id_usuarioretornadoassinatura;

    /**
     * @var \DateTime $dt_enviadosecretaria
     * @Column(name="dt_enviadosecretaria", type="datetime")
     */
    private $dt_enviadosecretaria;

    /**
     * @var integer $id_usuarioenviadosecretaria
     * @Column(name="id_usuarioenviadosecretaria", type="integer")
     */
    private $id_usuarioenviadosecretaria;

    /**
     * @var \DateTime $dt_enviadoregistro
     * @Column(name="dt_enviadoregistro", type="datetime")
     */
    private $dt_enviadoregistro;

    /**
     * @var integer $id_usuarioenviadoregistro
     * @Column(name="id_usuarioenviadoregistro", type="integer")
     */
    private $id_usuarioenviadoregistro;

    /**
     * @var \DateTime $dt_retornoregistro
     * @Column(name="dt_retornoregistro", type="datetime")
     */
    private $dt_retornoregistro;

    /**
     * @var integer $id_usuarioretornoregistro
     * @Column(name="id_usuarioretornoregistro", type="integer")
     */
    private $id_usuarioretornoregistro;

    /**
     * @var \DateTime $dt_enviopolo
     * @Column(name="dt_enviopolo", type="datetime")
     */
    private $dt_enviopolo;

    /**
     * @var integer $id_usuarioenviopolo
     * @Column(name="id_usuarioenviopolo", type="integer")
     */
    private $id_usuarioenviopolo;

    /**
     * @var \DateTime $dt_entreguealuno
     * @Column(name="dt_entreguealuno", type="datetime")
     */
    private $dt_entreguealuno;

    /**
     * @var integer $id_usuarioentreguealuno
     * @Column(name="id_usuarioentreguealuno", type="integer")
     */
    private $id_usuarioentreguealuno;

    /**
     * @var boolean $bl_ativodiplomacao
     * @Column(name="bl_ativodiplomacao", type="boolean", length=1)
     */
    private $bl_ativodiplomacao;

    /**
     * @var integer $id_evolucaodiplomacao
     * @Column(name="id_evolucaodiplomacao", type="integer")
     */
    private $id_evolucaodiplomacao;

    /**
     * @var string $st_diplomanumero
     * @Column(name="st_diplomanumero", type="string")
     */
    private $st_diplomanumero;

    /**
     * @var string $st_diplomalivro
     * @Column(name="st_diplomalivro", type="string")
     */
    private $st_diplomalivro;

    /**
     * @var string $st_diplomafolha
     * @Column(name="st_diplomafolha", type="string")
     */
    private $st_diplomafolha;

    /**
     * @var string $st_cedula
     * @Column(name="st_cedula", type="string")
     */
    private $st_cedula;

    /**
     * @var string $st_motivo
     * @Column(name="st_motivo", type="string")
     */
    private $st_motivo;

    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string")
     */
    private $st_observacao;

    /**
     * @var boolean $bl_segundavia
     * @Column(name="bl_segundavia", type="boolean",  nullable=false, length=1)
     */
    private $bl_segundavia;


    /**
     * @var integer $id_matriculacolacao
     * @Column(name="id_matriculacolacao", type="integer")
     */
    private $id_matriculacolacao;

    /**
     * @var \DateTime $dt_registroprocesso
     * @Column(name="dt_registroprocesso", type="datetime")
     */
    private $dt_registroprocesso;

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param int $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return int
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param int $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_reconhecimento()
    {
        return $this->st_reconhecimento;
    }

    /**
     * @param string $st_reconhecimento
     * @return $this
     */
    public function setSt_reconhecimento($st_reconhecimento)
    {
        $this->st_reconhecimento = $st_reconhecimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param int $id_tiposelecao
     * @return $this
     */
    public function setId_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ingresso()
    {
        return $this->dt_ingresso;
    }

    /**
     * @param \DateTime $dt_ingresso
     * @return $this
     */
    public function setDt_ingresso($dt_ingresso)
    {
        $this->dt_ingresso = $dt_ingresso;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return \DateTime
     */
    public function getDt_concluinte()
    {
        return $this->dt_concluinte;
    }

    /**
     * @param \DateTime $dt_concluinte
     */
    public function setDt_concluinte($dt_concluinte)
    {
        $this->dt_concluinte = $dt_concluinte;
    }

    /**
     * @return int
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @param int $id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
    }

    /**
     * @return string
     */
    public function getSt_entidadematriz()
    {
        return $this->st_entidadematriz;
    }

    /**
     * @param string $st_entidadematriz
     */
    public function setSt_entidadematriz($st_entidadematriz)
    {
        $this->st_entidadematriz = $st_entidadematriz;
    }

    /**
     * @return int
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param int $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @return string
     */
    public function getSt_entidadematricula()
    {
        return $this->st_entidadematricula;
    }

    /**
     * @param string $st_entidadematricula
     */
    public function setSt_entidadematricula($st_entidadematricula)
    {
        $this->st_entidadematricula = $st_entidadematricula;
    }

    /**
     * @return int
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param int $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @return string
     */
    public function getSt_entidadeatendimento()
    {
        return $this->st_entidadeatendimento;
    }

    /**
     * @param string $st_entidadeatendimento
     */
    public function setSt_entidadeatendimento($st_entidadeatendimento)
    {
        $this->st_entidadeatendimento = $st_entidadeatendimento;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return \DateTime
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @param \DateTime $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \DateTime
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param \DateTime $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_turmaorigem()
    {
        return $this->id_turmaorigem;
    }

    /**
     * @param int $id_turmaorigem
     */
    public function setId_turmaorigem($id_turmaorigem)
    {
        $this->id_turmaorigem = $id_turmaorigem;
    }

    /**
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param string $st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
    }

    /**
     * @return \DateTime
     */
    public function getDt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param \DateTime $dt_inicioturma
     */
    public function setDt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
    }

    /**
     * @return bool
     */
    public function getBl_institucional()
    {
        return $this->bl_institucional;
    }

    /**
     * @param bool $bl_institucional
     */
    public function setBl_institucional($bl_institucional)
    {
        $this->bl_institucional = $bl_institucional;
    }

    /**
     * @return \DateTime
     */
    public function getDt_terminoturma()
    {
        return $this->dt_terminoturma;
    }

    /**
     * @param \DateTime $dt_terminoturma
     */
    public function setDt_terminoturma($dt_terminoturma)
    {
        $this->dt_terminoturma = $dt_terminoturma;
    }

    /**
     * @return date
     */
    public function getDt_terminomatricula()
    {
        return $this->dt_terminomatricula;
    }

    /**
     * @param date $dt_terminomatricula
     */
    public function setDt_terminomatricula($dt_terminomatricula)
    {
        $this->dt_terminomatricula = $dt_terminomatricula;
    }


    /**
     * @return bool
     */
    public function getBl_documentacao()
    {
        return $this->bl_documentacao;
    }

    /**
     * @param bool $bl_documentacao
     */
    public function setBl_documentacao($bl_documentacao)
    {
        $this->bl_documentacao = $bl_documentacao;
    }

    /**
     * @return bool
     */
    public function getBl_academico()
    {
        return $this->bl_academico;
    }

    /**
     * @param bool $bl_academico
     */
    public function setBl_academico($bl_academico)
    {
        $this->bl_academico = $bl_academico;
    }

    /**
     * @return string
     */
    public function getSt_statusdocumentacao()
    {
        return $this->st_statusdocumentacao;
    }

    /**
     * @param string $st_statusdocumentacao
     */
    public function setSt_statusdocumentacao($st_statusdocumentacao)
    {
        $this->st_statusdocumentacao = $st_statusdocumentacao;
    }

    /**
     * @return \DateTime
     */
    public function getDt_colacao()
    {
        return $this->dt_colacao;
    }

    /**
     * @param \DateTime $dt_colacao
     */
    public function setDt_colacao($dt_colacao)
    {
        $this->dt_colacao = $dt_colacao;
    }

    /**
     * @return bool
     */
    public function getBl_presenca()
    {
        return $this->bl_presenca;
    }

    /**
     * @param bool $bl_presenca
     */
    public function setBl_presenca($bl_presenca)
    {
        $this->bl_presenca = $bl_presenca;
    }

    /**
     * @return \DateTime
     */
    public function getDt_aptodiplomar()
    {
        return $this->dt_aptodiplomar;
    }

    /**
     * @param \DateTime $dt_aptodiplomar
     * @return $this
     */
    public function setDt_aptodiplomar($dt_aptodiplomar)
    {
        $this->dt_aptodiplomar = $dt_aptodiplomar;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuarioaptodiplomar()
    {
        return $this->id_usuarioaptodiplomar;
    }

    /**
     * @param int $id_usuarioaptodiplomar
     * @return $this
     */
    public function setId_usuarioaptodiplomar($id_usuarioaptodiplomar)
    {
        $this->id_usuarioaptodiplomar = $id_usuarioaptodiplomar;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_diplomagerado()
    {
        return $this->dt_diplomagerado;
    }

    /**
     * @param \DateTime $dt_diplomagerado
     */
    public function setDt_diplomagerado($dt_diplomagerado)
    {
        $this->dt_diplomagerado = $dt_diplomagerado;
    }

    /**
     * @return int
     */
    public function getId_usuariodiplomagerado()
    {
        return $this->id_usuariodiplomagerado;
    }

    /**
     * @param int $id_usuariodiplomagerado
     */
    public function setId_usuariodiplomagerado($id_usuariodiplomagerado)
    {
        $this->id_usuariodiplomagerado = $id_usuariodiplomagerado;
    }

    /**
     * @return \DateTime
     */
    public function getDt_historicogerado()
    {
        return $this->dt_historicogerado;
    }

    /**
     * @param \DateTime $dt_historicogerado
     */
    public function setDt_historicogerado($dt_historicogerado)
    {
        $this->dt_historicogerado = $dt_historicogerado;
    }

    /**
     * @return \DateTime
     */
    public function getDt_equivalenciagerado()
    {
        return $this->dt_equivalenciagerado;
    }

    /**
     * @param \DateTime $dt_equivalenciagerado
     */
    public function setDt_equivalenciagerado($dt_equivalenciagerado)
    {
        $this->dt_equivalenciagerado = $dt_equivalenciagerado;
    }

    /**
     * @return int
     */
    public function getId_usuarioequivalenciagerado()
    {
        return $this->id_usuarioequivalenciagerado;
    }

    /**
     * @param int $id_usuarioequivalenciagerado
     * @return $this
     */
    public function setId_usuarioequivalenciagerado($id_usuarioequivalenciagerado)
    {
        $this->id_usuarioequivalenciagerado = $id_usuarioequivalenciagerado;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariohistoricogerado()
    {
        return $this->id_usuariohistoricogerado;
    }

    /**
     * @param int $id_usuariohistoricogerado
     */
    public function setId_usuariohistoricogerado($id_usuariohistoricogerado)
    {
        $this->id_usuariohistoricogerado = $id_usuariohistoricogerado;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadoassinatura()
    {
        return $this->dt_enviadoassinatura;
    }

    /**
     * @param \DateTime $dt_enviadoassinatura
     */
    public function setDt_enviadoassinatura($dt_enviadoassinatura)
    {
        $this->dt_enviadoassinatura = $dt_enviadoassinatura;
    }

    /**
     * @return int
     */
    public function getId_usuarioenviadoassinatura()
    {
        return $this->id_usuarioenviadoassinatura;
    }

    /**
     * @param int $id_usuarioenviadoassinatura
     */
    public function setId_usuarioenviadoassinatura($id_usuarioenviadoassinatura)
    {
        $this->id_usuarioenviadoassinatura = $id_usuarioenviadoassinatura;
    }

    /**
     * @return \DateTime
     */
    public function getDt_retornadoassinatura()
    {
        return $this->dt_retornadoassinatura;
    }

    /**
     * @param \DateTime $dt_retornadoassinatura
     */
    public function setDt_retornadoassinatura($dt_retornadoassinatura)
    {
        $this->dt_retornadoassinatura = $dt_retornadoassinatura;
    }

    /**
     * @return int
     */
    public function getId_usuarioretornadoassinatura()
    {
        return $this->id_usuarioretornadoassinatura;
    }

    /**
     * @param int $id_usuarioretornadoassinatura
     */
    public function setId_usuarioretornadoassinatura($id_usuarioretornadoassinatura)
    {
        $this->id_usuarioretornadoassinatura = $id_usuarioretornadoassinatura;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadosecretaria()
    {
        return $this->dt_enviadosecretaria;
    }

    /**
     * @param \DateTime $dt_enviadosecretaria
     */
    public function setDt_enviadosecretaria($dt_enviadosecretaria)
    {
        $this->dt_enviadosecretaria = $dt_enviadosecretaria;
    }

    /**
     * @return int
     */
    public function getId_usuarioenviadosecretaria()
    {
        return $this->id_usuarioenviadosecretaria;
    }

    /**
     * @param int $id_usuarioenviadosecretaria
     */
    public function setId_usuarioenviadosecretaria($id_usuarioenviadosecretaria)
    {
        $this->id_usuarioenviadosecretaria = $id_usuarioenviadosecretaria;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadoregistro()
    {
        return $this->dt_enviadoregistro;
    }

    /**
     * @param \DateTime $dt_enviadoregistro
     */
    public function setDt_enviadoregistro($dt_enviadoregistro)
    {
        $this->dt_enviadoregistro = $dt_enviadoregistro;
    }

    /**
     * @return int
     */
    public function getId_usuarioenviadoregistro()
    {
        return $this->id_usuarioenviadoregistro;
    }

    /**
     * @param int $id_usuarioenviadoregistro
     */
    public function setId_usuarioenviadoregistro($id_usuarioenviadoregistro)
    {
        $this->id_usuarioenviadoregistro = $id_usuarioenviadoregistro;
    }

    /**
     * @return \DateTime
     */
    public function getDt_retornoregistro()
    {
        return $this->dt_retornoregistro;
    }

    /**
     * @param \DateTime $dt_retornoregistro
     */
    public function setDt_retornoregistro($dt_retornoregistro)
    {
        $this->dt_retornoregistro = $dt_retornoregistro;
    }

    /**
     * @return int
     */
    public function getId_usuarioretornoregistro()
    {
        return $this->id_usuarioretornoregistro;
    }

    /**
     * @param int $id_usuarioretornoregistro
     */
    public function setId_usuarioretornoregistro($id_usuarioretornoregistro)
    {
        $this->id_usuarioretornoregistro = $id_usuarioretornoregistro;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviopolo()
    {
        return $this->dt_enviopolo;
    }

    /**
     * @param \DateTime $dt_enviopolo
     */
    public function setDt_enviopolo($dt_enviopolo)
    {
        $this->dt_enviopolo = $dt_enviopolo;
    }

    /**
     * @return int
     */
    public function getId_usuarioenviopolo()
    {
        return $this->id_usuarioenviopolo;
    }

    /**
     * @param int $id_usuarioenviopolo
     */
    public function setId_usuarioenviopolo($id_usuarioenviopolo)
    {
        $this->id_usuarioenviopolo = $id_usuarioenviopolo;
    }

    /**
     * @return \DateTime
     */
    public function getDt_entreguealuno()
    {
        return $this->dt_entreguealuno;
    }

    /**
     * @param \DateTime $dt_entreguealuno
     */
    public function setDt_entreguealuno($dt_entreguealuno)
    {
        $this->dt_entreguealuno = $dt_entreguealuno;
    }

    /**
     * @return int
     */
    public function getId_usuarioentreguealuno()
    {
        return $this->id_usuarioentreguealuno;
    }

    /**
     * @param int $id_usuarioentreguealuno
     */
    public function setId_usuarioentreguealuno($id_usuarioentreguealuno)
    {
        $this->id_usuarioentreguealuno = $id_usuarioentreguealuno;
    }

    /**
     * @return bool
     */
    public function getBl_ativodiplomacao()
    {
        return $this->bl_ativodiplomacao;
    }

    /**
     * @param bool $bl_ativodiplomacao
     */
    public function setBl_ativodiplomacao($bl_ativodiplomacao)
    {
        $this->bl_ativodiplomacao = $bl_ativodiplomacao;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return int
     */
    public function getId_matriculadiplomacao()
    {
        return $this->id_matriculadiplomacao;
    }

    /**
     * @param int $id_matriculadiplomacao
     */
    public function setId_matriculadiplomacao($id_matriculadiplomacao)
    {
        $this->id_matriculadiplomacao = $id_matriculadiplomacao;
    }

    /**
     * @return int
     */
    public function getId_evolucaodiplomacao()
    {
        return $this->id_evolucaodiplomacao;
    }

    /**
     * @param int $id_evolucaodiplomacao
     */
    public function setId_evolucaodiplomacao($id_evolucaodiplomacao)
    {
        $this->id_evolucaodiplomacao = $id_evolucaodiplomacao;
    }

    /**
     * @return string
     */
    public function getSt_diplomanumero()
    {
        return $this->st_diplomanumero;
    }

    /**
     * @return int
     */
    public function getId_matriculacolacao()
    {
        return $this->id_matriculacolacao;
    }

    /**
     * @param string $st_diplomanumero
     * @return $this
     */
    public function setSt_diplomanumero($st_diplomanumero)
    {
        $this->st_diplomanumero = $st_diplomanumero;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_diplomalivro()
    {
        return $this->st_diplomalivro;
    }

    /**
     * @param int $id_matriculacolacao
     */
    public function setId_matriculacolacao($id_matriculacolacao)
    {
        $this->id_matriculacolacao = $id_matriculacolacao;
    }

    /**
     * @param string $st_diplomalivro
     * @return $this
     */
    public function setSt_diplomalivro($st_diplomalivro)
    {
        $this->st_diplomalivro = $st_diplomalivro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_diplomafolha()
    {
        return $this->st_diplomafolha;
    }

    /**
     * @param string $st_diplomafolha
     * @return $this
     */
    public function setSt_diplomafolha($st_diplomafolha)
    {
        $this->st_diplomafolha = $st_diplomafolha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cedula()
    {
        return $this->st_cedula;
    }

    /**
     * @param string $st_cedula
     * @return $this
     */
    public function setSt_cedula($st_cedula)
    {
        $this->st_cedula = $st_cedula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivo()
    {
        return $this->st_motivo;
    }

    /**
     * @param string $st_motivo
     * @return $this
     */
    public function setSt_motivo($st_motivo)
    {
        $this->st_motivo = $st_motivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param string $st_observacao
     * @return $this
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_segundavia()
    {
        return $this->bl_segundavia;
    }

    /**
     * @param bool $bl_segundavia
     * @return $this
     */
    public function setBl_segundavia($bl_segundavia)
    {
        $this->bl_segundavia = $bl_segundavia;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_registroprocesso()
    {
        return $this->dt_registroprocesso;
    }

    /**
     * @param \DateTime $dt_registroprocesso
     * @return $this
     */
    public function setDt_registroprocesso($dt_registroprocesso)
    {
        $this->dt_registroprocesso = $dt_registroprocesso;
        return $this;
    }

}

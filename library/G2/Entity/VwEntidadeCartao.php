<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_entidadecartao")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwEntidadeCartao
{
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_cartaoconfig
     * @Column(name="id_cartaoconfig", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_cartaoconfig;
    /**
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=false, length=4)
     */
    private $id_cartaobandeira;
    /**
     * @var integer $id_cartaooperadora
     * @Column(name="id_cartaooperadora", type="integer", nullable=false, length=4)
     */
    private $id_cartaooperadora;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;
    /**
     * @var string $st_cartaobandeira
     * @Column(name="st_cartaobandeira", type="string", nullable=false, length=150)
     */
    private $st_cartaobandeira;
    /**
     * @var string $st_cartaooperadora
     * @Column(name="st_cartaooperadora", type="string", nullable=false, length=50)
     */
    private $st_cartaooperadora;
    /**
     * @var string $st_contratooperadora
     * @Column(name="st_contratooperadora", type="string", nullable=true, length=50)
     */
    private $st_contratooperadora;
    /**
     * @var string $st_gateway
     * @Column(name="st_gateway", type="string", nullable=true, length=50)
     */
    private $st_gateway;
    /**
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=false, length=255)
     */
    private $st_sistema;


    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_cartaoconfig
     */
    public function getId_cartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @param id_cartaoconfig
     */
    public function setId_cartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
        return $this;
    }

    /**
     * @return integer id_cartaobandeira
     */
    public function getId_cartaobandeira()
    {
        return $this->id_cartaobandeira;
    }

    /**
     * @param id_cartaobandeira
     */
    public function setId_cartaobandeira($id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
        return $this;
    }

    /**
     * @return integer id_cartaooperadora
     */
    public function getId_cartaooperadora()
    {
        return $this->id_cartaooperadora;
    }

    /**
     * @param id_cartaooperadora
     */
    public function setId_cartaooperadora($id_cartaooperadora)
    {
        $this->id_cartaooperadora = $id_cartaooperadora;
        return $this;
    }

    /**
     * @return integer id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return string st_cartaobandeira
     */
    public function getSt_cartaobandeira()
    {
        return $this->st_cartaobandeira;
    }

    /**
     * @param st_cartaobandeira
     */
    public function setSt_cartaobandeira($st_cartaobandeira)
    {
        $this->st_cartaobandeira = $st_cartaobandeira;
        return $this;
    }

    /**
     * @return string st_cartaooperadora
     */
    public function getSt_cartaooperadora()
    {
        return $this->st_cartaooperadora;
    }

    /**
     * @param st_cartaooperadora
     */
    public function setSt_cartaooperadora($st_cartaooperadora)
    {
        $this->st_cartaooperadora = $st_cartaooperadora;
        return $this;
    }

    /**
     * @return string st_contratooperadora
     */
    public function getSt_contratooperadora()
    {
        return $this->st_contratooperadora;
    }

    /**
     * @param st_contratooperadora
     */
    public function setSt_contratooperadora($st_contratooperadora)
    {
        $this->st_contratooperadora = $st_contratooperadora;
        return $this;
    }

    /**
     * @return string st_gateway
     */
    public function getSt_gateway()
    {
        return $this->st_gateway;
    }

    /**
     * @param st_gateway
     */
    public function setSt_gateway($st_gateway)
    {
        $this->st_gateway = $st_gateway;
        return $this;
    }

    /**
     * @return string st_sistema
     */
    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

    /**
     * @param st_sistema
     */
    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
        return $this;
    }

}
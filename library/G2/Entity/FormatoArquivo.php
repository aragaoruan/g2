<?php

namespace G2\Entity;

/**
 * Class FormatoArquivo
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formatoarquivo")
 * @Entity
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @package G2\Entity
 */

class FormatoArquivo
{

    /**
     * @Id
     * @var integer $id_formatoarquivo
     * @Column(name="id_formatoarquivo", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE");
     */
    private $id_formatoarquivo;

    /**
     * @var integer $st_formatoarquivo
     * @Column(name="st_formatoarquivo", type="string", nullable=false, length=4)
     */
    private $st_formatoarquivo;

    /**
     * @var integer $st_extensaoarquivo
     * @Column(name="st_extensaoarquivo", type="string", nullable=false, length=4)
     */
    private $st_extensaoarquivo;

    /**
     * @return int
     */
    public function getId_formatoarquivo()
    {
        return $this->id_formatoarquivo;
    }

    /**
     * @return int $st_extensaoarquivo
     */
    public function getSt_extensaoarquivo()
    {
        return $this->st_extensaoarquivo;
    }

    /**
     * @param int $id_formatoarquivo
     */
    public function setId_formatoarquivo($id_formatoarquivo)
    {
        $this->id_formatoarquivo = $id_formatoarquivo;
    }

    /**
     * @return int
     */
    public function getSt_formatoarquivo()
    {
        return $this->st_formatoarquivo;
    }

    /**
     * @param int $st_formatoarquivo
     */
    public function setSt_formatoarquivo($st_formatoarquivo)
    {
        $this->st_formatoarquivo = $st_formatoarquivo;
    }

    /**
     * @param int $st_formatoarquivo
     */
    public function setSt_extensaoarquivo($st_extensaoarquivo)
    {
        $this->st_extensaoarquivo = $st_extensaoarquivo;
    }

}
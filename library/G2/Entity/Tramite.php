<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 *  * @SWG\Definition(
 *     required={},
 *     @SWG\Xml(name="Tramite")
 * )
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tramite")
 * @Entity
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class Tramite extends G2Entity
{
    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(format="integer")
     */
    private $id_tramite;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     * @SWG\Property(format="date")
     */
    private $dt_cadastro;

    /**
     * @var TipoTramite $id_tipotramite
     * @ManyToOne(targetEntity="TipoTramite")
     * @JoinColumn(name="id_tipotramite", referencedColumnName="id_tipotramite")
     * @SWG\Property(format="integer")
     */
    private $id_tipotramite;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * @SWG\Property(format="integer")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     * @SWG\Property(format="integer")
     */
    private $id_entidade;

    /**
     * @var Upload $id_upload
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", nullable=false, referencedColumnName="id_upload")
     * @SWG\Property(format="integer")
     */
    private $id_upload;

    /**
     * @var boolean $bl_visivel
     * @Column(name="bl_visivel", type="boolean", nullable=true)
     * @SWG\Property(format="bool")
     */
    private $bl_visivel;

    /**
     * @var string $st_tramite
     * @Column(name="st_tramite", type="string", nullable=true)
     * @SWG\Property(format="string")
     */
    private $st_tramite;

    /**
     * @var string $st_url
     * @Column(name="st_url", type="string", nullable=true)
     * @SWG\Property(format="string")
     */
    private $st_url;

    /**
     * @return string
     */
    public function getSt_url()
    {
        return $this->st_url;
    }

    /**
     * @param string $st_url
     */
    public function setSt_url($st_url)
    {
        $this->st_url = $st_url;
        return $this;

    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    public function getId_tipotramite()
    {
        return $this->id_tipotramite;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_upload()
    {
        return $this->id_upload;
    }

    public function getBl_visivel()
    {
        return $this->bl_visivel;
    }

    public function getSt_tramite()
    {
        return $this->st_tramite;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
        return $this;
    }

    /**
     * @param TipoTramite $id_tipotramite
     * @return $this
     */
    public function setId_tipotramite(TipoTramite $id_tipotramite)
    {
        $this->id_tipotramite = $id_tipotramite;
        return $this;
    }

    /**
     * @param Usuario $id_usuario
     * @return $this
     */
    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_upload(Upload $id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;
    }

    public function setBl_visivel($bl_visivel)
    {
        $this->bl_visivel = $bl_visivel;
        return $this;
    }

    public function setSt_tramite($st_tramite)
    {
        $this->st_tramite = $st_tramite;
        return $this;
    }

}

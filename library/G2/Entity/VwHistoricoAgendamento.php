<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_historicoagendamento")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwHistoricoAgendamento {

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false)
     */
    private $id_avaliacaoagendamento;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipodeavaliacao;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_tramite;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var boolean $bl_visivel
     * @Column(name="bl_visivel", type="boolean", nullable=false)
     */
    private $bl_visivel;

    /**
     * @var string $st_tipodeavaliacao
     * @Column(name="st_tipodeavaliacao", type="string", nullable=true)
     */
    private $st_tipodeavaliacao;

    /**
     * @var string $st_tramite
     * @Column(name="st_tramite", type="string", nullable=false)
     */
    private $st_tramite;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    
     /**
     * @var integer $id_ano
     * @Column(name="id_ano", type="integer")
     */
    private $id_ano;
    
    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getId_tipodeavaliacao() {
        return $this->id_tipodeavaliacao;
    }

    public function getId_tramite() {
        return $this->id_tramite;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getBl_visivel() {
        return $this->bl_visivel;
    }

    public function getSt_tipodeavaliacao() {
        return $this->st_tipodeavaliacao;
    }

    public function getSt_tramite() {
        return $this->st_tramite;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao) {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
    }

    public function setId_tramite($id_tramite) {
        $this->id_tramite = $id_tramite;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setBl_visivel($bl_visivel) {
        $this->bl_visivel = $bl_visivel;
    }

    public function setSt_tipodeavaliacao($st_tipodeavaliacao) {
        $this->st_tipodeavaliacao = $st_tipodeavaliacao;
    }

    public function setSt_tramite($st_tramite) {
        $this->st_tramite = $st_tramite;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function getId_ano() {
        return $this->id_ano;
    }

    public function setId_ano($id_ano) {
        $this->id_ano = $id_ano;
    }


}

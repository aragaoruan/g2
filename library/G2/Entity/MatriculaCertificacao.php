<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_matriculacertificacao")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MatriculaCertificacao
{

    /**
     * @Id
     * @var integer $id_indicecertificado
     * @Column(name="id_indicecertificado", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */

    private $id_indicecertificado;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $nu_ano
     * @Column(name="nu_ano", type="integer", nullable=false, length=4)
     */
    private $nu_ano;

    /**
     * @var integer $id_usuarioenviocertificado
     * @ManyToOne(targetEntity="Usuario", fetch="EXTRA_LAZY")
     * @JoinColumn(name="id_usuarioenviocertificado", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuarioenviocertificado;

    /**
     * @var integer $id_usuarioretornocertificadora
     * @ManyToOne(targetEntity="Usuario", fetch="EXTRA_LAZY")
     * @JoinColumn(name="id_usuarioretornocertificadora", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuarioretornocertificadora;

    /**
     * @var integer $id_usuarioenvioaluno
     * @ManyToOne(targetEntity="Usuario", fetch="EXTRA_LAZY")
     * @JoinColumn(name="id_usuarioenvioaluno", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuarioenvioaluno;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario", fetch="EXTRA_LAZY")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime $dt_enviocertificado
     * @Column(name="dt_enviocertificado", type="datetime", nullable=true, length=8)
     */
    private $dt_enviocertificado;

    /**
     * @var datetime $dt_retornocertificadora
     * @Column(name="dt_retornocertificadora", type="datetime", nullable=true, length=8)
     */
    private $dt_retornocertificadora;

    /**
     * @var datetime $dt_envioaluno
     * @Column(name="dt_envioaluno", type="datetime", nullable=true, length=8)
     */
    private $dt_envioaluno;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_siglaentidade
     * @Column(name="st_siglaentidade", type="string", nullable=true, length=200)
     */
    private $st_siglaentidade;

    /**
     * @var string $st_codigoacompanhamento
     * @Column(name="st_codigoacompanhamento", type="string", nullable=true, length=13)
     */
    private $st_codigoacompanhamento;

    /**
     * @return integer id_indicecertificado
     */
    public function getId_indicecertificado()
    {
        return $this->id_indicecertificado;
    }

    /**
     * @param int $id_indicecertificado
     */
    public function setId_indicecertificado($id_indicecertificado)
    {
        $this->id_indicecertificado = $id_indicecertificado;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        if ($this->id_entidade) {
            return $this->id_entidade;
        }

        return new \G2\Entity\Entidade();
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer nu_ano
     */
    public function getNu_ano()
    {
        return $this->nu_ano;
    }

    /**
     * @param nu_ano
     */
    public function setNu_ano($nu_ano)
    {
        $this->nu_ano = $nu_ano;
        return $this;
    }

    /**
     * @return integer id_usuarioenviocertificado
     */
    public function getId_usuarioenviocertificado()
    {
        if ($this->id_usuarioenviocertificado) {
            return $this->id_usuarioenviocertificado;
        }

        return new \G2\Entity\Usuario();
    }

    /**
     * @param id_usuarioenviocertificado
     */
    public function setId_usuarioenviocertificado($id_usuarioenviocertificado)
    {
        $this->id_usuarioenviocertificado = $id_usuarioenviocertificado;
        return $this;
    }

    /**
     * @return integer id_usuarioretornocertificadora
     */
    public function getId_usuarioretornocertificadora()
    {
        if ($this->id_usuarioretornocertificadora) {
            return $this->id_usuarioretornocertificadora;
        }

        return new \G2\Entity\Usuario();
    }

    /**
     * @param id_usuarioretornocertificadora
     */
    public function setId_usuarioretornocertificadora($id_usuarioretornocertificadora)
    {
        $this->id_usuarioretornocertificadora = $id_usuarioretornocertificadora;
        return $this;
    }

    /**
     * @return integer id_usuarioenvioaluno
     */
    public function getId_usuarioenvioaluno()
    {
        if ($this->id_usuarioenvioaluno) {
            return $this->id_usuarioenvioaluno;
        }

        return new \G2\Entity\Usuario();
    }

    /**
     * @param id_usuarioenvioaluno
     */
    public function setId_usuarioenvioaluno($id_usuarioenvioaluno)
    {
        $this->id_usuarioenvioaluno = $id_usuarioenvioaluno;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        if ($this->id_usuariocadastro) {
            return $this->id_usuariocadastro;
        }

        return new \G2\Entity\Usuario();
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        if ($this->dt_cadastro) {
            return $this->dt_cadastro;
        }

        return new \DateTime();
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime dt_enviocertificado
     */
    public function getDt_enviocertificado()
    {
        if ($this->dt_enviocertificado) {
            return $this->dt_enviocertificado;
        }

        return new \DateTime();
    }

    /**
     * @param dt_enviocertificado
     */
    public function setDt_enviocertificado($dt_enviocertificado)
    {
        $this->dt_enviocertificado = $dt_enviocertificado;
        return $this;
    }

    /**
     * @return datetime dt_retornocertificadora
     */
    public function getDt_retornocertificadora()
    {
        if ($this->dt_retornocertificadora) {
            return $this->dt_retornocertificadora;
        }

        return new \DateTime();
    }

    /**
     * @param dt_retornocertificadora
     */
    public function setDt_retornocertificadora($dt_retornocertificadora)
    {
        $this->dt_retornocertificadora = $dt_retornocertificadora;
        return $this;
    }

    /**
     * @return datetime dt_envioaluno
     */
    public function getDt_envioaluno()
    {
        if ($this->dt_envioaluno) {
            return $this->dt_envioaluno;
        }

        return new \DateTime();
    }

    /**
     * @param dt_envioaluno
     */
    public function setDt_envioaluno($dt_envioaluno)
    {
        $this->dt_envioaluno = $dt_envioaluno;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_siglaentidade
     */
    public function getSt_siglaentidade()
    {
        return $this->st_siglaentidade;
    }

    /**
     * @param st_siglaentidade
     */
    public function setSt_siglaentidade($st_siglaentidade)
    {
        $this->st_siglaentidade = $st_siglaentidade;
        return $this;
    }

    /**
     * @return string st_codigoacompanhamento
     */
    public function getSt_codigoacompanhamento()
    {
        return $this->st_codigoacompanhamento;
    }

    /**
     * @param st_codigoacompanhamento
     */
    public function setSt_codigoacompanhamento($st_codigoacompanhamento)
    {
        $this->st_codigoacompanhamento = $st_codigoacompanhamento;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_disciplinasprr")
 * @Entity
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwDisciplinasPrr {

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;

    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var decimal $st_nota
     * @Column(name="st_nota", type="decimal", nullable=true, length=5)
     */
    private $st_nota;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=true, length=9)
     */
    private $nu_valor;

    /**
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=true, length=4)
     */
    private $id_vendaproduto;

    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2", nullable=true, length=3)
     */
    private $dt_entrega;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=true, length=4)
     */
    private $id_produto;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;

    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_ocorrencia;

    /**
     * @var integer $bl_cursandoprr
     * @Column(name="bl_cursandoprr", type="integer", nullable=true, length=4)
     */
    private $bl_cursandoprr;

    /**
     * @var datetime2 $dt_cadastrosala
     * @Column(name="dt_cadastrosala", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastrosala;

    /**
     * @var date $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="date", nullable=true, length=3)
     */
    private $dt_encerramentosala;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=20)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_saladeaulaprr
     * @Column(name="st_saladeaulaprr", type="string", nullable=true, length=20)
     */
    private $st_saladeaulaprr;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=20)
     */
    private $id_usuario;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true, length=20)
     */
    private $id_evolucao;

    /**
     * @var decimal $nu_valorbrutovenda
     * @Column(name="nu_valorbrutovenda", type="decimal", nullable=true, length=4)
     */
    private $nu_valorbrutovenda;

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getSt_tituloexibicaodisciplina()
    {
        return $this->st_tituloexibicaodisciplina;
    }

    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina)
    {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function getSt_nota()
    {
        return $this->st_nota;
    }

    public function setSt_nota($st_nota)
    {
        $this->st_nota = $st_nota;
        return $this;
    }

    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    public function getDt_entrega()
    {
        return $this->dt_entrega;
    }

    public function setDt_entrega($dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
        return $this;
    }

    public function getId_produto()
    {
        return $this->id_produto;
    }

    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getId_venda()
    {
        return $this->id_venda;
    }

    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    public function getBl_cursandoprr()
    {
        return $this->bl_cursandoprr;
    }

    public function setBl_cursandoprr($bl_cursandoprr)
    {
        $this->bl_cursandoprr = $bl_cursandoprr;
        return $this;
    }

    public function getDt_cadastrosala()
    {
        return $this->dt_cadastrosala;
    }

    public function setDt_cadastrosala($dt_cadastrosala)
    {
        $this->dt_cadastrosala = $dt_cadastrosala;
        return $this;
    }

    public function getDt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    public function setDt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
        return $this;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function getSt_saladeaulaprr()
    {
        return $this->st_saladeaulaprr;
    }

    public function setSt_saladeaulaprr($st_saladeaulaprr)
    {
        $this->st_saladeaulaprr = $st_saladeaulaprr;
        return $this;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function getNu_valorbrutovenda()
    {
        return $this->nu_valorbrutovenda;
    }

    public function setNu_valorbrutovenda($nu_valorbrutovenda)
    {
        $this->nu_valorbrutovenda = $nu_valorbrutovenda;
        return $this;
    }
}

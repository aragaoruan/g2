<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipolivro")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class TipoLivro
{


    /**
     *
     * @var integer $id_tipolivro
     * @Column(name="id_tipolivro", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipolivro;
    
    /**
     *
     * @var string $st_tipolivro
     * @Column(name="st_tipolivro", type="string", nullable=true, length=20)
     */
    private $st_tipolivro;
    
    
	/**
	 * @return the $id_tipolivro
	 */
	public function getId_tipolivro() {
		return $this->id_tipolivro;
	}

	/**
	 * @return the $st_tipolivro
	 */
	public function getSt_tipolivro() {
		return $this->st_tipolivro;
	}

	/**
	 * @param number $id_tipolivro
	 */
	public function setId_tipolivro($id_tipolivro) {
		$this->id_tipolivro = $id_tipolivro;
		return $this;
	}

	/**
	 * @param string $st_tipolivro
	 */
	public function setSt_tipolivro($st_tipolivro) {
		$this->st_tipolivro = $st_tipolivro;
		return $this;
	}

    

	
}
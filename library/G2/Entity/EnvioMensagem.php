<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_enviomensagem")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class EnvioMensagem
{
    const ENVIADO = 22;
    const NAO_ENVIADO = 23;
    const ERRO_AO_ENVIAR = 24;

    /**
     * @var integer $id_enviomensagem
     * @Column(name="id_enviomensagem", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_enviomensagem;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;


    /**
     * @var datetime2 $dt_envio
     * @Column(name="dt_envio", type="datetime2", nullable=true, length=8)
     */
    private $dt_envio;


    /**
     * @var datetime2 $dt_enviar
     * @Column(name="dt_enviar", type="datetime2", nullable=false, length=6)
     */
    private $dt_enviar;


    /**
     * @var datetime2 $dt_tentativa
     * @Column(name="dt_tentativa", type="datetime2", nullable=true, length=8)
     */
    private $dt_tentativa;

    /**
     * @var datetime2 $dt_saida
     * @Column(name="dt_saida", type="datetime2", nullable=true, length=8)
     */
    private $dt_saida;

    /**
     * @var integer $id_mensagem
     * @ManyToOne(targetEntity="Mensagem")
     * @JoinColumn(name="id_mensagem", referencedColumnName="id_mensagem")
     */
    private $id_mensagem;


    /**
     * @var integer $id_tipoenvio
     * @ManyToOne(targetEntity="TipoEnvio")
     * @JoinColumn(name="id_tipoenvio", referencedColumnName="id_tipoenvio")
     */
    private $id_tipoenvio;


    /**
     * @var integer $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;


    /**
     * @var integer $id_emailconfig
     * @ManyToOne(targetEntity="EmailConfig")
     * @JoinColumn(name="id_emailconfig", referencedColumnName="id_emailconfig")
     */
    private $id_emailconfig;


    /**
     * @var integer $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $id_sistema;


    public function getId_enviomensagem()
    {
        return $this->id_enviomensagem;
    }

    public function getId_emailconfig()
    {
        return $this->id_emailconfig;
    }

    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    public function getId_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getDt_envio()
    {
        return $this->dt_envio;
    }

    public function getDt_enviar()
    {
        return $this->dt_enviar;
    }

    public function getDt_tentativa()
    {
        return $this->dt_tentativa;
    }

    public function getDt_saida()
    {
        return $this->dt_saida;
    }


    public function setId_enviomensagem($id_enviomensagem)
    {
        $this->id_enviomensagem = $id_enviomensagem;
        return $this;
    }

    public function setId_emailconfig(\G2\Entity\EmailConfig $id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    public function setId_mensagem(\G2\Entity\Mensagem $id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
        return $this;
    }

    public function setId_tipoenvio(\G2\Entity\TipoEnvio $id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    public function setId_evolucao(\G2\Entity\Evolucao $id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setDt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
        return $this;
    }

    public function setDt_enviar($dt_enviar)
    {
        $this->dt_enviar = $dt_enviar;
        return $this;
    }

    public function setDt_tentativa($dt_tentativa)
    {
        $this->dt_tentativa = $dt_tentativa;
        return $this;
    }

    public function setDt_saida($dt_saida)
    {
        $this->dt_saida = $dt_saida;
        return $this;
    }

    public function setId_sistema(\G2\Entity\Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    public function getId_sistema()
    {
        return $this->id_sistema;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_regrapagamento")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class RegraPagamento {

    /**
     * @var RegraPagamento
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(type="integer")
     */
    private $id_regrapagamento;

    /**
     * @var TipoRegraPagamento
     * @ManyToOne(targetEntity="TipoRegraPagamento")
     * @JoinColumn(name="id_tiporegrapagamento", referencedColumnName="id_tiporegrapagamento")
     */
    private $id_tiporegrapagamento;

    /**
     * @var Entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var AreaConhecimento
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", referencedColumnName="id_areaconhecimento")
     */
    private $id_areaconhecimento;

    /**
     * @var ProjetoPedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_professor", referencedColumnName="id_usuario")
     */
    private $id_professor;

    /**
     * @var CargaHoraria
     * @ManyToOne(targetEntity="CargaHoraria")
     * @JoinColumn(name="id_cargahoraria", referencedColumnName="id_cargahoraria")
     */
    private $id_cargahoraria;

    /**
     * @var float
     * @Column(type="float")
     */
    private $nu_valor;

    /**
     * @var TipoDisciplina
     * @ManyToOne(targetEntity="TipoDisciplina")
     * @JoinColumn(name="id_tipodisciplina", referencedColumnName="id_tipodisciplina")
     */
    private $id_tipodisciplina;

    /**
     * @var \DateTime
     * @Column(type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @var boolean
     * @Column(type="boolean")
     */
    private $bl_ativo;

    public function getId_regrapagamento() {
        return $this->id_regrapagamento;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_professor() {
        return $this->id_professor;
    }

    public function getId_cargahoraria() {
        return $this->id_cargahoraria;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_regrapagamento(RegraPagamento $id_regrapagamento) {
        $this->id_regrapagamento = $id_regrapagamento;
    }

    public function setId_entidade(Entidade $id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_areaconhecimento(AreaConhecimento $id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setId_projetopedagogico(ProjetoPedagogico $id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_professor(Usuario $id_professor) {
        $this->id_professor = $id_professor;
    }

    public function setId_cargahoraria(CargaHoraria $id_cargahoraria) {
        $this->id_cargahoraria = $id_cargahoraria;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function setId_tipodisciplina(TipoDisciplina $id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function setDt_cadastro(\DateTime $dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getId_tiporegrapagamento() {
        return $this->id_tiporegrapagamento;
    }

    public function setId_tiporegrapagamento(TipoRegraPagamento $id_tiporegrapagamento) {
        $this->id_tiporegrapagamento = $id_tiporegrapagamento;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

}

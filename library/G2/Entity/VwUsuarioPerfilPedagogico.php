<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_usuarioperfilpedagogico")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-09
 */
class VwUsuarioPerfilPedagogico {

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;


    /**
     * @var date $dt_nascimento
     * @Column(name="dt_nascimento", type="date", nullable=true)
     */
    private $dt_nascimento;

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true)
     */
    private $dt_termino;

    /**
     * @var date $dt_inicioturma
     * @Column(name="dt_inicioturma", type="date", nullable=true)
     */
    private $dt_inicioturma;

    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false)
     */
    private $id_perfil;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true)
     */
    private $id_perfilpedagogico;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true)
     */
    private $id_trilha;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true)
     */
    private $id_saladeaula;

    /**
     * @var integer $st_saladeaula
     * @Column(name="st_saladeaula", type="integer", nullable=true)
     */
    private $st_saladeaula;

    /**
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    /**
     * @var integer $id_entidadeportal
     * @Column(name="id_entidadeportal", type="integer", nullable=true)
     */
    private $id_entidadeportal;

    /**
     * @var boolean $bl_bloqueiadisciplina
     * @Column(name="bl_bloqueiadisciplina", type="boolean", nullable=true)
     */
    private $bl_bloqueiadisciplina;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false)
     */
    private $st_login;

    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true)
     */
    private $st_senha;

    /**
     * @var string $st_loginentidade
     * @Column(name="st_loginentidade", type="string", nullable=true)
     */
    private $st_loginentidade;

    /**
     * @var string $st_senhaentidade
     * @Column(name="st_senhaentidade", type="string", nullable=true)
     */
    private $st_senhaentidade;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true)
     */
    private $st_email;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false)
     */
    private $st_razaosocial;

    /**
     * @var string $st_urlimglogo
     * @Column(name="st_urlimglogo", type="string", nullable=true)
     */
    private $st_urlimglogo;

    /**
     * @var string $st_nomeexibicao
     * @Column(name="st_nomeexibicao", type="string", nullable=true)
     */
    private $st_nomeexibicao;

    /**
     * @var string $st_urlavatar
     * @Column(name="st_urlavatar", type="string", nullable=true)
     */
    private $st_urlavatar;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false)
     */
    private $st_nomeperfil;

    /**
     * @var string $st_perfilpedagogico
     * @Column(name="st_perfilpedagogico", type="string", nullable=false)
     */
    private $st_perfilpedagogico;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true)
     */
    private $st_tituloexibicao;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_urlacesso
     * @Column(name="st_urlacesso", type="string", nullable=true)
     */
    private $st_urlacesso;


    /**
     * Substitui a criacao dos metodos GET e SET
     */
    public function __call($method, $params = array()) {
        $action = substr($method, 0, 3);
        $attribute = strtolower(substr($method, 3));

        if ($action == 'get')
            return $this->$attribute;
        else if ($action == 'set') {
            $this->$attribute = implode($params);
            return $this;
        }
    }

}

<?php

namespace G2\Entity;

/**
 * Description of VendaProdutoNivelSerie
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_vendaprodutonivelserie")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VendaProdutoNivelSerie
{

    /**
     * @Id
     * @var integer $id_vendaproduto
     * @OneToOne(targetEntity="VendaProduto")
     * @JoinColumn(name="id_vendaproduto", referencedColumnName="id_vendaproduto")
     */
    private $id_vendaproduto;

    /**
     * @Id
     * @var ProjetoPedagogicoSerieNivelEnsino $id_projetopedagogico
     * @OneToMany(targetEntity="ProjetoPedagogicoSerieNivelEnsino", mappedBy="id_projetopedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @Id
     * @var integer $id_serie
     * @OneToMany(targetEntity="ProjetoPedagogicoSerieNivelEnsino", mappedBy="id_serie")
     * @JoinColumn(name="id_serie", referencedColumnName="id_serie")
     */
    private $id_serie;

    /**
     * @Id
     * @var integer $id_nivelensino
     * @OneToMany(targetEntity="ProjetoPedagogicoSerieNivelEnsino", mappedBy="id_nivelensino")
     * @JoinColumn(name="id_nivelensino", referencedColumnName="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     * @return integer id_vendaproduto
     */
    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param id_vendaproduto
     * @return $this
     */
    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_serie
     */
    public function getId_serie()
    {
        return $this->id_serie;
    }

    /**
     * @param id_serie
     * @return $this
     */
    public function setId_serie($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    /**
     * @return integer id_nivelensino
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param id_nivelensino
     * @return $this
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }
}
<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_disciplinaserienivelensino")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class DisciplinaSerieNivelEnsino
{

    /**
     *
     * @var integer $id_serie
     * @Column(type="integer", nullable=false, name="id_serie")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_serie;

    /**
     *
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;

    /**
     *
     * @var integer $id_nivelensino
     * @Column(type="integer", nullable=false, name="id_nivelensino")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_nivelensino;

    /**
     * @return integer
     */
    public function getId_serie ()
    {
        return $this->id_serie;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * @return integer id_nivelensino
     */
    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param integer $id_serie
     * @return \G2\Entity\DisciplinaSerieNivelEnsino
     */
    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    /**
     * @param integer $id_disciplina
     * @return \G2\Entity\DisciplinaSerieNivelEnsino
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @param integer $id_nivelensino
     * @return \G2\Entity\DisciplinaSerieNivelEnsino
     */
    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

}

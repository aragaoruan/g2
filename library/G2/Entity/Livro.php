<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_livro")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-22-01
 */
class Livro
{

    /**
     *
     * @var integer $id_livro
     * @Column(name="id_livro", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_livro;

    /**
     *
     * @var integer $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     *
     * @var integer $id_tipolivro
     * @ManyToOne(targetEntity="TipoLivro")
     * @JoinColumn(name="id_tipolivro", nullable=false, referencedColumnName="id_tipolivro")
     */
    private $id_tipolivro;

    /**
     *
     * @var integer $id_livrocolecao
     * @ManyToOne(targetEntity="LivroColecao")
     * @JoinColumn(name="id_livrocolecao", nullable=false, referencedColumnName="id_livrocolecao")
     */
    private $id_livrocolecao;

    /**
     *
     * @var string $st_livro
     * @Column(name="st_livro", type="string", nullable=false, length=250)
     */
    private $st_livro;

    /**
     *
     * @var string $st_isbn
     * @Column(name="st_isbn", type="string", nullable=true, length=50)
     */
    private $st_isbn;

    /**
     *
     * @var string $st_codigocontrole
     * @Column(name="st_codigocontrole", type="string", nullable=true, length=50)
     */
    private $st_codigocontrole;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var string $st_edicao
     * @Column(type="string", length=50, nullable=true, name="st_edicao") 
     */
    private $st_edicao;

    /**
     * @var integer $nu_anolancamento
     * @Column(type="integer", length=4, nullable=true, name="nu_anolancamento") 
     */
    private $nu_anolancamento;

    /**
     * @var integer $nu_pagina
     * @Column(type="integer", length=4, nullable=true, name="nu_pagina") 
     */
    private $nu_pagina;

    /**
     * @var integer $nu_peso
     * @Column(type="integer", length=4, nullable=true, name="nu_peso") 
     */
    private $nu_peso;

    /**
     * @var UsuarioPerfilEntidadeReferencia $autores
     * @OneToMany(targetEntity="UsuarioPerfilEntidadeReferencia", mappedBy="id_livro") 
     */
    private $autores;

    /**
     * @return UsuarioPerfilEntidadeReferencia
     */
    public function getAutores ()
    {
        return $this->autores;
    }

//    public function setAutores (UsuarioPerfilEntidadeReferencia $autores)
//    {
//        $this->autores = $autores;
//    }

    /**
     * @return string
     */
    public function getSt_edicao ()
    {
        return $this->st_edicao;
    }

    /**
     * 
     * @param string $st_edicao
     * @return \G2\Entity\Livro
     */
    public function setSt_edicao ($st_edicao)
    {
        $this->st_edicao = $st_edicao;
        return $this;
    }

    /**
     * 
     * @return integer nu_anolancamento
     */
    public function getNu_anolancamento ()
    {
        return $this->nu_anolancamento;
    }

    /**
     * 
     * @param integer $nu_anolancamento
     * @return \G2\Entity\Livro
     */
    public function setNu_anolancamento ($nu_anolancamento)
    {
        $this->nu_anolancamento = $nu_anolancamento;
        return $this;
    }

    /**
     * 
     * @return integer nu_pagina
     */
    public function getNu_pagina ()
    {
        return $this->nu_pagina;
    }

    /**
     * @param integer $nu_pagina
     * @return \G2\Entity\Livro
     */
    public function setNu_pagina ($nu_pagina)
    {
        $this->nu_pagina = $nu_pagina;
        return $this;
    }

    /**
     * @return integer nu_peso
     */
    public function getNu_peso ()
    {
        return $this->nu_peso;
    }

    /**
     * 
     * @param integer $nu_peso
     * @return \G2\Entity\Livro
     */
    public function setNu_peso ($nu_peso)
    {
        $this->nu_peso = $nu_peso;
        return $this;
    }

    /**
     * @return the $id_livro
     */
    public function getId_livro ()
    {
        return $this->id_livro;
    }

    /**
     * @return the $id_entidadecadastro
     */
    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_tipolivro
     */
    public function getId_tipolivro ()
    {
        return $this->id_tipolivro;
    }

    /**
     * @return the $id_livrocolecao
     */
    public function getId_livrocolecao ()
    {
        return $this->id_livrocolecao;
    }

    /**
     * @return the $st_livro
     */
    public function getSt_livro ()
    {
        return $this->st_livro;
    }

    /**
     * @return the $st_isbn
     */
    public function getSt_isbn ()
    {
        return $this->st_isbn;
    }

    /**
     * @return the $st_codigocontrole
     */
    public function getSt_codigocontrole ()
    {
        return $this->st_codigocontrole;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @param number $id_livro
     */
    public function setId_livro ($id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    /**
     * @param number $id_entidadecadastro
     */
    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @param number $id_usuariocadastro
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param number $id_situacao
     */
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param number $id_tipolivro
     */
    public function setId_tipolivro ($id_tipolivro)
    {
        $this->id_tipolivro = $id_tipolivro;
        return $this;
    }

    /**
     * @param number $id_livrocolecao
     */
    public function setId_livrocolecao ($id_livrocolecao)
    {
        $this->id_livrocolecao = $id_livrocolecao;
        return $this;
    }

    /**
     * @param string $st_livro
     */
    public function setSt_livro ($st_livro)
    {
        $this->st_livro = $st_livro;
        return $this;
    }

    /**
     * @param string $st_isbn
     */
    public function setSt_isbn ($st_isbn)
    {
        $this->st_isbn = $st_isbn;
        return $this;
    }

    /**
     * @param string $st_codigocontrole
     */
    public function setSt_codigocontrole ($st_codigocontrole)
    {
        $this->st_codigocontrole = $st_codigocontrole;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

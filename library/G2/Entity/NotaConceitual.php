<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 08/03/18
 * Time: 15:17
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_notaconceitual")
 * @Entity
 * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
 */
class NotaConceitual
{

    /**
     * @var integer $id_notaconceitual
     * @Column(name="id_notaconceitual", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_notaconceitual;

    /**
     * @var string $st_notaconceitual
     * @Column(name="st_notaconceitual", type="string", nullable=false, length=255)
     */
    private $st_notaconceitual;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @return int
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param int $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }

    /**
     * @return string
     */
    public function getSt_notaconceitual()
    {
        return $this->st_notaconceitual;
    }

    /**
     * @param string $st_notaconceitual
     */
    public function setSt_notaconceitual($st_notaconceitual)
    {
        $this->st_notaconceitual = $st_notaconceitual;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }


}

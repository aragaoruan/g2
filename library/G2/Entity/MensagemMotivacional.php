<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_mensagemmotivacional")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class MensagemMotivacional
{

    /**
     *
     * @var integer $id_mensagemmotivacional
     * @Column(name="id_mensagemmotivacional", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_mensagemmotivacional;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var text $st_mensagemmotivacional
     * @Column(name="st_mensagemmotivacional", type="text", nullable=false)
     */
    private $st_mensagemmotivacional;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    public function getId_mensagemmotivacional ()
    {
        return $this->id_mensagemmotivacional;
    }

    public function setId_mensagemmotivacional ($id_mensagemmotivacional)
    {
        $this->id_mensagemmotivacional = $id_mensagemmotivacional;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getSt_mensagemmotivacional ()
    {
        return $this->st_mensagemmotivacional;
    }

    public function setSt_mensagemmotivacional ($st_mensagemmotivacional)
    {
        $this->st_mensagemmotivacional = $st_mensagemmotivacional;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @Table (name="tb_itemconfiguracao")
 * @Entity()
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 */
class ItemConfiguracao extends G2Entity
{

    /**
     *
     * @var integer $id_itemconfiguracao
     * @Column(name="id_itemconfiguracao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemconfiguracao;

    /**
     *
     * @var string $st_itemconfiguracao
     * @Column(name="st_itemconfiguracao", type="string", nullable=false, length=100)
     */
    private $st_itemconfiguracao;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false)
     */
    private $st_descricao;

    /**
     *
     * @var string $st_default
     * @Column(name="st_default", type="string", nullable=false, length=200)
     */
    private $st_default;

    /**
     * @return int
     */
    public function getid_itemconfiguracao()
    {
        return $this->id_itemconfiguracao;
    }

    /**
     * @param $id_itemconfiguracao
     * @return $this
     */
    public function setid_itemconfiguracao($id_itemconfiguracao)
    {
        $this->id_itemconfiguracao = $id_itemconfiguracao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_itemconfiguracao()
    {
        return $this->st_itemconfiguracao;
    }

    /**
     * @param $st_itemconfiguracao
     * @return $this
     */
    public function setst_itemconfiguracao($st_itemconfiguracao)
    {
        $this->st_itemconfiguracao = $st_itemconfiguracao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param $st_descricao
     * @return $this
     */
    public function setst_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_default()
    {
        return $this->st_default;
    }

    /**
     * @param $st_default
     * @return $this
     */
    public function setst_default($st_default)
    {
        $this->st_default = $st_default;
        return $this;

    }

}

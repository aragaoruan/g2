<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_meiopagamento")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class MeioPagamento extends G2Entity
{

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_meiopagamento;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=false, length=255)
     */
    private $st_meiopagamento;

    /**
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=false)
     */
    private $st_descricao;

    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    public function setId_meiopagamento ($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    public function getSt_meiopagamento ()
    {
        return $this->st_meiopagamento;
    }

    public function setSt_meiopagamento ($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_campanhacomercial")
 * @Entity(repositoryClass="\G2\Repository\CampanhaComercial")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CampanhaComercial
{

    /**
     *
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_campanhacomercial;

    /**
     *
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=false, length=255)
     */
    private $st_campanhacomercial;

    /**
     *
     * @var boolean $bl_aplicardesconto
     * @Column(name="bl_aplicardesconto", type="boolean", nullable=true)
     */
    private $bl_aplicardesconto;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_disponibilidade
     * @Column(name="bl_disponibilidade", type="boolean", nullable=false)
     */
    private $bl_disponibilidade;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var datetime2 $dt_fim
     * @Column(name="dt_fim", type="datetime2", nullable=true)
     */
    private $dt_fim;

    /**
     *
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var TipoDesconto $id_tipodesconto
     * @ManyToOne(targetEntity="TipoDesconto")
     * @JoinColumn(name="id_tipodesconto", nullable=true, referencedColumnName="id_tipodesconto")
     */
    private $id_tipodesconto;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var TipoCampanha $id_tipocampanha
     * @ManyToOne(targetEntity="TipoCampanha")
     * @JoinColumn(name="id_tipocampanha", nullable=false, referencedColumnName="id_tipocampanha")
     */
    private $id_tipocampanha;

    /**
     *
     * @var integer $nu_disponibilidade
     * @Column(name="nu_disponibilidade", type="integer", nullable=true)
     */
    private $nu_disponibilidade;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=2500)
     */
    private $st_descricao;

    /**
     *
     * @var boolean $bl_todosprodutos
     * @Column(name="bl_todosprodutos", type="boolean", nullable=true)
     */
    private $bl_todosprodutos;

    /**
     *
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=true)
     */
    private $bl_todasformas;

    /**
     * @var CategoriaCampanha $id_situacao
     * @ManyToOne(targetEntity="CategoriaCampanha")
     * @JoinColumn(name="id_categoriacampanha", nullable=false, referencedColumnName="id_categoriacampanha")
     */
    private $id_categoriacampanha;

    /**
     * @var decimal $nu_valordesconto
     * @Column(type="decimal", nullable=false, name="nu_valordesconto")
     */
    private $nu_valordesconto;

    /**
     * @var integer $nu_diasprazo
     * @Column(type="integer",nullable=true, name="nu_diasprazo")
     */
    private $nu_diasprazo;

    /**
     * @var Premio $id_premio
     * @ManyToOne(targetEntity="Premio")
     * @JoinColumn(name="id_premio", nullable=false, referencedColumnName="id_premio")
     */
    private $id_premio;

    /**
     * @var FinalidadeCampanha $id_finalidadecampanha
     * @ManyToOne(targetEntity="FinalidadeCampanha")
     * @JoinColumn(name="id_finalidadecampanha", nullable=false, referencedColumnName="id_finalidadecampanha")
     */
    private $id_finalidadecampanha;

    /**
     * @var string $st_link
     * @Column(name="st_link", type="string", nullable=true, length=255)
     */
    private $st_link;

    /**
     * @var string $st_imagem
     * @Column(name="st_imagem", type="string", nullable=true, length=255)
     */
    private $st_imagem;

    /**
     * @var boolean $bl_mobile
     * @Column(name="bl_mobile", type="boolean", nullable=true, length=1)
     */
    private $bl_mobile;

    /**
     * @var boolean $bl_portaldoaluno
     * @Column(name="bl_portaldoaluno", type="boolean", nullable=true, length=1)
     */

    private $bl_portaldoaluno;

    /**
     * @return int
     */
    public function getNu_diasprazo()
    {
        return $this->nu_diasprazo;
    }

    /**
     * @param integer $nu_diasprazo
     * @return \G2\Entity\CampanhaComercial
     */
    public function setNu_diasprazo($nu_diasprazo)
    {
        $this->nu_diasprazo = $nu_diasprazo;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_valordesconto()
    {
        return $this->nu_valordesconto;
    }

    /**
     * @param decimal $nu_valordesconto
     * @return \G2\Entity\CampanhaComercial
     */
    public function setNu_valordesconto($nu_valordesconto)
    {
        $this->nu_valordesconto = $nu_valordesconto;
        return $this;
    }


    /**
     * @return CategoriaCampanha
     */
    public function getId_categoriacampanha()
    {
        return $this->id_categoriacampanha;
    }

    /**
     * @param \G2\Entity\CategoriaCampanha $id_categoriacampanha
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_categoriacampanha(CategoriaCampanha $id_categoriacampanha)
    {
        $this->id_categoriacampanha = $id_categoriacampanha;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param integer $id_campanhacomercial
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @param string $st_campanhacomercial
     * @return \G2\Entity\CampanhaComercial
     */
    public function setSt_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_aplicardesconto()
    {
        return $this->bl_aplicardesconto;
    }

    /**
     * @param boolean $bl_aplicardesconto
     * @return \G2\Entity\CampanhaComercial
     */
    public function setBl_aplicardesconto($bl_aplicardesconto)
    {
        $this->bl_aplicardesconto = $bl_aplicardesconto;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return \G2\Entity\CampanhaComercial
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_disponibilidade()
    {
        return $this->bl_disponibilidade;
    }

    /**
     * @param boolean $bl_disponibilidade
     * @return \G2\Entity\CampanhaComercial
     */
    public function setBl_disponibilidade($bl_disponibilidade)
    {
        $this->bl_disponibilidade = $bl_disponibilidade;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\CampanhaComercial
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param datetime2 $dt_fim
     * @return \G2\Entity\CampanhaComercial
     */
    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param datetime2 $dt_inicio
     * @return \G2\Entity\CampanhaComercial
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return TipoDesconto
     */
    public function getId_tipodesconto()
    {
        return $this->id_tipodesconto;
    }

    /**
     *
     * @param \G2\Entity\TipoDesconto $id_tipodesconto
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_tipodesconto(TipoDesconto $id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     *
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return TipoCampanha
     */
    public function getId_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param \G2\Entity\TipoCampanha $id_tipocampanha
     * @return \G2\Entity\CampanhaComercial
     */
    public function setId_tipocampanha(TipoCampanha $id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_disponibilidade()
    {
        return $this->nu_disponibilidade;
    }

    /**
     * @param integer $nu_disponibilidade
     * @return \G2\Entity\CampanhaComercial
     */
    public function setNu_disponibilidade($nu_disponibilidade)
    {
        $this->nu_disponibilidade = $nu_disponibilidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param text $st_descricao
     * @return \G2\Entity\CampanhaComercial
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_todosprodutos()
    {
        return $this->bl_todosprodutos;
    }

    /**
     * @param boolean $bl_todosprodutos
     * @return \G2\Entity\CampanhaComercial
     */
    public function setBl_todosprodutos($bl_todosprodutos)
    {
        $this->bl_todosprodutos = $bl_todosprodutos;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @param boolean $bl_todasformas
     * @return \G2\Entity\CampanhaComercial
     */
    public function setBl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;
    }

    /**
     * @return integer id_cupompremio
     */
    public function getId_premio()
    {
        return $this->id_premio;
    }

    /**
     * @param $id_premio
     * @return $this
     */
    public function setId_premio($id_premio)
    {
        $this->id_premio = $id_premio;
        return $this;
    }

    /**
     * @return FinalidadeCampanha
     */
    public function getId_finalidadecampanha()
    {
        return $this->id_finalidadecampanha;
    }

    /**
     * @param FinalidadeCampanha $id_finalidadecampanha
     */
    public function setId_finalidadecampanha($id_finalidadecampanha)
    {
        $this->id_finalidadecampanha = $id_finalidadecampanha;
    }

    /**
     * @return Link
     */
    public function getSt_link()
    {
        return $this->st_link;
    }

    /**
     * @param Link $st_link
     * @return $this
     */
    public function setSt_link($st_link)
    {
        $this->st_link = $st_link;
        return $this;
    }

    /**
     * @return String
     */
    public function getSt_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param String $st_imagem
     * $return $this
     */
    public function setSt_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
    }

    /**
     * @return Integer
     */
    public function getBl_portaldoaluno()
    {
        return $this->bl_portaldoaluno;
    }

    /**
     *
     * @param $bl_portaldoaluno
     * @return $this
     */
    public function setBl_portaldoaluno($bl_portaldoaluno)
    {
        $this->bl_portaldoaluno = $bl_portaldoaluno;
        return $this;
    }


    /**
     * @return Integer
     */
    public function getBl_mobile()
    {
        return $this->bl_mobile;
    }

    /**
     *
     * @param $bl_mobile
     * @return $this
     */
    public function setBl_mobile($bl_mobile)
    {
        $this->bl_mobile = $bl_mobile;
        return $this;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 02/07/14
 * Time: 15:28
 */

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_transacaolancamento")
 * @Entity
 */
class TransacaoLancamento extends G2Entity
{

    /**
     * @var integer $id_transacaofinanceira
     * @Column(name="id_transacaofinanceira ", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_transacaofinanceira;

    /**
     *
     * @var integer $id_lancamento
     * @Column(name="id_lancamento ", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_lancamento;


    /**
     * @param integer $id_lancamento
     * @deprecated
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return integer
     * @deprecated
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param integer $id_transacaofinanceira
     * @deprecated
     */
    public function setIdTransacaofinanceira($id_transacaofinanceira)
    {
        $this->id_transacaofinanceira = $id_transacaofinanceira;
    }

    /**
     * @return integer
     * @deprecated
     */
    public function getIdTransacaofinanceira()
    {
        return $this->id_transacaofinanceira;
    }

    /**
     * @return int
     */
    public function getId_transacaofinanceira()
    {
        return $this->id_transacaofinanceira;
    }

    /**
     * @return int
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_transacaofinanceira
     * @return $this
     */
    public function setId_transacaofinanceira($id_transacaofinanceira)
    {
        $this->id_transacaofinanceira = $id_transacaofinanceira;
        return $this;
    }

    /**
     * @param int $id_lancamento
     * @return $this
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }


}
<?php

/*
 * Entity FormaPagamentoAplicacaoRelacao
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-29
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formapagamentoaplicacaorelacao")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class FormaPagamentoAplicacaoRelacao
{

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;

    /**
     *
     * @var integer $id_formapagamentoaplicacao
     * @Column(name="id_formapagamentoaplicacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamentoaplicacao;


    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    public function getId_formapagamentoaplicacao()
    {
        return $this->id_formapagamentoaplicacao;
    }

    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao)
    {
        $this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
    }

}

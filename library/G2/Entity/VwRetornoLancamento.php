<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity()
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_retornolancamento")
 */
class VwRetornoLancamento {

    /**
     * @var $id_arquivoretorno
     * @Column(type="integer")
     */
    private $id_arquivoretorno;

    /**
     * @var $st_arquivoretorno
     * @Column(type="string")
     */
    private $st_arquivoretorno;

    /**
     * @var $id_entidade
     * @Column(type="integer")
     */
    private $id_entidade;

    /**
     * @var $bl_ativoretorno
     * @Column(type="boolean")
     */
    private $bl_ativoretorno;

    /**
     * @var $st_banco
     * @Column(type="string")
     */
    private $st_banco;

    /**
     * @var integer $id_arquivoretornolancamento
     * @Column(name="id_arquivoretornolancamento", type="integer")
     * @Id
     */
    private $id_arquivoretornolancamento;

    /**
     * @var $st_nossonumero
     * @Column(type="string")
     */
    private $st_nossonumero;

    /**
     * @var $nu_valorretorno
     * @Column(type="decimal")
     */
    private $nu_valorretorno;

    /**
     * @var $nu_descontoretorno
     * @Column(type="decimal")
     */
    private $nu_descontoretorno;

    /**
     * @var $nu_jurosretorno
     * @Column(type="decimal")
     */
    private $nu_jurosretorno;

    /**
     * @var $dt_ocorrenciaretorno
     * @Column(type="datetime")
     */
    private $dt_ocorrenciaretorno;

    /**
     * @var $bl_quitadoretorno
     * @Column(type="boolean")
     */
    private $bl_quitadoretorno;

    /**
     * @var $nu_valor
     * @Column(type="decimal")
     */
    private $nu_valor;

    /**
     * @var $id_meiopagamento
     * @Column(type="integer")
     */
    private $id_meiopagamento;

    /**
     * @var $bl_quitado
     * @Column(type="boolean")
     */
    private $bl_quitado;

    /**
     * @var $dt_vencimento
     * @Column(type="datetime")
     */
    private $dt_vencimento;

    /**
     * @var $dt_quitado
     * @Column(type="datetime")
     */
    private $dt_quitado;

    /**
     * @var $dt_emissao
     * @Column(type="datetime")
     */
    private $dt_emissao;

    /**
     * @var $dt_prevquitado
     * @Column(type="datetime")
     */
    private $dt_prevquitado;

    /**
     * @var $id_entidadelancamento
     * @Column(type="integer")
     */
    private $id_entidadelancamento;

    /**
     * @var $bl_ativo
     * @Column(type="boolean")
     */
    private $bl_ativo;

    /**
     * @var $id_lancamento
     * @Column(type="integer")
     */
    private $id_lancamento;

    /**
     * @var $st_coddocumento
     * @Column(type="string")
     */
    private $st_coddocumento;

    /**
     * @var $st_carteira
     * @Column(type="string")
     */
    private $st_carteira;

    /**
     * @var $nu_valornominal
     * @Column(type="decimal")
     */
    private $nu_valornominal;

    /**
     * @var $dt_quitadoretorno
     * @Column(type="datetime")
     */
    private $dt_quitadoretorno;

    /**
     * @var $nu_tarifa
     * @Column(type="decimal")
     */
    private $nu_tarifa;

    private $nu_valortotal;

    /**
     * @var $id_venda
     * @Column(type="integer")
     */
    private $id_venda;

    /**
     * @param mixed $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return mixed
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativoretorno
     */
    public function setBl_ativoretorno($bl_ativoretorno)
    {
        $this->bl_ativoretorno = $bl_ativoretorno;
    }

    /**
     * @return mixed
     */
    public function getBl_ativoretorno()
    {
        return $this->bl_ativoretorno;
    }

    /**
     * @param mixed $bl_quitado
     */
    public function setBl_quitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @return mixed
     */
    public function getBl_quitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @param mixed $bl_quitadoretorno
     */
    public function setBl_quitadoretorno($bl_quitadoretorno)
    {
        $this->bl_quitadoretorno = $bl_quitadoretorno;
    }

    /**
     * @return mixed
     */
    public function getBl_quitadoretorno()
    {
        return $this->bl_quitadoretorno;
    }

    /**
     * @param mixed $dt_emissao
     */
    public function setDt_emissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @return mixed
     */
    public function getDt_emissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @param mixed $dt_ocorrenciaretorno
     */
    public function setDt_ocorrenciaretorno($dt_ocorrenciaretorno)
    {
        $this->dt_ocorrenciaretorno = $dt_ocorrenciaretorno;
    }

    /**
     * @return mixed
     */
    public function getDt_ocorrenciaretorno()
    {
        return $this->dt_ocorrenciaretorno;
    }

    /**
     * @param mixed $dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @return mixed
     */
    public function getDt_prevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @param mixed $dt_quitado
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @return mixed
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param mixed $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @return mixed
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param mixed $id_arquivoretorno
     */
    public function setId_arquivoretorno($id_arquivoretorno)
    {
        $this->id_arquivoretorno = $id_arquivoretorno;
    }

    /**
     * @return mixed
     */
    public function getId_arquivoretorno()
    {
        return $this->id_arquivoretorno;
    }

    /**
     * @param mixed $id_arquivoretornolancamento
     */
    public function setId_arquivoretornolancamento($id_arquivoretornolancamento)
    {
        $this->id_arquivoretornolancamento = $id_arquivoretornolancamento;
    }

    /**
     * @return mixed
     */
    public function getId_arquivoretornolancamento()
    {
        return $this->id_arquivoretornolancamento;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidadelancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @return mixed
     */
    public function getId_entidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @param mixed $id_lancamento
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return mixed
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param mixed $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return mixed
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param mixed $nu_descontoretorno
     */
    public function setNu_descontoretorno($nu_descontoretorno)
    {
        $this->nu_descontoretorno = $nu_descontoretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_descontoretorno()
    {
        return $this->nu_descontoretorno;
    }

    /**
     * @param mixed $nu_jurosretorno
     */
    public function setNu_jurosretorno($nu_jurosretorno)
    {
        $this->nu_jurosretorno = $nu_jurosretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_jurosretorno()
    {
        return $this->nu_jurosretorno;
    }

    /**
     * @param mixed $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return mixed
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param mixed $nu_valorretorno
     */
    public function setNu_valorretorno($nu_valorretorno)
    {
        $this->nu_valorretorno = $nu_valorretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_valorretorno()
    {
        return $this->nu_valorretorno;
    }

    /**
     * @param mixed $st_arquivoretorno
     */
    public function setSt_arquivoretorno($st_arquivoretorno)
    {
        $this->st_arquivoretorno = $st_arquivoretorno;
    }

    /**
     * @return mixed
     */
    public function getSt_arquivoretorno()
    {
        return $this->st_arquivoretorno;
    }

    /**
     * @param mixed $st_banco
     */
    public function setStBanco($st_banco)
    {
        $this->st_banco = $st_banco;
    }

    /**
     * @return mixed
     */
    public function getSt_banco()
    {
        return $this->st_banco;
    }

    /**
     * @param mixed $st_coddocumento
     */
    public function setSt_coddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @return mixed
     */
    public function getSt_coddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @param mixed $st_nossonumero
     */
    public function setSt_nossonumero($st_nossonumero)
    {
        $this->st_nossonumero = $st_nossonumero;
    }

    /**
     * @return mixed
     */
    public function getSt_nossonumero()
    {
        return $this->st_nossonumero;
    }

    /**
     * @param mixed $st_carteira
     */
    public function setSt_carteira($st_carteira)
    {
        $this->st_carteira = $st_carteira;
    }

    /**
     * @return mixed
     */
    public function getSt_carteira()
    {
        return $this->st_carteira;
    }

    /**
     * @param mixed $nu_tarifa
     */
    public function setNu_tarifa($nu_tarifa)
    {
        $this->nu_tarifa = $nu_tarifa;
    }

    /**
     * @return mixed
     */
    public function getNu_tarifa()
    {
        return $this->nu_tarifa;
    }

    /**
     * @param mixed $nu_valornominal
     */
    public function setNu_valornominal($nu_valornominal)
    {
        $this->nu_valornominal = $nu_valornominal;
    }

    /**
     * @return mixed
     */
    public function getNu_valornominal()
    {
        return $this->nu_valornominal;
    }

    /**
     * @param mixed $dt_quitadoretorno
     */
    public function setDt_quitadoretorno($dt_quitadoretorno)
    {
        $this->dt_quitadoretorno = $dt_quitadoretorno;
    }

    /**
     * @return mixed
     */
    public function getDt_quitadoretorno()
    {
        return $this->dt_quitadoretorno;
    }

    /**
     * @return mixed
     */
    public function getNu_Valortotal()
    {
        return $this->nu_valortotal;
    }

    /**
     * @param mixed $nu_valortotal
     */
    public function setNu_Valortotal($nu_valortotal)
    {
        $this->nu_valortotal = $nu_valortotal;
    }

    /**
     * @param mixed $id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param mixed $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }
} 
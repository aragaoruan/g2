<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_categoriaentidade")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
use G2\G2Entity;

class VwCategoriaEntidade extends G2Entity
{
/**
* @var integer $id_categoria
* @Column(name="id_categoria", type="integer", nullable=false, length=4)
* @Id
* @GeneratedValue(strategy="NONE")
*/

private $id_categoria;
    /**
* @var datetime2 $dt_cadastro
* @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
*/
 private $dt_cadastro;

/**
* @var integer $id_usuariocadastro
* @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
*/
 private $id_usuariocadastro;
/**
* @var integer $id_entidadecadastro
* @Column(name="id_entidadecadastro", type="integer", nullable=false, length=4)
*/
 private $id_entidadecadastro;
/**
* @var integer $id_uploadimagem
* @Column(name="id_uploadimagem", type="integer", nullable=true, length=4)
*/
 private $id_uploadimagem;
/**
* @var integer $id_categoriapai
* @Column(name="id_categoriapai", type="integer", nullable=true, length=4)
*/
 private $id_categoriapai;
/**
* @var integer $id_situacao
* @Column(name="id_situacao", type="integer", nullable=true, length=4)
*/
 private $id_situacao;
/**
* @var integer $id_entidade
* @Column(name="id_entidade", type="integer", nullable=false, length=4)
*/
 private $id_entidade;
/**
* @var boolean $bl_ativo
* @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
*/
 private $bl_ativo;
/**
* @var string $st_nomeexibicao
* @Column(name="st_nomeexibicao", type="string", nullable=false, length=200)
*/
 private $st_nomeexibicao;
/**
* @var string $st_categoria
* @Column(name="st_categoria", type="string", nullable=false, length=200)
*/
 private $st_categoria;
 
 
/**
* @return datetime2 dt_cadastro
*/
public function getDt_cadastro() {
return $this->dt_cadastro;
}

/**
* @param dt_cadastro
*/
public function setDt_cadastro($dt_cadastro) {
$this->dt_cadastro = $dt_cadastro;
return $this;
}

/**
* @return integer id_categoria
*/
public function getId_categoria() {
return $this->id_categoria;
}

/**
* @param id_categoria
*/
public function setId_categoria($id_categoria) {
$this->id_categoria = $id_categoria;
return $this;
}

/**
* @return integer id_usuariocadastro
*/
public function getId_usuariocadastro() {
return $this->id_usuariocadastro;
}

/**
* @param id_usuariocadastro
*/
public function setId_usuariocadastro($id_usuariocadastro) {
$this->id_usuariocadastro = $id_usuariocadastro;
return $this;
}

/**
* @return integer id_entidadecadastro
*/
public function getId_entidadecadastro() {
return $this->id_entidadecadastro;
}

/**
* @param id_entidadecadastro
*/
public function setId_entidadecadastro($id_entidadecadastro) {
$this->id_entidadecadastro = $id_entidadecadastro;
return $this;
}

/**
* @return integer id_uploadimagem
*/
public function getId_uploadimagem() {
return $this->id_uploadimagem;
}

/**
* @param id_uploadimagem
*/
public function setId_uploadimagem($id_uploadimagem) {
$this->id_uploadimagem = $id_uploadimagem;
return $this;
}

/**
* @return integer id_categoriapai
*/
public function getId_categoriapai() {
return $this->id_categoriapai;
}

/**
* @param id_categoriapai
*/
public function setId_categoriapai($id_categoriapai) {
$this->id_categoriapai = $id_categoriapai;
return $this;
}

/**
* @return integer id_situacao
*/
public function getId_situacao() {
return $this->id_situacao;
}

/**
* @param id_situacao
*/
public function setId_situacao($id_situacao) {
$this->id_situacao = $id_situacao;
return $this;
}

/**
* @return integer id_entidade
*/
public function getId_entidade() {
return $this->id_entidade;
}

/**
* @param id_entidade
*/
public function setId_entidade($id_entidade) {
$this->id_entidade = $id_entidade;
return $this;
}

/**
* @return boolean bl_ativo
*/
public function getBl_ativo() {
return $this->bl_ativo;
}

/**
* @param bl_ativo
*/
public function setBl_ativo($bl_ativo) {
$this->bl_ativo = $bl_ativo;
return $this;
}

/**
* @return string st_nomeexibicao
*/
public function getSt_nomeexibicao() {
return $this->st_nomeexibicao;
}

/**
* @param st_nomeexibicao
*/
public function setSt_nomeexibicao($st_nomeexibicao) {
$this->st_nomeexibicao = $st_nomeexibicao;
return $this;
}

/**
* @return string st_categoria
*/
public function getSt_categoria() {
return $this->st_categoria;
}

/**
* @param st_categoria
*/
public function setSt_categoria($st_categoria) {
$this->st_categoria = $st_categoria;
return $this;
}

}
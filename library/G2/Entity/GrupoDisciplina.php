<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_grupodisciplina")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2051-12-16
 */

class GrupoDisciplina extends G2Entity
{

    /**
     * @var integer $id_grupodisciplina
     * @Column(type="integer", nullable=false, name="id_grupodisciplina")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_grupodisciplina;

    /**
     * @var string $st_grupodisciplina
     * @Column(type="string", length=255, nullable=false, name="st_grupodisciplina")
     */
    private $st_grupodisciplina;

    /**
     * @return int
     */
    public function getid_grupodisciplina()
    {
        return $this->id_grupodisciplina;
    }

    /**
     * @param int $id_grupodisciplina
     * @return GrupoDisciplina
     */
    public function setid_grupodisciplina($id_grupodisciplina)
    {
        $this->id_grupodisciplina = $id_grupodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_grupodisciplina()
    {
        return $this->st_grupodisciplina;
    }

    /**
     * @param string $st_grupodisciplina
     * @return GrupoDisciplina
     */
    public function setst_grupodisciplina($st_grupodisciplina)
    {
        $this->st_grupodisciplina = $st_grupodisciplina;
        return $this;
    }

}

<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidade")
 * @Entity(repositoryClass="G2\Repository\Entidade")
 * @EntityLog
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Entidade extends G2Entity
{

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo",type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var string $st_siglaentidade
     * @Column(name="st_siglaentidade", type="string", nullable=true, length=20, options={"default":null})
     */
    private $st_siglaentidade;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade",type="string", nullable=true, length=2500, options={"default":null})
     */
    private $st_nomeentidade;

    /**
     * @var string $st_urlimglogo
     * @Column(name="st_urlimglogo",type="string", nullable=true, length=1000, options={"default":null})
     */
    private $st_urlimglogo;

    /**
     * @var boolean $bl_acessasistema
     * @Column(name="bl_acessasistema", type="string", nullable=false)
     */
    private $bl_acessasistema;

    /**
     * @var string $nu_cnpj
     * @Column(name="nu_cnpj", type="string", nullable=true, length=15)
     */
    private $nu_cnpj;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     *
     * @var string $nu_inscricaoestadual
     * @Column(name="nu_inscricaoestadual", type="string", nullable=true, length=2500)
     */
    private $nu_inscricaoestadual;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var string $st_urlsite
     * @Column(name="st_urlsite", type="string", nullable=true, length=2500)
     */
    private $st_urlsite;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_cnpj
     * @Column(name="st_cnpj", type="string", nullable=true, length=15)
     */
    private $st_cnpj;

    /**
     * @var string $st_wschave
     * @Column(name="st_wschave", type="string", nullable=true, length=40)
     */
    private $st_wschave;

    /**
     * @var string $st_conversao
     * @Column(name="st_conversao", type="string", length=255, nullable=true)
     */
    private $st_conversao;

    /**
     *
     * @var $sistemas
     * @ManyToMany(targetEntity="Sistema", mappedBy="entidades")
     */
    private $sistemas;

    /**
     * @var $projetoPedagogico
     */
    private $projetoPedagogico;

    /**
     * @var string $st_urlportal
     * @Column(name="st_urlportal", type="string", nullable=true, length=2500)
     */
    private $st_urlportal;

    /**
     * @var string $str_urlimglogoentidade
     * @Column(name="str_urlimglogoentidade",type="string", nullable=true, length=1000)
     */
    private $str_urlimglogoentidade;
    /**
     * @var string $st_apelido
     * @Column(name="st_apelido",type="string", nullable=true, length=15)
     */
    private $st_apelido;

    /**
     * @var integer $id_uploadloja
     * @Column(name="id_uploadloja", type="integer", nullable=true)
     */
    private $id_uploadloja;

    /**
     * @var string $st_urllogoutportal
     * @Column(name="st_urllogoutportal", type="string", nullable=true, length=7)
     */
    private $st_urllogoutportal;

    /**
     * @var string $st_urlimgapp
     * @Column(name="st_urlimgapp", type="string", nullable=true, length=255)
     */
    private $st_urlimgapp;

    /**
     * @var EsquemaConfiguracao $id_esquemaconfiguracao
     *
     * @ManyToOne(targetEntity="EsquemaConfiguracao")
     * @JoinColumn(name="id_esquemaconfiguracao", nullable=true, referencedColumnName="id_esquemaconfiguracao")
     */
    private $id_esquemaconfiguracao;

    /**
     * @var EsquemaPergunta $id_esquemapergunta
     *
     * @ManyToOne(targetEntity="EsquemaPergunta")
     * @JoinColumn(name="id_esquemapergunta", nullable=true, referencedColumnName="id_esquemapergunta")
     */
    private $id_esquemapergunta;

    /**
     * @var string $st_urlnovoportal
     * @Column(name="st_urlnovoportal", type="string", nullable=true, length=255)
     */
    private $st_urlnovoportal;

    /**
     * @var Usuario $id_usuariosecretariado
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariosecretariado", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariosecretariado;

    public function __construct()
    {
        $this->projetoPedagogico = new ArrayCollection();
        $this->sistemas = new ArrayCollection();
    }

    public function getProjetoPedagogico()
    {
        return $this->projetoPedagogico;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getSt_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    public function setSt_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
        return $this;
    }

    public function getStr_urlimglogoentidade()
    {
        return $this->str_urlimglogoentidade;

    }

    public function setStr_urlimglogoentidade($str_urlimglogoentidade)
    {
        $this->str_urlimglogoentidade = $str_urlimglogoentidade;
        return $this;

    }

    public function getBl_acessasistema()
    {
        return $this->bl_acessasistema;
    }

    public function setBl_acessasistema($bl_acessasistema)
    {
        $this->bl_acessasistema = $bl_acessasistema;
        return $this;
    }

    public function getNu_cnpj()
    {
        return $this->nu_cnpj;
    }

    public function setNu_cnpj($nu_cnpj)
    {
        $this->nu_cnpj = $nu_cnpj;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    public function getNu_inscricaoestadual()
    {
        return $this->nu_inscricaoestadual;
    }

    public function setNu_inscricaoestadual($nu_inscricaoestadual)
    {
        $this->nu_inscricaoestadual = $nu_inscricaoestadual;
        return $this;
    }

    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro(Entidade $id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_urlsite()
    {
        return $this->st_urlsite;
    }

    public function setSt_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getSt_cnpj()
    {
        return $this->st_cnpj;
    }

    public function setSt_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
        return $this;
    }

    public function getSt_wschave()
    {
        return $this->st_wschave;
    }

    public function setSt_wschave($st_wschave)
    {
        $this->st_wschave = $st_wschave;
        return $this;
    }

    public function getSt_conversao()
    {
        return $this->st_conversao;
    }

    public function setSt_conversao($st_conversao)
    {
        $this->st_conversao = $st_conversao;
        return $this;
    }

    public function getSt_urlportal()
    {
        return $this->st_urlportal;
    }

    public function setSt_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
        return $this;
    }

    /**
     * @param string $st_siglaentidade
     */
    public function setSt_siglaentidade($st_siglaentidade)
    {
        $this->st_siglaentidade = $st_siglaentidade;
    }

    /**
     * @return string
     */
    public function getSt_siglaentidade()
    {
        return $this->st_siglaentidade;
    }

    /**
     * @return string
     */
    public function getId_uploadloja()
    {
        return $this->id_uploadloja;
    }

    /**
     * @param string $id_uploadloja
     */
    public function setId_uploadloja($id_uploadloja)
    {
        $this->id_uploadloja = $id_uploadloja;
    }

    /**
     * @return string
     */
    public function getSt_apelido()
    {
        return $this->st_apelido;
    }

    /**
     * @param string $st_apelido
     */
    public function setSt_apelido($st_apelido)
    {
        $this->st_apelido = $st_apelido;
    }

    /**
     * @return string
     */
    public function getSt_urllogoutportal()
    {
        return $this->st_urllogoutportal;
    }

    /**
     * @param string $st_urllogoutportal
     * @return $this
     */
    public function setSt_urllogoutportal($st_urllogoutportal)
    {
        $this->st_urllogoutportal = $st_urllogoutportal;
        return $this;
    }

    /**
     * @param string $st_urlimgapp
     * @return $this
     */
    public function setSt_urlimgapp($st_urlimgapp)
    {
        $this->st_urlimgapp = $st_urlimgapp;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlimgapp()
    {
        return $this->st_urlimgapp;
    }

    /**
     * @return EsquemaConfiguracao
     */
    public function getId_esquemaconfiguracao()
    {
        return $this->id_esquemaconfiguracao;
    }

    /**
     * @param EsquemaConfiguracao $id_esquemaconfiguracao
     * @return $this
     */
    public function setId_esquemaconfiguracao($id_esquemaconfiguracao)
    {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
        return $this;

    }

    /**
     * @return EsquemaPergunta
     */
    public function getId_esquemapergunta()
    {
        return $this->id_esquemapergunta;
    }

    /**
     * @param EsquemaPergunta $id_esquemapergunta
     * @return $this
     */
    public function setId_esquemapergunta($id_esquemapergunta)
    {
        $this->id_esquemapergunta = $id_esquemapergunta;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlnovoportal()
    {
        return $this->st_urlnovoportal;
    }

    /**
     * @param string $st_urlnovoportal
     * @return $this;
     */
    public function setSt_urlnovoportal($st_urlnovoportal)
    {
        $this->st_urlnovoportal = $st_urlnovoportal;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariosecretariado()
    {
        return $this->id_usuariosecretariado;
    }

    /**
     * @param Usuario $id_usuariosecretariado
     * @return $this;
     */
    public function setId_usuariosecretariado($id_usuariosecretariado)
    {
        $this->id_usuariosecretariado = $id_usuariosecretariado;
        return $this;
    }

}

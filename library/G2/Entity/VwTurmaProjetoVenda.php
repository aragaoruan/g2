<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\Id;

/**
 * @Table (name="vw_turmaprojetovenda")
 * @Entity(repositoryClass="\G2\Repository\VwTurmaProjetoVenda")
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwTurmaProjetoVenda
{
    /**
     * @Id
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_venda;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;
    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto() {
        return $this->id_produto;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @return decimal nu_valorliquido
     */
    public function getNu_valorliquido() {
        return $this->nu_valorliquido;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @return string st_produto
     */
    public function getSt_produto() {
        return $this->st_produto;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma() {
        return $this->st_turma;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

}
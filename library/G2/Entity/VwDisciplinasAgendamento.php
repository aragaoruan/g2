<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_disciplinasagendamento")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwDisciplinasAgendamento {

    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     * @var int $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer")
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer")
     */
    private $id_tipodeavaliacao;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer")
     */
    private $id_avaliacao;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplina;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer")
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $nu_notafinal
     * @Column(name="nu_notafinal", type="integer")
     */
    private $nu_notafinal;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=true, length=255)
     */
    private $st_status;

    /**
     * @var integer $bl_status
     * @Column(name="bl_status", type="integer", nullable=true)
     */
    private $bl_status;
    /**
     * @var integer $bl_agendado
     * @Column(name="bl_agendado", type="integer", nullable=true)
     */
    private $bl_agendado;
    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true)
     */
    private $bl_provaglobal;
    
    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_tipodeavaliacao() {
        return $this->id_tipodeavaliacao;
    }

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getNu_notafinal() {
        return $this->nu_notafinal;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getSt_status() {
        return $this->st_status;
    }

    public function getBl_status() {
        return $this->bl_status;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao) {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    public function setNu_notafinal($nu_notafinal) {
        $this->nu_notafinal = $nu_notafinal;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function setSt_status($st_status) {
        $this->st_status = $st_status;
    }

    public function setBl_status($bl_status) {
        $this->bl_status = $bl_status;
    }

    public function getBl_agendado() {
        return $this->bl_agendado;
    }

    public function setBl_agendado($bl_agendado) {
        $this->bl_agendado = $bl_agendado;
    }
    public function getBl_provaglobal() {
        return $this->bl_provaglobal;
    }

    public function setBl_provaglobal($bl_provaglobal) {
        $this->bl_provaglobal = $bl_provaglobal;
    }




}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_usuarioperfilentidade")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @update Marcus Pereira <marcus.pereira@unyleya.com.br>
 */
class VwUsuarioPerfilEntidade
{

    /**
     * @Column(type="integer")
     * @Id
     */
    private $id_usuario;

    /**
     * @Column(type="string")
     */
    private $st_nomecompleto;

    /**
     * @Column(type="string")
     * @Id
     */
    private $id_perfil;

    /**
     * @Column(type="string")
     */
    private $st_nomeperfil;

    /**
     * @Column(type="string")
     */
    private $st_perfilalias;

    /**
     * @Column(type="date")
     */
    private $dt_inicio;

    /**
     * @Column(type="date")
     */
    private $dt_termino;

    /**
     * @Column(type="integer")
     */
    private $id_situacaoperfil;

    /**
     * @Column(type="string")
     */
    private $st_situacaoperfil;

    /**
     * @Column(type="integer")
     * @Id
     */
    private $id_entidade;

    /**
     * @Column(type="string")
     */
    private $st_nomeentidade;

    /**
     * @Column(type="integer")
     */
    private $id_entidadecadastro;

    /**
     * @Column(type="integer")
     */
    private $id_perfilpedagogico;

    /**
     * @Column(type="integer")
     */
    private $id_sistema;

    /**
     * @Column(type="string")
     */
    private $st_urlimglogo;

    /**
     * @Column(type="string")
     */
    private $st_cpf;

    /**
     * @Column(type="boolean")
     */
    private $bl_ativo;

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getId_perfil() {
        return $this->id_perfil;
    }

    public function getSt_nomeperfil() {
        return $this->st_nomeperfil;
    }

    public function getSt_perfilalias() {
        return $this->st_perfilalias;
    }

    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function getDt_termino() {
        return $this->dt_termino;
    }

    public function getId_situacaoperfil() {
        return $this->id_situacaoperfil;
    }

    public function getSt_situacaoperfil() {
        return $this->st_situacaoperfil;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    public function getId_sistema() {
        return $this->id_sistema;
    }

    public function getId_perfilpedagogico() {
        return $this->id_perfilpedagogico;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function getSt_urlimglogo() {
        return $this->st_nomeentidade;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    public function setSt_nomeperfil($st_nomeperfil) {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    public function setSt_perfilalias($st_perfilalias) {
        $this->st_perfilalias = $st_perfilalias;
    }

    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_termino($dt_termino) {
        $this->dt_termino = $dt_termino;
    }

    public function setId_situacaoperfil($id_situacaoperfil) {
        $this->id_situacaoperfil = $id_situacaoperfil;
    }

    public function setSt_situacaoperfil($st_situacaoperfil) {
        $this->st_situacaoperfil = $st_situacaoperfil;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    public function setId_sistema($id_sistema) {
        $this->id_sistema = $id_sistema;
    }

    public function setId_perfilpedagogico($id_perfilpedagogico) {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    public function setSt_urlimglogo($st_urlimglogo) {
        $this->st_urlimglogo = $st_urlimglogo;
    }

    public function setStcpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }


}
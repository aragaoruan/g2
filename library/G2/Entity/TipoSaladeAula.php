<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tiposaladeaula")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class TipoSaladeAula extends G2Entity
{

    /**
     *
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tiposaladeaula;

    /**
     *
     * @var string $st_tiposaladeaula
     * @Column(name="st_tiposaladeaula", type="string", nullable=false, length=255)
     */
    private $st_tiposaladeaula;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    public function getId_tiposaladeaula ()
    {
        return $this->id_tiposaladeaula;
    }

    public function setId_tiposaladeaula ($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
        return $this;
    }

    public function getSt_tiposaladeaula ()
    {
        return $this->st_tiposaladeaula;
    }

    public function setSt_tiposaladeaula ($st_tiposaladeaula)
    {
        $this->st_tiposaladeaula = $st_tiposaladeaula;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}

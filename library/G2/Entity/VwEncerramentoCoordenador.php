<?php

namespace G2\Entity;

/**
 * @Entity()
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_encerramentocoordenador")
 */
class VwEncerramentoCoordenador {

    /**
     * @Id
     * @Column(name="id_matricula", type="integer")
     */
    private $id_matricula;

    /**
     * @Column(name="id_saladeaula", type="integer")
     */
    private $id_saladeaula;

    /**
     * @Column(name="id_encerramentosala", type="integer")
     */
    private $id_encerramentosala;

    /**
     * @Column(name="id_disciplina", type="integer")
     */
    private $id_disciplina;

    /**
     * @Column(name="id_usuariocoordenador", type="integer")
     */
    private $id_usuariocoordenador;

    /**
     * @Column(name="id_usuariopedagogico", type="integer")
     */
    private $id_usuariopedagogico;

    /**
     * @Column(name="id_usuarioprofessor", type="integer")
     */
    private $id_usuarioprofessor;

    /**
     * @Column(name="st_professor", type="string")
     */
    private $st_professor;

    /**
     * @Column(name="id_usuariofinanceiro", type="integer")
     */
    private $id_usuariofinanceiro;

    /**
     * @Column(name="dt_encerramentoprofessor", type="datetime2")
     */
    private $dt_encerramentoprofessor;

    /**
     * @Column(name="dt_encerramentocoordenador", type="datetime2")
     */
    private $dt_encerramentocoordenador;

    /**
     * @Column(name="dt_encerramentopedagogico", type="datetime2")
     */
    private $dt_encerramentopedagogico;

    /**
     * @Column(name="dt_encerramentofinanceiro", type="datetime2")
     */
    private $dt_encerramentofinanceiro;

    /**
     * @Column(name="st_disciplina", type="string")
     */
    private $st_disciplina;

    /**
     * @Column(name="st_saladeaula", type="string")
     */
    private $st_saladeaula;

    /**
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @Column(name="id_coordenador", type="integer")
     */
    private $id_coordenador;

    /**
     * @Column(name="id_areaconhecimento", type="integer")
     */
    private $id_areaconhecimento;

    /**
     * @Column(name="st_areaconhecimento", type="string")
     */
    private $st_areaconhecimento;

    /**
     * @Column(name="st_aluno", type="string")
     */
    private $st_aluno;

    /**
     * @Column(name="st_tituloavaliacao", type="string")
     */
    private $st_tituloavaliacao;

    /**
     * @Column(name="id_tipodisciplina", type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @Column(name="st_upload", type="string")
     */
    private $st_upload;

    /**
     * @Column(name="id_aluno", type="integer")
     */
    private $id_aluno;

    /**
     * @param mixed $id_aluno
     */
    public function setId_aluno($id_aluno) {
        $this->id_aluno = $id_aluno;
    }

    /**
     * @return mixed
     */
    public function getId_aluno() {
        return $this->id_aluno;
    }

    /**
     * @return string
     */
    public function getSt_aluno() {
        return $this->st_aluno;
    }

    /**
     * @param string $st_aluno
     */
    public function setSt_aluno($st_aluno) {
        $this->st_aluno = $st_aluno;
    }

    /**
     * @return string
     */
    public function getSt_tituloavaliacao() {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param string $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao) {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return int
     */
    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_coordenador() {
        return $this->id_coordenador;
    }

    /**
     * @param int $id_coordenador
     */
    public function setId_coordenador($id_coordenador) {
        $this->id_coordenador = $id_coordenador;
    }

    /**
     * @return int
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_usuarioprofessor() {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param int $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor) {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    /**
     * @return int
     */
    public function getId_usuariocoordenador() {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param int $id_usuariocoordenador
     */
    public function setId_usuariocoordenador($id_usuariocoordenador) {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    /**
     * @return int
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuariopedagogico() {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param int $id_usuariopedagogico
     */
    public function setId_usuariopedagogico($id_usuariopedagogico) {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    /**
     * @return int
     */
    public function getId_usuariofinanceiro() {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param int $id_usuariofinanceiro
     */
    public function setId_usuariofinanceiro($id_usuariofinanceiro) {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor() {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador() {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getDt_encerramentopedagogico() {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getDt_encerramentofinanceiro() {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * @return string
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return field_type $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function getSt_upload() {
        return $this->st_upload;
    }

    /**
     * @param the $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function setSt_upload($st_upload) {
        $this->st_upload = $st_upload;
    }

    public function getSt_professor() {
        return $this->st_professor;
    }

    public function setSt_professor($st_professor) {
        $this->st_professor = $st_professor;
    }

}

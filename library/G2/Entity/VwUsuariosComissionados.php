<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\ComissaoLancamento")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_usuarioscomissionados")
 */
class VwUsuariosComissionados
{

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=false, length=255)
     */
    private $st_funcao;

    /**
     * @var int $id_funcao
     * @Column(name="id_funcao", type="integer")
     */
    private $id_funcao;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_perfilpedagogico
     * @Column(name="st_perfilpedagogico", type="string", nullable=false, length=255)
     */
    private $st_perfilpedagogico;

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function getSt_funcao()
    {
        return $this->st_funcao;
    }

    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function getSt_perfilpedagogico()
    {
        return $this->st_perfilpedagogico;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function setSt_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }


    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function setSt_perfilpedagogico($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
        return $this;
    }

}

<?php

/*
 * Entity DocumentosUtilizacao
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-02
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentosutilizacao")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class DocumentosUtilizacao {

    /**
     *
     * @var integer $id_documentosutilizacao
     * @Column(name="id_documentosutilizacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_documentosutilizacao;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_documentosutilizacao")
     * @var string
     */
    private $st_documentosutilizacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    public function getId_documentosutilizacao() {
        return $this->id_documentosutilizacao;
    }

    public function getSt_documentosutilizacao() {
        return $this->st_documentosutilizacao;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_documentosutilizacao($id_documentosutilizacao) {
        $this->id_documentosutilizacao = $id_documentosutilizacao;
    }

    public function setSt_documentosutilizacao($st_documentosutilizacao) {
        $this->st_documentosutilizacao = $st_documentosutilizacao;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

}

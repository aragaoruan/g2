<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_salaoferta")
 * @Entity
 * @EntityView
 */

class VwSalaOferta
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @var datetime2 $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicioinscricao;
    /**
     * @var datetime2 $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_fiminscricao;
    /**
     * @var datetime2 $dt_abertura
     * @Column(name="dt_abertura", type="datetime2", nullable=false, length=8)
     */
    private $dt_abertura;
    /**
     * @var datetime2 $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2", nullable=false, length=8)
     */
    private $dt_encerramento;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=true)
     */
    private $nu_creditos;
    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=false, length=4)
     */
    private $id_periodoletivo;
    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true, length=4)
     */
    private $nu_ordem;
    /**
     * @var boolean $bl_ofertaexcepcional
     * @Column(name="bl_ofertaexcepcional", type="boolean", nullable=false, length=1)
     */
    private $bl_ofertaexcepcional;
    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_periodoletivo
     * @Column(name="st_periodoletivo", type="string", nullable=false, length=255)
     */
    private $st_periodoletivo;
    /**
     * @var integer $id_tipooferta
     * @Column(name="id_tipooferta", type="integer", nullable=false, length=4)
     */
    private $id_tipooferta;
    /**
     * @var integer $nu_obrigatorioalocacao
     * @Column(name="nu_obrigatorioalocacao", type="integer", nullable=true, length=4)
     */
    private $nu_obrigatorioalocacao;
    /**
     * @var integer $nu_disponivelapartirdo
     * @Column(name="nu_disponivelapartirdo", type="integer", nullable=true, length=4)
     */
    private $nu_disponivelapartirdo;
    /**
     * @var integer $nu_percentualsemestre
     * @Column(name="nu_percentualsemestre", type="integer", nullable=true, length=4)
     */
    private $nu_percentualsemestre;


    /**
     * @return datetime2
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param datetime2 $dt_inicioinscricao
     * @return VwSalaOferta
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param datetime2 $dt_fiminscricao
     * @return VwSalaOferta
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param datetime2 $dt_abertura
     * @return VwSalaOferta
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param datetime2 $dt_encerramento
     * @return VwSalaOferta
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwSalaOferta
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwSalaOferta
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param int $id_modulo
     * @return VwSalaOferta
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwSalaOferta
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param int $nu_maxalunos
     * @return VwSalaOferta
     */
    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @param int $id_trilha
     * @return VwSalaOferta
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwSalaOferta
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return VwSalaOferta
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param int $id_periodoletivo
     * @return VwSalaOferta
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param int $nu_ordem
     * @return VwSalaOferta
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_creditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @param int $nu_creditos
     * @return VwSalaOfertaTurma
     */
    public function setNu_creditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ofertaexcepcional()
    {
        return $this->bl_ofertaexcepcional;
    }

    /**
     * @param boolean $bl_ofertaexcepcional
     * @return VwSalaOferta
     */
    public function setBl_ofertaexcepcional($bl_ofertaexcepcional)
    {
        $this->bl_ofertaexcepcional = $bl_ofertaexcepcional;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @param string $st_modulo
     * @return VwSalaOferta
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return VwSalaOferta
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_periodoletivo()
    {
        return $this->st_periodoletivo;
    }

    /**
     * @param string $st_periodoletivo
     * @return VwSalaOferta
     */
    public function setSt_periodoletivo($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_obrigatorioalocacao()
    {
        return $this->nu_obrigatorioalocacao;
    }

    /**
     * @param int $nu_obrigatorioalocacao
     * @return $this
     */
    public function setNu_obrigatorioalocacao($nu_obrigatorioalocacao)
    {
        $this->nu_obrigatorioalocacao = $nu_obrigatorioalocacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_disponivelapartirdo()
    {
        return $this->nu_disponivelapartirdo;
    }

    /**
     * @param int $nu_disponivelapartirdo
     * @return $this
     */
    public function setNu_disponivelapartirdo($nu_disponivelapartirdo)
    {
        $this->nu_disponivelapartirdo = $nu_disponivelapartirdo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_percentualsemestre()
    {
        return $this->nu_percentualsemestre;
    }

    /**
     * @param int $nu_percentualsemestre
     * @return $this
     */
    public function setNu_percentualsemestre($nu_percentualsemestre)
    {
        $this->nu_percentualsemestre = $nu_percentualsemestre;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipooferta()
    {
        return $this->id_tipooferta;
    }

    /**
     * @param int $id_tipooferta
     * @return $this
     */
    public function setId_tipooferta($id_tipooferta)
    {
        $this->id_tipooferta = $id_tipooferta;
        return $this;
    }
}
<?php

/*
 * Entity DocumentosCategoria
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-09-26
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentoscategoria")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class DocumentosCategoria {

    /**
     *
     * @var integer $id_documentoscategoria
     * @Column(name="id_documentoscategoria", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_documentoscategoria;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_documentoscategoria")
     * @var string
     */
    private $st_documentoscategoria;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    public function getId_documentoscategoria() {
        return $this->id_documentoscategoria;
    }

    public function getSt_documentoscategoria() {
        return $this->st_documentoscategoria;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_documentoscategoria($id_documentoscategoria) {
        $this->id_documentoscategoria = $id_documentoscategoria;
    }

    public function setSt_documentoscategoria($st_documentoscategoria) {
        $this->st_documentoscategoria = $st_documentoscategoria;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }


}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_catraca")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */

use G2\G2Entity;

class Catraca extends G2Entity
{
    /**
     * @Id
     * @var integer $id_catraca
     * @Column(name="id_catraca", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_catraca;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_codigocatraca
     * @Column(name="st_codigocatraca", type="string", nullable=true, length=255)
     */
    private $st_codigocatraca;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_catraca
     */
    public function getId_catraca()
    {
        return $this->id_catraca;
    }

    /**
     * @param id_catraca
     */
    public function setId_catraca($id_catraca)
    {
        $this->id_catraca = $id_catraca;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_codigocatraca
     */
    public function getSt_codigocatraca()
    {
        return $this->st_codigocatraca;
    }

    /**
     * @param st_codigocatraca
     */
    public function setSt_codigocatraca($st_codigocatraca)
    {
        $this->st_codigocatraca = $st_codigocatraca;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_Situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_Situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }



}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipodisciplina")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoDisciplina
{

    /**
     *
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodisciplina;

    /**
     *
     * @var string $st_tipodisciplina
     * @Column(name="st_tipodisciplina", type="string", nullable=false, length=255)
     */
    private $st_tipodisciplina;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    public function getId_tipodisciplina ()
    {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina ($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    public function getSt_tipodisciplina ()
    {
        return $this->st_tipodisciplina;
    }

    public function setSt_tipodisciplina ($st_tipodisciplina)
    {
        $this->st_tipodisciplina = $st_tipodisciplina;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
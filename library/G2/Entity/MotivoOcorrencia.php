<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_motivoocorrencia")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class MotivoOcorrencia {

    /**
     * @Id
     * @var integer $id_motivoocorrencia
     * @Column(name="id_motivoocorrencia", type="integer",  nullable=false, length=4)
     */
    private $id_motivoocorrencia;

    /**
     * @var string $st_motivoocorrencia
     * @Column(name="st_motivoocorrencia", type="string",  nullable=true, length=100)
     */
    private $st_motivoocorrencia;

    public function getId_motivoocorrencia()
    {
        return $this->id_motivoocorrencia;
    }

    public function getSt_motivoocorrencia()
    {
        return $this->st_motivoocorrencia;
    }

    public function setId_motivoocorrencia($id_motivoocorrencia)
    {
        $this->id_motivoocorrencia = $id_motivoocorrencia;
        return $this;
    }

    public function setSt_motivoocorrencia($st_motivoocorrencia)
    {
        $this->st_motivoocorrencia = $st_motivoocorrencia;
        return $this;
    }
}

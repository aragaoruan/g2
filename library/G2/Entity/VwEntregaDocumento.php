<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entregadocumento")
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwEntregaDocumento")
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwEntregaDocumento {

    const SITUACAO_PENDENTE = 57;
    const SITUACAO_SOB_ANALISE = 58;
    const SITUACAO_ENTREGUE_CONFIRMADO = 59;

    /**
     * @var integer $id_usuarioaluno
     * @Column(name="id_usuarioaluno", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuarioaluno;

    /**
     * @var integer $id_documentos
     * @Column(name="id_documentos", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_documentos;

    /**
     * @var string $st_documentos
     * @Column(name="st_documentos", type="string", nullable=true, length=510)
     */
    private $st_documentos;

    /**
     * @var integer $id_tipodocumento
     * @Column(name="id_tipodocumento", type="integer", nullable=false, length=4)
     */
    private $id_tipodocumento;

    /**
     * @var integer $id_documentosutilizacao
     * @Column(name="id_documentosutilizacao", type="integer", nullable=false, length=4)
     */
    private $id_documentosutilizacao;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var integer $id_contratoresponsavel
     * @Column(name="id_contratoresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_contratoresponsavel;

    /**
     * @var integer $id_entregadocumentos
     * @Column(name="id_entregadocumentos", type="integer", nullable=true, length=4)
     */
    private $id_entregadocumentos;


    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;


    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;


    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var integer $nu_ordenacao
     * @Column(name="nu_ordenacao", type="integer", nullable=true)
     */
    private $nu_ordenacao;

    /**
     * @param int $id_contratoresponsavel
     */
    public function setId_contratoresponsavel($id_contratoresponsavel)
    {
        $this->id_contratoresponsavel = $id_contratoresponsavel;
    }

    /**
     * @return int
     */
    public function getId_contratoresponsavel()
    {
        return $this->id_contratoresponsavel;
    }

    /**
     * @param int $id_documentos
     */
    public function setId_documentos($id_documentos)
    {
        $this->id_documentos = $id_documentos;
    }

    /**
     * @return int
     */
    public function getId_documentos()
    {
        return $this->id_documentos;
    }

    /**
     * @param int $id_documentosutilizacao
     */
    public function setId_documentosutilizacao($id_documentosutilizacao)
    {
        $this->id_documentosutilizacao = $id_documentosutilizacao;
    }

    /**
     * @return int
     */
    public function getId_documentosutilizacao()
    {
        return $this->id_documentosutilizacao;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entregadocumentos
     */
    public function setId_entregadocumentos($id_entregadocumentos)
    {
        $this->id_entregadocumentos = $id_entregadocumentos;
    }

    /**
     * @return int
     */
    public function getId_entregadocumentos()
    {
        return $this->id_entregadocumentos;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_tipodocumento
     */
    public function setId_tipodocumento($id_tipodocumento)
    {
        $this->id_tipodocumento = $id_tipodocumento;
    }

    /**
     * @return int
     */
    public function getId_tipodocumento()
    {
        return $this->id_tipodocumento;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuarioaluno
     */
    public function setId_usuarioaluno($id_usuarioaluno)
    {
        $this->id_usuarioaluno = $id_usuarioaluno;
    }

    /**
     * @return int
     */
    public function getId_usuarioaluno()
    {
        return $this->id_usuarioaluno;
    }

    /**
     * @param string $st_documentos
     */
    public function setSt_documentos($st_documentos)
    {
        $this->st_documentos = $st_documentos;
    }

    /**
     * @return string
     */
    public function getSt_documentos()
    {
        return $this->st_documentos;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param int $nu_ordenacao
     */
    public function setNu_ordenacao($nu_ordenacao)
    {
        $this->nu_ordenacao = $nu_ordenacao;
    }

    /**
     * @return int
     */
    public function getNu_ordenacao()
    {
        return $this->nu_ordenacao;
    }
}

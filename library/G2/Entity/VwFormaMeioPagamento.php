<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_formameiopagamento")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwFormaMeioPagamento
{

    /**
     *
     * @var integer $id_formapagamentoaplicacao
     * @Column(name="id_formapagamentoaplicacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamentoaplicacao;

    /**
     * @Id
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=false)
     */
    private $id_meiopagamento;

    /**
     *
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=false, length=255)
     */
    private $st_meiopagamento;

    /**
     *
     * @var string $st_descricaomeiopagamento
     * @Column(name="st_descricaomeiopagamento", type="string", nullable=false, length=2500)
     */
    private $st_descricaomeiopagamento;

    /**
     * @Id
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false)
     */
    private $id_formapagamento;

    /**
     *
     * @var string $st_formapagamento
     * @Column(name="st_formapagamento", type="string", nullable=false,length=255)
     */
    private $st_formapagamento;

    /**
     *
     * @var string $id_situacao
     * @Column(name="id_situacao", type="string", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false,length=2500)
     */
    private $st_descricao;

    /**
     *
     * @var decimal $nu_entradavalormin
     * @Column(name="nu_entradavalormin", type="decimal", nullable=true)
     */
    private $nu_entradavalormin;

    /**
     *
     * @var decimal $nu_entradavalormax
     * @Column(name="nu_entradavalormax", type="decimal", nullable=true)
     */
    private $nu_entradavalormax;

    /**
     *
     * @var decimal $nu_valormin
     * @Column(name="nu_valormin", type="decimal", nullable=true)
     */
    private $nu_valormin;

    /**
     *
     * @var decimal $nu_valormax
     * @Column(name="nu_valormax", type="decimal", nullable=true)
     */
    private $nu_valormax;

    /**
     *
     * @var decimal $nu_valorminparcela
     * @Column(name="nu_valorminparcela", type="decimal", nullable=true)
     */
    private $nu_valorminparcela;

    /**
     *
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_tipoformapagamentoparcela
     * @Column(name="id_tipoformapagamentoparcela", type="integer", nullable=true)
     */
    private $id_tipoformapagamentoparcela;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     *
     * @var boolean $bl_todosprodutos
     * @Column(name="bl_todosprodutos", type="boolean", nullable=false)
     */
    private $bl_todosprodutos;

    /**
     *
     * @var decimal $nu_multa
     * @Column(name="nu_multa", type="decimal", nullable=true)
     */
    private $nu_multa;

    /**
     *
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", nullable=true)
     */
    private $nu_juros;

    /**
     *
     * @var integer $nu_maxparcelas
     * @Column(name="nu_maxparcelas", type="integer", nullable=false)
     */
    private $nu_maxparcelas;

    /**
     *
     * @var decimal $nu_jurosmax
     * @Column(name="nu_jurosmax", type="decimal", nullable=true)
     */
    private $nu_jurosmax;

    /**
     *
     * @var decimal $nu_jurosmin
     * @Column(name="nu_jurosmin", type="decimal", nullable=true)
     */
    private $nu_jurosmin;

    public function getId_formapagamentoaplicacao ()
    {
        return $this->id_formapagamentoaplicacao;
    }

    public function setId_formapagamentoaplicacao ($id_formapagamentoaplicacao)
    {
        $this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
        return $this;
    }

    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    public function setId_meiopagamento ($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    public function getSt_meiopagamento ()
    {
        return $this->st_meiopagamento;
    }

    public function setSt_meiopagamento ($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    public function getSt_descricaomeiopagamento ()
    {
        return $this->st_descricaomeiopagamento;
    }

    public function setSt_descricaomeiopagamento ($st_descricaomeiopagamento)
    {
        $this->st_descricaomeiopagamento = $st_descricaomeiopagamento;
        return $this;
    }

    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    public function setId_formapagamento ($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    public function getSt_formapagamento ()
    {
        return $this->st_formapagamento;
    }

    public function setSt_formapagamento ($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getNu_entradavalormin ()
    {
        return $this->nu_entradavalormin;
    }

    public function setNu_entradavalormin ($nu_entradavalormin)
    {
        $this->nu_entradavalormin = $nu_entradavalormin;
        return $this;
    }

    public function getNu_entradavalormax ()
    {
        return $this->nu_entradavalormax;
    }

    public function setNu_entradavalormax ($nu_entradavalormax)
    {
        $this->nu_entradavalormax = $nu_entradavalormax;
        return $this;
    }

    public function getNu_valormin ()
    {
        return $this->nu_valormin;
    }

    public function setNu_valormin ($nu_valormin)
    {
        $this->nu_valormin = $nu_valormin;
        return $this;
    }

    public function getNu_valormax ()
    {
        return $this->nu_valormax;
    }

    public function setNu_valormax ($nu_valormax)
    {
        $this->nu_valormax = $nu_valormax;
        return $this;
    }

    public function getNu_valorminparcela ()
    {
        return $this->nu_valorminparcela;
    }

    public function setNu_valorminparcela ($nu_valorminparcela)
    {
        $this->nu_valorminparcela = $nu_valorminparcela;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_tipoformapagamentoparcela ()
    {
        return $this->id_tipoformapagamentoparcela;
    }

    public function setId_tipoformapagamentoparcela ($id_tipoformapagamentoparcela)
    {
        $this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_todosprodutos ()
    {
        return $this->bl_todosprodutos;
    }

    public function setBl_todosprodutos ($bl_todosprodutos)
    {
        $this->bl_todosprodutos = $bl_todosprodutos;
        return $this;
    }

    public function getNu_multa ()
    {
        return $this->nu_multa;
    }

    public function setNu_multa ($nu_multa)
    {
        $this->nu_multa = $nu_multa;
        return $this;
    }

    public function getNu_juros ()
    {
        return $this->nu_juros;
    }

    public function setNu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    public function getNu_maxparcelas ()
    {
        return $this->nu_maxparcelas;
    }

    public function setNu_maxparcelas ($nu_maxparcelas)
    {
        $this->nu_maxparcelas = $nu_maxparcelas;
        return $this;
    }

    public function getNu_jurosmax ()
    {
        return $this->nu_jurosmax;
    }

    public function setNu_jurosmax ($nu_jurosmax)
    {
        $this->nu_jurosmax = $nu_jurosmax;
        return $this;
    }

    public function getNu_jurosmin ()
    {
        return $this->nu_jurosmin;
    }

    public function setNu_jurosmin ($nu_jurosmin)
    {
        $this->nu_jurosmin = $nu_jurosmin;
        return $this;
    }

}
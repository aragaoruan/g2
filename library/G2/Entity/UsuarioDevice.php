<?php

namespace G2\Entity;

/**
 * @SWG\Definition(required={"st_key", "id_usuario", "st_infodevice"}, @SWG\Xml(name="UsuarioDevice"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuariodevice")
 * @Entity
 *
 *
 */
class UsuarioDevice
{
    /**
     * @SWG\Property(format="integer")
     * @var integer - Chave da tabela tb_usuariodevice
     * @Id
     * @Column(name="id_usuariodevice", type="integer", length=40, nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_usuariodevice;

    /**
     * @SWG\Property()
     * @Column(name="st_key", type="string", length=250, nullable=false)
     * @var string - Chave
     */
    private $st_key;

    /**
     * @SWG\Property(format="integer")
     * @var integer - Id do Usuário
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @SWG\Property()
     * @Column(name="st_infodevice", type="string", length=250, nullable=false)
     * @var string - Informações do dispositivo
     */
    private $st_infodevice;

    /**
     * @Column(name="dt_cadastro", type="datetime", length=24, nullable=true)
     * @var datetime
     */
    private $dt_cadastro;

    /**
     * @return mixed
     */
    public function getid_usuariodevice()
    {
        return $this->id_usuariodevice;
    }

    /**
     * @param mixed $id_usuariodevice
     */
    public function setid_usuariodevice($id_usuariodevice)
    {
        $this->id_usuariodevice = $id_usuariodevice;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_key()
    {
        return $this->st_key;
    }

    /**
     * @param string $st_key
     */
    public function setst_key($st_key)
    {
        $this->st_key = $st_key;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_infodevice()
    {
        return $this->st_infodevice;
    }

    /**
     * @param string $st_infodevice
     */
    public function setst_infodevice($st_infodevice)
    {
        $this->st_infodevice = $st_infodevice;
        return $this;

    }

    /**
     * @return datetime
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }



}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Class UsuarioIntegracao
 * @package G2\Entity
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuariointegracao")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-06-13
 * @EntityLog
 * TODO Mapear os relacionamentos e rastrear no código a modificação
 */
class UsuarioIntegracao extends G2Entity
{

    /**
     * @var integer $id_usuariointegracao
     * @Column(name="id_usuariointegracao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_usuariointegracao;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=false)
     */
    private $st_codusuario;

    /**
     * @var string $st_senhaintegrada
     * @Column(name="st_senhaintegrada", type="string", nullable=true)
     */
    private $st_senhaintegrada;

    /**
     * @var string $st_loginintegrado
     * @Column(name="st_loginintegrado", type="string", nullable=true)
     */
    private $st_loginintegrado;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=false)
     */
    private $bl_encerrado;


    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true)
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getId_usuariointegracao()
    {
        return $this->id_usuariointegracao;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @return string
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @return string
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @param integer $id_usuariointegracao
     * @return $this
     */
    public function setId_usuariointegracao($id_usuariointegracao)
    {
        $this->id_usuariointegracao = $id_usuariointegracao;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param $id_sistema
     * @return $this
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param $st_codusuario
     * @return $this
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @param $st_senhaintegrada
     * @return $this
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
        return $this;
    }

    /**
     * @param $st_loginintegrado
     * @return $this
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param $bl_encerrado
     * @return $this
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param $id_entidadeintegracao
     * @return $this
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtoprojeto")
 * @Entity
 * @EntityView
 * @author Felipe Pastor
 */
class VwProdutoProjeto
{
    /**
     *
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_projetopedagogico;
    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     */
    private $id_produto;
    /**
     *
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $nu_cargahoraria;
    /**
     *
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=true)
     */
    private $dt_inicio;
    /**
     *
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=true, length=250)
     */
    private $st_turno;
    /**
     *
     * @var string $st_diassemana
     * @Column(name="st_diassemana", type="string", nullable=true, length=250)
     */
    private $st_diassemana;
    /**
     *
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="text", nullable=true)
     */
    private $st_disciplina;

    /**
     * @param \G2\Entity\datetime2 $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param int $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param string $st_diassemana
     */
    public function setSt_diassemana($st_diassemana)
    {
        $this->st_diassemana = $st_diassemana;
    }

    /**
     * @return string
     */
    public function getSt_diassemana()
    {
        return $this->st_diassemana;
    }

    /**
     * @param string $st_turno
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
    }

    /**
     * @return string
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }




}
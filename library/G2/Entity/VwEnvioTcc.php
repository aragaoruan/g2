<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwEnvioTcc"))
 * @Entity()
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_enviotcc")
 *
 *
 */
class VwEnvioTcc extends G2Entity {

    /**
     * @Id
     * @SWG\Property(property="id_matricula",type="integer")
     * @Column(name="id_matricula", type="integer")
     */
    private $id_matricula;

    /**
     *
     * @SWG\Property(property="id_saladeaula",type="integer")
     * @Column(name="id_saladeaula", type="integer")
     */
    private $id_saladeaula;

    /**
     *
     * @SWG\Property(property="id_disciplina",type="integer")
     * @Column(name="id_disciplina", type="integer")
     */
    private $id_disciplina;

    /**
     *
     * @SWG\Property(property="dt_encerramentoprofessor",type="date")
     * @Column(name="dt_encerramentoprofessor", type="datetime2")
     */
    private $dt_encerramentoprofessor;

    /**
     *
     * @SWG\Property(property="id_entidade",type="integer")
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     *
     * @SWG\Property(property="id_usuario",type="integer")
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     *
     * @SWG\Property(property="st_nomecompleto",type="string")
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     *
     * @SWG\Property(property="st_justificativa",type="string")
     * @Column(name="st_justificativa", type="string")
     */
    private $st_justificativa;

    /**
     * @SWG\Property(property="st_cpf",type="string")
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @SWG\Property(property="st_nomeexibicao",type="string")
     * @Column(name="st_nomeexibicao", type="string")
     */
    private $st_nomeexibicao;

    /**
     * @SWG\Property(property="id_entidadesala",type="string")
     * @Column(name="id_entidadesala", type="string")
     */
    private $id_entidadesala;

    /**
     *
     * @SWG\Property(property="id_projetopedagogico",type="integer")
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @SWG\Property(property="st_tituloexibicaoprojeto",type="string")
     * @Column(name="st_tituloexibicaoprojeto", type="string")
     */
    private $st_tituloexibicaoprojeto;

    /**
     *
     * @SWG\Property(property="id_modulo",type="integer")
     * @Column(name="id_modulo", type="integer")
     */
    private $id_modulo;

    /**
     * @SWG\Property(property="st_tituloexibicaomodulo",type="string")
     * @Column(name="st_tituloexibicaomodulo", type="string")
     */
    private $st_tituloexibicaomodulo;

    /**
     * @SWG\Property(property="st_disciplina",type="string")
     * @Column(name="st_disciplina", type="string")
     */
    private $st_disciplina;

    /**
     * @SWG\Property(property="st_saladeaula",type="string")
     * @Column(name="st_saladeaula", type="string")
     */
    private $st_saladeaula;

    /**
     * @SWG\Property(property="st_coordenador",type="string")
     * @Column(name="st_coordenador", type="string")
     */
    private $st_coordenador;

    /**
     *
     * @SWG\Property(property="id_professor",type="integer")
     * @Column(name="id_professor", type="integer")
     */
    private $id_professor;

    /**
     * @SWG\Property(property="st_professor",type="string")
     * @Column(name="st_professor", type="string")
     */
    private $st_professor;

    /**
     *
     * @SWG\Property(property="id_situacaotcc",type="integer")
     * @Column(name="id_situacaotcc", type="integer")
     */
    private $id_situacaotcc;

    /**
     *
     * @SWG\Property(property="id_avaliacaoconjuntoreferencia",type="integer")
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer")
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     *
     * @SWG\Property(property="id_avaliacao",type="integer")
     * @Column(name="id_avaliacao", type="integer")
     */
    private $id_avaliacao;

    /**
     *
     * @SWG\Property(property="id_alocacao",type="integer")
     * @Column(name="id_alocacao", type="integer")
     */
    private $id_alocacao;

    /**
     * @return mixed
     */
    public function getst_justificativa()
    {
        return $this->st_justificativa;
    }

    /**
     * @param mixed $st_justificativa
     */
    public function setst_justificativa($st_justificativa)
    {
        $this->st_justificativa = $st_justificativa;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param mixed $id_alocacao
     */
    public function setid_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param mixed $id_avaliacaoconjuntoreferencia
     */
    public function setid_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @param mixed $id_avaliacao
     */
    public function setid_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_coordenador()
    {
        return $this->st_coordenador;
    }

    /**
     * @param mixed $st_coordenador
     */
    public function setst_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
        return $this;

    }


    /**
     * @return mixed
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param mixed $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param mixed $id_saladeaula
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param mixed $id_disciplina
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_encerramentoprofessor()
    {
        return $this->dt_encerramentoprofessor;
    }

    /**
     * @param mixed $dt_encerramentoprofessor
     */
    public function setdt_encerramentoprofessor($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param mixed $st_nomecompleto
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param mixed $st_cpf
     */
    public function setst_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @param mixed $st_nomeexibicao
     */
    public function setst_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_entidadesala()
    {
        return $this->id_entidadesala;
    }

    /**
     * @param mixed $id_entidadesala
     */
    public function setid_entidadesala($id_entidadesala)
    {
        $this->id_entidadesala = $id_entidadesala;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param mixed $id_projetopedagogico
     */
    public function setid_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_tituloexibicaoprojeto()
    {
        return $this->st_tituloexibicaoprojeto;
    }

    /**
     * @param mixed $st_tituloexibicaoprojeto
     */
    public function setst_tituloexibicaoprojeto($st_tituloexibicaoprojeto)
    {
        $this->st_tituloexibicaoprojeto = $st_tituloexibicaoprojeto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param mixed $id_modulo
     */
    public function setid_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @param mixed $st_tituloexibicaomodulo
     */
    public function setst_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param mixed $st_disciplina
     */
    public function setst_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param mixed $st_saladeaula
     */
    public function setst_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_professor()
    {
        return $this->id_professor;
    }

    /**
     * @param mixed $id_professor
     */
    public function setid_professor($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param mixed $st_professor
     */
    public function setst_professor($st_professor)
    {
        $this->st_professor = $st_professor;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @param mixed $id_situacaotcc
     */
    public function setid_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
        return $this;

    }





}

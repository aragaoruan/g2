<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmaturno")
 * @Entity(repositoryClass="\G2\Repository\Turma")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TurmaTurno
{

    /**
     * @Id
     * @var integer $id_turmaturno
     * @Column(name="id_turmaturno", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turmaturno;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;

    /**
     * @var Turno $id_turno
     * @ManyToOne(targetEntity="Turno")
     * @JoinColumn(name="id_turno", referencedColumnName="id_turno", nullable=true)
     */
    private $id_turno;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_turmaturno
     */
    public function getId_turmaturno()
    {
        return $this->id_turmaturno;
    }

    /**
     * @param id_turmaturno
     */
    public function setId_turmaturno($id_turmaturno)
    {
        $this->id_turmaturno = $id_turmaturno;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer id_turno
     */
    public function getId_turno()
    {
        return $this->id_turno;
    }

    /**
     * @param id_turno
     */
    public function setId_turno($id_turno)
    {
        $this->id_turno = $id_turno;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

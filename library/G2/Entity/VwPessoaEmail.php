<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pessoaemail")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPessoaEmail
{

    /**
     * @var integer $id_entidade
     * @column(name="id_entidade", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var integer $id_usuario
     * @Id
     * @column(name="id_usuario", nullable=false, type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $id_perfil
     * @column(name="id_perfil", nullable=true, type="integer")
     */
    private $id_perfil;

    /**
     * @var integer $id_email
     * @Id
     * @column(name="id_email", nullable=false, type="integer")
     */
    private $id_email;

    /**
     * @var string $st_email
     * @column(name="st_email", nullable=false, type="string", length=255)
     */
    private $st_email;

    /**
     * @var boolean $bl_padrao
     * @column(name="bl_padrao", nullable=false, type="boolean")
     */
    private $bl_padrao;

    /**
     * @var boolean $bl_ativo
     * @column(name="bl_ativo", nullable=false, type="boolean")
     */
    private $bl_ativo;

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    public function getId_email ()
    {
        return $this->id_email;
    }

    public function setId_email ($id_email)
    {
        $this->id_email = $id_email;
        return $this;
    }

    public function getSt_email ()
    {
        return $this->st_email;
    }

    public function setSt_email ($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * Return array of object
     * @return array
     */
    public function _toArray ()
    {
        return array(
            'id_entidade' => $this->id_entidade,
            'id_usuario' => $this->id_usuario,
            'id_perfil' => $this->id_perfil,
            'id_email' => $this->id_email,
            'st_email' => $this->st_email,
            'bl_padrao' => $this->bl_padrao,
            'bl_ativo' => $this->bl_ativo,
        );
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alterarmatriculadecursodeprazo")
 * @Entity
 * @EntityView
 * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
 */
class VwAlterarMatriculaDecursoDePrazo
{
    /** * @var integer $id_matricula
     * @Column(name="id_matricula",
     * type="integer", nullable=false, length=4)
     * @id
     */
    private $id_matricula;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_entidadematriz
     * @Column(name="id_entidadematriz", type="integer", nullable=true, length=4)
     */
    private $id_entidadematriz;
    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true, length=4)
     */
    private $id_entidadematricula;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @param id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
        return $this;
    }

    /**
     * @return integer id_entidadematricula
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }
}


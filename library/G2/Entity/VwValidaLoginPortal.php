<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_validaloginportal")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwValidaLoginPortal
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;
    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false, length=40)
     */
    private $st_login;
    /**
     * @var string $st_senhaentidade
     * @Column(name="st_senhaentidade", type="string", nullable=true, length=50)
     */
    private $st_senhaentidade;
    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=32)
     */
    private $st_senha;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", length=255)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_urlavatar
     * @Column(name="st_urlavatar", type="string", length=255)
     */
    private $st_urlavatar;

    /**
     * @var boolean $bl_acessoapp
     * @Column(name="bl_acessoapp", type="boolean")
     */
    private $bl_acessoapp;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string",length=255)
     */
    private $st_cpf;

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string st_email
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param st_email
     * @return $this
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return string st_login
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @param st_login
     * @return $this
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    /**
     * @return string st_senhaentidade
     */
    public function getSt_senhaentidade()
    {
        return $this->st_senhaentidade;
    }

    /**
     * @param st_senhaentidade
     * @return $this
     */
    public function setSt_senhaentidade($st_senhaentidade)
    {
        $this->st_senhaentidade = $st_senhaentidade;
        return $this;
    }

    /**
     * @return string st_senha
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param st_senha
     * @return $this
     */
    public function setSt_senha($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param string $st_urlavatar
     * @return $this
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_acessoapp()
    {
        return $this->bl_acessoapp;
    }

    /**
     * @param boolean $bl_acessoapp
     * @return $this
     */
    public function setBl_acessoapp($bl_acessoapp)
    {
        $this->bl_acessoapp = $bl_acessoapp;
        return $this;
    }


    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

}

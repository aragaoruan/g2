<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Class AtividadeComplementar
 * @package G2\Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_atividadecomplementar")
 * @Entity(repositoryClass="\G2\Repository\AtividadeComplementar")
 * @EntityLog
 */
class AtividadeComplementar extends G2Entity
{

    /**
     * @var integer $id_atividadecomplementar
     * @Column(name="id_atividadecomplementar", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_atividadecomplementar;


    /**
     * @var TipoAtividadeComplementar $id_tipoatividade
     * @ManyToOne(targetEntity="TipoAtividadeComplementar")
     * @JoinColumn(name="id_tipoatividade", referencedColumnName="id_tipoatividadecomplementar" , nullable=false)
     */
    private $id_tipoatividade;

    /**
     * @var Upload $id_upload
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", referencedColumnName="id_upload" , nullable=false)
     */
    private $id_upload;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao" , nullable=false)
     */
    private $id_situacao;


    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula" , nullable=false)
     */
    private $id_matricula;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade" , nullable=false)
     */
    private $id_entidadecadastro;

    /**
     * @var Usuario $id_usuarioanalise
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioanalise", referencedColumnName="id_usuario" , nullable=true)
     */
    private $id_usuarioanalise;

    /**
     * @var float $nu_horasconvalidada
     * @Column(name="nu_horasconvalidada", type="decimal", nullable=true, length=8)
     */
    private $nu_horasconvalidada;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var mixed $dt_analisado
     * @Column(name="dt_analisado", type="datetime2", nullable=true, length=8)
     */
    private $dt_analisado;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_tituloatividade
     * @Column(name="st_tituloatividade", type="string", nullable=false, length=100)
     */
    private $st_tituloatividade;

    /**
     * @var string $st_resumoatividade
     * @Column(name="st_resumoatividade", type="string", nullable=true, length=300)
     */
    private $st_resumoatividade;

    /**
     * @var string $st_caminhoarquivo
     * @Column(name="st_caminhoarquivo", type="string", nullable=false, length=200)
     */
    private $st_caminhoarquivo;

    /**
     * @var string $st_observacaoavaliador
     * @Column(name="st_observacaoavaliador", type="string", nullable=true, length=300)
     */
    private $st_observacaoavaliador;

    /**
     * @return string
     */
    public function getSt_observacaoavaliador()
    {
        return $this->st_observacaoavaliador;
    }

    /**
     * @param string $st_observacaoavaliador
     * @return AtividadeComplementar
     */
    public function setSt_observacaoavaliador($st_observacaoavaliador)
    {
        $this->st_observacaoavaliador = $st_observacaoavaliador;
        return $this;
    }


    /**
     * @return int
     */
    public function getId_atividadecomplementar()
    {
        return $this->id_atividadecomplementar;
    }

    /**
     * @param int $id_atividadecomplementar
     * @return AtividadeComplementar
     */
    public function setId_atividadecomplementar($id_atividadecomplementar)
    {
        $this->id_atividadecomplementar = $id_atividadecomplementar;
        return $this;
    }

    /**
     * @return TipoAtividadeComplementar
     */
    public function getId_tipoatividade()
    {
        return $this->id_tipoatividade;
    }

    /**
     * @param TipoAtividadeComplementar $id_tipoatividade
     * @return AtividadeComplementar
     */
    public function setId_tipoatividade($id_tipoatividade)
    {
        $this->id_tipoatividade = $id_tipoatividade;
        return $this;
    }

    /**
     * @return Upload
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param Upload $id_upload
     * @return AtividadeComplementar
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     * @return AtividadeComplementar
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     * @return AtividadeComplementar
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return AtividadeComplementar
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioanalise()
    {
        return $this->id_usuarioanalise;
    }

    /**
     * @param Usuario $id_usuarioanalise
     * @return AtividadeComplementar
     */
    public function setId_usuarioanalise($id_usuarioanalise)
    {
        $this->id_usuarioanalise = $id_usuarioanalise;
        return $this;
    }

    /**
     * @return flaot
     */
    public function getNu_horasconvalidada()
    {
        return $this->nu_horasconvalidada;
    }

    /**
     * @param flaot $nu_horasconvalidada
     * @return AtividadeComplementar
     */
    public function setNu_horasconvalidada($nu_horasconvalidada)
    {
        $this->nu_horasconvalidada = $nu_horasconvalidada;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return AtividadeComplementar
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_analisado()
    {
        return $this->dt_analisado;
    }

    /**
     * @param mixed $dt_analisado
     * @return AtividadeComplementar
     */
    public function setDt_analisado($dt_analisado)
    {
        $this->dt_analisado = $dt_analisado;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return AtividadeComplementar
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloatividade()
    {
        return $this->st_tituloatividade;
    }

    /**
     * @param string $st_tituloatividade
     * @return AtividadeComplementar
     */
    public function setSt_tituloatividade($st_tituloatividade)
    {
        $this->st_tituloatividade = $st_tituloatividade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_resumoatividade()
    {
        return $this->st_resumoatividade;
    }

    /**
     * @param string $st_resumoatividade
     * @return AtividadeComplementar
     */
    public function setSt_resumoatividade($st_resumoatividade)
    {
        $this->st_resumoatividade = $st_resumoatividade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_caminhoarquivo()
    {
        return $this->st_caminhoarquivo;
    }

    /**
     * @param string $st_caminhoarquivo
     * @return AtividadeComplementar
     */
    public function setSt_caminhoarquivo($st_caminhoarquivo)
    {
        $this->st_caminhoarquivo = $st_caminhoarquivo;
        return $this;
    }


}

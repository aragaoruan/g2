<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_parametrotcc")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class ParametroTcc {

    /**
     * @Id
     * @var integer $id_parametrotcc
     * @Column(name="id_parametrotcc", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_parametrotcc;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_parametrotcc
     * @Column(name="st_parametrotcc", type="string",  nullable=false, length=200)
     */
    private $st_parametrotcc;

    /**
     * @var integer $nu_peso
     * @Column(name="nu_peso", type="integer", nullable=true)
     */
    private $nu_peso;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;


    public function getId (){
        return $this->id_parametrotcc;
    }

    public function setId ($id){
        $this->id_parametrotcc = $id;
        return $this;
    }
    public function getId_parametrotcc() {
        return $this->id_parametrotcc;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getSt_parametrotcc() {
        return $this->st_parametrotcc;
    }

    public function getNu_peso() {
        return $this->nu_peso;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_parametrotcc($id_parametrotcc) {
        $this->id_parametrotcc = $id_parametrotcc;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setSt_parametrotcc($st_parametrotcc) {
        $this->st_parametrotcc = $st_parametrotcc;
    }

    public function setNu_peso($nu_peso) {
        $this->nu_peso = $nu_peso;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }


}

<?php

namespace G2\Entity;


use G2\G2Entity;

/**
 * Class TipoAtividadeComplementar
 * @package G2\Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoatividadecomplementar")
 * @Entity
 * @EntityLog
 */
class TipoAtividadeComplementar extends G2Entity
{

    /**
     * @var integer $id_tipoatividadecomplementar
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_tipoatividadecomplementar", type="integer", nullable=false)
     */
    private $id_tipoatividadecomplementar;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;


    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario" , nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade" , nullable=false)
     */
    private $id_entidadecadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var string $st_tipoatividadecomplementar
     * @Column(name="st_tipoatividadecomplementar", type="string", nullable=false, length=50)
     */
    private $st_tipoatividadecomplementar;

    /**
     * @return int
     */
    public function getId_tipoatividadecomplementar()
    {
        return $this->id_tipoatividadecomplementar;
    }

    /**
     * @param int $id_tipoatividadecomplementar
     * @return TipoAtividadeComplementar
     */
    public function setId_tipoatividadecomplementar($id_tipoatividadecomplementar)
    {
        $this->id_tipoatividadecomplementar = $id_tipoatividadecomplementar;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return TipoAtividadeComplementar
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return TipoAtividadeComplementar
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return TipoAtividadeComplementar
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return TipoAtividadeComplementar
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tipoatividadecomplementar()
    {
        return $this->st_tipoatividadecomplementar;
    }

    /**
     * @param string $st_tipoatividadecomplementar
     * @return TipoAtividadeComplementar
     */
    public function setSt_tipoatividadecomplementar($st_tipoatividadecomplementar)
    {
        $this->st_tipoatividadecomplementar = $st_tipoatividadecomplementar;
        return $this;
    }


}

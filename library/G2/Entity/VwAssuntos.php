<?php

namespace G2\Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwAssuntos"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_assuntos")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAssuntos
{

    /**
     * @SWG\Property()
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_assuntoco;

    /**
     * @SWG\Property()
     *
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_abertura
     * @Column(name="bl_abertura", type="boolean", nullable=false)
     */
    private $bl_abertura;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    /**
     * @SWG\Property()
     *
     * @var integer $id_assuntocopai
     * @Column(name="id_assuntocopai", type="integer", nullable=true)
     */
    private $id_assuntocopai;


    /**
     * @SWG\Property()
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;


    /**
     * @SWG\Property()
     *
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=false)
     */
    private $id_tipoocorrencia;


    /**
     * @SWG\Property()
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;


    /**
     * @SWG\Property()
     *
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=true)
     */
    private $id_textosistema;

    /**
     * @SWG\Property()
     * @var boolean $bl_autodistribuicao
     * @Column(name="bl_autodistribuicao", type="boolean", nullable=false)
     */
    private $bl_autodistribuicao;

    /**
     * @SWG\Property()
     * @var boolean $bl_notificaatendente
     * @Column(name="bl_notificaatendente", type="boolean", nullable=false)
     */
    private $bl_notificaatendente;

    /**
     * @SWG\Property()
     * @var boolean $bl_notificaresponsavel
     * @Column(name="bl_notificaresponsavel", type="boolean", nullable=false)
     */
    private $bl_notificaresponsavel;


    /**
     * @SWG\Property()
     * @var boolean $bl_interno
     * @Column(name="bl_interno", type="boolean", nullable=false)
     */
    private $bl_interno;


    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
        return $this;
    }

    public function getBl_abertura()
    {
        return $this->bl_abertura;
    }

    public function setBl_abertura($bl_abertura)
    {
        $this->bl_abertura = $bl_abertura;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


    public function getId_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    public function setId_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
        return $this;
    }


    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @param boolean $bl_autodistribuicao
     */
    public function setBl_autodistribuicao($bl_autodistribuicao)
    {
        $this->bl_autodistribuicao = $bl_autodistribuicao;
    }

    /**
     * @return boolean
     */
    public function getBl_autodistribuicao()
    {
        return $this->bl_autodistribuicao;
    }

    /**
     * @param boolean $bl_notificaatendente
     */
    public function setBl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
    }

    /**
     * @return boolean
     */
    public function getBl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param boolean $bl_notificaresponsavel
     */
    public function setBl_notificaresponsavel($bl_notificaresponsavel)
    {
        $this->bl_notificaresponsavel = $bl_notificaresponsavel;
    }

    /**
     * @return boolean
     */
    public function getBl_notificaresponsavel()
    {
        return $this->bl_notificaresponsavel;
    }

    /**
     * @return mixed
     */
    public function getBl_interno()
    {
        return $this->bl_interno;
    }

    /**
     * @param mixed $bl_interno
     * @return VwAssuntos
     */
    public function setBl_interno($bl_interno)
    {
        $this->bl_interno = $bl_interno;
        return $this;
    }


}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_professoraulasministradas")
 * @Entity
 * @EntityView
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class VwProfessorAulasMinistradas
{
    /**
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @Id
     * @var integer $id_unidade
     * @Column(name="id_unidade", type="integer", nullable=false, length=4)
     */
    private $id_unidade;
    /**
     * @Id
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @Id
     * @var integer $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=150)
     */
    private $st_areaconhecimento;
    /**
     * @var integer $nu_mes
     * @Column(name="nu_mes", type="integer", nullable=true, length=4)
     */
    private $nu_mes;
    /**
     * @var integer $nu_ano
     * @Column(name="nu_ano", type="integer", nullable=true, length=4)
     */
    private $nu_ano;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var nvarchar $dt_diasemana
     * @Column(name="dt_diasemana", type="string", nullable=false, length=8000)
     */
    private $dt_diasemana;
    /**
     * @Id
     * @var nvarchar $id_gradehoraria
     * @Column(name="id_gradehoraria", type="string", nullable=false, length=8000)
     */
    private $id_gradehoraria;
    /**
     * @var nvarchar $dt_periodo
     * @Column(name="dt_periodo", type="string", nullable=false, length=8000)
     */
    private $dt_periodo;

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_unidade
     */
    public function getId_unidade()
    {
        return $this->id_unidade;
    }

    /**
     * @param id_unidade
     */
    public function setId_unidade($id_unidade)
    {
        $this->id_unidade = $id_unidade;
        return $this;
    }

    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return integer st_areaconhecimento
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * @return integer nu_mes
     */
    public function getNu_mes()
    {
        return $this->nu_mes;
    }

    /**
     * @param nu_mes
     */
    public function setNu_mes($nu_mes)
    {
        $this->nu_mes = $nu_mes;
        return $this;
    }

    /**
     * @return integer nu_ano
     */
    public function getNu_ano()
    {
        return $this->nu_ano;
    }

    /**
     * @param nu_ano
     */
    public function setNu_ano($nu_ano)
    {
        $this->nu_ano = $nu_ano;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return nvarchar dt_periodo
     */
    public function getDt_periodo()
    {
        return $this->dt_periodo;
    }

    /**
     * @param dt_periodo
     */
    public function setDt_periodo($dt_periodo)
    {
        $this->dt_periodo = $dt_periodo;
        return $this;
    }

    /**
     * @return nvarchar dt_diasemana
     */
    public function getDt_diasemana()
    {
        return $this->dt_diasemana;
    }

    /**
     * @param dt_diasemana
     */
    public function setDt_diasemana($dt_diasemana)
    {
        $this->dt_diasemana = $dt_diasemana;
        return $this;
    }

    /**
     * @return nvarchar id_gradehoraria
     */
    public function getId_gradehoraria()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @param dt_diasemana
     */
    public function setId_gradehoraria($id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

}
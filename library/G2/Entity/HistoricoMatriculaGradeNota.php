<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_historicomatriculagradenota")
 * @Entity
 * @author Janilson Silva <janilson.silva@unyleya.com.br>
 */
class HistoricoMatriculaGradeNota extends G2Entity
{
    /**
     * @var int
     *
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_historicogradenota", type="integer", nullable=false)
     */
    private $id_historicogradenota;

    /**
     * @var \DateTime
     *
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string
     *
     * @Column(name="st_htmlgrade", type="text", length=-1, nullable=false)
     */
    private $st_htmlgrade;

    /**
     * @var Matricula
     *
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var ProjetoPedagogico
     *
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var TramiteMatricula
     *
     * @ManyToOne(targetEntity="TramiteMatricula")
     * @JoinColumn(name="id_tramitematricula", referencedColumnName="id_tramitematricula")
     */
    private $id_tramitematricula;

    /**
     * @return int
     */
    public function getId_historicogradenota()
    {
        return $this->id_historicogradenota;
    }


    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_htmlgrade()
    {
        return $this->st_htmlgrade;
    }

    /**
     * @param string $st_htmlgrade
     */
    public function setSt_htmlgrade($st_htmlgrade)
    {
        $this->st_htmlgrade = $st_htmlgrade;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return ProjetoPedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param ProjetoPedagogico $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return TramiteMatricula
     */
    public function getId_tramitematricula()
    {
        return $this->id_tramitematricula;
    }

    /**
     * @param TramiteMatricula $id_tramitematricula
     */
    public function setId_tramitematricula($id_tramitematricula)
    {
        $this->id_tramitematricula = $id_tramitematricula;
        return $this;
    }
}

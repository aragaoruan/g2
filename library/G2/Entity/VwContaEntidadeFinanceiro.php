<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_contaentidadefinanceiro")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwContaEntidadeFinanceiro
{
    /**
     * @var integer $id_contaentidade
     * @Column(name="id_contaentidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_contaentidade;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_tipodeconta
     * @Column(name="id_tipodeconta", type="integer", nullable=false, length=4)
     */
    private $id_tipodeconta;
    /**
     * @var integer $id_entidadefinanceiro
     * @Column(name="id_entidadefinanceiro", type="integer", nullable=true, length=4)
     */
    private $id_entidadefinanceiro;
    /**
     * @var integer $nu_diaspagamentoentrada
     * @Column(name="nu_diaspagamentoentrada", type="integer", nullable=true, length=4)
     */
    private $nu_diaspagamentoentrada;
    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false, length=1)
     */
    private $bl_padrao;
    /**
     * @var boolean $bl_automatricula
     * @Column(name="bl_automatricula", type="boolean", nullable=true, length=1)
     */
    private $bl_automatricula;
    /**
     * @var string $st_banco
     * @Column(name="st_banco", type="string", nullable=true, length=5)
     */
    private $st_banco;
    /**
     * @var string $st_digitoagencia
     * @Column(name="st_digitoagencia", type="string", nullable=true, length=2)
     */
    private $st_digitoagencia;
    /**
     * @var string $st_agencia
     * @Column(name="st_agencia", type="string", nullable=false, length=20)
     */
    private $st_agencia;
    /**
     * @var string $st_conta
     * @Column(name="st_conta", type="string", nullable=false, length=10)
     */
    private $st_conta;
    /**
     * @var string $st_digitoconta
     * @Column(name="st_digitoconta", type="string", nullable=true, length=2)
     */
    private $st_digitoconta;
    /**
     * @var string $st_tipodeconta
     * @Column(name="st_tipodeconta", type="string", nullable=true, length=100)
     */
    private $st_tipodeconta;
    /**
     * @var string $st_nomebanco
     * @Column(name="st_nomebanco", type="string", nullable=true, length=255)
     */
    private $st_nomebanco;

    /**
     * @var decimal $nu_multacomcarta
     * @Column(name="nu_multacomcarta", type="decimal", nullable=true)
     */
    private $nu_multacomcarta;

    /**
     * @var decimal $nu_multasemcarta
     * @Column(name="nu_multasemcarta", type="decimal", nullable=true)
     */
    private $nu_multasemcarta;



    /**
     * @return integer id_contaentidade
     */
    public function getId_contaentidade() {
        return $this->id_contaentidade;
    }

    /**
     * @param id_contaentidade
     */
    public function setId_contaentidade($id_contaentidade) {
        $this->id_contaentidade = $id_contaentidade;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_tipodeconta
     */
    public function getId_tipodeconta() {
        return $this->id_tipodeconta;
    }

    /**
     * @param id_tipodeconta
     */
    public function setId_tipodeconta($id_tipodeconta) {
        $this->id_tipodeconta = $id_tipodeconta;
        return $this;
    }

    /**
     * @return integer id_entidadefinanceiro
     */
    public function getId_entidadefinanceiro() {
        return $this->id_entidadefinanceiro;
    }

    /**
     * @param id_entidadefinanceiro
     */
    public function setId_entidadefinanceiro($id_entidadefinanceiro) {
        $this->id_entidadefinanceiro = $id_entidadefinanceiro;
        return $this;
    }

    /**
     * @return integer nu_diaspagamentoentrada
     */
    public function getNu_diaspagamentoentrada() {
        return $this->nu_diaspagamentoentrada;
    }

    /**
     * @param nu_diaspagamentoentrada
     */
    public function setNu_diaspagamentoentrada($nu_diaspagamentoentrada) {
        $this->nu_diaspagamentoentrada = $nu_diaspagamentoentrada;
        return $this;
    }

    /**
     * @return boolean bl_padrao
     */
    public function getBl_padrao() {
        return $this->bl_padrao;
    }

    /**
     * @param bl_padrao
     */
    public function setBl_padrao($bl_padrao) {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    /**
     * @return boolean bl_automatricula
     */
    public function getBl_automatricula() {
        return $this->bl_automatricula;
    }

    /**
     * @param bl_automatricula
     */
    public function setBl_automatricula($bl_automatricula) {
        $this->bl_automatricula = $bl_automatricula;
        return $this;
    }

    /**
     * @return string st_banco
     */
    public function getSt_banco() {
        return $this->st_banco;
    }

    /**
     * @param st_banco
     */
    public function setSt_banco($st_banco) {
        $this->st_banco = $st_banco;
        return $this;
    }

    /**
     * @return string st_digitoagencia
     */
    public function getSt_digitoagencia() {
        return $this->st_digitoagencia;
    }

    /**
     * @param st_digitoagencia
     */
    public function setSt_digitoagencia($st_digitoagencia) {
        $this->st_digitoagencia = $st_digitoagencia;
        return $this;
    }

    /**
     * @return string st_agencia
     */
    public function getSt_agencia() {
        return $this->st_agencia;
    }

    /**
     * @param st_agencia
     */
    public function setSt_agencia($st_agencia) {
        $this->st_agencia = $st_agencia;
        return $this;
    }

    /**
     * @return string st_conta
     */
    public function getSt_conta() {
        return $this->st_conta;
    }

    /**
     * @param st_conta
     */
    public function setSt_conta($st_conta) {
        $this->st_conta = $st_conta;
        return $this;
    }

    /**
     * @return string st_digitoconta
     */
    public function getSt_digitoconta() {
        return $this->st_digitoconta;
    }

    /**
     * @param st_digitoconta
     */
    public function setSt_digitoconta($st_digitoconta) {
        $this->st_digitoconta = $st_digitoconta;
        return $this;
    }

    /**
     * @return string st_tipodeconta
     */
    public function getSt_tipodeconta() {
        return $this->st_tipodeconta;
    }

    /**
     * @param st_tipodeconta
     */
    public function setSt_tipodeconta($st_tipodeconta) {
        $this->st_tipodeconta = $st_tipodeconta;
        return $this;
    }

    /**
     * @return string st_nomebanco
     */
    public function getSt_nomebanco() {
        return $this->st_nomebanco;
    }

    /**
     * @param st_nomebanco
     */
    public function setSt_nomebanco($st_nomebanco) {
        $this->st_nomebanco = $st_nomebanco;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNuMultacomcarta()
    {
        return $this->nu_multacomcarta;
    }

    /**
     * @param decimal $nu_multacomcarta
     */
    public function setNuMultacomcarta($nu_multacomcarta)
    {
        $this->nu_multacomcarta = $nu_multacomcarta;
    }

    /**
     * @return decimal
     */
    public function getNu_multasemcarta()
    {
        return $this->nu_multasemcarta;
    }

    /**
     * @param decimal $nu_multasemcarta
     */
    public function setNu_multasemcarta($nu_multasemcarta)
    {
        $this->nu_multasemcarta = $nu_multasemcarta;
    }




}
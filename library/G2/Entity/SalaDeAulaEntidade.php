<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_saladeaulaentidade")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class SalaDeAulaEntidade {

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false)
     * @Id
     *
     */
    private $id_saladeaula;

  
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gerararquivodeenvio")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwGerarArquivoDeEnvio {

    /**
     * @var date $dt_criacaopacote
     * @Column(name="dt_criacaopacote", type="date", nullable=true, length=3)
     */
    private $dt_criacaopacote;
    /**
     * @var integer $id_pacote
     * @Column(name="id_pacote", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_pacote;
    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false, length=4)
     */
    private $id_itemdematerial;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_lotematerial
     * @Column(name="id_lotematerial", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_lotematerial;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;
    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;
    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500)
     */
    private $st_complemento;
    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=255)
     */
    private $st_nomemunicipio;
    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12)
     */
    private $st_cep;
    /**
     * @var string $st_itemdematerial
     * @Column(name="st_itemdematerial", type="string", nullable=false, length=255)
     */
    private $st_itemdematerial;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;
    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;


    /**
     * @return date dt_criacaopacote
     */
    public function getDt_criacaopacote() {
        return $this->dt_criacaopacote;
    }

    /**
     * @param dt_criacaopacote
     */
    public function setDt_criacaopacote($dt_criacaopacote) {
        $this->dt_criacaopacote = $dt_criacaopacote;
        return $this;
    }

    /**
     * @return integer id_pacote
     */
    public function getId_pacote() {
        return $this->id_pacote;
    }

    /**
     * @param id_pacote
     */
    public function setId_pacote($id_pacote) {
        $this->id_pacote = $id_pacote;
        return $this;
    }

    /**
     * @return integer id_itemdematerial
     */
    public function getId_itemdematerial() {
        return $this->id_itemdematerial;
    }

    /**
     * @param id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial) {
        $this->id_itemdematerial = $id_itemdematerial;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_lotematerial
     */
    public function getId_lotematerial() {
        return $this->id_lotematerial;
    }

    /**
     * @param id_lotematerial
     */
    public function setId_lotematerial($id_lotematerial) {
        $this->id_lotematerial = $id_lotematerial;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_endereco
     */
    public function getSt_endereco() {
        return $this->st_endereco;
    }

    /**
     * @param st_endereco
     */
    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return string nu_numero
     */
    public function getNu_numero() {
        return $this->nu_numero;
    }

    /**
     * @param nu_numero
     */
    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string st_complemento
     */
    public function getSt_complemento() {
        return $this->st_complemento;
    }

    /**
     * @param st_complemento
     */
    public function setSt_complemento($st_complemento) {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * @return string st_bairro
     */
    public function getSt_bairro() {
        return $this->st_bairro;
    }

    /**
     * @param st_bairro
     */
    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_nomemunicipio
     */
    public function getSt_nomemunicipio() {
        return $this->st_nomemunicipio;
    }

    /**
     * @param st_nomemunicipio
     */
    public function setSt_nomemunicipio($st_nomemunicipio) {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    /**
     * @return string st_cep
     */
    public function getSt_cep() {
        return $this->st_cep;
    }

    /**
     * @param st_cep
     */
    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
        return $this;
    }

    /**
     * @return string st_itemdematerial
     */
    public function getSt_itemdematerial() {
        return $this->st_itemdematerial;
    }

    /**
     * @param st_itemdematerial
     */
    public function setSt_itemdematerial($st_itemdematerial) {
        $this->st_itemdematerial = $st_itemdematerial;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_areaconhecimento
     */
    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }


    public function getSg_uf() {
        return $this->sg_uf;
    }


    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
        return $this;
    }

}

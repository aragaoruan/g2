<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_projetosala")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwProjetoSala {

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_saladeaula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $bl_vinculado
     * @Column(name="bl_vinculado", type="integer", nullable=true, length=4)
     */
    private $bl_vinculado;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getBl_vinculado() {
        return $this->bl_vinculado;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setBl_vinculado($bl_vinculado) {
        $this->bl_vinculado = $bl_vinculado;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

}

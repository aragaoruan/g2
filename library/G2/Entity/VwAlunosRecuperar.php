<?php

namespace G2\Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="rel.vw_alunosrecuperar")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwAlunosRecuperar")
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class VwAlunosRecuperar
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=false)
     */
    private $id_entidadematricula;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=200)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false, length=30)
     */
    private $st_cpf;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=false, length=50)
     */
    private $st_email;

    /**
     * @var integer $nu_ddd
     * @Column(name="nu_ddd", type="integer", nullable=true, length=3)
     */
    private $nu_ddd;

    /**
     * @var integer $nu_telefone
     * @Column(name="nu_telefone", type="integer", nullable=true, length=10)
     */
    private $nu_telefone;

    /**
     * @var integer $nu_dddalternativo
     * @Column(name="nu_dddalternativo", type="integer", nullable=true, length=3)
     */
    private $nu_dddalternativo;

    /**
     * @var integer $nu_telefonealternativo
     * @Column(name="nu_telefonealternativo", type="integer", nullable=true, length=10)
     */
    private $nu_telefonealternativo;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", nullable=false, type="integer")
     */
    private $id_areaconhecimento;

    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", nullable=false, type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", nullable=false, type="integer")
     */
    private $id_disciplina;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=30)
     */
    private $st_evolucao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", nullable=false, type="integer")
     */
    private $id_evolucao;

    /**
     * @var datetime $dt_inicioturma
     * @Column(name="dt_inicioturma", type="datetime", nullable=true)
     */
    private $dt_inicioturma;

    /**
     * @var datetime $dt_aberturasala
     * @Column(name="dt_aberturasala", type="datetime", nullable=true)
     */
    private $dt_aberturasala;

    /**
     * @var datetime $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="datetime", nullable=true)
     */
    private $dt_encerramentosala;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina",nullable=false, type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @var string $st_avaliacaofinal
     * @Column(name="st_avaliacaofinal", type="string", nullable=true, length=20)
     */
    private $st_avaliacaofinal;

    /**
     * @var string $st_avaliacaoead
     * @Column(name="st_avaliacaoead", type="string", nullable=true, length=20)
     */
    private $st_avaliacaoead;

    /**
     * @var string $st_avaliacaotcc
     * @Column(name="st_avaliacaotcc", type="string", nullable=true, length=20)
     */
    private $st_avaliacaotcc;

    /**
     * @var string $st_notafinal
     * @Column(name="st_notafinal", type="string", nullable=true, length=5)
     */
    private $st_notafinal;

    /**
     * @var string $st_notaead
     * @Column(name="st_notaead", type="string", nullable=true, length=5)
     */
    private $st_notaead;

    /**
     * @var integer $nu_notafinal
     * @Column(name="nu_notafinal", type="integer", nullable=true, length=5)
     */
    private $nu_notafinal;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", nullable=false, type="integer")
     */
    private $id_matricula;

    /**
     * @var integer $id_situacaotiposervico
     * @Column(name="id_situacaotiposervico", nullable=true, type="integer")
     */
    private $id_situacaotiposervico;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", nullable=true, type="integer")
     */
    private $id_saladeaula;

    /**
     * @return int
     */
    public function getId_situacaotiposervico()
    {
        return $this->id_situacaotiposervico;
    }

    /**
     * @param int $id_situacaotiposervico
     */
    public function setId_situacaotiposervico($id_situacaotiposervico)
    {
        $this->id_situacaotiposervico = $id_situacaotiposervico;
    }

    /**
     * @return int
     */
    public function getId_Matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_Matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @return int
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }
    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param int $nu_ddd
     */
    public function setNu_ddd($nu_ddd)
    {
        $this->nu_maxaplicacao = $nu_ddd;
    }

    /**
     * @return int
     */
    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    /**
     * @param int $nu_telefone
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
    }

    /**
     * @return int
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @param int $nu_dddalternativo
     */
    public function setNu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
    }

    /**
     * @return int
     */
    public function getNu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    /**
     * @param int $nu_telefonealternativo
     */
    public function setNu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
    }

    /**
     * @return int
     */
    public function getNu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param int id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param string st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param \Datetime $dt_inicioturma
     */
    public function setDt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
    }

    /**
     * @return \Datetime
     */
    public function getDt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param \Datetime $dt_aberturasala
     */
    public function setDt_aberturasala($dt_aberturasala)
    {
        $this->dt_aberturasala = $dt_aberturasala;
    }

    /**
     * @return \Datetime
     */
    public function getDt_aberturasala()
    {
        return $this->dt_aberturasala;
    }

    /**
     * @param \Datetime $dt_encerramentosala
     */
    public function setDt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
    }

    /**
     * @return \Datetime
     */
    public function getDt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param string $$st_avaliacaofinal
     */
    public function setSt_avaliacaofinal($st_avaliacaofinal)
    {
        $this->st_avaliacaofinal = $st_avaliacaofinal;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaofinal()
    {
        return $this->st_avaliacaofinal;
    }


    /**
     * @param string $st_avaliacaoead
     */
    public function setSt_avaliacaoead($st_avaliacaoead)
    {
        $this->st_avaliacaoead = $st_avaliacaoead;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaoead()
    {
        return $this->st_avaliacaoead;
    }


    /**
     * @param string $st_avaliacaotcc
     */
    public function setSt_avaliacaotcc($st_avaliacaotcc)
    {
        $this->st_avaliacaotcc = $st_avaliacaotcc;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaotcc()
    {
        return $this->st_avaliacaotcc;
    }


    /**
     * @param string $st_notafinal
     */
    public function setSt_notafinal($st_notafinal)
    {
        $this->st_notafinal = $st_notafinal;
    }

    /**
     * @return string
     */
    public function getSt_notafinal()
    {
        return $this->st_notafinal;
    }


    /**
     * @param string $st_notaead
     */
    public function setSt_notaead($st_notaead)
    {
        $this->st_notaead = $st_notaead;
    }

    /**
     * @return string
     */
    public function getSt_notaead()
    {
        return $this->st_notaead;
    }

    /**
     * @param int $nu_notafinal
     */
    public function setNu_notafinal($nu_notafinal)
    {
        $this->nu_notafinal = $nu_notafinal;
    }

    /**
     * @return int
     */
    public function getNu_notafinal()
    {
        return $this->nu_notafinal;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }



}

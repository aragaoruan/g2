<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tiposanguineo")
 * @Entity
 * @EntityLog
 */
class TipoSanguineo extends G2Entity
{
    /**
     * @var int
     * @Column(name="id_tiposanguineo", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tiposanguineo;

    /**
     * @var string
     * @Column(name="sg_tiposanguineo", type="string", nullable=false, length=2)
     */
    private $sg_tiposanguineo;

    /**
     * @return mixed
     */
    public function getId_tiposanguineo()
    {
        return $this->id_tiposanguineo;
    }

    /**
     * @param mixed $id_tiposanguineo
     * @return $this
     */
    public function setId_tiposanguineo($id_tiposanguineo)
    {
        $this->id_tiposanguineo = $id_tiposanguineo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSg_tiposanguineo()
    {
        return $this->sg_tiposanguineo;
    }

    /**
     * @param string $sg_tiposanguineo
     * @return $this
     */
    public function setSg_tiposanguineo($sg_tiposanguineo)
    {
        $this->sg_tiposanguineo = $sg_tiposanguineo;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadefuncionalidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadeFuncionalidade
{

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=false)
     * @Id
     * @ManyToOne(targetEntity="Funcionalidade")
     */
    private $id_funcionalidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo",type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var Situacao $id_situacao
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var string $st_label
     * @Column(name="st_label",type="string", nullable=true, length=255)
     */
    private $st_label;

    /**
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio",type="boolean", nullable=false)
     */
    private $bl_obrigatorio;

    /**
     * @var boolean $bl_visivel
     * @Column(name="bl_visivel",type="boolean", nullable=true)
     */
    private $bl_visivel;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem",type="integer", nullable=true)
     */
    private $nu_ordem;

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_label ()
    {
        return $this->st_label;
    }

    public function setSt_label ($st_label)
    {
        $this->st_label = $st_label;
        return $this;
    }

    public function getBl_obrigatorio ()
    {
        return $this->bl_obrigatorio;
    }

    public function setBl_obrigatorio ($bl_obrigatorio)
    {
        $this->bl_obrigatorio = $bl_obrigatorio;
        return $this;
    }

    public function getBl_visivel ()
    {
        return $this->bl_visivel;
    }

    public function setBl_visivel ($bl_visivel)
    {
        $this->bl_visivel = $bl_visivel;
        return $this;
    }

    public function getNu_ordem ()
    {
        return $this->nu_ordem;
    }

    public function setNu_ordem ($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

}
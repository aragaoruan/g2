<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_salasparacriarprr")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwSalasParaCriarPrr {

    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true, length=3)
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=4)
     */
    private $dt_fiminscricao;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=4)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=false, length=4)
     */
    private $id_categoriasala;

    /**
     * @var integer $id_modalidadesaladeaula
     * @Column(name="id_modalidadesaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_modalidadesaladeaula;

    /**
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_tiposaladeaula;

    /**
     * @var integer $nu_diasencerramento
     * @Column(name="nu_diasencerramento", type="integer", nullable=false, length=4)
     */
    private $nu_diasencerramento;

    /**
     * @var integer $bl_semencerramento
     * @Column(name="bl_semencerramento", type="integer", nullable=false, length=4)
     */
    private $bl_semencerramento;

    /**
     * @var integer $bl_semdiasaluno
     * @Column(name="bl_semdiasaluno", type="integer", nullable=false, length=4)
     */
    private $bl_semdiasaluno;

    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjunto;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true, length=4)
     */
    private $id_periodoletivo;

    /**
     * @var integer $bl_usardoperiodoletivo
     * @Column(name="bl_usardoperiodoletivo", type="integer", nullable=false, length=4)
     */
    private $bl_usardoperiodoletivo;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=false, length=4)
     */
    private $nu_maxalunos;

    /**
     * @var integer $nu_diasaluno
     * @Column(name="nu_diasaluno", type="integer", nullable=false, length=4)
     */
    private $nu_diasaluno;

    /**
     * @var integer $nu_diasextensao
     * @Column(name="nu_diasextensao", type="integer", nullable=false, length=4)
     */
    private $nu_diasextensao;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=261)
     */
    private $st_saladeaula;

    public function getDt_inicioinscricao() {
        return $this->dt_inicioinscricao;
    }

    public function getDt_abertura() {
        return $this->dt_abertura;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getDt_fiminscricao() {
        return $this->dt_fiminscricao;
    }

    public function getDt_encerramento() {
        return $this->dt_encerramento;
    }

    public function getId_categoriasala() {
        return $this->id_categoriasala;
    }

    public function getId_modalidadesaladeaula() {
        return $this->id_modalidadesaladeaula;
    }

    public function getId_tiposaladeaula() {
        return $this->id_tiposaladeaula;
    }

    public function getNu_diasencerramento() {
        return $this->nu_diasencerramento;
    }

    public function getBl_semencerramento() {
        return $this->bl_semencerramento;
    }

    public function getBl_semdiasaluno() {
        return $this->bl_semdiasaluno;
    }

    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_periodoletivo() {
        return $this->id_periodoletivo;
    }

    public function getBl_usardoperiodoletivo() {
        return $this->bl_usardoperiodoletivo;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    public function getNu_diasaluno() {
        return $this->nu_diasaluno;
    }

    public function getNu_diasextensao() {
        return $this->nu_diasextensao;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    public function setDt_inicioinscricao(date $dt_inicioinscricao) {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    public function setDt_abertura(date $dt_abertura) {
        $this->dt_abertura = $dt_abertura;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function setDt_fiminscricao($dt_fiminscricao) {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    public function setDt_encerramento($dt_encerramento) {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function setId_categoriasala($id_categoriasala) {
        $this->id_categoriasala = $id_categoriasala;
    }

    public function setId_modalidadesaladeaula($id_modalidadesaladeaula) {
        $this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
    }

    public function setId_tiposaladeaula($id_tiposaladeaula) {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
    }

    public function setNu_diasencerramento($nu_diasencerramento) {
        $this->nu_diasencerramento = $nu_diasencerramento;
    }

    public function setBl_semencerramento($bl_semencerramento) {
        $this->bl_semencerramento = $bl_semencerramento;
    }

    public function setBl_semdiasaluno($bl_semdiasaluno) {
        $this->bl_semdiasaluno = $bl_semdiasaluno;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_periodoletivo($id_periodoletivo) {
        $this->id_periodoletivo = $id_periodoletivo;
    }

    public function setBl_usardoperiodoletivo($bl_usardoperiodoletivo) {
        $this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setNu_maxalunos($nu_maxalunos) {
        $this->nu_maxalunos = $nu_maxalunos;
    }

    public function setNu_diasaluno($nu_diasaluno) {
        $this->nu_diasaluno = $nu_diasaluno;
    }

    public function setNu_diasextensao($nu_diasextensao) {
        $this->nu_diasextensao = $nu_diasextensao;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

}

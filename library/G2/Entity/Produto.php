<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produto")
 * @Entity
 * @Entity(repositoryClass="\G2\Repository\Produto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2013-11-07
 * @author Ilson Nóbrega <ilson.nobrega@unlyleya.com.br> 02-10-2014
 */
use G2\G2Entity;

class Produto extends G2Entity
{

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produto;

    /**
     *
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=true, length=250)
     */
    private $st_produto;

    /**
     * @var TipoProduto $id_tipoproduto
     * @ManyToOne(targetEntity="TipoProduto")
     * @JoinColumn(name="id_tipoproduto", nullable=false, referencedColumnName="id_tipoproduto")
     */
    private $id_tipoproduto;

    /**
     * @var ModeloVenda $id_modelovenda
     * @ManyToOne(targetEntity="ModeloVenda")
     * @JoinColumn(name="id_modelovenda", nullable=false, referencedColumnName="id_modelovenda")
     */
    private $id_modelovenda;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false)
     */
    private $bl_todasformas;

    /**
     *
     * @var boolean $bl_todascampanhas
     * @Column(name="bl_todascampanhas", type="boolean", nullable=false)
     */
    private $bl_todascampanhas;

    /**
     *
     * @var boolean $bl_unico
     * @Column(name="bl_unico", type="boolean", nullable=false)
     */
    private $bl_unico;

    /**
     *
     * @var integer $nu_gratuito
     * @Column(name="nu_gratuito", type="integer", nullable=false)
     */
    private $nu_gratuito;

    /**
     *
     * @var integer $id_produtoimagempadrao
     * @Column(name="id_produtoimagempadrao", type="integer", nullable=true)
     */
    private $id_produtoimagempadrao;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", length=255, nullable=true)
     */
    private $st_descricao;

    /**
     *
     * @var string $st_observacoes
     * @Column(name="st_observacoes", type="string", length=255, nullable=true)
     */
    private $st_observacoes;

    /**
     *
     * @var string $st_informacoesadicionais
     * @Column(name="st_informacoesadicionais", type="string", length=255, nullable=true)
     */
    private $st_informacoesadicionais;

    /**
     *
     * @var string $st_estruturacurricular
     * @Column(name="st_estruturacurricular", type="string", length=255, nullable=true)
     */
    private $st_estruturacurricular;

    /**
     *
     * @var string $st_subtitulo
     * @Column(name="st_subtitulo", type="string", nullable=true, length=300)
     */
    private $st_subtitulo;

    /**
     *
     * @var string $st_slug
     * @Column(name="st_slug", type="string", nullable=true, length=200)
     */
    private $st_slug;

    /**
     *
     * @var boolean $bl_destaque
     * @Column(name="bl_destaque", type="boolean", nullable=false)
     */
    private $bl_destaque;

    /**
     *
     * @var boolean $bl_mostrarpreco
     * @Column(name="bl_mostrarpreco", type="boolean", nullable=false)
     */
    private $bl_mostrarpreco;


    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_atualizado
     * @Column(type="datetime2", nullable=false, name="dt_atualizado")
     */
    private $dt_atualizado;

    /**
     * @var datetime2 $dt_iniciopontosprom
     * @Column(type="datetime2", nullable=true, name="dt_iniciopontosprom")
     */
    private $dt_iniciopontosprom;

    /**
     * @var datetime2 $dt_fimpontosprom
     * @Column(type="datetime2", nullable=true, name="dt_fimpontosprom")
     */
    private $dt_fimpontosprom;

    /**
     *
     * @var integer $nu_pontos
     * @Column(name="nu_pontos", type="integer", nullable=false)
     */
    private $nu_pontos;

    /**
     *
     * @var integer $nu_pontospromocional
     * @Column(name="nu_pontospromocional", type="integer", nullable=false)
     */
    private $nu_pontospromocional;

    /**
     *
     * @var integer $nu_estoque
     * @Column(name="nu_estoque", type="integer", nullable=false)
     */
    private $nu_estoque;

    /**
     *
     * @var boolean $bl_todasentidades
     * @Column(name="bl_todasentidades", type="boolean", nullable=false)
     */
    private $bl_todasentidades;
    /**
     *
     * @var boolean $st_cargahoraria
     * @Column(name="st_cargahoraria", type="string", length=255, nullable=false)
     */
    private $st_cargahoraria;

    /**
     *
     * @var string $st_codigoavaliacao
     * @Column(name="st_codigoavaliacao", type="string", length=255, nullable=true)
     */
    private $st_codigoavaliacao;


    /**
     * @return integer $id_modelovenda
     */
    public function getId_modelovenda()
    {
        return $this->id_modelovenda;
    }

    /**
     * @param $id_modelovenda
     * @return $this
     */
    public function setId_modelovenda($id_modelovenda)
    {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    /**
     * @return boolean $bl_todasentidades
     */
    public function getBl_todasentidades()
    {
        return $this->bl_todasentidades;
    }

    /**
     * @param boolean $bl_todasentidades
     * @return $this
     */
    public function setBl_todasentidades($bl_todasentidades)
    {
        $this->bl_todasentidades = $bl_todasentidades;
        return $this;
    }

    /**
     * @return string $st_descricao
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return $this
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return string $st_observacoes
     */
    public function getSt_observacoes()
    {
        return $this->st_observacoes;
    }

    /**
     * @param string $st_observacoes
     * @return $this
     */
    public function setSt_observacoes($st_observacoes)
    {
        $this->st_observacoes = $st_observacoes;
        return $this;
    }

    /**
     * @return string $st_informacoesadicionais
     */
    public function getSt_informacoesadicionais()
    {
        return $this->st_informacoesadicionais;
    }

    /**
     * @param string $st_informacoesadicionais
     * @return $this
     */
    public function setSt_informacoesadicionais($st_informacoesadicionais)
    {
        $this->st_informacoesadicionais = $st_informacoesadicionais;
        return $this;
    }

    /**
     * @return string $st_estruturacurricular
     */
    public function getSt_estruturacurricular()
    {
        return $this->st_estruturacurricular;
    }

    /**
     * @param string $st_estruturacurricular
     * @return $this
     */
    public function setSt_estruturacurricular($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
        return $this;
    }

    /**
     * @return string $st_subtitulo
     */
    public function getSt_subtitulo()
    {
        return $this->st_subtitulo;
    }

    /**
     * @param string $st_subtitulo
     * @return $this
     */
    public function setSt_subtitulo($st_subtitulo)
    {
        $this->st_subtitulo = $st_subtitulo;
        return $this;
    }

    /**
     * @return string $st_slug
     */
    public function getSt_slug()
    {
        return $this->st_slug;
    }

    /**
     * @param string $st_slug
     * @return $this
     */
    public function setSt_slug($st_slug)
    {
        $this->st_slug = $st_slug;
        return $this;
    }

    /**
     * @return boolean $bl_destaque
     */
    public function getBl_destaque()
    {
        return $this->bl_destaque;
    }

    /**
     * @param boolean $bl_destaque
     * @return $this
     */
    public function setBl_destaque($bl_destaque)
    {
        $this->bl_destaque = $bl_destaque;
        return $this;
    }

    /**
     * @return boolean $bl_mostrarpreco
     */
    public function getBl_mostrarpreco()
    {
        return $this->bl_mostrarpreco;
    }

    /**
     * @return boolean $bl_mostrarpreco
     */
    public function setBl_mostrarpreco($bl_mostrarpreco)
    {
        $this->bl_mostrarpreco = $bl_mostrarpreco;
        return $this;
    }

    /**
     * @return datetime2 $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime2 $dt_atualizado
     */
    public function getDt_atualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_atualizado
     * @return $this
     */
    public function setDt_atualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
        return $this;
    }

    /**
     * @return datetime2 $dt_iniciopontosprom
     */
    public function getDt_iniciopontosprom()
    {
        return $this->dt_iniciopontosprom;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_iniciopontosprom
     * @return $this
     */
    public function setDt_iniciopontosprom($dt_iniciopontosprom)
    {
        $this->dt_iniciopontosprom = $dt_iniciopontosprom;
        return $this;
    }

    /**
     * @return datetime2 $dt_fimpontosprom
     */
    public function getDt_fimpontosprom()
    {
        return $this->dt_fimpontosprom;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_fimpontosprom
     * @return $this
     */
    public function setDt_fimpontosprom($dt_fimpontosprom)
    {
        $this->dt_fimpontosprom = $dt_fimpontosprom;
        return $this;
    }

    /**
     * @return integer $nu_pontos
     */
    public function getNu_pontos()
    {
        return $this->nu_pontos;
    }

    /**
     * @param number $nu_pontos
     * @return $this
     */
    public function setNu_pontos($nu_pontos)
    {
        $this->nu_pontos = $nu_pontos;
        return $this;
    }

    /**
     * @return integer $nu_pontospromocional
     */
    public function getNu_pontospromocional()
    {
        return $this->nu_pontospromocional;
    }

    /**
     * @param number $nu_pontospromocional
     * @return $this
     */
    public function setNu_pontospromocional($nu_pontospromocional)
    {
        $this->nu_pontospromocional = $nu_pontospromocional;
        return $this;
    }

    /**
     * @return integer $nu_estoque
     */
    public function getNu_estoque()
    {
        return $this->nu_estoque;
    }

    /**
     * @param number $nu_estoque
     * @return $this
     */
    public function setNu_estoque($nu_estoque)
    {
        $this->nu_estoque = $nu_estoque;
        return $this;
    }

    /**
     * @return boolean $bl_unico
     */
    public function getBl_unico()
    {
        return $this->bl_unico;
    }

    /**
     * @param boolean $bl_unico
     */
    public function setBl_unico($bl_unico)
    {
        $this->bl_unico = $bl_unico;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param $id_produto
     * @return $this
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @param $st_produto
     * @return $this
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @return TipoProduto
     */
    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @param TipoProduto $id_tipoproduto
     * @return $this
     */
    public function setId_tipoproduto(TipoProduto $id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     * @return $this
     */
    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @param $bl_todasformas
     * @return $this
     */
    public function setBl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_todascampanhas()
    {
        return $this->bl_todascampanhas;
    }

    /**
     * @param $bl_todascampanhas
     * @return $this
     */
    public function setBl_todascampanhas($bl_todascampanhas)
    {
        $this->bl_todascampanhas = $bl_todascampanhas;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_gratuito()
    {
        return $this->nu_gratuito;
    }

    /**
     * @param $nu_gratuito
     * @return $this
     */
    public function setNu_gratuito($nu_gratuito)
    {
        $this->nu_gratuito = $nu_gratuito;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_produtoimagempadrao()
    {
        return $this->id_produtoimagempadrao;
    }

    /**
     * @param $id_produtoimagempadrao
     * @return $this
     */
    public function setId_produtoimagempadrao($id_produtoimagempadrao)
    {
        $this->id_produtoimagempadrao = $id_produtoimagempadrao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSt_Cargahoraria()
    {
        return $this->st_cargahoraria;
    }

    /**
     * @param boolean $st_cargahoraria
     */
    public function setSt_Cargahoraria($st_cargahoraria)
    {
        $this->st_cargahoraria = $st_cargahoraria;
    }

    /**
     * @return string
     */
    public function getSt_Codigoavaliacao()
    {
        return $this->st_codigoavaliacao;
    }

    /**
     * @param string $st_codigoavaliacao
     */
    public function setSt_Codigoavaliacao($st_codigoavaliacao)
    {
        $this->st_codigoavaliacao = $st_codigoavaliacao;
    }


}
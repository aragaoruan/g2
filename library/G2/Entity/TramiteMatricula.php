<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tramitematricula")
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class TramiteMatricula
{

    /**
     * @Id
     * @var integer $id_tramitematricula
     * @Column(name="id_tramitematricula", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramitematricula;

    /**
     * @var integer $id_tramite
     * @ManyToOne(targetEntity="Tramite")
     * @JoinColumn(name="id_tramite", referencedColumnName="id_tramite")
     */
    private $id_tramite;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;


    /**
     * @return integer id_tramitematricula
     */
    public function getId_tramitematricula()
    {
        return $this->id_tramitematricula;
    }

    /**
     * @param id_tramitematricula
     */
    public function setId_tramitematricula($id_tramitematricula)
    {
        $this->id_tramitematricula = $id_tramitematricula;
        return $this;
    }

    /**
     * @return integer id_tramite
     */
    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    /**
     * @param id_tramite
     */
    public function setId_tramite(\G2\Entity\Tramite $id_tramite)
    {
        $this->id_tramite = $id_tramite;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula(\G2\Entity\Matricula $id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

} 
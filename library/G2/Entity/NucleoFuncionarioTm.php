<?php


namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_nucleofuncionariotm")
 * @Entity(repositoryClass="G2\Repository\NucleoFuncionarioTm")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class NucleoFuncionarioTm
{

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @Id
     * @var integer $id_nucleofuncionariotm
     * @Column(name="id_nucleofuncionariotm", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_nucleofuncionariotm;
    /**
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=true, length=4)
     */
    private $id_funcao;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_nucleotm
     * @Column(name="id_nucleotm", type="integer", nullable=true, length=4)
     */
    private $id_nucleotm;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_nucleofuncionariotm
     */
    public function getId_nucleofuncionariotm()
    {
        return $this->id_nucleofuncionariotm;
    }

    /**
     * @param id_nucleofuncionariotm
     */
    public function setId_nucleofuncionariotm($id_nucleofuncionariotm)
    {
        $this->id_nucleofuncionariotm = $id_nucleofuncionariotm;
        return $this;
    }

    /**
     * @return integer id_funcao
     */
    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param id_funcao
     */
    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_nucleotm
     */
    public function getId_nucleotm()
    {
        return $this->id_nucleotm;
    }

    /**
     * @param id_nucleotm
     */
    public function setId_nucleotm($id_nucleotm)
    {
        $this->id_nucleotm = $id_nucleotm;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


}
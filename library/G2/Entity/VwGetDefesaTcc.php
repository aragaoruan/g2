<?php

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_getdefesatcc")
 * @Entity
 * @EntityView
 * @author Ruan Aragão <ruan.aragao@unyleya.com>
 */
class VwGetDefesaTcc
{
    /**
     * @var \DateTime $dt_defesa
     * @Column(name="dt_defesa", type="datetime2")
     */
    private $dt_defesa;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_tipodisciplina;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_evolucao;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_matriculadisciplina;

    /**
     * @return date
     */
    public function getdt_defesa()
    {
        return $this->dt_defesa;
    }

    /**
     * @param date $dt_defesa
     */
    public function setdt_defesa($dt_defesa)
    {
        $this->dt_defesa = $dt_defesa;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }


}

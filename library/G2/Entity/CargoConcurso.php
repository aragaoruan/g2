<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_cargoconcurso")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 
 */
class CargoConcurso
{

    /**
     * @var integer $id_cargoconcurso
     * @Column(name="id_cargoconcurso", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cargoconcurso;

    /**
     * @var Concurso $id_concurso
     * @ManyToOne(targetEntity="Concurso", inversedBy="cargos")
     * @JoinColumn(name="id_concurso", referencedColumnName="id_concurso", nullable=true)
     */
    private $id_concurso;

    /**
     * @var decimal $nu_salario
     * @Column(name="nu_salario", type="decimal", nullable=true, precision=30, scale=2)
     */
    private $nu_salario;

    /**
     * @var string $st_cargoconcurso
     * @Column(name="st_cargoconcurso", type="string", nullable=false, length=200)
     */
    private $st_cargoconcurso;

    /**
     * @return integer id_cargoconcurso
     */
    public function getId_cargoconcurso ()
    {
        return $this->id_cargoconcurso;
    }

    /**
     * @param integer $id_cargoconcurso
     * @return \G2\Entity\CargoConcurso
     */
    public function setId_cargoconcurso ($id_cargoconcurso)
    {
        $this->id_cargoconcurso = $id_cargoconcurso;
        return $this;
    }

    /**
     * @return \G2\Entity\Concurso id_concurso
     */
    public function getId_concurso ()
    {
        return $this->id_concurso;
    }

    /**
     * @param \G2\Entity\Concurso $id_concurso
     * @return \G2\Entity\CargoConcurso
     */
    public function setId_concurso (Concurso $id_concurso)
    {
        $this->id_concurso = $id_concurso;
        return $this;
    }

    /**
     * @return decimal nu_salario
     */
    public function getNu_salario ()
    {
        return $this->nu_salario;
    }

    /**
     * @param decimal $nu_salario
     * @return \G2\Entity\CargoConcurso
     */
    public function setNu_salario ($nu_salario)
    {
        $this->nu_salario = $nu_salario;
        return $this;
    }

    /**
     * @return string st_cargoconcurso
     */
    public function getSt_cargoconcurso ()
    {
        return $this->st_cargoconcurso;
    }

    /**
     * @param string $st_cargoconcurso
     * @return \G2\Entity\CargoConcurso
     */
    public function setSt_cargoconcurso ($st_cargoconcurso)
    {
        $this->st_cargoconcurso = $st_cargoconcurso;
        return $this;
    }

    /**
     * Retorna array com atributos
     * @return array 
     */
    public function toArray ()
    {
        return array(
            'id_cargoconcurso' => $this->id_cargoconcurso,
            'id_concurso' => $this->id_concurso->toArray(),
            'st_cargoconcurso' => $this->st_cargoconcurso,
            'nu_salario' => $this->nu_salario,
        );
    }

}
<?php

/*
 * Entity MaterialProjeto
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-27
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_materialprojeto")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class MaterialProjeto {

    /**
     *
     * @var integer $id_materialprojeto
     * @Column(name="id_materialprojeto", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_materialprojeto;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", nullable=false, referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=true)
     */
    private $id_itemdematerial;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
    }

    /**
     * @return int
     */
    public function getId_itemdematerial()
    {
        return $this->id_itemdematerial;
    }

    /**
     * @param int $id_materialprojeto
     */
    public function setId_materialprojeto($id_materialprojeto)
    {
        $this->id_materialprojeto = $id_materialprojeto;
    }

    /**
     * @return int
     */
    public function getId_materialprojeto()
    {
        return $this->id_materialprojeto;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }





}

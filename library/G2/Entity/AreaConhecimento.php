<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_areaconhecimento")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2013-30-10
 */
class AreaConhecimento extends G2Entity
{

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_areaconhecimento;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @Column(name="st_descricao", type="string",length=2500,nullable=false)
     * @var string $st_descricao
     */
    private $st_descricao;

    /**
     * @Column(name="bl_ativo",type="boolean",nullable=false)
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     * @var string $st_areaconhecimento
     */
    private $st_areaconhecimento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     * @var datetime2 $dt_cadastro
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @Column(name="id_areaconhecimentopai", type="integer", nullable=true)
     * @var integer $id_areaconhecimentopai
     */
    private $id_areaconhecimentopai;

    /**
     * @var integer $id_tipoareaconhecimento
     * @ManyToOne(targetEntity="TipoAreaConhecimento")
     * @JoinColumn(name="id_tipoareaconhecimento", referencedColumnName="id_tipoareaconhecimento")
     */
    private $id_tipoareaconhecimento;

    /**
     * @Column(name="st_tituloexibicao",type="text",nullable=true)
     * @var text $st_tituloexibicao
     */
    private $st_tituloexibicao;

    /**
     * @Column(name="bl_extras",type="boolean",nullable=true)
     * @var boolean $bl_extras
     */
    private $bl_extras;

    /**
     *
     * @var string $st_imagemarea
     * @Column(type="string", length=255, nullable=true)
     */
    private $st_imagemarea;

    /**
     * @var AreaConhecimento $id_origemareaconhecimento
     * @ManyToOne(targetEntity="OrigemAreaConhecimento")
     * @JoinColumn(name="id_origemareaconhecimento", referencedColumnName="id_origemareaconhecimento")
     */
    private $id_origemareaconhecimento;

    public function getId()
    {
        return $this->id_areaconhecimento;
    }

    public function setId($id)
    {
        $this->id_areaconhecimento = $id;
        return $this;
    }

    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getId_areaconhecimentopai()
    {
        return $this->id_areaconhecimentopai;
    }

    public function getId_tipoareaconhecimento()
    {
        return $this->id_tipoareaconhecimento;
    }

    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    public function getBl_extras()
    {
        return $this->bl_extras;
    }

    public function getSt_imagemarea()
    {
        return $this->st_imagemarea;
    }

    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_areaconhecimentopai($id_areaconhecimentopai)
    {
        $this->id_areaconhecimentopai = $id_areaconhecimentopai;
        return $this;
    }

    public function setId_tipoareaconhecimento($id_tipoareaconhecimento)
    {
        $this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
        return $this;
    }

    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    public function setBl_extras($bl_extras)
    {
        $this->bl_extras = $bl_extras;
        return $this;
    }

    public function setSt_imagemarea($st_imagemarea)
    {
        $this->st_imagemarea = $st_imagemarea;
        return $this;
    }

    /**
     * @return OrigemAreaConhecimento
     */
    public function getId_origemareaconhecimento()
    {
        return $this->id_origemareaconhecimento;
    }

    /**
     * @param int $id_origemareaconhecimento
     * @return $this;
     */
    public function setId_origemareaconhecimento($id_origemareaconhecimento)
    {
        $this->id_origemareaconhecimento = $id_origemareaconhecimento;
        return $this;
    }

}

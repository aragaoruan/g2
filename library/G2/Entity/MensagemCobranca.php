<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_mensagemcobranca")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class MensagemCobranca {

    /**
     * @var integer $id_mensagemcobranca
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_mensagemcobranca;

    /**
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var boolean $nu_diasatraso
     * @Column(type="integer")
     */
    private $nu_diasatraso;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean")
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_antecedencia
     * @Column(type="boolean")
     */
    private $bl_antecedencia;

    public function getId_mensagemcobranca() {
        return $this->id_mensagemcobranca;
    }

    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getNu_diasatraso() {
        return $this->nu_diasatraso;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getBl_antecedencia() {
        return $this->bl_antecedencia;
    }

    public function setId_mensagemcobranca($id_mensagemcobranca) {
        $this->id_mensagemcobranca = $id_mensagemcobranca;
    }

    public function setId_textosistema(TextoSistema $id_textosistema) {
        $this->id_textosistema = $id_textosistema;
    }

    public function setId_entidade(Entidade $id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setNu_diasatraso($nu_diasatraso) {
        $this->nu_diasatraso = $nu_diasatraso;
    }

    public function setDt_cadastro(\DateTime $dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setBl_antecedencia($bl_antecedencia) {
        $this->bl_antecedencia = $bl_antecedencia;
    }

}

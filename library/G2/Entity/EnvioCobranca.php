<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_enviocobranca")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class EnvioCobranca extends G2Entity {

    /**
     * @var integer $id_enviocobranca
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_enviocobranca;

    /**
     * @var MensagemCobranca
     * 
     * @ManyToOne(targetEntity="MensagemCobranca")
     * @JoinColumn(name="id_mensagemcobranca", referencedColumnName="id_mensagemcobranca")
     */
    private $id_mensagemcobranca;

    /**
     * @var Mensagem
     * 
     * @ManyToOne(targetEntity="Mensagem")
     * @JoinColumn(name="id_mensagem", referencedColumnName="id_mensagem")
     */
    private $id_mensagem;

    /**
     * @var Lancamento
     * 
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumn(name="id_lancamento", referencedColumnName="id_lancamento")
     */
    private $id_lancamento;

    public function getId_enviocobranca() {
        return $this->id_enviocobranca;
    }

    public function getId_mensagemcobranca() {
        return $this->id_mensagemcobranca;
    }

    public function getId_mensagem() {
        return $this->id_mensagem;
    }

    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    public function setId_enviocobranca($id_enviocobranca) {
        $this->id_enviocobranca = $id_enviocobranca;
    }

    public function setId_mensagemcobranca(MensagemCobranca $id_mensagemcobranca) {
        $this->id_mensagemcobranca = $id_mensagemcobranca;
    }

    public function setId_mensagem(Mensagem $id_mensagem) {
        $this->id_mensagem = $id_mensagem;
    }

    public function setId_lancamento(Lancamento $id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

}

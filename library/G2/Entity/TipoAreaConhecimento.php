<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipoareaconhecimento")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
use G2\G2Entity;

class TipoAreaConhecimento extends G2Entity {

    /**
     *
     * @var integer $id_tipoareaconhecimento
     * @Column(name="id_tipoareaconhecimento", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoareaconhecimento;

    /**
     * @Column(type="string",length=100,nullable=false)
     * @var string $st_tipoareaconhecimento
     */
    private $st_tipoareaconhecimento;

    /**
     * @Column(type="string",length=300,nullable=false)
     * @var string
     */
    private $st_descricao;



    public function setId_tipoareaconhecimento($id_tipoareaconhecimento)
    {
        $this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
    }

    public function getId_tipoareaconhecimento()
    {
        return $this->id_tipoareaconhecimento;
    }


    public function setSt_tipoareaconhecimento($st_tipoareaconhecimento)
    {
        $this->st_tipoareaconhecimento = $st_tipoareaconhecimento;
    }

    public function getSt_tipoareaconhecimento()
    {
        return $this->st_tipoareaconhecimento;
    }

    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    public function getSt_descricao()
    {
        return $this->st_descricao;
    }



}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_modulo")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Modulo
{

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_modulo;

    /**
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     * @var string $st_modulo
     */
    private $st_modulo;

    /**
     * @Column(name="st_tituloexibicao", type="string", nullable=false, length=255)
     * @var string $st_tituloexibicao
     */
    private $st_tituloexibicao;

    /**
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     * @Column(name="st_descricao", type="string", nullable=true, length=500)
     * @var string $st_descricao
     */
    private $st_descricao;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @Column(name="id_moduloanterior", type="integer", nullable=true)
     * @var integer $id_moduloanterior
     */
    private $id_moduloanterior;

    /**
     * @return int
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @return string
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return mixed
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_moduloanterior()
    {
        return $this->id_moduloanterior;
    }

    /**
     * @param $id_modulo
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
    }

    /**
     * @param $st_modulo
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
    }

    /**
     * @param $st_tituloexibicao
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    /**
     * @param $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @param $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param $id_moduloanterior
     */
    public function setId_moduloanterior($id_moduloanterior)
    {
        $this->id_moduloanterior = $id_moduloanterior;
    }


}

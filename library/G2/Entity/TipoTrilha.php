<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipotrilha")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */

class TipoTrilha
{

    /**
     * @var integer $id_tipotrilha
     * @Column(type="integer", nullable=false, name="id_tipotrilha")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipotrilha;
    /**
     * @var string $st_descricao
     * @Column(name="st_descricao",type="string", nullable=false, length=2500)
     */
    private $st_descricao;
       /**
     * @var string $st_tipotrilha
     * @Column(name="st_tipotrilha",type="string", nullable=false, length=255)
     */
    private $st_tipotrilha;
    
    public function getId_tipotrilha() {
        return $this->id_tipotrilha;
    }

    public function setId_tipotrilha($id_tipotrilha) {
        $this->id_tipotrilha = $id_tipotrilha;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
    }

    public function getSt_tipotrilha() {
        return $this->st_tipotrilha;
    }

    public function setSt_tipotrilha($st_tipotrilha) {
        $this->st_tipotrilha = $st_tipotrilha;
    }



}
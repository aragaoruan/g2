<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeboletoconfig")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class EntidadeBoletoConfig
{

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_boletoconfig
     * @Column(name="id_boletoconfig", type="integer", nullable=false, length=4)
     */
    private $id_boletoconfig;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_entidadefinanceiro
     * @Column(name="id_entidadefinanceiro", type="integer", nullable=false, length=4)
     */
    private $id_entidadefinanceiro;

    public function setId_boletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
        return $this;
    }

    public function getId_boletoconfig()
    {
        return $this->id_boletoconfig;
    }

    public function setId_entidadefinanceiro($id_entidadefinanceiro)
    {
        $this->id_entidadefinanceiro = $id_entidadefinanceiro;
        return $this;
    }

    public function getId_entidadefinanceiro()
    {
        return $this->id_entidadefinanceiro;
    }
}
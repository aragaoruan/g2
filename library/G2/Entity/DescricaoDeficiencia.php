<?php

namespace G2\Entity;
use G2\G2Entity;

/** ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_descricaodeficiencia")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class DescricaoDeficiencia extends G2Entity {

    /**
     * @var integer $id_descricaodeficiencia
     * @Column(name="id_descricaodeficiencia", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_descricaodeficiencia;
    /**
     * @id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;
    /**
     * @var string $st_descricaodeficiencia
     * @Column(name="st_descricaodeficiencia", type="string", nullable=true, length=200)
     */
    private $st_descricaodeficiencia;

    /**
     * @return int
     */
    public function getId_descricaodeficiencia()
    {
        return $this->id_descricaodeficiencia;
    }

    /**
     * @param int $id_descricaodeficiencia
     */
    public function setId_descricaodeficiencia($id_descricaodeficiencia)
    {
        $this->id_descricaodeficiencia = $id_descricaodeficiencia;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return string
     */
    public function getSt_descricaodeficiencia()
    {
        return $this->st_descricaodeficiencia;
    }

    /**
     * @param string $st_descricaodeficiencia
     */
    public function setSt_descricaodeficiencia($st_descricaodeficiencia)
    {
        $this->st_descricaodeficiencia = $st_descricaodeficiencia;
    }

}
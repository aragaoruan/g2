<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_situacao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
use G2\G2Entity;

class Situacao extends G2Entity
{

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_tabela 
     * @Column(name="st_tabela", type="string", nullable=false, length=255)
     */
    private $st_tabela;

    /**
     * @var string $st_campo 
     * @Column(name="st_campo", type="string", nullable=false, length=255)
     */
    private $st_campo;

    /**
     * @var string $st_descricaosituacao 
     * @Column(name="st_descricaosituacao", type="string", nullable=false, length=8000)
     */
    private $st_descricaosituacao;

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getSt_tabela ()
    {
        return $this->st_tabela;
    }

    public function setSt_tabela ($st_tabela)
    {
        $this->st_tabela = $st_tabela;
        return $this;
    }

    public function getSt_campo ()
    {
        return $this->st_campo;
    }

    public function setSt_campo ($st_campo)
    {
        $this->st_campo = $st_campo;
        return $this;
    }

    public function getSt_descricaosituacao ()
    {
        return $this->st_descricaosituacao;
    }

    public function setSt_descricaosituacao ($st_descricaosituacao)
    {
        $this->st_descricaosituacao = $st_descricaosituacao;
        return $this;
    }

}
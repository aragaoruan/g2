<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_saladisciplinaentidade")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since: 2015-10-26
 */
class VwSalaDisciplinaEntidade
{

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false)
     */
    private $st_disciplina;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false)
     */
    private $st_saladeaula;

    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true, length=8)
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=8)
     */
    private $dt_fiminscricao;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=8)
     */
    private $dt_abertura;


    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=8)
     */
    private $dt_encerramento;


    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=true)
     */
    private $st_professor;

    /**
     * @var integer $bl_conteudo
     * @Column(name="bl_conteudo", type="integer", nullable=true)
     */
    private $bl_conteudo;

    /**
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=true)
     */
    private $st_sistema;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true)
     */
    private $id_periodoletivo;


    public function setBl_conteudo($bl_conteudo)
    {
        $this->bl_conteudo = $bl_conteudo;
    }

    public function getBl_conteudo()
    {
        return $this->bl_conteudo;
    }

    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    public function getSt_professor()
    {
        return $this->st_professor;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
    }

    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
    }

}
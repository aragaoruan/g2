<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoprova")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoProva
{

    /**
     *
     * @var integer $id_tipoprova 
     * @Column(name="id_tipoprova", type="integer", nullable=false)
     * @ID
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoprova;

    /**
     *
     * @var string $st_tipoprova 
     * @Column(name="st_tipoprova", type="string", nullable=false, length=255)
     */
    private $st_tipoprova;

    /**
     *
     * @var string $st_descricao 
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    public function getId_tipoprova ()
    {
        return $this->id_tipoprova;
    }

    public function setId_tipoprova ($id_tipoprova)
    {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    public function getSt_tipoprova ()
    {
        return $this->st_tipoprova;
    }

    public function setSt_tipoprova ($st_tipoprova)
    {
        $this->st_tipoprova = $st_tipoprova;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
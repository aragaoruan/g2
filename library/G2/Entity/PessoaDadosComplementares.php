<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pessoadadoscomplementares")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class PessoaDadosComplementares extends G2Entity
{
    /**
     * @Id
     * @var integer $id_pessoadadoscomplementares
     * @Column(name="id_pessoadadoscomplementares", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pessoadadoscomplementares;
    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario");
     */
    private $id_usuario;
    /**
     * @var string $dt_anoconclusao
     * @Column(name="dt_anoconclusao", type="string", nullable=true, length=4)
     */
    private $dt_anoconclusao;
    /**
     * @ManyToOne(targetEntity="TipoEscola")
     * @JoinColumn(name="id_tipoescola", referencedColumnName="id_tipoescola")
     */
    private $id_tipoescola;
    /**
     * @var integer $id_formaingresso
     * @Column(name="id_formaingresso", type="integer", nullable=true, length=4)
     */
    private $id_formaingresso;
    /**
     * @var integer $id_etnia
     * @Column(name="id_etnia", type="integer", nullable=true, length=4)
     */
    private $id_etnia;
    /**
     * @var integer $id_pessoacomdeficiencia
     * @Column(name="id_pessoacomdeficiencia", type="integer", nullable=true, length=4)
     */
    private $id_pessoacomdeficiencia;
    /**
     * @var string $st_nomeinstituicao
     * @Column(name="st_nomeinstituicao", type="string", nullable=true, length=140)
     */
    private $st_nomeinstituicao;

    /**
     * @var string $st_descricaodeficiencia
     * @Column(name="st_descricaodeficiencia", type="string", nullable=true, length=200)
     */
    private $st_descricaodeficiencia;

    /**
     * @var string $dt_ingresso
     * @Column(name="dt_ingresso", type="string", nullable=true, length=10)
     */
    private $dt_ingresso;


    /**
     * @var string $st_curso
     * @Column(name="st_curso", type="string", nullable=true, length=50)
     */
    private $st_curso;

    /**
     * @var char $sg_ufinstituicao
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_ufinstituicao", referencedColumnName="sg_uf")
     */
    private $sg_ufinstituicao;

    /**
     * @var numeric $id_municipioinstituicao
     * @ManyToOne(targetEntity="Municipio")
     * @JoinColumn(name="id_municipioinstituicao", referencedColumnName="id_municipio")
     */
    private $id_municipioinstituicao;



    /**
     * @return string
     */
    public function getSt_curso()
    {
        return $this->st_curso;
    }

    /**
     * @param string $st_curso
     */
    public function setSt_curso($st_curso)
    {
        $this->st_curso = $st_curso;
    }

    /**
     * @return string
     */
    public function getDt_ingresso()
    {
        return $this->dt_ingresso;
    }

    /**
     * @param string $dt_ingresso
     */
    public function setDt_ingresso($dt_ingresso)
    {
        $this->dt_ingresso = $dt_ingresso;
    }

    /**
     * @return string
     */
    public function getSt_descricaodeficiencia()
    {
        return $this->st_descricaodeficiencia;
    }

    /**
     * @param string $st_descricaodeficiencia
     */
    public function setSt_descricaodeficiencia($st_descricaodeficiencia)
    {
        $this->st_descricaodeficiencia = $st_descricaodeficiencia;
    }

    /**
     * @return int
     */
    public function getId_pessoadadoscomplementares()
    {
        return $this->id_pessoadadoscomplementares;
    }

    /**
     * @param int $id_pessoadadoscomplementares
     */
    public function setId_pessoadadoscomplementares($id_pessoadadoscomplementares)
    {
        $this->id_pessoadadoscomplementares = $id_pessoadadoscomplementares;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_tipoescola()
    {
        return $this->id_tipoescola;
    }

    /**
     * @param int $id_tipoescola
     */
    public function setId_tipoescola($id_tipoescola)
    {
        $this->id_tipoescola = $id_tipoescola;
    }

    /**
     * @return int
     */
    public function getId_formaingresso()
    {
        return $this->id_formaingresso;
    }

    /**
     * @param int $id_formaingresso
     */
    public function setId_formaingresso($id_formaingresso)
    {
        $this->id_formaingresso = $id_formaingresso;
    }

    /**
     * @return int
     */
    public function getId_etnia()
    {
        return $this->id_etnia;
    }

    /**
     * @param int $id_etnia
     */
    public function setId_etnia($id_etnia)
    {
        $this->id_etnia = $id_etnia;
    }

    /**
     * @return int
     */
    public function getId_pessoacomdeficiencia()
    {
        return $this->id_pessoacomdeficiencia;
    }

    /**
     * @param int $id_pessoacomdeficiencia
     */
    public function setId_pessoacomdeficiencia($id_pessoacomdeficiencia)
    {
        $this->id_pessoacomdeficiencia = $id_pessoacomdeficiencia;
    }

    /**
     * @return string
     */
    public function getSt_nomeinstituicao()
    {
        return $this->st_nomeinstituicao;
    }

    /**
     * @param string $st_nomeinstituicao
     */
    public function setSt_nomeinstituicao($st_nomeinstituicao)
    {
        $this->st_nomeinstituicao = $st_nomeinstituicao;
    }

    /**
     * @return char
     */
    public function getDt_anoconclusao()
    {
        return $this->dt_anoconclusao;
    }

    /**
     * @param char $dt_anoconclusao
     */
    public function setDt_anoconclusao($dt_anoconclusao)
    {
        $this->dt_anoconclusao = $dt_anoconclusao;
    }

    /**
     * @return char
     */
    public function getSg_ufinstituicao()
    {
        return $this->sg_ufinstituicao;
    }

    /**
     * @param char $sg_uf_instituicao
     */
    public function setSg_ufinstituicao($sg_ufinstituicao)
    {
        $this->sg_ufinstituicao = $sg_ufinstituicao;
    }

    /**
     * @return numeric
     */
    public function getId_municipioinstituicao()
    {
        return $this->id_municipioinstituicao;
    }

    /**
     * @param numeric $id_municipioinstituicao
     */
    public function setId_municipioinstituicao($id_municipioinstituicao)
    {
        $this->id_municipioinstituicao = $id_municipioinstituicao;
    }



}

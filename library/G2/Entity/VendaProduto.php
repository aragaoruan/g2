<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_vendaproduto")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @EntityLog
 */
class VendaProduto
{

    /**
     * @Id
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_vendaproduto;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2", nullable=true, length=8)
     */
    private $dt_entrega;

    /**
     * @var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @var Venda $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     * @abstract \G2\Entity\Venda
     */
    private $id_venda;

    /**
     * @var TipoSelecao $id_tiposelecao
     * @ManyToOne(targetEntity="TipoSelecao")
     * @JoinColumn(name="id_tiposelecao", referencedColumnName="id_tiposelecao")
     * @abstract \G2\Entity\TipoSelecao
     */
    private $id_tiposelecao;

    /**
     * @var CampanhaComercial $id_campanhacomercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhacomercial;

    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var ProdutoCombo $id_produtocombo
     * @ManyToOne(targetEntity="ProdutoCombo")
     * @JoinColumn(name="id_produtocombo", referencedColumnName="id_produtocombo")
     */
    private $id_produtocombo;

    /**
     * @var float $nu_desconto
     * @Column(name="nu_desconto", type="float", nullable=true, length=17)
     */
    private $nu_desconto;

    /**
     * @var float $nu_valorliquido
     * @Column(name="nu_valorliquido", type="float", nullable=true, length=17)
     */
    private $nu_valorliquido;

    /**
     * @var float $nu_valorbruto
     * @Column(name="nu_valorbruto", type="float", nullable=true, length=17)
     */
    private $nu_valorbruto;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $dt_ingresso
     * @Column(name="dt_ingresso", type="string", nullable=true)
     */
    private $dt_ingresso;

    /**
     * @return TipoSelecao
     */
    public function getId_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param $id_tiposelecao
     * @return $this
     */
    public function setId_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
        return $this;

    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    public function getId_produto()
    {
        return $this->id_produto;
    }

    public function setId_produto(Produto $id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getId_venda()
    {
        return $this->id_venda;
    }

    public function setId_venda(Venda $id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getId_turma()
    {
        return $this->id_turma;
    }

    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    public function getId_produtocombo()
    {
        return $this->id_produtocombo;
    }

    public function setId_produtocombo($id_produtocombo)
    {
        $this->id_produtocombo = $id_produtocombo;
        return $this;
    }

    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    public function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    public function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
        return $this;
    }

    public function getDt_entrega()
    {
        return $this->dt_entrega;
    }

    public function setDt_entrega(\DateTime $dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
        return $this;
    }

    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return string
     */
    public function getDt_ingresso()
    {
        return $this->dt_ingresso;
    }

    /**
     * @param string $dt_ingresso
     */
    public function setDt_ingresso($dt_ingresso)
    {
        $this->dt_ingresso = $dt_ingresso;
    }

}

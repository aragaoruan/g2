<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtoimagem")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class ProdutoImagem
{

    /**
     *
     * @var integer $id_produtoimagem
     * @Column(name="id_produtoimagem", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtoimagem;

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     *
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=false)
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", nullable=false, referencedColumnName="id_upload")
     */
    private $id_upload;
    
    
    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;
    
    
    public function getId_produtoimagem() {
        return $this->id_produtoimagem;
    }

    public function setId_produtoimagem($id_produtoimagem) {
        $this->id_produtoimagem = $id_produtoimagem;
        return $this;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getId_upload() {
        return $this->id_upload;
    }

    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
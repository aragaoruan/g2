<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_contatosemail")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class ContatosEmail
{

    /**
     * @var integer $id_email
     * @Column(name="id_email", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_email;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="text", nullable=true)
     */
    private $st_email;

    public function getId_email()
    {
        return $this->id_email;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function setId_email($id_email)
    {
        $this->id_email = $id_email;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

}

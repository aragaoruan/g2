<?php

/*
 * Entity OcorrenciaAcompanhante
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-15
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_ocorrenciaacompanhante")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class OcorrenciaAcompanhante {

    /**
     *
     * @var integer $id_ocorrenciaacompanhante
     * @Column(name="id_ocorrenciaacompanhante", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_ocorrenciaacompanhante;


    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer",nullable=false)
     */
    private $id_ocorrencia;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer",nullable=false)
     */
    private $id_usuario;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    public function getId_ocorrenciaacompanhante() {
        return $this->id_ocorrenciaacompanhante;
    }

    public function getId_ocorrencia() {
        return $this->id_ocorrencia;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_ocorrenciaacompanhante($id_ocorrenciaacompanhante) {
        $this->id_ocorrenciaacompanhante = $id_ocorrenciaacompanhante;
    }

    public function setId_ocorrencia($id_ocorrencia) {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }


}

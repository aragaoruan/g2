<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_aplicadorendereco")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAplicadorEndereco {

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer")
     * @Id
     */
    private $id_aplicadorprova;
    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string")
     */
    private $st_aplicadorprova;
    /**
     * @var integer $id_aplicadorpessoafisica
     * @Column(name="id_aplicadorpessoafisica", type="integer")
     */
    private $id_aplicadorpessoafisica;
      /**
     * @var string $st_nomepessoafisica
     * @Column(name="st_nomepessoafisica", type="string")
     */
    private $st_nomepessoafisica;
    /**
     * @var integer $id_aplicadorpessoajuridico
     * @Column(name="id_aplicadorpessoajuridico", type="integer")
     */
    private $id_aplicadorpessoajuridico;
     /**
     * @var string $st_nomepessoajuridica
     * @Column(name="st_nomepessoajuridica", type="string")
     */
    private $st_nomepessoajuridica;
    /**
     * @var integer $id_entidaderelacionada
     * @Column(name="id_entidaderelacionada", type="integer")
     */
    private $id_entidaderelacionada;
      /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer")
     */
    private $id_entidadecadastro;
       /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer")
     */
    private $id_endereco;
      /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string")
     */
    private $st_endereco;
     /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string")
     */
    private $sg_uf;
       /**
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer")
     */
    private $id_municipio;

    /**
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     * @Column(name="id_entidadeaplicacao", type="integer", nullable=true)
     * @var integer $id_entidadeaplicacao
     */
    private $id_entidadeaplicacao;


    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    public function getSt_aplicadorprova() {
        return $this->st_aplicadorprova;
    }

    public function getId_aplicadorpessoafisica() {
        return $this->id_aplicadorpessoafisica;
    }

    public function getSt_nomepessoafisica() {
        return $this->st_nomepessoafisica;
    }

    public function getId_aplicadorpessoajuridico() {
        return $this->id_aplicadorpessoajuridico;
    }

    public function getSt_nomepessoajuridica() {
        return $this->st_nomepessoajuridica;
    }

    public function getId_entidaderelacionada() {
        return $this->id_entidaderelacionada;
    }

    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function getSt_endereco() {
        return $this->st_endereco;
    }

    public function getSg_uf() {
        return $this->sg_uf;
    }

    public function getId_municipio() {
        return $this->id_municipio;
    }

    public function getId_entidadeaplicacao() {
        return $this->id_entidadeaplicacao;
    }

    public function setId_aplicadorprova($id_aplicadorprova) {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function setSt_aplicadorprova($st_aplicadorprova) {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    public function setId_aplicadorpessoafisica($id_aplicadorpessoafisica) {
        $this->id_aplicadorpessoafisica = $id_aplicadorpessoafisica;
    }

    public function setSt_nomepessoafisica($st_nomepessoafisica) {
        $this->st_nomepessoafisica = $st_nomepessoafisica;
    }

    public function setId_aplicadorpessoajuridico($id_aplicadorpessoajuridico) {
        $this->id_aplicadorpessoajuridico = $id_aplicadorpessoajuridico;
    }

    public function setSt_nomepessoajuridica($st_nomepessoajuridica) {
        $this->st_nomepessoajuridica = $st_nomepessoajuridica;
    }

    public function setId_entidaderelacionada($id_entidaderelacionada) {
        $this->id_entidaderelacionada = $id_entidaderelacionada;
    }

    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }

    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }

    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }



}

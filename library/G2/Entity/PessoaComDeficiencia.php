<?php

namespace G2\Entity;
use G2\G2Entity;

/** ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_pessoacomdeficiencia")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class PessoaComDeficiencia extends G2Entity {

    /**
     * @Id
     * @var integer $id_pessoacomdeficiencia
     * @Column(name="id_pessoacomdeficiencia", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pessoacomdeficiencia;
    /**
     * @var string $st_pessoacomdeficiencia
     * @Column(name="st_pessoacomdeficiencia", type="string", nullable=false, length=50)
     */
    private $st_pessoacomdeficiencia;

    public function __toString() {
        return strval($this->id_pessoacomdeficiencia);
    }

    /**
     * @return int
     */
    public function getId_pessoacomdeficiencia()
    {
        return $this->id_pessoacomdeficiencia;
    }

    /**
     * @param int $id_pessoacomdeficiencia
     */
    public function setId_pessoacomdeficiencia($id_pessoacomdeficiencia)
    {
        $this->id_pessoacomdeficiencia = $id_pessoacomdeficiencia;
    }

    /**
     * @return string
     */
    public function getSt_pessoacomdeficiencia()
    {
        return $this->st_pessoacomdeficiencia;
    }

    /**
     * @param string $st_pessoacomdeficiencia
     */
    public function setSt_pessoacomdeficiencia($st_pessoacomdeficiencia)
    {
        $this->st_pessoacomdeficiencia = $st_pessoacomdeficiencia;
    }

}
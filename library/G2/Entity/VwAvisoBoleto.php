<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avisoboleto")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwAvisoBoleto extends G2Entity {

    /**
     * @Id
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumn(name="id_lancamento", referencedColumnName="id_lancamento")
     */
    private $id_lancamento;

    /**
     * @Id
     * @ManyToOne(targetEntity="MensagemCobranca")
     * @JoinColumn(name="id_mensagemcobranca", referencedColumnName="id_mensagemcobranca")
     */
    private $id_mensagemcobranca;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    public function getId_mensagemcobranca() {
        return $this->id_mensagemcobranca;
    }

    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    public function setId_mensagemcobranca($id_mensagemcobranca) {
        $this->id_mensagemcobranca = $id_mensagemcobranca;
    }

    public function setId_textosistema(TextoSistema $id_textosistema) {
        $this->id_textosistema = $id_textosistema;
    }

    public function setId_entidade(Entidade $id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_itemativacaoentidade")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-20
 */
class VwItemAtivacaoEntidade {

    /**
     * @var integer $id_itemativacao
     * @Column(name="id_itemativacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemativacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_itemativacaoentidade
     * @Column(name="id_itemativacaoentidade", type="integer", nullable=true, length=4)
     */
    private $id_itemativacaoentidade;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_usuarioatualizacao
     * @Column(name="id_usuarioatualizacao", type="integer", nullable=true, length=4)
     */
    private $id_usuarioatualizacao;
    /**
     * @var datetime $dt_atualizacao
     * @Column(name="dt_atualizacao", type="datetime", nullable=true, length=8)
     */
    private $dt_atualizacao;
    /**
     * @var string $st_itemativacao
     * @Column(name="st_itemativacao", type="string", nullable=true, length=150)
     */
    private $st_itemativacao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true)
     */
    private $st_descricao;
    /**
     * @var string $st_responsavel
     * @Column(name="st_responsavel", type="string", nullable=true, length=50)
     */
    private $st_responsavel;
    /**
     * @var string $st_caminhosistema
     * @Column(name="st_caminhosistema", type="string", nullable=true, length=250)
     */
    private $st_caminhosistema;

    /**
     * @var integer $id_departamento
     * @Column(name="id_departamento", type="integer", nullable=true, length=4)
     */
    private $id_departamento;

    /**
     * @return integer id_itemativacao
     */
    public function getId_itemativacao() {
        return $this->id_itemativacao;
    }

    /**
     * @return int
     */
    public function getid_departamento()
    {
        return $this->id_departamento;
    }

    /**
     * @param int $id_departamento
     */
    public function setid_departamento($id_departamento)
    {
        $this->id_departamento = $id_departamento;
    }


    /**
     * @param id_itemativacao
     */
    public function setId_itemativacao($id_itemativacao) {
        $this->id_itemativacao = $id_itemativacao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_itemativacaoentidade
     */
    public function getId_itemativacaoentidade() {
        return $this->id_itemativacaoentidade;
    }

    /**
     * @param id_itemativacaoentidade
     */
    public function setId_itemativacaoentidade($id_itemativacaoentidade) {
        $this->id_itemativacaoentidade = $id_itemativacaoentidade;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_usuarioatualizacao
     */
    public function getId_usuarioatualizacao() {
        return $this->id_usuarioatualizacao;
    }

    /**
     * @param id_usuarioatualizacao
     */
    public function setId_usuarioatualizacao($id_usuarioatualizacao) {
        $this->id_usuarioatualizacao = $id_usuarioatualizacao;
        return $this;
    }

    /**
     * @return datetime dt_atualizacao
     */
    public function getDt_atualizacao() {
        return $this->dt_atualizacao;
    }

    /**
     * @param dt_atualizacao
     */
    public function setDt_atualizacao($dt_atualizacao) {
        $this->dt_atualizacao = $dt_atualizacao;
        return $this;
    }

    /**
     * @return string st_itemativacao
     */
    public function getSt_itemativacao() {
        return $this->st_itemativacao;
    }

    /**
     * @param st_itemativacao
     */
    public function setSt_itemativacao($st_itemativacao) {
        $this->st_itemativacao = $st_itemativacao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_descricao
     */
    public function getSt_descricao() {
        return $this->st_descricao;
    }

    /**
     * @param st_descricao
     */
    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return string st_responsavel
     */
    public function getSt_responsavel() {
        return $this->st_responsavel;
    }

    /**
     * @param st_responsavel
     */
    public function setSt_responsavel($st_responsavel) {
        $this->st_responsavel = $st_responsavel;
        return $this;
    }

    /**
     * @return string st_caminhosistema
     */
    public function getSt_caminhosistema() {
        return $this->st_caminhosistema;
    }

    /**
     * @param st_caminhosistema
     */
    public function setSt_caminhosistema($st_caminhosistema) {
        $this->st_caminhosistema = $st_caminhosistema;
        return $this;
    }

}

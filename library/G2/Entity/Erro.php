<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="G2\Repository\Erro")
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_erro")
 */
class Erro {

    /**
     * @var integer $id_erro
     * @id
     * @Column(name="id_erro", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_erro;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $sistema;

    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false)
     */
    private $st_mensagem;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var string $st_origem
     * @Column(name="st_origem", type="string", nullable=false)
     */
    private $st_origem;

    /**
     * @var string $st_codigo
     * @Column(name="st_codigo", type="string", nullable=true)
     */
    private $st_codigo;

    /**
     * @var boolean $bl_lido
     * @Column(name="bl_lido", type="boolean", nullable=false, options={"default"=0})
     */
    private $bl_lido;

    /**
     * @var boolean $bl_corrigido
     * @Column(name="bl_corrigido", type="boolean", nullable=false, options={"default"=0})
     */
    private $bl_corrigido;

    /**
     * @var Processo
     *
     * @ManyToOne(targetEntity="Processo", cascade={"persist", "remove"})
     * @JoinColumn(name="id_processo", referencedColumnName="id_processo")
     */
    private $processo;

    /**
     * @param boolean $bl_corrigido
     */
    public function setBlCorrigido($bl_corrigido)
    {
        $this->bl_corrigido = $bl_corrigido;
    }

    /**
     * @return boolean
     */
    public function getBlCorrigido()
    {
        return $this->bl_corrigido;
    }

    /**
     * @param boolean $bl_lido
     */
    public function setBlLido($bl_lido)
    {
        $this->bl_lido = $bl_lido;
    }

    /**
     * @return boolean
     */
    public function getBlLido()
    {
        return $this->bl_lido;
    }

    /**
     * @param \G2\Entity\datetime $dt_cadastro
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_erro
     */
    public function setIdErro($id_erro)
    {
        $this->id_erro = $id_erro;
    }

    /**
     * @return int
     */
    public function getIdErro()
    {
        return $this->id_erro;
    }

    /**
     * @param int $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param \G2\Entity\Processo $processo
     */
    public function setProcesso(\G2\Entity\Processo $processo)
    {
        $this->processo = $processo;
    }

    /**
     * @return \G2\Entity\Processo
     */
    public function getProcesso()
    {
        return $this->processo;
    }

    /**
     * @param \G2\Entity\Sistema $sistema
     */
    public function setSistema(\G2\Entity\Sistema $sistema)
    {
        $this->sistema = $sistema;
    }

    /**
     * @return \G2\Entity\Sistema
     */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
     * @param string $st_codigo
     */
    public function setStCodigo($st_codigo)
    {
        $this->st_codigo = $st_codigo;
    }

    /**
     * @return string
     */
    public function getStCodigo()
    {
        return $this->st_codigo;
    }

    /**
     * @param string $st_mensagem
     */
    public function setStMensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @return string
     */
    public function getStMensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @param string $st_origem
     */
    public function setStOrigem($st_origem)
    {
        $this->st_origem = $st_origem;
    }

    /**
     * @return string
     */
    public function getStOrigem()
    {
        return $this->st_origem;
    }

    /**
     *
     * @PrePersist()
     */
    public function prePersist(){
        if (is_null($this->getDtCadastro())){
            $this->setDtCadastro(new \DateTime());
        }

        if (is_null($this->getBlLido())){
            $this->setBlLido(0);
        }

        if (is_null($this->getBlCorrigido())){
            $this->setBlCorrigido(0);
        }
    }
}
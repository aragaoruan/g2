<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_professordisciplina")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwProfessorDisciplina
{

    /**
     * @Column(type="integer")
     * @Id
     */
    private $id_usuario;

    /**
     * @Column(type="string")
     */
    private $st_nomecompleto;

    /**
     * @Column(type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @Column(type="integer")
     */
    private $id_entidade;

    /**
     * @Column(type="integer")
     */
    private $id_disciplina;

    /**
     * @param integer $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return integer
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }


    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return mixed
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return mixed
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return mixed
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }
}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * Class Entity para Carta de Credito
 * @Entity
 * @Table(name="tb_cartacredito")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-13
 */
class CartaCredito
{

    /**
     * @var integer
     * @Column(type="integer", nullable=false, name="id_cartacredito")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cartacredito;

    /**
     * @var decimal
     * @Column(type="decimal", length=9, nullable=false, name="nu_valororiginal")
     */
    private $nu_valororiginal;

    /**
     * @var datetime2
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Cancelamento
     * @ManyToOne(targetEntity="Cancelamento")
     * @JoinColumn(name="id_cancelamento", referencedColumnName="id_cancelamento")
     */
    private $id_cancelamento;

    /**
     * @var boolean
     * @Column(type="boolean", nullable=false, name="bl_ativo")
     */
    private $bl_ativo;

    /**
     * @var Entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var date
     * @Column (type="datetime2", nullable=false, name="dt_validade")
     */
    private $dt_validade;

    /**
     * @return integer
     */
    public function getId_cartacredito()
    {
        return $this->id_cartacredito;
    }

    /**
     * @return decimal
     */
    public function getNu_valororiginal()
    {
        return $this->nu_valororiginal;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param integer $id_cartacredito
     * @return $this
     */
    public function setId_cartacredito($id_cartacredito)
    {
        $this->id_cartacredito = $id_cartacredito;
        return $this;
    }

    /**
     * @param decimal $nu_valororiginal
     * @return $this
     */
    public function setNu_valororiginal($nu_valororiginal)
    {
        $this->nu_valororiginal = $nu_valororiginal;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param Situacao $id_situacao
     * @return $this
     */
    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param Usuario $id_usuario
     * @return $this
     */
    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return Cancelamento
     */
    public function getId_Cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param Cancelamento $id_cancelamento
     */
    public function setId_Cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
    }

    /**
     * @return Entidade
     */
    public function getId_Entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     */
    public function setId_Entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getDt_validade()
    {
        return $this->dt_validade;
    }

    public function setDt_validade($dt_validade)
    {
        $this->dt_validade = $dt_validade;
    }

}
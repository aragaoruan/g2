<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15/04/2015
 * Time: 13:56
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_itemdematerialturma")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class ItemDeMaterialTurma {

    /**
     * @Id
     * @var integer $id_itemdematerialturma
     * @Column(name="id_itemdematerialturma", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemdematerialturma;
    /**
     * @var integer $id_material
     * @ManyToOne(targetEntity="ItemDeMaterial")
     * @JoinColumn(name="id_material", nullable=false, referencedColumnName="id_itemdematerial")
     */
    private $id_material;
    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", nullable=false, referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;


    /**
     * @return integer id_itemdematerialturma
     */
    public function getId_itemdematerialturma() {
        return $this->id_itemdematerialturma;
    }

    /**
     * @param id_itemdematerialturma
     */
    public function setId_itemdematerialturma($id_itemdematerialturma) {
        $this->id_itemdematerialturma = $id_itemdematerialturma;
        return $this;
    }

    /**
     * @return integer id_material
     */
    public function getId_material() {
        return $this->id_material;
    }

    /**
     * @param id_material
     */
    public function setId_material($id_material) {
        $this->id_material = $id_material;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

}
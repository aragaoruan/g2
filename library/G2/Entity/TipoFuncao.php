<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipofuncao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class TipoFuncao
{

    /**
     * @var integer $id_tipofuncao
     * @Column(name="id_tipofuncao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipofuncao;

    /**
     * @var string $st_tipofuncao
     * @Column(name="st_tipofuncao", type="string", nullable=false, length=150)
     */
    private $st_tipofuncao;


    public function setId_tipofuncao($id_tipofuncao)
    {
        $this->id_tipofuncao = $id_tipofuncao;
    }


    public function getId_tipofuncao()
    {
        return $this->id_tipofuncao;
    }


    public function setSt_tipofuncao($st_tipofuncao)
    {
        $this->st_tipofuncao = $st_tipofuncao;
    }


    public function getSt_tipofuncao()
    {
        return $this->st_tipofuncao;
    }





}
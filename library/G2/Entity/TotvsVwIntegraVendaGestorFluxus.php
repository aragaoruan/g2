<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity(repositoryClass="G2\Repository\TotvsVwIntegraVendaGestorFluxus")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="totvs.vw_Integra_Venda_GestorFluxus")
 */
class TotvsVwIntegraVendaGestorFluxus
{
    /**
     * @var smallint $codcoligada_fcfo
     * @Id
     * @Column(name="CODCOLIGADA_FCFO", type="smallint", length=2)
     */
    private $codcoligada_fcfo;

    /**
     *
     * @var string $codcfo_fcfo
     * @Column(name="CODCFO_FCFO", type="string", length=25)
     */
    private $codcfo_fcfo;

    /**
     * @var string $nomefantasia_fcfo
     * @Column(name="NOMEFANTASIA_FCFO", type="string", length=60)
     */
    private $nomefantasia_fcfo;

    /**
     * @var string $nome_fcfo
     * @Column(name="NOME_FCFO", type="string", length=60)
     */
    private $nome_fcfo;

    /**
     * @var string $cgccfo_fcfo
     * @Column(name="CGCCFO_FCFO", type="string", length=20)
     */
    private $cgccfo_fcfo;

    /**
     * @var string $inscrestadual_fcfo
     * @Column(name="INSCRESTADUAL_FCFO", type="string", length=20)
     */
    private $inscrestadual_fcfo;

    /**
     * @var string $pagrec_fcfo
     * @Column(name="PAGREC_FCFO", type="string", length=1)
     */
    private $pagrec_fcfo;

    /**
     * @var string $tipo_rua
     * @Column(name="TIPORUA", type="string", length=20)
     */
    private $tipo_rua;

    /**
     * @var string $rua_fcfo
     * @Column(name="RUA_FCFO", type="string", length=100)
     */
    private $rua_fcfo;

    /**
     * @var string $numero_fcfo
     * @Column(name="NUMERO_FCFO", type="string", length=8)
     */
    private $numero_fcfo;

    /**
     * @var string $complemento_fcfo
     * @Column(name="COMPLEMENTO_FCFO", type="string", length=30)
     */
    private $complemento_fcfo;

    /**
     * @var string $tipobairro_fcfo
     * @Column(name="TIPOBAIRRO_FCFO", type="integer")
     */
    private $tipobairro_fcfo;

    /**
     * @var string $bairro_fcfo
     * @Column(name="BAIRRO_FCFO", type="string", length=30)
     */
    private $bairro_fcfo;

    /**
     * @var string $cidade_fcfo
     * @Column(name="CIDADE_FCFO", type="string", length=32)
     */
    private $cidade_fcfo;

    /**
     * @var string $codetd_fcfo
     * @Column(name="CODETD_FCFO", type="string", length=2)
     */
    private $codetd_fcfo;

    /**
     * @var string $cep_fcfo
     * @Column(name="CEP_FCFO", type="string", length=9)
     */
    private $cep_fcfo;

    /**
     * @var string $telefone_fcfo
     * @Column(name="TELEFONE_FCFO", type="string", length=15)
     */
    private $telefone_fcfo;

    /**
     * @var string $fax_fcfo
     * @Column(name="FAX_FCFO", type="string", length=15)
     */
    private $fax_fcfo;

    /**
     * @var string $email_fcfo
     * @Column(name="EMAIL_FCFO", type="string", length=60)
     */
    private $email_fcfo;

    /**
     * @var string $contato_fcfo
     * @Column(name="CONTATO_FCFO", type="string", length=40)
     */
    private $contato_fcfo;

    /**
     * @var string $ativo_fcfo
     * @Column(name="ATIVO_FCFO", type="string", length=1)
     */
    private $ativo_fcfo;

    /**
     * @var string $codmunicipio_fcfo
     * @Column(name="CODMUNICIPIO_FCFO", type="string", length=20)
     */
    private $codmunicipio_fcfo;

    /**
     * @var string $pessoafisoujur_fcfo
     * @Column(name="PESSOAFISOUJUR_FCFO", type="string", length=1)
     */
    private $pessoafisoujur_fcfo;

    /**
     * @var string $idpais_fcfo
     * @Column(name="IDPAIS_FCFO", type="string", length=20)
     */
    private $idpais_fcfo;

    /**
     * @var string $pais_fcfo
     * @Column(name="PAIS_FCFO", type="string", length=20)
     */
    private $pais_fcfo;

    /**
     * @var string $idcfo_fcfo
     * @Column(name="IDCFO_FCFO", type="string")
     */
    private $idcfo_fcfo;

    /**
     * @var string $cei_fcfo
     * @Column(name="CEI_FCFO", type="string", length=20)
     */
    private $cei_fcfo;

    /**
     * @var string $nacionalidade_fcfo
     * @Column(name="NACIONALIDADE_FCFO", type="integer", length=1)
     */
    private $nacionalidade_fcfo;

    /**
     * @var string $tipocontribuinteinss_fcfo
     * @Column(name="TIPOCONTRIBUINTEINSS_FCFO", type="string")
     */
    private $tipocontribuinteinss_fcfo;

    /**
     * @var integer $idmov_tmov
     * @Column(name="IDMOV_TMOV", type="integer")
     *
     */
    private $idmov_tmov;

    /**
     * @var string $tipo_tmov
     * @Column(name="TIPO_TMOV", type="string", length=1)
     *
     */
    private $tipo_tmov;

    /**
     * @var string $status_tmov
     * @Column(name="STATUS_TMOV", type="string", length=1)
     *
     */
    private $status_tmov;

    /**
     * @var datetime $dataemissao_tmov
     * @Column(name="DATAEMISSAO", type="string", length=40)
     *
     */
    private $dataemissao_tmov;

    /**
     * @var string $codcpg_tmov
     * @Column(name="CODCPG_TMOV", type="string", length=15)
     *
     */
    private $codcpg_tmov;

    /**
     * @var string $valorbruto_tmov
     * @Column(name="VALORBRUTO_TMOV", type="decimal", precision=10, scale=2)
     */
    private $valorbruto_tmov;

    /**
     * @var string $valorliquido_tmov
     * @Column(name="VALORLIQUIDO_TMOV", type="decimal", precision=10, scale=2)
     */
    private $valorliquido_tmov;

    /**
     * @var string $valordesc_tmov
     * @Column(name="VALORDESC_TMOV", type="decimal", precision=10, scale=2)
     */
    private $valordesc_tmov;

    /**
     * @var integer $numerolctgerado_tmov
     * @Column(name="NUMEROLCTGERADO_TMOV", type="integer")
     *
     */
    private $numerolctgerado_tmov;

    /**
     * @var integer $numerolctaberto_tmov
     * @Column(name="NUMEROLCTABERTO_TMOV", type="integer")
     *
     */
    private $numerolctaberto_tmov;

    /**
     * @var string $codmunicipio_tmov
     * @Column(name="CODMUNICIPIO_TMOV", type="string", length=20)
     *
     * @Assert\Length(max = 20, maxMessage="Código do município (CODMUNSERVICO_TMOV) deve conter no máximo {{ limit }} caracteres.")
     */
    private $codmunicipio_tmov;

    /**
     * @var string $codetdmunserv_tmov
     * @Column(name="CODETDMUNSERV_TMOV", type="string", length=20)
     *
     */
    private $codetdmunserv_tmov;

    /**
     * @var integer $idnat_tmov
     * @Column(name="IDNAT_TMOV", type="integer")
     *
     */
    private $idnat_tmov;

    /**
     * @var integer $idnat2_tmov
     * @Column(name="IDNAT2_TMOV", type="integer")
     *
     */
    private $idnat2_tmov;

    /**
     * @var integer $movimpresso_tmov
     * @Column(name="MOVIMPRESSO_TMOV", type="integer")
     *
     */
    private $movimpresso_tmov;

    /**
     * @var integer $docimpresso_tmov
     * @Column(name="DOCIMPRESSO_TMOV", type="integer")
     *
     */
    private $docimpresso_tmov;

    /**
     * @var integer $fatimpressa_tmov
     * @Column(name="FATIMPRESSA_TMOV", type="integer")
     *
     */
    private $fatimpressa_tmov;

    /**
     * @var string $codtmv_tmov
     * @Column(name="CODTMV_TMOV", type="string", length=10)
     *
     */
    private $codtmv_tmov;

    /**
     * @var string $codtdo_tmov
     * @Column(name="CODTDO_TMOV", type="string", length=10)
     *
     */
    private $codtdo_tmov;

    /**
     * @var integer $integradoautomacao_tmov
     * @Column(name="INTEGRADOAUTOMACAO_TMOV", type="integer")
     *
     */
    private $integradoautomacao_tmov;

    /**
     * @var string $integraaplicacao_tmov
     * @Column(name="INTEGRAAPLICACAO_TMOV", type="string", length=1)
     *
     */
    private $integraaplicacao_tmov;

    /**
     * @var datetime $datalancamento_tmov
     * @Column(name="DATALANCAMENTO_TMOV", type="string", length=40)
     *
     */
    private $datalancamento_tmov;

    /**
     * @var string $recibonfestatus_tmov
     * @Column(name="RECIBONFESTATUS_TMOV", type="string", length=1)
     *
     */
    private $recibonfestatus_tmov;

    /**
     * @var integer $usarateiovalorfin_tmov
     * @Column(name="USARATEIOVALORFIN_TMOV", type="integer")
     *
     */
    private $usarateiovalorfin_tmov;

    /**
     * @var integer $codcolcfoaux_tmov
     * @Column(name="CODCOLCFOAUX_TMOV", type="integer")
     *
     */
    private $codcolcfoaux_tmov;

    /**
     * @var integer $geroucontatrabalho_tmov
     * @Column(name="GEROUCONTATRABALHO_TMOV", type="integer")
     *
     */
    private $geroucontatrabalho_tmov;

    /**
     * @var integer $geradoporcontatrabalho_tmov
     * @Column(name="GERADOPORCONTATRABALHO_TMOV", type="integer")
     *
     */
    private $geradoporcontatrabalho_tmov;

    /**
     * @var integer $integradobonum_tmov
     * @Column(name="INTEGRADOBONUM_TMOV", type="integer")
     *
     */
    private $integradobonum_tmov;

    /**
     * @var integer $flaprocessado_tmov
     * @Column(name="FLAPROCESSADO_TMOV", type="integer")
     *
     */
    private $flaprocessado_tmov;

    /**
     * @var integer $stsemail_tmov
     * @Column(name="STSEMAIL_TMOV", type="integer")
     *
     */
    private $stsemail_tmov;

    /**
     * @var integer $vinculadoestoquefl_tmov
     * @Column(name="VINCULADOESTOQUEFL_TMOV", type="integer")
     *
     */
    private $vinculadoestoquefl_tmov;

    /**
     * @var integer $codfilial_tmov
     * @Column(name="CODFILIAL_TMOV", type="integer")
     *
     */
    private $codfilial_tmov;

    /**
     * @var string $serie_tmov
     * @Column(name="SERIE_TMOV", type="string", length=1)
     *
     */
    private $serie_tmov;

    /**
     * @var integer $geroufatura_tmov
     * @Column(name="GEROUFATURA_TMOV", type="integer")
     *
     */
    private $geroufatura_tmov;

    /**
     * @var string $codcxa_tmov
     * @Column(name="CODCXA_TMOV", type="string", length=10)
     *
     */
    private $codcxa_tmov;

    /**
     * @var integer $codcolfco_tmov
     * @Column(name="CODCOLFCO_TMOV", type="integer")
     *
     */
    private $codcolfco_tmov;

    /**
     * @var integer $codfilialdestino_tmov
     * @Column(name="CODFILIALDESTINO_TMOV", type="integer")
     *
     */
    private $codfilialdestino_tmov;

    /**
     * @var integer $geradoporlote_tmov
     * @Column(name="GERADOPORLOTE_TMOV", type="integer")
     *
     */
    private $geradoporlote_tmov;

    /**
     * @var integer $statusexportcont_tmov
     * @Column(name="STATUSEXPORTCONT_TMOV", type="integer")
     *
     */
    private $statusexportcont_tmov;

    /**
     * @var string $numeromov_tmov
     * @Column(name="NUMEROMOV_TMOV", type="string", length=35)
     *
     */
    private $numeromov_tmov;

    /**
     * @var string $percentualdesc_tmov
     * @Column(name="PERCENTUALDESC_TMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $percentualdesc_tmov;

    /**
     * @var string $historicocurto_tmov
     * @Column(name="HISTORICOCURTO_TMOV", type="string", length=255)
     *
     */
    private $historicocurto_tmov;

    /**
     * @var string $codccusto_tmov
     * @Column(name="CODCCUSTO_TMOV", type="string", length=25)
     *
     */
    private $codccusto_tmov;

    /**
     * @var string $valor
     * @Column(name="VALOR", type="decimal", precision=10, scale=2)
     *
     */
    private $valor;

    /**
     * @var string $percentual
     * @Column(name="PERCENTUAL", type="decimal", precision=10, scale=2)
     *
     */
    private $percentual;

    /**
     * @var integer $nseqitmov_tmov
     * @Column(name="NSEQITMOV_TMOV", type="integer")
     *
     */
    private $nseqitmov_tmov;

    /**
     * @var string $id_venda
     * @Column(name="id_venda", type="string")
     *
     */
    private $id_venda;

    /**
     * @var string $codtb2flx
     * @Column(name="CODTB2FLX", type="string", length=25)
     */
    private $codtb2flx;

    /**
     * @return smallint
     */
    public function getCodcoligadaFcfo()
    {
        return $this->codcoligada_fcfo;
    }

    /**
     * @param smallint $codcoligada_fcfo
     */
    public function setCodcoligadaFcfo($codcoligada_fcfo)
    {
        $this->codcoligada_fcfo = $codcoligada_fcfo;
    }

    /**
     * @return string
     */
    public function getCodcfoFcfo()
    {
        return $this->codcfo_fcfo;
    }

    /**
     * @param string $codcfo_fcfo
     */
    public function setCodcfoFcfo($codcfo_fcfo)
    {
        $this->codcfo_fcfo = $codcfo_fcfo;
    }

    /**
     * @return string
     */
    public function getNomefantasiaFcfo()
    {
        return $this->nomefantasia_fcfo;
    }

    /**
     * @param string $nomefantasia_fcfo
     */
    public function setNomefantasiaFcfo($nomefantasia_fcfo)
    {
        $this->nomefantasia_fcfo = $nomefantasia_fcfo;
    }

    /**
     * @return string
     */
    public function getNomeFcfo()
    {
        return $this->nome_fcfo;
    }

    /**
     * @param string $nome_fcfo
     */
    public function setNomeFcfo($nome_fcfo)
    {
        $this->nome_fcfo = $nome_fcfo;
    }

    /**
     * @return string
     */
    public function getCgccfoFcfo()
    {
        return $this->cgccfo_fcfo;
    }

    /**
     * @param string $cgccfo_fcfo
     */
    public function setCgccfoFcfo($cgccfo_fcfo)
    {
        $this->cgccfo_fcfo = $cgccfo_fcfo;
    }

    /**
     * @return string
     */
    public function getInscrestadualFcfo()
    {
        return $this->inscrestadual_fcfo;
    }

    /**
     * @param string $inscrestadual_fcfo
     */
    public function setInscrestadualFcfo($inscrestadual_fcfo)
    {
        $this->inscrestadual_fcfo = $inscrestadual_fcfo;
    }

    /**
     * @return string
     */
    public function getPagrecFcfo()
    {
        return $this->pagrec_fcfo;
    }

    /**
     * @param string $pagrec_fcfo
     */
    public function setPagrecFcfo($pagrec_fcfo)
    {
        $this->pagrec_fcfo = $pagrec_fcfo;
    }

    /**
     * @return string
     */
    public function getTipoRua()
    {
        return $this->tipo_rua;
    }

    /**
     * @param string $tipo_rua
     */
    public function setTipoRua($tipo_rua)
    {
        $this->tipo_rua = $tipo_rua;
    }

    /**
     * @return string
     */
    public function getRuaFcfo()
    {
        return $this->rua_fcfo;
    }

    /**
     * @param string $rua_fcfo
     */
    public function setRuaFcfo($rua_fcfo)
    {
        $this->rua_fcfo = $rua_fcfo;
    }

    /**
     * @return string
     */
    public function getNumeroFcfo()
    {
        return $this->numero_fcfo;
    }

    /**
     * @param string $numero_fcfo
     */
    public function setNumeroFcfo($numero_fcfo)
    {
        $this->numero_fcfo = $numero_fcfo;
    }

    /**
     * @return string
     */
    public function getComplementoFcfo()
    {
        return $this->complemento_fcfo;
    }

    /**
     * @param string $complemento_fcfo
     */
    public function setComplementoFcfo($complemento_fcfo)
    {
        $this->complemento_fcfo = $complemento_fcfo;
    }

    /**
     * @return string
     */
    public function getTipobairroFcfo()
    {
        return $this->tipobairro_fcfo;
    }

    /**
     * @param string $tipobairro_fcfo
     */
    public function setTipobairroFcfo($tipobairro_fcfo)
    {
        $this->tipobairro_fcfo = $tipobairro_fcfo;
    }

    /**
     * @return string
     */
    public function getBairroFcfo()
    {
        return $this->bairro_fcfo;
    }

    /**
     * @param string $bairro_fcfo
     */
    public function setBairroFcfo($bairro_fcfo)
    {
        $this->bairro_fcfo = $bairro_fcfo;
    }

    /**
     * @return string
     */
    public function getCidadeFcfo()
    {
        return $this->cidade_fcfo;
    }

    /**
     * @param string $cidade_fcfo
     */
    public function setCidadeFcfo($cidade_fcfo)
    {
        $this->cidade_fcfo = $cidade_fcfo;
    }

    /**
     * @return string
     */
    public function getCodetdFcfo()
    {
        return $this->codetd_fcfo;
    }

    /**
     * @param string $codetd_fcfo
     */
    public function setCodetdFcfo($codetd_fcfo)
    {
        $this->codetd_fcfo = $codetd_fcfo;
    }

    /**
     * @return string
     */
    public function getCepFcfo()
    {
        return $this->cep_fcfo;
    }

    /**
     * @param string $cep_fcfo
     */
    public function setCepFcfo($cep_fcfo)
    {
        $this->cep_fcfo = $cep_fcfo;
    }

    /**
     * @return string
     */
    public function getTelefoneFcfo()
    {
        return $this->telefone_fcfo;
    }

    /**
     * @param string $telefone_fcfo
     */
    public function setTelefoneFcfo($telefone_fcfo)
    {
        $this->telefone_fcfo = $telefone_fcfo;
    }

    /**
     * @return string
     */
    public function getFaxFcfo()
    {
        return $this->fax_fcfo;
    }

    /**
     * @param string $fax_fcfo
     */
    public function setFaxFcfo($fax_fcfo)
    {
        $this->fax_fcfo = $fax_fcfo;
    }

    /**
     * @return string
     */
    public function getEmailFcfo()
    {
        return $this->email_fcfo;
    }

    /**
     * @param string $email_fcfo
     */
    public function setEmailFcfo($email_fcfo)
    {
        $this->email_fcfo = $email_fcfo;
    }

    /**
     * @return string
     */
    public function getContatoFcfo()
    {
        return $this->contato_fcfo;
    }

    /**
     * @param string $contato_fcfo
     */
    public function setContatoFcfo($contato_fcfo)
    {
        $this->contato_fcfo = $contato_fcfo;
    }

    /**
     * @return string
     */
    public function getAtivoFcfo()
    {
        return $this->ativo_fcfo;
    }

    /**
     * @param string $ativo_fcfo
     */
    public function setAtivoFcfo($ativo_fcfo)
    {
        $this->ativo_fcfo = $ativo_fcfo;
    }

    /**
     * @return string
     */
    public function getCodmunicipioFcfo()
    {
        return $this->codmunicipio_fcfo;
    }

    /**
     * @param string $codmunicipio_fcfo
     */
    public function setCodmunicipioFcfo($codmunicipio_fcfo)
    {
        $this->codmunicipio_fcfo = $codmunicipio_fcfo;
    }

    /**
     * @return string
     */
    public function getPessoafisoujurFcfo()
    {
        return $this->pessoafisoujur_fcfo;
    }

    /**
     * @param string $pessoafisoujur_fcfo
     */
    public function setPessoafisoujurFcfo($pessoafisoujur_fcfo)
    {
        $this->pessoafisoujur_fcfo = $pessoafisoujur_fcfo;
    }

    /**
     * @return string
     */
    public function getIdpaisFcfo()
    {
        return $this->idpais_fcfo;
    }

    /**
     * @param string $idpais_fcfo
     */
    public function setIdpaisFcfo($idpais_fcfo)
    {
        $this->idpais_fcfo = $idpais_fcfo;
    }

    /**
     * @return string
     */
    public function getPaisFcfo()
    {
        return $this->pais_fcfo;
    }

    /**
     * @param string $pais_fcfo
     */
    public function setPaisFcfo($pais_fcfo)
    {
        $this->pais_fcfo = $pais_fcfo;
    }

    /**
     * @return string
     */
    public function getIdcfoFcfo()
    {
        return $this->idcfo_fcfo;
    }

    /**
     * @param string $idcfo_fcfo
     */
    public function setIdcfoFcfo($idcfo_fcfo)
    {
        $this->idcfo_fcfo = $idcfo_fcfo;
    }

    /**
     * @return string
     */
    public function getCeiFcfo()
    {
        return $this->cei_fcfo;
    }

    /**
     * @param string $cei_fcfo
     */
    public function setCeiFcfo($cei_fcfo)
    {
        $this->cei_fcfo = $cei_fcfo;
    }

    /**
     * @return string
     */
    public function getNacionalidadeFcfo()
    {
        return $this->nacionalidade_fcfo;
    }

    /**
     * @param string $nacionalidade_fcfo
     */
    public function setNacionalidadeFcfo($nacionalidade_fcfo)
    {
        $this->nacionalidade_fcfo = $nacionalidade_fcfo;
    }

    /**
     * @return string
     */
    public function getTipocontribuinteinssFcfo()
    {
        return $this->tipocontribuinteinss_fcfo;
    }

    /**
     * @param string $tipocontribuinteinss_fcfo
     */
    public function setTipocontribuinteinssFcfo($tipocontribuinteinss_fcfo)
    {
        $this->tipocontribuinteinss_fcfo = $tipocontribuinteinss_fcfo;
    }

    /**
     * @return int
     */
    public function getIdmovTmov()
    {
        return $this->idmov_tmov;
    }

    /**
     * @param int $idmov_tmov
     */
    public function setIdmovTmov($idmov_tmov)
    {
        $this->idmov_tmov = $idmov_tmov;
    }

    /**
     * @return string
     */
    public function getTipoTmov()
    {
        return $this->tipo_tmov;
    }

    /**
     * @param string $tipo_tmov
     */
    public function setTipoTmov($tipo_tmov)
    {
        $this->tipo_tmov = $tipo_tmov;
    }

    /**
     * @return string
     */
    public function getStatusTmov()
    {
        return $this->status_tmov;
    }

    /**
     * @param string $status_tmov
     */
    public function setStatusTmov($status_tmov)
    {
        $this->status_tmov = $status_tmov;
    }

    /**
     * @return datetime
     */
    public function getDataemissaoTmov()
    {
        return $this->dataemissao_tmov;
    }

    /**
     * @param datetime $dataemissao_tmov
     */
    public function setDataemissaoTmov($dataemissao_tmov)
    {
        $this->dataemissao_tmov = $dataemissao_tmov;
    }

    /**
     * @return string
     */
    public function getCodcpgTmov()
    {
        return $this->codcpg_tmov;
    }

    /**
     * @param string $codcpg_tmov
     */
    public function setCodcpgTmov($codcpg_tmov)
    {
        $this->codcpg_tmov = $codcpg_tmov;
    }

    /**
     * @return string
     */
    public function getValorbrutoTmov()
    {
        return $this->valorbruto_tmov;
    }

    /**
     * @param string $valorbruto_tmov
     */
    public function setValorbrutoTmov($valorbruto_tmov)
    {
        $this->valorbruto_tmov = $valorbruto_tmov;
    }

    /**
     * @return string
     */
    public function getValorliquidoTmov()
    {
        return $this->valorliquido_tmov;
    }

    /**
     * @param string $valorliquido_tmov
     */
    public function setValorliquidoTmov($valorliquido_tmov)
    {
        $this->valorliquido_tmov = $valorliquido_tmov;
    }

    /**
     * @return string
     */
    public function getValordescTmov()
    {
        return $this->valordesc_tmov;
    }

    /**
     * @param string $valordesc_tmov
     */
    public function setValordescTmov($valordesc_tmov)
    {
        $this->valordesc_tmov = $valordesc_tmov;
    }

    /**
     * @return int
     */
    public function getNumerolctgeradoTmov()
    {
        return $this->numerolctgerado_tmov;
    }

    /**
     * @param int $numerolctgerado_tmov
     */
    public function setNumerolctgeradoTmov($numerolctgerado_tmov)
    {
        $this->numerolctgerado_tmov = $numerolctgerado_tmov;
    }

    /**
     * @return int
     */
    public function getNumerolctabertoTmov()
    {
        return $this->numerolctaberto_tmov;
    }

    /**
     * @param int $numerolctaberto_tmov
     */
    public function setNumerolctabertoTmov($numerolctaberto_tmov)
    {
        $this->numerolctaberto_tmov = $numerolctaberto_tmov;
    }

    /**
     * @return string
     */
    public function getCodmunicipioTmov()
    {
        return $this->codmunicipio_tmov;
    }

    /**
     * @param string $codmunicipio_tmov
     */
    public function setCodmunicipioTmov($codmunicipio_tmov)
    {
        $this->codmunicipio_tmov = $codmunicipio_tmov;
    }

    /**
     * @return string
     */
    public function getCodetdmunservTmov()
    {
        return $this->codetdmunserv_tmov;
    }

    /**
     * @param string $codetdmunserv_tmov
     */
    public function setCodetdmunservTmov($codetdmunserv_tmov)
    {
        $this->codetdmunserv_tmov = $codetdmunserv_tmov;
    }

    /**
     * @return int
     */
    public function getIdnatTmov()
    {
        return $this->idnat_tmov;
    }

    /**
     * @param int $idnat_tmov
     */
    public function setIdnatTmov($idnat_tmov)
    {
        $this->idnat_tmov = $idnat_tmov;
    }

    /**
     * @return int
     */
    public function getIdnat2Tmov()
    {
        return $this->idnat2_tmov;
    }

    /**
     * @param int $idnat2_tmov
     */
    public function setIdnat2Tmov($idnat2_tmov)
    {
        $this->idnat2_tmov = $idnat2_tmov;
    }

    /**
     * @return int
     */
    public function getMovimpressoTmov()
    {
        return $this->movimpresso_tmov;
    }

    /**
     * @param int $movimpresso_tmov
     */
    public function setMovimpressoTmov($movimpresso_tmov)
    {
        $this->movimpresso_tmov = $movimpresso_tmov;
    }

    /**
     * @return int
     */
    public function getDocimpressoTmov()
    {
        return $this->docimpresso_tmov;
    }

    /**
     * @param int $docimpresso_tmov
     */
    public function setDocimpressoTmov($docimpresso_tmov)
    {
        $this->docimpresso_tmov = $docimpresso_tmov;
    }

    /**
     * @return int
     */
    public function getFatimpressaTmov()
    {
        return $this->fatimpressa_tmov;
    }

    /**
     * @param int $fatimpressa_tmov
     */
    public function setFatimpressaTmov($fatimpressa_tmov)
    {
        $this->fatimpressa_tmov = $fatimpressa_tmov;
    }

    /**
     * @return string
     */
    public function getCodtmvTmov()
    {
        return $this->codtmv_tmov;
    }

    /**
     * @param string $codtmv_tmov
     */
    public function setCodtmvTmov($codtmv_tmov)
    {
        $this->codtmv_tmov = $codtmv_tmov;
    }

    /**
     * @return string
     */
    public function getCodtdoTmov()
    {
        return $this->codtdo_tmov;
    }

    /**
     * @param string $codtdo_tmov
     */
    public function setCodtdoTmov($codtdo_tmov)
    {
        $this->codtdo_tmov = $codtdo_tmov;
    }

    /**
     * @return int
     */
    public function getIntegradoautomacaoTmov()
    {
        return $this->integradoautomacao_tmov;
    }

    /**
     * @param int $integradoautomacao_tmov
     */
    public function setIntegradoautomacaoTmov($integradoautomacao_tmov)
    {
        $this->integradoautomacao_tmov = $integradoautomacao_tmov;
    }

    /**
     * @return string
     */
    public function getIntegraaplicacaoTmov()
    {
        return $this->integraaplicacao_tmov;
    }

    /**
     * @param string $integraaplicacao_tmov
     */
    public function setIntegraaplicacaoTmov($integraaplicacao_tmov)
    {
        $this->integraaplicacao_tmov = $integraaplicacao_tmov;
    }

    /**
     * @return datetime
     */
    public function getDatalancamentoTmov()
    {
        return $this->datalancamento_tmov;
    }

    /**
     * @param datetime $datalancamento_tmov
     */
    public function setDatalancamentoTmov($datalancamento_tmov)
    {
        $this->datalancamento_tmov = $datalancamento_tmov;
    }

    /**
     * @return string
     */
    public function getRecibonfestatusTmov()
    {
        return $this->recibonfestatus_tmov;
    }

    /**
     * @param string $recibonfestatus_tmov
     */
    public function setRecibonfestatusTmov($recibonfestatus_tmov)
    {
        $this->recibonfestatus_tmov = $recibonfestatus_tmov;
    }

    /**
     * @return int
     */
    public function getUsarateiovalorfinTmov()
    {
        return $this->usarateiovalorfin_tmov;
    }

    /**
     * @param int $usarateiovalorfin_tmov
     */
    public function setUsarateiovalorfinTmov($usarateiovalorfin_tmov)
    {
        $this->usarateiovalorfin_tmov = $usarateiovalorfin_tmov;
    }

    /**
     * @return int
     */
    public function getCodcolcfoauxTmov()
    {
        return $this->codcolcfoaux_tmov;
    }

    /**
     * @param int $codcolcfoaux_tmov
     */
    public function setCodcolcfoauxTmov($codcolcfoaux_tmov)
    {
        $this->codcolcfoaux_tmov = $codcolcfoaux_tmov;
    }

    /**
     * @return int
     */
    public function getGeroucontatrabalhoTmov()
    {
        return $this->geroucontatrabalho_tmov;
    }

    /**
     * @param int $geroucontatrabalho_tmov
     */
    public function setGeroucontatrabalhoTmov($geroucontatrabalho_tmov)
    {
        $this->geroucontatrabalho_tmov = $geroucontatrabalho_tmov;
    }

    /**
     * @return int
     */
    public function getGeradoporcontatrabalhoTmov()
    {
        return $this->geradoporcontatrabalho_tmov;
    }

    /**
     * @param int $geradoporcontatrabalho_tmov
     */
    public function setGeradoporcontatrabalhoTmov($geradoporcontatrabalho_tmov)
    {
        $this->geradoporcontatrabalho_tmov = $geradoporcontatrabalho_tmov;
    }

    /**
     * @return int
     */
    public function getIntegradobonumTmov()
    {
        return $this->integradobonum_tmov;
    }

    /**
     * @param int $integradobonum_tmov
     */
    public function setIntegradobonumTmov($integradobonum_tmov)
    {
        $this->integradobonum_tmov = $integradobonum_tmov;
    }

    /**
     * @return int
     */
    public function getFlaprocessadoTmov()
    {
        return $this->flaprocessado_tmov;
    }

    /**
     * @param int $flaprocessado_tmov
     */
    public function setFlaprocessadoTmov($flaprocessado_tmov)
    {
        $this->flaprocessado_tmov = $flaprocessado_tmov;
    }

    /**
     * @return int
     */
    public function getStsemailTmov()
    {
        return $this->stsemail_tmov;
    }

    /**
     * @param int $stsemail_tmov
     */
    public function setStsemailTmov($stsemail_tmov)
    {
        $this->stsemail_tmov = $stsemail_tmov;
    }

    /**
     * @return int
     */
    public function getVinculadoestoqueflTmov()
    {
        return $this->vinculadoestoquefl_tmov;
    }

    /**
     * @param int $vinculadoestoquefl_tmov
     */
    public function setVinculadoestoqueflTmov($vinculadoestoquefl_tmov)
    {
        $this->vinculadoestoquefl_tmov = $vinculadoestoquefl_tmov;
    }

    /**
     * @return int
     */
    public function getCodfilialTmov()
    {
        return $this->codfilial_tmov;
    }

    /**
     * @param int $codfilial_tmov
     */
    public function setCodfilialTmov($codfilial_tmov)
    {
        $this->codfilial_tmov = $codfilial_tmov;
    }

    /**
     * @return string
     */
    public function getSerieTmov()
    {
        return $this->serie_tmov;
    }

    /**
     * @param string $serie_tmov
     */
    public function setSerieTmov($serie_tmov)
    {
        $this->serie_tmov = $serie_tmov;
    }

    /**
     * @return int
     */
    public function getGeroufaturaTmov()
    {
        return $this->geroufatura_tmov;
    }

    /**
     * @param int $geroufatura_tmov
     */
    public function setGeroufaturaTmov($geroufatura_tmov)
    {
        $this->geroufatura_tmov = $geroufatura_tmov;
    }

    /**
     * @return string
     */
    public function getCodcxaTmov()
    {
        return $this->codcxa_tmov;
    }

    /**
     * @param string $codcxa_tmov
     */
    public function setCodcxaTmov($codcxa_tmov)
    {
        $this->codcxa_tmov = $codcxa_tmov;
    }

    /**
     * @return int
     */
    public function getCodcolfcoTmov()
    {
        return $this->codcolfco_tmov;
    }

    /**
     * @param int $codcolfco_tmov
     */
    public function setCodcolfcoTmov($codcolfco_tmov)
    {
        $this->codcolfco_tmov = $codcolfco_tmov;
    }

    /**
     * @return int
     */
    public function getCodfilialdestinoTmov()
    {
        return $this->codfilialdestino_tmov;
    }

    /**
     * @param int $codfilialdestino_tmov
     */
    public function setCodfilialdestinoTmov($codfilialdestino_tmov)
    {
        $this->codfilialdestino_tmov = $codfilialdestino_tmov;
    }

    /**
     * @return int
     */
    public function getGeradoporloteTmov()
    {
        return $this->geradoporlote_tmov;
    }

    /**
     * @param int $geradoporlote_tmov
     */
    public function setGeradoporloteTmov($geradoporlote_tmov)
    {
        $this->geradoporlote_tmov = $geradoporlote_tmov;
    }

    /**
     * @return int
     */
    public function getStatusexportcontTmov()
    {
        return $this->statusexportcont_tmov;
    }

    /**
     * @param int $statusexportcont_tmov
     */
    public function setStatusexportcontTmov($statusexportcont_tmov)
    {
        $this->statusexportcont_tmov = $statusexportcont_tmov;
    }

    /**
     * @return string
     */
    public function getNumeromovTmov()
    {
        return $this->numeromov_tmov;
    }

    /**
     * @param string $numeromov_tmov
     */
    public function setNumeromovTmov($numeromov_tmov)
    {
        $this->numeromov_tmov = $numeromov_tmov;
    }

    /**
     * @return string
     */
    public function getPercentualdescTmov()
    {
        return $this->percentualdesc_tmov;
    }

    /**
     * @param string $percentualdesc_tmov
     */
    public function setPercentualdescTmov($percentualdesc_tmov)
    {
        $this->percentualdesc_tmov = $percentualdesc_tmov;
    }

    /**
     * @return string
     */
    public function getHistoricocurtoTmov()
    {
        return $this->historicocurto_tmov;
    }

    /**
     * @param string $historicocurto_tmov
     */
    public function setHistoricocurtoTmov($historicocurto_tmov)
    {
        $this->historicocurto_tmov = $historicocurto_tmov;
    }

    /**
     * @return string
     */
    public function getCodccustoTmov()
    {
        return $this->codccusto_tmov;
    }

    /**
     * @param string $codccusto_tmov
     */
    public function setCodccustoTmov($codccusto_tmov)
    {
        $this->codccusto_tmov = $codccusto_tmov;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * @param string $percentual
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;
    }

    /**
     * @return int
     */
    public function getNseqitmovTmov()
    {
        return $this->nseqitmov_tmov;
    }

    /**
     * @param int $nseqitmov_tmov
     */
    public function setNseqitmovTmov($nseqitmov_tmov)
    {
        $this->nseqitmov_tmov = $nseqitmov_tmov;
    }

    /**
     * @return string
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param string $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return string
     */
    public function getCodtb2flx()
    {
        return $this->codtb2flx;
    }

    /**
     * @param string $codtb2flx
     */
    public function setCodtb2flx($codtb2flx)
    {
        $this->codtb2flx = $codtb2flx;
    }

}
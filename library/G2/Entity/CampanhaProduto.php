<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_campanhaproduto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CampanhaProduto
{

    /**
     * @var integer $id_campanhacomercial
     * @Column(type="integer", nullable=false, name="id_campanhacomercial")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false, name="id_produto")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    public function getId_produto ()
    {
        return $this->id_produto;
    }

    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

}

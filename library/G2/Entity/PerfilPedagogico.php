<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_perfilpedagogico")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-03-27
 */
class PerfilPedagogico {

    /**
     *
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_perfilpedagogico;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_perfilpedagogico")
     * @var string
     */
    private $st_perfilpedagogico;

    /**
     * @return int
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @param int $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @return string
     */
    public function getSt_perfilpedagogico()
    {
        return $this->st_perfilpedagogico;
    }

    /**
     * @param string $st_perfilpedagogico
     * @return PerfilPedagogico
     */
    public function setSt_perfilpedagogico($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
        return $this;
    }



}

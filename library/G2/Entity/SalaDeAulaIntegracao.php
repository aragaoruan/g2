<?php

namespace G2\Entity;

use G2\G2Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Entity
 * @Table(name="tb_saladeaulaintegracao")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @EntityLog
 */
class SalaDeAulaIntegracao extends G2Entity
{
    /**
     * @Id
     * @var integer $id_saladeaulaintegracao
     * @Column(name="id_saladeaulaintegracao", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_saladeaulaintegracao;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;

    /**
     * @var boolean $bl_conteudo
     * @Column(name="bl_conteudo", type="boolean", nullable=true, length=1)
     */
    private $bl_conteudo;

    /**
     * @var boolean $bl_visivel
     * @Column(name="bl_visivel", type="boolean", nullable=false, length=1)
     */
    private $bl_visivel;

    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=false, length=30)
     */
    private $st_codsistemacurso;

    /**
     * @var string $st_codsistemasala
     * @Column(name="st_codsistemasala", type="string", nullable=true, length=30)
     */
    private $st_codsistemasala;

    /**
     * @var string $st_codsistemareferencia
     * @Column(name="st_codsistemareferencia", type="string", nullable=true, length=30)
     */
    private $st_codsistemareferencia;

    /**
     * @var string $st_retornows
     * @Column(name="st_retornows", type="string", nullable=true)
     */
    private $st_retornows;


    /**
     * @var DisciplinaIntegracao $id_disciplinaintegracao
     * @ManyToOne(targetEntity="DisciplinaIntegracao")
     * @JoinColumn(name="id_disciplinaintegracao", referencedColumnName="id_disciplinaintegracao", nullable=false)
     */
    private $id_disciplinaintegracao;

    /**
     * @var boolean $bl_erroimportacao
     * @Column(name="bl_erroimportacao", type="boolean", nullable=true, length=1)
     */
    private $bl_erroimportacao;

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param $id_saladeaulaintegracao
     * @return $this
     */
    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param $id_saladeaula
     * @return $this
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param $id_sistema
     * @return $this
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_conteudo()
    {
        return $this->bl_conteudo;
    }

    /**
     * @param $bl_conteudo
     * @return $this
     */
    public function setBl_conteudo($bl_conteudo)
    {
        $this->bl_conteudo = $bl_conteudo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_visivel()
    {
        return $this->bl_visivel;
    }

    /**
     * @param $bl_visivel
     * @return $this
     */
    public function setBl_visivel($bl_visivel)
    {
        $this->bl_visivel = $bl_visivel;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param $st_codsistemacurso
     * @return $this
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistemasala()
    {
        return $this->st_codsistemasala;
    }

    /**
     * @param $st_codsistemasala
     * @return $this
     */
    public function setSt_codsistemasala($st_codsistemasala)
    {
        $this->st_codsistemasala = $st_codsistemasala;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistemareferencia()
    {
        return $this->st_codsistemareferencia;
    }

    /**
     * @param $st_codsistemareferencia
     * @return $this
     */
    public function setSt_codsistemareferencia($st_codsistemareferencia)
    {
        $this->st_codsistemareferencia = $st_codsistemareferencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_retornows()
    {
        return $this->st_retornows;
    }

    /**
     * @param $st_retornows
     * @return $this
     */
    public function setSt_retornows($st_retornows)
    {
        $this->st_retornows = $st_retornows;
        return $this;
    }

    /**
     * @return DisciplinaIntegracao
     */
    public function getId_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    /**
     * @param $id_disciplinaintegracao
     * @return $this
     */
    public function setId_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_erroimportacao()
    {
        return $this->bl_erroimportacao;
    }

    /**
     * @param bool $bl_erroimportacao
     * @return SalaDeAulaIntegracao
     */
    public function setBl_erroimportacao($bl_erroimportacao)
    {
        $this->bl_erroimportacao = $bl_erroimportacao;
        return $this;
    }




}

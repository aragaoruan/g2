<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_perfilpedagogico")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class PefilPedagogico
{

    /**
     *
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_perfilpedagogico;

    /**
     *
     * @var string $st_perfilpedagogico
     * @Column(name="st_perfilpedagogico", type="string", nullable=false, length=255)
     */
    private $st_perfilpedagogico;

    public function getId_perfilpedagogico ()
    {
        return $this->id_perfilpedagogico;
    }

    public function setId_perfilpedagogico ($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    public function getSt_perfilpedagogico ()
    {
        return $this->st_perfilpedagogico;
    }

    public function setSt_perfilpedagogico ($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
        return $this;
    }

}

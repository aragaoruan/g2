<?php

namespace G2\Entity;

/**
 * @Table(name="tb_esquemaconfiguracaoitem")
 * @Entity(repositoryClass="\G2\Repository\EsquemaConfiguracaoItem")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 * @EntityLog
 */
class EsquemaConfiguracaoItem
{
    /**
     * @var EsquemaConfiguracao
     * @ManyToOne(targetEntity="EsquemaConfiguracao")
     * @JoinColumn(name="id_esquemaconfiguracao", referencedColumnName="id_esquemaconfiguracao")
     * @Id
     */
    private $id_esquemaconfiguracao;

    /**
     *
     * @var ItemConfiguracao
     * @ManyToOne(targetEntity="ItemConfiguracao")
     * @JoinColumn(name="id_itemconfiguracao", referencedColumnName="id_itemconfiguracao")
     * @Id
     */
    private $id_itemconfiguracao;

    /**
     *
     * @var string $st_valor
     * @Column(name="st_valor", type="string", nullable=false, length=200)
     */
    private $st_valor;

    /**
     * @return int
     */

    public function getid_esquemaconfiguracao()
    {
        return $this->id_esquemaconfiguracao;
    }

    /**
     * @param int $id_esquemaconfiguracao
     */
    public function setid_esquemaconfiguracao($id_esquemaconfiguracao)
    {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_itemconfiguracao()
    {
        return $this->id_itemconfiguracao;
    }

    /**
     * @param int $id_itemconfiguracao
     */
    public function setid_itemconfiguracao($id_itemconfiguracao)
    {
        $this->id_itemconfiguracao = $id_itemconfiguracao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_valor()
    {
        return $this->st_valor;
    }

    /**
     * @param string $st_valor
     */
    public function setst_valor($st_valor)
    {
        $this->st_valor = $st_valor;
        return $this;

    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_ocorrenciaaguardando")
 * @Entity
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwOcorrenciaAguardando {

    /**
     * @Id
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_ocorrencia;

    /**
     * 
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_ultimotramite
     * @Column(name="dt_ultimotramite", type="datetime2", nullable=true, length=8)
     */
    private $dt_ultimotramite;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariointeressado
     * @Column(name="id_usuariointeressado", type="integer", nullable=false, length=4)
     */
    private $id_usuariointeressado;

    /**
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_categoriaocorrencia;

    /**
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=true, length=4)
     */
    private $id_assuntoco;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_tipoocorrencia;

    /**
     * @var integer $id_usuarioresponsavel
     * @Column(name="id_usuarioresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_usuarioresponsavel;

    /**
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false, length=4)
     */
    private $id_nucleoco;

    /**
     * @var string $st_nomeinteressado
     * @Column(name="st_nomeinteressado", type="string", nullable=false, length=300)
     */
    private $st_nomeinteressado;

    /**
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=false)
     */
    private $st_titulo;

    /**
     * @var string $st_ocorrencia
     * @Column(name="st_ocorrencia", type="string", nullable=false)
     */
    private $st_ocorrencia;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     * @var string $st_categoriaocorrencia
     * @Column(name="st_categoriaocorrencia", type="string", nullable=false, length=250)
     */
    private $st_categoriaocorrencia;

    /**
     * @var string $st_ultimotramite
     * @Column(name="st_ultimotramite", type="string", nullable=true)
     */
    private $st_ultimotramite;

    /**
     * @var string $st_nomeresponsavel
     * @Column(name="st_nomeresponsavel", type="string", nullable=true, length=300)
     */
    private $st_nomeresponsavel;

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getDt_ultimotramite()
    {
        return $this->dt_ultimotramite;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function getId_usuarioresponsavel()
    {
        return $this->id_usuarioresponsavel;
    }

    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    public function getSt_nomeinteressado()
    {
        return $this->st_nomeinteressado;
    }

    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    public function getSt_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function getSt_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    public function getSt_ultimotramite()
    {
        return $this->st_ultimotramite;
    }

    public function getSt_nomeresponsavel()
    {
        return $this->st_nomeresponsavel;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setDt_cadastro(datetime2 $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setDt_ultimotramite(datetime2 $dt_ultimotramite)
    {
        $this->dt_ultimotramite = $dt_ultimotramite;
        return $this;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
        return $this;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function setId_usuarioresponsavel($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
        return $this;
    }

    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
        return $this;
    }

    public function setSt_nomeinteressado($st_nomeinteressado)
    {
        $this->st_nomeinteressado = $st_nomeinteressado;
        return $this;
    }

    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
        return $this;
    }

    public function setSt_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
        return $this;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
        return $this;
    }

    public function setSt_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
        return $this;
    }

    public function setSt_ultimotramite($st_ultimotramite)
    {
        $this->st_ultimotramite = $st_ultimotramite;
        return $this;
    }

    public function setSt_nomeresponsavel($st_nomeresponsavel)
    {
        $this->st_nomeresponsavel = $st_nomeresponsavel;
        return $this;
    }

}

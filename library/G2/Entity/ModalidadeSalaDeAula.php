<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_modalidadesaladeaula")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ModalidadeSalaDeAula
{

    /**
     *
     * @var integer $id_modalidadesaladeaula
     * @Column(name="id_modalidadesaladeaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_modalidadesaladeaula;

    /**
     *
     * @var string $st_modalidadesaladeaula
     * @Column(name="st_modalidadesaladeaula", type="text", nullable=false, length=255)
     */
    private $st_modalidadesaladeaula;

    public function getId_modalidadesaladeaula ()
    {
        return $this->id_modalidadesaladeaula;
    }

    public function setId_modalidadesaladeaula ($id_modalidadesaladeaula)
    {
        $this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
        return $this;
    }

    public function getSt_modalidadesaladeaula ()
    {
        return $this->st_modalidadesaladeaula;
    }

    public function setSt_modalidadesaladeaula ($st_modalidadesaladeaula)
    {
        $this->st_modalidadesaladeaula = $st_modalidadesaladeaula;
        return $this;
    }

}
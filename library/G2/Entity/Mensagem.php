<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_mensagem")
 * @Entity(repositoryClass="\G2\Repository\MensagemRepository")
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Mensagem {


    const ATIVA = 147;
    const INATIVA = 148;

    /**
     * @var integer $id_mensagem
     * @Column(name="id_mensagem", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_mensagem;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=true)
     */
    private $id_funcionalidade;

    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false, length=8000)
     */
    private $st_mensagem;


    /**
     * @var string $st_texto
     * @Column(name="st_texto", type="string", nullable=false, length=8000)
     */
    private $st_texto;


    /**
     * @var string $bl_importante
     * @Column(name="bl_importante", type="boolean", nullable=false)
     */
    private $bl_importante;

    /**
     * @var string $st_caminhoanexo
     * @Column(name="st_caminhoanexo", type="string", nullable=true , length=8000)
     */
    private $st_caminhoanexo;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_areaconhecimento
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", referencedColumnName="id_areaconhecimento")
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;


    public function getId_mensagem() {
        return $this->id_mensagem;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getSt_mensagem() {
        return $this->st_mensagem;
    }

    public function getSt_texto() {
        return $this->st_texto;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_mensagem($id_mensagem) {
        $this->id_mensagem = $id_mensagem;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setSt_mensagem($st_mensagem) {
        $this->st_mensagem = $st_mensagem;
        return $this;
    }

    public function setSt_texto($st_texto) {
        $this->st_texto = $st_texto;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    public function setId_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function getBl_importante()
    {
        return $this->bl_importante;
    }

    public function setBl_importante($bl_importante)
    {
        $this->bl_importante = $bl_importante;
        return $this;
    }

    public function getSt_caminhoanexo()
    {
        return $this->st_caminhoanexo;

    }

    public function setSt_caminhoanexo($st_caminhoanexo)
    {
        $this->st_caminhoanexo = $st_caminhoanexo;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    public function getId_turma()
    {
        return $this->id_turma;
    }

    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }




} 
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_prevendaproduto")
 * @Entity(repositoryClass="G2\Repository\PrevendaProduto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class PrevendaProduto
{

    /**
     * @var integer $id_prevendaproduto
     * @Column(name="id_prevendaproduto", type="integer",  nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_prevendaproduto;

    /**
     * @var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @var Prevenda $id_prevenda
     * @ManyToOne(targetEntity="Prevenda")
     * @JoinColumn(name="id_prevenda", referencedColumnName="id_prevenda")
     */
    private $id_prevenda;

    public function getId_prevendaproduto ()
    {
        return $this->id_prevendaproduto;
    }

    public function setId_prevendaproduto ($id_prevendaproduto)
    {
        $this->id_prevendaproduto = $id_prevendaproduto;
        return $this;
    }

    /**
     * Get \G2\Entity\PrevendaProduto
     * @return \G2\Entity\PrevendaProduto
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * Set \G2\Entity\PrevendaProduto
     * @param \G2\Entity\Produto $id_produto
     * @return \G2\Entity\PrevendaProduto
     */
    public function setId_produto (Produto $id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * Get \G2\Entity\PrevendaProduto
     * @return \G2\Entity\PrevendaProduto
     */
    public function getId_prevenda ()
    {
        return $this->id_prevenda;
    }

    /**
     * Set \G2\Entity\PrevendaProduto
     * @param \G2\Entity\Prevenda $id_prevenda
     * @return \G2\Entity\PrevendaProduto
     */
    public function setId_prevenda (Prevenda $id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
        return $this;
    }

}
<?php
namespace G2\Entity;

/**
 * Entity EntregaDocumentos
 * @author: Denise Xavier <denise.xavier@unyleya.com.br>
 * @since: 16/10/2014
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_entregadocumentos")
 * @Entity
 */
class EntregaDocumentos {


    /**
     * @var integer $id_entregadocumentos
     * @Column(name="id_entregadocumentos", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entregadocumentos;

    /**
     * @var Documentos $id_documentos
     * @ManyToOne(targetEntity="Documentos")
     * @JoinColumn(name="id_documentos", referencedColumnName="id_documentos")
     */
    private $id_documentos;


    /**
     * @var int $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var int $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;


    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;


    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;


    /**
     * @var int $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var ContratoResponsavel $id_contratoresponsavel
     * @ManyToOne(targetEntity="ContratoResponsavel")
     * @JoinColumn(name="id_contratoresponsavel", referencedColumnName="id_contratoresponsavel")
     */
    private $id_contratoresponsavel;

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }


    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }


    public function setId_contratoresponsavel($id_contratoresponsavel)
    {
        $this->id_contratoresponsavel = $id_contratoresponsavel;
    }


    public function getId_contratoresponsavel()
    {
        return $this->id_contratoresponsavel;
    }

    public function setId_documentos($id_documentos)
    {
        $this->id_documentos = $id_documentos;
    }

    public function getId_documentos()
    {
        return $this->id_documentos;
    }


    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entregadocumentos($id_entregadocumentos)
    {
        $this->id_entregadocumentos = $id_entregadocumentos;
    }

    public function getId_entregadocumentos()
    {
        return $this->id_entregadocumentos;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }




}

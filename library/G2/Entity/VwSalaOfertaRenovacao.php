<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_salaofertarenovacao")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwSalaOfertaRenovacao
{

    /**
     * @var datetime2 $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicioinscricao;
    /**
     * @var datetime2 $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_fiminscricao;
    /**
     * @var datetime2 $dt_abertura
     * @Column(name="dt_abertura", type="datetime2", nullable=false, length=8)
     */
    private $dt_abertura;
    /**
     * @var datetime2 $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2", nullable=false, length=8)
     */
    private $dt_encerramento;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @Id
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;
    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=true, length=4)
     */
    private $nu_creditos;
    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=false, length=4)
     */
    private $id_periodoletivo;
    /**
     * @var boolean $bl_ofertaexcepcional
     * @Column(name="bl_ofertaexcepcional", type="boolean", nullable=false, length=1)
     */
    private $bl_ofertaexcepcional;
    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_periodoletivo
     * @Column(name="st_periodoletivo", type="string", nullable=false, length=255)
     */
    private $st_periodoletivo;


    /**
     * @return datetime2 dt_inicioinscricao
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param dt_inicioinscricao
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return datetime2 dt_fiminscricao
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param dt_fiminscricao
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return datetime2 dt_abertura
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return datetime2 dt_encerramento
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_modulo
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param id_modulo
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param nu_maxalunos
     */
    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return integer id_trilha
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @param id_trilha
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_tipodisciplina
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return integer nu_creditos
     */
    public function getNu_creditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @param nu_creditos
     */
    public function setNu_creditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
        return $this;
    }

    /**
     * @return integer id_periodoletivo
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param id_periodoletivo
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return boolean bl_ofertaexcepcional
     */
    public function getBl_ofertaexcepcional()
    {
        return $this->bl_ofertaexcepcional;
    }

    /**
     * @param bl_ofertaexcepcional
     */
    public function setBl_ofertaexcepcional($bl_ofertaexcepcional)
    {
        $this->bl_ofertaexcepcional = $bl_ofertaexcepcional;
        return $this;
    }

    /**
     * @return string st_modulo
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @param st_modulo
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string st_periodoletivo
     */
    public function getSt_periodoletivo()
    {
        return $this->st_periodoletivo;
    }

    /**
     * @param st_periodoletivo
     */
    public function setSt_periodoletivo($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }




}
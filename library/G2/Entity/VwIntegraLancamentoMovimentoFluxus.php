<?php
namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity()
 * @Table( name="totvs.vw_Integra_lancamento_movimentoFluxus")
 * @HasLifecycleCallbacks
 */
class VwIntegraLancamentoMovimentoFluxus
{
    /**
     * @var integer $id_vendaintegracao
     * @id
     * @Column(name="idlan", type="integer", nullable=false)
     */
    private $idlan;

    /**
     * @var string $codcoligada
     * @Column(name="codcoligada", type="string", nullable=true)
     */
    private $codcoligada;

    /**
     * @var string $statusbaixa
     * @Column(name="statusbaixa", type="string", nullable=true)
     */
    private $statusbaixa;

    /**
     * @var string $id_venda
     * @Column(name="id_venda", type="string", nullable=true)
     */
    private $id_venda;

    /**
     * @var string $idmov
     * @Column(name="st_codvenda", type="string", nullable=true)
     */
    private $idmov;

    /**
     * @var string $id_lancamento
     * @Column(name="id_lancamento", type="string", nullable=true)
     */
    private $id_lancamento;

    /**
     * @return int
     */
    public function getIdlan()
    {
        return $this->idlan;
    }

    /**
     * @param int $idlan
     */
    public function setIdlan($idlan)
    {
        $this->idlan = $idlan;
    }

    /**
     * @return string
     */
    public function getCodcoligada()
    {
        return $this->codcoligada;
    }

    /**
     * @param string $codcoligada
     */
    public function setCodcoligada($codcoligada)
    {
        $this->codcoligada = $codcoligada;
    }

    /**
     * @return string
     */
    public function getStatusbaixa()
    {
        return $this->statusbaixa;
    }

    /**
     * @param string $statusbaixa
     */
    public function setStatusbaixa($statusbaixa)
    {
        $this->statusbaixa = $statusbaixa;
    }

    /**
     * @return string
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param string $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return string
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param string $id_lancamento
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return string
     */
    public function getIdmov()
    {
        return $this->idmov;
    }

    /**
     * @param string $idmov
     */
    public function setIdmov($idmov)
    {
        $this->idmov = $idmov;
    }

}
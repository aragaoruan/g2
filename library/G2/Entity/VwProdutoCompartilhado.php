<?php

namespace G2\Entity;

/**
 * Class VwProdutoCompartilhado
 * @package G2\Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtocompartilhado")
 * @Entity(repositoryClass="\G2\Repository\Produto")
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwProdutoCompartilhado
{

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_atualizado
     * @Column(name="dt_atualizado", type="datetime2", nullable=false, length=8)
     */
    private $dt_atualizado;

    /**
     * @var datetime2 $dt_iniciopontosprom
     * @Column(name="dt_iniciopontosprom", type="datetime2", nullable=true, length=8)
     */
    private $dt_iniciopontosprom;

    /**
     * @var datetime2 $dt_fimpontosprom
     * @Column(name="dt_fimpontosprom", type="datetime2", nullable=true, length=8)
     */
    private $dt_fimpontosprom;

    /**
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false, length=4)
     */
    private $id_tipoproduto;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_gratuito
     * @Column(name="nu_gratuito", type="integer", nullable=false, length=4)
     */
    private $nu_gratuito;

    /**
     * @var integer $nu_pontospromocional
     * @Column(name="nu_pontospromocional", type="integer", nullable=true, length=4)
     */
    private $nu_pontospromocional;

    /**
     * @var integer $id_modelovenda
     * @Column(name="id_modelovenda", type="integer", nullable=false, length=4)
     */
    private $id_modelovenda;

    /**
     * @var integer $id_produtovalorvenda
     * @Column(name="id_produtovalorvenda", type="integer", nullable=true, length=4)
     */
    private $id_produtovalorvenda;

    /**
     * @var integer $id_produtoimagempadrao
     * @Column(name="id_produtoimagempadrao", type="integer", nullable=true, length=4)
     */
    private $id_produtoimagempadrao;

    /**
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=true, length=4)
     */
    private $id_produtovalor;

    /**
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=true, length=4)
     */
    private $id_tipoprodutovalor;

    /**
     * @var integer $id_planopagamento
     * @Column(name="id_planopagamento", type="integer", nullable=true, length=4)
     */
    private $id_planopagamento;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=true, length=4)
     */
    private $nu_parcelas;

    /**
     * @var integer $nu_pontos
     * @Column(name="nu_pontos", type="integer", nullable=true, length=4)
     */
    private $nu_pontos;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false, length=1)
     */
    private $bl_todasformas;

    /**
     * @var boolean $bl_todascampanhas
     * @Column(name="bl_todascampanhas", type="boolean", nullable=false, length=1)
     */
    private $bl_todascampanhas;

    /**
     * @var boolean $bl_unico
     * @Column(name="bl_unico", type="boolean", nullable=false, length=1)
     */
    private $bl_unico;

    /**
     * @var boolean $bl_destaque
     * @Column(name="bl_destaque", type="boolean", nullable=false, length=1)
     */
    private $bl_destaque;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=true, length=9)
     */
    private $nu_valor;

    /**
     * @var decimal $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=true, length=9)
     */
    private $nu_valormensal;

    /**
     * @var decimal $nu_valorentrada
     * @Column(name="nu_valorentrada", type="decimal", nullable=true, length=17)
     */
    private $nu_valorentrada;

    /**
     * @var decimal $nu_valorparcela
     * @Column(name="nu_valorparcela", type="decimal", nullable=true, length=17)
     */
    private $nu_valorparcela;

    /**
     * @var decimal $nu_estoque
     * @Column(name="nu_estoque", type="decimal", nullable=true, length=17)
     */
    private $nu_estoque;

    /**
     * @var decimal $nu_valorvenda
     * @Column(name="nu_valorvenda", type="decimal", nullable=true, length=9)
     */
    private $nu_valorvenda;

    /**
     * @var decimal $nu_valorpromocional
     * @Column(name="nu_valorpromocional", type="decimal", nullable=true, length=9)
     */
    private $nu_valorpromocional;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;

    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true)
     */
    private $st_descricao;

    /**
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=true, length=255)
     */
    private $st_tipoproduto;

    /**
     * @var string $st_tipoprodutovalor
     * @Column(name="st_tipoprodutovalor", type="string", nullable=true, length=250)
     */
    private $st_tipoprodutovalor;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_subtitulo
     * @Column(name="st_subtitulo", type="string", nullable=true, length=300)
     */
    private $st_subtitulo;

    /**
     * @var string $st_slug
     * @Column(name="st_slug", type="string", nullable=true, length=200)
     */
    private $st_slug;

    /**
     * @var string $st_observacoes
     * @Column(name="st_observacoes", type="string", nullable=true)
     */
    private $st_observacoes;

    /**
     * @var string $st_informacoesadicionais
     * @Column(name="st_informacoesadicionais", type="string", nullable=true)
     */
    private $st_informacoesadicionais;

    /**
     * @var string $st_estruturacurricular
     * @Column(name="st_estruturacurricular", type="string", nullable=true)
     */
    private $st_estruturacurricular;

    /**
     * @var string $st_modelovenda
     * @Column(name="st_modelovenda", type="string", nullable=true, length=20)
     */
    private $st_modelovenda;

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime2 dt_atualizado
     */
    public function getDt_atualizado ()
    {
        return $this->dt_atualizado;
    }

    /**
     * @param dt_atualizado
     */
    public function setDt_atualizado ($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
        return $this;
    }

    /**
     * @return datetime2 dt_iniciopontosprom
     */
    public function getDt_iniciopontosprom ()
    {
        return $this->dt_iniciopontosprom;
    }

    /**
     * @param dt_iniciopontosprom
     */
    public function setDt_iniciopontosprom ($dt_iniciopontosprom)
    {
        $this->dt_iniciopontosprom = $dt_iniciopontosprom;
        return $this;
    }

    /**
     * @return datetime2 dt_fimpontosprom
     */
    public function getDt_fimpontosprom ()
    {
        return $this->dt_fimpontosprom;
    }

    /**
     * @param dt_fimpontosprom
     */
    public function setDt_fimpontosprom ($dt_fimpontosprom)
    {
        $this->dt_fimpontosprom = $dt_fimpontosprom;
        return $this;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * 
     * @param string $id_produto
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return integer id_tipoproduto
     */
    public function getId_tipoproduto ()
    {
        return $this->id_tipoproduto;
    }

    /**
     * 
     * @param integer $id_tipoproduto
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_tipoproduto ($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * 
     * @param integer $id_situacao
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * 
     * @param integer $id_usuariocadastro
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @param integer $id_entidade
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer nu_gratuito
     */
    public function getNu_gratuito ()
    {
        return $this->nu_gratuito;
    }

    /**
     * 
     * @param integer $nu_gratuito
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_gratuito ($nu_gratuito)
    {
        $this->nu_gratuito = $nu_gratuito;
        return $this;
    }

    /**
     * @return integer nu_pontospromocional
     */
    public function getNu_pontospromocional ()
    {
        return $this->nu_pontospromocional;
    }

    /**
     * 
     * @param integer $nu_pontospromocional
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_pontospromocional ($nu_pontospromocional)
    {
        $this->nu_pontospromocional = $nu_pontospromocional;
        return $this;
    }

    /**
     * @return integer id_modelovenda
     */
    public function getId_modelovenda ()
    {
        return $this->id_modelovenda;
    }

    /**
     * 
     * @param integer $id_modelovenda
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_modelovenda ($id_modelovenda)
    {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    /**
     * @return integer id_produtovalorvenda
     */
    public function getId_produtovalorvenda ()
    {
        return $this->id_produtovalorvenda;
    }

    /**
     * 
     * @param integer $id_produtovalorvenda
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_produtovalorvenda ($id_produtovalorvenda)
    {
        $this->id_produtovalorvenda = $id_produtovalorvenda;
        return $this;
    }

    /**
     * @return integer id_produtoimagempadrao
     */
    public function getId_produtoimagempadrao ()
    {
        return $this->id_produtoimagempadrao;
    }

    /**
     * 
     * @param integer $id_produtoimagempadrao
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_produtoimagempadrao ($id_produtoimagempadrao)
    {
        $this->id_produtoimagempadrao = $id_produtoimagempadrao;
        return $this;
    }

    /**
     * @return integer id_produtovalor
     */
    public function getId_produtovalor ()
    {
        return $this->id_produtovalor;
    }

    /**
     * 
     * @param integer $id_produtovalor
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_produtovalor ($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    /**
     * @return integer id_tipoprodutovalor
     */
    public function getId_tipoprodutovalor ()
    {
        return $this->id_tipoprodutovalor;
    }

    /**
     * 
     * @param integer $id_tipoprodutovalor
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_tipoprodutovalor ($id_tipoprodutovalor)
    {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    /**
     * @return integer id_planopagamento
     */
    public function getId_planopagamento ()
    {
        return $this->id_planopagamento;
    }

    /**
     * 
     * @param integer $id_planopagamento
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setId_planopagamento ($id_planopagamento)
    {
        $this->id_planopagamento = $id_planopagamento;
        return $this;
    }

    /**
     * @return integer nu_parcelas
     */
    public function getNu_parcelas ()
    {
        return $this->nu_parcelas;
    }

    /**
     * 
     * @param integer $nu_parcelas
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_parcelas ($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @return integer nu_pontos
     */
    public function getNu_pontos ()
    {
        return $this->nu_pontos;
    }

    /**
     * 
     * @param integer $nu_pontos
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_pontos ($nu_pontos)
    {
        $this->nu_pontos = $nu_pontos;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * 
     * @param boolean $bl_ativo
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return boolean bl_todasformas
     */
    public function getBl_todasformas ()
    {
        return $this->bl_todasformas;
    }

    /**
     * 
     * @param boolean $bl_todasformas
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setBl_todasformas ($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;
    }

    /**
     * @return boolean bl_todascampanhas
     */
    public function getBl_todascampanhas ()
    {
        return $this->bl_todascampanhas;
    }

    /**
     * 
     * @param boolean $bl_todascampanhas
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setBl_todascampanhas ($bl_todascampanhas)
    {
        $this->bl_todascampanhas = $bl_todascampanhas;
        return $this;
    }

    /**
     * @return boolean bl_unico
     */
    public function getBl_unico ()
    {
        return $this->bl_unico;
    }

    /**
     * 
     * @param boolean $bl_unico
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setBl_unico ($bl_unico)
    {
        $this->bl_unico = $bl_unico;
        return $this;
    }

    /**
     * @return boolean bl_destaque
     */
    public function getBl_destaque ()
    {
        return $this->bl_destaque;
    }

    /**
     * 
     * @param boolean $bl_destaque
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setBl_destaque ($bl_destaque)
    {
        $this->bl_destaque = $bl_destaque;
        return $this;
    }

    /**
     * @return decimal nu_valor
     */
    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    /**
     * 
     * @param decimal $nu_valor
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return decimal nu_valormensal
     */
    public function getNu_valormensal ()
    {
        return $this->nu_valormensal;
    }

    /**
     * 
     * @param decimal $nu_valormensal
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valormensal ($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    /**
     * @return decimal nu_valorentrada
     */
    public function getNu_valorentrada ()
    {
        return $this->nu_valorentrada;
    }

    /**
     * 
     * @param decimal $nu_valorentrada
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valorentrada ($nu_valorentrada)
    {
        $this->nu_valorentrada = $nu_valorentrada;
        return $this;
    }

    /**
     * @return decimal nu_valorparcela
     */
    public function getNu_valorparcela ()
    {
        return $this->nu_valorparcela;
    }

    /**
     * 
     * @param decimal $nu_valorparcela
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valorparcela ($nu_valorparcela)
    {
        $this->nu_valorparcela = $nu_valorparcela;
        return $this;
    }

    /**
     * @return decimal nu_estoque
     */
    public function getNu_estoque ()
    {
        return $this->nu_estoque;
    }

    /**
     * 
     * @param decimal $nu_estoque
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_estoque ($nu_estoque)
    {
        $this->nu_estoque = $nu_estoque;
        return $this;
    }

    /**
     * @return decimal nu_valorvenda
     */
    public function getNu_valorvenda ()
    {
        return $this->nu_valorvenda;
    }

    /**
     * 
     * @param decimal $nu_valorvenda
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valorvenda ($nu_valorvenda)
    {
        $this->nu_valorvenda = $nu_valorvenda;
        return $this;
    }

    /**
     * @return decimal nu_valorpromocional
     */
    public function getNu_valorpromocional ()
    {
        return $this->nu_valorpromocional;
    }

    /**
     * 
     * @param decimal $nu_valorpromocional
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setNu_valorpromocional ($nu_valorpromocional)
    {
        $this->nu_valorpromocional = $nu_valorpromocional;
        return $this;
    }

    /**
     * @return string st_produto
     */
    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    /**
     * 
     * @param string $st_produto
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @return string st_descricao
     */
    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    /**
     * 
     * @param string $st_descricao
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return string st_tipoproduto
     */
    public function getSt_tipoproduto ()
    {
        return $this->st_tipoproduto;
    }

    /**
     * 
     * @param string $st_tipoproduto
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_tipoproduto ($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    /**
     * @return string st_tipoprodutovalor
     */
    public function getSt_tipoprodutovalor ()
    {
        return $this->st_tipoprodutovalor;
    }

    /**
     * 
     * @param string $st_tipoprodutovalor
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_tipoprodutovalor ($st_tipoprodutovalor)
    {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    /**
     * 
     * @param string $st_situacao
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    /**
     * 
     * @param string $st_nomeentidade
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_subtitulo
     */
    public function getSt_subtitulo ()
    {
        return $this->st_subtitulo;
    }

    /**
     * 
     * @param string $st_subtitulo
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_subtitulo ($st_subtitulo)
    {
        $this->st_subtitulo = $st_subtitulo;
        return $this;
    }

    /**
     * @return string st_slug
     */
    public function getSt_slug ()
    {
        return $this->st_slug;
    }

    /**
     * 
     * @param string $st_slug
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_slug ($st_slug)
    {
        $this->st_slug = $st_slug;
        return $this;
    }

    /**
     * @return string st_observacoes
     */
    public function getSt_observacoes ()
    {
        return $this->st_observacoes;
    }

    /**
     * 
     * @param string $st_observacoes
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_observacoes ($st_observacoes)
    {
        $this->st_observacoes = $st_observacoes;
        return $this;
    }

    /**
     * @return string st_informacoesadicionais
     */
    public function getSt_informacoesadicionais ()
    {
        return $this->st_informacoesadicionais;
    }

    /**
     * 
     * @param string $st_informacoesadicionais
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_informacoesadicionais ($st_informacoesadicionais)
    {
        $this->st_informacoesadicionais = $st_informacoesadicionais;
        return $this;
    }

    /**
     * @return string st_estruturacurricular
     */
    public function getSt_estruturacurricular ()
    {
        return $this->st_estruturacurricular;
    }

    /**
     * 
     * @param string $st_estruturacurricular
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_estruturacurricular ($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
        return $this;
    }

    /**
     * @return string st_modelovenda
     */
    public function getSt_modelovenda ()
    {
        return $this->st_modelovenda;
    }

    /**
     * 
     * @param string $st_modelovenda
     * @return \G2\Entity\VwProdutoCompartilhado
     */
    public function setSt_modelovenda ($st_modelovenda)
    {
        $this->st_modelovenda = $st_modelovenda;
        return $this;
    }

}

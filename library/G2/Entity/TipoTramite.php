<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipotramite")
 * @Entity
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class TipoTramite extends G2Entity
{
    /**
     * @Id
     * @var integer $id_tipotramite
     * @Column(name="id_tipotramite", type="integer", nullable=false, length=4)
     */
    private $id_tipotramite;

    /**
     * @var integer $id_categoriatramite
     * @Column(name="id_categoriatramite", type="integer", nullable=false, length=4)
     */
    private $id_categoriatramite;

    /**
     * @var string $st_tipotramite
     * @Column(name="st_tipotramite", type="string", nullable=false, length=30)
     */
    private $st_tipotramite;

    /**
     * @return int
     */
    public function getId_tipotramite()
    {
        return $this->id_tipotramite;
    }

    /**
     * @param int $id_tipotramite
     */
    public function setId_tipotramite($id_tipotramite)
    {
        $this->id_tipotramite = $id_tipotramite;
    }

    /**
     * @return int
     */
    public function getId_categoriatramite()
    {
        return $this->id_categoriatramite;
    }

    /**
     * @param int $id_categoriatramite
     */
    public function setId_categoriatramite($id_categoriatramite)
    {
        $this->id_categoriatramite = $id_categoriatramite;
    }

    /**
     * @return string
     */
    public function getSt_tipotramite()
    {
        return $this->st_tipotramite;
    }

    /**
     * @param string $st_tipotramite
     */
    public function setSt_tipotramite($st_tipotramite)
    {
        $this->st_tipotramite = $st_tipotramite;
    }


}

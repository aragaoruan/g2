<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_cargahoraria")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class CargaHoraria {

    /**
     *
     * @var integer
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cargahoraria;

    /**
     * @var integer
     * @Column(type="integer", nullable=false)
     */
    private $nu_cargahoraria;

    public function getId_cargahoraria() {
        return $this->id_cargahoraria;
    }

    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    public function setId_cargahoraria($id_cargahoraria) {
        $this->id_cargahoraria = $id_cargahoraria;
    }

    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_professor")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwProfessor {

    /**
     * @Column(type="integer")
     * @Id
     */
    private $id_usuario;

    /**
     * @Column(type="integer")
     */
    private $id_entidade;

    /**
     * @Column(type="string")
     */
    private $st_nomecompleto;

    /**
     * @Column(type="string")
     */
    private $st_cpf;

    /**
     * @Column(type="integer")
     */
    private $id_perfil;

    /**
     * @Column(type="string")
     */
    private $st_nomeperfil;

    /**
     * @Column(type="string")
     */
    private $st_nomeentidade;

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_cpf() {
        return $this->st_cpf;
    }

    public function getId_perfil() {
        return $this->id_perfil;
    }

    public function getSt_nomeperfil() {
        return $this->st_nomeperfil;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    public function setSt_nomeperfil($st_nomeperfil) {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

}

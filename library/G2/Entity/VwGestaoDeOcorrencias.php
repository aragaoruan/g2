<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Table (name="vw_gestaodeocorrencias")
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @EntityView
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class VwGestaoDeOcorrencias extends G2Entity {

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var datetime2 $dt_ultimotramite
     * @Column(name="dt_ultimotramite", type="datetime2", nullable=true, length=8)
     */
    private $dt_ultimotramite;
    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false, length=4)
     * @id
     */
    private $id_ocorrencia;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_usuariointeressado
     * @Column(name="id_usuariointeressado", type="integer", nullable=false, length=4)
     */
    private $id_usuariointeressado;
    /**
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_categoriaocorrencia;
    /**
     * @var integer $id_ocorrenciaoriginal
     * @Column(name="id_ocorrenciaoriginal", type="integer", nullable=true, length=4)
     */
    private $id_ocorrenciaoriginal;
    /**
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=true, length=4)
     */
    private $id_nucleoco;
    /**
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=true, length=4)
     */
    private $id_assuntoco;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_tipoocorrencia;
    /**
     * @var integer $id_usuarioresponsavel
     * @Column(name="id_usuarioresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_usuarioresponsavel;
    /**
     * @var integer $id_evolucaomatricula
     * @Column(name="id_evolucaomatricula", type="integer", nullable=true, length=4)
     */
    private $id_evolucaomatricula;
    /**
     * @var datetime $dt_atendimento
     * @Column(name="dt_atendimento", type="datetime", nullable=true, length=8)
     */
    private $dt_atendimento;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_nomeinteressado
     * @Column(name="st_nomeinteressado", type="string", nullable=false, length=300)
     */
    private $st_nomeinteressado;
    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;
    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=32)
     */
    private $st_senha;
    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false, length=40)
     */
    private $st_login;
    /**
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string", nullable=true, length=61)
     */
    private $st_telefone;
    /**
     * @var string $st_ultimotramite
     * @Column(name="st_ultimotramite", type="string", nullable=true)
     */
    private $st_ultimotramite;
    /**
     * @var string $st_tramite
     * @Column(name="st_tramite", type="string", nullable=true)
     */
    private $st_tramite;
    /**
     * @var string $st_nomeresponsavel
     * @Column(name="st_nomeresponsavel", type="string", nullable=true, length=300)
     */
    private $st_nomeresponsavel;
    /**
     * @var string $st_assuntocopai
     * @Column(name="st_assuntocopai", type="string", nullable=true, length=200)
     */
    private $st_assuntocopai;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=false)
     */
    private $st_titulo;
    /**
     * @var string $st_ocorrencia
     * @Column(name="st_ocorrencia", type="string", nullable=false)
     */
    private $st_ocorrencia;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;
    /**
     * @var string $st_categoriaocorrencia
     * @Column(name="st_categoriaocorrencia", type="string", nullable=false, length=250)
     */
    private $st_categoriaocorrencia;
    /**
     * @var string $st_cadastro
     * @Column(name="st_cadastro", type="string", nullable=true)
     */
    private $st_cadastro;

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return datetime2 dt_ultimotramite
     */
    public function getDt_ultimotramite() {
        return $this->dt_ultimotramite;
    }

    /**
     * @return integer id_ocorrencia
     */
    public function getId_ocorrencia() {
        return $this->id_ocorrencia;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @return integer id_usuariointeressado
     */
    public function getId_usuariointeressado() {
        return $this->id_usuariointeressado;
    }

    /**
     * @return integer id_categoriaocorrencia
     */
    public function getId_categoriaocorrencia() {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @return integer id_ocorrenciaoriginal
     */
    public function getId_ocorrenciaoriginal() {
        return $this->id_ocorrenciaoriginal;
    }

    /**
     * @return integer id_nucleoco
     */
    public function getId_nucleoco() {
        return $this->id_nucleoco;
    }

    /**
     * @return integer id_assuntoco
     */
    public function getId_assuntoco() {
        return $this->id_assuntoco;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return integer id_tipoocorrencia
     */
    public function getId_tipoocorrencia() {
        return $this->id_tipoocorrencia;
    }

    /**
     * @return integer id_usuarioresponsavel
     */
    public function getId_usuarioresponsavel() {
        return $this->id_usuarioresponsavel;
    }

    /**
     * @return integer id_evolucaomatricula
     */
    public function getId_evolucaomatricula() {
        return $this->id_evolucaomatricula;
    }

    /**
     * @return datetime dt_atendimento
     */
    public function getDt_atendimento() {
        return $this->dt_atendimento;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @return string st_nomeinteressado
     */
    public function getSt_nomeinteressado() {
        return $this->st_nomeinteressado;
    }

    /**
     * @return string st_email
     */
    public function getSt_email() {
        return $this->st_email;
    }

    /**
     * @return string st_senha
     */
    public function getSt_senha() {
        return $this->st_senha;
    }

    /**
     * @return string st_login
     */
    public function getSt_login() {
        return $this->st_login;
    }

    /**
     * @return string st_telefone
     */
    public function getSt_telefone() {
        return $this->st_telefone;
    }

    /**
     * @return string st_ultimotramite
     */
    public function getSt_ultimotramite() {
        return $this->st_ultimotramite;
    }

    /**
     * @return string st_tramite
     */
    public function getSt_tramite() {
        return $this->st_tramite;
    }

    /**
     * @return string st_nomeresponsavel
     */
    public function getSt_nomeresponsavel() {
        return $this->st_nomeresponsavel;
    }

    /**
     * @return string st_assuntocopai
     */
    public function getSt_assuntocopai() {
        return $this->st_assuntocopai;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @return string st_titulo
     */
    public function getSt_titulo() {
        return $this->st_titulo;
    }

    /**
     * @return string st_ocorrencia
     */
    public function getSt_ocorrencia() {
        return $this->st_ocorrencia;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @return string st_assuntoco
     */
    public function getSt_assuntoco() {
        return $this->st_assuntoco;
    }

    /**
     * @return string st_categoriaocorrencia
     */
    public function getSt_categoriaocorrencia() {
        return $this->st_categoriaocorrencia;
    }

    /**
     * @return string st_cadastro
     */
    public function getSt_cadastro() {
        return $this->st_cadastro;
    }

}
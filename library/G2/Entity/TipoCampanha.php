<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipocampanha")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoCampanha
{

    /**
     *
     * @var integer $id_tipocampanha
     * @Column(name="id_tipocampanha", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipocampanha;

    /**
     *
     * @var string $st_tipocampanha
     * @Column(name="st_tipocampanha", type="string", nullable=true, length=250)
     */
    private $st_tipocampanha;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    public function getId_tipocampanha ()
    {
        return $this->id_tipocampanha;
    }

    public function setId_tipocampanha ($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;
    }

    public function getSt_tipocampanha ()
    {
        return $this->st_tipocampanha;
    }

    public function setSt_tipocampanha ($st_tipocampanha)
    {
        $this->st_tipocampanha = $st_tipocampanha;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
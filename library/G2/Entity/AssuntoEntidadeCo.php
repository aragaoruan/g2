<?php
namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_assuntoentidadeco")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AssuntoEntidadeCo extends G2Entity
{

    /**
     * @Id
     * @var integer $id_assuntoentidadeco
     * @Column(name="id_assuntoentidadeco", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_assuntoentidadeco;

    /**
     * @var integer $id_assuntoco
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoco", nullable=false, referencedColumnName="id_assuntoco")
     */
    private $id_assuntoco;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade" , nullable=false)
     */
    private $id_entidade;

    public function getId_assuntoentidadeco() {
        return $this->id_assuntoentidadeco;
    }

    public function getId_assuntoco() {
        return $this->id_assuntoco;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_assuntoentidadeco($id_assuntoentidadeco) {
        $this->id_assuntoentidadeco = $id_assuntoentidadeco;
    }

    public function setId_assuntoco($id_assuntoco) {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }


}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_motivo")
 * @Entity(repositoryClass = "\G2\Repository\Motivo")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Motivo extends G2Entity
{
    /**
     * @Id
     * @var integer $id_motivo
     * @Column(name="id_motivo", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_motivo;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_motivo
     * @Column(name="st_motivo", type="string", nullable=true, length=255)
     */
    private $st_motivo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var LinhaDeNegocio $id_linhadenegocio
     * @ManyToOne(targetEntity="LinhaDeNegocio")
     * @JoinColumn(name="id_linhadenegocio", referencedColumnName="id_linhadenegocio")
     */
    private $id_linhadenegocio;

    /**
     * @return integer id_motivo
     */
    public function getId_motivo() {
        return $this->id_motivo;
    }

    /**
     * @param id_motivo
     */
    public function setId_motivo($id_motivo) {
        $this->id_motivo = $id_motivo;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_motivo
     */
    public function getSt_motivo() {
        return $this->st_motivo;
    }

    /**
     * @param st_motivo
     */
    public function setSt_motivo($st_motivo) {
        $this->st_motivo = $st_motivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return LinhaDeNegocio
     */
    public function getId_linhadenegocio()
    {
        return $this->id_linhadenegocio;
    }

    /**
     * @param int $id_linhadenegocio
     */
    public function setId_linhadenegocio($id_linhadenegocio)
    {
        $this->id_linhadenegocio = $id_linhadenegocio;
    }

}

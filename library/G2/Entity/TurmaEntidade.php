<?php
/**
 * Created by PhpStorm.
 * User: helder.silva
 * Date: 29/08/14
 * Time: 09:22
 */

namespace G2\Entity;

/**
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmaentidade")
 */
class TurmaEntidade{

    /**
     * @Id
     * @var integer $id_turmaentidade
     * @Column(name="id_turmaentidade", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turmaentidade;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;


    public function setBl_Ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


    public function getBl_Ativo()
    {
        return $this->bl_ativo;
    }

    public function setDt_Cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getDt_Cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setId_Entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_Entidade()
    {
        return $this->id_entidade;
    }

    public function setId_Turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    public function getId_Turma()
    {
        return $this->id_turma;
    }

    public function setId_Turmaentidade($id_turmaentidade)
    {
        $this->id_turmaentidade = $id_turmaentidade;
        return $this;
    }

    public function getId_Turmaentidade()
    {
        return $this->id_turmaentidade;
    }

    /**
     * @return \G2\Entity\Usuario id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\TurmaEntidade
     */
    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

}
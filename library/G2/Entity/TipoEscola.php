<?php

namespace G2\Entity;
use G2\G2Entity;

/** ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipoescola")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class TipoEscola extends G2Entity {
    /**
     * @id
     * @var integer $id_tipoescola
     * @Column(name="id_tipoescola", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoescola;
    /**
     * @var string $st_tipoescola
     * @Column(name="st_tipoescola", type="string", nullable=false, length=50)
     */
    private $st_tipoescola;

    /**
     * @return int
     */
    public function getId_tipoescola()
    {
        return $this->id_tipoescola;
    }

    /**
     * @param int $id_tipoescola
     */
    public function setId_tipoescola($id_tipoescola)
    {
        $this->id_tipoescola = $id_tipoescola;
    }

    /**
     * @return string
     */
    public function getSt_tipoescola()
    {
        return $this->st_tipoescola;
    }

    /**
     * @param string $st_tipoescola
     */
    public function setSt_tipoescola($st_tipoescola)
    {
        $this->st_tipoescola = $st_tipoescola;
    }

}
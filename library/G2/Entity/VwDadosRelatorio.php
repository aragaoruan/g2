<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_dadosrelatorio")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwDadosRelatorio
{

    /**
     *
     * @var integer $id_relatorio
     * @Column(name="id_relatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_relatorio;

    /**
     *
     * @var integer $id_tipoorigemrelatorio
     * @Column(name="id_tipoorigemrelatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipoorigemrelatorio;

    /**
     *
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_funcionalidade;

    /**
     *
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     *
     * @var string $st_relatorio
     * @Column(name="st_relatorio", type="string", nullable=false, length=250)
     */
    private $st_relatorio;

    /**
     *
     * @var text $st_origem
     * @Column(name="st_origem", type="text", nullable=true)
     */
    private $st_origem;

    /**
     *
     * @var boolean $bl_geral
     * @Column(name="bl_geral", type="boolean", nullable=false)
     */
    private $bl_geral;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=false)
     */
    private $st_descricao;

    /**
     *
     * @var integer $id_camporelatorio
     * @Column(name="id_camporelatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_camporelatorio;

    /**
     *
     * @var string $st_camporelatorio
     * @Column(name="st_camporelatorio", type="string", nullable=false, length=150)
     */
    private $st_camporelatorio;

    /**
     *
     * @var string $st_titulocampo
     * @Column(name="st_titulocampo", type="string", nullable=false, length=150)
     */
    private $st_titulocampo;

    /**
     *
     * @var boolean $bl_filtro
     * @Column(name="bl_filtro", type="boolean", nullable=false)
     */
    private $bl_filtro;

    /**
     *
     * @var boolean $bl_exibido
     * @Column(name="bl_exibido", type="boolean", nullable=false)
     */
    private $bl_exibido;

    /**
     *
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio", type="boolean", nullable=false)
     */
    private $bl_obrigatorio;

    /**
     *
     * @var integer $id_operacaorelatorio
     * @Column(name="id_operacaorelatorio", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_operacaorelatorio;

    /**
     *
     * @var integer $id_tipocampo
     * @Column(name="id_tipocampo", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipocampo;

    /**
     *
     * @var integer $id_interface
     * @Column(name="id_interface", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_interface;

    /**
     *
     * @var integer $id_tipopropriedadecamporel
     * @Column(name="id_tipopropriedadecamporel", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipopropriedadecamporel;

    /**
     *
     * @var string $st_tipopropriedadecamporel
     * @Column(name="st_tipopropriedadecamporel", type="string", nullable=true,length=150)
     */
    private $st_tipopropriedadecamporel;

    /**
     *
     * @var text $st_valor
     * @Column(name="st_valor", type="text", nullable=true)
     */
    private $st_valor;

    public function getBl_obrigatorio ()
    {
        return $this->bl_obrigatorio;
    }

    public function setBl_obrigatorio ($bl_obrigatorio)
    {
        $this->bl_obrigatorio = $bl_obrigatorio;
        return $this;
    }

    public function getId_relatorio ()
    {
        return $this->id_relatorio;
    }

    public function setId_relatorio ($id_relatorio)
    {
        $this->id_relatorio = $id_relatorio;
        return $this;
    }

    public function getId_tipoorigemrelatorio ()
    {
        return $this->id_tipoorigemrelatorio;
    }

    public function setId_tipoorigemrelatorio ($id_tipoorigemrelatorio)
    {
        $this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
        return $this;
    }

    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function getNu_ordem ()
    {
        return $this->nu_ordem;
    }

    public function setNu_ordem ($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_relatorio ()
    {
        return $this->st_relatorio;
    }

    public function setSt_relatorio ($st_relatorio)
    {
        $this->st_relatorio = $st_relatorio;
        return $this;
    }

    public function getSt_origem ()
    {
        return $this->st_origem;
    }

    public function setSt_origem ($st_origem)
    {
        $this->st_origem = $st_origem;
        return $this;
    }

    public function getBl_geral ()
    {
        return $this->bl_geral;
    }

    public function setBl_geral ($bl_geral)
    {
        $this->bl_geral = $bl_geral;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getId_camporelatorio ()
    {
        return $this->id_camporelatorio;
    }

    public function setId_camporelatorio ($id_camporelatorio)
    {
        $this->id_camporelatorio = $id_camporelatorio;
        return $this;
    }

    public function getSt_camporelatorio ()
    {
        return $this->st_camporelatorio;
    }

    public function setSt_camporelatorio ($st_camporelatorio)
    {
        $this->st_camporelatorio = $st_camporelatorio;
        return $this;
    }

    public function getSt_titulocampo ()
    {
        return $this->st_titulocampo;
    }

    public function setSt_titulocampo ($st_titulocampo)
    {
        $this->st_titulocampo = $st_titulocampo;
        return $this;
    }

    public function getBl_filtro ()
    {
        return $this->bl_filtro;
    }

    public function setBl_filtro ($bl_filtro)
    {
        $this->bl_filtro = $bl_filtro;
        return $this;
    }

    public function getBl_exibido ()
    {
        return $this->bl_exibido;
    }

    public function setBl_exibido ($bl_exibido)
    {
        $this->bl_exibido = $bl_exibido;
        return $this;
    }

    public function getId_operacaorelatorio ()
    {
        return $this->id_operacaorelatorio;
    }

    public function setId_operacaorelatorio ($id_operacaorelatorio)
    {
        $this->id_operacaorelatorio = $id_operacaorelatorio;
        return $this;
    }

    public function getId_tipocampo ()
    {
        return $this->id_tipocampo;
    }

    public function setId_tipocampo ($id_tipocampo)
    {
        $this->id_tipocampo = $id_tipocampo;
        return $this;
    }

    public function getId_interface ()
    {
        return $this->id_interface;
    }

    public function setId_interface ($id_interface)
    {
        $this->id_interface = $id_interface;
        return $this;
    }

    public function getId_tipopropriedadecamporel ()
    {
        return $this->id_tipopropriedadecamporel;
    }

    public function setId_tipopropriedadecamporel ($id_tipopropriedadecamporel)
    {
        $this->id_tipopropriedadecamporel = $id_tipopropriedadecamporel;
        return $this;
    }

    public function getSt_tipopropriedadecamporel ()
    {
        return $this->st_tipopropriedadecamporel;
    }

    public function setSt_tipopropriedadecamporel ($st_tipopropriedadecamporel)
    {
        $this->st_tipopropriedadecamporel = $st_tipopropriedadecamporel;
        return $this;
    }

    public function getSt_valor ()
    {
        return $this->st_valor;
    }

    public function setSt_valor ($st_valor)
    {
        $this->st_valor = $st_valor;
        return $this;
    }

}
<?php

namespace G2\Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="rel.vw_alunosforarenovacao")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwAlunosForaRenovacao")
 * @author Janilson Mendes <janilson.silva@unyleya.com.br>
 */
class VwAlunosForaRenovacao
{
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @var integer $nu_ddd
     * @Column(name="nu_ddd", type="integer", nullable=true, length=3)
     */
    private $nu_ddd;

    /**
     * @var integer $nu_telefone
     * @Column(name="nu_telefone", type="integer", nullable=true, length=15)
     */
    private $nu_telefone;

    /**
     * @var integer $nu_dddalternativo
     * @Column(name="nu_dddalternativo", type="integer", nullable=true, length=3)
     */
    private $nu_dddalternativo;

    /**
     * @var integer $nu_telefonealternativo
     * @Column(name="nu_telefonealternativo", type="integer", nullable=true, length=15)
     */
    private $nu_telefonealternativo;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", nullable=false, type="integer")
     */
    private $id_matricula;

    /**
     * @var datetime $dt_matricula
     * @Column(name="dt_matricula", type="datetime", nullable=false)
     */
    private $dt_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", nullable=false, type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var string $st_outrosmotivos
     * @Column(name="st_outrosmotivos", type="string", nullable=true, length=300)
     */
    private $st_outrosmotivos;

    /**
     * @var boolean $bl_motivodocumentacao
     * @Column(name="bl_motivodocumentacao", type="boolean", nullable=false)
     */
    private $bl_motivodocumentacao;

    /**
     * @var string $st_motivodocumentacao
     * @Column(name="st_motivodocumentacao", type="string", nullable=false, length=6)
     */
    private $st_motivodocumentacao;

    /**
     * @var boolean $bl_motivoacademico
     * @Column(name="bl_motivoacademico", type="boolean", nullable=false)
     */
    private $bl_motivoacademico;

    /**
     * @var string $st_motivoacademico
     * @Column(name="st_motivoacademico", type="string", nullable=false, length=6)
     */
    private $st_motivoacademico;

    /**
     * @var boolean $bl_motivofinanceiro
     * @Column(name="bl_motivofinanceiro", type="boolean", nullable=false)
     */
    private $bl_motivofinanceiro;

    /**
     * @var string $st_motivofinanceiro
     * @Column(name="st_motivofinanceiro", type="string", nullable=false, length=6)
     */
    private $st_motivofinanceiro;

    /**
     * @var datetime $dt_ultimaoferta
     * @Column(name="dt_ultimaoferta", type="datetime", nullable=true)
     */
    private $dt_ultimaoferta;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", nullable=false, type="integer")
     */
    private $id_venda;

    /**
     * @var integer $nu_totalocorrencia
     * @Column(name="nu_totalocorrencia", nullable=false, type="integer")
     */
    private $nu_totalocorrencia;

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAlunosForaRenovacao
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwAlunosForaRenovacao
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return VwAlunosForaRenovacao
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     * @return VwAlunosForaRenovacao
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    /**
     * @param int $nu_ddd
     * @return VwAlunosForaRenovacao
     */
    public function setNu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @param int $nu_telefone
     * @return VwAlunosForaRenovacao
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    /**
     * @param int $nu_dddalternativo
     * @return VwAlunosForaRenovacao
     */
    public function setNu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    /**
     * @param int $nu_telefonealternativo
     * @return VwAlunosForaRenovacao
     */
    public function setNu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAlunosForaRenovacao
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param datetime $dt_matricula
     * @return VwAlunosForaRenovacao
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwAlunosForaRenovacao
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return VwAlunosForaRenovacao
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwAlunosForaRenovacao
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_outrosmotivos()
    {
        return $this->st_outrosmotivos;
    }

    /**
     * @param string $st_outrosmotivos
     * @return VwAlunosForaRenovacao
     */
    public function setSt_outrosmotivos($st_outrosmotivos)
    {
        $this->st_outrosmotivos = $st_outrosmotivos;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivodocumentacao()
    {
        return $this->bl_motivodocumentacao;
    }

    /**
     * @param bool $bl_motivodocumentacao
     * @return VwAlunosForaRenovacao
     */
    public function setBl_motivodocumentacao($bl_motivodocumentacao)
    {
        $this->bl_motivodocumentacao = $bl_motivodocumentacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivodocumentacao()
    {
        return $this->st_motivodocumentacao;
    }

    /**
     * @param string $st_motivodocumentacao
     * @return VwAlunosForaRenovacao
     */
    public function setSt_motivodocumentacao($st_motivodocumentacao)
    {
        $this->st_motivodocumentacao = $st_motivodocumentacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivoacademico()
    {
        return $this->bl_motivoacademico;
    }

    /**
     * @param bool $bl_motivoacademico
     * @return VwAlunosForaRenovacao
     */
    public function setBl_motivoacademico($bl_motivoacademico)
    {
        $this->bl_motivoacademico = $bl_motivoacademico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivoacademico()
    {
        return $this->st_motivoacademico;
    }

    /**
     * @param string $st_motivoacademico
     * @return VwAlunosForaRenovacao
     */
    public function setSt_motivoacademico($st_motivoacademico)
    {
        $this->st_motivoacademico = $st_motivoacademico;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_motivofinanceiro()
    {
        return $this->bl_motivofinanceiro;
    }

    /**
     * @param bool $bl_motivofinanceiro
     * @return VwAlunosForaRenovacao
     */
    public function setBl_motivofinanceiro($bl_motivofinanceiro)
    {
        $this->bl_motivofinanceiro = $bl_motivofinanceiro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivofinanceiro()
    {
        return $this->st_motivofinanceiro;
    }

    /**
     * @param string $st_motivofinanceiro
     * @return VwAlunosForaRenovacao
     */
    public function setSt_motivofinanceiro($st_motivofinanceiro)
    {
        $this->st_motivofinanceiro = $st_motivofinanceiro;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param datetime $dt_ultimaoferta
     * @return VwAlunosForaRenovacao
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     * @return VwAlunosForaRenovacao
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_totalocorrencia()
    {
        return $this->nu_totalocorrencia;
    }

    /**
     * @param int $nu_totalocorrencia
     * @return VwAlunosForaRenovacao
     */
    public function setNu_totalocorrencia($nu_totalocorrencia)
    {
        $this->nu_totalocorrencia = $nu_totalocorrencia;
        return $this;
    }
}

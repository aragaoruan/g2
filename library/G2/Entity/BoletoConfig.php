<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_boletoconfig")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class BoletoConfig extends G2Entity
{

    /**
     *
     * @var integer $id_boletoconfig
     * @Column(name="id_boletoconfig", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_boletoconfig;

    /**
     *
     * @var ContaEntidade $id_contaentidade
     * @ManyToOne(targetEntity="ContaEntidade")
     * @JoinColumn(name="id_contaentidade", nullable=false, referencedColumnName="id_contaentidade")
     */
    private $id_contaentidade;

    /**
     *
     * @var EntidadedeEndereco $id_entidadeendereco
     * @ManyToOne(targetEntity="EntidadedeEndereco")
     * @JoinColumn(name="id_entidadeendereco", nullable=true, referencedColumnName="id_entidadeendereco")
     */
    private $id_entidadeendereco;

    /**
     *
     * @var string $nu_carteira
     * @Column(name="nu_carteira", type="string", nullable=false, length=6)
     */
    private $nu_carteira;

    /**
     *
     * @var string $st_contacedente
     * @Column(name="st_contacedente", type="string", nullable=true, length=15)
     */
    private $st_contacedente;

    /**
     *
     * @var string $st_digitocedente
     * @Column(name="st_digitocedente", type="string", nullable=true, length=2)
     */
    private $st_digitocedente;

    /**
     *
     * @var $entidadeFinaceira
     * @ManyToMany(targetEntity="EntidadeFinanceiro", mappedBy="boletoConfig")
     */
    private $entidadeFinaceira;

    public function getId_boletoconfig()
    {
        return $this->id_boletoconfig;
    }

    public function setId_boletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
        return $this;
    }

    public function getId_contaentidade()
    {
        return $this->id_contaentidade;
    }

    public function setId_contaentidade(ContaEntidade $id_contaentidade)
    {
        $this->id_contaentidade = $id_contaentidade;
        return $this;
    }

    public function getId_entidadeendereco()
    {
        return $this->id_entidadeendereco;
    }

    public function setId_entidadeendereco(EntidadedeEndereco $id_entidadeendereco)
    {
        $this->id_entidadeendereco = $id_entidadeendereco;
        return $this;
    }

    public function getNu_carteira()
    {
        return $this->nu_carteira;
    }

    public function setNu_carteira($nu_carteira)
    {
        $this->nu_carteira = $nu_carteira;
        return $this;
    }

    public function getSt_contacedente()
    {
        return $this->st_contacedente;
    }

    public function setSt_contacedente($st_contacedente)
    {
        $this->st_contacedente = $st_contacedente;
        return $this;
    }

    public function getSt_digitocedente()
    {
        return $this->st_digitocedente;
    }

    public function setSt_digitocedente($st_digitocedente)
    {
        $this->st_digitocedente = $st_digitocedente;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladisciplinaparatransferencia")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwSalaDisciplinaParaTransferencia
{

    /**
     * @var integer $id_saladeaula
     * @Column(type="integer", nullable=false, name="id_saladeaula")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;

    /**
     * @var string $st_saladeaula
     * @Column(type="string", length=255, nullable=false, name="st_saladeaula")
     */
    private $st_saladeaula;


    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false, name="id_projetopedagogico")
     */
    private $id_projetopedagogico;


    /**
     * @var integer $id_projetotransfere
     * @Column(type="integer", nullable=false, name="id_projetotransfere")
     */
    private $id_projetotransfere;


    /**
     * @var integer $id_entidadetransfere
     * @Column(type="integer", nullable=false, name="id_entidadetransfere")
     */
    private $id_entidadetransfere;


    /**
     * @var integer $id_encerramentosala
     * @Column(type="integer", nullable=true, name="id_encerramentosala")
     */
    private $id_encerramentosala;



    /**
     * @var integer $id_matricula
     * @Column(type="integer", nullable=false, name="id_matricula")
     */
    private $id_matricula;


    /**
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     */
    private $id_disciplina;



    /**
     * @var string $st_disciplina;
     * @Column(type="string", length=255, nullable=false, name="st_disciplina")
     */
    private $st_disciplina;

    /**
     * @var string $st_notaatividade;
     * @Column(type="string", length=5, nullable=true, name="st_notaatividade")
     */
    private $st_notaatividade;

    /**
     * @var string $st_notaead;
     * @Column(type="string", length=5, nullable=true, name="st_notaead")
     */
    private $st_notaead;


    /**
     * @var string $st_notatcc;
     * @Column(type="string", length=5, nullable=true, name="st_notatcc")
     */
    private $st_notatcc;


    /**
     * @var string $st_notafinal;
     * @Column(type="string", length=5, nullable=true, name="st_notafinal")
     */
    private $st_notafinal;


    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true)
     */
    private $dt_abertura;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true)
     */
    private $dt_encerramento;


    /**
     * @var boolean $bl_check
     * @Column(name="bl_check", type="boolean", nullable=false)
     */
    private $bl_check;


    /**
     * @var integer $id_turmatransfere
     * @Column(type="integer", nullable=false, name="id_turmatransfere")
     */
    private $id_turmatransfere;

    /**
     * @var string $st_status
     * @Column(type="string", length=255, nullable=true, name="st_status")
     */
    private $st_status;

    /**
     * @var integer $id_tipodisciplina
     * @Column(type="integer", nullable=false, name="id_tipodisciplina")
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_categoriasala
     * @Column(type="integer", nullable=false, name="id_categoriasala")
     */
    private $id_categoriasala;

    /**
     * @var integer $nu_cargahoraria
     * @Column(type="integer", nullable=false, name="nu_cargahoraria")
     */
    private $nu_cargahoraria;

    /**
     * @var string $st_notatotal;
     * @Column(type="string", length=5, nullable=true, name="st_notatotal")
     */
    private $st_notatotal;

    public function setBl_check($bl_check)
    {
        $this->bl_check = $bl_check;
    }

    public function getBl_check()
    {
        return $this->bl_check;
    }

    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_encerramentosala($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    public function getId_encerramentosala()
    {
        return $this->id_encerramentosala;
    }

    public function setId_entidadetransfere($id_entidadetransfere)
    {
        $this->id_entidadetransfere = $id_entidadetransfere;
    }

    public function getId_entidadetransfere()
    {
        return $this->id_entidadetransfere;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetotransfere($id_projetotransfere)
    {
        $this->id_projetotransfere = $id_projetotransfere;
    }

    public function getId_projetotransfere()
    {
        return $this->id_projetotransfere;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function setStNotaead($st_notaead)
    {
        $this->st_notaead = $st_notaead;
    }

    public function getSt_notaead()
    {
        return $this->st_notaead;
    }

    public function setSt_notafinal($st_notafinal)
    {
        $this->st_notafinal = $st_notafinal;
    }

    public function getSt_notafinal()
    {
        return $this->st_notafinal;
    }

    public function setSt_notatcc($st_notatcc)
    {
        $this->st_notatcc = $st_notatcc;
    }

    public function getSt_notatcc()
    {
        return $this->st_notatcc;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function setId_turmatransfere($id_turmatransfere)
    {
        $this->id_turmatransfere = $id_turmatransfere;
    }

    public function getId_turmatransfere()
    {
        return $this->id_turmatransfere;
    }

    /**
     * @return string
     */
    public function getSt_notaatividade()
    {
        return $this->st_notaatividade;
    }

    /**
     * @param string $st_notaatividade
     * @return $this
     */
    public function setSt_notaatividade($st_notaatividade)
    {
        $this->st_notaatividade = $st_notaatividade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_status
     * @return $this
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
        return $this;
    }

    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return string
     */
    public function getSt_notatotal()
    {
        return $this->st_notatotal;
    }

    /**
     * @param string $st_notatotal
     */
    public function setSt_notatotal($st_notatotal)
    {
        $this->st_notatotal = $st_notatotal;
    }



    /**
     * @return array Array for attributes of the entity
     */
    public function _toArray ()
    {
        return array(
            'st_disciplina' => $this->st_disciplina,
            'st_notaatividade' => $this->st_notaatividade,
            'st_notaead' => $this->st_notaead,
            'st_notatcc' => $this->st_notatcc,
            'st_notafinal' => $this->st_notafinal,
            'st_status' => $this->st_status,
            'dt_abertura' => (is_object($this->dt_abertura) && $this->dt_abertura instanceof \DateTime ? $this->dt_abertura->format("d/m/Y"):'' ),// H:i:s
            'dt_encerramento' => (is_object($this->dt_encerramento) && $this->dt_encerramento instanceof \DateTime ? $this->dt_encerramento->format("d/m/Y"):'' ),
            'bl_check' => $this->bl_check,
            'id_encerramentosala' => $this->id_encerramentosala,
            'id_projetotransfere' => $this->id_projetotransfere,
            'id_entidadetransfere' => $this->id_entidadetransfere,
            'id_projetopedagogico' => $this->id_projetopedagogico,
            'id_saladeaula' => $this->id_saladeaula,
            'st_saladeaula' => $this->st_saladeaula,
            'id_disciplina' => $this->id_disciplina,
            'id_matricula' => $this->id_matricula,
            'id_turmatransfere' =>$this->id_turmatransfere,
            'id_tipodisciplina' => $this->id_tipodisciplina,
            'nu_cargahoraria' => $this->nu_cargahoraria,
            'st_notatotal' => $this->st_notatotal
        );
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_contratoafiliado")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwContratoAfiliado
{

    /**
     *
     * @var integer $id_contratoafiliado
     * @Column(name="id_contratoafiliado", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_contratoafiliado;

    /**
     *
     * @var string $st_contratoafiliado
     * @Column(name="st_contratoafiliado", type="string", nullable=false, length=200)
     */
    private $st_contratoafiliado;

    /**
     *
     * @var integer $id_entidadeafiliada
     * @Column(name="id_entidadeafiliada", type="integer", nullable=true)
     */
    private $id_entidadeafiliada;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false)
     */
    private $id_entidadecadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_usuarioresponsavel
     * @Column(name="id_usuarioresponsavel", type="integer", nullable=true)
     */
    private $id_usuarioresponsavel;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var string $st_url
     * @Column(name="st_url", type="string", nullable=true, length=300)
     */
    private $st_url;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=3000)
     */
    private $st_descricao;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var integer $id_classeafiliado
     * @Column(name="id_classeafiliado", type="integer", nullable=false)
     */
    private $id_classeafiliado;

    /**
     *
     * @var string $st_classeafiliado
     * @Column(name="st_classeafiliado", type="string", nullable=false, length=200)
     */
    private $st_classeafiliado;

    /**
     *
     * @var string $st_nomeentidadeafiliada
     * @Column(name="st_nomeentidadeafiliada", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidadeafiliada;

    /**
     *
     * @var string $st_nomecompletoresponsavel
     * @Column(name="st_nomecompletoresponsavel", type="string", nullable=true, length=300)
     */
    private $st_nomecompletoresponsavel;

    public function getId_contratoafiliado ()
    {
        return $this->id_contratoafiliado;
    }

    public function setId_contratoafiliado ($id_contratoafiliado)
    {
        $this->id_contratoafiliado = $id_contratoafiliado;
        return $this;
    }

    public function getSt_contratoafiliado ()
    {
        return $this->st_contratoafiliado;
    }

    public function setSt_contratoafiliado ($st_contratoafiliado)
    {
        $this->st_contratoafiliado = $st_contratoafiliado;
        return $this;
    }

    public function getId_entidadeafiliada ()
    {
        return $this->id_entidadeafiliada;
    }

    public function setId_entidadeafiliada ($id_entidadeafiliada)
    {
        $this->id_entidadeafiliada = $id_entidadeafiliada;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_usuarioresponsavel ()
    {
        return $this->id_usuarioresponsavel;
    }

    public function setId_usuarioresponsavel ($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_url ()
    {
        return $this->st_url;
    }

    public function setSt_url ($st_url)
    {
        $this->st_url = $st_url;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_classeafiliado ()
    {
        return $this->id_classeafiliado;
    }

    public function setId_classeafiliado ($id_classeafiliado)
    {
        $this->id_classeafiliado = $id_classeafiliado;
        return $this;
    }

    public function getSt_classeafiliado ()
    {
        return $this->st_classeafiliado;
    }

    public function setSt_classeafiliado ($st_classeafiliado)
    {
        $this->st_classeafiliado = $st_classeafiliado;
        return $this;
    }

    public function getSt_nomeentidadeafiliada ()
    {
        return $this->st_nomeentidadeafiliada;
    }

    public function setSt_nomeentidadeafiliada ($st_nomeentidadeafiliada)
    {
        $this->st_nomeentidadeafiliada = $st_nomeentidadeafiliada;
        return $this;
    }

    public function getSt_nomecompletoresponsavel ()
    {
        return $this->st_nomecompletoresponsavel;
    }

    public function setSt_nomecompletoresponsavel ($st_nomecompletoresponsavel)
    {
        $this->st_nomecompletoresponsavel = $st_nomecompletoresponsavel;
        return $this;
    }

}
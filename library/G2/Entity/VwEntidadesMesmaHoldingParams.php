<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidadesmesmaholdingparams")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwEntidadesMesmaHoldingParams {

    /**
     * @var string $st_nomeentidadeparceira
     * @Column(name="st_nomeentidadeparceira", type="string", nullable=true, length=150)
     */
    private $st_nomeentidadeparceira;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var integer $id_entidadeparceira
     * @Column(name="id_entidadeparceira", type="integer", nullable=false, length=4)
     */
    private $id_entidadeparceira;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_cartacompartilhada
     * @Column(name="bl_cartacompartilhada", type="boolean", nullable=true, length=1)
     */
    private $bl_cartacompartilhada;


    /**
     * @return string st_nomeentidadeparceira
     */
    public function getSt_nomeentidadeparceira() {
        return $this->st_nomeentidadeparceira;
    }

    /**
     * @param st_nomeentidadeparceira
     */
    public function setSt_nomeentidadeparceira($st_nomeentidadeparceira) {
        $this->st_nomeentidadeparceira = $st_nomeentidadeparceira;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return integer id_entidadeparceira
     */
    public function getId_entidadeparceira() {
        return $this->id_entidadeparceira;
    }

    /**
     * @param id_entidadeparceira
     */
    public function setId_entidadeparceira($id_entidadeparceira) {
        $this->id_entidadeparceira = $id_entidadeparceira;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_cartacompartilhada
     */
    public function getBl_cartacompartilhada() {
        return $this->bl_cartacompartilhada;
    }

    /**
     * @param bl_cartacompartilhada
     */
    public function setBl_cartacompartilhada($bl_cartacompartilhada) {
        $this->bl_cartacompartilhada = $bl_cartacompartilhada;
        return $this;
    }

}

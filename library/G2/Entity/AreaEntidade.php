<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_areaentidade")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AreaEntidade
{

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     * @Id
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;



    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }





}
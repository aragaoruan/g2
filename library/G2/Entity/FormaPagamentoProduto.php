<?php

/*
 * Entity FormaPagamentoProduto
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-29
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formapagamentoproduto")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class FormaPagamentoProduto
{

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false, options={"default":null})
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;


    public function getId_produto()
    {
        return $this->id_produto;
    }

    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

}

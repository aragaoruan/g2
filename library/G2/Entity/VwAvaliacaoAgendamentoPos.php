<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoagendamentopos")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAvaliacaoAgendamentoPos
{

    /**
     *
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false)
     * @Id
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true,  length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true,  length=300)
     */
    private $st_cpf;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=true )
     */
    private $id_avaliacao;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=true,  length=100)
     */
    private $st_avaliacao;

    /**
     * @Column(name="dt_agendamento", type="date", nullable=true)
     */
    private $dt_agendamento;


    /**
     * @var integer $bl_automatico
     * @Column(name="bl_automatico", type="integer", nullable=true )
     */
    private $bl_automatico;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true )
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true )
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true )
     */
    private $id_entidade;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer", nullable=true )
     */
    private $id_horarioaula;

    /**
     * @var string $st_horarioaula
     * @Column(name="st_horarioaula", type="string", nullable=true, length=255)
     */
    private $st_horarioaula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true )
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true )
     */
    private $bl_provaglobal;

    /**
     * @var integer $bl_possuiprova
     * @Column(name="bl_possuiprova", type="integer", nullable=true )
     */
    private $bl_possuiprova;

    /**
     * @var integer $st_possuiprova
     * @Column(name="st_possuiprova", type="string", nullable=true )
     */
    private $st_possuiprova;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @Column(name="dt_aplicacao", type="date", nullable=true)
     */
    private $dt_aplicacao;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true )
     */
    private $id_aplicadorprova;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=true, length=200)
     */
    private $st_aplicadorprova;


    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;

    /**
     * @var integer $nu_presenca
     * @Column(name="nu_presenca", type="integer", nullable=true)
     */
    private $nu_presenca;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true)
     */
    private $id_usuariolancamento;

    /**
     * @var string $st_usuariolancamento
     * @Column(name="st_usuariolancamento", type="string", nullable=true)
     */
    private $st_usuariolancamento;

    /**
     * @var string $st_documentacao
     * @Column(name="st_documentacao", type="string", nullable=true, length=255)
     */
    private $st_documentacao;

    /**
     * @var string $st_presenca
     * @Column(name="st_presenca", type="string", nullable=true, length=255)
     */
    private $st_presenca;

    /**
     * @var string $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true)
     */
    private $id_evolucao;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true)
     */
    private $st_email;

    /**
     * @var string $nu_telefone
     * @Column(name="nu_telefone", type="string", nullable=true)
     */
    private $nu_telefone;

    /**
     * @var string $nu_ddd
     * @Column(name="nu_ddd", type="string", nullable=true)
     */
    private $nu_ddd;

    /**
     * @var integer $nu_ddi
     * @Column(name="nu_ddi", type="integer", nullable=true)
     */
    private $nu_ddi;

    /**
     * @var string $nu_telefonealternativo
     * @Column(name="nu_telefonealternativo", type="string", nullable=true)
     */
    private $nu_telefonealternativo;

    /**
     * @var string $nu_dddalternativo
     * @Column(name="nu_dddalternativo", type="string", nullable=true)
     */
    private $nu_dddalternativo;

    /**
     * @var integer $nu_ddialternativo
     * @Column(name="nu_ddialternativo", type="integer", nullable=true)
     */
    private $nu_ddialternativo;

    /**
     * @var datetime $hr_inicio
     * @Column(name="hr_inicio", type="datetime2", nullable=true)
     */
    private $hr_inicio;

    /**
     * @var datetime $hr_fim
     * @Column(name="hr_fim", type="datetime2", nullable=true)
     */
    private $hr_fim;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true)
     */
    private $sg_uf;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true)
     */
    private $nu_numero;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true)
     */
    private $st_bairro;

    /**
     * @var string $st_telefoneaplicador
     * @Column(name="st_telefoneaplicador", type="string", nullable=true)
     */
    private $st_telefoneaplicador;

    /**
     * @var integer $bl_temprovaintegrada
     * @Column(name="bl_temprovaintegrada", type="integer", nullable=false)
     */
    private $bl_temprovaintegrada;

    /**
     * @return string
     */
    public function getst_tipoprova()
    {
        return $this->st_tipoprova;
    }

    /**
     * @param string $st_tipoprova
     */
    public function setst_tipoprova($st_tipoprova)
    {
        $this->st_tipoprova = $st_tipoprova;
        return $this;

    }

    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_avaliacao()
    {
        return $this->st_avaliacao;
    }

    public function setSt_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
        return $this;
    }

    public function getSt_horarioaula()
    {
        return $this->st_horarioaula;
    }

    public function setSt_horarioaula($st_horarioaula)
    {
        $this->st_horarioaula = $st_horarioaula;
        return $this;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    public function getDt_aplicacao()
    {
        return $this->dt_aplicacao;
    }

    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    public function setDt_aplicacao(date $dt_aplicacao)
    {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function getNu_presenca()
    {
        return $this->nu_presenca;
    }

    public function setNu_presenca($nu_presenca)
    {
        $this->nu_presenca = $nu_presenca;
    }

    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    public function getSt_usuariolancamento()
    {
        return $this->st_usuariolancamento;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    public function setSt_usuariolancamento($st_usuariolancamento)
    {
        $this->st_usuariolancamento = $st_usuariolancamento;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    public function getNu_ddi()
    {
        return $this->nu_ddi;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
    }

    public function setNu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
    }

    public function setNu_ddi($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
    }

    public function getHr_inicio()
    {
        return $this->hr_inicio;
    }

    public function getHr_fim()
    {
        return $this->hr_fim;
    }

    public function setHr_inicio(datetime $hr_inicio)
    {
        $this->hr_inicio = $hr_inicio;
    }

    public function setHr_fim(datetime $hr_fim)
    {
        $this->hr_fim = $hr_fim;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $nu_numero
     */
    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
    }

    /**
     * @return string
     */
    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param string $st_complemento
     */
    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
    }

    /**
     * @return string
     */
    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param string $st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @return string
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param string $st_telefoneaplicador
     */
    public function setSt_telefoneaplicador($st_telefoneaplicador)
    {
        $this->st_telefoneaplicador = $st_telefoneaplicador;
    }

    /**
     * @return string
     */
    public function getSt_telefoneaplicador()
    {
        return $this->st_telefoneaplicador;
    }

    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
    }


    /**
     * @return int
     */
    public function getBl_automatico()
    {
        return $this->bl_automatico;
    }

    /**
     * @param int $bl_automatico
     * @return $this
     */
    public function setBl_automatico($bl_automatico)
    {
        $this->bl_automatico = $bl_automatico;
        return $this;
    }

    /**
     * @return int
     */
    public function getBl_possuiprova()
    {
        return $this->bl_possuiprova;
    }

    /**
     * @param int $bl_possuiprova
     * @return $this
     */
    public function setBl_possuiprova($bl_possuiprova)
    {
        $this->bl_possuiprova = $bl_possuiprova;
        return $this;
    }

    /**
     * @return int
     */
    public function getSt_possuiprova()
    {
        return $this->st_possuiprova;
    }

    /**
     * @param int $st_possuiprova
     * @return $this
     */
    public function setSt_possuiprova($st_possuiprova)
    {
        $this->st_possuiprova = $st_possuiprova;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getNu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    /**
     * @param string $nu_telefonealternativo
     * @return $this
     */
    public function setNu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    /**
     * @param string $nu_dddalternativo
     * @return $this
     */
    public function setNu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ddialternativo()
    {
        return $this->nu_ddialternativo;
    }

    /**
     * @param int $nu_ddialternativo
     * @return $this
     */
    public function setNu_ddialternativo($nu_ddialternativo)
    {
        $this->nu_ddialternativo = $nu_ddialternativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getBl_temprovaintegrada()
    {
        return $this->bl_temprovaintegrada;
    }

    /**
     * @param int $bl_temprovaintegrada
     */
    public function setBl_temprovaintegrada($bl_temprovaintegrada)
    {
        $this->bl_temprovaintegrada = $bl_temprovaintegrada;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_formapagamento")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class FormaPagamento
{

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_formapagamento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var TipoFormaPagamentoParcela $id_situacao
     * @ManyToOne(targetEntity="TipoFormaPagamentoParcela")
     * @JoinColumn(name="id_tipoformapagamentoparcela", referencedColumnName="id_tipoformapagamentoparcela")
     */
    private $id_tipoformapagamentoparcela;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $nu_maxparcelas
     * @Column(name="nu_maxparcelas", type="integer", nullable=false)
     */
    private $nu_maxparcelas;

    /**
     * @var integer $id_tipocalculojuros
     * @Column(name="id_tipocalculojuros", type="integer", nullable=false)
     */

    /**
     * @var TipoCalculoJuros $id_tipocalculojuros
     * @ManyToOne(targetEntity="TipoCalculoJuros")
     * @JoinColumn(name="id_tipocalculojuros", referencedColumnName="id_tipocalculojuros")
     */
    private $id_tipocalculojuros;

    /**
     * @var boolean $bl_todosprodutos
     * @Column(name="bl_todosprodutos", type="boolean", nullable=false)
     */
    private $bl_todosprodutos;

    /**
     * @var decimal $nu_entradavalormin
     * @Column(name="nu_entradavalormin", type="decimal", nullable=true)
     */
    private $nu_entradavalormin;

    /**
     * @var decimal $nu_entradavalormax
     * @Column(name="nu_entradavalormax", type="decimal", nullable=true)
     */
    private $nu_entradavalormax;

    /**
     * @var decimal $nu_valormin
     * @Column(name="nu_valormin", type="decimal", nullable=true)
     */
    private $nu_valormin;

    /**
     * @var decimal $nu_valormax
     * @Column(name="nu_valormax", type="decimal", nullable=true)
     */
    private $nu_valormax;

    /**
     * @var decimal $nu_valorminparcela
     * @Column(name="nu_valorminparcela", type="decimal", nullable=true)
     */
    private $nu_valorminparcela;

    /**
     * @var decimal $nu_multa
     * @Column(name="nu_multa", type="decimal", nullable=true)
     */
    private $nu_multa;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", nullable=true)
     */
    private $nu_juros;

    /**
     * @var decimal $nu_jurosmax
     * @Column(name="nu_jurosmax", type="decimal", nullable=true)
     */
    private $nu_jurosmax;

    /**
     * @var decimal $nu_jurosmin
     * @Column(name="nu_jurosmin", type="decimal", nullable=true)
     */
    private $nu_jurosmin;

    /**
     * @var string $st_formapagamento
     * @Column(name="st_formapagamento", type="string", nullable=false, length=255)
     */
    private $st_formapagamento;

    /**
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=false)
     */
    private $st_descricao;

    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    public function setId_formapagamento ($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * Get \G2\Entity\FormaPagamento
     * @return \G2\Entity\FormaPagamento
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * Set \G2\Entity\FormaPagamento
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\FormaPagamento
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * Return \G2\Entity\FormaPagamento
     * @return \G2\Entity\FormaPagamento
     */
    public function getId_tipoformapagamentoparcela ()
    {
        return $this->id_tipoformapagamentoparcela;
    }

    /**
     * Set \G2\Entity\FormaPagamento
     * @param \G2\Entity\TipoFormaPagamentoParcela $id_tipoformapagamentoparcela
     * @return \G2\Entity\FormaPagamento
     */
    public function setId_tipoformapagamentoparcela (TipoFormaPagamentoParcela $id_tipoformapagamentoparcela)
    {
        $this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getNu_maxparcelas ()
    {
        return $this->nu_maxparcelas;
    }

    public function setNu_maxparcelas ($nu_maxparcelas)
    {
        $this->nu_maxparcelas = $nu_maxparcelas;
        return $this;
    }

    /**
     * Get \G2\Entity\FormaPagamento
     * @return \G2\Entity\FormaPagamento
     */
    public function getId_tipocalculojuros ()
    {
        return $this->id_tipocalculojuros;
    }

    /**
     * Set \G2\Entity\FormaPagamento
     * @param \G2\Entity\TipoCalculoJuros $id_tipocalculojuros
     * @return \G2\Entity\FormaPagamento
     */
    public function setId_tipocalculojuros (TipoCalculoJuros $id_tipocalculojuros)
    {
        $this->id_tipocalculojuros = $id_tipocalculojuros;
        return $this;
    }

    public function getBl_todosprodutos ()
    {
        return $this->bl_todosprodutos;
    }

    public function setBl_todosprodutos ($bl_todosprodutos)
    {
        $this->bl_todosprodutos = $bl_todosprodutos;
        return $this;
    }

    public function getNu_entradavalormin ()
    {
        return $this->nu_entradavalormin;
    }

    public function setNu_entradavalormin ($nu_entradavalormin)
    {
        $this->nu_entradavalormin = $nu_entradavalormin;
        return $this;
    }

    public function getNu_entradavalormax ()
    {
        return $this->nu_entradavalormax;
    }

    public function setNu_entradavalormax ($nu_entradavalormax)
    {
        $this->nu_entradavalormax = $nu_entradavalormax;
        return $this;
    }

    public function getNu_valormin ()
    {
        return $this->nu_valormin;
    }

    public function setNu_valormin ($nu_valormin)
    {
        $this->nu_valormin = $nu_valormin;
        return $this;
    }

    public function getNu_valormax ()
    {
        return $this->nu_valormax;
    }

    public function setNu_valormax ($nu_valormax)
    {
        $this->nu_valormax = $nu_valormax;
        return $this;
    }

    public function getNu_valorminparcela ()
    {
        return $this->nu_valorminparcela;
    }

    public function setNu_valorminparcela ($nu_valorminparcela)
    {
        $this->nu_valorminparcela = $nu_valorminparcela;
        return $this;
    }

    public function getNu_multa ()
    {
        return $this->nu_multa;
    }

    public function setNu_multa ($nu_multa)
    {
        $this->nu_multa = $nu_multa;
        return $this;
    }

    public function getNu_juros ()
    {
        return $this->nu_juros;
    }

    public function setNu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    public function getNu_jurosmax ()
    {
        return $this->nu_jurosmax;
    }

    public function setNu_jurosmax ($nu_jurosmax)
    {
        $this->nu_jurosmax = $nu_jurosmax;
        return $this;
    }

    public function getNu_jurosmin ()
    {
        return $this->nu_jurosmin;
    }

    public function setNu_jurosmin ($nu_jurosmin)
    {
        $this->nu_jurosmin = $nu_jurosmin;
        return $this;
    }

    public function getSt_formapagamento ()
    {
        return $this->st_formapagamento;
    }

    public function setSt_formapagamento ($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
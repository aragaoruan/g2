<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_matriculadiplomacao")
 * @Entity
 * @EntityLog
 */
class MatriculaDiplomacao extends G2Entity
{
    /**
     * @var integer $id_matriculadiplomacao
     * @Column(name="id_matriculadiplomacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_matriculadiplomacao;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var \DateTime $dt_aptodiplomar
     * @Column(name="dt_aptodiplomar", type="datetime", nullable=true, length=8)
     */
    private $dt_aptodiplomar;

    /**
     * @var Usuario $id_usuarioaptodiplomar
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioaptodiplomar", referencedColumnName="id_usuario")
     */
    private $id_usuarioaptodiplomar;

    /**
     * @var \DateTime $dt_diplomagerado
     * @Column(name="dt_diplomagerado", type="datetime", nullable=true, length=8)
     */
    private $dt_diplomagerado;

    /**
     * @var Usuario $id_usuariodiplomagerado
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariodiplomagerado", referencedColumnName="id_usuario")
     */
    private $id_usuariodiplomagerado;

    /**
     * @var \DateTime $dt_enviadoassinatura
     * @Column(name="dt_enviadoassinatura", type="datetime", nullable=true, length=8)
     */
    private $dt_enviadoassinatura;

    /**
     * @var Usuario $id_usuarioenviadoassinatura
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioenviadoassinatura", referencedColumnName="id_usuario")
     */
    private $id_usuarioenviadoassinatura;

    /**
     * @var \DateTime $dt_retornadoassinatura
     * @Column(name="dt_retornadoassinatura", type="datetime", nullable=true, length=8)
     */
    private $dt_retornadoassinatura;

    /**
     * @var Usuario $id_usuarioretornadoassinatura
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioretornadoassinatura", referencedColumnName="id_usuario")
     */
    private $id_usuarioretornadoassinatura;

    /**
     * @var \DateTime $dt_enviadosecretaria
     * @Column(name="dt_enviadosecretaria", type="datetime", nullable=true, length=8)
     */
    private $dt_enviadosecretaria;


    /**
     * @var Usuario $id_usuarioenviadosecretaria
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioenviadosecretaria", referencedColumnName="id_usuario")
     */
    private $id_usuarioenviadosecretaria;

    /**
     * @var \DateTime $dt_enviadoregistro
     * @Column(name="dt_enviadoregistro", type="datetime", nullable=true, length=8)
     */
    private $dt_enviadoregistro;

    /**
     * @var Usuario $id_usuarioenviadoregistro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioenviadoregistro", referencedColumnName="id_usuario")
     */
    private $id_usuarioenviadoregistro;

    /**
     * @var \DateTime $dt_retornoregistro
     * @Column(name="dt_retornoregistro", type="datetime", nullable=true, length=8)
     */
    private $dt_retornoregistro;


    /**
     * @var Usuario $id_usuarioretornoregistro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioretornoregistro", referencedColumnName="id_usuario")
     */
    private $id_usuarioretornoregistro;

    /**
     * @var \DateTime $dt_enviopolo
     * @Column(name="dt_enviopolo", type="datetime", nullable=true, length=8)
     */
    private $dt_enviopolo;

    /**
     * @var Usuario $id_usuarioretornoregistro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioretornoregistro", referencedColumnName="id_usuario")
     */
    private $id_usuarioenviopolo;

    /**
     * @var \DateTime $dt_entreguealuno
     * @Column(name="dt_entreguealuno", type="datetime", nullable=true, length=8)
     */
    private $dt_entreguealuno;

    /**
     * @var Usuario $id_usuarioentreguealuno
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioentreguealuno", referencedColumnName="id_usuario")
     */
    private $id_usuarioentreguealuno;

    /**
     * @var \DateTime $dt_historicogerado
     * @Column(name="dt_historicogerado", type="datetime", nullable=true, length=8)
     */
    private $dt_historicogerado;

    /**
     * @var Usuario $id_usuariohistoricogerado
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariohistoricogerado", referencedColumnName="id_usuario")
     */
    private $id_usuariohistoricogerado;

    /**
     * @var \DateTime $dt_equivalenciagerado
     * @Column(name="dt_equivalenciagerado", type="datetime", nullable=true, length=8)
     */
    private $dt_equivalenciagerado;

    /**
     * @var Usuario $id_usuarioequivalenciagerado
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioequivalenciagerado", referencedColumnName="id_usuario")
     */
    private $id_usuarioequivalenciagerado;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_diplomanumero
     * @Column(name="st_diplomanumero", type="string")
     */
    private $st_diplomanumero;

    /**
     * @var string $st_diplomalivro
     * @Column(name="st_diplomalivro", type="string")
     */
    private $st_diplomalivro;

    /**
     * @var string $st_diplomafolha
     * @Column(name="st_diplomafolha", type="string")
     */
    private $st_diplomafolha;

    /**
     * @var string $st_cedula
     * @Column(name="st_cedula", type="string")
     */
    private $st_cedula;

    /**
     * @var string $st_motivo
     * @Column(name="st_motivo", type="string")
     */
    private $st_motivo;

    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string")
     */
    private $st_observacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var MatriculaDiplomacao $id_diplomacaoorigem
     * @ManyToOne(targetEntity="MatriculaDiplomacao")
     * @JoinColumn(name="id_diplomacaoorigem", referencedColumnName="id_matriculadiplomacao")
     */
    private $id_diplomacaoorigem;

    /**
     * @var \DateTime $dt_registroprocesso
     * @Column(name="dt_registroprocesso", type="datetime", nullable=true, length=8)
     */
    private $dt_registroprocesso;

    /**
     * @return int
     */
    public function getId_matriculadiplomacao()
    {
        return $this->id_matriculadiplomacao;
    }

    /**
     * @param int $id_matriculadiplomacao
     * @return $this
     */
    public function setId_matriculadiplomacao($id_matriculadiplomacao)
    {
        $this->id_matriculadiplomacao = $id_matriculadiplomacao;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_aptodiplomar()
    {
        return $this->dt_aptodiplomar;
    }

    /**
     * @param \DateTime $dt_aptodiplomar
     * @return $this
     */
    public function setDt_aptodiplomar($dt_aptodiplomar)
    {
        $this->dt_aptodiplomar = $dt_aptodiplomar;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioaptodiplomar()
    {
        return $this->id_usuarioaptodiplomar;
    }

    /**
     * @param Usuario $id_usuarioaptodiplomar
     * @return $this
     */
    public function setId_usuarioaptodiplomar($id_usuarioaptodiplomar)
    {
        $this->id_usuarioaptodiplomar = $id_usuarioaptodiplomar;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_diplomagerado()
    {
        return $this->dt_diplomagerado;
    }

    /**
     * @param \DateTime $dt_diplomagerado
     * @return $this
     */
    public function setDt_diplomagerado($dt_diplomagerado)
    {
        $this->dt_diplomagerado = $dt_diplomagerado;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariodiplomagerado()
    {
        return $this->id_usuariodiplomagerado;
    }

    /**
     * @param Usuario $id_usuariodiplomagerado
     * @return $this
     */
    public function setId_usuariodiplomagerado($id_usuariodiplomagerado)
    {
        $this->id_usuariodiplomagerado = $id_usuariodiplomagerado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadoassinatura()
    {
        return $this->dt_enviadoassinatura;
    }

    /**
     * @param \DateTime $dt_enviadoassinatura
     * @return $this
     */
    public function setDt_enviadoassinatura($dt_enviadoassinatura)
    {
        $this->dt_enviadoassinatura = $dt_enviadoassinatura;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioenviadoassinatura()
    {
        return $this->id_usuarioenviadoassinatura;
    }

    /**
     * @param Usuario $id_usuarioenviadoassinatura
     * @return $this
     */
    public function setId_usuarioenviadoassinatura($id_usuarioenviadoassinatura)
    {
        $this->id_usuarioenviadoassinatura = $id_usuarioenviadoassinatura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_retornadoassinatura()
    {
        return $this->dt_retornadoassinatura;
    }

    /**
     * @param \DateTime $dt_retornadoassinatura
     * @return $this
     */
    public function setDt_retornadoassinatura($dt_retornadoassinatura)
    {
        $this->dt_retornadoassinatura = $dt_retornadoassinatura;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioretornadoassinatura()
    {
        return $this->id_usuarioretornadoassinatura;
    }

    /**
     * @param Usuario $id_usuarioretornadoassinatura
     * @return $this
     */
    public function setId_usuarioretornadoassinatura($id_usuarioretornadoassinatura)
    {
        $this->id_usuarioretornadoassinatura = $id_usuarioretornadoassinatura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadosecretaria()
    {
        return $this->dt_enviadosecretaria;
    }

    /**
     * @param \DateTime $dt_enviadosecretaria
     * @return $this
     */
    public function setDt_enviadosecretaria($dt_enviadosecretaria)
    {
        $this->dt_enviadosecretaria = $dt_enviadosecretaria;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioenviadosecretaria()
    {
        return $this->id_usuarioenviadosecretaria;
    }

    /**
     * @param Usuario $id_usuarioenviadosecretaria
     * @return $this
     */
    public function setId_usuarioenviadosecretaria($id_usuarioenviadosecretaria)
    {
        $this->id_usuarioenviadosecretaria = $id_usuarioenviadosecretaria;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviadoregistro()
    {
        return $this->dt_enviadoregistro;
    }

    /**
     * @param \DateTime $dt_enviadoregistro
     * @return $this
     */
    public function setDt_enviadoregistro($dt_enviadoregistro)
    {
        $this->dt_enviadoregistro = $dt_enviadoregistro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioenviadoregistro()
    {
        return $this->id_usuarioenviadoregistro;
    }

    /**
     * @param Usuario $id_usuarioenviadoregistro
     * @return $this
     */
    public function setId_usuarioenviadoregistro($id_usuarioenviadoregistro)
    {
        $this->id_usuarioenviadoregistro = $id_usuarioenviadoregistro;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_retornoregistro()
    {
        return $this->dt_retornoregistro;
    }

    /**
     * @param \DateTime $dt_retornoregistro
     * @return $this
     */
    public function setDt_retornoregistro($dt_retornoregistro)
    {
        $this->dt_retornoregistro = $dt_retornoregistro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioretornoregistro()
    {
        return $this->id_usuarioretornoregistro;
    }

    /**
     * @param Usuario $id_usuarioretornoregistro
     * @return $this
     */
    public function setId_usuarioretornoregistro($id_usuarioretornoregistro)
    {
        $this->id_usuarioretornoregistro = $id_usuarioretornoregistro;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_enviopolo()
    {
        return $this->dt_enviopolo;
    }

    /**
     * @param \DateTime $dt_enviopolo
     * @return $this
     */
    public function setDt_enviopolo($dt_enviopolo)
    {
        $this->dt_enviopolo = $dt_enviopolo;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioenviopolo()
    {
        return $this->id_usuarioenviopolo;
    }

    /**
     * @param Usuario $id_usuarioenviopolo
     * @return $this
     */
    public function setId_usuarioenviopolo($id_usuarioenviopolo)
    {
        $this->id_usuarioenviopolo = $id_usuarioenviopolo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_entreguealuno()
    {
        return $this->dt_entreguealuno;
    }

    /**
     * @param \DateTime $dt_entreguealuno
     * @return $this
     */
    public function setDt_entreguealuno($dt_entreguealuno)
    {
        $this->dt_entreguealuno = $dt_entreguealuno;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioentreguealuno()
    {
        return $this->id_usuarioentreguealuno;
    }

    /**
     * @param Usuario $id_usuarioentreguealuno
     * @return $this
     */
    public function setId_usuarioentreguealuno($id_usuarioentreguealuno)
    {
        $this->id_usuarioentreguealuno = $id_usuarioentreguealuno;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_historicogerado()
    {
        return $this->dt_historicogerado;
    }

    /**
     * @param \DateTime $dt_historicogerado
     * @return $this
     */
    public function setDt_historicogerado($dt_historicogerado)
    {
        $this->dt_historicogerado = $dt_historicogerado;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariohistoricogerado()
    {
        return $this->id_usuariohistoricogerado;
    }

    /**
     * @param Usuario $id_usuariohistoricogerado
     * @return $this
     */
    public function setId_usuariohistoricogerado($id_usuariohistoricogerado)
    {
        $this->id_usuariohistoricogerado = $id_usuariohistoricogerado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_equivalenciagerado()
    {
        return $this->dt_equivalenciagerado;
    }

    /**
     * @param \DateTime $dt_equivalenciagerado
     * @return $this
     */
    public function setDt_equivalenciagerado($dt_equivalenciagerado)
    {
        $this->dt_equivalenciagerado = $dt_equivalenciagerado;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuarioequivalenciagerado()
    {
        return $this->id_usuarioequivalenciagerado;
    }

    /**
     * @param Usuario $id_usuarioequivalenciagerado
     * @return $this
     */
    public function setId_usuarioequivalenciagerado($id_usuarioequivalenciagerado)
    {
        $this->id_usuarioequivalenciagerado = $id_usuarioequivalenciagerado;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_diplomanumero()
    {
        return $this->st_diplomanumero;
    }

    /**
     * @param string $st_diplomanumero
     * @return $this
     */
    public function setSt_diplomanumero($st_diplomanumero)
    {
        $this->st_diplomanumero = $st_diplomanumero;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_diplomalivro()
    {
        return $this->st_diplomalivro;
    }

    /**
     * @param string $st_diplomalivro
     * @return $this
     */
    public function setSt_diplomalivro($st_diplomalivro)
    {
        $this->st_diplomalivro = $st_diplomalivro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_diplomafolha()
    {
        return $this->st_diplomafolha;
    }

    /**
     * @param string $st_diplomafolha
     * @return $this
     */
    public function setSt_diplomafolha($st_diplomafolha)
    {
        $this->st_diplomafolha = $st_diplomafolha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cedula()
    {
        return $this->st_cedula;
    }

    /**
     * @param string $st_cedula
     * @return $this
     */
    public function setSt_cedula($st_cedula)
    {
        $this->st_cedula = $st_cedula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_motivo()
    {
        return $this->st_motivo;
    }

    /**
     * @param string $st_motivo
     * @return $this
     */
    public function setSt_motivo($st_motivo)
    {
        $this->st_motivo = $st_motivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param string $st_observacao
     * @return $this
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return MatriculaDiplomacao
     */
    public function getId_diplomacaoorigem()
    {
        return $this->id_diplomacaoorigem;
    }

    /**
     * @param MatriculaDiplomacao $id_diplomacaoorigem
     * @return $this
     */
    public function setId_diplomacaoorigem($id_diplomacaoorigem)
    {
        $this->id_diplomacaoorigem = $id_diplomacaoorigem;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_registroprocesso()
    {
        return $this->dt_registroprocesso;
    }

    /**
     * @param \DateTime $dt_registroprocesso
     * @return $this
     */
    public function setDt_registroprocesso($dt_registroprocesso)
    {
        $this->dt_registroprocesso = $dt_registroprocesso;
        return $this;
    }

}

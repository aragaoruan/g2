<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_finalidadecampanha")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */


class FinalidadeCampanha
{

    /**
     * @var integer $id_finalidadecampanha
     * @Column(name="id_finalidadecampanha", type="integer", nullable=false)
     * @Id
     */
    private $id_finalidadecampanha;

    /**
     * @var string $st_finalidadecampanha
     * @Column(name="st_finalidadecampanha", type="string", nullable=true, length=140)
     */
    private $st_finalidadecampanha;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_finalidadecampanha()
    {
        return $this->id_finalidadecampanha;
    }

    /**
     * @param int $id_finalidadecampanha
     */
    public function setId_finalidadecampanha($id_finalidadecampanha)
    {
        $this->id_finalidadecampanha = $id_finalidadecampanha;
    }

    /**
     * @return string
     */
    public function getSt_finalidadecampanha()
    {
        return $this->st_finalidadecampanha;
    }

    /**
     * @param string $st_finalidadecampanha
     */
    public function setSt_finalidadecampanha($st_finalidadecampanha)
    {
        $this->st_finalidadecampanha = $st_finalidadecampanha;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_turma")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwTurma
{

    /**
     *
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_turma;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255) 
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=true) 
     */
    private $id_entidadecadastro;

    /**
     *
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150) 
     */
    private $st_turma;

    /**
     *
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=false, length=200) 
     */
    private $st_tituloexibicao;

    /**
     *
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=false) 
     */
    private $nu_maxalunos;

    /**
     *
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=false) 
     */
    private $dt_inicioinscricao;

    /**
     *
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true) 
     */
    private $dt_fiminscricao;

    /**
     *
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="integer", nullable=false) 
     */
    private $dt_inicio;

    /**
     *
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true) 
     */
    private $dt_fim;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    public function getId_turma ()
    {
        return $this->id_turma;
    }

    public function setId_turma ($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_turma ()
    {
        return $this->st_turma;
    }

    public function setSt_turma ($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    public function getSt_tituloexibicao ()
    {
        return $this->st_tituloexibicao;
    }

    public function setSt_tituloexibicao ($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    public function getNu_maxalunos ()
    {
        return $this->nu_maxalunos;
    }

    public function setNu_maxalunos ($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    public function getDt_inicioinscricao ()
    {
        return $this->dt_inicioinscricao;
    }

    public function setDt_inicioinscricao ($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    public function getDt_fiminscricao ()
    {
        return $this->dt_fiminscricao;
    }

    public function setDt_fiminscricao ($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
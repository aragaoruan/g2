<?php


namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_lotematerial")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.ocm.br>
 */
class LoteMaterial {

    /**
     * @var integer $id_lotematerial
     * @Column(name="id_lotematerial", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_lotematerial;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setId_lotematerial($id_lotematerial)
    {
        $this->id_lotematerial = $id_lotematerial;
    }

    /**
     * @return int
     */
    public function getId_lotematerial()
    {
        return $this->id_lotematerial;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tramiteocorrencia")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class TramiteOcorrencia {

    /**
     * @var integer $id_tramiteocorrencia
     * @Column(name="id_tramiteocorrencia", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramiteocorrencia;


    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false)
     */
    private $id_tramite;


    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false)
     */
    private $id_ocorrencia;

    /**
     * @var boolean $bl_interessado
     * @Column(name="bl_interessado", type="boolean", nullable=true, length=1)
     */
    private $bl_interessado;

    public function getId_tramiteocorrencia() {
        return $this->id_tramiteocorrencia;
    }
    public function setId_tramiteocorrencia($id_tramiteocorrencia) {
        $this->id_tramiteocorrencia = $id_tramiteocorrencia;
    }

    public function getId_tramite() {
        return $this->id_tramite;
    }

    public function setId_tramite($id_tramite) {
        $this->id_tramite = $id_tramite;
    }

    public function getId_ocorrencia() {
        return $this->id_ocorrencia;
    }

    public function setId_ocorrencia($id_ocorrencia) {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    public function getBl_interessado() {
        return $this->bl_interessado;
    }

    public function setBl_interessado($bl_interessado) {
        $this->bl_interessado = $bl_interessado;
    }
}

?>
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_entidadeprojetodisciplina")
 * @Entity(repositoryClass="\G2\Repository\ProjetoPedagogico")
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwEntidadeProjetoDisciplina {

  /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer")
     * @Id
      */
    private $id_disciplina;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina",type="string")
     */
    private $st_disciplina;
    /**
     * @var decimal $nu_cargahoraria
     * @Column(name="nu_cargahoraria",type="integer")
     */
    private $nu_cargahoraria;
    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string")
     */
    private $st_descricao;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao",type="integer")
     */
    private $id_situacao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string")
     */
    private $st_situacao;
     /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;
      /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

   /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro",type="integer")
     * @Id
     */
    private $id_entidadecadastro;
   /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string")
     */
    private $st_areaconhecimento;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean")
     */
    private $bl_ativo;
   /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string")
     */
    private $st_nivelensino;

     /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino",type="integer")
     */
    private $id_nivelensino;

     /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_serie;

    /**
     * @var string $st_serie
     * @Column(name="st_serie", type="string")
     */
    private $st_serie;

     /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina",type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @var string $st_tipodisciplina
     * @Column(name="st_tipodisciplina", type="string")
     */
    private $st_tipodisciplina;

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function getSt_nivelensino() {
        return $this->st_nivelensino;
    }

    public function setSt_nivelensino($st_nivelensino) {
        $this->st_nivelensino = $st_nivelensino;
    }

    public function getId_nivelensino() {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino($id_nivelensino) {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function getId_serie() {
        return $this->id_serie;
    }

    public function setId_serie($id_serie) {
        $this->id_serie = $id_serie;
    }

    public function getSt_serie() {
        return $this->st_serie;
    }

    public function setSt_serie($st_serie) {
        $this->st_serie = $st_serie;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function getSt_tipodisciplina() {
        return $this->st_tipodisciplina;
    }

    public function setSt_tipodisciplina($st_tipodisciplina) {
        $this->st_tipodisciplina = $st_tipodisciplina;
    }

}

?>

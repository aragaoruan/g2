<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_disciplinaserienivel")
 * @Entity
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */


class VwDisciplinaSerieNivel
{
    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=true, length=4)
     */
    private $id_serie;
    /**
     * @var integer $id_serieanterior
     * @Column(name="id_serieanterior", type="integer", nullable=true, length=4)
     */
    private $id_serieanterior;
    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=true, length=4)
     */
    private $id_nivelensino;
    /**
     * @var integer $nu_repeticao
     * @Column(name="nu_repeticao", type="integer", nullable=true, length=4)
     */
    private $nu_repeticao;
    /**
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false, length=1)
     */
    private $bl_ativa;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true)
     */
    private $st_descricao;
    /**
     * @var string $nu_identificador
     * @Column(name="nu_identificador", type="string", nullable=true, length=255)
     */
    private $nu_identificador;
    /**
     * @var string $st_serie
     * @Column(name="st_serie", type="string", nullable=true, length=255)
     */
    private $st_serie;
    /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=true, length=255)
     */
    private $st_nivelensino;

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_serie()
    {
        return $this->id_serie;
    }

    /**
     * @param int $id_serie
     */
    public function setId_serie($id_serie)
    {
        $this->id_serie = $id_serie;
    }

    /**
     * @return int
     */
    public function getId_serieanterior()
    {
        return $this->id_serieanterior;
    }

    /**
     * @param int $id_serieanterior
     */
    public function setId_serieanterior($id_serieanterior)
    {
        $this->id_serieanterior = $id_serieanterior;
    }

    /**
     * @return int
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param int $id_nivelensino
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
    }

    /**
     * @return int
     */
    public function getNu_repeticao()
    {
        return $this->nu_repeticao;
    }

    /**
     * @param int $nu_repeticao
     */
    public function setNu_repeticao($nu_repeticao)
    {
        $this->nu_repeticao = $nu_repeticao;
    }

    /**
     * @return boolean
     */
    public function getBl_ativa()
    {
        return $this->bl_ativa;
    }

    /**
     * @param boolean $bl_ativa
     */
    public function setBl_ativa($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return string
     */
    public function getNu_identificador()
    {
        return $this->nu_identificador;
    }

    /**
     * @param string $nu_identificador
     */
    public function setNu_identificador($nu_identificador)
    {
        $this->nu_identificador = $nu_identificador;
    }

    /**
     * @return string
     */
    public function getSt_serie()
    {
        return $this->st_serie;
    }

    /**
     * @param string $st_serie
     */
    public function setSt_serie($st_serie)
    {
        $this->st_serie = $st_serie;
    }

    /**
     * @return string
     */
    public function getSt_nivelensino()
    {
        return $this->st_nivelensino;
    }

    /**
     * @param string $st_nivelensino
     */
    public function setSt_nivelensino($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
    }
}
<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_nucleotm")
 * @Entity(repositoryClass="\G2\Repository\NucleoTm")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */


class NucleoTm
{

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_nucleotm
     * @Column(name="id_nucleotm", type="integer", nullable=false, length=4)
     */
    private $id_nucleotm;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_entidadematriz
     * @Column(name="id_entidadematriz", type="integer", nullable=false, length=4)
     */
    private $id_entidadematriz;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_nucleotm
     * @Column(name="st_nucleotm", type="string", nullable=false, length=150)
     */
    private $st_nucleotm;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_nucleotm
     */
    public function getId_nucleotm()
    {
        return $this->id_nucleotm;
    }

    /**
     * @param id_nucleotm
     */
    public function setId_nucleotm($id_nucleotm)
    {
        $this->id_nucleotm = $id_nucleotm;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @param id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_nucleotm
     */
    public function getSt_nucleotm()
    {
        return $this->st_nucleotm;
    }

    /**
     * @param st_nucleotm
     */
    public function setSt_nucleotm($st_nucleotm)
    {
        $this->st_nucleotm = $st_nucleotm;
        return $this;
    }

}
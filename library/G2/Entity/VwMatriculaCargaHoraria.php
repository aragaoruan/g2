<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_matriculacargahoraria")
 * @Entity
 * @EntityView
 * @author Janilson Silva <janilson.silva@unyleya.com.br>
 */
class VwMatriculaCargaHoraria
{
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var integer $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="string")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer")
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string")
     */
    private $st_situacao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var \DateTime $dt_matricula
     * @Column(name="dt_matricula", type="datetime2")
     */
    private $dt_matricula;

    /**
     * @var decimal $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="decimal", nullable=true)
     */
    private $nu_cargahoraria;

    /**
     * @var decimal $nu_cargahorariaintegralizada
     * @Column(name="nu_cargahorariaintegralizada", type="decimal", nullable=true)
     */
    private $nu_cargahorariaintegralizada;

    /**
     * @return integer
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param integer $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param integer $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return integer
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param integer $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param integer $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param integer $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     * @return $this
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param integer $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return $this
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param DateTime $dt_matricula
     * @return $this
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
        return $this;
    }

    /**
     * @return numeric
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param numeric $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return numeric
     */
    public function getNu_cargahorariaintegralizada()
    {
        return $this->nu_cargahorariaintegralizada;
    }

    /**
     * @param numeric $nu_cargahorariaintegralizada
     * @return $this
     */
    public function setNu_cargahorariaintegralizada($nu_cargahorariaintegralizada)
    {
        $this->nu_cargahorariaintegralizada = $nu_cargahorariaintegralizada;
        return $this;
    }
}

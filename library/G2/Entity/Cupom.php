<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\Cupom")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_cupom")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Cupom
{

    /**
     * @var integer $id_cupom
     * @Column(type="integer", nullable=false, name="id_cupom")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cupom;

    /**
     * @var string $st_codigocupom 
     * @Column(type="string", length=22, nullable=false, name="st_codigocupom")
     */
    private $st_codigocupom;

    /**
     * @var string $st_prefixo 
     * @Column(type="string", length=11, nullable=false, name="st_prefixo")
     */
    private $st_prefixo;

    /**
     * @var string $st_complemento 
     * @Column(type="string", length=11, nullable=false, name="st_complemento")
     */
    private $st_complemento;

    /**
     * @var decimal $nu_desconto
     * @Column(type="decimal", nullable=false, name="nu_desconto")
     */
    private $nu_desconto;

    /**
     * @var datetime2 $dt_inicio
     * @Column(type="datetime2", nullable=false, name="dt_inicio")
     */
    private $dt_inicio;

    /**
     * @var datetime2 $dt_fim
     * @Column(type="datetime2", nullable=false, name="dt_fim")
     */
    private $dt_fim;

    /**
     * @var boolean $bl_ativacao
     * @Column(type="boolean", nullable=false, name="bl_ativacao")
     */
    private $bl_ativacao;

    /**
     * @var boolean $bl_unico
     * @Column(type="boolean", nullable=false, name="bl_unico")
     */
    private $bl_unico;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo")
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var TipoDesconto $id_tipodesconto
     * @ManyToOne(targetEntity="TipoDesconto")
     * @JoinColumn(name="id_tipodesconto", referencedColumnName="id_tipodesconto", nullable=false)
     */
    private $id_tipodesconto;

    /**
     * @var CampanhaComercial $id_campanhacomercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_campanhacomercial;

    /**
     * 
     * @return integer
     */
    public function getId_cupom ()
    {
        return $this->id_cupom;
    }

    /**
     * 
     * @return string
     */
    public function getSt_codigocupom ()
    {
        return $this->st_codigocupom;
    }

    /**
     * 
     * @return string
     */
    public function getSt_prefixo ()
    {
        return $this->st_prefixo;
    }

    /**
     * 
     * @return string
     */
    public function getSt_complemento ()
    {
        return $this->st_complemento;
    }

    /**
     * 
     * @return decimal
     */
    public function getNu_desconto ()
    {
        return $this->nu_desconto;
    }

    /**
     * 
     * @return datetime2
     */
    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    /**
     * 
     * @return datetime2
     */
    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_ativacao ()
    {
        return $this->bl_ativacao;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_unico ()
    {
        return $this->bl_unico;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * 
     * @return datetime2
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * 
     * @return G2\Entity\Usuario
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * 
     * @return G2\Entity\TipoDesconto
     */
    public function getId_tipodesconto ()
    {
        return $this->id_tipodesconto;
    }

    /**
     * 
     * @return G2\Entity\CampanhaComercial
     */
    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * 
     * @param integer $id_cupom
     * @return \G2\Entity\Cupom
     */
    public function setId_cupom ($id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * 
     * @param string $st_codigocupom
     * @return \G2\Entity\Cupom
     */
    public function setSt_codigocupom ($st_codigocupom)
    {
        $this->st_codigocupom = $st_codigocupom;
        return $this;
    }

    /**
     * 
     * @param string $st_prefixo
     * @return \G2\Entity\Cupom
     */
    public function setSt_prefixo ($st_prefixo)
    {
        $this->st_prefixo = $st_prefixo;
        return $this;
    }

    /**
     * 
     * @param string $st_complemento
     * @return \G2\Entity\Cupom
     */
    public function setSt_complemento ($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_desconto
     * @return \G2\Entity\Cupom
     */
    public function setNu_desconto ($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    /**
     * 
     * @param datetime2 $dt_inicio
     * @return \G2\Entity\Cupom
     */
    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * 
     * @param datetime2 $dt_fim
     * @return \G2\Entity\Cupom
     */
    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_ativacao
     * @return \G2\Entity\Cupom
     */
    public function setBl_ativacao ($bl_ativacao)
    {
        $this->bl_ativacao = $bl_ativacao;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_unico
     * @return \G2\Entity\Cupom
     */
    public function setBl_unico ($bl_unico)
    {
        $this->bl_unico = $bl_unico;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_ativo
     * @return \G2\Entity\Cupom
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * 
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\Cupom
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\Cupom
     */
    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\TipoDesconto $id_tipodesconto
     * @return \G2\Entity\Cupom
     */
    public function setId_tipodesconto (TipoDesconto $id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\CampanhaComercial $id_campanhacomercial
     * @return \G2\Entity\Cupom
     */
    public function setId_campanhacomercial (CampanhaComercial $id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

}

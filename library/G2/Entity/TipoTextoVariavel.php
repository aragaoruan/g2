<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipotextovariavel")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoTextoVariavel
{

    /**
     *
     * @var integer $id_tipotextovariavel
     * @Column(name="id_tipotextovariavel", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipotextovariavel;

    /**
     *
     * @var string $st_tipotextovariavel
     * @Column(name="st_tipotextovariavel", type="string", nullable=false, length=50) 
     */
    private $st_tipotextovariavel;

    public function getId_tipotextovariavel ()
    {
        return $this->id_tipotextovariavel;
    }

    public function setId_tipotextovariavel ($id_tipotextovariavel)
    {
        $this->id_tipotextovariavel = $id_tipotextovariavel;
        return $this;
    }

    public function getSt_tipotextovariavel ()
    {
        return $this->st_tipotextovariavel;
    }

    public function setSt_tipotextovariavel ($st_tipotextovariavel)
    {
        $this->st_tipotextovariavel = $st_tipotextovariavel;
        return $this;
    }

}
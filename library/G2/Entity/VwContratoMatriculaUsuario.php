<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_contratomatriculausuario")
 * @Entity
 * @EntityView
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */
class VwContratoMatriculaUsuario extends G2Entity {


    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=false, length=4)
     */
    private $id_contrato;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /** * @var integer $id_cancelamento
     * @Column(name="id_cancelamento", type="integer", nullable=true, length=4) */

    private $id_cancelamento;

    /**
     * @var integer
     * $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4) */

    private $id_evolucao;

    /**
     * @var integer
     * $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @return int
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @param int $id_contrato
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return int
     */
    public function getId_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param int $id_cancelamento
     */
    public function setId_cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: Unyleya06
 * Date: 28/10/2015
 * Time: 09:31
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pessoadadoscomplementares")
 * @Entity
 * @EntityView
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class VwPessoaDadosComplementares extends G2Entity
{

    /**
     * @id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var datetime2 $dt_nascimento
     * @Column(name="dt_nascimento", type="datetime2", nullable=false)
     */
    private $dt_nascimento;
    /**
     * @var datetime2 $dt_dataexpedicao
     * @Column(name="dt_dataexpedicao", type="datetime2", nullable=true)
     */
    private $dt_dataexpedicao;
    /**
     * @var integer $id_pais
     * @Column(name="id_pais", type="integer", nullable=true, length=4)
     */
    private $id_pais;
    /**
     * @var string $st_nomemae
     * @Column(name="st_nomemae", type="string", nullable=true, length=100)
     */
    private $st_nomemae;
    /**
     * @var string $st_nomepai
     * @Column(name="st_nomepai", type="string", nullable=true, length=255)
     */
    private $st_nomepai;
    /**
     * @var string $st_tituloeleitor
     * @Column(name="st_tituloeleitor", type="string", nullable=true, length=30)
     */
    private $st_tituloeleitor;
    /**
     * @var string $st_certificadoreservista
     * @Column(name="st_certificadoreservista", type="string", nullable=true, length=30)
     */
    private $st_certificadoreservista;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_rg
     * @Column(name="st_rg", type="string", nullable=false, length=20)
     */
    private $st_rg;
    /**
     * @var string $st_orgaoexpeditor
     * @Column(name="st_orgaoexpeditor", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpeditor;
    /**
     * @var string $st_sexo
     * @Column(name="st_sexo", type="string", nullable=true, length=1)
     */
    private $st_sexo;
    /**
     * @var string $st_curso
     * @Column(name="st_curso", type="string", nullable=false, length=400)
     */
    private $st_curso;
    /**
     * @var string $st_nomeinstituicao
     * @Column(name="st_nomeinstituicao", type="string", nullable=false, length=400)
     */
    private $st_nomeinstituicao;

    /**
     * @var string $st_nomepais
     * @Column(name="st_nomepais", type="string", nullable=true, length=100)
     */
    private $st_nomepais;

    /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=true, length=255)
     */
    private $st_nivelensino;

    /**
     * @var string $st_titulacao
     * @Column(name="st_titulacao", type="string", nullable=true, length=200)
     */
    private $st_titulacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_municipionascimento
     * @Column(name="st_municipionascimento", type="string", nullable=true, length=255)
     */
    private $st_municipionascimento;

    /**
     * @var string $st_ufnascimento
     * @Column(name="st_ufnascimento", type="string", nullable=true, length=100)
     */
    private $st_ufnascimento;

    /**
     * @return string
     */
    public function getSt_municipionascimento()
    {
        return $this->st_municipionascimento;
    }

    /**
     * @param string $st_municipionascimento
     */
    public function setSt_municipionascimento($st_municipionascimento)
    {
        $this->st_municipionascimento = $st_municipionascimento;
    }

    /**
     * @return string
     */
    public function getSt_ufnascimento()
    {
        return $this->st_ufnascimento;
    }

    /**
     * @param string $st_ufnascimento
     */
    public function setSt_ufnascimento($st_ufnascimento)
    {
        $this->st_ufnascimento = $st_ufnascimento;
    }


    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }


    /**
     * @return datetime2 dt_nascimento
     */
    public function getDt_nascimento() {
        return $this->dt_nascimento;
    }

    /**
     * @param dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento) {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    /**
     * @return datetime2 dt_dataexpedicao
     */
    public function getDt_dataexpedicao() {
        return $this->dt_dataexpedicao;
    }

    /**
     * @param dt_dataexpedicao
     */
    public function setDt_dataexpedicao($dt_dataexpedicao) {
        $this->dt_dataexpedicao = $dt_dataexpedicao;
        return $this;
    }

    /**
     * @return integer id_pais
     */
    public function getId_pais() {
        return $this->id_pais;
    }

    /**
     * @param id_pais
     */
    public function setId_pais($id_pais) {
        $this->id_pais = $id_pais;
        return $this;
    }

    /**
     * @return string st_nomemae
     */
    public function getSt_nomemae() {
        return $this->st_nomemae;
    }

    /**
     * @param st_nomemae
     */
    public function setSt_nomemae($st_nomemae) {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    /**
     * @return string st_nomepai
     */
    public function getSt_nomepai() {
        return $this->st_nomepai;
    }

    /**
     * @param st_nomepai
     */
    public function setSt_nomepai($st_nomepai) {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    /**
     * @return string st_tituloeleitor
     */
    public function getSt_tituloeleitor() {
        return $this->st_tituloeleitor;
    }

    /**
     * @param st_tituloeleitor
     */
    public function setSt_tituloeleitor($st_tituloeleitor) {
        $this->st_tituloeleitor = $st_tituloeleitor;
        return $this;
    }

    /**
     * @return string st_certificadoreservista
     */
    public function getSt_certificadoreservista() {
        return $this->st_certificadoreservista;
    }

    /**
     * @param st_certificadoreservista
     */
    public function setSt_certificadoreservista($st_certificadoreservista) {
        $this->st_certificadoreservista = $st_certificadoreservista;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_rg
     */
    public function getSt_rg() {
        return $this->st_rg;
    }

    /**
     * @param st_rg
     */
    public function setSt_rg($st_rg) {
        $this->st_rg = $st_rg;
        return $this;
    }

    /**
     * @return string st_orgaoexpeditor
     */
    public function getSt_orgaoexpeditor() {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @param st_orgaoexpeditor
     */
    public function setSt_orgaoexpeditor($st_orgaoexpeditor) {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    /**
     * @return char st_sexo
     */
    public function getSt_sexo() {
        return $this->st_sexo;
    }

    /**
     * @param st_sexo
     */
    public function setSt_sexo($st_sexo) {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    /**
     * @return string st_curso
     */
    public function getSt_curso() {
        return $this->st_curso;
    }

    /**
     * @param st_curso
     */
    public function setSt_curso($st_curso) {
        $this->st_curso = $st_curso;
        return $this;
    }

    /**
     * @return stringchar st_nomeinstituicao
     */
    public function getSt_nomeinstituicao() {
        return $this->st_nomeinstituicao;
    }

    /**
     * @param st_nomeinstituicao
     */
    public function setSt_nomeinstituicao($st_nomeinstituicao) {
        $this->st_nomeinstituicao = $st_nomeinstituicao;
        return $this;
    }

    /**
     * @return string st_nomepais
     */
    public function getSt_nomepais() {
        return $this->st_nomepais;
    }

    /**
     * @param st_nomepais
     */
    public function setSt_nomepais($st_nomepais)
    {
        $this->st_nomepais = $st_nomepais;
        return $this;
    }


    /**
     * @return string st_nivelensino
     */
    public function getSt_nivelensino() {
        return $this->st_nivelensino;
    }

    /**
     * @param st_nivelensino
     */
    public function setSt_nivelensino($st_nivelensino) {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    /**
     * @return string st_titulacao
     */
    public function getSt_titulacao() {
        return $this->st_titulacao;
    }

    /**
     * @param st_titulacao
     */
    public function setSt_titulacao($st_titulacao) {
        $this->st_titulacao = $st_titulacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


}
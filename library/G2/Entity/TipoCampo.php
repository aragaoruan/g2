<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipocampo")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-14
 */
class TipoCampo {

    /**
     *
     * @var integer $id_tipocampo
     * @Column(name="id_tipocampo", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipocampo;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_tipocampo")
     * @var string
     */
    private $st_tipocampo;

    /**
     * @return int
     */
    public function getId_tipocampo()
    {
        return $this->id_tipocampo;
    }

    /**
     * @param int $id_tipocampo
     */
    public function setId_tipocampo($id_tipocampo)
    {
        $this->id_tipocampo = $id_tipocampo;
    }

    /**
     * @return string
     */
    public function getSt_tipocampo()
    {
        return $this->st_tipocampo;
    }

    /**
     * @param string $st_tipocampo
     */
    public function setSt_tipocampo($st_tipocampo)
    {
        $this->st_tipocampo = $st_tipocampo;
    }





}

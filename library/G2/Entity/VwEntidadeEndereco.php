<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidadeendereco")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEntidadeEndereco
{

    /**
     * @var integer $id_entidadeendereco
     * @Column(name="id_entidadeendereco", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidadeendereco;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $bl_enderecoentidadepadrao
     * @Column(name="bl_enderecoentidadepadrao", type="integer", nullable=false, length=4)
     */
    private $bl_enderecoentidadepadrao;

    /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer", nullable=false, length=4)
     */
    private $id_endereco;

    /**
     * @var integer $id_tipoendereco
     * @Column(name="id_tipoendereco", type="integer", nullable=false, length=4)
     */
    private $id_tipoendereco;

    /**
     * @var string $st_tipoendereco
     * @Column(name="st_tipoendereco", type="string", nullable=false, length=255)
     */
    private $st_tipoendereco;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;

    /**
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=false, length=17)
     */
    private $id_municipio;

    /**
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=false, length=255)
     */
    private $st_nomemunicipio;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=false, length=2)
     */
    private $sg_uf;

    /**
     * @var string $st_estadoprovincia
     * @Column(name="st_estadoprovincia", type="string", nullable=true, length=255)
     */
    private $st_estadoprovincia;

    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=8)
     */
    private $st_cep;

    /**
     * @var integer $id_pais
     * @Column(name="id_pais", type="integer", nullable=false, length=4)
     */
    private $id_pais;

    /**
     * @var string $st_nomepais
     * @Column(name="st_nomepais", type="string", nullable=false, length=100)
     */
    private $st_nomepais;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    public function getId_entidadeendereco ()
    {
        return $this->id_entidadeendereco;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function getBl_enderecoentidadepadrao ()
    {
        return $this->bl_enderecoentidadepadrao;
    }

    public function getId_endereco ()
    {
        return $this->id_endereco;
    }

    public function getId_tipoendereco ()
    {
        return $this->id_tipoendereco;
    }

    public function getSt_tipoendereco ()
    {
        return $this->st_tipoendereco;
    }

    public function getSt_endereco ()
    {
        return $this->st_endereco;
    }

    public function getSt_complemento ()
    {
        return $this->st_complemento;
    }

    public function getNu_numero ()
    {
        return $this->nu_numero;
    }

    public function getSt_cidade ()
    {
        return $this->st_cidade;
    }

    public function getId_municipio ()
    {
        return $this->id_municipio;
    }

    public function getSt_nomemunicipio ()
    {
        return $this->st_nomemunicipio;
    }

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function getSt_estadoprovincia ()
    {
        return $this->st_estadoprovincia;
    }

    public function getSt_cep ()
    {
        return $this->st_cep;
    }

    public function getId_pais ()
    {
        return $this->id_pais;
    }

    public function getSt_nomepais ()
    {
        return $this->st_nomepais;
    }

    public function getSt_bairro ()
    {
        return $this->st_bairro;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setId_entidadeendereco ($id_entidadeendereco)
    {
        $this->id_entidadeendereco = $id_entidadeendereco;
        return $this;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setBl_enderecoentidadepadrao ($bl_enderecoentidadepadrao)
    {
        $this->bl_enderecoentidadepadrao = $bl_enderecoentidadepadrao;
        return $this;
    }

    public function setId_endereco ($id_endereco)
    {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    public function setId_tipoendereco ($id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
        return $this;
    }

    public function setSt_tipoendereco ($st_tipoendereco)
    {
        $this->st_tipoendereco = $st_tipoendereco;
        return $this;
    }

    public function setSt_endereco ($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function setSt_complemento ($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    public function setNu_numero ($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    public function setSt_cidade ($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    public function setId_municipio ($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function setSt_nomemunicipio ($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function setSt_estadoprovincia ($st_estadoprovincia)
    {
        $this->st_estadoprovincia = $st_estadoprovincia;
        return $this;
    }

    public function setSt_cep ($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    public function setId_pais ($id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    public function setSt_nomepais ($st_nomepais)
    {
        $this->st_nomepais = $st_nomepais;
        return $this;
    }

    public function setSt_bairro ($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_contratocarteirinha")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-29-12
 */
class VwContratoCarteirinha
{

    /**
     * @var integer $id_contrato
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_contrato;

    /**
     *
     * @var integer $id_contratoregra
     * @Column(type="integer", nullable=false)
     */
    private $id_contratoregra;

    /**
     *
     * @var string $st_contratoregra
     * @Column(type="string", nullable=false)
     */
    private $st_contratoregra;

    /**
     *
     * @var integer $id_modelocarteirinha
     * @Column(type="integer", nullable=false)
     */
    private $id_modelocarteirinha;

    /**
     * @var string $st_modelocarteirinha
     * @Column(type="string", nullable=false)
     */
    private $st_modelocarteirinha;

    /**
     * @return integer
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @return integer
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @return string
     */
    public function getSt_contratoregra()
    {
        return $this->st_contratoregra;
    }

    /**
     * @return integer
     */
    public function getId_modelocarteirinha()
    {
        return $this->id_modelocarteirinha;
    }

    /**
     * @return string
     */
    public function getSt_modelocarteirinha()
    {
        return $this->st_modelocarteirinha;
    }

    /**
     * @param integer $id_contrato
     * @return $this
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     * @param integer $id_contratoregra
     * @return $this
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }

    /**
     * @param string $st_contratoregra
     * @return $this
     */
    public function setSt_contratoregra($st_contratoregra)
    {
        $this->st_contratoregra = $st_contratoregra;
        return $this;
    }

    /**
     * @param integer $id_modelocarteirinha
     * @return $this
     */
    public function setId_modelocarteirinha($id_modelocarteirinha)
    {
        $this->id_modelocarteirinha = $id_modelocarteirinha;
        return $this;
    }

    /**
     * @param string $st_modelocarteirinha
     * @return $this
     */
    public function setSt_modelocarteirinha($st_modelocarteirinha)
    {
        $this->st_modelocarteirinha = $st_modelocarteirinha;
        return $this;
    }

}

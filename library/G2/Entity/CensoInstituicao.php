<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_censoinstituicao")
 * @Entity
 */
class CensoInstituicao
{

    /**
     * @var int $id_censoinstituicao
     * @Column(name="id_censoinstituicao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_censoinstituicao;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_ativo;

    /**
     * @var integer $nu_codigoinstituicao
     * @Column(name="nu_codigoinstituicao", type="integer", nullable=false)
     */
    private $nu_codigoinstituicao;

    /**
     * @var string $st_censoinstituicao
     * @Column(name="st_censoinstituicao", type="string", nullable=false, length=120)
     */
    private $st_censoinstituicao;

    /**
     * @return int
     */
    public function getId_censoinstituicao()
    {
        return $this->id_censoinstituicao;
    }

    /**
     * @param int $id_censoinstituicao
     * @return $this
     */
    public function setId_censoinstituicao($id_censoinstituicao)
    {
        $this->id_censoinstituicao = $id_censoinstituicao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_codigoinstituicao()
    {
        return $this->nu_codigoinstituicao;
    }

    /**
     * @param int $nu_codigoinstituicao
     * @return $this
     */
    public function setNu_codigoinstituicao($nu_codigoinstituicao)
    {
        $this->nu_codigoinstituicao = $nu_codigoinstituicao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_censoinstituicao()
    {
        return $this->st_censoinstituicao;
    }

    /**
     * @param string $st_censoinstituicao
     * @return $this
     */
    public function setSt_censoinstituicao($st_censoinstituicao)
    {
        $this->st_censoinstituicao = $st_censoinstituicao;
        return $this;
    }

}

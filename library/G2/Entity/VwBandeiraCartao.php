<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 10/04/2015
 * Time: 14:45
 */

namespace G2\Entity;

/**
 * Class Entity para vw Carta de Credito
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_bandeiracartao")
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 * @since 2015-04-10
 */
class VwBandeiraCartao {

    /**
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_cartaobandeira;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;

    /**
     * @var string $st_cartaobandeira
     * @Column(name="st_cartaobandeira", type="string", nullable=false, length=255)
     */
    private $st_cartaobandeira;

    /**
     * @return int
     */
    public function getIdCartaobandeira()
    {
        return $this->id_cartaobandeira;
    }

    /**
     * @param int $id_cartaobandeira
     */
    public function setIdCartaobandeira($id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
    }

    /**
     * @return int
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return mixed
     */
    public function getIdSistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     */
    public function setIdSistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return string
     */
    public function getStCartaobandeira()
    {
        return $this->st_cartaobandeira;
    }

    /**
     * @param string $st_cartaobandeira
     */
    public function setStCartaobandeira($st_cartaobandeira)
    {
        $this->st_cartaobandeira = $st_cartaobandeira;
    }
}
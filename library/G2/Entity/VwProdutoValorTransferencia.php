<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtovalortransferencia")
 * @Entity
 * @EntityView
 */
class VwProdutoValorTransferencia
{
    /**
     * @var integer $id_produtovalor
     * @Column(type="integer", nullable=false, name="id_produtovalor")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produtovalor;

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false, name="id_produto")
     */
    private $id_produto;

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false, name="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico ;
     * @Column(type="string", length=255, nullable=false, name="st_projetopedagogico")
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $id_situacao
     * @Column(type="integer", nullable=false, name="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="integer", nullable=false)
     */
    private $nu_valor;

    /**
     * @var integer $nu_valormensal
     * @Column(name="nu_valormensal", type="integer", nullable=true)
     */
    private $nu_valormensal;

    /**
     * @var \DateTime $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false)
     */
    private $dt_inicio;


    /**
     * @var \DateTime $dt_termino
     * @Column(name="dt_termino", type="datetime2", nullable=true)
     */
    private $dt_termino;

    /**
     * @return int
     */
    public function getId_produtovalor()
    {
        return $this->id_produtovalor;
    }

    /**
     * @param int $id_produtovalor
     * @return $this
     */
    public function setId_produtovalor($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     * @return $this
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_valormensal()
    {
        return $this->nu_valormensal;
    }

    /**
     * @param int $nu_valormensal
     * @return $this
     */
    public function setNu_valormensal($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param int $nu_valor
     * @return $this
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param \DateTime $dt_inicio
     * @return $this
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @param \DateTime $dt_termino
     * @return $this
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
        return $this;
    }
}

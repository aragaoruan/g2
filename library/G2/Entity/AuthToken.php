<?php

namespace G2\Entity;

/**
 * @EntityView
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class AuthToken
{
    /**
     * @Id
     * @Column(type="string",  nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $secret_token = "13ee1400e78fd4a3e1e80e93b0c62589f9563acb90338dae";

    /**
     * @Column(type="string",  nullable=false)
     */
    private $public_token = "ceb67e61be84d393a46495e36876bcd8d49e7e545a28de8a";

    /**
     * @return string
     */
    public function getSecretToken()
    {
        return $this->secret_token;
    }

    /**
     * @return string
     */
    public function getPublicToken()
    {
        return $this->public_token;
    }
}

<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_autorizacaoentrada")
 * @author Kayo Silva <kayo.silva@unyleya.com>
 */
class AutorizacaoEntrada
{

    /**
     * @var integer $id_autorizacaoentrada
     * @Column(type="integer", nullable=false, name="id_autorizacaoentrada")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_autorizacaoentrada;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuario;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuariocadastro;


    /**
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema", nullable=false)
     */
    private $id_textosistema;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     * @var datetime $dt_cadastro
     * @Column(type="datetime", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo")
     */
    private $bl_ativo;


    /**
     * @return integer
     */
    public function getId_autorizacaoentrada()
    {
        return $this->id_autorizacaoentrada;
    }

    /**
     *
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     *
     * @return \G2\Entity\Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     *
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     *
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     *
     * @return \G2\Entity\TextoSistema
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     *
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     *
     * @param integer $id_autorizacaoentrada
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setId_autorizacaoentrada($id_autorizacaoentrada)
    {
        $this->id_autorizacaoentrada = $id_autorizacaoentrada;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     *
     * @param datetime $dt_cadastro
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     *
     * @param boolean $bl_ativo
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\TextoSistema $id_textosistema
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setId_textosistema(TextoSistema $id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\AutorizacaoEntrada
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_controleturma")
 * @Entity
 * @EntityView
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class VwControleTurma
{
    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false, length=3)
     */
    private $dt_inicio;
    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true, length=3)
     */
    private $dt_fim;
    /**
     * @var integer $id_unidade
     * @Column(name="id_unidade", type="integer", nullable=false, length=4)
     */
    private $id_unidade;
    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     */
    private $id_turma;
    /**
     * @var integer $id_gradehoraria
     * @Column(name="id_gradehoraria", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_gradehoraria;
    /**
     * @var integer $id_professor
     * @Column(name="id_professor", type="integer", nullable=true, length=4)
     */
    private $id_professor;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;
    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_modulo;
    /**
     * @var integer $nu_cargahorariadisciplina
     * @Column(name="nu_cargahorariadisciplina", type="integer", nullable=false, length=4)
     */
    private $nu_cargahorariadisciplina;
    /**
     * @var integer $nu_cargahorariagrade
     * @Column(name="nu_cargahorariagrade", type="integer", nullable=true, length=4)
     */
    private $nu_cargahorariagrade;
    /**
     * @var integer $nu_cargarestante
     * @Column(name="nu_cargarestante", type="integer", nullable=true, length=4)
     */
    private $nu_cargarestante;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var datetime $dt_iniciogradehoraria
     * @Column(name="dt_iniciogradehoraria", type="datetime", nullable=true, length=8)
     */
    private $dt_iniciogradehoraria;
    /**
     * @var datetime $dt_fimgradehoraria
     * @Column(name="dt_fimgradehoraria", type="datetime", nullable=true, length=8)
     */
    private $dt_fimgradehoraria;
    /**
     * @var datetime $dt_diasemana
     * @Column(name="dt_diasemana", type="datetime", nullable=true, length=8)
     */
    private $dt_diasemana;
    /**
     * @var string $st_unidade
     * @Column(name="st_unidade", type="string", nullable=true, length=150)
     */
    private $st_unidade;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_codigo
     * @Column(name="st_codigo", type="string", nullable=false, length=150)
     */
    private $st_codigo;
    /**
     * @var string $st_nomegradehoraria
     * @Column(name="st_nomegradehoraria", type="string", nullable=true, length=100)
     */
    private $st_nomegradehoraria;
    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=false, length=300)
     */
    private $st_professor;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @Id
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $nu_cargahorariaprojeto
     * @Column(name="nu_cargahorariaprojeto", type="string", nullable=false, length=255)
     */
    private $nu_cargahorariaprojeto;
    /**
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=false, length=255)
     */
    private $st_turno;
    /**
     * @var varchar $st_valor
     * @Column(name="st_valor", type="string", nullable=false, length=400)
     */
    private $st_valor;

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return date dt_fim
     */
    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param dt_fim
     */
    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @return integer id_unidade
     */
    public function getId_unidade()
    {
        return $this->id_unidade;
    }

    /**
     * @param id_unidade
     */
    public function setId_unidade($id_unidade)
    {
        $this->id_unidade = $id_unidade;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer id_gradehoraria
     */
    public function getId_gradehoraria()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @param id_gradehoraria
     */
    public function setId_gradehoraria($id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

    /**
     * @return integer id_professor
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @param id_professor
     */
    public function setId_professor($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_modulo
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param id_modulo
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return integer nu_cargahorariadisciplina
     */
    public function getNu_cargahorariadisciplina()
    {
        return $this->nu_cargahorariadisciplina;
    }

    /**
     * @param nu_cargahorariadisciplina
     */
    public function setNu_cargahorariadisciplina($nu_cargahorariadisciplina)
    {
        $this->nu_cargahorariadisciplina = $nu_cargahorariadisciplina;
        return $this;
    }

    /**
     * @return integer nu_cargahorariagrade
     */
    public function getNu_cargahorariagrade()
    {
        return $this->nu_cargahorariagrade;
    }

    /**
     * @param nu_cargahorariagrade
     */
    public function setNu_cargahorariagrade($nu_cargahorariagrade)
    {
        $this->nu_cargahorariagrade = $nu_cargahorariagrade;
        return $this;
    }

    /**
     * @return integer nu_cargarestante
     */
    public function getNu_cargarestante()
    {
        return $this->nu_cargarestante;
    }

    /**
     * @param nu_cargarestante
     */
    public function setNu_cargarestante($nu_cargarestante)
    {
        $this->nu_cargarestante = $nu_cargarestante;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return datetime dt_iniciogradehoraria
     */
    public function getDt_iniciogradehoraria()
    {
        return $this->dt_iniciogradehoraria;
    }

    /**
     * @param dt_iniciogradehoraria
     */
    public function setDt_iniciogradehoraria($dt_iniciogradehoraria)
    {
        $this->dt_iniciogradehoraria = $dt_iniciogradehoraria;
        return $this;
    }

    /**
     * @return datetime dt_fimgradehoraria
     */
    public function getDt_fimgradehoraria()
    {
        return $this->dt_fimgradehoraria;
    }

    /**
     * @param dt_fimgradehoraria
     */
    public function setDt_fimgradehoraria($dt_fimgradehoraria)
    {
        $this->dt_fimgradehoraria = $dt_fimgradehoraria;
        return $this;
    }

    /**
     * @return datetime dt_diasemana
     */
    public function getDt_diasemana()
    {
        return $this->dt_diasemana;
    }

    /**
     * @param dt_fimgradehoraria
     */
    public function setDt_diasemana($dt_diasemana)
    {
        $this->dt_diasemana = $dt_diasemana;
        return $this;
    }

    /**
     * @return string st_unidade
     */
    public function getSt_unidade()
    {
        return $this->st_unidade;
    }

    /**
     * @param st_unidade
     */
    public function setSt_unidade($st_unidade)
    {
        $this->st_unidade = $st_unidade;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return string st_codigo
     */
    public function getSt_codigo()
    {
        return $this->st_codigo;
    }

    /**
     * @param st_codigo
     */
    public function setSt_codigo($st_codigo)
    {
        $this->st_codigo = $st_codigo;
        return $this;
    }

    /**
     * @return string st_nomegradehoraria
     */
    public function getSt_nomegradehoraria()
    {
        return $this->st_nomegradehoraria;
    }

    /**
     * @param st_nomegradehoraria
     */
    public function setSt_nomegradehoraria($st_nomegradehoraria)
    {
        $this->st_nomegradehoraria = $st_nomegradehoraria;
        return $this;
    }

    /**
     * @return string st_professor
     */
    public function getSt_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param st_professor
     */
    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_modulo
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @param st_modulo
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string nu_cargahorariaprojeto
     */
    public function getNu_cargahorariaprojeto()
    {
        return $this->nu_cargahorariaprojeto;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setNu_cargahorariaprojeto($nu_cargahorariaprojeto)
    {
        $this->nu_cargahorariaprojeto = $nu_cargahorariaprojeto;
        return $this;
    }

    /**
     * @return string st_turno
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @param st_turno
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }

    /**
     * @return varchar st_valor
     */
    public function getSt_valor()
    {
        return $this->st_valor;
    }

    /**
     * @param st_valor
     */
    public function setSt_valor($st_valor)
    {
        $this->st_valor = $st_valor;
        return $this;
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_usuariodeficiencia")
 * @Entity
 * @EntityLog
 */
class VwUsuarioDeficiencia extends G2Entity
{
    /**
     * @var int
     * @Column(name="id_usuariodeficiencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuariodeficiencia;

    /**
     * @var int
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var int
     * @Column(name="id_deficiencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_deficiencia;

    /**
     * @var string
     * @Column(name="st_nomecompleto", type="string", nullable=false)
     */
    private $st_nomecompleto;

    /**
     * @var string
     * @Column(name="st_deficiencia", type="string", nullable=false, length=60)
     */
    private $st_deficiencia;

    /**
     * @var string
     * @Column(name="st_descricao", type="string", nullable=true, length=120)
     */
    private $st_descricao;

    /**
     * @var bool $bl_ativo
     * @Column(name="bl_possuideficiencia", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_possuideficiencia;

    /**
     * @return int
     */
    public function getId_usuariodeficiencia()
    {
        return $this->id_usuariodeficiencia;
    }

    /**
     * @param int $id_usuariodeficiencia
     * @return $this
     */
    public function setId_usuariodeficiencia($id_usuariodeficiencia)
    {
        $this->id_usuariodeficiencia = $id_usuariodeficiencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_deficiencia()
    {
        return $this->id_deficiencia;
    }

    /**
     * @param int $id_deficiencia
     * @return $this
     */
    public function setId_deficiencia($id_deficiencia)
    {
        $this->id_deficiencia = $id_deficiencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_deficiencia()
    {
        return $this->st_deficiencia;
    }

    /**
     * @param string $st_deficiencia
     * @return $this
     */
    public function setSt_deficiencia($st_deficiencia)
    {
        $this->st_deficiencia = $st_deficiencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return $this
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_possuideficiencia()
    {
        return $this->bl_possuideficiencia;
    }

    /**
     * @param bool $bl_possuideficiencia
     * @return $this
     */
    public function setBl_possuideficiencia($bl_possuideficiencia)
    {
        $this->bl_possuideficiencia = $bl_possuideficiencia;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_concursouf")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ConcursoUf
{

    /**
     * @var integer $id_concursouf 
     * @Column(type="integer", nullable=false, name="id_concursouf")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_concursouf;

    /**
     * @var Concurso $id_concurso
     * @ManyToOne(targetEntity="Concurso", inversedBy="ufs")
     * @JoinColumn(name="id_concurso", referencedColumnName="id_concurso", nullable=false)
     */
    private $id_concurso;

    /**
     * @var Uf $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", referencedColumnName="sg_uf", nullable=false)
     */
    private $sg_uf;

    /**
     * @return integer id_concursouf
     */
    public function getId_concursouf ()
    {
        return $this->id_concursouf;
    }

    /**
     * @return \G2\Entity\Concurso id_concurso
     */
    public function getId_concurso ()
    {
        return $this->id_concurso;
    }

    /**
     * @return \G2\Entity\Uf sg_uf
     */
    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    /**
     * @param integer $id_concursouf
     * @return \G2\Entity\ConcursoUf
     */
    public function setId_concursouf ($id_concursouf)
    {
        $this->id_concursouf = $id_concursouf;
        return $this;
    }

    /**
     * @param \G2\Entity\Concurso $id_concurso
     * @return \G2\Entity\ConcursoUf
     */
    public function setId_concurso (Concurso $id_concurso)
    {
        $this->id_concurso = $id_concurso;
        return $this;
    }

    /**
     * @param \G2\Entity\Uf $sg_uf
     * @return \G2\Entity\ConcursoUf
     */
    public function setSg_uf (Uf $sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

}

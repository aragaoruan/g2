<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_entidadeesquemaconfiguracao")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwEntidadeEsquemaConfiguracao
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_esquemaconfiguracao
     * @Column(name="id_esquemaconfiguracao", type="integer", nullable=false, length=4)
     */
    private $id_esquemaconfiguracao;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_usuarioatualizacao
     * @Column(name="id_usuarioatualizacao", type="integer", nullable=false, length=4)
     */
    private $id_usuarioatualizacao;
    /**
     * @Id
     * @var integer $id_itemconfiguracao
     * @Column(name="id_itemconfiguracao", type="integer", nullable=false, length=4)
     */
    private $id_itemconfiguracao;
    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var datetime $dt_atualizacao
     * @Column(name="dt_atualizacao", type="datetime", nullable=true, length=8)
     */
    private $dt_atualizacao;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var nvarchar $st_esquemaconfiguracao
     * @Column(name="st_esquemaconfiguracao", type="string", nullable=false, length=400)
     */
    private $st_esquemaconfiguracao;
    /**
     * @var nvarchar $st_itemconfiguracao
     * @Column(name="st_itemconfiguracao", type="string", nullable=false, length=200)
     */
    private $st_itemconfiguracao;
    /**
     * @var nvarchar $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=510)
     */
    private $st_descricao;
    /**
     * @var nvarchar $st_default
     * @Column(name="st_default", type="string", nullable=false, length=400)
     */
    private $st_default;
    /**
     * @var nvarchar $st_valor
     * @Column(name="st_valor", type="string", nullable=false, length=400)
     */
    private $st_valor;

    /**
     * @return integer id_esquemaconfiguracao
     */
    public function getId_esquemaconfiguracao() {
        return $this->id_esquemaconfiguracao;
    }

    /**
     * @param id_esquemaconfiguracao
     */
    public function setId_esquemaconfiguracao($id_esquemaconfiguracao) {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_usuarioatualizacao
     */
    public function getId_usuarioatualizacao() {
        return $this->id_usuarioatualizacao;
    }

    /**
     * @param id_usuarioatualizacao
     */
    public function setId_usuarioatualizacao($id_usuarioatualizacao) {
        $this->id_usuarioatualizacao = $id_usuarioatualizacao;
        return $this;
    }

    /**
     * @return integer id_itemconfiguracao
     */
    public function getId_itemconfiguracao() {
        return $this->id_itemconfiguracao;
    }

    /**
     * @param id_itemconfiguracao
     */
    public function setId_itemconfiguracao($id_itemconfiguracao) {
        $this->id_itemconfiguracao = $id_itemconfiguracao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime dt_atualizacao
     */
    public function getDt_atualizacao() {
        return $this->dt_atualizacao;
    }

    /**
     * @param dt_atualizacao
     */
    public function setDt_atualizacao($dt_atualizacao) {
        $this->dt_atualizacao = $dt_atualizacao;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return nvarchar st_esquemaconfiguracao
     */
    public function getSt_esquemaconfiguracao() {
        return $this->st_esquemaconfiguracao;
    }

    /**
     * @param st_esquemaconfiguracao
     */
    public function setSt_esquemaconfiguracao($st_esquemaconfiguracao) {
        $this->st_esquemaconfiguracao = $st_esquemaconfiguracao;
        return $this;
    }

    /**
     * @return nvarchar st_itemconfiguracao
     */
    public function getSt_itemconfiguracao() {
        return $this->st_itemconfiguracao;
    }

    /**
     * @param st_itemconfiguracao
     */
    public function setSt_itemconfiguracao($st_itemconfiguracao) {
        $this->st_itemconfiguracao = $st_itemconfiguracao;
        return $this;
    }

    /**
     * @return nvarchar st_descricao
     */
    public function getSt_descricao() {
        return $this->st_descricao;
    }

    /**
     * @param st_descricao
     */
    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return nvarchar st_default
     */
    public function getSt_default() {
        return $this->st_default;
    }

    /**
     * @param st_default
     */
    public function setSt_default($st_default) {
        $this->st_default = $st_default;
        return $this;
    }

    /**
     * @return nvarchar st_valor
     */
    public function getSt_valor() {
        return $this->st_valor;
    }

    /**
     * @param st_valor
     */
    public function setSt_valor($st_valor) {
        $this->st_valor = $st_valor;
        return $this;
    }



}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_transferenciapolo")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @EntityLog
 */
class TransferenciaPolo extends G2Entity
{

    /**
     * @var integer $id_transferenciapolo
     * @Column(name="id_transferenciapolo", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_transferenciapolo;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(type="datetime", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo = false;

    /**
     * @var \G2\Entity\Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var \G2\Entity\Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula", nullable=false)
     */
    private $id_matricula;

    /**
     * @var \G2\Entity\Entidade $id_entidadeorigem
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadeorigem", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidadeorigem;

    /**
     * @var \G2\Entity\Entidade $id_entidadedestino
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadedestino", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidadedestino;

    /**
     * @var \G2\Entity\Venda $id_vendaorigem
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_vendaorigem", referencedColumnName="id_venda", nullable=false)
     */
    private $id_vendaorigem;

    /**
     * @var \G2\Entity\Venda $id_vendadestino
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_vendadestino", referencedColumnName="id_venda", nullable=false)
     */
    private $id_vendadestino;

    /**
     * @return int
     */
    public function getId_transferenciapolo()
    {
        return $this->id_transferenciapolo;
    }

    /**
     * @param int $id_transferenciapolo
     * @return $this
     */
    public function setId_transferenciapolo($id_transferenciapolo)
    {
        $this->id_transferenciapolo = $id_transferenciapolo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadeorigem()
    {
        return $this->id_entidadeorigem;
    }

    /**
     * @param Entidade $id_entidadeorigem
     * @return $this
     */
    public function setId_entidadeorigem($id_entidadeorigem)
    {
        $this->id_entidadeorigem = $id_entidadeorigem;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadedestino()
    {
        return $this->id_entidadedestino;
    }

    /**
     * @param Entidade $id_entidadedestino
     * @return $this
     */
    public function setId_entidadedestino($id_entidadedestino)
    {
        $this->id_entidadedestino = $id_entidadedestino;
        return $this;
    }

    /**
     * @return Venda
     */
    public function getId_vendaorigem()
    {
        return $this->id_vendaorigem;
    }

    /**
     * @param Venda $id_vendaorigem
     * @return $this
     */
    public function setId_vendaorigem($id_vendaorigem)
    {
        $this->id_vendaorigem = $id_vendaorigem;
        return $this;
    }

    /**
     * @return Venda
     */
    public function getId_vendadestino()
    {
        return $this->id_vendadestino;
    }

    /**
     * @param Venda $id_vendadestino
     * @return $this
     */
    public function setId_vendadestino($id_vendadestino)
    {
        $this->id_vendadestino = $id_vendadestino;
        return $this;
    }

}

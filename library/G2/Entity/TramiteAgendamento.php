<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tramiteagendamento")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class TramiteAgendamento {

    /**
     * @var integer $id_tramiteagendamento
     * @Column(name="id_tramiteagendamento",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramiteagendamento;

    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false)
     */
    private $id_tramite;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false)
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    public function getId_tramiteagendamento() {
        return $this->id_tramiteagendamento;
    }

    public function getId_tramite() {
        return $this->id_tramite;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function setId_tramiteagendamento($id_tramiteagendamento) {
        $this->id_tramiteagendamento = $id_tramiteagendamento;
        return $this;
    }

    public function setId_tramite($id_tramite) {
        $this->id_tramite = $id_tramite;
        return $this;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

}

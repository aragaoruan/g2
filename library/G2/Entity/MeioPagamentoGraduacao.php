<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_meiopagamentograduacao")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class MeioPagamentoGraduacao extends G2Entity
{

    /**
     * @var integer $id_meiopagamentograduacao
     * @Column(name="id_meiopagamentograduacao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_meiopagamentograduacao;

    /**
     * @var string $st_meiopagamentograduacao
     * @Column(name="st_meiopagamentograduacao", type="string", nullable=false, length=255)
     */
    private $st_meiopagamentograduacao;

    /**
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=false)
     */
    private $st_descricao;

    public function getId_meiopagamentograduacao ()
    {
        return $this->id_meiopagamentograduacao;
    }

    public function setId_meiopagamentograduacao ($id_meiopagamentograduacao)
    {
        $this->id_meiopagamentograduacao = $id_meiopagamentograduacao;
        return $this;
    }

    public function getSt_meiopagamentograduacao ()
    {
        return $this->st_meiopagamentograduacao;
    }

    public function setSt_meiopagamentograduacao ($st_meiopagamentograduacao)
    {
        $this->st_meiopagamentograduacao = $st_meiopagamentograduacao;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
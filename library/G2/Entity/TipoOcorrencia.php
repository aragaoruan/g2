<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoocorrencia")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoOcorrencia
{

    /**
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoocorrencia;

    /**
     * @var string $st_tipoocorrencia
     * @Column(name="st_tipoocorrencia", type="string", nullable=false, length=200)
     */
    private $st_tipoocorrencia;

    public function getId_tipoocorrencia ()
    {
        return $this->id_tipoocorrencia;
    }

    public function setId_tipoocorrencia ($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function getSt_tipoocorrencia ()
    {
        return $this->st_tipoocorrencia;
    }

    public function setSt_tipoocorrencia ($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
        return $this;
    }

}
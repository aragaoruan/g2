<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_categoriaendereco")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CategoriaEndereco
{

    /**
     *
     * @var integer $id_categoriaendereco
     * @Column(name="id_categoriaendereco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_categoriaendereco;

    /**
     *
     * @var string $st_categoriaendereco
     * @Column(name="st_categoriaendereco", type="string", nullable=false, length=255) 
     */
    private $st_categoriaendereco;

    public function getId_categoriaendereco ()
    {
        return $this->id_categoriaendereco;
    }

    public function setId_categoriaendereco ($id_categoriaendereco)
    {
        $this->id_categoriaendereco = $id_categoriaendereco;
        return $this;
    }

    public function getSt_categoriaendereco ()
    {
        return $this->st_categoriaendereco;
    }

    public function setSt_categoriaendereco ($st_categoriaendereco)
    {
        $this->st_categoriaendereco = $st_categoriaendereco;
        return $this;
    }

}
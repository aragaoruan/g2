<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtointegracao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ProdutoIntegracao
{

    /**
     * @var integer $id_produtointegracao
     * @Column(name="id_produtointegracao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtointegracao;

    /**
     * @var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", nullable=false, referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var string $nu_codprodutosistema
     * @Column(name="nu_codprodutosistema", type="string", nullable=false, length=10)
     */
    private $nu_codprodutosistema;

    /**
     * @return integer id_produtointegracao
     */
    public function getId_produtointegracao ()
    {
        return $this->id_produtointegracao;
    }

    /**
     * 
     * @param integer $id_produtointegracao
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function setId_produtointegracao ($id_produtointegracao)
    {
        $this->id_produtointegracao = $id_produtointegracao;
        return $this;
    }

    /**
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * @param \G2\Entity\Produto $id_produto
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function setId_produto (Produto $id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function getId_sistema ()
    {
        return $this->id_sistema;
    }

    /**
     * @param \G2\Entity\Sistema $id_sistema
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function setId_sistema (Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return string nu_codprodutosistema
     */
    public function getNu_codprodutosistema ()
    {
        return $this->nu_codprodutosistema;
    }

    /**
     * 
     * @param string $nu_codprodutosistema
     * @return \G2\Entity\ProdutoIntegracao
     */
    public function setNu_codprodutosistema ($nu_codprodutosistema)
    {
        $this->nu_codprodutosistema = $nu_codprodutosistema;
        return $this;
    }

}

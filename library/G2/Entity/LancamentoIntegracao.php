<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity(repositoryClass="G2\Repository\LancamentoIntegracao")
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_lancamentointegracao")
 *
 */
class LancamentoIntegracao {

    /**
     * @var integer $id_lancamentointegracao
     * @id
     * @Column(name="id_lancamentointegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_lancamentointegracao;

    /**
     * @var Lancamento
     *
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumns({
     *   @JoinColumn(name="id_lancamento", referencedColumnName="id_lancamento")
     * })
     */
    private $id_lancamento;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumns({
     *      @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     * })
     */
    private $id_sistema;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_codlancamento
     * @Column(name="st_codlancamento", type="string", nullable=true)
     */
    private $st_codlancamento;

    /**
     * @var string $st_codresponsavel
     * @Column(name="st_codresponsavel", type="string", nullable=true)
     */
    private $st_codresponsavel;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var datetime $dt_sincronizado
     * @Column(name="dt_sincronizado", type="datetime2", nullable=false)
     */
    private $dt_sincronizado;

    /**
     * @var string $st_codvenda
     * @Column(name="st_codvenda", type="string", nullable=true)
     */
    private $st_codvenda;

    /**
     * @param \G2\Entity\datetime $dt_cadastro
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \G2\Entity\datetime $dt_sincronizado
     */
    public function setDtSincronizado($dt_sincronizado)
    {
        $this->dt_sincronizado = $dt_sincronizado;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtSincronizado()
    {
        return $this->dt_sincronizado;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Lancamento $id_lancamento
     */
    public function setIdLancamento(\G2\Entity\Lancamento $id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return \G2\Entity\Lancamento
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamentointegracao
     */
    public function setIdLancamentointegracao($id_lancamentointegracao)
    {
        $this->id_lancamentointegracao = $id_lancamentointegracao;
    }

    /**
     * @return int
     */
    public function getIdLancamentointegracao()
    {
        return $this->id_lancamentointegracao;
    }

    /**
     * @param \G2\Entity\Sistema $id_sistema
     */
    public function setIdSistema(\G2\Entity\Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return \G2\Entity\Sistema
     */
    public function getIdSistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param string $st_codlancamento
     */
    public function setStCodlancamento($st_codlancamento)
    {
        $this->st_codlancamento = $st_codlancamento;
    }

    /**
     * @return string
     */
    public function getStCodlancamento()
    {
        return $this->st_codlancamento;
    }

    /**
     * @param string $st_codresponsavel
     */
    public function setStCodresponsavel($st_codresponsavel)
    {
        $this->st_codresponsavel = $st_codresponsavel;
    }

    /**
     * @return string
     */
    public function getStCodresponsavel()
    {
        return $this->st_codresponsavel;
    }

    /**
     * @return string
     */
    public function getStCodvenda()
    {
        return $this->st_codvenda;
    }

    /**
     * @param string $st_codvenda
     */
    public function setStCodvenda($st_codvenda)
    {
        $this->st_codvenda = $st_codvenda;
    }

    /**
     *
     * @PrePersist()
     */
    public function prePersist(){
        if (is_null($this->getDtCadastro())){
            $this->setDtCadastro(new \DateTime());
        }

        if (is_null($this->getDtSincronizado())){
            $this->setDtSincronizado(new \DateTime());
        }
    }

    /**
     *
     * @PreUpdate()
     */
    public function preUpdate(){
        $this->setDtSincronizado(new \DateTime());
    }
} 
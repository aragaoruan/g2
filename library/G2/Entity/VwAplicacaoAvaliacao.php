<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_aplicacaoavaliacao")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAplicacaoAvaliacao {

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", nullable=false, type="integer")
     */
    private $id_aplicadorprova;

    /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", nullable=false, type="integer")
     */
    private $id_endereco;

    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", nullable=false, type="date")
     */
    private $dt_aplicacao;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", nullable=false, type="string")
     */
    private $st_aplicadorprova;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", nullable=true, type="string")
     */
    private $sg_uf;

    /**
     * @var integer $id_municipio
     * @Column(name="id_municipio", nullable=true, type="integer")
     */
    private $id_municipio;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", nullable=true, type="string")
     */
    private $st_endereco;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", nullable=true, type="string")
     */
    private $st_cidade;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", nullable=true, type="string")
     */
    private $st_complemento;

    /**
     * @var integer $nu_numero
     * @Column(name="nu_numero", nullable=true, type="integer")
     */
    private $nu_numero;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", nullable=false, type="integer")
     */
    private $id_entidade;

    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function getDt_aplicacao() {
        return $this->dt_aplicacao;
    }

    public function getSt_aplicadorprova() {
        return $this->st_aplicadorprova;
    }

    public function getSg_uf() {
        return $this->sg_uf;
    }

    public function getId_municipio() {
        return $this->id_municipio;
    }

    public function getSt_endereco() {
        return $this->st_endereco;
    }

    public function getSt_cidade() {
        return $this->st_cidade;
    }

    public function getSt_complemento() {
        return $this->st_complemento;
    }

    public function getNu_numero() {
        return $this->nu_numero;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    public function setId_aplicadorprova($id_aplicadorprova) {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
    }

    public function setDt_aplicacao($dt_aplicacao) {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    public function setSt_aplicadorprova($st_aplicadorprova) {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }

    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
    }

    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    public function setSt_cidade($st_cidade) {
        $this->st_cidade = $st_cidade;
    }

    public function setSt_complemento($st_complemento) {
        $this->st_complemento = $st_complemento;
    }

    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

}

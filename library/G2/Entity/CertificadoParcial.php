<?php

/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 03/04/2018
 * Time: 15:11
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Table(name="tb_certificadoparcial")
 * @Entity(repositoryClass="\G2\Repository\CertificadoParcial")
 */
class CertificadoParcial extends G2Entity
{
    /**
     * @var integer $id_certificadoparcial
     * @Column(name="id_certificadoparcial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_certificadoparcial;

    /**
     * @var string $st_certificadoparcial
     * @Column(name="st_certificadoparcial", type="string", nullable=false, length=255)
     */
    private $st_certificadoparcial;

    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", nullable=false)
     */
    private $id_textosistema;

    /**
     * @var datetime2 $dt_iniciovigencia
     * @Column(name="dt_iniciovigencia", type="datetime2", nullable=false)
     */
    private $dt_iniciovigencia;

    /**
     * @var datetime2 $dt_fimvigencia
     * @Column(name="dt_fimvigencia", type="datetime2", nullable=true)
     */
    private $dt_fimvigencia;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_gerado
     * @Column(name="bl_gerado", type="boolean", nullable=false)
     */
    private $bl_gerado;

    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", nullable=false)
     */
    private $id_entidadecadastro;

    /**
     * @return int
     */
    public function getId_certificadoparcial()
    {
        return $this->id_certificadoparcial;
    }

    /**
     * @param int $id_certificadoparcial
     * @return $this
     */
    public function setId_certificadoparcial($id_certificadoparcial)
    {
        $this->id_certificadoparcial = $id_certificadoparcial;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_certificadoparcial()
    {
        return $this->st_certificadoparcial;
    }

    /**
     * @param string $st_certificadoparcial
     * @return $this
     */
    public function setSt_certificadoparcial($st_certificadoparcial)
    {
        $this->st_certificadoparcial = $st_certificadoparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param int $id_textosistema
     * @return $this
     */
    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_iniciovigencia()
    {
        return $this->dt_iniciovigencia;
    }

    /**
     * @param datetime $dt_iniciovigencia
     * @return $this
     */
    public function setDt_iniciovigencia($dt_iniciovigencia)
    {
        $this->dt_iniciovigencia = $dt_iniciovigencia;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_fimvigencia()
    {
        return $this->dt_fimvigencia;
    }

    /**
     * @param datetime $dt_fimvigencia
     * @return $this
     */
    public function setDt_fimvigencia($dt_fimvigencia)
    {
        $this->dt_fimvigencia = $dt_fimvigencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_gerado()
    {
        return $this->bl_gerado;
    }

    /**
     * @param bool $bl_gerado
     * @return $this
     */
    public function setBl_gerado($bl_gerado)
    {
        $this->bl_gerado = $bl_gerado;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

}

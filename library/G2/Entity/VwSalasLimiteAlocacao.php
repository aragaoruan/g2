<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_salaslimitealocacao")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwSalasLimiteAlocacao
{
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaula;
    /**
     * @var integer $id_notificacaoentidade
     * @Column(name="id_notificacaoentidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_notificacaoentidade;
    /**
     * @var integer $id_notificacao
     * @Column(name="id_notificacao", type="integer", nullable=true, length=4)
     */
    private $id_notificacao;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $nu_alunosalocados
     * @Column(name="nu_alunosalocados", type="integer", nullable=true, length=4)
     */
    private $nu_alunosalocados;
    /**
     * @var integer $nu_percentualalunossala
     * @Column(name="nu_percentualalunossala", type="integer", nullable=true, length=4)
     */
    private $nu_percentualalunossala;
    /**
     * @var integer $nu_percentuallimitealunosala
     * @Column(name="nu_percentuallimitealunosala", type="integer", nullable=true, length=4)
     */
    private $nu_percentuallimitealunosala;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var integer $id_usuarioprofessor
     * @Column(name="id_usuarioprofessor", type="integer", nullable=true, length=4)
     */
    private $id_usuarioprofessor;
//    /**
//     * @var integer $id_perfilenviar
//     * @Column(name="id_perfilenviar", type="integer", nullable=true, length=4)
//     * @Id
//     */
//    private $id_perfilenviar;

    /**
     * @var boolean $bl_enviaparaemail
     * @Column(name="bl_enviaparaemail", type="boolean", nullable=true, length=1)
     */
    private $bl_enviaparaemail;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_notificacao
     * @Column(name="st_notificacao", type="string", nullable=true, length=50)
     */
    private $st_notificacao;
    /**
     * @var string $st_nomeprofessor
     * @Column(name="st_nomeprofessor", type="string", nullable=true, length=300)
     */
    private $st_nomeprofessor;


    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_notificacaoentidade
     */
    public function getId_notificacaoentidade() {
        return $this->id_notificacaoentidade;
    }

    /**
     * @param id_notificacaoentidade
     */
    public function setId_notificacaoentidade($id_notificacaoentidade) {
        $this->id_notificacaoentidade = $id_notificacaoentidade;
        return $this;
    }

    /**
     * @return integer id_notificacao
     */
    public function getId_notificacao() {
        return $this->id_notificacao;
    }

    /**
     * @param id_notificacao
     */
    public function setId_notificacao($id_notificacao) {
        $this->id_notificacao = $id_notificacao;
        return $this;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    /**
     * @param nu_maxalunos
     */
    public function setNu_maxalunos($nu_maxalunos) {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return integer nu_alunosalocados
     */
    public function getNu_alunosalocados() {
        return $this->nu_alunosalocados;
    }

    /**
     * @param nu_alunosalocados
     */
    public function setNu_alunosalocados($nu_alunosalocados) {
        $this->nu_alunosalocados = $nu_alunosalocados;
        return $this;
    }

    /**
     * @return integer nu_percentualalunossala
     */
    public function getNu_percentualalunossala() {
        return $this->nu_percentualalunossala;
    }

    /**
     * @param nu_percentualalunossala
     */
    public function setNu_percentualalunossala($nu_percentualalunossala) {
        $this->nu_percentualalunossala = $nu_percentualalunossala;
        return $this;
    }

    /**
     * @return integer nu_percentuallimitealunosala
     */
    public function getNu_percentuallimitealunosala() {
        return $this->nu_percentuallimitealunosala;
    }

    /**
     * @param nu_percentuallimitealunosala
     */
    public function setNu_percentuallimitealunosala($nu_percentuallimitealunosala) {
        $this->nu_percentuallimitealunosala = $nu_percentuallimitealunosala;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_usuarioprofessor
     */
    public function getId_usuarioprofessor() {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor) {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
        return $this;
    }

//    /**
//     * @return integer id_perfilenviar
//     */
//    public function getId_perfilenviar() {
//        return $this->id_perfilenviar;
//    }
//
//    /**
//     * @param id_perfilenviar
//     */
//    public function setId_perfilenviar($id_perfilenviar) {
//        $this->id_perfilenviar = $id_perfilenviar;
//        return $this;
//    }
//



    /**
     * @return boolean bl_enviaparaemail
     */
    public function getBl_enviaparaemail() {
        return $this->bl_enviaparaemail;
    }

    /**
     * @param bl_enviaparaemail
     */
    public function setBl_enviaparaemail($bl_enviaparaemail) {
        $this->bl_enviaparaemail = $bl_enviaparaemail;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string st_notificacao
     */
    public function getSt_notificacao() {
        return $this->st_notificacao;
    }

    /**
     * @param st_notificacao
     */
    public function setSt_notificacao($st_notificacao) {
        $this->st_notificacao = $st_notificacao;
        return $this;
    }

    /**
     * @return string st_nomeprofessor
     */
    public function getSt_nomeprofessor() {
        return $this->st_nomeprofessor;
    }

    /**
     * @param st_nomeprofessor
     */
    public function setSt_nomeprofessor($st_nomeprofessor) {
        $this->st_nomeprofessor = $st_nomeprofessor;
        return $this;
    }

}
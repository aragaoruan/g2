<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtovalor")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class ProdutoValor extends G2Entity
{

    /**
     *
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtovalor;

    /**
     *
     * @var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;
    
    /**
     * @var date $dt_termino
     * @Column(type="datetime2", nullable=false, name="dt_termino")
     */
    private $dt_termino;
    
    /**
     * @var date $dt_inicio
     * @Column(type="datetime2", nullable=false, name="dt_inicio")
     */
    private $dt_inicio;
    
    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    
    
    /**
     * @var TipoProdutovalor $id_tipoprodutovalor
     * @ManyToOne(targetEntity="TipoProdutoValor")
     * @JoinColumn(name="id_tipoprodutovalor", nullable=false, referencedColumnName="id_tipoprodutovalor")
     */
    private $id_tipoprodutovalor;
    
    
    /**
     *
     * @var string $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false) 
     */
    private $nu_valor;
    
    
    /**
     *
     * @var string $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=false) 
     */
    private $nu_valormensal;
    
    
    /**
     *
     * @var string $nu_basepropor
     * @Column(name="nu_basepropor", type="decimal", nullable=false) 
     */
    private $nu_basepropor;
    
    public function getId_produtovalor() {
        return $this->id_produtovalor;
    }

    public function setId_produtovalor($id_produtovalor) {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getDt_termino() {
        return $this->dt_termino;
    }

    public function setDt_termino($dt_termino) {
        $this->dt_termino = $dt_termino;
        return $this;
    }

    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_tipoprodutovalor() {
        return $this->id_tipoprodutovalor;
    }

    public function setId_tipoprodutovalor($id_tipoprodutovalor) {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function getNu_valormensal() {
        return $this->nu_valormensal;
    }

    public function setNu_valormensal($nu_valormensal) {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    public function getNu_basepropor() {
        return $this->nu_basepropor;
    }

    public function setNu_basepropor($nu_basepropor) {
        $this->nu_basepropor = $nu_basepropor;
        return $this;
    }



}
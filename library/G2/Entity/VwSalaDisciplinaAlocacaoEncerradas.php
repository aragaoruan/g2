<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladisciplinaalocacaoencerradas")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwSalaDisciplinaAlocacaoEncerradas
{

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_saladeaula;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     * @SWG\Property(format="string")
     */
    private $st_saladeaula;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     * @SWG\Property(format="string")
     */
    private $st_projetopedagogico;

    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $nu_maxalunos;

    /**
     * @var integer $nu_alocados
     * @Column(name="nu_alocados", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $nu_alocados;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_usuario;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_trilha;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_matricula;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_disciplina;

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_categoriasala;

    /**
     * @Id
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     * @SWG\Property(format="integer")
     */
    private $id_alocacao;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     * @SWG\Property(format="date")
     */
    private $dt_encerramento;

    /**
     * @var date $dt_encerramentoext
     * @Column(name="dt_encerramentoext", type="date", nullable=true, length=3)
     * @SWG\Property(format="date")
     */
    private $dt_encerramentoext;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     * @SWG\Property(format="date")
     */
    private $dt_abertura;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=3)
     * @SWG\Property(format="boolean")
     */
    private $bl_ativo;

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    public function getNu_alocados()
    {
        return $this->nu_alocados;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    public function setNu_alocados($nu_alocados)
    {
        $this->nu_alocados = $nu_alocados;
        return $this;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_encerramentoext()
    {
        return $this->dt_encerramentoext;
    }

    /**
     * @param date $dt_encerramentoext
     */
    public function setdt_encerramentoext($dt_encerramentoext)
    {
        $this->dt_encerramentoext = $dt_encerramentoext;
    }


}

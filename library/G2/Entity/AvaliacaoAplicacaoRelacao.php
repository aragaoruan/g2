<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_avaliacaoaplicacaorelacao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvaliacaoAplicacaoRelacao
{


    /**
     *
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false)
     * @Id
     * @ManyToOne(targetEntity="AvaliacaoAplicacao")
     * @JoinColumn(name="id_avaliacaoaplicacao", nullable=false, referencedColumnName="id_avaliacaoaplicacao")
     */
    private $id_avaliacaoaplicacao;
    
    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false)
     * @ManyToOne(targetEntity="Avaliacao")
     * @Id
     * @JoinColumn(name="id_avaliacao", nullable=false, referencedColumnName="id_avaliacao")
     */
    private $id_avaliacao;
    
    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }



}
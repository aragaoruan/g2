<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_upload")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class Upload
{

    /**
     *
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_upload;

    /**
     *
     * @var integer $st_upload
     * @Column(name="st_upload", type="string", nullable=false)
     */
    private $st_upload;

    public function getId_upload() {
        return $this->id_upload;
    }

    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    public function getSt_upload() {
        return $this->st_upload;
    }

    public function setSt_upload($st_upload) {
        $this->st_upload = $st_upload;
        return $this;
    }


}
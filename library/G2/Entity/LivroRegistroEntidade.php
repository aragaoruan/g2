<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_livroregistroentidade")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */

class LivroRegistroEntidade
{

    /**
     * @var integer $id_livroregistroentidade
     * @Column(type="integer", nullable=false, name="id_livroregistroentidade")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_livroregistroentidade;
    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;
    /**
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=false, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;
    /**
     * @var string $st_livroregistroentidade
     * @Column(type="string", length=30, nullable=false, name="st_livroregistroentidade")
     */
    private $st_livroregistroentidade;
    /**
     * @var decimal $nu_folhas
     * @Column(type="decimal",nullable=false, name="nu_folhas")
     */
    private $nu_folhas;
    /**
     * @var decimal $nu_registros
     * @Column(type="decimal",nullable=false, name="nu_registros")
     */
    private $nu_registros;
    /**
     * @var decimal $nu_registroinicial
     * @Column(type="decimal", nullable=false, name="nu_registroinicial")
     */
    private $nu_registroinicial;


    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_livroregistroentidade($id_livroregistroentidade)
    {
        $this->id_livroregistroentidade = $id_livroregistroentidade;
    }

    public function getId_livroregistroentidade()
    {
        return $this->id_livroregistroentidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setNu_folhas($nu_folhas)
    {
        $this->nu_folhas = $nu_folhas;
    }

    public function getNu_folhas()
    {
        return $this->nu_folhas;
    }

    public function setNu_registroinicial($nu_registroinicial)
    {
        $this->nu_registroinicial = $nu_registroinicial;
    }

    public function getNu_registroinicial()
    {
        return $this->nu_registroinicial;
    }

    public function setNu_registros($nu_registros)
    {
        $this->nu_registros = $nu_registros;
    }

    public function getNu_registros()
    {
        return $this->nu_registros;
    }

    public function setSt_livroregistroentidade($st_livroregistroentidade)
    {
        $this->st_livroregistroentidade = $st_livroregistroentidade;
    }

    public function getSt_livroregistroentidade()
    {
        return $this->st_livroregistroentidade;
    }
}
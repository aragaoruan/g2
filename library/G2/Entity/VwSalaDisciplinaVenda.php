<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladisciplinavenda")
 * @Entity
 * @EntityView
 */

class VwSalaDisciplinaVenda
{

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_saladeaula;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     */
    private $id_alocacao;
    /**
     * @var boolean $bl_alocado
     * @Column(name="bl_alocado", type="boolean", nullable=false, length=1)
     */
    private $bl_alocado;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $nu_alocados
     * @Column(name="nu_alocados", type="integer", nullable=true, length=4)
     */
    private $nu_alocados;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;

    /**
     * @var integer $id_evolucaovenda
     * @Column(name="id_evolucaovenda", type="integer", nullable=false, length=4)
     */
    private $id_evolucaovenda;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;


    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param nu_maxalunos
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_alocacao
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param id_alocacao
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_alocado()
    {
        return $this->bl_alocado;
    }

    /**
     * @param bool $bl_alocado
     * @return $this
     */
    public function setBl_alocado($bl_alocado)
    {
        $this->bl_alocado = $bl_alocado;

        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer nu_alocados
     */
    public function getNu_alocados()
    {
        return $this->nu_alocados;
    }

    /**
     * @param nu_alocados
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setNu_alocados($nu_alocados)
    {
        $this->nu_alocados = $nu_alocados;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     * @return \G2\Entity\VwSalaDisciplinaVenda
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucaovenda()
    {
        return $this->id_evolucaovenda;
    }

    /**
     * @param int $id_evolucaovenda
     */
    public function setId_evolucaovenda($id_evolucaovenda)
    {
        $this->id_evolucaovenda = $id_evolucaovenda;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Débora Castro <debora.castro@unyleya.com.br>
 * Date: 11/11/14
 * Time: 10:11
 */

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_matriculadisciplina")
 * @Entity
 * @EntityView
 */
class VwMatriculaDisciplina {

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=false, length=4)
     */
    private $id_serie;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @var decimal $nu_aprovafinal
     * @Column(name="nu_aprovafinal", type="decimal", nullable=true, length=5)
     */
    private $nu_aprovafinal;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_serie
     * @Column(name="st_serie", type="string", nullable=true, length=255)
     */
    private $st_serie;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true, length=255)
     */
    private $st_areaconhecimento;

    /**
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio", type="boolean", nullable=true)
     */
    private $bl_obrigatorio;

    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", type="integer", nullable=false, length=4)
     */
    private $id_situacaoagendamento;


    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param boolean $bl_obrigatorio
     */
    public function setBl_obrigatorio($bl_obrigatorio)
    {
        $this->bl_obrigatorio = $bl_obrigatorio;
    }

    /**
     * @return boolean
     */
    public function getBl_obrigatorio()
    {
        return $this->bl_obrigatorio;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_serie
     */
    public function getId_serie() {
        return $this->id_serie;
    }

    /**
     * @param id_serie
     */
    public function setId_serie($id_serie) {
        $this->id_serie = $id_serie;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return decimal nu_aprovafinal
     */
    public function getNu_aprovafinal() {
        return $this->nu_aprovafinal;
    }

    /**
     * @param nu_aprovafinal
     */
    public function setNu_aprovafinal($nu_aprovafinal) {
        $this->nu_aprovafinal = $nu_aprovafinal;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_serie
     */
    public function getSt_serie() {
        return $this->st_serie;
    }

    /**
     * @param st_serie
     */
    public function setSt_serie($st_serie) {
        $this->st_serie = $st_serie;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_areaconhecimento
     */
    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param int $id_situacaoagendamento
     * @return $this
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
        return $this;
    }



} 
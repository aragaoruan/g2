<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_uf")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */

use G2\G2Entity;

class Uf extends G2Entity
{

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=false, length=2)
     * @Id
     */
    private $sg_uf;

    /**
     * @var string $st_uf
     * @Column(name="st_uf", type="string", nullable=false, length=100) 
     */
    private $st_uf;

    /**
     * @var decimal $nu_codigocapital
     * @Column(name="nu_codigocapital", type="decimal", nullable=true) 
     */
    private $nu_codigocapital;

    /**
     * @var decimal $nu_codigoibge
     * @Column(name="nu_codigoibge", type="decimal", nullable=true) 
     */
    private $nu_codigoibge;

    /**
     * @var string $st_nomeregiao
     * @Column(name="st_nomeregiao", type="string", nullable=true, length=30) 
     */
    private $st_nomeregiao;

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getSt_uf ()
    {
        return $this->st_uf;
    }

    public function setSt_uf ($st_uf)
    {
        $this->st_uf = $st_uf;
        return $this;
    }

    public function getNu_codigocapital ()
    {
        return $this->nu_codigocapital;
    }

    public function setNu_codigocapital ($nu_codigocapital)
    {
        $this->nu_codigocapital = $nu_codigocapital;
        return $this;
    }

    public function getNu_codigoibge ()
    {
        return $this->nu_codigoibge;
    }

    public function setNu_codigoibge ($nu_codigoibge)
    {
        $this->nu_codigoibge = $nu_codigoibge;
        return $this;
    }

    public function getSt_nomeregiao ()
    {
        return $this->st_nomeregiao;
    }

    public function setSt_nomeregiao ($st_nomeregiao)
    {
        $this->st_nomeregiao = $st_nomeregiao;
        return $this;
    }

    public function toArray ()
    {
        return array(
            'sg_uf' => $this->sg_uf,
            'st_uf' => $this->st_uf,
            'nu_codigocapital' => $this->nu_codigocapital,
            'nu_codigoibge' => $this->nu_codigoibge,
            'st_nomeregiao' => $this->st_nomeregiao,
        );
    }

}
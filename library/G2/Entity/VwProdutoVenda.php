<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * Class VwProdutoVenda
 * @package G2\Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_produtovenda")
 * @Entity
 * @EntityView
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwProdutoVenda extends G2Entity {

    /**
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_vendaproduto;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;

    /**
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false, length=4)
     */
    private $id_tipoproduto;

    /**
     * @var integer $id_tiposelecao
     * @Column(name="id_tiposelecao", type="integer", nullable=false, length=4)
     */
    private $id_tiposelecao;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;

    /**
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=true, length=4)
     */
    private $id_tipoprodutovalor;

    /**
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=true, length=4)
     */
    private $id_campanhacomercial;

    /**
     * @var integer $id_modelovenda
     * @Column(name="id_modelovenda", type="integer", nullable=false, length=4)
     */
    private $id_modelovenda;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var decimal $nu_valorbruto
     * @Column(name="nu_valorbruto", type="decimal", nullable=false, length=17)
     */
    private $nu_valorbruto;

    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     */
    private $id_turma;

    /**
     * @var decimal $nu_descontoporcentagem
     * @Column(name="nu_descontoporcentagem", type="decimal", nullable=true, length=17)
     */
    private $nu_descontoporcentagem;

    /**
     * @var decimal $nu_descontovalor
     * @Column(name="nu_descontovalor", type="decimal", nullable=true, length=17)
     */
    private $nu_descontovalor;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;

    /**
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=false, length=255)
     */
    private $st_tipoproduto;

    /**
     * @var string $st_tiposelecao
     * @Column(name="st_tiposelecao", type="string", nullable=false, length=255)
     */
    private $st_tiposelecao;

    /**
     * @var string $st_tipoprodutovalor
     * @Column(name="st_tipoprodutovalor", type="string", nullable=true, length=250)
     */
    private $st_tipoprodutovalor;

    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=true, length=255)
     */
    private $st_campanhacomercial;

    /**
     * @var string $st_produtocombo
     * @Column(name="st_produtocombo", type="string", nullable=true, length=250)
     */
    private $st_produtocombo;

    /**
     * @var string $st_modelovenda
     * @Column(name="st_modelovenda", type="string", nullable=true, length=20)
     */
    private $st_modelovenda;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @return int
     */
    public function getid_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param int $id_tiposelecao
     */
    public function setid_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tiposelecao()
    {
        return $this->st_tiposelecao;
    }

    /**
     * @param string $st_tiposelecao
     */
    public function setst_tiposelecao($st_tiposelecao)
    {
        $this->st_tiposelecao = $st_tiposelecao;
        return $this;

    }


    function getId_vendaproduto() {
        return $this->id_vendaproduto;
    }

    function getId_produto() {
        return $this->id_produto;
    }

    function getId_matricula()
    {
        return $this->id_matricula;
    }

    function getId_tipoproduto() {
        return $this->id_tipoproduto;
    }

    function getId_venda() {
        return $this->id_venda;
    }

    function getId_tipoprodutovalor() {
        return $this->id_tipoprodutovalor;
    }

    function getId_campanhacomercial() {
        return $this->id_campanhacomercial;
    }

    function getId_modelovenda() {
        return $this->id_modelovenda;
    }

    function getBl_ativo() {
        return $this->bl_ativo;
    }

    function getNu_valorbruto() {
        return $this->nu_valorbruto;
    }

    function getId_turma() {
        return $this->id_turma;
    }

    function getNu_valorliquido() {
        return $this->nu_valorliquido;
    }

    function getNu_descontoporcentagem() {
        return $this->nu_descontoporcentagem;
    }

    function getNu_descontovalor() {
        return $this->nu_descontovalor;
    }

    function getSt_produto() {
        return $this->st_produto;
    }

    function getSt_tipoproduto() {
        return $this->st_tipoproduto;
    }

    function getSt_tipoprodutovalor() {
        return $this->st_tipoprodutovalor;
    }

    function getSt_campanhacomercial() {
        return $this->st_campanhacomercial;
    }

    function getSt_produtocombo() {
        return $this->st_produtocombo;
    }

    function getSt_modelovenda() {
        return $this->st_modelovenda;
    }
    function getId_entidade() {
        return $this->id_entidade;
    }

    function setId_vendaproduto($id_vendaproduto) {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    function setId_tipoproduto($id_tipoproduto) {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
        return $this;
    }

    function setId_tipoprodutovalor($id_tipoprodutovalor) {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    function setId_campanhacomercial($id_campanhacomercial) {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    function setId_modelovenda($id_modelovenda) {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    function setNu_valorbruto($nu_valorbruto) {
        $this->nu_valorbruto = $nu_valorbruto;
        return $this;
    }

    function setNu_valorliquido($nu_valorliquido) {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    function setNu_descontoporcentagem($nu_descontoporcentagem) {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
        return $this;
    }

    function setNu_descontovalor($nu_descontovalor) {
        $this->nu_descontovalor = $nu_descontovalor;
        return $this;
    }

    function setSt_produto($st_produto) {
        $this->st_produto = $st_produto;
        return $this;
    }

    function setSt_tipoproduto($st_tipoproduto) {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    function setSt_tipoprodutovalor($st_tipoprodutovalor) {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
        return $this;
    }

    function setSt_campanhacomercial($st_campanhacomercial) {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    function setSt_produtocombo($st_produtocombo) {
        $this->st_produtocombo = $st_produtocombo;
        return $this;
    }

    function setSt_modelovenda($st_modelovenda) {
        $this->st_modelovenda = $st_modelovenda;
        return $this;
    }
    function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

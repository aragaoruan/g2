<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_geraretiquetas")
 * @Entity
 * @EntityView
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class VwGerarEtiquetas extends G2Entity
{
    /**
     * @Id
     * @var integer $id_entregadeclaracao
     * @Column(name="id_entregadeclaracao", type="integer", nullable=false, length=4)
     */
    private $id_entregadeclaracao;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true, length=4)
     */
    private $id_entidadematricula;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=true, length=200)
     */
    private $st_textosistema;
    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;
    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500)
     */
    private $st_complemento;
    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;
    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12)
     */
    private $st_cep;
    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;
    /**
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=255)
     */
    private $st_nomemunicipio;
    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;
    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_solicitacao;


    /**
     * @return integer id_entregadeclaracao
     */
    public function getId_entregadeclaracao() {
        return $this->id_entregadeclaracao;
    }

    /**
     * @param id_entregadeclaracao
     */
    public function setId_entregadeclaracao($id_entregadeclaracao) {
        $this->id_entregadeclaracao = $id_entregadeclaracao;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidadematricula
     */
    public function getId_entidadematricula() {
        return $this->id_entidadematricula;
    }

    /**
     * @param id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula) {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_textosistema
     */
    public function getSt_textosistema() {
        return $this->st_textosistema;
    }

    /**
     * @param st_textosistema
     */
    public function setSt_textosistema($st_textosistema) {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

    /**
     * @return string st_endereco
     */
    public function getSt_endereco() {
        return $this->st_endereco;
    }

    /**
     * @param st_endereco
     */
    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return string st_complemento
     */
    public function getSt_complemento() {
        return $this->st_complemento;
    }

    /**
     * @param st_complemento
     */
    public function setSt_complemento($st_complemento) {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * @return string st_bairro
     */
    public function getSt_bairro() {
        return $this->st_bairro;
    }

    /**
     * @param st_bairro
     */
    public function setSt_bairro($st_bairro) {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    /**
     * @return string st_cep
     */
    public function getSt_cep() {
        return $this->st_cep;
    }

    /**
     * @param st_cep
     */
    public function setSt_cep($st_cep) {
        $this->st_cep = $st_cep;
        return $this;
    }

    /**
     * @return string nu_numero
     */
    public function getNu_numero() {
        return $this->nu_numero;
    }

    /**
     * @param nu_numero
     */
    public function setNu_numero($nu_numero) {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string st_nomemunicipio
     */
    public function getSt_nomemunicipio() {
        return $this->st_nomemunicipio;
    }

    /**
     * @param st_nomemunicipio
     */
    public function setSt_nomemunicipio($st_nomemunicipio) {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    /**
     * @return string st_email
     */
    public function getSt_email() {
        return $this->st_email;
    }

    /**
     * @param st_email
     */
    public function setSt_email($st_email) {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return char sg_uf
     */
    public function getSg_uf() {
        return $this->sg_uf;
    }

    /**
     * @param sg_uf
     */
    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
        return $this;
    }


    /**
     * @return datetime2 dt_solicitacao
     */
    public function getDt_solicitacao() {
        return $this->dt_solicitacao;
    }

    /**
     * @param dt_solicitacao
     */
    public function setDt_solicitacao($dt_solicitacao) {
        $this->dt_solicitacao = $dt_solicitacao;
        return $this;
    }


}
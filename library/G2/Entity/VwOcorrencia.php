<?php

namespace G2\Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwOcorrencia"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_ocorrencia")
 * @Entity(repositoryClass="\G2\Repository\Ocorrencia")
 * @EntityView
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 */
class VwOcorrencia
{

    /**
     * @SWG\Property(format="datetime")
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @SWG\Property(format="datetime")
     * @var datetime2 $dt_ultimotramite
     * @Column(name="dt_ultimotramite", type="datetime2", nullable=true, length=8)
     */
    private $dt_ultimotramite;

    /**
     * @SWG\Property(format="integer")
     * @ID
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false, length=4)
     */
    private $id_ocorrencia;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuariointeressado
     * @Column(name="id_usuariointeressado", type="integer", nullable=false, length=4)
     */
    private $id_usuariointeressado;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_categoriaocorrencia;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=true, length=4)
     */
    private $id_assuntoco;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @SWG\Property(format="string")
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true)
     */
    private $st_saladeaula;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_tipoocorrencia;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuarioresponsavel
     * @Column(name="id_usuarioresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_usuarioresponsavel;

    /**
     * @SWG\Property(format="datetime")
     * @var datetime $dt_atendimento
     * @Column(name="dt_atendimento", type="datetime", nullable=true, length=8)
     */
    private $dt_atendimento;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @SWG\Property(format="string")
     * @var string $st_urlportal
     * @Column(name="st_urlportal", type="string", nullable=true, length=400)
     */
    private $st_urlportal;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomeinteressado
     * @Column(name="st_nomeinteressado", type="string", nullable=false, length=300)
     */
    private $st_nomeinteressado;

    /**
     * @SWG\Property(format="string")
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @SWG\Property(format="string")
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=true)
     */
    private $st_login;

    /**
     * @SWG\Property(format="string")
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string", nullable=true, length=61)
     */
    private $st_telefone;

    /**
     * @SWG\Property(format="string")
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=false)
     */
    private $st_titulo;

    /**
     * @SWG\Property(format="string")
     * @var string $st_ocorrencia
     * @Column(name="st_ocorrencia", type="string", nullable=false)
     */
    private $st_ocorrencia;

    /**
     * @SWG\Property(format="string")
     * @var string $st_assuntocopai
     * @Column(name="st_assuntocopai", type="string", nullable=true, length=200)
     */
    private $st_assuntocopai;

    /**
     * @SWG\Property(format="string")
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     * @SWG\Property(format="string")
     * @var string $st_categoriaocorrencia
     * @Column(name="st_categoriaocorrencia", type="string", nullable=false, length=250)
     */
    private $st_categoriaocorrencia;

    /**
     * @SWG\Property(format="string")
     * @var string $st_ultimotramite
     * @Column(name="st_ultimotramite", type="string", nullable=true)
     */
    private $st_ultimotramite;

    /**
     * @SWG\Property(format="string")
     * @var datetime $dt_ultimaacao
     * @Column(name="dt_ultimaacao", type="datetime", nullable=true)
     */
    private $dt_ultimaacao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_tramite
     * @Column(name="st_tramite", type="string", nullable=true)
     */
    private $st_tramite;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomeresponsavel
     * @Column(name="st_nomeresponsavel", type="string", nullable=true, length=300)
     */
    private $st_nomeresponsavel;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_ocorrenciaoriginal
     * @Column(name="id_ocorrenciaoriginal", type="integer", nullable=true, length=4)
     */
    private $id_ocorrenciaoriginal;


    /**
     * @SWG\Property(format="string")
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;


    /**
     * @SWG\Property(format="string")
     * @var string $st_codissue
     * @Column(name="st_codissue", type="string", nullable=true, length=50)
     */
    private $st_codissue;


    /**
     * @SWG\Property(format="string")
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true)
     */
    private $st_senha;


    /**
     * @SWG\Property(format="string")
     * @var integer $id_evolucaomatricula
     * @Column(name="id_evolucaomatricula", type="integer", nullable=true)
     */
    private $id_evolucaomatricula;

    /**
     * @return string
     */
    public function getst_urlportal()
    {
        return $this->st_urlportal;
    }

    /**
     * @param string $st_urlportal
     */
    public function setst_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
        return $this;

    }





    /**
     * @return datetime
     */
    public function getDtUltimaacao()
    {
        return $this->dt_ultimaacao;
    }

    /**
     * @param datetime $dt_ultimaacao
     */
    public function setDtUltimaacao($dt_ultimaacao)
    {
        $this->dt_ultimaacao = $dt_ultimaacao;
    }

    /**
     * @return int
     */
    public function getId_evolucaomatricula()
    {
        return $this->id_evolucaomatricula;
    }

    /**
     * @param int $id_evolucaomatricula
     * @return VwOcorrencia
     */
    public function setId_evolucaomatricula($id_evolucaomatricula)
    {
        $this->id_evolucaomatricula = $id_evolucaomatricula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return VwOcorrencia
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param string $st_senha
     * @return VwOcorrencia
     */
    public function setSt_senha($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codissue()
    {
        return $this->st_codissue;
    }

    /**
     * @param string $st_codissue
     * @return Ocorrencia
     */
    public function setSt_codissue($st_codissue)
    {
        $this->st_codissue = $st_codissue;
        return $this;
    }


    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getDt_ultimotramite()
    {
        return $this->dt_ultimotramite;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function getId_usuarioresponsavel()
    {
        return $this->id_usuarioresponsavel;
    }

    public function getDt_atendimento()
    {
        return $this->dt_atendimento;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function getSt_nomeinteressado()
    {
        return $this->st_nomeinteressado;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function getSt_telefone()
    {
        return $this->st_telefone;
    }

    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    public function getSt_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    public function getSt_assuntocopai()
    {
        return $this->st_assuntocopai;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function getSt_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    public function getSt_ultimotramite()
    {
        return $this->st_ultimotramite;
    }

    public function getSt_tramite()
    {
        return $this->st_tramite;
    }

    public function getSt_nomeresponsavel()
    {
        return $this->st_nomeresponsavel;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setDt_cadastro(datetime2 $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setDt_ultimotramite(datetime2 $dt_ultimotramite)
    {
        $this->dt_ultimotramite = $dt_ultimotramite;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function setId_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    public function setId_usuarioresponsavel($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
    }

    public function setDt_atendimento(datetime $dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
    }

    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    public function setSt_nomeinteressado($st_nomeinteressado)
    {
        $this->st_nomeinteressado = $st_nomeinteressado;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    public function setSt_telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
    }

    public function setSt_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
    }

    public function setSt_assuntocopai($st_assuntocopai)
    {
        $this->st_assuntocopai = $st_assuntocopai;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    public function setSt_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
    }

    public function setSt_ultimotramite($st_ultimotramite)
    {
        $this->st_ultimotramite = $st_ultimotramite;
    }

    public function setSt_tramite($st_tramite)
    {
        $this->st_tramite = $st_tramite;
    }

    public function setSt_nomeresponsavel($st_nomeresponsavel)
    {
        $this->st_nomeresponsavel = $st_nomeresponsavel;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getId_ocorrenciaoriginal()
    {
        return $this->id_ocorrenciaoriginal;
    }

    public function setId_ocorrenciaoriginal($id_ocorrenciaoriginal)
    {
        $this->id_ocorrenciaoriginal = $id_ocorrenciaoriginal;
    }

    /**
     * @return string
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @param string $st_login
     * @return VwOcorrencia
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }


}

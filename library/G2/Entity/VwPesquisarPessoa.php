<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pesquisarpessoa")
 * @Entity(repositoryClass="G2\Repository\VwPesquisarPessoa")
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarPessoa extends G2Entity
{

    const REPOSITORY = '\G2\Entity\VwPesquisarPessoa';

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;


    /**
     *
     * @var string $st_entidade
     * @Column(name="st_entidade", type="string", nullable=false, length=300)
     */
    private $st_entidade;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_nomeresponsavellegal
     * @Column(name="st_nomeresponsavellegal", type="string", nullable=false, length=300)
     */
    private $st_nomeresponsavellegal;

    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false, length=11)
     */
    private $st_cpf;

    /**
     *
     * @var string $st_cpfresponsavellegal
     * @Column(name="st_cpfresponsavellegal", type="string", nullable=false, length=11)
     */
    private $st_cpfresponsavellegal;

    /**
     *
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=true)
     */
    private $id_municipio;

    /**
     *
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     *
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=255)
     */
    private $st_nomemunicipio;

    /**
     *
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var string $st_sexo
     * @Column(name="st_sexo", type="string", nullable=true, length=1)
     */
    private $st_sexo;

    /**
     *
     * @var date $dt_nascimento
     * @Column(name="dt_nascimento", type="date", nullable=true)
     */
    private $dt_nascimento;

    /**
     *
     * @var string $st_nomepai
     * @Column(name="st_nomepai", type="string", nullable=true, length=255)
     */
    private $st_nomepai;

    /**
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string")
     */
    private $st_telefone;


    /**
     * @var string $st_telefonealternativo
     * @Column(name="st_telefonealternativo", type="string")
     */
    private $st_telefonealternativo;

    /**
     * @var integer $nu_dddresponsavellegal
     * @Column(name="nu_dddresponsavellegal", type="string")
     */
    private $nu_dddresponsavellegal;

    /**
     * @var integer $nu_telefoneresponsavellegal
     * @Column(name="nu_telefoneresponsavellegal", type="string")
     */
    private $nu_telefoneresponsavellegal;

    /**
     *
     * @var string $st_nomemae
     * @Column(name="st_nomemae", type="string", nullable=true, length=100)
     */
    private $st_nomemae;

    /**
     * @var string
     * @Column(name="st_identificacao", type="string", nullable=true)
     */
    private $st_identificacao;

    /**
     * @var string
     * @Column(name="st_passaporte", type="string", nullable=true)
     */
    private $st_passaporte;

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function getId_municipio()
    {
        return $this->id_municipio;
    }

    public function setId_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getSt_nomemunicipio()
    {
        return $this->st_nomemunicipio;
    }

    public function setSt_nomemunicipio($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getSt_sexo()
    {
        return $this->st_sexo;
    }

    public function setSt_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    public function setDt_nascimento(date $dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    public function setSt_telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    public function getSt_telefone()
    {
        return $this->st_telefone;
    }

    public function setSt_telefonealternativo($st_telefonealternativo)
    {
        $this->st_telefonealternativo = $st_telefonealternativo;
    }

    public function getSt_telefonealternativo()
    {
        return $this->st_telefonealternativo;
    }

    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
        return $this;
    }

    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }

    public function setSt_passaporte($st_passaporte)
    {
        $this->st_passaporte = $st_passaporte;
        return $this;
    }

    public function getSt_passaporte()
    {
        return $this->st_passaporte;
    }

    /**
     * @return string
     */
    public function getSt_nomeresponsavellegal()
    {
        return $this->st_nomeresponsavellegal;
    }

    /**
     * @param string $st_nomeresponsavellegal
     */
    public function setSt_nomeresponsavellegal($st_nomeresponsavellegal)
    {
        $this->st_nomeresponsavellegal = $st_nomeresponsavellegal;
    }

    /**
     * @return int
     */
    public function getNu_dddresponsavellegal()
    {
        return $this->nu_dddresponsavellegal;
    }

    /**
     * @param int $nu_dddresponsavellegal
     */
    public function setNu_dddresponsavellegal($nu_dddresponsavellegal)
    {
        $this->nu_dddresponsavellegal = $nu_dddresponsavellegal;
    }

    /**
     * @return int
     */
    public function getNu_telefoneresponsavellegal()
    {
        return $this->nu_telefoneresponsavellegal;
    }

    /**
     * @param int $nu_telefoneresponsavellegal
     */
    public function setNu_telefoneresponsavellegal($nu_telefoneresponsavellegal)
    {
        $this->nu_telefoneresponsavellegal = $nu_telefoneresponsavellegal;
    }

    /**
     * @return string
     */
    public function getSt_cpfresponsavellegal()
    {
        return $this->st_cpfresponsavellegal;
    }

    /**
     * @param string $st_cpfresponsavellegal
     */
    public function setSt_cpfresponsavellegal($st_cpfresponsavellegal)
    {
        $this->st_cpfresponsavellegal = $st_cpfresponsavellegal;
    }

    /**
     * @return string
     */
    public function getSt_entidade()
    {
        return $this->st_entidade;
    }

    /**
     * @param string $st_entidade
     */
    public function setSt_entidade($st_entidade)
    {
        $this->st_entidade = $st_entidade;
    }


}

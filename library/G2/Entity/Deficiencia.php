<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_deficiencia")
 * @Entity
 * @EntityLog
 */
class Deficiencia extends G2Entity
{
    /**
     * @var int
     * @Column(name="id_deficiencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_deficiencia;

    /**
     * @var string
     * @Column(name="st_deficiencia", type="string", nullable=false, length=60)
     */
    private $st_deficiencia;

    /**
     * @var string
     * @Column(name="st_descricao", type="string", nullable=true, length=120)
     */
    private $st_descricao;

    /**
     * @return int
     */
    public function getId_deficiencia()
    {
        return $this->id_deficiencia;
    }

    /**
     * @param int $id_deficiencia
     * @return $this
     */
    public function setId_deficiencia($id_deficiencia)
    {
        $this->id_deficiencia = $id_deficiencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_deficiencia()
    {
        return $this->st_deficiencia;
    }

    /**
     * @param string $st_deficiencia
     * @return $this
     */
    public function setSt_deficiencia($st_deficiencia)
    {
        $this->st_deficiencia = $st_deficiencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return $this
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }
}

<?php

/*
 * Entity ItemDeMaterialDisciplina
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-24
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_itemdematerialdisciplina")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class ItemDeMaterialDisciplina {

    /**
     *
     * @var integer $id_itemdematerialdisciplina
     * @Column(name="id_itemdematerialdisciplina", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemdematerialdisciplina;


    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=true)
     */
    private $id_itemdematerial;

    /**
     * @var Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", nullable=false, referencedColumnName="id_disciplina")
     */
    private $id_disciplina;

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function getId_itemdematerial()
    {
        return $this->id_itemdematerial;
    }

    public function getId_itemdematerialdisciplina()
    {
        return $this->id_itemdematerialdisciplina;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function setId_itemdematerial($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
    }

    public function setId_itemdematerialdisciplina($id_itemdematerialdisciplina)
    {
        $this->id_itemdematerialdisciplina = $id_itemdematerialdisciplina;
    }

}

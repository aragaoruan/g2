<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_logacesso")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-04-11
 */
class LogAcesso
{

    /**
     * @var integer $id_logacesso
     * @Column(name="id_logacesso", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_logacesso;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=false)
     */
    private $id_funcionalidade;

    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=true)
     */
    private $id_perfil;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $st_parametros
     * @Column(name="st_parametros", type="string", nullable=true)
     */
    private $st_parametros;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false)
     */
    private $id_sistema;

    /**
     * @var string $st_urlcompleta
     * @Column(name="st_urlcompleta", type="string", nullable=false)
     */
    private $st_urlcompleta;

    /**
     * @return integer
     */
    public function getId_logacesso ()
    {
        return $this->id_logacesso;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return integer
     */
    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @return integer
     */
    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    /**
     * @return integer
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @return integer
     */
    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    /**
     * @return integer
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_parametros ()
    {
        return $this->st_parametros;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @param integer $id_logacesso
     * @return \G2\Entity\LogAcesso
     */
    public function setId_logacesso ($id_logacesso)
    {
        $this->id_logacesso = $id_logacesso;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\LogAcesso
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param integer $id_funcionalidade
     * @return \G2\Entity\LogAcesso
     */
    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    /**
     * @param integer $id_perfil
     * @return \G2\Entity\LogAcesso
     */
    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @param integer $id_usuario
     * @return \G2\Entity\LogAcesso
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param integer $id_saladeaula
     * @return \G2\Entity\LogAcesso
     */
    public function setId_saladeaula ($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @param integer $id_entidade
     * @return \G2\Entity\LogAcesso
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param string $st_parametros
     * @return \G2\Entity\LogAcesso
     */
    public function setSt_parametros ($st_parametros)
    {
        $this->st_parametros = $st_parametros;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     * @return \G2\Entity\LogAcesso
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlcompleta()
    {
        return $this->st_urlcompleta;
    }

    /**
     * @param string $st_urlcompleta
     * @return \G2\Entity\LogAcesso
     */
    public function setSt_urlcompleta($st_urlcompleta)
    {
        $this->st_urlcompleta = $st_urlcompleta;
        return $this;
    }

}

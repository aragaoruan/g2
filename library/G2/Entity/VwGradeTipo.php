<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gradetipo")
 * @Entity
 * @EntityView
 */

class VwGradeTipo
{
    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @Id
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacao;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var boolean $bl_status
     * @Column(name="bl_status", type="boolean", nullable=false, length=4)
     */
    private $bl_status;

    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false, length=4)
     */
    private $id_tipoavaliacao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var float $nu_notatotal
     * @Column(name="nu_notatotal", type="float", nullable=true, length=8)
     */
    private $nu_notatotal;

    /**
     * @var decimal $st_nota
     * @Column(name="st_nota", type="decimal", nullable=true, length=5)
     */
    private $st_nota;

    /**
     * @var decimal $nu_notafinal
     * @Column(name="nu_notafinal", type="decimal", nullable=true, length=5)
     */
    private $nu_notafinal;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;

    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(name="st_tituloexibicaomodulo", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaomodulo;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;

    /**
     * @var string $st_labeltipo
     * @Column(name="st_labeltipo", type="string", nullable=true, length=200)
     */
    private $st_labeltipo;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=255)
     */
    private $st_status;

    /**
     * @return date
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param date $dt_abertura
     * @return $this
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param date $dt_encerramento
     * @return $this
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @param int $id_avaliacao
     * @return $this
     */
    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param int $id_modulo
     * @return $this
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param int $id_avaliacaoconjuntoreferencia
     * @return $this
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return $this
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_status()
    {
        return $this->bl_status;
    }

    /**
     * @param boolean $bl_status
     * @return $this
     */
    public function setBl_status($bl_status)
    {
        $this->bl_status = $bl_status;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param int $id_tipoavaliacao
     * @return $this
     */
    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return $this
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return float
     */
    public function getNu_notatotal()
    {
        return $this->nu_notatotal;
    }

    /**
     * @param float $nu_notatotal
     * @return $this
     */
    public function setNu_notatotal($nu_notatotal)
    {
        $this->nu_notatotal = $nu_notatotal;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getSt_nota()
    {
        return $this->st_nota;
    }

    /**
     * @param decimal $st_nota
     * @return $this
     */
    public function setSt_nota($st_nota)
    {
        $this->st_nota = $st_nota;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_notafinal()
    {
        return $this->nu_notafinal;
    }

    /**
     * @param decimal $nu_notafinal
     * @return $this
     */
    public function setNu_notafinal($nu_notafinal)
    {
        $this->nu_notafinal = $nu_notafinal;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return $this
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @param string $st_modulo
     * @return $this
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @param string $st_tituloexibicaomodulo
     * @return $this
     */
    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     * @return $this
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicaodisciplina()
    {
        return $this->st_tituloexibicaodisciplina;
    }

    /**
     * @param string $st_tituloexibicaodisciplina
     * @return $this
     */
    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina)
    {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_labeltipo()
    {
        return $this->st_labeltipo;
    }

    /**
     * @param string $st_labeltipo
     * @return $this
     */
    public function setSt_labeltipo($st_labeltipo)
    {
        $this->st_labeltipo = $st_labeltipo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return $this
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     * @return $this
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_status
     * @return $this
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
        return $this;
    }

}
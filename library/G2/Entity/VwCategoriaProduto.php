<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_categoriaproduto")
 * @Entity(repositoryClass="\G2\Repository\Produto")
 * @EntityView
 * @author Elcio Mauro Guimarães
 */
use G2\G2Entity;

class VwCategoriaProduto extends G2Entity
{

    /**
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_categoria;

    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidadecadastro;

    /**
     * @Column(name="st_nomeexibicao", type="string",length=255,nullable=false)
     * @var string $st_nomeexibicao
     */
    private $st_nomeexibicao;

    /**
     * @Column(name="st_categoria", type="string",length=255,nullable=false)
     * @var string $st_categoria
     */
    private $st_categoria;

    /**
     * @Column(name="dt_cadastro", type="datetime2",nullable=false)
     * @var string $dt_cadastro
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_uploadimagem
     * @Column(name="id_uploadimagem", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_uploadimagem;

    /**
     * @var integer $id_categoriapai
     * @Column(name="id_categoriapai", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_categoriapai;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_situacao;

    /**
     * @Column(type="boolean", nullable=true, name="bl_ativo")
     */
    private $bl_ativo;
    
    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Id
     */
    private $id_produto;


    /**
     * @Column(name="st_produto", type="string",length=255,nullable=false)
     * @var string $st_produto
     */
    private $st_produto;
    

    /**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_uploadimagem ()
    {
        return $this->id_uploadimagem;
    }

    public function setId_uploadimagem ($id_uploadimagem)
    {
        $this->id_uploadimagem = $id_uploadimagem;
        return $this;
    }

    public function getId_categoriapai ()
    {
        return $this->id_categoriapai;
    }

    public function setId_categoriapai ($id_categoriapai)
    {
        $this->id_categoriapai = $id_categoriapai;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_categoria ()
    {
        return $this->id_categoria;
    }

    public function setId_categoria ($id_categoria)
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_nomeexibicao ()
    {
        return $this->st_nomeexibicao;
    }

    public function setSt_nomeexibicao ($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;
    }

    public function getSt_categoria ()
    {
        return $this->st_categoria;
    }

    public function setSt_categoria ($st_categoria)
    {
        $this->st_categoria = $st_categoria;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_Produto()
    {
        return $this->st_produto;
    }

    public function setSt_Produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }



}

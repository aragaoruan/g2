<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_nucleotelemarketingentidade")
 * @Entity
 * @EntityView
 */
class VwNucleoTelemarketingEntidade
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_nucleotelemarketing
     * @Column(name="id_nucleotelemarketing", type="integer", nullable=false, length=4)
     */
    private $id_nucleotelemarketing;

    /**
     * @var string $st_nucleotelemarketing
     * @Column(name="st_nucleotelemarketing", type="string", nullable=false, length=255)
     */
    private $st_nucleotelemarketing;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadenucleo
     * @Column(name="id_entidadenucleo", type="integer")
     */
    private $id_entidadenucleo;

    /**
     * @return int
     */
    public function getId_nucleotelemarketing()
    {
        return $this->id_nucleotelemarketing;
    }

    /**
     * @param int $id_nucleotelemarketing
     * @return $this
     */
    public function setId_nucleotelemarketing($id_nucleotelemarketing)
    {
        $this->id_nucleotelemarketing = $id_nucleotelemarketing;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nucleotelemarketing()
    {
        return $this->st_nucleotelemarketing;
    }

    /**
     * @param string $st_nucleotelemarketing
     * @return $this
     */
    public function setSt_nucleotelemarketing($st_nucleotelemarketing)
    {
        $this->st_nucleotelemarketing = $st_nucleotelemarketing;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadenucleo()
    {
        return $this->id_entidadenucleo;
    }

    /**
     * @param int $id_entidadenucleo
     * @return $this
     */
    public function setId_entidadenucleo($id_entidadenucleo)
    {
        $this->id_entidadenucleo = $id_entidadenucleo;
        return $this;
    }
}
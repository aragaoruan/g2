<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_cartaobandeira")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CartaoBandeira
{

    /**
     *
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cartaobandeira;

    /**
     *
     * @var string $st_cartaobandeira
     * @Column(name="st_cartaobandeira", type="string", nullable=false, length=150)
     */
    private $st_cartaobandeira;

    public function getId_cartaobandeira ()
    {
        return $this->id_cartaobandeira;
    }

    public function setId_cartaobandeira ($id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
        return $this;
    }

    public function getSt_cartaobandeira ()
    {
        return $this->st_cartaobandeira;
    }

    public function setSt_cartaobandeira ($st_cartaobandeira)
    {
        $this->st_cartaobandeira = $st_cartaobandeira;
        return $this;
    }

}
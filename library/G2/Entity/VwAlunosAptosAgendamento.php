<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunosaptosagendamento")
 * @Entity
 * @EntityView
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class VwAlunosAptosAgendamento extends G2Entity {

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false, length=11)
     */
    private $st_avaliacao;

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_avaliacao
     */
    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    /**
     * @param st_avaliacao
     */
    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_motivocancelamento")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @EntityLog
 * @HasLifecycleCallbacks
 */
class MotivoCancelamento extends G2Entity
{
    /**
     * @Id
     * @var integer $id_motivocancelamento
     * @Column(name="id_motivocancelamento", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_motivocancelamento;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Cancelamento $id_cancelamento
     * @ManyToOne(targetEntity="Cancelamento")
     * @JoinColumn(name="id_cancelamento", referencedColumnName="id_cancelamento")
     */
    private $id_cancelamento;
    /**
     * @var Motivo $id_motivo
     * @ManyToOne(targetEntity="Motivo")
     * @JoinColumn(name="id_motivo", referencedColumnName="id_motivo")
     */
    private $id_motivo;
    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_motivocancelamento
     */
    public function getId_motivocancelamento()
    {
        return $this->id_motivocancelamento;
    }

    /**
     * @param $id_motivocancelamento
     * @return $this
     */
    public function setId_motivocancelamento($id_motivocancelamento)
    {
        $this->id_motivocancelamento = $id_motivocancelamento;
        return $this;
    }

    /**
     * @return Cancelamento
     */
    public function getId_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param $id_cancelamento
     * @return $this
     */
    public function setId_cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
        return $this;
    }

    /**
     * @return Motivo
     */
    public function getId_motivo()
    {
        return $this->id_motivo;
    }

    /**
     * @param $id_motivo
     * @return $this
     */
    public function setId_motivo($id_motivo)
    {
        $this->id_motivo = $id_motivo;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_Ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     * @return $this
     */
    public function setBl_Ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @PrePersist
     */
    public function prePersist()
    {
        if (!$this->getDt_cadastro()) {
            $this->setDt_cadastro(new \DateTime());
        }
        return $this;
    }

}

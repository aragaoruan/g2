<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_enviomensagem")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEnvioMensagem
{

    /**
     * @var integer $id_enviomensagem
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_enviomensagem;

    /**
     * @var integer $id_mensagem
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_mensagem;

    /**
     * @var integer $id_tipoenvio
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_tipoenvio;

    /**
     * @var integer $id_emailconfig
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_emailconfig;

    /**
     * @var integer $id_evolucao
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_evolucao;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var integer $dt_envio
     * @Column(type="integer", nullable=false)
     */
    private $dt_envio;

    /**
     * @var datetime2 $dt_enviar
     * @Column(type="datetime2", nullable=false)
     */
    private $dt_enviar;

    /**
     * @var datetime2 $dt_tentativa
     * @Column(type="datetime2", nullable=false)
     */
    private $dt_tentativa;

    /**
     * @var integer $id_enviodestinatario
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_enviodestinatario;

    /**
     * @var integer $id_usuario
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_usuario;

    /**
     * @var string $st_caminhoanexo
     * @Column(type="string", nullable=false)
     */
    private $st_caminhoanexo;


    /**
     * @var string $st_nome
     * @Column(type="string", nullable=false)
     */
    private $st_nome;

    /**
     * @var string $st_endereco
     * @Column(type="string", nullable=false)
     */
    private $st_endereco;

    /**
     * @var integer $id_tipodestinatario
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_tipodestinatario;

    /**
     * @var string $st_mensagem
     * @Column(type="string", nullable=false)
     */
    private $st_mensagem;

    /**
     * @var string $st_texto
     * @Column(type="string", nullable=false)
     */
    private $st_texto;

    /**
     * @var string $nu_telefone
     * @Column(type="string", nullable=false)
     */
    private $nu_telefone;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

    /**
     * @var string $st_evolucao
     * @Column(type="string", nullable=false)
     */
    private $st_evolucao;

    /**
     * @var string $st_tipoenvio
     * @Column(type="string", nullable=false)
     */
    private $st_tipoenvio;

    /**
     * @return int
     */
    public function getId_enviomensagem()
    {
        return $this->id_enviomensagem;
    }

    /**
     * @return int
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @return int
     */
    public function getId_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    /**
     * @return int
     */
    public function getId_emailconfig()
    {
        return $this->id_emailconfig;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return int
     */
    public function getDt_envio()
    {
        return $this->dt_envio;
    }

    /**
     * @return datetime2
     */
    public function getDt_enviar()
    {
        return $this->dt_enviar;
    }

    /**
     * @return datetime2
     */
    public function getDt_tentativa()
    {
        return $this->dt_tentativa;
    }

    /**
     * @return int
     */
    public function getId_enviodestinatario()
    {
        return $this->id_enviodestinatario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return string
     */
    public function getSt_caminhoanexo()
    {
        return $this->st_caminhoanexo;
    }


    /**
     * @return string
     */
    public function getSt_nome()
    {
        return $this->st_nome;
    }

    /**
     * @return string
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @return int
     */
    public function getId_tipodestinatario()
    {
        return $this->id_tipodestinatario;
    }

    /**
     * @return string
     */
    public function getSt_mensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @return string
     */
    public function getSt_texto()
    {
        return $this->st_texto;
    }

    /**
     * @return string
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_tipoenvio()
    {
        return $this->st_tipoenvio;
    }

    /**
     * @param $id_enviomensagem
     * @return $this
     */
    public function setId_enviomensagem($id_enviomensagem)
    {
        $this->id_enviomensagem = $id_enviomensagem;
        return $this;
    }

    /**
     * @param $id_mensagem
     * @return $this
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
        return $this;
    }

    /**
     * @param $id_tipoenvio
     * @return $this
     */
    public function setId_tipoenvio($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    /**
     * @param $id_emailconfig
     * @return $this
     */
    public function setId_emailconfig($id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    /**
     * @param $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param $dt_envio
     * @return $this
     */
    public function setDt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
        return $this;
    }

    /**
     * @param $dt_enviar
     * @return $this
     */
    public function setDt_enviar($dt_enviar)
    {
        $this->dt_enviar = $dt_enviar;
        return $this;
    }

    /**
     * @param $dt_tentativa
     * @return $this
     */
    public function setDt_tentativa($dt_tentativa)
    {
        $this->dt_tentativa = $dt_tentativa;
        return $this;
    }

    /**
     * @param $id_enviodestinatario
     * @return $this
     */
    public function setId_enviodestinatario($id_enviodestinatario)
    {
        $this->id_enviodestinatario = $id_enviodestinatario;
        return $this;
    }

    /**
     * @param $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param $st_caminhoanexo
     * @return $this
     */
    public function setSt_caminhoanexo($st_caminhoanexo)
    {
        $this->st_caminhoanexo = $st_caminhoanexo;
        return $this;
    }


    /**
     * @param $st_nome
     * @return $this
     */
    public function setSt_nome($st_nome)
    {
        $this->st_nome = $st_nome;
        return $this;
    }

    /**
     * @param $st_endereco
     * @return $this
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @param $id_tipodestinatario
     * @return $this
     */
    public function setId_tipodestinatario($id_tipodestinatario)
    {
        $this->id_tipodestinatario = $id_tipodestinatario;
        return $this;
    }

    /**
     * @param $st_mensagem
     * @return $this
     */
    public function setSt_mensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
        return $this;
    }

    /**
     * @param $st_texto
     * @return $this
     */
    public function setSt_texto($st_texto)
    {
        $this->st_texto = $st_texto;
        return $this;
    }

    /**
     * @param $nu_telefone
     * @return $this
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param $st_evolucao
     * @return $this
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @param $st_tipoenvio
     * @return $this
     */
    public function setSt_tipoenvio($st_tipoenvio)
    {
        $this->st_tipoenvio = $st_tipoenvio;
        return $this;
    }

}

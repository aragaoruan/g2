<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_smsentidademensagem")
 * @Entity
 * @EntityView
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */
class VwSmsEntidadeMensagem
{

    /**
     * @var integer $id_smsentidademensagem
     * @Column(name="id_smsentidademensagem", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_smsentidademensagem;

    /**
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=true, length=200)
     */
    private $st_textosistema;
    /**
     * @var string $st_mensagempadrao
     * @Column(name="st_mensagempadrao", type="string", nullable=false, length=150)
     */
    private $st_mensagempadrao;
    /**
     * @var string $st_default
     * @Column(name="st_default", type="string", nullable=true)
     */
    private $st_default;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_tipoenvio
     * @Column(name="id_tipoenvio", type="integer", nullable=true, length=4)
     */
    private $id_tipoenvio;
    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=true, length=4)
     */
    private $id_textosistema;

    /**
     * @var integer $id_mensagempadrao
     * @Column(name="id_mensagempadrao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_mensagempadrao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_mensagempadrao()
    {
        return $this->id_mensagempadrao;
    }

    /**
     * @param int $id_mensagempadrao
     */
    public function setId_mensagempadrao($id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_smsentidademensagem()
    {
        return $this->id_smsentidademensagem;
    }

    /**
     * @param int $id_smsentidademensagem
     */
    public function setId_smsentidademensagem($id_smsentidademensagem)
    {
        $this->id_smsentidademensagem = $id_smsentidademensagem;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param int $id_textosistema
     */
    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    /**
     * @param int $id_tipoenvio
     */
    public function setId_tipoenvio($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_default()
    {
        return $this->st_default;
    }

    /**
     * @param string $st_default
     */
    public function setSt_default($st_default)
    {
        $this->st_default = $st_default;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_mensagempadrao()
    {
        return $this->st_mensagempadrao;
    }

    /**
     * @param string $st_mensagempadrao
     */
    public function setSt_mensagempadrao($st_mensagempadrao)
    {
        $this->st_mensagempadrao = $st_mensagempadrao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_textosistema()
    {
        return $this->st_textosistema;
    }

    /**
     * @param string $st_textosistema
     */
    public function setSt_textosistema($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }


}
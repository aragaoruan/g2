<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 13/04/2018
 * Time: 10:19
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Table(name="tb_historicocertificadoparcial")
 * @Entity
 *
 */

class HistoricoCertificadoParcial extends G2Entity
{
    /**
     * @var integer $id_historicocertificadoparcial
     * @Column(name="id_historicocertificadoparcial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_historicocertificadoparcial;

    /**
     * @var integer $id_certificadoparcial
     * @Column(name="id_certificadoparcial", nullable=false)
     */
    private $id_certificadoparcial;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", nullable=false)
     */
    private $id_matricula;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_conteudo
     * @Column(name="st_conteudo", type="string", nullable=false)
     */
    private $st_conteudo;

    /**
     * @var string $st_registro
     * @Column(name="st_registro", type="string", nullable=false, length=255)
     */
    private $st_registro;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @return int
     */
    public function getId_historicocertificadoparcial()
    {
        return $this->id_historicocertificadoparcial;
    }

    /**
     * @param int $id_historicocertificadoparcial
     */
    public function setId_historicocertificadoparcial($id_historicocertificadoparcial)
    {
        $this->id_historicocertificadoparcial = $id_historicocertificadoparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_certificadoparcial()
    {
        return $this->id_certificadoparcial;
    }

    /**
     * @param int $id_certificadoparcial
     */
    public function setId_certificadoparcial($id_certificadoparcial)
    {
        $this->id_certificadoparcial = $id_certificadoparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_conteudo()
    {
        return $this->st_conteudo;
    }

    /**
     * @param string $st_conteudo
     */
    public function setSt_conteudo($st_conteudo)
    {
        $this->st_conteudo = $st_conteudo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_registro()
    {
        return $this->st_registro;
    }

    /**
     * @param string $st_registro
     */
    public function setSt_registro($st_registro)
    {
        $this->st_registro = $st_registro;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}

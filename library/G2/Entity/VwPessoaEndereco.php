<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwPessoaEndereco"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pessoaendereco")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwPessoaEndereco extends G2Entity
{

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_endereco
     * @column(name="id_endereco", nullable=false, type="integer")
     * @id
     * @GeneratedValue(strategy="NONE")
     */
    public $id_endereco;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @column(name="id_entidade", nullable=false, type="integer")
     * @id
     * @GeneratedValue(strategy="NONE")
     */
    public $id_entidade;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_padrao
     * @column(name="bl_padrao", nullable=false, type="boolean")
     */
    public $bl_padrao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_categoriaendereco
     * @column(name="st_categoriaendereco", nullable=true, type="string", length=255)
     */
    public $st_categoriaendereco;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tipoendereco
     * @column(name="id_tipoendereco", nullable=false, type="integer")
     */
    public $id_tipoendereco;


    /**
     * @SWG\Property(format="string")
     * @var string $st_tipoendereco
     * @column(name="st_tipoendereco", nullable=true, type="string", length=255)
     */
    public $st_tipoendereco;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @column(name="id_usuario", nullable=false, type="integer")
     */
    public $id_usuario;

    /**
     * @SWG\Property(format="string")
     * @var string $st_endereco
     * @column(name="st_endereco", nullable=true, type="string", length=255)
     */
    public $st_endereco;

    /**
     * @SWG\Property(format="string")
     * @var string $st_complemento
     * @column(name="st_complemento", nullable=true, type="string", length=255)
     */
    public $st_complemento;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_numero
     * @column(name="nu_numero", nullable=true, type="string", length=255)
     */
    public $nu_numero;

    /**
     * @SWG\Property(format="string")
     * @var string $st_bairro
     * @column(name="st_bairro", nullable=true, type="string", length=255)
     */
    public $st_bairro;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cidade
     * @column(name="st_cidade", nullable=true, type="string", length=255)
     */
    public $st_cidade;


    /**
     * @SWG\Property(format="string")
     * @var string $st_estadoprovincia
     * @column(name="st_estadoprovincia", nullable=true, type="string", length=255)
     */
    public $st_estadoprovincia;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cep
     * @column(name="st_cep", nullable=true, type="string", length=255)
     */
    public $st_cep;

    /**
     * @SWG\Property(format="string")
     * @var string $sg_uf
     * @column(name="sg_uf", nullable=true, type="string", length=255)
     */
    public $sg_uf;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_categoriaendereco
     * @column(name="id_categoriaendereco", nullable=true, type="integer")
     */
    public $id_categoriaendereco;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_municipio
     * @column(name="id_municipio", nullable=true, type="integer")
     */
    public $id_municipio;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomemunicipio
     * @column(name="st_nomemunicipio", nullable=true, type="string", length=255)
     */
    public $st_nomemunicipio;

    /**
     * @SWG\Property(format="string")
     * @var string $st_uf
     * @column(name="st_uf", nullable=true, type="string", length=255)
     */
    public $st_uf;

    /**
     * @SWG\Property(format="string")
     * @var integer $id_pais
     * @column(name="id_pais", nullable=true, type="integer")
     */
    public $id_pais;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomepais
     * @column(name="st_nomepais", nullable=true, type="string", length=255)
     */
    public $st_nomepais;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_ativo
     * @column(name="bl_ativo", nullable=false, type="boolean")
     */
    public $bl_ativo;


    public function getId_usuario(){
        return $this->id_usuario;
    }

    public function getId_entidade(){
        return $this->id_entidade;
    }

    public function getBl_padrao(){
        return $this->bl_padrao;
    }

    public function getSt_categoriaendereco(){
        return $this->st_categoriaendereco;
    }

    public function getId_tipoendereco(){
        return $this->id_tipoendereco;
    }

    public function getSt_tipoendereco(){
        return $this->st_tipoendereco;
    }

    public function getId_endereco(){
        return $this->id_endereco;
    }

    public function getSt_endereco(){
        return $this->st_endereco;
    }

    public function getSt_complemento(){
        return $this->st_complemento;
    }

    public function getNu_numero(){
        return $this->nu_numero;
    }

    public function getSt_bairro(){
        return $this->st_bairro;
    }

    public function getSt_cidade(){
        return $this->st_cidade;
    }

    public function getSt_estadoprovincia(){
        return $this->st_estadoprovincia;
    }

    public function getSt_cep(){
        return $this->st_cep;
    }

    public function getSg_uf(){
        return $this->sg_uf;
    }

    public function getId_categoriaendereco(){
        return $this->id_categoriaendereco;
    }

    public function getId_municipio(){
        return $this->id_municipio;
    }

    public function getSt_nomemunicipio(){
        return $this->st_nomemunicipio;
    }

    public function getSt_uf(){
        return $this->st_uf;
    }

    public function getId_pais(){
        return $this->id_pais;
    }

    public function getSt_nomepais(){
        return $this->st_nomepais;
    }

    public function getBl_ativo(){
        return $this->bl_ativo;
    }


    public function setId_usuario($id_usuario){
        $this->id_usuario = $id_usuario;
    }

    public function setId_entidade($id_entidade){
        $this->id_entidade = $id_entidade;
    }

    public function setBl_padrao($bl_padrao){
        $this->bl_padrao = $bl_padrao;
    }

    public function setSt_categoriaendereco($st_categoriaendereco){
        $this->st_categoriaendereco = $st_categoriaendereco;
    }

    public function setId_tipoendereco($id_tipoendereco){
        $this->id_tipoendereco = $id_tipoendereco;
    }

    public function setSt_tipoendereco($st_tipoendereco){
        $this->st_tipoendereco = $st_tipoendereco;
    }

    public function setId_endereco($id_endereco){
        $this->id_endereco = $id_endereco;
    }

    public function setSt_endereco($st_endereco){
        $this->st_endereco = $st_endereco;
    }

    public function setSt_complemento($st_complemento){
        $this->st_complemento = $st_complemento;
    }

    public function setNu_numero($nu_numero){
        $this->nu_numero = $nu_numero;
    }

    public function setSt_bairro($st_bairro){
        $this->st_bairro = $st_bairro;
    }

    public function setSt_cidade($st_cidade){
        $this->st_cidade = $st_cidade;
    }

    public function setSt_estadoprovincia($st_estadoprovincia){
        $this->st_estadoprovincia = $st_estadoprovincia;
    }

    public function setSt_cep($st_cep){
        $this->st_cep = $st_cep;
    }

    public function setSg_uf($sg_uf){
        $this->sg_uf = $sg_uf;
    }

    public function setId_categoriaendereco($id_categoriaendereco){
        $this->id_categoriaendereco = $id_categoriaendereco;
    }

    public function setId_municipio($id_municipio){
        $this->id_municipio = $id_municipio;
    }

    public function setSt_nomemunicipio($st_nomemunicipio){
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    public function setSt_uf($st_uf){
        $this->st_uf = $st_uf;
    }

    public function setId_pais($id_pais){
        $this->id_pais = $id_pais;
    }

    public function setSt_nomepais($st_nomepais){
        $this->st_nomepais = $st_nomepais;
    }

    public function setBl_ativo($bl_ativo){
        $this->bl_ativo = $bl_ativo;
    }





}
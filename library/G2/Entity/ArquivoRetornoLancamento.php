<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity()
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_arquivoretornolancamento")
 */
class ArquivoRetornoLancamento
{
    /**
     * @var integer $id_arquivoretornolancamento
     * @Column(name="id_arquivoretornolancamento", type="integer")
     * @Id
     */
    private $id_arquivoretornolancamento;

    /**
     * @var integer $id_arquivoretorno
     * @Column(name="id_arquivoretorno", type="integer")
     */
    private $id_arquivoretorno;

    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer")
     */
    private $id_lancamento;

    /**
     * @var string $st_nossonumero
     * @Column(name="st_nossonumero", type="string", length=30)
     */
    private $st_nossonumero;

    /**
     * @var string $nu_valor
     * @Column(name="nu_valor", type="decimal", precision=10, scale=2)
     */
    private $nu_valor;

    /**
     * @var datetime $dt_ocorrencia
     * @Column(name="dt_ocorrencia", type="datetime")
     */
    private $dt_ocorrencia;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer")
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_quitado
     * @Column(name="bl_quitado", type="boolean")
     */
    private $bl_quitado;

    /**
     * @var string $nu_desconto
     * @Column(name="nu_desconto", type="decimal", precision=10, scale=2)
     */
    private $nu_desconto;

    /**
     * @var string $nu_juros
     * @Column(name="nu_juros", type="decimal", precision=10, scale=2)
     */
    private $nu_juros;

    /**
     * @var string $st_carteira
     * @Column(name="st_carteira", type="string", length=3)
     */
    private $st_carteira;

    /**
     * @var string $st_documento
     * @Column(name="st_documento", type="string", length=20)
     */
    private $st_documento;

    /**
     * @var string $nu_tarifa
     * @Column(name="nu_tarifa", type="decimal", precision=10, scale=2)
     */
    private $nu_tarifa;

    /**
     * @var string $nu_valornominal
     * @Column(name="nu_valornominal", type="decimal", precision=10, scale=2)
     */
    private $nu_valornominal;

    /**
     * @var datetime $dt_quitado
     * @Column(name="dt_quitado", type="datetime")
     */
    private $dt_quitado;

    /**
     * @return int
     */
    public function getId_Arquivoretornolancamento()
    {
        return $this->id_arquivoretornolancamento;
    }

    /**
     * @param int $id_arquivoretornolancamento
     */
    public function setId_Arquivoretornolancamento($id_arquivoretornolancamento)
    {
        $this->id_arquivoretornolancamento = $id_arquivoretornolancamento;
    }

    /**
     * @return int
     */
    public function getId_Arquivoretorno()
    {
        return $this->id_arquivoretorno;
    }

    /**
     * @param int $id_arquivoretorno
     */
    public function setId_Arquivoretorno($id_arquivoretorno)
    {
        $this->id_arquivoretorno = $id_arquivoretorno;
    }

    /**
     * @return int
     */
    public function getId_Lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamento
     */
    public function setId_Lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return string
     */
    public function getSt_Nossonumero()
    {
        return $this->st_nossonumero;
    }

    /**
     * @param string $st_nossonumero
     */
    public function setSt_Nossonumero($st_nossonumero)
    {
        $this->st_nossonumero = $st_nossonumero;
    }

    /**
     * @return string
     */
    public function getNu_Valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param string $nu_valor
     */
    public function setNu_Valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return datetime
     */
    public function getDt_Ocorrencia()
    {
        return $this->dt_ocorrencia;
    }

    /**
     * @param datetime $dt_ocorrencia
     */
    public function setDt_Ocorrencia($dt_ocorrencia)
    {
        $this->dt_ocorrencia = $dt_ocorrencia;
    }

    /**
     * @return int
     */
    public function getId_Usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_Usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return datetime
     */
    public function getDt_Cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setDt_Cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return boolean
     */
    public function isBl_Quitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @param boolean $bl_quitado
     */
    public function setBl_Quitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @return string
     */
    public function getNu_Desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param string $nu_desconto
     */
    public function setNu_Desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return string
     */
    public function getNu_Juros()
    {
        return $this->nu_juros;
    }

    /**
     * @param string $nu_juros
     */
    public function setNu_Juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return string
     */
    public function getSt_Carteira()
    {
        return $this->st_carteira;
    }

    /**
     * @param string $st_carteira
     */
    public function setSt_Carteira($st_carteira)
    {
        $this->st_carteira = $st_carteira;
    }

    /**
     * @return string
     */
    public function getSt_Documento()
    {
        return $this->st_documento;
    }

    /**
     * @param string $st_documento
     */
    public function setSt_Documento($st_documento)
    {
        $this->st_documento = $st_documento;
    }

    /**
     * @return string
     */
    public function get_NuTarifa()
    {
        return $this->nu_tarifa;
    }

    /**
     * @param string $nu_tarifa
     */
    public function set_NuTarifa($nu_tarifa)
    {
        $this->nu_tarifa = $nu_tarifa;
    }

    /**
     * @return string
     */
    public function getNu_Valornominal()
    {
        return $this->nu_valornominal;
    }

    /**
     * @param string $nu_valornominal
     */
    public function setNu_Valornominal($nu_valornominal)
    {
        $this->nu_valornominal = $nu_valornominal;
    }

    /**
     * @return datetime
     */
    public function getDt_Quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param datetime $dt_quitado
     */
    public function setDt_Quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

}
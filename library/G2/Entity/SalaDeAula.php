<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_saladeaula")
 * @Entity
 * @EntityLog
 * @Entity(repositoryClass="\G2\Repository\SalaDeAula")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
use Doctrine\Common\Collections\ArrayCollection;
use G2\G2Entity;

class SalaDeAula extends G2Entity
{

    /**
     * @var integer $id_saladeaula
     * @Column(type="integer", nullable=false, name="id_saladeaula")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_saladeaula;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var string $st_saladeaula
     * @Column(type="string", length=255, nullable=false, name="st_saladeaula")
     */
    private $st_saladeaula;

    /**
     * @var boolean $bl_ativa
     * @Column(type="boolean", nullable=false, name="bl_ativa")
     */
    private $bl_ativa;

    /**
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=false, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;

    /**
     * @var ModalidadeSalaDeAula $id_modalidadesaladeaula
     * @ManyToOne(targetEntity="ModalidadeSalaDeAula")
     * @JoinColumn(name="id_modalidadesaladeaula", referencedColumnName="id_modalidadesaladeaula", nullable=false)
     */
    private $id_modalidadesaladeaula;

    /**
     * @var date $dt_inicioinscricao
     * @Column(type="date", nullable=false, name="dt_inicioinscricao")
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_fiminscricao
     * @Column(type="date", nullable=true, name="dt_fiminscricao")
     */
    private $dt_fiminscricao;

    /**
     * @var date $dt_abertura
     * @Column(type="date", nullable=true, name="dt_abertura")
     */
    private $dt_abertura;

    /**
     * @var string $st_localizacao
     * @Column(type="string", length=255, nullable=true, name="st_localizacao")
     */
    private $st_localizacao;

    /**
     * @var TipoSaladeAula $id_tiposaladeaula
     * @ManyToOne(targetEntity="TipoSaladeAula")
     * @JoinColumn(name="id_tiposaladeaula", referencedColumnName="id_tiposaladeaula", nullable=false)
     */
    private $id_tiposaladeaula;

    /**
     * @var date $dt_encerramento
     * @Column(type="date", nullable=true, name="dt_encerramento")
     */
    private $dt_encerramento;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var PeriodoLetivo $id_periodoletivo
     * @ManyToOne(targetEntity="PeriodoLetivo")
     * @JoinColumn(name="id_periodoletivo", referencedColumnName="id_periodoletivo", nullable=true)
     */
    private $id_periodoletivo;

    /**
     * @var boolean $bl_usardoperiodoletivo
     * @Column(type="boolean", nullable=false, name="bl_usardoperiodoletivo")
     */
    private $bl_usardoperiodoletivo;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao", nullable=false)
     */
    private $id_situacao;

    /**
     * @var integer $nu_maxalunos
     * @Column(type="integer",nullable=true, name="nu_maxalunos")
     */
    private $nu_maxalunos;

    /**
     * @var decimal $nu_diasencerramento
     * @Column(type="decimal",nullable=true, name="nu_diasencerramento")
     */
    private $nu_diasencerramento;

    /**
     * @var boolean $bl_semencerramento
     * @Column(type="boolean",nullable=false, name="bl_semencerramento")
     */
    private $bl_semencerramento;

    /**
     * @var boolean $bl_todasentidades
     * @Column(type="boolean",nullable=true, name="bl_todasentidades")
     */
    private $bl_todasentidades;

    /**
     * @var decimal $nu_diasaluno
     * @Column(type="decimal",nullable=true, name="nu_diasaluno")
     */
    private $nu_diasaluno;

    /**
     * @var boolean $bl_semdiasaluno
     * @Column(type="boolean",nullable=false, name="bl_semdiasaluno")
     */
    private $bl_semdiasaluno;

    /**
     * @var integer $nu_diasextensao
     * @Column(type="integer", nullable=false, name="nu_diasextensao")
     */
    private $nu_diasextensao;

    /**
     * @var text $st_pontosnegativos
     * @Column(type="text", nullable=true, name="st_pontosnegativos")
     */
    private $st_pontosnegativos;

    /**
     * @var text $st_pontospositivos
     * @Column(type="text", nullable=true, name="st_pontospositivos")
     */
    private $st_pontospositivos;

    /**
     * @var text $st_pontosmelhorar
     * @Column(type="text", nullable=true, name="st_pontosmelhorar")
     */
    private $st_pontosmelhorar;

    /**
     * @var text $st_pontosresgate
     * @Column(type="text", nullable=true, name="st_pontosresgate")
     */
    private $st_pontosresgate;

    /**
     * @var text $st_conclusoesfinais
     * @Column(type="text", nullable=true, name="st_conclusoesfinais")
     */
    private $st_conclusoesfinais;

    /**
     * @var text $st_justificativaacima
     * @Column(type="text", nullable=true, name="st_justificativaacima")
     */
    private $st_justificativaacima;

    /**
     * @var text $st_justificativaabaixo
     * @Column(type="text", nullable=true, name="st_justificativaabaixo")
     */
    private $st_justificativaabaixo;

    /**
     * @var datetime2 $dt_atualiza
     * @Column(type="datetime2", nullable=true, name="dt_atualiza")
     */
    private $dt_atualiza;

    /**
     * @var CategoriaSala $id_categoriasala
     * @ManyToOne(targetEntity="CategoriaSala")
     * @JoinColumn(name="id_categoriasala", referencedColumnName="id_categoriasala", nullable=false)
     */
    private $id_categoriasala;

    /**
     * @ManyToMany(targetEntity="Entidade")
     * @JoinTable(name="tb_saladeaulaentidade",
     *      joinColumns={@JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")},
     *      inverseJoinColumns={@JoinColumn(name="id_entidade", referencedColumnName="id_entidade")}
     *      )
     */
    private $salaDeAulaEntidade;

    private $salaDeAulaProjeto;

    /**
     * @ManyToMany(targetEntity="Disciplina", inversedBy="salaDisciplina")
     * @JoinTable(name="tb_disciplinasaladeaula",
     *      joinColumns={@JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")},
     *      inverseJoinColumns={@JoinColumn(name="id_disciplina", referencedColumnName="id_disciplina")}
     *      )
     */
    private $salaDeAulaDisciplina;

    /**
     * @var Usuario $id_usuariocancelamento
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocancelamento", referencedColumnName="id_usuario", nullable=true)
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    private $id_usuariocancelamento;

    /**
     * @var datetime $dt_cancelamento
     * @Column(type="datetime", nullable=true, name="dt_cancelamento")
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    private $dt_cancelamento;

    /**
     * @var integer $id_usuarioatualiza
     * @Column(type="integer", nullable=false, name="id_usuarioatualiza")
     */
    private $id_usuarioatualiza;

    /**
     * @var boolean $bl_ofertaexcepcional
     * @Column(type="boolean", nullable=false, name="bl_ofertaexcepcional")
     */
    private $bl_ofertaexcepcional;

    /**
     * @var boolean $bl_vincularturma
     * @Column(type="boolean", nullable=false, name="bl_vincularturma")
     */
    private $bl_vincularturma;

    /**
     * @var EntidadeIntegracao $id_entidadeintegracao
     * @ManyToOne(targetEntity="EntidadeIntegracao")
     * @JoinColumn(name="id_entidadeintegracao", referencedColumnName="id_entidadeintegracao", nullable=true)
     */
    private $id_entidadeintegracao;

    public function __construct()
    {
        $this->salaDeAulaEntidade = new ArrayCollection();
        $this->salaDeAulaProjeto = new ArrayCollection();
        $this->salaDeAulaDisciplina = new ArrayCollection();
    }

    public function getSalaDeAulaEntidade()
    {
        return $this->salaDeAulaEntidade;
    }

    public function setSalaDeAulaEntidade($salaDeAulaEntidade)
    {
        $this->salaDeAulaEntidade = $salaDeAulaEntidade;
        return $this;
    }

    public function getSalaDeAulaProjeto()
    {
        return $this->salaDeAulaProjeto;
    }

    public function setSalaDeAulaProjeto($salaDeAulaProjeto)
    {
        $this->salaDeAulaProjeto = $salaDeAulaProjeto;
        return $this;
    }

    public function getSalaDeAulaDisciplina()
    {
        return $this->salaDeAulaDisciplina;
    }

    public function setSalaDeAulaDisciplina($salaDeAulaDisciplina)
    {
        $this->salaDeAulaDisciplina = $salaDeAulaDisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return SalaDeAula
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return SalaDeAula
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return SalaDeAula
     */
    public function setst_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_ativa()
    {
        return $this->bl_ativa;
    }

    /**
     * @param bool $bl_ativa
     * @return SalaDeAula
     */
    public function setbl_ativa($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return SalaDeAula
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return ModalidadeSalaDeAula
     */
    public function getid_modalidadesaladeaula()
    {
        return $this->id_modalidadesaladeaula;
    }

    /**
     * @param ModalidadeSalaDeAula $id_modalidadesaladeaula
     * @return SalaDeAula
     */
    public function setid_modalidadesaladeaula($id_modalidadesaladeaula)
    {
        $this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param date $dt_inicioinscricao
     * @return SalaDeAula
     */
    public function setdt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param date $dt_fiminscricao
     * @return SalaDeAula
     */
    public function setdt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param date $dt_abertura
     * @return SalaDeAula
     */
    public function setdt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_localizacao()
    {
        return $this->st_localizacao;
    }

    /**
     * @param string $st_localizacao
     * @return SalaDeAula
     */
    public function setst_localizacao($st_localizacao)
    {
        $this->st_localizacao = $st_localizacao;
        return $this;
    }

    /**
     * @return TipoSaladeAula
     */
    public function getid_tiposaladeaula()
    {
        return $this->id_tiposaladeaula;
    }

    /**
     * @param TipoSaladeAula $id_tiposaladeaula
     * @return SalaDeAula
     */
    public function setid_tiposaladeaula($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param date $dt_encerramento
     * @return SalaDeAula
     */
    public function setdt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return SalaDeAula
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return PeriodoLetivo
     */
    public function getid_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param PeriodoLetivo $id_periodoletivo
     * @return SalaDeAula
     */
    public function setid_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_usardoperiodoletivo()
    {
        return $this->bl_usardoperiodoletivo;
    }

    /**
     * @param bool $bl_usardoperiodoletivo
     * @return SalaDeAula
     */
    public function setbl_usardoperiodoletivo($bl_usardoperiodoletivo)
    {
        $this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     * @return SalaDeAula
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param int $nu_maxalunos
     * @return SalaDeAula
     */
    public function setnu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getnu_diasencerramento()
    {
        return $this->nu_diasencerramento;
    }

    /**
     * @param decimal $nu_diasencerramento
     * @return SalaDeAula
     */
    public function setnu_diasencerramento($nu_diasencerramento)
    {
        $this->nu_diasencerramento = $nu_diasencerramento;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_semencerramento()
    {
        return $this->bl_semencerramento;
    }

    /**
     * @param bool $bl_semencerramento
     * @return SalaDeAula
     */
    public function setbl_semencerramento($bl_semencerramento)
    {
        $this->bl_semencerramento = $bl_semencerramento;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_todasentidades()
    {
        return $this->bl_todasentidades;
    }

    /**
     * @param bool $bl_todasentidades
     * @return SalaDeAula
     */
    public function setbl_todasentidades($bl_todasentidades)
    {
        $this->bl_todasentidades = $bl_todasentidades;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getnu_diasaluno()
    {
        return $this->nu_diasaluno;
    }

    /**
     * @param decimal $nu_diasaluno
     * @return SalaDeAula
     */
    public function setnu_diasaluno($nu_diasaluno)
    {
        $this->nu_diasaluno = $nu_diasaluno;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_semdiasaluno()
    {
        return $this->bl_semdiasaluno;
    }

    /**
     * @param bool $bl_semdiasaluno
     * @return SalaDeAula
     */
    public function setbl_semdiasaluno($bl_semdiasaluno)
    {
        $this->bl_semdiasaluno = $bl_semdiasaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_diasextensao()
    {
        return $this->nu_diasextensao;
    }

    /**
     * @param int $nu_diasextensao
     * @return SalaDeAula
     */
    public function setnu_diasextensao($nu_diasextensao)
    {
        $this->nu_diasextensao = $nu_diasextensao;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_pontosnegativos()
    {
        return $this->st_pontosnegativos;
    }

    /**
     * @param text $st_pontosnegativos
     * @return SalaDeAula
     */
    public function setst_pontosnegativos($st_pontosnegativos)
    {
        $this->st_pontosnegativos = $st_pontosnegativos;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_pontospositivos()
    {
        return $this->st_pontospositivos;
    }

    /**
     * @param text $st_pontospositivos
     * @return SalaDeAula
     */
    public function setst_pontospositivos($st_pontospositivos)
    {
        $this->st_pontospositivos = $st_pontospositivos;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_pontosmelhorar()
    {
        return $this->st_pontosmelhorar;
    }

    /**
     * @param text $st_pontosmelhorar
     * @return SalaDeAula
     */
    public function setst_pontosmelhorar($st_pontosmelhorar)
    {
        $this->st_pontosmelhorar = $st_pontosmelhorar;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_pontosresgate()
    {
        return $this->st_pontosresgate;
    }

    /**
     * @param text $st_pontosresgate
     * @return SalaDeAula
     */
    public function setst_pontosresgate($st_pontosresgate)
    {
        $this->st_pontosresgate = $st_pontosresgate;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_conclusoesfinais()
    {
        return $this->st_conclusoesfinais;
    }

    /**
     * @param text $st_conclusoesfinais
     * @return SalaDeAula
     */
    public function setst_conclusoesfinais($st_conclusoesfinais)
    {
        $this->st_conclusoesfinais = $st_conclusoesfinais;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_justificativaacima()
    {
        return $this->st_justificativaacima;
    }

    /**
     * @param text $st_justificativaacima
     * @return SalaDeAula
     */
    public function setst_justificativaacima($st_justificativaacima)
    {
        $this->st_justificativaacima = $st_justificativaacima;
        return $this;
    }

    /**
     * @return text
     */
    public function getst_justificativaabaixo()
    {
        return $this->st_justificativaabaixo;
    }

    /**
     * @param text $st_justificativaabaixo
     * @return SalaDeAula
     */
    public function setst_justificativaabaixo($st_justificativaabaixo)
    {
        $this->st_justificativaabaixo = $st_justificativaabaixo;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getdt_atualiza()
    {
        return $this->dt_atualiza;
    }

    /**
     * @param datetime2 $dt_atualiza
     * @return SalaDeAula
     */
    public function setdt_atualiza($dt_atualiza)
    {
        $this->dt_atualiza = $dt_atualiza;
        return $this;
    }

    /**
     * @return CategoriaSala
     */
    public function getid_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param CategoriaSala $id_categoriasala
     * @return SalaDeAula
     */
    public function setid_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getid_usuariocancelamento()
    {
        return $this->id_usuariocancelamento;
    }

    /**
     * @param Usuario $id_usuariocancelamento
     * @return SalaDeAula
     */
    public function setid_usuariocancelamento($id_usuariocancelamento)
    {
        $this->id_usuariocancelamento = $id_usuariocancelamento;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getdt_cancelamento()
    {
        return $this->dt_cancelamento;
    }

    /**
     * @param datetime $dt_cancelamento
     * @return SalaDeAula
     */
    public function setdt_cancelamento($dt_cancelamento)
    {
        $this->dt_cancelamento = $dt_cancelamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuarioatualiza()
    {
        return $this->id_usuarioatualiza;
    }

    /**
     * @param int $id_usuarioatualiza
     * @return SalaDeAula
     */
    public function setid_usuarioatualiza($id_usuarioatualiza)
    {
        $this->id_usuarioatualiza = $id_usuarioatualiza;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_ofertaexcepcional()
    {
        return $this->bl_ofertaexcepcional;
    }

    /**
     * @param bool $bl_ofertaexcepcional
     * @return SalaDeAula
     */
    public function setbl_ofertaexcepcional($bl_ofertaexcepcional)
    {
        $this->bl_ofertaexcepcional = $bl_ofertaexcepcional;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_vincularturma()
    {
        return $this->bl_vincularturma;
    }

    /**
     * @param bool $bl_vincularturma
     * @return SalaDeAula
     */
    public function setbl_vincularturma($bl_vincularturma)
    {
        $this->bl_vincularturma = $bl_vincularturma;
        return $this;
    }

    /**
     * @return EntidadeIntegracao
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param EntidadeIntegracao $id_entidadeintegracao
     * @return SalaDeAula
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


}

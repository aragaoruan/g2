<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_pais")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Pais
{

    /**
     *
     * @var integer $id_pais
     * @Column(name="id_pais", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pais;

    /**
     *
     * @var string $st_nomepais
     * @Column(name="st_nomepais", type="string", nullable=false, length=100) 
     */
    private $st_nomepais;

    /**
     *
     * @var string $st_nacionalidade
     * @Column(name="st_nacionalidade", type="string", nullable=true, length=150) 
     */
    private $st_nacionalidade;

    public function getId_pais ()
    {
        return $this->id_pais;
    }

    public function setId_pais ($id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    public function getSt_nomepais ()
    {
        return $this->st_nomepais;
    }

    public function setSt_nomepais ($st_nomepais)
    {
        $this->st_nomepais = $st_nomepais;
        return $this;
    }

    public function getSt_nacionalidade ()
    {
        return $this->st_nacionalidade;
    }

    public function setSt_nacionalidade ($st_nacionalidade)
    {
        $this->st_nacionalidade = $st_nacionalidade;
        return $this;
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Description of VwAlunoGradeIntegracao
 *
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alunogradeintegracao")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwAlunoGradeIntegracao extends G2Entity
{

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var datetime2 $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2", nullable=true, length=8)
     */
    private $dt_encerramento;

    /**
     * @var datetime2 $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="datetime2", nullable=true, length=8)
     */
    private $dt_encerramentosala;

    /**
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     */
    private $id_alocacao;

    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @Id
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @Id
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @Id
     * @var integer $id_status
     * @Column(name="id_status", type="integer", nullable=false, length=4)
     */
    private $id_status;

    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer", nullable=false, length=4)
     */
    private $id_situacaotcc;

    /**
     * @var integer $id_aproparcial
     * @Column(name="id_aproparcial", type="integer", nullable=false, length=4)
     */
    private $id_aproparcial;

    /**
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_tiposaladeaula;

    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=false, length=4)
     */
    private $id_categoriasala;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=true, length=1)
     */
    private $bl_encerrado;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_nomeexibicao
     * @Column(name="st_nomeexibicao", type="string", nullable=true, length=255)
     */
    private $st_nomeexibicao;

    /**
     * @var string $st_urlavatar
     * @Column(name="st_urlavatar", type="string", nullable=true, length=300)
     */
    private $st_urlavatar;

    /**
     * @var string $st_tituloexibicaoprojeto
     * @Column(name="st_tituloexibicaoprojeto", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaoprojeto;

    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(name="st_tituloexibicaomodulo", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaomodulo;

    /**
     * @var string $st_integracao
     * @Column(name="st_integracao", type="string", nullable=false, length=1)
     */
    private $st_integracao;

    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=12)
     */
    private $st_status;

    /**
     * @var string $st_codsistemaent
     * @Column(name="st_codsistemaent", type="string", nullable=true)
     */
    private $st_codsistemaent;

    /**
     * @var string $id_professor
     * @Column(name="id_professor", type="integer")
     */
    private $id_professor;

    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=true, length=300)
     */
    private $st_professor;

    /**
     * @var string $st_coordenador
     * @Column(name="st_coordenador", type="string", nullable=true, length=300)
     */
    private $st_coordenador;

    /**
     * @var string $st_caminho
     * @Column(name="st_caminho", type="string", nullable=true)
     */
    private $st_caminho;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;

    /**
     * @var string $st_descricaodisciplina
     * @Column(name="st_descricaodisciplina", type="string", nullable=true)
     */
    private $st_descricaodisciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_imagemdisciplina
     * @Column(name="st_imagemdisciplina", type="string", nullable=true, length=255)
     */
    private $st_imagemdisciplina;

    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=true)
     */
    private $st_codusuario;

    /**
     * @var string $st_loginintegrado
     * @Column(name="st_loginintegrado", type="string", nullable=true)
     */
    private $st_loginintegrado;

    /**
     * @var string $st_senhaintegrada
     * @Column(name="st_senhaintegrada", type="string", nullable=true)
     */
    private $st_senhaintegrada;

    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=true, length=30)
     */
    private $st_codsistemacurso;

    /**
     * @var string $st_codalocacao
     * @Column(name="st_codalocacao", type="string", nullable=true)
     */
    private $st_codalocacao;

    /**
     * @var integer $nu_diasacesso
     * @Column(name="nu_diasacesso", type="integer", nullable=true)
     */
    private $nu_diasacesso;

    /**
     * @var integer $id_entidadesala
     * @Column(name="id_entidadesala", type="integer", nullable=false, length=4)
     */
    private $id_entidadesala;

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true, length=4)
     */
    private $id_entidadeintegracao;

    /**
     * @var string $st_situacaotcc
     * @Column(name="st_situacaotcc", type="string", nullable=true)
     */
    private $st_situacaotcc;

    /**
     * @var integer $nu_ordenacao
     * @Column(name="nu_ordenacao", type="integer", nullable=true, length=1)
     */
    private $nu_ordenacao;

    /**
     * @return string
     */
    public function getst_imagemdisciplina()
    {
        return $this->st_imagemdisciplina;
    }

    /**
     * @param string $st_imagemdisciplina
     */
    public function setst_imagemdisciplina($st_imagemdisciplina)
    {
        $this->st_imagemdisciplina = $st_imagemdisciplina;
        return $this;

    }

    /**
     * @return int
     */
    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    /**
     * @param int $nu_diasacesso
     * @return VwAlunoGradeIntegracao
     */
    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
        return $this;
    }


    /**
     * @return date dt_abertura
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return datetime2 dt_encerramento
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return datetime2 dt_encerramentosala
     */
    public function getDt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    /**
     * @param dt_encerramentosala
     */
    public function setDt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_alocacao
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_modulo
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param id_modulo
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_status
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
        return $this;
    }

    /**
     * @return integer id_situacaotcc
     */
    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @param id_situacaotcc
     */
    public function setId_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
        return $this;
    }

    /**
     * @return integer id_aproparcial
     */
    public function getId_aproparcial()
    {
        return $this->id_aproparcial;
    }

    /**
     * @param id_aproparcial
     */
    public function setId_aproparcial($id_aproparcial)
    {
        $this->id_aproparcial = $id_aproparcial;
        return $this;
    }

    /**
     * @return integer id_tiposaladeaula
     */
    public function getId_tiposaladeaula()
    {
        return $this->id_tiposaladeaula;
    }

    /**
     * @param id_tiposaladeaula
     */
    public function setId_tiposaladeaula($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_categoriasala
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return integer id_tipodisciplina
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return boolean bl_encerrado
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bl_encerrado
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_nomeexibicao
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @param st_nomeexibicao
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;
    }

    /**
     * @return string st_urlavatar
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param st_urlavatar
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    /**
     * @return string st_tituloexibicaoprojeto
     */
    public function getSt_tituloexibicaoprojeto()
    {
        return $this->st_tituloexibicaoprojeto;
    }

    /**
     * @param st_tituloexibicaoprojeto
     */
    public function setSt_tituloexibicaoprojeto($st_tituloexibicaoprojeto)
    {
        $this->st_tituloexibicaoprojeto = $st_tituloexibicaoprojeto;
        return $this;
    }

    /**
     * @return string st_tituloexibicaomodulo
     */
    public function getSt_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @param st_tituloexibicaomodulo
     */
    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;
    }

    /**
     * @return string st_integracao
     */
    public function getSt_integracao()
    {
        return $this->st_integracao;
    }

    /**
     * @param st_integracao
     */
    public function setSt_integracao($st_integracao)
    {
        $this->st_integracao = $st_integracao;
        return $this;
    }

    /**
     * @return string st_status
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
        return $this;
    }

    /**
     * @return string st_codsistemaent
     */
    public function getSt_codsistemaent()
    {
        return $this->st_codsistemaent;
    }

    /**
     * @param st_codsistemaent
     */
    public function setSt_codsistemaent($st_codsistemaent)
    {
        $this->st_codsistemaent = $st_codsistemaent;
        return $this;
    }

    /**
     * @return string st_professor
     */
    public function getSt_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param st_professor
     */
    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
        return $this;
    }

    /**
     * @return string st_coordenador
     */
    public function getSt_coordenador()
    {
        return $this->st_coordenador;
    }

    /**
     * @param st_coordenador
     */
    public function setSt_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
        return $this;
    }


    /**
     * @param $id_professor
     * @return $this
     */
    public function setId_professor($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @return string
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @return string st_caminho
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param st_caminho
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string st_descricaodisciplina
     */
    public function getSt_descricaodisciplina()
    {
        return $this->st_descricaodisciplina;
    }

    /**
     * @param st_descricaodisciplina
     */
    public function setSt_descricaodisciplina($st_descricaodisciplina)
    {
        $this->st_descricaodisciplina = $st_descricaodisciplina;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_codusuario
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @return string st_loginintegrado
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @param st_loginintegrado
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
        return $this;
    }

    /**
     * @return string st_senhaintegrada
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @param st_senhaintegrada
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
        return $this;
    }

    /**
     * @return string st_codsistemacurso
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return string st_codalocacao
     */
    public function getSt_codalocacao()
    {
        return $this->st_codalocacao;
    }

    /**
     * @param st_codalocacao
     */
    public function setSt_codalocacao($st_codalocacao)
    {
        $this->st_codalocacao = $st_codalocacao;
        return $this;
    }

    public function getId_entidadesala()
    {
        return $this->id_entidadesala;
    }

    public function setId_entidadesala($id_entidadesala)
    {
        $this->id_entidadesala = $id_entidadesala;
    }

    /**
     * @return int
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return $this
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }

    public function getSt_situacaotcc()
    {
        return $this->st_situacaotcc;
    }

    public function setSt_situacaotcc($st_situacaotcc)
    {
        $this->st_situacaotcc = $st_situacaotcc;
    }

    /**
     * @return integer
     */
    public function getNu_ordenacao()
    {
        return $this->nu_ordenacao;
    }

    /**
     * @param integer $nu_ordenacao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setNu_ordenacao($nu_ordenacao)
    {
        $this->nu_ordenacao = $nu_ordenacao;
        return $this;
    }
}

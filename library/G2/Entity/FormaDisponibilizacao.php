<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_formadisponibilizacao")
 * @Entity
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */


class FormaDisponibilizacao {

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_formadisponibilizacao
     * @Column(name="id_formadisponibilizacao", type="integer", nullable=false, length=4)
     */
    private $id_formadisponibilizacao;
    /**
     * @var integer $bl_ativo
     * @Column(name="bl_ativo", type="integer", nullable=false, length=4)
     */
    private $bl_ativo;
    /**
     * @var string $st_formadisponibilizacao
     * @Column(name="st_formadisponibilizacao", type="string", nullable=false, length=250)
     */
    private $st_formadisponibilizacao;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return integer id_formadisponibilizacao
     */
    public function getId_formadisponibilizacao() {
        return $this->id_formadisponibilizacao;
    }

    /**
     * @return integer bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @return string st_formadisponibilizacao
     */
    public function getSt_formadisponibilizacao() {
        return $this->st_formadisponibilizacao;
    }



} 
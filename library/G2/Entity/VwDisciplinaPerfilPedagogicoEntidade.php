<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_disciplinaperfilpedagogicoentidade")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-12
 */
class VwDisciplinaPerfilPedagogicoEntidade {

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplina;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=true)
     */
    private $id_perfil;
    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true)
     */
    private $id_perfilpedagogico;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false)
     */
    private $st_disciplina;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true)
     */
    private $st_nomecompleto;


    /**
     * Substitui a criacao dos metodos GET e SET
     */
    public function __call($method, $params = array()) {
        $action = substr($method, 0, 3);
        $attribute = strtolower(substr($method, 3));

        if ($action == 'get')
            return $this->$attribute;
        else if ($action == 'set') {
            $this->$attribute = implode($params);
            return $this;
        }
    }

}

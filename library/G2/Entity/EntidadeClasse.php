<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeclasse")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class EntidadeClasse
{

    /**
     * @var integer $id_entidadeclasse
     * @Column(name="id_entidadeclasse", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidadeclasse;

    /**
     * @var string $st_entidadeclasse
     * @Column(name="st_entidadeclasse", type="string", nullable=false, length=255)
     */
    private $st_entidadeclasse;

    /**
     * @param int $id_entidadeclasse
     */
    public function setId_entidadeclasse($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeclasse()
    {
        return $this->id_entidadeclasse;
    }

    /**
     * @param string $st_entidadeclasse
     */
    public function setSt_entidadeclasse($st_entidadeclasse)
    {
        $this->st_entidadeclasse = $st_entidadeclasse;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_entidadeclasse()
    {
        return $this->st_entidadeclasse;
    }


}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_configuracaoentidade")
 * @Entity
 * @EntityLog
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class ConfiguracaoEntidade extends G2Entity
{

    /**
     * @var integer $id_configuracaoentidade
     * @Column(name="id_configuracaoentidade", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_configuracaoentidade;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_textosistemafichac
     * @Column(name="id_textosistemafichac", type="integer", nullable=true, length=4)
     */
    private $id_textosistemafichac;

    /**
     * @var integer $id_modelogradenotas
     * @Column(name="id_modelogradenotas", type="integer", nullable=false, length=4)
     */
    private $id_modelogradenotas;

    /**
     * @var integer $nu_maxextensao
     * @Column(name="nu_maxextensao", type="integer", nullable=false, length=4)
     */
    private $nu_maxextensao;

    /**
     * @var integer $nu_encerramentoprofessor
     * @Column(name="nu_encerramentoprofessor", type="integer", nullable=false, length=4)
     */
    private $nu_encerramentoprofessor;

    /**
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_categoriaocorrencia;

    /**
     * @var integer $id_textosemsala
     * @Column(name="id_textosemsala", type="integer", nullable=true, length=4)
     */
    private $id_textosemsala;


    /**
     * @var integer $nu_horasencontro
     * @Column(name="nu_horasencontro", type="integer", nullable=true)
     */
    private $nu_horasencontro;

    /**
     * * @var Upload $id_manualaluno
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_manualaluno", referencedColumnName="id_upload")
     */
    private $id_manualaluno;

    /**
     * @var integer $id_assuntoresgate
     * @Column(name="id_assuntoresgate", type="integer", nullable=true, length=4)
     */
    private $id_assuntoresgate;

    /**
     * @var integer $id_textoresgate
     * @Column(name="id_textoresgate", type="integer", nullable=true, length=4)
     */
    private $id_textoresgate;

    /**
     * * @var Upload $id_tabelapreco
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_tabelapreco", referencedColumnName="id_upload")
     */
    private $id_tabelapreco;

    /**
     * @var integer $id_assuntorecuperacao
     * @Column(name="id_assuntorecuperacao", type="integer", nullable=true, length=4)
     */
    private $id_assuntorecuperacao;

    /**
     * @var boolean $bl_bloqueiadisciplina
     * @Column(name="bl_bloqueiadisciplina", type="boolean", nullable=false, length=1)
     */
    private $bl_bloqueiadisciplina;

    /**
     * @var boolean $bl_encerrasemacesso
     * @Column(name="bl_encerrasemacesso", type="boolean", nullable=false, length=1)
     */
    private $bl_encerrasemacesso;

    /**
     * @var boolean $bl_emailunico
     * @Column(name="bl_emailunico", type="boolean", nullable=true, length=1)
     */
    private $bl_emailunico;

    /**
     * @var boolean $bl_tutorial
     * @Column(name="bl_tutorial", type="boolean", nullable=true, length=1)
     */
    private $bl_tutorial;

    /**
     * @var string $st_informacoestecnicas
     * @Column(name="st_informacoestecnicas", type="string", nullable=true, length=8000)
     */
    private $st_informacoestecnicas;

    /**
     * @var boolean $bl_primeiroacesso
     * @Column(name="bl_primeiroacesso", type="boolean", nullable=true, length=1)
     */
    private $bl_primeiroacesso;

    /**
     * @var boolean $bl_professordisciplina
     * @Column(name="bl_professordisciplina", type="boolean", nullable=true, length=1)
     */
    private $bl_professordisciplina;

    /**
     * @var boolean $st_campotextogooglecredito
     * @Column(name="st_campotextogooglecredito", type="string", nullable=true, length=8000)
     */
    private $st_campotextogooglecredito;

    /**
     * @var string $st_corprimarialoja
     * @Column(name="st_corprimarialoja", type="string", nullable=true, length=7)
     */
    private $st_corprimarialoja;
    /**
     * @var string $st_corsecundarialoja
     * @Column(name="st_corsecundarialoja", type="string", nullable=true, length=7)
     */
    private $st_corsecundarialoja;
    /**
     * @var string $st_corterciarialoja
     * @Column(name="st_corterciarialoja", type="string", nullable=true, length=7)
     */
    private $st_corterciarialoja;

    /**
     * @var boolean $st_campotextogoogleboleto
     * @Column(name="st_campotextogoogleboleto", type="string", nullable=true, length=8000)
     */
    private $st_campotextogoogleboleto;

    /**
     * @var boolean $st_corentidade
     * @Column(name="st_corentidade", type="string", nullable=true, length=15)
     */
    private $st_corentidade;

    /**
     * @var boolean $bl_acessoapp
     * @Column(name="bl_acessoapp", type="boolean")
     */
    private $bl_acessoapp;
    /**
     * @var boolean $id_assuntorequerimento
     * @Column(name="id_assuntorequerimento", type="string", nullable=true, length=15)
     */
    private $id_assuntorequerimento;


    /**
     * @var AssuntoCo $id_assuntoduplicidade
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoduplicidade", referencedColumnName="id_assuntoco")
     */
    private $id_assuntoduplicidade;

    /**
     * @var boolean $bl_msgagendamentoportal
     * @Column(name="bl_msgagendamentoportal", type="boolean")
     */
    private $bl_msgagendamentoportal;

    /**
     * @var AssuntoCo $id_assuntoisencao
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoisencao", referencedColumnName="id_assuntoco")
     */
    private $id_assuntoisencao;

    /**
     * @var string $st_codgoogleanalytics
     * @Column(name="st_codgoogleanalytics", type="string", nullable=true, length=20)
     */
    private $st_codgoogleanalytics;

    /**
     * @var string $st_googletagmanager
     * @Column(name="st_googletagmanager", type="string", nullable=true, length=20)
     */
    private $st_googletagmanager;

    /**
     * @var boolean $bl_loginsimultaneo
     * @Column(name="bl_loginsimultaneo", type="boolean", nullable=true, length=1)
     */
    private $bl_loginsimultaneo;

    /**
     * @var boolean $bl_minhasprovas
     * @Column(name="bl_minhasprovas", type="boolean")
     */
    private $bl_minhasprovas;

    /**
     * @param boolean $bl_professordisciplina
     * @return $this
     */
    public function setBl_professordisciplina($bl_professordisciplina)
    {
        $this->bl_professordisciplina = $bl_professordisciplina;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_professordisciplina()
    {
        return $this->bl_professordisciplina;
    }

    /**
     * @param boolean $bl_bloqueiadisciplina
     */
    public function setBl_bloqueiadisciplina($bl_bloqueiadisciplina)
    {
        $this->bl_bloqueiadisciplina = $bl_bloqueiadisciplina;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_bloqueiadisciplina()
    {
        return $this->bl_bloqueiadisciplina;
    }

    /**
     * @param boolean $bl_emailunico
     */
    public function setBl_emailunico($bl_emailunico)
    {
        $this->bl_emailunico = $bl_emailunico;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_emailunico()
    {
        return $this->bl_emailunico;
    }

    /**
     * @param boolean $bl_encerrasemacesso
     */
    public function setBl_encerrasemacesso($bl_encerrasemacesso)
    {
        $this->bl_encerrasemacesso = $bl_encerrasemacesso;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_encerrasemacesso()
    {
        return $this->bl_encerrasemacesso;
    }

    /**
     * @param boolean $bl_primeiroacesso
     */
    public function setBl_primeiroacesso($bl_primeiroacesso)
    {
        $this->bl_primeiroacesso = $bl_primeiroacesso;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_primeiroacesso()
    {
        return $this->bl_primeiroacesso;
    }

    /**
     * @return boolean
     */
    public function getBl_loginsimultaneo()
    {
        return $this->bl_primeiroacesso;
    }

    /**
     * @param boolean $bl_tutorial
     */
    public function setBl_tutorial($bl_tutorial)
    {
        $this->bl_tutorial = $bl_tutorial;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_tutorial()
    {
        return $this->bl_tutorial;
    }

    /**
     * @param int $id_assuntorecuperacao
     */
    public function setId_assuntorecuperacao($id_assuntorecuperacao)
    {
        $this->id_assuntorecuperacao = $id_assuntorecuperacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_assuntorecuperacao()
    {
        return $this->id_assuntorecuperacao;
    }

    /**
     * @param int $id_assuntoresgate
     */
    public function setId_assuntoresgate($id_assuntoresgate)
    {
        $this->id_assuntoresgate = $id_assuntoresgate;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_assuntoresgate()
    {
        return $this->id_assuntoresgate;
    }

    /**
     * @param int $id_categoriaocorrencia
     */
    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    /**
     * @param int $id_configuracaoentidade
     */
    public function setId_configuracaoentidade($id_configuracaoentidade)
    {
        $this->id_configuracaoentidade = $id_configuracaoentidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_configuracaoentidade()
    {
        return $this->id_configuracaoentidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_manualaluno
     */
    public function setId_manualaluno($id_manualaluno)
    {
        $this->id_manualaluno = $id_manualaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_manualaluno()
    {
        return $this->id_manualaluno;
    }

    /**
     * @param int $id_modelogradenotas
     */
    public function setId_modelogradenotas($id_modelogradenotas)
    {
        $this->id_modelogradenotas = $id_modelogradenotas;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_modelogradenotas()
    {
        return $this->id_modelogradenotas;
    }

    /**
     * @param int $id_tabelapreco
     */
    public function setId_tabelapreco($id_tabelapreco)
    {
        $this->id_tabelapreco = $id_tabelapreco;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tabelapreco()
    {
        return $this->id_tabelapreco;
    }

    /**
     * @param int $id_textoresgate
     */
    public function setId_textoresgate($id_textoresgate)
    {
        $this->id_textoresgate = $id_textoresgate;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_textoresgate()
    {
        return $this->id_textoresgate;
    }

    /**
     * @param int $id_textosemsala
     */
    public function setId_textosemsala($id_textosemsala)
    {
        $this->id_textosemsala = $id_textosemsala;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_textosemsala()
    {
        return $this->id_textosemsala;
    }

    /**
     * @param int $id_textosistemafichac
     */
    public function setId_textosistemafichac($id_textosistemafichac)
    {
        $this->id_textosistemafichac = $id_textosistemafichac;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_textosistemafichac()
    {
        return $this->id_textosistemafichac;
    }

    /**
     * @param int $nu_encerramentoprofessor
     */
    public function setNu_encerramentoprofessor($nu_encerramentoprofessor)
    {
        $this->nu_encerramentoprofessor = $nu_encerramentoprofessor;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_encerramentoprofessor()
    {
        return $this->nu_encerramentoprofessor;
    }

    /**
     * @param int $nu_maxextensao
     */
    public function setNu_maxextensao($nu_maxextensao)
    {
        $this->nu_maxextensao = $nu_maxextensao;
        return $this;
    }

    /**
     * @param int $bl_loginsimultaneo
     */
    public function setBl_loginsimultaneo($bl_loginsimultaneo)
    {
        $this->bl_loginsimultaneo = $bl_loginsimultaneo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_maxextensao()
    {
        return $this->nu_maxextensao;
    }

    /**
     * @param string $st_informacoestecnicas
     */
    public function setSt_informacoestecnicas($st_informacoestecnicas)
    {
        $this->st_informacoestecnicas = $st_informacoestecnicas;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_informacoestecnicas()
    {
        return $this->st_informacoestecnicas;
    }

    /**
     * @return string
     */
    public function getSt_campotextogoogleboleto()
    {
        return $this->st_campotextogoogleboleto;
    }

    /**
     * @param string $st_campotextogoogleboleto
     */
    public function setSt_campotextogoogleboleto($st_campotextogoogleboleto)
    {
        $this->st_campotextogoogleboleto = $st_campotextogoogleboleto;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSt_campotextogooglecredito()
    {
        return $this->st_campotextogooglecredito;
    }

    /**
     * @param boolean $st_campotextogooglecredito
     */
    public function setSt_campotextogooglecredito($st_campotextogooglecredito)
    {
        $this->st_campotextogooglecredito = $st_campotextogooglecredito;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSt_corentidde()
    {
        return $this->st_corentidade;
    }

    /**
     * @param boolean $st_corentidade
     */
    public function setSt_corentidade($st_corentidade)
    {
        $this->st_corentidade = $st_corentidade;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_horasencontro()
    {
        return $this->nu_horasencontro;
    }

    /**
     * @return string
     */
    public function getSt_corprimarialoja()
    {
        return $this->st_corprimarialoja;
    }

    /**
     * @param integer $nu_horasencontro
     * @return \G2\Entity\ConfiguracaoEntidade
     */
    public function setNu_horasencontro($nu_horasencontro)
    {
        $this->nu_horasencontro = $nu_horasencontro;
        return $this;
    }

    /**
     * @param string $st_corprimarialoja
     */
    public function setSt_corprimarialoja($st_corprimarialoja)
    {
        $this->st_corprimarialoja = $st_corprimarialoja;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_corsecundarialoja()
    {
        return $this->st_corsecundarialoja;
    }

    /**
     * @param string $st_corsecundarialoja
     */
    public function setSt_corsecundarialoja($st_corsecundarialoja)
    {
        $this->st_corsecundarialoja = $st_corsecundarialoja;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_corterciarialoja()
    {
        return $this->st_corterciarialoja;
    }

    /**
     * @param string $st_corterciarialoja
     */
    public function setSt_corterciarialoja($st_corterciarialoja)
    {
        $this->st_corterciarialoja = $st_corterciarialoja;
        return $this;
    }

    /**
     * @return string
     */
    public function getId_assuntorequerimento()
    {
        return $this->id_assuntorequerimento;
    }

    /**
     * @param string $id_assuntorequerimento
     */
    public function setId_assuntorequerimento($id_assuntorequerimento)
    {
        $this->id_assuntorequerimento = $id_assuntorequerimento;
        return $this;
    }

    public function getId_assuntoduplicidade()
    {
        return $this->id_assuntoduplicidade;
    }

    public function setId_assuntoduplicidade($id_assuntoduplicidade)
    {
        $this->id_assuntoduplicidade = $id_assuntoduplicidade;
    }

    /**
     * @return boolean
     */
    public function getBl_acessoapp()
    {
        return $this->bl_acessoapp;
    }

    /**
     * @param boolean $bl_acessoapp
     * @return $this;
     */
    public function setBl_acessoapp($bl_acessoapp)
    {
        $this->bl_acessoapp = $bl_acessoapp;
        return $this;
    }

    /**
     * @return AssuntoCo
     */
    public function getId_assuntoisencao()
    {
        return $this->id_assuntoisencao;
    }

    /**
     * @param AssuntoCo $id_assuntoisencao
     * @return $this
     */
    public function setId_assuntoisencao($id_assuntoisencao)
    {
        $this->id_assuntoisencao = $id_assuntoisencao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_msgagendamentoportal()
    {
        return $this->bl_msgagendamentoportal;
    }

    /**
     * @param boolean $bl_msgagendamentoportal
     * @return ConfiguracaoEntidade
     */
    public function setBl_msgagendamentoportal($bl_msgagendamentoportal)
    {
        $this->bl_msgagendamentoportal = $bl_msgagendamentoportal;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codgoogleanalytics()
    {
        return $this->st_codgoogleanalytics;
    }

    /**
     * @param string $st_codgoogleanalytics
     * @return ConfiguracaoEntidade
     */
    public function setSt_codgoogleanalytics($st_codgoogleanalytics)
    {
        $this->st_codgoogleanalytics = $st_codgoogleanalytics;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_googletagmanager()
    {
        return $this->st_googletagmanager;
    }

    /**
     * @param string $st_googletagmanager
     * @return ConfiguracaoEntidade
     */
    public function setSt_googletagmanager($st_googletagmanager)
    {
        $this->st_googletagmanager = $st_googletagmanager;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_minhasprovas()
    {
        return $this->bl_minhasprovas;
    }

    /**
     * @param bool $bl_minhasprovas
     * @return $this
     */
    public function setBl_minhasprovas($bl_minhasprovas)
    {
        $this->bl_minhasprovas = $bl_minhasprovas;
        return $this;
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 12/11/13
 * Time: 09:55
 */

namespace G2\Entity;

use \Doctrine\Mapping as ORM;
use G2\G2Entity;

/**
 * @Entity(repositoryClass="G2\Repository\Lancamento")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_lancamento")
 */
class Lancamento extends G2Entity
{

    /**
     * @var integer $id_lancamento
     * @Id
     * @Column(name="id_lancamento", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_lancamento;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valor;

    /**
     * @var decimal $nu_vencimento
     * @Column(name="nu_vencimento", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_vencimento;

    /**
     * @var TipoLancamento
     *
     * @ManyToOne(targetEntity="TipoLancamento")
     * @JoinColumn(name="id_tipolancamento", referencedColumnName="id_tipolancamento")
     */
    private $id_tipolancamento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var MeioPagamento
     *
     * @ManyToOne(targetEntity="MeioPagamento")
     * @JoinColumn(name="id_meiopagamento", referencedColumnName="id_meiopagamento")
     */
    private $id_meiopagamento;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true)
     */
    private $id_usuariolancamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var Banco
     *
     * @ManyToOne(targetEntity="Banco")
     * @JoinColumn(name="st_banco", referencedColumnName="st_banco")
     */
    private $st_banco;

    /**
     * @var CartaoConfig
     * @ManyToOne(targetEntity="CartaoConfig")
     * @JoinColumn(name="id_cartaoconfig", referencedColumnName="id_cartaoconfig")
     */
    private $id_cartaoconfig;

    /**
     * @var BoletoConfig
     * @ManyToOne(targetEntity="BoletoConfig")
     * @JoinColumn(name="id_boletoconfig", referencedColumnName="id_boletoconfig")
     */
    private $id_boletoconfig;

    /**
     * @var boolean $bl_quitado
     * @Column(name="bl_quitado", type="boolean", nullable=false)
     */
    private $bl_quitado;

    /**
     * @var date $dt_vencimento
     * @Column(name="dt_vencimento", type="date", nullable=true)
     */
    private $dt_vencimento;

    /**
     * @var date $dt_vencimentobackup
     * @Column(name="dt_vencimentobackup", type="datetime", nullable=true)
     */
    private $dt_vencimentobackup;

    /**
     * @var date $dt_quitado
     * @Column(name="dt_quitado", type="date", nullable=true)
     */
    private $dt_quitado;

    /**
     * @var datetime2 $dt_emissao
     * @Column(name="dt_emissao", type="datetime2", nullable=true)
     */
    private $dt_emissao;

    /**
     * @var datetime2 $dt_prevquitado
     * @Column(name="dt_prevquitado", type="datetime2", nullable=true)
     */
    private $dt_prevquitado;

    /**
     * @var string $st_emissor
     * @Column(name="st_emissor", type="string", nullable=true)
     */
    private $st_emissor;

    /**
     * @var string $st_coddocumento
     * @Column(name="st_coddocumento", type="string", nullable=true)
     */
    private $st_coddocumento;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_juros;

    /**
     * @var decimal $nu_desconto
     * @Column(name="nu_desconto", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_desconto;

    /**
     * @var decimal $nu_quitado
     * @Column(name="nu_quitado", type="decimal", precision=30, scale=5, nullable=true)
     */
    private $nu_quitado;

    /**
     * @var decimal $nu_multa
     * @Column(name="nu_multa", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_multa;

    /**
     * @var Entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadelancamento", referencedColumnName="id_entidade")
     */
    private $id_entidadelancamento;


    /**
     * @var Sistema
     * @ManyToOne(targetEntity="Sistema")
     * @Column(name="id_sistemacobranca", type="integer", nullable=true)
     */
    private $id_sistemacobranca;


    /**
     * @var datetime2 $dt_atualizado
     * @Column(name="dt_atualizado", type="datetime2", nullable=true)
     */
    private $dt_atualizado;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_original
     * @Column(name="bl_original", type="boolean", nullable=false, options={"default":0})
     */
    private $bl_original = 0;

    /**
     * @var string $st_renegociacao
     * @Column(name="st_renegociacao", type="string", nullable=true, length=10)
     */
    private $st_renegociacao;

    /**
     * @var integer $id_lancamentooriginal
     * @Column(name="id_lancamentooriginal", type="integer", nullable=true)
     */
    private $id_lancamentooriginal;

    /**
     * @var integer $id_acordo
     * @Column(name="id_acordo", type="integer", nullable=true)
     */
    private $id_acordo;

    /**
     * @var string $st_agencia
     * @Column(name="st_agencia", type="string", nullable=true, length=10)
     */
    private $st_agencia;

    /**
     * @var string $st_numcheque
     * @Column(name="st_numcheque", type="string", nullable=true, length=10)
     */
    private $st_numcheque;

    /**
     * @var string $st_nossonumero
     * @Column(name="st_nossonumero", type="string", nullable=true, length=20)
     */
    private $st_nossonumero;

    /**
     * @var string $st_codconta
     * @Column(name="st_codconta", type="string", nullable=true, length=10)
     */
    private $st_codconta;

    /**
     * @var integer $id_codcoligada
     * @Column(name="id_codcoligada", type="integer", nullable=true)
     */
    private $id_codcoligada;

    /**
     * @var string $st_statuslan
     * @Column(name="st_statuslan", type="string", nullable=true, length=1)
     */
    private $st_statuslan;

    /**
     * @var date $dt_vencimentocheque
     * @Column(name="dt_vencimentocheque", type="date", nullable=true)
     */
    private $dt_vencimentocheque;

    /**
     * @var integer $nu_verificacao
     * @Column(name="nu_verificacao", type="integer", nullable=false)
     */
    private $nu_verificacao;

    /**
     * @var boolean $bl_baixadofluxus
     * @Column(name="bl_baixadofluxus", type="boolean", nullable=false, options={"default":0})
     */
    private $bl_baixadofluxus = 0;

    /**
     * @var integer $nu_tentativabraspag
     * @Column(name="nu_tentativabraspag", type="integer", nullable=false)
     */
    private $nu_tentativabraspag;

    /**
     * @var integer $nu_cartao
     * @Column(name="nu_cartao", type="integer", nullable=false)
     */
    private $nu_cartao;

    /**
     * @var string $st_retornoverificacao
     * @Column(name="st_retornoverificacao", type="string", nullable=true)
     */
    private $st_retornoverificacao;

    /**
     * @var string $st_autorizacao
     * @Column(name="st_autorizacao", type="string", nullable=true)
     */
    private $st_autorizacao;

    /**
     * @var string $st_ultimosdigitos
     * @Column(name="st_ultimosdigitos", type="string", nullable=true, length=4)
     */
    private $st_ultimosdigitos;

    /**
     * @var boolean $bl_chequedevolvido
     *
     * @Column(name="bl_chequedevolvido", type="boolean", nullable=true)
     */
    private $bl_chequedevolvido;

    /**
     * @var string $st_urlboleto
     * @Column(name="st_urlboleto", type="string", length=250)
     */
    private $st_urlboleto;

    /**
     * @var string $st_codigobarras
     * @Column(name="st_codigobarras", type="string", length=250)
     */
    private $st_codigobarras;

    /**
     * @var string $st_idtransacaoexterna
     * @Column(name="id_transacaoexterna", type="string", length=150)
     */
    private $st_idtransacaoexterna;

    /**
     * @var boolean $bl_cancelamento
     *
     * @Column(name="bl_cancelamento", type="boolean", nullable=true)
     */
    private $bl_cancelamento;

    /**
     * @var integer $nu_cobranca
     * @Column(name="nu_cobranca", type="integer", nullable=true, length=4)
     */
    private $nu_cobranca;

    /**
     * @var integer $nu_assinatura
     * @Column(name="nu_assinatura", type="integer", nullable=true, length=4)
     */
    private $nu_assinatura;

    /**
     * @return int
     */
    public function getNu_assinatura()
    {
        return $this->nu_assinatura;
    }

    /**
     * @param int $nu_assinatura
     */
    public function setNu_assinatura($nu_assinatura)
    {
        $this->nu_assinatura = $nu_assinatura;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cobranca()
    {
        return $this->nu_cobranca;
    }

    /**
     * @param int $nu_cobranca
     */
    public function setNu_cobranca($nu_cobranca)
    {
        $this->nu_cobranca = $nu_cobranca;
        return $this;
    }

    /**
     * @return Sistema
     */
    public function getid_sistemacobranca()
    {
        return $this->id_sistemacobranca;
    }

    /**
     * @param Sistema $id_sistemacobranca
     */
    public function setid_sistemacobranca($id_sistemacobranca)
    {
        $this->id_sistemacobranca = $id_sistemacobranca;
        return $this;

    }


    /**
     * @return int
     */
    public function getnu_cartao()
    {
        return $this->nu_cartao;
    }

    /**
     * @param int $nu_cartao
     */
    public function setnu_cartao($nu_cartao)
    {
        $this->nu_cartao = $nu_cartao;
        return $this;

    }

    /**
     * @param string $st_chequedevolvido
     * @deprecated
     */
    public function setBlChequedevolvido($bl_chequedevolvido)
    {
        $this->bl_chequedevolvido = $bl_chequedevolvido;
    }

    /**
     * @param $bl_chequedevolvido
     */
    public function setBl_chequedevolvido($bl_chequedevolvido) {
        $this->bl_chequedevolvido = $bl_chequedevolvido;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getBlChequedevolvido()
    {
        return $this->bl_chequedevolvido;
    }

    /**
     * @return bool
     */
    public function getBl_chequedevolvido() {
        return $this->bl_chequedevolvido;
    }

    /**
     * @return char
     * @deprecated
     */
    public function getStUltimosdigitos()
    {
        return $this->st_ultimosdigitos;
    }

    /**
     * @return string
     */
    public function getSt_ultimosdigitos() {
        return $this->st_ultimosdigitos;
    }

    /**
     * @param char $st_ultimosdigitos
     * @deprecated
     */
    public function setStUltimosdigitos($st_ultimosdigitos)
    {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @param $st_ultimosdigitos
     */
    public function setSt_ultimosdigitos($st_ultimosdigitos) {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStAutorizacao()
    {
        return $this->st_autorizacao;
    }

    /**
     * @return string
     */
    public function getSt_autorizacao() {
        return$this->st_autorizacao;
    }

    /**
     * @param string $st_autorizacao
     * @deprecated
     */
    public function setStAutorizacao($st_autorizacao)
    {
        $this->st_autorizacao = $st_autorizacao;
    }

    /**
     * @param $st_autorizacao
     */
    public function setSt_autorizacao($st_autorizacao) {
        $this->st_autorizacao = $st_autorizacao;
    }

    /**
     * @param string $st_retornoverificacao
     * @deprecated
     */
    public function setStRetornoverificacao($st_retornoverificacao)
    {
        $this->st_retornoverificacao = $st_retornoverificacao;
    }

    /**
     * @param $st_retornoverificacao
     */
    public function setSt_retornoverificacao($st_retornoverificacao) {
        $this->st_retornoverificacao = $st_retornoverificacao;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStRetornoverificacao()
    {
        return $this->st_retornoverificacao;
    }

    /**
     * @return string
     */
    public function getSt_retornoverificacao() {
        return $this->st_retornoverificacao;
    }

    /**
     * @param int $nu_tentativabraspag
     * @deprecated
     */
    public function setNuTentativabraspag($nu_tentativabraspag)
    {
        $this->nu_tentativabraspag = $nu_tentativabraspag;
    }

    /**
     * @param $nu_tentativabraspag
     */
    public function setNu_tentativabraspag($nu_tentativabraspag) {
        $this->nu_tentativabraspag = $nu_tentativabraspag;
    }

    /**
     * @return int
     */
    public function getNuTentativabraspag()
    {
        return $this->nu_tentativabraspag;
    }

    /**
     * @return int
     */
    public function getNu_tentativabraspag() {
        return $this->nu_tentativabraspag;
    }

    /**
     * @param boolean $bl_ativo
     * @deprecated
     */
    public function setBlAtivo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param $bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlAtivo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return bool
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_quitado
     * @deprecated
     */
    public function setBlQuitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @param $bl_quitado
     */
    public function setBl_quitado($bl_quitado) {
        $this->bl_quitado = $bl_quitado;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlQuitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @return bool
     */
    public function getBl_quitado() {
        return $this->bl_quitado;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_atualizado
     * @deprecated
     */
    public function setDtAtualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @param $dt_atualizado
     */
    public function setDt_atualizado($dt_atualizado) {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @return \G2\Entity\datetime2
     * @deprecated
     */
    public function getDtAtualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @return datetime2
     */
    public function getDt_atualizado() {
        return $this->dt_atualizado;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     * @deprecated
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     * @deprecated
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_emissao
     * @deprecated
     */
    public function setDtEmissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @param $dt_emissao
     */
    public function setDt_emissao($dt_emissao) {
        $this->dt_emissao = $dt_emissao;
    }

    /**
     * @return \G2\Entity\datetime2
     * @deprecated
     */
    public function getDtEmissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @return datetime2
     */
    public function getDt_emissao() {
        return $this->dt_emissao;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_prevquitado
     * @deprecated
     */
    public function setDtPrevquitado($dt_prevquitado = null)
    {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @param null $dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado = null) {
        $this->dt_prevquitado = $dt_prevquitado;
    }

    /**
     * @return \G2\Entity\datetime2
     * @deprecated
     */
    public function getDtPrevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @return datetime2
     */
    public function getDt_prevquitado() {
        return$this->dt_prevquitado;
    }

    /**
     * @param \G2\Entity\date $dt_quitado
     * @deprecated
     */
    public function setDtQuitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
    }

    /**
     * @param $dt_quitado
     */
    public function setDt_quitado($dt_quitado) {
        $this->dt_quitado =$dt_quitado;
    }

    /**
     * @return \G2\Entity\date
     * @deprecated
     */
    public function getDtQuitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @return date
     */
    public function getDt_quitado() {
        return $this->dt_quitado;
    }

    /**
     * @param \G2\Entity\date $dt_vencimento
     * @deprecated
     */
    public function setDtVencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @param $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento) {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @return \G2\Entity\date
     * @deprecated
     */
    public function getDtVencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @return date
     */
    public function getDt_vencimento() {
        return $this->dt_vencimento;
    }

    /**
     * @param \G2\Entity\date $dt_vencimentocheque
     * @deprecated
     */
    public function setDtVencimentocheque($dt_vencimentocheque)
    {
        $this->dt_vencimentocheque = $dt_vencimentocheque;
    }

    /**
     * @param $dt_vencimentocheque
     */
    public function setDt_vencimentocheque($dt_vencimentocheque) {
        $this->dt_vencimentocheque= $dt_vencimentocheque;
    }

    /**
     * @return \G2\Entity\date
     * @deprecated
     */
    public function getDtVencimentocheque()
    {
        return $this->dt_vencimentocheque;
    }

    /**
     * @return date
     */
    public function getDt_vencimentocheque() {
        return $this->dt_vencimentocheque;
    }

    /**
     * @param int $id_acordo
     * @deprecated
     */
    public function setIdAcordo($id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @param $id_acordo
     */
    public function setId_acordo($id_acordo) {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdAcordo()
    {
        return $this->id_acordo;
    }

    /**
     * @return int
     */
    public function getId_acordo() {
        return $this->id_acordo;
    }

    /**
     * @param \G2\Entity\BoletoConfig $id_boletoconfig
     * @deprecated
     */
    public function setIdBoletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
    }

    /**
     * @param $id_boletoconfig
     */
    public function setId_boletoconfig($id_boletoconfig) {
        $this->id_boletoconfig = $id_boletoconfig;
    }

    /**
     * @return \G2\Entity\BoletoConfig
     * @deprecated
     */
    public function getIdBoletoconfig()
    {
        return $this->id_boletoconfig;
    }

    /**
     * @return BoletoConfig
     */
    public function getId_boletoconfig() {
        return $this->id_boletoconfig;
    }

    /**
     * @param \G2\Entity\CartaoConfig $id_cartaoconfig
     * @deprecated
     */
    public function setIdCartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
    }

    /**
     * @param $id_cartaoconfig
     */
    public function setId_cartaoconfig($id_cartaoconfig) {
        $this->id_cartaoconfig = $id_cartaoconfig;
    }

    /**
     * @return \G2\Entity\CartaoConfig
     * @deprecated
     */
    public function getIdCartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @return CartaoConfig
     */
    public function getId_cartaoconfig() {
        return $this->id_cartaoconfig;
    }

    /**
     * @param int $id_codcoligada
     * @deprecated
     */
    public function setIdCodcoligada($id_codcoligada)
    {
        $this->id_codcoligada = $id_codcoligada;
    }

    /**
     * @param $id_codcoligada
     */
    public function setId_Codcoligada($id_codcoligada) {
        $this->id_codcoligada = $id_codcoligada;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdCodcoligada()
    {
        return $this->id_codcoligada;
    }

    /**
     * @return int
     */
    public function getId_codcoligada() {
        return $this->id_codcoligada;
    }

    /**
     * @param int $id_entidade
     * @deprecated
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade() {
        return$this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidadelancamento
     * @deprecated
     */
    public function setIdEntidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @param $id_entidadelancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento) {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @return \G2\Entity\Entidade
     * @deprecated
     */
    public function getIdEntidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadelancamento() {
        return $this->id_entidadelancamento;
    }

    /**
     * @param int $id_lancamento
     * @deprecated
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @param $id_lancamento
     */
    public function setId_lancamento($id_lancamento) {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @return int
     */
    public function getId_lancamento() {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamentooriginal
     * @deprecated
     */
    public function setIdLancamentooriginal($id_lancamentooriginal)
    {
        $this->id_lancamentooriginal = $id_lancamentooriginal;
    }

    /**
     * @param $id_lancamentooriginal
     */
    public function setId_lancamentooriginal($id_lancamentooriginal) {
        $this->id_lancamentooriginal = $id_lancamentooriginal;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdLancamentooriginal()
    {
        return $this->id_lancamentooriginal;
    }

    /**
     * @return int
     */
    public function getId_lancamentooriginal() {
        return $this->id_lancamentooriginal;
    }

    /**
     * @param \G2\Entity\MeioPagamento $id_meiopagamento
     * @deprecated
     */
    public function setIdMeiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @param $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento) {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return \G2\Entity\MeioPagamento
     * @deprecated
     */
    public function getIdMeiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @return MeioPagamento
     */
    public function getId_meiopagamento() {
        return $this->id_meiopagamento;
    }

    /**
     * @param \G2\Entity\TipoLancamento $id_tipolancamento
     * @deprecated
     */
    public function setIdTipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @param $id_tipolancamento
     */
    public function setId_tipolancamento($id_tipolancamento) {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @return \G2\Entity\TipoLancamento
     * @deprecated
     */
    public function getIdTipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @return TipoLancamento
     */
    public function getId_tipolancamento() {
        return $this->id_tipolancamento;
    }

    /**
     * @param int $id_usuariocadastro
     * @deprecated
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariolancamento
     * @deprecated
     */
    public function setIdUsuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @param $id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento) {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @return int
     */
    public function getId_usuariolancamento() {
        return $this->id_usuariolancamento;
    }

    /**
     * @param \G2\Entity\decimal $nu_desconto
     * @deprecated
     */
    public function setNuDesconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @param $nu_desconto
     */
    public function setNu_desconto($nu_desconto) {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuDesconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @return decimal
     */
    public function getNu_desconto() {
        return $this->nu_desconto;
    }

    /**
     * @param \G2\Entity\decimal $nu_juros
     * @deprecated
     */
    public function setNuJuros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @param $nu_juros
     */
    public function setNu_juros($nu_juros) {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuJuros()
    {
        return $this->nu_juros;
    }

    /**
     * @return decimal
     */
    public function getNu_juros() {
        return $this->nu_juros;
    }

    /**
     * @param \G2\Entity\decimal $nu_multa
     * @deprecated
     */
    public function setNuMulta($nu_multa)
    {
        $this->nu_multa = $nu_multa;
    }

    /**
     * @param $nu_multa
     */
    public function setNu_multa($nu_multa) {
        $this->nu_multa = $nu_multa;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuMulta()
    {
        return $this->nu_multa;
    }

    /**
     * @return decimal
     */
    public function getNu_multa() {
        return $this->nu_multa;
    }

    /**
     * @param \G2\Entity\decimal $nu_quitado
     * @deprecated
     */
    public function setNuQuitado($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
    }

    /**
     * @param $nu_quitado
     */
    public function setNu_quitado($nu_quitado) {
        $this->nu_quitado = $nu_quitado;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuQuitado()
    {
        return $this->nu_quitado;
    }

    /**
     * @return decimal
     */
    public function getNu_quitado() {
        return $this->nu_quitado;
    }

    /**
     * @param \G2\Entity\decimal $nu_valor
     * @deprecated
     */
    public function setNuValor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @param $nu_valor
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuValor()
    {
        return $this->nu_valor;
    }

    /**
     * @return decimal
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @param \G2\Entity\decimal $nu_vencimento
     * @deprecated
     */
    public function setNuVencimento($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @param $nu_vencimento
     */
    public function setNu_vencimento($nu_vencimento) {
        $this->nu_vencimento = $nu_vencimento;
    }

    /**
     * @return \G2\Entity\decimal
     * @deprecated
     */
    public function getNuVencimento()
    {
        return $this->nu_vencimento;
    }

    /**
     * @return decimal
     */
    public function getNu_vencimento() {
        return $this->nu_vencimento;
    }

    /**
     * @param int $nu_verificacao
     * @deprecated
     */
    public function setNuVerificacao($nu_verificacao)
    {
        $this->nu_verificacao = $nu_verificacao;
    }

    /**
     * @param $nu_verificacao
     */
    public function setNu_verificacao($nu_verificacao) {
        $this->nu_verificacao = $nu_verificacao;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuVerificacao()
    {
        return $this->nu_verificacao;
    }

    /**
     * @return int
     */
    public function getNu_verificacao() {
        return $this->nu_verificacao;
    }

    /**
     * @param string $st_agencia
     * @deprecated
     */
    public function setStAgencia($st_agencia)
    {
        $this->st_agencia = $st_agencia;
    }

    /**
     * @param $st_agencia
     */
    public function setSt_agencia($st_agencia) {
        $this->st_agencia = $st_agencia;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStAgencia()
    {
        return $this->st_agencia;
    }

    /**
     * @return string
     */
    public function getSt_agencia() {
        return $this->st_agencia;
    }

    /**
     * @param \G2\Entity\Banco $st_banco
     * @deprecated
     */
    public function setStBanco($st_banco)
    {
        $this->st_banco = $st_banco;
    }

    /**
     * @param $st_banco
     */
    public function setSt_banco($st_banco) {
        $this->st_banco = $st_banco;
    }

    /**
     * @return \G2\Entity\Banco
     * @deprecated
     */
    public function getStBanco()
    {
        return $this->st_banco;
    }

    /**
     * @return Banco
     */
    public function getSt_banco() {
        return $this->st_banco;
    }

    /**
     * @param string $st_codconta
     * @deprecated
     */
    public function setStCodconta($st_codconta)
    {
        $this->st_codconta = $st_codconta;
    }

    /**
     * @param $st_codconta
     */
    public function setSt_codconta($st_codconta) {
        $this->st_codconta = $st_codconta;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStCodconta()
    {
        return $this->st_codconta;
    }

    /**
     * @return string
     */
    public function getSt_codconta() {
        return $this->st_codconta;
    }

    /**
     * @param string $st_coddocumento
     * @deprecated
     */
    public function setStCoddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @param $st_coddocumento
     */
    public function setSt_coddocumento($st_coddocumento){
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStCoddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @return string
     */
    public function getSt_coddocumento() {
        return $this->st_coddocumento;
    }

    /**
     * @param string $st_emissor
     * @deprecated
     */
    public function setStEmissor($st_emissor)
    {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @param $st_emissor
     */
    public function setSt_emissor($st_emissor) {
        $this->st_emissor = $st_emissor;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStEmissor()
    {
        return $this->st_emissor;
    }

    /**
     * @return string
     */
    public function getSt_emissor() {
        return $this->st_emissor;
    }

    /**
     * @param string $st_nossonumero
     * @deprecated
     */
    public function setStNossonumero($st_nossonumero)
    {
        $this->st_nossonumero = $st_nossonumero;
    }

    /**
     * @param $st_nossonumero
     */
    public function setSt_nossonumero($st_nossonumero) {
        $this->st_nossonumero = $st_nossonumero;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStNossonumero()
    {
        return $this->st_nossonumero;
    }

    /**
     * @return string
     */
    public function getSt_nossonumero() {
        return $this->st_nossonumero;
    }

    /**
     * @param string $st_numcheque
     * @deprecated
     */
    public function setStNumcheque($st_numcheque)
    {
        $this->st_numcheque = $st_numcheque;
    }

    /**
     * @param $st_numcheque
     */
    public function setSt_numcheque($st_numcheque) {
        $this->st_numcheque = $st_numcheque;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStNumcheque()
    {
        return $this->st_numcheque;
    }

    /**
     * @return string
     */
    public function getSt_numcheque() {
        return $this->st_numcheque;
    }

    /**
     * @param string $st_renegociacao
     * @deprecated
     */
    public function setStRenegociacao($st_renegociacao)
    {
        $this->st_renegociacao = $st_renegociacao;
    }

    /**
     * @param $st_renegociacao
     */
    public function setSt_renegociacao($st_renegociacao) {
        $this->st_renegociacao = $st_renegociacao;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStRenegociacao()
    {
        return $this->st_renegociacao;
    }

    /**
     * @return string
     */
    public function getSt_renegociacao() {
        return $this->st_renegociacao;
    }

    /**
     * @param string $st_statuslan
     * @deprecated
     */
    public function setStStatuslan($st_statuslan)
    {
        $this->st_statuslan = $st_statuslan;
    }

    /**
     * @param $st_statuslan
     */
    public function setSt_statuslan($st_statuslan) {
        $this->st_statuslan = $st_statuslan;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStStatuslan()
    {
        return $this->st_statuslan;
    }

    /**
     * @return string
     */
    public function getSt_statuslan() {
        return $this->st_statuslan;
    }

    /**
     * @param boolean $bl_original
     * @deprecated
     */
    public function setBlOriginal($bl_original)
    {
        $this->bl_original = $bl_original;
    }

    /**
     * @param $bl_original
     */
    public function setBl_original($bl_original) {
        $this->bl_original = $bl_original;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlOriginal()
    {
        return $this->bl_original;
    }

    /**
     * @return bool
     */
    public function getBl_original() {
        return $this->bl_original;
    }

    /**
     * @param boolean $bl_baixadofluxus
     * @deprecated
     */
    public function setBlBaixadofluxus($bl_baixadofluxus)
    {
        $this->bl_baixadofluxus = $bl_baixadofluxus;
    }

    /**
     * @param $bl_baixadofluxus
     */
    public function setBl_baixadofluxus($bl_baixadofluxus) {
        $this->bl_baixadofluxus = $bl_baixadofluxus;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlBaixadofluxus()
    {
        return $this->bl_baixadofluxus;
    }

    /**
     * @return bool
     */
    public function getBl_baixadofluxus() {
        return $this->bl_baixadofluxus;
    }

    /**
     * @return string
     */
    public function getSt_urlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param string $st_urlboleto
     */
    public function setSt_Urlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
    }

    /**
     * @return string
     */
    public function getSt_codigobarras()
    {
        return $this->st_codigobarras;
    }

    /**
     * @param string $st_codigobarras
     */
    public function setSt_codigobarras($st_codigobarras)
    {
        $this->st_codigobarras = $st_codigobarras;
    }

    /**
     * @return date
     */
    public function getDt_vencimentobackup()
    {
        return $this->dt_vencimentobackup;
    }

    /**
     * @param date $dt_vencimentobackup
     */
    public function setDt_vencimentobackup($dt_vencimentobackup)
    {
        $this->dt_vencimentobackup = $dt_vencimentobackup;
    }

    /**
     * @return string
     */
    public function getSt_idtransacaoexterna()
    {
        return $this->st_idtransacaoexterna;
    }

    /**
     * @param string $st_idtransacaoexterna
     */
    public function setSt_idtransacaoexterna($st_idtransacaoexterna)
    {
        $this->st_idtransacaoexterna = $st_idtransacaoexterna;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }



}

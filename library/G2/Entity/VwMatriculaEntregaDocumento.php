<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculaentregadocumento")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwMatriculaEntregaDocumento {

    /**
     * @var integer $id_documentos
     * @Column(name="id_documentos", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_documentos;
    /**
     * @var integer $id_tipodocumento
     * @Column(name="id_tipodocumento", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_tipodocumento;
    /**
     * @var integer $id_documentosutilizacao
     * @Column(name="id_documentosutilizacao", type="integer", nullable=false, length=4)
     */
    private $id_documentosutilizacao;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=false, length=4)
     */
    private $id_contrato;
    /**
     * @var integer $id_contratoresponsavel
     * @Column(name="id_contratoresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_contratoresponsavel;
    /**
     * @var integer $id_entregadocumentos
     * @Column(name="id_entregadocumentos", type="integer", nullable=true, length=4)
     */
    private $id_entregadocumentos;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=255)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_documentos
     * @Column(name="st_documentos", type="string", nullable=false, length=510)
     */
    private $st_documentos;


    /**
     * @return integer id_documentos
     */
    public function getId_documentos() {
        return $this->id_documentos;
    }

    /**
     * @param id_documentos
     */
    public function setId_documentos($id_documentos) {
        $this->id_documentos = $id_documentos;
        return $this;
    }

    /**
     * @return integer id_tipodocumento
     */
    public function getId_tipodocumento() {
        return $this->id_tipodocumento;
    }

    /**
     * @param id_tipodocumento
     */
    public function setId_tipodocumento($id_tipodocumento) {
        $this->id_tipodocumento = $id_tipodocumento;
        return $this;
    }

    /**
     * @return integer id_documentosutilizacao
     */
    public function getId_documentosutilizacao() {
        return $this->id_documentosutilizacao;
    }

    /**
     * @param id_documentosutilizacao
     */
    public function setId_documentosutilizacao($id_documentosutilizacao) {
        $this->id_documentosutilizacao = $id_documentosutilizacao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_contrato
     */
    public function getId_contrato() {
        return $this->id_contrato;
    }

    /**
     * @param id_contrato
     */
    public function setId_contrato($id_contrato) {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     * @return integer id_contratoresponsavel
     */
    public function getId_contratoresponsavel() {
        return $this->id_contratoresponsavel;
    }

    /**
     * @param id_contratoresponsavel
     */
    public function setId_contratoresponsavel($id_contratoresponsavel) {
        $this->id_contratoresponsavel = $id_contratoresponsavel;
        return $this;
    }

    /**
     * @return integer id_entregadocumentos
     */
    public function getId_entregadocumentos() {
        return $this->id_entregadocumentos;
    }

    /**
     * @param id_entregadocumentos
     */
    public function setId_entregadocumentos($id_entregadocumentos) {
        $this->id_entregadocumentos = $id_entregadocumentos;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_documentos
     */
    public function getSt_documentos() {
        return $this->st_documentos;
    }

    /**
     * @param st_documentos
     */
    public function setSt_documentos($st_documentos) {
        $this->st_documentos = $st_documentos;
        return $this;
    }



}

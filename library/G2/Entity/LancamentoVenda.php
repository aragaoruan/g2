<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/11/13
 * Time: 11:26
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Entity(repositoryClass="G2\Repository\LancamentoVenda")
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_lancamentovenda")
 */
class LancamentoVenda extends G2Entity {

    /**
     * @var Venda
     * @Id
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda", nullable=false)
     */
    private $id_venda;

    /**
     * @var Lancamento
     * @Id
     * @ManyToOne(targetEntity="Lancamento")
     * @JoinColumn(name="id_lancamento", referencedColumnName="id_lancamento")
     */
    private $id_lancamento;

    /**
     * @var boolean $bl_entrada
     * @Column(name="bl_entrada", type="boolean", nullable=false)
     */
    private $bl_entrada;

    /**
     * @var integer $nu_orderm
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @param boolean $bl_entrada
     */
    public function setBlEntrada($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
    }

    /**
     * @return boolean
     */
    public function getBlEntrada()
    {
        return $this->bl_entrada;
    }

    /**
     * @param \G2\Entity\Lancamento $id_lancamento
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return \G2\Entity\Lancamento
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param \G2\Entity\Venda $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return \G2\Entity\Venda
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $nu_ordem
     */
    public function setNuOrdem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    /**
     * @return int
     */
    public function getNuOrdem()
    {
        return $this->nu_ordem;
    }


}

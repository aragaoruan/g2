<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 12/11/13
 * Time: 16:07
 * 
 * @update: Paulo Silva  - Trocado o nome da tabela que estava errado como "tb_erro"
 */

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_processo")
 */
class Processo {

    /**
     * @var integer $id_processo
     * @id
     * @Column(name="id_processo", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_processo;

    /**
     * @var string $st_processo
     * @Column(name="st_processo", type="string", nullable=false, length=50)
     */
    private $st_processo;

    /**
     * @var string $st_classe
     * @Column(name="st_classe", type="string", nullable=false, length=150)
     */
    private $st_classe;

    /**
     * @param int $id_processo
     */
    public function setIdProcesso($id_processo)
    {
        $this->id_processo = $id_processo;
    }

    /**
     * @return int
     */
    public function getIdProcesso()
    {
        return $this->id_processo;
    }

    /**
     * @param string $st_classe
     */
    public function setStClasse($st_classe)
    {
        $this->st_classe = $st_classe;
    }

    /**
     * @return string
     */
    public function getStClasse()
    {
        return $this->st_classe;
    }

    /**
     * @param string $st_processo
     */
    public function setStProcesso($st_processo)
    {
        $this->st_processo = $st_processo;
    }

    /**
     * @return string
     */
    public function getStProcesso()
    {
        return $this->st_processo;
    }

} 
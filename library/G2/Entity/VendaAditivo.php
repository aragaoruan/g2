<?php

namespace G2\Entity;

use G2\Constante\MeioPagamento;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_vendaaditivo")
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class VendaAditivo{

    /**
     * @Id
     * @var integer $id_vendaaditivo
     * @Column(name="id_vendaaditivo", type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_vendaaditivo;

    /**
     * @var Venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     */
    private $id_venda;

    /**
     * @var Matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var dateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var decimal $nu_valorcursoorigem
     * @Column(name="nu_valorcursoorigem", type="decimal", nullable=false, precision=30, scale=2)
     */
    private $nu_valorcursoorigem;

    /**
     * @var decimal $nu_desconto
     * @Column(name="nu_desconto", type="decimal", nullable=false, precision=30, scale=2)
     */
    private $nu_desconto;

    /**
     * @var decimal $nu_valorapagar
     * @Column(name="nu_valorapagar", type="decimal", nullable=false, precision=30, scale=2)
     */
    private $nu_valorapagar;

    /**
     * @var MeioPagamento
     * @ManyToOne(targetEntity="MeioPagamento")
     * @JoinColumn(name="id_meiopagamento", referencedColumnName="id_meiopagamento")
     */
    private $id_meiopagamento;

    /**
     * @var CampanhaComercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhacomercial;

    /**
     * @var integer $nu_quantidadeparcelas
     * @Column(name="nu_quantidadeparcelas", type="integer", nullable=false)
     */
    private $nu_quantidadeparcelas;

    /**
     * @var decimal $nu_valortransferencia
     * @Column(name="nu_valortransferencia", type="decimal", nullable=false, precision=30, scale=2)
     */
    private $nu_valortransferencia;

    /**
     * @var dateTime $dt_primeiraparcela
     * @Column(name="dt_primeiraparcela", type="datetime", nullable=false, length=8)
     */
    private $dt_primeiraparcela;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_atendente", referencedColumnName="id_usuario")
     */
    private $id_atendente;

    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string", nullable=false)
     */
    private $st_observacao;

    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean", nullable=false)
     */
    private $bl_cancelamento;

    /**
     * @var decimal $nu_valorproporcional
     * @Column(name="nu_valorproporcional", type="decimal", nullable=false, precision=30, scale=2)
     */
    private $nu_valorproporcional;

    /**
     * @return int
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }

    /**
     * @return Venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param Venda $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return dateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return decimal
     */
    public function getNu_valorcursoorigem()
    {
        return $this->nu_valorcursoorigem;
    }

    /**
     * @param decimal $nu_valorcursoorigem
     * @return $this
     */
    public function setNu_valorcursoorigem($nu_valorcursoorigem)
    {
        $this->nu_valorcursoorigem = $nu_valorcursoorigem;
        return $this;
    }


    /**
     * @return decimal
     */
    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param decimal $nu_desconto
     */
    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return decimal
     */
    public function getNu_valorapagar()
    {
        return $this->nu_valorapagar;
    }

    /**
     * @param decimal $nu_valorapagar
     */
    public function setNu_valorapagar($nu_valorapagar)
    {
        $this->nu_valorapagar = $nu_valorapagar;
    }

    /**
     * @return MeioPagamento
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param MeioPagamento $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return CampanhaComercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param CampanhaComercial $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @return int
     */
    public function getNu_quantidadeparcelas()
    {
        return $this->nu_quantidadeparcelas;
    }

    /**
     * @param int $nu_quantidadeparcelas
     */
    public function setNu_quantidadeparcelas($nu_quantidadeparcelas)
    {
        $this->nu_quantidadeparcelas = $nu_quantidadeparcelas;
    }

    /**
     * @return decimal
     */
    public function getNu_valortransferencia()
    {
        return $this->nu_valortransferencia;
    }

    /**
     * @param decimal $nu_valortransferencia
     */
    public function setNu_valortransferencia($nu_valortransferencia)
    {
        $this->nu_valortransferencia = $nu_valortransferencia;
    }

    /**
     * @return dateTime
     */
    public function getDt_primeiraparcela()
    {
        return $this->dt_primeiraparcela;
    }

    /**
     * @param dateTime $dt_primeiraparcela
     */
    public function setDt_primeiraparcela($dt_primeiraparcela)
    {
        $this->dt_primeiraparcela = $dt_primeiraparcela;
    }

    /**
     * @return Usuario
     */
    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @param Usuario $id_atendente
     */
    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param string $st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }



    /**
     * @return decimal
     */
    public function getNu_valorproporcional()
    {
        return $this->nu_valorproporcional;
    }

    /**
     * @param decimal $nu_valorproporcional
     */
    public function setNu_valorproporcional($nu_valorproporcional)
    {
        $this->nu_valorproporcional = $nu_valorproporcional;
    }


}

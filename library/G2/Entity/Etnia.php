<?php

namespace G2\Entity;
use G2\G2Entity;

/** ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_etnia")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class Etnia extends G2Entity {
    /**
     * @id
     * @var integer $id_etnia
     * @Column(name="id_etnia", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_etnia;
    /**
     * @var string $st_etnia
     * @Column(name="st_etnia", type="string", nullable=false, length=50)
     */
    private $st_etnia;

    public function __toString() {
        return strval($this->id_etnia);
    }

    /**
     * @return int
     */
    public function getId_etnia()
    {
        return $this->id_etnia;
    }

    /**
     * @param int $id_etnia
     */
    public function setId_etnia($id_etnia)
    {
        $this->id_etnia = $id_etnia;
    }

    /**
     * @return string
     */
    public function getSt_etnia()
    {
        return $this->st_etnia;
    }

    /**
     * @param string $st_etnia
     */
    public function setSt_etnia($st_etnia)
    {
        $this->st_etnia = $st_etnia;
    }


}
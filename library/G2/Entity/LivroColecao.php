<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_livrocolecao")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class LivroColecao
{


    /**
     *
     * @var integer $id_livrocolecao
     * @Column(name="id_livrocolecao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_livrocolecao;
    
    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=true)
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;
   
    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    
    
    /**
     *
     * @var string $st_livrocolecao
     * @Column(name="st_livrocolecao", type="string", nullable=false, length=200)
     */
    private $st_livrocolecao;
    
    
    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    
    /**
     * @var datetime $dt_cadastro
     * @Column(type="datetime", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;
    
    
	/**
	 * @return the $id_livrocolecao
	 */
	public function getId_livrocolecao() {
		return $this->id_livrocolecao;
	}

	/**
	 * @return the $id_entidadecadastro
	 */
	public function getId_entidadecadastro() {
		return $this->id_entidadecadastro;
	}

	/**
	 * @return the $id_usuariocadastro
	 */
	public function getId_usuariocadastro() {
		return $this->id_usuariocadastro;
	}

	/**
	 * @return the $st_livrocolecao
	 */
	public function getSt_livrocolecao() {
		return $this->st_livrocolecao;
	}

	/**
	 * @return the $bl_ativo
	 */
	public function getBl_ativo() {
		return $this->bl_ativo;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @param number $id_livrocolecao
	 */
	public function setId_livrocolecao($id_livrocolecao) {
		$this->id_livrocolecao = $id_livrocolecao;
		return $this;
	}

	/**
	 * @param number $id_entidadecadastro
	 */
	public function setId_entidadecadastro($id_entidadecadastro) {
		$this->id_entidadecadastro = $id_entidadecadastro;
		return $this;
	}

	/**
	 * @param number $id_usuariocadastro
	 */
	public function setId_usuariocadastro($id_usuariocadastro) {
		$this->id_usuariocadastro = $id_usuariocadastro;
		return $this;
	}

	/**
	 * @param string $st_livrocolecao
	 */
	public function setSt_livrocolecao($st_livrocolecao) {
		$this->st_livrocolecao = $st_livrocolecao;
		return $this;
	}

	/**
	 * @param boolean $bl_ativo
	 */
	public function setBl_ativo($bl_ativo) {
		$this->bl_ativo = $bl_ativo;
		return $this;
	}

	/**
	 * @param \G2\Entity\datetime $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
		return $this;
	}

    


}
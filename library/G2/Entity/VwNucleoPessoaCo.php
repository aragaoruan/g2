<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_nucleopessoaco")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwNucleoPessoaCo
{

    /**
     *
     * @var integer $id_nucleopessoaco
     * @Column(name="id_nucleopessoaco", type="integer", nullable=false)
     * @Id
     */
    private $id_nucleopessoaco;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;
    /**
     * @var integer $id_textonotificacao
     * @Column(name="id_textonotificacao", type="integer", nullable=true)
     */
    private $id_textonotificacao;
    /**
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=false)
     */
    private $id_assuntoco;
    /**
     * @var integer $id_assuntocopai
     * @Column(name="id_assuntocopai", type="integer", nullable=true)
     */
    private $id_assuntocopai;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;
    /**
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=false)
     */
    private $id_funcao;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false)
     */
    private $id_nucleoco;
    /**
     * @var string $st_nucleoco
     * @Column(name="st_nucleoco", type="string", nullable=false, length=150)
     */
    private $st_nucleoco;
    /**
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;
    /**
     * @var string $st_assuntocopai
     * @Column(name="st_assuntocopai", type="string", nullable=true, length=200)
     */
    private $st_assuntocopai;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=true, length=100)
     */
    private $st_funcao;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @Column(name="bl_prioritario",type="boolean",nullable=false)
     * @var boolean $bl_prioritario
     */
    private $bl_prioritario;

    /**
     * @Column(name="bl_todos",type="boolean",nullable=false)
     * @var boolean $bl_todos
     */
    private $bl_todos;

    /**
     * @var integer $id_assuntocooriginal
     * @Column(name="id_assuntocooriginal", type="integer", nullable=true)
     */
    private $id_assuntocooriginal;

    public function setBl_prioritario($bl_prioritario)
    {
        $this->bl_prioritario = $bl_prioritario;
    }

    public function getBl_prioritario()
    {
        return $this->bl_prioritario;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function setId_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
    }

    public function getId_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    public function setId_nucleopessoaco($id_nucleopessoaco)
    {
        $this->id_nucleopessoaco = $id_nucleopessoaco;
    }

    public function getId_nucleopessoaco()
    {
        return $this->id_nucleopessoaco;
    }

    public function setId_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
    }

    public function getId_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function setSt_assuntocopai($st_assuntocopai)
    {
        $this->st_assuntocopai = $st_assuntocopai;
    }

    public function getSt_assuntocopai()
    {
        return $this->st_assuntocopai;
    }

    public function setSt_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
    }

    public function getSt_funcao()
    {
        return $this->st_funcao;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
    }

    public function getSt_nucleoco()
    {
        return $this->st_nucleoco;
    }

    /**
     * @return boolean
     */
    public function getbl_todos()
    {
        return $this->bl_todos;
    }

    /**
     * @param boolean $bl_todos
     */
    public function setbl_todos($bl_todos)
    {
        $this->bl_todos = $bl_todos;
    }

    /**
     * @return int
     */
    public function getid_assuntocooriginal()
    {
        return $this->id_assuntocooriginal;
    }

    /**
     * @param int $id_assuntocooriginal
     */
    public function setid_assuntocooriginal($id_assuntocooriginal)
    {
        $this->id_assuntoco = $id_assuntocooriginal;
    }

}
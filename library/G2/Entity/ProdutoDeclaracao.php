<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtotextosistema")
 * @Entity
 * @author Neemias Santos neemias.santos@unyleya.com.br>
 */
class ProdutoDeclaracao
{


    /**
     *
     * @var integer $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     *
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @Id
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @return the $id_textosistema
     */
    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return the $id_produto
     */
    public function getId_produto() {
        return $this->id_produto;
    }



    /**
     * @param number $id_textosistema
     */
    public function setId_livro($id_textosistema) {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @param number $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param number $id_produto
     */
    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }


}
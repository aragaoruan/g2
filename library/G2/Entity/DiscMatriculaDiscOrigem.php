<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_discmatriculadiscorigem")
 * @Entity
 * @EntityLog
 */

class DiscMatriculaDiscOrigem
{
    /**
     * @var integer $id_discmatriculadiscorigem
     * @Column(name="id_discmatriculadiscorigem", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_discmatriculadiscorigem;

    /**
     * @var string $dt_cadastro
     * @Column(name="dt_cadastro", type="string", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var MatriculaDisciplina $id_matriculadisciplina
     * @ManyToOne(targetEntity="MatriculaDisciplina")
     * @JoinColumn(name="id_matriculadisciplina", referencedColumnName="id_matriculadisciplina")
     */
    private $id_matriculadisciplina;

    /**
     * @var DisciplinaOrigem $id_disciplinaorigem
     * @ManyToOne(targetEntity="DisciplinaOrigem")
     * @JoinColumn(name="id_disciplinaorigem", referencedColumnName="id_disciplinaorigem")
     */
    private $id_disciplinaorigem;

    /**
     * @return int
     */
    public function getId_discmatriculadiscorigem()
    {
        return $this->id_discmatriculadiscorigem;
    }

    /**
     * @param int $id_discmatriculadiscorigem
     * @return $this
     */
    public function setId_discmatriculadiscorigem($id_discmatriculadiscorigem)
    {
        $this->id_discmatriculadiscorigem = $id_discmatriculadiscorigem;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param string $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return MatriculaDisciplina
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param MatriculaDisciplina $id_matriculadisciplina
     * @return $this
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return DisciplinaOrigem
     */
    public function getId_disciplinaorigem()
    {
        return $this->id_disciplinaorigem;
    }

    /**
     * @param DisciplinaOrigem $id_disciplinaorigem
     * @return $this
     */
    public function setId_disciplinaorigem($id_disciplinaorigem)
    {
        $this->id_disciplinaorigem = $id_disciplinaorigem;
        return $this;
    }
}

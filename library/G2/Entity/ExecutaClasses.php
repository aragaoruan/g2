<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_executaclasses")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class ExecutaClasses
{

    /**
     * @var integer $id_campo
     * @Column(name="id_campo",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campo;

    /**
     * @var integer $bl_executa
     * @Column(name="bl_executa", type="integer", nullable=true, length=4)
     */
    private $bl_executa;

    public function getId_campo()
    {
        return $this->id_campo;
    }

    public function getBl_executa()
    {
        return $this->bl_executa;
    }

    public function setId_campo($id_campo)
    {
        $this->id_campo = $id_campo;
        return $this;
    }

    public function setBl_executa($bl_executa)
    {
        $this->bl_executa = $bl_executa;
        return $this;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 12/11/13
 * Time: 11:36
 */

namespace G2\Entity;

use \Doctrine\Mapping as ORM;
use G2\G2Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_tipolancamento")
 */
class TipoLancamento extends G2Entity {

    /**
    * @var integer $id_tipolancamento
    * @Id
    * @Column(name="id_tipolancamento", type="integer", nullable=false)
    * @GeneratedValue(strategy="IDENTITY")
    */
    private $id_tipolancamento;

    /**
     * @var string $st_tipolancamento
     * @Column(name="st_tipolancamento", type="string", nullable=false)
     */
    private $st_tipolancamento;


    /**
     * @return int
     */
    public function getid_tipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @param int $id_tipolancamento
     */
    public function setid_tipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipolancamento()
    {
        return $this->st_tipolancamento;
    }

    /**
     * @param string $st_tipolancamento
     */
    public function setst_tipolancamento($st_tipolancamento)
    {
        $this->st_tipolancamento = $st_tipolancamento;
        return $this;

    }


    /**
     * @deprecated
     * @param int $id_tipolancamento
     */
    public function setIdTipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getIdTipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @deprecated
     * @param string $st_tipolancamento
     */
    public function setStTipolancamento($st_tipolancamento)
    {
        $this->st_tipolancamento = $st_tipolancamento;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getStTipolancamento()
    {
        return $this->st_tipolancamento;
    }


} 
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_projetopedagogico")
 * @Entity(repositoryClass="\G2\Repository\ProjetoPedagogico")
 * @EntityLog
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-29-01
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-04-02
 * @update Denise Xavier <denise.xavier@unyleya.com.br>
 */
use G2\G2Entity;

class ProjetoPedagogico extends G2Entity
{

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false, name="id_projetopedagogico")
     * @ManyToOne(targetEntity="ProjetoEntidade", fetch="EXTRA_LAZY")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_descricao
     * @Column(type="text", nullable=true, name="st_descricao")
     */
    private $st_descricao;

    /**
     * @var integer $nu_idademinimainiciar
     * @Column(type="integer", nullable=true, name="nu_idademinimainiciar")
     */
    private $nu_idademinimainiciar;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo")
     */
    private $bl_ativo;

    /**
     * Identifica qual Projeto Pedagógico pode acessar o Portal
     * @var boolean $bl_novoportal
     * @Column(type="boolean", nullable=false, name="bl_novoportal")
     */
    private $bl_novoportal;

    /**
     * @var integer $nu_tempominimoconcluir
     * @Column(type="integer", nullable=true, name="nu_tempominimoconcluir")
     */
    private $nu_tempominimoconcluir;

    /**
     * @var integer $nu_idademinimaconcluir
     * @Column(type="integer", nullable=true, name="nu_idademinimaconcluir")
     */
    private $nu_idademinimaconcluir;

    /**
     * @var integer $nu_tempomaximoconcluir
     * @Column(type="integer", nullable=true, name="nu_tempomaximoconcluir")
     */
    private $nu_tempomaximoconcluir;

    /**
     * @var integer $nu_tempopreviconcluir
     * @Column(type="integer", nullable=true, name="nu_tempopreviconcluir")
     */
    private $nu_tempopreviconcluir;

    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", length=255, nullable=false, name="st_projetopedagogico")
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_tituloexibicao
     * @Column(type="string", length=255, nullable=true, name="st_tituloexibicao")
     */
    private $st_tituloexibicao;

    /**
     * @var boolean $bl_autoalocaraluno
     * @Column(type="boolean", nullable=false, name="bl_autoalocaraluno")
     */
    private $bl_autoalocaraluno;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao", nullable=false)
     */
    private $id_situacao;

    /**
     * @var Trilha $id_trilha
     * @ManyToOne(targetEntity="Trilha")
     * @JoinColumn(name="id_trilha", referencedColumnName="id_trilha", nullable=false)
     */
    private $id_trilha;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogicoorigem", referencedColumnName="id_projetopedagogico", nullable=true)
     */
    private $id_projetopedagogicoorigem;

    /**
     * @var string $st_nomeversao
     * @Column(type="string", length=255, nullable=true, name="st_nomeversao")
     */
    private $st_nomeversao;

    /**
     * @var integer $id_tipoextracurricular
     * @Column(type="integer", nullable=true, name="id_tipoextracurricular")
     */
    private $id_tipoextracurricular;

    /**
     * @var integer $nu_notamaxima
     * @Column(type="integer",nullable=true, name="nu_notamaxima")
     */
    private $nu_notamaxima;

    /**
     * @var integer $nu_percentualaprovacao
     * @Column(type="integer",nullable=true, name="nu_percentualaprovacao")
     */
    private $nu_percentualaprovacao;

    /**
     * @var integer $nu_valorprovafinal
     * @Column(type="integer", nullable=true, name="nu_valorprovafinal")
     */
    private $nu_valorprovafinal;

    /**
     * @var integer $id_entidadecadastro
     * @Column(type="integer", nullable=false, name="id_entidadecadastro")
     */
    private $id_entidadecadastro;

    /**
     * @var boolean $bl_provafinal
     * @Column(type="boolean", nullable=false, name="bl_provafinal")
     */
    private $bl_provafinal;

    /**
     * @var boolean $bl_salasemprovafinal
     * @Column(type="boolean", nullable=false, name="bl_salasemprovafinal")
     */
    private $bl_salasemprovafinal;

    /**
     * @var string $st_objetivo
     * @Column(type="text", nullable=true, name="st_objetivo")
     */
    private $st_objetivo;

    /**
     * @var string $st_estruturacurricular
     * @Column(type="text", nullable=true, name="st_estruturacurricular")
     */
    private $st_estruturacurricular;

    /**
     * @var string $st_metodologiaavaliacao
     * @Column(type="text", nullable=true, name="st_metodologiaavaliacao")
     */
    private $st_metodologiaavaliacao;

    /**
     * @var string $st_certificacao
     * @Column(type="text", nullable=true, name="st_certificacao")
     */
    private $st_certificacao;

    /**
     * @var string $st_apelido
     * @Column(type="text", nullable=true, name="st_apelido")
     */
    private $st_apelido;
    /**
     * @var string $st_valorapresentacao
     * @Column(type="text", nullable=true, name="st_valorapresentacao")
     */
    private $st_valorapresentacao;

    /**
     * @var integer $nu_cargahoraria
     * @Column(type="integer", nullable=false, name="nu_cargahoraria")
     */
    private $nu_cargahoraria;

    /**
     * @var string $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=false, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_disciplinaextracurricular
     * @Column(type="boolean", nullable=false, name="bl_disciplinaextracurricular")
     */
    private $bl_disciplinaextracurricular;

    /**
     * @var integer $id_usuario
     * @Column(type="integer", nullable=false, name="id_usuario")
     */
    private $id_usuario;

    /**
     * @var string $st_conteudoprogramatico
     * @Column(type="text", nullable=false, name="st_conteudoprogramatico")
     */
    private $st_conteudoprogramatico;

    /**
     * @var MedidaTempoConclusao $id_medidatempoconclusao
     * @ManyToOne(targetEntity="MedidaTempoConclusao")
     * @JoinColumn(name="id_medidatempoconclusao", referencedColumnName="id_medidatempoconclusao", nullable=true)
     */
    private $id_medidatempoconclusao;

    /**
     * @var ContratoRegra $id_contratoregra
     * @ManyToOne(targetEntity="ContratoRegra")
     * @JoinColumn(name="id_contratoregra", referencedColumnName="id_contratoregra", nullable=true)
     */
    private $id_contratoregra;

    /**
     * @var LivroRegistroEntidade $id_livroregistroentidade
     * @ManyToOne(targetEntity="LivroRegistroEntidade")
     * @JoinColumn(name="id_livroregistroentidade", referencedColumnName="id_livroregistroentidade", nullable=true)
     */
    private $id_livroregistroentidade;

    /**
     * @var integer $nu_prazoagendamento
     * @Column(type="integer", nullable=true, name="nu_prazoagendamento")
     */
    private $nu_prazoagendamento;

    /**
     * @var string $st_perfilprojeto
     * @Column(type="text", nullable=true, name="st_perfilprojeto")
     */
    private $st_perfilprojeto;

    /**
     * @var string $st_coordenacao
     * @Column(type="text", nullable=true, name="st_coordenacao")
     */
    private $st_coordenacao;

    /**
     * @var string $st_horario
     * @Column(type="text", nullable=true, name="st_horario")
     */
    private $st_horario;

    /**
     * @var string $st_publicoalvo
     * @Column(type="text", nullable=true, name="st_publicoalvo")
     */
    private $st_publicoalvo;

    /**
     * @var string $st_mercadotrabalho
     * @Column(type="text", nullable=true, name="st_mercadotrabalho")
     */
    private $st_mercadotrabalho;

    /**
     * @var boolean $bl_turma
     * @Column(type="boolean", nullable=false, name="bl_turma")
     */
    private $bl_turma;

    /**
     * @var boolean $bl_gradepronta
     * @Column(type="boolean", nullable=true, name="bl_gradepronta")
     */
    private $bl_gradepronta;

    /**
     * @var string $dt_criagrade
     * @Column(type="datetime2", nullable=true, name="dt_criagrade")
     */
    private $dt_criagrade;

    /**
     * @var integer $id_usuariograde
     * @Column(type="integer", nullable=true, name="id_usuariograde")
     */
    private $id_usuariograde;

    /**
     *
     * @var integer $id_usuariograde
     * @Column(type="integer", nullable=true, name="id_horarioaula")
     */
    private $id_horarioaula;

    /**
     * @var boolean $bl_disciplinacomplementar
     * @Column(type="boolean", nullable=true, name="bl_disciplinacomplementar", options={"default": 0})
     */
    private $bl_disciplinacomplementar;

    /**
     * @var boolean $bl_enviaremail
     * @Column(type="boolean", nullable=true, name="bl_enviaremail", options={"default": 1})
     */
    private $bl_enviaremail;

    /**
     * @var Upload $id_anexoementa
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_anexoementa", referencedColumnName="id_upload", nullable=false)
     */
    private $id_anexoementa;

    /**
     * @var integer $nu_percentualaprovacaovariavel
     * @Column(type="integer",nullable=true, name="nu_percentualaprovacaovariavel")
     */
    private $nu_percentualaprovacaovariavel;

    /**
     * @var boolean $bl_renovar
     * @Column(type="boolean", nullable=false, name="bl_renovar", options={"default": 1})
     */
    private $bl_renovar;

    /**
     * @var boolean $bl_possuiprova
     * @Column(type="boolean", nullable=false, name="bl_possuiprova", options={"default": 1})
     */
    private $bl_possuiprova;

    /**
     * @var string $st_boasvindascurso
     * @Column(name="st_boasvindascurso", type="string", nullable=true, length=120)
     */
    private $st_boasvindascurso;

    /**
     * @var string $st_imagem
     * @Column(type="text", nullable=true, name="st_imagem")
     */
    private $st_imagem;

    /**
     * @var integer $nu_semestre
     * @Column(type="integer", nullable=true, name="nu_semestre")
     */
    private $nu_semestre;

    /**
     * @var string $st_reconhecimento
     * @Column(type="text", nullable=true, name="st_reconhecimento")
     */
    private $st_reconhecimento;

    /**
     * @var string $st_codprovaintegracao
     * @Column(type="string", length=30, nullable=true, name="st_codprovaintegracao")
     */
    private $st_codprovaintegracao;

    /**
     * @var string $st_provaintegracao
     * @Column(type="string", length=255, nullable=true, name="st_provaintegracao")
     */
    private $st_provaintegracao;

    /**
     * @var integer $nu_horasatividades
     * @Column(type="integer", nullable=true, name="nu_horasatividades")
     */
    private $nu_horasatividades;


    /**
     * Indica se será usado um atendimento virtual como o ChatBot
     * @var boolean
     * @Column(type="integer", nullable=true, name="bl_atendimentovirtual")
     */
    private $bl_atendimentovirtual;

    /**
     * @var integer $id_areaconhecimentomec
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimentomec", referencedColumnName="id_areaconhecimento", nullable=true)
     */
    private $id_areaconhecimentomec;

    /**
     * @return int
     */
    public function getBl_atendimentovirtual()
    {
        return $this->bl_atendimentovirtual;
    }

    /**
     * @param mixed $bl_atendimentovirtual
     */
    public function setBl_atendimentovirtual($bl_atendimentovirtual)
    {
        $this->bl_atendimentovirtual = $bl_atendimentovirtual;
        return $this;

    }

    /**
     * @var GrauAcademico $id_grauacademico
     * @ManyToOne(targetEntity="GrauAcademico")
     * @JoinColumn(name="id_grauacademico", referencedColumnName="id_grauacademico", nullable=true)
     */
    private $id_grauacademico;

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return $this
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_idademinimainiciar()
    {
        return $this->nu_idademinimainiciar;
    }

    /**
     * @param int $nu_idademinimainiciar
     * @return $this
     */
    public function setNu_idademinimainiciar($nu_idademinimainiciar)
    {
        $this->nu_idademinimainiciar = $nu_idademinimainiciar;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_novoportal()
    {
        return $this->bl_novoportal;
    }

    /**
     * @param bool $bl_novoportal
     * @return $this
     */
    public function setBl_novoportal($bl_novoportal)
    {
        $this->bl_novoportal = $bl_novoportal;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tempominimoconcluir()
    {
        return $this->nu_tempominimoconcluir;
    }

    /**
     * @param int $nu_tempominimoconcluir
     * @return $this
     */
    public function setNu_tempominimoconcluir($nu_tempominimoconcluir)
    {
        $this->nu_tempominimoconcluir = $nu_tempominimoconcluir;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_idademinimaconcluir()
    {
        return $this->nu_idademinimaconcluir;
    }

    /**
     * @param int $nu_idademinimaconcluir
     * @return $this
     */
    public function setNu_idademinimaconcluir($nu_idademinimaconcluir)
    {
        $this->nu_idademinimaconcluir = $nu_idademinimaconcluir;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tempomaximoconcluir()
    {
        return $this->nu_tempomaximoconcluir;
    }

    /**
     * @param int $nu_tempomaximoconcluir
     * @return $this
     */
    public function setNu_tempomaximoconcluir($nu_tempomaximoconcluir)
    {
        $this->nu_tempomaximoconcluir = $nu_tempomaximoconcluir;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tempopreviconcluir()
    {
        return $this->nu_tempopreviconcluir;
    }

    /**
     * @param int $nu_tempopreviconcluir
     * @return $this
     */
    public function setNu_tempopreviconcluir($nu_tempopreviconcluir)
    {
        $this->nu_tempopreviconcluir = $nu_tempopreviconcluir;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param string $st_tituloexibicao
     * @return $this
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_autoalocaraluno()
    {
        return $this->bl_autoalocaraluno;
    }

    /**
     * @param bool $bl_autoalocaraluno
     * @return $this
     */
    public function setBl_autoalocaraluno($bl_autoalocaraluno)
    {
        $this->bl_autoalocaraluno = $bl_autoalocaraluno;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return Trilha
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @param Trilha $id_trilha
     * @return $this
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    /**
     * @return ProjetoPedagogico
     */
    public function getId_projetopedagogicoorigem()
    {
        return $this->id_projetopedagogicoorigem;
    }

    /**
     * @param ProjetoPedagogico $id_projetopedagogicoorigem
     * @return $this
     */
    public function setId_projetopedagogicoorigem($id_projetopedagogicoorigem)
    {
        $this->id_projetopedagogicoorigem = $id_projetopedagogicoorigem;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeversao()
    {
        return $this->st_nomeversao;
    }

    /**
     * @param string $st_nomeversao
     * @return $this
     */
    public function setSt_nomeversao($st_nomeversao)
    {
        $this->st_nomeversao = $st_nomeversao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoextracurricular()
    {
        return $this->id_tipoextracurricular;
    }

    /**
     * @param int $id_tipoextracurricular
     * @return $this
     */
    public function setId_tipoextracurricular($id_tipoextracurricular)
    {
        $this->id_tipoextracurricular = $id_tipoextracurricular;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_notamaxima()
    {
        return $this->nu_notamaxima;
    }

    /**
     * @param int $nu_notamaxima
     * @return $this
     */
    public function setNu_notamaxima($nu_notamaxima)
    {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_percentualaprovacao()
    {
        return $this->nu_percentualaprovacao;
    }

    /**
     * @param int $nu_percentualaprovacao
     * @return $this
     */
    public function setNu_percentualaprovacao($nu_percentualaprovacao)
    {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_valorprovafinal()
    {
        return $this->nu_valorprovafinal;
    }

    /**
     * @param int $nu_valorprovafinal
     * @return $this
     */
    public function setNu_valorprovafinal($nu_valorprovafinal)
    {
        $this->nu_valorprovafinal = $nu_valorprovafinal;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_provafinal()
    {
        return $this->bl_provafinal;
    }

    /**
     * @param bool $bl_provafinal
     * @return $this
     */
    public function setBl_provafinal($bl_provafinal)
    {
        $this->bl_provafinal = $bl_provafinal;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_salasemprovafinal()
    {
        return $this->bl_salasemprovafinal;
    }

    /**
     * @param bool $bl_salasemprovafinal
     * @return $this
     */
    public function setBl_salasemprovafinal($bl_salasemprovafinal)
    {
        $this->bl_salasemprovafinal = $bl_salasemprovafinal;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_objetivo()
    {
        return $this->st_objetivo;
    }

    /**
     * @param string $st_objetivo
     * @return $this
     */
    public function setSt_objetivo($st_objetivo)
    {
        $this->st_objetivo = $st_objetivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_estruturacurricular()
    {
        return $this->st_estruturacurricular;
    }

    /**
     * @param string $st_estruturacurricular
     * @return $this
     */
    public function setSt_estruturacurricular($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_metodologiaavaliacao()
    {
        return $this->st_metodologiaavaliacao;
    }

    /**
     * @param string $st_metodologiaavaliacao
     * @return $this
     */
    public function setSt_metodologiaavaliacao($st_metodologiaavaliacao)
    {
        $this->st_metodologiaavaliacao = $st_metodologiaavaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_certificacao()
    {
        return $this->st_certificacao;
    }

    /**
     * @param string $st_certificacao
     * @return $this
     */
    public function setSt_certificacao($st_certificacao)
    {
        $this->st_certificacao = $st_certificacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_apelido()
    {
        return $this->st_apelido;
    }

    /**
     * @param string $st_apelido
     * @return $this
     */
    public function setSt_apelido($st_apelido)
    {
        $this->st_apelido = $st_apelido;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_valorapresentacao()
    {
        return $this->st_valorapresentacao;
    }

    /**
     * @param string $st_valorapresentacao
     * @return $this
     */
    public function setSt_valorapresentacao($st_valorapresentacao)
    {
        $this->st_valorapresentacao = $st_valorapresentacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param string $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_disciplinaextracurricular()
    {
        return $this->bl_disciplinaextracurricular;
    }

    /**
     * @param bool $bl_disciplinaextracurricular
     * @return $this
     */
    public function setBl_disciplinaextracurricular($bl_disciplinaextracurricular)
    {
        $this->bl_disciplinaextracurricular = $bl_disciplinaextracurricular;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_conteudoprogramatico()
    {
        return $this->st_conteudoprogramatico;
    }

    /**
     * @param string $st_conteudoprogramatico
     * @return $this
     */
    public function setSt_conteudoprogramatico($st_conteudoprogramatico)
    {
        $this->st_conteudoprogramatico = $st_conteudoprogramatico;
        return $this;
    }

    /**
     * @return MedidaTempoConclusao
     */
    public function getId_medidatempoconclusao()
    {
        return $this->id_medidatempoconclusao;
    }

    /**
     * @param MedidaTempoConclusao $id_medidatempoconclusao
     * @return $this
     */
    public function setId_medidatempoconclusao($id_medidatempoconclusao)
    {
        $this->id_medidatempoconclusao = $id_medidatempoconclusao;
        return $this;
    }

    /**
     * @return ContratoRegra
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param ContratoRegra $id_contratoregra
     * @return $this
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }

    /**
     * @return LivroRegistroEntidade
     */
    public function getId_livroregistroentidade()
    {
        return $this->id_livroregistroentidade;
    }

    /**
     * @param LivroRegistroEntidade $id_livroregistroentidade
     * @return $this
     */
    public function setId_livroregistroentidade($id_livroregistroentidade)
    {
        $this->id_livroregistroentidade = $id_livroregistroentidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_prazoagendamento()
    {
        return $this->nu_prazoagendamento;
    }

    /**
     * @param int $nu_prazoagendamento
     * @return $this
     */
    public function setNu_prazoagendamento($nu_prazoagendamento)
    {
        $this->nu_prazoagendamento = $nu_prazoagendamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_perfilprojeto()
    {
        return $this->st_perfilprojeto;
    }

    /**
     * @param string $st_perfilprojeto
     * @return $this
     */
    public function setSt_perfilprojeto($st_perfilprojeto)
    {
        $this->st_perfilprojeto = $st_perfilprojeto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_coordenacao()
    {
        return $this->st_coordenacao;
    }

    /**
     * @param string $st_coordenacao
     * @return $this
     */
    public function setSt_coordenacao($st_coordenacao)
    {
        $this->st_coordenacao = $st_coordenacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_horario()
    {
        return $this->st_horario;
    }

    /**
     * @param string $st_horario
     * @return $this
     */
    public function setSt_horario($st_horario)
    {
        $this->st_horario = $st_horario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_publicoalvo()
    {
        return $this->st_publicoalvo;
    }

    /**
     * @param string $st_publicoalvo
     * @return $this
     */
    public function setSt_publicoalvo($st_publicoalvo)
    {
        $this->st_publicoalvo = $st_publicoalvo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_mercadotrabalho()
    {
        return $this->st_mercadotrabalho;
    }

    /**
     * @param string $st_mercadotrabalho
     * @return $this
     */
    public function setSt_mercadotrabalho($st_mercadotrabalho)
    {
        $this->st_mercadotrabalho = $st_mercadotrabalho;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_turma()
    {
        return $this->bl_turma;
    }

    /**
     * @param bool $bl_turma
     * @return $this
     */
    public function setBl_turma($bl_turma)
    {
        $this->bl_turma = $bl_turma;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_gradepronta()
    {
        return $this->bl_gradepronta;
    }

    /**
     * @param bool $bl_gradepronta
     * @return $this
     */
    public function setBl_gradepronta($bl_gradepronta)
    {
        $this->bl_gradepronta = $bl_gradepronta;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_criagrade()
    {
        return $this->dt_criagrade;
    }

    /**
     * @param string $dt_criagrade
     * @return $this
     */
    public function setDt_criagrade($dt_criagrade)
    {
        $this->dt_criagrade = $dt_criagrade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariograde()
    {
        return $this->id_usuariograde;
    }

    /**
     * @param int $id_usuariograde
     * @return $this
     */
    public function setId_usuariograde($id_usuariograde)
    {
        $this->id_usuariograde = $id_usuariograde;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    /**
     * @param int $id_horarioaula
     * @return $this
     */
    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_disciplinacomplementar()
    {
        return $this->bl_disciplinacomplementar;
    }

    /**
     * @param bool $bl_disciplinacomplementar
     * @return $this
     */
    public function setBl_disciplinacomplementar($bl_disciplinacomplementar)
    {
        $this->bl_disciplinacomplementar = $bl_disciplinacomplementar;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_enviaremail()
    {
        return $this->bl_enviaremail;
    }

    /**
     * @param bool $bl_enviaremail
     * @return $this
     */
    public function setBl_enviaremail($bl_enviaremail)
    {
        $this->bl_enviaremail = $bl_enviaremail;
        return $this;
    }

    /**
     * @return Upload
     */
    public function getId_anexoementa()
    {
        return $this->id_anexoementa;
    }

    /**
     * @param Upload $id_anexoementa
     * @return $this
     */
    public function setId_anexoementa($id_anexoementa)
    {
        $this->id_anexoementa = $id_anexoementa;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_percentualaprovacaovariavel()
    {
        return $this->nu_percentualaprovacaovariavel;
    }

    /**
     * @param int $nu_percentualaprovacaovariavel
     * @return $this
     */
    public function setNu_percentualaprovacaovariavel($nu_percentualaprovacaovariavel)
    {
        $this->nu_percentualaprovacaovariavel = $nu_percentualaprovacaovariavel;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_renovar()
    {
        return $this->bl_renovar;
    }

    /**
     * @param bool $bl_renovar
     * @return $this
     */
    public function setBl_renovar($bl_renovar)
    {
        $this->bl_renovar = $bl_renovar;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_possuiprova()
    {
        return $this->bl_possuiprova;
    }

    /**
     * @param bool $bl_possuiprova
     * @return $this
     */
    public function setBl_possuiprova($bl_possuiprova)
    {
        $this->bl_possuiprova = $bl_possuiprova;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_boasvindascurso()
    {
        return $this->st_boasvindascurso;
    }

    /**
     * @param string $st_boasvindascurso
     * @return $this
     */
    public function setSt_boasvindascurso($st_boasvindascurso)
    {
        $this->st_boasvindascurso = $st_boasvindascurso;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param string $st_imagem
     * @return $this
     */
    public function setSt_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_semestre()
    {
        return $this->nu_semestre;
    }

    /**
     * @param int $nu_semestre
     * @return $this
     */
    public function setNu_semestre($nu_semestre)
    {
        $this->nu_semestre = $nu_semestre;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_reconhecimento()
    {
        return $this->st_reconhecimento;
    }

    /**
     * @param string $st_reconhecimento
     * @return $this
     */
    public function setSt_reconhecimento($st_reconhecimento)
    {
        $this->st_reconhecimento = $st_reconhecimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codprovaintegracao()
    {
        return $this->st_codprovaintegracao;
    }

    /**
     * @return int
     */
    public function getNu_horasatividades()
    {
        return $this->nu_horasatividades;
    }

    /**
     * @param string $st_codprovaintegracao
     */
    public function setSt_codprovaintegracao($st_codprovaintegracao)
    {
        $this->st_codprovaintegracao = $st_codprovaintegracao;
    }

    /**
     * @return string
     */
    public function getSt_provaintegracao()
    {
        return $this->st_provaintegracao;
    }

    /**
     * @param int $nu_horasatividades
     * @return $this
     */
    public function setNu_horasatividades($nu_horasatividades)
    {
        $this->nu_horasatividades = $nu_horasatividades;
        return $this;
    }

    /**
     * @return integer
     * @param string $st_provaintegracao
     */
    public function setSt_provaintegracao($st_provaintegracao)
    {
        $this->st_provaintegracao = $st_provaintegracao;
    }

    /**
     * @return GrauAcademico
     */
    public function getId_grauacademico()
    {
        return $this->id_grauacademico;
    }

    /**
     * @param integer $id_grauacademico
     * @return $this
     */
    public function setId_grauacademico($id_grauacademico)
    {
        $this->id_grauacademico = $id_grauacademico;
        return $this;
    }

    /**
     * @return AreaConhecimento
     */
    public function getId_areaconhecimentomec()
    {
        return $this->id_areaconhecimentomec;
    }

    /**
     * @param AreaConhecimento $id_areaconhecimentomec
     * @return $this;
     */
    public function setId_areaconhecimentomec($id_areaconhecimentomec)
    {
        $this->id_areaconhecimentomec = $id_areaconhecimentomec;
        return $this;
    }

}

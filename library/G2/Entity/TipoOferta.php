<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipooferta")
 * @Entity
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
use \G2\G2Entity;

class TipoOferta extends G2Entity
{

    /**
     *
     * @var integer $id_tipooferta
     * @Column(name="id_tipooferta", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipooferta;

    /**
     * 
     * @var string $st_tipooferta
     * @Column(name="st_tipooferta", type="string", nullable=false, length=200)
     */
    private $st_tipooferta;

    /**
     * @return int
     */
    public function getId_tipooferta()
    {
        return $this->id_tipooferta;
    }

    /**
     * @param integer $id_tipooferta
     * @return $this
     */
    public function setId_tipooferta($id_tipooferta)
    {
        $this->id_tipooferta = $id_tipooferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tipooferta()
    {
        return $this->st_tipooferta;
    }

    /**
     * @param string $st_tipooferta
     * @return $this
     */
    public function setSt_tipooferta($st_tipooferta)
    {
        $this->st_tipooferta = $st_tipooferta;
        return $this;
    }

}

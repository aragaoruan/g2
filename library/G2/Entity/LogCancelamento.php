<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_logcancelamento")
 * @Entity(repositoryClass="\G2\Repository\LogCancelamento")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class LogCancelamento extends G2Entity
{

    /**
     * @Id
     * @var integer $id_logcancelamento
     * @Column(name="id_logcancelamento", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_logcancelamento;
    /**
     * @var mixed $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;
    /**
     * @var Cancelamento $id_cancelamento
     * @ManyToOne(targetEntity="Cancelamento")
     * @JoinColumn(name="id_cancelamento", referencedColumnName="id_cancelamento")
     */
    private $id_cancelamento;
    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Evolucao $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var Evolucao $id_evolucaocancelamento
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucaocancelamento", referencedColumnName="id_evolucao")
     */
    private $id_evolucaocancelamento;
    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var float $nu_valorproporcionalcursado
     * @Column(name="nu_valorproporcionalcursado", type="decimal", nullable=true, length=4)
     */
    private $nu_valorproporcionalcursado;
    /**
     * @var float $nu_valordevolucao
     * @Column(name="nu_valordevolucao", type="decimal", nullable=true, length=4)
     */
    private $nu_valordevolucao;
    /**
     * @var float $nu_percentualmulta
     * @Column(name="nu_percentualmulta", type="decimal", nullable=true, length=4)
     */
    private $nu_percentualmulta;
    /**
     * @var integer $nu_cargahorariautilizada
     * @Column(name="nu_cargahorariautilizada", type="integer", nullable=true, length=4)
     */
    private $nu_cargahorariautilizada;
    /**
     * @var float $nu_valormaterialutilizado
     * @Column(name="nu_valormaterialutilizado", type="decimal", nullable=true, length=4)
     */
    private $nu_valormaterialutilizado;
    /**
     * @var string $st_motivo
     * @Column(name="st_motivo", type="string", nullable=true, length=1000)
     */
    private $st_motivo;

    /**
     * @var boolean $bl_cartacredito
     * @Column(name="bl_cartacredito", type="boolean", nullable=true, length=1)
     */
    private $bl_cartacredito;

    /**
     * @var boolean $bl_ressarcimento
     * @Column(name="bl_ressarcimento", type="boolean", nullable=true, length=1)
     */
    private $bl_ressarcimento;

    /**
     * @return int
     */
    public function getid_logcancelamento()
    {
        return $this->id_logcancelamento;
    }

    /**
     * @param int $id_logcancelamento
     * @return LogCancelamento
     */
    public function setid_logcancelamento($id_logcancelamento)
    {
        $this->id_logcancelamento = $id_logcancelamento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return LogCancelamento
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     * @return $this
     */
    public function setid_matricula(Matricula $id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return Cancelamento
     */
    public function getid_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param Cancelamento $id_cancelamento
     * @return $this
     */
    public function setid_cancelamento(Cancelamento $id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return LogCancelamento
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return LogCancelamento
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucaocancelamento()
    {
        return $this->id_evolucaocancelamento;
    }

    /**
     * @param int $id_evolucaocancelamento
     * @return LogCancelamento
     */
    public function setid_evolucaocancelamento($id_evolucaocancelamento)
    {
        $this->id_evolucaocancelamento = $id_evolucaocancelamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return LogCancelamento
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_valorproporcionalcursado()
    {
        return $this->nu_valorproporcionalcursado;
    }

    /**
     * @param float $nu_valorproporcionalcursado
     * @return LogCancelamento
     */
    public function setnu_valorproporcionalcursado($nu_valorproporcionalcursado)
    {
        $this->nu_valorproporcionalcursado = $nu_valorproporcionalcursado;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_valordevolucao()
    {
        return $this->nu_valordevolucao;
    }

    /**
     * @param float $nu_valordevolucao
     * @return LogCancelamento
     */
    public function setnu_valordevolucao($nu_valordevolucao)
    {
        $this->nu_valordevolucao = $nu_valordevolucao;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_percentualmulta()
    {
        return $this->nu_percentualmulta;
    }

    /**
     * @param float $nu_percentualmulta
     * @return LogCancelamento
     */
    public function setnu_percentualmulta($nu_percentualmulta)
    {
        $this->nu_percentualmulta = $nu_percentualmulta;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_cargahorariautilizada()
    {
        return $this->nu_cargahorariautilizada;
    }

    /**
     * @param int $nu_cargahorariautilizada
     * @return LogCancelamento
     */
    public function setnu_cargahorariautilizada($nu_cargahorariautilizada)
    {
        $this->nu_cargahorariautilizada = $nu_cargahorariautilizada;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_valormaterialutilizado()
    {
        return $this->nu_valormaterialutilizado;
    }

    /**
     * @param float $nu_valormaterialutilizado
     * @return LogCancelamento
     */
    public function setnu_valormaterialutilizado($nu_valormaterialutilizado)
    {
        $this->nu_valormaterialutilizado = $nu_valormaterialutilizado;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_motivo()
    {
        return $this->st_motivo;
    }

    /**
     * @param string $st_motivo
     * @return LogCancelamento
     */
    public function setst_motivo($st_motivo)
    {
        $this->st_motivo = $st_motivo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_cartacredito()
    {
        return $this->bl_cartacredito;
    }

    /**
     * @param bool $bl_cartacredito
     * @return LogCancelamento
     */
    public function setbl_cartacredito($bl_cartacredito)
    {
        $this->bl_cartacredito = $bl_cartacredito;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_ressarcimento()
    {
        return $this->bl_ressarcimento;
    }

    /**
     * @param bool $bl_ressarcimento
     */
    public function setbl_ressarcimento($bl_ressarcimento)
    {
        $this->bl_ressarcimento = $bl_ressarcimento;
    }



}

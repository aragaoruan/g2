<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\Id;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculas_canceladas_vencidas")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwMatriculasCanceladasVencidas
{
    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true, length=3)
     */
    private $dt_termino;
    /**
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicio;
    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2", nullable=false, length=8)
     */
    private $dt_solicitacao;
    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;
    /**
     * @var integer $id_cancelamento
     * @Column(name="id_cancelamento", type="integer", nullable=true, length=4)
     */
    private $id_cancelamento;
    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;
    /**
     * @var integer $nu_diascancelamento
     * @Column(name="nu_diascancelamento", type="integer", nullable=false, length=4)
     */
    private $nu_diascancelamento;
    /**
     * @var datetime $dt_ultimoacesso
     * @Column(name="dt_ultimoacesso", type="datetime", nullable=true, length=8)
     */
    private $dt_ultimoacesso;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return date dt_termino
     */
    public function getDt_termino() {
        return $this->dt_termino;
    }

    /**
     * @return datetime2 dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @return datetime2 dt_solicitacao
     */
    public function getDt_solicitacao() {
        return $this->dt_solicitacao;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @return integer id_cancelamento
     */
    public function getId_cancelamento() {
        return $this->id_cancelamento;
    }

    /**
     * @return integer id_entidadeatendimento
     */
    public function getId_entidadeatendimento() {
        return $this->id_entidadeatendimento;
    }

    /**
     * @return integer nu_diascancelamento
     */
    public function getNu_diascancelamento() {
        return $this->nu_diascancelamento;
    }

    /**
     * @return datetime dt_ultimoacesso
     */
    public function getDt_ultimoacesso() {
        return $this->dt_ultimoacesso;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_mensagempadraoentidade")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEmailEntidadeMensagem
{

    /**
     *
     * @var integer $id_mensagempadrao
     * @column(name="id_mensagempadrao", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_mensagempadrao;

    /**
     *
     * @var string $st_mensagempadrao
     * @column(name="st_mensagempadrao", nullable=false, type="string", length=150)
     */
    private $st_mensagempadrao;

    /**
     *
     * @var integer $id_tipoenvio
     * @column(name="id_tipoenvio", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipoenvio;

    /**
     *
     * @var integer $id_emailconfig
     * @column(name="id_emailconfig", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_emailconfig;

    /**
     *
     * @var string $st_conta
     * @column(name="st_conta", nullable=true, type="string", length=100)
     */
    private $st_conta;

    /**
     * @var TextoCategoria $id_textocategoria
     * @ManyToOne(targetEntity="TextoCategoria")
     * @JoinColumn(name="id_textocategoria", nullable=true, referencedColumnName="id_textocategoria")
     */
    private $id_textocategoria;

    /**
     *
     * @var integer $id_textosistema
     * @column(name="id_textosistema", nullable=true, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_textosistema;

    /**
     *
     * @var string $st_textosistema
     * @column(name="st_textosistema", nullable=true, type="string", length=200)
     */
    private $st_textosistema;

    /**
     *
     * @var integer $id_entidade
     * @column(name="id_entidade", nullable=true, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    public function getId_mensagempadrao ()
    {
        return $this->id_mensagempadrao;
    }

    public function setId_mensagempadrao ($id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
        return $this;
    }

    public function getSt_mensagempadrao ()
    {
        return $this->st_mensagempadrao;
    }

    public function setSt_mensagempadrao ($st_mensagempadrao)
    {
        $this->st_mensagempadrao = $st_mensagempadrao;
        return $this;
    }

    public function getId_tipoenvio ()
    {
        return $this->id_tipoenvio;
    }

    public function setId_tipoenvio ($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    public function getId_emailconfig ()
    {
        return $this->id_emailconfig;
    }

    public function setId_emailconfig ($id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    public function getSt_conta ()
    {
        return $this->st_conta;
    }

    public function setSt_conta ($st_conta)
    {
        $this->st_conta = $st_conta;
        return $this;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria (TextoCategoria $id_textocategoria = null)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema ($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getSt_textosistema ()
    {
        return $this->st_textosistema;
    }

    public function setSt_textosistema ($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/06/2015
 * Time: 17:32
 */

namespace G2\Entity;
/**
 * @Table (name="tb_premioproduto")
 * @Entity
 * @Entity(repositoryClass="\G2\Repository\PremioProduto")
 * @author Helder Silva <helder.silva@unyleya.com.br> 2015-06-19
 */

class PremioProduto {
    /**
     * @Id
     * @var integer $id_premioproduto
     * @Column(name="id_premioproduto", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_premioproduto;
    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    /**
     * @var integer $id_premio
     * @ManyToOne(targetEntity="Premio")
     * @JoinColumn(name="id_premio", nullable=false, referencedColumnName="id_premio")
     */
    private $id_premio;
    /**
     * @var CampanhaComercial $id_campanhacomercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_campanhacomercial;


    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return integer id_premioproduto
     */
    public function getId_premioproduto() {
        return $this->id_premioproduto;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto() {
        return $this->id_produto;
    }

    /**
     * @return integer id_campanhacomercial
     */
    public function getId_campanhacomercial() {
        return $this->id_campanhacomercial;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param id_premioproduto
     */
    public function setId_premioproduto($id_premioproduto) {
        $this->id_premioproduto = $id_premioproduto;
        return $this;
    }
    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }
    /**
     * @param id_produto
     */
    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }
    /**
     * @param id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial) {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }
    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }


    /**
     * @return integer id_premio
     */
    public function getId_premio() {
        return $this->id_premio;
    }

    /**
     * @param id_premio
     */
    public function setId_premio($id_premio) {
        $this->id_premio = $id_premio;
        return $this;
    }



}
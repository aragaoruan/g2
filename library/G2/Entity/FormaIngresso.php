<?php

namespace G2\Entity;
use G2\G2Entity;

/** ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_formaingresso")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class FormaIngresso extends G2Entity
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_formaingresso
     * @Column(name="id_formaingresso", type="integer", nullable=false, length=4)
     */
    private $id_formaingresso;

    /**
     * @var string $st_formaingresso
     * @Column(name="st_formaingresso", type="string", nullable=true, length=50)
     */
    private $st_formaingresso;

    public function __toString() {
        return strval($this->id_formaingresso);
    }

    /**
     * @return int
     */
    public function getId_formaingresso()
    {
        return $this->id_formaingresso;
    }

    /**
     * @param int $id_etnia
     */
    public function setId_formaingresso($id_formaingresso)
    {
        $this->id_formaingresso = $id_formaingresso;
    }

    /**
     * @return string
     */
    public function getSt_formaingresso()
    {
        return $this->st_formaingresso;
    }

    /**
     * @param string $st_etnia
     */
    public function setSt_formaingresso($st_formaingresso)
    {
        $this->st_formaingresso = $st_formaingresso;
    }
    
    
}
<?php


namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_entregamaterial")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.ocm.br>
 */
class EntregaMaterial {

    /**
     *
     * @var integer $id_entregamaterial
     * @Column(name="id_entregamaterial", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entregamaterial;
    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2", nullable=true, length=8)
     */
    private $dt_entrega;
    /**
     * @var datetime $dt_devolucao
     * @Column(name="dt_devolucao", type="datetime", nullable=true, length=8)
     */
    private $dt_devolucao;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false, length=4)
     */
    private $id_itemdematerial;
    /**
     * @var integer $id_pacote
     * @Column(name="id_pacote", type="integer", nullable=true, length=4)
     */
    private $id_pacote;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @return int
     */
    public function getId_entregamaterial()
    {
        return $this->id_entregamaterial;
    }

    /**
     * @param int $id_entregamaterial
     */
    public function setId_entregamaterial($id_entregamaterial)
    {
        $this->id_entregamaterial = $id_entregamaterial;
    }

    /**
     * @return datetime2
     */
    public function getDt_entrega()
    {
        return $this->dt_entrega;
    }

    /**
     * @param datetime2 $dt_entrega
     */
    public function setDt_entrega($dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
    }

    /**
     * @return datetime2
     */
    public function getDt_devolucao()
    {
        return $this->dt_devolucao;
    }

    /**
     * @param datetime2 $dt_devolucao
     */
    public function setDt_devolucao($dt_devolucao)
    {
        $this->dt_devolucao = $dt_devolucao;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getId_itemdematerial()
    {
        return $this->id_itemdematerial;
    }

    /**
     * @param int $id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
    }

    /**
     * @return int
     */
    public function getId_pacote()
    {
        return $this->id_pacote;
    }

    /**
     * @param int $id_pacote
     */
    public function setId_pacote($id_pacote)
    {
        $this->id_pacote = $id_pacote;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return int
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipodestinatario")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class TipoDestinatario
{
    const DESTINATARIO = 1;
    const CC = 2;
    const BCC = 3;

    /**
     * @var integer $id_tipodestinatario
     * @Column(name="id_tipodestinatario", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodestinatario;
    /**
     * @var string $st_tipodestinatario
     * @Column(name="st_tipodestinatario", type="string", nullable=true, length=20)
     */
    private $st_tipodestinatario;


    public function getId_tipodestinatario(){
        return $this->id_tipodestinatario;
    }

    public function getSt_tipodestinatario(){
        return $this->st_tipodestinatario;
    }


    public function setId_tipodestinatario($id_tipodestinatario){
        $this->id_tipodestinatario = $id_tipodestinatario;
    }

    public function setSt_tipodestinatario($st_tipodestinatario){
        $this->st_tipodestinatario = $st_tipodestinatario;
    }

}
<?php

namespace G2\Entity;

/**
 * Class VwGridCertificado
 * @package G2\Entity
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gridcertificado")
 * @EntityView
 */
class VwGridCertificado
{

    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     *
     */
    private $id_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicao;

    /**
     * @Id
     * @var string $id_disciplina
     * @Column(name="id_disciplina", type="string", nullable=true, length=200)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var decimal $nu_aprovafinal
     * @Column(name="nu_aprovafinal", type="decimal", nullable=true, length=5)
     */
    private $nu_aprovafinal;

    /**
     * @var decimal $nu_notamaxima
     * @Column(name="nu_notamaxima", type="decimal", nullable=true, length=9)
     */
    private $nu_notamaxima;

    /**
     * @var date $dt_conclusaodisciplina
     * @Column(name="dt_conclusaodisciplina", type="date", nullable=true, length=3)
     */
    private $dt_conclusaodisciplina;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var string $st_conceito
     * @Column(name="st_conceito", type="string", nullable=true, length=12)
     */
    private $st_conceito;

    /**
     * @var string $st_titularcertificacao
     * @Column(name="st_titularcertificacao", type="string", nullable=true, length=300)
     */
    private $st_titularcertificacao;

    /**
     * @var string $st_titulacao
     * @Column(name="st_titulacao", type="string", nullable=true, length=50)
     */
    private $st_titulacao;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var date $dt_encerramentoextensao
     * @Column(name="dt_encerramentoextensao", type="date", nullable=true, length=3)
     */
    private $dt_encerramentoextensao;


    /**
     * @return date dt_conclusaodisciplina
     */
    public function getDt_conclusaodisciplina()
    {
        return $this->dt_conclusaodisciplina;
    }

    /**
     * @param dt_conclusaodisciplina
     * @return \G2\Entity\VwGridCertificado
     */
    public function setDt_conclusaodisciplina($dt_conclusaodisciplina)
    {
        $this->dt_conclusaodisciplina = $dt_conclusaodisciplina;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param nu_cargahoraria
     * @return \G2\Entity\VwGridCertificado
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return decimal nu_aprovafinal
     */
    public function getNu_aprovafinal()
    {
        return $this->nu_aprovafinal;
    }

    /**
     * @param nu_aprovafinal
     * @return \G2\Entity\VwGridCertificado
     */
    public function setNu_aprovafinal($nu_aprovafinal)
    {
        $this->nu_aprovafinal = $nu_aprovafinal;
        return $this;
    }

    /**
     * @return decimal nu_notamaxima
     */
    public function getNu_notamaxima()
    {
        return $this->nu_notamaxima;
    }

    /**
     * @param nu_notamaxima
     * @return \G2\Entity\VwGridCertificado
     */
    public function setNu_notamaxima($nu_notamaxima)
    {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    /**
     * @return string st_tituloexibicao
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param st_tituloexibicao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return string id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_conceito
     */
    public function getSt_conceito()
    {
        return $this->st_conceito;
    }

    /**
     * @param st_conceito
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_conceito($st_conceito)
    {
        $this->st_conceito = $st_conceito;
        return $this;
    }

    /**
     * @return string st_titularcertificacao
     */
    public function getSt_titularcertificacao()
    {
        return $this->st_titularcertificacao;
    }

    /**
     * @param st_titularcertificacao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_titularcertificacao($st_titularcertificacao)
    {
        $this->st_titularcertificacao = $st_titularcertificacao;
        return $this;
    }

    /**
     * @return string st_titulacao
     */
    public function getSt_titulacao()
    {
        return $this->st_titulacao;
    }

    /**
     * @param st_titulacao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setSt_titulacao($st_titulacao)
    {
        $this->st_titulacao = $st_titulacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return \G2\Entity\VwGridCertificado
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_encerramentoextensao()
    {
        return $this->dt_encerramentoextensao;
    }

    /**
     * @param date $dt_encerramentoextensao
     * @return \G2\Entity\VwGridCertificado
     */
    public function setDt_encerramentoextensao($dt_encerramentoextensao)
    {
        $this->dt_encerramentoextensao = $dt_encerramentoextensao;
        return $this;
    }


}
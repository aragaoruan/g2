<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Avaliacao {

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacao;

    /**
     * @Column(name="st_avaliacao", type="string", nullable=false, length=100)
     * @var string $st_avaliacao
     */
    private $st_avaliacao;

    /**
     * @ManyToOne(targetEntity="TipoAvaliacao")
     * @JoinColumn(name="id_tipoavaliacao", referencedColumnName="id_tipoavaliacao")
     */
    private $id_tipoavaliacao;

    /**
     * @Column(name="nu_valor", type="integer", nullable=true)
     * @var integer $nu_valor
     */
    private $nu_valor;

    /**
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     * @var datetime2 $dt_cadastro
     */
    private $dt_cadastro;

      /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     * @Column(name="st_descricao", type="string", nullable=true)
     * @var string $st_descricao
     */
    private $st_descricao;

    /**
     * @Column(name="nu_quantquestoes", type="integer", nullable=true)
     * @var integer $nu_quantquestoes
     */
    private $nu_quantquestoes;

    /**
     * @Column(name="st_linkreferencia", type="string", nullable=true, length=500)
     * @var string $st_linkreferencia
     */
    private $st_linkreferencia;

    /**
     * @Column(name="bl_recuperacao", type="boolean", nullable=true)
     * @var boolean $bl_recuperacao
     */
    private $bl_recuperacao;
    
     function __construct() {
        $this->id_usuariocadastro = new Usuario();
        $this->id_entidade = new Entidade();
    }

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    public function getId_tipoavaliacao() {
        return $this->id_tipoavaliacao;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function getNu_quantquestoes() {
        return $this->nu_quantquestoes;
    }

    public function getSt_linkreferencia() {
        return $this->st_linkreferencia;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
    }

    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
    }

    public function setId_tipoavaliacao($id_tipoavaliacao) {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
    }

    public function setNu_quantquestoes($nu_quantquestoes) {
        $this->nu_quantquestoes = $nu_quantquestoes;
    }

    public function setSt_linkreferencia($st_linkreferencia) {
        $this->st_linkreferencia = $st_linkreferencia;
    }

    public function getBl_recuperacao()
    {
        return $this->bl_recuperacao;
    }

    public function setBl_recuperacao($bl_recuperacao)
    {
        $this->bl_recuperacao = $bl_recuperacao;
    }



}

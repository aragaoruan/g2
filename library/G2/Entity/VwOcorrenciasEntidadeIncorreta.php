<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_ocorrenciasentidadeincorreta")
 * @Entity
 * @EntityView
 */
class VwOcorrenciasEntidadeIncorreta
{
    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_ocorrencia;

    /**
     * @var integer $id_entidadeocorrencia
     * @Column(name="id_entidadeocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_entidadeocorrencia;

    /**
     * @var integer $id_entidadenucleoco
     * @Column(name="id_entidadenucleoco", type="integer", nullable=true, length=4)
     */
    private $id_entidadenucleoco;

    /**
     * @return int
     */
    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param int $id_ocorrencia
     * @return $this
     */
    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeocorrencia()
    {
        return $this->id_entidadeocorrencia;
    }

    /**
     * @param int $id_entidadeocorrencia
     * @return $this
     */
    public function setId_entidadeocorrencia($id_entidadeocorrencia)
    {
        $this->id_entidadeocorrencia = $id_entidadeocorrencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadenucleoco()
    {
        return $this->id_entidadenucleoco;
    }

    /**
     * @param int $id_entidadenucleoco
     * @return $this
     */
    public function setId_entidadenucleoco($id_entidadenucleoco)
    {
        $this->id_entidadenucleoco = $id_entidadenucleoco;
        return $this;
    }
}

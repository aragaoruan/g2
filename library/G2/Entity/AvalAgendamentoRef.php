<?php

namespace G2\Entity;

/**
 * @Table(name="tb_avalagendamentoref")
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvalAgendamentoRef {

    
     /**
     * @var integer $id_avalagendamentoref
     * @Column(name="id_avalagendamentoref",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avalagendamentoref;
    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=true)
     */
    private $id_avaliacaoagendamento;
    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true)
     */
    private $id_avaliacaoconjuntoreferencia;
    
    public function getId_avalagendamentoref() {
        return $this->id_avalagendamentoref;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function setId_avalagendamentoref($id_avalagendamentoref) {
        $this->id_avalagendamentoref = $id_avalagendamentoref;
        return $this;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * Description of PerfilEntiade
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_perfilentidade")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class PerfilEntidade
{

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @Id
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=true, length=255)
     */
    private $st_nomeperfil;

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @param string $st_nomeperfil
     */
    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }


}

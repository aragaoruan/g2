<?php

namespace G2\Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_ecommerce")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwEcommerce")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class VwEcommerce
{
    /**
     * @var datetime2 $dt_confirmacao
     * @Column(name="dt_confirmacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_confirmacao;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var string $dt_confirmacaoshow
     * @Column(name="dt_confirmacaoshow", type="string", nullable=true, length=8)
     */
    private $dt_confirmacaoshow;
    /**
     * @var string $dt_cadastroshow
     * @Column(name="dt_cadastroshow", type="string", nullable=false, length=8)
     */
    private $dt_cadastroshow;

    /**
     * @var string $dt_nascimento
     * @Column(name="dt_nascimento", type="string", nullable=false, length=80)
     */
    private $dt_nascimento;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=true, length=4)
     */
    private $id_meiopagamento;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false, length=4)
     */
    private $id_tipoproduto;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_produtocombo
     * @Column(name="id_produtocombo", type="integer", nullable=true, length=4)
     */
    private $id_produtocombo;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=false, length=4)
     */
    private $id_entidadepai;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $cod_aluno
     * @Column(name="cod_aluno", type="integer", nullable=false, length=4)
     */
    private $cod_aluno;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_cupom
     * @Column(name="id_cupom", type="integer", nullable=true, length=4)
     */
    private $id_cupom;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer", nullable=true, length=4)
     */
    private $id_categoria;
    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=true, length=4)
     */
    private $nu_parcelas;
    /**
     * @var decimal $nu_descontoproduto
     * @Column(name="nu_descontoproduto", type="decimal", nullable=true, length=17)
     */
    private $nu_descontoproduto;
    /**
     * @var decimal $nu_valortabela
     * @Column(name="nu_valortabela", type="decimal", nullable=false, length=17)
     */
    private $nu_valortabela;
    /**
     * @var decimal $nu_valornegociado
     * @Column(name="nu_valornegociado", type="decimal", nullable=false, length=17)
     */
    private $nu_valornegociado;
    /**
     * @var decimal $nu_valorliquidovenda
     * @Column(name="nu_valorliquidovenda", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquidovenda;
    /**
     * @var decimal $nu_valorcupom
     * @Column(name="nu_valorcupom", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcupom;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string", nullable=true, length=60)
     */
    private $st_telefone;
    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;
    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12)
     */
    private $st_cep;
    /**
     * @var string $st_tipodesconto
     * @Column(name="st_tipodesconto", type="string", nullable=true, length=255)
     */
    private $st_tipodesconto;
    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=true, length=255)
     */
    private $st_campanhacomercial;
    /**
     * @var string $st_nomeatendente
     * @Column(name="st_nomeatendente", type="string", nullable=true, length=300)
     */
    private $st_nomeatendente;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=255)
     */
    private $st_status;
    /**
     * @var string $st_combo
     * @Column(name="st_combo", type="string", nullable=true, length=250)
     */
    private $st_combo;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string", nullable=true)
     */
    private $st_observacao;
    /**
     * @var string $st_codigocupom
     * @Column(name="st_codigocupom", type="string", nullable=true, length=22)
     */
    private $st_codigocupom;
    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;
    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500)
     */
    private $st_complemento;
    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;
    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;
    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;
    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=true, length=255)
     */
    private $st_meiopagamento;
    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;
    /**
     * @var string $st_categorias
     * @Column(name="st_categorias", type="string", nullable=true)
     */
    private $st_categorias;
    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true)
     */
    private $st_areaconhecimento;

    /**
     * @return string
     */
    public function getDt_confirmacaoshow()
    {
        return $this->dt_confirmacaoshow;
    }

    /**
     * @param string $dt_confirmacaoshow
     * @return VwEcommerce
     */
    public function setDt_confirmacaoshow($dt_confirmacaoshow)
    {
        $this->dt_confirmacaoshow = $dt_confirmacaoshow;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_cadastroshow()
    {
        return $this->dt_cadastroshow;
    }

    /**
     * @param string $dt_cadastroshow
     */
    public function setDt_cadastroshow($dt_cadastroshow)
    {
        $this->dt_cadastroshow = $dt_cadastroshow;
    }


    /**
     * @return datetime2 dt_confirmacao
     */
    public function getDt_confirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @param dt_confirmacao
     */
    public function setDt_confirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return integer id_meiopagamento
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @return integer id_tipoproduto
     */
    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @param id_tipoproduto
     */
    public function setId_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_produtocombo
     */
    public function getId_produtocombo()
    {
        return $this->id_produtocombo;
    }

    /**
     * @param id_produtocombo
     */
    public function setId_produtocombo($id_produtocombo)
    {
        $this->id_produtocombo = $id_produtocombo;
        return $this;
    }

    /**
     * @return integer id_entidadepai
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param id_entidadepai
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    /**
     * @return integer cod_aluno
     */
    public function getCod_aluno()
    {
        return $this->cod_aluno;
    }

    /**
     * @param cod_aluno
     */
    public function setCod_aluno($cod_aluno)
    {
        $this->cod_aluno = $cod_aluno;
        return $this;
    }

    /**
     * @return integer id_cupom
     */
    public function getId_cupom()
    {
        return $this->id_cupom;
    }

    /**
     * @param id_cupom
     */
    public function setId_cupom($id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @return integer id_categoria
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

    /**
     * @param id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    /**
     * @return integer nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @return decimal nu_descontoproduto
     */
    public function getNu_descontoproduto()
    {
        return $this->nu_descontoproduto;
    }

    /**
     * @param nu_descontoproduto
     */
    public function setNu_descontoproduto($nu_descontoproduto)
    {
        $this->nu_descontoproduto = $nu_descontoproduto;
        return $this;
    }

    /**
     * @return decimal nu_valortabela
     */
    public function getNu_valortabela()
    {
        return $this->nu_valortabela;
    }

    /**
     * @param nu_valortabela
     */
    public function setNu_valortabela($nu_valortabela)
    {
        $this->nu_valortabela = $nu_valortabela;
        return $this;
    }

    /**
     * @return decimal nu_valornegociado
     */
    public function getNu_valornegociado()
    {
        return $this->nu_valornegociado;
    }

    /**
     * @param nu_valornegociado
     */
    public function setNu_valornegociado($nu_valornegociado)
    {
        $this->nu_valornegociado = $nu_valornegociado;
        return $this;
    }

    /**
     * @return decimal nu_valorliquidovenda
     */
    public function getNu_valorliquidovenda()
    {
        return $this->nu_valorliquidovenda;
    }

    /**
     * @param nu_valorliquidovenda
     */
    public function setNu_valorliquidovenda($nu_valorliquidovenda)
    {
        $this->nu_valorliquidovenda = $nu_valorliquidovenda;
        return $this;
    }

    /**
     * @return decimal nu_valorcupom
     */
    public function getNu_valorcupom()
    {
        return $this->nu_valorcupom;
    }

    /**
     * @param nu_valorcupom
     */
    public function setNu_valorcupom($nu_valorcupom)
    {
        $this->nu_valorcupom = $nu_valorcupom;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_email
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_telefone
     */
    public function getSt_telefone()
    {
        return $this->st_telefone;
    }

    /**
     * @param st_telefone
     */
    public function setSt_telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
        return $this;
    }

    /**
     * @return string st_endereco
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return string st_cep
     */
    public function getSt_cep()
    {
        return $this->st_cep;
    }

    /**
     * @param st_cep
     */
    public function setSt_cep($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    /**
     * @return string st_tipodesconto
     */
    public function getSt_tipodesconto()
    {
        return $this->st_tipodesconto;
    }

    /**
     * @param st_tipodesconto
     */
    public function setSt_tipodesconto($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;
    }

    /**
     * @return string st_campanhacomercial
     */
    public function getSt_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @param st_campanhacomercial
     */
    public function setSt_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    /**
     * @return string st_nomeatendente
     */
    public function getSt_nomeatendente()
    {
        return $this->st_nomeatendente;
    }

    /**
     * @param st_nomeatendente
     */
    public function setSt_nomeatendente($st_nomeatendente)
    {
        $this->st_nomeatendente = $st_nomeatendente;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_combo
     */
    public function getSt_combo()
    {
        return $this->st_combo;
    }

    /**
     * @param st_combo
     */
    public function setSt_combo($st_combo)
    {
        $this->st_combo = $st_combo;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_observacao
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
        return $this;
    }

    /**
     * @return string st_codigocupom
     */
    public function getSt_codigocupom()
    {
        return $this->st_codigocupom;
    }

    /**
     * @param st_codigocupom
     */
    public function setSt_codigocupom($st_codigocupom)
    {
        $this->st_codigocupom = $st_codigocupom;
        return $this;
    }

    /**
     * @return string st_bairro
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    /**
     * @return string st_complemento
     */
    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param st_complemento
     */
    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * @return string nu_numero
     */
    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param nu_numero
     */
    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string st_cidade
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param st_cidade
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    /**
     * @return string st_produto
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @param st_produto
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @return string st_meiopagamento
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    /**
     * @return string sg_uf
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return nvarchar st_categorias
     */
    public function getSt_categorias()
    {
        return $this->st_categorias;
    }

    /**
     * @param st_categorias
     */
    public function setSt_categorias($st_categorias)
    {
        $this->st_categorias = $st_categorias;
        return $this;
    }

    /**
     * @return nvarchar st_areaconhecimento
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @param datetime2 $dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }


}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_saladeaulaprofessorintegracao")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\SalaDeAula")
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class VwSalaDeAulaProfessorIntegracao
{

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     */
    private $id_saladeaula;
    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true, length=3)
     */
    private $dt_inicioinscricao;
    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=3)
     */
    private $dt_fiminscricao;
    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_tiposaladeaula;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;
    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;
    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=false, length=4)
     */
    private $id_perfilpedagogico;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=false, length=4)
     */
    private $id_categoriasala;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @var integer $id_status
     * @Column(name="id_status", type="integer", nullable=false, length=4)
     */
    private $id_status;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false, length=40)
     */
    private $st_login;
    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=32)
     */
    private $st_senha;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=false)
     */
    private $st_codsistema;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;
    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=12)
     */
    private $st_status;
    /**
     * @var string $st_tiposaladeaula
     * @Column(name="st_tiposaladeaula", type="string", nullable=false, length=255)
     */
    private $st_tiposaladeaula;
    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=false, length=30)
     */
    private $st_codsistemacurso;
    /**
     * @var string $st_codsistemasala
     * @Column(name="st_codsistemasala", type="string", nullable=true, length=30)
     */
    private $st_codsistemasala;
    /**
     * @var string $st_codsistemareferencia
     * @Column(name="st_codsistemareferencia", type="string", nullable=true, length=30)
     */
    private $st_codsistemareferencia;
    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=100)
     */
    private $st_nomeperfil;
    /**
     * @var string $st_caminho
     * @Column(name="st_caminho", type="string", nullable=true, length=30)
     */
    private $st_caminho;
    /**
     * /**
     * @var string $st_perfilpedagogico
     * @Column(name="st_perfilpedagogico", type="string", nullable=false, length=255)
     */
    private $st_perfilpedagogico;
    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=false)
     */
    private $st_codusuario;
    /**
     * @var string $st_senhaintegrada
     * @Column(name="st_senhaintegrada", type="string", nullable=true)
     */
    private $st_senhaintegrada;
    /**
     * @var string $st_loginintegrado
     * @Column(name="st_loginintegrado", type="string", nullable=true)
     */
    private $st_loginintegrado;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_abertura
     * @Column(name="st_abertura", type="string", nullable=true)
     */
    private $st_abertura;
    /**
     * @var string $st_encerramento
     * @Column(name="st_encerramento", type="string", nullable=true)
     */
    private $st_encerramento;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var string $st_alocados
     * @Column(name="st_alocados", type="string", nullable=true)
     */
    private $st_alocados;
    /**
     * @var string $st_semacesso
     * @Column(name="st_semacesso", type="string", nullable=true)
     */
    private $st_semacesso;

    /**
     * @var integer $id_situacaosala
     * @Column(name="id_situacaosala", type="integer", nullable=false, length=4)
     */
    private $id_situacaosala;

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=false, length=4)
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwSalaDeAulaProfessorIntegracao
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


    /**
     * @param mixed $dt_fiminscricao
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
    }

    /**
     * @return mixed
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param mixed $dt_inicioinscricao
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
    }

    /**
     * @return mixed
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return int
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param int $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @return int
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
    }

    /**
     * @return int
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param int $id_tiposaladeaula
     */
    public function setId_tiposaladeaula($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
    }

    /**
     * @return int
     */
    public function getId_tiposaladeaula()
    {
        return $this->id_tiposaladeaula;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param string $st_abertura
     */
    public function setSt_abertura($st_abertura)
    {
        $this->st_abertura = $st_abertura;
    }

    /**
     * @return string
     */
    public function getSt_abertura()
    {
        return $this->st_abertura;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @return string
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }

    /**
     * @return string
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param string $st_codsistemareferencia
     */
    public function setSt_codsistemareferencia($st_codsistemareferencia)
    {
        $this->st_codsistemareferencia = $st_codsistemareferencia;
    }

    /**
     * @return string
     */
    public function getSt_codsistemareferencia()
    {
        return $this->st_codsistemareferencia;
    }

    /**
     * @param string $st_codsistemasala
     */
    public function setSt_codsistemasala($st_codsistemasala)
    {
        $this->st_codsistemasala = $st_codsistemasala;
    }

    /**
     * @return string
     */
    public function getSt_codsistemasala()
    {
        return $this->st_codsistemasala;
    }

    /**
     * @param string $st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
    }

    /**
     * @return string
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_encerramento
     */
    public function setSt_encerramento($st_encerramento)
    {
        $this->st_encerramento = $st_encerramento;
    }

    /**
     * @return string
     */
    public function getSt_encerramento()
    {
        return $this->st_encerramento;
    }

    /**
     * @param string $st_login
     */
    public function setSt_login($st_login)
    {
        $this->st_login = $st_login;
    }

    /**
     * @return string
     */
    public function getSt_login()
    {
        return $this->st_login;
    }

    /**
     * @param string $st_loginintegrado
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
    }

    /**
     * @return string
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeperfil
     */
    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    /**
     * @return string
     */
    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @param string $st_perfilpedagogico
     */
    public function setSt_perfilpedagogico($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
    }

    /**
     * @return string
     */
    public function getSt_perfilpedagogico()
    {
        return $this->st_perfilpedagogico;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_senha
     */
    public function setSt_senha($st_senha)
    {
        $this->st_senha = $st_senha;
    }

    /**
     * @return string
     */
    public function getSt_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param string $st_senhaintegrada
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
    }

    /**
     * @return string
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @param string $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_tiposaladeaula
     */
    public function setSt_tiposaladeaula($st_tiposaladeaula)
    {
        $this->st_tiposaladeaula = $st_tiposaladeaula;
    }

    /**
     * @return string
     */
    public function getSt_tiposaladeaula()
    {
        return $this->st_tiposaladeaula;
    }

    /**
     * @return date
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param date $dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    /**
     * @return string
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param string $st_caminho
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
    }

    /**
     * @return int
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param int $id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    /**
     * @return int
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return string
     */
    public function getSt_alocados()
    {
        return $this->st_alocados;
    }

    /**
     * @return int
     */
    public function getId_situacaosala()
    {
        return $this->id_situacaosala;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacaosala($id_situacaosala)
    {
        $this->id_situacaosala = $id_situacaosala;
    }


    /**
     * @param string $st_alocados
     */
    public function setSt_alocados($st_alocados)
    {
        $this->st_alocados = $st_alocados;
    }

    /**
     * @return string
     */
    public function getSt_semacesso()
    {
        return $this->st_semacesso;
    }

    /**
     * @param string $st_semacesso
     */
    public function setSt_semacesso($st_semacesso)
    {
        $this->st_semacesso = $st_semacesso;
    }

}

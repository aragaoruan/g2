<?php

namespace G2\Entity;

/**
 * Class VwCampanhaPremioProduto
 * @package G2\Entity
 * @Entity
 * @EntityView
 * @Table(name="vw_produtospremio")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-07-20
 */
class VwProdutosPremio
{

    /**
     * @var integer $id_premioproduto
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(type="integer", nullable=false)
     */
    private $id_premioproduto;

    /**
     * @var integer $id_usuariocadastro
     * @Id
     * @Column(type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_campanhacomercial
     * @Id
     * @Column(type="integer", nullable=false)
     */
    private $id_campanhacomercial;

    /**
     * @var integer $id_produto
     * @Id
     * @Column(type="integer", nullable=false)
     */
    private $id_produto;

    /**
     * @var string $id_campanhacomercial
     * @Column(type="string", nullable=false)
     */
    private $st_produto;

    /**
     * @var integer $id_tipoproduto
     * @Id
     * @Column(type="integer", nullable=false)
     */
    private $id_tipoproduto;

    /**
     * @var string $st_tipoproduto
     * @Column(type="string", nullable=false)
     */
    private $st_tipoproduto;

    /**
     * @return int
     */
    public function getId_premioproduto ()
    {
        return $this->id_premioproduto;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return int
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * @return string
     */
    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    /**
     * @return int
     */
    public function getId_tipoproduto ()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @return string
     */
    public function getSt_tipoproduto ()
    {
        return $this->st_tipoproduto;
    }

    /**
     * @param $id_premioproduto
     * @return $this
     */
    public function setId_premioproduto ($id_premioproduto)
    {
        $this->id_premioproduto = $id_premioproduto;
        return $this;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param $id_campanhacomercial
     * @return $this
     */
    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @param $id_produto
     * @return $this
     */
    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @param $st_produto
     * @return $this
     */
    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @param $id_tipoproduto
     * @return $this
     */
    public function setId_tipoproduto ($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @param $st_tipoproduto
     * @return $this
     */
    public function setSt_tipoproduto ($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_projetopedagogico")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwProjetoPedagogico
{

    /**
     *
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=2500)
     */
    private $st_descricao;

    /**
     *
     * @var integer $nu_idademinimainiciar
     * @Column(name="nu_idademinimainiciar", type="integer", nullable=true)
     */
    private $nu_idademinimainiciar;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var integer $nu_tempominimoconcluir
     * @Column(name="nu_tempominimoconcluir", type="integer", nullable=true)
     */
    private $nu_tempominimoconcluir;

    /**
     *
     * @var integer $nu_idademinimaconcluir
     * @Column(name="nu_idademinimaconcluir", type="integer", nullable=true)
     */
    private $nu_idademinimaconcluir;

    /**
     *
     * @var integer $nu_tempomaximoconcluir
     * @Column(name="nu_tempomaximoconcluir", type="integer", nullable=true)
     */
    private $nu_tempomaximoconcluir;

    /**
     *
     * @var integer $nu_tempopreviconcluir
     * @Column(name="nu_tempopreviconcluir", type="integer", nullable=true)
     */
    private $nu_tempopreviconcluir;

    /**
     *
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     *
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicao;

    /**
     *
     * @var boolean $bl_autoalocaraluno
     * @Column(name="bl_autoalocaraluno", type="boolean", nullable=false)
     */
    private $bl_autoalocaraluno;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=false)
     */
    private $id_trilha;

    /**
     *
     * @var integer $id_projetopedagogicoorigem
     * @Column(name="id_projetopedagogicoorigem", type="integer", nullable=true)
     */
    private $id_projetopedagogicoorigem;

    /**
     *
     * @var string $st_nomeversao
     * @Column(name="st_nomeversao", type="string", nullable=true, length=255)
     */
    private $st_nomeversao;

    /**
     *
     * @var integer $id_tipoextracurricular
     * @Column(name="id_tipoextracurricular", type="integer", nullable=true)
     */
    private $id_tipoextracurricular;

    /**
     *
     * @var decimal $nu_notamaxima
     * @Column(name="nu_notamaxima", type="decimal", nullable=true)
     */
    private $nu_notamaxima;

    /**
     *
     * @var decimal $nu_percentualaprovacao
     * @Column(name="nu_percentualaprovacao", type="decimal", nullable=true)
     */
    private $nu_percentualaprovacao;

    /**
     *
     * @var integer $nu_valorprovafinal
     * @Column(name="nu_valorprovafinal", type="integer", nullable=true)
     */
    private $nu_valorprovafinal;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false)
     */
    private $id_entidadecadastro;

    /**
     *
     * @var boolean $bl_provafinal
     * @Column(name="bl_provafinal", type="boolean", nullable=false)
     */
    private $bl_provafinal;

    /**
     *
     * @var boolean $bl_salasemprovafinal
     * @Column(name="bl_salasemprovafinal", type="boolean", nullable=false)
     */
    private $bl_salasemprovafinal;

    /**
     *
     * @var string $st_objetivo
     * @Column(name="st_objetivo", type="string", nullable=true, length=2500)
     */
    private $st_objetivo;

    /**
     *
     * @var string $st_estruturacurricular
     * @Column(name="st_estruturacurricular", type="string", nullable=true, length=2500)
     */
    private $st_estruturacurricular;

    /**
     *
     * @var string $st_metodologiaavaliacao
     * @Column(name="st_metodologiaavaliacao", type="string", nullable=true, length=2500)
     */
    private $st_metodologiaavaliacao;

    /**
     *
     * @var string $st_certificacao
     * @Column(name="st_certificacao", type="string", nullable=true, length=2500)
     */
    private $st_certificacao;

    /**
     *
     * @var string $st_valorapresentacao
     * @Column(name="st_valorapresentacao", type="string", nullable=true, length=2500)
     */
    private $st_valorapresentacao;

    /**
     *
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true)
     */
    private $nu_cargahoraria;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var boolean $bl_disciplinaextracurricular
     * @Column(name="bl_disciplinaextracurricular", type="boolean", nullable=false)
     */
    private $bl_disciplinaextracurricular;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     *
     * @var string $st_conteudoprogramatico
     * @Column(name="st_conteudoprogramatico", type="string", length=2500, nullable=true)
     */
    private $st_conteudoprogramatico;

    /**
     *
     * @var integer $id_medidatempoconclusao
     * @Column(name="id_medidatempoconclusao", type="integer", nullable=true)
     */
    private $id_medidatempoconclusao;

    /**
     *
     * @var integer $id_contratoregra
     * @Column(name="id_contratoregra", type="integer", nullable=true)
     */
    private $id_contratoregra;

    /**
     *
     * @var integer $id_livroregistroentidade
     * @Column(name="id_livroregistroentidade", type="integer", nullable=true)
     */
    private $id_livroregistroentidade;

    /**
     *
     * @var integer $nu_prazoagendamento
     * @Column(name="nu_prazoagendamento", type="integer", nullable=true)
     */
    private $nu_prazoagendamento;

    /**
     *
     * @var text $st_perfilprojeto
     * @Column(name="st_perfilprojeto", type="text", nullable=true)
     */
    private $st_perfilprojeto;

    /**
     *
     * @var text $st_coordenacao
     * @Column(name="st_coordenacao", type="text", nullable=true)
     */
    private $st_coordenacao;

    /**
     *
     * @var text $st_horario
     * @Column(name="st_horario", type="text", nullable=true)
     */
    private $st_horario;

    /**
     *
     * @var text $st_publicoalvo
     * @Column(name="st_publicoalvo", type="text", nullable=true)
     */
    private $st_publicoalvo;

    /**
     *
     * @var text $st_mercadotrabalho
     * @Column(name="st_mercadotrabalho", type="text", nullable=true)
     */
    private $st_mercadotrabalho;

    /**
     *
     * @var integer $id_usuarioprojeto
     * @Column(name="id_usuarioprojeto", type="integer", nullable=false)
     */
    private $id_usuarioprojeto;

    /**
     *
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false, length=40)
     */
    private $st_login;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var decimal $nu_taxamatricula
     * @Column(name="nu_taxamatricula", type="decimal", nullable=true)
     */
    private $nu_taxamatricula;

    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getNu_idademinimainiciar ()
    {
        return $this->nu_idademinimainiciar;
    }

    public function setNu_idademinimainiciar ($nu_idademinimainiciar)
    {
        $this->nu_idademinimainiciar = $nu_idademinimainiciar;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getNu_tempominimoconcluir ()
    {
        return $this->nu_tempominimoconcluir;
    }

    public function setNu_tempominimoconcluir ($nu_tempominimoconcluir)
    {
        $this->nu_tempominimoconcluir = $nu_tempominimoconcluir;
        return $this;
    }

    public function getNu_idademinimaconcluir ()
    {
        return $this->nu_idademinimaconcluir;
    }

    public function setNu_idademinimaconcluir ($nu_idademinimaconcluir)
    {
        $this->nu_idademinimaconcluir = $nu_idademinimaconcluir;
        return $this;
    }

    public function getNu_tempomaximoconcluir ()
    {
        return $this->nu_tempomaximoconcluir;
    }

    public function setNu_tempomaximoconcluir ($nu_tempomaximoconcluir)
    {
        $this->nu_tempomaximoconcluir = $nu_tempomaximoconcluir;
        return $this;
    }

    public function getNu_tempopreviconcluir ()
    {
        return $this->nu_tempopreviconcluir;
    }

    public function setNu_tempopreviconcluir ($nu_tempopreviconcluir)
    {
        $this->nu_tempopreviconcluir = $nu_tempopreviconcluir;
        return $this;
    }

    public function getSt_projetopedagogico ()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico ($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function getSt_tituloexibicao ()
    {
        return $this->st_tituloexibicao;
    }

    public function setSt_tituloexibicao ($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    public function getBl_autoalocaraluno ()
    {
        return $this->bl_autoalocaraluno;
    }

    public function setBl_autoalocaraluno ($bl_autoalocaraluno)
    {
        $this->bl_autoalocaraluno = $bl_autoalocaraluno;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_trilha ()
    {
        return $this->id_trilha;
    }

    public function setId_trilha ($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    public function getId_projetopedagogicoorigem ()
    {
        return $this->id_projetopedagogicoorigem;
    }

    public function setId_projetopedagogicoorigem ($id_projetopedagogicoorigem)
    {
        $this->id_projetopedagogicoorigem = $id_projetopedagogicoorigem;
        return $this;
    }

    public function getSt_nomeversao ()
    {
        return $this->st_nomeversao;
    }

    public function setSt_nomeversao ($st_nomeversao)
    {
        $this->st_nomeversao = $st_nomeversao;
        return $this;
    }

    public function getId_tipoextracurricular ()
    {
        return $this->id_tipoextracurricular;
    }

    public function setId_tipoextracurricular ($id_tipoextracurricular)
    {
        $this->id_tipoextracurricular = $id_tipoextracurricular;
        return $this;
    }

    public function getNu_notamaxima ()
    {
        return $this->nu_notamaxima;
    }

    public function setNu_notamaxima ($nu_notamaxima)
    {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    public function getNu_percentualaprovacao ()
    {
        return $this->nu_percentualaprovacao;
    }

    public function setNu_percentualaprovacao ($nu_percentualaprovacao)
    {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    public function getNu_valorprovafinal ()
    {
        return $this->nu_valorprovafinal;
    }

    public function setNu_valorprovafinal ($nu_valorprovafinal)
    {
        $this->nu_valorprovafinal = $nu_valorprovafinal;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getBl_provafinal ()
    {
        return $this->bl_provafinal;
    }

    public function setBl_provafinal ($bl_provafinal)
    {
        $this->bl_provafinal = $bl_provafinal;
        return $this;
    }

    public function getBl_salasemprovafinal ()
    {
        return $this->bl_salasemprovafinal;
    }

    public function setBl_salasemprovafinal ($bl_salasemprovafinal)
    {
        $this->bl_salasemprovafinal = $bl_salasemprovafinal;
        return $this;
    }

    public function getSt_objetivo ()
    {
        return $this->st_objetivo;
    }

    public function setSt_objetivo ($st_objetivo)
    {
        $this->st_objetivo = $st_objetivo;
        return $this;
    }

    public function getSt_estruturacurricular ()
    {
        return $this->st_estruturacurricular;
    }

    public function setSt_estruturacurricular ($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
        return $this;
    }

    public function getSt_metodologiaavaliacao ()
    {
        return $this->st_metodologiaavaliacao;
    }

    public function setSt_metodologiaavaliacao ($st_metodologiaavaliacao)
    {
        $this->st_metodologiaavaliacao = $st_metodologiaavaliacao;
        return $this;
    }

    public function getSt_certificacao ()
    {
        return $this->st_certificacao;
    }

    public function setSt_certificacao ($st_certificacao)
    {
        $this->st_certificacao = $st_certificacao;
        return $this;
    }

    public function getSt_valorapresentacao ()
    {
        return $this->st_valorapresentacao;
    }

    public function setSt_valorapresentacao ($st_valorapresentacao)
    {
        $this->st_valorapresentacao = $st_valorapresentacao;
        return $this;
    }

    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro (datetime2 $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getBl_disciplinaextracurricular ()
    {
        return $this->bl_disciplinaextracurricular;
    }

    public function setBl_disciplinaextracurricular ($bl_disciplinaextracurricular)
    {
        $this->bl_disciplinaextracurricular = $bl_disciplinaextracurricular;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_conteudoprogramatico ()
    {
        return $this->st_conteudoprogramatico;
    }

    public function setSt_conteudoprogramatico ($st_conteudoprogramatico)
    {
        $this->st_conteudoprogramatico = $st_conteudoprogramatico;
        return $this;
    }

    public function getId_medidatempoconclusao ()
    {
        return $this->id_medidatempoconclusao;
    }

    public function setId_medidatempoconclusao ($id_medidatempoconclusao)
    {
        $this->id_medidatempoconclusao = $id_medidatempoconclusao;
        return $this;
    }

    public function getId_contratoregra ()
    {
        return $this->id_contratoregra;
    }

    public function setId_contratoregra ($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }

    public function getId_livroregistroentidade ()
    {
        return $this->id_livroregistroentidade;
    }

    public function setId_livroregistroentidade ($id_livroregistroentidade)
    {
        $this->id_livroregistroentidade = $id_livroregistroentidade;
        return $this;
    }

    public function getNu_prazoagendamento ()
    {
        return $this->nu_prazoagendamento;
    }

    public function setNu_prazoagendamento ($nu_prazoagendamento)
    {
        $this->nu_prazoagendamento = $nu_prazoagendamento;
        return $this;
    }

    public function getSt_perfilprojeto ()
    {
        return $this->st_perfilprojeto;
    }

    public function setSt_perfilprojeto ($st_perfilprojeto)
    {
        $this->st_perfilprojeto = $st_perfilprojeto;
        return $this;
    }

    public function getSt_coordenacao ()
    {
        return $this->st_coordenacao;
    }

    public function setSt_coordenacao ($st_coordenacao)
    {
        $this->st_coordenacao = $st_coordenacao;
        return $this;
    }

    public function getSt_horario ()
    {
        return $this->st_horario;
    }

    public function setSt_horario ($st_horario)
    {
        $this->st_horario = $st_horario;
        return $this;
    }

    public function getSt_publicoalvo ()
    {
        return $this->st_publicoalvo;
    }

    public function setSt_publicoalvo ($st_publicoalvo)
    {
        $this->st_publicoalvo = $st_publicoalvo;
        return $this;
    }

    public function getSt_mercadotrabalho ()
    {
        return $this->st_mercadotrabalho;
    }

    public function setSt_mercadotrabalho ($st_mercadotrabalho)
    {
        $this->st_mercadotrabalho = $st_mercadotrabalho;
        return $this;
    }

    public function getId_usuarioprojeto ()
    {
        return $this->id_usuarioprojeto;
    }

    public function setId_usuarioprojeto ($id_usuarioprojeto)
    {
        $this->id_usuarioprojeto = $id_usuarioprojeto;
        return $this;
    }

    public function getSt_login ()
    {
        return $this->st_login;
    }

    public function setSt_login ($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getNu_taxamatricula ()
    {
        return $this->nu_taxamatricula;
    }

    public function setNu_taxamatricula ($nu_taxamatricula)
    {
        $this->nu_taxamatricula = $nu_taxamatricula;
        return $this;
    }

}
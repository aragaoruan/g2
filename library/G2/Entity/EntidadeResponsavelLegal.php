<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidaderesponsavellegal")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadeResponsavelLegal
{

    /**
     *
     * @var integer $id_entidaderesponsavellegal
     * @Column(name="id_entidaderesponsavellegal", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidaderesponsavellegal;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     *
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false)
     */
    private $bl_padrao;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_orgaoexpeditor
     * @Column(name="st_orgaoexpeditor", type="string", nullable=false, length=10)
     */
    private $st_orgaoexpeditor;

    /**
     * @var TipoEntidadeResponsavelLegal $id_tipoentidaderesponsavel
     * @ManyToOne(targetEntity="TipoEntidadeResponsavelLegal")
     * @JoinColumn(name="id_tipoentidaderesponsavel", nullable=false, referencedColumnName="id_tipoentidaderesponsavel")
     */
    private $id_tipoentidaderesponsavel;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var Uf $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", nullable=false, referencedColumnName="sg_uf")
     */
    private $sg_uf;

    /**
     *
     * @var string $st_registro
     * @Column(name="st_registro", type="string", nullable=false, length=100)
     */
    private $st_registro;

    public function getId_entidaderesponsavellegal()
    {
        return $this->id_entidaderesponsavellegal;
    }

    public function setId_entidaderesponsavellegal($id_entidaderesponsavellegal)
    {
        $this->id_entidaderesponsavellegal = $id_entidaderesponsavellegal;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getBl_padrao()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    public function setSt_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    public function getId_tipoentidaderesponsavel()
    {
        return $this->id_tipoentidaderesponsavel;
    }

    public function setId_tipoentidaderesponsavel(TipoEntidadeResponsavelLegal $id_tipoentidaderesponsavel)
    {
        $this->id_tipoentidaderesponsavel = $id_tipoentidaderesponsavel;
        return $this;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getSt_registro()
    {
        return $this->st_registro;
    }

    public function setSt_registro($st_registro)
    {
        $this->st_registro = $st_registro;
        return $this;
    }

}

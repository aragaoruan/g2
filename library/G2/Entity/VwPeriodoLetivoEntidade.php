<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_periodoletivoentidade")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

class VwPeriodoLetivoEntidade
{
    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_periodoletivo;

    /**
     * @var datetime2 $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicioinscricao;

    /**
     * @var datetime2 $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="datetime2", nullable=false, length=8)
     */
    private $dt_fiminscricao;

    /**
     * @var datetime2 $dt_abertura
     * @Column(name="dt_abertura", type="datetime2", nullable=false, length=8)
     */
    private $dt_abertura;

    /**
     * @var datetime2 $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2", nullable=false, length=8)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_tipooferta
     * @Column(name="id_tipooferta", type="integer", nullable=true, length=4)
     */
    private $id_tipooferta;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadeperiodo
     * @Column(name="id_entidadeperiodo", type="integer", nullable=false, length=4)
     */
    private $id_entidadeperiodo;

    /**
     * @var string $st_periodoletivo
     * @Column(name="st_periodoletivo", type="string", nullable=false, length=255)
     */
    private $st_periodoletivo;

    /**
     * @return datetime2
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param datetime2 $dt_inicioinscricao
     * @return $this
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param datetime2 $dt_fiminscricao
     * @return $this
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param datetime2 $dt_abertura
     * @return $this
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param datetime2 $dt_encerramento
     * @return $this
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param int $id_periodoletivo
     * @return $this
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipooferta()
    {
        return $this->id_tipooferta;
    }

    /**
     * @param int $id_tipooferta
     * @return $this
     */
    public function setId_tipooferta($id_tipooferta)
    {
        $this->id_tipooferta = $id_tipooferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeperiodo()
    {
        return $this->id_entidadeperiodo;
    }

    /**
     * @param int $id_entidadeperiodo
     * @return $this
     */
    public function setId_entidadeperiodo($id_entidadeperiodo)
    {
        $this->id_entidadeperiodo = $id_entidadeperiodo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_periodoletivo()
    {
        return $this->st_periodoletivo;
    }

    /**
     * @param string $st_periodoletivo
     * @return $this
     */
    public function setSt_periodoletivo($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

}
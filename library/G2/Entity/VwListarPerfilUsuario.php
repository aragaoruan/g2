<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_listarperfilusuario")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwListarPerfilUsuario {


    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false)
     *@Id
     */
    private $id_perfil;


    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     */
    private $id_usuario;


    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true)
     */
    private $dt_termino;


    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;


    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;


    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;


    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=100)
     */
    private $st_nomeperfil;


    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;


    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     * @Id
     */
    private $id_entidade;

    /**
     * @var string $st_situacaoperfil
     * @Column(name="st_situacaoperfil", type="string", nullable=false, length=255)
     *
     */
    private $st_situacaoperfil;


    /**
     * @var integer $nu_entidadepai
     * @Column(name="nu_entidadepai", type="integer", nullable=true)
     */
    private $nu_entidadepai;

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    public function setSt_situacaoperfil($st_situacaoperfil)
    {
        $this->st_situacaoperfil = $st_situacaoperfil;
    }

    public function getSt_situacaoperfil()
    {
        return $this->st_situacaoperfil;
    }

    public function setNu_entidadepai($nu_entidadepai)
    {
        $this->nu_entidadepai = $nu_entidadepai;
    }

    public function getNu_entidadepai()
    {
        return $this->nu_entidadepai;
    }

}

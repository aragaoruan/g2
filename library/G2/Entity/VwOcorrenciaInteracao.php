<?php

namespace G2\Entity;

/**
 * @SWG\Definition(
 *     required={},
 *     @SWG\Xml(name="VwOcorrenciaInteracao")
 * )
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_ocorrenciainteracao")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\Ocorrencia")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwOcorrenciaInteracao
{

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false)
     * @Id
     */
    private $id_tramite;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer")
     */
    private $id_ocorrencia;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_visivel
     * @Column(name="bl_visivel", type="boolean")
     */
    private $bl_visivel;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @SWG\Property(format="datetime")
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @SWG\Property(format="string")
     * @var text $st_tramite
     * @Column(name="st_tramite", type="text")
     */
    private $st_tramite;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @SWG\Property(format="string")
     * @var text $st_upload
     * @Column(name="st_upload", type="text")
     */
    private $st_upload;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer")
     */
    private $id_upload;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cadastro
     * @Column(name="st_cadastro", type="string")
     */
    private $st_cadastro;


    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    public function getBl_visivel()
    {
        return $this->bl_visivel;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getSt_tramite()
    {
        return $this->st_tramite;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getSt_upload()
    {
        return $this->st_upload;
    }

    public function getId_upload()
    {
        return $this->id_upload;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    public function setBl_visivel($bl_visivel)
    {
        $this->bl_visivel = $bl_visivel;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setSt_tramite($st_tramite)
    {
        $this->st_tramite = $st_tramite;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function setSt_upload($st_upload)
    {
        $this->st_upload = $st_upload;
    }

    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function getSt_cadastro()
    {
        return $this->st_cadastro;
    }

    public function setSt_cadastro($st_cadastro)
    {
        $this->st_cadastro = $st_cadastro;
        return $this;
    }


}

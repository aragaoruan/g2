<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tiporegrapagamento")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class TipoRegraPagamento {

    /**
     * @var RegraPagamento
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(type="integer")
     */
    private $id_tiporegrapagamento;

    /**
     * @var float
     * @Column(type="string")
     */
    private $st_tiporegrapagamento;

    public function getId_tiporegrapagamento() {
        return $this->id_tiporegrapagamento;
    }

    public function setId_tiporegrapagamento(RegraPagamento $id_tiporegrapagamento) {
        $this->id_tiporegrapagamento = $id_tiporegrapagamento;
    }

    public function getSt_tiporegrapagamento() {
        return $this->st_tiporegrapagamento;
    }

    public function setSt_tiporegrapagamento($st_tiporegrapagamento) {
        $this->st_tiporegrapagamento = $st_tiporegrapagamento;
    }

}

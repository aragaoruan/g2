<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_registropessoa")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 */
class RegistroPessoa {

    /**
     *
     * @var integer $id_registropessoa
     * @Column(name="id_registropessoa", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_registropessoa;
    
    
    /**
     * @Column(type="string",length=255,nullable=false)
     * @var string
     */
    private $st_registropessoa;

    public function getId_registropessoa() {
        return $this->id_registropessoa;
    }

    public function setId_registropessoa($id_registropessoa) {
        $this->id_registropessoa = $id_registropessoa;
    }

    public function getSt_registropessoa() {
        return $this->st_registropessoa;
    }

    public function setSt_registropessoa($st_registropessoa) {
        $this->st_registropessoa = $st_registropessoa;
    }
    
}
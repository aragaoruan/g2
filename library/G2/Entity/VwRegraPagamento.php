<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_regrapagamento")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwRegraPagamento {

    /**
     * @Id
     * @Column(type="integer")
     */
    private $id_regrapagamento;

    /**
     * @Id
     * @Column(type="integer")
     */
    private $id_tiporegrapagamento;

    /**
     * @Id
     * @Column(type="string")
     */
    private $st_tiporegrapagamento;

    /**
     * @var Entidade
     * @Column(type="integer")
     */
    private $id_entidade;

    /**
     * @var string
     * @Column(type="string")
     */
    private $st_entidade;

    /**
     * @var AreaConhecimento
     * @Column(type="integer")
     */
    private $id_areaconhecimento;

    /**
     * @var string
     * @Column(type="string")
     */
    private $st_areaconhecimento;

    /**
     * @var ProjetoPedagogico
     * @Column(type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string
     * @Column(type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var Usuario
     * @Column(type="integer")
     */
    private $id_professor;

    /**
     * @var string
     * @Column(type="string")
     */
    private $st_professor;

    /**
     * @var CargaHoraria
     * @Column(type="integer")
     */
    private $id_cargahoraria;

    /**
     * @var integer
     * @Column(type="integer")
     */
    private $nu_cargahoraria;

    /**
     * @var float
     * @Column(type="float")
     */
    private $nu_valor;

    /**
     * @var TipoDisciplina
     * @Column(type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @var string
     * @Column(type="string")
     */
    private $st_tipodisciplina;

    /**
     * @var \DateTime
     * @Column(type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @var boolean
     * @Column(type="boolean")
     */
    private $bl_ativo;

    public function getId_regrapagamento() {
        return $this->id_regrapagamento;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_professor() {
        return $this->id_professor;
    }

    public function getId_cargahoraria() {
        return $this->id_cargahoraria;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_regrapagamento(RegraPagamento $id_regrapagamento) {
        $this->id_regrapagamento = $id_regrapagamento;
    }

    public function setId_entidade(Entidade $id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_areaconhecimento(AreaConhecimento $id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setId_projetopedagogico(ProjetoPedagogico $id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_professor(Usuario $id_professor) {
        $this->id_professor = $id_professor;
    }

    public function setId_cargahoraria(CargaHoraria $id_cargahoraria) {
        $this->id_cargahoraria = $id_cargahoraria;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function setId_tipodisciplina(TipoDisciplina $id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function setDt_cadastro(\DateTime $dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getSt_entidade() {
        return $this->st_entidade;
    }

    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_professor() {
        return $this->st_professor;
    }

    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    public function getSt_tipodisciplina() {
        return $this->st_tipodisciplina;
    }

    public function setSt_entidade($st_entidade) {
        $this->st_entidade = $st_entidade;
    }

    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_professor($st_professor) {
        $this->st_professor = $st_professor;
    }

    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    public function setSt_tipodisciplina($st_tipodisciplina) {
        $this->st_tipodisciplina = $st_tipodisciplina;
    }

    public function getId_tiporegrapagamento() {
        return $this->id_tiporegrapagamento;
    }

    public function getSt_tiporegrapagamento() {
        return $this->st_tiporegrapagamento;
    }

    public function setId_tiporegrapagamento($id_tiporegrapagamento) {
        $this->id_tiporegrapagamento = $id_tiporegrapagamento;
    }

    public function setSt_tiporegrapagamento($st_tiporegrapagamento) {
        $this->st_tiporegrapagamento = $st_tiporegrapagamento;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

}

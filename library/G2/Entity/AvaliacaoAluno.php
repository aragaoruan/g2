<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoaluno")
 * @Entity(repositoryClass="\G2\Repository\AvaliacaoAluno")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @EntityLog
 */
class AvaliacaoAluno extends G2Entity
{

    /**
     * @var integer $id_avaliacaoaluno
     * @Column(name="id_avaliacaoaluno",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoaluno;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_avaliacao
     * @ManyToOne(targetEntity="Avaliacao")
     * @JoinColumn(name="id_avaliacao", referencedColumnName="id_avaliacao")
     */
    private $id_avaliacao;

    /**
     * @var integer $id_avaliacaoagendamento
     * @ManyToOne(targetEntity="AvaliacaoAgendamento")
     * @JoinColumn(name="id_avaliacaoagendamento", referencedColumnName="id_avaliacaoagendamento")
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $st_nota
     * @Column(name="st_nota", type="integer", nullable=true)
     */
    private $st_nota;

    /**
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     * @var datetime2 $dt_cadastro
     */
    private $dt_cadastro;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_sistemaimportacao
     * @Column(name="id_sistemaimportacao", type="integer", nullable=true)
     */
    private $id_sistemaimportacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var date $dt_avaliacao
     * @Column(name="dt_avaliacao", type="date", nullable=true)
     */
    private $dt_avaliacao;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariorevisor
     * @Column(name="id_usuariorevisor", type="integer", nullable=true)
     */
    private $id_usuariorevisor;

    /**
     * @var integer $id_avaliacaoalunoorigem
     * @Column(name="id_avaliacaoalunoorigem", type="integer", nullable=true)
     */
    private $id_avaliacaoalunoorigem;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true)
     */
    private $id_upload;

    /**
     * @Column(name="st_tituloavaliacao", type="string",length=500, nullable=true)
     */
    private $st_tituloavaliacao;

    /**
     * @var date $dt_defesa
     * @Column(name="dt_defesa", type="date", nullable=true)
     */
    private $dt_defesa;

    /**
     * @var integer $id_tiponota
     * @Column(name="id_tiponota", type="integer", nullable=false)
     */
    private $id_tiponota;

    /**
     * @Column(name="st_justificativa", type="string", length=2000 ,nullable=true)
     */
    private $st_justificativa;

    /**
     * @var Usuario $id_usuarioalteracao
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioalteracao", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuarioalteracao;

    /**
     * @var datetime $dt_alteracao
     * @Column(name="dt_alteracao", type="datetime", nullable=true)
     */
    private $dt_alteracao;


    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var integer $id_notaconceitual
     * @ManyToOne(targetEntity="NotaConceitual")
     * @JoinColumn(name="id_notaconceitual", referencedColumnName="id_notaconceitual")
     */
    private $id_notaconceitual;

    public function getId_avaliacaoaluno()
    {
        return $this->id_avaliacaoaluno;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    public function getSt_nota()
    {
        return $this->st_nota;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getId_sistemaimportacao()
    {
        return $this->id_sistemaimportacao;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getDt_avaliacao()
    {
        return $this->dt_avaliacao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuariorevisor()
    {
        return $this->id_usuariorevisor;
    }

    public function getId_avaliacaoalunoorigem()
    {
        return $this->id_avaliacaoalunoorigem;
    }

    public function getId_upload()
    {
        return $this->id_upload;
    }

    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    public function getDt_defesa()
    {
        return $this->dt_defesa;
    }

    public function getId_tiponota()
    {
        return $this->id_tiponota;
    }

    public function getSt_justificativa()
    {
        return $this->st_justificativa;
    }

    public function getId_usuarioalteracao()
    {
        return $this->id_usuarioalteracao;
    }

    public function getDt_alteracao()
    {
        return $this->dt_alteracao;
    }

    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    public function setId_avaliacaoaluno($id_avaliacaoaluno)
    {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setSt_nota($st_nota)
    {
        $this->st_nota = $st_nota;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_sistemaimportacao($id_sistemaimportacao)
    {
        $this->id_sistemaimportacao = $id_sistemaimportacao;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setDt_avaliacao($dt_avaliacao)
    {
        $this->dt_avaliacao = $dt_avaliacao;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_usuariorevisor($id_usuariorevisor)
    {
        $this->id_usuariorevisor = $id_usuariorevisor;
        return $this;
    }

    public function setId_avaliacaoalunoorigem($id_avaliacaoalunoorigem)
    {
        $this->id_avaliacaoalunoorigem = $id_avaliacaoalunoorigem;
        return $this;
    }

    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;
    }

    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
        return $this;
    }

    public function setDt_defesa($dt_defesa)
    {
        $this->dt_defesa = $dt_defesa;
        return $this;
    }

    public function setId_tiponota($id_tiponota)
    {
        $this->id_tiponota = $id_tiponota;
        return $this;
    }

    public function setSt_justificativa($st_justificativa)
    {
        $this->st_justificativa = $st_justificativa;
        return $this;
    }

    public function setId_usuarioalteracao(Usuario $id_usuarioalteracao)
    {
        $this->id_usuarioalteracao = $id_usuarioalteracao;
        return $this;
    }

    public function setDt_alteracao($dt_alteracao)
    {
        $this->dt_alteracao = $dt_alteracao;
        return $this;
    }

    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param int $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }


}

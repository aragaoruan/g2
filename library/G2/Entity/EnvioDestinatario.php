<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_enviodestinatario")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class EnvioDestinatario {

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_enviodestinatario
     * @Column(name="id_enviodestinatario", type="integer", nullable=false, length=4)
     */
    private $id_enviodestinatario;


    /**
     * @var integer $id_tipodestinatario
     * @ManyToOne(targetEntity="TipoDestinatario")
     * @JoinColumn(name="id_tipodestinatario", referencedColumnName="id_tipodestinatario")
     */
    private $id_tipodestinatario;


    /**
     * @var integer $id_enviomensagem
     * @ManyToOne(targetEntity="EnvioMensagem")
     * @JoinColumn(name="id_enviomensagem", referencedColumnName="id_enviomensagem")
     */
    private $id_enviomensagem;


    /**
     * @var integer $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;



    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;


    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;


    /**
     * @var integer $nu_telefone
     * @Column(name="nu_telefone", type="integer", nullable=true, length=9)
     */
    private $nu_telefone;


    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=100)
     */
    private $st_endereco;


    /**
     * @var string $st_nome
     * @Column(name="st_nome", type="string", nullable=true, length=100)
     */
    private $st_nome;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;



    public function getId_enviodestinatario(){
        return $this->id_enviodestinatario;
    }

    public function getId_tipodestinatario(){
        return $this->id_tipodestinatario;
    }

    public function getId_enviomensagem(){
        return $this->id_enviomensagem;
    }

    public function getId_usuario(){
        return $this->id_usuario;
    }

    public function getSt_endereco(){
        return $this->st_endereco;
    }

    public function getSt_nome(){
        return $this->st_nome;
    }


    public function setId_enviodestinatario($id_enviodestinatario){
        $this->id_enviodestinatario = $id_enviodestinatario;
        return $this;
    }

    public function setId_tipodestinatario(\G2\Entity\TipoDestinatario $id_tipodestinatario){
        $this->id_tipodestinatario = $id_tipodestinatario;
        return $this;
    }

    public function setId_enviomensagem(\G2\Entity\EnvioMensagem $id_enviomensagem){
        $this->id_enviomensagem = $id_enviomensagem;
        return $this;
    }

    public function setId_usuario(\G2\Entity\Usuario $id_usuario){
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setSt_endereco($st_endereco){
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function setSt_nome($st_nome){
        $this->st_nome = $st_nome;
        return $this;
    }

    public function getId_matricula() {
        return $this->id_matricula;
        return $this;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $nu_telefone
     */
    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
    }

    /**
     * @return int
     */
    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }



}

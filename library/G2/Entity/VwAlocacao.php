<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alocacao")
 * @Entity
 * @EntityView
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 */
class VwAlocacao extends G2Entity
{
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer")
     * @Id
     */
    private $id_alocacao;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer")
     */
    private $id_saladeaula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var boolean $bl_institucional
     * @Column(name="bl_institucional", type="boolean")
     */
    private $bl_institucional;

    /**
     * @var integer $nu_perfilalunoobs
     * @Column(name="nu_perfilalunoobs", type="integer")
     */
    private $nu_perfilalunoobs;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer")
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $bl_ativo
     * @Column(name="bl_ativo", type="integer")
     */
    private $bl_ativo;

    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getBl_institucional()
    {
        return $this->bl_institucional;
    }

    public function getNu_perfilalunoobs()
    {
        return $this->nu_perfilalunoobs;
    }

    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @return int
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

}

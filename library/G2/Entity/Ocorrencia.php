<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @SWG\Definition(
 *     required={},
 *     @SWG\Xml(name="Ocorrencia")
 * )
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_ocorrencia")
 * @Entity(repositoryClass="\G2\Repository\Ocorrencia")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @EntityLog
 */
class Ocorrencia extends G2Entity
{
    /**
     * @Id
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", length=4)
     * @GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(format="integer")
     */
    private $id_ocorrencia;

    /**
     * @var datetime2 $dt_atendimento
     * @Column(name="dt_atendimento", type="datetime2", nullable=true, length=3)
     * @SWG\Property(format="date")
     */
    private $dt_atendimento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     * @SWG\Property(format="date")
     */
    private $dt_cadastro;

    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     * @SWG\Property(format="integer")
     */
    private $id_matricula;

    /**
     * @var integer $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     * @SWG\Property(format="integer")
     */
    private $id_evolucao;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     * @SWG\Property(format="integer")
     */
    private $id_situacao;

    /**
     * @var integer $id_usuariointeressado
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariointeressado", referencedColumnName="id_usuario")
     * @SWG\Property(format="integer")
     */
    private $id_usuariointeressado;

    /**
     * @var integer $id_categoriaocorrencia
     * @ManyToOne(targetEntity="CategoriaOcorrencia")
     * @JoinColumn(name="id_categoriaocorrencia", referencedColumnName="id_categoriaocorrencia")
     * @SWG\Property(format="integer")
     */
    private $id_categoriaocorrencia;

    /**
     * @var integer $id_assuntoco
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoco", referencedColumnName="id_assuntoco")
     * @SWG\Property(format="integer")
     */
    private $id_assuntoco;

    /**
     * @var integer $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")
     * @SWG\Property(format="integer")
     */
    private $id_saladeaula;

    /**
     * @var integer $id_ocorrenciaoriginal
     * @ManyToOne(targetEntity="Ocorrencia")
     * @JoinColumn(name="id_ocorrenciaoriginal", referencedColumnName="id_ocorrencia")
     * @SWG\Property(format="integer")
     */
    private $id_ocorrenciaoriginal;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     * @SWG\Property(format="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     * @SWG\Property(format="integer")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_motivoocorrencia
     * @ManyToOne(targetEntity="MotivoOcorrencia")
     * @JoinColumn(name="id_motivoocorrencia", referencedColumnName="id_motivoocorrencia")
     * @SWG\Property(format="integer")
     */
    private $id_motivoocorrencia;

    /**
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=true)
     * @SWG\Property(format="string")
     */
    private $st_titulo;

    /**
     * @var string $st_codigorastreio
     * @Column(name="st_codigorastreio", type="string", nullable=true)
     * @SWG\Property(format="string")
     */
    private $st_codigorastreio;

    /**
     * @var string $st_ocorrencia
     * @Column(name="st_ocorrencia", type="string", nullable=true)
     * @SWG\Property(format="string")
     */
    private $st_ocorrencia;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true)
     * @SWG\Property(format="integer")
     */
    private $id_venda;


    /**
     * @var datetime2 $dt_cadastroocorrencia
     * @Column(name="dt_cadastroocorrencia", type="datetime2", nullable=true, length=8)
     * @SWG\Property(format="date")
     */
    private $dt_cadastroocorrencia;

    /**
     * @var datetime2 $dt_ultimotramite
     * @Column(name="dt_ultimotramite", type="datetime2", nullable=true, length=8)
     * @SWG\Property(format="date")
     */
    private $dt_ultimotramite;

    /**
     * @var string $st_codissue
     * @Column(name="st_codissue", type="string", nullable=true, length=50)
     * @SWG\Property(format="string")
     */
    private $st_codissue;
    /**
     * @var boolean $bl_confirmarciencia
     * @Column(name="bl_confirmarciencia", type="boolean", nullable=true)
     * @SWG\Property(format="boolean")
     */
    private $bl_confirmarciencia;


    public function getDt_atendimento()
    {
        return $this->dt_atendimento;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getId_ocorrenciaoriginal()
    {
        return $this->id_ocorrenciaoriginal;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getId_motivoocorrencia()
    {
        return $this->id_motivoocorrencia;
    }

    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    public function getSt_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    public function setDt_atendimento($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
        return $this;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function setId_ocorrenciaoriginal($id_ocorrenciaoriginal)
    {
        $this->id_ocorrenciaoriginal = $id_ocorrenciaoriginal;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_motivoocorrencia($id_motivoocorrencia)
    {
        $this->id_motivoocorrencia = $id_motivoocorrencia;
        return $this;
    }

    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
        return $this;
    }

    public function setSt_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
        return $this;
    }

    public function getDt_cadastroocorrencia()
    {
        return $this->dt_cadastroocorrencia;
    }

    public function setDt_cadastroocorrencia($dt_cadastroocorrencia)
    {
        $this->dt_cadastroocorrencia = $dt_cadastroocorrencia;
        return $this;
    }

    public function getId_venda()
    {
        return $this->id_venda;
    }

    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return string
     */
    public function getSt_codigorastreio()
    {
        return $this->st_codigorastreio;
    }

    /**
     * @param string $st_codigorastreio
     */
    public function setSt_codigorastreio($st_codigorastreio)
    {
        $this->st_codigorastreio = $st_codigorastreio;
    }

    /**
     * @return datetime2
     */
    public function getDt_ultimotramite()
    {
        return $this->dt_ultimotramite;
    }

    /**
     * @param datetime2 $dt_ultimotramite
     */
    public function setDt_ultimotramite($dt_ultimotramite)
    {
        $this->dt_ultimotramite = $dt_ultimotramite;
    }

    /**
     * @return string
     */
    public function getSt_codissue()
    {
        return $this->st_codissue;
    }

    /**
     * @param string $st_codissue
     */
    public function setSt_codissue($st_codissue)
    {
        $this->st_codissue = $st_codissue;
    }

    /**
     * @return bool
     */
    public function getBl_confirmarciencia()
    {
        return $this->bl_confirmarciencia;
    }

    /**
     * @param bool $bl_confirmarciencia
     */
    public function setBl_confirmarciencia($bl_confirmarciencia)
    {
        $this->bl_confirmarciencia = $bl_confirmarciencia;
    }



}

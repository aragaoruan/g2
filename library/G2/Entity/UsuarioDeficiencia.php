<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuariodeficiencia")
 * @Entity
 * @EntityLog
 */
class UsuarioDeficiencia extends G2Entity
{
    /**
     * @var int
     * @Column(name="id_usuariodeficiencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_usuariodeficiencia;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Deficiencia $id_deficiencia
     * @ManyToOne(targetEntity="Deficiencia")
     * @JoinColumn(name="id_deficiencia", nullable=false, referencedColumnName="id_deficiencia")
     */
    private $id_deficiencia;

    /**
     * @return int
     */
    public function getId_usuariodeficiencia()
    {
        return $this->id_usuariodeficiencia;
    }

    /**
     * @param int $id_usuariodeficiencia
     * @return $this
     */
    public function setId_usuariodeficiencia($id_usuariodeficiencia)
    {
        $this->id_usuariodeficiencia = $id_usuariodeficiencia;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return Deficiencia
     */
    public function getId_deficiencia()
    {
        return $this->id_deficiencia;
    }

    /**
     * @param Deficiencia $id_deficiencia
     * @return $this
     */
    public function setId_deficiencia($id_deficiencia)
    {
        $this->id_deficiencia = $id_deficiencia;
        return $this;
    }
}

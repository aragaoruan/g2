<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_categoria")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
use G2\G2Entity;

class VwCategoria extends G2Entity
{

    /**
     *
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_categoria;

    /**
     *
     * @var string $st_categoria
     * @Column(name="st_categoria", type="string", nullable=false, length=200)
     */
    private $st_categoria;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_categoriapai
     * @Column(name="id_categoriapai", type="integer", nullable=true)
     */
    private $id_categoriapai;

    /**
     *
     * @var string $st_categoriapai
     * @Column(name="st_categoriapai", type="string", nullable=true, length=200)
     */
    private $st_categoriapai;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=200)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false)
     */
    private $id_entidadecadastro;

    public function getId_categoria ()
    {
        return $this->id_categoria;
    }

    public function setId_categoria ($id_categoria)
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    public function getSt_categoria ()
    {
        return $this->st_categoria;
    }

    public function setSt_categoria ($st_categoria)
    {
        $this->st_categoria = $st_categoria;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_categoriapai ()
    {
        return $this->id_categoriapai;
    }

    public function setId_categoriapai ($id_categoriapai)
    {
        $this->id_categoriapai = $id_categoriapai;
        return $this;
    }

    public function getSt_categoriapai ()
    {
        return $this->st_categoriapai;
    }

    public function setSt_categoriapai ($st_categoriapai)
    {
        $this->st_categoriapai = $st_categoriapai;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

}
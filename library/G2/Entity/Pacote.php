<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pacote")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.ocm.br>
 */
class Pacote {

    /**
     *
     * @var integer $id_pacote
     * @Column(name="id_pacote", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pacote;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_lotematerial
     * @Column(name="id_lotematerial", type="integer", nullable=true, length=4)
     */
    private $id_lotematerial;

    /**
     * @return int
     */
    public function getId_pacote()
    {
        return $this->id_pacote;
    }

    /**
     * @param int $id_pacote
     */
    public function setId_pacote($id_pacote)
    {
        $this->id_pacote = $id_pacote;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getId_lotematerial()
    {
        return $this->id_lotematerial;
    }

    /**
     * @param int $id_lotematerial
     */
    public function setId_lotematerial($id_lotematerial)
    {
        $this->id_lotematerial = $id_lotematerial;
    }



}

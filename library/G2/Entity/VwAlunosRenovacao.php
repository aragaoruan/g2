<?php
/**
 * Created by PhpStorm.
 * User: Denise
 * Date: 01/02/2016
 * Time: 14:50
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunosrenovacao")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAlunosRenovacao
{

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;


    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true)
     */
    private $id_periodoletivo;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;


    /**
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=false)
     */
    private $id_vendaproduto;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=false)
     */
    private $id_entidadematricula;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true)
     */
    private $id_turma;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=true)
     */
    private $id_produto;

    /**
     * @var Venda $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     */
    private $id_venda;


    /**
     * @var integer $id_tiposelecao
     * @Column(name="id_tiposelecao", type="integer", nullable=true)
     */
    private $id_tiposelecao;

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=true)
     */
    private $id_contrato;

    /**
     * @var integer $id_contratoregra
     * @Column(name="id_contratoregra", type="integer", nullable=true)
     */
    private $id_contratoregra;

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAlunosRenovacao
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwAlunosRenovacao
     */
    public function setid_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAlunosRenovacao
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param int $id_periodoletivo
     * @return VwAlunosRenovacao
     */
    public function setid_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwAlunosRenovacao
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param int $id_vendaproduto
     * @return VwAlunosRenovacao
     */
    public function setid_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param int $id_entidadematricula
     * @return VwAlunosRenovacao
     */
    public function setid_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     * @return VwAlunosRenovacao
     */
    public function setid_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     * @return VwAlunosRenovacao
     */
    public function setid_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return Venda
     */
    public function getid_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param Venda $id_venda
     * @return VwAlunosRenovacao
     */
    public function setid_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param int $id_tiposelecao
     * @return VwAlunosRenovacao
     */
    public function setid_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @param int $id_contrato
     * @return VwAlunosRenovacao
     */
    public function setid_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param int $id_contratoregra
     * @return VwAlunosRenovacao
     */
    public function setid_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }



}

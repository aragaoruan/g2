<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtoprr")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class ProdutoPrr {

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", length=32, nullable=false)
     */
    private $id_saladeaula;

    /**
     * @Id
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", length=32, nullable=false)
     */
    private $id_produto;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", length=32, nullable=false)
     */
    private $id_entidade;

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
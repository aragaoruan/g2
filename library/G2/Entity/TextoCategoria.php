<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_textocategoria")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TextoCategoria
{

    /**
     * @var integer $id_textocategoria
     * @Column(name="id_textocategoria", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_textocategoria;

    /**
     * @var string $st_textocategoria
     * @Column(name="st_textocategoria", type="string", nullable=false, length=100)
     */
    private $st_textocategoria;

    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=200)
     */
    private $st_descricao;

    /**
     * @var string $st_metodo
     * @Column(name="st_metodo", type="string", nullable=true, length=100)
     */
    private $st_metodo;

    /**
     * @ManyToMany(targetEntity="TextoExibicao", inversedBy="textoCategoria")
     * @JoinTable(name="tb_textoexibicaocategoria",
     *  joinColumns={@JoinColumn(name="id_textocategoria", referencedColumnName="id_textocategoria")},
     *  inverseJoinColumns={@JoinColumn(name="id_textoexibicao", referencedColumnName="id_textoexibicao")}
     * )
     */
    private $textoExibicao;

    public function __construct ()
    {
        $this->textoExibicao = new ArrayCollection();
    }

    public function getTextoExibicao ()
    {
        return $this->textoExibicao;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria ($id_textocategoria)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getSt_textocategoria ()
    {
        return $this->st_textocategoria;
    }

    public function setSt_textocategoria ($st_textocategoria)
    {
        $this->st_textocategoria = $st_textocategoria;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getSt_metodo ()
    {
        return $this->st_metodo;
    }

    public function setSt_metodo ($st_metodo)
    {
        $this->st_metodo = $st_metodo;
        return $this;
    }

}
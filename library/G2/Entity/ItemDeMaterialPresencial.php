<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30/03/2015
 * Time: 16:50
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_itemdematerialpresencial")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class ItemDeMaterialPresencial {

    /**
     * @Id
     * @var integer $id_material
     * @Column(name="id_material", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_material;
    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;
    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;
    /**
     * @var integer $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", referencedColumnName="id_disciplina")
     */
    private $id_disciplina;
    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_professor
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_professor", referencedColumnName="id_usuario")
     */
    private $id_professor;
    /**
     * @var integer $id_tipomaterial
     * @ManyToOne(targetEntity="TipodeMaterial")
     * @JoinColumn(name="id_tipomaterial", referencedColumnName="id_tipodematerial")
     */
    private $id_tipomaterial;
    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var integer $nu_paginas
     * @Column(name="nu_paginas", type="integer", nullable=true, length=4)
     */
    private $nu_paginas;

    /**
     * @var integer $nu_encontro
     * @Column(name="nu_encontro", type="integer", nullable=true, length=4)
     */
    private $nu_encontro;

    /**
     * @var integer $nu_qtdestoque
     * @Column(name="nu_qtdestoque", type="integer", nullable=true, length=4)
     */
    private $nu_qtdestoque;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false, length=9)
     */
    private $nu_valor;
    /**
     * @var string $st_nomematerial
     * @Column(name="st_nomematerial", type="string", nullable=false, length=255)
     */
    private $st_nomematerial;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;
    /**
     * @var boolean $bl_portal
     * @Column(name="bl_portal", type="boolean", nullable=true, length=1)
     */
    private $bl_portal;

    /**
     * @return integer id_material
     */
    public function getId_material() {
        return $this->id_material;
    }

    /**
     * @param id_material
     */
    public function setId_material($id_material) {
        $this->id_material = $id_material;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_professor
     */
    public function getId_professor() {
        return $this->id_professor;
    }

    /**
     * @param id_professor
     */
    public function setId_professor($id_professor) {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @return integer id_tipomaterial
     */
    public function getId_tipomaterial() {
        return $this->id_tipomaterial;
    }

    /**
     * @param id_tipomaterial
     */
    public function setId_tipomaterial($id_tipomaterial) {
        $this->id_tipomaterial = $id_tipomaterial;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer nu_paginas
     */
    public function getNu_paginas() {
        return $this->nu_paginas;
    }

    /**
     * @param nu_paginas
     */
    public function setNu_paginas($nu_paginas) {
        $this->nu_paginas = $nu_paginas;
        return $this;
    }

    /**
     * @return integer id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return decimal nu_valor
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @param nu_valor
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return string st_nomematerial
     */
    public function getSt_nomematerial() {
        return $this->st_nomematerial;
    }

    /**
     * @param st_nomematerial
     */
    public function setSt_nomematerial($st_nomematerial) {
        $this->st_nomematerial = $st_nomematerial;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_encontro()
    {
        return $this->nu_encontro;
    }

    /**
     * @param int $nu_encontro
     */
    public function setNu_encontro($nu_encontro)
    {
        $this->nu_encontro = $nu_encontro;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_portal()
    {
        return $this->bl_portal;
    }

    /**
     * @param boolean $bl_portal
     */
    public function setBl_portal($bl_portal)
    {
        $this->bl_portal = $bl_portal;
    }

    /**
     * @return int
     */
    public function getNu_qtdestoque()
    {
        return $this->nu_qtdestoque;
    }

    /**
     * @param int $nu_qtdestoque
     */
    public function setNu_qtdestoque($nu_qtdestoque)
    {
        $this->nu_qtdestoque = $nu_qtdestoque;
    }






}
<?php

/*
 * Entity HorarioAula
 * @author: Paulo Silva <paulo.silva@unyleya.com.br>
 * @since: 2013-11-20
 */

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_horarioaula")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class HorarioAula {

    /**
     *
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_horarioaula;

    /**
     *
     * @var integer $id_codhorarioacesso
     * @Column(name="id_codhorarioacesso", type="integer",nullable=true)
     */
    private $id_codhorarioacesso;


    /**
     * @var string $st_codhorarioacesso
     * @Column(name="st_codhorarioacesso", type="string", nullable=true, length=100)
     */
    private $st_codhorarioacesso;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_horarioaula")
     * @var string
     */
    private $st_horarioaula;

    /**
     * @var $hr_inicio
     * @Column(name="hr_inicio", type="datetime2",nullable=false)
     */
    private $hr_inicio;

    /**
     * @var $hr_fim
     * @Column(name="hr_fim", type="datetime2",nullable=false)
     */
    private $hr_fim;

    /**
     * @var Turno $id_turno
     * @ManyToOne(targetEntity="Turno")
     * @JoinColumn(name="id_turno", referencedColumnName="id_turno")
     */
    private $id_turno;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var $diaSemana
     * @ManyToMany(targetEntity="DiaSemana", mappedBy="horarioAula")
     */
    private $diaSemana;

    /**
     * @var TipoAula $id_tipoaula
     * @ManyToOne(targetEntity="TipoAula")
     * @JoinColumn(name="id_tipoaula", referencedColumnName="id_tipoaula")
     */
    private $id_tipoaula;

    /**
     * @return string
     */
    public function getst_codhorarioacesso()
    {
        return $this->st_codhorarioacesso;
    }

    /**
     * @param string $st_codhorarioacesso
     */
    public function setst_codhorarioacesso($st_codhorarioacesso)
    {
        $this->st_codhorarioacesso = $st_codhorarioacesso;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_codhorarioacesso()
    {
        return $this->id_codhorarioacesso;
    }

    /**
     * @param mixed $id_codhorarioacesso
     */
    public function setid_codhorarioacesso($id_codhorarioacesso)
    {
        $this->id_codhorarioacesso = $id_codhorarioacesso;
        return $this;
    }


    public function __construct() {
        $this->diaSemana = new ArrayCollection();
    }

    public function getDiaSemana() {
        return $this->diaSemana;
    }



    public function getId_horarioaula() {
        return $this->id_horarioaula;
    }

    public function getSt_horarioaula() {
        return $this->st_horarioaula;
    }

    public function getHr_inicio() {
        return $this->hr_inicio;
    }

    public function getHr_fim() {
        return $this->hr_fim;
    }

    public function getId_turno() {
        return $this->id_turno;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_horarioaula($id_horarioaula) {
        $this->id_horarioaula = $id_horarioaula;
    }

    public function setSt_horarioaula($st_horarioaula) {
        $this->st_horarioaula = $st_horarioaula;
    }

    public function setHr_inicio($hr_inicio) {
        $this->hr_inicio = $hr_inicio;
    }

    public function setHr_fim($hr_fim) {
        $this->hr_fim = $hr_fim;
    }

    public function setId_turno($id_turno) {
        $this->id_turno = $id_turno;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return TipoAula
     */
    public function getId_tipoaula()
    {
        return $this->id_tipoaula;
    }

    /**
     * @param TipoAula $id_tipoaula
     */
    public function setId_tipoaula($id_tipoaula)
    {
        $this->id_tipoaula = $id_tipoaula;
    }



}

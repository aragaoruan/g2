<?php

namespace G2\Entity;


/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_compartilhadados")
 * @author Kayo Silva <kayo.silva@unyelaya.com.br>
 * @since 2016-08-08
 */
class CompartilhaDados
{

    /**
     * @var integer $id_entidadeorigem
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_entidadeorigem", type="integer", nullable=false)
     */
    private $id_entidadeorigem;

    /**
     * @var integer $id_entidadedestino
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_entidadedestino", type="integer", nullable=false)
     */
    private $id_entidadedestino;

    /**
     * @var integer $id_tipodados
     * @Column(name="id_tipodados", type="integer", nullable=false)
     */
    private $id_tipodados;


    /**
     * @param $id_entidadeorigem
     * @return $this
     */
    public function setId_entidadeorigem($id_entidadeorigem)
    {
        $this->id_entidadeorigem = $id_entidadeorigem;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeorigem()
    {
        return $this->id_entidadeorigem;
    }

    /**
     * @param $id_entidadedestino
     * @return $this
     */
    public function setId_entidadedestino($id_entidadedestino)
    {
        $this->id_entidadedestino = $id_entidadedestino;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadedestino()
    {
        return $this->id_entidadedestino;
    }

    /**
     * @param $id_tipodados
     * @return $this
     */
    public function setId_tipodados($id_tipodados)
    {
        $this->id_tipodados = $id_tipodados;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodados()
    {
        return $this->id_tipodados;
    }


    /**
     * Retorn os dados em formato de array
     * @return array
     */
    public function toArray()
    {
        return array(
            'id_entidadeorigem' => $this->getId_entidadeorigem(),
            'id_entidadedestino' => $this->getId_entidadedestino(),
            'id_tipodados' => $this->getId_tipodados()
        );
    }

}
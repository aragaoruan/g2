<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_contrato")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Contrato
{

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_contrato;

    /**
     * @var date $dt_ativacao
     * @Column(name="dt_ativacao", type="date", nullable=true, length=3)
     */
    private $dt_ativacao;

    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true, length=3)
     */
    private $dt_termino;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var Evolucao $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", nullable=false, referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Venda $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", nullable=true, referencedColumnName="id_venda")
     */
    private $id_venda;

    /**
     * @var ContratoRegra $id_contratoregra
     * @ManyToOne(targetEntity="ContratoRegra")
     * @JoinColumn(name="id_contratoregra", nullable=true, referencedColumnName="id_contratoregra")
     */
    private $id_contratoregra;

    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=true, length=4)
     */
    private $id_textosistema;

    /**
     * @var integer $nu_codintegracao
     * @Column(name="nu_codintegracao", type="integer", nullable=true, length=4)
     */
    private $nu_codintegracao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var decimal $nu_bolsa
     * @Column(name="nu_bolsa", type="decimal", nullable=true, length=9)
     */
    private $nu_bolsa;


    /**
     * @var date $dt_entregadocumentacao
     * @Column(name="dt_entregadocumentacao", type="date", nullable=true, length=3)
     */
    private $dt_entregadocumentacao;


    /**
     * @var boolean $bl_documentacaocompleta
     * @Column(name="bl_documentacaocompleta", type="boolean", nullable=false, length=1)
     */
    private $bl_documentacaocompleta;

    /**
     * 
     * @return integer
     */
    public function getId_contrato ()
    {
        return $this->id_contrato;
    }

    /**
     * 
     * @return date
     */
    public function getDt_ativacao ()
    {
        return $this->dt_ativacao;
    }

    /**
     * 
     * @return date
     */
    public function getDt_termino ()
    {
        return $this->dt_termino;
    }

    /**
     * 
     * @return datime2
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * 
     * @return integer
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * 
     * @return integer
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @return integer
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * 
     * @return \G2\Entity\Evolucao
     */
    public function getId_evolucao ()
    {
        return $this->id_evolucao;
    }

    /**
     * 
     * @return \G2\Entity\Situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * 
     * @return \G2\Entity\Venda
     */
    public function getId_venda ()
    {
        return $this->id_venda;
    }

    /**
     * 
     * @return \G2\Entity\ContratoRegra
     */
    public function getId_contratoregra ()
    {
        return $this->id_contratoregra;
    }

    /**
     * 
     * @return integer
     */
    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    /**
     * 
     * @return integer
     */
    public function getNu_codintegracao ()
    {
        return $this->nu_codintegracao;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * 
     * @return decimal
     */
    public function getNu_bolsa ()
    {
        return $this->nu_bolsa;
    }

    /**
     * 
     * @param integer $id_contrato
     * @return \G2\Entity\Contrato
     */
    public function setId_contrato ($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     * 
     * @param date $dt_ativacao
     * @return \G2\Entity\Contrato
     */
    public function setDt_ativacao ($dt_ativacao)
    {
        $this->dt_ativacao = $dt_ativacao;
        return $this;
    }

    /**
     * 
     * @param date $dt_termino
     * @return \G2\Entity\Contrato
     */
    public function setDt_termino ($dt_termino)
    {
        $this->dt_termino = $dt_termino;
        return $this;
    }

    /**
     * 
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\Contrato
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * 
     * @param integer $id_usuario
     * @return \G2\Entity\Contrato
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * 
     * @param type $id_entidade
     * @return \G2\Entity\Contrato
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * 
     * @param integer $id_usuariocadastro
     * @return \G2\Entity\Contrato
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\Evolucao $id_evolucao
     * @return \G2\Entity\Contrato
     */
    public function setId_evolucao (Evolucao $id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\Contrato
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\Venda $id_venda
     * @return \G2\Entity\Contrato
     */
    public function setId_venda (Venda $id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * 
     * @param \G2\Entity\ContratoRegra $id_contratoregra
     * @return \G2\Entity\Contrato
     */
    public function setId_contratoregra (ContratoRegra $id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }

    /**
     * 
     * @param integer $id_textosistema
     * @return \G2\Entity\Contrato
     */
    public function setId_textosistema ($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * 
     * @param integer $nu_codintegracao
     * @return \G2\Entity\Contrato
     */
    public function setNu_codintegracao ($nu_codintegracao)
    {
        $this->nu_codintegracao = $nu_codintegracao;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_ativo
     * @return \G2\Entity\Contrato
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_bolsa
     * @return \G2\Entity\Contrato
     */
    public function setNu_bolsa ($nu_bolsa)
    {
        $this->nu_bolsa = $nu_bolsa;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_entregadocumentacao()
    {
        return $this->dt_entregadocumentacao;
    }

    /**
     * @param mixed $dt_entregadocumentacao
     */
    public function setDt_entregadocumentacao($dt_entregadocumentacao)
    {
        $this->dt_entregadocumentacao = $dt_entregadocumentacao;
    }

    /**
     * @return boolean
     */
    public function getBl_documentacaocompleta()
    {
        return $this->bl_documentacaocompleta;
    }

    /**
     * @param boolean $bl_documentacaocompleta
     */
    public function setBl_documentacaocompleta($bl_documentacaocompleta)
    {
        $this->bl_documentacaocompleta = $bl_documentacaocompleta;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipopessoa")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoPessoa
{

    /**
     *
     * @var integer $id_tipopessoa 
     * @Column(name="id_tipopessoa", type="integer", nullable=false)
     * @ID
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipopessoa;

    /**
     *
     * @var string $st_tipopessoa 
     * @Column(name="st_tipopessoa", type="string", nullable=false, length=20)
     */
    private $st_tipopessoa;

    public function getId_tipopessoa ()
    {
        return $this->id_tipopessoa;
    }

    public function setId_tipopessoa ($id_tipopessoa)
    {
        $this->id_tipopessoa = $id_tipopessoa;
        return $this;
    }

    public function getSt_tipopessoa ()
    {
        return $this->st_tipopessoa;
    }

    public function setSt_tipopessoa ($st_tipopessoa)
    {
        $this->st_tipopessoa = $st_tipopessoa;
        return $this;
    }

}
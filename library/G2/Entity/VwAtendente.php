<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 25-03-2015
 * Time: 15:14
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_atendentes")
 * @Entity(repositoryClass="\G2\Repository\VwAtendente")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @EntityView
 */


class VwAtendente
{
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_nucleofuncionariotm
     * @Column(name="id_nucleofuncionariotm", type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_nucleofuncionariotm;
    /**
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=true, length=4)
     */
    private $id_funcao;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_nucleotm
     * @Column(name="id_nucleotm", type="integer", nullable=true, length=4)
     */
    private $id_nucleotm;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_nucleotm
     * @Column(name="st_nucleotm", type="string", nullable=false, length=150)
     */
    private $st_nucleotm;
    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=true, length=100)
     */
    private $st_funcao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @Id
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $st_cpf;
    /**
     * @var string $id_entidadematriz
     * @Column(name="id_entidadematriz", type="string", nullable=false)
     */
    private $id_entidadematriz;

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_nucleofuncionariotm
     */
    public function getId_nucleofuncionariotm()
    {
        return $this->id_nucleofuncionariotm;
    }

    /**
     * @param id_nucleofuncionariotm
     */
    public function setId_nucleofuncionariotm($id_nucleofuncionariotm)
    {
        $this->id_nucleofuncionariotm = $id_nucleofuncionariotm;
        return $this;
    }

    /**
     * @return integer id_funcao
     */
    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param id_funcao
     */
    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_nucleotm
     */
    public function getId_nucleotm()
    {
        return $this->id_nucleotm;
    }

    /**
     * @param id_nucleotm
     */
    public function setId_nucleotm($id_nucleotm)
    {
        $this->id_nucleotm = $id_nucleotm;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_nucleotm
     */
    public function getSt_nucleotm()
    {
        return $this->st_nucleotm;
    }

    /**
     * @param st_nucleotm
     */
    public function setSt_nucleotm($st_nucleotm)
    {
        $this->st_nucleotm = $st_nucleotm;
        return $this;
    }

    /**
     * @return string st_funcao
     */
    public function getSt_funcao()
    {
        return $this->st_funcao;
    }

    /**
     * @param st_funcao
     */
    public function setSt_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @param id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
        return $this;
    }


}
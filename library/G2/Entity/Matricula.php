<?php

namespace G2\Entity;

use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_matricula")
 * @Entity(repositoryClass="\G2\Repository\Matricula")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @EntityLog
 */
class Matricula extends G2Entity
{

    const APTO_FINAL = 120;
    const NAO_APTO = 119;

    /**
     * @Id
     * @var integer $id_matricula
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var date $dt_concluinte
     * @Column(name="dt_concluinte", type="date", nullable=true, length=3)
     */
    private $dt_concluinte;

    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true, length=3)
     */
    private $dt_termino;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicio;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true, length=4)
     */
    private $id_periodoletivo;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Evolucao $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var Aproveitamento $id_aproveitamento
     * @ManyToOne(targetEntity="Aproveitamento")
     * @JoinColumn(name="id_aproveitamento", referencedColumnName="id_aproveitamento")
     */
    private $id_aproveitamento;


    /**
     * @var integer $id_matriculaorigem
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matriculaorigem", referencedColumnName="id_matricula")
     */
    private $id_matriculaorigem;

    /**
     * @var integer $id_situacaoagendamento
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacaoagendamento", referencedColumnName="id_situacao")
     */
    private $id_situacaoagendamento;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * Relacionamento nao criado devido o pouco tempo para construcao da
     * funcionalidade favor criar caso necessario.
     *
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=true, length=4)
     */
    private $id_vendaproduto;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $id_entidadematriz
     * @Column(name="id_entidadematriz", type="integer", nullable=true, length=4)
     */
    private $id_entidadematriz;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true, length=4)
     */
    private $id_entidadematricula;

    /**
     * @var Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * Relacionamento nao criado devido o pouco tempo para construcao da
     * funcionalidade favor criar caso necessario.
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_institucional
     * @Column(name="bl_institucional", type="boolean", nullable=false, length=1, options={"default":false})
     */
    private $bl_institucional;

    /**
     * @var integer $id_mensagemrecuperacao
     * @Column(name="id_mensagemrecuperacao", type="integer", nullable=true, length=4)
     */
    private $id_mensagemrecuperacao;

    /**
     * @var integer $id_evolucaoagendamento
     * @Column(name="id_evolucaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_evolucaoagendamento;

    /**
     * @var Cancelamento $id_cancelamento
     * @ManyToOne(targetEntity="Cancelamento")
     * @JoinColumn(name="id_cancelamento", referencedColumnName="id_cancelamento")
     */
    private $id_cancelamento;

    /**
     * @var boolean $bl_documentacao
     * @Column(name="bl_documentacao", type="boolean", nullable=true,  options={"default":NULL})
     */
    private $bl_documentacao;

    /**
     * @var string $st_codcertificacao
     * @Column(name="st_codcertificacao", type="string", nullable=true, length=200)
     */
    private $st_codcertificacao;

    /**
     * @var Evolucao $id_evolucaocertificacao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucaocertificacao", referencedColumnName="id_evolucao")
     */
    private $id_evolucaocertificacao;

    /**
     * @var Matricula $id_matriculavinculada
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matriculavinculada", referencedColumnName="id_matricula", nullable=true)
     */
    private $id_matriculavinculada;

    /**
     * @var datetime2 $dt_ultimoacesso
     * @Column(name="dt_ultimoacesso", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_ultimoacesso;

    /**
     * @var boolean $bl_academico
     * @Column(name="bl_academico", type="boolean", nullable=true, options={"default":NULL})
     */
    private $bl_academico;

    /**
     * @var datetime2 $dt_aptoagendamento
     * @Column(name="dt_aptoagendamento", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_aptoagendamento;

    /**
     * @var Turma $id_turmaorigem
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turmaorigem", referencedColumnName="id_turma")
     */
    private $id_turmaorigem;

    /**
     * @var Situacao $id_situacaoagendamentoparcial
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacaoagendamentoparcial", referencedColumnName="id_situacao")
     */
    private $id_situacaoagendamentoparcial;

    /**
     * @var Evolucao $id_evolucaodiplomacao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucaodiplomacao", referencedColumnName="id_evolucao")
     */
    private $id_evolucaodiplomacao;

    /**
     * @var integer $nu_semestreatual
     * @Column(name="nu_semestreatual", type="integer", nullable=true, length=1)
     */
    private $nu_semestreatual;

    /**
     * @var \DateTime $dt_ultimaoferta
     * @Column(name="dt_ultimaoferta", type="datetime", nullable=true)
     */
    private $dt_ultimaoferta;

    /**
     * @var datetime2 $dt_matriculavinculada
     * @Column(name="dt_matriculavinculada", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_matriculavinculada;

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getDt_concluinte()
    {
        return $this->dt_concluinte;
    }

    public function setDt_concluinte($dt_concluinte)
    {
        $this->dt_concluinte = $dt_concluinte;
        return $this;
    }

    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function getId_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    public function setId_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
        return $this;
    }

    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
        return $this;
    }

    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    public function getId_turma()
    {
        return $this->id_turma;
    }

    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getBl_institucional()
    {
        return $this->bl_institucional;
    }

    public function setBl_institucional($bl_institucional)
    {
        $this->bl_institucional = $bl_institucional;
        return $this;
    }

    public function getId_mensagemrecuperacao()
    {
        return $this->id_mensagemrecuperacao;
    }

    public function setId_mensagemrecuperacao($id_mensagemrecuperacao)
    {
        $this->id_mensagemrecuperacao = $id_mensagemrecuperacao;
    }

    public function getId_evolucaoagendamento()
    {
        return $this->id_mensagemrecuperacao;
    }

    public function setId_evolucaoagendamento($id_evolucaoagendamento)
    {
        $this->id_evolucaoagendamento = $id_evolucaoagendamento;
        return $this;
    }

    public function getBl_documentacao()
    {
        return $this->bl_documentacao;
    }

    public function setBl_documentacao($bl_documentacao)
    {
        $this->bl_documentacao = $bl_documentacao;
        return $this;
    }

    /**
     * @return string st_codcertificacao
     */
    public function getSt_codcertificacao()
    {
        return $this->st_codcertificacao;
    }


    /**
     * @param st_codcertificacao
     * @return \G2\Entity\Matricula
     */
    public function setSt_codcertificacao($st_codcertificacao)
    {
        $this->st_codcertificacao = $st_codcertificacao;
        return $this;
    }

    /**
     * @return int id_cancelamento
     */
    public function getId_Cancelamento()
    {
        return $this->id_cancelamento;
    }


    /**
     * @param int $id_cancelamento
     */
    public function setId_Cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
    }

    /**
     * @return int
     */
    public function getId_evolucaocertificacao()
    {
        return $this->id_evolucaocertificacao;
    }

    /**
     * @param int $id_evolucaocertificacao
     * @return \G2\Entity\Matricula
     */
    public function setId_evolucaocertificacao($id_evolucaocertificacao)
    {
        $this->id_evolucaocertificacao = $id_evolucaocertificacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matriculavinculada()
    {
        return $this->id_matriculavinculada;
    }

    /**
     * @param int $id_matriculavinculada
     * @return \G2\Entity\Matricula
     */
    public function setId_matriculavinculada($id_matriculavinculada)
    {
        $this->id_matriculavinculada = $id_matriculavinculada;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_ultimoacesso()
    {
        return $this->dt_ultimoacesso;
    }

    /**
     * @param datetime2 $dt_ultimoacesso
     * @return \G2\Entity\Matricula
     */
    public function setDt_ultimoacesso($dt_ultimoacesso)
    {
        $this->dt_ultimoacesso = $dt_ultimoacesso;
        return $this;
    }

    public function getBl_academico()
    {
        return $this->bl_academico;
    }

    public function setBl_academico($bl_academico)
    {
        $this->bl_academico = $bl_academico;
    }

    /**
     * @return Aproveitamento
     */
    public function getId_aproveitamento()
    {
        return $this->id_aproveitamento;
    }

    /**
     * @param Aproveitamento $id_aproveitamento
     */
    public function setId_aproveitamento($id_aproveitamento)
    {
        $this->id_aproveitamento = $id_aproveitamento;
    }

    /**
     * @return datetime2
     */
    public function getDt_aptoagendamento()
    {
        return $this->dt_aptoagendamento;
    }

    /**
     * @param datetime2 $dt_aptoagendamento
     * @return $this
     */
    public function setDt_aptoagendamento($dt_aptoagendamento)
    {
        $this->dt_aptoagendamento = $dt_aptoagendamento;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacaoagendamentoparcial()
    {
        return $this->id_situacaoagendamentoparcial;
    }

    /**
     * @param Situacao $id_situacaoagendamentoparcial
     */
    public function setId_situacaoagendamentoparcial($id_situacaoagendamentoparcial)
    {
        $this->id_situacaoagendamentoparcial = $id_situacaoagendamentoparcial;
    }

    /**
     * @return Turma
     */
    public function getId_turmaorigem()
    {
        return $this->id_turmaorigem;
    }

    /**
     * @param Turma $id_turmaorigem
     * @return $this
     */
    public function setId_turmaorigem($id_turmaorigem)
    {
        $this->id_turmaorigem = $id_turmaorigem;
        return $this;
    }

    /**
     * @return Evolucao
     */
    public function getId_evolucaodiplomacao()
    {
        return $this->id_evolucaodiplomacao;
    }

    /**
     * @param $id_evolucaodiplomacao
     */
    public function setId_evolucaodiplomacao($id_evolucaodiplomacao)
    {
        $this->id_evolucaodiplomacao = $id_evolucaodiplomacao;
    }

    /**
     * @return int
     */
    public function getNu_semestreatual()
    {
        return $this->nu_semestreatual;
    }
    /**
     * @return \DateTime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param \DateTime $dt_ultimaoferta
     * @return $this
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }
    public function setNu_semestreatual($nu_semestreatual)
    {
        $this->nu_semestreatual = $nu_semestreatual;
    }

    /**
     * @return datetime2
     */
    public function getDt_matriculavinculada()
    {
        return $this->dt_matriculavinculada;
    }

    /**
     * @param datetime2 $dt_matriculavinculada
     */
    public function setDt_matriculavinculada($dt_matriculavinculada)
    {
        $this->dt_matriculavinculada = $dt_matriculavinculada;
    }


}

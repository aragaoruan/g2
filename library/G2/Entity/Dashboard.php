<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_dashboard")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-03-30
 */
class Dashboard {

    /**
     *
     * @var integer $id_dashboard
     * @Column(name="id_dashboard", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_dashboard;
    /**
     * @var string $st_dashboard
     * @Column(name="st_dashboard", type="string", nullable=false, length=250)
     */
    private $st_dashboard;
    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true)
     */
    private $st_descricao;
    /**
     * @var string $st_origem
     * @Column(name="st_origem", type="string", nullable=true)
     */
    private $st_origem;
    /**
     * @var string $st_xtipo
     * @Column(name="st_xtipo", type="string", nullable=true)
     */
    private $st_xtipo;
    /**
     * @var string $st_xtitulo
     * @Column(name="st_xtitulo", type="string", nullable=true)
     */
    private $st_xtitulo;
    /**
     * @var string $st_ytipo
     * @Column(name="st_ytipo", type="string", nullable=true)
     */
    private $st_ytipo;
    /**
     * @var string $st_ytitulo
     * @Column(name="st_ytitulo", type="string", nullable=true)
     */
    private $st_ytitulo;
    /**
     * @var string $st_campolabel
     * @Column(name="st_campolabel", type="string", nullable=true)
     */
    private $st_campolabel;
    /**
     * @var string $st_tipochart
     * @Column(name="st_tipochart", type="string", nullable=true)
     */
    private $st_tipochart;
    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var Funcionalidade $id_funcionalidade
     * @ManyToOne(targetEntity="Funcionalidade")
     * @JoinColumn(name="id_funcionalidade", nullable=true, referencedColumnName="id_funcionalidade")
     */
    private $id_funcionalidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return int
     */
    public function getId_dashboard()
    {
        return $this->id_dashboard;
    }

    /**
     * @param int $id_dashboard
     * @return Dashboard
     */
    public function setId_dashboard($id_dashboard)
    {
        $this->id_dashboard = $id_dashboard;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_dashboard()
    {
        return $this->st_dashboard;
    }

    /**
     * @param string $st_dashboard
     * @return Dashboard
     */
    public function setSt_dashboard($st_dashboard)
    {
        $this->st_dashboard = $st_dashboard;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return Dashboard
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_origem()
    {
        return $this->st_origem;
    }

    /**
     * @param string $st_origem
     * @return Dashboard
     */
    public function setSt_origem($st_origem)
    {
        $this->st_origem = $st_origem;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tipochart()
    {
        return $this->st_tipochart;
    }

    /**
     * @param string $st_tipochart
     * @return Dashboard
     */
    public function setSt_tipochart($st_tipochart)
    {
        $this->st_tipochart = $st_tipochart;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_campolabel()
    {
        return $this->st_campolabel;
    }

    /**
     * @param string $st_campolabel
     * @return Dashboard
     */
    public function setSt_campolabel($st_campolabel)
    {
        $this->st_campolabel = $st_campolabel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     * @return Dashboard
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_ytitulo()
    {
        return $this->st_ytitulo;
    }

    /**
     * @param string $st_ytitulo
     */
    public function setSt_ytitulo($st_ytitulo)
    {
        $this->st_ytitulo = $st_ytitulo;
    }

    /**
     * @return string
     */
    public function getSt_xtipo()
    {
        return $this->st_xtipo;
    }

    /**
     * @param string $st_xtipo
     */
    public function setSt_xtipo($st_xtipo)
    {
        $this->st_xtipo = $st_xtipo;
    }

    /**
     * @return string
     */
    public function getSt_xtitulo()
    {
        return $this->st_xtitulo;
    }

    /**
     * @param string $st_xtitulo
     */
    public function setSt_xtitulo($st_xtitulo)
    {
        $this->st_xtitulo = $st_xtitulo;
    }

    /**
     * @return string
     */
    public function getSt_ytipo()
    {
        return $this->st_ytipo;
    }

    /**
     * @param string $st_ytipo
     */
    public function setSt_ytipo($st_ytipo)
    {
        $this->st_ytipo = $st_ytipo;
    }

    /**
     * @return Funcionalidade
     */
    public function getId_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @param Funcionalidade $id_funcionalidade
     */
    public function setId_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
    }





}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pautaalunos")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwPautaAlunos
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     * @Id
     */
    private $id_disciplina;


    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_aluno
     * @Column(name="st_aluno", type="string", nullable=false, length=300)
     */
    private $st_aluno;


    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;


    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=true, length=300)
     */
    private $st_professor;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;


    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getSt_aluno()
    {
        return $this->st_aluno;
    }

    public function setSt_aluno($st_aluno)
    {
        $this->st_aluno = $st_aluno;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_professor()
    {
        return $this->st_professor;
    }

    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


}
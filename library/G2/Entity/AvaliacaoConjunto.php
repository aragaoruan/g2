<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoconjunto")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvaliacaoConjunto {

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoconjunto;
    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipocalculoavaliacao;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var string $st_avaliacaoconjunto
     * @Column(name="st_avaliacaoconjunto", type="string", nullable=false, length=100)
     */
    private $st_avaliacaoconjunto;
    
    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    public function getSt_avaliacaoconjunto() {
        return $this->st_avaliacaoconjunto;
    }

    public function getId_tipoprova() {
        return $this->id_tipoprova;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_tipocalculoavaliacao() {
        return $this->id_tipocalculoavaliacao;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
        $this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
        return $this;

    }

    public function setId_tipoprova($id_tipoprova) {
        $this->id_tipoprova = $id_tipoprova;
        return $this;

    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;

    }

    public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
        return $this;

    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;

    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;

    }


}

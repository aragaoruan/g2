<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_ocorrenciaspessoageral")
 * @Entity
 * @EntityView
 */
class VwOcorrenciasPessoaGeral extends G2Entity
{

    /**
     *
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=false)
     * @Id
     */
    private $id_ocorrencia;

    /**
     *
     * @var string $st_nomeinteressado
     * @Column(name="st_nomeinteressado", type="string", nullable=false, length=300)
     */
    private $st_nomeinteressado;

    /**
     *
     * @var string $st_emailinteressado
     * @Column(name="st_emailinteressado", type="string", nullable=true, length=255)
     */
    private $st_emailinteressado;

    /**
     *
     * @var string $st_urlportal
     * @Column(name="st_urlportal", type="string", nullable=true, length=255)
     */
    private $st_urlportal;

    /**
     *
     * @var string $st_telefoneinteressado
     * @Column(name="st_telefoneinteressado", type="string", nullable=true, length=61)
     */
    private $st_telefoneinteressado;

    /**
     *
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false)
     */
    private $id_nucleoco;

    /**
     *
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=true)
     */
    private $id_assuntoco;

    /**
     *
     * @var integer $id_assuntocopai
     * @Column(name="id_assuntocopai", type="integer", nullable=true)
     */
    private $id_assuntocopai;

    /**
     * @var integer $id_ocorrenciaoriginal
     * @Column(name="id_ocorrenciaoriginal", type="integer", nullable=false)
     */
    private $id_ocorrenciaoriginal;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=false)
     */
    private $id_funcao;

    /**
     *
     * @var boolean $bl_prioritario
     * @Column(name="bl_prioritario", type="boolean", nullable=false)
     */
    private $bl_prioritario;

    /**
     *
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true)
     */
    private $id_tipoocorrencia;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=false)
     */
    private $id_entidadematricula;

    /**
     * @var string $st_entidadematricula
     * @Column(name="st_entidadematricula", type="string", nullable=false, length=300)
     */
    private $st_entidadematricula;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_nucleoco
     * @Column(name="st_nucleoco", type="string", nullable=false, length=150)
     */
    private $st_nucleoco;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=true, length=100)
     */
    private $st_funcao;

    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    /**
     *
     * @var integer $id_evolucaomatricula
     * @Column(name="id_evolucaomatricula", type="integer", nullable=true)
     */
    private $id_evolucaomatricula;

    /**
     *
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     *
     * @var integer $id_usuariointeressado
     * @Column(name="id_usuariointeressado", type="integer", nullable=false)
     */
    private $id_usuariointeressado;

    /**
     *
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=true)
     */
    private $id_categoriaocorrencia;

    /**
     *
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true)
     */
    private $id_saladeaula;

    /**
     *
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=false, length=8000)
     */
    private $st_titulo;

    /**
     *
     * @var string $st_ocorrencia
     * @Column(name="st_ocorrencia", type="string", nullable=false, length=8000)
     */
    private $st_ocorrencia;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     *
     * @var datetime2 $dt_cadastroocorrencia
     * @Column(name="dt_cadastroocorrencia", type="datetime2", nullable=false)
     */
    private $dt_cadastroocorrencia;

    /**
     *
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     *
     * @var integer $id_atendente
     * @Column(name="id_atendente", type="integer", nullable=true)
     */
    private $id_atendente;

    /**
     *
     * @var string $st_atendente
     * @Column(name="st_atendente", type="string", nullable=true, length=400)
     */
    private $st_atendente;

    /**
     *
     * @var string $st_categoriaocorrencia
     * @Column(name="st_categoriaocorrencia", type="string", nullable=false, length=250)
     */
    private $st_categoriaocorrencia;

    /**
     *
     * @var string $st_ultimotramite
     * @Column(name="st_ultimotramite", type="string", nullable=true, length=8000)
     */
    private $st_ultimotramite;

    /**
     *
     * @var datetime2 $dt_ultimotramite
     * @Column(name="dt_ultimotramite", type="datetime2", nullable=true)
     */
    private $dt_ultimotramite;

    /**
     *
     * @var integer $nu_horastraso
     * @Column(name="nu_horastraso", type="integer", nullable=true)
     */
    private $nu_horastraso;

    /**
     *
     * @var integer $nu_porcentagemmeta
     * @Column(name="nu_porcentagemmeta", type="integer", nullable=true)
     */
    private $nu_porcentagemmeta;

    /**
     * @var string $st_cor
     * @Column(name="st_cor", type="string", nullable=true, length=10)
     */
    private $st_cor;

    /**
     *
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=false)
     */
    private $nu_ordem;

    /**
     *
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean", nullable=true)
     */
    private $bl_cancelamento;

    /**
     *
     * @var boolean $bl_trancamento
     * @Column(name="bl_trancamento", type="boolean", nullable=true)
     */
    private $bl_trancamento;

    /**
     *
     * @var datetime2 $dt_atendimento
     * @Column(name="dt_atendimento", type="datetime2", nullable=false)
     */
    private $dt_atendimento;

    /**
     *
     * @var integer $nu_diasaberto
     * @Column(name="nu_diasaberto", type="integer", nullable=true)
     */
    private $nu_diasaberto;

    /**
     * @var string $st_assuntocopai
     * @Column(name="st_assuntocopai", type="string", nullable=true)
     */
    private $st_assuntocopai;

    /**
     * @var string $st_cadastroocorrencia
     * @Column(name="st_cadastroocorrencia", type="string", nullable=true)
     */
    private $st_cadastroocorrencia;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true)
     */
    private $st_cpf;

    /**
     * @var boolean $bl_resgate
     * @Column(name="bl_resgate", type="boolean", nullable=true)
     */
    private $bl_resgate;

    /**
     * @var string $st_urlacesso
     * @Column(name="st_urlacesso", type="string", nullable=true, length=150)
     */
    private $st_urlacesso;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=false)
     */
    private $st_tituloexibicao;

    /**
     * @var string $st_coordenadortitular
     * @Column(name="st_coordenadortitular", type="string", nullable=false)
     */
    private $st_coordenadortitular;

    /**
     * @return string
     */
    public function getSt_urlportal()
    {
        return $this->st_urlportal;
    }

    public function setSt_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
        return $this;

    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }

    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    public function setBl_prioritario($bl_prioritario)
    {
        $this->bl_prioritario = $bl_prioritario;
    }

    public function getBl_prioritario()
    {
        return $this->bl_prioritario;
    }

    public function setBl_trancamento($bl_trancamento)
    {
        $this->bl_trancamento = $bl_trancamento;
    }

    public function getBl_trancamento()
    {
        return $this->bl_trancamento;
    }


    public function setDt_atendimento($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
    }

    public function getDt_atendimento()
    {
        return $this->dt_atendimento;
    }

    public function setDt_cadastroocorrencia($dt_cadastroocorrencia)
    {
        $this->dt_cadastroocorrencia = $dt_cadastroocorrencia;
    }

    public function getDt_cadastroocorrencia()
    {
        return $this->dt_cadastroocorrencia;
    }

    public function setDt_ultimotramite($dt_ultimotramite)
    {
        $this->dt_ultimotramite = $dt_ultimotramite;
    }

    public function getDt_ultimotramite()
    {
        return $this->dt_ultimotramite;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function setId_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
    }

    public function getId_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    /**
     * @return int
     */
    public function getId_ocorrenciaoriginal()
    {
        return $this->id_ocorrenciaoriginal;
    }

    /**
     * @param int $id_ocorrenciaoriginal
     * @return $this
     */
    public function setId_ocorrenciaoriginal($id_ocorrenciaoriginal)
    {
        $this->id_ocorrenciaoriginal = $id_ocorrenciaoriginal;
        return $this;
    }

    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
    }

    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function setId_evolucaomatricula($id_evolucaomatricula)
    {
        $this->id_evolucaomatricula = $id_evolucaomatricula;
    }

    public function getId_evolucaomatricula()
    {
        return $this->id_evolucaomatricula;
    }

    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuariointeressado($id_usuariointeressado)
    {
        $this->id_usuariointeressado = $id_usuariointeressado;
    }

    public function getId_usuariointeressado()
    {
        return $this->id_usuariointeressado;
    }

    public function setNu_diasaberto($nu_diasaberto)
    {
        $this->nu_diasaberto = $nu_diasaberto;
    }

    public function getNu_diasaberto()
    {
        return $this->nu_diasaberto;
    }

    public function setNu_horastraso($nu_horastraso)
    {
        $this->nu_horastraso = $nu_horastraso;
    }

    public function getNu_horastraso()
    {
        return $this->nu_horastraso;
    }

    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    public function setNu_porcentagemmeta($nu_porcentagemmeta)
    {
        $this->nu_porcentagemmeta = $nu_porcentagemmeta;
    }

    public function getNu_porcentagemmeta()
    {
        return $this->nu_porcentagemmeta;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function setSt_atendente($st_atendente)
    {
        $this->st_atendente = $st_atendente;
    }

    public function getSt_atendente()
    {
        return $this->st_atendente;
    }

    public function setSt_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
    }

    public function getSt_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    public function setSt_cor($st_cor)
    {
        $this->st_cor = $st_cor;
    }

    public function getSt_cor()
    {
        return $this->st_cor;
    }

    public function setSt_emailinteressado($st_emailinteressado)
    {
        $this->st_emailinteressado = $st_emailinteressado;
    }

    public function getSt_emailinteressado()
    {
        return $this->st_emailinteressado;
    }

    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    public function setSt_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
    }

    public function getSt_funcao()
    {
        return $this->st_funcao;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomeinteressado($st_nomeinteressado)
    {
        $this->st_nomeinteressado = $st_nomeinteressado;
    }

    public function getSt_nomeinteressado()
    {
        return $this->st_nomeinteressado;
    }

    public function setSt_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
    }

    public function getSt_nucleoco()
    {
        return $this->st_nucleoco;
    }

    public function setSt_ocorrencia($st_ocorrencia)
    {
        $this->st_ocorrencia = $st_ocorrencia;
    }

    public function getSt_ocorrencia()
    {
        return $this->st_ocorrencia;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function setSt_telefoneinteressado($st_telefoneinteressado)
    {
        $this->st_telefoneinteressado = $st_telefoneinteressado;
    }

    public function getSt_telefoneinteressado()
    {
        return $this->st_telefoneinteressado;
    }

    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
    }

    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    public function setSt_ultimotramite($st_ultimotramite)
    {
        $this->st_ultimotramite = $st_ultimotramite;
    }

    public function getSt_ultimotramite()
    {
        return $this->st_ultimotramite;
    }

    public function getSt_assuntocopai()
    {
        return $this->st_assuntocopai;
    }

    public function setSt_assuntocopai($st_assuntocopai)
    {
        $this->st_assuntocopai = $st_assuntocopai;
        return $this;
    }

    public function getSt_cadastroocorrencia()
    {
        return $this->st_cadastroocorrencia;
    }

    public function setSt_cadastroocorrencia($st_cadastroocorrencia)
    {
        $this->st_cadastroocorrencia = $st_cadastroocorrencia;
        return $this;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function getBl_resgate()
    {
        return $this->bl_resgate;
    }

    public function setBl_resgate($bl_resgate)
    {
        $this->bl_resgate = $bl_resgate;
        return $this;
    }

    public function setSt_urlacesso($st_urlacesso)
    {
        $this->st_urlacesso = $st_urlacesso;
    }

    public function getSt_urlacesso()
    {
        return $this->st_urlacesso;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param int $id_entidadematricula
     * @return $this
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_entidadematricula()
    {
        return $this->st_entidadematricula;
    }

    /**
     * @param string $st_entidadematricula
     * @return $this
     */
    public function setSt_entidadematricula($st_entidadematricula)
    {
        $this->st_entidadematricula = $st_entidadematricula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param string $st_tituloexibicao
     * @return $this
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_coordenadortitular()
    {
        return $this->st_coordenadortitular;
    }

    /**
     * @param string $st_coordenadortitular
     */
    public function setSt_coordenadortitular($st_coordenadortitular)
    {
        $this->st_coordenadortitular = $st_coordenadortitular;
        return $this;
    }


}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_etiquetacertificacao")
 * @Entity
 * @EntityView
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 */
class VwEtiquetaCertificacao extends G2Entity
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", length=200)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     * @Id
     */
    private $id_matricula;

    /**
     *
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string")
     */
    private $st_endereco;

    /**
     *
     * @var string $st_numero
     * @Column(name="st_numero", type="string", nullable=true, length=200)
     */
    private $st_numero;

    /**
     *
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", length=200)
     */
    private $st_bairro;

    /**
     *
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", length=255)
     */
    private $st_cidade;

    /**
     *
     * @var string $st_uf
     * @Column(name="st_uf", type="string")
     */
    private $st_uf;

    /**
     *
     * @var string $st_cep
     * @Column(name="st_cep", type="string")
     */
    private $st_cep;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string")
     */
    private $st_complemento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2")
     */
    private $dt_solicitacao;

    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2")
     */
    private $dt_entrega;

    /**
     * @var string $st_servico
     * @Column(name="st_servico", type="string")
     */
    private $st_servico;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string")
     */
    private $st_email;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

}

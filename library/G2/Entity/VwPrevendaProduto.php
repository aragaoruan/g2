<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_prevendaproduto")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPrevendaProduto
{

    /**
     * @var integer $id_prevendaproduto
     * @Column(name="id_prevendaproduto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_prevendaproduto;

    /**
     * @var integer $id_prevenda
     * @Column(name="id_prevenda", type="integer", nullable=true)
     */
    private $id_prevenda;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     */
    private $id_produto;

    /**
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=false)
     */
    private $id_produtovalor;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;

    /**
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=false, length=255)
     */
    private $st_tipoproduto;

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true)
     */
    private $dt_termino;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false)
     */
    private $nu_valor;

    /**
     * @var decimal $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=true)
     */
    private $nu_valormensal;

    public function getId_prevendaproduto ()
    {
        return $this->id_prevendaproduto;
    }

    public function setId_prevendaproduto ($id_prevendaproduto)
    {
        $this->id_prevendaproduto = $id_prevendaproduto;
        return $this;
    }

    public function getId_prevenda ()
    {
        return $this->id_prevenda;
    }

    public function setId_prevenda ($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
        return $this;
    }

    public function getId_produto ()
    {
        return $this->id_produto;
    }

    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getId_produtovalor ()
    {
        return $this->id_produtovalor;
    }

    public function setId_produtovalor ($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    public function getSt_tipoproduto ()
    {
        return $this->st_tipoproduto;
    }

    public function setSt_tipoproduto ($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getDt_termino ()
    {
        return $this->dt_termino;
    }

    public function setDt_termino ($dt_termino)
    {
        $this->dt_termino = $dt_termino;
        return $this;
    }

    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function getNu_valormensal ()
    {
        return $this->nu_valormensal;
    }

    public function setNu_valormensal ($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

}
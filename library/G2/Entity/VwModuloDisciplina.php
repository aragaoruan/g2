<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_modulodisciplina")
 * @Entity(repositoryClass="\G2\Repository\ModuloDisciplina")
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Update Denise Xavier <06-03-2017>
 */
class VwModuloDisciplina
{

    /**
     * @var integer $id_modulodisciplina
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_modulodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_modulodisciplina;

    /**
     * @var datetime2 $dt_cadastroponderacao
     * @Column(name="dt_cadastroponderacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastroponderacao;

    /**
     * @var integer $id_projetopedagogico
     * @Id
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_modulo
     * @Id
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_disciplina
     * @Id
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_areaconhecimento
     * @Id
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_serie
     * @Id
     * @Column(name="id_serie", type="integer", nullable=false, length=4)
     */
    private $id_serie;

    /**
     * @var integer $id_nivelensino
     * @Id
     * @Column(name="id_nivelensino", type="integer", nullable=false, length=4)
     */
    private $id_nivelensino;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true, length=4)
     */
    private $nu_ordem;

    /**
     * @var integer $id_usuarioponderacao
     * @Id
     * @Column(name="id_usuarioponderacao", type="integer", nullable=true, length=4)
     */
    private $id_usuarioponderacao;

    /**
     * @var integer $nu_importancia
     * @Column(name="nu_importancia", type="integer", nullable=true, length=4)
     */
    private $nu_importancia;

    /**
     * @var boolean $bl_obrigatoria
     * @Column(name="bl_obrigatoria", type="boolean", nullable=false, length=1)
     */
    private $bl_obrigatoria;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var decimal $nu_ponderacaocalculada
     * @Column(name="nu_ponderacaocalculada", type="decimal", nullable=true, length=5)
     */
    private $nu_ponderacaocalculada;

    /**
     * @var decimal $nu_ponderacaoaplicada
     * @Column(name="nu_ponderacaoaplicada", type="decimal", nullable=true, length=5)
     */
    private $nu_ponderacaoaplicada;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true, length=255)
     */
    private $st_areaconhecimento;

    /**
     * @var string $st_serie
     * @Column(name="st_serie", type="string", nullable=false, length=255)
     */
    private $st_serie;

    /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=false, length=255)
     */
    private $st_nivelensino;

    /**
     * @var integer $nu_cargahoraria
     * @Column(type="integer", nullable=true, name="nu_cargahoraria")
     */
    private $nu_cargahoraria;

    /**
     * @var integer $nu_repeticao
     * @Column(type="integer", nullable=true, name="nu_repeticao")
     */
    private $nu_repeticao;

    /**
     * @var string $st_estudio
     * @Column(type="string", nullable=true, name="st_estudio")
     */
    private $st_estudio;


    /**
     * @var integer $nu_cargahorariad
     * @Column(type="integer", nullable=true, name="nu_cargahorariad")
     */
    private $nu_cargahorariad;

    /**
     * @var integer $nu_obrigatorioalocacao
     * @Column(type="integer", nullable=true, name="nu_obrigatorioalocacao")
     */
    private $nu_obrigatorioalocacao;

    /**
     * @var integer $nu_disponivelapartirdo
     * @Column(type="integer", nullable=true, name="nu_disponivelapartirdo")
     */
    private $nu_disponivelapartirdo;

    /**
     * @var decimal $nu_percentualsemestre
     * @Column(type="decimal", nullable=true, name="nu_percentualsemestre")
     */
    private $nu_percentualsemestre;

    /**
     * @var integer $id_tipodisciplina
     * @Column(type="integer", nullable=true, name="id_tipodisciplina")
     */
    private $id_tipodisciplina;

    /**
     * 
     * @return integer
     */
    public function getId_modulodisciplina ()
    {
        return $this->id_modulodisciplina;
    }

    /**
     * 
     * @return datetime2
     */
    public function getDt_cadastroponderacao ()
    {
        return $this->dt_cadastroponderacao;
    }

    /**
     * 
     * @return integer
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * 
     * @return integer
     */
    public function getId_modulo ()
    {
        return $this->id_modulo;
    }

    /**
     * 
     * @return integer
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * 
     * @return integer
     */
    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * 
     * @return integer
     */
    public function getId_serie ()
    {
        return $this->id_serie;
    }

    /**
     * 
     * @return integer
     */
    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    /**
     * 
     * @return integer
     */
    public function getNu_ordem ()
    {
        return $this->nu_ordem;
    }

    /**
     * 
     * @return integer
     */
    public function getId_usuarioponderacao ()
    {
        return $this->id_usuarioponderacao;
    }

    /**
     * 
     * @return integer
     */
    public function getNu_importancia ()
    {
        return $this->nu_importancia;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_obrigatoria ()
    {
        return $this->bl_obrigatoria;
    }

    /**
     * 
     * @return boolean
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * 
     * @return decimal
     */
    public function getNu_ponderacaocalculada ()
    {
        return $this->nu_ponderacaocalculada;
    }

    /**
     * 
     * @return decimal
     */
    public function getNu_ponderacaoaplicada ()
    {
        return $this->nu_ponderacaoaplicada;
    }

    /**
     * 
     * @return string
     */
    public function getSt_projetopedagogico ()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * 
     * @return string
     */
    public function getSt_modulo ()
    {
        return $this->st_modulo;
    }

    /**
     * 
     * @return string
     */
    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    /**
     * 
     * @return string
     */
    public function getSt_areaconhecimento ()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * 
     * @return string
     */
    public function getSt_serie ()
    {
        return $this->st_serie;
    }

    /**
     * 
     * @return string
     */
    public function getSt_nivelensino ()
    {
        return $this->st_nivelensino;
    }

    /**
     * 
     * @param integer $id_modulodisciplina
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_modulodisciplina ($id_modulodisciplina)
    {
        $this->id_modulodisciplina = $id_modulodisciplina;
        return $this;
    }

    /**
     * 
     * @param datetime2 $dt_cadastroponderacao
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setDt_cadastroponderacao ($dt_cadastroponderacao)
    {
        $this->dt_cadastroponderacao = $dt_cadastroponderacao;
        return $this;
    }

    /**
     * 
     * @param integer $id_projetopedagogico
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * 
     * @param integer $id_modulo
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_modulo ($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * 
     * @param integer $id_disciplina
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * 
     * @param integer $id_areaconhecimento
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * 
     * @param integer $id_serie
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    /**
     * 
     * @param integer $id_nivelensino
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    /**
     * 
     * @param integer $nu_ordem
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_ordem ($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     * 
     * @param integer $id_usuarioponderacao
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setId_usuarioponderacao ($id_usuarioponderacao)
    {
        $this->id_usuarioponderacao = $id_usuarioponderacao;
        return $this;
    }

    /**
     * 
     * @param integer $nu_importancia
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_importancia ($nu_importancia)
    {
        $this->nu_importancia = $nu_importancia;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_obrigatoria
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setBl_obrigatoria ($bl_obrigatoria)
    {
        $this->bl_obrigatoria = $bl_obrigatoria;
        return $this;
    }

    /**
     * 
     * @param boolean $bl_ativo
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_ponderacaocalculada
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_ponderacaocalculada ($nu_ponderacaocalculada)
    {
        $this->nu_ponderacaocalculada = $nu_ponderacaocalculada;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_ponderacaoaplicada
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_ponderacaoaplicada ($nu_ponderacaoaplicada)
    {
        $this->nu_ponderacaoaplicada = $nu_ponderacaoaplicada;
        return $this;
    }

    /**
     * 
     * @param string $st_projetopedagogico
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_projetopedagogico ($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * 
     * @param string $st_modulo
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_modulo ($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * 
     * @param string $st_disciplina
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * 
     * @param string $st_areaconhecimento
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_areaconhecimento ($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * 
     * @param string $st_serie
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_serie ($st_serie)
    {
        $this->st_serie = $st_serie;
        return $this;
    }

    /**
     * 
     * @param string $st_nivelensino
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_nivelensino ($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * 
     * @return interger
     */
    public function getNu_repeticao ()
    {
        return $this->nu_repeticao;
    }

    /**
     * 
     * @return string
     */
    public function getSt_estudio ()
    {
        return $this->st_estudio;
    }

    /**
     * @param integer $nu_cargahoraria
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @param integer $nu_repeticao
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setNu_repeticao ($nu_repeticao)
    {
        $this->nu_repeticao = $nu_repeticao;
        return $this;
    }

    /**
     * @param string $st_estudio
     * @return \G2\Entity\VwModuloDisciplina
     */
    public function setSt_estudio ($st_estudio)
    {
        $this->st_estudio = $st_estudio;
        return $this;
    }

    /**
     * @param int $nu_cargahorariad
     */
    public function setNu_cargahorariad($nu_cargahorariad)
    {
        $this->nu_cargahorariad = $nu_cargahorariad;
    }

    /**
     * @return int
     */
    public function getNu_cargahorariad()
    {
        return $this->nu_cargahorariad;
    }

    /**
     * @return int
     */
    public function getNu_obrigatorioalocacao()
    {
        return $this->nu_obrigatorioalocacao;
    }

    /**
     * @param int $nu_obrigatorioalocacao
     */
    public function setNu_obrigatorioalocacao($nu_obrigatorioalocacao)
    {
        $this->nu_obrigatorioalocacao = $nu_obrigatorioalocacao;
    }

    /**
     * @return int
     */
    public function getNu_disponivelapartirdo()
    {
        return $this->nu_disponivelapartirdo;
    }

    /**
     * @param int $nu_disponivelapartirdo
     */
    public function setNu_disponivelapartirdo($nu_disponivelapartirdo)
    {
        $this->nu_disponivelapartirdo = $nu_disponivelapartirdo;
    }

    /**
     * @return decimal
     */
    public function getNu_percentualsemestre()
    {
        return $this->nu_percentualsemestre;
    }

    /**
     * @param decimal $nu_percentualsemestre
     */
    public function setNu_percentualsemestre($nu_percentualsemestre)
    {
        $this->nu_percentualsemestre = $nu_percentualsemestre;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

}

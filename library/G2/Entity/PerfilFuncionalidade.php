<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_perfilfuncionalidade")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class PerfilFuncionalidade
{
    /**
     * @var integer $id_perfil
     * @ManyToOne(targetEntity="Perfil")
     * @JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     * @id
     */
    private $id_perfil;

    /**
     * @var integer $id_funcionalidade
     * @ManyToOne(targetEntity="Funcionalidade")
     * @JoinColumn(name="id_funcionalidade", referencedColumnName="id_funcionalidade")
     * @id
     */
    private $id_funcionalidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return int
     */
    public function getId_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @param int $id_funcionalidade
     */
    public function setId_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
    }

    /**
     * @return boolean
     */
    public function isBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }
}
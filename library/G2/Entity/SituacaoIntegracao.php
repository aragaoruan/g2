<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_situacaointegracao")
 * @Entity
 */
class SituacaoIntegracao extends G2Entity{

    /**
     *
     * @var integer $id_situacaointegracao
     * @Column(name="id_situacaointegracao ", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_situacaointegracao;

    /**
     * @var integer $nu_status
     * @Column(name="nu_status", type="integer", nullable=true)
     */
    private $nu_status;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=100)
     */
    private $st_status;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumns({
     *      @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     * })
     */
    private $id_sistema;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @return string
     */
    public function getst_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_status
     */
    public function setst_status($st_status)
    {
        $this->st_status = $st_status;
        return $this;

    }


    /**
     * @param boolean $bl_ativo
     */
    public function setBlAtivo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBlAtivo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param \G2\Entity\datetime $dt_cadastro
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \G2\Entity\Sistema $id_sistema
     */
    public function setIdSistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return \G2\Entity\Sistema
     */
    public function getIdSistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_situacaointegracao
     */
    public function setIdSituacaointegracao($id_situacaointegracao)
    {
        $this->id_situacaointegracao = $id_situacaointegracao;
    }

    /**
     * @return int
     */
    public function getIdSituacaointegracao()
    {
        return $this->id_situacaointegracao;
    }

    /**
     * @param int $nu_status
     */
    public function setNuStatus($nu_status)
    {
        $this->nu_status = $nu_status;
    }

    /**
     * @return int
     */
    public function getNuStatus()
    {
        return $this->nu_status;
    }

    /**
     * @param string $st_situacao
     */
    public function setStSituacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return string
     */
    public function getStSituacao()
    {
        return $this->st_situacao;
    }
} 
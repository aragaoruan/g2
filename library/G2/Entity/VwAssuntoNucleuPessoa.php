<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_assuntonucleupessoa")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwAssuntoNucleoPessoa")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwAssuntoNucleuPessoa
{
    /**
     * @var integer $id_assuntopai
     * @Column(name="id_assuntopai", type="integer", nullable=true, length=4)
     */
    private $id_assuntopai;
    /**
     * @var integer $id_subassunto
     * @Column(name="id_subassunto", type="integer", nullable=true, length=4)
     */
    private $id_subassunto;
    /**
     * @var integer $id_assunto
     * @Column(name="id_assunto", type="integer", nullable=true, length=4)
     */
    private $id_assunto;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false, length=4)
     */
    private $id_entidadecadastro;
    /**
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @Id
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=false, length=4)
     */
    private $id_funcao;
    /**
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false, length=4)
     */
    private $id_nucleoco;
    /**
     * @var boolean $bl_todos
     * @Column(name="bl_todos", type="boolean", nullable=true, length=1)
     */
    private $bl_todos;
    /**
     * @var decimal $id_nucleopessoaco
     * @Column(name="id_nucleopessoaco", type="decimal", nullable=true, length=9)
     */
    private $id_nucleopessoaco;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_assuntopai
     * @Column(name="st_assuntopai", type="string", nullable=true, length=200)
     */
    private $st_assuntopai;
    /**
     * @var string $st_subassunto
     * @Column(name="st_subassunto", type="string", nullable=true, length=200)
     */
    private $st_subassunto;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_nucleoco
     * @Column(name="st_nucleoco", type="string", nullable=false, length=150)
     */
    private $st_nucleoco;
    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=true, length=100)
     */
    private $st_funcao;
    /**
     * @var string $st_tipoocorrencia
     * @Column(name="st_tipoocorrencia", type="string", nullable=true, length=200)
     */
    private $st_tipoocorrencia;


    /**
     * @return integer id_assuntopai
     */
    public function getId_assuntopai() {
        return $this->id_assuntopai;
    }

    /**
     * @param id_assuntopai
     */
    public function setId_assuntopai($id_assuntopai) {
        $this->id_assuntopai = $id_assuntopai;
        return $this;
    }

    /**
     * @return integer id_subassunto
     */
    public function getId_subassunto() {
        return $this->id_subassunto;
    }

    /**
     * @param id_subassunto
     */
    public function setId_subassunto($id_subassunto) {
        $this->id_subassunto = $id_subassunto;
        return $this;
    }

    /**
     * @return integer id_assunto
     */
    public function getId_assunto() {
        return $this->id_assunto;
    }

    /**
     * @param id_assunto
     */
    public function setId_assunto($id_assunto) {
        $this->id_assunto = $id_assunto;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_entidadecadastro
     */
    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    /**
     * @param id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_funcao
     */
    public function getId_funcao() {
        return $this->id_funcao;
    }

    /**
     * @param id_funcao
     */
    public function setId_funcao($id_funcao) {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    /**
     * @return integer id_nucleoco
     */
    public function getId_nucleoco() {
        return $this->id_nucleoco;
    }

    /**
     * @param id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco) {
        $this->id_nucleoco = $id_nucleoco;
        return $this;
    }

    /**
     * @return boolean bl_todos
     */
    public function getBl_todos() {
        return $this->bl_todos;
    }

    /**
     * @param bl_todos
     */
    public function setBl_todos($bl_todos) {
        $this->bl_todos = $bl_todos;
        return $this;
    }

    /**
     * @return decimal id_nucleopessoaco
     */
    public function getId_nucleopessoaco() {
        return $this->id_nucleopessoaco;
    }

    /**
     * @param id_nucleopessoaco
     */
    public function setId_nucleopessoaco($id_nucleopessoaco) {
        $this->id_nucleopessoaco = $id_nucleopessoaco;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_assuntopai
     */
    public function getSt_assuntopai() {
        return $this->st_assuntopai;
    }

    /**
     * @param st_assuntopai
     */
    public function setSt_assuntopai($st_assuntopai) {
        $this->st_assuntopai = $st_assuntopai;
        return $this;
    }

    /**
     * @return string st_subassunto
     */
    public function getSt_subassunto() {
        return $this->st_subassunto;
    }

    /**
     * @param st_subassunto
     */
    public function setSt_subassunto($st_subassunto) {
        $this->st_subassunto = $st_subassunto;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_nucleoco
     */
    public function getSt_nucleoco() {
        return $this->st_nucleoco;
    }

    /**
     * @param st_nucleoco
     */
    public function setSt_nucleoco($st_nucleoco) {
        $this->st_nucleoco = $st_nucleoco;
        return $this;
    }

    /**
     * @return string st_funcao
     */
    public function getSt_funcao() {
        return $this->st_funcao;
    }

    /**
     * @param st_funcao
     */
    public function setSt_funcao($st_funcao) {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    /**
     * @return string st_tipoocorrencia
     */
    public function getSt_tipoocorrencia() {
        return $this->st_tipoocorrencia;
    }

    /**
     * @param st_tipoocorrencia
     */
    public function setSt_tipoocorrencia($st_tipoocorrencia) {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
        return $this;
    }

}

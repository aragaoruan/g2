<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_carreira")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 
 */

use G2\G2Entity;

class Carreira extends G2Entity
{

    /**
     * @var integer $id_carreira
     * @Column(name="id_carreira", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_carreira;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string $st_carreira
     * @Column(name="st_carreira", type="string", nullable=false, length=200)
     */
    private $st_carreira;

    /**
     * @return Integer $this->id_carreira
     */
    public function getId_carreira ()
    {
        return $this->id_carreira;
    }

    /**
     * @param Integer $id_carreira
     * @return \G2\Entity\Carreiras
     */
    public function setId_carreira ($id_carreira)
    {
        $this->id_carreira = $id_carreira;
        return $this;
    }

    /**
     * @return DateTime $this->dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param DateTime $dt_cadastro
     * @return \G2\Entity\Carreiras
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return String $this->st_carreira
     */
    public function getSt_carreira ()
    {
        return $this->st_carreira;
    }

    /**
     * 
     * @param String $st_carreira
     * @return \G2\Entity\Carreiras
     */
    public function setSt_carreira ($st_carreira)
    {
        $this->st_carreira = $st_carreira;
        return $this;
    }

    /**
     * Return array com atributos
     * @return Array
     */
    public function _toArray ()
    {
        return array(
            'id_carreira' => $this->id_carreira,
            'dt_cadastro' => $this->dt_cadastro,
            'st_carreira' => $this->st_carreira
        );
    }

}
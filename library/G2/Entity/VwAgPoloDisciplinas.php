<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_agpolodisciplinas")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAgPoloDisciplinas {

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;
    /**
     * @var integer $nu_ddi
     * @Column(name="nu_ddi", type="integer", nullable=true, length=4)
     */
    private $nu_ddi;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoagendamento;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipodeavaliacao;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaplicacao;


    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @var string $nu_telefone
     * @Column(name="nu_telefone", type="string", nullable=true, length=15)
     */
    private $nu_telefone;

    /**
     * @var string $nu_ddd
     * @Column(name="nu_ddd", type="string", nullable=true, length=3)
     */
    private $nu_ddd;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_tipodeavaliacao
     * @Column(name="st_tipodeavaliacao", type="string", nullable=true, length=51)
     */
    private $st_tipodeavaliacao;
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getNu_ddi() {
        return $this->nu_ddi;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_tipodeavaliacao() {
        return $this->id_tipodeavaliacao;
    }

    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_email() {
        return $this->st_email;
    }

    public function getNu_telefone() {
        return $this->nu_telefone;
    }

    public function getNu_ddd() {
        return $this->nu_ddd;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getSt_tipodeavaliacao() {
        return $this->st_tipodeavaliacao;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function setNu_ddi($nu_ddi) {
        $this->nu_ddi = $nu_ddi;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao) {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setSt_email($st_email) {
        $this->st_email = $st_email;
    }

    public function setNu_telefone($nu_telefone) {
        $this->nu_telefone = $nu_telefone;
    }

    public function setNu_ddd($nu_ddd) {
        $this->nu_ddd = $nu_ddd;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function setSt_tipodeavaliacao($st_tipodeavaliacao) {
        $this->st_tipodeavaliacao = $st_tipodeavaliacao;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }




}

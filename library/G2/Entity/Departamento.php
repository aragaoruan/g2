<?php
namespace G2\Entity;

/**
 *
 * @Table(name="tb_departamento")
 * @Entity
 * @author Vinicius Avelino Alcantara <vinicius.alcantara@unyleya.com.br>
 * @since: 01/09/2015
 */
class Departamento
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_departamento
     * @Column(name="id_departamento", type="integer", nullable=false, length=4)
     */
    private $id_departamento;
    /**
     * @var string $st_departamento
     * @Column(name="st_departamento", type="string", nullable=false, length=255)
     */
    private $st_departamento;

    /**
     * @return int
     */
    public function getid_departamento()
    {
        return $this->id_departamento;
    }

    /**
     * @param int $id_departamento
     * @return \G2\Entity\Departamento
     */
    public function setid_departamento($id_departamento)
    {
        $this->id_departamento = $id_departamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_departamento()
    {
        return $this->st_departamento;
    }

    /**
     * @param string $st_departamento
     * @return \G2\Entity\Departamento
     */

    public function setst_departamento($st_departamento)
    {
        $this->st_departamento = $st_departamento;
        return $this;
    }


}
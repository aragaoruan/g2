<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_campanhadesconto")
 * @author kayo.silva@unyleya.com.br <kayo.silva@unyleya.com.br>
 */
class CampanhaDesconto
{

    /**
     * @var integer $id_campanhadesconto
     * @Column(type="integer", nullable=false, name="id_campanhadesconto") 
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_campanhadesconto;

    /**
     * @var decimal $nu_valormin
     * @Column(type="decimal", nullable=false, name="nu_valormin")
     */
    private $nu_valormin;

    /**
     * @var decimal $nu_valormax
     * @Column(type="decimal", nullable=false, name="nu_valormax") 
     */
    private $nu_valormax;

    /**
     * @var CampanhaComercial $id_campanhacomercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_campanhacomercial;

    /**
     * @var MeioPagamento $id_meiopagamento
     * @ManyToOne(targetEntity="MeioPagamento")
     * @JoinColumn(name="id_meiopagamento", referencedColumnName="id_meiopagamento", nullable=false)
     */
    private $id_meiopagamento;

    /**
     * @var decimal $nu_descontominimo
     * @Column(name="nu_descontominimo", type="decimal", nullable=false)
     */
    private $nu_descontominimo;

    /**
     * @var decimal $nu_descontomaximo
     * @Column(name="nu_descontomaximo", type="decimal", nullable=false)
     */
    private $nu_descontomaximo;

    /**
     * @return integer id_campanhadesconto
     */
    public function getId_campanhadesconto ()
    {
        return $this->id_campanhadesconto;
    }

    /**
     * 
     * @return decimal nu_valormin
     */
    public function getNu_valormin ()
    {
        return $this->nu_valormin;
    }

    /**
     * @return decimal nu_valormax
     */
    public function getNu_valormax ()
    {
        return $this->nu_valormax;
    }

    /**
     * @return \G2\Entity\CampanhaComercial id_campanhacomercial
     */
    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return \G2\Entity\MeioPagamento id_meiopagamento
     */
    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @return decimal nu_descontominimo
     */
    public function getNu_descontominimo ()
    {
        return $this->nu_descontominimo;
    }

    /**
     * @return decimal nu_descontomaximo
     */
    public function getNu_descontomaximo ()
    {
        return $this->nu_descontomaximo;
    }

    /**
     * @param integer $id_campanhadesconto
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setId_campanhadesconto ($id_campanhadesconto)
    {
        $this->id_campanhadesconto = $id_campanhadesconto;
        return $this;
    }

    /**
     * @param decimal $nu_valormin
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setNu_valormin ($nu_valormin)
    {
        $this->nu_valormin = $nu_valormin;
        return $this;
    }

    /**
     * @param decimal $nu_valormax
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setNu_valormax ($nu_valormax)
    {
        $this->nu_valormax = $nu_valormax;
        return $this;
    }

    /**
     * @param \G2\Entity\CampanhaComercial $id_campanhacomercial
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setId_campanhacomercial (CampanhaComercial $id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @param \G2\Entity\MeioPagamento $id_meiopagamento
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setId_meiopagamento (MeioPagamento $id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @param decimal $nu_descontominimo
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setNu_descontominimo ($nu_descontominimo)
    {
        $this->nu_descontominimo = $nu_descontominimo;
        return $this;
    }

    /**
     * @param decimal $nu_descontomaximo
     * @return \G2\Entity\CampanhaDesconto
     */
    public function setNu_descontomaximo ($nu_descontomaximo)
    {
        $this->nu_descontomaximo = $nu_descontomaximo;
        return $this;
    }

}

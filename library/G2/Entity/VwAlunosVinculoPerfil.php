<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alunosvinculoperfil")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAlunosVinculoPerfil {

    /**
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false, length=8)
     */
    private $dt_inicio;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_usuarioperfil
     * @Column(name="id_usuarioperfil", type="integer", nullable=false, length=4)
     */
    private $id_usuarioperfil;
    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;
    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_perfilpedagogico;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var string $st_entidadematriz
     * @Column(name="st_entidadematriz", type="string", nullable=false, length=300)
     */
    private $st_entidadematriz;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=150)
     */
    private $st_turma;
    /**
     * @var integer $id_esquemaconfiguracao
     * @Column(name="id_esquemaconfiguracao", type="integer", nullable=false, length=4)
     */
    private $id_esquemaconfiguracao;


    /**
     * @return datetime2 dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_usuarioperfil
     */
    public function getId_usuarioperfil() {
        return $this->id_usuarioperfil;
    }

    /**
     * @param id_usuarioperfil
     */
    public function setId_usuarioperfil($id_usuarioperfil) {
        $this->id_usuarioperfil = $id_usuarioperfil;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil() {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return integer id_perfilpedagogico
     */
    public function getId_perfilpedagogico() {
        return $this->id_perfilpedagogico;
    }

    /**
     * @param id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico) {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_email
     */
    public function getSt_email() {
        return $this->st_email;
    }

    /**
     * @param st_email
     */
    public function setSt_email($st_email) {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma() {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma) {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_esquemaconfiguracao()
    {
        return $this->id_esquemaconfiguracao;
    }

    public function setId_esquemaconfiguracao($id_esquemaconfiguracao)
    {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
    }

    /**
     * @return string
     */
    public function getSt_entidadematriz()
    {
        return $this->st_entidadematriz;
    }

    /**
     * @param string $st_entidadematriz
     * @return $this
     */
    public function setSt_entidadematriz($st_entidadematriz)
    {
        $this->st_entidadematriz = $st_entidadematriz;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pesquisardisciplinanivelarea")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarDisciplinaNivelArea
{

    /**
     *
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;

    /**
     *
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255) 
     */
    private $st_disciplina;

    /**
     *
     * @var text $st_descricao
     * @Column(name="st_descricao", type="text", nullable=true) 
     */
    private $st_descricao;

    /**
     *
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false) 
     */
    private $id_tipodisciplina;

    /**
     *
     * @var string $st_tipodisciplina
     * @Column(name="st_tipodisciplina", type="string", nullable=false) 
     */
    private $st_tipodisciplina;

    /**
     *
     * @var string $nu_identificador
     * @Column(name="nu_identificador", type="string", nullable=false, length=255) 
     */
    private $nu_identificador;

    /**
     *
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false) 
     */
    private $nu_cargahoraria;

    /**
     *
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=true) 
     */
    private $nu_creditos;

    /**
     *
     * @var string $nu_codigoparceiro
     * @Column(name="nu_codigoparceiro", type="string", nullable=true, length=255) 
     */
    private $nu_codigoparceiro;

    /**
     *
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false) 
     */
    private $bl_ativa;

    /**
     *
     * @var boolean $bl_compartilhargrupo
     * @Column(name="bl_compartilhargrupo", type="boolean", nullable=false) 
     */
    private $bl_compartilhargrupo;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255) 
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="string", nullable=false) 
     */
    private $id_entidade;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", nullable=true, type="string", length=2500)
     */
    private $st_nomeentidade;

    /**
     *
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="string", nullable=true) 
     */
    private $id_nivelensino;

    /**
     *
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", nullable=true, type="string", length=255)
     */
    private $st_nivelensino;

    /**
     *
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="string", nullable=true) 
     */
    private $id_areaconhecimento;

    /**
     *
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", nullable=true, type="string", length=255)
     */
    private $st_areaconhecimento;

    /**
     * @var integer $id_grupodisciplina
     * @Column(name="id_grupodisciplina", type="string", nullable=true)
     */
    private $id_grupodisciplina;


    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getId_tipodisciplina ()
    {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina ($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    public function getSt_tipodisciplina ()
    {
        return $this->st_tipodisciplina;
    }

    public function setSt_tipodisciplina ($st_tipodisciplina)
    {
        $this->st_tipodisciplina = $st_tipodisciplina;
        return $this;
    }

    public function getNu_identificador ()
    {
        return $this->nu_identificador;
    }

    public function setNu_identificador ($nu_identificador)
    {
        $this->nu_identificador = $nu_identificador;
        return $this;
    }

    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    public function getNu_creditos ()
    {
        return $this->nu_creditos;
    }

    public function setNu_creditos ($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
        return $this;
    }

    public function getNu_codigoparceiro ()
    {
        return $this->nu_codigoparceiro;
    }

    public function setNu_codigoparceiro ($nu_codigoparceiro)
    {
        $this->nu_codigoparceiro = $nu_codigoparceiro;
        return $this;
    }

    public function getBl_ativa ()
    {
        return $this->bl_ativa;
    }

    public function setBl_ativa ($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    public function getBl_compartilhargrupo ()
    {
        return $this->bl_compartilhargrupo;
    }

    public function setBl_compartilhargrupo ($bl_compartilhargrupo)
    {
        $this->bl_compartilhargrupo = $bl_compartilhargrupo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    public function getSt_nivelensino ()
    {
        return $this->st_nivelensino;
    }

    public function setSt_nivelensino ($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    public function getSt_areaconhecimento ()
    {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento ($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getid_grupodisciplina()
    {
        return $this->id_grupodisciplina;
    }

    /**
     * @param mixed $id_grupodisciplina
     * @return VwPesquisarDisciplinaNivelArea
     */
    public function setid_grupodisciplina($id_grupodisciplina)
    {
        $this->id_grupodisciplina = $id_grupodisciplina;
        return $this;
    }



}
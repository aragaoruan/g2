<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoproduto")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoProduto extends G2Entity
{

    /**
     *
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoproduto;

    /**
     *
     * @var string $st_tabelarelacao
     * @Column(name="st_tabelarelacao", type="string", nullable=true, length=255)
     */
    private $st_tabelarelacao;

    /**
     *
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=true, length=255)
     */
    private $st_tipoproduto;

    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    public function setId_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    public function getSt_tabelarelacao()
    {
        return $this->st_tabelarelacao;
    }

    public function setSt_tabelarelacao($st_tabelarelacao)
    {
        $this->st_tabelarelacao = $st_tabelarelacao;
        return $this;
    }

    public function getSt_tipoproduto()
    {
        return $this->st_tipoproduto;
    }

    public function setSt_tipoproduto($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

}
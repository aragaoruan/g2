<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_gradepontosoft")
 * @Entity
 * @EntityView
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
use G2\G2Entity;

class VwGradePontoSoft extends G2Entity
{

    /**
     *
     * @var integer $id
     * @Id
     * @Column(name="id", type="integer", nullable=true)
     * @GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     *
     * @var integer $id_codhorarioacesso
     * @Column(name="id_codhorarioacesso", type="integer", nullable=true)
     */
    private $id_codhorarioacesso;

    /**
     * @var integer $id_gradehoraria
     * @Column(name="id_gradehoraria", type="integer", nullable=false)
     */
    private $id_gradehoraria;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false)
     */
    private $id_turma;

    /**
     *
     * @var string $st_codigocatraca
     * @Column(name="st_codigocatraca", type="string", nullable=false, length=200)
     */
    private $st_codigocatraca;

     /**
     *
     * @var integer $id_projetopedagogicosistema
     * @Column(name="id_projetopedagogicosistema", type="integer", nullable=true)
     */
    private $id_projetopedagogicosistema;

     /**
     *
     * @var string $st_turmasistema
     * @Column(name="st_turmasistema", type="string", nullable=true, length=20)
     */
    private $st_turmasistema;



    /**
     *
     * @var integer $id_diasemana
     * @Column(name="id_diasemana", type="integer", nullable=false)
     */
    private $id_diasemana;

    /**
     * @var \DateTime $dt_iniciogradehoraria
     * @Column(name="dt_iniciogradehoraria", type="datetime2")
     */
    private $dt_iniciogradehoraria;


    /**
     * @var \DateTime $dt_inicio
     * @Column(name="dt_inicio", type="datetime2")
     */
    private $dt_inicio;

    /**
     * @return int
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setid($id)
    {
        $this->id = $id;
        return $this;
    }







    /**
     * @return \DateTime
     */
    public function getdt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param \DateTime $dt_inicio
     */
    public function setdt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }





    /**
     * @return int
     */
    public function getid_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setid_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }



    /**
     * @return int
     */
    public function getid_gradehoraria()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @param int $id_gradehoraria
     */
    public function setid_gradehoraria($id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codigocatraca()
    {
        return $this->st_codigocatraca;
    }

    /**
     * @param string $st_codigocatraca
     */
    public function setst_codigocatraca($st_codigocatraca)
    {
        $this->st_codigocatraca = $st_codigocatraca;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_projetopedagogicosistema()
    {
        return $this->id_projetopedagogicosistema;
    }

    /**
     * @param int $id_projetopedagogicosistema
     */
    public function setid_projetopedagogicosistema($id_projetopedagogicosistema)
    {
        $this->id_projetopedagogicosistema = $id_projetopedagogicosistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getst_turmasistema()
    {
        return $this->st_turmasistema;
    }

    /**
     * @param int $st_turmasistema
     */
    public function setst_turmasistema($st_turmasistema)
    {
        $this->st_turmasistema = $st_turmasistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_codhorarioacesso()
    {
        return $this->id_codhorarioacesso;
    }

    /**
     * @param int $id_codhorarioacesso
     */
    public function setid_codhorarioacesso($id_codhorarioacesso)
    {
        $this->id_codhorarioacesso = $id_codhorarioacesso;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_diasemana()
    {
        return $this->id_diasemana;
    }

    /**
     * @param int $id_diasemana
     */
    public function setid_diasemana($id_diasemana)
    {
        $this->id_diasemana = $id_diasemana;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getdt_iniciogradehoraria()
    {
        return $this->dt_iniciogradehoraria;
    }

    /**
     * @param \DateTime $dt_iniciogradehoraria
     */
    public function setdt_iniciogradehoraria($dt_iniciogradehoraria)
    {
        $this->dt_iniciogradehoraria = $dt_iniciogradehoraria;
        return $this;
    }

}
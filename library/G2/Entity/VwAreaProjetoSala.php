<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_areaprojetosala")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAreaProjetoSala
{

    /**
     * @var integer $id_areaprojetosala
     * @Column(name="id_areaprojetosala", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaprojetosala;

    /**
     * @var integer $id_saladeaula
     * @Column(type="integer", name="id_saladeaula", nullable=false)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_areaconhecimento
     * @Column(type="integer", name="id_areaconhecimento", nullable=true)
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", name="id_projetopedagogico", nullable=true)
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_areaconhecimento
     * @Column(type="string", length=255, name="st_areaconhecimento", nullable=true)
     */
    private $st_areaconhecimento;

    /**
     * @var integer $id_nivelensino
     * @Column(type="integer", name="id_nivelensino", nullable=true)
     */
    private $id_nivelensino;

    /**
     * @var string $st_saladeaula
     * @Column(type="string", length=255, name="st_saladeaula", nullable=true)
     */
    private $st_saladeaula;

    /**
     * @var string $st_nivelensino
     * @Column(type="string", length=255, name="st_nivelensino", nullable=true)
     */
    private $st_nivelensino;

    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", length=255, name="st_projetopedagogico", nullable=true)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_coordenadorprojeto
     * @Column(type="string", length=255, name="st_coordenadorprojeto", nullable=true)
     */
    private $st_coordenadorprojeto;

    /**
     * @var string $st_referencia
     * @Column(type="string", length=255, name="st_referencia", nullable=true)
     */
    private $st_referencia;

    /**
     * @var string $st_descricao
     * @Column(type="string", length=255, name="st_descricao", nullable=true)
     */
    private $st_descricao;

    /**
     * @var integer $id_perfilpedagogico
     * @Column(type="integer", name="id_perfilpedagogico", nullable=true)
     */
    private $id_perfilpedagogico;

    /**
     * @var integer $nu_diasacesso
     * @Column(type="integer", name="nu_diasacesso", nullable=true)
     */
    private $nu_diasacesso;


    public function getId_areaprojetosala() {
        return $this->id_areaprojetosala;
    }

    public function setId_areaprojetosala($id_areaprojetosala) {
        $this->id_areaprojetosala = $id_areaprojetosala;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getSt_referencia() {
        return $this->st_referencia;
    }

    public function setSt_referencia($st_referencia) {
        $this->st_referencia = $st_referencia;
        return $this;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getId_nivelensino() {
        return $this->id_nivelensino;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    public function getSt_coordenadorprojeto() {
        return $this->st_coordenadorprojeto;
    }

    public function getSt_nivelensino() {
        return $this->st_nivelensino;
    }

    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setId_nivelensino($id_nivelensino) {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function setSt_coordenadorprojeto($st_coordenadorprojeto) {
        $this->st_coordenadorprojeto = $st_coordenadorprojeto;
    }


    public function setSt_nivelensino($st_nivelensino) {
        $this->st_nivelensino = $st_nivelensino;
    }

    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
    }



}


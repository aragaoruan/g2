<?php

namespace G2\Entity;
use G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipocontratoresponsavel")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @size 15/10/2014
 */
class TipoContratoResponsavel {

    /**
     * @Id
     * @var integer $id_tipocontratoresponsavel
     * @Column(name="id_tipocontratoresponsavel", type="integer", nullable=false, length=4)
     */
    private $id_tipocontratoresponsavel;


    /**
     * @var string $st_tipocontratoresponsavel
     * @Column(name="st_tipocontratoresponsavel", type="string", nullable=false, length=255)
     */
    private $st_tipocontratoresponsavel;

    public function setId_tipocontratoresponsavel($id_tipocontratoresponsavel)
    {
        $this->id_tipocontratoresponsavel = $id_tipocontratoresponsavel;
    }

    public function getId_tipocontratoresponsavel()
    {
        return $this->id_tipocontratoresponsavel;
    }

    public function setSt_tipocontratoresponsavel($st_tipocontratoresponsavel)
    {
        $this->st_tipocontratoresponsavel = $st_tipocontratoresponsavel;
    }

    public function getSt_tipocontratoresponsavel()
    {
        return $this->st_tipocontratoresponsavel;
    }




}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunosaptosagendamentopordisciplina")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

class VwAlunosAptosAgendamentoPorDisciplina
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_situacaomatricula
     * @Column(name="id_situacaomatricula", type="integer", nullable=false, length=4)
     */
    private $id_situacaomatricula;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     */
    private $id_saladeaula;

    /**
     * @var \DateTime $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var \DateTime $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_situacaosala
     * @Column(name="id_situacaosala", type="integer", nullable=false, length=4)
     */
    private $id_situacaosala;

    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_situacaoagendamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false, length=1)
     */
    private $bl_ativa;

    /**
     * @var string $st_notaatividade
     * @Column(name="st_notaatividade", type="string", nullable=false, length=5)
     */
    private $st_notaatividade;

    /**
     * @var string $st_notaead
     * @Column(name="st_notaead", type="string", nullable=false, length=5)
     */
    private $st_notaead;

    /**
     * @var string $st_notafinal
     * @Column(name="st_notafinal", type="string", nullable=false, length=5)
     */
    private $st_notafinal;

    /**
     * @var integer $nu_percentualfinal
     * @Column(name="nu_percentualfinal", type="integer", nullable=true)
     */
    private $nu_percentualfinal;

    /**
     * @var boolean $bl_aprovado
     * @Column(name="bl_aprovado", type="boolean", nullable=false)
     */
    private $bl_aprovado;

    /**
     * @return int
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     * @return $this
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaomatricula()
    {
        return $this->id_situacaomatricula;
    }

    /**
     * @param int $id_situacaomatricula
     * @return $this
     */
    public function setId_situacaomatricula($id_situacaomatricula)
    {
        $this->id_situacaomatricula = $id_situacaomatricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return $this
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param \DateTime $dt_abertura
     * @return $this
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param \DateTime $dt_encerramento
     * @return $this
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaosala()
    {
        return $this->id_situacaosala;
    }

    /**
     * @param int $id_situacaosala
     * @return $this
     */
    public function setId_situacaosala($id_situacaosala)
    {
        $this->id_situacaosala = $id_situacaosala;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param int $id_situacaoagendamento
     * @return $this
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativa()
    {
        return $this->bl_ativa;
    }

    /**
     * @param boolean $bl_ativa
     * @return $this
     */
    public function setBl_ativa($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_notaatividade()
    {
        return $this->st_notaatividade;
    }

    /**
     * @param string $st_notaatividade
     * @return $this
     */
    public function setSt_notaatividade($st_notaatividade)
    {
        $this->st_notaatividade = $st_notaatividade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_notaead()
    {
        return $this->st_notaead;
    }

    /**
     * @param string $st_notaead
     * @return $this
     */
    public function setSt_notaead($st_notaead)
    {
        $this->st_notaead = $st_notaead;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_notafinal()
    {
        return $this->st_notafinal;
    }

    /**
     * @param string $st_notafinal
     * @return $this
     */
    public function setSt_notafinal($st_notafinal)
    {
        $this->st_notafinal = $st_notafinal;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_percentualfinal()
    {
        return $this->nu_percentualfinal;
    }

    /**
     * @param int $nu_percentualfinal
     * @return $this
     */
    public function setNu_percentualfinal($nu_percentualfinal)
    {
        $this->nu_percentualfinal = $nu_percentualfinal;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_aprovado()
    {
        return $this->bl_aprovado;
    }

    /**
     * @param boolean $bl_aprovado
     * @return $this
     */
    public function setBl_aprovado($bl_aprovado)
    {
        $this->bl_aprovado = $bl_aprovado;
        return $this;
    }

}
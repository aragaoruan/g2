<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmasala")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TurmaSala {
    /**
     * @Id
     * @var integer $id_salaturma
     * @Column(name="id_salaturma", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_salaturma;

    /**
     * @var integer $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")
     */
    private $id_saladeaula;
    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @return integer id_salaturma
     */
    public function getId_salaturma() {
        return $this->id_salaturma;
    }

    /**
     * @param id_salaturma
     */
    public function setId_salaturma($id_salaturma) {
        $this->id_salaturma = $id_salaturma;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}
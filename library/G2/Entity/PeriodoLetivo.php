<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_periodoletivo")
 * @Entity(repositoryClass="\G2\Repository\PeriodoLetivo")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class PeriodoLetivo
{

    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_periodoletivo;

    /**
     * @var TipoOferta $id_tipooferta
     * @ManyToOne(targetEntity="TipoOferta")
     * @JoinColumn(name="id_tipooferta", nullable=false, referencedColumnName="id_tipooferta")
     */
    private $id_tipooferta;

    /**
     * @Column(name="st_periodoletivo", type="string", length=255, nullable=false)
     * @var string $st_periodoletivo
     */
    private $st_periodoletivo;

    /**
     * @var \Datetime $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="datetime", nullable=false)
     */
    private $dt_inicioinscricao;
    /**
     * @var \Datetime $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="datetime", nullable=false)
     */
    private $dt_fiminscricao;
    /**
     * @var \Datetime $dt_abertura
     * @Column(name="dt_abertura", type="datetime", nullable=false)
     */
    private $dt_abertura;
    /**
     * @var \Datetime $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime", nullable=false)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer",nullable=false)
     */
    private $id_entidade;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer",nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @param $dt_abertura
     * @return $this
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param $dt_encerramento
     * @return $this
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param $dt_fiminscricao
     * @return $this
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param $dt_inicioinscricao
     * @return $this
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_periodoletivo
     * @return $this
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param $id_tipooferta
     * @return $this
     */
    public function setId_tipooferta($id_tipooferta)
    {
        $this->id_tipooferta = $id_tipooferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipooferta()
    {
        return $this->id_tipooferta;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $st_periodoletivo
     * @return $this
     */
    public function setSt_periodoletivo($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_periodoletivo()
    {
        return $this->st_periodoletivo;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_afiliadoclasseentidade")
 * @Entity
 * @EntityView
 */
class VwAfiliadoClasseEntidade
{

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_afiliado
     * @Column(name="id_afiliado", type="integer")
     */
    private $id_afiliado;

    /**
     * @var string $st_afiliado
     * @Column(name="st_afiliado", type="string")
     */
    private $st_afiliado;

    /**
     * @var integer $id_classeafiliacao
     * @Column(name="id_classeafiliacao", type="integer")
     */
    private $id_classeafiliacao;

    /**
     * @var string $st_classeafiliacao
     * @Column(name="st_classeafiliacao", type="string")
     */
    private $st_classeafiliacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @return int
     */
    public function getId_afiliado()
    {
        return $this->id_afiliado;
    }

    /**
     * @param int $id_afiliado
     * @return $this
     */
    public function setId_afiliado($id_afiliado)
    {
        $this->id_afiliado = $id_afiliado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_afiliado()
    {
        return $this->st_afiliado;
    }

    /**
     * @param string $st_afiliado
     * @return $this
     */
    public function setSt_afiliado($st_afiliado)
    {
        $this->st_afiliado = $st_afiliado;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_classeafiliacao()
    {
        return $this->id_classeafiliacao;
    }

    /**
     * @param int $id_classeafiliacao
     * @return $this
     */
    public function setId_classeafiliacao($id_classeafiliacao)
    {
        $this->id_classeafiliacao = $id_classeafiliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_classeafiliacao()
    {
        return $this->st_classeafiliacao;
    }

    /**
     * @param string $st_classeafiliacao
     * @return $this
     */
    public function setSt_classeafiliacao($st_classeafiliacao)
    {
        $this->st_classeafiliacao = $st_classeafiliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 13/04/2018
 * Time: 16:18
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunosaptoscertificadoparcial")
 * @Entity
 * @EntityView
 */

class VwAlunosaptoscertificadoparcial
{
    /**
     * @var integer $id_certificadoparcial
     * @Column(name="id_certificadoparcial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_certificadoparcial;

    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false)
     */
    private $id_entidadecadastro;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=false)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_certificadoparcial
     * @Column(name="st_certificadoparcial", type="string", nullable=false)
     */
    private $st_certificadoparcial;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false)
     */
    private $st_cpf;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     * @var string $dt_impressao
     * @Column(name="dt_impressao", type="string")
     */
    private $dt_impressao;

    /**
     * @return int
     */
    public function getId_certificadoparcial()
    {
        return $this->id_certificadoparcial;
    }

    /**
     * @param int $id_certificadoparcial
     * @return $this
     */
    public function setId_certificadoparcial($id_certificadoparcial)
    {
        $this->id_certificadoparcial = $id_certificadoparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     * @return $this
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_certificadoparcial()
    {
        return $this->st_certificadoparcial;
    }

    /**
     * @param string $st_certificadoparcial
     * @return $this
     */
    public function setSt_certificadoparcial($st_certificadoparcial)
    {
        $this->st_certificadoparcial = $st_certificadoparcial;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_impressao()
    {
        return $this->dt_impressao;
    }

    /**
     * @param string $dt_impressao
     * @return $this
     */
    public function setDt_impressao($dt_impressao)
    {
        $this->dt_impressao = $dt_impressao;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_usuariomatriculaprojeto")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwUsuarioMatriculaProjeto {

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_trilha;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=false, length=40)
     */
    private $st_login;

    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=32)
     */
    private $st_senha;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_nomeexibicao
     * @Column(name="st_nomeexibicao", type="string", nullable=true, length=255)
     */
    private $st_nomeexibicao;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=5)
     */
    private $st_nomeperfil;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    
    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function getId_entidadeatendimento() {
        return $this->id_entidadeatendimento;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_perfil() {
        return $this->id_perfil;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_trilha() {
        return $this->id_trilha;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getSt_login() {
        return $this->st_login;
    }

    public function getSt_senha() {
        return $this->st_senha;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_nomeexibicao() {
        return $this->st_nomeexibicao;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function getSt_razaosocial() {
        return $this->st_razaosocial;
    }

    public function getSt_nomeperfil() {
        return $this->st_nomeperfil;
    }

    public function getSt_tituloexibicao() {
        return $this->st_tituloexibicao;
    }

    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function setId_entidadeatendimento($id_entidadeatendimento) {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_trilha($id_trilha) {
        $this->id_trilha = $id_trilha;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setSt_login($st_login) {
        $this->st_login = $st_login;
    }

    public function setSt_senha($st_senha) {
        $this->st_senha = $st_senha;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setSt_nomeexibicao($st_nomeexibicao) {
        $this->st_nomeexibicao = $st_nomeexibicao;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    public function setSt_razaosocial($st_razaosocial) {
        $this->st_razaosocial = $st_razaosocial;
    }

    public function setSt_nomeperfil($st_nomeperfil) {
        $this->st_nomeperfil = $st_nomeperfil;
    }

    public function setSt_tituloexibicao($st_tituloexibicao) {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
    }

}

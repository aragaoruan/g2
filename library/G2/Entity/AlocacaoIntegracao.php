<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @Table(name="tb_alocacaointegracao")
 * @Entity
 * @EntityLog
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class AlocacaoIntegracao extends G2Entity
{

    /**
     * @var integer $id_alocacaointegracao
     * @Column(name="id_alocacaointegracao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_alocacaointegracao;

    /**
     * @var Alocacao $id_alocacao
     * @ManyToOne(targetEntity="Alocacao")
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     */
    private $id_alocacao;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     */
    private $id_sistema;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @Column(name="st_codalocacao", type="string",length=5000,nullable=false)
     * @var string $st_codalocacao
     */
    private $st_codalocacao;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=false, length=1)
     */
    private $bl_encerrado;

    /**
     * @Column(name="st_codalocacaogrupo", type="string",length=5000,nullable=false)
     * @var string $st_codalocacaogrupo
     */
    private $st_codalocacaogrupo;

    /**
     * @var EntidadeIntegracao $id_entidadeintegracao
     * @ManyToOne(targetEntity="EntidadeIntegracao")
     * @JoinColumn(name="id_entidadeintegracao", referencedColumnName="id_entidadeintegracao")
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getId_alocacaointegracao()
    {
        return $this->id_alocacaointegracao;
    }

    /**
     * @param int $id_alocacaointegracao
     */
    public function setId_alocacaointegracao($id_alocacaointegracao)
    {
        $this->id_alocacaointegracao = $id_alocacaointegracao;
        return $this;

    }

    /**
     * @return Alocacao
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param Alocacao $id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;

    }

    /**
     * @return Sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param Sistema $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;

    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;

    }

    /**
     * @return datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }

    /**
     * @return string
     */
    public function getSt_codalocacao()
    {
        return $this->st_codalocacao;
    }

    /**
     * @param string $st_codalocacao
     */
    public function setSt_codalocacao($st_codalocacao)
    {
        $this->st_codalocacao = $st_codalocacao;
        return $this;

    }

    /**
     * @return bool
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bool $bl_encerrado
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;

    }

    /**
     * @return string
     */
    public function getSt_codalocacaogrupo()
    {
        return $this->st_codalocacaogrupo;
    }

    /**
     * @param string $st_codalocacaogrupo
     */
    public function setSt_codalocacaogrupo($st_codalocacaogrupo)
    {
        $this->st_codalocacaogrupo = $st_codalocacaogrupo;
        return $this;

    }

    /**
     * @return EntidadeIntegracao
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param EntidadeIntegracao $id_entidadeintegracao
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;

    }


}

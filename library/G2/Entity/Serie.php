<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_serie")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Serie
{

    /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_serie;

    /**
     * @var string $st_serie
     * @Column(name="st_serie", type="string", nullable=false, length=255)
     */
    private $st_serie;

    /**
     * @var integer $id_serieanterior
     * @Column(name="id_serieanterior", type="integer", nullable=true)
     */
    private $id_serieanterior;

     /**
     * @ManyToMany(targetEntity="NivelEnsino", inversedBy="serie")
     * @JoinTable(name="tb_serienivelensino",
     *  joinColumns={@JoinColumn(name="id_serie", referencedColumnName="id_serie")},
     *  inverseJoinColumns={@JoinColumn(name="id_nivelensino", referencedColumnName="id_nivelensino")}
     * )
     */
    private $nivelEnsino;

    public function __construct ()
    {
        $this->nivelEnsino = new ArrayCollection();
    }

    /**
     * Array Collection
     * @return \G2\Entity\NivelEnsino
     */
    public function get_NivelEnsino ()
    {
        return $this->nivelEnsino;
    }

    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
    }

    public function getId_serie ()
    {
        return $this->id_serie;
    }

    public function setId_serieanterior ($id_serieanterior)
    {
        $this->id_serieanterior = $id_serieanterior;
    }

    public function getId_serieanterior ()
    {
        return $this->id_serieanterior;
    }

    public function setSt_serie ($st_serie)
    {
        $this->st_serie = $st_serie;
    }

    public function getSt_serie ()
    {
        return $this->st_serie;
    }

}


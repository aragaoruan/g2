<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_pesquisarcampanha")
 * @Entity
 * @EntityView
 * @author Reinaldo Pereira <reinaldo.pereira@unyleya.com.br>
 */
class VwPesquisarCampanha
{

    /**
     *
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string")
     */
    private $st_campanhacomercial;

    /**
     * @var string $st_finalidadecampanha
     * @Column(name="st_finalidadecampanha", type="string")
     */
    private $st_finalidadecampanha;

    /**
     *
     * @var string $st_tipocampanha
     * @Column(name="st_tipocampanha", type="string")
     */
    private $st_tipocampanha;

    /**
     *
     * @var string $st_tipocampanha
     * @Column(name="st_situacao", type="string")
     */
    private $st_situacao;

    /**
     * @var integer $id_tipocampanha
     * @Column(name="id_tipocampanha", type="integer", nullable=false, length=4)
     */
    private $id_tipocampanha;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer")
     */
    private $id_situacao;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     *
     * @var integer $bl_ativo
     * @Column(name="bl_ativo", type="integer")
     */
    private $bl_ativo;

    /**
     * @return mixed
     */
    public function getst_finalidadecampanha()
    {
        return $this->st_finalidadecampanha;
    }

    /**
     * @param mixed $st_finalidadecampanha
     */
    public function setst_finalidadecampanha($st_finalidadecampanha)
    {
        $this->st_finalidadecampanha = $st_finalidadecampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return integer id_tipocampanha
     */
    public function getId_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param id_tipocampanha
     */
    public function setId_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;
    }

    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }
    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    public function getSt_campanhacomercial ()
    {
        return $this->st_campanhacomercial;
    }
    public function setSt_campanhacomercial ($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    public function getSt_tipocampanha ()
    {
        return $this->st_tipocampanha;
    }
    public function setSt_tipocampanha ($st_tipocampanha)
    {
        $this->st_tipocampanha = $st_tipocampanha;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

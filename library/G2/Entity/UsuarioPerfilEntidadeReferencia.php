<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuarioperfilentidadereferencia")
 * @Entity(repositoryClass="\G2\Repository\UsuarioPerfilEntidadeReferencia")
 * @EntityLog
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class UsuarioPerfilEntidadeReferencia extends G2Entity
{

    /**
     * @var integer $id_perfilreferencia
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_perfilreferencia", type="integer", nullable=false, length=4)
     */
    private $id_perfilreferencia;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Perfil $id_perfil
     * @ManyToOne(targetEntity="Perfil")
     * @JoinColumn(name="id_perfil", nullable=false, referencedColumnName="id_perfil")
     */
    private $id_perfil;

    /**
     * @var AreaConhecimento $id_areaconhecimento
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", referencedColumnName="id_areaconhecimento", nullable=true)
     */
    private $id_areaconhecimento;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", nullable=true, referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var SalaDeAula $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", nullable=true, referencedColumnName="id_saladeaula")
     */
    private $id_saladeaula;

    /**
     * @var Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", nullable=true, referencedColumnName="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @var Livro $id_livro
     * @ManyToOne(targetEntity="Livro", inversedBy="autores")
     * @JoinColumn(name="id_livro", referencedColumnName="id_livro", nullable=true)
     */
    private $id_livro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_titular
     * @Column(name="bl_titular", type="boolean", nullable=false, length=1)
     */
    private $bl_titular;

    /**
     * @var boolean $bl_autor
     * @Column(name="bl_autor", type="boolean", nullable=true, length=1)
     */
    private $bl_autor;

    /**
     * @var decimal $nu_porcentagem
     * @Column(name="nu_porcentagem", type="decimal", nullable=true, length=5)
     */
    private $nu_porcentagem;

    /**
     *
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false)
     */
    private $dt_inicio;

    /**
     *
     * @var datetime2 $dt_fim
     * @Column(name="dt_fim", type="datetime2", nullable=false)
     */
    private $dt_fim;


    /**
     * @var Titulacao $id_titulacao
     * @ManyToOne(targetEntity="Titulacao")
     * @JoinColumn(name="id_titulacao", nullable=true, referencedColumnName="id_titulacao")
     */
    private $id_titulacao;


    /**
     * @var boolean $bl_desativarmoodle
     * @Column(name="bl_desativarmoodle", type="boolean", nullable=true, length=1)
     */
    private $bl_desativarmoodle;


    /**
     * @return integer id_perfilreferencia
     */
    public function getId_perfilreferencia ()
    {
        return $this->id_perfilreferencia;
    }

    /**
     *
     * @param integer $id_perfilreferencia
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_perfilreferencia ($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     *
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_usuario (Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     *
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Perfil
     */
    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    /**
     *
     * @param \G2\Entity\Perfil $id_perfil
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_perfil (Perfil $id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return \G2\Entity\AreaConhecimento
     */
    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param \G2\Entity\AreaConhecimento $id_areaconhecimento
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_areaconhecimento (AreaConhecimento $id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return \G2\Entity\ProjetoPedagogico
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param \G2\Entity\ProjetoPedagogico $id_projetopedagogico
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_projetopedagogico (ProjetoPedagogico $id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return \G2\Entity\SalaDeAula
     */
    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param \G2\Entity\SalaDeAula $id_saladeaula
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_saladeaula (SalaDeAula $id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\Disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     *
     * @param \G2\Entity\Disciplina $id_disciplina
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_disciplina (Disciplina $id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return \G2\Entity\Livro
     */
    public function getId_livro ()
    {
        return $this->id_livro;
    }

    /**
     * @param \G2\Entity\Livro $id_livro
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setId_livro (Livro $id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    /**
     *
     * @return boolean bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     *
     * @param boolean $bl_ativo
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     *
     * @return boolean bl_titular
     */
    public function getBl_titular ()
    {
        return $this->bl_titular;
    }

    /**
     *
     * @param boolean $bl_titular
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setBl_titular ($bl_titular)
    {
        $this->bl_titular = $bl_titular;
        return $this;
    }

    /**
     *
     * @return boolean bl_autor
     */
    public function getBl_autor ()
    {
        return $this->bl_autor;
    }

    /**
     *
     * @param boolean $bl_autor
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setBl_autor ($bl_autor)
    {
        $this->bl_autor = $bl_autor;
        return $this;
    }

    /**
     *
     * @return decimal nu_porcentagem
     */
    public function getNu_porcentagem ()
    {
        return $this->nu_porcentagem;
    }

    /**
     *
     * @param decimal $nu_porcentagem
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function setNu_porcentagem ($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
        return $this;
    }


    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    public function getId_titulacao()
    {
        return $this->id_titulacao;
    }

    public function setId_titulacao($id_titulacao)
    {
        $this->id_titulacao = $id_titulacao;
    }

    /**
     * @return boolean
     */
    public function getBl_desativarmoodle()
    {
        return $this->bl_desativarmoodle;
    }

    /**
     * @param boolean $bl_desativarmoodle
     */
    public function setBl_desativarmoodle($bl_desativarmoodle)
    {
        $this->bl_desativarmoodle = $bl_desativarmoodle;
    }


}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pesquisarorganizacao")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarOrganizacao
{

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     *
     * @var string $st_cnpj
     * @Column(name="st_cnpj", type="string", nullable=true, length=15) 
     */
    private $st_cnpj;

    /**
     *
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=true) 
     */
    private $id_entidadepai;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=true) 
     */
    private $id_entidadecadastro;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500) 
     */
    private $st_nomeentidade;

    /**
     *
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800) 
     */
    private $st_razaosocial;

    /**
     *
     * @var integer $st_razaosocial
     * @Column(name="id_entidadeclasse", type="integer", nullable=true) 
     */
    private $id_entidadeclasse;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false) 
     */
    private $st_situacao;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_cnpj ()
    {
        return $this->st_cnpj;
    }

    public function setSt_cnpj ($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
        return $this;
    }

    public function getId_entidadepai ()
    {
        return $this->id_entidadepai;
    }

    public function setId_entidadepai ($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getSt_razaosocial ()
    {
        return $this->st_razaosocial;
    }

    public function setSt_razaosocial ($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    public function getId_entidadeclasse ()
    {
        return $this->id_entidadeclasse;
    }

    public function setId_entidadeclasse ($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
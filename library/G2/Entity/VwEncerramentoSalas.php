<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_encerramentosalas")
 * @Entity
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwEncerramentoSalas {

    /**
     * @Id
     * @Column(type="integer")
     * @var int
     */
    private $id_encerramentosala;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_matricula;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $nu_alunos;

    /**
     * @Column(type="string")
     * @var int
     */
    private $st_professor;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_saladeaula;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_disciplina;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_usuariocoordenador;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_usuariopedagogico;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_usuarioprofessor;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_usuariofinanceiro;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $dt_encerramentoprofessor;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $dt_encerramentocoordenador;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $dt_encerramentopedagogico;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $dt_encerramentofinanceiro;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_disciplina;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_saladeaula;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_areaconhecimento;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_areaconhecimento;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_entidade;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_aluno;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_tituloavaliacao;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_tipodisciplina;

    /**
     * @Column(type="float")
     * @var float
     */
    private $nu_cargahoraria;

    /**
     * @Column(type="string")
     * @var string
     */
    private $st_upload;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $id_aluno;

    /**
     * @param mixed $id_aluno
     */
    public function setId_aluno($id_aluno) {
        $this->id_aluno = $id_aluno;
    }

    /**
     * @return mixed
     */
    public function getId_aluno() {
        return $this->id_aluno;
    }

    /**
     * @return int
     */
    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_aluno() {
        return $this->st_aluno;
    }

    /**
     * @param string $st_aluno
     */
    public function setSt_aluno($st_aluno) {
        $this->st_aluno = $st_aluno;
    }

    /**
     * @return string
     */
    public function getSt_tituloavaliacao() {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param string $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao) {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    /**
     * @return int
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_usuarioprofessor() {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param int $id_usuarioprofessor
     */
    public function setId_usuarioprofessor($id_usuarioprofessor) {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    /**
     * @return int
     */
    public function getId_usuariocoordenador() {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param int $id_usuariocoordenador
     */
    public function setId_usuariocoordenador($id_usuariocoordenador) {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    /**
     * @return int
     */
    public function getId_usuariopedagogico() {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param int $id_usuariopedagogico
     */
    public function setId_usuariopedagogico($id_usuariopedagogico) {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    /**
     * @return int
     */
    public function getId_usuariofinanceiro() {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param int $id_usuariofinanceiro
     */
    public function setId_usuariofinanceiro($id_usuariofinanceiro) {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor() {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador() {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getDt_encerramentopedagogico() {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getDt_encerramentofinanceiro() {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    /**
     * @return string
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
    }

    /**
     * @return the $nu_cargahoraria
     */
    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    /**
     * @param field_type $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return the $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function getSt_upload() {
        return $this->st_upload;
    }

    /**
     * @param field $st_upload
     * @author Rafael Bruno <rafaelbruno.ti@gmail.com>
     */
    public function setSt_upload($st_upload) {
        $this->st_upload = $st_upload;
    }

    /**
     * @param mixed $st_categoriasala
     */
    public function setSt_categoriasala($st_categoriasala) {
        $this->st_categoriasala = $st_categoriasala;
    }

    /**
     * @return mixed
     */
    public function getSt_categoriasala() {
        return $this->st_categoriasala;
    }

}

<?php

namespace G2\Entity;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-01-09
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_turmatotalcargahoraria")
 */
class VwTurmaTotalCargaHoraria
{

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_turma;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150)
     */
    private $st_turma;

    /**
     * @var integer $nu_totalcargahoraria
     * @Column(name="nu_totalcargahoraria", type="integer", nullable=true, length=4)
     */
    private $nu_totalcargahoraria;

    /**
     * @return integer
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return integer
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @return integer
     */
    public function getNu_totalcargahoraria()
    {
        return $this->nu_totalcargahoraria;
    }

    /**
     * @param integer $id_projetopedagogico
     * @return \G2\Entity\VwTurmaTotalCargaHoraria
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @param integer $id_turma
     * @return \G2\Entity\VwTurmaTotalCargaHoraria
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @param integer $st_turma
     * @return \G2\Entity\VwTurmaTotalCargaHoraria
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @param integer $nu_totalcargahoraria
     * @return \G2\Entity\VwTurmaTotalCargaHoraria
     */
    public function setNu_totalcargahoraria($nu_totalcargahoraria)
    {
        $this->nu_totalcargahoraria = $nu_totalcargahoraria;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_campanhaformapagamento")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CampanhaFormaPagamento
{

    /**
     * @var integer $id_formapagamento
     * @Column(type="integer", nullable=false, name="id_formapagamento")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;

    /**
     * @var integer $id_campanhacomercial
     * @Column(type="integer", nullable=false, name="id_campanhacomercial")
     */
    private $id_campanhacomercial;

    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    public function setId_formapagamento ($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

}

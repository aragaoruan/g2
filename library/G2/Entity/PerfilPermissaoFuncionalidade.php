<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_perfilpermissaofuncionalidade")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class PerfilPermissaoFuncionalidade {

    /**
     * @Id
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @Id
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=false, length=4)
     */
    private $id_funcionalidade;

    /**
     * @Id
     * @var integer $id_permissao
     * @Column(name="id_permissao", type="integer", nullable=false, length=4)
     */
    private $id_permissao;

    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    public function getId_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    public function getId_permissao()
    {
        return $this->id_permissao;
    }

    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    public function setId_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function setId_permissao($id_permissao)
    {
        $this->id_permissao = $id_permissao;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_entidadeclasse")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEntidadeClasse
{

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_situacao;

    /**
     *
     * @var string $nu_cnpj
     * @Column(name="nu_cnpj", type="string", nullable=true, length=15)
     */
    private $nu_cnpj;

    /**
     *
     * @var string $nu_inscricaoestadual
     * @Column(name="nu_inscricaoestadual", type="string", nullable=true, length=2500)
     */
    private $nu_inscricaoestadual;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     *
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     *
     * @var string $st_urlimglogo
     * @Column(name="st_urlimglogo", type="string", nullable=true, length=1000)
     */
    private $st_urlimglogo;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_acessasistema
     * @Column(name="bl_acessasistema", type="boolean", nullable=false)
     */
    private $bl_acessasistema;

    /**
     *
     * @var integer $id_entidadeclasse
     * @Column(name="id_entidadeclasse", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidadeclasse;

    /**
     *
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidadepai;

    /**
     *
     * @var string $st_entidadeclasse
     * @Column(name="st_entidadeclasse", type="string", nullable=true, length=255)
     */
    private $st_entidadeclasse;

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getNu_cnpj ()
    {
        return $this->nu_cnpj;
    }

    public function setNu_cnpj ($nu_cnpj)
    {
        $this->nu_cnpj = $nu_cnpj;
        return $this;
    }

    public function getNu_inscricaoestadual ()
    {
        return $this->nu_inscricaoestadual;
    }

    public function setNu_inscricaoestadual ($nu_inscricaoestadual)
    {
        $this->nu_inscricaoestadual = $nu_inscricaoestadual;
        return $this;
    }

    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getSt_razaosocial ()
    {
        return $this->st_razaosocial;
    }

    public function setSt_razaosocial ($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    public function getSt_urlimglogo ()
    {
        return $this->st_urlimglogo;
    }

    public function setSt_urlimglogo ($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getBl_acessasistema ()
    {
        return $this->bl_acessasistema;
    }

    public function setBl_acessasistema ($bl_acessasistema)
    {
        $this->bl_acessasistema = $bl_acessasistema;
        return $this;
    }

    public function getId_entidadeclasse ()
    {
        return $this->id_entidadeclasse;
    }

    public function setId_entidadeclasse ($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    public function getId_entidadepai ()
    {
        return $this->id_entidadepai;
    }

    public function setId_entidadepai ($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    public function getSt_entidadeclasse ()
    {
        return $this->st_entidadeclasse;
    }

    public function setSt_entidadeclasse ($st_entidadeclasse)
    {
        $this->st_entidadeclasse = $st_entidadeclasse;
        return $this;
    }

}
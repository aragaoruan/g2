<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\PerguntasFrequentes")
 * @Table(name="tb_pergunta")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Pergunta
{

    /**
     * @var integer $id_pergunta
     * @Column(type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pergunta;

    /**
     * @var text $st_pergunta
     * @Column(type="text", nullable=false)
     */
    private $st_pergunta;

    /**
     * @var text $st_resposta
     * @Column(type="text", nullable=false)
     */
    private $st_resposta;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro ,
     * @Column(type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var CategoriaPergunta $id_categoriapergunta
     * @ManyToOne(targetEntity="CategoriaPergunta")
     * @JoinColumn(name="id_categoriapergunta", referencedColumnName="id_categoriapergunta")
     */
    private $id_categoriapergunta;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @return int
     */
    public function getId_pergunta()
    {
        return $this->id_pergunta;
    }

    /**
     * @return text
     */
    public function getSt_pergunta()
    {
        return $this->st_pergunta;
    }

    /**
     * @return text
     */
    public function getSt_resposta()
    {
        return $this->st_resposta;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return CategoriaPergunta
     */
    public function getId_categoriapergunta()
    {
        return $this->id_categoriapergunta;
    }

    /**
     * @param $id_pergunta
     * @return $this
     */
    public function setId_pergunta($id_pergunta)
    {
        $this->id_pergunta = $id_pergunta;
        return $this;
    }

    /**
     * @param $st_pergunta
     * @return $this
     */
    public function setSt_pergunta($st_pergunta)
    {
        $this->st_pergunta = $st_pergunta;
        return $this;
    }

    /**
     * @param $st_resposta
     * @return $this
     */
    public function setSt_resposta($st_resposta)
    {
        $this->st_resposta = $st_resposta;
        return $this;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param CategoriaPergunta $id_categoriapergunta
     * @return $this
     */
    public function setId_categoriapergunta(CategoriaPergunta $id_categoriapergunta)
    {
        $this->id_categoriapergunta = $id_categoriapergunta;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro(Entidade $id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

}

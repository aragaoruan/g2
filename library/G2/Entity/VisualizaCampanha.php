<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_visualizacampanha")
 * @Entity
 * @EntityLog
 */
class VisualizaCampanha extends G2Entity
{

    /**
     * @var integer $id_visualizacampanha
     * @Column(name="id_visualizacampanha", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     * @Id
     */
    private $id_visualizacampanha;

    /**
     * @var CampanhaComercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhacomercial;

    /**
     * @var Usuario
     *
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @return int
     */
    public function getId_visualizacampanha()
    {
        return $this->id_visualizacampanha;
    }

    /**
     * @param int $id_visualizacampanha
     */
    public function setId_visualizacampanha($id_visualizacampanha)
    {
        $this->id_visualizacampanha = $id_visualizacampanha;
    }

    /**
     * @return CampanhaComercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param CampanhaComercial $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

}

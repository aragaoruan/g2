<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_clientevenda")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwClienteVenda extends \G2\G2Entity
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     *
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true)
     */
    private $st_email;

    /**
     *
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     *
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     *
     * @var integer $id_venda
     * @Id
     * @Column(name="id_venda", type="integer", nullable=false)
     */
    private $id_venda;

    /**
     *
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false)
     */
    private $nu_valorliquido;

    /**
     *
     * @var boolean $bl_contrato
     * @Column(name="bl_contrato", type="boolean", nullable=false)
     */
    private $bl_contrato;

    /**
     *
     * @var decimal $nu_valorbruto
     * @Column(name="nu_valorbruto", type="decimal", nullable=false)
     */
    private $nu_valorbruto;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     *
     * @var integer $id_evolucaocontrato
     * @Column(name="id_evolucaocontrato", type="integer", nullable=true)
     */
    private $id_evolucaocontrato;

    /**
     *
     * @var integer $id_situacaocontrato
     * @Column(name="id_situacaocontrato", type="integer", nullable=true)
     */
    private $id_situacaocontrato;

    /**
     *
     * @var integer $id_textosistemarecibo
     * @Column(name="id_textosistemarecibo", type="integer", nullable=true)
     */
    private $id_textosistemarecibo;

    /**
     *
     * @var integer $id_reciboconsolidado
     * @Column(name="id_reciboconsolidado", type="integer", nullable=true)
     */
    private $id_reciboconsolidado;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_cpf ()
    {
        return $this->st_cpf;
    }

    public function setSt_cpf ($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function getId_evolucao ()
    {
        return $this->id_evolucao;
    }

    public function setId_evolucao ($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function getSt_evolucao ()
    {
        return $this->st_evolucao;
    }

    public function setSt_evolucao ($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    public function getId_venda ()
    {
        return $this->id_venda;
    }

    public function setId_venda ($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function getNu_valorliquido ()
    {
        return $this->nu_valorliquido;
    }

    public function setNu_valorliquido ($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    public function getBl_contrato ()
    {
        return $this->bl_contrato;
    }

    public function setBl_contrato ($bl_contrato)
    {
        $this->bl_contrato = $bl_contrato;
        return $this;
    }

    public function getNu_valorbruto ()
    {
        return $this->nu_valorbruto;
    }

    public function setNu_valorbruto ($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getId_evolucaocontrato ()
    {
        return $this->id_evolucaocontrato;
    }

    public function setId_evolucaocontrato ($id_evolucaocontrato)
    {
        $this->id_evolucaocontrato = $id_evolucaocontrato;
        return $this;
    }

    public function getId_situacaocontrato ()
    {
        return $this->id_situacaocontrato;
    }

    public function setId_situacaocontrato ($id_situacaocontrato)
    {
        $this->id_situacaocontrato = $id_situacaocontrato;
        return $this;
    }

    public function getId_textosistemarecibo ()
    {
        return $this->id_textosistemarecibo;
    }

    public function setId_textosistemarecibo ($id_textosistemarecibo)
    {
        $this->id_textosistemarecibo = $id_textosistemarecibo;
        return $this;
    }

    public function getId_reciboconsolidado ()
    {
        return $this->id_reciboconsolidado;
    }

    public function setId_reciboconsolidado ($id_reciboconsolidado)
    {
        $this->id_reciboconsolidado = $id_reciboconsolidado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwClienteVenda
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }



}

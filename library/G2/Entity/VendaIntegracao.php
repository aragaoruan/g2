<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity()
 * @Table( name="tb_vendaintegracao")
 * @HasLifecycleCallbacks
 */
class VendaIntegracao
{

    /**
     * @var integer $id_vendaintegracao
     * @id
     * @Column(name="id_vendaintegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_vendaintegracao;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumns({
     *      @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     * })
     */
    private $id_sistema;

    /**
     * @var Venda
     *
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumns({
     *      @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     * })
     */
    private $id_venda;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var string $st_vendaexterna
     * @Column(name="st_vendaexterna", type="string", nullable=true)
     */
    private $st_vendaexterna;

    /**
     * @var string $st_urlvendaexterna
     * @Column(name="st_urlvendaexterna", type="string", nullable=true)
     */
    private $st_urlvendaexterna;

    /**
     * @var string $st_urlconfirmacao
     * @Column(name="st_urlconfirmacao", type="string", nullable=true)
     */
    private $st_urlconfirmacao;

    /**
     * @var string $st_codvenda
     * @Column(name="st_codvenda", type="string", nullable=true)
     */
    private $st_codvenda;

    /**
     * @var string $st_codresponsavel
     * @Column(name="st_codresponsavel", type="string")
     */
    private $st_codresponsavel;

    /**
     * @return string
     */
    public function getSt_codresponsavel()
    {
        return $this->st_codresponsavel;
    }

    /**
     * @param string $st_codresponsavel
     */
    public function setSt_codresponsavel($st_codresponsavel)
    {
        $this->st_codresponsavel = $st_codresponsavel;
    }

    /**
     * @return int
     */
    public function getId_vendaintegracao()
    {
        return $this->id_vendaintegracao;
    }

    /**
     * @param int $id_vendaintegracao
     */
    public function setId_vendaintegracao($id_vendaintegracao)
    {
        $this->id_vendaintegracao = $id_vendaintegracao;
    }

    /**
     * @return Sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param Sistema $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return Venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param Venda $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return string
     */
    public function getSt_vendaexterna()
    {
        return $this->st_vendaexterna;
    }

    /**
     * @param string $st_vendaexterna
     */
    public function setSt_vendaexterna($st_vendaexterna)
    {
        $this->st_vendaexterna = $st_vendaexterna;
    }

    /**
     * @return string
     */
    public function getSt_urlvendaexterna()
    {
        return $this->st_urlvendaexterna;
    }

    /**
     * @param string $st_urlvendaexterna
     */
    public function setSt_urlvendaexterna($st_urlvendaexterna)
    {
        $this->st_urlvendaexterna = $st_urlvendaexterna;
    }

    /**
     * @return string
     */
    public function getSt_urlconfirmacao()
    {
        return $this->st_urlconfirmacao;
    }

    /**
     * @param string $st_urlconfirmacao
     */
    public function setSt_urlconfirmacao($st_urlconfirmacao)
    {
        $this->st_urlconfirmacao = $st_urlconfirmacao;
    }

    /**
     * @return string
     */
    public function getSt_codvenda()
    {
        return $this->st_codvenda;
    }

    /**
     * @param string $st_codvenda
     */
    public function setSt_codvenda($st_codvenda)
    {
        $this->st_codvenda = $st_codvenda;
    }

    /**
     * @PrePersist
     */
    public function prePersist(){
        $this->setDt_cadastro(new \DateTime());
    }
}



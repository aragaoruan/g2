<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoaplicacao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */


class AvaliacaoAplicacao {

     /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoaplicacao;
      /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true)
     */
    private $id_aplicadorprova;
     /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer", nullable=true)
     */
    private $id_endereco;
      /**
     * @var integer $nu_maxaplicacao
     * @Column(name="nu_maxaplicacao",type="integer",nullable=true)
     */
    private $nu_maxaplicacao;
      /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula",type="integer",nullable=true)
     */
    private $id_horarioaula;
   /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer",nullable=false)
     */
    private $id_usuariocadastro;
    /**
     * @Column(name="dt_aplicacao", type="date", nullable=true)
     * @var date $dt_aplicacao
     */
    private $dt_aplicacao;
     /**
     * @Column(name="dt_alteracaolimite", type="date", nullable=false)
     * @var date $dt_alteracaolimite
     */
    private $dt_alteracaolimite;
    /**
     * @Column(name="dt_antecedenciaminima", type="date", nullable=false)
     * @var date $dt_antecedenciaminima
     */
    private $dt_antecedenciaminima;
    
    /**
     * @Column(name="bl_unica",type="boolean",nullable=false)
     * @var boolean $bl_unica
     */
    private $bl_unica;
    /**
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     * @var datetime2 $dt_cadastro
     */
    private $dt_cadastro;
    
    /**
     * @Column(name="bl_ativo",type="boolean",nullable=false)
     * @var boolean $bl_ativo
     */
    private $bl_ativo;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;
   

    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    public function getId_endereco() {
        return $this->id_endereco;
    }

    public function getNu_maxaplicacao() {
        return $this->nu_maxaplicacao;
    }

    public function getId_horarioaula() {
        return $this->id_horarioaula;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getDt_aplicacao() {
        return $this->dt_aplicacao;
    }

    public function getDt_alteracaolimite() {
        return $this->dt_alteracaolimite;
    }

    public function getDt_antecedenciaminima() {
        return $this->dt_antecedenciaminima;
    }

    public function getBl_unica() {
        return $this->bl_unica;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function setId_aplicadorprova($id_aplicadorprova) {
        $this->id_aplicadorprova = $id_aplicadorprova;
        return $this;
    }

    public function setId_endereco($id_endereco) {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    public function setNu_maxaplicacao($nu_maxaplicacao) {
        $this->nu_maxaplicacao = $nu_maxaplicacao;
        return $this;
    }

    public function setId_horarioaula($id_horarioaula) {
        $this->id_horarioaula = $id_horarioaula;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setDt_aplicacao($dt_aplicacao) {
        $this->dt_aplicacao = $dt_aplicacao;
        return $this;
    }

    public function setDt_alteracaolimite($dt_alteracaolimite) {
        $this->dt_alteracaolimite = $dt_alteracaolimite;
        return $this;
    }

    public function setDt_antecedenciaminima($dt_antecedenciaminima) {
        $this->dt_antecedenciaminima = $dt_antecedenciaminima;
        return $this;
    }

    public function setBl_unica($bl_unica) {
        $this->bl_unica = $bl_unica;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setId_entidade(\G2\Entity\Entidade $id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

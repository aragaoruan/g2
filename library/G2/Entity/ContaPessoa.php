<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_contapessoa")
 * @Entity
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class ContaPessoa
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_contapessoa
     * @Column(name="id_contapessoa", type="integer", nullable=false, length=4)
     */
    private $id_contapessoa;
    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var integer $id_tipodeconta
     * @Column(name="id_tipodeconta", type="integer")
     */
    private $id_tipodeconta;
    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;
    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false, length=1)
     */
    private $bl_padrao;
    /**
     * @var string $st_banco
     * @Column(name="st_banco", type="string", nullable=false, length=2)
     */
    private $st_banco;
    /**
     * @var string $st_digitoagencia
     * @Column(name="st_digitoagencia", type="string", nullable=true, length=2)
     */
    private $st_digitoagencia;
    /**
     * @var string $st_agencia
     * @Column(name="st_agencia", type="string", nullable=false, length=20)
     */
    private $st_agencia;
    /**
     * @var string $st_digitoconta
     * @Column(name="st_digitoconta", type="string", nullable=true, length=2)
     */
    private $st_digitoconta;
    /**
     * @var string $st_conta
     * @Column(name="st_conta", type="string", nullable=false, length=10)
     */
    private $st_conta;
    /**
     * @var string $nu_cnpj
     * @Column(name="nu_cnpj", type="string", nullable=false, length=10)
     */
    private $nu_cnpj;

    /**
     * @var TipoPessoa $id_tipopessoa
     * @ManyToOne(targetEntity="TipoPessoa")
     * @JoinColumn(name="id_tipopessoa", nullable=true, referencedColumnName="id_tipopessoa")
     */
    private $id_tipopessoa;

    /**
     * @return integer id_contapessoa
     */
    public function getId_contapessoa()
    {
        return $this->id_contapessoa;
    }

    /**
     * @param id_usuario
     */
    public function setId_contapessoa($id_contapessoa)
    {
        $this->id_contapessoa = $id_contapessoa;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_tipodeconta
     */
    public function getId_tipodeconta()
    {
        return $this->id_tipodeconta;
    }

    /**
     * @param id_tipodeconta
     */
    public function setId_tipodeconta($id_tipodeconta)
    {
        $this->id_tipodeconta = $id_tipodeconta;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_padrao
     */
    public function getBl_padrao()
    {
        return $this->bl_padrao;
    }

    /**
     * @param bl_padrao
     */
    public function setBl_padrao($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    /**
     * @return string st_banco
     */
    public function getSt_banco()
    {
        return $this->st_banco;
    }

    /**
     * @param st_banco
     */
    public function setSt_banco($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

    /**
     * @return string st_digitoagencia
     */
    public function getSt_digitoagencia()
    {
        return $this->st_digitoagencia;
    }

    /**
     * @param st_digitoagencia
     */
    public function setSt_digitoagencia($st_digitoagencia)
    {
        $this->st_digitoagencia = $st_digitoagencia;
        return $this;
    }

    /**
     * @return string st_agencia
     */
    public function getSt_agencia()
    {
        return $this->st_agencia;
    }

    /**
     * @param st_agencia
     */
    public function setSt_agencia($st_agencia)
    {
        $this->st_agencia = $st_agencia;
        return $this;
    }

    /**
     * @return string st_digitoconta
     */
    public function getSt_digitoconta()
    {
        return $this->st_digitoconta;
    }

    /**
     * @param st_digitoconta
     */
    public function setSt_digitoconta($st_digitoconta)
    {
        $this->st_digitoconta = $st_digitoconta;
        return $this;
    }

    /**
     * @return string st_conta
     */
    public function getSt_conta()
    {
        return $this->st_conta;
    }

    /**
     * @param st_conta
     */
    public function setSt_conta($st_conta)
    {
        $this->st_conta = $st_conta;
        return $this;
    }

    /**
     * @return string nu_cnpj
     */
    public function getSt_cnpj()
    {
        return $this->nu_cnpj;
    }

    /**
     * @param st_conta
     */
    public function setSt_cnpj($nu_cnpj)
    {
        $this->nu_cnpj = $nu_cnpj;
        return $this;
    }

    /**
     * @return string tipodepessoa
     */
    public function getId_tipopessoa()
    {
        return $this->id_tipopessoa;
    }

    /**
     * @param id_tipopessoa
     */
    public function setId_tipopessoa($id_tipopessoa)
    {
        $this->id_tipopessoa = $id_tipopessoa;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_afiliado")
 * @Entity
 */
class Afiliado
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_afiliado
     * @Column(name="id_afiliado", type="integer", nullable=false, length=4)
     */
    private $id_afiliado;

    /**
     * @var string $st_afiliado
     * @Column(name="st_afiliado", type="string", nullable=false, length=255)
     */
    private $st_afiliado;

    /**
     * @return int
     */
    public function getId_afiliado()
    {
        return $this->id_afiliado;
    }

    /**
     * @param int $id_afiliado
     * @return $this
     */
    public function setId_afiliado($id_afiliado)
    {
        $this->id_afiliado = $id_afiliado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_afiliado()
    {
        return $this->st_afiliado;
    }

    /**
     * @param string $st_afiliado
     * @return $this
     */
    public function setSt_afiliado($st_afiliado)
    {
        $this->st_afiliado = $st_afiliado;
        return $this;
    }
}
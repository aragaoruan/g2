<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity(repositoryClass="G2\Repository\TotvsVwIntegraGestorFluxus")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="totvs.vw_Integra_GestorFluxus")
 */
class TotvsVwIntegraGestorFluxus {

    /**
     * @var smallint $codcoligada_fcfo
     * @Column(name="CODCOLIGADA_FCFO", type="smallint", length=2)
     */
    private $codcoligada_fcfo;

    /**
     * @var string $codcfo_fcfo
     * @Column(name="CODCFO_FCFO", type="string", length=25)
     */
    private $codcfo_fcfo;

    /**
     * @var string $nomefantasia_fcfo
     * @Column(name="NOMEFANTASIA_FCFO", type="string", length=60)
     */
    private $nomefantasia_fcfo;

    /**
     * @var string $nome_fcfo
     * @Column(name="NOME_FCFO", type="string", length=60)
     */
    private $nome_fcfo;

    /**
     * @var string $cgccfo_fcfo
     * @Column(name="CGCCFO_FCFO", type="string", length=20)
     */
    private $cgccfo_fcfo;

    /**
     * @var string $inscrestadual_fcfo
     * @Column(name="INSCRESTADUAL_FCFO", type="string", length=20)
     */
    private $inscrestadual_fcfo;

    /**
     * @var string $pagrec_fcfo
     * @Column(name="PAGREC_FCFO", type="string", length=1)
     */
    private $pagrec_fcfo;

    /**
     * @var string $tipo_rua
     * @Column(name="TIPORUA", type="string", length=20)
     *
     * @Assert\Length(max = 20, maxMessage="Rua (TIPORUA_FCFO) deve conter no máximo {{ limit }} caracteres.")
     */
    private $tiporua;

    /**
     * @var string $rua_fcfo
     * @Column(name="RUA_FCFO", type="string", length=100)
     */
    private $rua_fcfo;

    /**
     * @var string $numero_fcfo
     * @Column(name="NUMERO_FCFO", type="string", length=8)
     */
    private $numero_fcfo;

    /**
     * @var string $complemento_fcfo
     * @Column(name="COMPLEMENTO_FCFO", type="string", length=30)
     */
    private $complemento_fcfo;

    /**
     * @var string $tipobairro_fcfo
     * @Column(name="TIPOBAIRRO_FCFO", type="integer")
     *
     */
    private $tipobairro_fcfo;

    /**
     * @var string $bairro_fcfo
     * @Column(name="BAIRRO_FCFO", type="string", length=30)
     */
    private $bairro_fcfo;

    /**
     * @var string $cidade_fcfo
     * @Column(name="CIDADE_FCFO", type="string", length=32)
     */
    private $cidade_fcfo;

    /**
     * @var string $codetd_fcfo
     * @Column(name="CODETD_FCFO", type="string", length=2)
     */
    private $codetd_fcfo;

    /**
     * @var string $cep_fcfo
     * @Column(name="CEP_FCFO", type="string", length=9)
     */
    private $cep_fcfo;

    /**
     * @var string $telefone_fcfo
     * @Column(name="TELEFONE_FCFO", type="string", length=15)
     */
    private $telefone_fcfo;

    /**
     * @var string $fax_fcfo
     * @Column(name="FAX_FCFO", type="string", length=15)
     */
    private $fax_fcfo;

    /**
     * @var string $email_fcfo
     * @Column(name="EMAIL_FCFO", type="string", length=60)
     */
    private $email_fcfo;

    /**
     * @var string $contato_fcfo
     * @Column(name="CONTATO_FCFO", type="string", length=40)
     */
    private $contato_fcfo;

    /**
     * @var string $ativo_fcfo
     * @Column(name="ATIVO_FCFO", type="string", length=1)
     */
    private $ativo_fcfo;

    /**
     * @var string $codmunicipio_fcfo
     * @Column(name="CODMUNICIPIO_FCFO", type="string", length=20)
     *
     * Length(max=20, maxMessage="")
     */
    private $codmunicipio_fcfo;

    /**
     * @var string $pessoafisoujur_fcfo
     * @Column(name="PESSOAFISOUJUR_FCFO", type="string", length=1)
     */
    private $pessoafisoujur_fcfo;

    /**
     * @var string $idpais_fcfo
     * @Column(name="IDPAIS_FCFO", type="string", length=20)
     *
     * @Assert\Length(max = 20, maxMessage="País (IDPAIS_FCFO) deve conter no máximo {{ limit }} caracteres.")
     */
    private $idpais_fcfo;

    /**
     * @var string $pais_fcfo
     * @Column(name="PAIS_FCFO", type="string", length=20)
     */
    private $pais_fcfo;

    /**
     * @var string $idcfo_fcfo
     * @Column(name="IDCFO_FCFO", type="string")
     */
    private $idcfo_fcfo;

    /**
     * @var string $cei_fcfo
     * @Column(name="CEI_FCFO", type="string", length=20)
     */
    private $cei_fcfo;

    /**
     * @var string $nacionalidade_fcfo
     * @Column(name="NACIONALIDADE_FCFO", type="integer", length=1)
     */
    private $nacionalidade_fcfo;

    /**
     * @var string $tipocontribuinteinss_fcfo
     * @Column(name="TIPOCONTRIBUINTEINSS_FCFO", type="string")
     */
    private $tipocontribuinteinss_fcfo;

    /**
     * @var string $idlan
     * @Column(name="IDLAN", type="string")
     */
    private $idlan_fcfo;

    /**
     * @var string $numerodocumento
     * @Column(name="NUMERODOCUMENTO", type="string", length=40)
     */
    private $numerodocumento;

    /**
     * @var string $pagrec
     * @Column(name="PAGREC", type="integer", length=1)
     */
    private $pagrec;

    /**
     * @var string $statuslan
     * @Column(name="STATUSLAN", type="integer", length=1)
     */
    private $statuslan;

    /**
     * @var string $datavencimento
     * @Column(name="DATAVENCIMENTO", type="datetime", length=40)
     */
    private $datavencimento;

    /**
     * @var string $dataemissao
     * @Column(name="DATAEMISSAO", type="datetime", length=40)
     */
    private $dataemissao;

    /**
     * @var string $dataprevbaixa
     * @Column(name="DATAPREVBAIXA", type="datetime", length=40)
     */
    private $dataprevbaixa;

    /**
     * @var datetime $databaixa
     * @Column(name="DATABAIXA", type="datetime")
     */
    private $databaixa;

    /**
     * @var string $valororiginal
     * @Column(name="VALORORIGINAL", type="decimal", precision=10, scale=2)
     */
    private $valororiginal;

    /**
     * @var string $valorbaixado
     * @Column(name="VALORBAIXADO", type="decimal", precision=10, scale=2)
     */
    private $valorbaixado;

    /**
     * @var string $valorcap
     * @Column(name="VALORCAP", type="decimal", precision=10, scale=2)
     */
    private $valorcap;

    /**
     * @var string $valorjuros
     * @Column(name="VALORJUROS", type="decimal", precision=10, scale=2)
     */
    private $valorjuros;

    /**
     * @var string $valordesconto
     * @Column(name="VALORDESCONTO", type="decimal", precision=10, scale=2)
     */
    private $valordesconto;

    /**
     * @var string $valorop1
     * @Column(name="VALOROP1", type="decimal", precision=10, scale=2)
     */
    private $valorop1;

    /**
     * @var string $valorop2
     * @Column(name="VALOROP2", type="decimal", precision=10, scale=2)
     */
    private $valorop2;

    /**
     * @var string $valorop3
     * @Column(name="VALOROP3", type="decimal", precision=10, scale=2)
     */
    private $valorop3;

    /**
     * @var string $valormulta
     * @Column(name="VALORMULTA", type="decimal", precision=10, scale=2)
     */
    private $valormulta;

    /**
     * @var string $jurosdia
     * @Column(name="JUROSDIA", type="decimal", precision=10, scale=2)
     */
    private $jurosdia;

    /**
     * @var string $capmensal
     * @Column(name="CAPMENSAL", type="decimal", precision=10, scale=2)
     */
    private $capmensal;

    /**
     * @var string $taxasvendor
     * @Column(name="TAXASVENDOR", type="decimal", precision=10, scale=2)
     */
    private $taxasvendor;

    /**
     * @var string $jurosvendor
     * @Column(name="JUROSVENDOR", type="decimal", precision=10, scale=2)
     */
    private $jurosvendor;

    /**
     * @var string $codcxa
     * @Column(name="CODCXA", type="string", length=10)
     */
    private $codcxa;

    /**
     * @var string $codtdo
     * @Column(name="CODTDO", type="string", length=40)
     */
    private $codtdo;

    /**
     * @var string $codtb1flx
     * @Column(name="CODTB1FLX", type="string", length=25)
     */
    private $codtb1flx;

    /**
     * @var string $codtb2flx
     * @Column(name="CODTB2FLX", type="string", length=25)
     */
    private $codtb2flx;

    /**
     * @var string $dataop2
     * @Column(name="DATAOP2", type="datetime", length=20)
     */
    private $dataop2;

    /**
     * @var string $campoalfaop1
     * @Column(name="CAMPOALFAOP1", type="string", length=100)
     */
    private $campoalfaop1;

    /**
     * @var string $campoalfaop2
     * @Column(name="CAMPOALFAOP2", type="string", length=20)
     */
    private $campoalfaop2;

    /**
     * @var string $historico
     * @Column(name="HISTORICO", type="string", length=100)
     */
    private $historico;

    /**
     * @var string $codcusto
     * @Column(name="CODCCUSTO", type="string", length=100)
     */
    private $codcusto;

    /**
     * @var string $numerocheque
     * @Column(name="NUMEROCHEQUE", type="string", length=20)
     */
    private $numerocheque;

    /**
     * @var string $datavencimento_cheque
     * @Column(name="DATAVENCIMENTO_CHEQUE", type="datetime", length=23)
     */
    private $datavencimento_cheque;

    /**
     * @var string $codigobanco
     * @Column(name="CODIGOBANCO", type="string", length=3)
     */
    private $codigobanco;

    /**
     * @var string $codigoagencia
     * @Column(name="CODIGOAGENCIA", type="string", length=6)
     */
    private $codigoagencia;

    /**
     * @var string $contacorrente
     * @Column(name="CONTACORRENTE", type="string", length=15)
     */
    private $contacorrente;

    /**
     * @var smallint $compensado
     * @Column(name="COMPENSADO", type="integer")
     */
    private $compensado;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer")
     */
    private $id_contrato;

    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer")
     * @Id
     */
    private $id_lancamento;

    /**
     * @var boolean $bl_entrada
     * @Column(name="bl_entrada", type="boolean")
     */
    private $bl_entrada;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer")
     */
    private $nu_ordem;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var string $st_caminho
     * @Column(name="st_caminho", type="string", length=800)
     */
    private $st_caminho;

    /**
     * @var string $st_linkedserver
     * @Column(name="st_linkedserver", type="string", length=20)
     */
    private $st_linkedserver;

    /**
     * @var string $numautocartao
     * @Column(name="NUMAUTOCARTAO", type="string", length=20)
     */
    private $numautocartao;

    /**
     * @var string $numcartaocredito
     * @Column(name="NUMCARTAOCREDITO", type="string", length=30)
     */
    private $numcartaocredito;

    /**
     * @var string $st_coddocumento
     * @Column(name="st_coddocumento", type="string", length=255)
     */
    private $st_coddocumento;

    /**
     * @var string $idacordo
     * @Column(name="IDACORDO", type="string", length=30)
     */
    private $idacordo;

    /**
     * @param $property
     * @param $value
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * @param string $ativo_fcfo
     */
    public function setAtivoFcfo($ativo_fcfo)
    {
        $this->ativo_fcfo = $ativo_fcfo;
    }

    /**
     * @return string
     */
    public function getAtivoFcfo()
    {
        return $this->ativo_fcfo;
    }

    /**
     * @param string $bairro_fcfo
     */
    public function setBairroFcfo($bairro_fcfo)
    {
        $this->bairro_fcfo = $bairro_fcfo;
    }

    /**
     * @return string
     */
    public function getBairroFcfo()
    {
        return $this->bairro_fcfo;
    }

    /**
     * @param boolean $bl_entrada
     */
    public function setBlEntrada($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
    }

    /**
     * @return boolean
     */
    public function getBlEntrada()
    {
        return $this->bl_entrada;
    }

    /**
     * @param string $campoalfaop1
     */
    public function setCampoalfaop1($campoalfaop1)
    {
        $this->campoalfaop1 = $campoalfaop1;
    }

    /**
     * @return string
     */
    public function getCampoalfaop1()
    {
        return $this->campoalfaop1;
    }

    /**
     * @param string $campoalfaop2
     */
    public function setCampoalfaop2($campoalfaop2)
    {
        $this->campoalfaop2 = $campoalfaop2;
    }

    /**
     * @return string
     */
    public function getCampoalfaop2()
    {
        return $this->campoalfaop2;
    }

    /**
     * @param string $capmensal
     */
    public function setCapmensal($capmensal)
    {
        $this->capmensal = $capmensal;
    }

    /**
     * @return string
     */
    public function getCapmensal()
    {
        return $this->capmensal;
    }

    /**
     * @param string $cei_fcfo
     */
    public function setCeiFcfo($cei_fcfo)
    {
        $this->cei_fcfo = $cei_fcfo;
    }

    /**
     * @return string
     */
    public function getCeiFcfo()
    {
        return $this->cei_fcfo;
    }

    /**
     * @param string $cep_fcfo
     */
    public function setCepFcfo($cep_fcfo)
    {
        $this->cep_fcfo = $cep_fcfo;
    }

    /**
     * @return string
     */
    public function getCepFcfo()
    {
        return $this->cep_fcfo;
    }

    /**
     * @param string $cgccfo_fcfo
     */
    public function setCgccfoFcfo($cgccfo_fcfo)
    {
        $this->cgccfo_fcfo = $cgccfo_fcfo;
    }

    /**
     * @return string
     */
    public function getCgccfoFcfo()
    {
        return $this->cgccfo_fcfo;
    }

    /**
     * @param string $cidade_fcfo
     */
    public function setCidadeFcfo($cidade_fcfo)
    {
        $this->cidade_fcfo = $cidade_fcfo;
    }

    /**
     * @return string
     */
    public function getCidadeFcfo()
    {
        return $this->cidade_fcfo;
    }

    /**
     * @param string $codcfo_fcfo
     */
    public function setCodcfoFcfo($codcfo_fcfo)
    {
        $this->codcfo_fcfo = $codcfo_fcfo;
    }

    /**
     * @return string
     */
    public function getCodcfoFcfo()
    {
        return $this->codcfo_fcfo;
    }

    /**
     * @param \G2\Entity\smallint $codcoligada_fcfo
     */
    public function setCodcoligadaFcfo($codcoligada_fcfo)
    {
        $this->codcoligada_fcfo = $codcoligada_fcfo;
    }

    /**
     * @return \G2\Entity\smallint
     */
    public function getCodcoligadaFcfo()
    {
        return $this->codcoligada_fcfo;
    }

    /**
     * @param string $codcusto
     */
    public function setCodcusto($codcusto)
    {
        $this->codcusto = $codcusto;
    }

    /**
     * @return string
     */
    public function getCodcusto()
    {
        return $this->codcusto;
    }

    /**
     * @param string $codcxa
     */
    public function setCodcxa($codcxa)
    {
        $this->codcxa = $codcxa;
    }

    /**
     * @return string
     */
    public function getCodcxa()
    {
        return $this->codcxa;
    }

    /**
     * @param string $codetd_fcfo
     */
    public function setCodetdFcfo($codetd_fcfo)
    {
        $this->codetd_fcfo = $codetd_fcfo;
    }

    /**
     * @return string
     */
    public function getCodetdFcfo()
    {
        return $this->codetd_fcfo;
    }

    /**
     * @param string $codigoagencia
     */
    public function setCodigoagencia($codigoagencia)
    {
        $this->codigoagencia = $codigoagencia;
    }

    /**
     * @return string
     */
    public function getCodigoagencia()
    {
        return $this->codigoagencia;
    }

    /**
     * @param string $codigobanco
     */
    public function setCodigobanco($codigobanco)
    {
        $this->codigobanco = $codigobanco;
    }

    /**
     * @return string
     */
    public function getCodigobanco()
    {
        return $this->codigobanco;
    }

    /**
     * @param string $codmunicipio_fcfo
     */
    public function setCodmunicipioFcfo($codmunicipio_fcfo)
    {
        $this->codmunicipio_fcfo = $codmunicipio_fcfo;
    }

    /**
     * @return string
     */
    public function getCodmunicipioFcfo()
    {
        return $this->codmunicipio_fcfo;
    }

    /**
     * @param string $codtb1flx
     */
    public function setCodtb1flx($codtb1flx)
    {
        $this->codtb1flx = $codtb1flx;
    }

    /**
     * @return string
     */
    public function getCodtb1flx()
    {
        return $this->codtb1flx;
    }

    /**
     * @param string $codtb2flx
     */
    public function setCodtb2flx($codtb2flx)
    {
        $this->codtb2flx = $codtb2flx;
    }

    /**
     * @return string
     */
    public function getCodtb2flx()
    {
        return $this->codtb2flx;
    }

    /**
     * @param string $codtdo
     */
    public function setCodtdo($codtdo)
    {
        $this->codtdo = $codtdo;
    }

    /**
     * @return string
     */
    public function getCodtdo()
    {
        return $this->codtdo;
    }

    /**
     * @param \G2\Entity\smallint $compensado
     */
    public function setCompensado($compensado)
    {
        $this->compensado = $compensado;
    }

    /**
     * @return \G2\Entity\smallint
     */
    public function getCompensado()
    {
        return $this->compensado;
    }

    /**
     * @param string $complemento_fcfo
     */
    public function setComplementoFcfo($complemento_fcfo)
    {
        $this->complemento_fcfo = $complemento_fcfo;
    }

    /**
     * @return string
     */
    public function getComplementoFcfo()
    {
        return $this->complemento_fcfo;
    }

    /**
     * @param string $contacorrente
     */
    public function setContacorrente($contacorrente)
    {
        $this->contacorrente = $contacorrente;
    }

    /**
     * @return string
     */
    public function getContacorrente()
    {
        return $this->contacorrente;
    }

    /**
     * @param string $contato_fcfo
     */
    public function setContatoFcfo($contato_fcfo)
    {
        $this->contato_fcfo = $contato_fcfo;
    }

    /**
     * @return string
     */
    public function getContatoFcfo()
    {
        return $this->contato_fcfo;
    }

    /**
     * @param \G2\Entity\datetime $databaixa
     */
    public function setDatabaixa($databaixa)
    {
        $this->databaixa = $databaixa;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDatabaixa()
    {
        return $this->databaixa;
    }

    /**
     * @param string $dataemissao
     */
    public function setDataemissao($dataemissao)
    {
        $this->dataemissao = $dataemissao;
    }

    /**
     * @return string
     */
    public function getDataemissao()
    {
        return $this->dataemissao;
    }

    /**
     * @param string $dataop2
     */
    public function setDataop2($dataop2)
    {
        $this->dataop2 = $dataop2;
    }

    /**
     * @return string
     */
    public function getDataop2()
    {
        return $this->dataop2;
    }

    /**
     * @param string $dataprevbaixa
     */
    public function setDataprevbaixa($dataprevbaixa)
    {
        $this->dataprevbaixa = $dataprevbaixa;
    }

    /**
     * @return string
     */
    public function getDataprevbaixa()
    {
        return $this->dataprevbaixa;
    }

    /**
     * @param string $datavencimento
     */
    public function setDatavencimento($datavencimento)
    {
        $this->datavencimento = $datavencimento;
    }

    /**
     * @return string
     */
    public function getDatavencimento()
    {
        return $this->datavencimento;
    }

    /**
     * @param string $datavencimento_cheque
     */
    public function setDatavencimentoCheque($datavencimento_cheque)
    {
        $this->datavencimento_cheque = $datavencimento_cheque;
    }

    /**
     * @return string
     */
    public function getDatavencimentoCheque()
    {
        return $this->datavencimento_cheque;
    }

    /**
     * @param string $email_fcfo
     */
    public function setEmailFcfo($email_fcfo)
    {
        $this->email_fcfo = $email_fcfo;
    }

    /**
     * @return string
     */
    public function getEmailFcfo()
    {
        return $this->email_fcfo;
    }

    /**
     * @param string $fax_fcfo
     */
    public function setFaxFcfo($fax_fcfo)
    {
        $this->fax_fcfo = $fax_fcfo;
    }

    /**
     * @return string
     */
    public function getFaxFcfo()
    {
        return $this->fax_fcfo;
    }

    /**
     * @param string $historico
     */
    public function setHistorico($historico)
    {
        $this->historico = $historico;
    }

    /**
     * @return string
     */
    public function getHistorico()
    {
        return $this->historico;
    }

    /**
     * @param int $id_contrato
     */
    public function setIdContrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @return int
     */
    public function getIdContrato()
    {
        return $this->id_contrato;
    }

    /**
     * @param int $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_lancamento
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return int
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param string $idcfo_fcfo
     */
    public function setIdcfoFcfo($idcfo_fcfo)
    {
        $this->idcfo_fcfo = $idcfo_fcfo;
    }

    /**
     * @return string
     */
    public function getIdcfoFcfo()
    {
        return $this->idcfo_fcfo;
    }

    /**
     * @param string $idlan_fcfo
     */
    public function setIdlanFcfo($idlan_fcfo)
    {
        $this->idlan_fcfo = $idlan_fcfo;
    }

    /**
     * @return string
     */
    public function getIdlanFcfo()
    {
        return $this->idlan_fcfo;
    }

    /**
     * @param string $inscrestadual_fcfo
     */
    public function setInscrestadualFcfo($inscrestadual_fcfo)
    {
        $this->inscrestadual_fcfo = $inscrestadual_fcfo;
    }

    /**
     * @return string
     */
    public function getInscrestadualFcfo()
    {
        return $this->inscrestadual_fcfo;
    }

    /**
     * @param string $jurosdia
     */
    public function setJurosdia($jurosdia)
    {
        $this->jurosdia = $jurosdia;
    }

    /**
     * @return string
     */
    public function getJurosdia()
    {
        return $this->jurosdia;
    }

    /**
     * @param string $jurosvendor
     */
    public function setJurosvendor($jurosvendor)
    {
        $this->jurosvendor = $jurosvendor;
    }

    /**
     * @return string
     */
    public function getJurosvendor()
    {
        return $this->jurosvendor;
    }

    /**
     * @param string $nacionalidade_fcfo
     */
    public function setNacionalidadeFcfo($nacionalidade_fcfo)
    {
        $this->nacionalidade_fcfo = $nacionalidade_fcfo;
    }

    /**
     * @return string
     */
    public function getNacionalidadeFcfo()
    {
        return $this->nacionalidade_fcfo;
    }

    /**
     * @param string $nome_fcfo
     */
    public function setNomeFcfo($nome_fcfo)
    {
        $this->nome_fcfo = $nome_fcfo;
    }

    /**
     * @return string
     */
    public function getNomeFcfo()
    {
        return $this->nome_fcfo;
    }

    /**
     * @param string $nomefantasia_fcfo
     */
    public function setNomefantasiaFcfo($nomefantasia_fcfo)
    {
        $this->nomefantasia_fcfo = $nomefantasia_fcfo;
    }

    /**
     * @return string
     */
    public function getNomefantasiaFcfo()
    {
        return $this->nomefantasia_fcfo;
    }

    /**
     * @param int $nu_ordem
     */
    public function setNuOrdem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    /**
     * @return int
     */
    public function getNuOrdem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param string $numero_fcfo
     */
    public function setNumeroFcfo($numero_fcfo)
    {
        $this->numero_fcfo = $numero_fcfo;
    }

    /**
     * @return string
     */
    public function getNumeroFcfo()
    {
        return $this->numero_fcfo;
    }

    /**
     * @param string $numerocheque
     */
    public function setNumerocheque($numerocheque)
    {
        $this->numerocheque = $numerocheque;
    }

    /**
     * @return string
     */
    public function getNumerocheque()
    {
        return $this->numerocheque;
    }

    /**
     * @param string $numerodocumento
     */
    public function setNumerodocumento($numerodocumento)
    {
        $this->numerodocumento = $numerodocumento;
    }

    /**
     * @return string
     */
    public function getNumerodocumento()
    {
        return $this->numerodocumento;
    }

    /**
     * @param string $pagrec
     */
    public function setPagrec($pagrec)
    {
        $this->pagrec = $pagrec;
    }

    /**
     * @return string
     */
    public function getPagrec()
    {
        return $this->pagrec;
    }

    /**
     * @param string $pagrec_fcfo
     */
    public function setPagrecFcfo($pagrec_fcfo)
    {
        $this->pagrec_fcfo = $pagrec_fcfo;
    }

    /**
     * @return string
     */
    public function getPagrecFcfo()
    {
        return $this->pagrec_fcfo;
    }

    /**
     * @param string $pais_fcfo
     */
    public function setPaisFcfo($pais_fcfo)
    {
        $this->pais_fcfo = $pais_fcfo;
    }

    /**
     * @return string
     */
    public function getPaisFcfo()
    {
        return $this->pais_fcfo;
    }

    /**
     * @param string $pessoafisoujur_fcfo
     */
    public function setPessoafisoujurFcfo($pessoafisoujur_fcfo)
    {
        $this->pessoafisoujur_fcfo = $pessoafisoujur_fcfo;
    }

    /**
     * @return string
     */
    public function getPessoafisoujurFcfo()
    {
        return $this->pessoafisoujur_fcfo;
    }

    /**
     * @param string $rua_fcfo
     */
    public function setRuaFcfo($rua_fcfo)
    {
        $this->rua_fcfo = $rua_fcfo;
    }

    /**
     * @return string
     */
    public function getRuaFcfo()
    {
        return $this->rua_fcfo;
    }

    /**
     * @param string $st_caminho
     */
    public function setStCaminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
    }

    /**
     * @return string
     */
    public function getStCaminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param string $st_linkedserver
     */
    public function setStLinkedserver($st_linkedserver)
    {
        $this->st_linkedserver = $st_linkedserver;
    }

    /**
     * @return string
     */
    public function getStLinkedserver()
    {
        return $this->st_linkedserver;
    }

    /**
     * @param string $statuslan
     */
    public function setStatuslan($statuslan)
    {
        $this->statuslan = $statuslan;
    }

    /**
     * @return string
     */
    public function getStatuslan()
    {
        return $this->statuslan;
    }

    /**
     * @param string $taxasvendor
     */
    public function setTaxasvendor($taxasvendor)
    {
        $this->taxasvendor = $taxasvendor;
    }

    /**
     * @return string
     */
    public function getTaxasvendor()
    {
        return $this->taxasvendor;
    }

    /**
     * @param string $telefone_fcfo
     */
    public function setTelefoneFcfo($telefone_fcfo)
    {
        $this->telefone_fcfo = $telefone_fcfo;
    }

    /**
     * @return string
     */
    public function getTelefoneFcfo()
    {
        return $this->telefone_fcfo;
    }

    /**
     * @param string $tipocontribuinteinss_fcfo
     */
    public function setTipocontribuinteinssFcfo($tipocontribuinteinss_fcfo)
    {
        $this->tipocontribuinteinss_fcfo = $tipocontribuinteinss_fcfo;
    }

    /**
     * @return string
     */
    public function getTipocontribuinteinssFcfo()
    {
        return $this->tipocontribuinteinss_fcfo;
    }

    /**
     * @param string $valorbaixado
     */
    public function setValorbaixado($valorbaixado)
    {
        $this->valorbaixado = $valorbaixado;
    }

    /**
     * @return string
     */
    public function getValorbaixado()
    {
        return $this->valorbaixado;
    }

    /**
     * @param string $valorcap
     */
    public function setValorcap($valorcap)
    {
        $this->valorcap = $valorcap;
    }

    /**
     * @return string
     */
    public function getValorcap()
    {
        return $this->valorcap;
    }

    /**
     * @param string $valordesconto
     */
    public function setValordesconto($valordesconto)
    {
        $this->valordesconto = $valordesconto;
    }

    /**
     * @return string
     */
    public function getValordesconto()
    {
        return $this->valordesconto;
    }

    /**
     * @param string $valorjuros
     */
    public function setValorjuros($valorjuros)
    {
        $this->valorjuros = $valorjuros;
    }

    /**
     * @return string
     */
    public function getValorjuros()
    {
        return $this->valorjuros;
    }

    /**
     * @param string $valormulta
     */
    public function setValormulta($valormulta)
    {
        $this->valormulta = $valormulta;
    }

    /**
     * @return string
     */
    public function getValormulta()
    {
        return $this->valormulta;
    }

    /**
     * @param string $valorop1
     */
    public function setValorop1($valorop1)
    {
        $this->valorop1 = $valorop1;
    }

    /**
     * @return string
     */
    public function getValorop1()
    {
        return $this->valorop1;
    }

    /**
     * @param string $valorop2
     */
    public function setValorop2($valorop2)
    {
        $this->valorop2 = $valorop2;
    }

    /**
     * @return string
     */
    public function getValorop2()
    {
        return $this->valorop2;
    }

    /**
     * @param string $valorop3
     */
    public function setValorop3($valorop3)
    {
        $this->valorop3 = $valorop3;
    }

    /**
     * @return string
     */
    public function getValorop3()
    {
        return $this->valorop3;
    }

    /**
     * @param string $valororiginal
     */
    public function setValororiginal($valororiginal)
    {
        $this->valororiginal = $valororiginal;
    }

    /**
     * @return string
     */
    public function getValororiginal()
    {
        return $this->valororiginal;
    }

    /**
     * @param string $idpais_fcfo
     */
    public function setIdpaisFcfo($idpais_fcfo)
    {
        $this->idpais_fcfo = $idpais_fcfo;
    }

    /**
     * @return string
     */
    public function getIdpaisFcfo()
    {
        return $this->idpais_fcfo;
    }

    /**
     * @param string $tipo_rua
     */
    public function setTipoRua($tipo_rua)
    {
        $this->tiporua = $tipo_rua;
    }

    /**
     * @return string
     */
    public function getTipoRua()
    {
        return $this->tiporua;
    }

    /**
     * @param string $tipobairro_fcfo
     */
    public function setTipobairroFcfo($tipobairro_fcfo)
    {
        $this->tipobairro_fcfo = $tipobairro_fcfo;
    }

    /**
     * @return string
     */
    public function getTipobairroFcfo()
    {
        return $this->tipobairro_fcfo;
    }

    /**
     * @return string
     */
    public function getnumautocartao()
    {
        return $this->numautocartao;
    }

    /**
     * @param string $numautocartao
     */
    public function setnumautocartao($numautocartao)
    {
        $this->numautocartao = $numautocartao;
        return $this;

    }

    /**
     * @return string
     */
    public function getnumcartaocredito()
    {
        return $this->numcartaocredito;
    }

    /**
     * @param string $numcartaocredito
     */
    public function setnumcartaocredito($numcartaocredito)
    {
        $this->numcartaocredito = $numcartaocredito;
        return $this;
    }

    /**
     * @return string
     */
    public function getStCoddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @param string $st_coddocumento
     */
    public function setStCoddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
    }

    /**
     * @return string
     */
    public function getIdacordo()
    {
        return $this->idacordo;
    }

    /**
     * @param string $idacordo
     */
    public function setIdacordo($idacordo)
    {
        $this->idacordo = $idacordo;
    }


}

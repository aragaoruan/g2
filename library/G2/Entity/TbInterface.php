<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_interface")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-14
 * OBS: A Classe Se Chama TbInterface, porque Interface é uma palavra reservada do PHP
 */
class TbInterface {

    /**
     *
     * @var integer $id_interface
     * @Column(name="id_interface", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_interface;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_interface")
     * @var string
     */
    private $st_interface;

    /**
     * @return int
     */
    public function getId_interface()
    {
        return $this->id_interface;
    }

    /**
     * @param int $id_interface
     */
    public function setId_interface($id_interface)
    {
        $this->id_interface = $id_interface;
    }

    /**
     * @return string
     */
    public function getSt_interface()
    {
        return $this->st_interface;
    }

    /**
     * @param string $st_interface
     */
    public function setSt_interface($st_interface)
    {
        $this->st_interface = $st_interface;
    }





}

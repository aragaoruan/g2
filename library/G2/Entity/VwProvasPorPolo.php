<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_provasporpolo")
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwProvasPorPolo {

    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", type="date", nullable=true, length=3)
     */
    private $dt_aplicacao;

    /**
     * @var \DateTime $hr_inicio
     * @Column(name="hr_inicio", type="datetime2", nullable=false, length=5)
     */
    private $hr_inicio;

    /**
     * @var \DateTime $hr_fim
     * @Column(name="hr_fim", type="datetime2", nullable=false, length=5)
     */
    private $hr_fim;

    /**
     * @Id
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=false, length=4)
     */
    private $id_aplicadorprova;

    /**
     * @Id
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_maxaplicacao
     * @Column(name="nu_maxaplicacao", type="integer", nullable=true, length=4)
     */
    private $nu_maxaplicacao;

    /**
     * @var integer $nu_totalagendamentos
     * @Column(name="nu_totalagendamentos", type="integer", nullable=true, length=4)
     */
    private $nu_totalagendamentos;

    /**
     * @var integer $nu_agendamentoprovafinal
     * @Column(name="nu_agendamentoprovafinal", type="integer", nullable=true, length=4)
     */
    private $nu_agendamentoprovafinal;

    /**
     * @var integer $nu_agendamentorecuperacao
     * @Column(name="nu_agendamentorecuperacao", type="integer", nullable=true, length=4)
     */
    private $nu_agendamentorecuperacao;

    /**
     * @var integer $nu_vagasrestantes
     * @Column(name="nu_vagasrestantes", type="integer", nullable=true, length=4)
     */
    private $nu_vagasrestantes;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=false, length=200)
     */
    private $st_aplicadorprova;

    /**
     * @return date dt_aplicacao
     */
    public function getDt_aplicacao() {
        return $this->dt_aplicacao;
    }

    /**
     * @return DateTime2 hr_inicio
     */
    public function getHr_inicio() {
        return $this->hr_inicio;
    }

    /**
     * @return DateTime2 hr_fim
     */
    public function getHr_fim() {
        return $this->hr_fim;
    }

    /**
     * @return integer id_aplicadorprova
     */
    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    /**
     * @return integer id_avaliacaoaplicacao
     */
    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return integer nu_maxaplicacao
     */
    public function getNu_maxaplicacao() {
        return $this->nu_maxaplicacao;
    }

    /**
     * @return integer nu_totalagendamentos
     */
    public function getNu_totalagendamentos() {
        return $this->nu_totalagendamentos;
    }

    /**
     * @return integer nu_agendamentoprovafinal
     */
    public function getNu_agendamentoprovafinal() {
        return $this->nu_agendamentoprovafinal;
    }

    /**
     * @return integer nu_agendamentorecuperacao
     */
    public function getNu_agendamentorecuperacao() {
        return $this->nu_agendamentorecuperacao;
    }

    /**
     * @return integer nu_vagasrestantes
     */
    public function getNu_vagasrestantes() {
        return $this->nu_vagasrestantes;
    }

    /**
     * @return string st_aplicadorprova
     */
    public function getSt_aplicadorprova() {
        return $this->st_aplicadorprova;
    }

} 
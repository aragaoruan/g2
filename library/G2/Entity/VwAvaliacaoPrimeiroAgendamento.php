<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoprimeiroagendamento")
 * @Entity
 * @EntityView
 */
class VwAvaliacaoPrimeiroAgendamento
{
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_alocacao;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_avaliacao;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false, length=4)
     */
    private $id_tipoavaliacao;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false, length=100)
     */
    private $st_avaliacao;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_alocacao
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param int $id_entidadeatendimento
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @param int $id_avaliacao
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param int $id_tipoavaliacao
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param int $id_avaliacaoconjuntoreferencia
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setid_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_avaliacao()
    {
        return $this->st_avaliacao;
    }

    /**
     * @param string $st_avaliacao
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setst_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     * @return VwAvaliacaoPrimeiroAgendamento
     */
    public function setst_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }
}
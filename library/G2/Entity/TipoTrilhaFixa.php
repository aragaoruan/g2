<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipotrilhafixa")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */

class TipoTrilhaFixa
{

    /**
     * @var integer $id_tipotrilhafixa
     * @Column(type="integer", nullable=false, name="id_tipotrilhafixa")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipotrilhafixa;
     /**
     * @var string $st_tipotrilhafixa
     * @Column(name="st_tipotrilhafixa",type="string", nullable=false, length=255)
     */
    private $st_tipotrilhafixa;
     /**
     * @var string $st_descricao
     * @Column(name="st_descricao",type="string", nullable=false, length=2500)
     */
    private $st_descricao;
    /**
     * @var $id_tipotrilha
     * @ManyToOne(targetEntity="TipoTrilha")
     * @JoinColumn(name="id_tipotrilha", nullable=false, referencedColumnName="id_tipotrilha")
     */
    private $id_tipotrilha;
    
    public function getId_tipotrilhafixa() {
        return $this->id_tipotrilhafixa;
    }

    public function setId_tipotrilhafixa($id_tipotrilhafixa) {
        $this->id_tipotrilhafixa = $id_tipotrilhafixa;
    }

    public function getSt_tipotrilhafixa() {
        return $this->st_tipotrilhafixa;
    }

    public function setSt_tipotrilhafixa($st_tipotrilhafixa) {
        $this->st_tipotrilhafixa = $st_tipotrilhafixa;
    }

    public function getSt_descricao() {
        return $this->st_descricao;
    }

    public function setSt_descricao($st_descricao) {
        $this->st_descricao = $st_descricao;
    }

    public function getId_tipotrilha() {
        return $this->id_tipotrilha;
    }

    public function setId_tipotrilha($id_tipotrilha) {
        $this->id_tipotrilha = $id_tipotrilha;
    }




}
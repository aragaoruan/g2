<?php

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * Entity of ItemGradeHoraria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Entity(repositoryClass="\G2\Repository\GradeHoraria")
 * @Table(name="tb_itemgradehoraria")
 * @EntityLog
 */
class ItemGradeHoraria
{

    /**
     * @var integer $id_itemgradehoraria
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemgradehoraria;

    /**
     * @var \G2\Entity\GradeHoraria $id_gradehoraria
     * @ManyToOne(targetEntity="GradeHoraria")
     * @JoinColumn(name="id_gradehoraria", referencedColumnName="id_gradehoraria")
     */
    private $id_gradehoraria;

    /**
     * @var \G2\Entity\Entidade $id_unidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_unidade", referencedColumnName="id_entidade")
     */
    private $id_unidade;

    /**
     * @var \G2\Entity\Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var \G2\Entity\Turno $id_turno
     * @ManyToOne(targetEntity="Turno")
     * @JoinColumn(name="id_turno", referencedColumnName="id_turno")
     */
    private $id_turno;

    /**
     * @var \G2\Entity\Turno $id_turnoaula
     * @ManyToOne(targetEntity="Turno")
     * @JoinColumn(name="id_turnoaula", referencedColumnName="id_turno")
     */
    private $id_turnoaula;

    /**
     * @var \G2\Entity\TipoAula $id_tipoaula
     * @ManyToOne(targetEntity="TipoAula")
     * @JoinColumn(name="id_tipoaula", referencedColumnName="id_tipoaula")
     */
    private $id_tipoaula;

    /**
     * @var \G2\Entity\Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var \G2\Entity\ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var \G2\Entity\DiaSemana $id_diasemana
     * @ManyToOne(targetEntity="DiaSemana")
     * @JoinColumn(name="id_diasemana", referencedColumnName="id_diasemana")
     */
    private $id_diasemana;

    /**
     * @var date $dt_diasemana
     * @Column(type="date", nullable=false)
     */
    private $dt_diasemana;

    /**
     * @var \G2\Entity\Usuario $id_professor
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_professor", referencedColumnName="id_usuario")
     */
    private $id_professor;

    /**
     * @var \G2\Entity\Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", referencedColumnName="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var string $st_observacao
     * @Column(type="string", nullable=true)
     */
    private $st_observacao;

    /**
     * @var integer $nu_encontro
     * @Column(type="integer", nullable=true)
     */
    private $nu_encontro;

    /**
     * @return int
     */
    public function getId_itemgradehoraria()
    {
        return $this->id_itemgradehoraria;
    }

    /**
     * @return GradeHoraria
     */
    public function getId_gradehoraria()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @return Entidade
     */
    public function getId_unidade()
    {
        return $this->id_unidade;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @return Turno
     */
    public function getId_turno()
    {
        return $this->id_turno;
    }

    /**
     * @return Turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @return ProjetoPedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return DiaSemana
     */
    public function getId_diasemana()
    {
        return $this->id_diasemana;
    }

    /**
     * @return date
     */
    public function getDt_diasemana()
    {
        return $this->dt_diasemana;
    }

    /**
     * @return Usuario
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @return Disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @return integer
     */
    public function getNu_encontro()
    {
        return $this->nu_encontro;
    }

    /**
     * @param integer $id_itemgradehoraria
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_itemgradehoraria($id_itemgradehoraria)
    {
        $this->id_itemgradehoraria = $id_itemgradehoraria;
        return $this;
    }

    /**
     * @param \G2\Entity\GradeHoraria $id_gradehoraria
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_gradehoraria(\G2\Entity\GradeHoraria $id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

    /**
     * @param \G2\Entity\Entidade $id_unidade
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_unidade(\G2\Entity\Entidade $id_unidade)
    {
        $this->id_unidade = $id_unidade;
        return $this;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidadecadastro
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_entidadecadastro(\G2\Entity\Entidade $id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @param \G2\Entity\Turno $id_turno
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_turno(\G2\Entity\Turno $id_turno)
    {
        $this->id_turno = $id_turno;
        return $this;
    }

    /**
     * @param \G2\Entity\Turma $id_turma
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_turma(\G2\Entity\Turma $id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @param \G2\Entity\ProjetoPedagogico $id_projetopedagogico
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_projetopedagogico(\G2\Entity\ProjetoPedagogico $id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @param \G2\Entity\DiaSemana $id_diasemana
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_diasemana(\G2\Entity\DiaSemana $id_diasemana)
    {
        $this->id_diasemana = $id_diasemana;
        return $this;
    }

    /**
     * @param date $dt_diasemana
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setDt_diasemana($dt_diasemana)
    {
        $this->dt_diasemana = $dt_diasemana;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_professor
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_professor(\G2\Entity\Usuario $id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @param \G2\Entity\Disciplina $id_disciplina
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setId_disciplina(\G2\Entity\Disciplina $id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param string $st_observacao
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
        return $this;
    }

    /**
     *
     * @param integer $nu_encontro
     * @return \G2\Entity\ItemGradeHoraria
     */
    public function setNu_encontro($nu_encontro)
    {
        $this->nu_encontro = $nu_encontro;
        return $this;
    }

    /**
     * @return Turno
     */
    public function getId_turnoaula()
    {
        return $this->id_turnoaula;
    }

    /**
     * @param Turno $id_turnoaula
     */
    public function setId_turnoaula($id_turnoaula)
    {
        $this->id_turnoaula = $id_turnoaula;
    }

    /**
     * @return TipoAula
     */
    public function getId_tipoaula()
    {
        return $this->id_tipoaula;
    }

    /**
     * @param TipoAula $id_tipoaula
     */
    public function setId_tipoaula($id_tipoaula)
    {
        $this->id_tipoaula = $id_tipoaula;
    }




}

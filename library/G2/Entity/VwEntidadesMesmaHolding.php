<?php

namespace G2\Entity;
/**
* @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
* @Table(name="vw_entidadesmesmaholding")
* @Entity
* @EntityView
* @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
 */

class VwEntidadesMesmaHolding
{
    /**
     * @var integer id_entidade
     * @Id
     * @Column(name="id_entidade",type="integer",nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer id_entidadeparceira
     * @Id
     * @Column(name="id_entidadeparceira",type="integer", nullable=false)
     */
    private $id_entidadeparceira;

    /**
     * @var string st_nomeentidadeparceira
     * @Column(name="st_nomeentidadeparceira",type="string",nullable=false)
     */
    private $st_nomeentidadeparceira;

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidadeparceira()
    {
        return $this->id_entidadeparceira;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidadeparceira()
    {
        return $this->st_nomeentidadeparceira;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param int $id_entidadeparceira
     * @return $this
     */
    public function setId_entidadeparceira($id_entidadeparceira)
    {
        $this->id_entidadeparceira = $id_entidadeparceira;
        return $this;
    }

    /**
     * @param string $st_nomeentidadeparceira
     * @return $this
     */
    public function setSt_nomeentidadeparceira($st_nomeentidadeparceira)
    {
        $this->st_nomeentidadeparceira = $st_nomeentidadeparceira;
        return $this;
    }
}

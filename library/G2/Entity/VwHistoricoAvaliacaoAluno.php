<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_historicoavaliacaoaluno")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwHistoricoAvaliacaoAluno {

    /**
     * @var integer $id_avaliacaoaluno
     * @Column(name="id_avaliacaoaluno", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaluno;

    /**
     * @var date $dt_agendamento
     * @Column(name="dt_agendamento", type="date", nullable=true, length=3)
     */
    private $dt_agendamento;

    /**
     * @var date $dt_avaliacao
     * @Column(name="dt_avaliacao", type="date", nullable=true, length=3)
     */
    private $dt_avaliacao;

    /**
     * @var date $dt_defesa
     * @Column(name="dt_defesa", type="date", nullable=true, length=3)
     */
    private $dt_defesa;

    /**
     * @var date $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="date", nullable=true, length=3)
     */
    private $dt_encerramentosala;

    /**
     * @var datetime2 $dt_cadastrosala
     * @Column(name="dt_cadastrosala", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastrosala;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=true, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoagendamento;
    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_avaliacao;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=true, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_tiponota
     * @Column(name="id_tiponota", type="integer", nullable=true, length=4)
     */
    private $id_tiponota;

    /**
     * @var integer $id_avaliacaorecupera
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaorecupera;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaula;

    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoconjunto;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=true, length=4)
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipocalculoavaliacao;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipoavaliacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var float $st_nota
     * @Column(name="st_nota", type="float", nullable=true, length=5)
     * @Id
     */
    private $st_nota;

    /**
     * @var float $nu_notamaxima
     * @Column(name="nu_notamaxima", type="float", nullable=true, length=9)
     */
    private $nu_notamaxima;

    /**
     * @var float $nu_percentualaprovacao
     * @Column(name="nu_percentualaprovacao", type="float", nullable=true, length=5)
     */
    private $nu_percentualaprovacao;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(name="st_tituloexibicaomodulo", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaomodulo;

    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=true, length=100)
     */
    private $st_avaliacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=true, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $nu_notamax
     * @Column(name="nu_notamax", type="string", nullable=true, length=100)
     */
    private $nu_notamax;

    /**
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=true, length=200)
     */
    private $st_tipoavaliacao;

    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="text", nullable=true)
     */
    private $st_codusuario;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_tituloavaliacao
     * @Column(name="st_tituloavaliacao", type="string", nullable=true, length=500)
     */
    private $st_tituloavaliacao;

    /**
     * @var string $st_justificativa
     * @Column(name="st_justificativa", type="string", nullable=true, length=2000)
     */
    private $st_justificativa;

    /**
     * @var integer $bl_agendamento
     * @Column(name="bl_agendamento", type="integer", nullable=true, length=4)
     */
    private $bl_agendamento;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true, length=4)
     */
    private $bl_provaglobal;

    /**
     * @var date $dt_cadastroavaliacao
     * @Column(name="dt_cadastroavaliacao", type="datetime", nullable=true)
     */
    private $dt_cadastroavaliacao;

    /**
     * @var integer $id_usuariocadavaliacao
     * @Column(name="id_usuariocadavaliacao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_usuariocadavaliacao;
    
    /**
     * @var string $st_usuariocadavaliacao
     * @Column(name="st_usuariocadavaliacao", type="string", nullable=true)
     */
    private $st_usuariocadavaliacao;
    /**
     * @var integer $id_evolucaodisciplina
     * @Column(name="id_evolucaodisciplina", type="integer", nullable=true, length=4)
     *      */
    private $id_evolucaodisciplina;
    
    /**
     * @var string $st_evolucaodisciplina
     * @Column(name="st_evolucaodisciplina", type="string", nullable=true)
     */
    private $st_evolucaodisciplina;
 

    public function getDt_agendamento() {
        return $this->dt_agendamento;
    }

    public function getDt_avaliacao() {
        return $this->dt_avaliacao;
    }

    public function getDt_defesa() {
        return $this->dt_defesa;
    }

    public function getDt_encerramentosala() {
        return $this->dt_encerramentosala;
    }

    public function getDt_cadastrosala() {
        return $this->dt_cadastrosala;
    }

    public function getId_avaliacaoaluno() {
        return $this->id_avaliacaoaluno;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function getId_modulo() {
        return $this->id_modulo;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    public function getId_tiponota() {
        return $this->id_tiponota;
    }

    public function getId_avaliacaorecupera() {
        return $this->id_avaliacaorecupera;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_entidadeatendimento() {
        return $this->id_entidadeatendimento;
    }

    public function getId_upload() {
        return $this->id_upload;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_tipoprova() {
        return $this->id_tipoprova;
    }

    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    public function getId_tipocalculoavaliacao() {
        return $this->id_tipocalculoavaliacao;
    }

    public function getId_tipoavaliacao() {
        return $this->id_tipoavaliacao;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getSt_nota() {
        return $this->st_nota;
    }

    public function getNu_notamaxima() {
        return $this->nu_notamaxima;
    }

    public function getNu_percentualaprovacao() {
        return $this->nu_percentualaprovacao;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_tituloexibicaomodulo() {
        return $this->st_tituloexibicaomodulo;
    }

    public function getSt_tituloexibicaodisciplina() {
        return $this->st_tituloexibicaodisciplina;
    }

    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    public function getNu_notamax() {
        return $this->nu_notamax;
    }

    public function getSt_tipoavaliacao() {
        return $this->st_tipoavaliacao;
    }

    public function getSt_codusuario() {
        return $this->st_codusuario;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_tituloavaliacao() {
        return $this->st_tituloavaliacao;
    }

    public function getSt_justificativa() {
        return $this->st_justificativa;
    }

    public function setDt_agendamento($dt_agendamento) {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    public function setDt_avaliacao(date $dt_avaliacao) {
        $this->dt_avaliacao = $dt_avaliacao;
        return $this;
    }

    public function setDt_defesa(date $dt_defesa) {
        $this->dt_defesa = $dt_defesa;
        return $this;
    }

    public function setDt_encerramentosala($dt_encerramentosala) {
        $this->dt_encerramentosala = $dt_encerramentosala;
        return $this;
    }

    public function setDt_cadastrosala(datetime2 $dt_cadastrosala) {
        $this->dt_cadastrosala = $dt_cadastrosala;
        return $this;
    }

    public function setId_avaliacaoaluno($id_avaliacaoaluno) {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
        return $this;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_modulo($id_modulo) {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    public function setId_tiponota($id_tiponota) {
        $this->id_tiponota = $id_tiponota;
        return $this;
    }

    public function setId_avaliacaorecupera($id_avaliacaorecupera) {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
        return $this;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function setId_entidadeatendimento($id_entidadeatendimento) {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_tipoprova($id_tipoprova) {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
        return $this;
    }

    public function setId_tipoavaliacao($id_tipoavaliacao) {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setSt_nota($st_nota) {
        $this->st_nota = $st_nota;
        return $this;
    }

    public function setNu_notamaxima($nu_notamaxima) {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    public function setNu_percentualaprovacao($nu_percentualaprovacao) {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo) {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;
    }

    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    public function setNu_notamax($nu_notamax) {
        $this->nu_notamax = $nu_notamax;
        return $this;
    }

    public function setSt_tipoavaliacao($st_tipoavaliacao) {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
        return $this;
    }

    public function setSt_codusuario($st_codusuario) {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function setSt_tituloavaliacao($st_tituloavaliacao) {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
        return $this;
    }

    public function setSt_justificativa($st_justificativa) {
        $this->st_justificativa = $st_justificativa;
        return $this;
    }

    public function getBl_agendamento() {
        return $this->bl_agendamento;
    }

    public function getBl_provaglobal() {
        return $this->bl_provaglobal;
    }

    public function setBl_agendamento($bl_agendamento) {
        $this->bl_agendamento = $bl_agendamento;
    }

    public function setBl_provaglobal($bl_provaglobal) {
        $this->bl_provaglobal = $bl_provaglobal;
    }
    public function getDt_cadastroavaliacao() {
        return $this->dt_cadastroavaliacao;
    }

    public function getId_usuariocadavaliacao() {
        return $this->id_usuariocadavaliacao;
    }

    public function setDt_cadastroavaliacao(datetime2 $dt_cadastroavaliacao) {
        $this->dt_cadastroavaliacao = $dt_cadastroavaliacao;
    }

    public function setId_usuariocadavaliacao($id_usuariocadavaliacao) {
        $this->id_usuariocadavaliacao = $id_usuariocadavaliacao;
    }

    public function getSt_usuariocadavaliacao() {
        return $this->st_usuariocadavaliacao;
    }

    public function setSt_usuariocadavaliacao($st_usuariocadavaliacao) {
        $this->st_usuariocadavaliacao = $st_usuariocadavaliacao;
    }

    public function getId_evolucaodisciplina() {
        return $this->id_evolucaodisciplina;
    }

    public function getSt_evolucaodisciplina() {
        return $this->st_evolucaodisciplina;
    }

    public function setId_evolucaodisciplina($id_evolucaodisciplina) {
        $this->id_evolucaodisciplina = $id_evolucaodisciplina;
    }

    public function setSt_evolucaodisciplina($st_evolucaodisciplina) {
        $this->st_evolucaodisciplina = $st_evolucaodisciplina;
    }


}

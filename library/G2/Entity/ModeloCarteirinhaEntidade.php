<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_modelocarteirinhaentidade")
 * @Entity
 * @author Elcio Mauro Guimarães <elcio.guimaraes@unyleya.com.br>
 */
use \G2\G2Entity;

class ModeloCarteirinhaEntidade extends G2Entity
{

    /**
     *
     * @var integer $id_modelocarteirinhaentidade
     * @Column(name="id_modelocarteirinhaentidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_modelocarteirinhaentidade;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var ModeloCarteirinha $id_modelocarteirinha
     * @ManyToOne(targetEntity="ModeloCarteirinha")
     * @JoinColumn(name="id_modelocarteirinha", nullable=false, referencedColumnName="id_modelocarteirinha")
     */
    private $id_modelocarteirinha;

    /**
     * @param \G2\Entity\Entidade $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\ModeloCarteirinha $id_modelocarteirinha
     */
    public function setid_modelocarteirinha($id_modelocarteirinha)
    {
        $this->id_modelocarteirinha = $id_modelocarteirinha;
        return $this;
    }

    /**
     * @return \G2\Entity\ModeloCarteirinha
     */
    public function getid_modelocarteirinha()
    {
        return $this->id_modelocarteirinha;
    }

    /**
     * @param int $id_modelocarteirinhaentidade
     */
    public function setid_modelocarteirinhaentidade($id_modelocarteirinhaentidade)
    {
        $this->id_modelocarteirinhaentidade = $id_modelocarteirinhaentidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_modelocarteirinhaentidade()
    {
        return $this->id_modelocarteirinhaentidade;
    }



}

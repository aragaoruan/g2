<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_motivoentidade")
 * @Entity
 * @Entity(repositoryClass="\G2\Repository\MotivoEntidade")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class MotivoEntidade {

    /**
     * @Id
     * @var integer $id_motivoentidade
     * @Column(name="id_motivoentidade", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_motivoentidade;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_motivo
     * @ManyToOne(targetEntity="Motivo")
     * @JoinColumn(name="id_motivo", referencedColumnName="id_motivo")
     */
    private $id_motivo;
    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_motivoentidade
     */
    public function getId_motivoentidade() {
        return $this->id_motivoentidade;
    }

    /**
     * @param id_motivoentidade
     */
    public function setId_motivoentidade($id_motivoentidade) {
        $this->id_motivoentidade = $id_motivoentidade;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_motivo
     */
    public function getId_motivo() {
        return $this->id_motivo;
    }

    /**
     * @param id_motivo
     */
    public function setId_motivo($id_motivo) {
        $this->id_motivo = $id_motivo;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
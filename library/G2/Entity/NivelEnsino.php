<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_nivelensino")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class NivelEnsino
{

    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_nivelensino;

    /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=false, length=255)
     */
    private $st_nivelensino;

    /**
     * @ManyToMany(targetEntity="Serie", mappedBy="nivelEnsino")
     */
    private $serie;

    public function __construct ()
    {
        $this->serie = new ArrayCollection();
    }

    /**
     * Array Collection
     * @return \G2\Entity\Serie
     */
    public function getSerie ()
    {
        return $this->serie;
    }

    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    public function getSt_nivelensino ()
    {
        return $this->st_nivelensino;
    }

    public function setSt_nivelensino ($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

}
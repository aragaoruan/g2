<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_modulodisciplinaprojetotrilha")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwModuloDisciplinaProjetoTrilha {

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=true, length=4)
     */
    private $nu_creditos;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;

    /**
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=false, length=4)
     */
    private $id_serie;

    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=false, length=4)
     */
    private $id_nivelensino;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=true, length=255)
     */
    private $st_modulo;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $nu_codigoparceiro
     * @Column(name="nu_codigoparceiro", type="string", nullable=true, length=255)
     */
    private $nu_codigoparceiro;

    public function getId_modulo() {
        return $this->id_modulo;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    public function getNu_creditos() {
        return $this->nu_creditos;
    }

    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_trilha() {
        return $this->id_trilha;
    }

    public function getId_serie() {
        return $this->id_serie;
    }

    public function getId_nivelensino() {
        return $this->id_nivelensino;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_modulo() {
        return $this->st_modulo;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getNu_codigoparceiro() {
        return $this->nu_codigoparceiro;
    }

    public function setId_modulo($id_modulo) {
        $this->id_modulo = $id_modulo;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
    }

    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    public function setNu_creditos($nu_creditos) {
        $this->nu_creditos = $nu_creditos;
    }

    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_trilha($id_trilha) {
        $this->id_trilha = $id_trilha;
    }

    public function setId_serie($id_serie) {
        $this->id_serie = $id_serie;
    }

    public function setId_nivelensino($id_nivelensino) {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_modulo($st_modulo) {
        $this->st_modulo = $st_modulo;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
    }

    public function setNu_codigoparceiro($nu_codigoparceiro) {
        $this->nu_codigoparceiro = $nu_codigoparceiro;
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24/04/2015
 * Time: 17:28
 */

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tramiteentregamaterial")
 * @Entity
 * @author Helder Silva <Helder.silva@unyleya.com.br>
 */
class TramiteEntregaMaterial {
    /**
     * @Id
     * @var integer $id_tramiteentregamaterial
     * @Column(name="id_tramiteentregamaterial", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramiteentregamaterial;
    /**
     * @var integer $id_tramite
     * @ManyToOne(targetEntity="Tramite")
     * @JoinColumn(name="id_tramite", nullable=false, referencedColumnName="id_tramite")
     */
    private $id_tramite;
    /**
     * @var integer $id_entregamaterial
     * @ManyToOne(targetEntity="EntregaMaterial")
     * @JoinColumn(name="id_entregamaterial", nullable=false, referencedColumnName="id_entregamaterial")
     */
    private $id_entregamaterial;

    /**
     * @return integer id_tramiteentregamaterial
     */
    public function getId_tramiteentregamaterial() {
        return $this->id_tramiteentregamaterial;
    }

    /**
     * @param id_tramiteentregamaterial
     */
    public function setId_tramiteentregamaterial($id_tramiteentregamaterial) {
        $this->id_tramiteentregamaterial = $id_tramiteentregamaterial;
        return $this;
    }

    /**
     * @return integer id_tramite
     */
    public function getId_tramite() {
        return $this->id_tramite;
    }

    /**
     * @param id_tramite
     */
    public function setId_tramite($id_tramite) {
        $this->id_tramite = $id_tramite;
        return $this;
    }

    /**
     * @return integer id_entregamaterial
     */
    public function getId_entregamaterial() {
        return $this->id_entregamaterial;
    }

    /**
     * @param id_entregamaterial
     */
    public function setId_entregamaterial($id_entregamaterial) {
        $this->id_entregamaterial = $id_entregamaterial;
        return $this;
    }

}
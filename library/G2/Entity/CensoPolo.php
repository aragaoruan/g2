<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_censopolo")
 * @Entity
 */
class CensoPolo
{

    /**
     * @var int $id_censopolo
     * @Column(name="id_censopolo", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_censopolo;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_ativo;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var CensoInstituicao $id_censoinstituicao
     * @ManyToOne(targetEntity="CensoInstituicao")
     * @JoinColumn(name="id_censoinstituicao", referencedColumnName="id_censoinstituicao")
     */
    private $id_censoinstituicao;

    /**
     * @var integer $nu_codigopolo
     * @Column(name="nu_codigopolo", type="integer", nullable=false)
     */
    private $nu_codigopolo;

    /**
     * @return int
     */
    public function getId_censopolo()
    {
        return $this->id_censopolo;
    }

    /**
     * @param int $id_censopolo
     * @return $this
     */
    public function setId_censopolo($id_censopolo)
    {
        $this->id_censopolo = $id_censopolo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return CensoInstituicao
     */
    public function getId_censoinstituicao()
    {
        return $this->id_censoinstituicao;
    }

    /**
     * @param CensoInstituicao $id_censoinstituicao
     * @return $this
     */
    public function setId_censoinstituicao($id_censoinstituicao)
    {
        $this->id_censoinstituicao = $id_censoinstituicao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_codigopolo()
    {
        return $this->nu_codigopolo;
    }

    /**
     * @param int $nu_codigopolo
     * @return $this
     */
    public function setNu_codigopolo($nu_codigopolo)
    {
        $this->nu_codigopolo = $nu_codigopolo;
        return $this;
    }

}

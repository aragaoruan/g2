<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculacancelamento")
 * @Entity
 * @EntityView
 * @author Ruan Aragao <ruan.aragao@unyleya.com.br>
 */

class VwMatriculaCancelamento extends G2Entity
{

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     */
    private $id_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     * @Id
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string")
     */
    private $st_tituloexibicao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var \DateTime $dt_aceitecontrato
     * @Column(name="dt_aceitecontrato", type="datetime2")
     */
    private $dt_aceitecontrato;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer")
     * @Id
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string")
     */
    private $st_disciplina;

    /**
     * @var \DateTime $dt_abertura
     * @Column(name="dt_abertura", type="datetime2")
     */
    private $dt_abertura;

    /**
     * @var \DateTime $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2")
     */
    private $dt_encerramento;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer")
     */
    private $nu_cargahoraria;

    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean")
     */
    private $bl_cancelamento;

    /**
     * @var float $nu_valortotalcurso
     * @Column(name="nu_valortotalcurso", type="float")
     */
    private $nu_valortotalcurso;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var \DateTime $dt_cancelamento
     * @Column(name="dt_cancelamento", type="datetime2")
     */
    private $dt_cancelamento;

    /**
     * @var \DateTime $dt_matricula
     * @Column(name="dt_matricula", type="datetime2")
     */
    private $dt_matricula;

    /**
     * @var float $nu_cargahorariaprojeto
     * @Column(name="nu_cargahorariaprojeto", type="integer")
     */
    private $nu_cargahorariaprojeto;

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param string $st_tituloexibicao
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return \DateTime
     */
    public function getDt_aceitecontrato()
    {
        return $this->dt_aceitecontrato;
    }

    /**
     * @param \DateTime $dt_aceitecontrato
     */
    public function setDt_aceitecontrato($dt_aceitecontrato)
    {
        $this->dt_aceitecontrato = $dt_aceitecontrato;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return \DateTime
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param \DateTime $dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    /**
     * @return \DateTime
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param \DateTime $dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwMatriculaCancelamento
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     * @return VwMatriculaCancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwMatriculaCancelamento
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_valortotalcurso()
    {
        return $this->nu_valortotalcurso;
    }

    /**
     * @param float $nu_valortotalcurso
     * @return VwMatriculaCancelamento
     */
    public function setnu_valortotalcurso($nu_valortotalcurso)
    {
        $this->nu_valortotalcurso = $nu_valortotalcurso;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_cargahorariaprojeto()
    {
        return $this->nu_cargahorariaprojeto;
    }

    /**
     * @param float $nu_cargahorariaprojeto
     * @return VwMatriculaCancelamento
     */
    public function setnu_cargahorariaprojeto($nu_cargahorariaprojeto)
    {
        $this->nu_cargahorariaprojeto = $nu_cargahorariaprojeto;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cancelamento()
    {
        return $this->dt_cancelamento;
    }

    /**
     * @param \DateTime $dt_cancelamento
     */
    public function setDt_cancelamento($dt_cancelamento)
    {
        $this->dt_cancelamento = $dt_cancelamento;
    }

    /**
     * @return \DateTime
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param \DateTime $dt_matricula
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
    }


}

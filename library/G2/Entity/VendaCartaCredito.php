<?php

namespace G2\Entity;

/**
 * Class Entity para Carta de Credito
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_vendacartacredito")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-13
 */
class VendaCartaCredito
{

    /**
     * @var integer $id_vendacartacredito
     * @Column(name="id_vendacartacredito", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_vendacartacredito;

    /**
     * @var CartaCredito $id_cartacredito
     * @ManyToOne(targetEntity="CartaCredito")
     * @JoinColumn(name="id_cartacredito", referencedColumnName="id_cartacredito")
     */
    private $id_cartacredito;

    /**
     * @var Venda $id_cartacredito
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     */
    private $id_venda;

    /**
     * @var decimal $nu_valorutilizado
     * @Column(name="nu_valorutilizado", type="decimal", nullable=false, length=9)
     */
    private $nu_valorutilizado;

    /**
     * @return integer
     */
    public function getId_vendacartacredito()
    {
        return $this->id_vendacartacredito;
    }

    /**
     * @return CartaCredito
     */
    public function getId_cartacredito()
    {
        return $this->id_cartacredito;
    }

    /**
     * @return Venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return decimal
     */
    public function getNu_valorutilizado()
    {
        return $this->nu_valorutilizado;
    }

    /**
     * @param integer $id_vendacartacredito
     * @return $this
     */
    public function setId_vendacartacredito($id_vendacartacredito)
    {
        $this->id_vendacartacredito = $id_vendacartacredito;
        return $this;
    }

    /**
     * @param CartaCredito $id_cartacredito
     * @return $this
     */
    public function setId_cartacredito(CartaCredito $id_cartacredito)
    {
        $this->id_cartacredito = $id_cartacredito;
        return $this;
    }

    /**
     * @param Venda $id_venda
     * @return $this
     */
    public function setId_venda(Venda $id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @param decimal $nu_valorutilizado
     * @return $this
     */
    public function setNu_valorutilizado($nu_valorutilizado)
    {
        $this->nu_valorutilizado = $nu_valorutilizado;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_emailconfig")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EmailConfig
{

    /**
     *
     * @var integer $id_emailconfig
     * @Column(name="id_emailconfig", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_emailconfig;

    /**
     *
     * @var TipoConexaoEmail $id_tipoconexaoemailsaida
     * @ManyToOne(targetEntity="TipoConexaoEmail")
     * @JoinColumn(name="id_tipoconexaoemailsaida", nullable=true, referencedColumnName="id_tipoconexaoemail")
     */
    private $id_tipoconexaoemailsaida;

    /**
     *
     * @var TipoConexaoEmail $id_tipoconexaoemailentrada
     * @ManyToOne(targetEntity="TipoConexaoEmail")
     * @JoinColumn(name="id_tipoconexaoemailentrada", nullable=true, referencedColumnName="id_tipoconexaoemail")
     */
    private $id_tipoconexaoemailentrada;

    /**
     *
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var string $st_smtp
     * @Column(name="st_smtp", type="string", nullable=true, length=100)
     */
    private $st_smtp;

    /**
     *
     * @var string $st_pop
     * @Column(name="st_pop", type="string", nullable=true, length=100)
     */
    private $st_pop;

    /**
     *
     * @var string $st_imap
     * @Column(name="st_imap", type="string", nullable=true, length=100)
     */
    private $st_imap;

    /**
     *
     * @var boolean $bl_autenticacaosegura
     * @Column(name="bl_autenticacaosegura", type="boolean", nullable=true)
     */
    private $bl_autenticacaosegura;

    /**
     *
     * @var boolean $bt_autenticacaosegura
     * @Column(name="bt_autenticacaosegura", type="boolean", nullable=true)
     */
    private $bt_autenticacaosegura;

    /**
     *
     * @var string $st_conta
     * @Column(name="st_conta", type="string", nullable=true, length=100)
     */
    private $st_conta;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $nu_portaentrada
     * @Column(name="nu_portaentrada", type="integer", nullable=true)
     */
    private $nu_portaentrada;

    /**
     *
     * @var integer $nu_portasaida
     * @Column(name="nu_portasaida", type="integer", nullable=true)
     */
    private $nu_portasaida;

    /**
     *
     * @var string $st_usuario
     * @Column(name="st_usuario", type="string", nullable=true, length=50)
     */
    private $st_usuario;

    /**
     *
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=20)
     */
    private $st_senha;

    /**
     *
     * @var ServidorEmail $id_servidoremailentrada
     * @ManyToOne(targetEntity="ServidorEmail")
     * @JoinColumn(name="id_servidoremailentrada", nullable=true, referencedColumnName="id_servidoremail")
     */
    private $id_servidoremailentrada;

    /**
     *
     * @var string $st_titulo
     * @Column(name="st_titulo", type="string", nullable=true, length=200)
     */
    private $st_titulo;


    /**
     * @var bool $bl_erro
     * @Column(name="bl_erro", type="boolean", nullable=false)
     */
    private $bl_erro;

    public function getId_emailconfig()
    {
        return $this->id_emailconfig;
    }

    public function setId_emailconfig($id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    public function getId_tipoconexaoemailsaida()
    {
        return $this->id_tipoconexaoemailsaida;
    }

    public function setId_tipoconexaoemailsaida(TipoConexaoEmail $id_tipoconexaoemailsaida)
    {
        $this->id_tipoconexaoemailsaida = $id_tipoconexaoemailsaida;
        return $this;
    }

    public function getId_tipoconexaoemailentrada()
    {
        return $this->id_tipoconexaoemailentrada;
    }

    public function setId_tipoconexaoemailentrada(TipoConexaoEmail $id_tipoconexaoemailentrada)
    {
        $this->id_tipoconexaoemailentrada = $id_tipoconexaoemailentrada;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_smtp()
    {
        return $this->st_smtp;
    }

    public function setSt_smtp($st_smtp)
    {
        $this->st_smtp = $st_smtp;
        return $this;
    }

    public function getSt_pop()
    {
        return $this->st_pop;
    }

    public function setSt_pop($st_pop)
    {
        $this->st_pop = $st_pop;
        return $this;
    }

    public function getSt_imap()
    {
        return $this->st_imap;
    }

    public function setSt_imap($st_imap)
    {
        $this->st_imap = $st_imap;
        return $this;
    }

    public function getBl_autenticacaosegura()
    {
        return $this->bl_autenticacaosegura;
    }

    public function setBl_autenticacaosegura($bl_autenticacaosegura)
    {
        $this->bl_autenticacaosegura = $bl_autenticacaosegura;
        return $this;
    }

    public function getBt_autenticacaosegura()
    {
        return $this->bt_autenticacaosegura;
    }

    public function setBt_autenticacaosegura($bt_autenticacaosegura)
    {
        $this->bt_autenticacaosegura = $bt_autenticacaosegura;
        return $this;
    }

    public function getSt_conta()
    {
        return $this->st_conta;
    }

    public function setSt_conta($st_conta)
    {
        $this->st_conta = $st_conta;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getNu_portaentrada()
    {
        return $this->nu_portaentrada;
    }

    public function setNu_portaentrada($nu_portaentrada)
    {
        $this->nu_portaentrada = $nu_portaentrada;
        return $this;
    }

    public function getNu_portasaida()
    {
        return $this->nu_portasaida;
    }

    public function setNu_portasaida($nu_portasaida)
    {
        $this->nu_portasaida = $nu_portasaida;
        return $this;
    }

    public function getSt_usuario()
    {
        return $this->st_usuario;
    }

    public function setSt_usuario($st_usuario)
    {
        $this->st_usuario = $st_usuario;
        return $this;
    }

    public function getSt_senha()
    {
        return $this->st_senha;
    }

    public function setSt_senha($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    public function getId_servidoremailentrada()
    {
        return $this->id_servidoremailentrada;
    }

    public function setId_servidoremailentrada(ServidorEmail $id_servidoremailentrada)
    {
        $this->id_servidoremailentrada = $id_servidoremailentrada;
        return $this;
    }

    public function getSt_titulo()
    {
        return $this->st_titulo;
    }

    public function setSt_titulo($st_titulo)
    {
        $this->st_titulo = $st_titulo;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_Erro()
    {
        return $this->bl_erro;
    }

    /**
     * @param boolean $bl_erro
     * @return EmailConfig
     */
    public function setBl_erro($bl_erro)
    {
        $this->bl_erro = $bl_erro;
        return $this;
    }


}
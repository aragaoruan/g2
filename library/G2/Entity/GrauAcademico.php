<?php

namespace G2\Entity;

/**
 * @Table(name="tb_grauacademico")
 * @Entity
 * @author Stéfan Amaral <stefan.amaral@unyleya.com.br>
 */

use G2\G2Entity;

class GrauAcademico extends G2Entity
{

    /**
     * @var integer $id_grauacademico
     * @Column(name="id_grauacademico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_grauacademico;

    /**
     * @var string $st_grauacademico
     * @Column(name="st_grauacademico", type="string", nullable=false, length=200)
     */
    private $st_grauacademico;

    /**
     * @var string $st_titulacao
     * @Column(name="st_titulacao", type="string", nullable=false, length=200)
     */
    private $st_titulacao;

    /**
     * @return int
     */
    public function getId_grauacademico()
    {
        return $this->id_grauacademico;
    }

    /**
     * @return string
     */
    public function getSt_grauacademico()
    {
        return $this->st_grauacademico;
    }

    /**
     * @param $st_grauacademico
     * @return $this
     */
    public function setSt_grauacademico($st_grauacademico)
    {
        $this->st_grauacademico = $st_grauacademico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_titulacao()
    {
        return $this->st_titulacao;
    }

    /**
     * @param $st_titulacao
     * @return $this
     */
    public function setSt_titulacao($st_titulacao)
    {
        $this->st_titulacao = $st_titulacao;
        return $this;
    }

}

<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_contratoregra")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ContratoRegra extends G2Entity
{

    /**
     *
     * @var integer $id_contratoregra
     * @Column(name="id_contratoregra", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_contratoregra;

    /**
     *
     * @var string $st_contratoregra
     * @Column(name="st_contratoregra", type="string", nullable=false, length=100)
     */
    private $st_contratoregra;

    /**
     *
     * @var integer $nu_contratoduracao
     * @Column(name="nu_contratoduracao", type="integer", nullable=true)
     */
    private $nu_contratoduracao;

    /**
     *
     * @var integer $nu_mesesmulta
     * @Column(name="nu_mesesmulta", type="integer", nullable=true)
     */
    private $nu_mesesmulta;

    /**
     *
     * @var boolean $bl_proporcaomes
     * @Column(name="bl_proporcaomes", type="boolean", nullable=false)
     */
    private $bl_proporcaomes;

    /**
     * @var ProjetoContratoDuracaoTipo $id_projetocontratoduracaotipo
     * @ManyToOne(targetEntity="ProjetoContratoDuracaoTipo")
     * @JoinColumn(name="id_projetocontratoduracaotipo", nullable=true, referencedColumnName="id_projetocontratoduracaotipo")
     */
    private $id_projetocontratoduracaotipo;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var decimal $nu_contratomultavalor
     * @Column(name="nu_contratomultavalor", type="decimal", nullable=true)
     */
    private $nu_contratomultavalor;

    /**
     *
     * @var boolean $bl_renovarcontrato
     * @Column(name="bl_renovarcontrato", type="boolean", nullable=false)
     */
    private $bl_renovarcontrato;

    /**
     *
     * @var integer $nu_mesesmensalidade
     * @Column(name="nu_mesesmensalidade", type="integer", nullable=true)
     */
    private $nu_mesesmensalidade;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $nu_tempoextensao
     * @Column(name="nu_tempoextensao", type="integer", nullable=true)
     */
    private $nu_tempoextensao;

    /**
     *
     * @var integer $id_contratomodelo
     * @Column(name="id_contratomodelo", type="integer", nullable=true)
     */
    private $id_contratomodelo;

    /**
     *
     * @var integer $id_extensaomodelo
     * @Column(name="id_extensaomodelo", type="integer", nullable=true)
     */
    private $id_extensaomodelo;

    /**
     * @var ModeloCarteirinha $id_modelocarteirinha
     * @ManyToOne(targetEntity="ModeloCarteirinha")
     * @JoinColumn(name="id_modelocarteirinha", nullable=true, referencedColumnName="id_modelocarteirinha")
     */
    private $id_modelocarteirinha;


    /**
     * @var TipoRegraContrato $id_tiporegracontrato
     * @ManyToOne(targetEntity="TipoRegraContrato")
     * @JoinColumn(name="id_tiporegracontrato", nullable=true, referencedColumnName="id_tiporegracontrato")
     */
    private $id_tiporegracontrato;

    /**
     *
     * @var TextoSistema $id_aditivotransferencia
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_aditivotransferencia", nullable=true, referencedColumnName="id_textosistema")
     */
    private $id_aditivotransferencia;

    /**
     * @param \G2\Entity\ModeloCarteirinha $id_modelocarteirinha
     * @return $this
     */
    public function setid_modelocarteirinha($id_modelocarteirinha)
    {
        $this->id_modelocarteirinha = $id_modelocarteirinha;
        return $this;
    }

    /**
     * @return \G2\Entity\ModeloCarteirinha
     */
    public function getid_modelocarteirinha()
    {
        return $this->id_modelocarteirinha;
    }

    /**
     * @return int
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param $id_contratoregra
     * @return $this
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_contratoregra()
    {
        return $this->st_contratoregra;
    }

    /**
     * @param $st_contratoregra
     * @return $this
     */
    public function setSt_contratoregra($st_contratoregra)
    {
        $this->st_contratoregra = $st_contratoregra;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_contratoduracao()
    {
        return $this->nu_contratoduracao;
    }

    /**
     * @param $nu_contratoduracao
     * @return $this
     */
    public function setNu_contratoduracao($nu_contratoduracao)
    {
        $this->nu_contratoduracao = $nu_contratoduracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_mesesmulta()
    {
        return $this->nu_mesesmulta;
    }

    /**
     * @param $nu_mesesmulta
     * @return $this
     */
    public function setNu_mesesmulta($nu_mesesmulta)
    {
        $this->nu_mesesmulta = $nu_mesesmulta;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_proporcaomes()
    {
        return $this->bl_proporcaomes;
    }

    /**
     * @param $bl_proporcaomes
     * @return $this
     */
    public function setBl_proporcaomes($bl_proporcaomes)
    {
        $this->bl_proporcaomes = $bl_proporcaomes;
        return $this;
    }

    /**
     * @return ProjetoContratoDuracaoTipo
     */
    public function getId_projetocontratoduracaotipo()
    {
        return $this->id_projetocontratoduracaotipo;
    }

    /**
     * @param $id_projetocontratoduracaotipo
     * @return $this
     */
    public function setId_projetocontratoduracaotipo($id_projetocontratoduracaotipo)
    {
        $this->id_projetocontratoduracaotipo = $id_projetocontratoduracaotipo;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_contratomultavalor()
    {
        return $this->nu_contratomultavalor;
    }

    /**
     * @param $nu_contratomultavalor
     * @return $this
     */
    public function setNu_contratomultavalor($nu_contratomultavalor)
    {
        $this->nu_contratomultavalor = $nu_contratomultavalor;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_renovarcontrato()
    {
        return $this->bl_renovarcontrato;
    }

    /**
     * @param $bl_renovarcontrato
     * @return $this
     */
    public function setBl_renovarcontrato($bl_renovarcontrato)
    {
        $this->bl_renovarcontrato = $bl_renovarcontrato;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_mesesmensalidade()
    {
        return $this->nu_mesesmensalidade;
    }

    /**
     * @param $nu_mesesmensalidade
     * @return $this
     */
    public function setNu_mesesmensalidade($nu_mesesmensalidade)
    {
        $this->nu_mesesmensalidade = $nu_mesesmensalidade;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tempoextensao()
    {
        return $this->nu_tempoextensao;
    }

    /**
     * @param $nu_tempoextensao
     * @return $this
     */
    public function setNu_tempoextensao($nu_tempoextensao)
    {
        $this->nu_tempoextensao = $nu_tempoextensao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_contratomodelo()
    {
        return $this->id_contratomodelo;
    }

    /**
     * @param $id_contratomodelo
     * @return $this
     */
    public function setId_contratomodelo($id_contratomodelo)
    {
        $this->id_contratomodelo = $id_contratomodelo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_extensaomodelo()
    {
        return $this->id_extensaomodelo;
    }

    /**
     * @param $id_extensaomodelo
     * @return $this
     */
    public function setId_extensaomodelo($id_extensaomodelo)
    {
        $this->id_extensaomodelo = $id_extensaomodelo;
        return $this;
    }

    /**
     * @return TipoRegraContrato
     */
    public function getId_tiporegracontrato()
    {
        return $this->id_tiporegracontrato;
    }

    /**
     * @param TipoRegraContrato $id_tiporegracontrato
     */
    public function setId_tiporegracontrato($id_tiporegracontrato)
    {
        $this->id_tiporegracontrato = $id_tiporegracontrato;
        return $this;
    }

    /**
     * @return TextoSistema
     */
    public function getId_aditivotransferencia()
    {
        return $this->id_aditivotransferencia;
    }

    /**
     * @param TextoSistema $id_aditivotransferencia
     */
    public function setId_aditivotransferencia($id_aditivotransferencia)
    {
        $this->id_aditivotransferencia = $id_aditivotransferencia;
        return $this;
    }

}

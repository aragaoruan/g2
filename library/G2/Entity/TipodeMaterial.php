<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipodematerial")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipodeMaterial extends G2Entity
{

    /**
     *
     * @var integer $id_tipodematerial
     * @Column(name="id_tipodematerial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodematerial;

    /**
     *
     * @var string $st_tipodematerial
     * @Column(name="st_tipodematerial", type="string", nullable=false, length=255)
     */
    private $st_tipodematerial;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    public function getId_tipodematerial ()
    {
        return $this->id_tipodematerial;
    }

    public function setId_tipodematerial ($id_tipodematerial)
    {
        $this->id_tipodematerial = $id_tipodematerial;
        return $this;
    }

    public function getSt_tipodematerial ()
    {
        return $this->st_tipodematerial;
    }

    public function setSt_tipodematerial ($st_tipodematerial)
    {
        $this->st_tipodematerial = $st_tipodematerial;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
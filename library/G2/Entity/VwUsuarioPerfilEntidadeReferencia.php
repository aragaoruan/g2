<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_usuarioperfilentidadereferencia")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwUsuarioPerfilEntidadeReferencia
{

    /**
     * @var integer $id_perfilreferencia
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_perfilreferencia", type="integer", nullable=false, length=4)
     */
    private $id_perfilreferencia;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;

    /**
     * @var integer $id_livro
     * @Column(name="id_livro", type="integer", nullable=true, length=4)
     */
    private $id_livro;

    /**
     * @var integer $id_perfilreferenciaintegracao
     * @Column(name="id_perfilreferenciaintegracao", type="integer", nullable=true, length=4)
     */
    private $id_perfilreferenciaintegracao;


    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true, length=4)
     */
    private $id_perfilpedagogico;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_titular
     * @Column(name="bl_titular", type="boolean", nullable=false, length=1)
     */
    private $bl_titular;

    /**
     * @var boolean $bl_autor
     * @Column(name="bl_autor", type="boolean", nullable=true, length=1)
     */
    private $bl_autor;

    /**
     * @var decimal $nu_porcentagem
     * @Column(name="nu_porcentagem", type="decimal", nullable=true, length=5)
     */
    private $nu_porcentagem;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    public function getId_perfilreferencia ()
    {
        return $this->id_perfilreferencia;
    }

    public function setId_perfilreferencia ($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula ($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getId_livro ()
    {
        return $this->id_livro;
    }

    public function setId_livro ($id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    public function getId_perfilreferenciaintegracao ()
    {
        return $this->id_perfilreferenciaintegracao;
    }

    public function setId_perfilreferenciaintegracao ($id_perfilreferenciaintegracao)
    {
        $this->id_perfilreferenciaintegracao = $id_perfilreferenciaintegracao;
        return $this;
    }

    public function getId_perfilpedagogico ()
    {
        return $this->$id_perfilpedagogico;
    }

    public function setId_perfilpedagogico ($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getBl_titular ()
    {
        return $this->bl_titular;
    }

    public function setBl_titular ($bl_titular)
    {
        $this->bl_titular = $bl_titular;
        return $this;
    }

    public function getBl_autor ()
    {
        return $this->bl_autor;
    }

    public function setBl_autor ($bl_autor)
    {
        $this->bl_autor = $bl_autor;
        return $this;
    }

    public function getNu_porcentagem ()
    {
        return $this->nu_porcentagem;
    }

    public function setNu_porcentagem ($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
        return $this;
    }

    public function getSt_projetopedagogico ()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico ($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_saladeaula ()
    {
        return $this->st_saladeaula;
    }

    public function setSt_saladeaula ($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 03/12/14
 * Time: 15:11
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_respostaautorizacao")
 * @Entity
 */
class RespostaAutorizacao {

    /**
     *
     * @var integer $id_respostaautorizacao
     * @Column(name="id_respostaautorizacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_respostaautorizacao;

    /**
     *
     * @var string $st_codigoresposta
     * @Column(name="st_codigoresposta", type="string", length=50, nullable=false)
     */
    private $st_codigoresposta;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", length=50, nullable=false)
     */
    private $st_descricao;

    /**
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=false, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_respostaautorizacao
     */
    public function setId_respostaautorizacao($id_respostaautorizacao)
    {
        $this->id_respostaautorizacao = $id_respostaautorizacao;
    }

    /**
     * @return int
     */
    public function getId_respostaautorizacao()
    {
        return $this->id_respostaautorizacao;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $st_codigoresposta
     */
    public function setSt_codigoresposta($st_codigoresposta)
    {
        $this->st_codigoresposta = $st_codigoresposta;
    }

    /**
     * @return mixed
     */
    public function getSt_codigoresposta()
    {
        return $this->st_codigoresposta;
    }

    /**
     * @param string $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }
}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_arquivoretorno")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ArquivoRetorno
{

    /**
     *
     * @var integer $id_arquivoretorno
     * @Column(name="id_arquivoretorno", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_arquivoretorno;

    /**
     *
     * @var string $st_arquivoretorno
     * @Column(name="st_arquivoretorno", type="string", nullable=true, length=250)
     */
    private $st_arquivoretorno;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_banco
     * @Column(name="st_banco", type="string", nullable=true, length=3)
     */
    private $st_banco;

    public function getId_arquivoretorno ()
    {
        return $this->id_arquivoretorno;
    }

    public function setId_arquivoretorno ($id_arquivoretorno)
    {
        $this->id_arquivoretorno = $id_arquivoretorno;
        return $this;
    }

    public function getSt_arquivoretorno ()
    {
        return $this->st_arquivoretorno;
    }

    public function setSt_arquivoretorno ($st_arquivoretorno)
    {
        $this->st_arquivoretorno = $st_arquivoretorno;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_banco ()
    {
        return $this->st_banco;
    }

    public function setSt_banco ($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

}
<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_nucleopessoaco")
 * @Entity
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class NucleoPessoaCo extends G2Entity {

    /**
     * @var integer $id_nucleopessoaco
     * @Column(name="id_nucleopessoaco", type="decimal", nullable=false, length=9)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_nucleopessoaco;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @ManyToOne(targetEntity="NucleoCo")
     * @JoinColumn(name="id_nucleoco", nullable=false, referencedColumnName="id_nucleoco")
     */
    private $id_nucleoco;
    /**
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoco", nullable=false, referencedColumnName="id_assuntoco")
     */
    private $id_assuntoco;
    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;
    /**
     * @ManyToOne(targetEntity="Funcao")
     * @JoinColumn(name="id_funcao", nullable=false, referencedColumnName="id_funcao")
     */
    private $id_funcao;
    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var boolean $bl_prioritario
     * @Column(name="bl_prioritario", type="boolean", nullable=false, length=1)
     */
    private $bl_prioritario;
    /**
     * @var boolean $bl_todos
     * @Column(name="bl_todos", type="boolean", nullable=true, length=1)
     */
    private $bl_todos;

    /**
     * @return decimal
     */
    public function getid_nucleopessoaco()
    {
        return $this->id_nucleopessoaco;
    }

    /**
     * @param decimal $id_nucleopessoaco
     */
    public function setid_nucleopessoaco($id_nucleopessoaco)
    {
        $this->id_nucleopessoaco = $id_nucleopessoaco;
    }

    /**
     * @return datetime2
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getid_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setid_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    /**
     * @return int
     */
    public function getid_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param int $id_assuntoco
     */
    public function setid_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getid_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param int $id_funcao
     */
    public function setid_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return boolean
     */
    public function getbl_prioritario()
    {
        return $this->bl_prioritario;
    }

    /**
     * @param boolean $bl_prioritario
     */
    public function setbl_prioritario($bl_prioritario)
    {
        $this->bl_prioritario = $bl_prioritario;
    }

    /**
     * @return boolean
     */
    public function getbl_todos()
    {
        return $this->bl_todos;
    }

    /**
     * @param boolean $bl_todos
     */
    public function setbl_todos($bl_todos)
    {
        $this->bl_todos = $bl_todos;
    }

}
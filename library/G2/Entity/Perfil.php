<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_perfil")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Perfil
{

    /**
     *
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false)
     * @OneToMany(targetEntity="Perfil", mappedBy="id_perfilpai")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_perfil;

    /**
     * @var Perfil $id_perfilpai
     * @ManyToOne(targetEntity="Perfil")
     * @JoinColumn(name="id_perfilpai", nullable=true, referencedColumnName="id_perfil")
     */
    private $id_perfilpai;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil",type="string", nullable=false, length=100)
     */
    private $st_nomeperfil;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo",type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao",type="boolean", nullable=false)
     */
    private $bl_padrao;

    /**
     * @var EntidadedeClasse $id_entidadeclasse
     * @ManyToOne(targetEntity="EntidadedeClasse")
     * @JoinColumn(name="id_entidadeclasse", nullable=true, referencedColumnName="id_entidadeclasse")
     */
    private $id_entidadeclasse;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro",type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var PefilPedagogico $id_perfilpedagogico
     * @ManyToOne(targetEntity="PerfilPedagogico")
     * @JoinColumn(name="id_perfilpedagogico", nullable=true, referencedColumnName="id_perfilpedagogico")
     */
    private $id_perfilpedagogico;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", nullable=false, referencedColumnName="id_sistema")
     */
    private $id_sistema;

    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    public function getId_perfilpai ()
    {
        return $this->id_perfilpai;
    }

    public function setId_perfilpai (Perfil $id_perfilpai)
    {
        $this->id_perfilpai = $id_perfilpai;
        return $this;
    }

    public function getSt_nomeperfil ()
    {
        return $this->st_nomeperfil;
    }

    public function setSt_nomeperfil ($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getId_entidadeclasse ()
    {
        return $this->id_entidadeclasse;
    }

    public function setId_entidadeclasse (EntidadedeClasse $id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_perfilpedagogico ()
    {
        return $this->id_perfilpedagogico;
    }

    public function setId_perfilpedagogico(PerfilPedagogico $id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    public function getId_sistema ()
    {
        return $this->id_sistema;
    }

    public function setId_sistema (Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

}
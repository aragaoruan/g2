<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_desativarcoordenadoresmoodle")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwDesativarCoordenadoresMoodle
{
    /**
     * @var integer $id_perfilreferencia
     * @Column(name="id_perfilreferencia", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_perfilreferencia;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_perfilreferenciaintegracao
     * @Column(name="id_perfilreferenciaintegracao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_perfilreferenciaintegracao;
    /**
     * @var integer $id_saladeaulaintegracao
     * @Column(name="id_saladeaulaintegracao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaulaintegracao;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_sistema;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=false, length=1)
     */
    private $bl_encerrado;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=false, length=30)
     */
    private $st_codusuario;
    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=false, length=30)
     * @Id
     */
    private $st_codsistemacurso;
    /**
     * @var char $bl_desativarmoodle
     * @Column(name="bl_desativarmoodle", type="boolean", nullable=true, length=1)
     */
    private $bl_desativarmoodle;

    /**
     * @var int $bl_desativarmoodle
     * @Column(type="integer")
     * @Id
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwDesativarCoordenadoresMoodle
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


    /**
     * @return integer id_perfilreferencia
     */
    public function getId_perfilreferencia()
    {
        return $this->id_perfilreferencia;
    }

    /**
     * @param id_perfilreferencia
     */
    public function setId_perfilreferencia($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_perfilreferenciaintegracao
     */
    public function getId_perfilreferenciaintegracao()
    {
        return $this->id_perfilreferenciaintegracao;
    }

    /**
     * @param id_perfilreferenciaintegracao
     */
    public function setId_perfilreferenciaintegracao($id_perfilreferenciaintegracao)
    {
        $this->id_perfilreferenciaintegracao = $id_perfilreferenciaintegracao;
        return $this;
    }

    /**
     * @return integer id_saladeaulaintegracao
     */
    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param id_saladeaulaintegracao
     */
    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
        return $this;
    }

    /**
     * @return integer id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_encerrado
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bl_encerrado
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_codusuario
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @return string st_codsistemacurso
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param st_codsistemacurso
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return boolean bl_desativarmoodle
     */
    public function getBl_desativarmoodle()
    {
        return $this->bl_desativarmoodle;
    }

    /**
     * @param bl_desativarmoodle
     */
    public function setBl_desativarmoodle($bl_desativarmoodle)
    {
        $this->bl_desativarmoodle = $bl_desativarmoodle;
        return $this;
    }

}

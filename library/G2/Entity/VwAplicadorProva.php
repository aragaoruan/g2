<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_aplicadorprova")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAplicadorProva
{

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_aplicadorprova;

    /**
     *
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=false, length=200)
     */
    private $st_aplicadorprova;



    /**
     * @var Entidade $id_entidadecadastro
     * @Column(name="id_entidadecadastro", nullable=false, type="integer")
     */
    private $id_entidadecadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_local
     * @Column(name="st_local", type="string", nullable=true, length=300)
     */
    private $st_local;
    
    /**
     *
     * @var integer $id_tipopessoa
     * @Column(name="id_tipopessoa", type="integer", nullable=false)
     */
    private $id_tipopessoa;


    /**
     *
     * @var integer $id_entidadeaplicador
     * @Column(name="id_entidadeaplicador", type="integer", nullable=false)
     */
    private $id_entidadeaplicador;


    /**
     *
     * @var integer $id_usuarioaplicador
     * @Column(name="id_usuarioaplicador", type="integer", nullable=false)
     */
    private $id_usuarioaplicador;


    /**
     *
     * @var string $st_tipopessoa
     * @Column(name="st_tipopessoa", type="string", nullable=true, length=300)
     */
    private $st_tipopessoa;

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param int $id_entidadeaplicador
     */
    public function setId_entidadeaplicador($id_entidadeaplicador)
    {
        $this->id_entidadeaplicador = $id_entidadeaplicador;
    }

    /**
     * @return int
     */
    public function getId_entidadeaplicador()
    {
        return $this->id_entidadeaplicador;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_tipopessoa
     */
    public function setId_tipopessoa($id_tipopessoa)
    {
        $this->id_tipopessoa = $id_tipopessoa;
    }

    /**
     * @return int
     */
    public function getId_tipopessoa()
    {
        return $this->id_tipopessoa;
    }

    /**
     * @param int $id_usuarioaplicador
     */
    public function setId_usuarioaplicador($id_usuarioaplicador)
    {
        $this->id_usuarioaplicador = $id_usuarioaplicador;
    }

    /**
     * @return int
     */
    public function getId_usuarioaplicador()
    {
        return $this->id_usuarioaplicador;
    }

    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @param string $st_local
     */
    public function setSt_local($st_local)
    {
        $this->st_local = $st_local;
    }

    /**
     * @return string
     */
    public function getSt_local()
    {
        return $this->st_local;
    }

    /**
     * @param string $st_tipopessoa
     */
    public function setSt_tipopessoa($st_tipopessoa)
    {
        $this->st_tipopessoa = $st_tipopessoa;
    }

    /**
     * @return string
     */
    public function getSt_tipopessoa()
    {
        return $this->st_tipopessoa;
    }





}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_titulacao")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Titulacao {



    /**
     * @var integer $id_titulacao
     * @Column(name="id_titulacao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_titulacao;



    /**
     * @var string $st_titulacao
     * @Column(name="st_titulacao", type="string", nullable=false, length=200)
     */
    private $st_titulacao;


    public function getId_titulacao()
    {
        return $this->id_titulacao;
    }

    public function setId_titulacao($id_titulacao)
    {
        $this->id_titulacao = $id_titulacao;
    }

    public function getSt_titulacao()
    {
        return $this->st_titulacao;
    }

    public function setSt_titulacao($st_titulacao)
    {
        $this->st_titulacao = $st_titulacao;
    }





}

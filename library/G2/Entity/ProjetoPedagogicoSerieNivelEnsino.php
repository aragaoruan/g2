<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_projetopedagogicoserienivelensino")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class ProjetoPedagogicoSerieNivelEnsino
{

    /**
     * @var integer $id_projetopedagogico
     * @Id
     * @ManyToOne(targetEntity="ProjetoPedagogico", cascade={"all"}, fetch="LAZY")
     * @JoinColumn(referencedColumnName="id_projetopedagogico", name="id_projetopedagogico")
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;


    /**
     * @var Serie $id_serie
     * @ManyToOne(targetEntity="Serie", fetch="LAZY")
     * @JoinColumn(name="id_serie", nullable=false, referencedColumnName="id_serie")
     */
    private $id_serie;

    /**
     * @var NivelEnsino $id_nivelensino
     * @ManyToOne(targetEntity="NivelEnsino", fetch="LAZY")
     * @JoinColumn(name="id_nivelensino", nullable=false, referencedColumnName="id_nivelensino")
     */
    private $id_nivelensino;


	/**
	 * @return integer $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return integer $id_serie
	 */
	public function getId_serie() {
		return $this->id_serie;
	}

	/**
	 * @return integer $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @param \G2\Entity\ProjetoPedagogico $id_projetopedagogico
     * @return $this
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
		return $this;
	}

	/**
	 * @param \G2\Entity\Serie $id_serie
     * @return $this
	 */
	public function setId_serie($id_serie) {
		$this->id_serie = $id_serie;
		return $this;
	}

	/**
	 * @param \G2\Entity\NivelEnsino $id_nivelensino
     * @return $this
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
		return $this;
	}




}


<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_pesquisasaladeaula")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisaSalaDeAula
{

    /**
     *
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     *
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;

    /**
     *
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false)
     */
    private $bl_ativa;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_modalidadesaladeaula
     * @Column(name="id_modalidadesaladeaula", type="integer", nullable=false)
     */
    private $id_modalidadesaladeaula;

    /**
     *
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true)
     */
    private $dt_inicioinscricao;

    /**
     *
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true)
     */
    private $dt_fiminscricao;

    /**
     *
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true)
     */
    private $dt_abertura;

    /**
     *
     * @var string $st_localizacao
     * @Column(name="st_localizacao", type="string", nullable=true, length=255)
     */
    private $st_localizacao;

    /**
     *
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false)
     */
    private $id_tiposaladeaula;

    /**
     *
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true)
     */
    private $dt_encerramento;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true)
     */
    private $id_periodoletivo;

    /**
     *
     * @var boolean $bl_usardoperiodoletivo
     * @Column(name="bl_usardoperiodoletivo", type="boolean", nullable=false)
     */
    private $bl_usardoperiodoletivo;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true)
     */
    private $nu_maxalunos;

    /**
     *
     * @var decimal $nu_diasencerramento
     * @Column(name="nu_diasencerramento", type="decimal", nullable=true)
     */
    private $nu_diasencerramento;

    /**
     *
     * @var boolean $bl_semencerramento
     * @Column(name="bl_semencerramento", type="boolean", nullable=false)
     */
    private $bl_semencerramento;

    /**
     *
     * @var decimal $nu_diasaluno
     * @Column(name="nu_diasaluno", type="decimal", nullable=true)
     */
    private $nu_diasaluno;

    /**
     *
     * @var boolean $bl_semdiasaluno
     * @Column(name="bl_semdiasaluno", type="boolean", nullable=false)
     */
    private $bl_semdiasaluno;

    /**
     *
     * @var integer $nu_diasextensao
     * @Column(name="nu_diasextensao", type="integer", nullable=false)
     */
    private $nu_diasextensao;

    /**
     *
     * @var text $st_pontosnegativos
     * @Column(name="st_pontosnegativos", type="text", nullable=true)
     */
    private $st_pontosnegativos;

    /**
     *
     * @var text $st_pontospositivos
     * @Column(name="st_pontospositivos", type="text", nullable=true)
     */
    private $st_pontospositivos;

    /**
     *
     * @var text $st_pontosmelhorar
     * @Column(name="st_pontosmelhorar", type="text", nullable=true)
     */
    private $st_pontosmelhorar;

    /**
     *
     * @var text $st_pontosresgate
     * @Column(name="st_pontosresgate", type="text", nullable=true)
     */
    private $st_pontosresgate;

    /**
     *
     * @var text $st_conclusoesfinais
     * @Column(name="st_conclusoesfinais", type="text", nullable=true)
     */
    private $st_conclusoesfinais;

    /**
     *
     * @var text $st_justificativaacima
     * @Column(name="st_justificativaacima", type="text", nullable=true)
     */
    private $st_justificativaacima;

    /**
     *
     * @var text $st_justificativaabaixo
     * @Column(name="st_justificativaabaixo", type="text", nullable=true)
     */
    private $st_justificativaabaixo;

    /**
     *
     * @var string $st_modalidadesaladeaula
     * @Column(name="st_modalidadesaladeaula", type="string", nullable=false, length=255)
     */
    private $st_modalidadesaladeaula;

    /**
     *
     * @var integer $id_perfilreferencia
     * @Column(name="id_perfilreferencia", type="integer", nullable=true)
     */
    private $id_perfilreferencia;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_saladeaulaprofessor
     * @Column(name="id_saladeaulaprofessor", type="integer", nullable=true)
     */
    private $id_saladeaulaprofessor;

    /**
     * @var boolean $bl_titular
     * @Column(name="bl_titular", type="boolean", nullable=true)
     */
    private $bl_titular;

    /**
     *
     * @var integer $nu_maximoalunos
     * @Column(name="nu_maximoalunos", type="integer", nullable=true)
     */
    private $nu_maximoalunos;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    /**
     *
     * @var string $st_tiposaladeaula
     * @Column(name="st_tiposaladeaula", type="string", nullable=false, length=255)
     */
    private $st_tiposaladeaula;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     *
     * @var string $st_periodoletivo
     * @Column(name="st_periodoletivo", type="string", nullable=true, length=255)
     */
    private $st_periodoletivo;

    /**
     *
     * @var datetime2 $dt_inicioinscricaoletivo
     * @Column(name="dt_inicioinscricaoletivo", type="datetime2", nullable=true)
     */
    private $dt_inicioinscricaoletivo;

    /**
     *
     * @var datetime2 $dt_fiminscricaoletivo
     * @Column(name="dt_fiminscricaoletivo", type="datetime2", nullable=true)
     */
    private $dt_fiminscricaoletivo;

    /**
     *
     * @var datetime2 $dt_aberturaletivo
     * @Column(name="dt_aberturaletivo", type="datetime2", nullable=true)
     */
    private $dt_aberturaletivo;

    /**
     *
     * @var datetime2 $dt_encerramentoletivo
     * @Column(name="dt_encerramentoletivo", type="datetime2", nullable=true)
     */
    private $dt_encerramentoletivo;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var string $st_nomeprofessor
     * @Column(name="st_nomeprofessor", type="string", nullable=true, length=300)
     */
    private $st_nomeprofessor;

    /**
     *
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true)
     */
    private $id_disciplina;

    /**
     *
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=400)
     */
    private $st_disciplina;


    /**
     * @var datetime $dt_comextensao
     * @Column(name="dt_comextensao", type="datetime", nullable=true)
     */
    private $dt_comextensao;


    /**
     *
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=false, length=255)
     */
    private $st_sistema;

    /**
     * @return string
     */
    public function getst_sistema()
    {
        return $this->st_sistema;
    }

    /**
     * @param string $st_sistema
     */
    public function setst_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
        return $this;

    }


    public function getId_saladeaula ()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula ($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_saladeaula ()
    {
        return $this->st_saladeaula;
    }

    public function setSt_saladeaula ($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    public function getBl_ativa ()
    {
        return $this->bl_ativa;
    }

    public function setBl_ativa ($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_modalidadesaladeaula ()
    {
        return $this->id_modalidadesaladeaula;
    }

    public function setId_modalidadesaladeaula ($id_modalidadesaladeaula)
    {
        $this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
        return $this;
    }

    public function getDt_inicioinscricao ()
    {
        return $this->dt_inicioinscricao;
    }

    public function setDt_inicioinscricao ($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    public function getDt_fiminscricao ()
    {
        return $this->dt_fiminscricao;
    }

    public function setDt_fiminscricao ($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    public function getDt_abertura ()
    {
        return $this->dt_abertura;
    }

    public function setDt_abertura ($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    public function getSt_localizacao ()
    {
        return $this->st_localizacao;
    }

    public function setSt_localizacao ($st_localizacao)
    {
        $this->st_localizacao = $st_localizacao;
        return $this;
    }

    public function getId_tiposaladeaula ()
    {
        return $this->id_tiposaladeaula;
    }

    public function setId_tiposaladeaula ($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
        return $this;
    }

    public function getDt_encerramento ()
    {
        return $this->dt_encerramento;
    }

    public function setDt_encerramento ($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_periodoletivo ()
    {
        return $this->id_periodoletivo;
    }

    public function setId_periodoletivo ($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    public function getBl_usardoperiodoletivo ()
    {
        return $this->bl_usardoperiodoletivo;
    }

    public function setBl_usardoperiodoletivo ($bl_usardoperiodoletivo)
    {
        $this->bl_usardoperiodoletivo = $bl_usardoperiodoletivo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getNu_maxalunos ()
    {
        return $this->nu_maxalunos;
    }

    public function setNu_maxalunos ($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    public function getNu_diasencerramento ()
    {
        return $this->nu_diasencerramento;
    }

    public function setNu_diasencerramento ($nu_diasencerramento)
    {
        $this->nu_diasencerramento = $nu_diasencerramento;
        return $this;
    }

    public function getBl_semencerramento ()
    {
        return $this->bl_semencerramento;
    }

    public function setBl_semencerramento ($bl_semencerramento)
    {
        $this->bl_semencerramento = $bl_semencerramento;
        return $this;
    }

    public function getNu_diasaluno ()
    {
        return $this->nu_diasaluno;
    }

    public function setNu_diasaluno ($nu_diasaluno)
    {
        $this->nu_diasaluno = $nu_diasaluno;
        return $this;
    }

    public function getBl_semdiasaluno ()
    {
        return $this->bl_semdiasaluno;
    }

    public function setBl_semdiasaluno ($bl_semdiasaluno)
    {
        $this->bl_semdiasaluno = $bl_semdiasaluno;
        return $this;
    }

    public function getNu_diasextensao ()
    {
        return $this->nu_diasextensao;
    }

    public function setNu_diasextensao ($nu_diasextensao)
    {
        $this->nu_diasextensao = $nu_diasextensao;
        return $this;
    }

    public function getSt_pontosnegativos ()
    {
        return $this->st_pontosnegativos;
    }

    public function setSt_pontosnegativos ($st_pontosnegativos)
    {
        $this->st_pontosnegativos = $st_pontosnegativos;
        return $this;
    }

    public function getSt_pontospositivos ()
    {
        return $this->st_pontospositivos;
    }

    public function setSt_pontospositivos ($st_pontospositivos)
    {
        $this->st_pontospositivos = $st_pontospositivos;
        return $this;
    }

    public function getSt_pontosmelhorar ()
    {
        return $this->st_pontosmelhorar;
    }

    public function setSt_pontosmelhorar ($st_pontosmelhorar)
    {
        $this->st_pontosmelhorar = $st_pontosmelhorar;
        return $this;
    }

    public function getSt_pontosresgate ()
    {
        return $this->st_pontosresgate;
    }

    public function setSt_pontosresgate ($st_pontosresgate)
    {
        $this->st_pontosresgate = $st_pontosresgate;
        return $this;
    }

    public function getSt_conclusoesfinais ()
    {
        return $this->st_conclusoesfinais;
    }

    public function setSt_conclusoesfinais ($st_conclusoesfinais)
    {
        $this->st_conclusoesfinais = $st_conclusoesfinais;
        return $this;
    }

    public function getSt_justificativaacima ()
    {
        return $this->st_justificativaacima;
    }

    public function setSt_justificativaacima ($st_justificativaacima)
    {
        $this->st_justificativaacima = $st_justificativaacima;
        return $this;
    }

    public function getSt_justificativaabaixo ()
    {
        return $this->st_justificativaabaixo;
    }

    public function setSt_justificativaabaixo ($st_justificativaabaixo)
    {
        $this->st_justificativaabaixo = $st_justificativaabaixo;
        return $this;
    }

    public function getSt_modalidadesaladeaula ()
    {
        return $this->st_modalidadesaladeaula;
    }

    public function setSt_modalidadesaladeaula ($st_modalidadesaladeaula)
    {
        $this->st_modalidadesaladeaula = $st_modalidadesaladeaula;
        return $this;
    }

    public function getId_perfilreferencia ()
    {
        return $this->id_perfilreferencia;
    }

    public function setId_perfilreferencia ($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_saladeaulaprofessor ()
    {
        return $this->id_saladeaulaprofessor;
    }

    public function setId_saladeaulaprofessor ($id_saladeaulaprofessor)
    {
        $this->id_saladeaulaprofessor = $id_saladeaulaprofessor;
        return $this;
    }

    public function getBl_titular ()
    {
        return $this->bl_titular;
    }

    public function setBl_titular ($bl_titular)
    {
        $this->bl_titular = $bl_titular;
        return $this;
    }

    public function getNu_maximoalunos ()
    {
        return $this->nu_maximoalunos;
    }

    public function setNu_maximoalunos ($nu_maximoalunos)
    {
        $this->nu_maximoalunos = $nu_maximoalunos;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getSt_tiposaladeaula ()
    {
        return $this->st_tiposaladeaula;
    }

    public function setSt_tiposaladeaula ($st_tiposaladeaula)
    {
        $this->st_tiposaladeaula = $st_tiposaladeaula;
        return $this;
    }

    public function getSt_nomeentidade ()
    {
        return $this->st_nomeentidade;
    }

    public function setSt_nomeentidade ($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function getSt_periodoletivo ()
    {
        return $this->st_periodoletivo;
    }

    public function setSt_periodoletivo ($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

    public function getDt_inicioinscricaoletivo ()
    {
        return $this->dt_inicioinscricaoletivo;
    }

    public function setDt_inicioinscricaoletivo ($dt_inicioinscricaoletivo)
    {
        $this->dt_inicioinscricaoletivo = $dt_inicioinscricaoletivo;
        return $this;
    }

    public function getDt_fiminscricaoletivo ()
    {
        return $this->dt_fiminscricaoletivo;
    }

    public function setDt_fiminscricaoletivo ($dt_fiminscricaoletivo)
    {
        $this->dt_fiminscricaoletivo = $dt_fiminscricaoletivo;
        return $this;
    }

    public function getDt_aberturaletivo ()
    {
        return $this->dt_aberturaletivo;
    }

    public function setDt_aberturaletivo ($dt_aberturaletivo)
    {
        $this->dt_aberturaletivo = $dt_aberturaletivo;
        return $this;
    }

    public function getDt_encerramentoletivo ()
    {
        return $this->dt_encerramentoletivo;
    }

    public function setDt_encerramentoletivo ($dt_encerramentoletivo)
    {
        $this->dt_encerramentoletivo = $dt_encerramentoletivo;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getSt_nomeprofessor ()
    {
        return $this->st_nomeprofessor;
    }

    public function setSt_nomeprofessor ($st_nomeprofessor)
    {
        $this->st_nomeprofessor = $st_nomeprofessor;
        return $this;
    }

    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function getDt_comextensao()
    {
        return $this->dt_comextensao;
    }

    public function setDt_comextensao($dt_comextensao)
    {
        $this->dt_comextensao = $dt_comextensao;
    }


}
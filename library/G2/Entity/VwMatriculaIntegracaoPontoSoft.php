<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculaintegracao_pontosoft")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwMatriculaIntegracaoPontoSoft
{
    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_matricula;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false)
     */
    private $st_cpf;

    /**
     *
     * @var string $st_identificacao
     * @Column(name="st_identificacao", type="string", nullable=false)
     */
    private $st_identificacao;

    /**
     *
     * @var string $st_sexo
     * @Column(name="st_sexo", type="string", nullable=false)
     */
    private $st_sexo;

    /**
     *
     * @var string $st_codigo
     * @Column(name="st_codigo", type="string", nullable=true)
     */
    private $st_codigo;

    /**
     *
     * @var string $st_turmasistema
     * @Column(name="st_turmasistema", type="string", nullable=false)
     */
    private $st_turmasistema;

    /**
     *
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false)
     */
    private $id_turma;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var \Datetime $dt_inicioturma
     * @Column(name="dt_inicioturma", type="datetime", nullable=false)
     */
    private $dt_inicioturma;

    /**
     *
     * @var \Datetime $dt_iniciomatricula
     * @Column(name="dt_iniciomatricula", type="datetime", nullable=false)
     */
    private $dt_iniciomatricula;

    /**
     *
     * @var \Datetime $dt_terminomatricula
     * @Column(name="dt_terminomatricula", type="datetime", nullable=false)
     */
    private $dt_terminomatricula;

    /**
     *
     * @var integer $id_turmaintegracao
     * @Column(name="id_turmaintegracao", type="integer", nullable=false)
     */
    private $id_turmaintegracao;

    /**
     * @return string
     */
    public function getst_codigo()
    {
        return $this->st_codigo;
    }

    /**
     * @param string $st_codigo
     */
    public function setst_codigo($st_codigo)
    {
        $this->st_codigo = $st_codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_turmasistema()
    {
        return $this->st_turmasistema;
    }

    /**
     * @param string $st_turmasistema
     */
    public function setst_turmasistema($st_turmasistema)
    {
        $this->st_turmasistema = $st_turmasistema;
        return $this;
    }


    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return \Datetime
     */
    public function getDt_iniciomatricula()
    {
        return $this->dt_iniciomatricula;
    }

    /**
     * @param \Datetime $dt_iniciomatricula
     */
    public function setDt_iniciomatricula($dt_iniciomatricula)
    {
        $this->dt_iniciomatricula = $dt_iniciomatricula;
    }

    /**
     * @return \Datetime
     */
    public function getDt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param \Datetime $dt_inicioturma
     */
    public function setDt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
    }

    /**
     * @return \Datetime
     */
    public function getDt_terminomatricula()
    {
        return $this->dt_terminomatricula;
    }

    /**
     * @param \Datetime $dt_terminomatricula
     */
    public function setDt_terminomatricula($dt_terminomatricula)
    {
        $this->dt_terminomatricula = $dt_terminomatricula;
    }

    /**
     * @return int
     */
    public function getId_turmaintegracao()
    {
        return $this->id_turmaintegracao;
    }

    /**
     * @param int $id_turmaintegracao
     */
    public function setId_turmaintegracao($id_turmaintegracao)
    {
        $this->id_turmaintegracao = $id_turmaintegracao;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }

    /**
     * @param string $st_identificacao
     */
    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
    }

    /**
     * @return string
     */
    public function getSt_sexo()
    {
        return $this->st_sexo;
    }

    /**
     * @param string $st_sexo
     */
    public function setSt_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
    }
}
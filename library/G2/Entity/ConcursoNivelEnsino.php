<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_concursonivelensino")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ConcursoNivelEnsino
{

    /**
     * @var integer $id_concursonivelensino
     * @Column(type="integer", nullable=false, name="id_concursonivelensino")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_concursonivelensino;

    /**
     * @var Concurso $id_concurso
     * @ManyToOne(targetEntity="Concurso", inversedBy="niveisEnsino")
     * @JoinColumn(name="id_concurso", referencedColumnName="id_concurso", nullable=false)
     */
    private $id_concurso;

    /**
     * @var NivelEnsino $id_concurso
     * @ManyToOne(targetEntity="NivelEnsino")
     * @JoinColumn(name="id_nivelensino", referencedColumnName="id_nivelensino", nullable=false)
     */
    private $id_nivelensino;

    /**
     * @return integer id_concursonivelensino
     */
    public function getId_concursonivelensino ()
    {
        return $this->id_concursonivelensino;
    }

    /**
     * 
     * @return \G2\Entity\Concurso id_concurso
     */
    public function getId_concurso ()
    {
        return $this->id_concurso;
    }

    /**
     * 
     * @return \G2\Entity\NivelEnsino id_nivelensino
     */
    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param integer $id_concursonivelensino
     * @return \G2\Entity\ConcursoNivelEnsino
     */
    public function setId_concursonivelensino ($id_concursonivelensino)
    {
        $this->id_concursonivelensino = $id_concursonivelensino;
        return $this;
    }

    /**
     * @param \G2\Entity\Concurso $id_concurso
     * @return \G2\Entity\ConcursoNivelEnsino
     */
    public function setId_concurso (Concurso $id_concurso)
    {
        $this->id_concurso = $id_concurso;
        return $this;
    }

    /**
     * @param \G2\Entity\NivelEnsino $id_nivelensino
     * @return \G2\Entity\ConcursoNivelEnsino
     */
    public function setId_nivelensino (NivelEnsino $id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

}

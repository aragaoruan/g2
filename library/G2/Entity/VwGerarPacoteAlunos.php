<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_gerarpacotealunos")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwGerarPacoteAlunos {

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_itemdematerial;
    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;
    /**
     * @var integer $id_situacaoitem
     * @Column(name="id_situacaoitem", type="integer", nullable=false, length=4)
     */
    private $id_situacaoitem;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_itemdematerial
     */
    public function getId_itemdematerial() {
        return $this->id_itemdematerial;
    }

    /**
     * @param id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial) {
        $this->id_itemdematerial = $id_itemdematerial;
        return $this;
    }

    /**
     * @return integer id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return integer id_situacaoitem
     */
    public function getId_situacaoitem() {
        return $this->id_situacaoitem;
    }

    /**
     * @param id_situacaoitem
     */
    public function setId_situacaoitem($id_situacaoitem) {
        $this->id_situacaoitem = $id_situacaoitem;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }


}

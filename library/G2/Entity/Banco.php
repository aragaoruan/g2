<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_banco")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Banco extends G2Entity
{

    /**
     *
     * @var integer $id_banco
     * @Column(name="id_banco", type="integer", nullable=false)
     */
    private $id_banco;

    /**
     *
     * @var string $st_nomebanco
     * @Column(name="st_nomebanco", type="string", nullable=false, length=255)
     */
    private $st_nomebanco;

    /**
     *
     * @var string $st_banco
     * @Column(name="st_banco", type="string", nullable=false, length=3)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $st_banco;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getId_banco ()
    {
        return $this->id_banco;
    }

    public function setId_banco($id_banco)
    {
        $this->id_banco = $id_banco;
        return $this;
    }

    public function getSt_nomebanco()
    {
        return $this->st_nomebanco;
    }

    public function setSt_nomebanco($st_nomebanco)
    {
        $this->st_nomebanco = $st_nomebanco;
        return $this;
    }

    public function getSt_banco()
    {
        return $this->st_banco;
    }

    public function setSt_banco($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }
}
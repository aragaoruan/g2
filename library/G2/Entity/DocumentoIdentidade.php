<?php

namespace G2\Entity;

/**
 * Class DocumentoIdentidade
 * @package G2\Entity
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentoidentidade")
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class DocumentoIdentidade
{


    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var string $st_rg
     * @Column(name="st_rg", type="string", nullable=false, length=20)
     */
    private $st_rg;

    /**
     * @var string $st_orgaoexpeditor
     * @Column(name="st_orgaoexpeditor", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpeditor;

    /**
     * @var date $dt_dataexpedicao
     * @Column(name="dt_dataexpedicao", type="date", nullable=true, length=3)
     */
    private $dt_dataexpedicao;

    /**
     * @return integer
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return integer
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_rg()
    {
        return $this->st_rg;
    }

    /**
     * @return string
     */
    public function getSt_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @return date
     */
    public function getDt_dataexpedicao()
    {
        return $this->dt_dataexpedicao;
    }

    /**
     * @param integer $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param integer $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param string $st_rg
     * @return $this
     */
    public function setSt_rg($st_rg)
    {
        $this->st_rg = $st_rg;
        return $this;
    }

    /**
     * @param string $st_orgaoexpeditor
     * @return $this
     */
    public function setSt_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    /**
     * @param date $dt_dataexpedicao
     * @return $this
     */
    public function setDt_dataexpedicao($dt_dataexpedicao)
    {
        $this->dt_dataexpedicao = $dt_dataexpedicao;
        return $this;
    }


}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_dadosacesso")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class DadosAcesso
{

    /**
     * @var integer $id_dadosacesso
     * @Column(name="id_dadosacesso", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_dadosacesso;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var string $st_login
     * @Column(name="st_login", type="string", nullable=true, length=50)
     */
    private $st_login;

    /**
     * @var string $st_senha
     * @Column(name="st_senha", type="string", nullable=true, length=50)
     */
    private $st_senha;

    public function getId_dadosacesso ()
    {
        return $this->id_dadosacesso;
    }

    public function setId_dadosacesso ($id_dadosacesso)
    {
        $this->id_dadosacesso = $id_dadosacesso;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\DadosAcesso
     */
    public function setId_usuario (Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * 
     * @return \G2\Entity\Usuario
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\DadosAcesso
     */
    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\DadosAcesso
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_login ()
    {
        return $this->st_login;
    }

    public function setSt_login ($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    public function getSt_senha ()
    {
        return $this->st_senha;
    }

    public function setSt_senha ($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    /**
     * Return Entity Attributes
     * @return Array Entity
     */
    public function _toArray ()
    {
        return array(
            'id_dadosacesso' => $this->id_dadosacesso,
            'id_usuario' => $this->id_usuario,
            'st_login' => $this->st_login,
            'st_senha' => $this->st_senha,
            'dt_cadastro' => $this->dt_cadastro,
            'bl_ativo' => $this->bl_ativo,
            'id_usuariocadastro' => $this->id_usuariocadastro,
            'id_entidade' => $this->id_entidade
        );
    }

}
<?php

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_minhapasta")
 * @Entity
 * @author Neemias Santos <neemias.santos@gmail.com>
 */
class MinhaPasta
{

    /**
     *
     * @var integer $id_minhapasta
     * @Column(name="id_minhapasta", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_minhapasta;

    /**
     * @var string $st_contrato
     * @Column(name="st_contrato", type="string", nullable=true)
     */
    private $st_contrato;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var integer $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     */
    private $id_venda;

    /**
     * @var integer $id_upload
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", referencedColumnName="id_upload", nullable=true)
     */
    private $id_upload;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_usuarioatualizacao
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioatualizacao", referencedColumnName="id_usuario")
     */
    private $id_usuarioatualizacao;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var integer $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var $bl_ativo
     * @Column(name="bl_ativo", type="boolean")
     */
    private $bl_ativo;

    /**
     * @var $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var $dt_atualizacao
     * @Column(name="dt_atualizacao", type="datetime", nullable=false)
     */
    private $dt_atualizacao;

    /**
     * @var string $st_contratourl
     * @Column(name="st_contratourl", type="string", nullable=true)
     */
    private $st_contratourl;

    /**
     * @return int
     */
    public function getId_minhapasta()
    {
        return $this->id_minhapasta;
    }

    /**
     * @param int $id_minhapasta
     * @return MinhaPasta
     */
    public function setId_minhapasta($id_minhapasta)
    {
        $this->id_minhapasta = $id_minhapasta;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_contrato()
    {
        return $this->st_contrato;
    }

    /**
     * @param string $st_contrato
     * @return MinhaPasta
     */
    public function setSt_contrato($st_contrato)
    {
        $this->st_contrato = $st_contrato;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return MinhaPasta
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param int $id_upload
     * @return MinhaPasta
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return MinhaPasta
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuarioatualizacao()
    {
        return $this->id_usuarioatualizacao;
    }

    /**
     * @param int $id_usuarioatualizacao
     */
    public function setId_usuarioatualizacao($id_usuarioatualizacao)
    {
        $this->id_usuarioatualizacao = $id_usuarioatualizacao;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return MinhaPasta
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return MinhaPasta
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return MinhaPasta
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     * @return MinhaPasta
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return MinhaPasta
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_atualizacao()
    {
        return $this->dt_atualizacao;
    }

    /**
     * @param mixed $dt_atualizacao
     * @return MinhaPasta
     */
    public function setDt_atualizacao($dt_atualizacao)
    {
        $this->dt_atualizacao = $dt_atualizacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_contratourl()
    {
        return $this->st_contratourl;
    }

    /**
     * @param string $st_contratourl
     */
    public function setSt_contratourl($st_contratourl)
    {
        $this->st_contratourl = $st_contratourl;
    }



}
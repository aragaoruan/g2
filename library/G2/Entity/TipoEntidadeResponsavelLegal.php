<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoentidaderesponsavellegal")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoEntidadeResponsavelLegal
{

    /**
     *
     * @var integer $id_tipoentidaderesponsavel
     * @Column(name="id_tipoentidaderesponsavel", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoentidaderesponsavel;

    /**
     *
     * @var string $st_tipoentidaderesponsavel
     * @Colum(name="st_tipoentidaderesponsavel", type="string", nullable=true, length=255)
     */
    private $st_tipoentidaderesponsavel;

    public function getId_tipoentidaderesponsavel ()
    {
        return $this->id_tipoentidaderesponsavel;
    }

    public function setId_tipoentidaderesponsavel ($id_tipoentidaderesponsavel)
    {
        $this->id_tipoentidaderesponsavel = $id_tipoentidaderesponsavel;
        return $this;
    }

    public function getSt_tipoentidaderesponsavel ()
    {
        return $this->st_tipoentidaderesponsavel;
    }

    public function setSt_tipoentidaderesponsavel ($st_tipoentidaderesponsavel)
    {
        $this->st_tipoentidaderesponsavel = $st_tipoentidaderesponsavel;
        return $this;
    }

}
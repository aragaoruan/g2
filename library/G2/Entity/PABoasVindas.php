<?php

namespace G2\Entity;

use G2\Entity\Entidade;
use G2\Entity\Usuario;
use G2\Entity\TextoSistema;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pa_boasvindas")
 * @Entity(repositoryClass="G2\Repository\PABoasVindasRepository")
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 */
class PABoasVindas {

    /**
     * @Id
     * @GeneratedValue
     * @Column(name="id_pa_boasvindas", type="integer")
     */
    private $id_pa_boasvindas;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /** @Column(name="dt_cadastro", type="datetime2", nullable=false) */
    private $dt_cadastro;

    function __construct() {
        $this->id_usuariocadastro = new Usuario();
        $this->id_textosistema = new TextoSistema();
        $this->id_entidade = new Entidade();
    }

    public function getId_pa_boasvindas() {
        return $this->id_pa_boasvindas;
    }

    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_pa_boasvindas($id_pa_boasvindas) {
        $this->id_pa_boasvindas = $id_pa_boasvindas;
    }

    public function setId_textosistema($id_textosistema) {
        $this->id_textosistema = $id_textosistema;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

}

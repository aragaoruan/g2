<?php

namespace G2\Entity;

/**
 * Class InformacaoAcademicaPessoa
 * @package G2\Entity
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_informacaoacademicapessoa")
 * @Author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class InformacaoAcademicaPessoa
{

    /**
     * @var integer $id_informacaoacademicapessoa
     * @Column(name="id_informacaoacademicapessoa", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_informacaoacademicapessoa;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var NivelEnsino $id_nivelensino
     * @ManyToOne(targetEntity="NivelEnsino")
     * @JoinColumn(name="id_nivelensino", referencedColumnName="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     * @var string $st_curso
     * @Column(name="st_curso", type="string", nullable=false, length=400)
     */
    private $st_curso;

    /**
     * @var string $st_nomeinstituicao
     * @Column(name="st_nomeinstituicao", type="string", nullable=false, length=400)
     */
    private $st_nomeinstituicao;

    /**
     * @return integer
     */
    public function getId_informacaoacademicapessoa()
    {
        return $this->id_informacaoacademicapessoa;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return NivelEnsino
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @return string
     */
    public function getSt_curso()
    {
        return $this->st_curso;
    }

    /**
     * @return string
     */
    public function getSt_nomeinstituicao()
    {
        return $this->st_nomeinstituicao;
    }

    /**
     *
     * @param integer $id_informacaoacademicapessoa
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setId_informacaoacademicapessoa($id_informacaoacademicapessoa)
    {
        $this->id_informacaoacademicapessoa = $id_informacaoacademicapessoa;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setId_usuario(Usuario $id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\NivelEnsino $id_nivelensino
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setId_nivelensino(NivelEnsino $id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    /**
     * @param string $st_curso
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setSt_curso($st_curso)
    {
        $this->st_curso = $st_curso;
        return $this;
    }

    /**
     * @param string $st_nomeinstituicao
     * @return \G2\Entity\InformacaoAcademicaPessoa
     */
    public function setSt_nomeinstituicao($st_nomeinstituicao)
    {
        $this->st_nomeinstituicao = $st_nomeinstituicao;
        return $this;
    }

}

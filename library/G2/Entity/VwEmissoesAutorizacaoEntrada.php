<?php

namespace G2\Entity;

/**
 * Description of VwEmissoesAutorizacaoEntrada
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_emissoesautorizacaoentrada")
 * @EntityView
 */
class VwEmissoesAutorizacaoEntrada
{

    /**
     * @var integer $id_usuario
     * @Id
     * @GeneratedValue(strategy="NONE") 
     * @Column(type="integer", nullable=true, name="id_usuario")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(type="string", nullable=true, name="st_nomecompleto") 
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_cpf
     * @Column(type="string", nullable=true, name="st_cpf") 
     */
    private $st_cpf;

    /**
     * @var integer $nu_totalemissao
     * @Column(type="integer", nullable=true, name="nu_totalemissao") 
     */
    private $nu_totalemissao;

    /**
     * @var integer $id_entidade
     * @Id
     * @GeneratedValue(strategy="NONE") 
     * @Column(type="integer", nullable=true, name="id_entidade")
     */
    private $id_entidade;

    /**
     * 
     * @return integer
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * 
     * @return string
     */
    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    /**
     * 
     * @return string
     */
    public function getSt_cpf ()
    {
        return $this->st_cpf;
    }

    /**
     * 
     * @return integer
     */
    public function getNu_totalemissao ()
    {
        return $this->nu_totalemissao;
    }

    /**
     * 
     * @return integer
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @param integer $id_usuario
     * @return \G2\Entity\VwEmissoesAutorizacaoEntrada
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * 
     * @param string $st_nomecompleto
     * @return \G2\Entity\VwEmissoesAutorizacaoEntrada
     */
    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * 
     * @param string $st_cpf
     * @return \G2\Entity\VwEmissoesAutorizacaoEntrada
     */
    public function setSt_cpf ($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * 
     * @param integer $nu_totalemissao
     * @return \G2\Entity\VwEmissoesAutorizacaoEntrada
     */
    public function setNu_totalemissao ($nu_totalemissao)
    {
        $this->nu_totalemissao = $nu_totalemissao;
        return $this;
    }

    /**
     * 
     * @param integer $id_entidade
     * @return \G2\Entity\VwEmissoesAutorizacaoEntrada
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

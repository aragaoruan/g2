<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_evasaoaluno")
 * @Entity
 * @author Janilson Silva <janilson.silva@unyleya.com.br>
 */
class VwEvasaoAluno extends G2Entity
{

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var string $st_entidade
     * @Column(name="st_entidade", type="string")
     */
    private $st_entidade;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var \DateTime $dt_matricula
     * @Column(name="dt_matricula", type="datetime2")
     */
    private $dt_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var decimal $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="decimal")
     */
    private $nu_cargahoraria;

    /**
     * @var decimal $nu_cargahorariaintegralizada
     * @Column(name="nu_cargahorariaintegralizada", type="decimal")
     */
    private $nu_cargahorariaintegralizada;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var \DateTime $dt_renovacao
     * @Column(name="dt_renovacao", type="datetime2")
     */
    private $dt_renovacao;

    /**
     * @var \DateTime $dt_ultimaoferta
     * @Column(name="dt_ultimaoferta", type="datetime2")
     */
    private $dt_ultimaoferta;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string")
     */
    private $st_saladeaula;

    /**
     * @var integer $id_logacesso
     * @Column(name="id_logacesso", type="integer")
     */
    private $id_logacesso;

    /**
     * @var \DateTime $dt_ultimoacesso
     * @Column(name="dt_ultimoacesso", type="datetime2")
     */
    private $dt_ultimoacesso;

    /**
     * @var boolean $bl_inadimplente
     * @Column(name="bl_inadimplente", type="boolean")
     */
    private $bl_inadimplente;

    /**
     * @var string $st_inadimplente
     * @Column(name="st_inadimplente", type="string")
     */
    private $st_inadimplente;

    /**
     * @var integer $nu_qtdparcelaaberto
     * @Column(name="nu_qtdparcelaaberto", type="integer")
     */
    private $nu_qtdparcelaaberto;

    /**
     * @var string $st_qtdparcelaaberto
     * @Column(name="st_qtdparcelaaberto", type="string")
     */
    private $st_qtdparcelaaberto;

    /**
     * @var decimal $nu_valortotalaberto
     * @Column(name="nu_valortotalaberto", type="decimal")
     */
    private $nu_valortotalaberto;

    /**
     * @var boolean $bl_ocorrenciacancelamento
     * @Column(name="bl_ocorrenciacancelamento", type="boolean")
     */
    private $bl_ocorrenciacancelamento;

    /**
     * @var string $st_ocorrenciacancelamento
     * @Column(name="st_ocorrenciacancelamento", type="string")
     */
    private $st_ocorrenciacancelamento;

    /**
     * @var integer $id_ocorrenciacancelamento
     * @Column(name="id_ocorrenciacancelamento", type="integer")
     */
    private $id_ocorrenciacancelamento;

    /**
     * @var boolean $bl_ocorrenciatrancamento
     * @Column(name="bl_ocorrenciatrancamento", type="boolean")
     */
    private $bl_ocorrenciatrancamento;

    /**
     * @var string $st_ocorrenciatrancamento
     * @Column(name="st_ocorrenciatrancamento", type="string")
     */
    private $st_ocorrenciatrancamento;

    /**
     * @var integer $id_ocorrenciatrancamento
     * @Column(name="id_ocorrenciatrancamento", type="integer")
     */
    private $id_ocorrenciatrancamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @return integer
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param integer $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_entidade()
    {
        return $this->st_entidade;
    }

    /**
     * @param string $st_entidade
     * @return $this
     */
    public function setSt_entidade($st_entidade)
    {
        $this->st_entidade = $st_entidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return $this
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return $this
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param \DateTime $dt_matricula
     * @return $this
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param integer $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param decimal $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_cargahorariaintegralizada()
    {
        return $this->nu_cargahorariaintegralizada;
    }

    /**
     * @param decimal $nu_cargahorariaintegralizada
     * @return $this
     */
    public function setNu_cargahorariaintegralizada($nu_cargahorariaintegralizada)
    {
        $this->nu_cargahorariaintegralizada = $nu_cargahorariaintegralizada;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param integer $id_venda
     * @return $this
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_renovacao()
    {
        return $this->dt_renovacao;
    }

    /**
     * @param \DateTime $dt_renovacao
     * @return $this
     */
    public function setDt_renovacao($dt_renovacao)
    {
        $this->dt_renovacao = $dt_renovacao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param \DateTime $dt_ultimaoferta
     * @return $this
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return $this
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_logacesso()
    {
        return $this->id_logacesso;
    }

    /**
     * @param integer $id_logacesso
     * @return $this
     */
    public function setId_logacesso($id_logacesso)
    {
        $this->id_logacesso = $id_logacesso;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ultimoacesso()
    {
        return $this->dt_ultimoacesso;
    }

    /**
     * @param \DateTime $dt_ultimoacesso
     * @return $this
     */
    public function setDt_ultimoacesso($dt_ultimoacesso)
    {
        $this->dt_ultimoacesso = $dt_ultimoacesso;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_inadimplente()
    {
        return $this->st_inadimplente;
    }

    /**
     * @param boolean $bl_inadimplente
     * @return $this
     */
    public function setBl_inadimplente($bl_inadimplente)
    {
        $this->bl_inadimplente = $bl_inadimplente;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_inadimplente()
    {
        return $this->st_inadimplente;
    }

    /**
     * @param string $st_inadimplente
     * @return $this
     */
    public function setSt_inadimplente($st_inadimplente)
    {
        $this->st_inadimplente = $st_inadimplente;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_qtdparcelaaberto()
    {
        return $this->nu_qtdparcelaaberto;
    }

    /**
     * @param integer $nu_qtdparcelaaberto
     * @return $this
     */
    public function setNu_qtdparcelaaberto($nu_qtdparcelaaberto)
    {
        $this->nu_qtdparcelaaberto = $nu_qtdparcelaaberto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_qtdparcelaaberto()
    {
        return $this->st_qtdparcelaaberto;
    }

    /**
     * @param string $st_qtdparcelaaberto
     * @return $this
     */
    public function setSt_qtdparcelaaberto($st_qtdparcelaaberto)
    {
        $this->st_qtdparcelaaberto = $st_qtdparcelaaberto;
        return $this;
    }

    /**
     * @return numeric
     */
    public function getNu_valortotalaberto()
    {
        return $this->nu_valortotalaberto;
    }

    /**
     * @param numeric $nu_valortotalaberto
     * @return $this
     */
    public function setNu_valortotalaberto($nu_valortotalaberto)
    {
        $this->nu_valortotalaberto = $nu_valortotalaberto;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ocorrenciacancelamento()
    {
        return $this->bl_ocorrenciacancelamento;
    }

    /**
     * @param boolean $bl_ocorrenciacancelamento
     * @return $this
     */
    public function setBl_ocorrenciacancelamento($bl_ocorrenciacancelamento)
    {
        $this->bl_ocorrenciacancelamento = $bl_ocorrenciacancelamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_ocorrenciacancelamento()
    {
        return $this->st_ocorrenciacancelamento;
    }

    /**
     * @param string $st_ocorrenciacancelamento
     * @return $this
     */
    public function setSt_ocorrenciacancelamento($st_ocorrenciacancelamento)
    {
        $this->st_ocorrenciacancelamento = $st_ocorrenciacancelamento;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_ocorrenciacancelamento()
    {
        return $this->id_ocorrenciacancelamento;
    }

    /**
     * @param integer $id_ocorrenciacancelamento
     * @return $this
     */
    public function setId_ocorrenciacancelamento($id_ocorrenciacancelamento)
    {
        $this->id_ocorrenciacancelamento = $id_ocorrenciacancelamento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ocorrenciatrancamento()
    {
        return $this->bl_ocorrenciatrancamento;
    }

    /**
     * @param boolean $bl_ocorrenciatrancamento
     * @return $this
     */
    public function setBl_ocorrenciatrancamento($bl_ocorrenciatrancamento)
    {
        $this->bl_ocorrenciatrancamento = $bl_ocorrenciatrancamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_ocorrenciatrancamento()
    {
        return $this->st_ocorrenciatrancamento;
    }

    /**
     * @param string $st_ocorrenciatrancamento
     * @return $this
     */
    public function setSt_ocorrenciatrancamento($st_ocorrenciatrancamento)
    {
        $this->st_ocorrenciatrancamento = $st_ocorrenciatrancamento;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_ocorrenciatrancamento()
    {
        return $this->id_ocorrenciatrancamento;
    }

    /**
     * @param integer $id_ocorrenciatrancamento
     * @return $this
     */
    public function setId_ocorrenciatrancamento($id_ocorrenciatrancamento)
    {
        $this->id_ocorrenciatrancamento = $id_ocorrenciatrancamento;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param integer $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param integer $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param integer $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }
}

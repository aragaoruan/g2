<?php

namespace G2\Entity;

/**
 * Description of VwGerarDeclaracao
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gerardeclaracao")
 * @Entity
 * @EntityView
 */
class VwGerarDeclaracao
{

    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_usuarioaluno
     * @Column(name="id_usuarioaluno", type="integer", nullable=false, length=4)
     */
    private $id_usuarioaluno;

    /**
     * @var string $st_nomecompletoaluno
     * @Column(name="st_nomecompletoaluno", type="string", nullable=false, length=300)
     */
    private $st_nomecompletoaluno;

    /**
     * @var string $st_loginaluno
     * @Column(name="st_loginaluno", type="string", nullable=false, length=40)
     */
    private $st_loginaluno;

    /**
     * @var string $st_senhaaluno
     * @Column(name="st_senhaaluno", type="string", nullable=true, length=50)
     */
    private $st_senhaaluno;

    /**
     * @var string $st_cpfaluno
     * @Column(name="st_cpfaluno", type="string", nullable=true, length=11)
     */
    private $st_cpfaluno;

    /**
     * @var string $st_rgaluno
     * @Column(name="st_rgaluno", type="string", nullable=true, length=20)
     */
    private $st_rgaluno;

    /**
     * @var string $st_orgaoexpeditor
     * @Column(name="st_orgaoexpeditor", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpeditor;

    /**
     * @var date $dt_dataexpedicaoaluno
     * @Column(name="dt_dataexpedicaoaluno", type="date", nullable=true, length=3)
     */
    private $dt_dataexpedicaoaluno;

    /**
     * @var string $st_filiacao
     * @Column(name="st_filiacao", type="string", nullable=true, length=100)
     */
    private $st_filiacao;

    /**
     * @var string $st_datamatricula
     * @Column(name="st_datamatricula", type="string", nullable=true, length=29)
     */
    private $st_datamatricula;

    /**
     * @var integer $id_cancelamento
     * @Column(name="id_cancelamento", type="integer", nullable=true, length=4)
     */
    private $id_cancelamento;

    /**
     * @var string $st_observacaocancelamento
     * @Column(name="st_observacaocancelamento", type="string", nullable=true, length=100)
     */
    private $st_observacaocancelamento;

    /**
     * @var string $st_observacao_calculo
     * @Column(name="st_observacao_calculo", type="string", nullable=true, length=100)
     */
    private $st_observacao_calculo;

    /**
     * @var string $st_solicitacaocancelamento
     * @Column(name="st_solicitacaocancelamento", type="string", nullable=true, length=100)
     */
    private $st_solicitacaocancelamento;

    /**
     * @var string $st_valorcredito
     * @Column(name="st_valorcredito", type="string", nullable=true, length=100)
     */
    private $st_valorcredito;

    /**
     * @var string $st_validadecredito
     * @Column(name="st_validadecredito", type="string", nullable=true, length=100)
     */
    private $st_validadecredito;

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=true, length=4)
     */
    private $id_contrato;

    /**
     * @var date $dt_nascimentoaluno
     * @Column(name="dt_nascimentoaluno", type="date", nullable=true, length=3)
     */
    private $dt_nascimentoaluno;

    /**
     * @var string $st_municipioaluno
     * @Column(name="st_municipioaluno", type="string", nullable=true, length=256)
     */
    private $st_municipioaluno;

    /**
     * @var string $st_ufaluno
     * @Column(name="st_ufaluno", type="string", nullable=true, length=100)
     */
    private $st_ufaluno;

    /**
     * @var string $sg_ufaluno
     * @Column(name="sg_ufaluno", type="string", nullable=true, length=2)
     */
    private $sg_ufaluno;

    /**
     * @var string $nu_dddaluno
     * @Column(name="nu_dddaluno", type="string", nullable=true, length=3)
     */
    private $nu_dddaluno;

    /**
     * @var string $nu_telefonealuno
     * @Column(name="nu_telefonealuno", type="string", nullable=true, length=15)
     */
    private $nu_telefonealuno;

    /**
     * @var string $st_emailaluno
     * @Column(name="st_emailaluno", type="string", nullable=true, length=255)
     */
    private $st_emailaluno;

    /**
     * @var string $st_projeto
     * @Column(name="st_projeto", type="string", nullable=true, length=255)
     */
    private $st_projeto;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=false, length=255)
     */
    private $st_nivelensino;

    /**
     * @var date $dt_concluintealuno
     * @Column(name="dt_concluintealuno", type="date", nullable=true, length=3)
     */
    private $dt_concluintealuno;

    /**
     * @var string $st_coordenadorprojeto
     * @Column(name="st_coordenadorprojeto", type="string", nullable=true, length=300)
     */
    private $st_coordenadorprojeto;

    /**
     * @var integer $id_fundamentolei
     * @Column(name="id_fundamentolei", type="integer", nullable=true, length=4)
     */
    private $id_fundamentolei;

    /**
     * @var string $nu_lei
     * @Column(name="nu_lei", type="string", nullable=true, length=255)
     */
    private $nu_lei;

    /**
     * @var integer $id_fundamentoresolucao
     * @Column(name="id_fundamentoresolucao", type="integer", nullable=true, length=4)
     */
    private $id_fundamentoresolucao;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=150)
     */
    private $st_turma;

    /**
     * @var date $dt_inicioturma
     * @Column(name="dt_inicioturma", type="date", nullable=true, length=3)
     */
    private $dt_inicioturma;

    /**
     * @var date $dt_terminoturma
     * @Column(name="dt_terminoturma", type="date", nullable=true, length=3)
     */
    private $dt_terminoturma;

    /**
     * @var string $nu_resolucao
     * @Column(name="nu_resolucao", type="string", nullable=true, length=255)
     */
    private $nu_resolucao;

    /**
     * @var integer $id_fundamentoparecer
     * @Column(name="id_fundamentoparecer", type="integer", nullable=true, length=4)
     */
    private $id_fundamentoparecer;

    /**
     * @var string $nu_parecer
     * @Column(name="nu_parecer", type="string", nullable=true, length=255)
     */
    private $nu_parecer;

    /**
     * @var integer $id_fundamentoportaria
     * @Column(name="id_fundamentoportaria", type="integer", nullable=true, length=4)
     */
    private $id_fundamentoportaria;

    /**
     * @var string $nu_portaria
     * @Column(name="nu_portaria", type="string", nullable=true, length=255)
     */
    private $nu_portaria;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     * @var string $st_cnpj
     * @Column(name="st_cnpj", type="string", nullable=true, length=15)
     */
    private $st_cnpj;

    /**
     * @var string $st_urlimglogo
     * @Column(name="st_urlimglogo", type="string", nullable=true, length=1000)
     */
    private $st_urlimglogo;

    /**
     * @var string $st_urlsite
     * @Column(name="st_urlsite", type="string", nullable=true, length=200)
     */
    private $st_urlsite;

    /**
     * @var string $st_nomepais
     * @Column(name="st_nomepais", type="string", nullable=true, length=200)
     */
    private $st_nomepais;

    /**
     * @var string $sexo_aluno
     * @Column(name="sexo_aluno", type="string", nullable=true, length=1)
     */
    private $sexo_aluno;

    /**
     * @var string $st_titulomonografia
     * @Column(name="st_titulomonografia", type="string", nullable=true, length=500)
     */
    private $st_titulomonografia;

    /**
     * @var string $st_data
     * @Column(name="st_data", type="string", nullable=true, length=29)
     */
    private $st_data;

    /**
     * @var string $st_dia
     * @Column(name="st_dia", type="string", nullable=true, length=4)
     */
    private $st_dia;

    /**
     * @var string $st_mes
     * @Column(name="st_mes", type="string", nullable=true, length=60)
     */
    private $st_mes;

    /**
     * @var string $st_ano
     * @Column(name="st_ano", type="string", nullable=true, length=4)
     */
    private $st_ano;

    /**
     * @var date $dt_primeirasala
     * @Column(name="dt_primeirasala", type="date", nullable=true, length=3)
     */
    private $dt_primeirasala;

    /**
     * @var date $dt_previsaofim
     * @Column(name="dt_previsaofim", type="date", nullable=true, length=3)
     */
    private $dt_previsaofim;

    /**
     * @var string $st_titulotcc
     * @Column(name="st_titulotcc", type="string", nullable=true, length=200)
     */
    private $st_titulotcc;

    /**
     * @var date $dt_iniciotcc
     * @Column(name="dt_iniciotcc", type="date", nullable=true, length=3)
     */
    private $dt_iniciotcc;

    /**
     * @var date $dt_terminotcc
     * @Column(name="dt_terminotcc", type="date", nullable=true, length=3)
     */
    private $dt_terminotcc;

    /**
     * @var integer $nu_notatcc
     * @Column(name="nu_notatcc", type="integer", nullable=true, length=4)
     */
    private $nu_notatcc;

    /**
     * @var date $dt_cadastrotcc
     * @Column(name="dt_cadastrotcc", type="date", nullable=true, length=3)
     */
    private $dt_cadastrotcc;

    /**
     * @var date $dt_defesatcc
     * @Column(name="dt_defesatcc", type="date", nullable=true, length=3)
     */
    private $dt_defesatcc;

    /**
     * @var string $st_codcertificacao
     * @Column(name="st_codcertificacao", type="string", nullable=true, length=25)
     */
    private $st_codcertificacao;

    /**
     * @var string $st_atual
     * @Column(name="st_atual", type="string", nullable=true, length=25)
     */
    private $st_atual;

    /**
     * @var string $st_codigorastreamento
     * @Column(name="st_codigorastreamento", type="string", nullable=true, length=25)
     */
    private $st_codigorastreamento;

    /**
     * @var integer $id_matriculaorigem
     * @Column(name="id_matriculaorigem", type="integer", nullable=false, length=4)
     */
    private $id_matriculaorigem;

    /**
     * @var date $dt_iniciomatriculaorigem
     * @Column(name="dt_iniciomatriculaorigem", type="date", nullable=true, length=3)
     */
    private $dt_iniciomatriculaorigem;


    /**
     * @var string $st_terminoturmaextenso
     * @Column(name="st_terminoturmaextenso", type="string", nullable=true, length=150)
     */
    private $st_terminoturmaextenso;

    /**
     * @var string $st_inicioturmaextenso
     * @Column(name="st_inicioturmaextenso", type="string", nullable=true, length=150)
     */
    private $st_inicioturmaextenso;

    /**
     * @var string $st_atualextenso
     * @Column(name="st_atualextenso", type="string", nullable=true, length=150)
     */
    private $st_atualextenso;

    /**
     * @var string $st_limiterenovacao
     * @Column(name="st_limiterenovacao", type="string", nullable=true, length=150)
     */
    private $st_limiterenovacao;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;

    /**
     * @var integer $nu_mensalidade
     * @Column(name="nu_mensalidade", type="float", precision=30, scale=2, nullable=false)
     */
    private $nu_mensalidade;

    /**
     * @var integer $nu_mensalidadepontualidade
     * @Column(name="nu_mensalidadepontualidade", type="float", precision=30, scale=2, nullable=false)
     */
    private $nu_mensalidadepontualidade;

    /**
     * @var string $st_link_grade_portal
     * @Column(name="st_link_grade_portal", type="string", nullable=true, length=850)
     */
    private $st_link_grade_portal;

    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true, length=250)
     */
    private $st_areaconhecimento;

    /**
     * @var date $dt_agendamento
     * @Column(name="dt_agendamento", type="date", nullable=true, length=3)
     */
    private $dt_agendamento;

    /**
     * @var string $st_horarioaulaagendamento
     * @Column(name="st_horarioaulaagendamento", type="string", nullable=true, length=50)
     */
    private $st_horarioaulaagendamento;

    /**
     * @var string $st_enderecoagendamento
     * @Column(name="st_enderecoagendamento", type="string", nullable=true, length=250)
     */
    private $st_enderecoagendamento;

    /**
     * @var string $st_municipioufagendamento
     * @Column(name="st_municipioufagendamento", type="string", nullable=true, length=250)
     */
    private $st_municipioufagendamento;

    /**
     * @var string $st_disciplinaagendamento
     * @Column(name="st_disciplinaagendamento", type="string", nullable=true, length=250)
     */
    private $st_disciplinaagendamento;

    /**
     * @var integer $id_disciplinaagendamento
     * @Column(name="id_disciplinaagendamento", type="integer", nullable=false, length=4)
     */
    private $id_disciplinaagendamento;


    /**
     * @return string
     */
    public function getst_terminoturmaextenso()
    {
        return $this->st_terminoturmaextenso;
    }

    /**
     * @param string $st_terminoturmaextenso
     */
    public function setst_terminoturmaextenso($st_terminoturmaextenso)
    {
        $this->st_terminoturmaextenso = $st_terminoturmaextenso;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_inicioturmaextenso()
    {
        return $this->st_inicioturmaextenso;
    }

    /**
     * @param string $st_inicioturmaextenso
     */
    public function setst_inicioturmaextenso($st_inicioturmaextenso)
    {
        $this->st_inicioturmaextenso = $st_inicioturmaextenso;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_atualextenso()
    {
        return $this->st_atualextenso;
    }

    /**
     * @param string $st_atualextenso
     */
    public function setst_atualextenso($st_atualextenso)
    {
        $this->st_atualextenso = $st_atualextenso;
        return $this;

    }


    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwGerarDeclaracao
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuarioaluno()
    {
        return $this->id_usuarioaluno;
    }

    /**
     * @param int $id_usuarioaluno
     * @return VwGerarDeclaracao
     */
    public function setid_usuarioaluno($id_usuarioaluno)
    {
        $this->id_usuarioaluno = $id_usuarioaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomecompletoaluno()
    {
        return $this->st_nomecompletoaluno;
    }

    /**
     * @param string $st_nomecompletoaluno
     * @return VwGerarDeclaracao
     */
    public function setst_nomecompletoaluno($st_nomecompletoaluno)
    {
        $this->st_nomecompletoaluno = $st_nomecompletoaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_loginaluno()
    {
        return $this->st_loginaluno;
    }

    /**
     * @param string $st_loginaluno
     * @return VwGerarDeclaracao
     */
    public function setst_loginaluno($st_loginaluno)
    {
        $this->st_loginaluno = $st_loginaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_senhaaluno()
    {
        return $this->st_senhaaluno;
    }

    /**
     * @param string $st_senhaaluno
     * @return VwGerarDeclaracao
     */
    public function setst_senhaaluno($st_senhaaluno)
    {
        $this->st_senhaaluno = $st_senhaaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cpfaluno()
    {
        return $this->st_cpfaluno;
    }

    /**
     * @param string $st_cpfaluno
     * @return VwGerarDeclaracao
     */
    public function setst_cpfaluno($st_cpfaluno)
    {
        $this->st_cpfaluno = $st_cpfaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_rgaluno()
    {
        return $this->st_rgaluno;
    }

    /**
     * @param string $st_rgaluno
     * @return VwGerarDeclaracao
     */
    public function setst_rgaluno($st_rgaluno)
    {
        $this->st_rgaluno = $st_rgaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @param string $st_orgaoexpeditor
     * @return VwGerarDeclaracao
     */
    public function setst_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_dataexpedicaoaluno()
    {
        return $this->dt_dataexpedicaoaluno;
    }

    /**
     * @param date $dt_dataexpedicaoaluno
     * @return VwGerarDeclaracao
     */
    public function setdt_dataexpedicaoaluno($dt_dataexpedicaoaluno)
    {
        $this->dt_dataexpedicaoaluno = $dt_dataexpedicaoaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_filiacao()
    {
        return $this->st_filiacao;
    }

    /**
     * @param string $st_filiacao
     * @return VwGerarDeclaracao
     */
    public function setst_filiacao($st_filiacao)
    {
        $this->st_filiacao = $st_filiacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_datamatricula()
    {
        return $this->st_datamatricula;
    }

    /**
     * @param string $st_datamatricula
     * @return VwGerarDeclaracao
     */
    public function setst_datamatricula($st_datamatricula)
    {
        $this->st_datamatricula = $st_datamatricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param int $id_cancelamento
     * @return VwGerarDeclaracao
     */
    public function setid_cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_observacaocancelamento()
    {
        return $this->st_observacaocancelamento;
    }

    /**
     * @param string $st_observacaocancelamento
     * @return VwGerarDeclaracao
     */
    public function setst_observacaocancelamento($st_observacaocancelamento)
    {
        $this->st_observacaocancelamento = $st_observacaocancelamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_observacao_calculo()
    {
        return $this->st_observacao_calculo;
    }

    /**
     * @param string $st_observacao_calculo
     * @return VwGerarDeclaracao
     */
    public function setst_observacao_calculo($st_observacao_calculo)
    {
        $this->st_observacao_calculo = $st_observacao_calculo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_solicitacaocancelamento()
    {
        return $this->st_solicitacaocancelamento;
    }

    /**
     * @param string $st_solicitacaocancelamento
     * @return VwGerarDeclaracao
     */
    public function setst_solicitacaocancelamento($st_solicitacaocancelamento)
    {
        $this->st_solicitacaocancelamento = $st_solicitacaocancelamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_valorcredito()
    {
        return $this->st_valorcredito;
    }

    /**
     * @param string $st_valorcredito
     * @return VwGerarDeclaracao
     */
    public function setst_valorcredito($st_valorcredito)
    {
        $this->st_valorcredito = $st_valorcredito;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_validadecredito()
    {
        return $this->st_validadecredito;
    }

    /**
     * @param string $st_validadecredito
     * @return VwGerarDeclaracao
     */
    public function setst_validadecredito($st_validadecredito)
    {
        $this->st_validadecredito = $st_validadecredito;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @param int $id_contrato
     * @return VwGerarDeclaracao
     */
    public function setid_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_nascimentoaluno()
    {
        return $this->dt_nascimentoaluno;
    }

    /**
     * @param date $dt_nascimentoaluno
     * @return VwGerarDeclaracao
     */
    public function setdt_nascimentoaluno($dt_nascimentoaluno)
    {
        $this->dt_nascimentoaluno = $dt_nascimentoaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_municipioaluno()
    {
        return $this->st_municipioaluno;
    }

    /**
     * @param string $st_municipioaluno
     * @return VwGerarDeclaracao
     */
    public function setst_municipioaluno($st_municipioaluno)
    {
        $this->st_municipioaluno = $st_municipioaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_ufaluno()
    {
        return $this->st_ufaluno;
    }

    /**
     * @param string $st_ufaluno
     * @return VwGerarDeclaracao
     */
    public function setst_ufaluno($st_ufaluno)
    {
        $this->st_ufaluno = $st_ufaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getsg_ufaluno()
    {
        return $this->sg_ufaluno;
    }

    /**
     * @param string $sg_ufaluno
     * @return VwGerarDeclaracao
     */
    public function setsg_ufaluno($sg_ufaluno)
    {
        $this->sg_ufaluno = $sg_ufaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_dddaluno()
    {
        return $this->nu_dddaluno;
    }

    /**
     * @param string $nu_dddaluno
     * @return VwGerarDeclaracao
     */
    public function setnu_dddaluno($nu_dddaluno)
    {
        $this->nu_dddaluno = $nu_dddaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_telefonealuno()
    {
        return $this->nu_telefonealuno;
    }

    /**
     * @param string $nu_telefonealuno
     * @return VwGerarDeclaracao
     */
    public function setnu_telefonealuno($nu_telefonealuno)
    {
        $this->nu_telefonealuno = $nu_telefonealuno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_emailaluno()
    {
        return $this->st_emailaluno;
    }

    /**
     * @param string $st_emailaluno
     * @return VwGerarDeclaracao
     */
    public function setst_emailaluno($st_emailaluno)
    {
        $this->st_emailaluno = $st_emailaluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_projeto()
    {
        return $this->st_projeto;
    }

    /**
     * @param string $st_projeto
     * @return VwGerarDeclaracao
     */
    public function setst_projeto($st_projeto)
    {
        $this->st_projeto = $st_projeto;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     * @return VwGerarDeclaracao
     */
    public function setnu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nivelensino()
    {
        return $this->st_nivelensino;
    }

    /**
     * @param string $st_nivelensino
     * @return VwGerarDeclaracao
     */
    public function setst_nivelensino($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_concluintealuno()
    {
        return $this->dt_concluintealuno;
    }

    /**
     * @param date $dt_concluintealuno
     * @return VwGerarDeclaracao
     */
    public function setdt_concluintealuno($dt_concluintealuno)
    {
        $this->dt_concluintealuno = $dt_concluintealuno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_coordenadorprojeto()
    {
        return $this->st_coordenadorprojeto;
    }

    /**
     * @param string $st_coordenadorprojeto
     * @return VwGerarDeclaracao
     */
    public function setst_coordenadorprojeto($st_coordenadorprojeto)
    {
        $this->st_coordenadorprojeto = $st_coordenadorprojeto;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_fundamentolei()
    {
        return $this->id_fundamentolei;
    }

    /**
     * @param int $id_fundamentolei
     * @return VwGerarDeclaracao
     */
    public function setid_fundamentolei($id_fundamentolei)
    {
        $this->id_fundamentolei = $id_fundamentolei;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_lei()
    {
        return $this->nu_lei;
    }

    /**
     * @param string $nu_lei
     * @return VwGerarDeclaracao
     */
    public function setnu_lei($nu_lei)
    {
        $this->nu_lei = $nu_lei;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_fundamentoresolucao()
    {
        return $this->id_fundamentoresolucao;
    }

    /**
     * @param int $id_fundamentoresolucao
     * @return VwGerarDeclaracao
     */
    public function setid_fundamentoresolucao($id_fundamentoresolucao)
    {
        $this->id_fundamentoresolucao = $id_fundamentoresolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     * @return VwGerarDeclaracao
     */
    public function setid_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param string $st_turma
     * @return VwGerarDeclaracao
     */
    public function setst_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param date $dt_inicioturma
     * @return VwGerarDeclaracao
     */
    public function setdt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_terminoturma()
    {
        return $this->dt_terminoturma;
    }

    /**
     * @param date $dt_terminoturma
     * @return VwGerarDeclaracao
     */
    public function setdt_terminoturma($dt_terminoturma)
    {
        $this->dt_terminoturma = $dt_terminoturma;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_resolucao()
    {
        return $this->nu_resolucao;
    }

    /**
     * @param string $nu_resolucao
     * @return VwGerarDeclaracao
     */
    public function setnu_resolucao($nu_resolucao)
    {
        $this->nu_resolucao = $nu_resolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_fundamentoparecer()
    {
        return $this->id_fundamentoparecer;
    }

    /**
     * @param int $id_fundamentoparecer
     * @return VwGerarDeclaracao
     */
    public function setid_fundamentoparecer($id_fundamentoparecer)
    {
        $this->id_fundamentoparecer = $id_fundamentoparecer;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_parecer()
    {
        return $this->nu_parecer;
    }

    /**
     * @param string $nu_parecer
     * @return VwGerarDeclaracao
     */
    public function setnu_parecer($nu_parecer)
    {
        $this->nu_parecer = $nu_parecer;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_fundamentoportaria()
    {
        return $this->id_fundamentoportaria;
    }

    /**
     * @param int $id_fundamentoportaria
     * @return VwGerarDeclaracao
     */
    public function setid_fundamentoportaria($id_fundamentoportaria)
    {
        $this->id_fundamentoportaria = $id_fundamentoportaria;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_portaria()
    {
        return $this->nu_portaria;
    }

    /**
     * @param string $nu_portaria
     * @return VwGerarDeclaracao
     */
    public function setnu_portaria($nu_portaria)
    {
        $this->nu_portaria = $nu_portaria;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwGerarDeclaracao
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     * @return VwGerarDeclaracao
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_razaosocial()
    {
        return $this->st_razaosocial;
    }

    /**
     * @param string $st_razaosocial
     * @return VwGerarDeclaracao
     */
    public function setst_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cnpj()
    {
        return $this->st_cnpj;
    }

    /**
     * @param string $st_cnpj
     * @return VwGerarDeclaracao
     */
    public function setst_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    /**
     * @param string $st_urlimglogo
     * @return VwGerarDeclaracao
     */
    public function setst_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_urlsite()
    {
        return $this->st_urlsite;
    }

    /**
     * @param string $st_urlsite
     * @return VwGerarDeclaracao
     */
    public function setst_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomepais()
    {
        return $this->st_nomepais;
    }

    /**
     * @param string $st_nomepais
     * @return VwGerarDeclaracao
     */
    public function setst_nomepais($st_nomepais)
    {
        $this->st_nomepais = $st_nomepais;
        return $this;
    }

    /**
     * @return string
     */
    public function getsexo_aluno()
    {
        return $this->sexo_aluno;
    }

    /**
     * @param string $sexo_aluno
     * @return VwGerarDeclaracao
     */
    public function setsexo_aluno($sexo_aluno)
    {
        $this->sexo_aluno = $sexo_aluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_titulomonografia()
    {
        return $this->st_titulomonografia;
    }

    /**
     * @param string $st_titulomonografia
     * @return VwGerarDeclaracao
     */
    public function setst_titulomonografia($st_titulomonografia)
    {
        $this->st_titulomonografia = $st_titulomonografia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_data()
    {
        return $this->st_data;
    }

    /**
     * @param string $st_data
     * @return VwGerarDeclaracao
     */
    public function setst_data($st_data)
    {
        $this->st_data = $st_data;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_dia()
    {
        return $this->st_dia;
    }

    /**
     * @param string $st_dia
     * @return VwGerarDeclaracao
     */
    public function setst_dia($st_dia)
    {
        $this->st_dia = $st_dia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_mes()
    {
        return $this->st_mes;
    }

    /**
     * @param string $st_mes
     * @return VwGerarDeclaracao
     */
    public function setst_mes($st_mes)
    {
        $this->st_mes = $st_mes;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_ano()
    {
        return $this->st_ano;
    }

    /**
     * @param string $st_ano
     * @return VwGerarDeclaracao
     */
    public function setst_ano($st_ano)
    {
        $this->st_ano = $st_ano;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_primeirasala()
    {
        return $this->dt_primeirasala;
    }

    /**
     * @param date $dt_primeirasala
     * @return VwGerarDeclaracao
     */
    public function setdt_primeirasala($dt_primeirasala)
    {
        $this->dt_primeirasala = $dt_primeirasala;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_previsaofim()
    {
        return $this->dt_previsaofim;
    }

    /**
     * @param date $dt_previsaofim
     * @return VwGerarDeclaracao
     */
    public function setdt_previsaofim($dt_previsaofim)
    {
        $this->dt_previsaofim = $dt_previsaofim;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_titulotcc()
    {
        return $this->st_titulotcc;
    }

    /**
     * @param string $st_titulotcc
     * @return VwGerarDeclaracao
     */
    public function setst_titulotcc($st_titulotcc)
    {
        $this->st_titulotcc = $st_titulotcc;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_iniciotcc()
    {
        return $this->dt_iniciotcc;
    }

    /**
     * @param date $dt_iniciotcc
     * @return VwGerarDeclaracao
     */
    public function setdt_iniciotcc($dt_iniciotcc)
    {
        $this->dt_iniciotcc = $dt_iniciotcc;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_terminotcc()
    {
        return $this->dt_terminotcc;
    }

    /**
     * @param date $dt_terminotcc
     * @return VwGerarDeclaracao
     */
    public function setdt_terminotcc($dt_terminotcc)
    {
        $this->dt_terminotcc = $dt_terminotcc;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_notatcc()
    {
        return $this->nu_notatcc;
    }

    /**
     * @param int $nu_notatcc
     * @return VwGerarDeclaracao
     */
    public function setnu_notatcc($nu_notatcc)
    {
        $this->nu_notatcc = $nu_notatcc;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_cadastrotcc()
    {
        return $this->dt_cadastrotcc;
    }

    /**
     * @param date $dt_cadastrotcc
     * @return VwGerarDeclaracao
     */
    public function setdt_cadastrotcc($dt_cadastrotcc)
    {
        $this->dt_cadastrotcc = $dt_cadastrotcc;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_defesatcc()
    {
        return $this->dt_defesatcc;
    }

    /**
     * @param date $dt_defesatcc
     * @return VwGerarDeclaracao
     */
    public function setdt_defesatcc($dt_defesatcc)
    {
        $this->dt_defesatcc = $dt_defesatcc;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codcertificacao()
    {
        return $this->st_codcertificacao;
    }

    /**
     * @param string $st_codcertificacao
     * @return VwGerarDeclaracao
     */
    public function setst_codcertificacao($st_codcertificacao)
    {
        $this->st_codcertificacao = $st_codcertificacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_atual()
    {
        return $this->st_atual;
    }

    /**
     * @param string $st_atual
     * @return VwGerarDeclaracao
     */
    public function setst_atual($st_atual)
    {
        $this->st_atual = $st_atual;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codigorastreamento()
    {
        return $this->st_codigorastreamento;
    }

    /**
     * @param string $st_codigorastreamento
     * @return VwGerarDeclaracao
     */
    public function setst_codigorastreamento($st_codigorastreamento)
    {
        $this->st_codigorastreamento = $st_codigorastreamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    /**
     * @param int $id_matriculaorigem
     * @return VwGerarDeclaracao
     */
    public function setid_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_iniciomatriculaorigem()
    {
        return $this->dt_iniciomatriculaorigem;
    }

    /**
     * @param date $dt_iniciomatriculaorigem
     * @return VwGerarDeclaracao
     */
    public function setdt_iniciomatriculaorigem($dt_iniciomatriculaorigem)
    {
        $this->dt_iniciomatriculaorigem = $dt_iniciomatriculaorigem;
        return $this;
    }


    public function getst_limiterenovacao()
    {
        return $this->st_limiterenovacao;
    }

    public function setst_limiterenovacao($st_limiterenovacao)
    {
        $this->st_limiterenovacao = $st_limiterenovacao;
    }


    public function getid_venda()
    {
        return $this->id_venda;
    }

    public function setid_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    public function getnu_mensalidade()
    {
        return $this->nu_mensalidade;
    }


    public function setnu_mensalidade($nu_mensalidade)
    {
        $this->nu_mensalidade = $nu_mensalidade;
    }

    public function getnu_mensalidadepontualidade()
    {
        return $this->nu_mensalidadepontualidade;
    }


    public function setnu_mensalidadepontualidade($nu_mensalidadepontualidade)
    {
        $this->nu_mensalidadepontualidade = $nu_mensalidadepontualidade;
    }

    public function getst_linkgradeportal()
    {
        return $this->st_link_grade_portal;
    }

    public function setst_linkgradeportal($st_link_grade_portal)
    {
        $this->st_link_grade_portal = $st_link_grade_portal;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return date
     */
    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @param date $dt_agendamento
     */
    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    /**
     * @return string
     */
    public function getSt_horarioaulaagendamento()
    {
        return $this->st_horarioaulaagendamento;
    }

    /**
     * @param string $st_horarioaulaagendamento
     */
    public function setSt_horarioaulaagendamento($st_horarioaulaagendamento)
    {
        $this->st_horarioaulaagendamento = $st_horarioaulaagendamento;
    }

    /**
     * @return string
     */
    public function getSt_enderecoagendamento()
    {
        return $this->st_enderecoagendamento;
    }

    /**
     * @param string $st_enderecoagendamento
     */
    public function setSt_enderecoagendamento($st_enderecoagendamento)
    {
        $this->st_enderecoagendamento = $st_enderecoagendamento;
    }

    /**
     * @return string
     */
    public function getStMunicipioufagendamento()
    {
        return $this->st_municipioufagendamento;
    }

    /**
     * @param string $st_municipioufagendamento
     */
    public function setSt_municipioufagendamento($st_municipioufagendamento)
    {
        $this->st_municipioufagendamento = $st_municipioufagendamento;
    }

    /**
     * @return string
     */
    public function getSt_disciplinaagendamento()
    {
        return $this->st_disciplinaagendamento;
    }

    /**
     * @param string $st_disciplinaagendamento
     */
    public function setSt_disciplinaagendamento($st_disciplinaagendamento)
    {
        $this->st_disciplinaagendamento = $st_disciplinaagendamento;
    }

    /**
     * @return int
     */
    public function getId_disciplinaagendamento()
    {
        return $this->id_disciplinaagendamento;
    }

    /**
     * @param int $id_disciplinaagendamento
     */
    public function setId_disciplinaagendamento($id_disciplinaagendamento)
    {
        $this->id_disciplinaagendamento = $id_disciplinaagendamento;
    }





}